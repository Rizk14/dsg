<div class="container" id="daftar-sukses">
    <p class="h2">Informasi Untuk Penginputan Master jenis Bahan Baku</p>
    <hr>

    <div class="alert alert-success alert-dismissible" role="alert">
        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Penginputan Berhasil.
    </div>
    
    <p>Anda dapat kembali ke halaman Master jenis Bahan Baku di <?php echo anchor('/master_jenis_bb/master_jenis_bb/view', 'Master jenis Bahan Baku'); ?>.</p>

</div> <!-- container -->
