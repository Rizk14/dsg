<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript">
$(document).ready(function() {
 
 $(".xhapus").click(function(){
 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var hps = element.attr("id");
 
 //Built a url to send
 var info = 'pid=' + hps;
 if(confirm("Yakin akan menghapus data dgn id: "+hps+" ?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('/kodevoucher/cform/actdelete')?>",
 data: info,
 success: function(){
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
 
})

function konfirm() {
	
	if(document.getElementById('ijnsvoucher').value=='') {
		alert("Maaf, jenis voucher yg diisi!");
		document.getElementById('ijnsvoucher').focus();
		return false;
	}

	if(document.getElementById('kodevoucher').value=='') {
		alert("Maaf, kode voucher yg diisi!");
		document.getElementById('kodevoucher').focus();
		return false;
	}

	if(document.getElementById('ketvoucher').value=='') {
		alert("Maaf, keterangan voucher yg diisi!");
		document.getElementById('ketvoucher').focus();
		return false;
	}	
}

</script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div id="tabs">
	<div id="tab1" class="tab_sel_kodevoucher" align="center" onClick="javascript: displayKodeVoucher('1');"><?php echo $form_panel_daftar_kodevoucher; ?></div>
<!--
	<div id="tab2" class="tab_kodevoucher" style="margin-left:1px;" align="center" onClick="javascript: displayKodeVoucher('2');"><?php echo $form_panel_form_kodevoucher; ?></div>
	<div id="tab3" class="tab_kodevoucher" style="margin-left:1px;" align="center" onClick="javascript: displayKodeVoucher('3');"><?php echo $form_panel_cari_kodevoucher; ?></div>
-->
</div>
        <a href="<?php echo base_url(); ?>index.php/kodevoucher/cform/tambah">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/kodevoucher/cform">View Data</a>&nbsp;<br><br>
          
<div class="tab_bdr_kodevoucher"></div>
	<div class="panel" id="panel1" style="display:block;">
	<?php
	if( !empty($query) || isset($query) ) {
		
		$cc		= 1;
		
		foreach($query as $row) {
			
			$Classnya = $cc % 2 == 0 ? "row" :"row1";
			$list .= "
			 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
			  <td>".$cc."</td>
			  <td>".$row->i_voucher_code."&nbsp;</td>	 
			  <td>".$row->e_voucher_name."&nbsp;</td>
			  <td>".$row->e_description."&nbsp;</td>
			   <td align='center'><a href=".base_url()."index.php/kodevoucher/cform/edit/".$row->i_voucher.">
			   <img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;
			   <a href=".base_url()."index.php/kodevoucher/cform/actdelete/".$row->i_voucher.">
			   <img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13></a></td>
			 </tr>";
			 
			$cc+=1;
		}
	}
	?>

	<table class="listtable_kodevoucher">
	 <th width="5">No</th>
	 <th width="40"><?php echo $form_kode_kodevoucher; ?></th>
	 <th width="40"><?php echo $form_nama_kodevoucher; ?></th>
	 <th width="100"><?php echo $form_ket_kodevoucher; ?></th>
	 <th width="10" class="action">Aksi</th>
	<tbody>
	<?php
	if( !empty($list) && !isset($not_defined) ) {
		echo $list."</tbody></table>";
	} else {
		echo $list."</tbody></table>
		<div style=\"font-family:arial; font-size:11px; color:#FF0000;\">".strtoupper($not_defined)."</div>";
	}
	?>

	<table align="center" width="100%">
	  <tr>
	  	<td align="center"><?php echo $create_links; ?></span></td>
	  </tr>
	</table>	
	</div>

	<div class="panel" id="panel2" style="display: none">
	    <table>
	      <tr>
	    	<td align="left">
		      <?php
			echo $this->pquery->form_remote_tag(array('url'=>'kodevoucher/cform/simpan','update'=>'#content','type'=>'post'));
		      ?>
		<div id="masterkodevoucherform">
		      <table>
		      	<tr>
			  <td width="200px"><?php echo $form_jns_kodevoucher; ?></td>
			  <td width="1px">:</td>
			  <td width="280px">
			    <?php
			    $ijns = array(
				'name'=>'ijnsvoucher',
				'id'=>'ijnsvoucher',
				'onclick'	=>'ckodevoucher();',
				'value'=>'',
				'maxlength'=>'20');
			    echo form_input($ijns); ?>
			    <input type="hidden" name="ijnsvoucherhidden" id="ijnsvoucherhidden" />
			  </td>
			</tr>			  
		      	<tr>
			  <td><?php echo $form_kode_kodevoucher; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    
			    $kodevoucher = array(
				'name'=>'kodevoucher',
				'id'=>'kodevoucher',
				'value'=>'',
				'maxlength'=>'5');
				echo form_input($kodevoucher);
				 
			    ?>		
			  </td>
			</tr>
		      	<tr>
			  <td><?php echo $form_nama_kodevoucher; ?></td>
			  <td>:</td>
			  <td>
			    <select name="nmvoucher" id="nmvoucher">
					<option value="reciept">reciept</option>
					<option value="payment">payment</option>
			    </select>		    		    
			  </td>
			</tr>
			</tr>
		      	<tr>
			  <td><?php echo $form_ket_kodevoucher; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    $ketvoucher = array(
				'name'=>'ketvoucher',
				'id'=>'ketvoucher',
				'value'=>'',
				'maxlength'=>'255' );
			    echo form_input($ketvoucher); ?>
			  </td>
			</tr>								
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			    <input name="tblsimpan" id="tblsimpan" value="Simpan" type="submit" onclick="return konfirm();" />
			    <input name="btnbatal" id="btnbatal" value="Batal" type="button" onclick='show("kodevoucher/cform/","#content")' />
			  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
	<div class="panel" id="panel3" style="display:none">
		<table>
			<tr>
			  <td align="left">
			    <?php echo $this->pquery->form_remote_tag(array('url'=>'kodevoucher/cform/cari','update'=>'#content','type'=>'post'));?>
				<table class="listtable">
				<thead>
			      	 <tr>
				   <td><?php echo $txt_cari; ?>:
				    <input type="text" name="txtcari" id="txtcari" value="" maxlength='50'><input type="submit" id="btncari" name="btncari" value="Pencarian">
					</td>
				 </tr>
				</thead>
				</table>
			  </td>
			</tr>
		</table>
	</div>
