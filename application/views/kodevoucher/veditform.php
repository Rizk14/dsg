<div id="tabs">
	<div id="tab2" class="tab_kodevoucher" style="margin-left:1px;" align="center" onClick="javascript: displayKodeVoucher('2');"><?php echo $form_panel_form_kodevoucher; ?></div>
</div>

<div class="tab_bdr"></div>

	<div class="panel" id="panel2" style="display:block;">
	    <table class="maintable">
	      <tr>
	    	<td align="left">
				<?php
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('kodevoucher/cform/actedit', $attributes);?>
		     
		<div id="masterkodevoucherform">
		<div class="effect">
		  <div class="accordion2">
		      <table class="mastertable">
		      	<tr>
			  <td width="200px"><?php echo $form_jns_kodevoucher; ?></td>
			  <td width="1px">:</td>
			  <td width="280px">
			    <?php
			    $ijns = array(
				'name'=>'ijnsvoucher',
				'id'=>'ijnsvoucher',
				'onclick'	=>'ckodevoucher();',
				'value'=>$e_jenis_voucher,
				'maxlength'=>'5');
			    echo form_input($ijns); ?>
			    <input type="hidden" name="ijnsvoucherhidden" id="ijnsvoucherhidden" value="<?=$i_jenis_voucher?>" />
			  </td>
			</tr>			  
		      	<tr>
			  <td><?php echo $form_kode_kodevoucher; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    $kodevoucher = array(
				'name'=>'kodevoucher',
				'id'=>'kodevoucher',
				'value'=>$i_voucher_code,
				'maxlength'=>'5');
				echo form_input($kodevoucher);
			    ?>		
			  </td>
			</tr>
		      	<tr>
			  <td><?php echo $form_nama_kodevoucher; ?></td>
			  <td>:</td>
			  <td>
			    <select name="nmvoucher" id="nmvoucher">
					<option value="reciept" <?php if($e_voucher_name=='reciept') echo "selected"; ?> >reciept</option>
					<option value="payment" <?php if($e_voucher_name=='payment') echo "selected"; ?> >payment</option>
			    </select>			    
			  </td>
			</tr>
			</tr>
		      	<tr>
			  <td><?php echo $form_ket_kodevoucher; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    $ketvoucher = array(
				'name'=>'ketvoucher',
				'id'=>'ketvoucher',
				'value'=>$e_description,
				'maxlength'=>'255' );
			    echo form_input($ketvoucher); ?>
			  </td>	
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			  	<input type="hidden" name="ivoucher" id="ivoucher" value="<?php echo $i_voucher; ?>" />
			    <input name="tblsimpan" id="tblsimpan" value="Simpan" type="submit">
			     <input name="btnbatal" id="btnbatal" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/kodevoucher/cform/'">

			  </td>
		       </tr>
		      </table>
			</div>
		</div>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
