<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Marker Gelaran/Set</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-marker/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-marker/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-marker/cform/cari'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>Kode & Nama Brg Jadi </th>
		 <th>Motif </th>
		 <th>Kode & Nama Bahan Baku</th>
		 <th>Diprint/bordir</th>
		 <th>Diquilting</th>
		 <th>Nama Bhn Quilting</th>
		 <th>Gelaran</th>
		 <th>Set</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."</td>";
				 echo    "<td nowrap>";
				 if ($query[$j]['id_motif'] == '0')
					echo " Tidak Ada ";
				 else
					echo $query[$j]['nama_motif'];
				 echo "</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>";
			     
			     if ($query[$j]['diprint'] == 't')
					echo    "<td align='center'>Ya</td>";
				 else
					echo    "<td align='center'>Tidak</td>";
			     
			     if ($query[$j]['diquilting'] == 't')
					echo    "<td align='center'>Ya</td>";
				 else
					echo    "<td align='center'>Tidak</td>";
								     
			     if ($query[$j]['kode_brg_quilting'] != '')
					echo    "<td nowrap>".$query[$j]['kode_brg_quilting']." - ".$query[$j]['nama_brg_quilting']."</td>";
				 else
					echo "<td>&nbsp;</td>";
				 echo    "<td align='right'>".$query[$j]['gelaran']."</td>";
				 echo    "<td align='right'>".$query[$j]['jum_set']."</td>";
				 echo    "<td nowrap align='center'>".$query[$j]['tgl_update']."</td>";
				
				 $ada = 0;
				 // cek apakah data kode sudah dipake di tabel tm_pb_cutting
				 $query3	= $this->db->query(" SELECT id FROM tm_pb_cutting_detail WHERE id_marker_gelaran = ".$query[$j]['id']);
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 if ($query[$j]['status_aktif'] == 't') {
					$statusnya = "Non-Aktifkan";
					$aksi = "off";
				}
				else {
					$statusnya = "Aktifkan";
					$aksi = "on";
				}
				
					 echo    "<td align=center><a href=".base_url()."index.php/mst-marker/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>";
				if ($ada ==0)
					 echo    "&nbsp; <a href=".base_url()."index.php/mst-marker/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				echo "&nbsp;<a href=".base_url()."index.php/mst-marker/cform/updatestatus/".$query[$j]['id']."/".$aksi."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Update status aktifnya?')\">".$statusnya."</a>";
				echo "</td>";
				
				
					 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
