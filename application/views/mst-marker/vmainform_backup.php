<h3>Data Marker Gelaran/Set</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-marker/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-marker/cform/view">View Data</a><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_input() {
	var brg_jadi= $('#brg_jadi').val();
	var kode_brg_jadi= $('#kode_brg_jadi').val();
	var bhn_baku= $('#bhn_baku').val();
	var kode_bhn_baku= $('#kode_bhn_baku').val();
	var gelaran= $('#gelaran').val();
	var jum_set= $('#jum_set').val();
	
	if (brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		$('#brg_jadi').focus();
		return false;
	}
	
	if (bhn_baku == '') {
		alert("Bahan Baku/Quilting harus dipilih..!");
		$('#bhn_baku').focus();
		return false;
	}
	if (gelaran == '' || gelaran == '0') {
		alert("Gelaran tidak boleh kosong/nol..!");
		$('#gelaran').focus();
		return false;
	}
	
	if (isNaN(gelaran)) {
		alert("Gelaran harus berupa angka..!");
		$('#gelaran').focus();
		return false;
	}
	
	if (jum_set == '' || jum_set == '0') {
		alert("Jumlah set tidak boleh kosong/nol..!");
		$('#jum_set').focus();
		return false;
	}
	if (isNaN(jum_set)) {
		alert("Jumlah set harus berupa angka..!");
		$('#jum_set').focus();
		return false;
	}	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/mst-marker/cform/submit" method="post" enctype="multipart/form-data">

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	
<tr>
	<td width="15%">Barang Jadi</td>
	<td><input name="brg_jadi" type="text" id="brg_jadi" size="30" readonly="true" value=""/>
           <input name="kode_brg_jadi" type="hidden" id="kode_brg_jadi" value=""/>
           <input title="browse data brg jadi" name="pilih_brg_jadi" value="..." type="button" id="pilih_brg_jadi" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/mst-marker/cform/show_popup_brg_jadi/');" type="button"></td>
  </tr>
<tr>
	<td>Nama Bahan</td>
	<td>Quilting <input type="checkbox" name="quilting" id="quilting" value="t" onclick="$('#kode_bhn_baku').val(''); $('#bhn_baku').val(''); ">
			<input name="bhn_baku" type="text" id="bhn_baku" size="30" readonly="true" value=""/>
           <input name="kode_bhn_baku" type="hidden" id="kode_bhn_baku" value=""/>
           <input title="browse data bhn baku" name="pilih_bhn_baku" value="..." type="button" id="pilih_bhn_baku" 
           onclick="javascript: var q = ''; if ($('input[name=quilting]').is(':checked')) { q= 't'; } else { q='f'; } 
           openCenteredWindow('<?php echo base_url(); ?>index.php/mst-marker/cform/show_popup_brg/'+q);" type="button"></td>
  </tr>  
  <tr>
    <td>Gelaran</td>
    <td>
      <input name="gelaran" type="text" id="gelaran" size="5" maxlength="10" value="0">
    </td>
  </tr>
  <tr>
    <td>Set</td>
    <td>
      <input name="jum_set" type="text" id="jum_set" size="5" maxlength="10" value="0">
    </td>
  </tr>
  
  <tr><td>&nbsp;</td>
	<td><input type="submit" name="submit" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-marker/cform/view'"></td>
  </tr>

</table>
</form>
