<h3>Data Faktur Quilting</h3><br>
<a href="<? echo base_url(); ?>index.php/faktur-quilting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/faktur-quilting/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	//$("#no").val('2');
	
	//hitung_total();
	
	$('#unit_makloon').change(function(){
		$("#sj_masuk").val('');
	  	$("#id_sj").val('');	
	  });
	  
	  hitungnilai();
		
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 1200;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

/*function hitung_total() {
	var jum_detail = $('#no').val()-1;
	
	var i=1;
	var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var biaya=$("#biaya_"+i).val(); 
					if (biaya == '')
						biaya = 0;

					gtotal = parseFloat(gtotal)+parseFloat(biaya);
					gtotal = gtotal.toFixed(2);
					$("#totalnya").val(gtotal);
				}
    
} */

function cek_item_brg() {
	var sj_masuk= $('#sj_masuk').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	
	if (jenis_pembelian == '0') {
		alert("Jenis Pembelian harus dipilih..!");
		$('#jenis_pembelian').focus();
		return false;
	}
	
	if (sj_masuk == '') {
		alert("Nomor SJ Masuk harus dipilih..!");
		return false;
	}
}

function cek_input() {
	var no_sj= $('#no_sj').val();
	var tgl= $('#tgl_sj').val();
		
	if (no_sj == '') {
		alert("Nomor SJ Masuk harus diisi..!");
		return false;
	}
	if (tgl == '') {
		alert("Tanggal SJ Masuk harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#qty_hasil_'+k).val() == '0' || $('#qty_hasil_'+k).val() == '' ) {				
				alert("Data qty hasil quilting tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_hasil_'+k).val()) ) {
				alert("Qty hasil quilting harus berupa angka..!");
				return false;
			}
			
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
	var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty_hasil=$("#qty_hasil_"+i).val();
					var harga=$("#harga_"+i).val();
					var hitung = harga*qty_hasil;
					$("#total_"+i).val(hitung);
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				$("#gtotal").val(gtotal);
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
	$attributes = array('name' => 'f_makloon', 'id' => 'f_makloon');
	echo form_open('faktur-quilting/cform/', $attributes);
?>
<table>
	<tr>
		<td width="20%">Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian" onkeyup="this.blur();this.focus();">
					<option value="0">-Pilih-</option>
					<option value="1">Cash</option>
					<option value="2">Kredit</option>
				</select>&nbsp;</td> 
	</tr>
	<tr>
		<td>Unit Makloon</td>
		<td><select name="unit_makloon" id="unit_makloon" onkeyup="this.blur();this.focus();">
				<?php foreach ($list_quilting as $quilting) { ?>
					<option value="<?php echo $quilting->kode_supplier ?>"><?php echo $quilting->kode_supplier." - ". $quilting->nama ?></option>
				<?php } ?>
			</select></td> 
</tr>
	<tr>
		<td>Nomor SJ Masuk</td>
		<td><input name="sj_masuk" type="text" id="sj_masuk" size="40" readonly="true" value=""/>
			<input name="id_sj" type="hidden" id="id_sj" value=""/>
			<input title="browse data SJ masuk" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: var x= $('#unit_makloon').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/faktur-quilting/cform/show_popup_sj_masuk/'+ x);" type="button"></td>
	</tr>
	
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-quilting/cform/view'">
<?php echo form_close(); 
} else { ?>


<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-quilting/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="unit_makloon" value="<?php echo $unit_makloon ?>">
<input type="hidden" name="jenis_pembelian" value="<?php echo $jenis_pembelian ?>">

<?php 
		if (count($sj_detail)>0) {
			$no=1;
			foreach ($sj_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<div align="center">

<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
			<td width="15%">Jenis Pembelian</td>
			<td width="70%"><?php if ($jenis_pembelian == 1) echo "Cash"; else echo "Kredit"; ?></td> 
	</tr>
  <tr>

<tr>
		<td>Unit Makloon</td>
		<td><?php echo $unit_makloon." - ". $nama_unit ?></td> 
</tr>
  <tr>
    <td>No SJ Masuk</td>
    <td>
      <input name="no_sj_masuk" type="text" id="no_sj_masuk" size="30" readonly="true" value="<?php echo $no_sj_masuk; ?>">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Nomor Faktur</td>
    <td>
      <input name="no_faktur" type="text" id="no_faktur" size="20" maxlength="20" value="">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Tgl Faktur</td>
    <td>
	<label>
      <input name="tgl_faktur" type="text" id="tgl_faktur" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_faktur" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_faktur,'dd-mm-yyyy',this)">
	</td>
  </tr>
	
	<tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>No / Tgl SJ Masuk</th>
          <!-- <th>Utk Brg Jadi</th> -->
           <th>Kode & Nama Bhn Baku</th>
           <th>Kode & Nama Bhn Quilting</th>
	      <th>Qty Hsl Quilting (Yard)</th>
	     <!-- <th>Detail Pjg Hsl Quilting (Yard)</th> -->
	      <th>Harga</th>
	      <th>Total</th>
        </tr>

        <?php $i=1;
        if (count($sj_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($sj_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          
          <td nowrap="nowrap">
           <input name="no_sj_masuk_<?php echo $i ?>" type="text" id="no_sj_masuk_<?php echo $i ?>" size="25" readonly="true" value="<?php echo $sj_detail[$j]['no_sj']." / ".$sj_detail[$j]['tgl_sj'] ?>"/>
           </td>
          
       <!--   <td nowrap="nowrap">
           <input name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="30" readonly="true" value="<?php echo  $sj_detail[$j]['kode_brg_jadi']." - ".$sj_detail[$j]['nama_brg_jadi'] ?>"/>
           <input type="hidden" name="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['kode_brg_jadi'] ?>" >
           </td> -->
          
          <td nowrap="nowrap">
           <input name="bhn_baku_<?php echo $i ?>" type="text" id="bhn_baku_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg']." - ".$sj_detail[$j]['nama_brg'] ?>"/>
           <input type="hidden" name="id_sj_detail_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['id'] ?>" >
           <input type="hidden" name="kode_brg_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['kode_brg'] ?>" >
           </td>
           <td nowrap="nowrap">
           <input name="brg_makloon_<?php echo $i ?>" type="text" id="brg_makloon_<?php echo $i ?>" size="40" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg_makloon']." - ".str_replace("\"", "&quot;", $sj_detail[$j]['nama_brg_makloon']) ?>"/>
           <input type="hidden" name="kode_brg_makloon_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['kode_brg_makloon'] ?>" >
           </td>

          <td><input name="qty_hasil_<?php echo $i ?>" type="text" id="qty_hasil_<?php echo $i ?>" size="5" maxlength="5" 
          value="<?php echo $sj_detail[$j]['qty_makloon'] ?>" readonly="true"  /> </td>
          
         <!-- <td nowrap="nowrap">
           <input name="detail_yard_<?php echo $i ?>" type="text" id="detail_yard_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['detail_pjg_quilting'] ?>"/>
          </td> -->
          
          <td nowrap="nowrap">
           <input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="10" value="<?php echo $sj_detail[$j]['harganya'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()"/>
           <input name="harga_lama_<?php echo $i ?>" type="hidden" id="harga_lama_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['harganya'] ?>" />
          </td>
          <td nowrap="nowrap">
           <input readonly="true" name="total_<?php echo $i ?>" type="text" id="total_<?php echo $i ?>" size="10" value=""/>
          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
	</table>	
	
	</form>
</td>
    </tr>
    <tr>
		<td>Grand Total</td>
		<td>: <input type="text" name="gtotal" id="gtotal" value="0" size="10" readonly="true"></td>
	</tr>
    <tr>
		<td colspan="2" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-quilting/cform/'"></td>
	</tr>
	
</table>
</div>
</form>
<?php } ?>
