<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Faktur Quilting</h3><br>
<a href="<? echo base_url(); ?>index.php/faktur-quilting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/faktur-quilting/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('faktur-quilting/cform/cari'); ?>
Unit Makloon: <select name="unit_makloon" id="unit_makloon">
				<option value="0" <?php if ($unit_makloon == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_unit as $unitnya) { ?>
					<option value="<?php echo $unitnya->kode_supplier ?>" <?php if ($unit_makloon == $unitnya->kode_supplier) { ?> 
					selected="true" <?php } ?> ><?php echo $unitnya->kode_supplier." - ". $unitnya->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>Jenis Pembelian </th>
		 <th>No/Tgl Faktur </th>
		 <th>No/Tgl SJ</th>
		 <th>Unit Makloon</th>
		 <th>Jum Total (Rp.)</th>
		 <th>Status Lunas</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){								 
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>";
					if ($query[$j]['jenis_pembelian'] == 1) echo "Cash"; else echo "Kredit";
				 echo "</td>";
				 echo    "<td nowrap>".$query[$j]['no_faktur']." / ".$query[$j]['tgl_faktur']."</td>";

				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";

				 echo    "<td>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['totalnya'], 2, ',','.')."</td>";
				 echo "<td>";
					if ($query[$j]['status_lunas'] == 't')
						echo "Lunas";
					else
						echo "Belum";
				 echo "</td>";
				 echo    "<td>".$query[$j]['tgl_update']."</td>";
				
				if ($query[$j]['status_faktur_pajak'] == 'f') {
					if ($query[$j]['status_lunas'] == 'f') {
					 echo    "<td align=center>
					 <a href=".base_url()."index.php/faktur-quilting/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$unit_makloon."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>";
					 echo    "&nbsp; <a href=".base_url()."index.php/faktur-quilting/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$unit_makloon."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>
					</td>";
					}
					else
						echo "<td align=center>&nbsp;</td>";
				}
				else {
					echo "<td align=center>&nbsp;</td>";
				}
				
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
