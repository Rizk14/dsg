<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Material Barang Jadi</h3><br>
<a href="<? echo base_url(); ?>index.php/brgjadi_hpp/cform/addmaterialbrgjadi">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/brgjadi_hpp/cform/viewmaterialbrgjadi">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('brgjadi_hpp/cform/carimaterialbrgjadi'); ?>

<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Kode</th>
		 <th>Nama Barang Jadi</th>
		 <th>List Bahan Baku</th>
		 <th>Warna Utk Brg Jadi</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
			
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td align='right'>$i &nbsp;</td>";
				 echo    "<td>".$query[$j]['i_product_motif']."&nbsp;</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['e_product_motifname']."&nbsp;</td>";
				 echo "<td style='white-space:nowrap;'>";
				   if (is_array($query[$j]['detailbrg'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailbrg'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					 }
				  }
				  echo "</td>";
				  
				  echo "<td>";
				   if (is_array($query[$j]['detailbrg'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailbrg'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_warna'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					 }
				  }
				  echo "</td>";
				
				 $ada = 0;
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail WHERE kode_brg_jadi = '".$query[$j]['i_product_motif']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE kode_brg_jadi = '".$query[$j]['i_product_motif']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail WHERE kode_brg_jadi = '".$query[$j]['i_product_motif']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				
				 echo    "<td align=center><a href=".base_url()."index.php/brgjadi_hpp/cform/editmaterialbrgjadi/".$query[$j]['i_product_motif']." \" >Edit</a>";
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/brgjadi_hpp/cform/deletematerialbrgjadi/".$query[$j]['i_product_motif']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";				 
				 
				 echo "</td>";
				 echo  "</tr>";
				 $i++;
		 	} // end for
		 }
		 ?>
 	</tbody>
</table><br>
<? echo $this->pagination->create_links();?>
</div>
