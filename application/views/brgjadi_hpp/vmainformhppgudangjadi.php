<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Harga Pokok Produksi (HPP) Gudang Jadi</h3><br>
<a href="<?php echo base_url(); ?>index.php/brgjadi_hpp/cform/addhppgudangjadi">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/brgjadi_hpp/cform/viewhppgudangjadi">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

// --------- 02-04-2014 ------------------
$(function()
{
	//$("#no").val('2');
	
	//var is_new = $('#is_new').val(); 
	//if(is_new == 1)
	//	$("#jum_data").val('2');
	
	//generate_nomor();		
	$("#additem").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#jum_data").val();		
		var n=parseInt(no)-1;
		//var n=parseInt(no);
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode_brg_jadi*************************************
		var kode_brg_jadi="#kode_brg_jadi_"+n;
		var new_kode_brg_jadi="#kode_brg_jadi_"+no;
		$(kode_brg_jadi, lastRow).attr("id", "kode_brg_jadi_"+no);
		$(new_kode_brg_jadi, lastRow).attr("name", "kode_brg_jadi_"+no);		
		$(new_kode_brg_jadi, lastRow).attr("onkeyup", "cari('"+no+"', this.value);");		
		$(new_kode_brg_jadi, lastRow).val('');		
		//*****end kode_brg_jadi*********************************
												
		//*****nama_brg_jadi*************************************
		var nama_brg_jadi="#nama_brg_jadi_"+n;
		var new_nama_brg_jadi="#nama_brg_jadi_"+no;
		$(nama_brg_jadi, lastRow).attr("id", "nama_brg_jadi_"+no);
		$(new_nama_brg_jadi, lastRow).attr("name", "nama_brg_jadi_"+no);		
		$(new_nama_brg_jadi, lastRow).val('');				
		//*****end nama_brg_jadi*************************************
										
		//******div infobrgjadi*************************************
		var infobrgjadi="#infobrgjadi_"+n;
		var new_infobrgjadi="#infobrgjadi_"+no;
		$(infobrgjadi, lastRow).attr("id", "infobrgjadi_"+no);

		$(new_infobrgjadi, lastRow).html("<input type='text' id='nama_brg_jadi_"+no+"' name='nama_brg_jadi_"+no+"' value='' readonly='true' size='40'>");
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('0');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
		var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('0');				
		//*****end harga_lama*************************************	
				
		//*****bulan*************************************
		var bulan="#bulan_"+n;
		var new_bulan="#bulan_"+no;
		$(bulan, lastRow).attr("id", "bulan_"+no);
		$(new_bulan, lastRow).attr("name", "bulan_"+no);		
		$(new_bulan, lastRow).val('0');				
		//*****end harga*************************************	
		
		//*****tahun*************************************
		var tahun="#tahun_"+n;
		var new_tahun="#tahun_"+no;
		$(tahun, lastRow).attr("id", "tahun_"+no);
		$(new_tahun, lastRow).attr("name", "tahun_"+no);		
		$(new_tahun, lastRow).val('');				
		//*****end harga*************************************													
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//jum_data++
		var x=parseInt(no);
		$("#jum_data").val(x+1);	
		
	});	
	
	$("#deleteitem").click(function()
	{
		var x= $("#jum_data").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#jum_data").val();	
			var y= x-1;
			$("#jum_data").val(y);	
		}
	});	
	
});
// ---------------------------------------

function cek_input() {
	var jum_data = $('#jum_data').val()-1;
	var s = 0;
	kon = window.confirm("Yakin akan simpan data HPP gudang jadi ??");
		
	if (kon) {
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
			/*	var kode_brg_jadi=$("#kode_brg_jadi_"+i).val();
				var nama_brg_jadi=$("#nama_brg_jadi_"+i).val();
				var harga=$("#harga_"+i).val(); */
				
				if ($('#kode_brg_jadi_'+k).val() == '') {
					alert("Kode barang harus diinput..!");
					s=1;
					return false;
				}
				if ($('#nama_brg_jadi_'+k).val() == '' ) {
					alert("Barang Jadi tidak valid..!");
					s=1;
					return false;
				}
				if (isNaN($('#harga_'+k).val()) || $('#harga_'+k).val() == '') {
					alert("Data harga harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				if ($('#harga_'+k).val() == '0') {
					alert("Data harga tidak boleh 0..!");
					s=1;
					return false;
				}
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}

function cari(posisi, kodebrgjadi) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/brgjadi_hpp/cform/caribrgjadi', 
				data: 'kode_brg_jadi='+kodebrgjadi+'&posisi='+posisi, success: function(response) {
					$("#infobrgjadi_"+posisi).html(response);
					window.close();
			}});

}
</script>

<form name="f_hpp" id="f_hpp" action="<?php echo base_url(); ?>index.php/brgjadi_hpp/cform/submithppgudangjadi" method="post" enctype="multipart/form-data">
<input type="hidden" name="jum_data" id="jum_data" value="<?php echo $jum_data ?>">

<div>
<input id="additem" type="button" name="additem" value=" + " title="Tambah Item Barang">&nbsp;
<input id="deleteitem" type="button" name="deleteitem" value=" - " title="Hapus Item Barang">
<br><br>

	<table id="tabelku" border="1" cellpadding="1" cellspacing="1" width="60%" >
        <tr class="judulnya">
          <th width="20">No</th>
          <th>Kode</th>
           <th>Nama Barang Jadi</th>
           
	      <th>HPP (Rp.)</th>
	      <th>Bulan</th>
           <th>Tahun</th>
        </tr>
		
		<?php
		  if (is_array($query)) {
			  $i=1;
			 for($j=0;$j<count($query);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
           <input type="text" name="kode_brg_jadi_<?php echo $i ?>" id="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $query[$j]['kode_brg_jadi'] ?>" onkeyup="cari('<?php echo $i ?>',this.value);">
          </td>
          <td>
          <div id="infobrgjadi_<?php echo $i ?>"><input type="text" size="40" name="nama_brg_jadi_<?php echo $i ?>" id="nama_brg_jadi_<?php echo $i ?>" value="<?php echo $query[$j]['nama_brg_jadi'] ?>" readonly="true"></div>
          </td>
          
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="5" value="<?php echo $query[$j]['harga'] ?>" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'" />
			<input type="hidden" name="harga_lama_<?php echo $i ?>" id="harga_lama_<?php echo $i ?>" value="<?php echo $query[$j]['harga'] ?>">
          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		else {
		?>
		
		<tr>
		<td align="center" id="num_1">1</td>
		<td><input type="text" name="kode_brg_jadi_1" id="kode_brg_jadi_1" size="10" value="" onkeyup="cari('1',this.value);">
		</td>
		<td><div id="infobrgjadi_1">
			<input type="text" name="nama_brg_jadi_1" id="nama_brg_jadi_1" size="40" value="" readonly="true"></div></td>
		<td><input type="text" name="harga_1" id="harga_1" size="5" value="0" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'">
		<input type="hidden" name="harga_lama_1" id="harga_lama_1" value="0">
		</td>
		 <?php
			  $bulan=array("","january","february","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
           
           ?>
          <td>
         <select id="bulan_1" name="bulan_1">
		<option selected="selected">Bulan</option>
	<?php
		$bln=array("","Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
	for($bulan=1; $bulan<=12; $bulan++){
	if($bulan<=9) { echo "<option value='0$bulan'>$bln[$bulan]</option>"; }
	else { echo "<option value='$bulan'>$bln[$bulan]</option>"; }
}
?>
</select>
          </td>
          <td>
           <input type="text" name="tahun_1" id="tahun_1">
          </td>
	</tr>
	
		<?php } ?>
	</table><br>
		<input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgjadi_hpp/cform/addhppgudangjadi'">

</div>
</form>
