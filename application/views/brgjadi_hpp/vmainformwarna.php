<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var brg_jadi= $('#brg_jadi').val();
	var kode_brg_jadi= $('#kode_brg_jadi').val();

	if (kode_brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		$('#kode_brg_jadi').focus();
		return false;
	}
}

function addElement() {
  var ni = document.getElementById('myDiv');
  var newdiv = document.createElement('div');
  
  var jumwarna= $('#jumwarna').val();
  var jumwarnanew = parseInt(jumwarna)+1;
  $('#jumwarna').val(jumwarnanew);
    
  newdiv.innerHTML = "<select name='kode_warna_"+jumwarnanew+"' id='kode_warna_"+jumwarnanew+"'>"+
					"<option value=''> -Pilih- </option>"+
					<?php
					foreach($list_warna as $color) {
					?>
					"<option value='<?php echo $color->kode ?>'><?php echo $color->nama ?></option>"+
					<?php }?>
					"</select>";
  
  ni.appendChild(newdiv);
}

function hapusElement() {
  var jumwarna= $('#jumwarna').val();
  $("#kode_warna_"+jumwarna).remove(); 
  var jumwarnanew = parseInt(jumwarna)-1;
  $('#jumwarna').val(jumwarnanew);
}

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
</script>

<h3>Data Warna Barang Jadi WIP</h3><br>
<a href="<? echo base_url(); ?>index.php/brgjadi_hpp/cform/addwarnabrgjadi">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/brgjadi_hpp/cform/viewwarnabrgjadi">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_warna', 'id' => 'f_warna');
	echo form_open('brgjadi_hpp/cform/submitwarnabrgjadi', $attributes); ?>
	<table>
		<tr>
			<td>Barang Jadi</td>
			<td> <input type="text" name="brg_jadi" id="brg_jadi" value="" size="40" readonly="true" >
			<input type="hidden" name="kode_brg_jadi" id="kode_brg_jadi" value="">
			<input title="browse data barang" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/brgjadi_hpp/cform/show_popup_brgjadi/');" >
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			  <td>Warna</td>
			  <td> <input type="hidden" name="jumwarna" id="jumwarna" value="1"> 
			  <div id="myDiv">
				<a href="javascript:addElement()">Tambah</a>&nbsp;<a href="javascript:hapusElement()">Hapus</a><br />
				<select name="kode_warna_1" id="kode_warna_1">
					<option value=""> -Pilih- </option>
					<?php
					foreach($list_warna as $color) {
						echo "<option value=".$color->kode.">".$color->nama."</option>";
					}
					?>
				</select>
				</div>
			  </td>
			</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgjadi_hpp/cform/viewwarnabrgjadi'">
			
		</tr>
	</table>
<?php echo form_close(); ?> <br>
