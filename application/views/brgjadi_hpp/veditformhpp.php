<h3>Data Harga Pokok Produksi (HPP) WIP</h3><br>
<a href="<? echo base_url(); ?>index.php/brgjadi/cform/addhpp">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhpp">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">
 
 function cek_data() {
	var harga= $('#harga').val();


	if (harga == '' || harga == '0') {
		alert("Harga tidak boleh 0 / kosong..!");
		$('#harga').focus();
		return false;
	}
	
	if (isNaN($('#harga').val())) {
		alert("Harga harus berupa angka..!");
		$('#harga').focus();
		return false;
	}
	
 }	

</script>

<?php 
$attributes = array('name' => 'f_hpp', 'id' => 'f_hpp');
echo form_open('brgjadi/cform/edithpp', $attributes); ?>
<input type="hidden" name="go_edit" value="1">
<input type="hidden" name="idharga" value="<?php echo $idharga ?>">
<input type="hidden" name="kode_brg_jadi" value="<?php echo $query[0]['kode_brg_jadi'] ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<table width="50%">
		<tr>
			<td colspan="2">Edit Data</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td>Kode & Nama Brg Jadi</td>
			<td> <?php echo $query[0]['kode_brg_jadi']." - ".$query[0]['nama_brg_jadi'] ?>
			</td>
		</tr>
		<tr>
			<td>Harga (Rp.)</td>
			<td> <input type="text" name="harga" id="harga" value="<?php echo $query[0]['harga'] ?>" size="10" >
				<input type="hidden" name="harga_lama" id="harga_lama" value="<?php echo $query[0]['harga'] ?>">
			</td>
		</tr>
</table><br>
		<?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "brgjadi/cform/viewhpp/index/".$cur_page;
			else
				$url_redirectnya = "brgjadi/cform/carihpp/index/".$eunit_jahit."/".$carinya."/".$cur_page;
        ?>
<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<?php echo form_close(); ?>
