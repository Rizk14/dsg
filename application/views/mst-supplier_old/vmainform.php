<script type="text/javascript">
function cek_input() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();
	var alamat= $('#alamat').val();
	var kota= $('#kota').val();
	var kontak_person= $('#kontak_person').val();
	var top= $('#top').val();
	
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (kode == '') {
			alert("Kode Supplier harus diisi..!");
			s = 1;
			return false;
		}
		if (nama == '') {
			alert("Nama Supplier harus diisi..!");
			s = 1;
			return false;
		}
		if (alamat == '') {
			alert("Alamat harus harus diisi..!");
			s = 1;
			return false;
		}
		if (kota == '') {
			alert("Kota harus diisi..!");
			s = 1;
			return false;
		}
		if (kontak_person == '') {
			alert("Kontak person harus diisi..!");
			s = 1;
			return false;
		}
		if (top == '') {
			alert("TOP harus diisi..!");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}
</script>

<h3>Data Supplier</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-supplier/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-supplier/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php echo form_open('mst-supplier/cform/submit'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="kodeedit" value="<?php echo $ekode ?>"><?php } ?>
	<table>
		<tr>
			<td>Kode</td>
			<td> <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="10" size="10" <?php if ($edit == '1') { ?> readonly="true" <?php } ?> >
			</td>
		</tr>
		<tr>
			<td>Nama Supplier</td>
			<td> <input type="text" name="nama" id="nama" value="<?php echo $enama ?>" maxlength="50" size="50"></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td> <textarea name="alamat" id="alamat" rows = "6" cols= "40"><?php echo $ealamat ?></textarea></td>
		</tr>
		<tr>
			<td>Kota</td>
			<td> <input type="text" name="kota" id="kota" value="<?php echo $ekota ?>" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>Kontak Person</td>
			<td> <input type="text" name="kontak_person" id="kontak_person" value="<?php echo $ekontak_person ?>" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>Telepon</td>
			<td> <input type="text" name="telp" id="telp" value="<?php echo $etelp ?>" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>Faksimile</td>
			<td> <input type="text" name="fax" value="<?php echo $efax ?>" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>T.O.P</td>
			<td> <input type="text" name="top" id="top" value="<?php echo $etop ?>" maxlength="3" size="3"> Hari</td>
		</tr>
		<tr>
			<td>PKP</td>
			<td> <input type="checkbox" name="pkp" <?php if ($epkp == 't') { ?> checked="true" <?php } ?> value="t" ></td>
		</tr>
		<tr>
			<td>NPWP</td>
			<td> <input type="text" name="npwp" value="<?php echo $enpwp ?>" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>Nama NPWP</td>
			<td> <input type="text" name="nama_npwp" value="<?php echo $enama_npwp ?>" maxlength="30" size="30"></td>
		</tr>
		<tr>
			<td>Tipe Pajak</td>
			<td> <select name="tipe_pajak" id="tipe_pajak">
				<option value="I" <?php if ($etipe_pajak == 'I') { ?> selected="true" <?php } ?> >Include</option>
				<option value="E" <?php if ($etipe_pajak == 'E') { ?> selected="true" <?php } ?> >Exclude</option>
			</select></td>
		</tr>
		<tr>
			<td>Kategori Supplier</td>
			<td> <select name="kategori" id="kategori">
				<option value="1" <?php if ($ekategori == '1') { ?> selected="true" <?php } ?> >Pembelian</option>
				<option value="2" <?php if ($ekategori == '2') { ?> selected="true" <?php } ?> >Makloon</option>
			</select></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_input();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-supplier/cform/view'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

