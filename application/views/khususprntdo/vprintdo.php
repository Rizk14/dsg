<!--<style type="text/css" media="print">
	@page 
        {
            size: auto;   /* auto is the current printer page size */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
</style> -->

<style type="text/css">

/*@media print {
	div {
		color: #f00;
	}
} */

/*yang ini buat setting ukuran kertasnya assumsi A4/letter, sementara ga dipake */
/*#Letter {background-color:#FFFFFF;
left:1px;
right:1px;
height:11in ; */ /*Ukuran Panjang Kertas */
/*width: 8.50in; */ /*Ukuran Lebar Kertas */
/*margin:1px solid #FFFFFF; */
 
/* font-family:Georgia, "Times New Roman", Times, serif; } */
	@page {
      size: Letter;
      margin: 0mm;
	  margin-right :0.37in;
      /* this affects the margin in the printer settings */
      /* margin: 0in 0.37in 0.07in 0.26in; */
	  /* margin-top : 0in;*/
	  /*margin-bottom :0.07in;
	  margin-left : 0.26in; */
    }
	/*.page {
        width: 9.5in;
        min-height: 5.5in;
        padding: 0.01in;
        margin: 0.05in auto;
        border-radius: 5px;
        background: white;
    }
    .subpage {
        padding: 0.08in;
        height: 5.5in;
    }*/
   .isinya {
		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
		font-size: 13px;
    }
    .kepala {
		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
    	font-size: 19px;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
    	font-size: 13px;
	}
	.kepala2 {
		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
    	font-size: 13px;
	}
	.detailbrg {
		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
		font-size: 11px;
	}
	.detailbrg2 {
		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
    	font-size: 12px;
	}
	.kotak {
   		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
		font-size: 11px;
		border-collapse:collapse;
		border:1px solid black;
    }
    .tabelheader {
    	font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
		font-size: 13px;}
    .subtotal {
		font-family: Helvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
    	font-size: 14px;
	}
	.garisbawah { 
		border-bottom:#000000 0.1px solid;
	}
</style>
<style type="text/css" media="print">
	.noDisplay{
		display:none;
	}
	.pagebreak {
		page-break-before: always;
	}
</style>

<?php include_once ("funcs/terbilang.php"); ?>
<?php if($isi){?>
	<table border="0" width="100%" class="tabelheader">
		<tr>
			<td class="kepala"><b>Delivery Order</b></td>
			<td class="kepalaxx" align="right"><b><?php echo $nminitial ?></b></td>
		</tr>	

		<tr class="kepala2" style="outline: thin solid">
			<td style="white-space:nowrap;">&nbsp;&nbsp;Nomor DO : <b><?php echo $nomor ?> </b></td>
			<td align="right">Nomor OP : <b><?php echo $nomorop ?></b>&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td width="20%">Tanggal DO </td>
			<td>: <?php echo $dotgl." ".$dobln." ".$dothn ?>
			</td>
		</tr>
		<tr>
			<td>Customer</td>
			<td>: <?php echo $nmcabang ?>
			</td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>: <?php echo ucwords(strtolower($addr)); ?>
			</td>
		</tr>
	</table>

	<table border="0" width="100% "class="isinya" cellspacing="0">
		<thead>
		<tr>
			<td colspan="6"><hr></td>
			<!--<td colspan="6" class="garisbawah">&nbsp;</td>-->
		</tr>
		<tr class="kepala2">
			<th width="3%">No</th>
			<th width="10%" style="white-space:nowrap;">No OP</th>
			<th width="10%" style="white-space:nowrap;">Kode</th>
			<th width="25%" style="white-space:nowrap;">Nama Barang</th>
			<th width="3%" align="right">Jumlah</th>
			<th width="20%">Keterangan</th>
		</tr>
		<tr>
			<td colspan="6"><hr></td>
			<!--<td colspan="6" class="garisbawah">&nbsp;</td>-->
		</tr>
		</thead>
		<tbody>
			<?php
				$no=1; 
				$lup	= 0;
				$waktu	= date("H:i:s");	
		if($isi){
			foreach($isi as $row) {
				//if($lup<22) {		
					$db2 = $this->load->database('db_external', TRUE);
					$detailwarna= "";
					// 08-09-2014 --------
					$sqlxx = " SELECT a.i_color, b.e_color_name, a.qty FROM tm_do_item_color a 
							INNER JOIN tr_color b ON a.i_color=b.i_color
							WHERE a.i_do_item = '".$row->i_do_item."' ";
									
					$queryxx = $db2->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						
						$detailwarna = "(";
						foreach ($hasilxx as $rowxx) {
							if ($rowxx->qty != 0) {
								// 27-11-2015
								if (trim($rowxx->e_color_name) == 'MERAH MAROON')
									$rowxx->e_color_name = 'MRH MRN';
								
								$detailwarna.= $rowxx->e_color_name.":".$rowxx->qty." ";
							}
						}
						$detailwarna .= ")";
					}
					else
						$detailwarna="";
					// -------------------
					
					if (trim($row->keterangan) != ''){
						$teksket = $row->keterangan;
					}else{
						$teksket = "";
					}
		
					$nmmotif	= $row->motifname;
					echo "<tr class='detailbrg'>
							<td align='center'>".$no."</td>
							<td>".$row->iop."</td>
							<td>".$row->iproduct."</td>
							<td style='white-space:nowrap;'>".$row->motifname."</td>
							<td align='right'>".$row->qty."</td>
							<td style='white-space:nowrap;' align='center'>&nbsp;".$teksket.$detailwarna."</td>
					</tr>";
					$no++; $lup++;
				//}
			}
		}else{
			echo "";
		}
		
		?>
		<tr>
			<td colspan="6"><hr></td>
		</tr>
		</tbody>
	</table>
	<table border="0" width="100%" class="isinya">
					<tbody>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penerima</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yang Membawa</td>
							<!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kepala Gudang</td> -->
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kepala Gudang</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>

							<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo KepalaGudang ;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
							<!-- <td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td></td> -->
						</tr>
						</tbody>
						<?php
							$tglprnt	= explode("-",$tglprint,strlen($tglprint));
							$tg_print	= html_entity_decode($tglprnt[0]);
							$bl_print	= html_entity_decode($tglprnt[1]);
							$th_print	= html_entity_decode($tglprnt[2]);

							echo "<tr>
									<td colspan = 3>&nbsp;</td>
								</tr>
								<tr>
									<td>".$tg_print." ".$bl_print." ".$th_print." ".$waktu."</td>
								</tr>";
						?>
					</table>

				<!-- <br class = "pagebreak"> -->
				<div class="noDisplay"><center><b><a href="#" onclick="window.print();">Print</a></b></center></div>
<?php 
	}else{
		echo "<font style = text-align : center>
				<h2 align = center>OOPS! DO SUDAH TERCETAK! :)</h2>
			  </font>";
	}
?>				

<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.4.4.min.js"></script>
<script language="javascript" type="text/javascript">
    window.onafterprint = function (){
        var kddo = '<?php echo $nomor; ?>';
		
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('khususprntdo/cform/updatecetak');?>",
            data:"kddo="+kddo,
            success: function(data){
                // show('khususprntdosj/cformdo','#content');
                // window.close();
                // alert('sukses print '+data);
                //opener.window.refreshview();
                setTimeout(window.close,0);
            },
            error:function(XMLHttpRequest){
                alert('fail');
                // alert(XMLHttpRequest.responseText);
            }

        });
        
    }
</script>