<!--<style type="text/css" media="print">
	@page 
        {
            size: auto;   /* auto is the current printer page size */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
</style> -->

<style type="text/css">

/*@media print {
	div {
		color: #f00;
	}
} */

/*yang ini buat setting ukuran kertasnya assumsi A4/letter, sementara ga dipake */
/*#Letter {background-color:#FFFFFF;
left:1px;
right:1px;
height:11in ; */ /*Ukuran Panjang Kertas */
/*width: 8.50in; */ /*Ukuran Lebar Kertas */
/*margin:1px solid #FFFFFF; */
 
/* font-family:Georgia, "Times New Roman", Times, serif; } */

   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
    
    }
    
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 19px;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	.kepala2 {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	.detailbrg {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
	}
.detailbrg2 {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
	}
	.kotak {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 13px;}
    
    .subtotal {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;
	}
	
	.garisbawah { 
		border-bottom:#000000 0.1px solid;
	}
</style>

<?php include_once ("funcs/terbilang.php"); ?>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	//window.print();
});

</script>

<table border="0" width="100%" class="tabelheader">
	<tr>
		<td class="kepala"><b>Delivery Order</b></td>
		<td class="kepalaxx" align="right"><b><?php echo $nminitial ?></b></td>
	</tr>	

	<tr class="kepala2" style="outline: thin solid">
		<td style="white-space:nowrap;">&nbsp;&nbsp;Nomor DO : <b><?php echo $nomor ?> </b></td>
		<td align="right">Nomor OP : <b><?php echo $nomorop ?></b>&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">Tanggal DO </td>
		<td>: <?php echo $dotgl." ".$dobln." ".$dothn ?>
		</td>
	</tr>
	<tr>
		<td>Customer/Area</td>
		<td>: <?php echo $nmcabang ?>
		</td>
	</tr>
</table>

<table border="0" width="100% "class="isinya" cellspacing="0">
	<thead>
	<tr>
		<td colspan="6"><hr></td>
		<!--<td colspan="6" class="garisbawah">&nbsp;</td>-->
	</tr>
	 <tr class="kepala2">
		<th width="3%">No</th>
		<th width="10%" style="white-space:nowrap;">No OP</th>
		<th width="10%" style="white-space:nowrap;">Kode</th>
		<th width="25%" style="white-space:nowrap;">Nama Barang</th>
		<th width="3%" align="right">Jumlah</th>
		<th width="20%">Keterangan</th>
	 </tr>
	 <tr>
		<td colspan="6"><hr></td>
		<!--<td colspan="6" class="garisbawah">&nbsp;</td>-->
	</tr>
	</thead>
	<tbody>
		<?php $no=1; $lup	= 0;
				
	foreach($isi as $row) {
		//if($lup<22) {		
			
			$nmmotif	= $row->motifname;
			echo "<tr class='detailbrg'>
					<td align='center'>$no</td>
					<td>$row->iop</td>
					<td>$row->iproduct</td>
					<td style='white-space:nowrap;'>$row->motifname</td>
					<td align='right'>$row->qty</td>
					<td style='white-space:nowrap;' align='center'>&nbsp; $row->keterangan</td>
			</tr>";
			$no++; $lup++;
		//}
	}
	?>
	<tr>
		<td colspan="6"><hr></td>
	</tr>
	</tbody>
</table>
<table border="0" width="100%" class="isinya">
				<tbody>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penerima</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mengetahui</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>

						<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
						<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
					</tr>
					</tbody>
				</table>
