<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>

<style type="text/css" media="screen, print">
body {
	background:#FFFFFF;
	color:black;
	margin:0;
}

#tbl_utama {
	width:1100px;
}

</style>

<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 18000;

t = null;
function closeMe() {
t = setTimeout("self.close()",howLong);
}

</script>

</head>

<body onload="window.print();closeMe();">

<table>
  <tr>
    <td><table border="0" cellspacing="0" cellpadding="0" width="1240px;">
      <tr>
        <td width="51%"><span style="font-family:Times New Roman, Times, serif; font-size:26px;">Delivery Order</span></td>
        <td width="49%" align="right"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; text-align:right;">CV. DUTA SETIA GARMEN</span></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; font-style:italic; border-left:#000000 1px solid; border-top:#000000 1px solid; border-bottom:#000000 1px solid; border-left:#000000 1px solid;border-right:#000000 1px solid; line-height:30px; padding-left:5px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td align="left">Nomor DO :<b> <?php echo $nomor; ?></b></td>
				<td align="right" style="padding-right:5px;"><?php echo $nomorop; ?></td>
			  </tr>
			</table>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12%" style="line-height:25px;"><span style="font-family:Times New Roman, Times, serif; font-size:12px; font-weight:bold;">Tanggal DO</span></td>
        <td width="1%"><span style="font-family:Times New Roman, Times, serif; font-size:12px; font-weight:bold;">:</span></td>
        <td width="87%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px;"><?php echo $shtgl; ?></td>
      </tr>
      <tr>
        <td style="line-height:25px;"><span style="font-family:Times New Roman, Times, serif; font-size:12px; font-weight:bold;">Customer/ Area</span></td>
        <td><span style="font-family:Times New Roman, Times, serif; font-size:12px; font-weight:bold;">:</span></td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:11px;"><?php echo $nmcabang; ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr style="font-family:Times New Roman, Times, serif; font-size:14px; font-weight:bold; text-align:center; vertical-align:middle; line-height:25px;">
        <td width="5%" align="left" style="border-bottom:#000000 1px solid;">No.</td>
        <td width="10%" align="left" style="border-bottom:#000000 1px solid;">No. OP</td>
        <td width="13%" align="left" style="border-bottom:#000000 1px solid;">Kode Barang </td>
        <td width="40%" align="left" style="border-bottom:#000000 1px solid;">Nama Barang </td>
        <td width="12%" align="right" style="border-bottom:#000000 1px solid;">Jumlah &nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="15%" align="center" style="border-bottom:#000000 1px solid;">Keterangan</td>
      </tr>
	  <?php
	  $no	= 1;
	  foreach($isi as $row) {
		if($no<10) {
			  $ldo	.= "
			  <tr>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$no."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$row->iop."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$row->iproduct."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$row->motifname."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\" align=\"right\">".$row->qty."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$row->keterangan."&nbsp;</td>
			  </tr>";
		}
	  $no+=1;
	  }
	  echo $ldo;
	  
	  $akhr=$no;
	  if($akhr<=4) {
	  	for($loop=$akhr;$loop<=4; $loop++) {
			echo "
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>";
		}
	  }
	  ?>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="31%" style="font-family:Times New Roman, Times, serif; font-size:13px; text-align:center; vertical-align:middle; line-height:35px; font-weight:bold;">Penerima</td>
        <td width="34%" style="font-family:Times New Roman, Times, serif; font-size:13px; text-align:center; vertical-align:middle; line-height:35px; font-weight:bold;">Yang Membawa </td>
        <td width="35%" style="font-family:Times New Roman, Times, serif; font-size:13px; text-align:center; vertical-align:middle; line-height:35px; font-weight:bold;">Factory Manager </td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; vertical-align:middle; line-height:60px;">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; vertical-align:middle;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; vertical-align:middle;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; vertical-align:middle;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  	<td style="font-family:Arial, Helvetica, sans-serif; font-size:10px; border-bottom:#000000 1px solid; line-height:35px;"><?php echo $tglprint." ".date("h:i:s"); ?></td>
  </tr>
</table>
</body>
</html>
