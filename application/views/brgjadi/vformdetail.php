<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#d_penawaran").datepicker();
	$("#d_launching1").datepicker();
});
</script>

<div id="tabs">
	<div id="tab2" class="tab_e_brg_jadi" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">FORM BARANG JADI</div>
</div>

<div class="tab_e_bdr_brg_jadi"></div>

	    <table width="90%">
			<tr>
				<td align="left">
					<?php echo $this->pquery->form_remote_tag(array('url'=>'brgjadi/cform/','update'=>'#content','type'=>'post'));?>
				  	<div id="masterbrgjadiform">
					<table>
					<tr>
					  <td width="20%"><?php echo $form_kd_brg_brgjadi; ?></td>
					  <td width="1%">:</td>
					  <td width="29%">
						<?php
						$kdbrg = array(
						  'name'=>'i_product_base1',
						  'id'=>'i_product_base1',
						  'value'=>$i_product_base,
						  'maxlength'=>'7',
						  'readonly'=>true
						);
						echo form_input($kdbrg);
						?> 
					  </td>
					  <td width="20%"><?php echo $form_nm_brg_brgjadi; ?></td>
					  <td width="1%">:</td>
					  <td width="29%">
						<?php
						$nmbrg = array(
						  'name'=>'e_product_basename1',
						  'id'=>'e_product_basename1',
						  'value'=>$e_product_basename,
						  'maxlength'=>'200'	
						);
						echo form_input($nmbrg);
						?> 
		
					  </td>
						</tr>
						<tr>
					  <td><?php echo $form_kat_brgjadi; ?></td>
					  <td>:</td>
					  <td>
					   <?php
						$kategori = array(
						  'name'=>'i_category1',
						  'id'=>'i_category1',
						  'value'=>$category,
						  'maxlength'=>'50',
						  'onclick'=>'kategori();' 	
						);
						echo form_input($kategori);
					   ?>
					  </td>
						</tr>
						<tr>
					  <td><?php echo $form_merek_brgjadi; ?></td>
					  <td>:</td>
					  <td>
					   <?php
						$brand = array(
						  'name'=>'i_brand1',
						  'id'=>'i_brand1',
						  'value'=>$brand,
						  'maxlength'=>'50',
						  'readonly'=>'true',
						  'onclick'=>'merek();' 		
						);
						echo form_input($brand);					
					   ?>
					  </td>
					  <td><?php echo $form_layout_brgjadi; ?></td>
					  <td>:</td>
					  <td>
					   <?php
						$layout = array(
						  'name'=>'i_layout1',
						  'id'=>'i_layout1',
						  'value'=>$layout,
						  'maxlength'=>'50',
						  'onclick'=>'tampilan();'
						);
						echo form_input($layout);
					   ?>
					  </td>
						</tr>
					<tr>
					  <td><?php echo $form_quality_brgjadi; ?></td>
					  <td>:</td>
					  <td>
					   <?php
						$quality = array(
						  'name'=>'e_quality1',
						  'id'=>'e_quality1',
						  'value'=>$e_quality,
						  'maxlength'=>'100'	
						);
						echo form_input($quality);
					   ?>
					  </td>
					  <td><?php echo $form_sproduksi_brgjadi; ?></td>
					  <td>:</td>
					  <td>
					  <input type="checkbox" name="f_stop_produksi1" id="f_stop_produksi1" <?=$f_stop_produksi?> value="1"/>	
					  </td>
					</tr>
					<tr>
					  <td><?php echo $form_spenawaran_brgjadi; ?></td>
					  <td>:</td>
					  <td>
					   <?php
						$penawaran = array(
						  'name'=>'e_surat_penawaran1',
						  'id'=>'e_surat_penawaran1',
						  'value'=>$e_surat_penawaran,
						  'maxlength'=>'50'
						);
						echo form_input($penawaran);
					   ?>
					  </td>
					  <td><?php echo $form_tpenawaran_brgjadi; ?></td>
					  <td>:</td>
					  <td>
					   <?php
						$dpenawaran = array(
						  'name'=>'d_penawaran',
						  'id'=>'d_penawaran',
						  'value'=>$d_surat_penawaran,
						  'readonly'=>'true',
						  'maxlength'=>'10'	
						);
						echo form_input($dpenawaran);
					   ?>
					  </td>
					</tr>
				   <tr>
					<td><?php echo $form_sbrg_brgjadi; ?></td>
					<td>:</td>
					<td>
					<select name="n_status">
					<option value="">[<?php echo $form_pilih_status_brgjadi; ?>]</option>
					<?php
					foreach($statusbarang as $row) {
						$status	= $n_status==$row->i_status_product?" selected ":"";
						$optstatusbarang .= "<option value=".$row->i_status_product." ".$status.">".$row->e_statusname."</option>";
					}
					echo $optstatusbarang;
					?>
					</select>
					</td>
					<td><?php echo $form_qty_brgjadi; ?></td>
					<td>:</td>
					<td>
					<?php
					$nqty = array(
						'name'=>'n_quantity',
						'id'=>'n_quantity',
						'value'=>$n_quantity,
						'maxlength'=>'10');
						
					echo form_input($nqty);	
					?>
					</td>
				   </tr>
				   <tr>
					<td><?php echo $form_tglregist_brgjadi; ?></td>
					<td>:</td>
					<td>
					<?php
					$dlaunch = array(
					  'name'=>'d_launching1',
					  'id'=>'d_launching1',
					  'value'=>$d_entry,
					  'readonly'=>'true',
					  'maxlength'=>'10'	
					);
					echo form_input($dlaunch);
					?>
					</td>
					<td><?php echo $form_hjp_brgjadi; ?></td>
					<td>:</td>
					<td><?php
					$price = array(
					  'name'=>'v_price',
					  'id'=>'v_price',
					  'value'=>number_format($v_unitprice),
					  'maxlength'=>'50'
					);
					echo form_input($price);
					?></td>
				   </tr>
				   <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Harga Grosir 
						<td>:</td> 
						<td><input type="checkbox" name="is_grosir" id="is_grosir" value="y" <?php if($harga_grosir != 0) { ?> checked="true" <?php } ?> disabled>&nbsp;
						<input type="text" name="harga_grosir" id="harga_grosir" maxlength="20" value="<?php echo number_format($harga_grosir); ?>" readonly="true"></td>
				   </tr>
				   <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="4" align="right">
					<input name="btnbatal" id="btnbatal" value="Keluar" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/brgjadi/cform'">
					</td>
				   </tr>
				  </table>

					</div>  
					<?php echo form_close(); ?>
				</td>
			</tr>
		</table>
