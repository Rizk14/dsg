<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function cek_data() {
	var username= $('#username').val();
	var passwd1= $('#passwd1').val();
	var passwd2= $('#passwd2').val();
	var passwd3= $('#passwd3').val();
	var nama= $('#nama').val();

	if (passwd1 == '') {
		alert("Password Lama harus diisi..!");
		$('#passwd1').focus();
		return false;
	}
	if (passwd2 == '') {
		alert("Password Baru harus diisi..!");
		$('#passwd2').focus();
		return false;
	}
	if (passwd1 == passwd2) {
		alert("Password Baru harus beda dari Password Lama..!");
		$('#passwd2').focus();
		return false;
	}
	
	if (passwd3 != passwd2) {
		alert("Konfirmasi Password Baru harus sama dengan Password Baru..!");
		$('#passwd3').focus();
		return false;
	}
	
}
	
</script>

<h3>Ganti Password</h3><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_passwd', 'id' => 'f_passwd');
	echo form_open('users-ganti-passwd/cform/submit', $attributes); ?>
	<table>
		<tr>
			<td>Password Lama</td>
			<td> : <input type="password" name="passwd1" id="passwd1" value="" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>Password Baru</td>
			<td> : <input type="password" name="passwd2" id="passwd2" value="" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>Konfirmasi Password Baru</td>
			<td> : <input type="password" name="passwd3" id="passwd3" value="" maxlength="20" size="20"></td>
		</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset"></td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

