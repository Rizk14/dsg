<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Lokasi Gudang</h3><br>
<?php echo form_open('mst-lok-gudang/cform/submit'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="id_lokasi" value="<?php echo $eid ?>"><?php } ?>
	<table>
		<tr>
			<td>Kode Lokasi</td>
			<td>: <input type="text" name="kode_lokasi" value="<?php echo $ekode ?>" maxlength="3" size="3">
			</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>: <input type="text" name="nama" value="<?php echo $enama ?>" maxlength="60"></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-lok-gudang/cform/'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

<div>
<table border="1" cellpadding= "1" cellspacing = "1" width="50%">
	<thead>
	 <tr class="judulnya">
		 <th>Kode Lokasi</th>
		 <th>Nama</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode_lokasi</td>";
				 echo    "<td>$row->nama</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 $ada = 0;
				 // cek apakah data kode sudah dipake di tabel tm_gudang				 
				 $query3	= $this->db->query(" SELECT id FROM tm_gudang WHERE id_lokasi = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 echo    "<td align=center><a href=".base_url()."index.php/mst-lok-gudang/cform/index/$row->id \" >Edit</a>";
				 
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/mst-lok-gudang/cform/delete/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table>
</div>
