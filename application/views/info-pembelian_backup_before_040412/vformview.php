<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}

</style>

<h3>Laporan Pembelian</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Supplier: <?php if ($supplier != '0') { echo $supplier." - ".$nama_supplier; } else { echo "All"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		<th>Tgl Faktur</th>
		 <th>Nomor Faktur</th>
		 <th>Supplier</th>
		 <th>No / Tgl SJ</th>
		 <th>List Barang</th>
		 <th>No Perk</th>
		 <th>Harga Satuan</th>
		 <th>Qty</th>
		 <th>Satuan</th>
		 <th>Jumlah</th>
		 <th>Tot Hutang Dagang</th>
		 <th>PPN</th>
		 <th>Bahan Baku</th>
		 <th>Bahan Pembantu</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_hutang += $query[$j]['jumlah'];
					$tot_ppn += $query[$j]['pajaknya'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
							 if ($var_detail[$k]['kode_perk'] == "511.100")
								$tot_baku += $var_detail[$k]['total'];
							 else
								$tot_pembantu += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
			}
		 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_faktur']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>".$tgl_faktur."</td>";
				 echo    "<td>".$query[$j]['no_faktur']."</td>";
				// echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				echo    "<td>".$query[$j]['nama_supplier']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  //echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_perk'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga'], 2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['total'],2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['jumlah'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['pajaknya'],2,',','.')."</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "511.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "512.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo  "</tr>";

					
		 	}
		   }
		 ?>
		 <tr>
			<td colspan="9" align="right">TOTAL</td>
			<td align="right"><?php echo number_format($tot_jumlah_detail,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_hutang,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_ppn,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_baku,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_pembantu,2,',','.')  ?></td>
		 </tr>
 	</tbody>
</table><br>
<?php //echo $this->pagination->create_links();?>
</div>
