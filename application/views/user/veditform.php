<script type="text/javascript">
$(document).ready(function() {
 
 $(".delbutton").click(function(){
	 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var del_id = element.attr("id");
 
 //Built a url to send
 var info = 'id=' + del_id;
 if(confirm("Anda yakin akan menghapus?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('user/cform/hapus')?>",
 data: info,
 success: function(){
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
	 
})
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_adduser; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_adduser; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<div id="masteradduserform">

			<div id="tabs">
				<div id="tab2" class="tab_user" style="margin-left:1px;" align="center" onClick="javascript: displayPanelUser('2');">FORM TAMBAH PEMAKAI</div>

			<div class="tab_bdr_user"></div>
			
			<div class="panel_user" id="panel2" style="display:block;">
				<table>
				  <tr>
					<td align="left">
					  <?php 
					     $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('user/cform/actedit', $attributes);?>
				
				  	  <div class="accordion2">
					  <table>
						<tr>
					  		<td width="150px"><?php echo $form_user_name_usr; ?></td>
					  		<td width="1px">:</td>
					  		<td>
							<?php
								$eusername	= array(
									'name'=>'e_user_name',
									'id'=>'e_user_name',
									'value'=>$e_user_name,
									'maxlength'=>'30',
									'readonly'=>true
								);
								echo form_input($eusername);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_passwd_old_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$epassword_old	= array(
									'name'=>'e_password_old',
									'id'=>'e_password_old',
									'value'=>'',
									'maxlength'=>'15'
								);
								echo form_password($epassword_old);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_passwd_new_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$epassword_new	= array(
									'name'=>'e_password_new',
									'id'=>'e_password_new',
									'value'=>'',
									'maxlength'=>'15'
								);
								echo form_password($epassword_new);
							?>
							</td>
					    </tr>						
						<tr>
					  		<td><?php echo $form_re_passwd_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$re_epassword	= array(
									'name'=>'re_e_password',
									'id'=>'re_e_password',
									'value'=>'',
									'maxlength'=>'15'
								);
								echo form_password($re_epassword);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_level_user_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<select name="ilevel">
							<option value="">[ <?php echo $form_pilihan_level_usr; ?> ]</option>
							<?php 
							foreach($opt_level as $row) {
								$sel	= $i_level==$row->i_level?"selected":"";
								$llevel	.= "<option value=";
								$llevel	.= $row->i_level." ".$sel.">";
								$llevel	.= $row->e_level;
								$llevel	.= "</option>";
							}
							echo $llevel;
							?>
							</select>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_status_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<select name="iuseractive">
							<option value="">[ <?php echo $form_pilihan_status_usr; ?> ]</option>
							<option value="1" <?php echo $aktif; ?> >Aktif</option>
							<option value="0" <?php echo $nonaktif; ?> >Tidak Aktif</option>
							</select>							
							</td>
					    </tr>
						<tr><td colspan="3">&nbsp;</td></tr>
					    <tr>
					  		<td>&nbsp;</td>
					  		<td>&nbsp;</td>
					  		<td>
							<input type="hidden" name="i_user_id" id="i_user_id" value="<?php echo $i_user_id; ?>"  />
							<input name="tblsimpan" id="tblsimpan" value="<?php echo $button_simpan; ?>" type="submit">
							<input name="tblreset" id="tblreset" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/user/cform/'">
					  		</td>
					    </tr>
					  </table>
				      </div>
					  <?php echo form_close(); ?>
					</td>
				  </tr> 
				</table>
			</div>
			
		</div>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
