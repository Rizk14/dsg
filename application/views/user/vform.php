<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_adduser; ?></td></tr>
</table>
 <a href="<?php echo base_url(); ?>index.php/user/cform/tambah">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/user/cform">View Data</a>&nbsp;<br><br>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_adduser; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<div id="masteradduserform">
			<div id="tabs">
				<div id="tab1" class="tab_sel_user" align="center" onClick="javascript: displayPanelUser('1');">DAFTAR PEMAKAI</div>
<!--
				<div id="tab2" class="tab_user" style="margin-left:1px;" align="center" <?php if($this->session->userdata('ses_level')==1){ ?> onClick="javascript: displayPanelUser('2');" <?php } ?> >FORM TAMBAH PEMAKAI</div>
-->

			<div class="tab_bdr_user"></div>
			<div class="panel_user" id="panel1" style="display: block">
			
				<?php
				
				if(!empty($query) || isset($query)) {
					
					$simgedit	= "<img src=".base_url()."asset/theme/images/edit.gif"."width=12 height=13>";
					$simgdelete	= "<img src=".base_url()."asset/theme/images/delete.gif"."width=12 height=13>";
					
					$cc = 1;
					
					foreach($query as $row) {
						
						$Classnya = $cc % 2 == 0 ? "row" :"row1";
							
						foreach($opt_level as $opt) {
							if($opt->i_level==$row->i_level) {
								$ilevel	= $opt->e_level;
								break;
							}
						}
						
						$active	= $row->i_user_active=='1'?"Aktif":"Tidak Aktif";
						$hps	= ($this->session->userdata('ses_level')==1)?"
						
						<a href='".base_url()."index.php/user/cform/actdelete/".$row->i_user_id."'><img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13></a>":"";

						if($this->session->userdata('ses_level')==$row->i_level) {
						$list .= "
						 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
						  <td>".$cc."</td>	
						  <td>".$row->i_user_id."&nbsp;</td>	 
						  <td>".$row->e_user_name." [".$row->e_password."] "."&nbsp;</td>
						  <td>".$ilevel."&nbsp;</td>
						  <td align=\"center\">".$active."&nbsp;</td>
						  <td align=center>
						  <a href='".base_url()."index.php/user/cform/edit/".$row->i_user_id."'><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;".$hps."
						  </td>
						 </tr>";
						}elseif($this->session->userdata('ses_level')==1){
						$list .= "
						 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
						  <td>".$cc."</td>	
						  <td>".$row->i_user_id."&nbsp;</td>	 
						  <td>".$row->e_user_name." [".$row->e_password."] "."&nbsp;</td>
						  <td>".$ilevel."&nbsp;</td>
						  <td align=\"center\">".$active."&nbsp;</td>
						  <td align=center>
						 <a href='".base_url()."index.php/user/cform/edit/".$row->i_user_id."'><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;".$hps."
						  </td>
						 </tr>";
						}	
						$cc+=1;	
					}
				}
				?>
				<table class="listtable_user" border="1">
				 <th width="4%">No</th>
				 <th width="16%"><?php echo $form_tbl_user_id; ?></th>
				 <th width="40%"><?php echo $form_tbl_user_name; ?></th>
				 <th width="15%"><?php echo $form_tbl_level; ?></th>
				 <th width="15%"><?php echo $form_tbl_status; ?></th>
				 <th width="10%" align="center"><?php echo $link_aksi; ?></th>
				<tbody>
				<?php
				if( !empty($list) && !isset($not_defined) ) {
					echo $list;
				} else {
					echo $list."</tbody></table>".$not_defined;
				}
				?>
			</div>
			
			<div class="panel_user" id="panel2" style="display: none;">
				<table>
				  <tr>
					<td align="left">
					  <?php 
					     $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('user/cform/simpan', $attributes);?>
			
				  	  <div class="accordion2">
					  <table>
						<tr>
					  		<td width="150px"><?php echo $form_user_name_usr; ?></td>
					  		<td width="1px">:</td>
					  		<td>
							<?php
								$eusername	= array(
									'name'=>'e_user_name',
									'id'=>'e_user_name',
									'value'=>'',
									'maxlength'=>'30'
								);
								echo form_input($eusername);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_passwd_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$epassword	= array(
									'name'=>'e_password',
									'id'=>'e_password',
									'value'=>'',
									'maxlength'=>'15'
								);
								echo form_password($epassword);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_re_passwd_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$re_epassword	= array(
									'name'=>'re_e_password',
									'id'=>'re_e_password',
									'value'=>'',
									'maxlength'=>'15'
								);
								echo form_password($re_epassword);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_level_user_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<select name="ilevel">
							<option value="">[ <?php echo $form_pilihan_level_usr; ?> ]</option>
							<?php 
							foreach($opt_level as $row) {
								$llevel	.= "<option value=";
								$llevel	.= $row->i_level.">";
								$llevel	.= $row->e_level;
								$llevel	.= "</option>";
							}
							echo $llevel;
							?>
							</select>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_status_usr; ?></td>
					  		<td>:</td>
					  		<td>
							<select name="iuseractive">
							<option value="">[ <?php echo $form_pilihan_status_usr; ?> ]</option>
							<option value="1">Aktif</option>
							<option value="0">Tidak Aktif</option>
							</select>							
							</td>
					    </tr>
						<tr><td colspan="3">&nbsp;</td></tr>
					    <tr>
					  		<td>&nbsp;</td>
					  		<td>&nbsp;</td>
					  		<td>
							<input name="tblsimpan" id="tblsimpan" value="<?php echo $button_simpan; ?>" type="submit">
							<input name="tblreset" id="tblreset" value="<?php echo $button_batal; ?>" type="button" onclick='show("user/cform/","#content");'>
					  		</td>
					    </tr>
					  </table>
				      </div>
					  <?php echo form_close()?>
					</td>
				  </tr> 
				</table>
			</div>
			
		</div>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
