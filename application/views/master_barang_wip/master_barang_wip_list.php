<?php
// Nomor urut data di tabel.
$per_page = 5;


if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Data Master Barang WIP</h2>
    <hr>

    <!-- Paging dan form pencarian -->
    <div class="row navigasi_cari">
        <!-- Paging -->
        <div class="col-xs-12 col-md-6">
            <?php echo (!empty($paging)) ? $paging : ''?>
       
        </div>
        <!-- /Paging -->

        <!-- Form Pencarian -->
        <div class="col-xs-12 col-md-4 pull-right">
            <form method="get" action="<?php echo $form_action;?>" role="form" class="form-horizontal">
                <div class="input-group">
                    <input type="text" name="kata_kunci" class="form-control" placeholder="Masukkan nama barang atau kode barang pencarian" id="kata_kunci" value="<?php echo $this->input->get('kata_kunci')?>">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /Form Pencarian -->
    </div>
    <!-- /Paging dan form pencarian -->

    <?php if (!empty($master_barang_wip) && is_array($master_barang_wip)): ?>
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Tanggal Input</th>
                    <th>Tanggal Update</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
                foreach($master_barang_wip as $row):
				echo form_hidden('id', $row->id) ;
                 $link_edit = anchor('master_barang_wip/master_barang_wip/edit/'.$row->id, '<span class="glyphicon glyphicon-edit"></span>', array('title' => 'Edit'));
                 $link_hapus = anchor('master_barang_wip/master_barang_wip/hapus/'.$row->id,'<span class="glyphicon glyphicon-trash"></span>', array('title' => 'Hapus', 'data-confirm' => 'Anda yakin akan menghapus data ini?'));
                 $link_cetak = anchor('master_barang_wip/master_barang_wip/cetak/'.$row->id,'<span class="glyphicon glyphicon-print"></span>', array('title' => 'Cetak'));
                 ?>
                    <tr>
						
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $row->kode_barang_wip ?></td>
						<td><?php echo $row->nama_barang_wip ?></td>
                        <td><?php echo $row->created_at ?></td>
                        <td><?php echo $row->updated_at ?></td>
                        <td>
                            <?php echo $link_edit.'&nbsp;&nbsp;&nbsp;&nbsp;'.$link_hapus ?>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
	
	<div class="col-sm-offset-5">
            <form method="post" action="cetak" role="form" class="form-horizontal">
				 <input type="hidden" name="jumlah" value="<?php echo $offset ?>" />
				
                    <div class="btn-group" >
                     <?php echo form_button(array('content'=>'Cetak Data', 'type'=>'submit', 'class'=>'btn btn-info')) ?>
                     <?php echo anchor('/master_barang_wip/master_barang_wip/', 'Tambah Item', 'class="btn btn-warning " role="button" '); ?>
					<?php echo anchor('/master_barang_wip/master_barang_wip/export', 'Export Data', 'class="btn btn-info " role="button" '); ?>
                    </div>
            </form>
        </div>
	
        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $master_barang_wip ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->
