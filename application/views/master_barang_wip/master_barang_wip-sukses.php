<div class="container" id="daftar-sukses">
    <p class="h2">Informasi Untuk Penginputan Master Barang WIP</p>
    <hr>

    <div class="alert alert-success alert-dismissible" role="alert">
        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Penginputan Berhasil.
    </div>
    
    <p>Anda dapat kembali ke halaman Master Barang WIP di <?php echo anchor('/master_barang_wip/master_barang_wip/view', 'Master Barang WIP'); ?>.</p>

</div> <!-- container -->
