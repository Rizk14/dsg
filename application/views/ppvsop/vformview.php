<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan PP VS OP</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php if ($gudang != '0') { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else { echo "Semua"; } ?><br>
<!--
Kelompok: <?php if ($kelompok != '0') { echo $kode_perkiraan." - ".$nama_kelompok; } else { echo "All"; } ?><br>
Jenis Barang: <?php if ($jns_brg != '0') { echo $kode_jenis_brg." - ".$nama_jenis_brg; } else { echo "All"; } ?><br>
-->
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
<!--
Jenis Laporan: <?php echo $nama_jenis ?><br>
-->
<!--
<i>*) Data yang ditampilkan sudah dalam satuan konversi<br>
*) Data harga adalah diluar pajak<br><br></i>
-->
<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('ppvsop/cform/exportexcel', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="hidden" name="kelompok" value="<?php echo $kelompok ?>" >
<input type="hidden" name="jenis_brg" value="<?php echo $jns_brg ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		  <th>Nomor PP</th>
		 <th>Tanggal PP</th>
		 <th>Departemen</th>
		 <th>List Barang</th>
		 <th>Satuan</th>
	
		 <th>Qty</th>
		 <th>Qty Pemenuhan OP</th>
		 <th>Qty Sisa</th>
	 </tr>
	
	</thead>
	<tbody>
		 <?php
			 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {

			
						 
			 	 $bl	= array(
				 	'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember'
				 );
				 
			 	 $etgl	= explode("-",$query[$j]['tgl_pp'],strlen($query[$j]['tgl_pp'])); // YYYY-mm-dd
				 $hr	= substr($etgl[2],0,1)==0?substr($etgl[2],1,1):$etgl[2];
				 $nbl	= $etgl[1];
				 $ntgl	= $hr." ".$bl[$nbl]." ".$etgl[0];
				 
				 $etgl_last	= explode("-",$query[$j]['tgl_update'],strlen($query[$j]['tgl_update'])); // YYYY-mm-dd
				 $hr_last	= substr($etgl_last[2],0,1)==0?substr($etgl_last[2],1,1):$etgl_last[2];
				 $nbl_last	= $etgl_last[1];
				 $ntgl_last	= $hr_last." ".$bl[$nbl_last]." ".$etgl_last[0];
				 
				 echo "<tr class=\"record\">";
				 echo "<td nowrap>".$query[$j]['no_pp']."</td>";
				 echo "<td>".$ntgl."</td>";
				 echo "<td>".$query[$j]['nama_bagian']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
		
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_op'],2,'.','');
						  if ($var_detail[$k]['count_op'] > 1)
							 echo " (".$var_detail[$k]['count_op']." OP)";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo number_format($var_detail[$k]['sisanya'],2,'.','');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 

				echo "</tr>";
					}
		   } 
				 ?>
 	</tbody>
</table><br>
</div>
