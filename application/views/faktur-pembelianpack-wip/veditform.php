<h3>Data Faktur Pembelian WIP Makloon Packing</h3><br>
<a href="<?php echo base_url(); ?>index.php/faktur-pembelianpack-wip/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/faktur-pembelianpack-wip/cform/view">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	$('#pilih_faktur').click(function(){
		var id_ujh= jQuery('#id_unit_packing').val();
		
		
		var no_fp= jQuery('#no_fp').val();
		var urlnya = "<?php echo base_url(); ?>index.php/faktur-pembelianpack-wip/cform/show_popup_pembelian/E/"+id_ujh+"/"+no_fp;
		
		//var urlnya = "<?php echo base_url(); ?>index.php/faktur-pembelianpack-wip/cform/show_popup_pembelian/E/"+id_sup;
		openCenteredWindow(urlnya);
	  });
	
	$('#unit_packing').change(function(){
	  	    $("#no_sj").val('');	
	  	    $("#jum").val('0');	
	  });
	  
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_faktur() {
	var no_fp= $('#no_fp').val();
	var tgl_fp= $('#tgl_fp').val();
	var no_sj= $('#no_sj').val();
	var jum= $('#jum').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	
	
	
	if (no_sj == '') {
		alert("Nomor SJ harus dipilih..!");
		$('#no_sj').focus();
		return false;
	}
	if (no_fp == '') {
		alert("No Faktur harus diisi..!");
		$('#no_fp').focus();
		return false;
	}
	if (tgl_fp == '') {
		alert("Tanggal Faktur harus dipilih..!");
		$('#tgl_fp').focus();
		return false;
	}
	if (jum == '' || jum == '0') {
		$('#jum').focus();
		alert("Jumlah Total harus diisi..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-pembelianpack-wip/cform/updatedata" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_faktur" value="<?php echo $id_faktur ?>">
<input type="hidden" name="no_faktur_lama" value="<?php echo $query[0]['no_faktur'] ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">

<input type="hidden" name="cstatus_lunas" value="<?php echo $cstatus_lunas ?>">
<input type="hidden" name="cunit_packing" value="<?php echo $cunit_packing ?>">
<input type="hidden" name="tgl_awal" value="<?php echo $tgl_awal ?>">
<input type="hidden" name="tgl_akhir" value="<?php echo $tgl_akhir ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<br>
Edit Data<br><br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >

  <tr>
	<td>unit_packing </td>
	<td><select name="id_unit_packing" id="id_unit_packing" disabled="true">
				
				<?php foreach ($list_unit_packing as $ujh) { ?>
					<option value="<?php echo $ujh->id ?>" <?php if ($ujh->id == $query[0]['id_unit_packing']) { ?> selected="true" <?php } ?> ><?php echo $ujh->kode_unit." - ". $ujh->nama ?></option>
				<?php } ?>
				</select>
		<input type="hidden" name="unit_packing" id="unit_packing" value="<?php echo $query[0]['id_unit_packing'] ?>">
	</td>
</tr>

  <tr>
		<td width="15%">Nomor SJ</td>
		<td width="70%"> <input type="text" name="no_sjmasukpembelianpack" id="no_sjmasukpembelianpack" value="<?php echo $query[0]['no_sjmasukpembelianpack'] ?>" size="40" maxlength="40" readonly="true">
		<input type="hidden" name="id_sj" id="id_sj" value="<?php echo $query[0]['id_sj'] ?>">
		&nbsp; <input type="hidden" name="no_sj_lama" id="no_sj_lama" value="<?php echo $query[0]['no_sjmasukpembelianpack'] ?>" size="40" maxlength="40">
		<input type="hidden" name="id_sj_lama" id="id_sj_lama" value="<?php echo $query[0]['id_sj'] ?>">
		<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data sj pembelian">
		</td>
	</tr>
  <tr>
    <td>Nomor Faktur</td>
    <td>
      <input name="no_fp" type="text" id="no_fp" size="20" maxlength="20" value="<?php echo $query[0]['no_faktur'] ?>">
    </td>
  </tr>
  <tr>
    <td>Tanggal Faktur</td>
    <td>
	<label>
      <input name="tgl_fp" type="text" id="tgl_fp" size="10" value="<?php echo $query[0]['tgl_faktur'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_fp" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_fp,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td>Jumlah Total</td>
    <td>
      <input name="jum2" type="text" id="jum2" size="10" maxlength="10" value="<?php echo $query[0]['jumlah'] ?>" style="text-align:right;" readonly="true">
      <input name="jum" type="hidden" id="jum" value="<?php echo $query[0]['jumlah'] ?>">
    </td>
  </tr>
		<?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "faktur-pembelianpack-wip/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "faktur-pembelianpack-wip/cform/cari/".$cstatus_lunas."/".$cunit_packing."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
        ?>
  
  <tr><td>&nbsp;</td>
	<td><input type="submit" name="submit" value="Edit" onclick="return cek_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
  </tr>

</table>
</form>
