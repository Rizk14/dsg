<h3>Data SJ Keluar Barang WIP Dari Gudang QC</h3><br>
<a href="<?php echo base_url(); ?>index.php/wip/cform/addsjkeluar">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/wip/cform/viewsjkeluar">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>" . $msg . "</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>

<script>
	//tambah
	$(function() {
		//$("#no").val('2');

		//generate_nomor();

		$("#addrow").click(function() {
			//inisialisasi untuk id unik
			var no = $("#no").val();
			var n = parseInt(no) - 1;
			//copy last row
			var lastRow = $("#tabelku tr:last").clone();
			//----------------SETTING KONDISI ROW BARU*****
			//******no*************************************
			var num = "#num_" + n;
			var new_num = "#num_" + no;
			$(num, lastRow).attr("id", "num_" + no);
			$(new_num, lastRow).html(no);
			//******end no*********************************

			//*****kode_brg_wip*************************************
			var kode_brg_wip = "#kode_brg_wip_" + n;
			var new_kode_brg_wip = "#kode_brg_wip_" + no;
			$(kode_brg_wip, lastRow).attr("id", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("name", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("onkeyup", "cari('" + no + "', this.value);");
			$(new_kode_brg_wip, lastRow).val('');
			//*****end kode_brg_wip*********************************

			//******div infobrgwip*************************************
			var infobrgwip = "#infobrgwip_" + n;
			var new_infobrgwip = "#infobrgwip_" + no;
			$(infobrgwip, lastRow).attr("id", "infobrgwip_" + no);

			$(new_infobrgwip, lastRow).html("<input type='hidden' id='id_brg_wip_" + no + "' name='id_brg_wip_" + no + "' value=''>" +
				"<input type='text' id='nama_brg_wip_" + no + "' name='nama_brg_wip_" + no + "' value='' readonly='true' size='40'>");

			//******div qtywarna*************************************
			var qtywarna = "#qtywarna_" + n;
			var new_qtywarna = "#qtywarna_" + no;
			$(qtywarna, lastRow).attr("id", "qtywarna_" + no);
			$(new_qtywarna, lastRow).html("<input type='hidden' id='temp_qty_" + no + "' name='temp_qty_" + no + "' value=''>");

			//*****ket_detail*************************************
			var ket_detail = "#ket_detail_" + n;
			var new_ket_detail = "#ket_detail_" + no;
			$(ket_detail, lastRow).attr("id", "ket_detail_" + no);
			$(new_ket_detail, lastRow).attr("name", "ket_detail_" + no);
			$(new_ket_detail, lastRow).val('');
			//*****end ket_detail*************************************	

			//*****ket_warna*************************************
			/*	var ket_warna="#ket_warna_"+n;
				var new_ket_warna="#ket_warna_"+no;
				$(ket_warna, lastRow).attr("id", "ket_warna_"+no);
				$(new_ket_warna, lastRow).attr("name", "ket_warna_"+no);		
				$(new_ket_warna, lastRow).val('');		*/
			//*****end ket_warna*************************************	

			//button pilih*****************************************
			// 25-03-2014 DIKOMEN, SOALNYA PAKE KETIK KODE TRUS PAKE JQUERY MUNCULIN NAMA BRG DAN ITEM WARNA
			/* var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);		 
            
		 var  even_klik= "var x= $('#gudang').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/wip/cform/show_popup_brgjadi/'+ x+'/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik); */
			//end button pilih		

			//----------------END SETTING KONDISI ROW BARU*		
			//tambahin row nya sesuai settingan
			$("#tabelku").append(lastRow);
			//no++
			var x = parseInt(no);
			$("#no").val(x + 1);

		});

		$("#deleterow").click(function() {
			var x = $("#no").val();
			var jumawal = $("#jumawal").val();
			// kita rubah supaya boleh hapus semuaa
			//if (parseInt(x) > parseInt(jumawal)) {
			if (x > 2) {
				$("#tabelku tr:last").remove();
				var x = $("#no").val();
				var y = x - 1;
				$("#no").val(y);
			}
		});

	});
</script>
<script type="text/javascript">
	function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth / 2) - (width / 2));
		var top = parseInt((screen.availHeight / 2) - (height / 2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

	function cek_sj() {
		var no_sj = $('#no_sj').val();
		var tgl_sj = $('#tgl_sj').val();
		var id_unit_jahit = $('#id_unit_jahit').val();
		var id_unit_packing = $('#id_unit_packing').val();
		var jenis_keluar = $('#jenis_keluar').val();
		var s = 0;
		kon = window.confirm("Yakin akan simpan data ini ??");

		if (kon) {
			if (no_sj == '') {
				alert("Nomor SJ harus diisi..!");
				s = 1;
				return false;
			}
			if (tgl_sj == '') {
				alert("Tanggal SJ harus dipilih..!");
				s = 1;
				return false;
			}
			// 23-03-2015
			if (jenis_keluar != '2' && jenis_keluar != '4' && jenis_keluar != '5') {
				if (id_unit_jahit == '0' && id_unit_packing == '0') {
					alert("Unit Jahit dan Unit Packing harus dipilih salah satu, tidak boleh kosong dua2nya..!");
					s = 1;
					return false;
				}
			}

			var jum = $('#no').val() - 1;

			if (jum > 0) {
				for (var k = 1; k <= jum; k++) {

					if ($('#nama_brg_wip_' + k).val() == '') {
						alert("Data item barang tidak boleh ada yang kosong...!");
						s = 1;
						return false;
					}
					/*	if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
							alert("Data qty tidak boleh 0 / kosong...!");
							s = 1;
							return false;
						}
						if (isNaN($('#qty_'+k).val()) ) {
							alert("Qty harus berupa angka..!");
							s = 1;
							return false;
						} */
					if ($('#temp_qty_' + k).val() == '') {
						alert("Data item barang " + $('#kode_brg_wip_' + k).val() + " belum ada warnanya, silahkan input dulu di menu Master Warna Brg WIP...!");
						s = 1;
						return false;
					}
				}
			} else {
				alert("Data detail tidak ada");
				s = 1;
				return false;
			}

			if (s == 0)
				return true;
		} // end if kon
		else
			return false;
	}

	function cari(posisi, kodebrgwip) {
		$.ajax({
			type: 'post',
			url: '<?php echo base_url(); ?>index.php/wip/cform/caribrgwip',
			data: 'kode_brg_wip=' + kodebrgwip + '&posisi=' + posisi,
			success: function(response) {
				$("#infobrgwip_" + posisi).html(response);
			}
		});

		$.ajax({
			type: 'post',
			url: '<?php echo base_url(); ?>index.php/wip/cform/additemwarna',
			data: 'kode_brg_wip=' + kodebrgwip + '&posisi=' + posisi,
			success: function(response) {
				$("#qtywarna_" + posisi).html(response);
				window.close();
			}
		});
	}
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" enctype="multipart/form-data"> <!-- action="<?php echo base_url(); ?>index.php/wip/cform/updatedatasjkeluar" method="post" -->
	<input type="hidden" name="id_sj" value="<?php echo $query[0]['id'] ?>">
	<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
	<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
	<input type="hidden" name="tgl_awal" value="<?php echo $tgl_awal ?>">
	<input type="hidden" name="tgl_akhir" value="<?php echo $tgl_akhir ?>">
	<input type="hidden" name="unit_jahit" value="<?php echo $id_unit_jahit ?>">
	<input type="hidden" name="unit_packing" value="<?php echo $id_unit_packing ?>">
	<input type="hidden" name="gudang" value="<?php echo $gudang ?>">
	<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
	<input type="hidden" name="caribrg" value="<?php echo $caribrg ?>">
	<input type="hidden" name="filterbrg" value="<?php echo $filterbrg ?>">
	<input type="hidden" name="cjenis_keluar" value="<?php echo $jenis_keluar ?>">
	<input type="hidden" name="id_gudang_lama" value="<?php echo $query[0]['id_gudang'] ?>">
	<input type="hidden" name="id_unit_jahit_lama" value="<?php echo $query[0]['id_unit_jahit'] ?>">
	<input type="hidden" name="id_unit_packing_lama" value="<?php echo $query[0]['id_unit_packing'] ?>">

	<?php $detail_sj = $query[0]['detail_sj'];
	$no = 1;
	$jumawal = count($detail_sj) + 1;

	foreach ($detail_sj as $hitung) {
		$no++;
	}
	?>
	Edit Data

	<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
	<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
	<div align="center">
		<label id="status"></label>
		<br>

		<table class="proit-view" width="100%" cellspacing="2" cellpadding="1">
			<tr>
				<td width="15%">Lokasi Gudang</td>
				<td> <select name="gudang" id="gudang" disabled>
						<?php foreach ($list_gudang as $gud) { ?>
							<option value="<?php echo $gud->id ?>" <?php if ($query[0]['id_gudang'] == $gud->id) { ?> selected <?php } ?>><?php echo "[" . $gud->nama_lokasi . "] " . $gud->kode_gudang . "-" . $gud->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td width="15%">Nomor SJ</td>
				<td>
					<input name="no_sj" type="text" id="no_sj" size="10" maxlength="20" value="<?php echo $query[0]['no_sj'] ?>" readonly>
				</td>

			</tr>
			<tr>
				<td width="15%">Nomor BP</td>
				<td>
					<input name="no_bp" type="text" id="no_bp" size="10" maxlength="20" value="<?php echo $query[0]['no_bp'] ?>" readonly>
				</td>

			</tr>
			<tr>
				<td>Tanggal SJ</td>
				<td>
					<label>
						<input name="tgl_sj" type="text" id="tgl_sj" size="10" value="<?php echo $query[0]['tgl_sj'] ?>" readonly="true">
					</label>
					<img alt="" id="tgl_sj" align="middle" title="Pilih tgl.." src="<?php echo base_url(); ?>images/calendar.gif">
				</td>
			</tr>
			<tr>
				<td width="15%">Periode Forecast</td>
				<td>
					<select name="bln_forecast" id="bln_forecast" disabled>
						<option selected="selected">Bulan</option>
						<?php
						$bulan = array("Bulan", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
						$jlh_bln = count($bulan);
						for ($c = 1; $c < $jlh_bln; $c += 1) {
							if (strlen($c) == 1) {
								$d = "0" . $c;
							} else {
								$d = $c;
							}
						?>
							<option value="<?php echo $d ?>" <?php if ($d == $query[0]['bln_forecast']) { ?> selected <?php } ?>><?php echo $bulan[$c] ?></option>
						<?php
						}
						?>
						<input name="thn_forecast" type="text" id="thn_forecast" size="4" value='<?php echo $query[0]['thn_forecast'] ?>' maxlength="4" readonly>
				</td>
			</tr>
			<tr>
				<td>Jenis Keluar</td>
				<td> <select name="jenis_keluar" id="jenis_keluar" disabled>
						<option value="1" <?php if ($query[0]['jenis'] == '1') { ?> selected <?php } ?>>Keluar bagus ke unit packing</option>
						<option value="2" <?php if ($query[0]['jenis'] == '2') { ?> selected <?php } ?>>Keluar bagus ke gudang jadi</option>
						<option value="3" <?php if ($query[0]['jenis'] == '3') { ?> selected <?php } ?>>Lain-lain (Retur ke unit jahit)</option>
						<option value="4" <?php if ($query[0]['jenis'] == '4') { ?> selected <?php } ?>>Lain-lain (Lainnya)</option>
						<option value="5" <?php if ($query[0]['jenis'] == '5') { ?> selected <?php } ?>>Keluar Bagus ke gudang QC lain</option>
						<option value="6" <?php if ($query[0]['jenis'] == '6') { ?> selected <?php } ?>>Keluar Retur ke gudang QC lain</option>
						<option value="6" <?php if ($query[0]['jenis'] == '6') { ?> selected <?php } ?>>Keluar Lain-lain (Penjualan BS)</option>
					</select>
				</td>
			</tr>

			<tr>
				<td>Unit Jahit</td>
				<td> <select name="id_unit_jahit" id="id_unit_jahit" disabled>
						<option value="0">Tidak Ada</option>
						<?php foreach ($list_unit_jahit as $jht) { ?>
							<option value="<?php echo $jht->id ?>" <?php if ($query[0]['id_unit_jahit'] == $jht->id) { ?> selected <?php } ?>><?php echo $jht->kode_unit . "-" . $jht->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Unit Packing</td>
				<td> <select name="id_unit_packing" id="id_unit_packing" disabled>
						<option value="0">Tidak Ada</option>
						<?php foreach ($list_unit_packing as $pck) { ?>
							<option value="<?php echo $pck->id ?>" <?php if ($query[0]['id_unit_packing'] == $pck->id) { ?> selected <?php } ?>><?php echo $pck->kode_unit . "-" . $pck->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>

			<tr>
				<td>Keterangan</td>
				<td>
					<input name="ket" type="text" id="ket" size="30" value="<?php echo $query[0]['keterangan'] ?>" readonly>
				</td>
			</tr>

			<tr>
				<td colspan="2"><br>
					<form name="myform">
						<table id="tabelku" border="0" align="center" cellpadding="1" cellspacing="2" class="proit-view">
							<tr>
								<td colspan="5" align="left">
									<!-- <input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
									<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang"> -->
								</td>
							</tr>
							<tr>
								<th width="20">No</th>
								<th>Kode</th>
								<th>Nama Barang WIP</th>
								<th>Qty</th>
								<!--   <th>Ket Qty Per Warna</th> -->
								<th>Keterangan</th>
							</tr>
							<?php $i = 1;
							if (count($query[0]['detail_sj']) == 0) {
							?>
								<tr align="center">
									<td align="center" id="num_1">1</td>
									<td nowrap="nowrap">
										Data tidak ada</td>

								</tr>
								<?php
							} else {
								$detail_sj = $query[0]['detail_sj'];
								for ($k = 0; $k < count($detail_sj); $k++) {
								?>

									<tr align="center">
										<td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
										<td><input name="kode_brg_wip_<?php echo $i ?>" type="text" id="kode_brg_wip_<?php echo $i ?>" size="10" value="<?php echo $detail_sj[$k]['kode_brg_wip'] ?>" readonly /></td>
										<td style="white-space:nowrap;">
											<div id="infobrgwip_<?php echo $i ?>">
												<input name="nama_brg_wip_<?php echo $i ?>" type="text" id="nama_brg_wip_<?php echo $i ?>" size="40" readonly="true" value="<?php echo str_replace("\"", "&quot;", $detail_sj[$k]['nama_brg_wip']) ?>" readonly />
												<input name="id_brg_wip_<?php echo $i ?>" type="hidden" id="id_brg_wip_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['id_brg_wip'] ?>" />
												<input name="stok_<?php echo $i ?>" type="hidden" id="stok_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['jum_stok'] ?>" readonly />
												<!--   <input title="browse data barang" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" 
           onclick="javascript: var x= $('#gudang').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/wip/cform/show_popup_brgjadi/'+ x+'/<?php echo $i ?>');" > -->
											</div>
										</td>

										<!-- <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="5" value="<?php //echo $detail_sj[$k]['qty'] 
																																			?>" /></td> -->
										<td>
											<div id="qtywarna_<?php echo $i ?>" align="right">
												<?php
												if (is_array($detail_sj[$k]['detail_warna'])) {
													$var_detail = $detail_sj[$k]['detail_warna'];
													$hitung = count($var_detail);
													for ($zz = 0; $zz < count($var_detail); $zz++) {
														echo $var_detail[$zz]['nama_warna'] . "&nbsp;";
												?>
														<input type="text" name="qty_warna_<?php echo $i ?>[]" style="text-align: right;" value="<?php echo $var_detail[$zz]['qty_warna'] ?>" size="3" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'" readonly>
														<input type="hidden" name="id_warna_<?php echo $i ?>[]" value="<?php echo $var_detail[$zz]['id_warna'] ?>"><br>
													<?php
													}
													?>
													<input type="hidden" id="temp_qty_<?php echo $i ?>" name="temp_qty_<?php echo $i ?>" value="ada">
												<?php
												} else {
												?>
													<input type="hidden" id="temp_qty_<?php echo $i ?>" name="temp_qty_<?php echo $i ?>" value="">
												<?php
												}
												?>
											</div>
											<hr>
										</td>

										<!-- <td><input name="ket_warna_<?php echo $i ?>" type="text" id="ket_warna_<?php echo $i ?>" size="30" value="<?php //echo $detail_sj[$k]['ket_qty_warna'] 
																																						?>" /></td> -->
										<td><input name="ket_detail_<?php echo $i ?>" type="text" id="ket_detail_<?php echo $i ?>" size="30" value="<?php echo $detail_sj[$k]['keterangan'] ?>" readonly /></td>

									</tr>
							<?php $i++;
								} // end foreach 
							}
							?>
						</table>

					</form>
					<div align="center"><br>
						<?php
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "wip/cform/viewsjkeluar/" . $cur_page;
						else
							$url_redirectnya = "wip/cform/carisjkeluar/" . $tgl_awal . "/" . $tgl_akhir . "/" . $id_unit_jahit . "/" . $id_unit_packing . "/" . $gudang . "/" . $carinya . "/" . $caribrg . '/' . $filterbrg . '/' . $jenis_keluar . "/" . $cur_page;
						//$url_redirectnya = "wip/cform/carisjkeluar/".$tgl_awal."/".$tgl_akhir."/".$kode_unit_jahit."/".$kode_unit_packing."/".$gudang."/".$carinya."/".$cur_page;
						?>
						<!-- <input type="submit" name="submit2" value="Simpan" onclick="return cek_sj();">&nbsp; -->
						<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">

					</div>
				</td>
			</tr>

		</table>
	</div>
</form>