<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Transaksi Barang Global</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
<?php 
$attributes = array('name' => 'f_global', 'id' => 'f_global');
echo form_open('wip/creport/export_excel_laptransaksiglobal', $attributes); ?>
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="nama_bulan" value="<?php echo $nama_bulan ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="60%">
	<thead>
	 <tr class="judulnya">
		<th>Minggu Ke</th>
		 <th>Produksi Ke Unit Jahit</th>
		 <th>Unit Jahit Ke WIP</th>
		 <th>WIP Ke Packing</th>
		 <th>Gudang Ke Distributor</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
		?>
					<tr>
						<?php
							$pisah1 = explode("-", $query[$j]['awal']);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$awal = $tgl1."-".$bln1."-".$thn1;
							
							$pisah1 = explode("-", $query[$j]['akhir']);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$akhir = $tgl1."-".$bln1."-".$thn1;
						?>
						<td align="center"><?php echo $query[$j]['minggu'] ?> (<?php echo $awal ?> s/d <?php echo $akhir ?>)</td>
						<td align="right"><?php echo $query[$j]['produksi2unit1'] ?>&nbsp;</td>
						<td align="right"><?php echo $query[$j]['jahit2wip1'] ?>&nbsp;</td>
						<td align="right"><?php echo $query[$j]['wip2packing1'] ?>&nbsp;</td>
						<td align="right"><?php echo $query[$j]['gdjadi2dist1'] ?>&nbsp;</td>
					</tr>
		<?php			
				}
			}
		?>
		
 	</tbody>
	</table><br>
</div>
