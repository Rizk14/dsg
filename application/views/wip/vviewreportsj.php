<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan SJ Keluar Barang WIP Dari Gudang QC</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('wip/cform/export_excel_report', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table class="table table-condensed" border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya" >
		 <th width='3%'>No</th>
		 <th width='8%'>No SJ</th>
		 <th width='10%'>Tanggsl SJ</th>
		 <th width='10%'>Kode Barang</th>
		 <th width='35%'>Nama Barang</th>
		 <th width='8%'><center>Quantity</center></th>
		 <th width='40%'>Keterangan</th>
		
	 </tr>

	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {

			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['no_sj']."</td>";
				 echo    "<td>".$query[$j]['tgl_sj']."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 echo    "<td align='center'> ".number_format($query[$j]['qty'],0,',','.')."</td>";
				 echo    "<td>".$query[$j]['keterangan']."</td>";
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>

