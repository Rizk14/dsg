<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>

</head>

<body onLoad="closeMe();">

<?php
echo "Proses mencetak, silahkan tunggu sebentar. Printer URI: ".$ip_address.$printer_uri;
?>

<?php
include ("php/fungsi.php");

//$get_attributes	= false;
$waktu	= date("H:i:s");
$line	= 80;
$line_first	= 2;
$line_last	= 2;

//if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php"); 
//} else {
//	include_once("printipp_classes/BasicIPP.php");
//}

$ipp = new PrintIPP();

$ipp->setHost($ip_address);
$ipp->setPrinterURI($printer_uri);
//$ipp->setCharset($charset);
//$ipp->setMimeMediaType($mime_media_type);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

// Function ;
/*
Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
Italic	= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)
*/

$List	= "";

$List	.= CHR(1).str_repeat(CHR(205),($line-2)).CHR(0)."\n";

$List	.= "\n";

$range	= $line-($line_first+strlen('nota retur')+strlen('pt. tirai pelangi nusantara')+$line_last+1);

$List	.= str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(107).CHR(48).CHR(27).CHR(119).CHR(1). CHR(27).CHR(69)."Nota Retur".CHR(27).CHR(70).CHR(27).CHR(119).CHR(0).CHR(27).CHR(107).CHR(48).CHR(0).str_repeat(" ",($range)).CHR(27).CHR(69)."PT. TIRAI PELANGI NUSANTARA".CHR(27).CHR(70).str_repeat(" ",$line_last)."\n";

$List	.= str_repeat(CHR(205),($line-2))."\n";

$range0	= $line-($line_first+strlen('nomor nota')+2+strlen($query[0]['no_nota'])+strlen('tanggal nota')+2+strlen($query[0]['tgl_nota'])+$line_last);

$List	.= str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(69)."Nomor Nota".str_repeat(" ",2).$query[0]['no_nota'].CHR(27).CHR(70).CHR(0).str_repeat(" ",$range0).CHR(1).CHR(27).CHR(69)."Tanggal Nota".str_repeat(" ",2).$query[0]['tgl_nota'].CHR(27).CHR(70).CHR(0).str_repeat(" ",$line_last)."\n";
$List	.= "\n";

//$List	.= str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(69)."Kepada :".CHR(27).CHR(70).CHR(0)."\n";

$pjgnosj = strlen($query[0]['no_sj']);
if ($pjgnosj > 10)
	$rangenosj = $range0-13;
else
	$rangenosj = $range0+13;
$List	.= str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(69)."Kepada :".CHR(27).CHR(70).CHR(0).str_repeat(" ",$rangenosj).CHR(1).CHR(27).CHR(69)."Nomor SJ: ".CHR(27).CHR(70).CHR(0).$query[0]['no_sj'].str_repeat(" ",$line_last)."\n";
$List	.= str_repeat(" ",$line_first).$query[0]['nama_unit_jahit']."\n";
$List.= "\n";

$List	.= str_repeat(CHR(196),($line-2))."\n";
$range4	= $line-($line_first+strlen('no')+1+strlen('kode barang')+1+strlen('nama barang')+strlen('unit')+1+strlen('harga')+1+strlen('jumlah')+1);
$List	.= CHR(1).CHR(27).CHR(69).str_repeat(" ",$line_first)."No".str_repeat(" ",1)."Kode Barang".str_repeat(" ",1)."Nama Barang".str_repeat(" ",($range4-11))."Unit".str_repeat(" ",4)."Harga".str_repeat(" ",5)."Jumlah".str_repeat(" ",2).CHR(27).CHR(70).CHR(0)."\n";
$List	.= str_repeat(CHR(196),($line-2))."\n";

$no	= 1;
$spcno	= 6; // sesuaikan dgn header No + 2
$spckb	= 20;
$spcnb	= 58;
$spcunit= 9;
$spcprice	= 12;
$spcjml	= 15;

$arrno	= array();
$arrkb	= array();
$arrnb	= array();
$arrunit= array();
$arrprice= array();
$arrjml	= array();

$spunit	= array();
$spprice	= array();
$spamount	= array();

$lup	= 0;
$jmlqty		= array();
$uprice		= array();
$tprice		= array();

	$detail_nota = $query[0]['detail_nota'];
	for($k=0;$k<count($detail_nota);$k++){

	if($lup<11) {		
		
		//if($qvalue->num_rows()>0) {
			//$row_value	= $qvalue->row();
			$jmlqty[$lup]	= $detail_nota[$k]['qty'];
			$uprice[$lup]	= $detail_nota[$k]['harga'];
			$tprice[$lup]	= $detail_nota[$k]['total'];
		//} 

		$arrno[$lup]	= (strlen($no)<=$spcno)?$spcno-strlen($no):0;
		$arrkb[$lup]	= (strlen(trim($detail_nota[$k]['kode_brg_jadi']))<=$spckb)?$spckb-strlen($detail_nota[$k]['kode_brg_jadi']):0;
		$arrnb[$lup]	= (strlen($detail_nota[$k]['nama_brg_jadi'])<=$spcnb)?$spcnb-strlen($detail_nota[$k]['nama_brg_jadi']):0;
		$arrunit[$lup]	= (strlen($jmlqty[$lup])<=$spcunit)?$spcunit-strlen($jmlqty[$lup]):strlen($jmlqty[$lup]);
		$arrprice[$lup]	= (strlen($uprice[$lup])<=$spcprice)?$spcprice-strlen($uprice[$lup]):strlen($uprice[$lup]);
		$arrjml[$lup]	= (strlen($tprice[$lup])<=$spcjml)?$spcjml-strlen($tprice[$lup]):strlen($tprice[$lup]);
	
		$spunit[$lup]	= strlen(number_format($jmlqty[$lup]))<=$spcunit?$spcunit-strlen(number_format($jmlqty[$lup])):0;
		$spprice[$lup]	= strlen(number_format($uprice[$lup]))<=$spcprice?$spcprice-strlen(number_format($uprice[$lup])):0;
		$spamount[$lup]	= strlen(number_format($tprice[$lup]))<=$spcjml?$spcjml-strlen(number_format($tprice[$lup])):0;

		$List	.= CHR(15).str_repeat(" ",3).$no.str_repeat(" ",$arrno[$lup]).trim($detail_nota[$k]['kode_brg_jadi']).str_repeat(" ",$arrkb[$lup]).strtoupper($detail_nota[$k]['nama_brg_jadi']).str_repeat(" ",$arrnb[$lup]).str_repeat(" ",$spunit[$lup]).number_format($jmlqty[$lup]).str_repeat(" ",3).str_repeat(" ",$spprice[$lup]).number_format($uprice[$lup]).str_repeat(" ",3).str_repeat(" ",$spamount[$lup]).number_format($tprice[$lup]).CHR(18)."\n";

	}
	$no+=1;
	$lup+=1;
}

$List	.= CHR(1).str_repeat(CHR(196),($line-2)).CHR(0)."\n";

$bts_rp_nilai_uang	= 20;
$space_awal_rp	= 1;
$bts_kotak_dg_jml	= 3;

$xx1	= $bts_rp_nilai_uang-strlen(number_format($query[0]['gtotal']));
$left6	= ( $line-($line_first+strlen('jumlah')+1+strlen('rp.')+$xx1+strlen(number_format($query[0]['gtotal']))) )-($bts_kotak_dg_jml+24);

$List	.= CHR(15).str_repeat(" ",77).str_repeat(" ",$bts_kotak_dg_jml-2).str_repeat(" ",$left6).CHR(27).CHR(69)."Jumlah".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx1).number_format($query[0]['gtotal']).CHR(0)."\n";

/*$teks1	= 66; //"1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,";
$teks2	= 39; //"kecuali ada perjanjian terlebih dahulu.";
$teks3	= 48; //"2. Faktur asli merupakan bukti pembayaran yg sah";
$teks4	= 63; //"3. Pembayaran dgn cek/ giro baru dianggap sah setelah diuangkan"; */

/*$xx1	= $bts_rp_nilai_uang-strlen(number_format($jumlah));
$xx2	= $bts_rp_nilai_uang-strlen(number_format($diskon));
$xx3	= $bts_rp_nilai_uang-strlen(number_format($dpp));
$xx4	= $bts_rp_nilai_uang-strlen(number_format($nilai_ppn));
$xx5	= $bts_rp_nilai_uang-strlen(number_format($nilai_faktur));

$left1	= $line-($line_first+1+strlen('tgl. jatuh tempo :')+strlen($tgljthtempo)+1+strlen('jumlah')+1+strlen('rp.')+$xx1+strlen(number_format($jumlah)));
$left2	= ( $line-(1+strlen('diskon')+1+strlen('rp.')+$xx2+strlen(number_format($diskon))) )-($bts_kotak_dg_jml+24);
$left3	= ( $line-($line_first+strlen('dpp')+1+strlen('rp.')+$xx3+strlen(number_format($dpp))) )-($bts_kotak_dg_jml+24);
$left4	= ( $line-($line_first+strlen('nilai faktur')+1+strlen('rp.')+$xx5+strlen(number_format($nilai_faktur))) )-($bts_kotak_dg_jml+24);
$left5	= ( $line-($line_first+strlen('ppn')+1+strlen('rp.')+$xx4+strlen(number_format($nilai_ppn))) )-($bts_kotak_dg_jml+24);
$left6	= ( $line-($line_first+strlen('jumlah')+1+strlen('rp.')+$xx1+strlen(number_format($jumlah))) )-($bts_kotak_dg_jml+24);

$right4	= $left4;
$left44	= ($line-($right4+1+$bts_kotak_dg_jml))-24;

$repeat1	= 1;
$repeat2	= strlen($teks1)-strlen($teks2);
$repeat3	= strlen($teks1)-strlen($teks3);
$repeat4	= strlen($teks1)-strlen($teks4);

$grs_atas	= (2+strlen($teks1)+2)-2;
$grs_bwh	= (2+strlen($teks1)+2)-2;

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",77).str_repeat(" ",$bts_kotak_dg_jml-2).str_repeat(" ",$left6).CHR(27).CHR(69)."Jumlah".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx1).number_format($jumlah).CHR(0)."\n";

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).str_repeat(" ",$line_first).CHR(27).CHR(69)."Tgl. Jatuh Tempo :".CHR(27).CHR(70).$tgljthtempo."\n";

$List	.= CHR(1).CHR(15).CHR(218).str_repeat(CHR(196),71).CHR(191).CHR(18).CHR(0)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,".str_repeat(" ",$repeat1).str_repeat(" ",3).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+1).str_repeat(" ",$left2).CHR(27).CHR(69)."Diskon".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx2).number_format($diskon).CHR(18)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0).str_repeat(" ",3)."kecuali ada perjanjian terlebih dahulu.".str_repeat(" ",$repeat2).str_repeat(" ",28).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",($bts_kotak_dg_jml+15))." ".str_repeat(CHR(196),$left44).CHR(18)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."2. Faktur asli merupakan bukti pembayaran yg sah".str_repeat(" ",$repeat3).str_repeat(" ",22).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+2).str_repeat(" ",$left3).CHR(27).CHR(69)."DPP".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx3).number_format($dpp).CHR(18)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."3. Pembayaran dgn cek/ giro baru dianggap sah setelah diuangkan".str_repeat(" ",$repeat4).str_repeat(" ",7).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+2).str_repeat(" ",$left5).CHR(27).CHR(69)."PPN".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx4).number_format($nilai_ppn).CHR(18)."\n";

$List	.= CHR(1).CHR(15).CHR(192).str_repeat(CHR(196),71).CHR(217).CHR(18).CHR(0);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",($bts_kotak_dg_jml+15))." ".str_repeat(CHR(196),$left44).CHR(18)."\n";

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",77).str_repeat(" ",$bts_kotak_dg_jml-2).str_repeat(" ",$left4).CHR(27).CHR(69)."Nilai Faktur".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx5).number_format($nilai_faktur).CHR(0)."\n";
*/
//$terbil=&Terbilang($query[0]['gtotal']);
$terbil = &terbilangxx($query[0]['gtotal'],3); 
$bts_terbil	= $line-(strlen($terbil));

if($bts_terbil<0)
 $bts_terbil = 1;

$List	.= CHR(1).CHR(15).str_repeat(" ",$bts_terbil)."(Terbilang :".$terbil." Rupiah.)".CHR(18).CHR(0)."\n";

$List	.= CHR(11); // Vertical Tab

$List	.= CHR(11);

$List	.= CHR(27).CHR(69).str_repeat(" ",$line_first)."Hormat Kami".CHR(27).CHR(70)."\n";

for($j=1;$j<=3;$j++) {
  $List	.= "\n";
}

//$List	.= CHR(1).CHR(27).CHR(88).CHR(28).CHR(0).CHR(27).CHR(69).str_repeat(" ",$line_first).$TtdPajak02.CHR(27).CHR(70)."\n";
$List	.= CHR(27).CHR(69).str_repeat(" ",$line_first)."Dewi Sudiyar".CHR(27).CHR(70)."\n";

/*if($lup < 22) {
 for($jj=$lup;$jj < 24;$jj++) {
   $List .= "\n";
 }
}

$List	.= str_repeat(CHR(205),($line-2))."\n";

for($jj=0;$jj < 7;$jj++) {
   $List .= "\n";
} */
//echo $List; die();
$ipp->setData($List);
$ipp->printJob();
?>
