<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Rekapitulasi Presentasi Kerja Unit Tahunan</h3><br><br>

<div>
Tahun: <?php echo $tahun ?><br><br>

<?php 
$attributes = array('name' => 'f_presentasi', 'id' => 'f_presentasi');
echo form_open('wip/creport/export_excel_rekappresentasikerjaunit', $attributes); ?>
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	
<?php
	$nomor = 0; $temp_unit = ""; $totalperunit=0; $jumgrupperunit=0;
	$tjanuari=0; $tfebruari=0; $tmaret=0; $tapril=0; $tmei=0; $tjuni=0; $tjuli=0; $tagustus=0; $tseptember=0;
	$toktober=0; $tnovember=0; $tdesember=0;
	$cjanuari=0; $cfebruari=0; $cmaret=0; $capril=0; $cmei=0; $cjuni=0; $cjuli=0; $cagustus=0; $cseptember=0;
	$coktober=0; $cnovember=0; $cdesember=0;
	
	$jum1=0; $cunit1=0; $temp_kel_unit = "";
	
	if (is_array($queryutama)) {
		for($aa=0;$aa<count($queryutama);$aa++){
?>
	
		<?php 
			if ($queryutama[$aa]['kode'] != $temp_kel_unit) {
				$temp_kel_unit = $queryutama[$aa]['kode'];
		?>
				<thead>
				<tr class="judulnya">
						<td colspan="16"><b><?php echo $queryutama[$aa]['kode']." - ".$queryutama[$aa]['nama'] ?></b></td>
				</tr>
				 <tr class="judulnya">
					 <th width="3%" rowspan="2">No</th>
					 <th width="10%" rowspan="2">Unit</th>
					 <th width="10%" rowspan="2">Grup</th>
					 <th colspan="12">Bulan</th>
					 <th width="10%" rowspan="2">Rata-Rata</th>
				 </tr>
				 <tr class="judulnya">
					<th width="10%">Januari</th>
					<th width="10%">Februari</th>
					<th width="10%">Maret</th>
					<th width="10%">April</th>
					<th width="10%">Mei</th>
					<th width="10%">Juni</th>
					<th width="10%">Juli</th>
					<th width="10%">Agustus</th>
					<th width="10%">September</th>
					<th width="10%">Oktober</th>
					<th width="10%">November</th>
					<th width="10%">Desember</th>
				 </tr>
				</thead>
				
		<?php

				$query = $queryutama[$aa]['data_rekap'];
		
				if (is_array($query)) {
					for($a=0;$a<count($query);$a++){
		
		?>
				<tr>
		<?php
				if ($temp_unit != $query[$a]['kode_unit']) {
					$temp_unit = $query[$a]['kode_unit'];
					$nomor++;
					echo "<td align='center'>".$nomor."</td>";
					echo "<td>&nbsp;".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</td>";
				}
				else {
					echo "<td align='center'>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
				}
		?>
		
		<td>&nbsp;<?php echo $query[$a]['nama_grup_jahit'] ?></td>
		<?php
			$listratabulan = $query[$a]['listratabulan'];
			
			$bulan=1; $total = 0; $cbulan=0;
			if (is_array($listratabulan)) {
				for($j=0;$j<count($listratabulan);$j++){
					//echo $bulan." ";
					if ($listratabulan[$j][$bulan] != 0) {
						$total+=$listratabulan[$j][$bulan];
						$cbulan++;
					}
					
					if ($bulan == 1) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tjanuari+=$listratabulan[$j][$bulan];
							$cjanuari++;
						}
					}
					else if ($bulan == 2) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tfebruari+=$listratabulan[$j][$bulan];
							$cfebruari++;
						}
					}
					else if ($bulan == 3) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tmaret+=$listratabulan[$j][$bulan];
							$cmaret++;
						}
					}
					else if ($bulan == 4) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tapril+=$listratabulan[$j][$bulan];
							$capril++;
						}
					}
					else if ($bulan == 5) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tmei+=$listratabulan[$j][$bulan];
							$cmei++;
						}
					}
					else if ($bulan == 6) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tjuni+=$listratabulan[$j][$bulan];
							$cjuni++;
						}
					}
					else if ($bulan == 7) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tjuli+=$listratabulan[$j][$bulan];
							$cjuli++;
						}
					}
					else if ($bulan == 8) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tagustus+=$listratabulan[$j][$bulan];
							$cagustus++;
						}
					}
					else if ($bulan == 9) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tseptember+=$listratabulan[$j][$bulan];
							$cseptember++;
						}
					}
					else if ($bulan == 10) {
						if ($listratabulan[$j][$bulan] != 0) {
							$toktober+=$listratabulan[$j][$bulan];
							$coktober++;
						}
					}
					else if ($bulan == 11) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tnovember+=$listratabulan[$j][$bulan];
							$cnovember++;
						}
					}
					else if ($bulan == 12) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tdesember+=$listratabulan[$j][$bulan];
							$cdesember++;
						}
					}
		?>
		<td align="right"><?php echo number_format($listratabulan[$j][$bulan], 2, '.',',') ?>&nbsp;</td>
		<?php		$bulan++;
				}
				//$totrata = $total/12;
				$totrata = $total/$cbulan;
				$totalperunit+=$totrata;
				$jumgrupperunit++;
			}
		?>
		<td align="right"><?php echo number_format($totrata, 2, '.',',') ?>&nbsp;</td>
	</tr>
			
	<?php 		//$nomor++;
				
				if ( isset($query[$a+1]['kode_unit']) && ($query[$a]['kode_unit'] != $query[$a+1]['kode_unit']) ) {
					
	?>
			<tr>
				<td colspan="15" align="right"><b>&nbsp;Rata-Rata Unit <?php echo $query[$a]['nama_unit'] ?>&nbsp;</b></td>
				<td align="right"><b><?php 
					if ($jumgrupperunit != 0)
						$rataperunit = $totalperunit/$jumgrupperunit;
					else
						$rataperunit = 0;
						
					$jum1+= $rataperunit;
					$cunit1++;
					
					echo number_format($rataperunit, 2, '.',',') ?>&nbsp;</b></td>
			</tr>
	<?php				
					$totalperunit = 0; $jumgrupperunit = 0;
				}
				else if ( !isset($query[$a+1]['kode_unit'])) {
	?>
			<tr>
				<td colspan="15" align="right"><b>&nbsp;Rata-Rata Unit <?php echo $query[$a]['nama_unit'] ?>&nbsp;</b></td>
				<td align="right"><b><?php 
					if ($jumgrupperunit != 0)
						$rataperunit = $totalperunit/$jumgrupperunit;
					else
						$rataperunit = 0;
					
					$jum1+= $rataperunit;
					$cunit1++;
					
					echo number_format($rataperunit, 2, '.',',') ?>&nbsp;</b></td>
			</tr>
	<?php			$totalperunit = 0; $jumgrupperunit = 0;
				}
			}
		} // end if query
		if (isset($queryutama[$aa+1]['kode']) && ($queryutama[$aa]['kode'] != $queryutama[$aa+1]['kode'])) {
			$rataperkel = $jum1/$cunit1;
	?>					
		
			<tr>
				<th colspan="15">TOTAL RATA-RATA</th>
				<th align="right"><b><?php echo number_format($rataperkel, 2, '.',',') ?> %&nbsp;</b></th>
			</tr>				
	<?php				
			$jum1=0; $cunit1=0;
			
			}
			else if ( !isset($queryutama[$aa+1]['kode'])) {
				$rataperkel = $jum1/$cunit1;
			?>
					<tr>
						<th colspan="15">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($rataperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
				$jum1=0; $cunit1=0;
			}
	}
	else {
		// ini tanpa header
		
		$query = $queryutama[$aa]['data_rekap'];
		
				if (is_array($query)) {
					for($a=0;$a<count($query);$a++){
		
		?>
				<tr>
		<?php
				if ($temp_unit != $query[$a]['kode_unit']) {
					$temp_unit = $query[$a]['kode_unit'];
					$nomor++;
					echo "<td align='center'>".$nomor."</td>";
					echo "<td>&nbsp;".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</td>";
				}
				else {
					echo "<td align='center'>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
				}
		?>
		
		<td>&nbsp;<?php echo $query[$a]['nama_grup_jahit'] ?></td>
		<?php
			$listratabulan = $query[$a]['listratabulan'];
			
			$bulan=1; $total = 0; $cbulan=0;
			if (is_array($listratabulan)) {
				for($j=0;$j<count($listratabulan);$j++){
					//echo $bulan." ";
					if ($listratabulan[$j][$bulan] != 0) {
						$total+=$listratabulan[$j][$bulan];
						$cbulan++;
					}
					
					if ($bulan == 1) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tjanuari+=$listratabulan[$j][$bulan];
							$cjanuari++;
						}
					}
					else if ($bulan == 2) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tfebruari+=$listratabulan[$j][$bulan];
							$cfebruari++;
						}
					}
					else if ($bulan == 3) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tmaret+=$listratabulan[$j][$bulan];
							$cmaret++;
						}
					}
					else if ($bulan == 4) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tapril+=$listratabulan[$j][$bulan];
							$capril++;
						}
					}
					else if ($bulan == 5) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tmei+=$listratabulan[$j][$bulan];
							$cmei++;
						}
					}
					else if ($bulan == 6) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tjuni+=$listratabulan[$j][$bulan];
							$cjuni++;
						}
					}
					else if ($bulan == 7) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tjuli+=$listratabulan[$j][$bulan];
							$cjuli++;
						}
					}
					else if ($bulan == 8) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tagustus+=$listratabulan[$j][$bulan];
							$cagustus++;
						}
					}
					else if ($bulan == 9) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tseptember+=$listratabulan[$j][$bulan];
							$cseptember++;
						}
					}
					else if ($bulan == 10) {
						if ($listratabulan[$j][$bulan] != 0) {
							$toktober+=$listratabulan[$j][$bulan];
							$coktober++;
						}
					}
					else if ($bulan == 11) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tnovember+=$listratabulan[$j][$bulan];
							$cnovember++;
						}
					}
					else if ($bulan == 12) {
						if ($listratabulan[$j][$bulan] != 0) {
							$tdesember+=$listratabulan[$j][$bulan];
							$cdesember++;
						}
					}
		?>
		<td align="right"><?php echo number_format($listratabulan[$j][$bulan], 2, '.',',') ?>&nbsp;</td>
		<?php		$bulan++;
				}
				//$totrata = $total/12;
				$totrata = $total/$cbulan;
				$totalperunit+=$totrata;
				$jumgrupperunit++;
			}
		?>
		<td align="right"><?php echo number_format($totrata, 2, '.',',') ?>&nbsp;</td>
	</tr>
			
	<?php 		//$nomor++;
				
				if ( isset($query[$a+1]['kode_unit']) && ($query[$a]['kode_unit'] != $query[$a+1]['kode_unit']) ) {
					
	?>
			<tr>
				<td colspan="15" align="right"><b>&nbsp;Rata-Rata Unit <?php echo $query[$a]['nama_unit'] ?>&nbsp;</b></td>
				<td align="right"><b><?php 
					if ($jumgrupperunit != 0)
						$rataperunit = $totalperunit/$jumgrupperunit;
					else
						$rataperunit = 0;
					
					$jum1+= $rataperunit;
					$cunit1++;
					
					echo number_format($rataperunit, 2, '.',',') ?>&nbsp;</b></td>
			</tr>
	<?php				
					$totalperunit = 0; $jumgrupperunit = 0;
				}
				else if ( !isset($query[$a+1]['kode_unit'])) {
	?>
			<tr>
				<td colspan="15" align="right"><b>&nbsp;Rata-Rata Unit <?php echo $query[$a]['nama_unit'] ?>&nbsp;</b></td>
				<td align="right"><b><?php 
					if ($jumgrupperunit != 0)
						$rataperunit = $totalperunit/$jumgrupperunit;
					else
						$rataperunit = 0;
					
					$jum1+= $rataperunit;
					$cunit1++;
					
					echo number_format($rataperunit, 2, '.',',') ?>&nbsp;</b></td>
			</tr>
	<?php			$totalperunit = 0; $jumgrupperunit = 0;
				}
			}
		} // end if query
		if (isset($queryutama[$aa+1]['kode']) && ($queryutama[$aa]['kode'] != $queryutama[$aa+1]['kode'])) {
			$rataperkel = $jum1/$cunit1;
	?>					
		
			<tr>
				<th colspan="15">TOTAL RATA-RATA</th>
				<th align="right"><b><?php echo number_format($rataperkel, 2, '.',',') ?> %&nbsp;</b></th>
			</tr>				
	<?php				
			$jum1=0; $cunit1=0;
			
			}
			else if ( !isset($queryutama[$aa+1]['kode'])) {
				$rataperkel = $jum1/$cunit1;
			?>
					<tr>
						<th colspan="15">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($rataperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
				$jum1=0; $cunit1=0;
			}
		
	} // end if
	
  } // end for utama
} // end if utama
	?>
		<tr>
			<td>&nbsp;</td>
			<td><b>Rata-Rata</b></td>
			<td>&nbsp;</td>
			<td align="right"><b><?php if($cjanuari!=0) echo number_format($tjanuari/$cjanuari, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cfebruari!=0) echo number_format($tfebruari/$cfebruari, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cmaret!=0) echo number_format($tmaret/$cmaret, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($capril!=0) echo number_format($tapril/$capril, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cmei!=0) echo number_format($tmei/$cmei, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cjuni!=0) echo number_format($tjuni/$cjuni, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cjuli!=0) echo number_format($tjuli/$cjuli, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cagustus!=0) echo number_format($tagustus/$cagustus, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cseptember!=0) echo number_format($tseptember/$cseptember, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($coktober!=0) echo number_format($toktober/$coktober, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cnovember!=0) echo number_format($tnovember/$cnovember, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td align="right"><b><?php if($cdesember!=0) echo number_format($tdesember/$cdesember, 2, '.',','); else echo number_format(0, 2, '.',','); ?>&nbsp;</b></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
	</table>
</div>
