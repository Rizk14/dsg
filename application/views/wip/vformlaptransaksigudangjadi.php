<h3>Laporan Transaksi Barang WIP  Di Gudang Jadi</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>

<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	//var nama_brg_wip = $('#nama_brg_wip').val();
	
	/*if (nama_brg_wip == '') {
		alert("Item barang harus dipilih. Ketik kode barang yang sesuai..!");
		return false;
	} */
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}
}

function addElement() {
  var ni = document.getElementById('myDiv');
  var newdiv = document.createElement('div');
  
  var jumbrg= $('#jumbrg').val();
  var jumbrgnew = parseInt(jumbrg)+1;
  $('#jumbrg').val(jumbrgnew);
    
  newdiv.innerHTML = "<input type='text' name='kode_brg_wip_"+jumbrgnew+"' id='kode_brg_wip_"+jumbrgnew+"' size='10' onkeyup='cari(this.value, "+jumbrgnew+");'> "+
					"<span id='infobrgwip_"+jumbrgnew+"'>"+
					"<input name='nama_brg_wip_"+jumbrgnew+"' type='text' id='nama_brg_wip_"+jumbrgnew+"' size='40' value='' readonly='true'/>"+
					"<input name='id_brg_wip_"+jumbrgnew+"' type='hidden' id='id_brg_wip_"+jumbrgnew+"' value=''/></span>";
					  
  ni.appendChild(newdiv);
}

function hapusElement() {
  var jumbrg= $('#jumbrg').val();
  $("#kode_brg_wip_"+jumbrg).remove(); 
  $("#infobrgwip_"+jumbrg).remove(); 
  var jumbrgnew = parseInt(jumbrg)-1;
  $('#jumbrg').val(jumbrgnew);
}

function cari(kodebrgwip, posisi) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/wip/creport/caribrgwip_laptransaksi', 
				data: 'kode_brg_wip='+kodebrgwip+'&posisi='+posisi, success: function(response) {
					$("#infobrgwip_"+posisi).html(response);
			}});
}
</script>

<?php 
$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
echo form_open('wip/creport/viewtransaksigudangjadi', $attributes); ?>
<table width="60%">
	<tr>
			  <td>Kode & Nama Barang WIP</td>
			  <td> <input type="hidden" name="jumbrg" id="jumbrg" value="1"> 
			  <div id="myDiv">
				<a href="javascript:addElement()">Tambah</a>&nbsp;<a href="javascript:hapusElement()">Hapus</a><br />
				<input type="text" name="kode_brg_wip_1" id="kode_brg_wip_1" size="10" onkeyup="cari(this.value, 1);">
				<span id="infobrgwip_1">
					<input name="nama_brg_wip_1" type="text" id="nama_brg_wip_1" size="40" value="" readonly="true"/>
					<input name="id_brg_wip_1" type="hidden" id="id_brg_wip_1" value=""/>
				</span>				
				</div>
			  </td>
			</tr>
			
	<!--<tr>
		<td width="20%">Kode & Barang WIP</td>
		<td style="white-space:nowrap;"> <input type="text" name="kode_brg_wip" id="kode_brg_wip" size="10" onkeyup="cari(this.value);">
		<br><span id="infobrgwip">
			<input name="nama_brg_wip" type="text" id="nama_brg_wip" size="40" value="" readonly="true"/>
			<input name="id_brg_wip" type="hidden" id="id_brg_wip" value=""/>
		</span>
			
		</td>
	</tr>-->
	
	
	<!--<tr>
		<td>Periode (bulan-tahun)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>	-->
  <tr>
		<td>Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
  </tr>

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/transaksigudangjadi'">
<?php echo form_close();  ?>
