<h3>Update Saldo Awal Unit Jahit</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>

<script type="text/javascript">

function cek_input() {
	var kon=window.confirm("Yakin akan mengupdate data saldo awal?");
	if (kon){
		/*var date_from= $('#date_from').val();
		var date_to= $('#date_to').val();

		if (date_from == '') {
			alert("Tanggal Awal harus dipilih..!");
			return false;
		}
		if (date_to == '') {
			alert("Tanggal Akhir harus dipilih..!");
			return false;
		}
		
		var tgl_dari = date_from.substr(0,2);
		var bln_dari = date_from.substr(3,2);
		var thn_dari = date_from.substr(6,4);
		var dari = new Date(thn_dari, bln_dari, tgl_dari);
		
		var tgl_ke = date_to.substr(0,2);
		var bln_ke = date_to.substr(3,2);
		var thn_ke = date_to.substr(6,4);
		var ke = new Date(thn_ke, bln_ke, tgl_ke);
		
		if (dari > ke) {
			alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");*/
		var tahun = $('#tahun').val();
		if (tahun == '') {
			alert("Tahun harus diisi..!");
			return false;
		}
		if (tahun.length < 4) {
			alert("Tahun harus 4 digit ..!");
			return false;
		}
		if (isNaN(tahun)) {
			alert("Tahun harus berupa angka ..!");
			return false;
		}else
				return true;
	}else{
		return false;
	}	
}
</script>

<?php 
if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<?php 
$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
echo form_open('wip/creport/updatesaldoawalunitjahit', $attributes); ?>
&nbsp;
<table width="60%">
	<tr>
		<td width="20%">Unit Jahit</td>
		<td> <select name="unit_jahit" id="unit_jahit">
				<option value="00"> Semua Unit Jahit</option>
				<?php foreach ($list_unit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>	
	<tr>
		<td width="20%">Periode (sebelumnya)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>	
	<!-- <tr>
		<td>Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
	</tr> -->
</table><br>
<input type="submit" name="submit" value="Update" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/updatesaldouj'">
<?php echo form_close();  ?>