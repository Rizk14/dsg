<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Retur QC</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var jum_total= $('#jum_total').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data retur QC ??");
		
	if (kon) {
		// new 17-04-2014 =====================
		if (jum_total > 0) {
			for (var i=1; i <= jum_total; i++) {
				var qty= $('#qty_'+i).val();
				
				if (isNaN(qty) || qty == '' ) {
					alert("Data retur harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}
</script>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br>
Jenis: <?php echo $jenis ?><br><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<i>Keterangan: data-data barangnya diambil dari transaksi barang masuk di bulan dan tahun yg dipilih (sama dgn laporan QC unit jahit)</i><br>
<?php 
$attributes = array('name' => 'f_returqc', 'id' => 'f_returqc');
echo form_open('wip/creport/submitreturqc', $attributes);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="jenis" id="jenis" value="<?php echo $jenis ?>">
<input type="hidden" name="jum_total" id="jum_total" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		<th width="20%">Unit Jahit</th>
		 <th>Kode</th>
		 <th>Nama Barang Jadi</th>
		 <th width="10%">Qty Retur</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){					
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td >&nbsp;".$query[$j]['kode_unit_jahit']." - ".$query[$j]['nama_unit_jahit']."
					 <td>&nbsp;".$query[$j]['kode_brg_jadi']."</td>
					 <input type='hidden' name='kode_unit_$i' id='kode_unit_$i' value='".$query[$j]['kode_unit_jahit']."'>
					 <input type='hidden' name='kode_brg_jadi_$i' id='kode_brg_jadi_$i' value='".$query[$j]['kode_brg_jadi']."'>
					 <input type='hidden' name='iddetail_$i' id='iddetail_$i' value='".$query[$j]['id']."'>
					 </td>
					 ";
		?>
			<td>&nbsp;<?php echo $query[$j]['nama_brg_jadi'] ?></td>
			<td>
				<input type="text" style="text-align: right;" size="5" name="qty_<?php echo $i ?>" id="qty_<?php echo $i ?>" value="<?php echo $query[$j]['retur'] ?>">
			</td>
				</tr>
		<?php	$i++; 
			}
		}
		 ?>
 	</tbody>
</table><br>
<input type="submit" name="submit" value="Simpan" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Data Periode Ini" onclick="return confirm('Yakin akan hapus data periode ini ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/addreturqc'">
<?php echo form_close();  ?>
</div>
