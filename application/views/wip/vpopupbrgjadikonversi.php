<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>

<script>
$(function()
{
	$(".pilih").click(function()
	{
		var kodebrgjadi	= $("#kodebrgjadi").val();
		var namabrgjadi	= $("#namabrgjadi").val();
		var adawarna	= $("#adawarna").val();
		var idwarna	= $("#idwarna").val();
		
		opener.document.forms["f_konversi"].kode_brg_jadi<?php echo $posisi ?>.value	= kodebrgjadi;
		opener.document.forms["f_konversi"].brg_jadi<?php echo $posisi ?>.value	= kodebrgjadi+" - "+namabrgjadi;
		opener.document.forms["f_konversi"].adawarna<?php echo $posisi ?>.value	= adawarna;
		opener.document.forms["f_konversi"].idwarna<?php echo $posisi ?>.value	= idwarna;
		
		self.close();
	});
});
</script>

<center><h3>Daftar Stok Barang WIP di Gudang <br><?php echo "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang ?></h3></center>
	
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="kodebrgjadi" id="kodebrgjadi">
<input type="hidden" name="namabrgjadi" id="namabrgjadi">
<input type="hidden" name="adawarna" id="adawarna">
<input type="hidden" name="idwarna" id="idwarna">
</form>

<div align="center">
<?php echo form_open('wip/cgudang/show_popup_brgjadi/'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">
<input type="hidden" name="gudang" value="<?php echo $gudang ?>">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
  	<table border="1" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" width="100%" >
		<tr>
		  <th bgcolor="#999999">No</th>
		  <th bgcolor="#999999">Kode</th>
		  <th bgcolor="#999999">Nama Barang</th>
		  <th bgcolor="#999999">Stok Total</th>
		  <th bgcolor="#999999">Stok Per Warna</th>
		  <th bgcolor="#999999">Action</th>
		</tr>
		
			<?php 
			if($startnya=='')
				$i	= 1;
			else
				$i	= $startnya+1; 
		
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++) {
			?>
		
		<tr>
		  <td align="center"><?php echo $i; ?></td>
		  <td><?php echo $query[$j]['kode_brg'] ?></td>
		  <td style="white-space:nowrap;"><?php echo $query[$j]['nama_brg']; ?></td>
		  <td align="right"><?php if ($query[$j]['stok']!= 0) echo $query[$j]['stok']; else echo "0"; ?></td>  
		  <td style="white-space:nowrap;" align="right">
      <?php
		$idwarna= '';
		// ambil data warna dan stoknya dari tm_warna_brg_jadi dan tm_stok_hasil_jahit_warna
				$sqlxx	= $this->db->query(" SELECT c.kode, c.nama, a.id_warna_brg_jadi, a.stok FROM tm_stok_hasil_jahit_warna a, 
									tm_warna c
									WHERE a.kode_warna = c.kode 
									AND a.id_stok_hasil_jahit = '".$query[$j]['id']."' 
									order by c.kode ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
					
					foreach ($hasilxx as $rownya) {
						$idwarna.= $rownya->kode.";";
						echo $rownya->nama." : ".$rownya->stok."<br>";
					}
					$adawarna=1;
				}
				else {
					$adawarna=0;
					$idwarna= '';
					echo "&nbsp;";
				}
      ?>
      </td>
	  
		  <td align='center'>
			  <a class="pilih" style="cursor:pointer" id="pilih" 
			  onMouseOver="window.document.f_master_brg.kodebrgjadi.value='<?php echo $query[$j]['kode_brg'] ?>';
			  window.document.f_master_brg.namabrgjadi.value='<?php echo $query[$j]['nama_brg'] ?>';
			  window.document.f_master_brg.adawarna.value='<?php echo $adawarna ?>';
			  window.document.f_master_brg.idwarna.value='<?php echo $idwarna ?>'; ">Pilih</a>
		  </td>
		</tr>
			<?php $i++; }
				}
			 ?>
  	</table>
	
  	<?php echo $this->pagination->create_links(); ?>
</div>
