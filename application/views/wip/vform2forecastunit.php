<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Forecast Berdasarkan Kelompok Unit Jahit</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

// --------- 02-04-2014 ------------------
$(function()
{
	//$("#no").val('2');
	
	var is_new = $('#is_new').val(); 
	if(is_new == 1)
		$("#jum_data").val('2');
	
	//generate_nomor();		
	$("#additem").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#jum_data").val();		
		var n=parseInt(no)-1;
		//var n=parseInt(no);
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode_brg_jadi*************************************
		var kode_brg_jadi="#kode_brg_jadi_"+n;
		var new_kode_brg_jadi="#kode_brg_jadi_"+no;
		$(kode_brg_jadi, lastRow).attr("id", "kode_brg_jadi_"+no);
		$(new_kode_brg_jadi, lastRow).attr("name", "kode_brg_jadi_"+no);		
		$(new_kode_brg_jadi, lastRow).attr("onkeyup", "cari('"+no+"', this.value);");		
		$(new_kode_brg_jadi, lastRow).val('');		
		//*****end kode_brg_jadi*********************************
												
		//*****nama_brg_jadi*************************************
		var nama_brg_jadi="#nama_brg_jadi_"+n;
		var new_nama_brg_jadi="#nama_brg_jadi_"+no;
		$(nama_brg_jadi, lastRow).attr("id", "nama_brg_jadi_"+no);
		$(new_nama_brg_jadi, lastRow).attr("name", "nama_brg_jadi_"+no);		
		$(new_nama_brg_jadi, lastRow).val('');				
		//*****end nama_brg_jadi*************************************
								
		//*****iddetail*************************************
		var iddetail="#iddetail_"+n;
		var new_iddetail="#iddetail_"+no;
		$(iddetail, lastRow).attr("id", "iddetail_"+no);
		$(new_iddetail, lastRow).attr("name", "iddetail_"+no);		
		$(new_iddetail, lastRow).val('0');				
		//*****end iddetail*************************************	
		
		//******div infobrgjadi*************************************
		var infobrgjadi="#infobrgjadi_"+n;
		var new_infobrgjadi="#infobrgjadi_"+no;
		$(infobrgjadi, lastRow).attr("id", "infobrgjadi_"+no);

		$(new_infobrgjadi, lastRow).html("<input type='text' id='nama_brg_jadi_"+no+"' name='nama_brg_jadi_"+no+"' value='' readonly='true' size='40'>");
		
		//*****fc*************************************
		var fc="#fc_"+n;
		var new_fc="#fc_"+no;
		$(fc, lastRow).attr("id", "fc_"+no);
		$(new_fc, lastRow).attr("name", "fc_"+no);		
		$(new_fc, lastRow).val('0');				
		//*****end fc*************************************	
		
		//*****ket*************************************
		var ket="#ket_"+n;
		var new_ket="#ket_"+no;
		$(ket, lastRow).attr("id", "ket_"+no);
		$(new_ket, lastRow).attr("name", "ket_"+no);		
		$(new_ket, lastRow).val('');				
		//*****end ket*************************************	
												
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//jum_data++
		var x=parseInt(no);
		$("#jum_data").val(x+1);	
		
	});	
	
	$("#deleteitem").click(function()
	{
		var x= $("#jum_data").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#jum_data").val();	
			var y= x-1;
			$("#jum_data").val(y);	
		}
	});	
	
});
// ---------------------------------------

function cek_input() {
	var jum_data = $('#jum_data').val()-1;
	var s = 0;
	kon = window.confirm("Yakin akan simpan data forecast ??");
		
	if (kon) {
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
				
				if ($('#kode_brg_jadi_'+k).val() == '') {
					alert("Kode barang harus diinput..!");
					s=1;
					return false;
				}
				if ($('#nama_brg_jadi_'+k).val() == '' ) {
					alert("Barang Jadi tidak valid..!");
					s=1;
					return false;
				}
				if (isNaN($('#fc_'+k).val()) || $('#fc_'+k).val() == '') {
					alert("Data FC Produksi harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}

function cari(posisi, kodebrgjadi) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/wip/cform/caribrgjadi', 
				data: 'kode_brg_jadi='+kodebrgjadi+'&posisi='+posisi, success: function(response) {
					$("#infobrgjadi_"+posisi).html(response);
					window.close();
			}});

}
</script>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br>
Kelompok Unit Jahit: <?php echo $namaunit ?> <br><br>
<!-- Total Data = <?php //echo $jum_total ?><br><br> -->
<?php 
$attributes = array('name' => 'f_unit', 'id' => 'f_unit');
echo form_open('wip/creport/submitforecastunit', $attributes);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="id_kel_unit" id="id_kel_unit" value="<?php echo $id_kel_unit ?>">
<input type="hidden" name="namaunit" id="namaunit" value="<?php echo $namaunit ?>">
<input type="hidden" name="jum_data" id="jum_data" value="<?php if(is_array($query)) echo count($query)+1; else echo '2'; ?>">

<input type="submit" name="submit" value="Simpan" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit3" value="Hapus Data Periode Ini" onclick="return confirm('Yakin akan hapus data periode ini ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/addforecastunit'">

<br><br>
<input id="additem" type="button" name="additem" value=" + " title="Tambah Item Barang">&nbsp;
<input id="deleteitem" type="button" name="deleteitem" value=" - " title="Hapus Item Barang">

<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%" id="tabelku">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode</th>
		 <th>Nama Barang Jadi</th>
		 <th>FC Produksi</th>
		 <th>Keterangan</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){					
					 echo "<tr class=\"record\">";
					 echo "<td align='center' id='num_$i'>$i</td>";
					 
					 echo    "<td>";
		?>
			<input type="text" name="kode_brg_jadi_<?php echo $i ?>" id="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $query[$j]['kode_brg_jadi'] ?>" onkeyup="cari('<?php echo $i ?>',this.value);">
		<?php
					echo " 
					 <input type='hidden' name='iddetail_$i' id='iddetail_$i' value='".$query[$j]['id']."'>
					 </td>";
					 
					 echo    "<td><div id='infobrgjadi_$i'><input type='text' name='nama_brg_jadi_$i' id='nama_brg_jadi_$i' size='40' readonly='true' value='".$query[$j]['nama_brg_jadi']."'></div> </td>";
					 ?>
					 <td><input type="text" name="fc_<?php echo $i ?>" id="fc_<?php echo $i ?>" value="<?php echo $query[$j]['fc'] ?>" size="5" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'"></td>
					<?php
					echo    "<td><input type='text' name='ket_$i' id='ket_$i' value='".$query[$j]['keterangan']."' size='10' ></td>";				
					 echo  "</tr>";
					 $i++;
				}
			}
			else {
	?>
	<tr>
		<td align="center" id="num_1">1</td>
		<td><input type="text" name="kode_brg_jadi_1" id="kode_brg_jadi_1" size="10" value="" onkeyup="cari('1',this.value);">
			<input type="hidden" name="iddetail_1" id="iddetail_1" value="0">
		</td>
		<td><div id="infobrgjadi_1">
			<input type="text" name="nama_brg_jadi_1" id="nama_brg_jadi_1" size="40" value="" readonly="true"></div></td>
		<td><input type="text" name="fc_1" id="fc_1" size="5" value="0" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'"></td>
		<td><input type="text" name="ket_1" id="ket_1" size="10" value=""></td>
	</tr>
	<?php			
			}
		 ?>
 	</tbody>
</table><br>

<?php echo form_close();  ?>
</div>
