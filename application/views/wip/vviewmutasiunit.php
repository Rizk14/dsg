<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>


<script language="javascript" type="text/javascript">

function konfirm(){



	var kon=window.confirm("YAKIN AKAN CLOSING DATA HARUS SEPENUHNYA BENAR??");


	if (kon){
		return true;
	}else{
		return false;
	}
}
</script>



<h3>Laporan Mutasi Stok Barang WIPPPP (Hasil Jahit) Unit Jahit</h3><br><br>
<div>

Unit Jahit: <?php if ($unit_jahit!= 0) { echo $kode_unit."-".$nama_unit; } else echo "Semua"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('wip/creport/export_excel_mutasiunitwarna', $attributes); ?>

<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="kode_unit" value="<?php echo $kode_unit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >

<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<!-- <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button"> -->
<?php echo form_close();  ?>
<?php 
$attributes = array('name' => 'f_stok2', 'id' => 'f_stok2');
echo form_open('wip/creport/closingperiode', $attributes); ?>

<br><br>
<?php
$t=0;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			$t++;
		}
	}
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			echo "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b>"."<br>";
			/*$id_unit7=$query[$a]['id_unit7'];*/			
?>


<?php
if ($this->session->userdata('gid') != 5 ) {
if ($this->session->userdata('gid') != 15 ) {
?>
<?php
if(($query[$a]['statusclosing2'])==null){
?>	
<td><i> <font color="red"> ** Bulan sebelumnya belum di closing </font></i></td>

<?php }else{ ?>


<?php
if(($query[$a]['statusclosing'])==null && $t<=1){
?>	
<input type="submit" name="closeperiode" id="closeperiode" value="Close Periode" onclick="return konfirm();">
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >

<?php }?>

<?php
if(($query[$a]['statusclosing'])==null ){
?>	
<td><i>* Bulan Ini Belum di closing </i></td>
<?php }else
{?>
	<td><i>* Data sudah di closing </i></td>	 
	 <?php } ?>
	 <?php } ?>
	 <?php } ?>
<?php }?>
	<input type="hidden" name="jml" id="jml" value="<?php echo $a; ?>">
	<table border="1" cellpadding= "1" cellspacing = "1" width="100%"  id="sitabel">
	<thead>
	 <tr class="judulnya">
		 <th width='3%' rowspan='2'>No</th>
		 <th width='15%' rowspan='2'>Kode</th>
		 <th width='25%' rowspan='2'>Nama Brg WIP</th>
		 <th rowspan="2">Saldo<br>Awal</th>
		<th colspan='4'>Masuk</th>
		<th colspan='7'>Keluar</th>
		<th rowspan="2">Saldo Akhir</th>
<?php
				if ($this->session->userdata('gid') != 5) { ?>
		<th rowspan="2">Stok Opname</th>
		<th width='3%' rowspan='2'>Selisih</th>
		<?php } ?>
	 </tr>
	 <tr class="judulnya">
		 <th width='8%'>Bgs</th>
		 <th width='8%'>Retur Gdg QC</th>
		 <th width='8%'>Lain2</th>
		 <!--<th width='8%'>Pengembalian Gdg Pengadaan</th>-->
		 <th width='8%'>Total</th>
		 <th width='8%'>Bgs<br>Gdg QC</th>
		 <th width='8%'>Bgs<br>Gdg Jadi</th>
		 <th width='8%'>Hsl Perbaikan</th>
		 <th width='8%'>Retur Bhn Baku</th>
		 <th width='8%'>Ke Unit Packing</th>
		 <th width='8%'>Lain2</th>
		 <th width='8%'>Total</th>
	 </tr>
	</thead>
	<tbody>
			
<?php
$totsaldoawal=0;
$totbgs=0;
$totmasuk_returbrgjadi=0;
$totmasuk_lain=0;
$totjum_masuk=0;
$totklr=0;
$totkeluar_gudangjadi=0;
$totkeluar_perbaikan=0;
$totkeluar_retur_bhnbaku=0;
$totkeluar_unit_packing=0;
$totkeluar_lain=0;
$totjum_keluar=0;
$totsaldoakhir=0;
$totstokopname=0;
$totadjustment=0;
$totselisih=0;

			$detail_stok = $query[$a]['data_stok'];
			if (is_array($detail_stok)) {
				for($j=0;$j<count($detail_stok);$j++){
					$totsaldoawal=$totsaldoawal+$detail_stok[$j]['jum_saldo_awal'];
					$totsaldoakhir=$totsaldoakhir+$detail_stok[$j]['jum_saldo_akhir'];
					$totstokopname=$totstokopname+$detail_stok[$j]['jum_so'];
					$totselisih=$totselisih+$detail_stok[$j]['selisih'];

					if ($detail_stok[$j]['masuk_bgsback'] == 0 && $detail_stok[$j]['masuk_returbrgjadiback'] == 0 && $detail_stok[$j]['masuk_pengembalianback'] == 0 && $detail_stok[$j]['masuk_lainback'] == 0 
					&& $detail_stok[$j]['keluar_bgsback'] == 0 && $detail_stok[$j]['keluar_perbaikanback'] == 0 && $detail_stok[$j]['keluar_retur_bhnbakuback'] == 0 && $detail_stok[$j]['keluar_gudangjadiback'] == 0  && $detail_stok[$j]['keluar_lainback'] == 0){
						//print_r($detail_stok);
						echo "<tr class=\"record\" bgcolor='#ff3333'>";	
					}
					else
?>

				<td align="center"><?php echo ($j+1) ?></td>
				<td>&nbsp;<?php echo $detail_stok[$j]['kode_brg_wip'] ?></td>
				<td>&nbsp;<?php echo $detail_stok[$j]['nama_brg_wip'] ?></td>

				<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['saldo_awal'],0,',','.')."<br>";
					}
				}
					
				//echo "Total: ".number_format($detail_stok[$j]['jum_saldo_awal'],0,',','.') 
				?>&nbsp;
				</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['masukbgs'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['masuk_bgs'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['masukretur'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['masuk_returbrgjadi'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['masuklain'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['masuklainall'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['jummasuk'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['jum_masuk'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['keluarbgs'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['keluar_bgs'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['keluargdjd'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['keluar_gudangjadi'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['keluarperb'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['keluar_perbaikan'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['keluarreturbb'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['keluar_retur_bhnbaku'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['keluarpacking'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['keluar_unit_packing'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['keluarlain'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['keluarlainall'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['jumkeluar'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['jum_keluar'],0,',','.') 
		?>&nbsp;</td>

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['saldoakhir'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['jum_saldo_akhir'],0,',','.') 
		?>&nbsp;</td>
				<!--	if (is_array($detail_stok[$j]['saldo_so_warna'])) {
					//$detailwarna = $detail_stok[$j]['saldo_so_warna'];-->
		<?php
				if ($this->session->userdata('gid') != 5) { ?>
				<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna_a[$z]['currentso']."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['jum_so'],0,',','.') 
		?>&nbsp;</td>
				

		<td style='white-space:nowrap;'><?php 
				if (is_array($detail_stok[$j]['saldo_awal_warna'])) {
					$detailwarna_a = $detail_stok[$j]['saldo_awal_warna'];
					for($z=0;$z<count($detailwarna_a);$z++){
						echo $detailwarna_a[$z]['nama_warna'].": ".number_format($detailwarna_a[$z]['selisih'],0,',','.')."<br>";
					}
				}
					
		//echo "Total: ".number_format($detail_stok[$j]['selisih'],0,',','.') ?>&nbsp;</td>				
					
				<?php } ?>
			</tr>
<?php
				} // end for2


			echo  "<tr>";
		 	echo "<td align='center' colspan='3'>TOTAL</td>";
		 	echo    "<td align='right'>".number_format($totsaldoawal,2,',','.')."</td>";
		 	echo "<td align='center' colspan='11'></td>";
		 	echo    "<td align='right'>".number_format($totsaldoakhir,2,',','.')."</td>";

		 	if ($this->session->userdata('gid') != 5) {
		 	echo    "<td align='right'>".number_format($totstokopname,2,',','.')."</td>";
		 	echo    "<td align='right'>".number_format($totselisih,2,',','.')."</td>";
		 }
		 	echo  "</tr>";
/*===================================================================================================================================================*/
			} // end if2
?>			
<input type="hidden" name="totsaldoakhir<?php echo $a ?>" value="<?php echo $totsaldoakhir ?>" >
<!-- <input type="hidden" name="id_unit7<?php echo $a ?>" value="<?php echo $id_unit7 ?>" > -->
<?php echo form_close();  ?>
			</tbody>
			</table><br><br>
<?php
		} //end for1
	} // end if1
?>
</div>
	<font color="red">* <strong ><font color="black">jika dibulan sebelumnya tidak ada transaksi.</font></strong></font><br>



	<script>
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>