<h3>Laporan Transaksi Barang WIP (Hasil Jahit) Di Gudang QC</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	var nama_brg_wip = $('#nama_brg_wip').val();
	
	if (nama_brg_wip == '') {
		alert("Item barang harus dipilih. Ketik kode barang yang sesuai..!");
		return false;
	}
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}
}

function cari(kodebrgwip) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/wip/creport/caribrgwip', 
				data: 'kode_brg_wip='+kodebrgwip, success: function(response) {
					$("#infobrgwip").html(response);
			}});
}

</script>

<?php 
$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
echo form_open('wip/creport/viewtransaksihasiljahit', $attributes); ?>
<table width="60%">
	<tr>
			<td width="20%">Lokasi Gudang</td>
			<td> <select name="id_gudang" id="id_gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>"><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
	<tr>
	<td>Kode & Barang WIP</td>
	<td><input type="text" name="kode_brg_wip" id="kode_brg_wip" value="" onkeyup="cari(this.value);" size="10">
	<div id="infobrgwip"><input type="text" readonly="true" name="nama_brg_wip" id="nama_brg_wip" value="" size="40">
	<input type="hidden" name="id_brg_wip" value="">
	</div></td>
  </tr>
	<tr>
		<td>Periode (bulan-tahun)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>	

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/transaksihasiljahit'">
<?php echo form_close();  ?>
