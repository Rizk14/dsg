<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>


<script language="javascript" type="text/javascript">
function fr(){
	if(document.getElementById('ftransfer').checked==true){
		document.getElementById('ftrans').value	= 1;
	}else{
		document.getElementById('ftrans').value	= 0;
	}
}

function faktur(){
	if(document.getElementById('ffaktur').checked==true){
		document.getElementById('ff').value	= 1;
	}else{
		document.getElementById('ff').value	= 0;
	}
}

function cetak(){
	if(document.getElementById('fcetak').checked==true){
		document.getElementById('fc').value	= 1;
	}else{
		document.getElementById('fc').value	= 0;
	}
}
</script>

<script language="javascript" type="text/javascript">
<!--
$(document).ready(function(){
	$("#d_do_first").datepicker();
	$("#d_do_last").datepicker();
});
-->
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_do; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_do; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
			<?php 
				$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listdo/cform/carilistdo', $attributes);?>
		
		<div id="masterldobrgform">
		  <table width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_tgl_mulai_do; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					<input name="d_do_first" type="text" id="d_do_first" maxlength="10" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)"/>
					<!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)"> -->
					<?php echo $list_tgl_akhir_do; ?>
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)"/>
					<span style="color:#FF0000">*</span>
					<!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)"> -->            </td>
				  </tr>
				  <tr>
					<td width="33%"><?php echo $list_no_do; ?></td>
					<td width="0%">:</td>
					<td>
					<?php
					$nomor_do	= array(
					'name'=>'nomor_do',
					'id'=>'nomor_do',
					'type'=>'text',
					'maxlength'=>'14'
					);
					echo form_input($nomor_do);
					?>
					<img name="img_i_do_" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Tampilkan Nomor DO" class="imgLink" align="absmiddle" onclick="infodo();">
					</td>
				  </tr>				  
				  <tr>
					<td colspan="2"></td>
				  	<td height="20px;"><input type="checkbox" name="ftransfer" id="ftransfer" value="1" onchange="fr();"/>&nbsp;telah ditransfer</td>
				  </tr>		
				  <tr>
					<td colspan="2"></td>
				  	<td height="20px;"><input type="checkbox" name="ffaktur" id="ffaktur" value="1" onchange="faktur();"/>&nbsp;telah difakturkan</td>
				  </tr>					  		  
				  <tr>
					<td colspan="2"></td>
				  	<td height="20px;"><input type="checkbox" name="fcetak" id="fcetak" value="1" onchange="cetak();"/>&nbsp;telah dicetak</td>
				  </tr>				  
				  <tr>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td align="right">
					<!-- <input name="btnrefresh" type="button" id="btnrefresh" value="Refresh Harga" onclick="show('listdo/cform/refresh/','#content');" <?php if($disabled=='t'){?> disabled <? } ?> /> -->
					<input type="hidden" name="ftrans" id="ftrans" value="0" />
					<input type="hidden" name="ff" id="ff" value="0" />
					<input type="hidden" name="fc" id="fc" value="0" />
					<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" />
					<input name="btnbatal" id="btnbatal" value="Batal" type="reset" onclick="window.location='<?php echo base_url(); ?>index.php/listdo/cform/'">

					</td>
				  </tr>
				</table>
		</div>
		
		<?php echo form_close(); ?>
		
	    </td>
	  </tr>
	</table>
   </td>
  </tr>
</table>
