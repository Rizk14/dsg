<div class="container" id="daftar-sukses">
    <p class="h2">Informasi Untuk Penginputan Master Unit Jahit</p>
    <hr>

    <div class="alert alert-success alert-dismissible" role="alert">
        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Penginputan Berhasil.
    </div>
    
    <p>Anda dapat kembali ke halaman Master Unit Jahit di <?php echo anchor('/dashboard/master_unit_jahit/view', 'Master Unit Jahit'); ?>.</p>

</div> <!-- container -->
