<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Rekap Budgeting</h3><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Supplier: <?php if ($supplier != '0') { echo $kode_supplier."-".$nama_supplier; } else { echo "Semua"; } ?><br>
Jenis pembelian : <?php echo $nama_jns_data ?> <br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-rekap-budgeting/cform/exportexcel', $attributes); ?>

<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_supplier" value="<?php echo $supplier ?>" >
<input type="hidden" name="kode_supplier" value="<?php echo $kode_supplier ?>" >
<input type="hidden" name="nama_supplier" value="<?php echo $nama_supplier ?>" >
<input type="hidden" name="jns_data" value="<?php echo $jns_data ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>

<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Kode Supplier</th>
		  <th>Nama Supplier</th>
		 <th>Harga Permintaan</th>
		 <th>Harga Realisasi</th>
	 </tr>
	
	</thead>
	<tbody>
		 <?php
			 $no=1;$tottotal_permintaan=0;$tottotal_pemenuhan=0;
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {
				echo "<tr>
					<td>".$no."</td>
					<td>".$query[$j]['kode_supplier']."</td>
					<td>".$query[$j]['nama_supplier']."</td>
					<td>".number_format($query[$j]['total_permintaan'], 2, ',','.')."</td>
					<td>".number_format($query[$j]['total_pemenuhan'],0,',','.')."</td>
					</tr>";
				$tottotal_permintaan+=$query[$j]['total_permintaan'];
				$tottotal_pemenuhan+=$query[$j]['total_pemenuhan'];
				$no++;		 
			 	 }
			 	 echo "<tr>
					<td colspan='3' align='center'>Total</td>
					
					<td>".number_format($tottotal_permintaan,2,',','.')."</td>
					<td>".number_format($tottotal_pemenuhan,0,',','.')."</td>
					</tr>";
			 	}
			 ?>	
 	</tbody>
</table><br>
</div>
