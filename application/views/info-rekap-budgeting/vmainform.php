<h3>Rekap Budgeting</h3>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$('#jenis').change(function(){
		$('input[name=kalkulasi_ulang_so]').attr('checked', false);
	  	if ($("#jenis").val() == '2') {
			$('#kalkulasi').show();
		}
		else {
			$('#kalkulasi').hide();
			$('input[name=kalkulasi_ulang_so]').attr('checked', false);
		}
	  });
});
</script>
<script type="text/javascript">

function cek_input() {
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();
	var format_harga= $('#format_harga').val();

	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	var tgl_dari = date_from.substr(0,2);
	var bln_dari = date_from.substr(3,2);
	var thn_dari = date_from.substr(6,4);
	var dari = new Date(thn_dari, bln_dari, tgl_dari);
	
	var tgl_ke = date_to.substr(0,2);
	var bln_ke = date_to.substr(3,2);
	var thn_ke = date_to.substr(6,4);
	var ke = new Date(thn_ke, bln_ke, tgl_ke);
	
	if (dari > ke) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}
	
	if (format_harga == '2') {
		alert("Format harga average belum tersedia, silahkan pilih format harga FIFO");
		return false;
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-rekap-budgeting/cform/view', $attributes); ?>
<table width="60%">
	<tr>
			<td width="20%">Supplier</td>
			<td> <select name="supplier" id="supplier">
				<option value="0">- Semua -</option>
				<?php foreach ($list_supplier as $gud) { ?>
					<option value="<?php echo $gud->id ?>" ><?php echo $gud->kode_supplier."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		
<!--
		<tr>
		<td>Kelompok Barang</td></td>
		<td><select name="kel_brg" id="kel_brg" >
					<option value="0" >- All -</option>
				
				<?php foreach ($list_kelompok_barang as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode_perkiraan." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>
	<tr>
		<td>Jenis Barang</td></td>
		<td><select name="jns_brg" id="jns_brg" >
					<option value="0" >- All -</option>
				
				<?php foreach ($list_jenis_barang as $jns) { ?>
					<option value="<?php echo $jns->id ?>" ><?php echo $jns->kode." - ". $jns->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>
-->
		
	<tr>
		<td>Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <!--
  <tr>
			<td>Jenis Laporan</td>
			<td> <select name="jenis" id="jenis">
				<option value="1">Qty Saja (tanpa harga)</option>
				<option value="2">Lengkap (dengan harga(Berdasarkan harga OP))</option>
				</select>
			</td>
			
		</tr>

	
	<tr style="display:none;" id="kalkulasi">
			<td>Kalkulasi Ulang Stok Opname Berdasarkan Harga</td>
			<td> <input type="checkbox" name="kalkulasi_ulang_so" id="kalkulasi_ulang_so" value="t">
			<br>
			<br>
			<font color="red">*Kalkulasi ulang apabila Saldo awal di setiap Gudang adalah 0, Pilih kalkulasi di bulan sebelumnya dan bulan tersebut.</font><br>
			</td>
		</tr-->
			<tr>
			<td>Jenis Pembelian </td>
			<td> <select name="jns_data" id="jns_data">
				<option value="0">ALL</option>
				<option value="1">Cash</option>
				<option value="2">Credit</option>
				</select>
			</td>
		</tr>
	<input type="hidden" name="format_harga" value="1">

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/info-rekap-budgeting/cform/'">
<?php echo form_close();  ?>
