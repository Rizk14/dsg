<h3>Rekap Pembayaran Pembelian Per Supplier</h3><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();

	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	var tgl_dari = date_from.substr(0,2);
	var bln_dari = date_from.substr(3,2);
	var thn_dari = date_from.substr(6,4);
	var dari = new Date(thn_dari, bln_dari, tgl_dari);
	
	var tgl_ke = date_to.substr(0,2);
	var bln_ke = date_to.substr(3,2);
	var thn_ke = date_to.substr(6,4);
	var ke = new Date(thn_ke, bln_ke, tgl_ke);
	
	if (dari > ke) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
$attributes = array('name' => 'f_pelunasan', 'id' => 'f_pelunasan');
echo form_open('info-rekap-pelunasan-persup/cform/view', $attributes); ?>
<table width="60%">
	<tr>
			<td>Jenis Pembelian</td>
			<td> <select name="jenis_beli" id="jenis_beli" >
					<option value="0" >- All -</option>
					<option value="1" >Cash</option>
					<option value="2" >Kredit</option>
				</select>
			</td>
	</tr>
	<tr>
		<td width="20%">Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
  </tr>

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/info-rekap-pelunasan-persup/cform/'">
<?php echo form_close();  ?>
