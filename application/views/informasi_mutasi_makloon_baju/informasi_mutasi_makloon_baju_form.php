<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<div class="container">
<h2>Informasi Mutasi Makloon Baju di Unit Jahit</h2>
<hr>



    <form action ="view" name="myform" id='myform' class="form-inline" method="post">
        
       
    <div class="row">
        <?php echo form_label('Tanggal SJ dari &nbsp;&nbsp;&nbsp;:&nbsp;', 'tanggal_sj_dari', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback ">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
				<input id='tanggal_sj_dari' name='tanggal_sj_dari' value='<?php echo date_to_id($values->tanggal_sj_dari) ?>' class='form-control'
				placeholder="Tanggal SJ Dari" maxlength="10" onclick="displayCalendar(document.forms[0].tanggal_sj_dari,'dd-mm-yyyy',this)"></input>
                
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>
    
    <div class="row">
        <?php echo form_label('Tanggal SJ ke &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;', 'tanggal_sj_ke', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback ">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
               <input id='tanggal_sj_ke' name='tanggal_sj_ke' value='<?php echo date_to_id($values->tanggal_sj_ke) ?>' class='form-control'
				placeholder="Tanggal SJ Ke" maxlength="10" onclick="displayCalendar(document.forms[0].tanggal_sj_ke,'dd-mm-yyyy',this)"></input>
                
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>    
         <div class="row">
        <div class="form-group has-feedback ">
            <label for="unit_jahit" class="control-label">Unit Jahit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="unit_jahit" name="unit_jahit">
			<option value="0"> - Pilih -</option>
		<?php foreach ($unit_jahit as $unj) {?> 
    <option value=<?php echo $unj->id ?>><?php echo $unj->nama_unit_jahit?></option>
    <?php } ?>
  </select>
        </div>
        </div>   
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="unit_packing" class="control-label">Unit Packing &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="unit_packing" name="unit_packing">
			<option value="0"> - Pilih -</option>
		<?php foreach ($unit_packing as $unp) {?> 
    <option value=<?php echo $unp->id ?>><?php echo $unp->nama_unit_packing?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="gudang" class="control-label">Gudang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="gudang" name="gudang">
			<option value="0"> - Pilih -</option>
		<?php foreach ($gudang as $gud) {?> 
    <option value=<?php echo $gud->id ?>><?php echo $gud->nama_gudang?></option>
    <?php } ?>
  </select>
        </div>
        </div>     
        <?php echo form_button(array('content'=>'View', 'type'=>'submit', 'class'=>'btn btn-info')) ?>

</form>
        </div>
