

<?php
// Nomor urut data di tabel.
$per_page = 10;

// Posisi nomor halaman (untuk paging) jika user login / tidak.
$login_status = $this->session->userdata('login_status');
$user_bagian = $this->session->userdata('user_bagian');




if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Informasi Mutasi Makloon Baju di Unit Jahit</h2>
    <hr>

    <?php if (!empty($informasi_mutasi_makloon_baju) && is_array($informasi_mutasi_makloon_baju)): ?>
    <div class="row">
        <div class="col-md-12">
 <form action ="export_csv" name="myform" id='myform' class="form-inline" method="post">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
					<th>Barang BB</th>
					<th>Saldo Awal</th>
					<th>Masuk Bagus</th>
                    <th>Masuk Lain - lain</th>
                    <th>Keluar Bagus</th>
                    <th>Keluar Lain - lain</th>
                    <th>Stopkopname</th>
                    <th>Saldo Akhir</th>
                    <th>Selisih</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
 // print_r($informasi_mutasi_makloon_baju);
               
               for($k=0;$k<count($informasi_mutasi_makloon_baju);$k++){
				   $data_detail=$informasi_mutasi_makloon_baju[0]['data_detail'];
				  for($i=0;$i<count($data_detail);$i++){   
				
				 ?>  
				   <tr>
						
                        <td><?php echo ++$offset ?></td>
						
						<td align='center'><?php echo $data_detail[$i]['kode_barang_bb'] ?> </td>
						<td align='center'> </td>
						<td align='center'><?php echo $data_detail[$i]['jum_masuk_bagus_unit_jahit'] ?> </td>
						<td align='center'><?php echo $data_detail[$i]['jum_masuk_bagus_unit_jahit'] ?> </td>
						<td align='center'><?php echo $data_detail[$i]['jum_masuk_lain_unit_jahit'] ?> </td>
						<td align='center'><?php echo $data_detail[$i]['jum_keluar_lain_unit_jahit'] ?> </td>
						<td align='center'><?php echo $data_detail[$i]['jum_stok_opname_jahit'] ?> </td>
						<td align='center'><?php echo $data_detail[$i]['saldo_akhir'] ?> </td>
						<td align='center'><?php echo $data_detail[$i]['selisih'] ?> </td>
                    </tr>
                    
                    
				   <?php
					}
					?>
					 <tr>
					
						<td align='center' colspan='3'>Total</td>	
						<td align='center'><?php echo $informasi_mutasi_makloon_baju[0]['total_masuk_bagus_unit_jahit'] ?> </td>
						<td align='center'><?php echo $informasi_mutasi_makloon_baju[0]['total_masuk_bagus_unit_jahit'] ?> </td>
						<td align='center'><?php echo $informasi_mutasi_makloon_baju[0]['total_masuk_lain_unit_jahit'] ?> </td>
						<td align='center'><?php echo $informasi_mutasi_makloon_baju[0]['total_keluar_lain_unit_jahit'] ?> </td>
						<td align='center'><?php echo $informasi_mutasi_makloon_baju[0]['total_stok_opname_jahit'] ?> </td>
						<td align='center'><?php echo $informasi_mutasi_makloon_baju[0]['total_saldo_akhir'] ?> </td>
						<td align='center'><?php echo $informasi_mutasi_makloon_baju[0]['total_selisih'] ?> </td>
                    </tr>
				<?php	
			   }
				 ?>
                    
					
						
                </tbody>
            </table>
            <input type="hidden" name="tanggal_sj_dari" id="tanggal_sj_dari" value="<?php echo $tanggal_sj_dari ?>">
             <input type="hidden" name="tanggal_sj_ke" id="tanggal_sj_ke" value="<?php echo $tanggal_sj_ke ?>">
               <input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">

</form>

        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $informasi_mutasi_makloon_baju ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



