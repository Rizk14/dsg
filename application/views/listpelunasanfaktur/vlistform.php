<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_pelunasan; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_pelunasan; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listpelunasanfaktur/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlpelunasanform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="80%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_pelunasan_no_voucher; ?> </td>
					<td width="1%">:</td>
					<td width="70%">
					  <input name="no_voucher" type="text" id="no_voucher" maxlength="14" value="<?=$novoucher?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_pelunasan_tgl_voucher; ?></td>
					<td>:</td>
					<td>
					  <input name="d_voucher_first" type="text" id="d_voucher_first" maxlength="10" value="<?=$tglvouchermulai?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_voucher_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_voucher_last" type="text" id="d_voucher_last" maxlength="10" value="<?=$tglvoucherakhir?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_voucher_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
			    <td>
			    <div id="title-box2"><?php echo $form_title_detail_pelunasan; ?></div></td>	
			  </tr>
			  <tr>
				<td>
				<?php if($template==1) { ?>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="14%" class="tdatahead"><?php echo $list_pelunasan_no_voucher; ?> </td>
					<td width="18%" class="tdatahead"><?php echo $list_pelunasan_keterangan; ?> </td>
					<td width="12%" class="tdatahead"><?php echo $list_pelunasan_tgl_voucher; ?> </td>
					<td width="20%" class="tdatahead"><?php echo $list_pelunasan_recieve; ?> </td>
					<td width="20%" class="tdatahead"><?php echo $list_pelunasan_approve; ?> </td>
					<td width="24%" class="tdatahead"><?php echo $list_pelunasan_total_pelunasan; ?> </td>
				  </tr>
				  
				  <?php

				  $no	= 1;
				  $cc	= 1;
				  $arr	= 0;
				  
				  $totalpelunasan = 0;
				  
				  if(sizeof($query) > 0 ) {

					  foreach($query as $row) {
					  
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$exp_d_voucher = explode("-",$row->d_voucher,strlen($row->d_voucher));
						
						$lpelunasan	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>"."<a href=\"#\">".$row->i_voucher_code." - ".$row->i_voucher_no."</a>"."</td>
								<td>".$row->e_description."</td>
								<td align=\"center\">".$exp_d_voucher[2].'/'.$exp_d_voucher[1].'/'.$exp_d_voucher[0]."</td>
								<td align=\"left\">".$row->e_recieved."</td>
								<td align=\"left\">".$row->e_approved."</td>
								<td align=\"right\">".number_format($row->pelunasan,'2','.',',')."</td>
							  </tr>";	
							  
							  $totalpelunasan = $totalpelunasan+$row->pelunasan;
							  
							  $no+=1;
							  $cc+=1;	
							  $arr+=1;
					  }
					  echo $lpelunasan;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
				<?php } ?>

				<?php if($template==2) { ?>
				  				
				<table width="85%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="12%" class="tdatahead"><?php echo $list_pelunasan_no_voucher; ?> </td>
					<td width="14%" class="tdatahead"><?php echo $list_pelunasan_tgl_voucher; ?> </td>
					<td width="20%" class="tdatahead"><?php echo $list_pelunasan_nilai_voucher; ?> </td>
					<td width="15%" class="tdatahead"><?php echo $list_pelunasan_no_faktur; ?> </td>
					<td width="15%" class="tdatahead"><?php echo $list_pelunasan_tgl_faktur; ?> </td>
					<td width="20%" class="tdatahead"><?php echo $list_pelunasan_sumber_faktur; ?></td>
				  </tr>
				  
				  <?php

				  $no	= 1;
				  $cc	= 1;
				  $arr	= 0;
				  
				  //$totalpelunasan = 0;
				  
				  if(sizeof($query)>0) {
					  
					  foreach($query as $row) {
						
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";					
						
						$f_nota_sederhana	= $row->f_nota_sederhana=='f'?'DO':'Non DO';
						
						$exp_d_voucher = explode("-",$row->d_voucher,strlen($row->d_voucher));
						$exp_d_dt =  explode("-",$row->d_dt,strlen($row->d_dt));
						
						$lpelunasan	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->i_voucher_code." - ".$row->i_voucher_no."</td>
								<td align=\"center\">".$exp_d_voucher[2].'/'.$exp_d_voucher[1].'/'.$exp_d_voucher[0]."</td>
								<td align=\"right\">".number_format($row->v_voucher,'2','.',',')."</td>
								<td align=\"left\">".$row->i_dt_code."</td>
								<td align=\"center\">".$exp_d_dt[2].'/'.$exp_d_dt[1].'/'.$exp_d_dt[0]."</td>
								<td align=\"center\">".$f_nota_sederhana."</td>
							  </tr>";	
							  
							  //$totalpelunasan = $totalpelunasan+$row->v_total_voucher;
							  
							  $no+=1;
							  $cc+=1;	
							  $arr+=1;
					  }
					  echo $lpelunasan;
				  }
				  
				  $query	= $this->mclass->totalvoucher($ivoucher);
				  if($query->num_rows()>0){
					  $row_totalvoucher = $query->row();
					  $v_total_voucher	= $row_totalvoucher->pelunasan;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
				<?php } ?>				
				</td>
			  </tr>
			  <tr>
			  	<td align="center"><?php echo $create_link; ?></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>				  		  
			  <tr>
				<td><table width="95%" border="0" cellspacing="0" cellpadding="0">
				<?php if($template==1) { ?>
				  <tr>	
					<td width="20%" align="right"></td>
					<td width="0%"></td>
					<td width="20%" align="right"></td>
					<td width="20%" align="right"></td>
					<td width="0%"></td>
					<td width="20%" align="right"></td>
				  </tr>	
				<?php } ?>
				
				<?php if($template==2) { ?>
				  <tr>	
					<td width="60%" align="right"><?php echo $list_pelunasan_total_pelunasan; ?></td>
					<td width="1%">:</td>
					<td width="39%">
					<input name="v_total_pelunasan" type="text" id="v_total_pelunasan" maxlength="50" style="text-align:right;" value="<?php echo number_format($v_total_voucher,'2','.',','); ?>" style="text-align:right;" />
					</td>
				  </tr>				  
				<?php } ?>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
							<input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listpelunasanfaktur/cform/'">

				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
