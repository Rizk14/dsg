<table class="tborder" cellpadding="6" cellspacing="1" border="0" style="width: 1022px;" align="center" style="border-bottom-width:0px">
	<tr><td class="tcat"><?php echo "Daftar Bank" ?></td></tr>
</table>
<table class="tborder" cellpadding="6" cellspacing="1" border="0" style="width: 1022px;" align="center">
	<tr>
		<td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo "Daftar Bank" ?></td>
	</tr>
	<tr>
		<td class="alt2" style="padding:0px;">
			<table class="maintable">
				<tr>
					<td align="left">
						<? echo $this->pquery->form_remote_tag(array('url'=>'list-akt-bk/cform/view','update'=>'#content','type'=>'post'));?>
						<div id="spbperareaform">
							<div class="effect">
								<div id="masterforcastform" style="width: 100%;" border='1'>
									<table class="mastertable">
										<tr>
											<td width="40%">Date From</td>
											<td width="1%">:</td>
											<td width="80%">
												<input type="hidden" id="areafrom" name="areafrom" value="">
												<?php 
												$data = array(
													'name'        => 'dfrom',
													'id'          => 'dfrom',
													'value'       => '01-'.date('m-Y'),
													'placeholder' => 'Pilih Tanggal ...',
													'readonly'    => 'true',
													'maxlength'	  => '20');
												echo form_input($data);
												?>
											</td>
										</tr>
										<tr>
											<td width="19%">Date To</td>
											<td width="1%">:</td>
											<td width="80%">
												<?php 
												$data = array(
													'name'        => 'dto',
													'id'          => 'dto',
													'value'       => date('d-m-Y'),
													'placeholder' => 'Pilih Tanggal ...',
													'readonly'   	=> 'true',
													'maxlength'	=> '20');
												echo form_input($data);
												?>
											</td>
										</tr>
										<tr>
											<td width="19%">Bank</td>
											<td width="1%">:</td>
											<td width="80%">
												<input type="hidden" id="ibank" name="ibank" value="">
												<input type="text" id="ebankname" readonly="" placeholder="Pilih Bank" name="ebankname" value="" onclick='openbank();'>
												<input type="hidden" id="icoa" name="icoa" value="">
											</td>
										</tr>
										<tr>
											<td colspan="3">
												<button name="login" id="login" value="View" type="submit">View</button>
												<button name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('list-akt-bk/cform/','#tmpx')">Keluar</button>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<?=form_close()?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script type="text/javascript">
	$(function() {
		$('#dfrom').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy'
		});
		$('#dto').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy'
		});
	});

	function openbank() {
		lebar =450;
		tinggi=400;
		eval('window.open("<?php echo site_url(); ?>"+"/list-akt-bk/cform/bank","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
</script>