<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Rekap Pembelian Per Supplier</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) {echo "Cash"; } elseif ($jenis_beli == 2) {echo "Kredit";} else  {echo "Cash + Credit";}?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-rekap-pembelian-persup/cform/export_excel', $attributes); ?>
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		 <th rowspan="2">No</th>
		 <th rowspan="2">Nama Supplier</th>
		 <th colspan="3">Bahan Baku</th>
		 <th colspan="3">Bahan Pembantu</th>
		 <th rowspan="2">Jumlah</th>
	 </tr>
	 <tr class="judulnya">
		<th>Pembelian</th>
		<th>DPP</th>
		<th>PPN</th>
		<th>Pembelian</th>
		<th>DPP</th>
		<th>PPN</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
				$total_baku = 0;
				$total_pembantu = 0;
				$total_semua = 0;
			if (is_array($query)) {
				$total_baku = 0;
				$total_pembantu = 0;
				$total_semua = 0;
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_total_baku'],4,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['dpp_baku'],4,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['ppn_baku'],4,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_total_pembantu'],4,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['dpp_pembantu'],4,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['ppn_pembantu'],4,',','.')."</td>";

				 $totalnya = $query[$j]['jum_total_baku'] + $query[$j]['jum_total_pembantu'];
				 echo    "<td align='right'>".number_format($totalnya,4,',','.')."</td>";
				 $total_baku += $query[$j]['jum_total_baku'];
				 $total_pembantu += $query[$j]['jum_total_pembantu'];
				 $total_semua += $totalnya;
				 echo  "</tr>";					
		 	}
		   }
		 ?>
		 <tr>
			<td>&nbsp;</td>
			<td><b>TOTAL</b></td>
			<td align="center" colspan="3"><b><?php echo number_format($total_baku,4,',','.') ?></b></td>
			<td align="center" colspan="3"><b><?php echo number_format($total_pembantu,4,',','.') ?></b></td>
			<td align="right"><b><?php echo number_format($total_semua,4,',','.') ?></b></td>
		 </tr>
 	</tbody>
</table><br>
</div>
