<style type="text/css">
	table {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 12px;
	}

	.judulnya {
		background-color: #DDD;
	}
</style>

<h3>Data Supplier</h3><br>
<?php
/* FILTER HANYA UNTUK ADMIN & FIS (11 AGS 2022) */
if (
	$this->session->userdata('euser') == 'fs1' || $this->session->userdata('euser') == 'fs2' ||
	$this->session->userdata('euser') == 'fs3' || $this->session->userdata('euser') == 'admin' ||
	$this->session->userdata('euser') == 'fs8'
) {
?>
	<a href="<?php echo base_url(); ?>index.php/mst-supplier/cform">Tambah Data</a>&nbsp;&nbsp;
<?php } ?>
<a href="<?php echo base_url(); ?>index.php/mst-supplier/cform/view">View Data</a>&nbsp;&nbsp;
<a href="<? echo base_url(); ?>index.php/mst-supplier/cform/exportexcel/1">Export ke Excel (Window)</a>&nbsp;&nbsp;
<a href="<? echo base_url(); ?>index.php/mst-supplier/cform/exportexcel/2">Export ke ODS (Linux)</a><br><br>

<div>
	Total Data = <?php echo $jum_total ?><br><br>
	<?php echo form_open('mst-supplier/cform/view'); ?>
	<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
	<?php echo form_close(); ?>
	<br>

	<table border="1" cellpadding="1" cellspacing="1" width="90%">
		<thead>
			<p style="font-family: Helvetica, Geneva, Arial, SunSans-Regular; font-size:14px; color:#fc0303;">
				*Jika Kode membesar & berwarna merah menandakan supplier tidak aktif
			</p>
			<tr class="judulnya">
				<th>Kode</th>
				<th>Nama Supplier</th>
				<th>Alamat</th>
				<th>Kota</th>
				<th>Telepon</th>
				<th>Kontak Person</th>
				<th>PKP</th>
				<th>Kategori</th>
				<th>Last Update</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($query as $row) {
				if ($row->tgl_update) {
					$pisah1 = explode("-", $row->tgl_update);
					$tgl1 = $pisah1[2];
					$bln1 = $pisah1[1];
					$thn1 = $pisah1[0];

					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;

					$exptgl1 = explode(" ", $tgl1);
					$tgl1nya = $exptgl1[0];
					$jam1nya = $exptgl1[1];
					$tgl_update = $tgl1nya . " " . $nama_bln . " " . $thn1 . " " . $jam1nya;
				} else {
					$tgl_update = '';
				}

				echo "<tr class=\"record\">";
				if ($row->f_aktif == 't') {
					echo    "<td>$row->kode_supplier</td>";
				} else {
					echo    "<td style='color:#fc0303;font-size:20px;'><b>$row->kode_supplier</b></td>";
				}
				echo "<td>$row->nama</td>";
				echo "<td>$row->alamat</td>";
				echo "<td>$row->kota</td>";
				echo "<td>$row->telp</td>";
				echo "<td>$row->kontak_person</td>";
				if ($row->pkp == 't')
					echo    "<td>Ya</td>";
				else
					echo    "<td>Tidak</td>";

				echo    "<td>" . $row->nama_kategory . "</td>";

				echo    "<td>$tgl_update</td>";

				$ada = 0;
				// cek apakah data kode sudah dipake di OP ataupun SJ				 
				$query3	= $this->db->query(" SELECT id FROM tm_op WHERE id_supplier = '$row->id' ");
				if ($query3->num_rows() > 0) {
					$ada = 1;
				}

				$query3	= $this->db->query(" SELECT id FROM tm_pembelian WHERE id_supplier = '$row->id' ");
				if ($query3->num_rows() > 0) {
					$ada = 1;
				}

				echo    "<td align=center><a href=" . base_url() . "index.php/mst-supplier/cform/index/$row->id \" >Edit</a>";
				if ($ada == 0)
					echo    "&nbsp; <a href=" . base_url() . "index.php/mst-supplier/cform/delete/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				echo "</td>";
				echo  "</tr>";
			}
			?>
		</tbody>
	</table><br>
	<center><?php echo $this->pagination->create_links(); ?></center>
</div>