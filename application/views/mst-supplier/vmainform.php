<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
<script type="text/javascript">
	function cek_data() {
		var kode = $('#kode').val();
		var nama = $('#nama').val();
		var alamat = $('#alamat').val();
		var kota = $('#kota').val();
		//var telp= $('#telp').val();
		var top = $('#top').val();
		var email = $('#email').val();

		if (kode == '') {
			alert("Kode harus diisi..!");
			$('#kode_jenis').focus();
			return false;
		}

		if (nama == '') {
			alert("Nama supplier harus diisi..!");
			$('#nama').focus();
			return false;
		}
		if (alamat == '') {
			alert("Alamat harus diisi..!");
			$('#alamat').focus();
			return false;
		}
		if (kota == '') {
			alert("Kota harus diisi..!");
			$('#kota').focus();
			return false;
		}
		/*if (telp == '') {
			alert("Telepon harus diisi..!");
			$('#telp').focus();
			return false;
		} */
		if (top == '') {
			alert("TOP harus diisi..!");
			$('#top').focus();
			return false;
		}

		var atps = email.indexOf("@");
		var dots = email.lastIndexOf(".");

		if (email == '') {
			return true;
		} else if (atps < 1 || dots < atps + 2 || dots + 2 >= email.length) {
			alert("Alamat email tidak valid.");
			return false;
		}
	}
</script>

<h3>Data Supplier</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-supplier/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-supplier/cform/view">View Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-supplier/cform/exportexcel/1">Export ke Excel (Window)</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-supplier/cform/exportexcel/2">Export ke ODS (Linux)</a><br><br>

<?php if ($msg != '') echo "<i>" . $msg . "</i><br>"; ?>
<?php echo form_open('mst-supplier/cform/submit'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1">
	<input type="hidden" name="kode_lama" value="<?php echo $ekode ?>">
	<input type="hidden" name="id_supplier" value="<?php echo $eid ?>">
<?php } ?>
<table>
	<!--<tr>
			<td>Kode</td>
			<td> <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="10" size="10">
			</td>
		</tr>-->
	<tr>
		<td>Inisial Awal</td>
		<td> <select name="inisial" id="inisial">
				<?php
				$s = 'A';
				while ($s != 'Z') {

				?>
					<option value="<?php echo $s; ?>" <?php if ($einisial == $s) { ?>selected <?php } ?>><?php echo $s; ?></option>
				<?php
					$s = chr(ord($s) + 1);
				} ?>
				<option value="<?php echo 'Z'; ?>"><?php echo 'Z'; ?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Nama Supplier</td>
		<td> <input type="text" name="nama" id="nama" value="<?php echo $enama ?>" maxlength="50" size="50"></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td> <textarea name="alamat" id="alamat" rows="6" cols="40"><?php echo $ealamat ?></textarea></td>
	</tr>
	<tr>
		<td>Kota</td>
		<td> <input type="text" name="kota" id="kota" value="<?php echo $ekota ?>" maxlength="20" size="20"></td>
	</tr>
	<tr>
		<td>Kontak Person</td>
		<td> <input type="text" name="kontak_person" value="<?php echo $ekontak_person ?>" maxlength="20" size="20"></td>
	</tr>
	<tr>
		<td>Telepon / HP</td>
		<td> <input type="text" name="telp" id="telp" value="<?php echo $etelp ?>" maxlength="20" size="20"></td>
	</tr>
	<tr>
		<td>E-mail</td>
		<td> <input type="text" name="email" id="email" value="<?php echo $email ?>" size="20"></td>
	</tr>
	<!-- <tr>
		<td>Faksimile</td>
		<td> <input type="text" name="fax" value="<?php echo $efax ?>" maxlength="20" size="20"></td>
	</tr> -->
	<tr>
		<td>Sistem Pembayaran</td>
		<td>
			<select name="paymenttype" id="paymenttype" value="">
				<option value="" <?php echo $paymenttype == "" ? 'selected' : ''; ?>></option>
				<option value="Tunai" <?php echo $paymenttype == "Tunai" ? 'selected' : ''; ?>>Tunai</option>
				<option value="Transfer" <?php echo $paymenttype == "Transfer" ? 'selected' : ''; ?>>Transfer</option>
				<option value="Cek/Giro" <?php echo $paymenttype == "Cek/Giro" ? 'selected' : ''; ?>>Cek/Giro</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Bank</td>
		<td>
			<input type="text" name="bankname" id="bankname" value="<?php echo $bankname; ?>" size="3">
		</td>
	</tr>
	<tr>
		<td>Nomor Rekening</td>
		<td>
			<input type="text" name="accountnumber" id="accountnumber" value="<?php echo $accountnumber; ?>" maxlength="16">
		</td>
	</tr>
	<tr>
		<td>Nama Rekening</td>
		<td>
			<input type="text" name="accountname" id="accountname" value="<?php echo $accountname; ?>">
		</td>
	</tr>
	<tr>
		<td>T.O.P</td>
		<td> <input type="text" name="top" id="top" value="<?php echo $etop ?>" maxlength="3" size="3"> Hari</td>
	</tr>
	<tr>
		<td>PKP</td>
		<td> <input type="checkbox" name="pkp" <?php if ($epkp == 't') { ?> checked="true" <?php } ?> value="t"></td>
	</tr>
	<tr>
		<td>NPWP</td>
		<td> <input type="text" name="npwp" value="<?php echo $enpwp ?>" maxlength="20" size="20"></td>
	</tr>
	<tr>
		<td>Nama NPWP</td>
		<td> <input type="text" name="nama_npwp" value="<?php echo $enama_npwp ?>" maxlength="30" size="30"></td>
	</tr>
	<tr>
		<td>Tipe Pajak</td>
		<td> <select name="tipe_pajak" id="tipe_pajak">
				<option value="I" <?php if ($etipe_pajak == 'I') { ?> selected="true" <?php } ?>>Include</option>
				<option value="E" <?php if ($etipe_pajak == 'E') { ?> selected="true" <?php } ?>>Exclude</option>
			</select></td>
	</tr>
	<tr>
		<td>Kategori Supplier</td>
		<td> <select name="kategori" id="kategori">
				<?php foreach ($ekategori as $kat) { ?>
					<option value="<?php echo $kat->id ?>" <?php if ($kat->id == $kategori) { ?> selected <?php } ?>><?php echo "[" . $kat->kode_kategory . "] " . $kat->nama ?></option>

				<?php } ?>


			</select></td>
	</tr>
	<tr>
		<td>Jenis Supplier</td>
		<td>
			<select name="jenis" id="jenis">
				<?php foreach ($ejeniskat as $jns) { ?>
					<option value="<?php echo $jns->id ?>" <?php if ($jns->id == $jenis) { ?> selected <?php } ?>><?php echo "[" . $jns->kode_jenis . "] " . $jns->nama ?></option>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Status Aktif/Non Aktif</td>
		<td>
			<input type="checkbox" name="chkstatus" id="chkstatus" value="<?= $statusaktif; ?>" <?= $statusaktif; ?> onclick='chkaktif()'>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<?php if ($edit == '') {
				/* FILTER HANYA UNTUK ADMIN & FIS (11 AGS 2022) */
				if (
					$this->session->userdata('euser') == 'fs1' || $this->session->userdata('euser') == 'fs2' ||
					$this->session->userdata('euser') == 'fs3' || $this->session->userdata('euser') == 'admin' ||
					$this->session->userdata('euser') == 'fs8'
				) {
					echo "<input type='submit' name='submit' value='Simpan' onclick='return cek_data();'>&nbsp;";
				}
				echo "<input type='reset' name='batal' value='Reset'>";
			} else {
				/* FILTER HANYA UNTUK ADMIN & FIS (11 AGS 2022) */
				if (
					$this->session->userdata('euser') == 'fs1' || $this->session->userdata('euser') == 'fs2' ||
					$this->session->userdata('euser') == 'fs3' || $this->session->userdata('euser') == 'admin'  ||
					$this->session->userdata('euser') == 'fs8'
				) {
			?>
					<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;
				<?php } ?>
				<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-supplier/cform/view'">
			<?php }
			?>
		</td>
	</tr>
</table>
<?php echo form_close(); ?> <br>