<h3>Data Harga Bahan Baku/Pembantu Berdasarkan Unit Jahit</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-bb-wip/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-bb-wip/cform/view">View Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-bb-wip/cform/print_harga">Print/Export Excel</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

 $(function()
 {
		
	$('#pilih_jenis').click(function(){

	  	    var id_kel_brg= jQuery('#kel_brg').val();

			var urlnya = "<?php echo base_url(); ?>index.php/mst-bb-wip/cform/show_popup_jenis/"+id_kel_brg;

			openCenteredWindow(urlnya);

	  });
	 	  
	  $('#kel_brg').change(function(){
	  	    $("#id_jenis").val('');	
	  	    $("#kode_jenis").val('');		  	    
	  });

 });
 $(function()
{
	//$("#no").val('2');
	
	//var is_new = $('#is_new').val(); 
	//if(is_new == 1)
	//	$("#jum_data").val('2');
	
	//generate_nomor();		
	$("#additem").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#jum_data").val();		
		var n=parseInt(no)-1;
		//var n=parseInt(no);
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode_brg_jadi*************************************
		var kode_brg_jadi="#kode_brg_jadi_"+n;
		var new_kode_brg_jadi="#kode_brg_jadi_"+no;
		$(kode_brg_jadi, lastRow).attr("id", "kode_brg_jadi_"+no);
		$(new_kode_brg_jadi, lastRow).attr("name", "kode_brg_jadi_"+no);		
		$(new_kode_brg_jadi, lastRow).attr("onkeyup", "cari('"+no+"', this.value);");		
		$(new_kode_brg_jadi, lastRow).val('');		
		//*****end kode_brg_jadi*********************************
												
		//*****nama_brg_jadi*************************************
		var nama_brg_jadi="#nama_brg_jadi_"+n;
		var new_nama_brg_jadi="#nama_brg_jadi_"+no;
		$(nama_brg_jadi, lastRow).attr("id", "nama_brg_jadi_"+no);
		$(new_nama_brg_jadi, lastRow).attr("name", "nama_brg_jadi_"+no);		
		$(new_nama_brg_jadi, lastRow).val('');				
		//*****end nama_brg_jadi*************************************
										
		//******div infobrgjadi*************************************
		var infobrgjadi="#infobrgjadi_"+n;
		var new_infobrgjadi="#infobrgjadi_"+no;
		$(infobrgjadi, lastRow).attr("id", "infobrgjadi_"+no);

		$(new_infobrgjadi, lastRow).html("<input type='text' id='nama_brg_jadi_"+no+"' name='nama_brg_jadi_"+no+"' value='' readonly='true' size='40'>");
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('0');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
		var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('0');				
		//*****end harga_lama*************************************	
														
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//jum_data++
		var x=parseInt(no);
		$("#jum_data").val(x+1);	
		
	});	
	
	$("#deleteitem").click(function()
	{
		var x= $("#jum_data").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#jum_data").val();	
			var y= x-1;
			$("#jum_data").val(y);	
		}
	});	
	
});
 
 function cek_data() {
	var id_jenis= $('#id_jenis').val();

	function cek_input() {
	var jum_data = $('#jum_data').val()-1;
	var s = 0;
	kon = window.confirm("Yakin akan simpan data HPP ??");
		
	if (kon) {
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
			/*	var kode_brg_jadi=$("#kode_brg_jadi_"+i).val();
				var nama_brg_jadi=$("#nama_brg_jadi_"+i).val();
				var harga=$("#harga_"+i).val(); */
				
				if ($('#kode_brg_jadi_'+k).val() == '') {
					alert("Kode barang harus diinput..!");
					s=1;
					return false;
				}
				if ($('#nama_brg_jadi_'+k).val() == '' ) {
					alert("Barang Jadi tidak valid..!");
					s=1;
					return false;
				}
				if (isNaN($('#harga_'+k).val()) || $('#harga_'+k).val() == '') {
					alert("Data harga harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				if ($('#harga_'+k).val() == '0') {
					alert("Data harga tidak boleh 0..!");
					s=1;
					return false;
				}
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}

 }	
 
 function cari(posisi, kodebrgwip) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/mst-bb-wip/cform/caribrgwip', 
				data: 'kode_brg_jadi='+kodebrgwip+'&posisi='+posisi, success: function(response) {
					$("#infobrgjadi_"+posisi).html(response);
			}});
	
	
}
 
 
 
 
/*
 function cek_cb() {
	var ceknya = $(":checkbox:checked").length;
	var hitung = $('#jum').val();

	if (ceknya == 0) {
		alert("Data-data barangnya harus dipilih..!");
		return false;
	}
	else {
		for (var k=1; k <= hitung; k++) {
			if ($('input[name=cek_'+k+']').is(':checked')== true) {
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Harga harus berupa angka..!");
					return false;
				}
			} // end if
		}
	}
	
}
*/
</script>

<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
echo form_open('mst-bb-wip/cform/proseslistbrg', $attributes); ?>
<table>
		<tr>
			<td>Unit Jahit</td>
			<td> <select name="unit_jahit" id="unit_jahit">
				<?php foreach ($unit_jahit as $ujh) { ?>
					<option value="<?php echo $ujh->id ?>" ><?php echo $ujh->kode_unit." - ". $ujh->nama ?></option>
				<?php } ?>
				</select>
				<input type="hidden" name="kode_unit_jahit" value="<?php echo $ujh->kode_unit ?>">
			</td>
		</tr>
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kel_brg" id="kel_brg">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->id ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td> <input type="text" name="kode_jenis" id="kode_jenis" value="" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang">
			</td>
		</tr>
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb-wip/cform/view'">
<?php echo form_close(); 
}
else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/mst-bb-wip/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_unit_jahit" value="<?php echo $id_unit_jahit ?>">
<input type="hidden" name="kode_unit_jahit" value="<?php echo $kode_unit_jahit ?>">

<input type="hidden" name="jum_data" id="jum_data" value="<?php echo $jum_data ?>">

<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td width="15%">Unit Jahit</td>
		<td width="70%">
		  <?php echo $nama_unit_jahit; ?>
		</td>
    </tr>
	<tr>
		<td>Kelompok Barang</td>
		<td>
		  <?php echo $kode_kel." - ".$nama_kel; ?>
		</td>
	</tr>
	<tr>
		<td>Jenis Barang</td>
		<td>
		  <?php echo $nama_jenis; ?>
		</td>
	</tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
		<div>
<input id="additem" type="button" name="additem" value=" + " title="Tambah Item Barang">&nbsp;
<input id="deleteitem" type="button" name="deleteitem" value=" - " title="Hapus Item Barang">
<br><br>
		
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>

	      <th>Harga (Rp.)</th>
	      <th>Pilih</th>
        </tr>

       <?php
		  if (is_array($list_brg)) {
			  $i=1;

			 for($j=0;$j<count($list_brg);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
			<input type="hidden" name="id_brg_jadi_<?php echo $i ?>" id="id_brg_jadi_<?php echo $i ?>" value="<?php echo $list_brg[$j]['id_brg'] ?>">          
           <input type="text" name="kode_brg_jadi_<?php echo $i ?>" id="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $list_brg[$j]['kode_brg_jadi'] ?>" onkeyup="cari('<?php echo $i ?>',this.value);">
          </td>
          <td>
          <div id="infobrgjadi_<?php echo $i ?>"><input type="text" size="40" name="nama_brg_jadi_<?php echo $i ?>" id="nama_brg_jadi_<?php echo $i ?>" value="<?php echo $list_brg[$j]['nama_brg_jadi'] ?>" readonly="true"></div>
          </td>
          
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="5" value="<?php echo $list_brg[$j]['harga'] ?>" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'" />
			<input type="hidden" name="harga_lama_<?php echo $i ?>" id="harga_lama_<?php echo $i ?>" value="<?php echo $list_brg[$j]['harga'] ?>"> 
          

          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		else {
		?>
		
		<tr>
		<td align="center" id="num_1">1</td>
		<td><input type="text" name="kode_brg_jadi_1" id="kode_brg_jadi_1" size="10" value="" onkeyup="cari('1',this.value);">
		</td>
		<td><div id="infobrgjadi_1">
			<input type="text" name="nama_brg_jadi_1" id="nama_brg_jadi_1" size="40" value="" readonly="true"></div></td>
		<td><input type="text" name="harga_1" id="harga_1" size="5" value="0" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'">
		<input type="hidden" name="harga_lama_1" id="harga_lama_1" value="0">
		</td>
	</tr>
	
		<?php } 
		?>
		
			
	</table>
		
	</div>
	<tr>
		<td colspan="5" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_cb();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb-wip/cform/'"></td>
	</tr>
	</form>
		</td>
    </tr>

</table>
<?php
}
		?>
</div>
</form>

