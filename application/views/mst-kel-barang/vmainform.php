<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">
function cek_data() {
	var kode= $('#kode').val();
	var kode_perkiraan= $('#kode_perkiraan').val();
	var nama= $('#nama').val();
	if (kode == '') {
		alert("Kode harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (kode_perkiraan == '') {
		alert("Kode Perkiraan harus diisi..!");
		$('#kode_perkiraan').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Kelompok harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
}
</script>
<h3>Data Kelompok Barang</h3><br>
<?php echo form_open('mst-kel-barang/cform/submit'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="kodeedit" value="<?php echo $ekode ?>"><?php } ?>
	<table>
		<tr>
			<td>Kode</td>
			<td>: <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="1" size="2" <?php if ($edit == '1') { ?> readonly="true" <?php } ?> > <i> *) 1 digit alphabet (Contoh :B)</i>
			</td>
		</tr>
		<tr>
			<td>Kode Perkiraan</td>
			<td>: <input type="text" name="kode_perkiraan" id="kode_perkiraan" value="<?php echo $ekode_perkiraan ?>" maxlength="10" size="10"> <i> *)3 digit awal diikuti .100 (contoh:501.100)</i>
			</td>
		</tr>
		<tr>
			<td>Nama Kelompok</td>
			<td>: <input type="text" name="nama" id="nama" value="<?php echo $enama ?>" maxlength="30"> <i> contoh:Bahan Baku</i>
			</td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>: <input type="text" name="ket" id="ket" value="<?php echo $eket ?>" maxlength="50"></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-kel-barang/cform/'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

<div>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th>Kode</th>
		 <th>Kode Perk</th>
		 <th>Nama Kelompok</th>
		 <th>Keterangan</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode</td>";
				 echo    "<td>$row->kode_perkiraan</td>";
				 echo    "<td>$row->nama</td>";
				 echo    "<td>$row->keterangan</td>";
				$pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				 echo    "<td>$tgl_update</td>";
				 
				 // cek apakah data kode sudah dipake di jenis barang
				 $query3	= $this->db->query(" SELECT id FROM tm_jenis_barang WHERE kode_kel_brg = '$row->kode' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 else
					$ada = 0;
				 
				 echo    "<td align=center><a href=".base_url()."index.php/mst-kel-barang/cform/index/$row->kode \" id=\"$row->kode\">Edit</a>";
				 
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/mst-kel-barang/cform/delete/$row->kode \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table>
</div>
