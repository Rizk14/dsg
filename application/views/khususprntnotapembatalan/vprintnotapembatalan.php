<!--<style type="text/css" media="print">
	@page 
        {
            size: auto;   /* auto is the current printer page size */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
</style> -->

<style type="text/css">

/*@media print {
	div {
		color: #f00;
	}
} */

/*yang ini buat setting ukuran kertasnya assumsi A4/letter, sementara ga dipake */
/*#Letter {background-color:#FFFFFF;
left:1px;
right:1px;
height:11in ; */ /*Ukuran Panjang Kertas */
/*width: 8.50in; */ /*Ukuran Lebar Kertas */
/*margin:1px solid #FFFFFF; */
 
/* font-family:Georgia, "Times New Roman", Times, serif; } */

   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
    
    }
    
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 19px;
    font-weight: bold;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
	}
	.kepala2 {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	
	.detailbrg2 {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
	}
	.detailbrg {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
	}
	
	.kotak {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .kotak2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 13px;}
    	
	.garisbawah { 
		border-bottom:#000000 0.1px solid;
	}
	.gariskanan { 
		border-right:#000000 0.1px solid;
	}
	
	.nomorpajak {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 15px;}
</style>

<?php include_once ("funcs/terbilang.php"); ?>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	//window.print();
});

</script>
<table border="0" width="100%" align="center">
	<tr>
		<td class="kepala" width="60%" align="center">NOTA PEMBATALAN</td>
	</tr>
	<tr>
		<td class="detailbrg2" width="60%" align="center">Nomor : <?php echo $nonota ?></td>
	</tr>
</table>

<table border="1" width="100%" class="kotak" align="center">
	<tr class="kepala2">
		<td>&nbsp;&nbsp;&nbsp;Atas Faktur Pajak Nomor : <span class="nomorpajak"><?php echo $nomorpajak ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanggal : <?php echo $tglfaktur ?> </td>
	</tr>
	<tr class="kepala2">
		<td>
		
		<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td colspan="2" align="center">&nbsp;&nbsp;Penerima Jasa Kena Pajak<hr></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;" width="25%">&nbsp;&nbsp;N a m a </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo strtoupper($nminitial) ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;A l a m a t </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo strtoupper($almtperusahaan) ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;N P W P </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo strtoupper($npwpperusahaan) ?></td>
			</tr>
		</table>
		
		</td>
	</tr>
	<tr class="kepala2">
		<td>
		
		<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td colspan="2" style="white-space:nowrap;" align="center">&nbsp;&nbsp;Kepada Pemberi Jasa Kena Pajak<hr></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;" width="25%">&nbsp;&nbsp;N a m a </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo $nmkostumer ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;A l a m a t </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo $almtkostumer ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;N P W P </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo $npwp ?></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0">
			<tr align="center" class="detailbrg2">
				<td width="5%" class="garisbawah gariskanan">No. Urut</td>
				<td width="65%" class="garisbawah gariskanan">Jasa Kena Pajak Yang Dibatalkan</td>
				<td width="30%" class="garisbawah">Penggantian JKP (Rp).</td>
			</tr>
			<?php
				$no=1;
				$j = 0;
				$hitung = count($isi);
				foreach($isi as $row) {
					if($no<23 && $row->motifname!='') {
						$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);

						if($qvalue->num_rows()>0) {
							$row_value	= $qvalue->row();
							$tprice[$j]	= $row_value->amount;
						}else{
							$tprice[$j]	= 0;
						}

						$jj	= $j+1;
						
			?>
			<!-- disini perulangan item brg -->
			<tr class="detailbrg">
				<td align="right" class="gariskanan"><?php echo $no; ?>&nbsp;</td>
				<td class="gariskanan">&nbsp;<?php echo strtoupper($row->motifname); ?></td>
				<td align="right"><?php echo number_format($tprice[$j]); ?> &nbsp;</td>
			</tr>
			<?php 
						$j+=1;
						$no+=1;
					}
				}
			?>
			<!-- end perulangan -->
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td width="8%" style="white-space:nowrap;">&nbsp;&nbsp;Jumlah Penggantian JKP Yang Dibatalkan</td>
				<td align="right"><?php echo number_format($jumlah); ?>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td>&nbsp;&nbsp;PPN Yang Diminta Kembali</td>
				<td align="right"><?php echo number_format($nilai_ppn); ?>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
		
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td>&nbsp;&nbsp;</td>
				<td align="right"><?php echo $kota ?>, Tanggal <?php echo $tglnota ?></td>
			</tr>
			<tr>
				<td>
					&nbsp;&nbsp;
				</td>
				<td align="right">
					<br><br><br><br><u><?php echo $TtdPajak01 ?></u><br>Adm Keuangan
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
