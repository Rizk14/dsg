<style>
.vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
}

</style>

<div class="container">
<h2>Edit Makloon Baju dari Gudang ke Gudang</h2>
<hr>

<?php echo form_open('/makloon_jadi_gudang_jadi_wip/makloon_jadi_gudang_jadi_wip/updatedata', array('id'=>'myform', 'class'=>'form-vertical', 'role'=>'form', 'method'=>'post')) ?>
	  
	   <input type="hidden" name="no" id="no" value="">
	   
	   <input type="hidden" name="id" id="id" value=<?php echo $values[0]['id'] ; ?>>
	   <div id='my_div_1'>   
			<?php echo form_input('num',1,'id="num" class="form-control" maxlength="3" size="1" align="center" ' ) ?> 
           
           <span class="label label-info">No SJ</span>
           <input type="hidden" name="no_sj_wip_lama" id="no_sj_wip_lama" value="<?php echo $values[0]['no_sj'] ?>">
            <?php echo form_input('no_sj', $values[0]['no_sj'], 'id="no_sj" class="form-control" placeholder="No SJ" maxlength="10"') ?>            
            
            <span class="label label-info">Tanggal SJ</span>
			<div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_sj', $values[0]['tanggal_sj'], 'id="tanggal_sj" class="form-control" placeholder="Tanggal SJ" maxlength="10"') ?>
			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
            
           <span class="label label-info">Jenis Keluar</span>
            <?php
                   $jenis_keluar = array(
                '0' => '- Pilih -',
                '1' => 'Keluar ke Gudang',
                '2' => 'Masuk Lain-lain', 
               
            );
                    $atribut_jenis_keluar = 'class="form-control"';
            echo form_dropdown('jenis_keluar', $jenis_keluar, $values[0]['jenis_keluar'], $atribut_jenis_keluar);
            ?>

<span class="label label-info">Gudang Keluar:</span>
  <select class="form-control" id="gudang_keluar" name="gudang_keluar" >
   <?php
		foreach ($list_gudang as $lug){
			?>	
			 <option 
			 <?php if ($values[0]['id_gudang_keluar'] == $lug->id) { ?>  selected <?php } ?> value="<?php echo $values[0]['id_gudang_keluar']  ?>" >
				<?php echo $lug->nama_gudang ?> 
			 </option>
	<?php } ?>
  </select>

<span class="label label-info">Gudang Masuk :</span>
  <select class="form-control" id="gudang_masuk" name="gudang_masuk" >
   <?php
		foreach ($list_gudang as $lug){
			?>	
			 <option 
			 <?php if ($values[0]['id_gudang_masuk'] == $lug->id) { ?>  selected <?php } ?> value="<?php echo $values[0]['id_gudang_masuk']  ?>">
				<?php echo $lug->nama_gudang ?> 
			 </option>
	<?php } ?>
  </select>			



</div>
<span class="label label-info">Keterangan Header</span>
           <input type="hidden" name="keterangan_header_lama" id="keterangan_header_lama" value="<?php echo $values[0]['keterangan_header'] ?>">
            <?php echo form_input('keterangan_header', $values[0]['keterangan_header'], 'id="keterangan_header" class="form-control" placeholder="Keterangan Header" maxlength="10"') ?>      
       
        
        
        <br>

                 <div id='my_div_1'>
				  <?php 
				  $i =1 ;
         $detail_data =$values[0]['detail_data'];
         for($k=0;$k<count($detail_data);$k++){
			 ?>
				<input type="text" name="no" id="no"  class="form-control input-group-lg" value="<?php echo $i ?> " readonly="true">
				
				<input type="hidden" name="id_barang_wip_<?php echo $i ?>" id="id_barang_wip_<?php echo $i ?>" value="<?php echo $detail_data[$k]['id_barang_wip'] ?>">
                <input id="kode_barang_wip_<?php echo $i ?>" class="col-lg-3 " type="text" name="kode_barang_wip_<?php echo $i ?>"
                       title="Masukkan Kode Barang Wip"  value="<?php echo $detail_data[$k]['kode_barang_wip'] ?>"
                       placeholder="Kode Barang WIP" onclick= "javascript: openCenteredWindows('<?php echo base_url(); ?>index.php/makloon_jadi_gudang_jadi_wip/item_makloon/barang_wip/<?php echo $i ;?>');"
                       />
	
                <input id="nama_barang_wip_<?php echo $i ?>" class="col-lg-3" type="text" name="nama_barang_wip_<?php echo $i ?>"
                       title="Masukkan Nama Barang Wip"  value="<?php echo $detail_data[$k]['nama_barang_wip'] ?>"
                       placeholder="Nama Barang WIP" disabled/ >
		
                <input id="qty_<?php echo $i ?>" class="col-lg-3" type="text" name="qty_<?php echo $i ?>"
                       title="Masukkan Quantity" value="<?php echo $detail_data[$k]['qty'] ?>"
                       placeholder="Quantity"/>
                <input type="hidden" name="qty_lama_<?php echo $i ?>" id="qty_lama_<?php echo $i ?>" value="<?php echo $detail_data[$k]['qty'] ?>">       
               <input id="keterangan_detail_<?php echo $i ?>" class="col-lg-3" type="text" name="keterangan_detail_<?php echo $i ?>"
                       title="Masukkan keterangan detail" value="<?php echo $detail_data[$k]['keterangan_detail'] ?>"
                       placeholder="Keterangan detail"/>   

<?php $i++ ;
                       	 }
         
         
         ?>     
        </div>
        <br>
  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 
 <?php echo anchor('/makloon_jadi_gudang_jadi_wip/makloon_jadi_gudang_jadi_wip/view', 'Batal', 'class="btn btn-danger " role="button" '); ?> 
<?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-success', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>

<?php echo form_close() ?>
 
</div>


