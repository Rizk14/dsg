<div class="container" id="daftar-sukses">
    <p class="h2">Informasi Untuk Penginputan Makloon dari Gudang Jadi 1 ke Gudang Jadi 2 WIP</p>
    <hr>

    <div class="alert alert-success alert-dismissible" role="alert">
        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Penginputan Berhasil.
    </div>
    
    <p>Anda dapat kembali ke halaman Makloon dari Gudang Jadi 1 ke Gudang Jadi 2 WIP di <?php echo anchor('/makloon_jadi_gudang_jadi_wip/makloon_jadi_gudang_jadi_wip/view', 'Makloon jadi gudang Jadi WIP'); ?>.</p>

</div> <!-- container -->

