<!-- <script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link> -->

<script type="text/javascript" language="javascript">
	/*$(document).ready(function(){
	$("#d_do_first").datepicker();
	$("#d_do_last").datepicker();
	}); */
	
function cek_input() {
	/*var date_from= $('#d_do_first').val();
	var date_to= $('#d_do_last').val();

	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	var tgl_dari = date_from.substr(0,2);
	var bln_dari = date_from.substr(3,2);
	var thn_dari = date_from.substr(6,4);
	var dari = new Date(thn_dari, bln_dari, tgl_dari);
	
	var tgl_ke = date_to.substr(0,2);
	var bln_ke = date_to.substr(3,2);
	var thn_ke = date_to.substr(6,4);
	var ke = new Date(thn_ke, bln_ke, tgl_ke);
	
	if (dari > ke) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	} */
	var nofaktur_first= $('#nofaktur_first').val();
	var nofaktur_last= $('#nofaktur_last').val();

	if (nofaktur_first == '') {
		alert("Nomor faktur awal harus dipilih..!");
		return false;
	}
	if (nofaktur_last == '') {
		alert("Nomor faktur akhir harus dipilih..!");
		return false;
	}
	
	$('#gambarnya').show();
	$('#btnexport').attr("hidden", true);
	
}

</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat">Export File CSV Faktur Penjualan</td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form Export CSV</td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		//echo $this->pquery->form_remote_tag(array('url'=>'exportpenjualan/cform/exportcsvfakturdo','update'=>'#content','type'=>'post'));
		?>
		<?php 
			$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
			echo form_open('exportpenjualandoo/cform/exportcsvfakturdo', $attributes); ?>
		<div id="masterlpenjualanperdoform">
		
			<table width="60%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					<td>Jenis Faktur</td>
					<td>:</td>
					<td style="white-space:nowrap;">
					  <select name="jenis_faktur" id="jenis_faktur">
						<option value="1">Faktur DO</option>
						<option value="2">Faktur Non-DO</option>
						<option value="3">Faktur Bahan Baku</option>
					  </select>
					</td>
				  </tr>
				  <tr>
					<td style="white-space:nowrap;">Nomor Faktur Awal</td>
					<td>:</td>
					<td style="white-space:nowrap;">
					  <input name="nofaktur_first" type="text" id="nofaktur_first" />
					s.d 
					<input name="nofaktur_last" type="text" id="nofaktur_last" />
					</td>
				  </tr>
				  
				  <tr>
					<td width="20%">Faktur Pajak Pengganti </td>
					<td width="1%">:</td>
					<td>
					  <input type="checkbox" name="fpengganti" id="fpengganti" value="y">
					</td>
				  </tr>
				  
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>			  			  
			  <tr>
				<td>
				<input name="btnexport" type="submit" id="btnexport" value="Export ke CSV" onclick="return cek_input();" />&nbsp;<img id="gambarnya" src="<?php echo base_url();?>asset/images/loading.gif" style="display:none">
				<input name="btnbatal" type="reset" id="btnbatal" value="<?php echo $button_batal; ?>" />
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
