<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_opvsdo; ?> Gudang</td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_opvsdo; ?></td></tr>
   <tr>
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php
		echo $this->pquery->form_remote_tag(array('url'=>'expoopvsdogudang/cform/','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlopvsdoform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td><?php echo $list_opvsdo_kd_brg; ?></td>
							<td>:</td>
							<td>
							  <input name="i_product" type="text" id="i_product" maxlength="100" value="<?=$kproduksi?>" />
							</td>
						  </tr>
						  <tr>
							<td width="19%"><?php echo $list_opvsdo_tgl_mulai_op; ?> </td>
							<td width="0%">&nbsp;</td>
							<td width="81%">
							  <input name="d_op_first" type="text" id="d_op_first" maxlength="10" value="<?=$tglopmulai?>"/>
							  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_first,'dd/mm/yyyy',this)">
							s.d 
							<input name="d_op_last" type="text" id="d_op_last" maxlength="10" value="<?=$tglopakhir?>" />
							<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_last,'dd/mm/yyyy',this)">
							</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							  <input name="f_stop_produksi" type="checkbox" id="f_stop_produksi" value="1" <?=$stop?> />
							<?php echo $list_opvsdo_stop_produk; ?></td>
						  </tr>
						  
						  <tr>
							<td>Pelanggan</td>
							<td></td>
							<td>
							<select name="customer" id="customer">
							 <option value="">[ PILIH PELANGGAN ]</option>
							 <?php
							 foreach($customer as $rowcustomer) {
								 $Sel = ($icustomer==$rowcustomer->i_customer)?("selected"):("");
								 echo "<option value=\"$rowcustomer->i_customer\" $Sel >".$rowcustomer->e_customer_name."</option>";
							 }
							 ?>
							</select>
							</td>
						  </tr>

						  <tr>
							<td>OP Drop Forcast</td>
							<td></td>
							<td>
							<select name="fdropforcast" id="fdropforcast">
							 <option value="0" <?php if($fdropforcast=='0') { echo "selected"; } ?> >Tampilan Semua</option>
							 <option value="1" <?php if($fdropforcast=='1') { echo "selected"; } ?> >Ya</option>
							 <option value="2" <?php if($fdropforcast=='2') { echo "selected"; } ?> >Bukan</option>
							</select>
							</td>
						  </tr>
						  						  
						</table>
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td>
			  	<div id="title-box2"><?php echo $form_title_detail_opvsdo; ?></div>
			  	</td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="2%" class="tdatahead">NO</td>
					<td width="3%" align="center" class="tdatahead"><?php echo strtoupper($list_opvsdo_stop_produk); ?> </td>
					<td width="10%" align="center" class="tdatahead"><?php echo strtoupper($list_opvsdo_kd_brg); ?> </td>
					<td width="40%" align="center" class="tdatahead"><?php echo $list_opvsdo_nm_brg; ?> </td>
				<!--	<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_unit_price; ?> </td> -->
					<td width="10%" align="center" class="tdatahead"><?php echo $list_opvsdo_op; ?></td>
				<!--	<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_n_op; ?> </td> -->
					<td width="10%" align="center" class="tdatahead"><?php echo $list_opvsdo_do; ?></td>
				<!--	<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_n_do; ?> </td> -->
					<td width="10%" align="center" class="tdatahead"><?php echo $list_opvsdo_selisih_opdo; ?> </td>
				<!--	<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_n_selisih; ?></td> -->
				  </tr>
				  
				  <?php
				  
				  $totalpermintaan	= 0;
				//  $nilaipermintaan	= 0;
				  $totalpengiriman	= 0;
				//  $nilaipenjualan	= 0;
				  $selisihtotal	= 0;
				//  $selisihnilai	= 0;
				//  $totalqtyfaktur	= 0;
				  
				  	$no	= 1;
					$cc	= 1;
				  
				  if (is_array($query)) {
				  for($j=0;$j<count($query);$j++){
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$sproduct	= $query[$j]['stopproduct']=='t'?"checked":"";
						
						if ($query[$j]['jmlorder']!=0) {
							$lopvsdo	.= "
								  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
									onMouseOut=\"this.className='$Classnya'\">
									<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
									<td><input type=\"checkbox\" value=\"1\" $sproduct></td>
									<td>".$query[$j]['imotif']."</td>
									<td>".$query[$j]['productmotif']."</td>
									<td align=\"right\">".$query[$j]['jmlorder']."</td>
									<td align=\"right\">".$query[$j]['pemenuhan']."</td>
									<td align=\"right\">".$query[$j]['selisihopdo']."</td>";
							$lopvsdo.="</tr>";
							
							$no+=1; $cc+=1;
							
							$totalpermintaan+=$query[$j]['jmlorder'];
						//	$nilaipermintaan+=$query[$j]['nilaiorder'];
							$totalpengiriman+=$query[$j]['pemenuhan'];
						//	$nilaipenjualan+=$query[$j]['nilaipemenuhan'];
						//	$totalqtyfaktur+= $query[$j]['qty_faktur'];
						} // end if jmlorder!=0
				  }
				}
				
				// ----------------- end 10-12-2012 ---------------------------	
					
					// 13-08-2012
					if (is_array($isixx)) {
						for($j=0;$j<count($isixx);$j++){
					
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

						$sproduct	= $isixx[$j]['stopproduct']=='t'?"checked":"";
						if ($isixx[$j]['jmlorder']!=0) {
							$lopvsdo	.= "
								  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
									onMouseOut=\"this.className='$Classnya'\">
									<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
									<td><input type=\"checkbox\" value=\"1\" $sproduct></td>
									<td>".$isixx[$j]['imotif']."</td>
									<td>".$isixx[$j]['productmotif']."</td>
									<td align=\"right\">".$isixx[$j]['jmlorder']."</td>
									<td align=\"right\">".$isixx[$j]['pemenuhan']."</td>
									<td align=\"right\">".$isixx[$j]['selisihopdo']."</td>";
							$lopvsdo.="</tr>";
							
							$no+=1; $cc+=1;
							
							$totalpermintaan+=$isixx[$j]['jmlorder'];
						//	$nilaipermintaan+=$isixx[$j]['nilaiorder'];
							$totalpengiriman+=$isixx[$j]['pemenuhan'];
						//	$nilaipenjualan+=$isixx[$j]['nilaipemenuhan'];
						//	$totalqtyfaktur+= $isixx[$j]['qty_faktur'];
						} // end if jmlorder !=0
					}
				}
				  	
					$selisihtotal	= ($totalpermintaan - $totalpengiriman);
				//	$selisihnilai	= ($nilaipermintaan - $nilaipenjualan);
					
					echo $lopvsdo;
				  
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>	  
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="15%"><?php echo $list_opvsdo_t_permintaan; ?> </td>
							<td width="1%">:</td>
							<td width="35%">
							  <input name="t_permintaan" type="text" id="t_permintaan" maxlength="20" value="<?php echo $totalpermintaan; ?>"/>
							</td>
							<td width="15%"><?php echo $list_opvsdo_t_pengiriman; ?> </td>
							<td width="1%">:</td>
							<td width="%">
							  <input name="t_pengiriman" type="text" id="t_pengiriman" maxlength="20" value="<?php echo $totalpengiriman; ?>"/>
							</td>
						  </tr>
						  <tr>
							<td><?php echo $list_opvsdo_selisih; ?></td>
							<td>:</td>
							<td>
							  <input name="selisih" type="text" id="selisih" maxlength="20" value="<?php echo $selisihtotal; ?>"/>
							</td>
						  </tr>
						</table>
				</td>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	<td align="right">
					 <input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/expoopvsdogudang/cform/'">

				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
