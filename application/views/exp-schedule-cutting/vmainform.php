<h3>Realisasi Cutting Per-Operator</h3><br>

<?php if ($msg!='') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if($go_proses==''){
$attributes = array('name'=>'f_op', 'id'=>'f_op');
echo form_open('exp-schedule-cutting/cform/', $attributes); ?>
<table>
	<tr>
		<td>Tgl. Schedule</td>
		<td> : <input type="text" name="tgl" id="tgl" value="" size="10" maxlength="10" readonly="true" />&nbsp;<img alt="" id="tgl_sc" align="middle" title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif" onclick="displayCalendar(document.forms[0].tgl,'dd-mm-yyyy',this)">
		</td>
	</tr>
</table><br>
<input type="submit" name="submit" value="Proses" />
<?php echo form_close(); ?>
<?php }else{ ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/exp-schedule-cutting/cform/export/" method="post" enctype="multipart/form-data">

<div align="center">
<label id="status"></label>
<table class="proit-view" width="100%" cellspacing="2" cellpadding="1" border="0">
	<tr>
		<td width="140px">Tgl. Schedule</td>
		<td> : <input type="text" name="tglrealisasi" id="tglrealisasi" maxlength="10" size="10" readonly="true" value="<?=$tgl?>" /></td>
  	</tr>
  	<tr>
		<td colspan="2"><br>
		<form name="myform">
		<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
			<tr>
			  <th width="20">No</th>
			  <th width="10">Operator</th>
			  <th width="135">No. Realisasi</th>
			  <th width="135">Kode Brg</th>
			  <th width="145">Kode Brg Jadi</th>
			  <th width="70">Hasil</th>
			  <th width="70">JK</th>
			  <!-- <th width="200">Keterangan</th> -->
			</tr>
			<?php
			$i=0;
			for($j=0;$j<count($detail);$j++){
			?>
			<tr align="center">
			  <td valign="top" align="center" id="num_<?php echo $i ?>"><?php echo $i+1 ?></td>
			  <td nowrap="nowrap" valign="top"><input name="operator_<?php echo $i ?>" type="text" id="operator_<?php echo $i ?>" size="10" readonly="true" value="<?=$detail[$j]['operator_cutting']?>"/></td>
			  <td  colspan="6">
			  <?php
			  $detail_realisasi = $detail[$j]['detail'];
			  for($jj=0;$jj<count($detail_realisasi); $jj++){
			  ?>	
			  <table>
			  	<tr>
				  <td><input name="realisasi_<?php echo $i ?>" type="text" id="realisasi_<?php echo $i ?>" size="16" readonly="true" value="<?=$detail_realisasi[$jj]['no_realisasi']?>" /></td>
				  <td><input name="kodebrg_<?php echo $i ?>" type="text" id="kodebrg_<?php echo $i ?>" size="17" readonly="true" value="<?=$detail_realisasi[$jj]['kode_brg']?>" /></td>
				  <td><input name="kodebrg_jd_<?php echo $i ?>" type="text" id="kodebrg_jd_<?php echo $i ?>" size="18" readonly="true" value="<?=$detail_realisasi[$jj]['kode_brg_jadi']?>" /></td>
				  <td><input name="hsil_<?php echo $i ?>" type="text" id="hsil_<?php echo $i ?>" size="8" readonly="true" value="<?=$detail_realisasi[$jj]['hasil']?>" /></td>
				  <td><input name="jk_<?php echo $i ?>" type="text" id="jk_<?php echo $i ?>" size="8" readonly="true" value="<?=$detail_realisasi[$jj]['jk']?>" /></td>
				  <!-- <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="25" readonly="true" value="<?=$detail_realisasi[$jj]['keterangan']?>" /></td> -->
				</tr>
			  </table>	
			  <?php
				}
			  ?>	
			  </td>
			</tr>
			<?php $i++; }
			?>
			<tr>
			   <td colspan="8" align="right"><input type="submit" name="submit" value="Export" >&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/exp-schedule-cutting/cform/'"></td>
			</tr>
		</table>	
		  </form>
		  </td>
	</tr>
</table>
</div>
</form>
<?php } ?>
