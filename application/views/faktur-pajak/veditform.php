<h3>Data Faktur Pajak</h3><br>
<a href="<?php echo base_url(); ?>index.php/faktur-pajak/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/faktur-pajak/cform/view">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	get_detail_supplier();
	$('#pilih_faktur').click(function(){
		var id_sup= jQuery('#supplier').val();
		var urlnya = "<?php echo base_url(); ?>index.php/faktur-pajak/cform/show_popup_faktur_pembelian/E/"+ id_sup;
		openCenteredWindow(urlnya);
	  });
	
	$('#kode_supplier').change(function(){
	  	    get_detail_supplier();
	  });
	  
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_faktur() {
	var no_fp= $('#no_fp').val();
	var tgl_fp= $('#tgl_fp').val();
	var no_faktur= $('#no_faktur').val();
	var jum= $('#jum').val();
	if (no_faktur == '') {
		alert("Faktur harus dipilih..!");
		$('#no_faktur').focus();
		return false;
	}
	if (no_fp == '') {
		alert("Faktur Pajak harus diisi..!");
		$('#no_fp').focus();
		return false;
	}
	if (jum == '') {
		$('#jum').focus();
		alert("Jumlah pajak harus diisi..!");
		return false;
	}	
}

function get_detail_supplier() {
	var id_sup= $('#id_supplier').val();
    $.getJSON("<?php echo base_url(); ?>index.php/faktur-pajak/cform/get_detail_supplier/"+id_sup, function(data) {

		$(data).each(function(index, item) {
			if (item.pkp == 't')
				var pkp = "Ya";
			else
				var pkp = "Tidak";
			
			if (item.tipe_pajak == 'I')
				var tipe_pajak = "Include";
			else
				var tipe_pajak = "Exclude";
			
			if (item.npwp == null)
				var npwp = '-';
			else
				var npwp = item.npwp;
			
			if (item.nama_npwp == null)
				var nama_npwp = '-';
			else
				var nama_npwp = item.nama_npwp;
			
			$("#topnya").html("No NPWP: "+npwp+"<br>Nama NPWP: "+nama_npwp+"<br>PKP: "+pkp+"<br>Tipe Pajak: "+tipe_pajak+"<br> T.O.P : "+item.top+" Hari");
			$("#topnya").show();
				
        });
    });
    
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-pajak/cform/updatedata" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_pajak" value="<?php echo $query[0]['id'] ?>">
<input type="hidden" name="no_faktur_pajak_lama" value="<?php echo $query[0]['no_faktur_pajak'] ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="csupplier" value="<?php echo $csupplier ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<br>Edit Data<br><br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
	<td valign="top">Supplier </td>
	<td><select name="id_supplier" id="id_supplier" onkeyup="this.blur();this.focus();" disabled="true">
				
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($sup->id == $query[0]['id_supplier']) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select><br><span id="topnya"></span>
	<input type="hidden" name="supplier" id="supplier" value="<?php echo $query[0]['id_supplier'] ?>">			
	</td>
</tr>

  <tr>
		<td width="15%">Nomor Faktur Pembelian</td>
		<td width="70%"> <input type="text" name="no_faktur" id="no_faktur" value="<?php echo $query[0]['no_faktur'] ?>" size="40" maxlength="40" readonly="true">
		<input type="hidden" name="is_quilting" id="is_quilting" value="<?php echo $query[0]['is_makloon'] ?>">
		<input type="hidden" name="id_faktur" id="id_faktur" value="<?php echo $query[0]['id_faktur'] ?>">
		&nbsp;
		<input type="hidden" name="no_faktur_lama" id="no_faktur_lama" value="<?php echo $query[0]['no_faktur'] ?>">
		<input type="hidden" name="is_quilting_lama" id="is_quilting_lama" value="<?php echo $query[0]['is_makloon'] ?>">
		<input type="hidden" name="id_faktur_lama" id="id_faktur_lama" value="<?php echo $query[0]['id_faktur'] ?>">
		<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data faktur">
		</td>
	</tr>
  <tr>
    <td>Nomor Faktur Pajak</td>
    <td>
      <input name="no_fp" style="margin-bottom: 5px;" type="text" id="no_fp" size="20" maxlength="20" value="<?php echo $query[0]['no_faktur_pajak'] ?>">
    </td>
  </tr>
  <tr>
    <td>Tanggal Faktur Pajak</td>
    <td>
	<label>
      <input name="tgl_fp" type="text" id="tgl_fp" size="10" value="<?php echo $query[0]['tgl_faktur_pajak'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_fp" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_fp,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td>Dasar Pengenaan Pajak (DPP)</td>
    <td style="margin-bottom: 3px;">
      <input name="dpp2" type="text" style="margin-top: 3px;" id="dpp2" size="10" maxlength="10" value="<?php echo $query[0]['dpp'] ?>" readonly="true" style="text-align:right;">
      <input name="dpp" type="hidden"style="margin-bottom: 3px;" id="dpp" value="<?php echo $query[0]['dpp'] ?>">
    </td><br>
  </tr>
  <tr>
    <td>Jumlah PPN</td>
    <td>
      <input name="jum2" style="margin-top: 3px;" type="text" id="jum2" size="10" maxlength="10" value="<?php echo $query[0]['jumlah'] ?>" readonly="true" style="text-align:right;">
      <input name="jum" type="hidden" style="margin-top: 3px;" id="jum" value="<?php echo $query[0]['jumlah'] ?>">
    </td>
  </tr>
		<?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "faktur-pajak/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "faktur-pajak/cform/cari/".$csupplier."/".$carinya."/".$cur_page;
        ?>
  
  <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td><input type="submit" style="margin-top: 5px;" name="submit" value="Edit" onclick="return cek_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
  </tr>

</table>
</form>
