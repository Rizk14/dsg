<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Faktur Pajak</h3><br>
<a href="<?php echo base_url(); ?>index.php/faktur-pajak/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/faktur-pajak/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('faktur-pajak/cform/cari'); ?>
Supplier <select name="supplier" id="supplier">
				<option value="0" <?php if ($csupplier == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($csupplier == $sup->id) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		 <th>No/Tgl Faktur Pajak</th>
		 <th>No/Tgl Faktur Pembelian</th>
		 <th>Supplier</th>
		 <th>Jum PPN (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				$pisah1 = explode("-", $query[$j]['tgl_faktur_pajak']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_faktur_pajak = $tgl1." ".$nama_bln." ".$thn1; 
				 
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>".$query[$j]['no_faktur_pajak']." / ".$tgl_faktur_pajak."</td>";
				 echo "<td nowrap>";
				  if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_faktur']." / ".$var_detail[$k]['tgl_faktur'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jumlah'], 2, ',','.')."</td>";
				 echo    "<td>".$tgl_update."</td>";
				 echo    "<td align=center><a href=".base_url()."index.php/faktur-pajak/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>";
				 
				 //if ($this->session->userdata('gid') == 1)
					echo    "&nbsp; <a href=".base_url()."index.php/faktur-pajak/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 echo "</td>";
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
