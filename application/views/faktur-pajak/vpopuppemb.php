<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    .judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
// 24-07-2013
function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

$(function()
{
	$("#pilih").click(function()
	{
	
		opener.document.forms["f_purchase"].no_faktur.value = '';
		opener.document.forms["f_purchase"].id_faktur.value = '';
		opener.document.forms["f_purchase"].is_quilting.value = '';
		var jumlah = 0;
		var jum_dpp = 0;
		if (document.f_master_brg.faktur.length > 0) {
			for(var i=0; i < document.f_master_brg.faktur.length; i++){
				if(document.f_master_brg.faktur[i].checked) {
					//total +=document.form1.scripts[i].value + "\n"
					opener.document.forms["f_purchase"].no_faktur.value+= document.f_master_brg.faktur[i].value + ",";
					opener.document.forms["f_purchase"].id_faktur.value+= document.f_master_brg.id_faktur[i].value + ";";
					opener.document.forms["f_purchase"].is_quilting.value+= document.f_master_brg.is_makloon[i].value + ",";
					jumlah = parseFloat(jumlah) + parseFloat(document.f_master_brg.pajak[i].value); 
					jum_dpp = parseFloat(jum_dpp) + parseFloat(document.f_master_brg.dpp[i].value); 
				}
			}
		}
		else {
			opener.document.forms["f_purchase"].no_faktur.value = document.f_master_brg.faktur.value+",";
			opener.document.forms["f_purchase"].id_faktur.value = document.f_master_brg.id_faktur.value+";";
			opener.document.forms["f_purchase"].is_quilting.value = document.f_master_brg.is_makloon.value+",";
			jumlah = parseFloat(jumlah) + parseFloat(document.f_master_brg.pajak.value); 
			jum_dpp = parseFloat(jum_dpp) + parseFloat(document.f_master_brg.dpp.value); 
		}
		opener.document.forms["f_purchase"].jum.value = parseInt(jumlah);
		opener.document.forms["f_purchase"].dpp.value = parseInt(jum_dpp);
		
		// 24-07-2013
		var hitungformat1 = formatMoney(jumlah, 2,',','.');
		var hitungformat2 = formatMoney(jum_dpp, 2,',','.');
		opener.document.forms["f_purchase"].jum2.value = hitungformat1;
		opener.document.forms["f_purchase"].dpp2.value = hitungformat2;
		
		self.close();
	});
});
</script>

<center><h3>Daftar Faktur Pembelian</h3></center>
<div align="center"><br>
<?php echo form_open('faktur-pajak/cform/show_popup_faktur_pembelian'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>">
<input type="hidden" name="supplier" value="<?php echo $csupplier ?>">
<input type="hidden" name="jnsaction" value="<?php echo $jnsaction ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="no_faktur" id="no_faktur">

	<table border="1" width="100%" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" nowrap="nowrap">
	<thead>
	 <tr class="judulnya">
		 <th>No/Tgl Faktur </th>
		 <th>Supplier</th>
		 <th>PPN (Rp.)</th>
		 <th>Total Pembelian (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				$pisah1 = explode("-", $query[$j]['tgl_faktur']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_faktur']." / ".$tgl_faktur."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['pajaknya'], 2, ',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['totalnya'], 2, ',','.')."</td>";

				 echo    "<td>".$tgl_update."</td>"; ?>
				 <td align=center><input type="hidden" name="pajak" id="pajak" value="<?php echo $query[$j]['pajaknya'] ?>">
				 <input type="hidden" name="dpp" id="dpp" value="<?php echo $query[$j]['dpp'] ?>">
				 <input type="hidden" name="is_makloon" id="is_makloon" value="<?php echo $query[$j]['is_makloon'] ?>">
				 <input type="hidden" name="id_faktur" id="id_faktur" value="<?php echo $query[$j]['id'] ?>">
				 <input type="checkbox" name="faktur" id="faktur" value="<?php echo $query[$j]['no_faktur'] ?>"></td>
				<?php echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table>
</form>
  <?php echo $this->pagination->create_links();?>
  <input type="button" name="pilih" id="pilih" value="Pilih" >
</div>
