<h3>Data Faktur Pajak</h3><br>
<a href="<? echo base_url(); ?>index.php/faktur-pajak/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/faktur-pajak/cform/view">View Data</a><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	get_detail_supplier();
	$('#pilih_faktur').click(function(){
		var id_sup= jQuery('#supplier').val();
		var urlnya = "<?php echo base_url(); ?>index.php/faktur-pajak/cform/show_popup_faktur_pembelian/A/"+ id_sup;
		openCenteredWindow(urlnya);
	  });
	  
	  $('#supplier').change(function(){
	  	    get_detail_supplier();
	  	     $("#no_faktur").val('');	
	  	     $("#is_quilting").val('');	
	  	     $("#dpp").val('0');	
	  	     $("#jum").val('0');	
	  });
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_faktur() {
	var no_fp= $('#no_fp').val();
	var tgl_fp= $('#tgl_fp').val();
	var no_faktur= $('#no_faktur').val();
	var jum= $('#jum').val();
	var dpp= $('#dpp').val();
	if (no_faktur == '') {
		alert("Faktur harus dipilih..!");
		$('#no_faktur').focus();
		return false;
	}
	if (no_fp == '') {
		alert("Faktur Pajak harus diisi..!");
		$('#no_fp').focus();
		return false;
	}
	if (dpp == '0' || dpp == '') {
		$('#dpp').focus();
		alert("DPP tidak boleh 0 dan harus diisi..!");
		return false;
	}	
	if (jum == '0' || jum == '') {
		$('#jum').focus();
		alert("Jumlah pajak tidak boleh 0 dan harus diisi..!");
		return false;
	}	
}


function get_detail_supplier() {
	var kode_sup= $('#supplier').val();
    $.getJSON("<?php echo base_url(); ?>index.php/faktur-pajak/cform/get_detail_supplier/"+kode_sup, function(data) {

		$(data).each(function(index, item) {
			if (item.pkp == 't')
				var pkp = "Ya";
			else
				var pkp = "Tidak";
			
			if (item.tipe_pajak == 'I')
				var tipe_pajak = "Include";
			else
				var tipe_pajak = "Exclude";
			
			if (item.npwp == null)
				var npwp = '-';
			else
				var npwp = item.npwp;
			
			if (item.nama_npwp == null)
				var nama_npwp = '-';
			else
				var nama_npwp = item.nama_npwp;
			
			$("#topnya").html("No NPWP: "+npwp+"<br>Nama NPWP: "+nama_npwp+"<br>PKP: "+pkp+"<br>Tipe Pajak: "+tipe_pajak+"<br> T.O.P : "+item.top+" Hari");
			$("#topnya").show();
				
        });
    });
    
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-pajak/cform/submit" method="post" enctype="multipart/form-data">

<br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
<tr>
	<td valign="top">Supplier </td>
	<td><select name="supplier" id="supplier" onkeyup="this.blur();this.focus();">
				
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select><br>
				<span id="topnya"></span>
				</td>
</tr>

  <tr>
		<td width="15%">Nomor Faktur Pembelian</td>
		<td width="70%"> <input type="text" name="no_faktur" id="no_faktur" value="" size="40" readonly="true">
		<input type="hidden" name="is_quilting" id="is_quilting" value="">
		&nbsp;
		<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data faktur">
		</td>
	</tr>
  <tr>
    <td>Nomor Faktur Pajak</td>
    <td>
      <input name="no_fp" type="text" id="no_fp" size="20" maxlength="20" value="">
    </td>
  </tr>
  <tr>
    <td>Tanggal Faktur Pajak</td>
    <td>
	<label>
      <input name="tgl_fp" type="text" id="tgl_fp" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_fp" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_fp,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td>Dasar Pengenaan Pajak (DPP)</td>
    <td>
      <input name="dpp" type="text" id="dpp" size="10" maxlength="10" value="0" readonly="true">
    </td>
  </tr>
  <tr>
    <td>Jumlah PPN</td>
    <td>
      <input name="jum" type="text" id="jum" size="10" maxlength="10" value="0" readonly="true">
    </td>
  </tr>
  <tr><td>&nbsp;</td>
	<td><input type="submit" name="submit" value="Simpan" onclick="return cek_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-pajak/cform/view'"></td>
  </tr>

</table>
</form>
