<!--<style type="text/css" media="print">
	@page 
        {
            size: auto;   /* auto is the current printer page size */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
</style> -->

<style type="text/css">
	@page {
		size: US Letter;
		margin: 0.29in 0.29in 0.29in 0.70in;
	}

	.isinya {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;

	}

	.kepala {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 19px;
	}

	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.kepala2 {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.kotak {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 11px;
		border-collapse: collapse;
		border: 1px solid black;
	}

	.tabelheader {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.subtotal {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 14px;
	}
</style>

<?php include_once("funcs/terbilang.php"); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.3.2.js"></script>
<!-- <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script> -->
<script type="text/javascript">
	$(function() {
		//printpr();
		//window.print();
	});

	window.onafterprint = function() {
		var faktur = <?php echo $nomorfaktur ?>;
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('khususprntfpenjualan/cform/update'); ?>",
			data: "faktur=" + faktur,
			success: function(data) {
				opener.window.refreshview();
				setTimeout(window.close, 0);
			},
			error: function(XMLHttpRequest) {
				alert('fail');
			}
		});
	}
</script>

<table border="0" width="730px" class="tabelheader">
	<tr>
		<td align="left" colspan="2"> ================================================================================================ </td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<div class="kepala">&nbsp;&nbsp;<b>Faktur Penjualan </b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<b class="kepalaxx"><?php echo $nminitial ?></b>
			</div>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2"> ================================================================================================ </td>
	</tr>

	<tr class="kepala2">
		<td>Nomor Faktur : <b><?php echo $nomorfaktur ?> </b></td>
		<td align="right" style="padding-right: 4px">Tanggal Faktur : <b><?php echo $tglfaktur ?></b></td>
	</tr>
	<!-- <tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr> -->
	<tr>
		<td>Kepada :</td>
		<td rowspan="3" align="right" style="padding-right: 4px">
			<?php foreach ($iopdo as $opdonya) { ?>
				No DO: <b><?php echo $opdonya->idocode ?></b> &nbsp;&nbsp; No OP: <b><?php echo $opdonya->iopcode ?></b> <br>
			<?php } ?>
		</td>
	</tr>
	<tr>
		<td><b><?php echo $nmcabang ?></b></td>

	</tr>
	<tr>
		<td><b><?php echo $alamatcabang  ?></b></td>

	</tr>
</table>

<table border="0" width="730px" class="isinya" cellspacing="0">
	<thead>
		<tr>
			<td colspan="6">
				<hr>
			</td>
			<!--<td colspan="6" class="garisbawah">&nbsp;</td>-->
		</tr>
		<tr class="kepala2">
			<th width="3%">No</th>
			<th width="10%" style="white-space:nowrap;">Kode Barang</th>
			<th width="30%" style="white-space:nowrap;">Nama Barang</th>
			<th width="4%">Unit</th>
			<th width="8%" align="right">Harga</th>
			<th width="10%" align="right">Jumlah</th>
		</tr>
		<tr>
			<td colspan="6">
				<hr>
			</td>
			<!--<td colspan="6" class="garisbawah">&nbsp;</td>-->
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		$lup	= 0;
		$grandtotal = 0;

		foreach ($isi as $row) {
			if ($lup < 22) {

				$qvalue	= $this->mclass->jmlitemharga($row->i_faktur, $row->imotif);

				if ($qvalue->num_rows() > 0) {
					$row_value	= $qvalue->row();
					$jmlqty[$lup]	= $row_value->qty;
					$uprice[$lup]	= $row_value->unitprice;
					$tprice[$lup]	= $row_value->amount;
				} else {
					$jmlqty[$lup]	= 0;
					$uprice[$lup]	= 0;
					$tprice[$lup]	= 0;
				}
				echo "<tr class='detailbrg'>
					<td align='center'>$no</td>
					<td>&nbsp;$row->imotif</td>
					<td style='white-space:nowrap;'>&nbsp;$row->motifname</td>
					<td align='right'>" . number_format($jmlqty[$lup]) . "&nbsp;</td>
					<td align='right'>" . number_format($uprice[$lup]) . "&nbsp;</td>
					<td align='right'>" . number_format($tprice[$lup]) . "&nbsp;</td>
			</tr>";
				$grandtotal += $tprice[$lup];
				$no++;
				$lup++;
			}
		}
		?>
		<tr>
			<td colspan="6">
				<hr>
			</td>
		</tr>
	</tbody>
</table>

<table width="730px" border="0" class="kepala2">
	<tr class="detailbrg2">
		<td colspan='3'>&nbsp;</td>

		<td colspan="1" valign="middle" align="right">Jumlah Rp</td>
		<!--<td width="20%">Rp. </td>-->
		<td align="right"><b><?php echo number_format($grandtotal) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td colspan="2" style="white-space:nowrap;">Tgl Jatuh Tempo : <b><?php echo $tgljthtempo ?></b></td>
		<td colspan="3">&nbsp;</td>

	</tr>
	<tr>
		<td rowspan="7" colspan="3">
			<table border="2" width="100%" class="kotak">
				<tr>
					<td style="white-space:nowrap;">&nbsp;1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan, <br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kecuali ada perjanjian terlebih dahulu<br>
						&nbsp;2. Faktur asli merupakan bukti pembayaran yg sah<br>
						&nbsp;3. Pembayaran dgn cek/giro baru dianggap sah setelah diuangkan
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="1">Diskon Rp.
			<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp; -->
		</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($diskon) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td>&nbsp;</td>
		<td colspan="1">
			<hr>
		</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="1">DPP Rp.</td>
		<td align="right"><b><?php echo number_format($dpp) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="1">PPN Rp.</td>
		<td align="right"><b><?php echo number_format($nilai_ppn) ?></b>&nbsp;</td>
	</tr>

	<!-- MATERAI -->
	<? if ($nilai_faktur > 4999999) { ?>
		<tr class="detailbrg2">
			<!-- <td align="right" colspan="1">BEA METERAI Rp.</td>
		<td align="right"><b><?php echo number_format($materai) ?></b>&nbsp;</td> -->
		</tr>
		<tr class="detailbrg2">
			<td>&nbsp;</td>
			<td colspan="1">
				<hr>
			</td>
		</tr>
	<? } else { ?>
		<tr class="detailbrg2">
			<td>&nbsp;</td>
			<td colspan="1"></td>
			<td colspan="1">
				<hr>
			</td>
		</tr>
	<? } ?>
	<!-- ----------------------------------------------------- -->

	<? if ($nilai_faktur > 4999999) { ?>
		<tr class="detailbrg2">
			<td align="right" colspan="4">Nilai Faktur Rp.</td>
			<!--<td>Rp.</td>-->
			<td align="right"><b><?php echo number_format($nilai_faktur) ?></b>&nbsp;</td>
		</tr>
	<? } else { ?>
		<tr class="detailbrg2">
			<td align="right" colspan="1">Nilai Faktur Rp.</td>
			<!--<td>Rp.</td>-->
			<td align="right"><b><?php echo number_format($nilai_faktur) ?></b>&nbsp;</td>
		</tr>
	<? } ?>
	<!-- <tr class="detailbrg2">
		<td>&nbsp;</td>
		<td colspan="2"><hr></td>
	</tr> -->
	<tr class="detailbrg">
		<!--
		<td colspan="5" align="center" style="white-space:nowrap;" ><?php $terbil = &terbilangxx($nilai_faktur, 3); ?>(Terbilang: <?php echo $terbil ?> Rupiah)</td> -->
		<td colspan="5" align="center"><?php $terbil = &terbilangxx($nilai_faktur, 3); ?>(Terbilang: <?php echo $terbil ?> Rupiah)</td>
	</tr>
</table><!-- <br> -->
<div class="kepala2" style="margin-bottom: 0;"><b>SE & O<br><br><br><br><br><br><br><!-- <br> -->

		Adm Penjualan</b></div>
<!--
Adm Keuangan</b></div>
-->