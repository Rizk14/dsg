<!--<style type="text/css" media="print">
	@page 
        {
            size: auto;   /* auto is the current printer page size */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
</style> -->

<style type="text/css">

/*@media print {
	div {
		color: #f00;
	}
} */

/*yang ini buat setting ukuran kertasnya assumsi A4/letter, sementara ga dipake */
/*#Letter {background-color:#FFFFFF;
left:1px;
right:1px;
height:11in ; */ /*Ukuran Panjang Kertas */
/*width: 8.50in; */ /*Ukuran Lebar Kertas */
/*margin:1px solid #FFFFFF; */
 
/* font-family:Georgia, "Times New Roman", Times, serif; } */

   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    
    }
    
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 18px;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
	}
	.kepala2 {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
	}
	.detailbrg {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 10px;
	}
	.kotak {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 10px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 12px;}
    
    .subtotal {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
</style>

<?php include_once ("funcs/terbilang.php"); ?>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	//window.print();
});

</script>

<table border="0" width="650px" class="tabelheader">
	<tr>
		<td align="left" colspan="2"> ===================================================================================================== </td>
	</tr>
	<tr>
		<td align="left" colspan="2"> <div class="kepala">&nbsp;<b>Faktur Penjualan</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b class="kepalaxx"><?php echo $nminitial ?></b> </div></td>
	</tr>	
	<tr>
		<td align="left" colspan="2"> ===================================================================================================== </td>
	</tr>

	<tr class="kepala2">
		<td>Nomor Faktur : <b><?php echo $nomorfaktur ?> </b></td>
		<td align="right">Tanggal Faktur : <b><?php echo $tglfaktur ?></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Kepada :</td>
		<td rowspan="3" align="right">
		<?php foreach($iopdo as $opdonya) { ?>
			No DO: <b><?php echo $opdonya->idocode ?></b> &nbsp;&nbsp;&nbsp; No OP: <b><?php echo $opdonya->iopcode ?></b> <br>
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td><b><?php echo $nmcabang ?></b></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><b><?php echo $alamatcabang  ?></b></td>
		<td>&nbsp;</td>
	</tr>
</table>

<table border="0" width="680px" class="isinya">
	<thead>
	<tr>
		<td colspan="6"><hr></td>
	</tr>
	 <tr class="kepala2">
		<th width="3%">No</th>
		<th width="10%" style="white-space:nowrap;">Kode Barang</th>
		<th width="30%" style="white-space:nowrap;">Nama Barang</th>
		<th width="4%">Unit</th>
		<th width="8%" align="right">Harga</th>
		<th width="10%" align="right">Jumlah</th>
	 </tr>
	 <tr>
		<td colspan="6"><hr></td>
	</tr>
	</thead>
	<tbody>
		<?php $no=1; $lup	= 0;
				$grandtotal = 0;
				
	foreach($isi as $row) {
		if($lup<22) {		
			
			$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
			
			if($qvalue->num_rows()>0) {
				$row_value	= $qvalue->row();
				$jmlqty[$lup]	= $row_value->qty;
				$uprice[$lup]	= $row_value->unitprice;
				$tprice[$lup]	= $row_value->amount;
			}else{
				$jmlqty[$lup]	= 0;
				$uprice[$lup]	= 0;
				$tprice[$lup]	= 0;		
			}
			echo "<tr class='detailbrg'>
					<td align='center'>$no</td>
					<td>&nbsp;$row->imotif</td>
					<td style='white-space:nowrap;'>&nbsp;$row->motifname</td>
					<td align='right'>".number_format($jmlqty[$lup])."&nbsp;</td>
					<td align='right'>".number_format($uprice[$lup])."&nbsp;</td>
					<td align='right'>".number_format($tprice[$lup])."&nbsp;</td>
			</tr>";
			$grandtotal+=$tprice[$lup];
			$no++; $lup++;
		}
	}
	?>
	<tr>
		<td colspan="6"><hr></td>
	</tr>
	</tbody>
</table>

<table width="680px" border="0" class="kepala2">
	<tr class="detailbrg">
		<td width="3%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%" colspan="2" align="right">Jumlah Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<!--<td width="20%">Rp. </td>-->
		<td align="right"><b><?php echo number_format($grandtotal) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg">
		<td colspan="2" style="white-space:nowrap;">Tgl Jatuh Tempo : <b><?php echo $tgljthtempo ?></b></td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
	</tr>
	<tr>
		<td rowspan="7" colspan="3">
			<table border="2" width="100%" class="kotak">
								<tr>
									<td>&nbsp;1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,<br>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kecuali ada perjanjian terlebih dahulu<br>
										&nbsp;2. Faktur asli merupakan bukti pembayaran yg sah<br>
										&nbsp;3. Pembayaran dgn cek/giro baru dianggap sah setelah diuangkan
									</td>
								</tr>
							</table>
		</td>
	</tr>
	<tr class="detailbrg">
		<td align="right" colspan="2">Diskon Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($diskon) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg">
		<td>&nbsp;</td>
		<td colspan="2"><hr></td>
	</tr>
	<tr class="detailbrg">
		<td align="right" colspan="2">DPP Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($dpp) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg">
		<td align="right" colspan="2">PPN Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($nilai_ppn) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg">
		<td>&nbsp;</td>
		<td colspan="2"><hr></td>
	</tr>
	<tr class="detailbrg">
		<td align="right" colspan="2">Nilai Faktur Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($nilai_faktur) ?></b>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5" align="center" style="white-space:nowrap;"><?php $terbil=&terbilangxx($nilai_faktur, 3); ?>(Terbilang: <?php echo $terbil ?> Rupiah)</td>
	</tr>
</table><br>
<div class="kepala2"><b>SE & O<br><br><br><br>

Adm Penjualan</b></div>
