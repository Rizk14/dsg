<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript">
$(document).ready(function() {
 
 $(".xhapus").click(function(){
 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var hps = element.attr("id");
 
 //Built a url to send
 var info = 'pid=' + hps;
 if(confirm("Yakin akan menghapus data dgn id: "+hps+" ?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('/color/cform/actdelete')?>",
 data: info,
 success: function(){
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
 
})

function toUpper1(){
	var ecolorname	= document.getElementById('ecolorname').value;
	var toUpper	= ecolorname.toUpperCase();
	document.getElementById('ecolorname').value = toUpper;
}

</script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<!--
tb = textbox
cc = cacah
-->
<div id="tabs">
	<div id="tab1" class="tab_sel_warna" align="center" onClick="javascript: displayPanelWarna('1');">MASTER WARNA</div>
<!--
	<div id="tab2" class="tab_warna" style="margin-left:1px;" align="center" onClick="javascript: displayPanelWarna('2');">FORM MASTER WARNA</div>
	<div id="tab3" class="tab_warna" style="margin-left:1px;" align="center" onClick="javascript: displayPanelWarna('3');">CARI WARNA</div>
-->
</div>
    
<div class="tab_bdr_warna"></div>
	<div class="panel" id="panel1" style="display:none;">
	<?php
	if( !empty($query) || isset($query) ){
		$cc = 1;
		foreach($query as $row){
			/* 20052011
			<a href=\"#\" class=\"xhapus\" id=\"".$row->i_color."\" ><img src=".base_url()."asset/theme/images/delete.gif width=12 height=13></a>
			*/
			$Classnya = $cc % 2 == 0 ? "row" :"row1";
			$list .= "
			 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
			  <td width=5>".$cc."</td>	
			  <td width=40>".$row->i_color."&nbsp;</td>	 
			  <td width=100>".$row->e_color_name."&nbsp;</td>
			  <td align='center'><a href=".base_url()."index.php/color/cform/edit/".$row->i_color."><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;<a href=\"javascript:void(0)\" onclick='hapus(\"color/cform/actdelete/".$row->i_color."\",\"#content\")'><img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13></a></td>
			 </tr>";
			 
			$cc+=1;	
		}
	}
	?>

	<table class="listtable_warna">
	 <th width="5">No</th>
	 <th width="40">Kode Motif/ Warna</th>
	 <th width="100">Nama Motif/ Warna</th>
	 <th width="10" class="action">Aksi</th>
	<tbody>
	<?php
	if( !empty($list) && !isset($not_defined) ) {
		echo $list."</tbody></table>";
	} else {
		echo $list."</tbody></table>
		<div style=\"font-family:arial; font-size:11px; color:#FF0000;\">".strtoupper($not_defined)."</div>";
	}
	?>

	<table align="center" width="100%">
	  <tr>
	  	<td align="center"><?php echo $create_links; ?></span></td>
	  </tr>
	</table>
	</div>

	<div class="panel" id="panel2" style="display:block;">
	    <table>
	      <tr>
	    	<td align="left">
				   <?php 
			$attributes = array('id'=>'f_master','class'=>'myform');
			echo form_open ('color/cform/simpan',$attributes);
		      ?>
		     
		<div id="masterstatusform">
		      <table>
		      	<tr>
			  <td width="150px">Kode Motif/ Warna</td>
			  <td width="1px">:</td>
			  <td>
				<?php 
				$kdstatus = array(
			              'name'=>'icolor',
			              'id'=>'icolor',
			              'value'=>$icolor,
			              'maxlength'=>'5',
						  'readonly'=>true);
				echo form_input($kdstatus); ?>
			  </td>
			</tr>
		      	<tr>
			  <td>Nama Motif/ Warna</td>
			  <td>:</td>
			  <td>
				<?php
				$nmstatus = array(
			              'name'=>'ecolorname',
			              'id'=>'ecolorname',
			              'value'=>'',
			              'maxlength'=>'255',
			              'onkeyup'=>'toUpper1();');
				echo form_input($nmstatus); ?>
			  </td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			    <input name="tblsimpan" id="tblsimpan" value="Simpan" type="submit">
			      <input name="tblreset" id="tblreset" value="Batal" type="button" onclick="window.location='<?php echo base_url() ?>index.php/color/cform/'">
	
			  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>

	<div class="panel" id="panel3" style="display:none;">
		<table>
			<tr>
			  <td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url'=>'color/cform/cari','update'=>'#content','type'=>'post'));?>
				<table>
				<thead>
			      	 <tr>
				   <td width="160px">Kode Motif/ Warna</td>
				   <td width="1px">:</td>
				   <td>
				    <input type="text" name="txticolor" id="txticolor" maxlength='17'></td>
				 </tr>
			      	 <tr>
				   <td>Nama Motif/ Warna</td>
				   <td>:</td>
				   <td>
				    <input type="text" name="txtecolorname" id="txtecolorname" maxlength='50'></td>
				 </tr>
				 <tr>
				  <td colspan="2">
				  </td>
				  <td align="left"><input type="submit" id="btncari" name="btncari" value="Pencarian"></td>
				 </tr>
				</thead>
				</table>
			  </td>
			</tr>
		</table>
	</div>
