<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript">
function toUpper1(){
	var ecolorname	= document.getElementById('ecolorname').value;
	var toUpper	= ecolorname.toUpperCase();
	document.getElementById('ecolorname').value = toUpper;
}
</script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<!--
tb = textbox
cc = cacah
-->
<div id="tabs">
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">FORM MASTER WARNA</div>
</div>
         
<div class="tab_bdr"></div>

	<div class="panel" id="panel2" style="display:block;">
	    <table class="maintable">
	      <tr>
	    	<td align="left">
		     	<?php
			$attributes = array('id'=>'f_master','class'=>'myform');
			echo form_open ('color/cform/actedit',$attributes);
		    ?>
		<div id="masterstatusform">
		<div class="effect">
		  <div class="accordion2">
		      <table class="mastertable">
		      	<tr>
			  <td width="150px">Kode Motif/ Warna</td>
			  <td width="1px">:</td>
			  <td>
				<?php 
				$kdstatus = array(
			              'name'=>'icolor',
			              'id'=>'icolor',
			              'value'=>$i_color,
			              'maxlength'=>'5',
						  'readonly'=>true);
				echo form_input($kdstatus); ?>
			  </td>
			</tr>
		      	<tr>
			  <td>Nama Motif/ Warna</td>
			  <td>:</td>
			  <td>
				<?php
				$nmstatus = array(
			              'name'=>'ecolorname',
			              'id'=>'ecolorname',
			              'value'=>$e_color_name,
			              'maxlength'=>'255',
			              'onkeyup'=>'toUpper1();');
				echo form_input($nmstatus); ?>
			  </td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			    <input name="tblsimpan" id="tblsimpan" value="Simpan" type="submit">
			     <input name="tblreset" id="tblreset" value="Batal" type="button" onclick="window.location='<?php echo base_url() ?>index.php/color/cform/'">

			  </td>
		       </tr>
		      </table>
		  </div>
		</div>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
