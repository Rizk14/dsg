<!--<style type="text/css" media="print">
	@page 
        {
            size: auto;   /* auto is the current printer page size */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
</style> -->

<style type="text/css">

/*@media print {
	div {
		color: #f00;
	}
} */

/*yang ini buat setting ukuran kertasnya assumsi A4/letter, sementara ga dipake */
/*#Letter {background-color:#FFFFFF;
left:1px;
right:1px;
height:11in ; */ /*Ukuran Panjang Kertas */
/*width: 8.50in; */ /*Ukuran Lebar Kertas */
/*margin:1px solid #FFFFFF; */
 
/* font-family:Georgia, "Times New Roman", Times, serif; } */

   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
    
    }
    
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 19px;
    font-weight: bold;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
	}
	.kepala2 {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	
	.detailbrg2 {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
	}
	.detailbrg {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
	}
	
	.kotak {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .kotak2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 13px;}
    	
	.garisbawah { 
		border-bottom:#000000 0.1px solid;
	}
	.gariskanan { 
		border-right:#000000 0.1px solid;
	}
	
	.nomorpajak {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 15px;}
</style>

<?php include_once ("funcs/terbilang.php"); ?>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	//window.print();
});

</script>
<table border="0" width="100%" align="center">
	<tr>
		<td class="kepala" width="60%" align="center" rowspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FAKTUR PAJAK</td>
	</tr>
	<tr>
		<td class="kepalaxx">
		Lembar Ke 1 : Untuk Pembeli BKP/Penerimaan JKP<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sebagai Bukti Pajak Masukan
		</td>
	</tr>
	<tr>
		<td class="kepalaxx">Lembar Ke 2 : Untuk Pengusaha Kena Pajak </td>
	</tr>
</table>
<table border="1" width="100%" class="kotak" align="center">
	<tr class="kepala2">
		<td>&nbsp;&nbsp;&nbsp;Kode dan Nomor Seri Faktur Pajak : <span class="nomorpajak"><?php echo $nomorpajak ?></span> </td>
	</tr>
	<tr class="kepala2">
		<td>
		
		<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td colspan="2">&nbsp;&nbsp;PENGUSAHA KENA PAJAK</td>
			</tr>
			<tr>
				<td style="white-space:nowrap;" width="25%">&nbsp;&nbsp;N a m a </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo strtoupper($nminitial) ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;A l a m a t </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo strtoupper($almtperusahaan) ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;N P W P </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo strtoupper($npwpperusahaan) ?></td>
			</tr>
		</table>
		
		</td>
	</tr>
	<tr class="kepala2">
		<td>
		
		<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td colspan="2" style="white-space:nowrap;">&nbsp;&nbsp;PEMBELI BARANG KENA PAJAK/PENERIMA JASA KENA PAJAK</td>
			</tr>
			<tr>
				<td style="white-space:nowrap;" width="25%">&nbsp;&nbsp;N a m a </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo $nmkostumer ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;A l a m a t </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo $almtkostumer ?></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">&nbsp;&nbsp;N P W P </td>
				<td style="white-space:nowrap;">: &nbsp;&nbsp;<?php echo $npwp ?></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0">
			<tr align="center" class="detailbrg2">
				<td width="5%" class="garisbawah gariskanan">No. Urut</td>
				<td width="65%" class="garisbawah gariskanan">Nama Barang Kena Pajak<br>Jasa Kena Pajak</td>
				<td width="30%" class="garisbawah">Harga Jual/Penggantian<br>Uang Muka/Termijn (Rp).</td>
			</tr>
			<?php
				$no=1;
				$j = 0;
				$hitung = count($isi);
				foreach($isi as $row) {
					if($no<23 && $row->motifname!='') {
						$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);

						if($qvalue->num_rows()>0) {
							$row_value	= $qvalue->row();
							$tprice[$j]	= $row_value->amount;
						}else{
							$tprice[$j]	= 0;
						}

						$jj	= $j+1;
						
			?>
			<!-- disini perulangan item brg -->
			<tr class="detailbrg">
				<td align="right" class="gariskanan"><?php echo $no; ?>&nbsp;</td>
				<td class="gariskanan">&nbsp;<?php echo strtoupper($row->motifname); ?></td>
				<td align="right"><?php echo number_format($tprice[$j]); ?> &nbsp;</td>
			</tr>
			<?php 
						$j+=1;
						$no+=1;
					}
				}
			?>
			<!-- end perulangan -->
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td width="8%" style="white-space:nowrap;">&nbsp;&nbsp;Harga Jual/<span style="text-decoration:line-through;">Penggantian/Uang Muka/Termijn</span> *)</td>
				<td align="right"><?php echo number_format($jumlah); ?>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td>&nbsp;&nbsp;Dikurangi potongan harga</td>
				<td align="right"><?php echo number_format($diskon); ?>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td>&nbsp;&nbsp;Dikurangi uang muka yang telah diterima</td>
				<td align="right">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td>&nbsp;&nbsp;Dasar Pengenaan Pajak</td>
				<td align="right"><?php echo number_format($dpp); ?>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td>&nbsp;&nbsp;PPN = 10% X Dasar Pengenaan Pajak</td>
				<td align="right"><?php echo number_format($nilai_ppn); ?>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" class="detailbrg2">
			<tr>
				<td>&nbsp;&nbsp;Pajak Penjualan Atas Barang Mewah</td>
				<td align="center"><?php echo $falamat ?>, Tanggal <?php echo $tglfaktur ?></td>
			</tr>
			<tr>
				<td>
					<table border="1" width="80%" class="kotak2">
						<tr align="center">
							<td width="20%">TARIF</td>
							<td width="35%">DPP</td>
							<td width="35%">PPn BM</td>
						</tr>
						<tr>
							<td style="white-space:nowrap;">.................. % <br>
							.................. % <br>
							.................. % <br>
							.................. %
							</td>
							<td style="white-space:nowrap;">
								Rp. ..................................<br>
								Rp. ..................................<br>
								Rp. ..................................<br>
								Rp. ..................................<br>
							</td>
							<td style="white-space:nowrap;">
								Rp. ..................................<br>
								Rp. ..................................<br>
								Rp. ..................................<br>
								Rp. ..................................<br>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">TOTAL</td>
							<td style="white-space:nowrap;">Rp. ..................................</td>
						</tr>
					</table>
				</td>
				<td align="center">
					<br><br><br><br><?php echo $TtdPajak01 ?><br><hr>Adm Keuangan
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table><div class="detailbrg2">*) Coret yang tidak perlu </div>
