<?
  include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>

<div align="center">
  <style type="text/css" media="all">
.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 12px;
  font-weight:normal;
  padding:1px;
}
.eusi {
  font-size: 14px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
  </style>
  <style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
  </style>
  <table width="920px" border="0" cellspacing=0 cellpadding=0>
    <tr>
      <td><div align="center"><strong>Voucher Pembayaran</strong></div></td>
    </tr>
    <tr>
      <td><div align="center"><strong>AREA <? echo $eareaname; ?></strong></div></td>
    </tr>
    <tr>
      <td><div align="center"><strong>NO : <? echo $ipv; ?></strong></div></td>
    </tr>
    </table>
</div>
      <table width="920px" cellspacing=0 cellpadding=0 class="garis">
        <tr>
          <td width="80" align="center" class="huruf isi">CoA</td>
          <td width="300" align="center" class="huruf isi">KETERANGAN</td>
          <td width="80" align="center" class="huruf isi">DEBET</td>
          <td width="80" align="center" class="huruf isi">KREDIT</td>
        </tr>
        <?
          $i=0;
          if($isi){
            $tot=0;
            foreach($isi as $row){
              $tot=$tot+$row->v_pv;
            }
 	          if($iarea!='00'){
	            $coa='111.2'.$iarea;
	          }
	          $sql	= "select e_coa_name from tr_coa where i_coa='$coa'";
	          $rs		=  $this->db->query($sql);
	          if($rs->num_rows>0){
		          foreach($rs->result() as $tes){
		             $coaname=$tes->e_coa_name;
		          }
		        }
            echo "<tr height=27>
               <td>$coa</td>
               <td>$coaname</td>
               <td align='right'></td>
               <td align='right'>".number_format($tot)."</td></tr>";
		        foreach($isi as $row){
              $i++;
              $tmp=explode("-",$row->d_pv);
		          $th=$tmp[0];
		          $bl=$tmp[1];
		          $hr=$tmp[2];
		          $row->d_pv=$hr."-".$bl."-".$th;
		          if($iarea!='00'){
	              $coa=substr($row->i_coa,0,5).$iarea;
	            }
              echo "<tr height=27>
                 <td>$coa</td>
                 <td>$row->e_remark</td>
                 <td align='right'>".number_format($row->v_pv)."</td>
                 <td align='right'></td></tr>";
            }
            echo "<tr height=27>
               <td colspan=2 align=right>Total</td>
               <td align='right'>".number_format($tot)."</td>
               <td align='right'>".number_format($tot)."</td></tr>";
		        $bilangan = new Terbilang;
		        $kata=ucwords($bilangan->eja($tot));	
            echo "<tr height=27><td colspan=4>Terbilang : ".$kata." Rupiah</td></tr>";
            echo "</table>";
            
            echo "<table width=\"920px\" border=0 cellspacing=0 cellpadding=0>
                  <tr rowspan=3>
                    <td colspan=3>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width=\"320\" align=\"center\" class=\"huruf isi\">Diperiksa Oleh</td>
                    <td width=\"320\" align=\"center\" class=\"huruf isi\">Disetujui Oleh</td>
                    <td width=\"320\" align=\"center\" class=\"huruf isi\">Dibuat Oleh</td>
                  </tr>
                  <tr rowspan=3>
                    <td colspan=3>&nbsp;</td>
                  </tr>
                  ";
          }
          echo "</table>";
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
</BODY>
</html>
