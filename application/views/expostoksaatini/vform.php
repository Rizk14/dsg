<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>


<script type="text/javascript" language="javascript">
	function cstopproduct() {
		var valstop;
		if (document.getElementById('f_stop_produksi').checked) {
			document.getElementById('stop').value=1;
		} else {
			document.getElementById('stop').value=0;
		}
	}

	function cbln() {
		if (document.getElementById('lapbln').checked) {
			document.getElementById('bulanan').value=1;
		} else {
			document.getElementById('bulanan').value=0;
		}
	}	
</script>

<!--
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#d_do_first").datepicker();
	$("#d_do_last").datepicker();
});
-->
</script>

<!-- 
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
-->

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_stoksaatini; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_stoksaatini; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		
		$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('expostoksaatini/cform/carilistbrgjadi', $attributes);?>
		
		<div id="masterlstoksaatiniform">

		  <table width="50%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="30%"><?php echo $list_stoksaatini_tgl_do_mulai; ?></td>
					<td width="1%">:</td>
					<td width="69%">
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)"/>
					  <!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)"> -->
					s.d 
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)"/>
					<span style="color:#FF0000">*</span>
					<!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)"> -->
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>
					<!-- <input type="checkbox" name="f_stop_produksi" value="y" /> -->
					<?php
					$stopproduk = array(
						'name'      => 'f_stop_produksi',
						'id'        => 'f_stop_produksi',
						'value'     => '1',
						'onclick'	=> 'cstopproduct();' 
						);
					echo form_checkbox($stopproduk);
					?>  
					
					<?php echo $list_stoksaatini_s_produk; ?></td>
					<td>&nbsp;</td>
					<td><?php echo $list_stoksaatini_j_produk; ?> : 
					<select name="i_class">
					<?php
					foreach($opt_jns_brg as $row) {
						$ljnsbrg .= "
							<option value=\"$row->i_class\">$row->e_class_name</option>
						";
					} 
					echo $ljnsbrg;
					?>
					</select>
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="3" align="right"><input type="checkbox" name="lapbln" id="lapbln" value="1" onclick="cbln();" />&nbsp;Bulanan <span style="color:#FF0000">**</span></td>
				  </tr>				  				  
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:260px; padding:5px;"><span style="color:#FF0000">*</span> ) Format Tgl: dd/mm/YYYY<br /><span style="color:#FF0000">**</span> ) Jika akan mengambil Pelaporan Bulanan</div></div></td>
			</tr>			
			<tr align="right">
			  <td>
			  	<input type="hidden" name="stop" id="stop" value="0"/>
				<input type="hidden" name="bulanan" id="bulanan" value="0"/>
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" />
				<input name="btnbatal" type="reset" id="btnbatal" value="<?php echo $button_batal; ?>" />
			  </td>
			</tr>
		  </table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
