<script type="text/javascript" src="<?php echo base_url(); ?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
		<td class="tcat"><?php echo $page_title_penjualanperdo; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
		<td class="thead"><img src="<?php echo base_url(); ?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_penjualanperdo; ?></td>
	</tr>
	<tr>
		<td class="alt2" style="padding:0px;">

			<table id="table-add-box">
				<tr>
					<td align="left">
						<?php
						echo $this->pquery->form_remote_tag(array('url' => 'listpenjualanperdo/cform/aksi', 'update' => '#content', 'type' => 'post'));
						?>
						<div id="masterlpenjualanperdoform">

							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<table width="98%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="20%"><?php echo $list_penjperdo_no_faktur; ?> </td>
												<td width="1%">:</td>
												<td width="70%">
													<input name="no_faktur" type="text" id="no_faktur" maxlength="14" value="<?= $nofaktur ?>" />
												</td>
											</tr>
											<tr>
												<td><?php echo $list_penjperdo_tgl_mulai; ?> </td>
												<td>:</td>
												<td>
													<input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?= $tgldomulai ?>" />
													<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)">
													s.d
													<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?= $tgldoakhir ?>" />
													<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)">
												</td>
											</tr>

											<tr>
												<td width="20%"><?php echo $list_penjperdo_kd_brg; ?> </td>
												<td width="1%">:</td>
												<td>
													<input name="i_product" type="text" id="i_product" maxlength="9" value="<?php echo $iproduct; ?>" />
												</td>
											</tr>

										</table>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
										<div id="title-box2"><?php echo $form_title_detail_penjualanperdo; ?></div>
									</td>
								</tr>

								<?php if ($template == 1) { ?>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="5%" class="tdatahead">NO</td>
													<td width="12%" class="tdatahead"><?php echo $list_penjperdo_no_do; ?> </td>
													<td width="10%" class="tdatahead"><?php echo strtoupper($list_penjperdo_kd_brg); ?> </td>
													<td width="38%" class="tdatahead"><?php echo $list_penjperdo_nm_brg; ?> </td>
													<td width="10%" class="tdatahead"><?php echo $list_penjperdo_qty; ?> </td>
													<td width="12%" class="tdatahead"><?php echo $list_penjperdo_hjp; ?></td>
													<td width="13%" class="tdatahead"><?php echo $list_penjperdo_amount; ?></td>
													<td width="20%" class="tdatahead" align="center"><?php echo $link_aksi; ?></td>
												</tr>

												<?php
												$totalqty	= 0;
												$totalpenjualan	= 0;

												$nomororder	= array();

												$no	= 1;
												$cc	= 1;
												$arr	= 0;

												if (sizeof($query) > 0) {
													foreach ($query as $row) {
														$Classnya	= $cc % 2 == 0 ? "row1" : "row2";
														$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

														$jmlFaktur	= $this->mclass->jmlFaktur($row->i_faktur_code);
														$njmlFaktur	= $jmlFaktur->num_rows();

														$nomororder[$arr]	= $row->i_faktur_code;

														if ($tgldomulai == '') {
															if ($cc == 1 /* && $row->f_kontrabon == 'f' */) {
																$link_act	= "	<td align=\"center\" rowspan='$njmlFaktur' >";

																/* Lvl 4 & Dpt 7 = Finance Manager, Lvl 4 & Dpt 8 = FIS Manager, Lvl 0 = Administrator, Lvl 3 = GM */
																if ($row->f_printed == 't' && (($this->session->userdata('departemen') == "7" && $this->session->userdata('level') == "4") ||
																	($this->session->userdata('departemen') == "8" && $this->session->userdata('level') == "4") ||
																	$this->session->userdata('level') == "0" || $this->session->userdata('level') == "3")) {
																	// $link_act	.=	"<a href=" . base_url() . "index.php/listpenjualanperdo/cform/undo/" . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . " title=\"Cancel Print (Cetak Ulang)\">
																	// 					<img src=" . base_url() . "asset/images/button_print_cancel.png width=12 height=13 alt=\"Cancel Print\" border=\"0\">
																	// 				</a>&nbsp;";
																	$link_act	.=	"<a href=\"javascript:void(0)\" title=\"Cancel Print (Cetak Ulang)\" onclick='reprint(\"listpenjualanperdo/cform/reprint/" . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . "\",\"#content\");'>
																						<img src=" . base_url() . "asset/images/button_print_cancel.png width=\"12\" height=\"15\" alt=\"Cancel Print\">
																					</a>&nbsp;";
																}

																if ($row->f_kontrabon == 'f') {
																	$link_act	.= "<a href=" . base_url() . "index.php/listpenjualanperdo/cform/edit/" . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . " title=\"Edit Faktur\">
																						<img src=" . base_url() . "asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Faktur\" border=\"0\">
																					</a>&nbsp;
																					<a href=" . base_url() . "index.php/listpenjualanperdo/cform/undo/" . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . " title=\"Cancel faktur\">
																						<img src=" . base_url() . "asset/theme/images/cross.png width=12 height=13 alt=\"Cancel faktur\" border=\"0\">
																					</a>";
																}

																$link_act	.=	"</td>";
															} elseif ($cc > 1 && $nomororder[$arr - 1] != $row->i_faktur_code /* && $row->f_kontrabon == 'f' */) {
																$link_act	= "	<td align=\"center\" rowspan='$njmlFaktur' >";

																if ($row->f_kontrabon == 'f') {
																	$link_act	.= "<a href=" . base_url() . "index.php/listpenjualanperdo/cform/edit/" . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . " title=\"Edit Faktur\">
																						<img src=" . base_url() . "asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Faktur\" border=\"0\">
																					</a>&nbsp;
																					<a href=" . base_url() . "index.php/listpenjualanperdo/cform/undo/" . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . " title=\"Cancel faktur\">
																						<img src=" . base_url() . "asset/theme/images/cross.png width=12 height=13 alt=\"Cancel faktur\" border=\"0\">
																					</a>";
																}
																$link_act	.=	"</td>";
															} else {
																$link_act	= "";
															}
														} else {
															$link_act	= "";
														}

														$lpenjperdo	.= " 	<tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\" onMouseOut=\"this.className='$Classnya'\">
																				<td height=\"22px\" bgcolor=\"$bgcolor\">" . $no . ".&nbsp;</td>
																				<td>" . $row->idocode . "</td>
																				<td>" . $row->imotif . "</td>
																				<td>" . $row->motifname . "</td>
																				<td align=\"right\">" . $row->qty . "</td>
																				<td align=\"right\">" . number_format($row->unitprice, '2', '.', ',') . "</td>
																				<td align=\"right\">" . number_format($row->amount, '2', '.', ',') . "</td>
																				$link_act
																			</tr>";
														$no += 1;
														$cc += 1;
														$totalqty += $row->qty;
														$totalpenjualan += $row->amount;
														$arr += 1;
													}
													echo $lpenjperdo;
												}
												?>
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td align="center"><?php echo $create_link; ?></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="98%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="40%" align="right"><?php echo $list_penjperdo_total_pengiriman; ?></td>
													<td width="1%">:</td>
													<td width="20%">
														<input name="v_t_pengiriman" type="text" id="v_t_pengiriman" maxlength="50" style="text-align:right;" value="<?php echo $jmltotalpenjualan; ?>" style="text-align:right;" />
													</td>
													<td width="20%" align="right"><?php echo $list_penjperdo_total_penjualan; ?></td>
													<td width="0%">:</td>
													<td width="20%" align="right">
														<input name="v_t_penjualan" type="text" id="v_t_penjualan" maxlength="50" style="text-align:right;" value="<?php echo number_format($nilaitotalpenjualan, '2', '.', ','); ?>" style="text-align:right;" />
													</td>
												</tr>
											</table>
										</td>
									</tr>
								<?php } else { ?>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">

												<tr>
													<td width="5%" class="tdatahead">NO</td>
													<td width="30%" class="tdatahead"><?php echo "PELANGGAN"; ?> </td>
													<td width="12%" class="tdatahead"><?php echo $list_penjperdo_no_do; ?> </td>
													<td width="10%" class="tdatahead"><?php echo "Tgl. DO"; ?> </td>
													<td width="8%" class="tdatahead"><?php echo "DO"; ?> </td>
													<td width="10%" class="tdatahead"><?php echo "Faktur Faktur"; ?> </td>
													<td width="12%" class="tdatahead"><?php echo "Tgl. Faktur"; ?></td>
													<td width="8%" class="tdatahead"><?php echo "Faktur"; ?></td>
												</tr>

												<?php

												$no	= 1;
												$cc	= 1;

												if (sizeof($query) > 0) {

													foreach ($query as $row) {

														$Classnya	= $cc % 2 == 0 ? "row1" : "row2";
														$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

														$query = $this->mclass->nmcabang($row->idocode);

														if ($query->num_rows() > 0) {
															$rcabang = $query->row();
															$cabangname = $rcabang->e_branch_name;
														} else {
															$cabangname = "";
														}

														$lpenjperdo	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">" . $no . ".&nbsp;</td>
								<td>" . $cabangname . "</td>
								<td>" . $row->idocode . "</td>
								<td align=\"center\">" . $row->ddo . "</td>
								<td align=\"right\">" . $row->jmldo . "</td>
								<td>" . $row->ifakturcode . "</td>
								<td align=\"center\">" . $row->dfaktur . "</td>
								<td align=\"right\">" . $row->jmlfaktur . "</td>
							  </tr>";
														$no += 1;
														$cc += 1;
													}
													echo $lpenjperdo;
												}
												?>
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td align="center"><?php echo $create_link; ?></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								<?php } ?>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="right">
										<!-- <input name="btnlaporan" type="button" id="btnlaporan" value="Laporan" /> -->
										<input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listpenjualanperdo/cform/'">
									</td>
								</tr>
							</table>

						</div>
						<?php echo form_close(); ?>
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>