<table class="tborder" cellpadding="6" cellspacing="1" border="0" style="width: 1022px;" align="center" style="border-bottom-width:0px">
	<tr><td class="tcat"><?php echo $title; ?></td></tr>
</table>
<table class="tborder" cellpadding="6" cellspacing="1" border="0" style="width: 1022px;" align="center">
	<tr>
		<td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $title; ?></td>
	</tr>
	<tr>
		<td class="alt2" style="padding:0px;">
			<table class="maintable" style="width: 100%;">
				<tr>
					<td align="left">
						<? echo $this->pquery->form_remote_tag(array('url'=>$folder.'/cform/closing','update'=>'#content','type'=>'post','id'=>'cekbox'));?>
						<div id="jthform">
							<div class="effect">
								<center>
									<div id="masterforcastform" style="width: 60%;" border='1'>
										<table id="tabledata" class="mastertable" border="0">
											<tr>
												<td><b>Date Closing</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<?php $data = array(
														'name'        => 'dclosing',
														'id'          => 'dclosing',
														'value'       => $dclosing,
														'placeholder' => 'Date ...',
														'readonly'    => 'true');
													echo form_input($data);
													?>
												</td>
												<td><b>Date Open</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<?php $data = array(
														'name'        => 'dopen',
														'id'          => 'dopen',
														'value'       => $dopen,
														'placeholder' => 'Date ...',
														'readonly'    => 'true');
													echo form_input($data);
													?>
												</td>
											</tr>
											<tr>
												<td><b>Kas Besar (Keluar)</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<input type="checkbox" name="f_kb" id="f_kb">
												</td>
												<td><b>Kas Besar (Masuk)</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<input type="checkbox" name="f_kbin" id="f_kbin">
												</td>
											</tr>
											<tr>
												<td><b>Bank (Keluar)</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<input type="checkbox" name="f_kbank" id="f_kbank">
												</td>
												<td><b>Bank (Masuk)</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<input type="checkbox" name="f_kbankin" id="f_kbankin">
												</td>
											</tr>
											<tr>
												<td><b>Kas Kecil (Keluar)</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<input type="checkbox" name="f_kk" id="f_kk">
												</td>
												<td><b>Pengisian Kas Kecil (Masuk)</b></td>
												<td>:</td>
												<td>&nbsp;&nbsp;
													<input type="checkbox" name="f_kkin" id="f_kkin">
												</td>
											</tr>
											<tr>
												<td width="100%" colspan="6">
													<hr style="width: 100%;">
												</td>
											</tr>
											<tr>
												<td colspan="6" align="center">
													<button  name="login" id="login" value="View" type="submit">Simpan</button>
													<button name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('<?= $folder; ?>/cform/','#content')">Reset</button>
												</td>
											</tr>
										</table>
									</div>
									<div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:96%; padding:5px; text-align: left;"><span style="color:#FF0000">*</span> Pilih Transaksi apa saja yang akan diclosing!</div>
								</center>
							</div>
						</div>
						<?=form_close()?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script type="text/javascript">
	$(function() {
		$('#dclosing').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy'
		});
		$('#dopen').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy'
		});
	});

	$('#login').click(function(event) {
		if ($("#tabledata input:checkbox:checked").length > 0){
			return true;
		}else{
			alert('Pilih Transaksi Kas/Bank minimal satu!');
			return false;
		}
	});
</script>
