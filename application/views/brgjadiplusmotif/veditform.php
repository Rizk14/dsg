<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div id="tabs">
	<div id="tab2" class="tab_brg_motif" style="margin-left:1px;" align="center" onClick="javascript: displayPanelBrgMotif('2');">FORM BRG JADI + MOTIF</div>
</div>

<div class="panel_brg_motif" id="panel2" style="display: block;">
<table width="90%">
	<tr>
		<td align="left">
			
		<?php
		$attributes=array('class'=>'f_master', 'id'=>'my_form');
		 echo form_open('brgjadiplusmotif/cform/actedit',$attributes); ?>
		<div id="mastermotifbrgform">
		    <table>
				<tr>
				  <td width="25%"><?php echo $form_kd_brg_brgmotif; ?></td>
				  <td width="1%">:</td>
				  <td>
				  <?php
				   $kdbrg = array(
					'name'=>'i_product2',
					'id'=>'i_product2',
					'value'=>$i_product,
					'maxlength'=>'15',
					'readonly'=>'true'
					);
				   echo form_input($kdbrg);
				  ?> 		
				  </td>
				</tr>
				<tr>
				  <td><?php echo $form_kd_motif_brgmotif; ?></td>
				  <td>:</td>
				  <td>
				  <?php
				   $kdmotif = array(
					'name'=>'i_product_motif',
					'id'=>'i_product_motif',
					'value'=>$i_product_motif,
					'maxlength'=>'15',
					'readonly'=>'true' );
				   echo form_input($kdmotif);	 
				  ?>
				  </td>
				</tr>
				<!--
				<tr>
				  <td><?php // echo $form_nm_motif_brgmotif; ?></td>
				  <td>:</td>
				  <td>
				  <?php
				  /*
				   $nmotif = array(
						'name'=>'e_product_motifname',
						'id'=>'e_product_motifname',
						'value'=>$e_product_motifname,
						'maxlength'=>'200');
				   echo form_input($nmotif);
				   */
				  ?>
				  </td>
				</tr>
				-->
				<tr>
				  <td><?php echo $form_qty_brgmotif; ?></td>
				  <td>:</td>
				  <td>
				  <?php
				   $nqty = array(
					'name'=>'n_quantity',
					'id'=>'n_quantity',
					'value'=>$n_quantity,
					'maxlength'=>'10'
					);
				   echo form_input($nqty);		
				  ?>	 	
				  </td>
				</tr>
				<tr>
				  <td><?php echo $form_color_brgmotif; ?></td>
				  <td>:</td>
				  <td>
					<select name="icolor">
						<option value="">[<?php echo $form_pilih_color_brgmotif; ?>]</option>
						<?php
						foreach($opt_color as $color) {
							$sel	= $COL==$color->i_color?" selected ":"";
							$lcolor	.= "<option value=".$color->i_color." $sel >".$color->e_color_name."</option>";
						}
						echo $lcolor;
						?>
					</select>	 	
				  </td>
				</tr>				
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>
					<input name="btnsimpan" id="btnsimpan" value="<?php echo $button_simpan; ?>" type="submit">
					<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url() ?>index.php/brgjadiplusmotif/cform'">
				  </td>
				</tr>
			</table>
		</div>
		<?php echo form_close(); ?>
		</td>
	</tr> 
</table>
</div>
