<h3>Data Bahan Pendukung</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-bp/cform/inputdata">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bp/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****kel brg*************************************
		
		var kel_brg="#kode_kel_brg_"+n;
		var new_kel_brg="#kode_kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kode_kel_brg_"+no);
		$(new_kel_brg, lastRow).attr("name", "kode_kel_brg_"+no);		
		$(new_kel_brg, lastRow).val('');				
		//*****end kel brg*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');
		//*****end satuan*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****deskripsi*************************************
		var deskripsi="#deskripsi_"+n;
		var new_deskripsi="#deskripsi_"+no;
		$(deskripsi, lastRow).attr("id", "deskripsi_"+no);
		$(new_deskripsi, lastRow).attr("name", "deskripsi_"+no);		
		$(new_deskripsi, lastRow).val('');				
		//*****end deskripsi*************************************	
		
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>
<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_bp" id="f_bp" action="<?php echo base_url(); ?>index.php/mst-bp/cform/inputsubmit" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">
<label id="status"></label>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
    <td><br>
	<form name="myform">
	<table id="tabelku" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="7" align="right"><input id="returpurchase" type="button" name="returpurchase" value=" + "> &nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
	      <th>Kelompok Brg</th>
	      <th>Satuan</th>
          <th>Harga</th>
          <th>Deskripsi</th>
        </tr>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_1" type="text" id="kode_1" size="20" maxlength="20" /></td>
          <td><input name="nama_1" type="text" id="nama_1" size="30" maxlength="60" /></td>
          <td><select name="kode_kel_brg_1" id="kode_kel_brg_1">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          <td><select name="satuan_1" id="satuan_1">
					<option value="Roll">Roll</option>
					<option value="Yard">Yard</option>
					<option value="Meter">Meter</option>
					<option value="Lembar">Lembar</option>
					<option value="Kg">Kg</option>
					<option value="Pieces">Pieces</option>
					<option value="Lusin">Lusin</option>
				</select></td>
          <td><input name="harga_1" type="text" id="harga_1" size="10" maxlength="10" /></td>
          <td><input name="deskripsi_1" type="text" id="deskripsi_1" size="30" maxlength="30" /></td>
        </tr>

	</table>	
	</form>
      <div align="center"><br><br> 
        <input type="submit" name="submit" value="Simpan">&nbsp;<input type="reset" name="batal" value="Reset">
      </div></td>
    </tr>

</table>
</div>
</form>
