<h3>Data Bahan Pendukung</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-bp/cform/inputdata">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bp/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php echo form_open('mst-bp/cform/submit'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="kodeedit" value="<?php echo $ekode ?>"><?php } ?>
	<table>
		<tr>
			<td>Kode Barang</td>
			<td> <input type="text" name="kode" value="<?php echo $ekode ?>" maxlength="20" size="20">
			</td>
		</tr>
		<tr>
			<td>Nama Barang</td>
			<td> <input type="text" name="nama" value="<?php echo $enama_brg ?>" maxlength="60" size="60"></td>
		</tr>
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kode_kel_brg" id="kode_kel_brg">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($kel->kode == $ekode_kel_brg) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Satuan</td>
			<td> <select name="satuan" id="satuan">
					<option value="Roll" <?php if ($esatuan == "Roll") { ?> selected="true" <?php } ?> >Roll</option>
					<option value="Yard" <?php if ($esatuan == "Yard") { ?> selected="true" <?php } ?> >Yard</option>
					<option value="Meter" <?php if ($esatuan == "Meter") { ?> selected="true" <?php } ?> >Meter</option>
					<option value="Lembar" <?php if ($esatuan == "Lembar") { ?> selected="true" <?php } ?> >Lembar</option>
					<option value="Kg" <?php if ($esatuan == "Kg") { ?> selected="true" <?php } ?> >Kg</option>
					<option value="Pieces" <?php if ($esatuan == "Pieces") { ?> selected="true" <?php } ?> >Pieces</option>
					<option value="Lusin" <?php if ($esatuan == "Lusin") { ?> selected="true" <?php } ?> >Lusin</option>
				</select></td>
		</tr>
		<tr>
			<td>Harga</td>
			<td> <input type="text" name="harga" value="<?php echo $eharga ?>" maxlength="10" size="10"></td>
		</tr>
		<tr>
			<td>Deskripsi</td>
			<td> <input type="text" name="deskripsi" value="<?php echo $edeskripsi ?>" maxlength="30" size="30"></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bp/cform/view'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

