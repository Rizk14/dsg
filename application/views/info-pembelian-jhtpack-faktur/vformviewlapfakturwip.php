<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
   
   .judulnya {
		background-color:#DDD;
	}

</style>

<h3>Laporan Faktur Pembelian Makloon Hasil Jahit + Packing (WIP)</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Unit Jahit: <?php if ($kode_unit_jahit != '0') { echo $kode_unit_jahit." - ".$nama_unit_jahit; } else { echo "All"; } ?><br>
Unit Packing: <?php if ($kode_unit_packing != '0') { echo $kode_unit_packing." - ".$nama_unit_packing; } else { echo "All"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br> 
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-pembelian-jhtpack-faktur/cform/export_excel_lapfakturwip', $attributes); ?>
<input type="hidden" name="kode_unit_jahit" value="<?php echo $kode_unit_jahit ?>" >
<input type="hidden" name="kode_unit_packing" value="<?php echo $kode_unit_packing ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis_masuk" value="<?php echo $jenis_masuk ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th rowspan="2">No</th>
		 <th rowspan="2">Unit Jahit</th>
		 <th rowspan="2">Unit Packing</th>
		 <th rowspan="2">No Faktur</th>
		 <th rowspan="2">Tgl Faktur</th>
		 <th rowspan="2">No SJ / Tanggal SJ</th>  
		 <th colspan="8">List Brg Jadi</th>		 
		 <th rowspan="2">Grand Total</th>
	 </tr>
	 <tr class="judulnya">
		<th>Kode Brg Jadi</th>
		<th>Nama Brg Jadi</th>
		<th>Satuan</th>
		<th>Quantity</th>
		<th>Harga Jahit (Rp.)</th>
		<th>Harga Packing (Rp.)</th>
		<th>Diskon</th>
		<th>Subtotal</th>
		
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				$tot_grandtotal = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					//$tot_grandtotal += $query[$j]['grandtotal'];
				} // end header
				
			}
			else {
				$tot_grandtotal = 0;
			}
		 
			if (is_array($query)) {
				$no = 1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$no."</td>";
				 echo    "<td>".$query[$j]['nama_unit_jahit']."</td>";
				 echo    "<td>".$query[$j]['nama_unit_packing']."</td>";
				 
				echo    "<td>".$query[$j]['no_faktur']."</td>";
	
				 echo    "<td>".$query[$j]['tgl_faktur']."</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				  echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				// echo    "<td></td>";
				
				
				
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='center'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
			//	 echo    "<td></td>";
				 echo "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga_j'], 2,',','.');
						echo  "" ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				  echo "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga_p'], 2,',','.');
						echo  "" ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['diskon'], 2,',','.');
						 echo ""   ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['subtotal'],2,',','.');
						 echo ""   ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";

				

				 echo    "<td align='right'>".number_format($query[$j]['grandtotal'],2,',','.')."</td>";
				$tot_grandtotal+=$query[$j]['grandtotal'];
				
				 
				 echo  "</tr>";

				$no++;
		 	} // end for
		   }
		 ?>
		 <tr>
			<td colspan="14" align="center"><b>TOTAL</b></td>
			<td colspan="2" align="right"><b><?php echo number_format($tot_grandtotal,2,',','.') ?></b></td>
		 </tr>
 	</tbody>
</table><br>
</div>
