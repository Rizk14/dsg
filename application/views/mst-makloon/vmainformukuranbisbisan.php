<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">
function cek_data() {
	var nama= $('#nama').val();
	if (nama == '') {
		alert("Nama ukuran harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
}
</script>
<h3>Data Ukuran Bis-Bisan</h3><br>
<?php echo form_open('mst-makloon/cform/submitukuranbisbisan'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="id_ukuran" value="<?php echo $eid ?>"><?php } ?>
	<table>
		<tr>
			<td>Nama Ukuran</td>
			<td>: <input type="text" name="nama" id="nama" value="<?php echo $enama ?>" maxlength="30"></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-makloon/cform/ukuranbisbisan'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

<div>
<table border="1" cellpadding= "1" cellspacing = "1" width="50%">
	<thead>
	 <tr class="judulnya">
		 <th>Nama Ukuran</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->nama</td>";
				$pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 echo    "<td>$tgl_update</td>";
				 				 
				 echo    "<td align=center><a href=".base_url()."index.php/mst-makloon/cform/ukuranbisbisan/$row->id \" id=\"$row->id\">Edit</a>";
				echo    "&nbsp; <a href=".base_url()."index.php/mst-makloon/cform/deleteukuranbisbisan/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table>
</div>
