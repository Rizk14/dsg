<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">
function cek_data() {
	var harga= $('#harga').val();

	if (harga == '') {
		alert("Harga harus diisi..!");
		$('#harga').focus();
		return false;
	}
	if (isNaN(harga)) {
		alert("Harga harus berupa angka..!");
		$('#harga').focus();
		return false;
	}
	
}
</script>

<h3>Data Harga Bisbisan</h3><br>
<?php echo form_open('mst-makloon/cform/submithargabisbisan'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> 
<input type="hidden" name="id_harga" value="<?php echo $eid ?>">
<?php } ?>
	<table>
		<tr>
			<td>Supplier Makloon</td>
			<td>: <select name="supplier" id="supplier">
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($sup->id == $eid_supplier) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Potong</td>
			<td>: <select name="jenis_potong" id="jenis_potong">
					<option value="1">Potong Serong</option>
					<option value="2">Potong Lurus</option>
					<option value="3">Potong Spiral</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Harga</td>
			<td>: <input style="text-align:right;" type="text" name="harga" id="harga" value="<?php echo $eharga ?>" size="5" ></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-makloon/cform/hargabisbisan'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br><br>

<div>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		<th>Supplier</th>
		 <th>Jenis Potong</th>
		 <th>Harga</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode_supplier - $row->nama_supplier</td>";
				 if ($row->jenis_potong == '1')
					$jnspot = "Potong Serong";
				 else if ($row->jenis_potong == '2')
					$jnspot = "Potong Lurus";
				 else
					$jnspot = "Potong Spiral";
				 echo    "<td>$jnspot</td>";
				 echo    "<td align='right'>".number_format($row->harga, 2, '.',',')."</td>";
				 
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo    "<td>$tgl_update</td>";
				 				 
				 echo    "<td align=center><a href=".base_url()."index.php/mst-makloon/cform/hargabisbisan/$row->id \" id=\"$row->id\">Edit</a>";
				 
				 echo    "&nbsp; <a href=".base_url()."index.php/mst-makloon/cform/deletehargabisbisan/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table>
</div>
