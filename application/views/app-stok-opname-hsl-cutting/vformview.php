<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Approval Stok Opname Bahan Hasil Cutting</h3><br><br>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>

Tanggal Pencatatan SO: <?php echo $tgl_so ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('app-stok-opname-hsl-cutting/cform/submit', $attributes);
$no = count($query);
 ?>
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="id_header" id="id_header" value="<?php echo $query[0]['id_header'] ?>">
<input type="hidden" name="tgl_so" id="tgl_so" value="<?php echo $tgl_so ?>">

<input type="submit" name="submit" value="Update Stok Opname" onclick="return confirm('Yakin akan approve data SO ini ? Data stok terkini akan otomatis terupdate')">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/app-stok-opname-hsl-cutting/cform'">
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		<th>Kode</th>
		<th>Nama Barang</th>
		<th>Satuan</th>
		<th>Total Stok Akhir<br>Per Warna</th>
		<th>Jml Fisik<br>Per Warna</th>
		<!--<th>Total Stok Akhir<br>Per Warna Bhn Baku</th>
		<th>Jml Fisik Per Bhn Baku</th>-->
		
		 <!--<th>Kode Bahan</th>
		 <th>Nama Bahan</th>
		 <th>Stok Akhir</th>
		 <th>Jumlah Fisik</th>-->
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 
					 echo    "<td>".$query[$j]['kode_brg']."</td><td> ".$query[$j]['nama_brg']."
					 <input type='hidden' name='id_brg_wip_$i' id='id_brg_wip_$i' value='".$query[$j]['id_brg_wip']."'>
					 <input type='hidden' name='id_$i' id='id_$i' value='".$query[$j]['id']."'>
					 </td>
					 <td>Set</td>";
					 					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
							for($zz=0;$zz<count($detailwarna);$zz++){ ?>
								<span style="white-space:nowrap;">
								<?php echo $detailwarna[$zz]['nama_warna']."&nbsp; : ".$detailwarna[$zz]['saldo_akhir']."<br>";
							}
					 }
					 echo "</td>";
					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
							for($zz=0;$zz<count($detailwarna);$zz++){ ?>
								<span style="white-space:nowrap;">
								<?php echo $detailwarna[$zz]['nama_warna']."&nbsp; : ".$detailwarna[$zz]['stok_opname']."<br>";
								
								?>
								<input type="hidden" name="id_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['id_warna'] ?>">
								<input type="hidden" name="stok_fisik_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['stok_opname'] ?>">
								<input type="hidden" name="jum_stok_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['saldo_akhir'] ?>">
								<?php
							}
					 }
				
					 echo "</td>";
					 
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>

<?php echo form_close();  ?>
</div>
