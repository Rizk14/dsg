<script type="text/javascript">

function fs() {
	
	if(document.getElementById('cstatus').checked) {
		document.getElementById('fstatus').value = 't';
	}else{
		document.getElementById('fstatus').value = 'f';
	}
}

</script>

<script type="text/javascript">
$(document).ready(function() {
 
 $(".delbutton").click(function(){
	 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var del_id = element.attr("id");
 
 //Built a url to send
 var info = 'id=' + del_id;
 if(confirm("Anda yakin akan menghapus?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('ipaddress/cform/actdelete')?>",
 data: info,
 success: function(){
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
	 
})
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_ip; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_ip; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<div id="masteraddprinterform">

			<div id="tabs">
				<div id="tab2" class="tab_user" style="margin-left:1px;" align="center" onClick="javascript: displayPanelPrinter('2');">FORM EDIT IP</div>

			<div class="tab_bdr_printer"></div>
			
			<div class="panel_printer" id="panel2" style="display:block;">
				<table>
				  <tr>
					<td align="left">
					  <?php 
					  
					 $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('ipaddress/cform/actedit', $attributes);?>
				  	  <div class="accordion2">
					  <table>

						<tr>
					  		<td width="150px"><?php echo $form_tbl_ip_name; ?></td>
					  		<td width="1px">:</td>
					  		<td>
							<?php
								$eprintername	= array(
									'name'=>'e_ip_name',
									'id'=>'e_ip_name',
									'value'=>'',
									'maxlength'=>'100',
									'value'=>$e_ip_name
								);
								echo form_input($eprintername);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_tbl_ip_ket; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$euri	= array(
									'name'=>'e_note',
									'id'=>'e_note',
									'maxlength'=>'200',
									'value'=>$e_note
								);
								echo form_input($euri);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_tbl_ip_status; ?></td>
					  		<td>:</td>
					  		<td>
							<input type="checkbox" name="cstatus" id="cstatus" value="<?php echo $cstatus; ?>" onclick="fs();" <?php if($cstatus=='t') { echo "checked"; } ?>/>
							</td>
					    </tr>
					    					  
					    <tr>
					  		<td>&nbsp;</td>
					  		<td>&nbsp;</td>
					  		<td>
					  		<input type="hidden" name="fstatus" id="fstatus" value="<?=$cstatus?>" />
							<input type="hidden" name="i_ip" id="i_ip" value="<?php echo $iip; ?>"  />
							<input name="tblsimpan" id="tblsimpan" value="<?php echo $button_simpan; ?>" type="submit">

					  	<input name="tblreset" id="tblreset" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/ipaddress/cform/'">

					  		</td>
					    </tr>
					  </table>
				      </div>
					  <?php echo form_close(); ?>
					</td>
				  </tr> 
				</table>
			</div>
			
		</div>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
