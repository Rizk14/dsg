<script type="text/javascript">

function fs() {
	
	if(document.getElementById('cstatus').checked) {
		document.getElementById('fstatus').value = 't';
	}else{
		document.getElementById('fstatus').value = 'f';
	}
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_ip; ?></td></tr>
</table>
<a href="<?php echo base_url(); ?>index.php/ipaddress/cform/tambah">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/ipaddress/cform">View Data</a>&nbsp;<br><br>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_ip; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<div id="masteraddprinterform">

			<div id="tabs">
				<div id="tab1" class="tab_sel_printer" align="center" onClick="javascript: displayPanelPrinter('1');">DAFTAR IP</div>
				<div id="tab2" class="tab_printer" style="margin-left:1px;" align="center" onClick="javascript: displayPanelPrinter('2');">FORM TAMBAH IP</div>

			<div class="tab_bdr_printer"></div>
			<div class="panel_printer" id="panel1" style="display: block">
				<?php			
				if( !empty($query) || isset($query) ) {
					$simgedit	= "<img src=".base_url()."asset/theme/images/edit.gif"."width=12 height=13>";
					$simgdelete	= "<img src=".base_url()."asset/theme/images/delete.gif"."width=12 height=13>";
					$cc = 1;
					foreach($query as $row) {
						
						$Classnya = $cc % 2 == 0 ? "row" :"row1";
							
						$factive	= ($row->f_active=='t')?('Aktif'):('Tdk Aktif');
						
						$list .= "
						 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
						  <td>".$cc."</td>		 
						  <td>".$row->e_ip."</td>
						  <td>".$row->e_note."&nbsp;</td>
						  <td align=\"center\">".$factive."</td>
						  <td align=center>
						  <a href='".base_url()."index.php/ipaddress/cform/edit/".$row->iip."'>
					
						  <img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;
						    <a href='".base_url()."index.php/ipaddress/cform/actdelete/".$row->iip."'>
						  <img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13></a>
						  </td>
						 </tr>
						";
						$cc+=1;	
					}
				}

				?>
				<table class="listtable_user" border="1">
				 <th width="4%">No</th>
				 <th width="40%"><?php echo $form_tbl_ip_name; ?></th>
				 <th width="15%"><?php echo $form_tbl_ip_ket; ?></th>
				 <th width="10%"><?php echo $form_tbl_ip_status; ?></th>
				 <th width="10%" align="center"><?php echo $link_aksi; ?></th>
				<tbody>
				<?php
				if( !empty($list) && !isset($not_defined) ) {
					echo $list;
				} else {
					echo $list."</tbody></table>".$not_defined;
				}
				?>
			</div>
			
			<div class="panel_printer" id="panel2" style="display: none">
				<table>
				  <tr>
					<td align="left">
					  <?php 
					  echo $this->pquery->form_remote_tag(array('url'=>'ipaddress/cform/simpan','update'=>'#content','type'=>'post'));
					  ?>
				  	  <div class="accordion2">
					  <table>
						<tr>
					  		<td width="150px"><?php echo $form_tbl_ip_name; ?></td>
					  		<td width="1px">:</td>
					  		<td>
							<?php
								$eprintername	= array(
									'name'=>'e_ip_name',
									'id'=>'e_ip_name',
									'value'=>'',
									'maxlength'=>'100'
								);
								echo form_input($eprintername);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_tbl_ip_ket; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$euri	= array(
									'name'=>'e_note',
									'id'=>'e_note',
									'maxlength'=>'200'
								);
								echo form_input($euri);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_tbl_ip_status; ?></td>
					  		<td>:</td>
					  		<td>
							<input type="checkbox" name="cstatus" id="cstatus" value="f" onclick="fs();" /> Aktif
							</td>
					    </tr>						

						<tr><td colspan="3">&nbsp;</td></tr>
					    <tr>
					  		<td>&nbsp;</td>
					  		<td>&nbsp;</td>
					  		<td>
					  		<input type="hidden" name="fstatus" id="fstatus" value="f" />
							<input name="tblsimpan" id="tblsimpan" value="<?php echo $button_simpan; ?>" type="submit">
							<input name="tblreset" id="tblreset" value="<?php echo $button_batal; ?>" type="button" onclick='show("ipaddress/cform/","#content");'>
					  		</td>
					    </tr>
					  </table>
				      </div>
					  <?php echo form_close()?>
					</td>
				  </tr> 
				</table>
			</div>
			
		</div>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
