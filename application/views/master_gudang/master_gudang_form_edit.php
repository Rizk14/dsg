
<div class="container">
<h2>Master Gudang</h2>
<hr>


<?php echo form_open('/master_gudang/master_gudang/updatedata', array('id'=>'myform', 'class'=>'form-vertical', 'role'=>'form', 'method'=>'post')) ?>
	  
	   <input type="hidden" name="no" id="no" value="">
	   <input type="hidden" name="id" id="id" value=<?php echo $values->id ; ?>>
	   <div id='my_div_1'  >   
			<?php echo form_input('num',1,'id="num" class="form-control" maxlength="3" size="1" align="center" disabled' ) ?> 
           
            <span class="label label-info">Nama Gudang</span>
           <input type="hidden" name="nama_gudang_lama" id="nama_gudang_lama" value="<?php echo $values->nama_gudang ?>">
            <?php echo form_input('nama_gudang', $values->nama_gudang, 'id="nama_gudang" class="form-control" placeholder="Nama gudang" maxlength="64"') ?>            
            <span class="label label-info">Jenis SO</span>
           <input type="hidden" name="id_jenis_so_lama" id="id_jenis_so_lama" value="<?php echo $values->id_jenis_so ?>">
           <?php
                   $id_jenis_so = array(
                '' => '- Pilih Jenis SO -',
                '1' => 'Berdasarkan Set',
                '2' => 'Berdasarkan Item', 
            );
                    $atribut_id_jenis_so = 'class="form-control" id="id_jenis_so"';
            echo form_dropdown('id_jenis_so', $id_jenis_so, $values->id_jenis_so, $atribut_id_jenis_so);
            ?>
            <span class="label label-info">Lokasi</span>
           <input type="hidden" name="lokasi_lama" id="lokasi_lama" value="<?php echo $values->lokasi ?>">
            <?php echo form_input('lokasi', $values->lokasi, 'id="lokasi" class="form-control" placeholder="Nama lokasi" maxlength="64"') ?>            
			<span class="label label-info">Nama Perusahaan</span>
           <input type="hidden" name="nama_perusahaan_lama" id="nama_perusahaan_lama" value="<?php echo $values->nama_perusahaan ?>">
            <?php echo form_input('nama_perusahaan', $values->nama_perusahaan, 'id="nama_perusahaan" class="form-control" placeholder="Nama perusahaan" maxlength="64"') ?>            
            <span class="label label-info">Nama Penanggung Jawab</span>
           <input type="hidden" name="nama_penanggung_jawab_lama" id="nama_penanggung_jawab_lama" value="<?php echo $values->nama_penanggung_jawab ?>">
            <?php echo form_input('nama_penanggung_jawab', $values->nama_penanggung_jawab, 'id="nama_penanggung_jawab" class="form-control" placeholder="Nama Penanggung Jawab" maxlength="64"') ?>            
			<span class="label label-info">Nama Administrator</span>
           <input type="hidden" name="nama_administrator_lama" id="nama_administrator_lama" value="<?php echo $values->nama_administrator ?>">
            <?php echo form_input('nama_administrator', $values->nama_administrator, 'id="nama_administrator" class="form-control" placeholder="Nama Administrator" maxlength="64"') ?>            
		
        </div>


<?php echo form_close() ?>
 <br>
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 
 <?php echo anchor('/dashboard/master_gudang/view', 'Batal', 'class="btn btn-danger " role="button" '); ?> 
<?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-success', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>

</div>
