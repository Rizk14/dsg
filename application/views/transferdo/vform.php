<script language="javascript" type="text/javascript">

function cstopproduct() {
	var cval;
	if(document.getElementById('f_stop_produksi').checked) {
		document.getElementById('stop').value	= 1;
	} else {
		document.getElementById('stop').value	= 0;
	}
}

function ckform() {
	if(document.getElementById('optformatberkas').value=='0') {
		alert('Pilih format berkas yg akan di export!');
		document.getElementById('optformatberkas').focus();
		return false;
	}else if(document.getElementById('d_do_first').value=='') {
		alert('Tentukan Tgl. DO awal & akhir transaksi!');
		document.getElementById('d_do_first').focus();
		return false;
	}else if(document.getElementById('d_do_last').value=='') {
		alert('Tentukan Tgl. DO awal & akhir transaksi!');
		document.getElementById('d_do_last').focus();
		return false;	
	}else if(document.getElementById('customertransfer').value=='') {
		alert('Kode Transfer Pelanggan hrs dipilih!');
		document.getElementById('customertransfer').focus();
		return false;
	}else{
		return true;
	}
}

</script>

<script type="text/javascript" language="javascript">

$(document).ready(function(){
$("#d_do_first").datepicker();
$("#d_do_last").datepicker();
});

</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_transfer_do; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_transfer_do; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
			 <?php 
		   $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('transferdo/cform/gexportopvsdo', $attributes);?>
	
		<div id="mastertransferdoform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="24%"><?php echo $list_tgl_mulai_do_transfer_do; ?> </td>
							<td width="0%">&nbsp;</td>
							<td>
							<input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?=$tgldomulai?>" />
							<?php echo $list_tgl_akhir_do_transfer_do; ?>
							<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?=$tgldoakhir?>" />
							<span style="color:#FF0000">*</span>
							</td>
						  </tr>
						  <!--
						  <tr>
							<td>Pelanggan</td>
							<td></td>
							<td>
							<select name="customer" id="customer">
							 <option value="">[ PILIH PELANGGAN ]</option>
							 <?php
							 /*
							 foreach($customer as $rowcustomer) {
								 echo "<option value=\"$rowcustomer->i_customer\">".$rowcustomer->e_customer_name."</option>";
							 }
							 */ 
							 ?>
							</select>
							</td>
						  </tr>
						  -->
						  <tr>
							<td>Kode Transfer Pelanggan</td>
							<td></td>
							<td>
							<select name="customertransfer" id="customertransfer">
							 <option value="">[ PILIH KODE TRANSFER ]</option>
							 <?php
							 foreach($customertransfer as $rowcustomertransfer) {
								 echo "<option value=\"$rowcustomertransfer->i_customer_from\">".$rowcustomertransfer->i_customer_transfer."</option>";
							 }
							 ?>
							</select>
							</td>
						  </tr>						  
						  <tr>
						  	<td>Format Berkas/ File</td>
							<td>&nbsp;</td>
							<td>
							<select name="optformatberkas" id="optformatberkas">
								<option value="0">[ PILIH FORMAT BERKAS ]</option>
								<option value="1">Format DBF</option>
								<option value="2">Format Excel</option>
							</select>
							</td>
						  </tr>
						  <!-- 
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><label>
							  <input name="f_stop_produksi" type="checkbox" id="f_stop_produksi" value="1" onclick="cstopproduct();"/>
							<?php //echo $list_transfer_do_stop_produk; ?> </label></td> 
						  </tr> 
						  -->
						</table>				
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
			  </tr>
			  <tr>
			  	<td align="right">
				<input type="hidden" name="stop" id="stop" value="0" />
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_transfer; ?>" <?=$disabled?> onclick="return ckform();"/>
				</td>
			  </tr>
			  <tr>
			  	<td align="center" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#000033; font-weight:bold;"><?php echo $file; ?></td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
