<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript">

$(function()
{
	$("#goprint").click(function()
	{
		var date_from=$("#date_from").val(); 
		var date_to=$("#date_to").val(); 
		var supplier=$("#supplier").val(); 
		var urlnya = "<?php echo base_url(); ?>index.php/info-opvssj/cform/do_print/"+date_from+"/"+date_to+"/"+supplier;
		openCenteredWindow(urlnya);
	});
	
	$("#export_excel").click(function()
	{
		var date_from=$("#date_from").val(); 
		var date_to=$("#date_to").val(); 
		var supplier=$("#supplier").val(); 
		var urlnya = "<?php echo base_url(); ?>index.php/info-opvssj/cform/export_excel/"+date_from+"/"+date_to+"/"+supplier+"/1";
		openCenteredWindow(urlnya);
	});
	
	$("#export_ods").click(function()
	{
		var date_from=$("#date_from").val(); 
		var date_to=$("#date_to").val(); 
		var supplier=$("#supplier").val(); 
		var urlnya = "<?php echo base_url(); ?>index.php/info-opvssj/cform/export_excel/"+date_from+"/"+date_to+"/"+supplier+"/2";
		openCenteredWindow(urlnya);
	});

});

function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,menubar, left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

</script>

<h3>Laporan OP vs Bukti Penerimaan</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Supplier: <?php if ($supplier != '0') { echo $kode_supplier." - ".$nama_supplier; } else { echo "All"; } ?><br>
Periode: <?php
$date_from_exp = explode("-",$date_from,strlen($date_from)); 
echo $date_from_exp[0]."-".$date_from_exp[1]."-".$date_from_exp[2]; 
?> s.d 
<?php 
$date_to_exp = explode("-",$date_to,strlen($date_to));
echo $date_to_exp[0]."-".$date_to_exp[1]."-".$date_to_exp[2]; ?> <br>
<input type="hidden" name="date_from" id="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" id="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="supplier" id="supplier" value="<?php echo $supplier ?>" >
<input type="button" name="goprint" id="goprint" value="Print">
<input type="button" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="button" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="95%">
	<thead>
	 <tr class="judulnya">
		<th>Tgl </th>
		 <th>Nomor OP</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Qty OP</th>
		 <th>Bukti Penerimaan</th>
		 <th>Sisa OP</th>
	 </tr>
	</thead>
	<tbody>
		  	
		 <?php
		 
			if (is_array($query)) {
				
			 for($j=0;$j<count($query);$j++) {
				 
				$pisah1 = explode("-", $query[$j]['tgl_op']);
				 
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1=='01')
					$nama_bln = "Januari";
				else if ($bln1=='02')
					$nama_bln = "Februari";
				else if ($bln1=='03')
					$nama_bln = "Maret";
				else if ($bln1=='04')
					$nama_bln = "April";
				else if ($bln1=='05')
					$nama_bln = "Mei";
				else if ($bln1=='06')
					$nama_bln = "Juni";
				else if ($bln1=='07')
					$nama_bln = "Juli";
				else if ($bln1=='08')
					$nama_bln = "Agustus";
				else if ($bln1=='09')
					$nama_bln = "September";
				else if ($bln1=='10')
					$nama_bln = "Oktober";
				else if ($bln1=='11')
					$nama_bln = "November";
				else if ($bln1=='12')
					$nama_bln = "Desember";
				
				$tgl_op = $tgl1." ".$nama_bln." ".$thn1;
				 
				echo "<tr class=\"record\" valign=\"top\">";
				echo    "<td>".$tgl_op."</td>";
				echo    "<td>".$query[$j]['no_op']."</td>";
				echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 
				 echo "<td style='white-space:nowrap'>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++) {
				
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  
						  if($k<$hitung-1) {
						     echo "<br>";
						  }  
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++) {
						  echo $var_detail[$k]['qty'];
						  if($k<$hitung-1) {
						     echo "<br>";
						  }   
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++){
						  echo number_format($var_detail[$k]['pemenuhan'],'2','.',',');
						  if($k<$hitung-1) {
						     echo "<br>";
						  }
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++){
						  echo number_format($var_detail[$k]['sisa'],'2','.',',');
						  if ($k<$hitung-1) {
						     echo "<br>";
						  }
					}
				 }
				 echo "</td>";
				 
				 echo  "</tr>";

		 	}
		 	
		   }
		   
		 ?>


 	</tbody>
</table><br>

</div>
