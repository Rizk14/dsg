<style type="text/css">
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	window.print();
});

/*function printpr()
	{
		var OLECMDID = 7; */
		/* OLECMDID values:
		* 6 - print
		* 7 - print preview
		* 1 - open window
		* 4 - Save As
		*/
/*		var PROMPT = 1; // 2 DONTPROMPTUSER
		var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
		WebBrowser1.ExecWB(OLECMDID, PROMPT);
		WebBrowser1.outerHTML = "";
	} */
</script>

<table border="0" width="100%" class="tabelheader">
	<tr>
		<td colspan="2" align="center"><b>Laporan OP vs Bukti Penerimaan Barang<br>
		Periode: <?php echo $date_from; ?> s.d 
<?php echo $date_to; ?><br>
		Supplier: <?php if ($supplier != '0') echo $kode_supplier." - ".$nama_supplier; else echo "All"; ?>
		</b>
		</td>
	</tr>
</table>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%" class="isinya" align="center">
	<thead>
	 <tr>
		<th>Tgl </th>
		 <th>Nomor OP</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Qty OP</th>
		 <th>Bukti Penerimaan</th>
		 <th>Sisa OP</th>
	 </tr>
	</thead>
	<tbody>
		  	
		 <?php
		 
			if (is_array($query)) {
				
			 for($j=0;$j<count($query);$j++) {
				 
				$pisah1 = explode("-", $query[$j]['tgl_op']);
				 
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1=='01')
					$nama_bln = "Januari";
				else if ($bln1=='02')
					$nama_bln = "Februari";
				else if ($bln1=='03')
					$nama_bln = "Maret";
				else if ($bln1=='04')
					$nama_bln = "April";
				else if ($bln1=='05')
					$nama_bln = "Mei";
				else if ($bln1=='06')
					$nama_bln = "Juni";
				else if ($bln1=='07')
					$nama_bln = "Juli";
				else if ($bln1=='08')
					$nama_bln = "Agustus";
				else if ($bln1=='09')
					$nama_bln = "September";
				else if ($bln1=='10')
					$nama_bln = "Oktober";
				else if ($bln1=='11')
					$nama_bln = "November";
				else if ($bln1=='12')
					$nama_bln = "Desember";
				
				$tgl_op = $tgl1." ".$nama_bln." ".$thn1;
				 
				echo "<tr class=\"record\" valign=\"top\">";
				echo    "<td>".$tgl_op."</td>";
				echo    "<td>".$query[$j]['no_op']."</td>";
				echo    "<td>".$query[$j]['nama_supplier']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++) {
				
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  
						  if($k<$hitung-1) {
						     echo "<br>";
						  }  
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++) {
						  echo $var_detail[$k]['qty'];
						  if($k<$hitung-1) {
						     echo "<br>";
						  }   
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++){
						  echo $var_detail[$k]['pemenuhan'];
						  if($k<$hitung-1) {
						     echo "<br>";
						  }
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail'];
					 $hitung = count($var_detail);
					 
					for($k=0;$k<$hitung; $k++){
						  echo number_format($var_detail[$k]['sisa'],'2','.',',');
						  if ($k<$hitung-1) {
						     echo "<br>";
						  }
					}
				 }
				 echo "</td>";
				 
				 echo  "</tr>";

		 	}
		 	
		   }
		   
		 ?>


 	</tbody>
</table>
