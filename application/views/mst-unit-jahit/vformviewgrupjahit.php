<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Grup Jahit di Unit Jahit</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform/addgrupjahit">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform/viewgrupjahit">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-unit-jahit/cform/carigrupjahit'); ?>

<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="60%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Unit Jahit</th>
		 <th>Nama Grup Jahit</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
			
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td align='right'>$i &nbsp;</td>";
				 echo    "<td>&nbsp;".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 echo "<td>";
				   if (is_array($query[$j]['detailgrup'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailgrup'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
						  echo "&nbsp;".$var_detail[$k]['nama_grup'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					 }
				  }
				  echo "</td>";
				  
				  // ga perlu ada pengecekan data, karena yg disimpan di tabel tm_presentasi_kerja_unit_detail adalah nama grupnya
				 /*$ada = 0;
				 // tabel tm_presentasi_kerja_unit
				 $query3	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit WHERE kode_unit = '".$query[$j]['kode_unit']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 } */
				 				  				
				 echo    "<td align=center><a href=".base_url()."index.php/mst-unit-jahit/cform/editgrupjahit/".$query[$j]['kode_unit']." \" >Edit</a>";
				 //if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/mst-unit-jahit/cform/deletegrupjahit/".$query[$j]['kode_unit']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 
				 echo "</td>";
				 echo  "</tr>";
				 $i++;
		 	} // end for
		 }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
