<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function cek_data() {
	var unit_jahit= $('#unit_jahit').val();

	if (unit_jahit == '0') {
		alert("Unit Jahit harus dipilih..!");
		$('#unit_jahit').focus();
		return false;
	}
}

function addElement() {
  var ni = document.getElementById('myDiv');
  var newdiv = document.createElement('div');
  
  var jumgrup= $('#jumgrup').val();
  var jumgrupnew = parseInt(jumgrup)+1;
  $('#jumgrup').val(jumgrupnew);
    
  newdiv.innerHTML = "<input type='text' name='nama_grup_"+jumgrupnew+"' id='nama_grup_"+jumgrupnew+"'> ";
  ni.appendChild(newdiv);
}

function hapusElement() {
  var jumgrup= $('#jumgrup').val();
  $("#nama_grup_"+jumgrup).remove(); 
  var jumgrupnew = parseInt(jumgrup)-1;
  $('#jumgrup').val(jumgrupnew);
}
	
</script>

<h3>Data Grup Jahit di Unit Jahit</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform/addgrupjahit">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform/viewgrupjahit">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_grup', 'id' => 'f_grup');
	echo form_open('mst-unit-jahit/cform/updategrupjahit', $attributes); ?>
	<table>
		<tr>
			<td>Unit Jahit </td>
			<td> <select name="unit_jahit" id="unit_jahit">
				<option value="0">- Pilih -</option>
				<?php foreach ($list_unit as $jht) { ?>
					<option value="<?php echo $jht->kode_unit ?>" <?php if ($jht->kode_unit == $unit_jahit) { ?> selected <?php } ?> ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			  <td>Nama Grup Jahit&nbsp;&nbsp;</td>
			  <td> <input type="hidden" name="jumgrup" id="jumgrup" value="<?php echo count($query[0]['detailgrup']) ?>"> 
			  <div id="myDiv">
				<a href="javascript:addElement()">Tambah</a>&nbsp;<a href="javascript:hapusElement()">Hapus</a><br />
				
				<?php
					if (is_array($query[0]['detailgrup'])) {
					 $var_detail = array();
					 $var_detail = $query[0]['detailgrup'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
						  ?>
						  <input type="text" name="nama_grup_<?php echo $k+1 ?>" id="nama_grup_<?php echo $k+1 ?>" value="<?php echo $var_detail[$k]['nama_grup'] ?>">						  
					<?php
						  if ($k<$hitung-1)
						     echo "<br> ";
					 }
				  }
				?>
				
				</div>
			  </td>
			</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-unit-jahit/cform/viewgrupjahit'">
			
		</tr>
	</table>
<?php echo form_close(); ?> <br>
