<h3>Data SJ Masuk Barang WIP</h3><br>
<a href="<? echo base_url(); ?>index.php/wip/cform/addsjmasuk">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/wip/cform/viewsjmasuk">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
										
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************
				
		//*****stok*************************************
		var stok="#stok_"+n;
		var new_stok="#stok_"+no;
		$(stok, lastRow).attr("id", "stok_"+no);
		$(new_stok, lastRow).attr("name", "stok_"+no);		
		$(new_stok, lastRow).val('');
		//*****end stok*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****ket_detail*************************************
		var ket_detail="#ket_detail_"+n;
		var new_ket_detail="#ket_detail_"+no;
		$(ket_detail, lastRow).attr("id", "ket_detail_"+no);
		$(new_ket_detail, lastRow).attr("name", "ket_detail_"+no);		
		$(new_ket_detail, lastRow).val('');				
		//*****end ket_detail*************************************	
		
		//*****ket_warna*************************************
		var ket_warna="#ket_warna_"+n;
		var new_ket_warna="#ket_warna_"+no;
		$(ket_warna, lastRow).attr("id", "ket_warna_"+no);
		$(new_ket_warna, lastRow).attr("name", "ket_warna_"+no);		
		$(new_ket_warna, lastRow).val('');				
		//*****end ket_warna*************************************	
								
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
            
		 var  even_klik= "var x= $('#gudang').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/wip/cform/show_popup_brgjadi/'+ x+'/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_sj() {
	var no_sj= $('#no_sj').val();
	var tgl_sj= $('#tgl_sj').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (no_sj == '') {
			alert("Nomor SJ harus diisi..!");
			s = 1;
			return false;
		}
		if (tgl_sj == '') {
			alert("Tanggal SJ harus dipilih..!");
			s = 1;
			return false;
		}
		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if ($('#kode_'+k).val() == '') {
					alert("Data item barang tidak boleh ada yang kosong...!");
					s = 1;
					return false;
				}
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka..!");
					s = 1;
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/wip/cform/submitsjmasuk" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td width="15%">Lokasi Gudang</td>
		<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
  <tr>
    <td width="15%">Nomor SJ</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="10" maxlength="20" value="">
    </td>
    
  </tr>
  <tr>
    <td>Tanggal SJ</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>  
  <tr>
		<td>Jenis Masuk</td>
		<td> <select name="jenis_masuk" id="jenis_masuk">
				<option value="1" >Masuk bagus hasil jahit</option>
				<option value="2" >Lain-lain (Perbaikan dari unit jahit)</option>
				<option value="3" >Lain-lain (Retur dari unit packing)</option>
				<option value="4" >Lain-lain (Retur dari gudang jadi)</option>
				<option value="5" >Lain-lain (Lainnya)</option>
				</select>
		</td>
	</tr>
	<tr>
		<td>Unit Jahit</td>
		<td> <select name="kode_unit_jahit" id="kode_unit_jahit">
				<option value="0">Tidak Ada</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->kode_unit ?>" ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Unit Packing</td>
		<td> <select name="kode_unit_packing" id="kode_unit_packing">
				<option value="0">Tidak Ada</option>
				<?php foreach ($list_unit_packing as $pck) { ?>
					<option value="<?php echo $pck->kode_unit ?>" ><?php echo $pck->kode_unit."-".$pck->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
  <tr>
    <td>Keterangan</td>
    <td>
      <input name="ket" type="text" id="ket" size="30" value="">
    </td>  
  </tr>
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku" width="70%" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="6" align="right">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode & Nama Brg Jadi</th>
	      <th>Qty</th>
	      <th>Ket Qty Per Warna</th>
	      <th>Keterangan</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>		  
          <td nowrap="nowrap">
          <input name="nama_1" type="text" id="nama_1" size="50" readonly="true" value="" />
           <input name="kode_1" type="hidden" id="kode_1" value=""/>
           <input name="stok_1" type="hidden" id="stok_1" value=""/>
           <input title="browse data barang" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#gudang').val(); 
            openCenteredWindow('<?php echo base_url(); ?>index.php/wip/cform/show_popup_brgjadi/'+ x+'/1');" >
           </td>

          <td><input name="qty_1" type="text" id="qty_1" size="5" value="" style="text-align: right;" /></td>
          <td><input name="ket_warna_1" type="text" id="ket_warna_1" size="30" value="" /></td>
          <td><input name="ket_detail_1" type="text" id="ket_detail_1" size="30" value="" /></td>
          
        </tr>
	</table>	
	
	</form><br>
	<div align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_sj();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/wip/cform/viewsjmasuk'"></div>

     </td>
    </tr>

</table>
</div>
</form>
