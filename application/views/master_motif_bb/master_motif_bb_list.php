<?php
// Nomor urut data di tabel.
$per_page = 10;


if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Data Master motif Bahan Baku</h2>
    <hr>

    <!-- Paging dan form pencarian -->
    <div class="row navigasi_cari">
        <!-- Paging -->
        <div class="col-xs-12 col-md-6">
            <?php echo (!empty($paging)) ? $paging : ''?>
        </div>
        <!-- /Paging -->

        <!-- Form Pencarian -->
        <div class="col-xs-12 col-md-4 pull-right">
            <form method="get" action="<?php echo $form_action;?>" role="form" class="form-horizontal">
                <div class="input-group">
                    <input type="text" name="kata_kunci" class="form-control" placeholder="Masukkan nama atau kode motif pencarian" id="kata_kunci" value="<?php echo $this->input->get('kata_kunci')?>">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /Form Pencarian -->
    </div>
    <!-- /Paging dan form pencarian -->

    <?php if (!empty($master_motif_bb) && is_array($master_motif_bb)): ?>
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kode motif</th>
                    <th>Nama motif</th>
                 
                    <th>Tanggal Input</th>
                    <th>Tanggal Update</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
                foreach($master_motif_bb as $row):
				echo form_hidden('id', $row->id) ;
                 $link_edit = anchor('master_motif_bb/master_motif_bb/edit/'.$row->id, '<span class="glyphicon glyphicon-edit"></span>', array('title' => 'Edit'));
                 $link_hapus = anchor('master_motif_bb/master_motif_bb/hapus/'.$row->id,'<span class="glyphicon glyphicon-trash"></span>', array('title' => 'Hapus', 'data-confirm' => 'Anda yakin akan menghapus data ini?'));
                 $link_cetak = anchor('master_motif_bb/master_motif_bb/cetak/'.$row->id,'<span class="glyphicon glyphicon-print"></span>', array('title' => 'Cetak'));
                 ?>
                    <tr>
						
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $row->kode_motif_bb ?></td>
						<td><?php echo $row->nama_motif_bb ?></td>
					
                        <td><?php echo $row->created_at ?></td>
                        <td><?php echo $row->updated_at ?></td>
                        <td>
                            <?php echo  $link_edit.'&nbsp;&nbsp;&nbsp;&nbsp;'.$link_hapus  ?>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
	
	<div class="col-sm-offset-5">
            <form method="post" action="cetak" role="form" class="form-horizontal">
				 <input type="hidden" name="jumlah" value="<?php echo $offset ?>" />
				
                    <div class="btn-group" >
                    
                     <?php echo anchor('/master_motif_bb/master_motif_bb/', 'Tambah Item', 'class="btn btn-warning " role="button" '); ?>
					
                    </div>
            </form>
        </div>
	
        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $master_motif_bb ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->
