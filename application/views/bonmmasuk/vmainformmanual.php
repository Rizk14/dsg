<h3>Data Bon Masuk Pembelian (Manual)</h3><br>
<a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/manual">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewmanual">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
										
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');
		//*****end satuan*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);
		$(new_qty, lastRow).val('0');				
		//*****end qty*************************************	
		
		//*****qty*************************************
		var ket_det_="#ket_det_"+n;
		var new_ket_det_="#ket_det_"+no;
		$(ket_det_, lastRow).attr("id", "ket_det_"+no);
		$(new_ket_det_, lastRow).attr("name", "ket_det_"+no);
		$(new_ket_det_, lastRow).val('');				
		//*****end qty*************************************	
														
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var  even_klik= "var x= $('#gudang').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/bonmmasuk/cform/show_popup_brg/'+ x+'/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_bonm() {
	var no_bonm= $('#no_bonm').val();
	var tgl_bonm= $('#tgl_bonm').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (no_bonm == '') {
			alert("Nomor Bon M harus diisi..!");
			s = 1;
			return false;
		}
		if (tgl_bonm == '') {
			alert("Tanggal Bon M harus dipilih..!");
			s = 1;
			return false;
		}
		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if ($('#kode_'+k).val() == '') {
					alert("Data item bahan baku/pembantu tidak boleh ada yang kosong...!");
					s = 1;
					return false;
				}
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka..!");
					s = 1;
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/bonmmasuk/cform/submitmanual" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td width="15%">Lokasi Gudang</td>
		<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
  <tr>
    <td width="15%">Nomor Bon M</td>
    <td>
			<input name="no_bonm" type="text" id="no_bonm" size="15" maxlength="15" value="">
    </td>
    
  </tr>
  <tr>
    <td>Tanggal Bon M</td>
    <td>
	<label>
      <input name="tgl_bonm" type="text" id="tgl_bonm" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_bonm" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_bonm,'dd-mm-yyyy',this)">
	</td>
  </tr>  
  <tr>
    <td width="15%">Keterangan</td>
    <td>
			<input name="ket_head" type="text" id="ket_head" size="15" maxlength="15" value="">
    </td>
    
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku" width="60%" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="4" align="right">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="3%">No</th>
          <th>Kode & Nama Bhn Baku/Pembantu</th>
          <th>Satuan Awal</th>
          <th>Qty</th>
           <th>Keterangan</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>		  
          <td style="white-space:nowrap;">
          <input name="nama_1" type="text" id="nama_1" size="50" readonly="true" value="" />
           <input name="id_brg_1" type="hidden" id="id_brg_1" value=""/>
           <input name="kode_1" type="hidden" id="kode_1" value=""/>
           <input title="browse data barang" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#gudang').val(); 
            openCenteredWindow('<?php echo base_url(); ?>index.php/bonmmasuk/cform/show_popup_brg/'+ x+'/1');" >
           </td>
          <td><input name="satuan_1" type="text" id="satuan_1" size="5" readonly="true" value="" /></td>
		  <td><input style="text-align:right;" name="qty_1" type="text" id="qty_1" size="3" value="0" /></td>
          <td><input style="text-align:right;" name="ket_det_1" type="text" id="ket_det_1" size="15" value="" /></td>
        </tr>
	</table>	
	
	</form><br>
	<div align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_bonm();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewmanual'"></div>

     </td>
    </tr>

</table>
</div>
</form>
