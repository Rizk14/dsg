<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:400px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

</script>

<h3>Data Bon Masuk Pembelian (Manual)</h3><br>
<a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/manual">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewmanual">View Data</a>&nbsp;<br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('bonmmasuk/cform/carimanual'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;">Lokasi Gudang</td>
		<td style="white-space:nowrap;">: <select name="gudang" id="gudang">

				<?php 
						if($this->session->userdata('gid') != 16){
				?>
						<option value="0" <?php if ($cgudang == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php
				}
				?>



				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($cgudang == $gud->id) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>No Bon M</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>
<?php 
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;
	
	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
?>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_bonmmasuk" id="id_bonmmasuk">
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>Gudang</th>
		<th>No Bon M</th>
		 <th>Tgl Bon M</th>	
		 <th>Keterangan Header</th>	 
		 <th>List Bhn Baku/Pembantu</th>
		  <th>Keterangan Detail</th>
		 <th>Satuan Awal</th>
		 <th>Qty<br>Sat Awal</th>
		 <th>Last Update</th>
		 
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;

				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;

				$pisah1 = explode("-", $query[$j]['tgl_bonm']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_bonm = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['nama_gudang']."</td>";
				 echo    "<td>".$query[$j]['no_bonm']."</td>";
				 echo    "<td>".$tgl_bonm."</td>";
				 	echo    "<td>".$query[$j]['keterangan']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				   echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['keterangan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "<td>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 									  
				 echo    "<td align='center'>".$tgl_update."</td>";
				
				 
				 
				  if(($query[$j]['tanggal_periode']) > ($query[$j]['tgl_bonm'])){
				echo "<td><i>* bonM Manual sudah di closing </i></td>";
				
				}
				else{
				 echo    "<td align=center>";
				 // 05-08-2015, sementara dikomen dulu yg function editnya
				 echo "<a href=".base_url()."index.php/bonmmasuk/cform/editmanual/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$tgl_awal."/".$tgl_akhir."/".$cgudang."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>&nbsp";
				 echo "<a href=".base_url()."index.php/bonmmasuk/cform/deletemanual/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$tgl_awal."/".$tgl_akhir."/".$cgudang."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 echo "</td>";
			 }
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
