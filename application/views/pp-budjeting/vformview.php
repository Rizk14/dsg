<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan PP Budgeting</h3><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php if ($gudang != '0') { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else { echo "Semua"; } ?><br>
<!--
Kelompok: <?php if ($kelompok != '0') { echo $kode_perkiraan." - ".$nama_kelompok; } else { echo "All"; } ?><br>
Jenis Barang: <?php if ($jns_brg != '0') { echo $kode_jenis_brg." - ".$nama_jenis_brg; } else { echo "All"; } ?><br>
-->
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br>

Jenis Laporan: <?php echo $nama_jenis ?><br>

<!--
<i>*) Data yang ditampilkan sudah dalam satuan konversi<br>
*) Data harga adalah diluar pajak<br><br></i>
-->
<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('pp-budjeting/cform/exportexcel', $attributes); ?>

<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="hidden" name="kelompok" value="<?php echo $kelompok ?>" >
<input type="hidden" name="jenis_brg" value="<?php echo $jns_brg ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>

<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
<th>Nama Supplier</th>
		 <th>Nomor PP</th>
		 <th>Tgl PP</th>
		
		 <th>List Barang</th>
		 <th>Qty PP</th>
		  <th>Nomor OP</th>
		 <th>Tgl OP</th>
		 <th>Qty Pemenuhan OP</th>
<!--
		 <th>Harga Pemenuhan OP</th>
-->
		 <th>Nomor SJ</th>
		  <th>Tgl SJ</th>
		 <th>Qty Pemenuhan SJ</th>
		 <th>Jeda PP ke OP (Hari) </th>
		 <th>Jeda PP ke SJ (Hari) </th>
		  <th>Jeda OP ke SJ (Hari) </th>
	 </tr>
	
	</thead>
	<tbody>
		<?php
				if(is_array($query)){

					$hitung=count($query)-1;
				 for($j=0;$j<=$hitung;$j++){
					 echo "<tr>";
					  echo "<td nowrap>".$query[$j]['nama_supplier']."</td>";
				 echo "<td nowrap>".$query[$j]['no_pp']."</td>";
				 echo "<td nowrap>".$query[$j]['tgl_pp']."</td>";
				
				 echo "<td nowrap>".$query[$j]['nama_brg']."</td>";
				 echo "<td nowrap>".$query[$j]['qty_pp']."</td>";
				
				  echo "<td nowrap>".$query[$j]['no_op']."</td>";
				 echo "<td nowrap>".$query[$j]['tgl_op']."</td>";
				 echo "<td nowrap>".$query[$j]['qty_op']."</td>";
				 //~ echo "<td nowrap>".$query[$j]['harga_op']."</td>";
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
					for($k=0;$k<$hitung_det; $k++){
						  echo $var_detail[$k]['no_sj'];
						  if ($k<$hitung_det-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  echo $var_detail[$k]['tgl_sj'];
						  if ($k<$hitung_det-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				  echo "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  echo $var_detail[$k]['qty_sj'];
						  if ($k<$hitung_det-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "<td nowrap>".$query[$j]['jeda_pp_op']."</td>";
				  echo "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  echo $var_detail[$k]['jeda_pp_sj'];
						  if ($k<$hitung_det-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				   echo "<td nowrap>";
				 if (is_array($query[$j]['data_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_detail'];
					 $hitung_det = count($var_detail);
						for($k=0;$k<$hitung_det; $k++){
						  echo $var_detail[$k]['jeda_op_sj'];
						  if ($k<$hitung_det-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "</tr>";
				}
			}	
				
			
				 ?>
 	</tbody>
</table><br>
</div>
