<script language="javascript">
	function factived() {
		if(document.getElementById('f_active').checked==true){
			document.getElementById('f_active_hidden').value = 't';
		}else{
			document.getElementById('f_active_hidden').value = 'f';
		}
	}
	
	function konfirm() {
		if(document.getElementById('vprice').value=='') {
			alert('Kolom harga hrs diisi!');
			return false;
		}else{
			if(!parseFloat(document.getElementById('vprice').value)) {
				alert('Kolom harga hrs diisi angka!');
				return false;
			}
		}
	}
	
</script>

<div id="tabs">
	<div id="tab2" class="tab_harga" style="margin-left:1px;" align="center" onClick="javascript: displayPanelHarga('2');">FORM HARGA BRG</div>
</div>
         
<div class="tab_bdr_harga"></div>

	<div class="panel_harga" id="panel2" style="display:block;">
	    <table>
	      <tr>
	    	<td align="left">
		      <?php
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('hargabrg/cform/actedit', $attributes);?>
		      
			
		<div id="masterpriceform">
		      <table>
				  <input type="hidden" name="i_customer" id="i_customer" value="<?php echo $i_customer ?>">
				  <!-- 21-03-2014 dikomen -->
			     <!-- <tr>
					  <td>Pelanggan</td>
					  <td>:</td>
					  <td>
						<?php
						  
						/*  $j = 0;
						  	
						  $lpelanggan	.= "<select name=\"i_customer\" id=\"i_customer\" >"; 
						  $lpelanggan	.= "<option value=\"\">[ Pilih Pelanggan ]</option>";
						  
						  foreach($opt_pelanggan as $key => $row) {	
							  
						  	$sel = $i_customer==$row->code?" selected ":" ";
						  	
						  	if($i_customer==0 && $j==0) {
								$lpelanggan .= $i_customer==0?"<option value=\"$i_customer\" selected >Default </option>":"";
							}
													  	
							$lpelanggan .= "<option value=\"$row->code\" $sel >".$row->customer."</option>";
							
							$j=$j+1;
						  }
						$lpelanggan	.= "</select>";
						echo $lpelanggan; */
 						?>
					  </td>
				  </tr>	-->
				  <tr>
					  <td width="60%">Kode Harga</td>
					  <td width="1%">:</td>
					  <td>
						<?php 
						$kdharga = array(
								  'name'=>'iproductprice',
								  'id'=>'iproductprice',
								  'value'=>$iprice,
								  'maxlength'=>'15',
								  'readonly'=>'true');
								  
						echo form_input($kdharga); ?>
						<?php 
						$kdharga2 = array(
								  'name'=>'iproductprice2',
								  'id'=>'iproductprice2',
								  'maxlength'=>'15',
								  'value'=>$iproductprice,
								  'readonly'=>'true');
						echo form_input($kdharga2); ?>						
					  </td>
				  </tr>
			      <tr>
					  <td>Kode Barang</td>
					  <td>:</td>
					  <td>
						<?php 
						$kdproduk = array(
								'name'=>'iproduct',
								'id'=>'iproduct',
								'value'=>$i_product,
							  	'onclick'=>"kodebarang(document.getElementById('i_customer'));",	
								'maxlength'=>'15',
								'readonly'=>'true');
								
						echo form_input($kdproduk); ?>
						<img name="img_i_product_" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Tampilkan Kode Barang" class="imgLink" align="absmiddle" onclick="kodebarang();">
					  </td>
				  </tr>
			      <tr>
					  <td>Kode Barang + Motif</td>
					  <td>:</td>
					  <td>
						<?php 
						$kdproduk = array(
								  'name'=>'iproductmotif',
								  'id'=>'iproductmotif',
								  'value'=>$i_product_motif,	
								  'maxlength'=>'15',
								  'readonly'=>'true');
								  
						echo form_input($kdproduk); ?>
						<input type="hidden" name="iproductmotifhidden" id="iproductmotifhidden" value="<?=$i_product_motif?>" />
					  </td>
				  </tr>			  				  
			      <tr>
					  <td>Harga</td>
					  <td>:</td>
					  <td>
						<?php
						$vprice = array(
								  'name'        => 'vprice',
								  'id'          => 'vprice',
								  'value'       => $v_price,
								  'maxlength'   => '50');
								  
						echo form_input($vprice); ?>
						<input type="hidden" name="vpricehidden" id="vpricehidden" value="<?php echo $v_price; ?>"/>
					  </td>
			      </tr>
			      <tr>
					  <td>Status Harga</td>
					  <td>:</td>
					  <td>
						<input type="checkbox" name="f_active" id="f_active" value="<?=$f_active?>" <?php if($f_active=='t'){ echo "checked";} ?> onclick="factived();" /> aktif
					  </td>
			      </tr>			      
			      <tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>
					  	<input type="hidden" name="iproductprice_hidden" id="iproductprice_hidden" value="<?=$iproductprice?>"/>
						<input type="hidden" name="iprice_hidden" id="iprice_hidden" value="<?=$iprice?>"/>
						<input type="hidden" name="i_customer_hidden" id="i_customer_hidden" value="<?=$i_customer?>"/>
						<input type="hidden" name="f_active_hidden" id="f_active_hidden" value="<?=$f_active?>"/>
						<input name="btnsimpan" id="btnsimpan" value="Simpan" type="submit" onclick="return konfirm();" />
						<input name="btnbatal" id="btnbatal" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/hargabrg/cform'" />
					  </td>
			      </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
			</td>
	      </tr> 
	    </table>
	</div>
