<script language="javascript" type="text/javascript">

function shprodukfpenjualandolar(iteration) {
	lebar =450;
	tinggi=400;
	eval('window.open("<?php echo site_url(); ?>"+"/listpenjualandolar/cform/listsjp/"+iteration+"/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}

function validNum(column,iterasi) {

	var angka	= document.getElementById(column+'_'+iterasi);
	
	if (isNaN(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka!");
		angka.value	= 0;
		angka.focus();
	}
}

/*************************
Faktur Penjualan :D

Kasus :
total nilai = 10000
Diskon 	= 2 % dalam nilai 5000


Perhitungan :
Total 	= 10000 - (5000*2%)
		= 10000 - 4900
		= 5100

PPN 10 %	= 5100 + (5100*10%=510)

Grand Total	= 5610


-----------------------------------------------------
Koreksi kolom "dlm nilai" ===> "total hasil nilai"

Kasus :
total nilai = 10000
Diskon = 2 % hasil nilai = "9800"


Perhitungan :
nilai diskon 2 %	= (10000*2%)
					= 200

Total 		= 10000 - 200
			= 9800
			
PPN 10 %	= 9800*10%)
			= 980

Grand Total	= 9800 + 980
			= 10780
		
**************************/

function tnilaibarang() {

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_faktur');

	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseInt(unitprice.value);
		}
	}
	
	totalnilai.value	= Math.round(totaln);
}

function total(iterasi) {
	
	var	total;
	var	price;
	var	unit0;
	
	price	= document.getElementById('v_hjp_tblItem_'+iterasi);
	unit0	= document.getElementById('n_quantity_tblItem_'+iterasi);
	
	if(parseInt(price.value) && parseInt(unit0.value)) {
		total	= parseInt(price.value) * parseInt(unit0.value);
		document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
	}
}

function addRowToTable(nItem) {
	
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	
	tbl.width='100%';
										
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	}else{
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:12px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:120px;\"><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:80px;\" onfocus=\"total("+iteration+");tnilaibarang();\"><img name=\"img_i_product_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualandolar('"+iteration+"');\"></DIV>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:200px;\" ><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:240px;\"></DIV>";
	
	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_v_hjp_"+nItem+"_"+iteration+"\" style=\"width:95px;\" ><input type=\"text\" ID=\"v_hjp_"+nItem+"_"+iteration+"\"  name=\"v_hjp_"+nItem+"_"+iteration+"\" style=\"width:100px;text-align:right;\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_n_quantity_"+nItem+"_"+iteration+"\" style=\"width:95px;\" ><input type=\"text\" ID=\"n_quantity_"+nItem+"_"+iteration+"\"  name=\"n_quantity_"+nItem+"_"+iteration+"\" style=\"width:100px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();validNum('n_quantity_tblItem','"+iteration+"');\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(5);
	cell1.innerHTML = "<DIV ID=\"ajax_v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:95px;text-align:right;\" ><input type=\"text\" ID=\"v_unit_price_"+nItem+"_"+iteration+"\"  name=\"v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:100px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();validNum('v_unit_price_tblItem','"+iteration+"')\" value=\"0\" ><input type=\"hidden\" name=\"iteration\" value=\""+iteration+"\" ><input type=\"hidden\" ID=\"isjcode_"+nItem+"_"+iteration+"\"  name=\"isjcode_"+nItem+"_"+iteration+"\"><input type=\"hidden\" ID=\"n_quantity_hidden_"+nItem+"_"+iteration+"\"  name=\"n_quantity_hidden_"+nItem+"_"+iteration+"\" ></DIV>";
}


function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}

</script>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#d_faktur").datepicker();
	$("#d_due_date").datepicker();
	$("#d_pajak").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat">Info Penjualan Barang (Dalam Dolar)</td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form Info Penjualan Barang (Dalam Dolar)</td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
			<?php 
		  	$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listpenjualandolar/cform/actedit', $attributes);?>
		 
		<div id="masterfpenjualandoform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $form_nomor_f_fpenjualanndo; ?> : 
							  <input name="i_faktur" type="text" id="i_faktur" maxlength="13" value="<?php echo $ifakturcode; ?>" onkeyup="ckfpenjualan(this.value);"/>
							  <div id="confnomorfpenj" style="color:#FF0000;"></div>
							  </td>
							<td width="33%"><?php echo $form_tgl_f_fpenjualanndo; ?> : 
							  <input name="d_faktur" type="text" id="d_faktur" maxlength="10" value="<?php echo $tgFAKTUR; ?>" />
							  <!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_faktur,'dd/mm/yyyy',this)"> -->
							  </td>
							<td width="33%"><?php echo $form_cabang_fpenjualanndo; ?> : 
							<input type="text" name="nama_pelanggan" id="nama_pelanggan" value="<?php echo $ibranchname ?>">
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>					
					<tr>
					 <td>
					  <div id="title-box2">DETAIL FAKTUR
					  <div style="float:right;">
					  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');total(document.getElementById('tblItem').rows.length);"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
					  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');total((document.getElementById('tblItem').rows.length)-1);"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
					  </div></div>
					 </td>	
					</tr>
					<tr>
					  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="4%" class="tdatahead">NO</td>
                          <td width="15%" class="tdatahead"><?php echo $form_kd_brg_fpenjualanndo; ?></td>
                          <td width="26%" class="tdatahead"><?php echo $form_nm_brg_fpenjualanndo; ?></td>
                          <td width="14%" class="tdatahead">HARGA ($)</td>
                          <td width="14%" class="tdatahead"><?php echo $form_qty_fpenjualanndo; ?></td>
                          <td width="14%" class="tdatahead"><?php echo $form_nilai_fpenjualanndo; ?> ($)</td>
                        </tr>
						<tr>
						 <td colspan="6">
						  <table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							<?php
							$iter	= 0;
							$xtotaln	= 0;
							foreach($fakturitem as $row3) {
								
								$xtotaln+=$row3->amount;	
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:12px;margin-right:0px;\">".($iter*1+1)."</div></td>
									<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:120px;\"><input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:80px;\" onfocus=\"total('".$iter."');tnilaibarang();\" value=\"".$row3->i_product."\"><img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualandolar('".$iter."');\"></td>
									
									<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:200px;\" ><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\" name=\"e_product_name_tblItem_".$iter."\" style=\"width:240px;\" value=\"".$row3->e_product_name."\"></DIV></td>
									
									<td><DIV ID=\"ajax_v_hjp_tblItem_".$iter."\" style=\"width:95px;\" ><input type=\"text\" ID=\"v_hjp_tblItem_".$iter."\"  name=\"v_hjp_tblItem_".$iter."\" style=\"width:100px;text-align:right;\" value=\"".$row3->v_unit_price."\" onkeyup=\"total('".$iter."');tnilaibarang();\" ></DIV></td>
									
									<td><DIV ID=\"ajax_n_quantity_tblItem_".$iter."\" style=\"width:95px;\" ><input type=\"text\" ID=\"n_quantity_tblItem_".$iter."\"  name=\"n_quantity_tblItem_".$iter."\" style=\"width:100px;text-align:right;\" onkeyup=\"total('".$iter."');tnilaibarang();validNum('n_quantity_tblItem','".$iter."');\" value=\"".$row3->n_quantity."\" ></DIV></td>
									<td><DIV ID=\"ajax_v_unit_price_tblItem_".$iter."\" style=\"width:95px;text-align:right;\" ><input type=\"text\" ID=\"v_unit_price_tblItem_".$iter."\"  name=\"v_unit_price_tblItem_".$iter."\" style=\"width:100px;text-align:right;\" onkeyup=\"total('".$iter."');tnilaibarang();validNum('v_unit_price_tblItem','".$iter."');\" value=\"".$row3->amount."\" ><input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\"><input type=\"hidden\" ID=\"n_quantity_hidden_tblItem_".$iter."\"  name=\"n_quantity_hidden_tblItem_".$iter."\" ></DIV></td>
								</tr>";
								$iter++;
							}		
										
							?>						  
						  </table>
						</tr>
                      </table></td>
					</tr>					
					<tr>
					 <td>
					  <div id="title-box2">
					  <div style="float:right;">
					  &nbsp;
					  </div></div>
					 </td>	
					</tr>					
					<tr>
					  <td valign="top"><table width="98%" border="0" cellpadding="0" cellspacing="0">
						<tr valign="top">
						  <td width="16%" rowspan="2"><?php echo $form_ket_f_fpenjualanndo; ?></td>
						  <td width="1%">:</td>
						  <td colspan="2" rowspan="2">
						  <?php
						  	$enotefaktur = array(
								'name'=>'e_note_faktur',
								'id'=>'e_note_faktur',
								'cols'=>2,
								'rows'=>1,
								'value'=>$e_note_faktur
							);
							echo form_textarea($enotefaktur);
						  ?></td>
						  <td width="14%">Grand Total</td>
						  <td width="1%">:</td>
						  <td width="21%">
							<input name="v_total_faktur" type="text" id="v_total_faktur" size="15" maxlength="15" value="<?=$xtotaln?>" /></td>
						</tr>
						
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
												
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						
					  </table></td>
					</tr>
					<tr align="right">
					  <td>

						<input type="hidden" name="ifakturhiden" id="ifakturhiden" value="<?=$i_faktur?>" />
						<input type="hidden" name="ifakturcodehiden" id="ifakturcodehiden" value="<?=$ifakturcode?>" />
						
						<input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit" />
						<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listpenjualandolar/cform/'">

					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
