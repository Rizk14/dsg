<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat">Info Penjualan Barang (Dalam Dolar)</td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form Info Pembebanan Biaya Jahit</td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listpenjualandolar/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlpenjualanndoform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%">Nomor Faktur</td>
					<td width="1%">:</td>
					<td>
					  <input name="no_faktur" type="text" id="no_faktur" maxlength="14" value="<?=$nofaktur?>"/>
					</td>
				  </tr>
				</table></td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			<tr>
			  <td>
			  <div id="title-box2">DETAIL PENJUALAN (DLM DOLAR)</div></td>	
			</tr>
			<tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="12%" class="tdatahead">KODE BARANG</td>
					<td width="35%" class="tdatahead">NAMA BARANG</td>
					<td width="11%" class="tdatahead">HARGA ($)</td>
					<td width="10%" class="tdatahead">QTY</td>
					<td width="13%" class="tdatahead">NILAI ($)</td>
					<td width="2%" class="tdatahead" align="center"><?php echo $link_aksi; ?></td>
				  </tr>
				  <?php
				  
				  $totalqty	= 0;
				  $totalpenjualan	= 0;
				  $nomororder	= array();
				  $arr	= 0;
								  
				  if(sizeof($query)>0) {
					  
					  $no	= 1;
					  $cc	= 1;
					  
					  foreach($query as $row) {
						
						$Classnya	= (($cc%2)==0)?"row1":"row2";
						$bgcolor	= (($cc%2)==0)?"#E4E4E4":"#E4E4E4";

						$jmlFaktur	= $this->mclass->jmlFaktur($row->i_faktur_code);
						$njmlFaktur	= $jmlFaktur->num_rows();
												
						$nomororder[$arr]	= $row->i_faktur_code;

						if($cc==1) {
							$link_act	= "<td align=\"center\" rowspan='$njmlFaktur'>
							<a href=".base_url()."index.php/listpenjualandolar/cform/edit/".$row->i_faktur_code." title=\"Edit Faktur\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Faktur\" border=\"0\"></a>&nbsp;
							
							&nbsp;
							<a href=".base_url()."index.php/listpenjualandolar/cform/undo/".$row->i_faktur_code."/".$row->i_faktur."/"." title=\"Cancel faktur\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel faktur\" border=\"0\"></a></td>";

						}elseif($cc>1 && $nomororder[$arr-1]!=$row->i_faktur_code) {
							$link_act	= "<td align=\"center\" rowspan='$njmlFaktur'>
						<a href=".base_url()."index.php/listpenjualandolar/cform/edit/".$row->i_faktur_code." title=\"Edit Faktur\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Faktur\" border=\"0\"></a>&nbsp;
							
							&nbsp;
							<a href=".base_url()."index.php/listpenjualandolar/cform/undo/".$row->i_faktur_code."/".$row->i_faktur."/"." title=\"Cancel faktur\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel faktur\" border=\"0\"></a></td>";
					
						}else{
							$link_act	= '';
						}
						
						$lpenjndo .= " <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\" onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no."</td>
								<td>".$row->imotif."</td>
								<td>".$row->motifname."</td>
								<td align=\"right\">".number_format($row->unitprice,'2','.',',')."</td>
								<td align=\"right\">".$row->qty."</td>
								<td align=\"right\">".number_format($row->amount,'2','.',',')."</td>
								$link_act								
							  </tr>";
							  
							  $no+=1;
							  $cc++;
							  $totalqty+=$row->qty;
							  $totalpenjualan+=$row->amount;
							  $arr+=1;
							  
					  }
					  
					  echo $lpenjndo;
				  }
				  
				  ?>
				  
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right"><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="40%" align="right"><?php echo $list_penjualanndo_total_pengiriman; ?> </td>
					<td width="1%">:</td>
					<td width="20%">
					  <input name="v_t_pengiriman" type="text" id="v_t_pengiriman" maxlength="50" value="<?php echo $totalqty; ?>"/>
					</td>
					<td width="20%" align="right"><?php echo $list_penjualanndo_total_penjualan; ?></td>
					<td width="1%">:</td>
					<td width="20%" align="right">
					  <input name="v_t_penjualan" type="text" id="v_t_penjualan" maxlength="50" value="<?php echo number_format($totalpenjualan,'2','.',','); ?>" style="text-align:right;" />
					</td>
					</tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
					<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listpenjualandolar/cform/'">

				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
