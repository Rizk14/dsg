<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Stok Opname Bahan Hasil Cutting</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">
 $(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
				
		//*****kode_brg_wip*************************************
		var kode_brg_wip="#kode_brg_wip_"+n;
		var new_kode_brg_wip="#kode_brg_wip_"+no;
		$(kode_brg_wip, lastRow).attr("id", "kode_brg_wip_"+no);
		$(new_kode_brg_wip, lastRow).attr("name", "kode_brg_wip_"+no);		
		$(new_kode_brg_wip, lastRow).attr("onkeyup", "cari('"+no+"', this.value, '0');");		
		$(new_kode_brg_wip, lastRow).val('');		
		//*****end kode_brg_wip*********************************
		
		//******div infobrgwip*************************************
		var infobrgwip="#infobrgwip_"+n;
		var new_infobrgwip="#infobrgwip_"+no;
		$(infobrgwip, lastRow).attr("id", "infobrgwip_"+no);

		$(new_infobrgwip, lastRow).html("<input type='hidden' id='id_brg_wip_"+no+"' name='id_brg_wip_"+no+"' value=''>"+
		"<input type='text' id='nama_brg_wip_"+no+"' name='nama_brg_wip_"+no+"' value='' readonly='true' size='30'>");
		
		//******div qtywarna*************************************
		var qtywarna="#qtywarna_"+n;
		var new_qtywarna="#qtywarna_"+no;
		$(qtywarna, lastRow).attr("id", "qtywarna_"+no);
		$(new_qtywarna, lastRow).html("<input type='hidden' id='temp_qty_"+no+"' name='temp_qty_"+no+"' value=''>");
		
		//*****ket_detail*************************************
		var ket_detail="#ket_detail_"+n;
		var new_ket_detail="#ket_detail_"+no;
		$(ket_detail, lastRow).attr("id", "ket_detail_"+no);
		$(new_ket_detail, lastRow).attr("name", "ket_detail_"+no);		
		$(new_ket_detail, lastRow).val('');				
		//*****end ket_detail*************************************	
								
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});

function cari(posisi, kodebrgwip, isedit) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/stok-opname-hsl-cutting/cform/caribrgwip', 
				data: 'kode_brg_wip='+kodebrgwip+'&posisi='+posisi+'&isedit='+isedit, success: function(response) {
					$("#infobrgwip_"+posisi).html(response);
			}}); 
	
	/*$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/bonmmasuk-hsl-cutting/cform/additembhnbaku', 
				data: 'kode_brg_jadi='+kodebrgjadi+'&posisi='+posisi, success: function(response) {
					$("#qtybhnbaku_"+posisi).html(response);
			}}); */
		
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/stok-opname-hsl-cutting/cform/additemwarna', 
				data: 'kode_brg_wip='+kodebrgwip+'&posisi='+posisi+'&isedit='+isedit, success: function(response) {
					$("#qtywarna_"+posisi).html(response);
			}});
}

function cek_input() {
	var jum_data = $('#no').val();
	var tgl_so = $('#tgl_so').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data SO ??");
	
	if (kon) {
		if(tgl_so == '') {				
			alert("Tanggal SO harus dipilih...!");
			s=1;
			return false;
		}
		
			for (var k=1; k <= jum_data-1; k++) {
				if($('#id_brg_wip_'+k).val() == '') {				
					alert("Data barang harus dipilih...!");
					s=1;
					return false;
				}
				if ($('#temp_qty_'+k).val() == '') {
					alert("Data item barang "+ $('#kode_brg_wip_'+k).val() +" belum ada warnanya, silahkan input dulu di menu Master Warna Brg WIP...!");
					s = 1;
					return false;
				}
			}
		if (s == 0)
			return true;
	}
	else
		return false;
	
}

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
</script>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?> (<i>* Belum ada data stok opname sama sekali</i>) <br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('stok-opname-hsl-cutting/cform/submit', $attributes);
 ?>
<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="jum_data" id="jum_data" value="0">
<input type="hidden" name="is_pertamakali" id="is_pertamakali" value="1">
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">

Tanggal Pencatatan SO: 
      <input name="tgl_so" type="text" id="tgl_so" size="10" value="<?php echo $tgl_so ?>" readonly="true">
	   <img alt="" id="tgl_so" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_so,'dd-mm-yyyy',this)">
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="70%" id="tabelku">
	<tr>
			<td colspan="6" align="right">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
	</tr>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang WIP</th>
		 <th>Jml Fisik<br>Per Warna</th>
	 </tr>
	<tr align="center">
          <td align="center" id="num_1">1</td>
          <td style="white-space:nowrap;"><input name="kode_brg_wip_1" type="text" id="kode_brg_wip_1" size="8" value="" onkeyup="cari('1',this.value, '0');"/></td>
          
          <td style="white-space:nowrap;"><div id="infobrgwip_1">
			  <input name="nama_brg_wip_1" type="text" id="nama_brg_wip_1" size="30" value="" readonly="true"/>
			  <input name="id_brg_wip_1" type="hidden" id="id_brg_wip_1" value=""/>
			  </div>
          </td>

          <td><div id="qtywarna_1" align="right">&nbsp;<input type="hidden" id="temp_qty_1" name="temp_qty_1" value=""></div>
			<hr>
		  </td>
          
        </tr>
</table><br>
<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return cek_input();">&nbsp;
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform'">
<?php echo form_close();  ?>
</div>
