<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Stok Opname Bahan Hasil Cutting</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

/*function cek_input() {
	var jum_data = $('#jum_data').val();
	if (jum_data == 0) {
		alert("Data barang tidak ada..!");
		return false;
	}
} */
function cek_input() {
	var jum_data = $('#jum_data').val();
	var tgl_so = $('#tgl_so').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data SO ??");
	
	if (kon) {
		if (tgl_so == '') {
			alert("Tanggal pencatatan SO harus dipilih..!");
			s = 1;
			return false;
		}
		
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
			// sementara dikomen
			/*	if($('#ada_warna_'+k).val() == '') {				
					alert("Ada data barang yang belum ada warnanya, silahkan input dulu di menu Master Warna Brg Jadi WIP...!");
					s=1;
					return false;
				}
				if($('#ada_bhn_'+k).val() == '') {				
					alert("Ada data barang yang belum ada material bahan baku/pembantu, silahkan input dulu di menu Master Material Barang Jadi...!");
					s=1;
					return false;
				} */
				
			// ------------------------------------------------------
				
				/*if($('#stok_fisik_'+k).val() == '') {				
					alert("Data stok tidak boleh kosong, jika kosong maka harus isikan angka 0...!");
					s=1;
					return false;
				}
				if (isNaN($('#stok_fisik_'+k).val()) ) {
					alert("Data stok harus berupa angka atau desimal..!");
					s=1;
					return false;
				} */
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}

</script>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('stok-opname-hsl-cutting/cform/submit', $attributes);
$no = count($query);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">

Tanggal Pencatatan SO: 
      <input name="tgl_so" type="text" id="tgl_so" size="10" value="<?php echo $tgl_so ?>" readonly="true">
	   <img alt="" id="tgl_so" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_so,'dd-mm-yyyy',this)">
<br><br>

<input type="hidden" name="jum_data" id="jum_data" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		<th>Kode Barang</th>
		<th>Nama Barang WIP</th>
		<th>Jml Fisik<br>Per Warna</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg_wip']."</td><td> ".$query[$j]['nama_brg_wip']."
					 <input type='hidden' name='id_brg_wip_$i' id='id_brg_wip_$i' value='".$query[$j]['id_brg_wip']."'></td>";
					
					// 28-11-2014
					echo "<td style='white-space:nowrap;' align='right'>";
					if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
							for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp;";
					?>
					<input type="text" name="stok_fisik_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['stok_opname'] ?>" size="3" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
					<input type="hidden" name="id_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['id_warna'] ?>">
					<input type="hidden" name="stok_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['saldo_akhir'] ?>"><br>
					<?php 
							}
					?>
						<input type="hidden" name="ada_warna_<?php echo $i ?>" id="ada_warna_<?php echo $i ?>" value="ada">
					<?php
					}
					else {
					?>
						<input type="hidden" name="ada_warna_<?php echo $i ?>" id="ada_warna_<?php echo $i ?>" value="">
					<?php } ?>
					</td>					
				<?php	 
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>
<!--<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform'">-->

<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Stok Opname Periode Ini" onclick="return confirm('Yakin akan hapus SO periode ini ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform/'">

<?php echo form_close();  ?>
</div>
