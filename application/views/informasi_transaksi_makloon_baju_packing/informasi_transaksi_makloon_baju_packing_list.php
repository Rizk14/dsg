

<?php
// Nomor urut data di tabel.
$per_page = 10;

// Posisi nomor halaman (untuk paging) jika user login / tidak.
$login_status = $this->session->userdata('login_status');
$user_bagian = $this->session->userdata('user_bagian');

if ($login_status ==  true && $user_bagian == '2') {
    $page = $this->uri->segment(4);
} else {
    $page = $this->uri->segment(3);
}


if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Informasi Transaksi Makloon Baju di Unit Packing</h2>
    <hr>

    <?php if (!empty($informasi_transaksi_makloon_baju_packing) && is_array($informasi_transaksi_makloon_baju_packing)): ?>
    <div class="row">
        <div class="col-md-12">
 <form action ="export_csv" name="myform" id='myform' class="form-inline" method="post">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>NO SJ</th>
                    <th>Tanggal SJ</th>
					<th>Masuk Bagus</th>
                    <th>Masuk Lain - lain</th>
                    <th>Keluar Bagus</th>
                    <th>Keluar Lain - lain</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
              
               $tot_masuk_bagus_packing=0;$tot_masuk_lain_packing=0;$tot_keluar_bagus_packing=0;$tot_keluar_lain_packing=0;
               for($i=0;$i<count($informasi_transaksi_makloon_baju_packing);$i++){
				 ?>
                    <tr>
						
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $informasi_transaksi_makloon_baju_packing[$i]['no_sj'] ;?></td>
						<td><?php echo $informasi_transaksi_makloon_baju_packing[$i]['tanggal_sj'] ;?></td>
						<td align='center'><?php if($informasi_transaksi_makloon_baju_packing[$i]['masuk_bagus_packing']) echo $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;?></td>
                        <td align='center'><?php if($informasi_transaksi_makloon_baju_packing[$i]['masuk_lain_packing']) echo $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;?></td>
                        <td align='center'><?php if($informasi_transaksi_makloon_baju_packing[$i]['keluar_bagus_packing']) echo $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;?></td>
                        <td align='center'><?php if($informasi_transaksi_makloon_baju_packing[$i]['keluar_lain_packing']) echo $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;?></td>
                    </tr>
					
						<?php 
						if($informasi_transaksi_makloon_baju_packing[$i]['masuk_bagus_packing']==1) {
						$tot_masuk_bagus_packing += $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;
						}
						if($informasi_transaksi_makloon_baju_packing[$i]['masuk_lain_packing']==1) {
						$tot_masuk_lain_packing += $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;
						}
						if($informasi_transaksi_makloon_baju_packing[$i]['keluar_bagus_packing']==1) {
						$tot_keluar_bagus_packing += $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;
						}
						if($informasi_transaksi_makloon_baju_packing[$i]['keluar_lain_packing']==1) {
						$tot_keluar_lain_packing += $informasi_transaksi_makloon_baju_packing[$i]['qty'] ;
						}
						?>
						
                <?php } ?>
					<tr>
						<td align='center' colspan='3'>TOTAL</td>
						<td align='center'><?php echo $tot_masuk_bagus_packing ;?></td>	
						<td align='center'><?php echo $tot_masuk_lain_packing ;?></td>	
						<td align='center'><?php echo $tot_keluar_bagus_packing ;?></td>	
						<td align='center'><?php echo $tot_keluar_lain_packing ;?></td>	
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="tanggal_sj_dari" id="tanggal_sj_dari" value="<?php echo $tanggal_sj_dari ?>">
             <input type="hidden" name="tanggal_sj_ke" id="tanggal_sj_ke" value="<?php echo $tanggal_sj_ke ?>">
               <input type="hidden" name="unit_packing" id="unit_packing" value="<?php echo $unit_packing ?>">

</form>

        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $informasi_transaksi_makloon_baju_packing ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



