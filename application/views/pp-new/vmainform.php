<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<h3>Data Permintaan Pembelian (PP)</h3><br>
<a href="<?php echo base_url(); ?>index.php/pp-new/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/pp-new/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tgl_pp = $("#tgl_retur").val();
	
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (tgl_pp == '') {
			alert ("Tanggal PP harus dipilih");
			$("#tgl_retur").focus();
			return false;
		}
	
		/*if(document.getElementById('kode_1').value=='') {
			alert('Maaf, item barangnya harus diisi.');
			document.getElementById('kode_1').focus();
			return false;
		} */
		
		var jum= $('#no').val()-1;

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				if ($('#kode_'+k).val() == '') {
					alert("Data item barang tidak boleh ada yang kosong...!");
					return false;
				}
				if($('#jumlah_'+k).val() == '0' || $('#jumlah_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#jumlah_'+k).val()) ) {
					alert("Qty harus berupa angka atau desimal..!");
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
	}
	else
		return false;
	
	// cek kode brg yg sama (ini terlalu lamaa, ga dipake)
	/*var arrayku = [];
	jQuery("input[class='kode']").each(function(){  
		arrayku.push(jQuery(this).val())   
	});
	//alert(arrayku); return false;
	var ownnumber = 1;
	for(var i=0; i < arrayku.length; i++){
		for(var j=0; j < arrayku.length; j++){
			alert(arrayku[i]+" "+arrayku[j]);
			if ((arrayku[i] == arrayku[j]) && ownnumber == 0 ) {
				alert("INI");
				//ownnumber = 1;
			}
			else if ((arrayku[i] == arrayku[j]) && ownnumber == 1 ) {
				alert("ownnumber");
				ownnumber = 0;
			}
			
		}

	}
	return false;
	*/
}

function ckpp(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('pp-new/cform/cari_pp');?>",
	data:"nomorpp="+nomor,
	success: function(data){
		$("#confnomorpp").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

//tambah
$(function()
{
	var goedit = $("#goedit").val();
	if(goedit!='1')
		$("#no").val('2');
	
	$("#no_faktur").blur(function()
		{
		 var invoice_number=$("#no_faktur").val();
		
		 $("#msgbox").removeClass().addClass('messagebox').html('<img src="modules/ProitInventory/pnimages/spinner_small.gif">').fadeIn("slow");
		
		 $.post("index.php?module=ProitInventory&type=admin&func=cek_invoice_penjualan",
		 { 
		 	invoice_number:invoice_number } ,function(data)
		 { 	
		 
		 
		 var cek=$("status",data).html()
		 var tgl=$("tgl",data).html()
		 $('#tgl_retur').val(tgl);
		 
		 	
		  if(cek=='0') 
		  {
		   $("#msgbox").fadeTo(200,0.1,function() 
		   {			
			$(this).html('No. Invoice yang anda masukkan tidak valid').addClass('messageboxerror').fadeTo(300,1);
		   });
		  }
		  else
		  {
		   $("#msgbox").fadeTo(200,0.1,function()  
		   {			
			$(this).html('<img src="modules/ProitInventory/pnimages/checkbullet.gif">').addClass('messageboxok').fadeTo(300,1);
		   });
		  }
		  
		 });
		});
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****id_satuan*************************************
		var id_satuan="#id_satuan_"+n;
		var new_id_satuan="#id_satuan_"+no;
		$(id_satuan, lastRow).attr("id", "id_satuan_"+no);
		$(new_id_satuan, lastRow).attr("name", "id_satuan_"+no);		
		$(new_id_satuan, lastRow).val('');				
		//*****end id_satuan*************************************	
		
		// 25-06-2015 GA DIPAKE LAGI
		//*****is_satuan_lain*************************************
		/*var is_satuan_lain="#is_satuan_lain_"+n;
		var new_is_satuan_lain="#is_satuan_lain_"+no;
		$(is_satuan_lain, lastRow).attr("id", "is_satuan_lain_"+no);
		$(new_is_satuan_lain, lastRow).attr("name", "is_satuan_lain_"+no);		
		$(new_is_satuan_lain, lastRow).val('t');				
		var even_click = "if ($('input[name=is_satuan_lain_"+no+ "]').is(':checked')) $('#satuan_lain_"+no+ "').attr('disabled',false); else $('#satuan_lain_"+no+ "').attr('disabled',true);";
		$(new_is_satuan_lain, lastRow).attr("onclick",even_click);
		$(new_is_satuan_lain, lastRow).attr("checked", false);
		//*****end is_satuan_lain*************************************	
		
		//*****satuan_lain*************************************
		var satuan_lain="#satuan_lain_"+n;
		var new_satuan_lain="#satuan_lain_"+no;
		$(satuan_lain, lastRow).attr("id", "satuan_lain_"+no);
		$(new_satuan_lain, lastRow).attr("name", "satuan_lain_"+no);	
		$(new_satuan_lain, lastRow).attr("disabled", true);
		//*****end satuan_lain*********************************
		*/
		
		//*****jumlah*************************************
		var jumlah="#jumlah_"+n;
		var new_jumlah="#jumlah_"+no;
		$(jumlah, lastRow).attr("id", "jumlah_"+no);
		$(new_jumlah, lastRow).attr("name", "jumlah_"+no);		
		$(new_jumlah, lastRow).val('');				
		//*****end jumlah*************************************	
		
		//*****subtotal*************************************
		var subtotal="#keterangan_"+n;
		var new_subtotal="#keterangan_"+no;
		$(subtotal, lastRow).attr("id", "keterangan_"+no);
		$(new_subtotal, lastRow).attr("name", "keterangan_"+no);		
		$(new_subtotal, lastRow).val('');				
		//*****end subtotal*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pp-new/cform/show_popup_brg/'+x+'/"+no+"/');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		var goedit = $("#goedit").val();
		var jumawal = $("#jumawal").val();
		
		if (goedit != 1) {
			if (x>2) {
				$("#tabelku tr:last").remove();	 
				var x= $("#no").val();	
				var y= x-1;
				$("#no").val(y);	
			}
		 }
		 else {
			if (parseInt(x) > parseInt(jumawal)) {
				$("#tabelku tr:last").remove();	 
				var x= $("#no").val();	
				var y= x-1;
				$("#no").val(y);	
			}
		}
		
	});
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pp-new/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="goedit" id="goedit" <?php if ($edit == '1') { ?> value="1" <?php } else { ?> value="" <?php } ?> >
<input type="hidden" name="kodeeditnopp" id="kodeeditnopp" value="<?php echo $eno_pp ?>">
<input type="hidden" name="id_pp" id="id_pp" value="<?php echo $eid ?>">

<?php if ($edit == '1') { ?>
<input type="hidden" name="curpage" value="<?php echo $curpage ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<?php } ?>

<?php
if ($edit==1) {
	if (count($pp_detail)>0) {
		$jumawal = count($pp_detail)+1;
		$no=1;
		foreach ($pp_detail as $hitung) {
			$no++;
		}
	}
	else {
		$no=2;
		$jumawal = 2;
	}
}
?>

<input type="hidden" name="no" id="no" <?php if ($edit==1) { ?> value="<?php echo $no ?>" <?php } ?> >
<input type="hidden" name="iddata" id="iddata" />
<?php if ($edit==1) { ?> <input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>"> <?php } ?>

<div align="center">
<?php 
	if ($edit == 1) {
?>
	Edit Data <br>
<?php
	}
?>
<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
			<td>Departemen</td>
			<td> <select name="bagian" id="bagian">
				<?php foreach ($bagian as $bag) { ?>
					<option value="<?php echo $bag->id ?>" <?php if ($eid_bagian == $bag->id) { ?> selected="true" <?php } ?> ><?php echo $bag->kode." - ". $bag->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
  <tr>
  <tr>
    <td width="15%">Nomor PP </td>
    <td>
      <input name="no_faktur" type="text" id="no_faktur" size="10" maxlength="10" value="<?php echo $eno_pp ?>" onkeyup="ckpp(this.value);" readonly="true">&nbsp;<span id="msgbox" style="display:none;"></span>
	  <div id="confnomorpp" style="color:#FF0000; font-size:10px;"></div>
    </td>
  </tr>
  <tr>
    <td>Tanggal PP </td>
    <td>
      <input name="tgl_retur" type="text" id="tgl_retur" size="10" value="<?php echo $etgl_pp ?>" readonly="true">
      <img alt="" id="tgl_retur" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_retur,'dd-mm-yyyy',this)">
			
	  <!-- <img alt="" id="tgl_retur" align="middle"
			title="Pick date.." src="<?php echo base_url();?>images/calendar.gif"
			onmouseover="if (timeoutId) clearTimeout(timeoutId);return true;"
			onclick="g_Calendar.show(event,'f_purchase.tgl_retur',false,'dd-mm-yyyy');return false;"
			onmouseout="if (timeoutDelay) calendarTimeout();"> -->
	</td>
  </tr>

  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="left"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
           <!--25-06-2015 GA USAH PAKE SATUAN LAIN<th>Ada Satuan Lain</th>
           <th>Satuan Lain</th>-->
	      <th>Qty</th>
          <th>Keterangan</th>
        </tr>
        <?php if ($edit == '') { ?>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          <td><select name="kel_brg_1" id="kel_brg_1">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_1" type="text" id="kode_1" size="10" readonly="true" class="kode"/>
           <input name="id_brg_1" type="hidden" id="id_brg_1"/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#kel_brg_1').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pp-new/cform/show_popup_brg/'+ x +'/1/');" ></td>
         <td><input name="nama_1" type="text" id="nama_1" size="20" readonly="true"/></td>
         <td><input name="satuan_1" type="text" id="satuan_1" size="7" readonly="true"/>
         <input name="id_satuan_1" type="hidden" id="id_satuan_1" value=""/>
         </td>
         <!-- 25-06-2015 GA DIPAKE LAGI
         <td><input name="is_satuan_lain_1" type="checkbox" id="is_satuan_lain_1" value ="t" onclick="javascript: if ($('input[name=is_satuan_lain_1]').is(':checked')) $('#satuan_lain_1').attr('disabled',false); else $('#satuan_lain_1').attr('disabled',true); " /></td>
         <td><select name="satuan_lain_1" id="satuan_lain_1" disabled = "true">
				<?php //foreach ($satuan as $sat) { ?>
					<option value="<?php //echo $sat->id ?>" ><?php //echo $sat->nama ?></option>
				<?php //} ?>
				</select></td>-->
         <td><input name="jumlah_1" type="text" id="jumlah_1" size="5" style="text-align:right;" /></td>
         <td><input name="keterangan_1" type="text" id="keterangan_1" size="20" /></td>
        </tr>
        <?php } else { $i=1;
			for($j=0;$j<count($pp_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td><select name="kel_brg_<?php echo $i ?>" id="kel_brg_<?php echo $i ?>">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($pp_detail[$j]['kode_kel_brg'] == $kel->kode) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          <td nowrap="nowrap">
		   
           <input class="kode" name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $pp_detail[$j]['kode_brg'] ?>"/>
           <input name="id_brg_<?php echo $i ?>" type="hidden" id="id_brg_<?php echo $i ?>" value="<?php echo $pp_detail[$j]['id_brg'] ?>"/>
           
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=<?php echo $i ?>" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" 
           onclick="javascript: var x= $('#kel_brg_<?php echo $i ?>').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pp-new/cform/show_popup_brg/'+x+'/<?php echo $i ?>');" ></td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $pp_detail[$j]['nama'] ?>" /></td>
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="7" readonly="true" value="<?php echo $pp_detail[$j]['nama_satuan'] ?>" />
          <input name="id_satuan_<?php echo $i ?>" type="hidden" id="id_satuan_<?php echo $i ?>" value="<?php echo $pp_detail[$j]['id_satuan'] ?>"/>
          </td>
          
         <!-- <td><input name="is_satuan_lain_<?php echo $i ?>" type="checkbox" id="is_satuan_lain_<?php echo $i ?>" value ="t" 
          <?php //if ($pp_detail[$j]['is_satuan_lain'] == 't') { ?> checked="true" <?php //} ?>
          onclick="javascript: if ($('input[name=is_satuan_lain_<?php echo $i ?>]').is(':checked')) $('#satuan_lain_<?php echo $i ?>').attr('disabled',false); else $('#satuan_lain_<?php echo $i ?>').attr('disabled',true); " /></td>
          <td><select name="satuan_lain_<?php echo $i ?>" id="satuan_lain_<?php echo $i ?>" <?php //if ($pp_detail[$j]['is_satuan_lain'] == 'f') { ?> disabled <?php //} ?> >
				<?php //foreach ($satuan as $sat) { ?>
					<option value="<?php //echo $sat->id ?>" <?php //if ($pp_detail[$j]['satuan_lain'] == $sat->id) { ?> selected="true" <?php //} ?> ><?php //echo $sat->nama ?></option>
				<?php //} ?>
				</select></td>-->
          
          <td><input name="jumlah_<?php echo $i ?>" type="text" id="jumlah_<?php echo $i ?>" size="5" value="<?php echo $pp_detail[$j]['qty'] ?>" style="text-align:right;" /></td>
          <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="20" value="<?php echo $pp_detail[$j]['keterangan'] ?>" /></td>
        </tr>
		<?php $i++; } // end foreach ?>
        <?php } ?>
	</table>	
	</form>
      <div align="center"><br><br> 
        
        <?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { 
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "pp-new/cform/view/index/".$curpage;
			else
				$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;	
			?>
			<input type="submit" name="submit" value="Edit" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"><?php } ?>
      </div></td>
    </tr>

</table>
</div>
</form>
