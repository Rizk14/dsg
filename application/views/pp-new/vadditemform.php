<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<h3>Data Permintaan Pembelian (PP)</h3><br>
<a href="<?php echo base_url(); ?>index.php/pp-new/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/pp-new/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tgl_pp = $("#tgl_retur").val();
	
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {	
		if (tgl_pp == '') {
			alert ("Tanggal PP harus dipilih");
			$("#tgl_retur").focus();
			return false;
		}
		
		/*if(document.getElementById('kode_1').value=='') {
			alert('Maaf, item barangnya harus diisi.');
			document.getElementById('kode_1').focus();
			return false;
		} */
		
		var jum= $('#no').val()-1;

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				if ($('#kode_'+k).val() == '') {
					alert("Data item barang tidak boleh ada yang kosong...!");
					return false;
				}
				if($('#jumlah_'+k).val() == '0' || $('#jumlah_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#jumlah_'+k).val()) ) {
					alert("Qty harus berupa angka atau desimal..!");
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
	}
	else
		return false;
}

function ckpp(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('pp-new/cform/cari_pp');?>",
	data:"nomorpp="+nomor,
	success: function(data){
		$("#confnomorpp").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

//tambah
$(function()
{
	//var goedit = $("#goedit").val();
	//if(goedit!='1')
		$("#no").val('2');
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****jumlah*************************************
		var jumlah="#jumlah_"+n;
		var new_jumlah="#jumlah_"+no;
		$(jumlah, lastRow).attr("id", "jumlah_"+no);
		$(new_jumlah, lastRow).attr("name", "jumlah_"+no);		
		$(new_jumlah, lastRow).val('');				
		//*****end jumlah*************************************	
		
		//*****subtotal*************************************
		var subtotal="#keterangan_"+n;
		var new_subtotal="#keterangan_"+no;
		$(subtotal, lastRow).attr("id", "keterangan_"+no);
		$(new_subtotal, lastRow).attr("name", "keterangan_"+no);		
		$(new_subtotal, lastRow).val('');				
		//*****end subtotal*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pp-new/cform/show_popup_brg/'+x+'/"+no+"/');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();			
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
		
	});
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pp-new/cform/tambahitemsubmit" method="post" enctype="multipart/form-data">
<input type="hidden" name="kodeeditnopp" id="kodeeditnopp" value="<?php echo $eno_pp ?>">
<input type="hidden" name="id_pp" id="id_pp" value="<?php echo $eid ?>">

<input type="hidden" name="curpage" value="<?php echo $curpage ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<input type="hidden" name="no" id="no" value="" >
<input type="hidden" name="iddata" id="iddata" />

<div align="center">
	Tambah Item <br>

<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
			<td>Departemen</td>
			<td> <select name="bagian" id="bagian" disabled>
				<?php foreach ($bagian as $bag) { ?>
					<option value="<?php echo $bag->id ?>" <?php if ($eid_bagian == $bag->id) { ?> selected="true" <?php } ?> ><?php echo $bag->kode." - ". $bag->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
  <tr>
  <tr>
    <td width="15%">Nomor PP </td>
    <td>
      <input name="no_faktur" type="text" id="no_faktur" size="10" maxlength="10" value="<?php echo $eno_pp ?>" onkeyup="ckpp(this.value);" readonly="true">&nbsp;<span id="msgbox" style="display:none;"></span>
	  <div id="confnomorpp" style="color:#FF0000; font-size:10px;"></div>
    </td>
  </tr>
  <tr>
    <td>Tanggal PP </td>
    <td>
      <input name="tgl_retur" type="text" id="tgl_retur" size="10" value="<?php echo $etgl_pp ?>" readonly="true">
      <!-- <img alt="" id="tgl_retur" align="middle"
			title="Pilih tgl.." src="<?php //echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_retur,'dd-mm-yyyy',this)"> -->
			
	  <!-- <img alt="" id="tgl_retur" align="middle"
			title="Pick date.." src="<?php echo base_url();?>images/calendar.gif"
			onmouseover="if (timeoutId) clearTimeout(timeoutId);return true;"
			onclick="g_Calendar.show(event,'f_purchase.tgl_retur',false,'dd-mm-yyyy');return false;"
			onmouseout="if (timeoutDelay) calendarTimeout();"> -->
	</td>
  </tr>

  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="7" align="right"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
	      <th>Qty</th>
          <th>Keterangan</th>
        </tr>
        
         <tr align="center">
          <td align="center" id="num_1">1</td>
          <td><select name="kel_brg_1" id="kel_brg_1">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          <td nowrap="nowrap">
           <input name="kode_1" type="text" id="kode_1" size="10" readonly="true" class="kode"/>
           <input name="id_brg_1" type="hidden" id="id_brg_1" />
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#kel_brg_1').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pp-new/cform/show_popup_brg/'+ x +'/1/');" type="button"></td>
         <td><input name="nama_1" type="text" id="nama_1" size="20" readonly="true"/></td>
         <td><input name="satuan_1" type="text" id="satuan_1" size="7" readonly="true"/></td>
         <td><input name="jumlah_1" type="text" id="jumlah_1" size="5" style="text-align:right;" /></td>
         <td><input name="keterangan_1" type="text" id="keterangan_1" size="20" /></td>
        </tr>
	</table>	
	</form>
      <div align="center"><br><br> 
			<?php 
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "pp-new/cform/view/index/".$curpage;
			else
				$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;	
			?>
			<input type="submit" name="submit" value="Simpan Item" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
      </div></td>
    </tr>

</table>
</div>
</form>
