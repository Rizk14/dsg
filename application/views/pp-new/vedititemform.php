<h3>Data Permintaan Pembelian (PP)</h3><br>
<a href="<?php echo base_url(); ?>index.php/pp-new/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/pp-new/cform/view">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

 $(function()
 {

 });
 
 function cek_data() {
	var qty= $('#jumlah_0').val();

	if (qty == '' || qty == '0') {
		alert("Qty tidak boleh 0 / kosong..!");
		$('#jumlah_0').focus();
		return false;
	}
	
	if (isNaN($('#jumlah_0').val())) {
		alert("Qty harus berupa angka..!");
		$('#jumlah_0').focus();
		return false;
	}
 }	

</script>

<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<?php 
$attributes = array('name' => 'f_purchase', 'id' => 'f_purchase');
echo form_open('pp-new/cform/edititem', $attributes); ?>
<input type="hidden" name="go_edit" value="1">
<input type="hidden" name="iddetail" value="<?php echo $iddetail ?>">
<input type="hidden" name="id_pp" value="<?php echo $query[0]['id_pp'] ?>">
<input type="hidden" name="curpage" value="<?php echo $curpage ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">

<table width="50%">
		<tr>
			<td colspan="2">Edit Data</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td width="30%">Nomor PP</td>
			<td> <?php echo $query[0]['no_pp'] ?>
			</td>
		</tr>
		<tr>
			<td>Tanggal PP</td>
			<td> 
			<?php 
			$pisah1 = explode("-", $query[0]['tgl_pp']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tgl_pp = $tgl1."-".$bln1."-".$thn1;
				
			?>
			
				<input name="tgl_pp" type="text" id="tgl_pp" size="10" value="<?php echo $tgl_pp ?>" readonly="true">
				  <img alt="" id="tgl_pp" align="middle"
						title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
						onclick="displayCalendar(document.forms[0].tgl_pp,'dd-mm-yyyy',this)">
			
			</td>
		</tr>
		<tr>
			<td>Departemen</td>
			<td> <?php echo $query[0]['nama_bagian'] ?>
			</td>
		</tr>
		<tr>
			<td>Kode & Nama Brg</td>
			<td nowrap> 
				<select name="kel_brg" id="kel_brg">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($query[0]['kode_kel_brg'] == $kel->kode) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
				<input readonly="true" name="nama_0" type="text" id="nama_0" size="20" value="<?php echo $query[0]['kode_brg']." - ".$query[0]['nama_brg'] ?>" />
				<input name="id_brg_0" type="hidden" id="id_brg_0" value="<?php echo $query[0]['id_brg'] ?>" />
				<input name="kode_0" type="hidden" id="kode_0" value="<?php echo $query[0]['kode_brg'] ?>" />
				<input name="satuan_0" type="hidden" id="satuan_0" value="<?php echo $query[0]['nama_satuan'] ?>" />
				<input title="browse data barang" name="pilih" value="..." type="button" id="pilih" 
				onclick="javascript: var x= $('#kel_brg').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pp-new/cform/show_popup_brg/'+ x +'/0/');" type="button">
				
			<?php //echo $query[0]['kode_brg']." - ".$query[0]['nama_brg']." (".$query[0]['nama_satuan'].")" ?>
			</td>
		</tr>
		
		<tr>
			<td>Qty</td>
			<td> <input type="text" name="jumlah_0" id="jumlah_0" style="text-align:right;" value="<?php echo $query[0]['qty'] ?>" size="7" >
				<input type="hidden" name="qty_lama" id="qty_lama" value="<?php echo $query[0]['qty'] ?>">
			</td>
		</tr>
</table><br>
<?php 
	if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "pp-new/cform/view/index/".$curpage;
	else
		$url_redirectnya = "pp-new/cform/cari/index/".$carinya."/".$curpage;
?>
<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<?php echo form_close(); ?>
