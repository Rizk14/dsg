<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var des=$("#des").val();
		var satuan=$("#satuan").val();
		var id_satuan=$("#id_satuan").val();
		var id_brg=$("#id_brg").val();
		var jumdata=$("#jumdata").val();
		var posisi2=$("#posisi2").val();
		
		if (posisi2 != 0) {
			for(var i=1; i <= jumdata; i++){
				var stringnya = opener.document.getElementById('id_brg_'+i).value;
				if (id_brg == stringnya) {
					alert("Kode barang sudah pernah dipilih...!");
					return false;
				}
			}
			
			opener.document.forms["f_purchase"].id_brg_<?php echo $posisi ?>.value=id_brg;
			opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=des;
			opener.document.forms["f_purchase"].satuan_<?php echo $posisi ?>.value=satuan;
			opener.document.forms["f_purchase"].id_satuan_<?php echo $posisi ?>.value=id_satuan;
			opener.document.forms["f_purchase"].jumlah_<?php echo $posisi ?>.focus();
		}
		else { 
			/*opener.document.forms["f_purchase"].brg_baku.value=idx+" - "+des;
			opener.document.forms["f_purchase"].kode_brg.value=idx; */
			opener.document.forms["f_purchase"].id_brg_<?php echo $posisi ?>.value=id_brg;
			opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=des;
			opener.document.forms["f_purchase"].satuan_<?php echo $posisi ?>.value=satuan;
			opener.document.forms["f_purchase"].id_satuan_<?php echo $posisi ?>.value=id_satuan;
			opener.document.forms["f_purchase"].jumlah_<?php echo $posisi ?>.focus();
		}
			
		self.close();
	});
});
</script>
<center><h3>Daftar Barang Untuk Kelompok <?php echo $nama_kel ?></h3></center>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="des" id="des">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="id_satuan" id="id_satuan">
<input type="hidden" name="id_brg" id="id_brg">
<input type="hidden" name="posisi2" id="posisi2">
<input type="hidden" name="jumdata" id="jumdata" value="<?php echo $jumdata ?>">
</form>
<div align="center"><br>
<?php echo form_open('pp-new/cform/show_popup_brg'); ?>
Jenis Barang <select name="id_jenis_brg" id="id_jenis_brg">
				<option value="0" <?php if ($cjenis_brg == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($jenis_brg as $jns) { ?>
					<option value="<?php echo $jns->id ?>" <?php if ($cjenis_brg == $jns->id) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nama_kel_brg."] ". $jns->kode." - ".$jns->nama ?></option>
				<?php } ?>
				</select> &nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="kel_brg" value="<?php echo $kel_brg ?>">
<?php echo form_close(); ?>
  <table border="1" align="center" width="100%" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Action</th>
    </tr>
	<?php 
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
		foreach($query as $brg) {
	
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $brg->kode_brg; ?></td>
      <td nowrap><?php echo $brg->nama_brg; ?></td>
      <td><?php echo $brg->nama_satuan; ?></td>
      <td align="center">
      <?php 
		$findme = '"';
		$pos = strpos($brg->nama_brg, $findme);
		if ($pos == false)
			$brgnya = str_replace("'", "\'", $brg->nama_brg);
		else
			$brgnya = str_replace("\"", "&quot;", $brg->nama_brg);
      ?>
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $brg->kode_brg ?>';
	  window.document.f_master_brg.des.value='<?php echo $brgnya ?>';
	  window.document.f_master_brg.id_satuan.value='<?php echo $brg->satuan ?>';
	  window.document.f_master_brg.satuan.value='<?php echo $brg->nama_satuan ?>';
	  window.document.f_master_brg.id_brg.value='<?php echo $brg->id ?>';
	  window.document.f_master_brg.posisi2.value='<?php echo $posisi ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } ?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
