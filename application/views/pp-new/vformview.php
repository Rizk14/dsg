<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Permintaan Pembelian (PP)</h3><br>
<a href="<?php echo base_url(); ?>index.php/pp-new/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/pp-new/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('pp-new/cform/cari'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>Nomor PP</th>
		 <th>Tanggal PP</th>
		 <th>Departemen</th>
		 <th>List Barang</th>
		 <th>Satuan</th>
		 <!--<th>Satuan Lain</th>-->
		 <th>Qty</th>
		 <th>Qty Pemenuhan OP</th>
		 <th>Qty Sisa</th>
		 <th>Last Update</th>
		 <th>Edit Per Item</th>
		 <th>Hapus Per Item</th>
		 <th>Per PP</th>
		 <?php //if ($this->session->userdata('gid') == 1) { ?>
			
		 <?php //} ?>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {

				/*$qstatusop	= $this->mmaster->statusop($query[$j]['id']);
				$jstatusop	= $qstatusop->num_rows();
				if($jstatusop>0) {
					$jmlop	= $jstatusop;
				} else {
					$jmlop	= 0;
				} */
						 
			 	 $bl	= array(
				 	'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember'
				 );
				 
			 	 $etgl	= explode("-",$query[$j]['tgl_pp'],strlen($query[$j]['tgl_pp'])); // YYYY-mm-dd
				 $hr	= substr($etgl[2],0,1)==0?substr($etgl[2],1,1):$etgl[2];
				 $nbl	= $etgl[1];
				 $ntgl	= $hr." ".$bl[$nbl]." ".$etgl[0];
				 
				 $etgl_last	= explode("-",$query[$j]['tgl_update'],strlen($query[$j]['tgl_update'])); // YYYY-mm-dd
				 $hr_last	= substr($etgl_last[2],0,1)==0?substr($etgl_last[2],1,1):$etgl_last[2];
				 $nbl_last	= $etgl_last[1];
				 $ntgl_last	= $hr_last." ".$bl[$nbl_last]." ".$etgl_last[0];
				 
				 echo "<tr class=\"record\">";
				 echo "<td nowrap>".$query[$j]['no_pp']."</td>";
				 echo "<td>".$ntgl."</td>";
				 echo "<td>".$query[$j]['nama_bagian']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				/* echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['sat_lain'] != '')
						  echo $var_detail[$k]['sat_lain'];
						else
						  echo "&nbsp;";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>"; */
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_op'],2,'.','');
						  if ($var_detail[$k]['count_op'] > 1)
							 echo " (".$var_detail[$k]['count_op']." OP)";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo number_format($var_detail[$k]['sisanya'],2,'.','');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				// echo "<td>".$ntgl_last."</td>";
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				echo "<td>".$tgl_update."</td>";
				
				 echo "<td align=center nowrap>";
				 //if($jmlop==0) {
			/*	if($query[$j]['status_edit'] == 'f') {
					 echo "<a href=".base_url()."index.php/pp-new/cform/index/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a>";
					 echo "&nbsp; <a href=".base_url()."index.php/pp-new/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 } */
				 
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if($var_detail[$k]['status_faktur'] == 'f' && $var_detail[$k]['jum_op'] == 0 ) {
							echo "<a href=".base_url()."index.php/pp-new/cform/edititem/".$var_detail[$k]['id']."/".$cur_page."/".$is_cari."/".$cari." \" id=\"".$var_detail[$k]['id']."\">Edit</a>";
							//if ($this->session->userdata('gid') == 1)
								//echo "&nbsp; <a href=".base_url()."index.php/pp-new/cform/deleteitem/".$var_detail[$k]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
								
							if ($k<$hitung-1)
								echo "<br> ";
						}
						else {
							echo "&nbsp;";
							if ($k<$hitung-1)
								echo "<br> ";
						}
					} // end for
				 } // end if
				 				 
				 echo "</td>";
				 
				 //	 if ($this->session->userdata('gid') == 1) {
					 echo "<td align='center'>";
					 
					 if (is_array($query[$j]['detail_pp'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_pp'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							if($var_detail[$k]['status_faktur'] == 'f' && $var_detail[$k]['jum_op'] == 0 ) {
								echo "<a href=".base_url()."index.php/pp-new/cform/deleteitem/".$var_detail[$k]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
								if ($k<$hitung-1)
									echo "<br> ";
							}
							else {
								echo "&nbsp;";
								if ($k<$hitung-1)
									echo "<br> ";
							}
						} // end for
					 } // end if
					 
					 echo "</td>";
			//	 } // end if
				 
				if ($query[$j]['status_aktif'] == 't') {
					$statusnya = "Non-Aktifkan";
					$aksi = "off";
				}
				else {
					$statusnya = "Aktifkan";
					$aksi = "on";
				}
					
				 echo "<td align=center>";
					if($query[$j]['status_edit'] == 'f') {
						echo "<a href=".base_url()."index.php/pp-new/cform/index/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>";
						//if ($this->session->userdata('gid') == 1)
							echo "&nbsp; <a href=".base_url()."index.php/pp-new/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
							echo "&nbsp;<a href=".base_url()."index.php/pp-new/cform/updatestatus/".$query[$j]['id']."/".$aksi."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Update status aktifnya?')\">".$statusnya."</a>";
							
					}
					else {
						if ($query[$j]['status_faktur'] == 'f') {
							echo "<a href=".base_url()."index.php/pp-new/cform/tambahitem/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" id=\"".$query[$j]['id']."\">Add</a>";
							echo "&nbsp;<a href=".base_url()."index.php/pp-new/cform/updatestatus/".$query[$j]['id']."/".$aksi."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Update status aktifnya?')\">".$statusnya."</a>";
						}
					}
				 echo "</td>";
				 
				 echo "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links(); ?>
</div>
