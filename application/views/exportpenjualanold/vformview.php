<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
    
    .fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}

</style>

<h3>Laporan Pembelian</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Supplier: <?php if ($supplier != '0') { echo $supplier." - ".$nama_supplier; } else { echo "All"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br> 
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-pembelian/cform/export_excel', $attributes); ?>
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="hidden" name="kode_supplier" value="<?php echo $supplier ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<!--<th>Tgl Faktur</th>
		 <th>Nomor Faktur</th> -->
		 <th>Supplier</th>
		 <th>No SJ</th>
		 <th>Tgl SJ</th>
		 <th>List Barang</th>
		 <th>No Perk</th>
		 <th>Harga Satuan</th>
		 <th>Qty</th>
		 <th>Satuan</th>
		 <th>Jumlah</th>
		 <th>Tot Hutang Dagang</th>
		 <th>PPN</th>
		 <th>Bahan Baku</th>
		 <th>Bahan Pembantu</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_hutang += $query[$j]['jumlah'];
					$tot_ppn += $query[$j]['pajaknya'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
							 if ($var_detail[$k]['kode_perk'] == "511.100")
								$tot_baku += $var_detail[$k]['total'];
							 else
								$tot_pembantu += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
			}
		 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['nama_supplier']."</td>";
				 echo    "<td>".$query[$j]['no_sj']."</td>";
				 echo    "<td>".$query[$j]['tgl_sj']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  //echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_perk'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga'], 2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['total'],2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['jumlah'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['pajaknya'],2,',','.')."</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "511.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "512.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo  "</tr>";

					
		 	}
		   }
		 ?>
		 <tr>
			<td colspan="8" align="right">TOTAL</td>
			<td align="right"><?php echo number_format($tot_jumlah_detail,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_hutang,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_ppn,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_baku,2,',','.')  ?></td>
			<td align="right"><?php echo number_format($tot_pembantu,2,',','.')  ?></td>
		 </tr>
 	</tbody>
</table><br>
<?php //echo $this->pagination->create_links();?>
</div>
