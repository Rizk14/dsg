<h3>Data Faktur Pembelian Packing + Jahit</h3><br>
<a href="<?php echo base_url(); ?>index.php/faktur-pembelianpackjht-wip/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/faktur-pembelianpackjht-wip/cform/view">View Data</a><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	$('#pilih_faktur').click(function(){
		var id_pck= jQuery('#unit_packing').val();
		var id_ujh= jQuery('#unit_jahit').val();
		var urlnya = "<?php echo base_url(); ?>index.php/faktur-pembelianpackjht-wip/cform/show_popup_pembelian/A/"+id_pck+"/"+id_ujh+"/xxx";
			openCenteredWindow(urlnya);
	  });
	  
	  $('#unit_packing').change(function(){
	  	    $("#no_sj").val('');	
	  	    $("#jum").val('0');	
	  });
	  
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_faktur() {
	var no_fp= $('#no_fp').val();
	var tgl_fp= $('#tgl_fp').val();
	var no_sj= $('#no_sj').val();
	var jum= $('#jum').val();
	
	
	if (no_sj == '') {
		alert("Nomor SJ harus dipilih..!");
		$('#no_sj').focus();
		return false;
	}
	if (no_fp == '') {
		alert("No Faktur harus diisi..!");
		$('#no_fp').focus();
		return false;
	}
	if (tgl_fp == '') {
		alert("Tanggal Faktur harus dipilih..!");
		$('#tgl_fp').focus();
		return false;
	}
	
	if (jum == '' || jum == '0') {
		$('#jum').focus();
		alert("Jumlah Total harus diisi..!");
		return false;
	}	
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-pembelianpackjht-wip/cform/submit" method="post" enctype="multipart/form-data">

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >

		
<tr>
	<td>Unit packing </td>
	<td><select name="unit_packing" id="unit_packing" onkeyup="this.blur();this.focus();">
				<option value="0"> - ALL -  </option>
				<?php foreach ($list_unit_packing as $upk) { ?>
					<option value="<?php echo $upk->id ?>" ><?php echo $upk->kode_unit." - ". $upk->nama ?></option>
				<?php } ?>
				</select></td>
</tr>
<br>
<tr>
	<td>Unit jahit </td>
	<td><select name="unit_jahit" id="unit_jahit" onkeyup="this.blur();this.focus();">
				<option value="0"> - ALL -  </option>
				<?php foreach ($list_unit_jahit as $ujh) { ?>
					<option value="<?php echo $ujh->id ?>" ><?php echo $ujh->kode_unit." - ". $ujh->nama ?></option>
				<?php } ?>
				</select></td>
</tr>
	
  <tr>
		<td width="15%">Nomor SJ BTB Wip Hasil packing + Jahit</td>
		<td width="70%"> <input type="text" name="no_sj" id="no_sj" value="" size="40" maxlength="40" readonly="true">
		<input type="hidden" name="id_sj" id="id_sj" value="">
		&nbsp;
		<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data sj pembelian">
		</td>
	</tr>
  <tr>
    <td>Nomor Faktur</td>
    <td>
      <input name="no_fp" type="text" id="no_fp" size="20" maxlength="20" value="">
    </td>
  </tr>
  <tr>
    <td>Tanggal Faktur</td>
    <td>
	<label>
      <input name="tgl_fp" type="text" id="tgl_fp" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_fp" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_fp,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td>Jumlah Total</td>
    <td>
      <input name="jum2" type="text" id="jum2" size="10" maxlength="20" value="0" readonly="true" style="text-align:right;">
      <input name="jum" type="hidden" id="jum" value="0">
    </td>
  </tr>
  
  <tr><td>&nbsp;</td>
	<td><input type="submit" name="submit" value="Simpan" onclick="return cek_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-pembelianpackjht-wip/cform/view'"></td>
  </tr>

</table>
</form>
