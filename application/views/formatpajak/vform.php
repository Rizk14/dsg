<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript">
$(document).ready(function() {
 
 $(".xhapus").click(function(){
 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var hps = element.attr("id");
 
 //Built a url to send
 var info = 'pid=' + hps;
 if(confirm("Yakin akan menghapus data dgn id: "+hps+" ?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('/bank/cform/actdelete')?>",
 data: info,
 success: function(){
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
 
})
</script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

SETTING FORMAT NOMOR PAJAK         

	<div>
		<i>Format Duta mulai April 2013: 010.900-13.31928693 sampai 010.900-13.31929876</i>
	    <table>
	      <tr>
	    	<td align="left">
		      <?php
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('formatpajak/cform/simpan', $attributes);?>
		<div id="masterjenisvoucherform">
		      <table>
		      	<tr>
					  <td>Segmen 1 (7 digit pertama, misal: 010.900)</td>
					  <td>:</td>
					  <td>
						<?php
						$nmsegmen = array(
								  'name'        => 'segmen1',
								  'id'          => 'segmen1',
								  'value'       => $segmen1,
								  'maxlength'   => '7');
						echo form_input($nmsegmen); ?>
					  </td>
				</tr>
				<tr>
					  <td>Nomor Urut Awal (misal: 31928693)</td>
					  <td>:</td>
					  <td>
						<?php
						$awal = array(
								  'name'        => 'nourut_awal',
								  'id'          => 'nourut_awal',
								  #'value'       => "0".$nourut_awal,
								  'value'       => $nourut_awal,
								  'maxlength'   => '8');
						echo form_input($awal); ?>
					  </td>
				</tr>
				<tr>
					  <td>Nomor Urut Akhir (misal: 31929876)</td>
					  <td>:</td>
					  <td>
						<?php
						$awal = array(
								  'name'        => 'nourut_akhir',
								  'id'          => 'nourut_akhir',
								  #'value'       => "0".$nourut_akhir,
								  'value'       => $nourut_akhir,
								  'maxlength'   => '8');
						echo form_input($awal); ?>
					  </td>
				</tr>
		      	<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>
					<input name="tblsimpan" id="tblsimpan" value="Simpan" type="submit">
				  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
