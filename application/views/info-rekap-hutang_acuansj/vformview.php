<h3>Rekapitulasi Hutang Dagang</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		<th>No </th>
		 <th>Supplier</th>
		 <th>Saldo Awal</th>
		 <th>Debet</th>
		 <th>D/n</th>
		 <th>Pembulatan</th>
		 <th>Pembelian</th>
		 <th>C/n</th>
		 <th>Pembulatan</th>
		 <th>Saldo Akhir</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			
			/*if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_hutang += $query[$j]['jumlah'];
					$tot_ppn += $query[$j]['pajaknya'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
							 if ($var_detail[$k]['kode_perk'] == "511.100")
								$tot_baku += $var_detail[$k]['total'];
							 else
								$tot_pembantu += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
			} */
			
			$nomor = 1;
			if (is_array($query)) {
				$tot_hutang = 0;
				$tot_debet = 0;
				$tot_retur = 0;
				$tot_pembelian = 0;
				$tot_bulat1 = 0;
				$tot_bulat2 = 0;
				$tot_saldo_akhir = 0;
				
			 for($j=0;$j<count($query);$j++){
				 $tot_hutang+= $query[$j]['tot_hutang'];
				 $tot_debet+= $query[$j]['tot_debet'];
				 $tot_retur+= $query[$j]['tot_retur'];
				 $tot_pembelian+= $query[$j]['tot_pembelian'];
				 $tot_saldo_akhir+= $query[$j]['saldo_akhir'];
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$nomor."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['tot_hutang'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['tot_debet'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['tot_retur'],2,',','.')."</td>";
				 
				 if ($query[$j]['tot_bulat'] < 0) {
					echo    "<td align='right'>".number_format(abs($query[$j]['tot_bulat']),2,',','.')."</td>";
					$tot_bulat1+= abs($query[$j]['tot_bulat']);
				 }
				 else
					echo    "<td>&nbsp;</td>";
								 
				 echo    "<td align='right'>".number_format($query[$j]['tot_pembelian'],2,',','.')."</td>";
				 echo    "<td>&nbsp;</td>";
				 
				 if ($query[$j]['tot_bulat'] > 0) {
					echo    "<td align='right'>".number_format($query[$j]['tot_bulat'],2,',','.')."</td>";
					$tot_bulat2+= $query[$j]['tot_bulat'];
				 }
				 else
					echo    "<td>&nbsp;</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['saldo_akhir'],2,',','.')."</td>";
				 				 				 							 
				 echo  "</tr>";
				$nomor++;
		 	}
		 	echo "<tr class=\"record\">";
			
			echo    "<td colspan='2' align='right'>GRAND TOTAL</td>";
				 echo    "<td align='right'>".number_format($tot_hutang,2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($tot_debet,2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($tot_retur,2,',','.')."</td>";
				 
				 echo    "<td align='right'>".number_format($tot_bulat1,2,',','.')."</td>";
								 
				 echo    "<td align='right'>".number_format($tot_pembelian,2,',','.')."</td>";
				 echo    "<td>&nbsp;</td>";
				 
				 echo    "<td align='right'>".number_format($tot_bulat2,2,',','.')."</td>";
				 
				 echo    "<td align='right'>".number_format($tot_saldo_akhir,2,',','.')."</td>";
		 	
		 	echo "</tr>";
		   }
		 ?>
		 
 	</tbody>
</table><br>
<?php //echo $this->pagination->create_links();?>
</div>
