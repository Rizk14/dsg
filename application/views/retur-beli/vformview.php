<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 850;
		var height = 550;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function()
{
	$(".pilih").click(function()
	{
		var id_sj_retur=$("#id_sj_retur").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/retur-beli/cform/print_nota_debet/"+id_sj_retur;
		openCenteredWindow(urlnya);
	});
});

</script>

<h3>Data Nota Debet Retur</h3><br>
<a href="<?php echo base_url(); ?>index.php/retur-beli/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/retur-beli/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('retur-beli/cform/cari'); ?>
Supplier <select name="supplier" id="supplier">
				<option value="0" <?php if ($csupplier == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($csupplier == $sup->id) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_sj_retur" id="id_sj_retur">

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>No Dn Retur</th>
		 <th>No Dn Manual</th>
		 <th>Tgl Dn Retur</th>
		 <th>Jenis Pembelian</th>
		 <th>Range Tgl Faktur</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Jumlah</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){				 
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				$pisah1 = explode("-", $query[$j]['tgl_retur']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_retur = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['no_dn_retur']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['no_dn_retur_manual']."</td>";
				 echo    "<td>".$tgl_retur."</td>";
				 echo    "<td>";
				 if ($query[$j]['jenis_pembelian'] == 1)
					echo "Cash";
				 else
					echo "Kredit";
				echo "</td>";
				 //echo    "<td>".$query[$j]['no_faktur']."</td>";
				 
				 echo    "<td nowrap>";
				 /*if (is_array($query[$j]['detail_faktur'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_faktur'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_faktur']." / ".$var_detail[$k]['tgl_faktur'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 } */ 
				 echo $query[$j]['faktur_date_from']." s.d. ".$query[$j]['faktur_date_to'];
				 echo "</td>";
				 
				  echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo "<td style='white-space:nowrap;'>";
				 
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (".$var_detail[$k]['qty']." ".$var_detail[$k]['satuan'].")";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo    "<td style='white-space:nowrap;' align='right'>".number_format($query[$j]['jumlah'], 2, ',','.')."</td>";
				 echo    "<td>".$tgl_update."</td>";
					if ($query[$j]['status_nota'] == 'f') {
						echo    "<td align=center> <a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_sj_retur.value=".$query[$j]['id']."'; ><u>Print</u></a>
						&nbsp; <a href=".base_url()."index.php/retur-beli/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \">Edit</a>";
						echo    "&nbsp; <a href=".base_url()."index.php/retur-beli/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
					}
					else {
						echo    "<td align=center> <a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_sj_retur.value=".$query[$j]['id']."'; ><u>Print</u></a> </td>";
						
					}
			echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
