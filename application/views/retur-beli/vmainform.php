<h3>Data Nota Debet Retur</h3><br>
<a href="<?php echo base_url(); ?>index.php/retur-beli/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/retur-beli/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
	
	
	$('#pilih_faktur').click(function(){
			var supplier= $('#kode_supplier').val();
			var urlnya = "<?php echo base_url(); ?>index.php/retur-beli/cform/show_popup_faktur/"+supplier;
			openCenteredWindow(urlnya);

	  });
	
	$("#returpurchase").click(function() {
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
						
		//*****no_faktur*************************************
		var no_faktur="#no_faktur_"+n;
		var new_no_faktur="#no_faktur_"+no;
		$(no_faktur, lastRow).attr("id", "no_faktur_"+no);
		$(new_no_faktur, lastRow).attr("name", "no_faktur_"+no);		
		$(new_no_faktur, lastRow).val('');		
		//*****end no_faktur*********************************
		
		//*****faktur*************************************
		var faktur="#faktur_"+n;
		var new_faktur="#faktur_"+no;
		$(faktur, lastRow).attr("id", "faktur_"+no);
		$(new_faktur, lastRow).attr("name", "faktur_"+no);		
		$(new_faktur, lastRow).val('');		
		//*****end faktur*********************************
		
		//*****id_faktur*************************************
		var id_faktur="#id_faktur_"+n;
		var new_id_faktur="#id_faktur_"+no;
		$(id_faktur, lastRow).attr("id", "id_faktur_"+no);
		$(new_id_faktur, lastRow).attr("name", "id_faktur_"+no);		
		$(new_id_faktur, lastRow).val('');		
		//*****end id_faktur*********************************
		
		//*****no_sj*************************************
		var no_sj="#no_sj_"+n;
		var new_no_sj="#no_sj_"+no;
		$(no_sj, lastRow).attr("id", "no_sj_"+no);
		$(new_no_sj, lastRow).attr("name", "no_sj_"+no);		
		$(new_no_sj, lastRow).val('');		
		//*****end no_sj*********************************
		
		//*****qty_faktur*************************************
		var qty_faktur="#qty_faktur_"+n;
		var new_qty_faktur="#qty_faktur_"+no;
		$(qty_faktur, lastRow).attr("id", "qty_faktur_"+no);
		$(new_qty_faktur, lastRow).attr("name", "qty_faktur_"+no);		
		$(new_qty_faktur, lastRow).val('');		
		//*****end qty_faktur*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****cek*************************************
		var cek="#cek_"+n;
		var new_cek="#cek_"+no;
		$(cek, lastRow).attr("id", "cek_"+no);
		$(new_cek, lastRow).attr("name", "cek_"+no);		
		$(new_cek, lastRow).val('y');				
		//*****end cek*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/retur-beli/cform/show_popup_brg/"+no+"/<?php echo $id_supplier ?>');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		 $(new_pilih, lastRow).attr("disabled", false);		
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		var goedit = $("#goedit").val();
		var jumawal = $("#jumawal").val();
		
		if (parseInt(x) > parseInt(jumawal)) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
		
	});

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	
    
}

function cek_item_brg() {
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	if (jenis_pembelian == '0') {
		alert("Jenis pembelian harus dipilih..!");
		return false;
	}
	if (date_from == '') {
		alert("Tanggal awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal akhir harus dipilih..!");
		return false;
	}
	
	//if (date_to < date_from) {
	//	alert("Tanggal akhir harus lebih besar dari tanggal awal..!");
	//	return false;
	//}
	
	var tgl_dari = date_from.substr(0,2);
	var bln_dari = date_from.substr(3,2);
	var thn_dari = date_from.substr(6,4);
	var dari = new Date(thn_dari, bln_dari, tgl_dari);
	
	var tgl_ke = date_to.substr(0,2);
	var bln_ke = date_to.substr(3,2);
	var thn_ke = date_to.substr(6,4);
	var ke = new Date(thn_ke, bln_ke, tgl_ke);
	
	if (dari > ke) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}

}

function cek_retur() {
	var tgl_retur= $('#tgl_retur').val();
	var is_luar_range= $('#is_luar_range').val();
	var jum= $('#no').val()-1;
	//alert(jum);

	if (tgl_retur == '') {
		alert("Tanggal nota debet harus dipilih..!");
		return false;
	}
	
	//if (document.f_master_brg.sj.length > 0) {
		var xx = 0;
		for(var i=1; i <= jum; i++){
			//if ($('#cek_'+i).val() == 'y')
			if ($('#cek_'+i).attr('checked') )
				xx= xx + 1;
		}
		//alert (xx);
		if (xx == 0) {
			alert("Item barang yg mau diretur harus dipilih..!");
			return false;
		}
		//alert(jum); return false;
	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#cek_'+k).attr('checked') ) {
			
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					return false;
				}
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka..!");
					return false;
				}
				
				/*var qty_faktur = parseFloat($('#qty_faktur_'+k).val());
				var jum_retur = parseFloat($('#jum_retur_'+k).val());
				var qty = parseFloat($('#qty_'+k).val());
				var cek_jumlah = 0;
				
				cek_jumlah = qty_faktur-jum_retur-qty;
				if (cek_jumlah < 0) {
					alert("Jumlah retur melebihi qty di faktur...!");
					return false;
				}*/
				
				// koreksi 31 okt 2011 dari ari: qty yg diretur boleh melebihi qty di sj tsb
				/*if (is_luar_range == '') {
					var qty = parseFloat($('#qty_'+k).val());
					var qty_faktur = parseFloat($('#qty_faktur_'+k).val());
					
					if (qty > qty_faktur) {
						alert("Jumlah qty retur melebihi qty di SJ...!");
						return false;
					}
				} */
				
			} // end checkbox
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
		
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_pp', 'id' => 'f_pp');
echo form_open('retur-beli/cform/', $attributes); ?>
<table>
		<tr>
			<td>Supplier</td>
			<td><select name="id_supplier" id="id_supplier" onkeyup="this.blur();this.focus();" >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr>
		<!-- <tr>
			<td>No Faktur Pembelian</td>
			<td> <input type="text" name="no_faktur" id="no_faktur" value="" size="40" maxlength="40" readonly="true"><input type="hidden" name="id_sj" value="">&nbsp;
				<input type="hidden" name="id_faktur" id="id_faktur" value="">
			<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data Faktur Pembelian">
			</td>
		</tr> -->
		<tr>
			<td>Jenis Pembelian</td>
			<td><select name="jenis_pembelian" id="jenis_pembelian">
					<option value="0">-Pilih-</option>
					<option value="1">Cash</option>
					<option value="2">Kredit</option>
				</select>&nbsp;</td> 
		</tr>
		<tr>
			<td>Faktur Dari tanggal</td>
			<td>
			<label>
			  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
			</label>
			   <img alt="" id="date_from" align="middle"
					title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
					onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
			</td>
	  </tr>
	  <tr>
			<td>Faktur Sampai tanggal</td>
			<td>
			<label>
			  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
			</label>
			   <img alt="" id="date_to" align="middle"
					title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
					onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
			</td>
	  </tr>
	  
	  <tr>
		<td> Ada item barang diluar range tgl faktur </td>
		<td> <input type="checkbox" name="is_luar_range" id="is_luar_range" value="t"> &nbsp;
		 </td>
	</tr>
		
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/retur-beli/cform/view'">
<?php echo form_close(); 
} else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/retur-beli/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="date_from" value="<?php echo $date_from ?>">
<input type="hidden" name="date_to" value="<?php echo $date_to ?>">
<input type="hidden" name="id_supplier" value="<?php echo $id_supplier ?>">
<input type="hidden" name="is_luar_range" id="is_luar_range" value="<?php echo $is_luar_range ?>">
<input type="hidden" name="jenis_pembelian" id="jenis_pembelian" value="<?php echo $jenis_pembelian ?>">

<?php 
		if (count($brg_detail)>0) {
			$no=1;
			$jumawal = count($brg_detail)+1;
			foreach ($brg_detail as $hitung) {
				$no++;
			}
		}
		else {
			$no=2;
			$jumawal = 2;
		}
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
<!--	<tr>
    <td width="15%">No Faktur Pembelian</td>
    <td width="70%">
      <?php //echo $no_faktur; ?>
    </td>
  </tr> -->
		<tr>
			<td width="15%">Jenis Pembelian</td>
			<td width="70%"><?php if($jenis_pembelian == 1) echo "Cash"; else echo "Kredit"; ?></td> 
		</tr>
		<tr>
			<td>Range Tgl Faktur</td>
			<td>
			  <?php echo $date_from. " s.d. ".$date_to; ?>
			</td>
	  </tr>
		<tr>
			<td>Supplier</td>
			<td><?php echo $kode_supplier." - ". $nama_supplier ?></td> 
		</tr>

		<tr>
			<td>No Nota Debet</td>
			<td><input type="text" name="no_dn" id="no_dn" value="<?php echo $nomordn ?>" size="20"></td>
		</tr>
		
		<tr>
			<td>No Nota Debet Manual</td>
			<td><input type="text" name="no_dn2" id="no_dn2" value="" size="20"></td>
		</tr>
  <tr>
  <tr>
    <td>Tgl Nota Debet</td>
    <td>
	<label>
      <input name="tgl_retur" type="text" id="tgl_retur" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_retur" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_retur,'dd-mm-yyyy',this)">
	</td>
  </tr>
  
		<tr>
			<td>Keterangan</td>
			<td><input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
	<?php if ($is_luar_range == 't') { ?>
		<tr>
			<td colspan="2">Ada item barang diluar range tgl faktur</td>
		</tr>
		<?php } ?>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="11" align="left">
			<?php 
				if ($is_luar_range == 't') {
			?>
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			<?php
				}
			?>
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>No / Tgl Faktur</th>
          <th>No / Tgl SJ</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
           <th>Harga (Rp.)</th>
           <th>Qty di SJ</th>
	      <!-- <th>Qty di Faktur</th>
	       <th>Qty Pernah Diretur</th> -->
	       <th>Qty Yg Diretur</th>
	      <th>Pilih</th>
        </tr>

        <?php $i=1;
        if (count($brg_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap" colspan="6">
		   Data faktur dalam range tanggal yang dipilih tidak ada, atau belum diapply stoknya di gudang</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($brg_detail);$j++){
			?>

          <input name="faktur_<?php echo $i ?>" type="hidden" id="faktur_<?php echo $i ?>" value="<?php echo $brg_detail[$j]['no_faktur'] ?>" />
          <input name="id_faktur_<?php echo $i ?>" type="hidden" id="id_faktur_<?php echo $i ?>" value="<?php echo $brg_detail[$j]['id_faktur'] ?>" />
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
           <input name="no_faktur_<?php echo $i ?>" type="text" id="no_faktur_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $brg_detail[$j]['no_faktur']." / ".$brg_detail[$j]['tgl_faktur'] ?>"/>
           </td>
          <td nowrap="nowrap">
           <input name="no_sj_<?php echo $i ?>" type="text" id="no_sj_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $brg_detail[$j]['no_sj']." / ".$brg_detail[$j]['tgl_sj'] ?>"/>
           </td>
          <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="15" readonly="true" value="<?php echo $brg_detail[$j]['kode_brg'] ?>"/>
           <input name="id_brg_<?php echo $i ?>" type="hidden" id="id_brg_<?php echo $i ?>" value="<?php echo $brg_detail[$j]['id_brg'] ?>"/>
           <input disabled title="browse data barang" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/retur-beli/cform/show_popup_brg/<?php echo $i."/".$id_supplier ?>');">
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $brg_detail[$j]['nama'] ?>" /></td>
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="5" readonly="true" value="<?php echo $brg_detail[$j]['satuan'] ?>" /></td>
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="7" readonly="true" value="<?php echo $brg_detail[$j]['harga'] ?>" /></td>
		<td><input name="qty_faktur_<?php echo $i ?>" type="text" id="qty_faktur_<?php echo $i ?>" size="5"
          value="<?php echo $brg_detail[$j]['qty_faktur'] ?>" readonly="true" /></td>
         
         <!-- <td><input name="qty_faktur_<?php //echo $i ?>" type="text" id="qty_faktur_<?php //echo $i ?>" size="5"
          value="<?php //echo $brg_detail[$j]['qty_faktur'] ?>" readonly="true" /></td>
         <td><input name="jum_retur_<?php //echo $i ?>" type="text" id="jum_retur_<?php //echo $i ?>" size="5"
          value="<?php //echo $brg_detail[$j]['jum_retur'] ?>" readonly="true" /></td> -->
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="5" value="0" /></td>
          <td><input name="cek_<?php echo $i ?>" type="checkbox" id="cek_<?php echo $i ?>" value="y" /> </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>		
	</table>	
	
	</form>
		 <div align="center"><br><br> 
			<input type="submit" name="submit2" value="Simpan" onclick="return cek_retur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/retur-beli/cform/'">
		 </div>
	
      </td>
    </tr>

</table>
</div>
</form>
<?php } ?>
