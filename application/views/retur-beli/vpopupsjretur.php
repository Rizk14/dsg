<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    .judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$("#pilih").click(function()
	{
		
		opener.document.forms["f_purchase"].no_sj.value = '';
		var jumlah = 0;
		
		if (document.f_master_brg.sj.length > 0) {
			for(var i=0; i < document.f_master_brg.sj.length; i++){
				if(document.f_master_brg.sj[i].checked) {
					//total +=document.form1.scripts[i].value + "\n"
					opener.document.forms["f_purchase"].no_sj.value+= document.f_master_brg.sj[i].value + ", ";
					jumlah = parseFloat(jumlah) + parseFloat(document.f_master_brg.jum[i].value); 
				}
			}
		}
		else {
			opener.document.forms["f_purchase"].no_sj.value = document.f_master_brg.sj.value;
			jumlah = parseFloat(jumlah) + parseFloat(document.f_master_brg.jum.value); 
		}
		
		opener.document.forms["f_purchase"].jum.value = jumlah;
		
		self.close();
	});
});
</script>

<center><h3>Daftar SJ Retur Pembelian Yg Blm Dinotakan <br> di <?php echo $nama_supplier ?></h3></center>
<i> * Ini harus sudah ada faktur pajaknya </i> <br>
<div align="center"><br>
<?php 
	echo form_open('retur-beli/cform/show_popup_retur'); ?>
	<input type="hidden" name="csupplier" value="<?php echo $csupplier ?>">
	<input type="hidden" name="jnsaction" value="<?php echo $jnsaction ?>">
	<input type="hidden" name="no_notanya" value="<?php echo $no_notanya ?>">
<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); 


?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="no_faktur" id="no_faktur">

	<table border="1" align="center" width="100%" cellpadding="1" cellspacing="2" bordercolor="#666666" nowrap="nowrap">
	<thead>
	 <tr class="judulnya">
		 <th>No/Tgl Dn Retur</th>
		 <th>Supplier</th>
		 <th>Jum Total (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_retur']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_retur = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_dn_retur']." / ".$tgl_retur."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['tot_sj'], 2, ',','.')."</td>";
				 echo    "<td>".$tgl_update."</td>"; ?>
				 <td align=center>
				 <input type="hidden" name="jum" id="jum" value="<?php echo $query[$j]['tot_sj'] ?>">
				 <input type="checkbox" name="sj" id="sj" value="<?php echo $query[$j]['no_dn_retur'] ?>">
				 
				 </td>
				<?php echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table>
</form>
  <?php echo $this->pagination->create_links();
  ?>
  <input type="button" name="pilih" id="pilih" value="Pilih" >
</div>
