<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 800;
		var height = 500;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function()
{
	$(".pilih").click(function()
	{
		var id_nota_retur=$("#id_nota_retur").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/retur-beli/cform/print_nota_retur/"+id_nota_retur;
		openCenteredWindow(urlnya);
	});
});

</script>

<h3>Data Nota Retur Pembelian</h3><br>
<a href="<?php echo base_url(); ?>index.php/retur-beli/cform/notaretur">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/retur-beli/cform/viewnotaretur">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('retur-beli/cform/carinotaretur'); ?>
Supplier <select name="supplier" id="supplier">
				<option value="0" <?php if ($csupplier == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($csupplier == $sup->id) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_nota_retur" id="id_nota_retur">

<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		 <th>No/Tgl Nota </th>
		 <th>No/Tgl Dn Retur</th>
		 <th>Supplier</th>
		 <th>Jum Total (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>".$query[$j]['no_nota']." / ".$query[$j]['tgl_nota']."</td>";
				 
				 echo "<td nowrap>";
				 
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_dn_retur']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['totalnya'], 2, ',','.')."</td>";
				 echo    "<td>".$query[$j]['tgl_update']."</td>";
				
				// edit dan delete hanya muncul jika blm ada pembayaran
				//if ($query[$j]['status_faktur_pajak'] == 'f') {
					 echo    "<td align=center><a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_nota_retur.value=".$query[$j]['id']."'; ><u>Print</u></a> 
							&nbsp;<a href=".base_url()."index.php/retur-beli/cform/editnotaretur/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a>";
					 echo    "&nbsp; <a href=".base_url()."index.php/retur-beli/cform/deletenotaretur/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				/*}
				else {
					echo "<td align=center> </td>";
				} */
					 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
