<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
	font-size: 12px;
  }

</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	//window.print();
	var go_print= jQuery('#go_print').val();
	
	if (go_print == '1')
		window.print();
});
</script>
<?php
	include ("php/fungsi.php");
	if ($proses != '') {
	//$tgl = date("d-m-Y");
	$pisah1 = explode("-", $query[0]['tgl_retur']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
				
		//$bilangan = new Terbilang;
		//$kata=ucwords($bilangan->eja($tot_retur));
		
		//$kata=&Terbilang($tot_retur);
		$kata = &terbilangxx($tot_retur,3); 

?>
<input type="hidden" name="go_print" id="go_print" value="1" >
<table border='0' align="center" width="80%">
	<tr>
		<td colspan="3" align="center"> <h2><i>Nota Debet</i></h2> </td>
	</tr>
	<tr>
		<td>No. <?php if ($chkdn == '') echo $query[0]['no_dn_retur']; else echo $query[0]['no_dn_retur_manual']; ?> </td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td nowrap>Bandung, <?php echo $tgl ?></td>
	</tr>

	<tr>
		<td>Kepada : <?php echo $query[0]['nama_supplier'] ?></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Kami telah mendebet faktur Saudara tanggal <?php echo $tgl ?> <br> Sejumlah : Rp. <?php echo number_format($tot_retur,2,',','.') ?> 
		untuk faktur tanggal <?php echo $faktur_date_from ?> - <?php echo $faktur_date_to ?> </td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Terbilang : <b><i> <?php echo $kata ?> Rupiah </i></b></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Ket : Retur Barang</td>
	</tr>
	<?php $i=1;
        if (count($query[0]['detail_retur'])==0) {
		?>
		<tr align="center">
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
	<?php
		} else {
			$detailnya = $query[0]['detail_retur'];
			for($j=0;$j<count($query[0]['detail_retur']);$j++){
			?>
			<tr>
			<td nowrap><?php echo $detailnya[$j]['nama']." = ".number_format($detailnya[$j]['qty'],0,',','.')." ".$detailnya[$j]['satuan']." x Rp.". number_format($detailnya[$j]['harga'],2,',','.')." = "; 
				$perkalian = $detailnya[$j]['qty'] * $detailnya[$j]['harga'];
				echo number_format($perkalian,2,',','.');
			?> </td>
          
        </tr>
		<?php $i++; } // end foreach 
		}
		?>

	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami,</td>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;Penerima,</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
		<td>&nbsp;</td>
		<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
	</tr>
	

</table>
<?php 
	}
	else {
		$attributes = array('name' => 'f_dn', 'id' => 'f_dn');
		echo form_open('retur-beli/cform/print_nota_debet', $attributes);
?>
	<input type="hidden" name="proses" value="1" >
	<input type="hidden" name="id_sj_retur" value="<?php echo $id_sj_retur ?>" >

		Pakai Nomor DN Manual : <input type="checkbox" name="chkdn" id="chkdn" value="t" > <br><br>
		<input type="submit" name="submit" value="Go Print">
<br>
<?php 
	echo form_close(); 
	}
?>
