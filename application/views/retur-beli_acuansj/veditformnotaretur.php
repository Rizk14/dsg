<h3>Data Nota Retur Pembelian</h3><br>
<a href="<? echo base_url(); ?>index.php/retur-beli/cform/notaretur">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/retur-beli/cform/viewnotaretur">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	/* $('#pilih_faktur').click(function(){
		var id_sup= jQuery('#supplier').val();
		var urlnya = "<?php echo base_url(); ?>index.php/retur-beli/cform/show_popup_retur/A/"+id_sup+"/xxx";
			openCenteredWindow(urlnya);
	  });
	  
	  $('#supplier').change(function(){
	  	    $("#no_sj").val('');	
	  	    $("#jum").val('0');	
	  }); */
	
	
	$('#pilih_faktur').click(function(){
		var id_sup= jQuery('#kode_supplier').val();
		var no_fp= jQuery('#no_fp').val();
		var urlnya = "<?php echo base_url(); ?>index.php/retur-beli/cform/show_popup_retur/E/"+id_sup+"/"+no_fp;
		
		openCenteredWindow(urlnya);
	  });
	
	$('#supplier').change(function(){
	  	    $("#no_sj").val('');	
	  	    $("#jum").val('0');	
	  });
	  
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_faktur() {
	var no_sj= $('#no_sj').val();
	var tgl_fp= $('#tgl_fp').val();
	var jum= $('#jum').val();
	
	if (no_sj == '') {
		alert("No Dn Retur harus dipilih..!");
		$('#no_fp').focus();
		return false;
	}
	if (tgl_fp == '') {
		alert("Tanggal Nota harus dipilih..!");
		$('#tgl_fp').focus();
		return false;
	}
	
	if (jum == '' || jum == '0') {
		$('#jum').focus();
		alert("Jumlah Total harus diisi..!");
		return false;
	}	
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/retur-beli/cform/updatedatanota" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_nota" value="<?php echo $id_nota ?>">
<input type="hidden" name="no_nota_lama" value="<?php echo $query[0]['no_nota'] ?>">
<br>
Edit Data<br><br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >

  <tr>
	<td>Supplier </td>
	<td><select name="kode_supplier" id="kode_supplier" disabled="true">
				
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" <?php if ($sup->kode_supplier == $query[0]['kode_supplier']) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>
		<input type="hidden" name="supplier" id="supplier" value="<?php echo $query[0]['kode_supplier'] ?>">
	</td>
</tr>

  <tr>
		<td width="15%">Nomor Dn Retur</td>
		<td width="70%"> <input type="text" name="no_sj" id="no_sj" value="<?php echo $query[0]['no_sj'] ?>" size="40" maxlength="40" readonly="true">
		&nbsp; <input type="hidden" name="no_sj_lama" id="no_sj_lama" value="<?php echo $query[0]['no_sj'] ?>" size="40" maxlength="40">
		<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data sj pembelian">
		</td>
	</tr>
  <tr>
    <td>No Nota Retur</td>
    <td>
      <input name="no_fp" type="text" id="no_fp" size="20" maxlength="20" value="<?php echo $query[0]['no_nota'] ?>" readonly="true">
    </td>
  </tr>
  <tr>
    <td>Tanggal Nota</td>
    <td>
	<label>
      <input name="tgl_fp" type="text" id="tgl_fp" size="10" value="<?php echo $query[0]['tgl_nota'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_fp" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_fp,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td>Jumlah Total</td>
    <td>
      <input name="jum" type="text" id="jum" size="10" maxlength="10" value="<?php echo $query[0]['jumlah'] ?>" readonly="true">
    </td>
  </tr>
  
  <tr><td>&nbsp;</td>
	<td><input type="submit" name="submit" value="Edit" onclick="return cek_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/retur-beli/cform/viewnotaretur'"></td>
  </tr>

</table>
</form>
