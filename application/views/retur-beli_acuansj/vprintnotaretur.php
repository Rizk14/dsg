<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }

</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	var go_print= jQuery('#go_print').val();
	
	if (go_print == '1')
		window.print();
});
</script>
<?php
	if ($proses != '') {
	//$tgl = date("d-m-Y");
	$pisah1 = explode("-", $query[0]['tgl_nota']);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
			
				$pisah1 = explode("-", $query[0]['tgl_faktur_pajak']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_faktur_pajak = $tgl1." ".$nama_bln." ".$thn1;
?>
<input type="hidden" name="go_print" id="go_print" value="1" >
<table border='0' align="center">
	<tr>
		<td colspan="4" align="center">Nota Retur</td>
	</tr>
	<tr>
		<td colspan="4" align="center">Nomor : <?php echo $query[0]['no_nota'] ?></td>
	</tr>
	<tr>
		<td colspan="4" align="center">(Atas Faktur Pajak Nomor : <?php echo $query[0]['no_faktur_pajak'] ?> Tanggal <?php echo $tgl_faktur_pajak ?>)</td>
	</tr>
	<tr>
		<td>Pembeli BKP</td>
	</tr>
	<tr>
		<td width="15%">Nama</td>
		<td colspan="3">: CV DUTA SETIA GARMEN</td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td colspan="3" nowrap>: Jl. Salendro Timur X No. 1A Rt. 003 / Rw. 010 Bandung</td>
	</tr>
	<tr>
		<td>NPWP</td>
		<td colspan="3">: 02.083.975.9.441.000</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap>Kepada Penjual</td>
	</tr>
	<tr>
		<td width="15%">Nama</td>
		<td colspan="3">: <?php echo $query[0]['nama_npwp'] ?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td colspan="3" nowrap>: <?php echo $query[0]['alamat'] ?></td>
	</tr>
	<tr>
		<td>NPWP</td>
		<td colspan="3">: <?php echo $query[0]['npwp'] ?></td>
	</tr>

	
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<table border='1' >
				<tr>
          <th width="20">No. Urut</th>
          <th>Macam dan Jenis BKP</th>
          <th>Kuantum*</th>
           <th>Harga Satuan Menurut Faktur Pajak (Rp.)</th>
           <th>Harga Jual BKP (Rp.)</th>
        </tr>

        <?php $i=1;
			$tot = 0;
        if (count($query[0]['detail_brg'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
			$detailnya = $query[0]['detail_brg'];
			for($j=0;$j<count($query[0]['detail_brg']);$j++){
			?>
			<tr>
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td width="30%" nowrap><?php echo $detailnya[$j]['nama_brg'] ?></td>
          <td width="10%" nowrap="nowrap"><?php echo number_format($detailnya[$j]['qty'],0,',','.')." ".$detailnya[$j]['satuan'] ?></td>
          <td width="25%" nowrap align="right"><?php echo number_format($detailnya[$j]['harga']/1.1,2,',','.') ?></td>
          <?php $perkalian = $detailnya[$j]['qty'] * ($detailnya[$j]['harga']/1.1); $tot+= $perkalian; ?>
          <td nowrap align="right"><?php echo number_format($perkalian,2,',','.') ?></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td colspan="4">Jumlah Harga Jual BKP yang dikembalikan</td>
			<td align="right"> <?php echo number_format($tot,2,',','.') ?> </td>
		</tr>
		<tr>
			<td colspan="4">PPN yang diminta kembali</td>
			<td align="right"> <?php echo number_format($tot*0.1,2,',','.') ?> </td>
		</tr>
		<tr>
			<td colspan="4">PPnBM yang diminta kembali</td>
			<td align="right"> - </td>
		</tr>
		<tr>
			<td colspan="5" align="center"><?php echo $tgl ?><br><br><br><br><br> 
			( <?php echo $nama_kasie_acc ?> )<br>------------------------------------ <br> Kasie Accounting</td>
		</tr>
		<tr>
			<td colspan="5">Lembar Ke-1 : untuk PKP Penjualan<br> 
			Lembar Ke-2 : untuk PKP Pembeli<br>
			Lembar Ke-3 : untuk KPP tempat Pembeli terdaftar (dalam hal pembeli bukan PKP)
			</td>
		</tr>
			</table>
		</td>
		</tr>
	</td>
	</tr>
	<!-- didieu -->
</table>
*Khusus untuk retur BKP tidak terwujud, kolom ini tidak perlu diisi
<?php 
	}
	else { 
		$attributes = array('name' => 'f_nr', 'id' => 'f_nr');
		echo form_open('retur-beli/cform/print_nota_retur', $attributes);
?>
<input type="hidden" name="proses" value="1" >
	<input type="hidden" name="id_nota_retur" value="<?php echo $id_nota_retur ?>" >
<table>
	<tr>
		<td>Nama Kasie Accounting</td>
		<td>: <input type="text" name="nama_kasie_acc" id="nama_kasie_acc" value="" size="30" >

		</td>
	</tr>

	<tr>
		<td><input type="submit" name="submit" value="Go Print"></td>
	</tr>
</table><br>
<?php 
	echo form_close(); 
	}
?>
