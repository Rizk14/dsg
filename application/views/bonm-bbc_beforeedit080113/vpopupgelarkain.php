<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$("#no").val('2');
	
	$("#pilih").click(function()
	{
		
		var jum= $('#no').val()-1; 
		opener.document.forms["f_purchase"].gelar_pjg_kain_<?php echo $posisi ?>.value='';
		var gabung = "";
		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				if($('#gelar_'+k).val() == '0' || $('#gelar_'+k).val() == '' ) {				
					alert("Data gelar tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#gelar_'+k).val()) ) {
					alert("Data gelar harus berupa angka..!");
					return false;
				}
				if ($('#pjg_kain_'+k).val() == '0' || $('#pjg_kain_'+k).val() == '' ) {
					alert("Data panjang kain tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#pjg_kain_'+k).val()) ) {
					alert("Data panjang kain harus berupa angka..!");
					return false;
				}
				
				gabung += $('#gelar_'+k).val()+ "/"+ $('#pjg_kain_'+k).val()+";";
				opener.document.forms["f_purchase"].gelar_pjg_kain_<?php echo $posisi ?>.value= gabung;
				
			}
			
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
		
		self.close();
	});
	
	
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
				
		//*****gelar*************************************
		var nama="#gelar_"+n;
		var new_nama="#gelar_"+no;
		$(nama, lastRow).attr("id", "gelar_"+no);
		$(new_nama, lastRow).attr("name", "gelar_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end gelar*************************************	
		
		//*****pjg_kain*************************************
		var qty="#pjg_kain_"+n;
		var new_qty="#pjg_kain_"+no;
		$(qty, lastRow).attr("id", "pjg_kain_"+no);
		$(new_qty, lastRow).attr("name", "pjg_kain_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end pjg_kain*************************************			
						
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});

function hitunggelar() {
	var jum_detail= $('#no').val()-1;

				var i=1;
				var gelaran=$("#gelaran").val(); 
				for (i=1;i<=jum_detail;i++) {
					var pjg_kain=$("#pjg_kain_"+i).val();
					
					var gelar = parseFloat(pjg_kain)/parseFloat(gelaran);
					gelar = Math.floor(gelar);
					
					$("#gelar_"+i).val(gelar);
				}
}
</script>
<center><h3>Input Data Detail Gelar Panjang Kain</h3></center>
<div align="center">

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="gelaran" id="gelaran" value="<?php echo $gelaran ?>">
</form>

  <table border="0" id="tabelku" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
  <tr>
			<td colspan="3" align="right">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item ">
			</td>
		</tr>

    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Panjang Kain (m)</th>
      <th bgcolor="#999999">Gelar</th>

    </tr>
	<tr align="center">
		<td align="center" id="num_1">1</td>
		<td><input type="text" name="pjg_kain_1" id="pjg_kain_1" size="5" value="" onkeyup="hitunggelar()" onblur="hitunggelar()"></td>
		<td><input type="text" name="gelar_1" id="gelar_1" size="5" value="" ></td>
	</tr>
  </table>
  <input type="button" id="pilih" name="pilih" value="OK">
</div>
