<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var id_sc_detail =$("#id_sc_detail").val();
		var no_sc =$("#no_sc").val();
		opener.document.forms["f_purchase"].sc_<?php echo $posisi ?>.value= '';
		opener.document.forms["f_purchase"].id_sc_detail_<?php echo $posisi ?>.value = id_sc_detail;
		
		var temp_sc = '';
		if (temp_sc != no_sc) {
			opener.document.forms["f_purchase"].sc_<?php echo $posisi ?>.value= no_sc+';';
			temp_sc = no_sc;
		}
				
		self.close();
	});
});
</script>

<center><h3>Daftar Item Brg Di Schedule Cutting Non-Dacron Yang Belum Realisasi</h3></center>
<div align="center"><br>
<?php echo form_open('bonm-bbc/cform/show_popup_sched_cutting'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">
<input type="hidden" name="posisi" id="posisi" value="<?php echo $posisi ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_sc_detail" id="id_sc_detail">
<input type="hidden" name="no_sc" id="no_sc">

	<table border="1" align="center" cellpadding="1" width="100%" cellspacing="2" bordercolor="#666666" nowrap="nowrap">
	<thead>
	 <tr>
		 <th>No Schedule</th>
		 <th>Tgl</th>
		 <th>Utk Barang Jadi</th>
		 <th>Plan Qty (Set)</th>
		 <th>Bhn Baku/Pembantu</th>
		 <th>Waktu</th>
		 <th>Operator</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				 $pisah1 = explode("-", $query[$j]['tgl_cutting']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_cutting = $tgl1." ".$nama_bln." ".$thn1; 
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_schedule']."</td>";
				 echo    "<td>".$tgl_cutting."</td>";
				 echo    "<td>".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."</td>";
				 echo    "<td>".$query[$j]['qty_bhn']."</td>";
				 if ($query[$j]['kode_brg_quilting'] == '')
					echo    "<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']." (".$query[$j]['satuan'].")</td>";
				else
					echo    "<td>".$query[$j]['kode_brg_quilting']." - ".$query[$j]['nama_brg_quilting']." (".$query[$j]['satuan_quilting'].")</td>";
				 
				 echo    "<td>".$query[$j]['jam_mulai']." - ".$query[$j]['jam_selesai']."</td>";
				 echo    "<td>".$query[$j]['operator_cutting']."</td>";
				 echo    "<td>".$tgl_update."</td>"; ?>
				 <td align=center><a class="pilih" style="cursor:pointer" id="pilih" 
				 onMouseOver="window.document.f_master_brg.id_sc_detail.value='<?php echo $query[$j]['id'] ?>';
				 window.document.f_master_brg.no_sc.value='<?php echo $query[$j]['no_schedule'] ?>' ">Pilih</a></td>
				<?php echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table>
</form>
  <? echo $this->pagination->create_links();?>
</div>
