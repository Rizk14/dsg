<h3>Laporan Kartu Stok Bahan Hasil Quilting</h3><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	$('#kel_brg').change(function(){
		$("#brg_baku").val('');
		$("#kode_brg").val('');	
	  });

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_input() {
	var brg_baku= $('#brg_baku').val();
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();

	if (brg_baku == '') {
		alert("Bahan Quilting harus dipilih..!");
		return false;
	}
	
	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	if (date_from > date_to) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
$attributes = array('name' => 'f_mutasi', 'id' => 'f_mutasi');
echo form_open('info-mutasi-quilting/cform/view', $attributes); ?>
<table width="60%">
	<tr>
		<td>Nama Barang</td>
		<td> <input name="brg_baku" type="text" id="brg_baku" size="40" readonly="true" value=""/>
			<input type="hidden" name="kode_brg" id="kode_brg" value="">
			<input title="browse data bahan quilting" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/info-mutasi-quilting/cform/show_popup_brg/');" type="button"></td>
	</tr>
	<tr>
		<td width="20%">Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
  </tr>

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/info-mutasi-quilting/cform/'">
<?php echo form_close();  ?>
