<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var des=$("#des").val();
		
		opener.document.forms["f_mutasi"].brg_baku.value=idx+" - "+des;
		opener.document.forms["f_mutasi"].kode_brg.value=idx;
		opener.document.forms["f_mutasi"].date_from.focus();
		self.close();
	});
});
</script>
<center><h3>Daftar Bahan Hasil Quilting</h3></center>
<div align="center"><br>
<?php echo form_open('info-mutasi-quilting/cform/show_popup_brg'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="des" id="des">
</form>

  <table border="1" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" width="100%" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Stok Terkini</th>
      <th bgcolor="#999999">Fungsi</th>
    </tr>
	<?php 
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
				
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg']; ?></td>
      <td><?php echo $query[$j]['nama_brg']; ?></td>
      <td><?php echo $query[$j]['satuan']; ?></td>
      <td><?php if ($query[$j]['jum_stok']!= 0) echo $query[$j]['jum_stok']; else echo "0"; ?></td>
      <td>
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
	  window.document.f_master_brg.des.value='<?php echo str_replace("\"", "&quot;", $query[$j]['nama_brg']) ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
