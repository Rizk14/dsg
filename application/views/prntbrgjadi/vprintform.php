<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_brgjadi; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_brgjadi; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box" width="100%">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'prntbrgjadi/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlbrgjadiform">
			
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_brgjadi_kd_brg;?> </td>
					<td width="1%">:</td>
					<td width="21%">
					  <input name="i_product" type="text" id="i_product" maxlength="15" readonly="true" value="<?php echo $idproduk; ?>">
					</td>
					<td width="20%">&nbsp;</td>
					<td width="16%"><?php echo $list_s_produksi;?> </td>
					<td width="0%">:</td>
					<td width="22%">
					  <input name="f_stop_produksi" type="checkbox" id="f_stop_produksi" value="1" readonly <?php echo $stop; ?> >
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_brgjadi_nm_brg;?> </td>
					<td>:</td>
					<td>
					  <input name="e_product" type="text" id="e_product" maxlength="200" readonly="true" value="<?php echo $nmproduk; ?> ">
					</td>
					<td>&nbsp;</td>
					<td><?php echo $list_brgjadi_tgl_penawaran;?> </td>
					<td>:</td>
					<td>
					  <input type="text" name="d_penawaran" readonly="true" value="<?php echo $tpenawaran; ?>">
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_brgjadi_kategori; ?></td>
					<td>:</td>
					<td>
					  <input name="e_category" type="text" id="e_category" maxlength="200" readonly="true" value="<?php echo $nmkat; ?>">
					</td>
					<td>&nbsp;</td>
					<td><?php echo $list_brgjadi_no_penawaran; ?> </td>
					<td>:</td>
					<td>
					  <input name="e_surat_penawaran" type="text" id="e_surat_penawaran" maxlength="50" readonly="true" value="<?php echo $penawaran; ?>">
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_brgjadi_merek; ?></td>
					<td>:</td>
					<td>
					  <input name="e_brand" type="text" id="e_brand" maxlength="50" readonly="true" value="<?php echo $nmmerek; ?>">
					</td>
					<td>&nbsp;</td>
					<td><?php echo $list_brgjadi_hjp; ?></td>
					<td>&nbsp;</td>
					<td>
					  <input name="v_hjp" type="text" id="v_hjp" maxlength="100" readonly="true" value="<?php echo $hjp; ?>">
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_brgjadi_layout; ?></td>
					<td>:</td>
					<td>
					  <input name="e_layout" type="text" id="e_layout" maxlength="50" readonly="true" value="<?php echo $nmtampilan; ?>">
					</td>
					<td>&nbsp;</td>
					<td><?php echo $list_brgjadi_status; ?></td>
					<td>:</td>
					<td>
					  <input name="n_status" type="text" id="n_status" maxlength="100" readonly="true" value="<?php echo $status; ?>">
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_brgjadi_qty; ?></td>
					<td>:</td>
					<td>
					  <input name="e_quality" type="text" id="e_quality" maxlength="50" readonly="true" value="<?php echo $kualitas; ?>">
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_brgjadi; ?></div></td>	
			</tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="7%" class="tdatahead">NO</td>
					<td width="23%" class="tdatahead"><?php echo $list_brgjadi_kd_motif; ?> </td>
					<td width="49%" class="tdatahead"><?php echo $list_brgjadi_nm_motif;?> </td>
					<td width="21%" class="tdatahead"><?php echo $list_brgjadi_stok; ?></td>
				  </tr>
				  <?php 
				  $no	= 1;
				  if(count($isi_detail)>0) {
					  foreach($isi_detail as $row) {
						$lmotif	.= "
							<tr>
								<td>".$no."</td>
								<td>".$row->imotif."</td>
								<td>".$row->motifname."</td>
								<td align=\"right\">".$row->qty."</td>
							</tr>";
						$no+=1;
					  }
					  echo $lmotif;
				  }
				  ?>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
					<input type="button" name="btnkeluar" value="<?php echo $button_keluar; ?>"onclick="window.location='<?php echo base_url(); ?>index.php/prntbrgjadi/cform'"/>

				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close()?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
