<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 18000;

t = null;
function closeMe() {
t = setTimeout("self.close()",howLong);
}

</script>

</head>

<body onLoad="window.print();closeMe();">

<?php
$ex_tpenawaran	= explode("-",$tpenawaran,strlen($tpenawaran)); // YYYY-mm-dd
$bl	= array(
	'01'=>'Januari',
	'02'=>'Februari',
	'03'=>'Maret',
	'04'=>'April',
	'05'=>'Mei',
	'06'=>'Juni',
	'07'=>'Juli',
	'08'=>'Agustus',
	'09'=>'September',
	'10'=>'Oktober',
	'11'=>'Nopember',
	'12'=>'Desember'
	);

$bl_new		= $ex_tpenawaran[1];
$tgl_new	= substr($ex_tpenawaran[2],0,1)=='0'?substr($ex_tpenawaran[2],1,1):$ex_tpenawaran[2];
$tpenawaran_new	= $tgl_new." ".$bl[$bl_new]." ".$ex_tpenawaran[0];
?>

<table cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> <?php echo "Item Barang Jadi"; ?></td></tr>
   <tr> 
     <td style="padding:0px;">

	<table width="100%">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'prntbrgjadi/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlbrgjadiform">
			
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="90%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="18%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_kd_brg;?> </td>
					<td width="1%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td width="30%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $idproduk!=""?$idproduk:"-"; ?>
					</td>
					<td width="4%">&nbsp;</td>
					<td width="14%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_s_produksi;?> </td>
					<td width="1%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td width="30%">
					  <input name="f_stop_produksi" type="checkbox" id="f_stop_produksi" value="1" readonly <?php echo $stop; ?> >
					</td>
				  </tr>
				  <tr>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_nm_brg;?> </td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $nmproduk!=""?$nmproduk:"-"; ?>
					</td>
					<td>&nbsp;</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_tgl_penawaran;?> </td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $tpenawaran!=""?$tpenawaran_new:"-"; ?>
					</td>
				  </tr>
				  <tr>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_kategori; ?></td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $nmkat!=""?$nmkat:"-"; ?>
					</td>
					<td>&nbsp;</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_no_penawaran; ?> </td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $penawaran!=""?$penawaran:"-"; ?>
					</td>
				  </tr>
				  <tr>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_merek; ?></td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $nmmerek!=""?$nmmerek:"-"; ?>
					</td>
					<td>&nbsp;</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_hjp; ?></td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">&nbsp;</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $hjp!=""?$hjp:"-"; ?>
					</td>
				  </tr>
				  <tr>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_layout; ?></td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $nmtampilan!=""?$nmtampilan:"-"; ?>
					</td>
					<td>&nbsp;</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_status; ?></td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					   <?php echo $status!=""?$status:"-"; ?>
					</td>
				  </tr>
				  <tr>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><?php echo $list_brgjadi_qty; ?></td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">:</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
					  <?php echo $kualitas!=""?$kualitas:"-"; ?>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
			  <?php echo $form_title_detail_brgjadi; ?></td>	
			</tr>
			  <tr>
				<td><table width="70%" border="1" cellspacing="0" cellpadding="0">
				  <tr bgcolor="#A4A4A4">
					<td width="7%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000; line-height:25px; padding-left:4px;">NO</td>
					<td width="23%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000; padding-left:4px;"><?php echo $list_brgjadi_kd_motif; ?> </td>
					<td width="49%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000; padding-left:4px;"><?php echo $list_brgjadi_nm_motif;?> </td>
					<td width="21%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000; padding-left:4px;"><?php echo $list_brgjadi_stok; ?></td>
				  </tr>
				  <?php 
				  $no	= 1;
				  if(count($isi_detail)>0) {
					  foreach($isi_detail as $row) {
						$lmotif	.= "
							<tr>
								<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; line-height:18px; border-bottom:thin #000000; border-right:thin #000000; padding-left:4px;\">".$no."</td>
								<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000; padding-left:4px;\">".$row->imotif."</td>
								<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000; padding-left:4px;\">".$row->motifname."</td>
								<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000; padding-left:4px;\" align=\"right\">".$row->qty."&nbsp;&nbsp;&nbsp;</td>
							</tr>";
						$no+=1;
					  }
					  echo $lmotif;
				  }
				  ?>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>