<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();

	if (kode == '') {
		alert("Kode Barang harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Barang harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
}

$(function()
{
	  
});
	
</script>

<h3>Data Bahan Bis-Bisan</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-brg-bisbisan/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
	echo form_open('mst-brg-bisbisan/cform/submit', $attributes); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="kodeedit" value="<?php echo $ekode ?>"><?php } ?>
	<table>
		<tr>
			<td>Kode Barang</td>
			<td> <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="20" size="20" <?php if ($edit == '1') { ?> readonly="true" <?php } ?> >
			</td>
		</tr>
		<tr>
			<td>Nama Barang</td>
			<td> <input type="text" name="nama" id="nama" value="<?php echo str_replace("\"", "&quot;", $enama_brg) ?>" maxlength="60" size="60"></td>
		</tr>
		<tr>
			<td>Satuan</td>
			<td> <select name="satuan" id="satuan">
				<?php foreach ($satuan as $sat) { ?>
					<option value="<?php echo $sat->id ?>" <?php if ($sat->id == $esatuan) { ?> selected="true" <?php } ?> ><?php echo $sat->nama ?></option>
				<?php } ?>
				</select></td>
		</tr>
		<tr>
			<td>Deskripsi</td>
			<td> <input type="text" name="deskripsi" value="<?php echo $edeskripsi ?>" maxlength="30" size="30"></td>
		</tr>
		<tr>
			<td>Gudang</td>
			<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($gud->id == $eid_gudang) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		
		<?php
			if ($edit == '1') {
		?>
		<tr>
			<td>Status Aktif</td>
			<td> <input type="checkbox" name="status_aktif" id="status_aktif" value="t" <?php if ($estatus_aktif == 't') { ?> checked="true" <?php } ?> ></td>
		</tr>
		<?php
			}
		?>
		
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view'">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>
