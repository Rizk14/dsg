<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<h3>Pengubahan Data Warna Barang Jadi (PIC / WIP)</h3><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_warna', 'id' => 'f_warna');
	echo form_open('pic/cform/editwarnabrgjadi5', $attributes); ?>
	<table width="40%">
		<tr>
			<td colspan="2" style="white-space:nowrap;">Step 4: Pilih warna-warna baru di tiap-tiap bahan baku material barang jadi<hr></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space:nowrap;">Barang Jadi</td>
			<td style="white-space:nowrap;"> <b><?php echo $kode_brg_jadi." - ".$nama_brg_jadi ?></b>
			<input type="hidden" name="kode_brg_jadi" id="kode_brg_jadi" value="<?php echo $kode_brg_jadi ?>">
			</td>
		</tr>

		<tr>
			  <td style="white-space:nowrap;">Material Brg Jadi Warna Asal</td>
			  <td style="white-space:nowrap;">
				<?php
				if (is_array($list_material_bhnbaku)) {
				?>
				<input type="hidden" name="adawarna" id="adawarna" value="ada">
				<?php
					$hitung = count($list_material_bhnbaku);
					for($j=0;$j<count($list_material_bhnbaku);$j++){
						echo $list_material_bhnbaku[$j]['kode_brg']." - ".$list_material_bhnbaku[$j]['nama_brg']." : ".$list_material_bhnbaku[$j]['nama_warna'];
						if ($j<$hitung-1)
						     echo "<br> ";
					}
				}
				else {
				?>
				<input type="hidden" name="adawarna" id="adawarna" value="">
				<?php
					echo "Belum ada data material barang jadi";
				}
				?>
			  </td>			  
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space:nowrap;">Material Brg Jadi Warna Baru</td>
			<td style="white-space:nowrap;">
				<input type="hidden" name="jumbhnbaku" value="<?php echo count($list_material_bhnbaku) ?>">
				<?php
				if (is_array($list_material_bhnbaku)) {
					$hitung = count($list_material_bhnbaku);
					$i=1;
					for($j=0;$j<count($list_material_bhnbaku);$j++){
						echo $list_material_bhnbaku[$j]['kode_brg']." - ".$list_material_bhnbaku[$j]['nama_brg']." ";
					?>
						<input type="hidden" name="kode_brg_<?php echo $i ?>" id="kode_brg_<?php echo $i ?>" value="<?php echo $list_material_bhnbaku[$j]['kode_brg'] ?>">
						&nbsp;<select name="kode_warna_<?php echo $i ?>" id="kode_warna_<?php echo $i ?>">
						<option value=""> -Pilih- </option>
						<?php
						for($xx=0;$xx<count($list_warna);$xx++){
							echo "<option value=".$list_warna[$xx]['kode_warna'].">".$list_warna[$xx]['nama_warna']."</option>";
						}
						?>
					</select>
					<?php
						if ($j<$hitung-1)
						     echo "<br> ";
						 $i++;
					}
				}
				else
					echo "Belum ada data material barang jadi";
				?>
			  </td>		
			</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Next" >
			
		</tr>
	</table>
<?php echo form_close(); ?> <br>
