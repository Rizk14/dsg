<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">
function konfirmasi() {
	var kodebrgjadi= $('#kode_brg_jadi').val();
	var s = 0;
	kon = window.confirm("Proses pengubahan telah sampai di tahap akhir. Anda yakin akan menyimpan semua data pengubahan yang berkaitan dgn warna untuk barang jadi "+kodebrgjadi+ "?");
	
	if (kon) {
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}
</script>

<h3>Pengubahan Data Warna Barang Jadi (PIC / WIP)</h3><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_warna', 'id' => 'f_warna');
	echo form_open('pic/cform/editwarnabrgjadi6', $attributes); ?>
	<table width="40%">
		<tr>
			<td colspan="2" style="white-space:nowrap;">Step 5: Masukkan stok hasil cutting (pengadaan) dengan warna-warna baru di tiap-tiap bahan baku material barang jadi<hr></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space:nowrap;">Barang Jadi</td>
			<td style="white-space:nowrap;"> <b><?php echo $kode_brg_jadi." - ".$nama_brg_jadi ?></b>
			<input type="hidden" name="kode_brg_jadi" id="kode_brg_jadi" value="<?php echo $kode_brg_jadi ?>">
			</td>
		</tr>
		
		<tr>
			  <td style="white-space:nowrap;">Stok Hasil Cutting Warna Asal</td>
			  <td style="white-space:nowrap;">
			  <?php
				if (is_array($list_stok_warna)) {
				?>
				<input type="hidden" name="adastokwarna" id="adastokwarna" value="ada">
				<?php
				for($j=0;$j<count($list_stok_warna);$j++){
					echo $list_stok_warna[$j]['nama_warna']." : ".$list_stok_warna[$j]['stok'];
					$hitung = count($list_stok_warna);
					if ($j<$hitung-1)
						echo "<br> ";								 
					}
				}
				else {
				?>
					<input type="hidden" name="adastokwarna" id="adastokwarna" value="">
				<?php
					echo "Belum ada data stok hasil cutting per warna";
				}
			  ?>	  
			  </td>
		</tr>
		<tr>
			  <td style="white-space:nowrap;">Stok Hasil Cutting Bhn Baku Asal</td>
			  <td style="white-space:nowrap;">
			  <?php
				if (is_array($list_stok_bhnbaku)) {
				?>
				<input type="hidden" name="adastokbhnbaku" id="adastokbhnbaku" value="ada">
				<?php
				for($j=0;$j<count($list_stok_bhnbaku);$j++){
					echo $list_stok_bhnbaku[$j]['kode_brg']." - ".$list_stok_bhnbaku[$j]['nama_brg']." (".$list_stok_bhnbaku[$j]['nama_warna'].") : ".$list_stok_bhnbaku[$j]['stok'];
					$hitung = count($list_stok_bhnbaku);
					if ($j<$hitung-1)
						echo "<br> ";								 
					}
				}
				else {
				?>
					<input type="hidden" name="adastokbhnbaku" id="adastokbhnbaku" value="">
				<?php
					echo "Belum ada data stok hasil cutting per bhn baku";
				}
			  ?>	  
			  </td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			  <td style="white-space:nowrap;">Stok Hasil Cutting Warna Baru</td>
			  <td style="white-space:nowrap;">
				  <input type="hidden" name="jumstokwarna" value="<?php echo count($list_warna) ?>">
			  <?php
				if (is_array($list_warna)) {
					$i=1;
					for($j=0;$j<count($list_warna);$j++){
						echo $list_warna[$j]['nama_warna']."&nbsp;";
				?>
					<input type="text" name="qty_warna_<?php echo $i ?>" value="0" size="3" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'">
					<input type="hidden" name="kode_warna_<?php echo $i ?>" value="<?php echo $list_warna[$j]['kode_warna'] ?>"><br>
				<?php
						$i++;
					}
				}
				else {
					echo "Belum ada data stok hasil cutting per warna";
				}
			  ?>	  
			  </td>
		</tr>
		<tr>
			  <td style="white-space:nowrap;">Stok Hasil Cutting Bhn Baku Baru</td>
			  <td style="white-space:nowrap;">
			  <input type="hidden" name="jumstokbhnbaku" value="<?php echo count($list_material_bhnbaku) ?>">
			  <?php
				if (is_array($list_material_bhnbaku)) {
					$i=1;
					for($j=0;$j<count($list_material_bhnbaku);$j++){
						echo $list_material_bhnbaku[$j]['kode_brg']." - ".$list_material_bhnbaku[$j]['nama_brg']." (".$list_material_bhnbaku[$j]['nama_warna'].")&nbsp;";
					?>
						<input type="text" name="qty_brg_<?php echo $i ?>" value="0" size="3" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'">
						<input type="hidden" name="kode_brg_<?php echo $i ?>" value="<?php echo $list_material_bhnbaku[$j]['kode_brg'] ?>">
						<input type="hidden" name="kode_warna_<?php echo $i ?>" value="<?php echo $list_material_bhnbaku[$j]['kode_warna'] ?>"><br>
					<?php
						$i++;
					}
				}
				else {
					echo "Belum ada data stok hasil cutting per bhn baku";
				}
			  ?>	  
			  </td>
		</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Selesai dan Simpan Perubahan" onclick="return konfirmasi();" >
			
		</tr>
	</table>
<?php echo form_close(); ?> <br>
