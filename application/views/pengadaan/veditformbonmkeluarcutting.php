<h3>Data SJ Keluar Bahan Baku (Hasil Cutting) Ke Jahitan</h3><br>
<a href="<?php echo base_url(); ?>index.php/pengadaan/cform/addbonmkeluarcutting">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/pengadaan/cform/viewbonmkeluarcutting">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>" . $msg . "</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>

<script>
	//tambah
	$(function() {
		//$("#no").val('2');

		//generate_nomor();

		$("#addrow").click(function() {
			//inisialisasi untuk id unik
			var no = $("#no").val();
			var n = parseInt(no) - 1;
			//copy last row
			var lastRow = $("#tabelku tr:last").clone();
			//----------------SETTING KONDISI ROW BARU*****
			//******no*************************************
			var num = "#num_" + n;
			var new_num = "#num_" + no;
			$(num, lastRow).attr("id", "num_" + no);
			$(new_num, lastRow).html(no);
			//******end no*********************************

			//*****kode_brg_wip*************************************
			var kode_brg_wip = "#kode_brg_wip_" + n;
			var new_kode_brg_wip = "#kode_brg_wip_" + no;
			$(kode_brg_wip, lastRow).attr("id", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("name", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("onkeyup", "cari('" + no + "', this.value);");
			$(new_kode_brg_wip, lastRow).val('');
			//*****end kode_brg_wip*********************************

			//*****brg_wip*************************************
			var brg_wip = "#brg_wip_" + n;
			var new_brg_wip = "#brg_wip_" + no;
			$(brg_wip, lastRow).attr("id", "brg_wip_" + no);
			$(new_brg_wip, lastRow).attr("name", "brg_wip_" + no);
			$(new_brg_wip, lastRow).val('');
			//*****end brg_wip*********************************

			//******div infobrgwip*************************************
			var infobrgwip = "#infobrgwip_" + n;
			var new_infobrgwip = "#infobrgwip_" + no;
			$(infobrgwip, lastRow).attr("id", "infobrgwip_" + no);
			$(new_infobrgwip, lastRow).html("<input type='text' id='brg_wip_" + no + "' name='brg_wip_" + no + "' value='' readonly='true' size='40'>" +
				"<input name='id_brg_wip_" + no + "' type='hidden' id='id_brg_wip_" + no + "' value='' >");

			//*****qty*************************************
			/*	var qty="#qty_"+n;
				var new_qty="#qty_"+no;
				$(qty, lastRow).attr("id", "qty_"+no);
				$(new_qty, lastRow).attr("name", "qty_"+no);		
				$(new_qty, lastRow).val('');	*/
			//*****end qty*************************************	

			// temp_qty
			var temp_qty = "#temp_qty_" + n;
			var new_temp_qty = "#temp_qty_" + no;
			$(temp_qty, lastRow).attr("id", "temp_qty_" + no);
			$(new_temp_qty, lastRow).attr("name", "temp_qty_" + no);
			$(new_temp_qty, lastRow).val('');

			//******div qtywarna*************************************
			var qtywarna = "#qtywarna_" + n;
			var new_qtywarna = "#qtywarna_" + no;
			$(qtywarna, lastRow).attr("id", "qtywarna_" + no);
			$(new_qtywarna, lastRow).html("<input type='hidden' id='temp_qty_" + no + "' name='temp_qty_" + no + "' value=''>");

			//*****stok*************************************
			var stok = "#stok_" + n;
			var new_stok = "#stok_" + no;
			$(stok, lastRow).attr("id", "stok_" + no);
			$(new_stok, lastRow).attr("name", "stok_" + no);
			$(new_stok, lastRow).val('');
			//*****end stok*************************************	

			//*****ket_detail*************************************
			var ket_detail = "#ket_detail_" + n;
			var new_ket_detail = "#ket_detail_" + no;
			$(ket_detail, lastRow).attr("id", "ket_detail_" + no);
			$(new_ket_detail, lastRow).attr("name", "ket_detail_" + no);
			$(new_ket_detail, lastRow).val('');
			//*****end ket_detail*************************************	

			//button pilih_brg*****************************************
			// 25-03-2014, DIKOMEN KARENA SKRG PAKE JQUERY ONKEYUP
			/* var pilih_brg="#pilih_brg_"+n;
		 var new_pilih_brg="#pilih_brg_"+no;
		 $(pilih_brg, lastRow).attr("id","pilih_brg_"+no);
            
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg_jadi/"+no+"');";

		 $(new_pilih_brg, lastRow).attr("name", "pilih_brg_"+no);		
		 $(new_pilih_brg, lastRow).attr("onclick",even_klik);		 */
			//end button pilih_brg

			//----------------END SETTING KONDISI ROW BARU*		
			//tambahin row nya sesuai settingan
			$("#tabelku").append(lastRow);
			//no++
			var x = parseInt(no);
			$("#no").val(x + 1);

		});

		$("#deleterow").click(function() {
			var x = $("#no").val();
			var jumawal = $("#jumawal").val();
			// kita rubah supaya boleh hapus semuaa
			//if (parseInt(x) > parseInt(jumawal)) {
			if (x > 2) {
				$("#tabelku tr:last").remove();
				var x = $("#no").val();
				var y = x - 1;
				$("#no").val(y);
			}
		});

	});
</script>
<script type="text/javascript">
	function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth / 2) - (width / 2));
		var top = parseInt((screen.availHeight / 2) - (height / 2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

	function cek_bonm() {
		var no_bonm_manual = $('#no_bonm_manual').val();
		var tgl_bonm = $('#tgl_bonm').val();
		var id_unit_jahit = $('#id_unit_jahit').val();
		var s = 0;
		kon = window.confirm("Yakin akan simpan data ini ??");

		if (kon) {
			if (no_bonm_manual == '') {
				alert("Nomor Bon M/SJ harus diisi..!");
				return false;
			}
			if (tgl_bonm == '') {
				alert("Tanggal Bon M/SJ harus dipilih..!");
				return false;
			}
			if (id_unit_jahit == '0') {
				alert("Unit Jahit harus dipilih..!");
				return false;
			}

			var jum = $('#no').val() - 1;

			if (jum > 0) {
				for (var k = 1; k <= jum; k++) {

					if ($('#brg_wip_' + k).val() == '') {
						alert("Data item barang WIP tidak boleh ada yang kosong...!");
						s = 1;
						return false;
					}
					/*	if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
							alert("Data qty tidak boleh 0 / kosong...!");
							s = 1;
							return false;
						}
						if (isNaN($('#qty_'+k).val()) ) {
							alert("Qty harus berupa angka..!");
							s = 1;
							return false;
						} */
					if ($('#temp_qty_' + k).val() == '') {
						alert("Data item barang " + $('#kode_brg_wip_' + k).val() + " belum ada warnanya, silahkan input dulu di menu Master Warna Brg WIP...!");
						s = 1;
						return false;
					}

				}
			} else {
				alert("Data detail tidak ada");
				s = 1;
				return false;
			}

			if (s == 0)
				return true;
		} // end if kon
		else
			return false;
	}

	function cari(posisi, kodebrgwip) {
		$.ajax({
			type: 'post',
			url: '<?php echo base_url(); ?>index.php/pengadaan/cform/caribrgwip',
			data: 'kode_brg_wip=' + kodebrgwip + '&posisi=' + posisi,
			success: function(response) {
				$("#infobrgwip_" + posisi).html(response);
			}
		});

		$.ajax({
			type: 'post',
			url: '<?php echo base_url(); ?>index.php/pengadaan/cform/additemwarna',
			data: 'kode_brg_wip=' + kodebrgwip + '&posisi=' + posisi,
			success: function(response) {
				$("#qtywarna_" + posisi).html(response);
				//window.close();
			}
		});
	}
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pengadaan/cform/updatedatabonmkeluarcutting" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id_bonm" value="<?php echo $query[0]['id'] ?>">
	<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
	<input type="hidden" name="tgl_awal" value="<?php echo $tgl_awal ?>">
	<input type="hidden" name="tgl_akhir" value="<?php echo $tgl_akhir ?>">
	<input type="hidden" name="id_unit_jahit" value="<?php echo $id_unit_jahit ?>">
	<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
	<input type="hidden" name="caribrg" value="<?php echo $caribrg ?>">
	<input type="hidden" name="filterbrg" value="<?php echo $filterbrg ?>">

	<input type="hidden" name="id_unit_lama" value="<?php echo $query[0]['id_unit'] ?>">

	<?php $detail_bonm = $query[0]['detail_bonm'];
	$no = 1;
	$jumawal = count($detail_bonm) + 1;

	foreach ($detail_bonm as $hitung) {
		$no++;
	}
	?>
	Edit Data

	<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
	<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
	<div align="center">
		<label id="status"></label>
		<br>

		<table class="proit-view" width="100%" cellspacing="2" cellpadding="1">
			<tr>
				<td width="15%">Nomor Bon M / SJ Manual</td>
				<td><input name="no_bonm_manual" type="text" id="no_bonm_manual" size="15" maxlength="15" value="<?php echo $query[0]['no_manual'] ?>">
				</td>

			</tr>
			<tr>
				<td>Tanggal Bon M / SJ</td>
				<td>
					<label>
						<input name="tgl_bonm" type="text" id="tgl_bonm" size="10" value="<?php echo $query[0]['tgl_bonm'] ?>" readonly="true">
					</label>
					<img alt="" id="tgl_bonm" align="middle" title="Pilih tgl.." src="<?php echo base_url(); ?>images/calendar.gif" onclick="displayCalendar(document.forms[0].tgl_bonm,'dd-mm-yyyy',this)">
				</td>
			</tr>

			<tr>
				<td>Dari</td>
				<td>
					<input name="esender" type="text" id="esender" value="<?php echo $query[0]['e_sender'] ?>" placeholder="Nama Penyerah" maxlength="125"> /
					<input style="width:200px" name="esendercompany" type="text" id="esendercompany" maxlength="125" value="<?php echo $query[0]['e_sender_company'] ?>">
				</td>
			</tr>

			<tr>
				<td>Untuk</td>
				<td>
					<input name="erecipient" type="text" id="erecipient" size="10" maxlength="125" value="<?php echo $query[0]['e_recipient'] ?>" placeholder="Nama Penerima"> /
					<input style="width:200px" name="erecipientcompany" type="text" id="erecipientcompany" placeholder="Nama Perusahaan/Departemen" maxlength="125" value="<?php echo $query[0]['e_recipient_company'] ?>">
				</td>
			</tr>
			<tr>
				<td width="15%">Periode Forecast</td>
				<td>
					<select name="bln_forecast" id="bln_forecast">
						<option selected="selected">Bulan</option>
						<?php
						$bulan = array("Bulan", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
						$jlh_bln = count($bulan);
						for ($c = 1; $c < $jlh_bln; $c += 1) {
							if (strlen($c) == 1) {
								$d = "0" . $c;
							} else {
								$d = $c;
							}
						?>
							<option value="<?php echo $d ?>" <?php if ($d == $query[0]['bln_forecast']) { ?> selected <?php } ?>><?php echo $bulan[$c] ?></option>
						<?php
						}
						?>
						<input name="thn_forecast" type="text" id="thn_forecast" size="4" value='<?php echo $query[0]['thn_forecast'] ?>' maxlength="4">
				</td>
			</tr>
			<tr>
				<td>Jenis Keluar</td>
				<td> <select name="jenis" id="jenis">
						<option value="1" <?php if ($query[0]['jenis'] == '1') { ?> selected <?php } ?>>Keluar Bagus</option>
						<!--<option value="2" <?php //if($query[0]['jenis']=='2') { 
												?> selected <?php //} 
															?> >Pengembalian Retur</option>-->
						<option value="3" <?php if ($query[0]['jenis'] == '3') { ?> selected <?php } ?>>Lain-Lain</option>
					</select>
				</td>
			</tr>

			<tr>
				<td>Unit Jahit</td>
				<td> <select name="id_unit" id="id_unit">
						<option value="0" <?php if ($query[0]['id_unit'] == '0') { ?>selected<?php } ?>>- Tidak Ada -</option>
						<?php foreach ($list_unit_jahit as $jht) { ?>
							<option value="<?php echo $jht->id ?>" <?php if ($jht->id == $query[0]['id_unit']) { ?> selected <?php } ?>><?php echo $jht->kode_unit . "-" . $jht->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>

			<tr>
				<td>Keterangan</td>
				<td>
					<input name="ket" type="text" id="ket" size="30" value="<?php echo $query[0]['keterangan'] ?>">
				</td>
			</tr>

			<tr>
				<td colspan="2"><br>
					<form name="myform">
						<table id="tabelku" border="0" align="center" cellpadding="1" cellspacing="2" class="proit-view">
							<tr>
								<td colspan="5" align="left">
									<input id="addrow" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
									<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
								</td>
							</tr>
							<tr>
								<th width="20">No</th>
								<th>Kode</th>
								<th>Nama Barang WIP</th>
								<th>Qty</th>
								<th>Keterangan</th>
							</tr>
							<?php $i = 1;
							if (count($query[0]['detail_bonm']) == 0) {
							?>
								<tr align="center">
									<td align="center" id="num_1">1</td>
									<td nowrap="nowrap">
										Data tidak ada</td>

								</tr>
								<?php
							} else {
								$detail_bonm = $query[0]['detail_bonm'];
								for ($k = 0; $k < count($detail_bonm); $k++) {
								?>

									<tr align="center">
										<td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
										<td><input name="kode_brg_wip_<?php echo $i ?>" type="text" id="kode_brg_wip_<?php echo $i ?>" size="10" value="<?php echo $detail_bonm[$k]['kode_brg_wip'] ?>" onkeyup="cari('<?php echo $i ?>',this.value);" /></td>
										<td style="white-space:nowrap;">
											<div id="infobrgwip_<?php echo $i ?>">
												<input name="brg_wip_<?php echo $i ?>" type="text" id="brg_wip_<?php echo $i ?>" size="40" value="<?php echo $detail_bonm[$k]['nama_brg_wip'] ?>" readonly="true" />
												<input name="id_brg_wip_<?php echo $i ?>" type="hidden" id="id_brg_wip_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['id_brg_wip'] ?>" />
												<!-- <input title="browse data barang jadi" name="pilih_brg_<?php echo $i ?>" value="..." type="button" id="pilih_brg_<?php echo $i ?>" 
          onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg_jadi/<?php echo $i ?>/');" > -->
											</div>
										</td>

										<!--  <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="5" value="<?php //echo $detail_bonm[$k]['qty'] 
																																			?>" />  </td> -->
										<td>
											<div id="qtywarna_<?php echo $i ?>" align="right">
												<?php
												if (is_array($detail_bonm[$k]['detail_warna'])) {
													$var_detail = $detail_bonm[$k]['detail_warna'];
													$hitung = count($var_detail);
													for ($zz = 0; $zz < count($var_detail); $zz++) {
														echo $var_detail[$zz]['nama_warna'] . "&nbsp;";
												?>
														<input type="text" name="qty_warna_<?php echo $i ?>[]" style="text-align: right;" value="<?php echo $var_detail[$zz]['qty_warna'] ?>" size="3" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'">
														<input type="hidden" name="id_warna_<?php echo $i ?>[]" value="<?php echo $var_detail[$zz]['id_warna'] ?>"><br>
													<?php
													}
													?>
													<input type="hidden" id="temp_qty_<?php echo $i ?>" name="temp_qty_<?php echo $i ?>" value="ada">
												<?php
												} else {
												?>
													<input type="hidden" id="temp_qty_<?php echo $i ?>" name="temp_qty_<?php echo $i ?>" value="">
												<?php
												}
												?>
											</div>
											<input name="stok_<?php echo $i ?>" type="hidden" id="stok_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['stok'] ?>" />
											<hr>
										</td>

										<td><input name="ket_detail_<?php echo $i ?>" type="text" id="ket_detail_<?php echo $i ?>" size="20" value="<?php echo $detail_bonm[$k]['keterangan'] ?>" /></td>

									</tr>
							<?php $i++;
								} // end foreach 
							}
							?>
						</table>

					</form>
					<div align="center"><br>
						<?php
						if ($carinya == '') $carinya = "all";
						$url_redirectnya = "pengadaan/cform/viewbonmkeluarcutting/" . $tgl_awal . "/" . $tgl_akhir . "/" . $id_unit_jahit . "/" . $carinya . "/" . $caribrg . "/" . $filterbrg . "/" . $cur_page;
						?>
						<input type="submit" name="submit2" value="Simpan" onclick="return cek_bonm();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">

					</div>
				</td>
			</tr>

		</table>
	</div>
</form>