<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

/*function cek_data() {
	var brg_jadi= $('#brg_jadi').val();
	var kode_brg_jadi= $('#kode_brg_jadi').val();

	if (kode_brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		$('#kode_brg_jadi').focus();
		return false;
	}
} */
</script>
<h3>Pengubahan Data Warna Barang Jadi (PIC / WIP)</h3><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_warna', 'id' => 'f_warna');
	echo form_open('pic/cform/editwarnabrgjadi4', $attributes); ?>
	<table width="40%">
		<tr>
			<td colspan="2" style="white-space:nowrap;">Step 3: Masukkan stok barang WIP di unit jahit sesuai dengan warna yang baru<hr></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space:nowrap;">Barang Jadi</td>
			<td style="white-space:nowrap;"> <b><?php echo $kode_brg_jadi." - ".$nama_brg_jadi ?></b>
			<input type="hidden" name="kode_brg_jadi" id="kode_brg_jadi" value="<?php echo $kode_brg_jadi ?>">
			</td>
		</tr>

		<tr>
			  <td style="white-space:nowrap;">Stok Barang WIP Warna Asal</td>
			  <?php
				if (is_array($list_stok_warna)) {
				?>
				<input type="hidden" name="adastok" id="adastok" value="ada">
				<?php
					for($j=0;$j<count($list_stok_warna);$j++){
			?>
				<td style="white-space:nowrap;"><div align="center"><u><?php echo $list_stok_warna[$j]['nama_unit'] ?></u></div><br>
				<div align="right">Stok Bagus:<br>
					<?php
				if (is_array($list_stok_warna[$j]['data_stok'])) {
					$var_detail = $list_stok_warna[$j]['data_stok'];
					$hitung = count($var_detail);
					for($zz=0;$zz<count($var_detail); $zz++){
						echo $var_detail[$zz]['nama_warna']." : ".$var_detail[$zz]['stok_bagus'];
						if ($zz<$hitung-1)
						     echo "<br> ";
					}
				}
				?><br><br>
				Stok Perbaikan:<br>
					<?php
				if (is_array($list_stok_warna[$j]['data_stok'])) {
					$var_detail = $list_stok_warna[$j]['data_stok'];
					$hitung = count($var_detail);
					for($zz=0;$zz<count($var_detail); $zz++){
						echo $var_detail[$zz]['nama_warna']." : ".$var_detail[$zz]['stok_perbaikan'];
						if ($zz<$hitung-1)
						     echo "<br> ";
					}
				}
				?>
				</div>
				</td>
			<?php
					}
				}
				else {
				?>
					<input type="hidden" name="adastok" id="adastok" value="">
				<?php
					echo "<td>Belum ada data stok barang WIP di unit jahit</td>";
				}
			  ?>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="white-space:nowrap;">Stok Barang WIP Warna Baru
				<input type="hidden" name="jumunit" value="<?php echo count($list_warna) ?>">
			</td>
			<?php
			$i=1;
				if (is_array($list_warna)) {
					for($j=0;$j<count($list_warna);$j++){
			?>
				<td style="white-space:nowrap;"><div align="center"><u><?php echo $list_warna[$j]['nama_unit'] ?></u>
				<input type="hidden" name="kode_unit_<?php echo $i ?>" value="<?php echo $list_warna[$j]['kode_unit'] ?>">
				<input type="hidden" name="jumwarna_<?php echo $i ?>" value="<?php echo count($list_warna[$j]['data_warna']) ?>">
				<br>
				<div align="right">Stok Bagus:<br>
					<?php
				if (is_array($list_warna[$j]['data_warna'])) {
					$var_detail = $list_warna[$j]['data_warna'];
					$hitung = count($var_detail);
					for($zz=0;$zz<count($var_detail); $zz++){
						echo $var_detail[$zz]['nama_warna']."&nbsp;";
						?>
						<input type="text" name="qty_warna_bagus_<?php echo $i ?>[]" value="0" size="3" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'">
						<input type="hidden" name="kode_warna_<?php echo $i ?>[]" value="<?php echo $var_detail[$zz]['kode_warna'] ?>"><br>
						<?php
					}
				}
				?><br><br>
				
				Stok Perbaikan:<br>
					<?php
				if (is_array($list_warna[$j]['data_warna'])) {
					$var_detail = $list_warna[$j]['data_warna'];
					$hitung = count($var_detail);
					for($zz=0;$zz<count($var_detail); $zz++){
						echo $var_detail[$zz]['nama_warna']."&nbsp;";
						?>
						<input type="text" name="qty_warna_perbaikan_<?php echo $i ?>[]" value="0" size="3" style="text-align: right;" onblur="if(this.value=='' || isNaN(this.value)) this.value='0'">
						<br>
						<?php
					}
				}
				?>
				</td>
			<?php
						$i++;
					}
				}
				else
					echo "<td>Belum ada data stok barang WIP di unit jahit</td>";
			  ?>
			</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Next" >
			
		</tr>
	</table>
<?php echo form_close(); ?> <br>
