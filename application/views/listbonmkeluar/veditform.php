<script type="text/javascript" language="javascript">

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka.Terimakasih.");
		angka.value	= 0;
		angka.focus();
	}
}

function ckbonmkeluar(nomor) {
	
	var i_outbonm_code_hidden = document.getElementById('i_outbonm_code_hidden').value;
	
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listbonmkeluar/cform/cari_bonmkeluar'); ?>",
	data:"nobonmkeluar="+nomor+"&i_outbonm_code_hidden="+i_outbonm_code_hidden,
	success: function(data){
		$("#confnobonmkeluar").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function showData(nItem) {
	if (document.getElementById(nItem+'_select').style.display=='none') {			
		document.getElementById(nItem+'_select').style.display='block';		
		document.getElementById(nItem+'_select').style.position='absolute';	
	} else {
		document.getElementById(nItem).style.display='block';
		document.getElementById(nItem+'_select').style.display='none';
	} 
}
  
function hideData(nItem){
}

function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	tbl.width='100%';
	
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	}else{
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:10px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:105px;\" >" +
	"<input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:80px;\" onclick=\"lshprodukbmk('"+iteration+"','<?=$tBonMkeluarH?>');\">" +
	"<img name=\"img_i_product_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"lshprodukbmk('"+iteration+"','<?=$tBonMkeluarH?>');\"></DIV>" +
	"<input type=\"hidden\" name=\"qty_product_"+nItem+"_"+iteration+"\" id=\"qty_product_"+nItem+"_"+iteration+"\">";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:220px;\"><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\" name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:240px;\"></DIV>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_n_count_product_"+nItem+"_"+iteration+"\" style=\"width:85px;\">"+
	//"<input type=\"text\" ID=\"n_count_product_"+nItem+"_"+iteration+"\"  name=\"n_count_product_"+nItem+"_"+iteration+"\" style=\"width:81px;text-align:right;\" onkeyup=\"validNum('n_count_product_tblItem','"+iteration+"')\">" +
	"<input type=\"hidden\" ID=\"f_stp_"+nItem+"_"+iteration+"\" name=\"f_stp_"+nItem+"_"+iteration+"\">" +
	"<input type=\"hidden\" ID=\"iteration\" name=\"iteration\" value=\""+iteration+"\">" +
	"<input type=\"hidden\" ID=\"iso_"+nItem+"_"+iteration+"\" name=\"iso_"+nItem+"_"+iteration+"\">" +
	"<input type=\"hidden\" name=\"i_qty_hidden_"+nItem+"_"+iteration+"\" ID=\"i_qty_hidden_"+nItem+"_"+iteration+"\"></DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}
function cek() {
	var e_to_outbonm = document.getElementById('e_to_outbonm').value;
	var i_outbonm = document.getElementById('i_outbonm').value;
	if (e_to_outbonm == "") {
		alert("Maaf, Kepada Belum diisi.Terimakasih.");
		//return false;
	}
	if (i_outbonm == "") {
		alert("Maaf, Nomor Belum diisi.Terimakasih.");
	//	return false;
	}
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
	  <td class="tcat"><?php echo $page_title_bmk; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
	 <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_bmk; ?></td></tr>
	<tr>
	 <td class="alt2" style="padding:0px;">

		<table id="table-add-box">
		  <tr>
		    <td align="left">
			  <?php 
			   
			   $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listbonmkeluar/cform/actedit', $attributes);?>
			<div id="masterbmkform">
			      <table>
			      	<tr align="left">
				  <td width="20px"><?php echo $form_kepada_bmk; ?></td>
				  <td width="1px">:</td>
				  <td width="25px">
					<select name="e_to_outbonm" id="e_to_outbonm">>
					 <option value="">[ <?php echo $form_option_kepada_bmk; ?> ]</option>
					 <?php
					   foreach($opt_kepada as $key => $row) {
					   	$Sel	= $etooutbonm==trim($row->to)?"selected":"";
						$lkepada .= "<option value=".$row->to." $Sel >".$row->to."</option>";
					   }
					   echo $lkepada;
					 ?>
					</select>
				  </td>
				  <td width="20px" align="right"><?php echo $form_nomor_bmk; ?></td>
				  <td width="1px">:</td>
				  <td width="110px">
					<?php
					 $outbonm = array(
						'name'=>'i_outbonm',
						'id'=>'i_outbonm',
						'value'=>$ibonmkeluarcode,
						'maxlength'=>'10',
						'onkeyup'=>'ckbonmkeluar(this.value)'
						);
					 echo form_input($outbonm);
					?>
					<div id="confnobonmkeluar" style="color:#FF0000;"></div>
				  </td>
				  <td width="35px" align="right"><?php echo $form_tgl_bmk; ?></td>
				  <td width="1px">:</td>
				  <td width="125px">
					<?php
					 $tgl = array(
						'name'=>'d_outbonm',
						'id'=>'d_outbonm',
						'value'=>$tBonMkeluar,
						'maxlength'=>'10'
						);
					 echo form_input($tgl);
					?>
					<input type="hidden" name="d_outbonm_hidden" id="d_outbonm_hidden" value="<?php echo $tBonMkeluarH; ?>">
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_outbonm,'dd/mm/yyyy',this)">
				  </td>
				</tr>
				<tr>
				 <td colspan="9">&nbsp;</td>	
				</tr>
				<tr>
				 <td colspan="9">
					<div id="title-box2"><?php echo $form_title_detail_bmk; ?>
					<div style="float:right;">
					<a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
					<a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
					</div></div>
				 </td>	
				</tr>
				<tr>
				 <td colspan="9">
					<table>
						<tr>
						 <td width="10px" class="tdatahead">NO</td>	
						 <td width="100px" class="tdatahead"><?php echo $form_kode_produk_bmk; ?></td>
						 <td width="300px" class="tdatahead"><?php echo $form_nm_produk_bmk; ?></td>
						 <td width="100px" class="tdatahead"><?php echo $form_jml_bmk; ?></td>
						</tr>
						<tr>
						 <td colspan="4">
						 	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							<?php 
							$db2=$this->load->database('db_external', TRUE);
							$iter	= 0;
							foreach($bonmkeluaritem as $row2) {

								$qstp	= $db2->query(" SELECT b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b ON b.i_so=a.i_so WHERE b.i_status_so='0' AND a.i_product='$row2->i_product' LIMIT 1 ");
								
								if($qstp->num_rows()>0){
									$rstp	= $qstp->row();
									$stp	= $rstp->stp;
								}else{
									$stp	= 'f';
								}
								
								/*
								<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:105px;\" ><input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:80px;\" onclick=\"shprodukbmk('".$iter."');\" value=\"".$row2->i_product."\" readonly ><input type=\"hidden\" name=\"qty_product_tblItem_".$iter."\" id=\"qty_product_tblItem_".$iter."\"><img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukbmk('".$iter."');\"></DIV><div id=\"ajax_i_product_tblItem_".$iter."_select\" style=\"display:none;\"></div></td>
								*/
								
								//--------------08-01-2014 23-10-2014 ----------------------------
								// ambil data qty warna dari tr_product_color dan tm_inbonm_item_color
								$sqlxx	= $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name, c.qty FROM tr_product_color a, tr_color b,
														tm_outbonm_item_color c
														 WHERE a.i_color=b.i_color AND c.i_product_color = a.i_product_color
														 AND c.i_outbonm_item = '$row2->i_outbonm_item' AND a.i_product_motif = '$row2->i_product' ");
								
								$listwarna = "";
								if ($sqlxx->num_rows() > 0){
									$hasilxx = $sqlxx->result();
									
									foreach ($hasilxx as $rownya) {
										//$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
										$listwarna .= $rownya->e_color_name." 
										
										<input type='text' name='qty_warna_".$iter."[]' value='".$rownya->qty."'>
										<input type='hidden' name='i_color_".$iter."[]' value='".$rownya->i_color."'>
										<input type='hidden' name='i_product_color_".$iter."[]' value='".$rownya->i_product_color."'><br>";
									}
								}
						
								//-----------------------------------------------------
								
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">".($iter*1+1)."</div></td>
									<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:105px;\" >
									<input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:80px;\" value=\"".$row2->i_product."\" readonly >
									<img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"lshprodukbmk('".$iter."','".$tBonMkeluarH."');\" ></DIV>
									<input type=\"hidden\" name=\"qty_product_tblItem_".$iter."\" id=\"qty_product_tblItem_".$iter."\"></td>
									<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:220px;\">
									<input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\" name=\"e_product_name_tblItem_".$iter."\" style=\"width:240px;\" value=\"".$row2->e_product_motifname."\" readonly ></DIV></td>
									
									<td style='white-space:nowrap;'><DIV ID=\"ajax_n_count_product_tblItem_".$iter."\" style=\"width:85px;\">										
										".$listwarna."
										<input type=\"hidden\" ID=\"f_stp_tblItem_".$iter."\"  name=\"f_stp_tblItem_".$iter."\" value=\"".$stp."\">
										<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\">
										<input type=\"hidden\" ID=\"iso_tblItem_".$iter."\"  name=\"iso_tblItem_".$iter."\" value=\"".$row2->i_so."\">
										<input type=\"hidden\" ID=\"i_qty_hidden_tblItem_".$iter."\"  name=\"i_qty_hidden_tblItem_".$iter."\" value=\""."0"."\"></DIV>
									</td>
								</tr>";
								// <input type=\"text\" ID=\"n_count_product_tblItem_".$iter."\"  name=\"n_count_product_tblItem_".$iter."\" style=\"width:81px;text-align:right;\" onkeyup=\"validNum('n_count_product_tblItem','".$iter."')\" value=\"".$row2->n_count_product."\">
								$iter++;
							}
							?>
						 	</table>
						 </td>
						</tr>
					</table>
				 </td>
				</tr>
			      	<tr>
				  <td colspan="9" align="right">
				  	<input type="hidden" name="i_outbonm_code_hidden" id="i_outbonm_code_hidden" value="<?=$ibonmkeluarcode?>" />
					<input type="hidden" name="i_outbonm_hidden" id="i_outbonm_hidden" value="<?=$ibonmkeluar?>" />
				    <input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit" onclick="return cek()">
				      <input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url() ?>index.php/listbonmkeluar/cform'"> 
				  </td>
			       </tr>
			      </table>
			</div>
			<?php echo form_close()?>
		    </td>
		  </tr> 
		</table>

	 </td>
	</tr>
</table>
