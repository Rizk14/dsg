<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title></title>
<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>
</head>

<body>
<span style="margin-top:10px; text-align:center; color:#FF0000; font-size:11px; font-weight:bold; font-family:Verdana, Arial, Helvetica, sans-serif; text-decoration:blink;">
Mohon utk tidak ditutup !!<br />
Sedang melakukan pencetakan, silahkan tunggu....
</span>

<?php
/*
include_once("printipp_classes/PrintIPP.php");
include_once("funcs/terbilang.php");

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);
$ipp->setRowText();
$ipp->unsetFormFeed();
$ipp->setData(CHR(1)); // Start 0f Heading
$ipp->printJob();
$ipp->setData(CHR(11)); // Vertical Tab
$ipp->printJob();
$ipp->setData(str_repeat(CHR(205),100)."\n");
$ipp->printJob();
$ipp->setData(CHR(2)."Faktur Penjualan".str_repeat(" ",50)."CV. DUTA SETIA GARMEN"."\n");
$ipp->printJob();
$ipp->setData(str_repeat(CHR(205),100)."\n");
$ipp->printJob();
$ipp->setData(CHR(2)."Nomor Faktur".str_repeat(" ",2).$nomorfaktur.str_repeat(" ",45)."Tanggal Faktur".str_repeat(" ",2).$tglfaktur."\n");
$ipp->printJob();
$ipp->setData(CHR(2)."Kepada :".str_repeat(" ",50));
$ipp->printJob();
$no_opdo	= 1;
foreach($infoopdo as $row_opdo){
	$ipp->setData("No DO:".$idocode.str_repeat(" ",5)."No OP:".$iop."\n");
	$ipp->printJob();
	$no_opdo+=1;
}
//$ipp->setData(CHR(2)."PT. Dialogue Garmindo Utama"."\n");
$ipp->setData(CHR(2).$namacab."\n");
$ipp->printJob();
$ipp->setData("JL. Karinding No. 11 Bandung"."\n");
$ipp->printJob();
$ipp->setData(CHR(11)); // Vertical Tab
$ipp->printJob();
$ipp->setData(str_repeat(CHR(196),100)."\n");
$ipp->printJob();
$ipp->setData(CHR(2)."No".str_repeat(" ",20)."Kode Barang".str_repeat(" ",15)."Nama Barang".str_repeat(" ",15)."Unit".str_repeat(" ",15)."Harga".str_repeat(" ",15)."Jumlah"."\n");
$ipp->printJob();
$ipp->setData(str_repeat(CHR(196),100)."\n");
$ipp->printJob();
$no	= 1;
foreach($isi as $row) {
	$ipp->setData(CHR(2).$no.str_repeat(" ",20).$row->imotif.str_repeat(" ",15).$row->motifname.str_repeat(" ",15).$row->qty.str_repeat(" ",15).$row->unitprice.str_repeat(" ",15).$row->amount."\n");
	$ipp->printJob();
	$no+=1;
}
$ipp->setData(str_repeat(CHR(196),100)."\n");
$ipp->printJob();
$ipp->setData(CHR(2)."Tgl. Jatuh Tempo :".$tgljthtempo.str_repeat(" ",40));
$ipp->printJob();
$ipp->setData(str_repeat(" ",60)."Jumlah"."Rp.".number_format($jumlah)."\n");
$ipp->printJob();
$ipp->setData(str_repeat(" ",60)."Diskon"."Rp.".$diskon."\n");
$ipp->printJob();
$ipp->setData(str_repeat(" ",60).str_repeat(CHR(196),50)."\n");
$ipp->printJob();
$ipp->setData(str_repeat(" ",60)."DPP"."Rp.".number_format($dpp)."\n");
$ipp->printJob();
$ipp->setData(str_repeat(" ",60)."PPN"."Rp.".number_format($nilai_ppn)."\n");
$ipp->printJob();
$ipp->setData(str_repeat(" ",60).str_repeat(CHR(196),50)."\n");
$ipp->printJob();
$ipp->setData(str_repeat(" ",60)."Nilai Faktur"."Rp.".number_format($nilai_faktur)."\n");
$ipp->printJob();
$ipp->setData(CHR(2).CHR(169).str_repeat(CHR(196),30).CHR(191));
$ipp->printJob();
$ipp->setData(CHR(179)."1. Barang-barang yang sudah dibeli tidak dapat ditukar/dikembalikan,".str_repeat(" ",10).CHR(179));
$ipp->printJob();
$ipp->setData(CHR(179)."kecuali ada perjanjian terlebih dahulu.".str_repeat(" ",15).CHR(179));
$ipp->printJob();
$ipp->setData(CHR(179)."2. Faktur asli merupakan bukti pembayaran yang sah".str_repeat(" ",15).CHR(179));
$ipp->printJob();
$ipp->setData(CHR(179)."3. Pembayaran dengan cek/ giro baru dianggap sah setelah diuangkan".str_repeat(" ",15).CHR(179));
$ipp->printJob();
$ipp->setData(CHR(2).CHR(192).str_repeat(CHR(196),30).CHR(217));
$ipp->printJob();
$ipp->setData(CHR(2).str_repeat(" ",10)."(".Terbilang($nilai_faktur).")"."\n");
$ipp->printJob();
$ipp->setData(CHR(11)); // Vertical Tab
$ipp->printJob();
$ipp->setData(CHR(2)."SECO"."\n");
$ipp->printJob();
$ipp->setData("\n");
$ipp->printJob();
$ipp->setData("\n");
$ipp->printJob();
$ipp->setData(CHR(2)."Kasie Accounting"."\n");
$ipp->printJob();
$ipp->setData("\n");
$ipp->printJob();
$ipp->setData(str_repeat(CHR(205),100)."\n");
$ipp->printJob();
*/
?>
</body>
</html>