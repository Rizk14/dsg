<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<div class="container">
<h2>Input Makloon Baju dari Cutting ke Pengadaan</h2>
<hr>
<?php echo anchor('/makloon_cutting_baju_wip/makloon_cutting_baju_wip/view', 'View', 'class="btn btn-info " role="button"'); ?>

<br>
<br>

    <form action ="makloon_cutting_baju_wip/submit" name="myform" id='myform' class="form-inline" method="post">
        
        <div class="row">
        <div class="form-group has-feedback ">            
            <label for="no_sj" class="control-label '">Nomor SJ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</label>
            <input id="no_sj" class="form-control input-group-lg" type="text" autocapitalize='off' name="no_sj"
                   title="Masukkan Nomor SJ"
                   placeholder="Nomor SJ"/>
        </div>
        
          <div class="row">
        <?php echo form_label('Tanggal SJ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;', 'tanggal_sj', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback ">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
               <input id='tanggal_sj' name='tanggal_sj' value='<?php echo date_to_id($values->tanggal_sj) ?>' class='form-control'
				placeholder="Tanggal SJ" maxlength="10" onclick="displayCalendar(document.forms[0].tanggal_sj,'dd-mm-yyyy',this)"></input>
                
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>   
        
        
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="jenis_masuk" class="control-label">Jenis Masuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>
            <?php
                   $jenis_masuk = array(
                '' => '- Pilih Jenis Masuk-',
                '1' => 'Masuk Bagus',
                '2' => 'Masuk Lain-lain', 
            );
                    $atribut_jenis_masuk = 'class="form-control" id="jenis_masuk"';
            echo form_dropdown('jenis_masuk', $jenis_masuk, $values->jenis_masuk, $atribut_jenis_masuk);
            ?>
        </div>
        </div>
        
         <div class="row">
        <div class="form-group has-feedback ">
            <label for="gudang_masuk" class="control-label">Gudang Masuk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="gudang_masuk" name="gudang_masuk">
		<option value=''> - Pilih Gudang Masuk -</option>
		<?php foreach ($gudang as $gud) {?> 
    <option value=<?php echo $gud->id ?>><?php echo $gud->nama_gudang?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="gudang_keluar" class="control-label">Gudang Keluar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="gudang_keluar" name="gudang_keluar">
			<option value=''> - Pilih Gudang Keluar -</option>
		<?php foreach ($gudang as $gud) {?> 
    <option value=<?php echo $gud->id ?>><?php echo $gud->nama_gudang?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        
        
       
       
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="keterangan_header" class="control-label">Keterangan Header &nbsp;: &nbsp;</label>
		<input id="keterangan_header" class="form-control input-group-lg" type="text" autocapitalize='off' name="keterangan_header"
                   title="Masukkan Keterangan Header"
                   placeholder="Keterangan Header"/>
        
        </div>
        </div>
        <br>
        
         <div class="row">
        <div class="col-xs-12 col-md-4 pull-right">
		<?php echo form_button(array('id'=>'addRow','content'=>'+', 'type'=>'button', 'class'=>'btn btn-primary'  )) ?>
		<?php echo form_button(array( 'id'=>"deleteRow",'content'=>'-', 'type'=>'button', 'class'=>'btn btn-primary')) ?>
		
         </div>
         </div>
         <br>
<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="no_x" id="no_x" value="">			
            <div id='my_div_1'>
				
				<input type="hidden" name="id_barang_wip_1" id="id_barang_wip_1" value="">
				<input id="kode_barang_wip_1" class="form-control input-group-lg" type="text" name="kode_barang_wip_1"
                       title="Masukkan Kode Barang WIP" value=""
                       placeholder="Kode Barang WIP" onclick= "openCenteredWindows('<?php echo base_url(); ?>index.php/item_makloon/item_makloon/barang_wip/1');"
                       />
	
                <input id="nama_barang_wip_1" class="form-control input-group-lg" type="text" name="nama_barang_wip_1"
                       title="Masukkan Nama Barang WIP" value=""
                       placeholder="Nama Barang WIP"/ disabled>
		
                <input id="qty_1" class="form-control input-group-lg" type="text" name="qty_1"
                       title="Masukkan Quantity" value=""
                       placeholder="Quantity"/>
                       
               <input id="keterangan_detail_1" class="form-control input-group-lg" type="text" name="keterangan_detail_1"
                       title="Masukkan keterangan detail" value=""
                       placeholder="Keterangan detail"/>   
                
				<input type="hidden" name="id_barang_bb_1" id="id_barang_bb_1" value="">
				<input type="hidden" name="nama_barang_bb_1" id="nama_barang_bb_1" value="">
               	<input id="kode_barang_bb_ck_1" class="form-control input-group-lg" type="text" name="kode_barang_bb_ck_1"
                       title="Masukkan Kode Barang BB" value=""
                       placeholder="Kode Barang BB" onclick= "openCenteredWindows('<?php echo base_url(); ?>index.php/item_makloon/item_makloon/barang_bb_ck/1');"
                       />

        </div>
        <br>
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-success', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>

</form>
 
        </div>
