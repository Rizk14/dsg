<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:12px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Drop Forecast Gudang Jadi</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-forecast-gdjd/cform/export_excel_wip', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="68%">
	<thead  rowspan='1'>
	 <tr class="judulnya">
		<!-- 		
		<th width='3%' ><font size='2'>No</font></th>
		<th width='5%' ><font size='2'>Kode</font></th>
		<th width='30%'><font size='2'>Nama Barang WIP</font></th>
		<th width='8%' ><font size='2'>Drop Forecast</font></th>
		<th width='8%' ><font size='2'>Quantity Keluar</font></th>
		<th width='8%' ><font size='2'>Sisa</font></th>
	--->
	<th width='3%' >No</th>
		<th width='5%' >Kode</th>
		<th width='30%'>Nama Barang WIP</th>
		<th width='8%' >Drop Forecast</th>
		<th width='8%' >Quantity Keluar</th>
		<th width='8%' >Sisa</th>
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				 for($j=0;$j<count($query);$j++){
				 
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center' >".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['sa'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar'],0,',','.')."&nbsp;</td>";
				  echo    "<td align='right'>".number_format($query[$j]['sisa'],0,',','.')."&nbsp;</td>";
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
