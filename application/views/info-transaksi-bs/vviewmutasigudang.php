<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Gudang BS</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-transaksi-bs/creport/export_excel_mutasi', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table class="table table-condensed" border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya" >
		 <th width='3%' >No</th>
		 <th width='15%'>Kode</th>
		 <th width='25%'>Nama Brg WIP</th>
		 <th width='8%' ><center>Saldo Awal</center></th>
		 <th width='8%'><center>Masuk</center></th>
		 <th width='8%'><center>Keluar</center></th>
		 <th width='8%'><center>Saldo Akhir</center></th>
		 <th width='8%'><center>Stok Opname</center></th>
		
	 </tr>

	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
		 ?>
					<tr>
						<td colspan="23">&nbsp;<b><?php echo $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ?></b></td>
					</tr>
		 <?php 
				 }
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 echo    "<td align='center'> ".number_format($query[$j]['saldo_awal'],0,',','.')."</td>";
				 echo    "<td align='center'> ".number_format($query[$j]['jum_masuk'],0,',','.')."</td>"; 
				 echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_pack'],0,',','.')."</td>";
				 echo   "<td align='right'>".number_format($query[$j]['jum_saldo_akhir'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_stok_opname'],0,',','.')."&nbsp;</td>"; 
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>

