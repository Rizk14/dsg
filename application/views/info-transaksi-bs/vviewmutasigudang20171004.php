<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Gudang BS</h3><br>

<div>

Gudang: <?php if ($gudang!= 0) { echo $kode_gudang."-".$nama_gudang; } else echo "Semua"; ?><br>
Periode: <?php echo $date_from." s/d ". $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-transaksi-bs/creport/export_excel_mutasi', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >

<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<?php
	if (is_array($query)) {
	//	for($a=0;$a<count($query);$a++){
	//		echo "<b>".$query[$a]['kode_gudang']." - ".$query[$a]['nama_gudang']."</b>"."<br>";
?>
	


	<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width='1%'>No</th>
		 <th width='4%'>Kode</th>
		 <th width='15%'>Nama Brg WIP</th>
		 <th width='5%'>Saldo Awal</th>
		<th width='5%'>Masuk</th>
		<th width='5%'>Keluar</th>
		<th width='5%'>Saldo Akhir</th>
		<th width='5%'>SO</th>
	 </tr>
	</thead>
	<tbody>






		<?php 
		$detail_fc= $query;
		if (is_array($detail_fc)) {
				for($j=0;$j<count($detail_fc);$j++){
					?>
					<tr>
					<td align="center"><?php echo ($j+1) ?></td>
					<td>&nbsp;<?php echo $detail_fc[$j]['kode_brg'] ?></td>
					<td>&nbsp;<?php echo $detail_fc[$j]['nama_brg'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['saldo_awal'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['jum_masuk'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['jum_keluar'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j] ['saldo_akhir'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['jum_stok_opname'] ?></td>
					</tr>
					<?php
					}
				}
		?>
			</tbody>
			</table><br><br>

<?php
		}
	//}
?>

</div>

