<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    .judulnya {
		background-color:#DDD;
	}

</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
// 24-07-2013
function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

$(function()
{
	$(".pilih").click(function()
	{
		var kode_brg=$("#kode_brg").val();
		var id_brg=$("#id_brg").val();
		
		
		//opener.document.forms["f_akunting"].nama_brgx.value = idx +" - "+nama_brg;
		opener.document.forms["f_forecast"].kode_brg_wip_<?php echo $posisi ?>.value = kode_brg;
		opener.document.forms["f_forecast"].id_brg_wip_<?php echo $posisi ?>.value = id_brg;
		
		self.close();
	});
});
</script>

<center><h3>Daftar Kode Barang (WIP)</h3></center>
<div align="center"><br>
<?php 
	echo form_open('forecast-gdjd/cform/show_popup_brg'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
&nbsp;<input type="submit" name="submit" value="Cari">

<?php echo form_close(); 

?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="kode_brg" id="kode_brg">
<input type="hidden" name="nama_brg" id="nama_brg">
<input type="hidden" name="id_brg" id="id_brg">

	<table border="1" align="center" width="100%" cellpadding="1" cellspacing="2" bordercolor="#666666" style="white-space:nowrap;">
	<thead>
	 <tr class="judulnya">
		 <th bgcolor="#999999">No</th>
		 <th bgcolor="#999999">Kode brg</th>
		 <th bgcolor="#999999">Nama brg</th>
		 <th bgcolor="#999999">Last Update</th>
		 <th bgcolor="#999999">Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
				
			 foreach($query as $databrg) {
				$pisah1 = explode("-", $databrg->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo "<td>".$i."</td>";
				 echo    "<td>".$databrg->kode_brg."</td>";
				 echo    "<td>".$databrg->nama_brg."</td>";
				 echo    "<td>".$tgl_update."</td>"; ?>
				 <td align="center">				  
				  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.kode_brg.value='<?php echo $databrg->kode_brg ?>';
				  window.document.f_master_brg.id_brg.value='<?php echo $databrg->id ?>';
				   ">Pilih</a></td>
				<?php echo  "</tr>";
				$i++; }
		 	}
		   
		 ?>
 	</tbody>
</table>
</form>
<?php echo $this->pagination->create_links();?>
</div>
