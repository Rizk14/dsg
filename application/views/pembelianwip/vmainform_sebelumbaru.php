<h3>Data Bukti Penerimaan Barang</h3><br>
<a href="<?php echo base_url(); ?>index.php/faktur-bb/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/faktur-bb/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
	
	get_data_pkp();
	ambil_pp();
	$('#topnya').hide();
	
	/*$("#no_faktur").blur(function()
		{
		 var invoice_number=$("#no_faktur").val();
		
		 $("#msgbox").removeClass().addClass('messagebox').html('<img src="modules/ProitInventory/pnimages/spinner_small.gif">').fadeIn("slow");
		
		 $.post("index.php?module=ProitInventory&type=admin&func=cek_invoice_penjualan",
		 { 
		 	invoice_number:invoice_number } ,function(data)
		 { 	
		 
		 
		 var cek=$("status",data).html()
		 var tgl=$("tgl",data).html()
		 $('#tgl_retur').val(tgl);
		 
		 	
		  if(cek=='0') 
		  {
		   $("#msgbox").fadeTo(200,0.1,function() 
		   {			
			$(this).html('No. Invoice yang anda masukkan tidak valid').addClass('messageboxerror').fadeTo(300,1);
		   });
		  }
		  else
		  {
		   $("#msgbox").fadeTo(200,0.1,function()  
		   {			
			$(this).html('<img src="modules/ProitInventory/pnimages/checkbullet.gif">').addClass('messageboxok').fadeTo(300,1);
		   });
		  }
		  
		 });
		}); */
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****pajak*************************************
		var pajak="#pajak_"+n;
		var new_pajak="#pajak_"+no;
		$(pajak, lastRow).attr("id", "pajak_"+no);
		$(new_pajak, lastRow).attr("name", "pajak_"+no);		
		$(new_pajak, lastRow).val('0');				
		//*****end pajak*************************************	
				
		//*****diskon*************************************
		var diskon="#diskon_"+n;
		var new_diskon="#diskon_"+no;
		$(diskon, lastRow).attr("id", "diskon_"+no);
		$(new_diskon, lastRow).attr("name", "diskon_"+no);		
		$(new_diskon, lastRow).val('0');				
		//*****end diskon*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/pp-bb/cform/show_popup_brg/"+no+"/');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
	$('#pilih_op').click(function(){
		var cek_pp = 0;
		if ($("#ambil_pp").is(":checked")) {
			cek_pp = 1;
		}
		else
			cek_pp = 0;
			
		var urlnya = "<?php echo base_url(); ?>index.php/faktur-bb/cform/show_popup_op/"+cek_pp;
		openCenteredWindow(urlnya);
		
	  });
	  
	$('#kode_supplier').change(function(){
	  	    get_data_pkp();
	  });
	
	$('#pkp').click(function(){
	  	    if ($("#pkp").is(":checked")) {
				$('#hide_pkp').val('t');
				hitungnilai();
			}
			else {
				$('#hide_pkp').val('f');
				hitungnilai();
			}
	  });
	  
	$('#tipe_pajak').change(function(){
	  	    if ($("#tipe_pajak").val() == 'I') {
				$('#hide_tipe_pajak').val('I');
				hitungnilai();
			}
			else {
				$('#hide_tipe_pajak').val('E');
				hitungnilai();
			}
	  });
	  
	  $('#ambil_pp').click(function(){
	  	    if ($('input[name=ambil_pp]').is(':checked')) {
				$("#ppop").html("List Brg di PP");
				$("#ppop").show();
				$("#kode_supplier2").attr("disabled", false);
				$('#no_op').val('');
				$('#id_op').val('');
				$('#id_brg').val('');
			}
			else {
				$("#ppop").html("List Brg di OP");
				$("#ppop").show();
				$("#kode_supplier2").attr("disabled", true);
				$('#no_op').val('');
				$('#id_op').val('');
				$('#id_brg').val('');
			}
	  });
	  
	  $('#is_no_ppop').click(function(){
	  	    if ($('input[name=is_no_ppop]').is(':checked')) {
				$("#kode_supplier2").attr("disabled", false);
			}
			else {
				$("#kode_supplier2").attr("disabled", true);
			}
	  });
	  
	 // skrip jquery utk save
	$("#button_simpan").click(function(){
		var id_op_cek= $('#id_op').val();
		cek_pembelian();
		
		var jum= $('#no').val()-1; 
		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				var kodenya = $('#kode_'+k).val();
				var qtynya = $('#qty_'+k).val();
				
				var qty_op_detail = 0;
				var qty_pembelian_detail = 0;
				
				// 1. ambil qty di op_detail
				jQuery.post("<?php echo base_url(); ?>index.php/faktur-bb/cform/get_qty_op_detail",
					{ id_op: id_op_cek,
					  kode_brg: kodenya,
					  qty: qtynya
					},
					function(data){	
						//jQuery('#ambil_qty_op_detail').html("<input type='hidden' name='qty_op_detail' id='qty_op_detail' value='"+data+"'> ");
						//alert ($('#qty_lama_'+k).val());
						var qty_lama = $('#qty_lama_'+k).val();
						if (data =='1') {
							//alert (data);
							$('#qty_'+k).val(qty_lama);
							hitungnilai();
						//	jQuery("#f_purchase").submit();
						}
						//else
						//	
					}
				);
				
			} // end for
			jQuery("#f_purchase").submit();
	   }  // end if
		
    });
	 //======================
	 
/*	 $('#lain_cash').click(function(){
			if ($("#lain_cash").is(":checked")) {
				$('input[name=lain_kredit]').attr('checked', false);
			}
			else {
				$('input[name=lain_kredit]').attr('checked', true);
			}

	  });
	  
	  $('#lain_kredit').click(function(){
			if ($("#lain_kredit").is(":checked")) {
				$('input[name=lain_cash]').attr('checked', false);
			}
			else {
				$('input[name=lain_cash]').attr('checked', true);
			}

	  }); */
	 
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 670;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	

function get_data_pkp() {
	var kode_sup= $('#kode_supplier').val();
	var jum_detail = $('#no').val()-1;
    $.getJSON("<?php echo base_url(); ?>index.php/faktur-bb/cform/get_pkp_tipe_pajak/"+kode_sup, function(data) {

		$(data).each(function(index, item) {
			$("#topnya").html("T.O.P : "+item.top+" Hari");
			$("#topnya").show();

			if (item.pkp == 't') {
				//alert($("input:checked").length);
				$('#hide_pkp').val('t');
				var i=1;
				var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
				
					$('input[name=pkp]').attr('checked', true);
					if (item.tipe_pajak == 'I') {
						$('#tipe_pajak option[value=I]').attr('selected', 'selected');
						$('#hide_tipe_pajak').val('I');
						var hitung = (harga*qty)-diskon;
						var pi = hitung/1.1;
						new_pajak = hitung-pi;
						new_pajak = new_pajak.toFixed(2);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(hitung);
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					}
					else {
						$('#tipe_pajak option[value=E]').attr('selected', 'selected');
						$('#hide_tipe_pajak').val('E');
						var hitung = (harga*qty)-diskon;
						var new_pajak = hitung*0.1;
						new_pajak = new_pajak.toFixed(2);
						new_total = parseFloat(new_pajak)+parseFloat(hitung);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(new_total);
						gtotal = parseFloat(gtotal)+parseFloat(new_total); 
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					} //end cek item pajak
				}
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(2);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal/1.1;
				dpp = dpp.toFixed(2);
				$("#dpp").val(dpp);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
			}
			else if (item.pkp == 'f') {
				$('input[name=pkp]').attr('checked', false);
				$('#tipe_pajak option[value=I]').attr('selected', 'selected');
				$('#hide_pkp').val('f');
				$('#hide_tipe_pajak').val('I');
				var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					var hitung = (harga*qty)-diskon;
					$("#pajak_"+i).val('0');
					$("#total_"+i).val(hitung);
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				$("#gtotal").val(gtotal);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
				$("#tot_pajak").val('0');
				$("#dpp").val('0');
			}
				
        });
    });
    
}

function hitunghutang() {
	var gtotal= $('#gtotal').val();
	var um= $('#uang_muka').val();
	var hitung= parseFloat(gtotal-um);
	$('#sisa_hutang').val(hitung);
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
	status_pkp = $('#hide_pkp').val();
	status_pajak = $('#hide_tipe_pajak').val();
	
	if (status_pkp == 't') {
		var i=1;
		var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
				
					//$('input[name=pkp]').attr('checked', true);
					if (status_pajak == 'I') {
						//$('#tipe_pajak option[value=I]').attr('selected', 'selected');
						var hitung = (harga*qty)-diskon;
						var pi = hitung/1.1;
						new_pajak = hitung-pi;
						new_pajak = new_pajak.toFixed(2);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(hitung);
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					}
					else {
						//$('#tipe_pajak option[value=E]').attr('selected', 'selected');
						var hitung = (harga*qty)-diskon;
						var new_pajak = hitung*0.1;
						new_pajak = new_pajak.toFixed(2);
						new_total = parseFloat(new_pajak)+parseFloat(hitung);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(new_total);
						gtotal = parseFloat(gtotal)+parseFloat(new_total);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					} //end cek item pajak
				}
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(2);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal/1.1;
				dpp = dpp.toFixed(2);
				$("#dpp").val(dpp);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
	}
	else if (status_pkp == 'f') {
				var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					var hitung = (harga*qty)-diskon;
					$("#pajak_"+i).val('0');
					$("#total_"+i).val(hitung);
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				$("#gtotal").val(gtotal);
				$("#tot_pajak").val('0');
				$("#dpp").val('0');
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
	}
}

function cek_item_brg() {
	var id_brg= $('#id_brg').val();
	var no_pp= $('#no_pp').val();
	
	if ($('input[name=is_no_ppop]').is(':checked') == false) {
		if (no_pp == '') {
			alert("Nomor OP harus dipilih..!");
			return false;
		}
		if (id_brg == '') {
			alert("item barang di OP/PP harus dipilih..!");
			return false;
		}
	}
}

function cek_pembelian() {
	var no_faktur= $('#no_sj').val();
	var tgl_retur= $('#tgl_sj').val();
	var id_op_cek= $('#id_op').val();
	
	if (no_faktur == '') {
		alert("Nomor SJ harus diisi..!");
		return false;
	}
	if (tgl_retur == '') {
		alert("Tanggal SJ harus dipilih..!");
		return false;
	}
	var jum= $('#no').val()-1; 
	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			
			if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
				alert("Data harga tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#harga_'+k).val()) ) {
				alert("Harga harus berupa angka..!");
				return false;
			}
			
			if (isNaN($('#diskon_'+k).val()) ) {
				alert("Diskon harus berupa angka..!");
				return false;
			}
			
			// 11 mei 2011, skrip ini ga dipake lagi, karena jumlah di penerimaan brg boleh lebih dari max OP
		/*	var qty_op = $('#qty_op_'+k).val();
			var jum_beli = $('#jum_beli_'+k).val();
			var qty_lama = $('#qty_lama_'+k).val();
			
			if (jum_beli == '')
				jum_beli = 0;
			var qty_skrg = parseFloat(jum_beli) + parseFloat($('#qty_'+k).val());
			
			if (qty_skrg > qty_op) {
				alert("Qty melebihi batas OP..!");
				$('#qty_'+k).val(qty_lama);
				hitungnilai();
				return false;
			} */
			//####################################################################### end of 11 mei 2011
			
			// cek qty, jika melebihi OP, maka sesuaikan ========================
			// <?php echo site_url('pp-bb/cform/cari_pp');?> 

		// ===================================================================================================
		
		// 2. ambil jum qty di pembelian_detail
	/*	jQuery.post("<?php echo base_url(); ?>index.php/faktur-bb/cform/get_qty_pembelian_detail",
			{ id_op: id_op_cek,
			  kode_brg: kodenya
			},
			function(data){	
				qty_pembelian_detail = data;			
				
		  });
			alert(qty_op_detail);
			alert(qty_pembelian_detail);
			return false; */
			
			/*if (qty_op_detail > 0) {
					alert("woi" + qty_op_detail); return false;
					var qty_pembelian_detail = data;
					var qty_item = $('#qty_'+k).val();
					var qty_skrg = parseFloat(qty_pembelian_detail) + parseFloat(qty_item);
					
					if (parseFloat(qty_skrg) > parseFloat(qty_op_detail)) {
						alert("Qty barang tidak boleh lebih besar dari qty di OP");
						
					}
			} */
		/*	

			
			if (qty_op_detail != 0) {
				var qty_skrg = qty_pembelian_detail + $('#qty_'+k).val();
						if (qty_skrg > qty_op_detail) {
							alert("Qty barang tidak boleh lebih besar dari qty di OP");
							$('#qty_'+k).val(qty_op_detail-qty_pembelian_detail); 
							hitungnilai();
							return false;
						}
				
			} */
			
			//===================================================================
			
		} // end for
		
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}	
}

function ambil_pp() {
	if ($('input[name=ambil_pp]').is(':checked')) {
		$("#ppop").html("List Brg di PP");
		$("#ppop").show();
		$("#kode_supplier2").attr("disabled", false);
	}
	else {
		$("#ppop").html("List Brg di OP");
		$("#ppop").show();
		$("#kode_supplier2").attr("disabled", true);
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_op', 'id' => 'f_op');
echo form_open('faktur-bb/cform/', $attributes); ?>
<table>
	<tr>
		<td> Ambil dari PP </td>
		<td> : <input type="checkbox" name="ambil_pp" id="ambil_pp" value="t"> &nbsp;
			
		 </td>
	</tr>

	<tr>
		<td><span id="ppop"></span></td>
		<td>: <input type="text" name="no_op" id="no_op" value="" size="10" maxlength="10" readonly="true">
				<input type="hidden" name="id_op" value="">&nbsp;
		<input type="hidden" name="id_brg" id="id_brg" value="">
		<input name="pilih_op" id="pilih_op" value="..." type="button">
		</td>
	</tr>
	<tr>
		<td> Tidak ada acuan PP/OP </td>
		<td> : <input type="checkbox" name="is_no_ppop" id="is_no_ppop" value="t"> &nbsp;
		 </td>
	</tr>
	<tr>
		<td>Supplier</td>
		<td>: <select name="kode_supplier2" id="kode_supplier2" disabled >
				<?php foreach ($supplier2 as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>
	
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-bb/cform/view'">
<?php echo form_close(); 
} else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-bb/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_op" id="id_op" value="<?php echo $id_op ?>">
<input type="hidden" name="kode_sup" value="<?php echo $kode_supplier ?>">

<input type="hidden" name="ambil_pp" value="<?php echo $ambil_pp ?>">

<div id="ambil_qty_op_detail"><input type="hidden" name="qty_op_detail" id="qty_op_detail" value=""></div>
<div id="ambil_qty_pembelian_detail"><input type="hidden" name="qty_pembelian_detail" id="qty_pembelian_detail" value=""></div>

<?php 
		if (count($op_detail)>0) {
			$no=1;
			foreach ($op_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
    <tr>
		<td width="20%">Nomor PP</td>
		<td>
		  <?php echo $no_pp; ?>
    </td>
  </tr>
	<tr>
    <td>Nomor OP</td>
    <td>
      <?php if ($ambil_pp == '') echo $no_op; else echo "-"; ?>
    </td>
  </tr>
		<tr>
			<td>Supplier</td>
			<td><select name="kode_supplier" id="kode_supplier" onkeyup="this.blur();this.focus();" disabled >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" <?php if ($sup->kode_supplier == $kode_supplier) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr>
		<tr>
			<td>
			&nbsp;
			</td>
			<td>PKP <input type="checkbox" name="pkp" id="pkp" value="t" checked="checked">&nbsp;&nbsp;&nbsp;
			Tipe Pajak <select name="tipe_pajak" id="tipe_pajak" onkeyup="this.blur();this.focus();">
				<option value="I">Include</option>
				<option value="E" >Exclude</option>
			</select>&nbsp;&nbsp;&nbsp;<span id="topnya"></span>
			<input type="hidden" name="hide_pkp" id="hide_pkp" value=""><input type="hidden" name="hide_tipe_pajak" id="hide_tipe_pajak" value="">
			</td>
		</tr>
  <tr>
    <td>Nomor SJ</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" maxlength="20" value="">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tanggal SJ</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  
  <tr>
    <td nowrap>Masukkan stok ke tipe lain-lain (cash)</td>
    <td>
		<input type="checkbox" name="lain_cash" id="lain_cash" value="t">
	</td>
  </tr>
  <tr>
    <td nowrap>Masukkan stok ke tipe lain-lain (kredit)</td>
    <td>
		<input type="checkbox" name="lain_kredit" id="lain_kredit" value="t">
	</td>
  </tr>
  
 <!-- <tr>
    <td>No & Tgl Faktur sama dengan SJ </td>
    <td>
      <input type="checkbox" name="faktur_sj" value="t" >
    </td>
  </tr>
  -->
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="right">
			<!-- <input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang"> -->
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
	      <th>Qty</th>
	      <th>Harga</th>
	      <th>Diskon</th>
	      <th>PPN</th>
          <th>Total</th>
        </tr>

        <?php $i=1;
        if (count($op_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($op_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $op_detail[$j]['kode_brg'] ?>"/>
           <input type="hidden" name="id_op_detail_<?php echo $i ?>" value="<?php echo $op_detail[$j]['id'] ?>" >
          <!-- <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=<?php echo $i ?>" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" onclick="javascript:openCenteredWindow('<?php echo base_url(); ?>index.php/pp-bb/cform/show_popup_brg/<?php echo $i ?>/');" type="button"> --></td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $op_detail[$j]['nama'] ?>" /></td>
          
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $op_detail[$j]['nama_satuan'] ?>" /></td>
          
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $op_detail[$j]['qty'] ?>" onkeyup="hitungnilai()" />
          <input name="qty_lama_<?php echo $i ?>" type="hidden" id="qty_lama_<?php echo $i ?>" size="10"
          value="<?php echo $op_detail[$j]['qty'] ?>" />
          <input name="qty_op_<?php echo $i ?>" type="hidden" id="qty_op_<?php echo $i ?>" size="10"
          value="<?php echo $op_detail[$j]['qty_op'] ?>" />
          <input name="jum_beli_<?php echo $i ?>" type="hidden" id="jum_beli_<?php echo $i ?>" size="10"
          value="<?php echo $op_detail[$j]['jum_beli'] ?>" />
          </td>
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $op_detail[$j]['harga'] ?>" onkeyup="hitungnilai()" />
          <input name="harga_lama_<?php echo $i ?>" type="hidden" id="harga_lama_<?php echo $i ?>"  
          value="<?php echo $op_detail[$j]['harga'] ?>" />
          </td>
          <td><input name="diskon_<?php echo $i ?>" type="text" id="diskon_<?php echo $i ?>" size="8" maxlength="8" 
		  value="0" onkeyup="hitungnilai()" /></td>
          <td><input name="pajak_<?php echo $i ?>" type="text" id="pajak_<?php echo $i ?>" readonly="true" size="10" maxlength="10" value="0" /></td>
          <td><input name="total_<?php echo $i ?>" type="text" id="total_<?php echo $i ?>" readonly="true" size="10" maxlength="10" value="0" /></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td>DPP</td>
			<td>: <input type="text" name="dpp" id="dpp" value="0" size="10" maxlength="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Total PPN</td>
			<td>: <input type="text" name="tot_pajak" id="tot_pajak" value="0" size="10" maxlength="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Grand Total</td>
			<td>: <input type="text" name="gtotal" id="gtotal" value="0" size="10" maxlength="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Uang Muka</td>
			<td>: <input type="text" name="uang_muka" id="uang_muka" value="0" size="10" maxlength="10" onkeyup="hitunghutang()"></td>
		</tr>
		<tr>
			<td>Sisa Hutang</td>
			<td>: <input type="text" name="sisa_hutang" id="sisa_hutang" value="0" size="10" maxlength="10" readonly="true"></td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>: <input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
	</table>
      </form>
      <div align="center"><br> 
        
      <input type="submit" name="submit" value="Simpan" onclick="return cek_pembelian(); ">
       
      <!-- <input type="button" name="button_simpan" id="button_simpan" value="Simpan"> -->
       
       &nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-bb/cform/'">

      </div>
      </td>
    </tr>

</table>
</div>
</form>
<?php } ?>
