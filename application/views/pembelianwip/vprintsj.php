<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
   
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .isinya2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">

$(function()
{
	//var go_print= jQuery('#go_print').val();
	
	//if (go_print == '1')
		window.print();
	
});


</script>

<?php
	$tgl = date("d-m-Y");
	$pisah1 = explode("-", $tgl);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
	
	$tgljt = date('Y-m-d', strtotime('+'.$query[0]['top'].' days', strtotime($query[0]['tgl_sj'])));
	$pisah1 = explode("-", $tgljt);
	$tgl1= $pisah1[2];
	$bln1= $pisah1[1];
	$thn1= $pisah1[0];
	$tgljt = $tgl1."-".$bln1."-".$thn1;
	//if ($proses != '') {
?>
<input type="hidden" name="go_print" id="go_print" value="1" >

<table class="isinya2" border='1' align="center" width="100%">
	<tr>
		<td>
			<table class="isinya" border='0' align="center" width="90%">
				<tr>
					<td colspan="2"> <b><?php echo $datasetting['nama'] ?></b> </td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2"><?php echo $datasetting['alamat'] ?></td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td align="center">Supplier :</td>
				</tr>
				<tr>
					<td colspan="2"><?php echo $datasetting['kota'] ?></td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td align="center"><?php echo $query[0]['nama_supplier'] ?></td>
				</tr>
				<tr>
					<td colspan="5" align="center"><b>BUKTI TERIMA BARANG</b></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	<td>
		<table class="isinya" border='0' align="center" width="90%">
		<tr>
			<td>No & Tgl OP: <br><?php echo $query[0]['no_op'] ?></td>
			<td valign="top">No & Tgl BTB: <br><?php echo $query[0]['no_bonm'] ?></td>
			<td valign="top">TOP: <?php echo $query[0]['top'] ?> Hari</td>
		</tr>
		<tr>
			<td valign="top">No & Tgl SJ: <br><?php echo $query[0]['no_sj']." (".$query[0]['tgl_sj'].")" ?></td>
			<td valign="top">Tgl JT: <br><?php echo $tgljt ?></td>
		</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td><br>
		<table class="isinya2" border='1' align="center" width="100%">
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Nama Barang</th>
			<th colspan="2" align="center">Jumlah Kedatangan</th>
			<th rowspan="2">Jumlah<br>Sisa di OP</th>
			<th rowspan="2">Satuan</th>
			<th rowspan="2">Harga</th>
			<th rowspan="2">Diskon</th>
			<th rowspan="2">PPN</th>
			<th rowspan="2">Subtotal</th>
		</tr>
		<tr>
			<th>SJ</th>
			<th>Fisik</th>
		</tr>
		<?php
			if (is_array($query[0]['detail_fb'])) {
				$i=1;
				$detailnya = $query[0]['detail_fb'];
				$hitungtotal = 0; $hitungpajak = 0;
				 for($j=0;$j<count($detailnya);$j++){
		?>
			<tr>
				<td align="center"><?php echo $i ?></td>
				<td><?php echo $detailnya[$j]['kode_brg']." - ".$detailnya[$j]['nama'] ?></td>
				<td align="right"><?php echo number_format($detailnya[$j]['qty'], 2, '.',',') ?></td>
				<td align="right"><?php if ($detailnya[$j]['qty_fisik'] != '-') { echo number_format($detailnya[$j]['qty_fisik'], 2, '.',','); } else echo $detailnya[$j]['qty_fisik']; ?></td>
				<td align="right"><?php echo $detailnya[$j]['sisa'] ?></td>
				<td><?php echo $detailnya[$j]['nama_satuan'] ?></td>
				<td align="right"><?php if ($query[0]['pkp'] == 'f') echo number_format($detailnya[$j]['harga'], 2, ',','.'); else echo number_format($detailnya[$j]['harga']/1.1, 2, ',','.'); ?></td>
				<td align="right"><?php if ($detailnya[$j]['qty_fisik'] == '-') echo '0'; else echo number_format($detailnya[$j]['diskon'], 2, ',','.') ?></td>
				<td align="right"><?php if ($detailnya[$j]['qty_fisik'] == '-') echo '0'; else { $hitungpajak+= $detailnya[$j]['pajak']; echo number_format($detailnya[$j]['pajak'], 2, ',','.'); } ?></td>
				<td align="right"><?php if ($detailnya[$j]['qty_fisik'] == '-') echo '0'; else { $hitungtotal+= $detailnya[$j]['total']; echo number_format($detailnya[$j]['total'], 2, ',','.'); } ?></td>
			</tr>
		<?php
					$i++;
				 }
			}
		?>
		<tr>
			<td colspan="8" align="right"><b>TOTAL</b></td>
			<td align="right"> <b><?php //echo number_format($query[0]['total_pajak'], 2, ',','.') 
			echo number_format($hitungpajak, 2, ',','.') ?></b></td>
			<td align="right"> <b><?php //echo number_format($query[0]['total'], 2, ',','.') 
			echo number_format($hitungtotal, 2, ',','.') ?></b></td>
		</tr>
		</table>
		<table class="isinya" border="0" align="center" width="100%">
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			<tr>
				<td align="center">Mengetahui<br>Admin Pembelian</td>
				<td align="center">Dibuat oleh,<br>Admin Stok</td>
				<td align="center">Menyetujui<br>Spv Admin Stok</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center"><u><?php echo $datasetting['bag_pembelian2'] ?></u></td>
				<!--<td align="center"><u><?php //echo $datasetting['spv_bag_admstok'] ?></u></td>-->
				<td align="center"><u><?php 
				if ($uid_update_by == 0) {
					if ($adastaf1 != '') echo $datasetting['bag_admstok']; 
					else echo $datasetting['bag_admstok2']; 
				}
				else
					echo $namastaf;
				?></u></td>
				<td align="center"><u><?php echo $datasetting['spv_bag_admstok'] ?></u></td>
			<!--	<td align="center"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td> -->
			</tr>
		</table>
	</td>
	</tr>
	</table>

<!--<i><div class="isinya" align="right">Tanggal Cetak: <?php echo date("d-m-Y H:i:s"); ?></i></div>-->
