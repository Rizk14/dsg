<style>
.garisbawah { 
		border-bottom:#000000 0.1px solid;
	}

</style>

<script type="text/javascript" language="javascript">


function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka.Terimakasih.");
		angka.value	= 0;
		angka.focus();
	}
}

function ckbonmmasuk(nomor){
	
	var i_inbonm_code_hidden = document.getElementById('i_inbonm_code_hidden').value;
	
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listbonmmasuk/cform/cari_bonmmasuk');?>",
	data:"nobonmmasuk="+nomor+"&i_inbonm_code_hidden="+i_inbonm_code_hidden,
	success: function(data){
		$("#confnobonmmasuk").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function showData(nItem) {
	if (document.getElementById(nItem+'_select').style.display=='none') {		
		document.getElementById(nItem+'_select').style.display='block';	
		document.getElementById(nItem+'_select').style.position='absolute';
	} else {
		document.getElementById(nItem).style.display='block';
		document.getElementById(nItem+'_select').style.display='none';
	}
}
  
function hideData(nItem){
}

function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	tbl.width='100%';
	
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	} else {
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:10px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:125px;\" ><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:95px; font-weight:bold;\" onclick=\"shprodukfc2('"+iteration+"');\" readonly><img name=\"img_i_product_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfc2('"+iteration+"');\"></DIV>" +
	"<div id=\"ajax_i_product_"+nItem+"_"+iteration+"_select\" style=\"display:none;\">" +
	"</div>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:255px;\"><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:245px;\" readonly></DIV>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_n_quantity_akhir_"+nItem+"_"+iteration+"\" style=\"width:100px;\"><input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\"><input type=\"hidden\" ID=\"f_stop_produksi_"+nItem+"_"+iteration+"\" name=\"f_stop_produksi_"+nItem+"_"+iteration+"\" ></DIV>";

}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat">Delivery Order Forecast (DOFC) Per Item Barang</td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form Delivery Order Forecast (DOFC) Per Item Barang</td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php echo $this->pquery->form_remote_tag(array('url'=>'update','update'=>'#pesan','type'=>'post'));?>

		<div id="masterlopvsdoform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="19%">DO dari Tgl </td>
							<td width="0%">&nbsp;</td>
							<td width="81%">
							  <input name="d_op_first" type="text" id="d_op_first" maxlength="10" value="<?=$tglmulai?>"/>
							  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_first,'dd/mm/yyyy',this)">
							s.d 
							<input name="d_last" type="text" id="d_op_last" maxlength="10" value="<?=$tglakhir?>" />
							<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_last,'dd/mm/yyyy',this)">
							</td>
						  </tr>						  
						</table>
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td>
			  	<div id="title-box2">Detail Item Barang</div>
			  	</td>
			  </tr>

				<tr>
				 <td colspan="6">
					<div id="title-box2">
					<div style="float:right;">
					<a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
					<a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
					</div></div>
				 </td>	
				</tr>



			  	<tr>
				<table>
				<td colspan="4">	 	
				<table class="table" cellspacing="0" cellpadding="0" width="100%" >
						<tr>
						 <td width="50px" class="tdatahead" align="center"><b>NO</b></td>
						 <td width="200px" class="tdatahead" ><b>KODE BARANG</b></td>
						 <td width="300px" class="tdatahead"><b>NAMA BARANG</b></td>
						 <td width="200px" class="tdatahead"><b>SALDO AWAL</b></td>
						</tr>
				
						<tr>
						 <td colspan="4">
						 	<table id="tblItem" class="table table-striped" cellspacing="0" cellpadding="0" width="100%" border="0">
							<?php
							$iter	= 0;
							$db2=$this->load->database('db_external', TRUE);
							foreach($bonmmasukitem as $row2) {
								
								//--------------08-01-2014 ----------------------------
								// ambil data qty warna dari tr_product_color dan tm_inbonm_item_color
								$sqlxx	= $db2->query(" SELECT f.i_product_color,
											UPPER(c.i_product) AS imotif, 
											c.e_product_name AS productmotif, 
											c.qty as saldo_awal, 
											d.e_color_name, 
											d.i_color 
											FROM tm_saldo_awalfc c 
											left join tm_dofc a on (a.f_do_cancel='f' 
											AND (a.d_do >='$var_ddofirst' 
											AND a.d_do <='$var_ddolast')) 
											left join tm_dofc_item b on(c.i_product=b.i_product 
											and a.i_do = b.i_do) 
											left join tm_dofc_item_color e on(e.i_do_item=b.i_do_item and c.i_color=e.i_color) 
											left join tr_color d on (d.i_color=c.i_color)
											left join tr_product_color f on (c.i_product = f.i_product_motif and d.i_color=f.i_color ) 
											WHERE c.i_periode='$periode' AND c.i_product='$row2->imotif'
											GROUP BY i_product_color, c.i_product, c.e_product_name, c.qty, d.e_color_name, d.i_color 
											ORDER BY c.i_product, d.i_color ASC ");
								
								$listwarna = "";
								if ($sqlxx->num_rows() > 0){
									$hasilxx = $sqlxx->result();
									
									foreach ($hasilxx as $rownya) {
										//$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
										$listwarna .= $rownya->e_color_name." 
										
										<input type='text' style='width:90px' name='qty_warna_".$iter."[]' value='".$rownya->saldo_awal."'>
										<input type='hidden' name='i_color_".$iter."[]' value='".$rownya->i_color."'>
										<input type='hidden' name='i_product_color_".$iter."[]' value='".$rownya->i_product_color."'><br>";
									}
								}
						
								//-----------------------------------------------------
								
								echo "
				<tr>
				<td><div style=\"font:24px;text-align:center;width:20px;margin-right:0px;\">".($iter*1+1)."</div></td>

				<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:130px;\" >
									
				<input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:110px;\" value=\"".$row2->imotif."\" readonly >
									
				<input type=\"hidden\" name=\"qty_product_tblItem_".$iter."\" id=\"qty_product_tblItem_".$iter."\">
			<input type=\"hidden\" ID=\"periode".$iter."\"  name=\"periode".$iter."\" value=\"".$periode."\" readonly >
									
				<img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfc2('".$iter."');\">
					</DIV></td>
									
				<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:300px;\"><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\"  name=\"e_product_name_tblItem_".$iter."\" style=\"width:290px;\" value=\"".$row2->productmotif."\" readonly ></DIV></td>
									
				<td style='white-space:nowrap;'>
				
				<DIV ID=\"ajax_n_count_product_tblItem_".$iter."\" style=\"width:150px;\">
				
				".$listwarna."
				<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$iter."\"></DIV>";
									
									echo "</td>
								</tr>";
								$iter+=1;
							}
							?>
						 	</table>
						 	</table>
			  </tr>
				<input type="hidden" name="jml" id="jml" value="<?php echo $iter; ?>">
			  <tr>

<?php
/*
<td><input name="update" id="update" value="Update Saldo Awal Fc" type="submit" class="btn btn-default btn-sm" onclick="window.location='<?php echo base_url() ?>index.php/listdofcperproduk/cform/viewdata/<?php echo $var_ddofirst; ?>/<?php echo $var_ddolast; ?>/'"></td>
*/
?>
<td><input name="update" id="update" value="Update Saldo Awal Fc" type="submit" class="btn btn-default btn-sm" onclick="window.location='<?php echo base_url() ?>index.php/listdofcperproduk/cform'"></td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
