<script language="javascript" type="text/javascript">

function getJatuhTempo(source,destination,range){ // d/m/Y
	var nilai;

	var tsplit	= source.split('/'); // d/m/Y
	var t	= parseInt(tsplit['0'])+parseInt(range);
	var tanggal = new Date(tsplit['2'],tsplit['1'],t,0,0,0); 

	var tgl	= tanggal.getDate();
	var bln = tanggal.getMonth();
	var thn = tanggal.getFullYear();

	nilai = tgl+'/'+bln+'/'+thn;
	destination.value	= nilai;
}

function tgpajak(tfaktur){
	document.getElementById('d_pajak').value=tfaktur;
}

function total(iterasi) {
	var	total;
	var	price;
	var	unit0;
	
	price	= document.getElementById('v_hjp_tblItem_'+iterasi);
	unit0	= document.getElementById('n_quantity_tblItem_'+iterasi);
	
	if(parseInt(price.value) && parseInt(unit0.value)) {
		total	= parseInt(price.value) * parseInt(unit0.value);
		document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
	}
}

function ckfpenjualan(nomor,nomorhiden){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listpenjualanperdo/cform/cari_fpenjualan');?>",
	data:"fpenj="+nomor+"&fpenjhiden="+nomorhiden,
	success: function(data){
		$("#confnomorfpenj").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka!");
		angka.value	= 0;
		angka.focus();
	}
}

function tnilaibarang(){

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseInt(unitprice.value);
		}
	}
	totalnilai.value	= totaln;
}

function test() {

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var nilai	= totalnilai;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;	
	var val		= document.getElementById('n_discount');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseFloat(unitprice.value);
		}
	}
		
	totalnilai.value	= totaln;
	
	if(parseInt(nilai.value)) {
		hasil 	= ( (parseInt(nilai.value)*val.value) / 100 ); // dikurangi discount
		total_sblm_ppn	= parseInt(nilai.value) - hasil; // DPP
		nilai_ppn	= ( (total_sblm_ppn*10) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;
		
		document.getElementById('v_discount').value = Math.round(hasil);
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*10) / 100 );
		total_grand	= xtotaln + nilai_ppn;

		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

function test2() {
	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var nilai	= totalnilai;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;	
	
	var aa;
	var val		= document.getElementById('v_discount');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseFloat(unitprice.value);
		}
	}
		
	totalnilai.value	= totaln;
	
	if(parseInt(nilai.value)) {
		aa	= (val.value / parseInt(nilai.value))*100;
		total_sblm_ppn	= parseInt(nilai.value) - val.value;
		nilai_ppn	= ( (total_sblm_ppn*10) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;

		if(aa>0 && val.value>0){
			document.getElementById('n_discount').value = Math.round(aa*Math.pow(10,2))/Math.pow(10,2);
		}else{
			document.getElementById('n_discount').value = 0;
		}
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*10) / 100 );
		total_grand	= xtotaln + nilai_ppn;

		document.getElementById('n_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
	}
}

function getdsk() {
	var nilai;
	var val;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;

	nilai	= document.getElementById('v_total_nilai');
	val		= document.getElementById('n_discount');

	if(parseInt(nilai.value)) {
		hasil 	= ( (parseInt(nilai.value)*val.value) / 100 );
		total_sblm_ppn	= parseInt(nilai.value) - hasil;
		nilai_ppn	= ( (total_sblm_ppn*10) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;

		document.getElementById('v_discount').value = Math.round(hasil);
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*10) / 100 );
		total_grand	= xtotaln + nilai_ppn;

		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	
	var cab	= document.getElementById('i_branch');
	
	tbl.width='100%';
	
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	}else{
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:18px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_do_"+nItem+"_"+iteration+"\" style=\"width:90px;\"><input type=\"text\" ID=\"i_do_"+nItem+"_"+iteration+"\"  name=\"i_do_"+nItem+"_"+iteration+"\" style=\"width:60px;\" onfocus=\"total("+iteration+");test();\"><img name=\"img_i_do_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor DO\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualanx('"+iteration+"','"+document.getElementById('i_branch').value+"');\"></DIV>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:80px;\"><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:80px;\"></DIV>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:290px;\"><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:290px;\"></DIV>";
	
	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_v_hjp_"+nItem+"_"+iteration+"\" style=\"width:80px;\"><input type=\"text\" ID=\"v_hjp_"+nItem+"_"+iteration+"\"  name=\"v_hjp_"+nItem+"_"+iteration+"\" style=\"width:80px;text-align:right;\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(5);
	cell1.innerHTML = "<DIV ID=\"ajax_n_quantity_"+nItem+"_"+iteration+"\" style=\"width:80px;\"><input type=\"text\" ID=\"n_quantity_"+nItem+"_"+iteration+"\"  name=\"n_quantity_"+nItem+"_"+iteration+"\" style=\"width:80px;text-align:right;\" onkeyup=\"total("+iteration+");test();validNum('n_quantity_tblItem','"+iteration+"');\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(6);
	cell1.innerHTML = "<DIV ID=\"ajax_v_unit_price_"+nItem+"_"+iteration+"\" text-align:right;\" ><input type=\"text\" ID=\"v_unit_price_"+nItem+"_"+iteration+"\"  name=\"v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:85px;text-align:right;\" onkeyup=\"total("+iteration+");test();validNum('v_unit_price_tblItem','"+iteration+"');\" value=\"0\" ><input type=\"hidden\" name=\"iteration\" value=\""+iteration+"\" ></DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}

function removeRowFromTableItem(nItem,Num) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.deleteRow(Num);
	}
}

</script>

<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#d_faktur").datepicker();
	$("#d_due_date").datepicker();
	$("#d_pajak").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_fpenjualando; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_fpenjualando; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		  <?php 
		    echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanperdo/cform/actedit','update'=>'#content','type'=>'post'));
		  ?>
		<div id="masterfpenjualandoform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $form_nomor_f_fpenjualando; ?> : 
							  <input name="i_faktur_code" type="text" id="i_faktur_code" maxlength="13" value="<?php echo $ifakturcode; ?>" onkeyup="ckfpenjualan(this.value,document.getElementById('ifakturcodehiden').value);" />
							  <div id="confnomorfpenj" style="color:#FF0000;"></div>
							  </td>
							<td width="33%"><?php echo $form_tgl_f_fpenjualando; ?> : 
							  <input name="d_faktur" type="text" id="d_faktur" maxlength="10" value="<?php echo $tgFAKTUR; ?>" onchange="getJatuhTempo(this.value,document.getElementById('d_due_date'),37); tgpajak(this.value);" />
							  <!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_faktur,'dd/mm/yyyy',this)"> -->
							  </td>
							<td width="33%"><?php echo $form_cabang_fpenjualando; ?>: 
							  <select name="i_branch" id="i_branch">
								<option value="">[ <?php echo $form_pilih_cab_fpenjualando;?> ]</option>
								<?php
									foreach($opt_cabang as $row) {
										//echo $row->einitial." ".$ibranchname;
										$sel	= $ibranchname==$row->einitial?"selected":"";
										$lcabang	.="<option value=".$row->codebranch." $sel >";
										$lcabang	.= $row->branch." ( ".$row->einitial." ) ";
										$lcabang	.= "</option>";
									}
									echo $lcabang;
								?>
							  </select>
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>
					<tr>
					  <td>
						  <div id="title-box2"><?php echo $form_detail_f_fpenjualando; ?>					  
						  <div style="float:right;">
						  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');total(document.getElementById('tblItem').rows.length);test();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
						  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');total((document.getElementById('tblItem').rows.length)-1);test();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
						  </div></div>					  
					  </td>
					</tr>
					<tr>
					  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="4%" class="tdatahead">NO</td>
						  <td width="12%" class="tdatahead"><?php echo $form_nomor_do_f_fpenjualando; ?></td>
						  <td width="10%" class="tdatahead"><?php echo $form_kd_brg_fpenjualando; ?></td>
						  <td width="38%" class="tdatahead"><?php echo $form_nm_brg_fpenjualando; ?></td>
						  <td width="10%" class="tdatahead"><?php echo $form_hjp_fpenjualando; ?></td>
						  <td width="12%" class="tdatahead"><?php echo $form_qty_fpenjualando; ?></td>
						  <td width="13%" class="tdatahead"><?php echo $form_nilai_fpenjualando; ?></td>
						</tr>
						<tr>
						  <td colspan="7">
						  	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							<?php
							$iter	= 0;
							$xtotaln	= 0;
							$size	= sizeof($fakturitem);
							foreach($fakturitem as $row3) {
								
								$tharga	= $row3->unitprice*$row3->qty;
								$xtotaln+=$tharga;	
								
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">".($iter*1+1)."</div></td>
									<td><DIV ID=\"ajax_i_do_tblItem_".$iter."\" style=\"width:90px;\"><input type=\"text\" ID=\"i_do_tblItem_".$iter."\"  name=\"i_do_tblItem_".$iter."\" style=\"width:60px;\" onfocus=\"total('".$iter."');test();\" value=\"".$row3->idocode."\"><img name=\"img_i_do_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor DO\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualanx('".$iter."',document.getElementById('i_branch').value)\"></DIV></td>
									<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\"  style=\"width:80px;\"><input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:80px;\" value=\"".$row3->imotif."\"></DIV></td>
									<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:290px;\" ><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\" name=\"e_product_name_tblItem_".$iter."\" style=\"width:290px;\" value=\"".$row3->motifname."\"></DIV></td>
									<td><DIV ID=\"ajax_v_hjp_tblItem_".$iter."\" style=\"width:80px;\"><input type=\"text\" ID=\"v_hjp_tblItem_".$iter."\" name=\"v_hjp_tblItem_".$iter."\" style=\"width:80px;text-align:right;\" value=\"".$row3->unitprice."\" ></DIV></td>
									<td><DIV ID=\"ajax_n_quantity_tblItem_".$iter."\" style=\"width:80px;\"><input type=\"text\" ID=\"n_quantity_tblItem_".$iter."\" name=\"n_quantity_tblItem_".$iter."\" style=\"width:80px;text-align:right;\" onkeyup=\"total('".$iter."');test();validNum('n_quantity_tblItem','".$iter."');\" value=\"".$row3->qty."\" ></DIV></td>
									<td><DIV ID=\"ajax_v_unit_price_tblItem_".$iter."\" text-align:right;\" ><input type=\"text\" ID=\"v_unit_price_tblItem_".$iter."\"  name=\"v_unit_price_tblItem_".$iter."\" style=\"width:85px;text-align:right;\" onkeyup=\"total('".$iter."');test();validNum('v_unit_price_tblItem','".$iter."');\" value=\"".$tharga."\" ><input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\"></DIV></td>
								</tr>";
								$iter++;
							}
							
							$nilai_ppn	= ( ($xtotaln*10) / 100 );
							$total_grand	= $xtotaln + $nilai_ppn;
							
							if($n_discount>0){
								$hasil 	= $v_discount; // dikurangi discount
								$total_sblm_ppn	= round($xtotaln - $hasil); // DPP
								$nilai_ppn2	= round((($total_sblm_ppn*10) / 100 ));
								$total_grand2	= round($total_sblm_ppn + $nilai_ppn2);
							}else{
								$total_sblm_ppn	= round($xtotaln); // DPP
								$nilai_ppn2	= round($nilai_ppn);
								$total_grand2	= round($total_grand);	
							}											
							?>
							</table>
						  </td>
						</tr>						
                      </table></td>
					</tr>	
					<tr>
					 <td>
					  <div id="title-box2">
					  <div style="float:right;">
					  &nbsp;
					  </div></div>
					 </td>	
					</tr>
					<tr>
					  <td><table width="98%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="16%">&nbsp;</td>
						  <td width="1%">&nbsp;</td>
						  <td width="23%">&nbsp;</td>
						  <td width="24%">&nbsp;</td>
						  <td width="14%"><?php echo $form_tnilai_fpenjualando; ?></td>
						  <td width="1%">:</td>
						  <td width="21%">
							<input name="v_total_nilai" type="text" id="v_total_nilai" size="15" maxlength="15" value="<?=$xtotaln?>"/>
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>

						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_diskon_fpenjualando; ?> (%)</td>
						  <td>:</td>
						  <td>
						  <input name="n_discount" type="text" id="n_discount" size="10" maxlength="10" value="<?=$n_discount?>" onkeyup="test();validNum('n_discount');" />
						  <?php echo $form_dlm_fpenjualando; ?> 
						  <input name="v_discount" type="text" id="v_discount" size="12" maxlength="12" value="<?=$v_discount?>" onkeyup="test2();" />
						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_tgl_jtempo_fpenjualando; ?> </td>
						  <td>:</td>
						  <td>
							<input name="d_due_date" type="text" id="d_due_date" size="10" maxlength="10" value="<?php echo $tjthtempo; ?>" />
						  </td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_total_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_faktur" type="text" id="v_total_faktur" size="15" maxlength="15" value="<?=$total_sblm_ppn?>" />
						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_no_fpajak_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="i_faktur_pajak" type="text" id="i_faktur_pajak" size="15" maxlength="18" value="<?php echo $i_faktur_pajak; ?>" />
						  </td>
						  <td><?php echo $form_tgl_fpajak_fpenjualando; ?> :
							<input name="d_pajak" type="text" id="d_pajak" size="10" maxlength="10" value="<?php echo $tgPajak; ?>"/>
							</td>
						  <td><?php echo $form_ppn_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="n_ppn" type="text" id="n_ppn" size="15" maxlength="15" value="<?=$nilai_ppn2?>" />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>
							<input type="checkbox" name="f_cetak" value="1" <?=$f_printed?> />
							&nbsp;<?php echo $form_ket_cetak_fpenjualando; ?></td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_grand_t_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_fppn" type="text" id="v_total_fppn" size="15" maxlength="15" value="<?=$total_grand2?>"/>
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					</tr>
					<tr align="right">
					  <td>
					  	<input type="hidden" name="ifakturhiden" id="ifakturhiden" value="<?=$i_faktur?>" />
						<input type="hidden" name="ifakturcodehiden" id="ifakturcodehiden" value="<?php echo $ifakturcode; ?>" />
						<input name="btnrefresh" type="button" id="btnrefresh" value="Refresh Harga Barang" onclick="show('listpenjualanperdo/cform/refreshperitem/<?php echo $i_faktur; ?>/','#content');" <?php if($disabled=='t'){?> disabled <? } ?> />
						<input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit">
						<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick='show("listpenjualanperdo/cform/","#content")'>
					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
