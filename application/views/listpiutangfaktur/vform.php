<script language="javascript">
	function ck_fnota(){
		if(document.getElementById('f_nota_sederhana').checked==true){
			document.getElementById('tf_nota_sederhana').value = 't';
		}else{
			document.getElementById('tf_nota_sederhana').value = 'f';
		}
	}
</script>

<script type="text/javascript" language="javascript">
	$(document).ready(function(){
	$("#d_faktur_first").datepicker();
	$("#d_faktur_last").datepicker();
	});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_piutang; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_piutang; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
			$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listpiutangfaktur/cform/carilistfaktur', $attributes);?>
		
		<div id="masterlpiutangform">
		
			<table width="60%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_piutang_no_faktur; ?> </td>
					<td width="1%">:</td>
					<td>
					  <input name="no_faktur" type="text" id="no_faktur" maxlength="14" />
					  <input name="i_faktur" type="hidden" id="i_faktur" />
					  <img name="img_i_fpenjualan_" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Tampilkan Nomor Faktur" class="imgLink" align="absmiddle" onclick="infolfaktur(document.getElementById('tf_nota_sederhana').value);">
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_piutang_tgl_faktur; ?> </td>
					<td>:</td>
					<td>
					  <input name="d_faktur_first" type="text" id="d_faktur_first" maxlength="10" />
					s.d 
					<input name="d_faktur_last" type="text" id="d_faktur_last" maxlength="10" />
					<span style="color:#FF0000">*</span>
					</td>
				  </tr>

				  <tr>
					<td width="20%"><?php echo $list_faktur_nota_sederhana; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="f_nota_sederhana" type="checkbox" id="f_nota_sederhana" value="f" onclick="ck_fnota();"/>
					  <input name="tf_nota_sederhana" type="hidden" id="tf_nota_sederhana" value="f" />
					</td>
				  </tr>				  
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" />
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
