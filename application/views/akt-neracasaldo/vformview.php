<?
 	include ("php/fungsi.php");
   	echo "<center><h2>$page_title</h2></center>";
	echo "<center><h3>Per $tanggal $namabulan $tahun</h3></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
    	<tr>
	   	<th rowspan=2 align=center>CoA</th>
			<th rowspan=2 align=center>Keterangan</th>
			<th colspan=2 align=center>Saldo Awal</th>
			<th colspan=2 align=center>Mutasi</th>
			<th colspan=2 align=center>Saldo Akhir</th>
			</tr>
			<tr>
			<th>Debet</th>
			<th>Kredit</th>
			<th>Debet</th>
			<th>Kredit</th>
			<th>Debet</th>
			<th>Kredit</th>
			</tr>
	    <tbody>
	      <?php
		if($query){
			$totaldebet	   = 0;
			$totalkredit   = 0;
			$totalsadebet	 = 0;
			$totalsakredit = 0;
			$totalmdebet	 = 0;
			$totalmkredit  = 0;
			$this->load->model('akt-bb/mmaster');
			foreach($query as $row)
			{
#				$sawal = $this->mmaster->saldoawal($periode,$row->i_coa);
#				$saldo = $sawal+$row->v_mutasi_debet-$row->v_mutasi_kredit;
        $nama='';
        $query=$this->db->query("	select nama from tm_coa where kode='$row->i_coa'",false);
		    if ($query->num_rows() > 0){
		      foreach($query->result() as $raw){
            $nama=$raw->nama;
            $nama=str_replace('(Pusat)','',$raw->nama);
		      }
		    }else{
		      $row->i_coa=substr($row->i_coa,0,5).'01';
		      $query=$this->db->query("	select nama from tm_coa where kode='$row->i_coa'",false);
		      if ($query->num_rows() > 0){
		        foreach($query->result() as $raw){
  #            $nama=$raw->e_coa_name;
              $nama=str_replace('(Pusat)','',$raw->e_coa_name);
		        }
		      } 
		    }
				echo "<tr> 
				<td>$row->i_coa</td>
				<td>$nama</td>";
				if($row->v_saldo_awal<0){
				$saldoawal = $row->v_saldo_awal*-1;
				$status='kredit';
				  echo "
				    <td align=right>0</td>
				    <td align=right>".number_format($saldoawal)."</td>";
				  $totalsakredit = $totalsakredit+$saldoawal;
				}else{
				$saldoawal  = $row->v_saldo_awal;
				$saldoakhir = $row->v_saldo_akhir;
				$status='debet';
				  echo "
				    <td align=right>".number_format($saldoawal)."</td>
 				    <td align=right>0</td>";
				  $totalsadebet = $totalsadebet+$saldoawal;
				}
				$debet  = $row->v_mutasi_debet;
        $kredit = $row->v_mutasi_kredit;
        
##############################################################################################################################
        
				if($row->i_coa==KasBesar){
	        $sql	  = "select sum(debet) as debetall, sum(kredit) as kreditall from f_coa_saldo_kasbesar('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw = pg_fetch_assoc($rs)){
		        $debet      = $raw['debetall'];
		        $kredit     = $raw['kreditall'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
		      }
				}elseif($row->i_coa==KasKecil.'00'){
	        $sql	  = "select sum(debet) as debetall, sum(kredit) as kreditall from f_coa_saldo_kaskecil('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['debetall'];
		        $kredit = $raw['kreditall'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
				}elseif(substr($row->i_coa,0,5) =='111.4'){
	        $sql	  = "select i_coa, debet, kredit from f_coa_saldo_bank('$periode') where i_coa ='$row->i_coa'";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['debet'];
		        $kredit = $raw['kredit'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
		     }
				}elseif($row->i_coa==KHP.'00'){
	        $sql	  = "select * from f_coa_saldo_khp('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['debet'];
		        $kredit = $raw['kredit'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
				}elseif($row->i_coa=='412.200'){
	        $sql	  = "select sum(diskon) as diskon from f_coa_saldo_penjualan('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['diskon'];
		        $kredit = 0;
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
				}elseif($row->i_coa=='411.100'){
	        $sql	  = "select * from f_coa_saldo_penjualan('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['kotor'];
		        $kredit = $raw['kredit'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
	      }elseif($row->i_coa=='112.200'){
	        $sql	  = "select * from f_coa_saldo_piutang('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['debet'];
		        $kredit = $raw['kredit'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
	      }elseif($row->i_coa=='211.100'){
	        $sql	  = "select * from f_coa_saldo_hutang('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['debet'];
		        $kredit = $raw['kredit'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
	      }elseif($row->i_coa=='511.100'){
	        $sql	  = "select sum(kotor) as kotor from f_coa_saldo_pembelian('$periode')";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['kotor'];
		        $kredit = 0;
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
	      }elseif(substr($row->i_coa,0,1)=='6'){
	        $sql	  = "select * from f_coa_saldo_biayaall('$periode') where i_coa='$row->i_coa'";
	        $rs 		= pg_query($sql);
	        while($raw= pg_fetch_assoc($rs)){
		        $debet  = $raw['debet'];
		        $kredit = $raw['kredit'];
		        if($status=='kredit'){
		          $saldoakhir = $debet - ($saldoawal + $kredit);
		        }else{
  		        $saldoakhir = ($saldoawal + $debet) - $kredit;
		        }
	        }
	      }
	      

#################################################################################################################################
				
				echo "
				<td align=right>".number_format($debet)."</td>
				<td align=right>".number_format($kredit)."</td>
				";
 				$totalmdebet  = $totalmdebet+$debet;
 				$totalmkredit = $totalmkredit+$kredit;

        if($status=='kredit'){
          if($saldoakhir>0){
            echo "<td align=right>".number_format($saldoakhir)."</td>
            <td align=right>0</td>";
            $totaldebet = $totaldebet+($saldoakhir);
          }else{
            echo "<td align=right>0</td>
            <td align=right>".number_format($saldoakhir*-1)."</td>";
            $totalkredit = $totalkredit+($saldoakhir*-1);  
          }
        }elseif($status=='debet'){
          if($saldoakhir<0){
            echo "<td align=right>0</td>
            <td align=right>".number_format($saldoakhir*-1)."</td>";
            $totalkredit = $totalkredit+($saldoakhir*-1);
          }else{
            echo "<td align=right>".number_format($saldoakhir)."</td>
            <td align=right>0</td>";
            $totaldebet = $totaldebet+($saldoakhir);          
          }
        }
				echo "</tr>";
			}
				echo "<tr>
				<td colspan=4>&nbsp;</td>";
/*
				<td align=right><h3>".number_format($totalsadebet)."</h3></td>
				<td align=right><h3>".number_format($totalsakredit)."</h3></td>
*/
        echo "
				<td align=right><h3>".number_format($totalmdebet)."</h3></td>
				<td align=right><h3>".number_format($totalmkredit)."</h3></td>
        <td align=right><h3>".number_format($totaldebet)."</h3></td>
				<td align=right><h3>".number_format($totalkredit)."</h3></td></tr>";
/*
				<td align=right><h3>".number_format(0)."</h3></td>
				<td align=right><h3>".number_format(0)."</h3></td>
*/
		}
	      ?>
	    </tbody>
	  </table>
      </div>
	</div>
    </td>
  </tr>
</table>
