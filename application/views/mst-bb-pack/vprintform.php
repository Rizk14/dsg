<h3>Data Harga Bahan Baku/Pembantu Berdasarkan unit_packing</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-bb-pack/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bb-pack/cform/view">View Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bb-pack/cform/print_harga">Export Excel</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">
//tambah
$(function()
{
	$("#goprint").click(function()
	{
		var unit_packing=$("#unit_packing").val(); 		
		var urlnya = "<?php echo base_url(); ?>index.php/mst-bb-pack/cform/do_print_harga/"+unit_packing;
		openCenteredWindow(urlnya);
	});
	
	$("#goexport").click(function()
	{
		var unit_packing=$("#unit_packing").val(); 		
		//alert (unit_packing);
		var urlnya = "<?php echo base_url(); ?>index.php/mst-bb-pack/cform/do_exportexcel_harga/"+unit_packing;
		openCenteredWindow(urlnya);
	});

});

function openCenteredWindow(url) {
		var width = 1200;
		var height = 700;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,menubar,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

Print/Export Excel Data Harga Barang Berdasarkan unit_packing
<div align="center">
<br>
<form>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
    <td width="10%">Unit packing</td>
    <td>
		<select name="unit_packing" id="unit_packing">
					<option value="0" selected="true" >- All -</option>
				<?php foreach ($list_unit_packing as $ujh) { ?>
					<option value="<?php echo $ujh->id ?>"><?php echo $ujh->kode_unit." - ".$ujh->nama ?></option>
				<?php } ?>
				</select>
	</td>
  </tr>
  <tr>
	<td colspan="2"><input type="button" name="goexport" id="goexport" value="Export ke Excel">
	&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb-pack/cform/view'"></td>
  </tr>
	
</table>
</form>
</div>
