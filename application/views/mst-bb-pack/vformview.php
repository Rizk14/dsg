<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Harga Barang WIP Berdasarkan Unit packing</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-bb-pack/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-bb-pack/cform/view">View Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bb-pack/cform/print_harga">Export Excel</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-bb-pack/cform/cari'); ?>
Unit packing
				<select name="unit_packing" id="unit_packing">
					<option value="0" <?php if ($eunit_packing == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_unit_packing as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($sup->id == $eunit_packing) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_unit." - ".$sup->nama ?></option>
				<?php } ?>
				</select>
<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		 <th>Unit packing</th>
		 <th>Kode & Nama Brg</th>
	
		 <th>Harga (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode_unit_packing - $row->nama_ujh</td>";
				 echo    "<td nowrap>$row->kode_brg - $row->nama_brg</td>";
				 
				
				 echo    "<td align='right'>".number_format($row->harga, 2, ',','.')."</td>";
				 echo    "<td>$tgl_update</td>";
				 //<a href=".base_url()."index.php/mst-bb-pack/cform/edit/$row->id/".$cur_page."/".$is_cari."/".$eunit_packing."/".$cari." \" >Edit</a>&nbsp;
				 echo    "<td align=center>";
				 echo    "<a href=".base_url()."index.php/mst-bb-pack/cform/delete/$row->id/".$cur_page."/".$is_cari."/".$eunit_packing."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
