<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function toUpper(val){
	var imotif	= val.value;
	var toUpper	= imotif.toUpperCase();
	val.value	= toUpper;
}

function settextfield(a,b,c) {

   	var ada	= false;
	var z	= "<?=$iterasi?>";
	var y	= parseInt(z)-1;
	
  	for(i=0;i<=y;i++){
		if(
			(a==opener.document.getElementById("i_product_tblItem_"+i).value) &&
			(c==opener.document.getElementById("i_gudang_tblItem_"+i).value)
		){
			alert ("kode Bhn Baku "+a+" sdh ada!");
			ada=true;
			break;
		}else{
			ada=false;
		}
    }
    
    if(!ada){	
		//opener.document.getElementById('i_product_tblItem_'+'<?php echo $iterasi; ?>').value=a;
		opener.document.getElementById('i_code_references_tblItem_'+'<?php echo $iterasi; ?>').value=a;
		opener.document.getElementById('e_product_name_tblItem_'+'<?php echo $iterasi; ?>').value=b;
		opener.document.getElementById('i_gudang_tblItem_'+'<?php echo $iterasi; ?>').value=c;
		opener.document.getElementById('v_product_price_tblItem_'+'<?php echo $iterasi; ?>').focus();
		
		this.close();
	}
}

function getInfo(txt) {

$.ajax({

type: "POST",
url: "<?php echo site_url('sjpbhnbaku/cform/flistbhnbaku');?>",
data:"key="+txt,
success: function(data) {
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};

function carifocus() {
	document.getElementById('txtcari').focus();
}
</script>

</head>
<body id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h2>$page_title</h2></center>"; ?>

<?php echo form_open('sjpbhnbaku/cform/listbhnbaku', array('id' => 'listformbrgjadi')); ?>

<table class="maintable">
  <tr>
  
  	<td>
  	
  	<select name="i_gudang" id="i_gudang">
		<option value="">[ Pilih Gudang ]</option>
		<?php
		foreach($lgudang as $rgudang) {
			$Sel = $igudang==$rgudang->id?'selected':'';
			echo "<option value=\"$rgudang->id\" $Sel >"."[".$rgudang->kode_gudang."]".$rgudang->nama."</option>";
		}
		?>
  	</select>
  	<input type="submit" name="btncari" id="btncari" value="<?php echo $button_cari; ?>" />
  	</td>
  	
  </tr>
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="toUpper(document.getElementById('txtcari'));getInfo(this.value);"/>
  	</td>
  </tr>
  <tr>
    <td align="left">
	
	<div class="effect">
	  <div class="accordion2">

		<?php
					
		if(!empty($isi) || isset($isi)) {
			
			if(count($isi)) {
				$cc	= 1;
				
				foreach($isi as $row) {
					
					$kode_brg	= trim($row->kode_brg);
					
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$kode_brg','$row->nama_brg','$row->id_gudang');\">".$row->kode_brg."</a></td>
					  <td><a href=\"javascript:settextfield('$kode_brg','$row->nama_brg','$row->id_gudang');\">".$row->nama_brg."</a></td>
					 </tr>";
					 $cc+=1;
					 
				}
			}
		}
		?>
		
		<table class="listtable2" border="1">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th><?php echo strtoupper("Kode Bhn Baku"); ?></th>
		 <th><?php echo strtoupper("Nama Bhn Baku"); ?></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>"; ?>
  	</div>
      </div>
      
    </td>
  </tr>
</table>
<?php echo form_close(); ?>
</div>
</body>
</html>
