<h3>Surat Jalan Pengantar (Umum)</h3><br>
<a href="<?php echo base_url(); ?>index.php/sjpumum/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/sjpumum/cform/view">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<?php /* <script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script> */ ?>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-3.7.0.min.js"></script>

<script>


//tambah
$(function()
{

    $("#no").val('1');

    const createItem = () => {   
        var index = $("#no").val();
        index++;

        $("#no").val(index);

        return `<tr key="${index}">
                    <td id="num_${index}" class="no">${index}</td>		  
                    <td nowrap=""><input name="kode_${index}" type="text" id="kode_${index}" size="20" value="" required /></td>
                    <td nowrap=""><input name="nama_${index}" type="text" id="nama_${index}" size="40" value=""  /></td>
                    <td nowrap=""><input name="qty_${index}" type="number" id="qty_${index}" size="10" value="0" required  /></td>
                    <td><input name="satuan_${index}" type="text" id="satuan_${index}" size="15" value="" /></td>   
                    <td><textarea name="remark_${index}" id="remark_${index}" placeholder="Keterangan" size="20"></textarea></td>
                    <td><button type="button" class="btn btn-sm iBtnDel">X</button></td>
                </tr>`;
    }

    $("#addrow2").click(function() {
        const newItem = createItem();
        let wrapper = $('#array-item');
        $(wrapper).append(newItem);
    });  

    $('#array-item').on('click', '.iBtnDel', function() {
        $(this).closest('tr').remove();

        let count = 1;
        var obj = $('#array-item tr:visible').find('.no');
        $.each(obj, function(key, value) {
            $(this).text(count);
            count++;
        });
        
        $('#no').val(count-1);
    });   
    
    




















	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>1) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
	// 11-07-2015
	$('#dibebankan').click(function(){
	  	    if ($("#dibebankan").is(":checked")) {
				$('#ket').attr('disabled', true);
				$('#ket').val('');
			}
			else {
				$('#ket').attr('disabled', false);
				$('#ket').val('');
			}
	  });
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

	var width = 720;
	var height = 480;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
	myWindow = window.open(url, "subWind", windowFeatures);
}

function cari(posisi, kodebrgwip) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/bonmkeluar/cform/caribrgwip', 
				data: 'kode_brg_wip='+kodebrgwip+'&posisi='+posisi, success: function(response) {
					$("#infobrgwip_"+posisi).html(response);
			}}); 
}
	
function cek_bonm() {
	var no_bonm= $('#no_bonm_manual').val();
	var tgl_bonm= $('#tgl_bonm').val();
	var tujuan= $('#tujuan').val();
	var id_unit_jahit= $('#id_unit_jahit').val();
	var id_unit_packing= $('#id_unit_packing').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (no_bonm == '') {
			alert("Nomor Bon M manual harus diisi..!");
			s = 1;
			return false;
		}
		if (tgl_bonm == '') {
			alert("Tanggal Bon M harus dipilih..!");
			s = 1;
			return false;
		}
		
		if (tujuan == 2 && id_unit_jahit==0) {
			alert("Unit Jahit harus Dipilih");
			s = 1;
			return false;
		}
		if (tujuan == 9 && id_unit_packing==0) {
			alert("Unit Packing harus Dipilih");
			s = 1;
			return false;
		}
		
		var jum= $('#no').val(); 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if ($('#kode_'+k).val() == '') {
					alert("Data item bahan baku/pembantu tidak boleh ada yang kosong...!");
					s = 1;
					return false;
				}
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka..!");
					s = 1;
					return false;
				}
				/*if ($('#id_brg_wip_'+k).val() == '') {
					alert("Kode barang WIP harus diisi..!");
					s = 1;
					return false;
				} */
				// 150113 disabled aja, biarin ngaruh ke stok harga juga
				/*if ($('#stok_'+k).val() - $('#qty_'+k).val() < 0 ) {
					alert("Pengeluaran barang "+$('#nama_'+k).val()+" melebihi stok...!");
					s = 1;
					return false;
				} */
				
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}

function generate_nomor_dokumen() {
    $.ajax({
        type:'post', 
        url: '<?php echo base_url();?>index.php/sjpumum/cform/generate_nomor_dokumen', 
        data: {
            date: $('#tgl_bonm').val()
        },
        success: function(response) {
            $('#no_bonm_manual').val(response);
        }
    })
}


</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sjpumum/cform/submit" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<input type="hidden" name="no_bonm" id="no_bonm" value="">
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
    <tr>
        <td>Pengirim</td>
        <td><input name="e_sender" type="text" id="e_sender" maxlength="30" value=""></td>
    </tr>
	<tr>
		<td width="15%">Lokasi Gudang</td>
		<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
    <tr>
        <td>Area</td>
        <td>
            <select name="i_area" id="i_area">
                <?php foreach ($list_area as $area) { ?>
					<option value="<?= $area->i_area ?>" ><?= "$area->e_area_city - $area->e_area_name" ?></option>
				<?php } ?>
            </select>
        </td>
    </tr>
  <!--<tr>
    <td width="15%">Nomor Bon M / SJ</td>
    <td>
      <input name="no_bonm" type="text" id="no_bonm" size="10" maxlength="20" value="<?php //echo $no_bonm ?>" readonly="true">
      &nbsp; Nomor Manual <input name="no_bonm_manual" type="text" id="no_bonm_manual" size="15" maxlength="15" value="">
    </td>
  </tr> -->
  <tr>
    <td>Tanggal Bon M / SJ</td>
    <td>
	<label>
      <input name="tgl_bonm" type="text" id="tgl_bonm" size="10" value="" readonly="true" onchange="generate_nomor_dokumen()">
    </label>
	   <img alt="" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_bonm,'dd-mm-yyyy',this);">
	</td>
  </tr> 
  <tr>
    <td width="15%">Nomor Bon M / SJ Manual</td>
    <td>
       <input name="no_bonm_manual" type="text" id="no_bonm_manual" size="20" maxlength="20" value="">
    </td>
  </tr> 
  <tr>
		<td>Tujuan</td>
		<td> <select name="tujuan" id="tujuan">
				<option value="1" >Cutting</option>
				<option value="2" >Unit Jahit</option>
				<option value="3" >Lain-Lain (Retur)</option>
				<option value="4" >Lain-Lain (Peminjaman)</option>
				<option value="5" >Lain-Lain (Lainnya)</option>
				<option value="6" >Pengadaan</option>
				<option value="7" >QC/WIP</option>
				<option value="8" >Makloon</option>
				<option value="9" >Unit Packing</option>
			<!--	<option value="6" >Pengadaan</option>
				<option value="7" >Quilting</option>
				<option value="8" >WIP</option>
				<option value="9" >RND</option>
				<option value="10" >BLN Kedungreja</option> -->
				</select>
		</td>
	</tr>
	
	<tr>
		<td>Unit Jahit</td>
		<td> <select name="id_unit_jahit" id="id_unit_jahit">
				<option value="0">Tidak Ada</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Unit Packing</td>
		<td> <select name="id_unit_packing" id="id_unit_packing">
				<option value="0">Tidak Ada</option>
				<?php foreach ($list_unit_packing as $pck) { ?>
					<option value="<?php echo $pck->id ?>" ><?php echo $pck->kode_unit."-".$pck->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
    <tr>
        <td>Dibebankan</td>
        <td>
            <?php /* <input name="ket" type="text" id="ket" size="30" value=""> */ ?>
            <input type="checkbox" name="dibebankan" id="dibebankan" value="DIBEBANKAN">
        </td>  
    </tr>
<!--  <tr>
	<td colspan="2"><i>*) Untuk bahan yg satuannya Yard, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Meter (kecuali quilting, tetap Yard)<br>
	*) Untuk bahan yg satuannya Lusin, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Pieces<br>
	*) Untuk bahan lain yg memerlukan konversi satuan, silahkan pilih di kolom Satuan Konversi<br></i></td>
  </tr> -->
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
        <table id="tabelku" width="90%" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="8" align="left">
			<input id="addrow2" type="button" name="addrow2" value="&nbsp&nbsp+&nbsp&nbsp" title="Tambah Item Barang">&nbsp;
			<?php /* <input id="deleterow" type="button" name="deleterow" value="&nbsp&nbsp-&nbsp&nbsp " title="Hapus Item Barang"> */ ?>
			</td>
		</tr>
        <tr>
            <th width="5%">No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Keterangan</th>
            <th>Aksi</th>
        </tr>
			<tbody id="array-item">
            <tr>
                <td id="num_1" class="no">1</td>		  
                <td nowrap="">
                    <input name="kode_1" type="text" id="kode_1" size="20" value="" required />
                </td>
                <td nowrap="">
                    <input name="nama_1" type="text" id="nama_1" size="40" value=""  />
                </td>
                <td nowrap="">
                    <input name="qty_1" type="number" id="qty_1" size="10" value="0" required  />
                </td>
                <td>
                    <input name="satuan_1" type="text" id="satuan_1" size="15" value="" /></td>   
                <td>
                    <textarea name="remark_1" id="remark_1" placeholder="Keterangan" size="20"></textarea>
                </td>
                <td><button class="btn btn-sm iBtnDel" id="">X</button></td>
	     	</tr>
	    </tbody>
	</table>	
	
	</form><br>
	<div align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_bonm();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/bonmkeluar/cform/view'"></div>

     </td>
    </tr>

</table>
</div>
</form>
