<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var id_brg=$("#id_brg").val();
		var des=$("#des").val();
		var qty=$("#qty").val();
		var stok=$("#stok").val();
		var satuan=$("#satuan").val();
		// 29-10-2014
		var satuan_konv=$("#satuan_konv").val();
		var rumus_konv=$("#rumus_konv").val();
		var angka_faktor_konversi=$("#angka_faktor_konversi").val();
		
		if (qty == undefined || qty == '')
			qty = 0;
		
			opener.document.forms["f_purchase"].id_brg_<?php echo $posisi ?>.value=id_brg;
			opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=idx+" - "+des;
			opener.document.forms["f_purchase"].satuan_<?php echo $posisi ?>.value=satuan;
			
			opener.document.forms["f_purchase"].tsatuan_konv_<?php echo $posisi ?>.value=satuan_konv;
			opener.document.forms["f_purchase"].satuan_konv_<?php echo $posisi ?>.value=satuan_konv; 
			opener.document.forms["f_purchase"].rumus_konv_<?php echo $posisi ?>.value=rumus_konv;
			opener.document.forms["f_purchase"].angka_faktor_konversi_<?php echo $posisi ?>.value=angka_faktor_konversi;
			
			//opener.document.forms["f_purchase"].qty_<?php echo $posisi ?>.value=qty;			
			opener.document.forms["f_purchase"].stok_<?php echo $posisi ?>.value=stok;
			self.close();
		
	});
});
</script>
<center>
<?php
	if ($is_quilting == 'f') {
?>
<h3>Daftar Barang Di <?php echo $nama_gudang ?></h3>
<?php
	} else {
?>
<h3>Daftar Barang Di Gudang Bahan Baku</h3>
<?php
	}
?>

</center>
<div align="center"><br>
<?php echo form_open('bonmkeluar/cform/show_popup_brg'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="id_gudang" value="<?php echo $id_gudang ?>">
<input type="hidden" name="is_quilting" value="<?php echo $is_quilting ?>">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="id_brg" id="id_brg">
<input type="hidden" name="des" id="des">
<input type="hidden" name="qty" id="qty">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="satuan_konv" id="satuan_konv">
<input type="hidden" name="rumus_konv" id="rumus_konv">
<input type="hidden" name="angka_faktor_konversi" id="angka_faktor_konversi">
<input type="hidden" name="stok" id="stok">

</form>

  <table border="1" align="center" cellpadding="1" width="100%" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Satuan<br>Konversi</th>
      <th bgcolor="#999999">Stok Terkini<br>(Sat Awal)</th>
      <th bgcolor="#999999">Stok Terkini<br>(Sat Konversi)</th>
      <th bgcolor="#999999">Action</th>
    </tr>
	<?php 
		//$i=1; 
		
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
				
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg']; ?></td>
      <td nowrap><?php echo $query[$j]['nama_brg']; ?></td>
      <td><?php echo $query[$j]['satuan']; ?></td>
      
      <td><?php if ($query[$j]['id_satuan_konversi'] == '0') echo "Tidak Ada"; else {
				$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '".$query[$j]['id_satuan_konversi']."' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan_konversi	= $hasilrow->nama;
				}
				else
					$nama_satuan_konversi = '';
				
				echo $nama_satuan_konversi;
		}
		   ?></td>
      
      <td align="right"><?php if ($query[$j]['jum_stok']!= 0) echo $query[$j]['jum_stok']; else echo "0"; ?></td>
      <td align="right"><?php if ($query[$j]['jum_stok_konversi']!= 0) echo $query[$j]['jum_stok_konversi']; else echo "0"; ?></td>
      <td align="center">
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
		window.document.f_master_brg.id_brg.value='<?php echo $query[$j]['id_brg'] ?>';
	  window.document.f_master_brg.des.value='<?php 
	  $pos = strpos($query[$j]['nama_brg'], "\"");
	  if ($pos > 0)
		echo str_replace("\"", "&quot;", $query[$j]['nama_brg']);
	  else
		echo str_replace("'", "\'", $query[$j]['nama_brg']);
	  
	  //if($quilting == 't') echo str_replace("\"", "&quot;", $query[$j]['nama_brg']); else echo str_replace("'", "\'", $query[$j]['nama_brg']); ?>'; 
	  window.document.f_master_brg.qty.value='<?php echo $query[$j]['jum_stok'] ?>'; 
	  window.document.f_master_brg.stok.value='<?php echo $query[$j]['jum_stok'] ?>'; 
	  window.document.f_master_brg.satuan.value='<?php echo $query[$j]['satuan'] ?>';
	  window.document.f_master_brg.satuan_konv.value='<?php echo $query[$j]['id_satuan_konversi'] ?>';
	  window.document.f_master_brg.rumus_konv.value='<?php echo $query[$j]['rumus_konversi'] ?>';
	  window.document.f_master_brg.angka_faktor_konversi.value='<?php echo $query[$j]['angka_faktor_konversi'] ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
