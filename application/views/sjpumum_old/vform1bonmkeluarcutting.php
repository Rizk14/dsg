<h3>Data SJ Keluar Bahan Baku (Hasil Cutting)</h3><br>
<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/addbonmkeluarcutting">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/viewbonmkeluarcutting">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kodex*************************************
		var kodex="#kodex_"+n;
		var new_kodex="#kodex_"+no;
		$(kodex, lastRow).attr("id", "kodex_"+no);
		$(new_kodex, lastRow).attr("name", "kodex_"+no);		
		$(new_kodex, lastRow).attr("onkeyup", "cari('"+no+"', this.value);");		
		$(new_kodex, lastRow).val('');		
		//*****end kodex*********************************
		
		//*****brg_jadi*************************************
		var brg_jadi="#brg_jadi_"+n;
		var new_brg_jadi="#brg_jadi_"+no;
		$(brg_jadi, lastRow).attr("id", "brg_jadi_"+no);
		$(new_brg_jadi, lastRow).attr("name", "brg_jadi_"+no);		
		$(new_brg_jadi, lastRow).val('');		
		//*****end brg_jadi*********************************
		
		//*****kode_brg_jadi*************************************
		var kode_brg_jadi="#kode_brg_jadi_"+n;
		var new_kode_brg_jadi="#kode_brg_jadi_"+no;
		$(kode_brg_jadi, lastRow).attr("id", "kode_brg_jadi_"+no);
		$(new_kode_brg_jadi, lastRow).attr("name", "kode_brg_jadi_"+no);		
		$(new_kode_brg_jadi, lastRow).val('');				
		//*****end kode_brg_jadi*************************************
		
		//******div infobrgjadi*************************************
		var infobrgjadi="#infobrgjadi_"+n;
		var new_infobrgjadi="#infobrgjadi_"+no;
		$(infobrgjadi, lastRow).attr("id", "infobrgjadi_"+no);
		$(new_infobrgjadi, lastRow).html("<input type='text' id='brg_jadi_"+no+"' name='brg_jadi_"+no+"' value='' readonly='true' size='40'>"+
							"<input name='kode_brg_jadi_"+no+"' type='hidden' id='kode_brg_jadi_"+no+"' value='' >");
																
		//*****qty*************************************
	/*	var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');	*/
		//*****end qty*************************************	
		
		// temp_qty
		var temp_qty="#temp_qty_"+n;
		var new_temp_qty="#temp_qty_"+no;
		$(temp_qty, lastRow).attr("id", "temp_qty_"+no);
		$(new_temp_qty, lastRow).attr("name", "temp_qty_"+no);		
		$(new_temp_qty, lastRow).val('');	
		
		//******div qtywarna*************************************
		var qtywarna="#qtywarna_"+n;
		var new_qtywarna="#qtywarna_"+no;
		$(qtywarna, lastRow).attr("id", "qtywarna_"+no);
		$(new_qtywarna, lastRow).html("<input type='hidden' id='temp_qty_"+no+"' name='temp_qty_"+no+"' value=''>");
		
		//*****stok*************************************
		var stok="#stok_"+n;
		var new_stok="#stok_"+no;
		$(stok, lastRow).attr("id", "stok_"+no);
		$(new_stok, lastRow).attr("name", "stok_"+no);		
		$(new_stok, lastRow).val('');				
		//*****end stok*************************************	
		
		//*****ket_detail*************************************
		var ket_detail="#ket_detail_"+n;
		var new_ket_detail="#ket_detail_"+no;
		$(ket_detail, lastRow).attr("id", "ket_detail_"+no);
		$(new_ket_detail, lastRow).attr("name", "ket_detail_"+no);		
		$(new_ket_detail, lastRow).val('');				
		//*****end ket_detail*************************************	
		
		//button pilih_brg*****************************************
		// 25-03-2014, DIKOMEN KARENA SKRG PAKE JQUERY ONKEYUP
		/* var pilih_brg="#pilih_brg_"+n;
		 var new_pilih_brg="#pilih_brg_"+no;
		 $(pilih_brg, lastRow).attr("id","pilih_brg_"+no);
            
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg_jadi/"+no+"');";

		 $(new_pilih_brg, lastRow).attr("name", "pilih_brg_"+no);		
		 $(new_pilih_brg, lastRow).attr("onclick",even_klik);		 */
		//end button pilih_brg
								
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_bonm() {
	var no_bonm_manual= $('#no_bonm_manual').val();
	var tgl_bonm= $('#tgl_bonm').val();
	var kode_unit_jahit= $('#kode_unit_jahit').val();
	var jenis= $('#jenis').val();
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {			
		if (no_bonm_manual == '') {
			alert("Nomor Bon M harus diisi..!");
			return false;
		}
		if (tgl_bonm == '') {
			alert("Tanggal Bon M harus dipilih..!");
			return false;
		}
		/*if (kode_unit_jahit == '0') {
			alert("Unit Jahit harus dipilih..!");
			return false;
		} */
		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				if ($('#brg_jadi_'+k).val() == '') {
					alert("Data item barang jadi tidak boleh ada yang kosong...!");
					return false;
				}
			//	if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
			//		alert("Data qty tidak boleh 0 / kosong...!");
			//		s = 1;
			//		return false;
			//	}
			//	if (isNaN($('#qty_'+k).val()) ) {
			//		alert("Qty harus berupa angka..!");
			//		s = 1;
			//		return false;
			//	}
				if ($('#temp_qty_'+k).val() == '') {
					alert("Data item barang "+ $('#kode_brg_jadi_'+k).val() +" belum ada warnanya, silahkan input dulu di menu Master Warna Brg Jadi WIP...!");
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
		
		$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/bonmkeluar/cform/ceknobonm', 
				data: 'no_bonm_manual='+no_bonm_manual+'&jenis='+jenis+'&tgl_bonm='+tgl_bonm+'&kode_unit_jahit='+kode_unit_jahit, 
				success: function(response) {
					if (response == "ada") {
						alert("Nomor Bon M "+no_bonm_manual+ " sudah ada. silahkan input nomor yang lain");
						return false;
					}
					else
						document.f_purchase.submit();
		}});
		
	} // end if kon
	else
		return false;
}

function cari(posisi, kodebrgjadi) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/bonmkeluar/cform/caribrgjadi', 
				data: 'kode_brg_jadi='+kodebrgjadi+'&posisi='+posisi, success: function(response) {
					$("#infobrgjadi_"+posisi).html(response);
			}});
	
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/bonmkeluar/cform/additemwarna', 
				data: 'kode_brg_jadi='+kodebrgjadi+'&posisi='+posisi, success: function(response) {
					$("#qtywarna_"+posisi).html(response);
					window.close();
			}});
}

</script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/bonmkeluar/cform/submitbonmkeluarcutting" method="post" enctype="multipart/form-data">
<input type="hidden" name="no" id="no" value="">
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
    <td width="15%">Nomor Bon M / SJ</td>
    <td>
      <input name="no_bonm" type="text" id="no_bonm" size="10" maxlength="20" value="<?php echo $no_bonm ?>" readonly="true">
      &nbsp; Nomor Manual <input name="no_bonm_manual" type="text" id="no_bonm_manual" size="15" maxlength="15" value=""> <!--<span id="hasilceknobonm">&nbsp;</span>-->
    </td>
    
  </tr>
  <tr>
    <td>Tanggal Bon M / SJ</td>
    <td>
	<label>
      <input name="tgl_bonm" type="text" id="tgl_bonm" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_bonm" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_bonm,'dd-mm-yyyy',this)">
	</td>
  </tr>  
  <tr>
		<td>Jenis Keluar</td>
		<td> <select name="jenis" id="jenis">
				<option value="1" >Keluar Bagus</option>
				<option value="2" >Pengembalian Retur</option>
				<option value="3" >Lain-Lain</option>
				</select>
		</td>
	</tr>
	
	<tr>
		<td>Unit Jahit</td>
		<td> <select name="kode_unit_jahit" id="kode_unit_jahit">
				<option value="0">- Tidak Ada -</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->kode_unit ?>" ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select> <i>* Jika jenis keluarnya Lain-Lain, unit jahit tidak usah dipilih</i>
		</td>
	</tr>
	
  <tr>
    <td>Keterangan</td>
    <td>
      <input name="ket" type="text" id="ket" size="30" value="">
    </td>  
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku" width="70%" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="5" align="left">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode</th>
          <th>Nama Barang Jadi</th>
	      <th>Qty</th>
	      <th>Keterangan</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>		  
          <td><input name="kodex_1" type="text" id="kodex_1" size="10" value="" onkeyup="cari('1',this.value);" /></td>  
          <td style="white-space:nowrap;"><div id="infobrgjadi_1">
			<input name="brg_jadi_1" type="text" id="brg_jadi_1" size="40" value="" readonly="true"/>
			<input name="kode_brg_jadi_1" type="hidden" id="kode_brg_jadi_1" value=""/>
          <!--<input title="browse data barang jadi" name="pilih_brg_1" value="..." type="button" id="pilih_brg_1" 
          onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg_jadi/1/');" > -->
          </div>
          </td>
        <!--  <td><input name="qty_1" type="text" id="qty_1" size="5" value="" /> </td> -->
        <td><div id="qtywarna_1" align="right">&nbsp;<input type="hidden" id="temp_qty_1" name="temp_qty_1" value=""></div>
        <input name="stok_1" type="hidden" id="stok_1" value="" />
			<hr>
			</td>
			
          <td><input name="ket_detail_1" type="text" id="ket_detail_1" size="30" value="" /></td>
          
        </tr>
	</table>	
	
	</form><br>
	<div align="center"><!--<input type="submit" name="submit2" value="Simpan" >-->
	<input type="button" name="simpan_data" id="simpan_data" value="Simpan" onclick="cek_bonm();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/bonmkeluar/cform/viewbonmkeluarcutting'"></div>

     </td>
    </tr>

</table>
</div>
</form>
