<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript">
$(document).ready(function() {
 
 $(".xhapus").click(function(){
 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var hps = element.attr("id");
 
 //Built a url to send
 var info = 'pid=' + hps;
 if(confirm("Yakin akan menghapus data dgn id: "+hps+" ?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('/jnssetorpajak/cform/actdelete')?>",
 data: info,
 success: function(){
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
 
})
</script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
 
          
<!--
cc = cacah
-->
<div id="tabs">
	<div id="tab1" class="tab_sel_jnssetorpajak" align="center" onClick="javascript: displayJnsSetorPajak('1');"><?php echo $form_panel_daftar_jsetor_pajak; ?></div>
<!--
	<div id="tab2" class="tab_jnssetorpajak" style="margin-left:1px;" align="center" onClick="javascript: displayJnsSetorPajak('2');"><?php echo $form_panel_form_jsetor_pajak; ?></div>
	<div id="tab3" class="tab_jnssetorpajak" style="margin-left:1px;" align="center" onClick="javascript: displayJnsSetorPajak('3');"><?php echo $form_panel_cari_jsetor_pajak; ?></div>
-->
</div>
   <a href="<?php echo base_url(); ?>index.php/jnssetorpajak/cform/tambah">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/jnssetorpajak/cform">View Data</a>&nbsp;<br><br>      
<div class="tab_bdr_jnssetorpajak"></div>
	<div class="panel" id="panel1" style="display:none;">
	<?php
	if( !empty($query) || isset($query) ) {
		$cc		= 1;
		foreach($query as $row){
			$Classnya = $cc % 2 == 0 ? "row" :"row1";
			$list .= "
			 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
			  <td>".$cc."</td>
			  <td>".$row->i_akun_pajak."&nbsp;</td>	 
			  <td>".$row->i_jsetor_pajak."&nbsp;</td>
			  <td>".$row->e_jsetor_pajak."&nbsp;</td>
			  <td align='center'><a href=".base_url()."index.php/jnssetorpajak/cform/edit/".$row->i_setoran.">
			  <img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;
			  <a href=".base_url()."index.php/jnssetorpajak/cform/actdelete/".$row->i_setoran.">
			  <img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13></a></td>
			 </tr>";
			 
			$cc+=1;
		}
	}
	?>

	<table class="listtable_jnssetorpajak">
	 <th width="5">No</th>
	 <th width="40"><?php echo $form_kode_jsetor_pajak; ?></th>
	 <th width="40"><?php echo $form_kode_kode_jsetor_pajak; ?></th>
	 <th width="100"><?php echo $form_jns_setoran; ?></th>
	 <th width="10" class="action">Aksi</th>
	<tbody>
	<?php
	if( !empty($list) && !isset($not_defined) ) {
		echo $list."</tbody></table>";
	} else {
		echo $list."</tbody></table>
		<div style=\"font-family:arial; font-size:11px; color:#FF0000;\">".strtoupper($not_defined)."</div>";
	}
	?>

	<table align="center" width="100%">
	  <tr>
	  	<td align="center"><?php echo $create_links; ?></span></td>
	  </tr>
	</table>	
	</div>

	<div class="panel" id="panel2" style="display: block">
	    <table>
	      <tr>
	    	<td align="left">
				 <?php
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('jnssetorpajak/cform/simpan', $attributes);?>
		    
		<div id="masterjsetorpajakform">
		      <table>
		      	<tr>
			  <td width="200px"><?php echo $form_kode_jsetor_pajak; ?></td>
			  <td width="1px">:</td>
			  <td width="280px">
			    <?php
			    $kodeakun = array(
				'name'=>'kodeakunpajak',
				'id'=>'kodeakunpajak',
				'onclick'	=>'ckodeakunpajak();',
				'value'=>'',
				'maxlength'=>'5');
			    echo form_input($kodeakun); ?>
			  </td>
			</tr>			  
		      	<tr>
			  <td><?php echo $form_kode_kode_jsetor_pajak; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    $kodejnssetor = array(
				'name'=>'kodejenissetor',
				'id'=>'kodejenissetor',
				'value'=>'',
				'maxlength'=>'5');
				echo form_input($kodejnssetor);
			    ?>		
			  </td>
			</tr>
		      	<tr>
			  <td><?php echo $form_jns_setoran; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    $uraian = array(
				'name'=>'uraianpembayaran',
				'id'=>'uraianpembayaran',
				'value'=>'',
				'maxlength'=>'100' );	
			    echo form_input($uraian); ?>		
			  </td>
			</tr>	
			<tr valign="top">
			  <td><?php echo $form_keterangan_jsetor_pajak; ?></td>
			  <td>:</td>
			  <td colspan="4">
				<?php
				 $ket = array(
					'name'=>'e_note',
					'id'=>'e_note',
					'value'=>'',
					'cols'=>'2',
					'rows'=>'1'
					);
				 echo form_textarea($ket);
				?>
			  </td>	
			</tr>					
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			    <input name="tblsimpanjnssetor" id="tblsimpanjnssetor" value="Simpan" type="submit">
			     <input name="btnbatal" id="tblreset" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/jnssetorpajak/cform/'">
			  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
	<div class="panel" id="panel3" style="display: none">
		<table>
			<tr>
			  <td align="left">
			    <?php echo $this->pquery->form_remote_tag(array('url'=>'jnssetorpajak/cform/cari','update'=>'#content','type'=>'post'));?>
				<table class="listtable">
				<thead>
			      	 <tr>
				   <td><?php echo $txt_cari; ?>:
				    <input type="text" name="txtcari" id="txtcari" value="" maxlength='50'><input type="submit" id="btncari" name="btncari" value="Pencarian">
					</td>
				 </tr>
				</thead>
				</table>
			  </td>
			</tr>
		</table>
	</div>
