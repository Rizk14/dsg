<!--
cc = cacah
-->
<div id="tabs">
	<div id="tab2" class="tab_jnssetorpajak" style="margin-left:1px;" align="center" onClick="javascript: displayJnsSetorPajak('2');"><?php echo $form_panel_form_jsetor_pajak; ?></div>
</div>
         
<div class="tab_bdr"></div>

	<div class="panel" id="panel2" style="display:block;">
	    <table class="maintable">
	      <tr>
	    	<td align="left">
		     <?php
			
		    $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('jnssetorpajak/cform/actedit', $attributes);?>
		<div id="masterjsetorpajakform">
		<div class="effect">
		  <div class="accordion2">
		      <table class="mastertable">
		      	<tr>
			  <td width="200px"><?php echo $form_kode_jsetor_pajak; ?></td>
			  <td width="1px">:</td>
			  <td width="280px">
			    <?php
			    $kodeakun = array(
				'name'=>'kodeakunpajak',
				'id'=>'kodeakunpajak',
				'onclick'	=>'ckodeakunpajak();',
				'value'=>$iakunpajak,
				'maxlength'=>'5');
			    echo form_input($kodeakun); ?>
			  </td>
			</tr>			  
		      	<tr>
			  <td><?php echo $form_kode_kode_jsetor_pajak; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    $kodejnssetor = array(
				'name'=>'kodejenissetor',
				'id'=>'kodejenissetor',
				'value'=>$ijsetorpajak,
				'maxlength'=>'5');
				echo form_input($kodejnssetor);
			    ?>		
			  </td>
			</tr>
		      	<tr>
			  <td><?php echo $form_jns_setoran; ?></td>
			  <td>:</td>
			  <td>
			    <?php
			    $uraian = array(
				'name'=>'uraianpembayaran',
				'id'=>'uraianpembayaran',
				'value'=>$ejsetorpajak,
				'maxlength'=>'100' );	
			    echo form_input($uraian); ?>		
			  </td>
			</tr>	
			<tr valign="top">
			  <td><?php echo $form_keterangan_jsetor_pajak; ?></td>
			  <td>:</td>
			  <td colspan="4">
				<?php
				 $ket = array(
					'name'=>'e_note',
					'id'=>'e_note',
					'value'=>$keterangan,
					'cols'=>'2',
					'rows'=>'1'
					);
				 echo form_textarea($ket);
				?>
			  </td>	
			</tr>	
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			  	<input type="hidden" name="isetoran" id="isetoran" value="<?php echo $isetoran; ?>" />
			    <input name="tblsimpanjnssetor" id="tblsimpanjnssetor" value="Simpan" type="submit">
			   <input name="tblreset" id="tblreset" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/jnssetorpajak/cform/'">

			  </td>
		       </tr>
		      </table>
			</div>
		</div>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
