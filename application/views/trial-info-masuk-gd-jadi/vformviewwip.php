<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Masuk Barang WIP Gudang Jadi</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('trial-info-masuk-gd-jadi/cform/export_excel_wip', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="68%">
	<thead height='35x' rowspan='1'>
	 <tr class="judulnya">
		 		
		<th width='3%' ><font size='4'>No</font></th>
		<th width='5%' ><font size='4'>Kode</font></th>
		<th width='30%'><font size='4'>Nama Barang WIP</font></th>
		<th width='8%' ><font size='4'>Quantity</font></th>
	
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				 for($j=0;$j<count($query);$j++){
				 
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center' >".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 $jum_saldo_masuk = $query[$j]['jum_keluar2']+$query[$j]['jum_masuk'];
				 echo    "<td align='right'>".number_format($jum_saldo_masuk,0,',','.')."&nbsp;</td>";
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
