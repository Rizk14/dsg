<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#b8b8c6;
	}
</style>

</head>
<body id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<h3><center>Trial/Error Laporan Mutasi Gudang QC</center></h3>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
 
  <tr>
    <td align="left">
	<?php echo form_open('info-mutasi-stok2/cformbaru/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

<h4><b>Periode:</b> <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br></h4> 


<tr>
	
<td><input type="hidden" name="date_from" value="<?php echo $date_from ?>" ></td>
<td><input type="hidden" name="date_to" value="<?php echo $date_to ?>" ></td>

</tr>

	<thead>
<table class="table table-bordered" align="center" style="width:100%;" >
	<thead>
	 <tr class="judulnya">
		 <th width='1%' rowspan='2'>No</th>
		 <th width='3%' rowspan='2'>Kode</th>
		 <th width='13%' rowspan='2'>Nama Brg WIP</th>
		 <!--<th width='8%' rowspan='2'>Satuan</th>-->
		 <th width='3%' rowspan='2'>Saldo<br>Awal</th>
		 <th colspan='6'>Masuk</th>
		 <th colspan='5'>Keluar</th>
		 <th width='3%' rowspan='2'>Saldo Akhir</th>
		 <th width='3%' rowspan='2'>Stok Opname</th>
		 <th width='3%' rowspan='2'>Selisih</th>
		
	 </tr>
	 <tr class="judulnya">
			<th width='3%'>Bgs</th>
			<th width='3%'>Hsl <br> Perbaikan</th>
			 <th width='3%'>Retur <br>Unit<br> Packing</th>
			 <th width='3%'>Retur <br>Gdg <br>Jadi</th>
			 <th width='3%'>Lain2</th>
			 <th width='3%'>Total</th>
			 <th width='3%'>Bgs Packing</th>
			 <th width='3%'>Bgs Gdg Jadi</th>
			 <th width='3%'>Retur Perbaikan</th>
			 <th width='3%'>Lain2</th>
			 <th width='3%'>Total</th>
	 </tr>
	</thead>
	<tbody>



		<?php

		$i=0;
		$gtotsaldoawal=0;
		$gmasuk_bgs=0;
		$gmasuk_prbaikn=0;
		$gmasuk_rtrpacking=0;
		$gmasuk_rtrgdjdi=0;
		$gmasuk_lainother=0;
		$gtot_masuk=0;
		$gkeluar_bgspcking=0;
		$gkeluar_gdjdi=0;
		$gkeluar_rtrprbaikan=0;
		$gkeluar_lain=0;
		$gtot_keluar=0;
		$gsaldo_akhir=0;
		$gstokopname=0;
		$gselisih=0;

		if($query){
		foreach ($query as $row){
			$i++;
				 echo "<tr class=\"record\">";
				 echo   "<td>$i</td>";
				 echo   "<td>$row->kode_brg</td>";
				 echo   "<td>$row->nama_brg</td>";
				 echo   "<td>$row->saldo_awal</td>";
				 echo   "<td>$row->masuk_bgs</td>";
				 echo   "<td>$row->masuk_prbaikn</td>";
				 echo   "<td>$row->masuk_rtrpacking</td>";
				 echo   "<td>$row->masuk_rtrgdjdi</td>";
				 //echo   "<td>$row->masuk_lain</td>";
				 echo   "<td>$row->masuk_lainother</td>";
				 echo   "<td>$row->tot_masuk</td>";
				 echo   "<td>$row->keluar_bgspcking</td>";
				 echo   "<td>$row->keluar_gdjdi</td>";
				 echo   "<td>$row->keluar_rtrprbaikan</td>";
				 echo   "<td>$row->keluar_lain</td>";
				 //echo   "<td>$row->keluar_other</td>";
				 echo   "<td>$row->tot_keluar</td>";
				 echo   "<td>$row->saldo_akhir</td>";
				 echo   "<td>$row->stokopname</td>";
				 echo   "<td>$row->selisih</td>";
				 echo "</tr>";

			   $gtotsaldoawal=$gtotsaldoawal+$row->saldo_awal;
	           $gmasuk_bgs=$gmasuk_bgs+$row->masuk_bgs;
	           $gmasuk_prbaikn=$gmasuk_prbaikn+$row->masuk_prbaikn;
	           $gmasuk_rtrpacking=$gmasuk_rtrpacking+$row->masuk_rtrpacking;
	           $gmasuk_rtrgdjdi=$gmasuk_rtrgdjdi+$row->masuk_rtrgdjdi;
	           $gmasuk_lainother=$gmasuk_lainother+$row->masuk_lainother;
	           $gtot_masuk=$gtot_masuk+$row->tot_masuk;
	           $gkeluar_bgspcking=$gkeluar_bgspcking+$row->keluar_bgspcking;
	           $gkeluar_gdjdi=$gkeluar_gdjdi+$row->keluar_gdjdi;
	           $gkeluar_rtrprbaikan=$gkeluar_rtrprbaikan+$row->keluar_rtrprbaikan;
	           $gkeluar_lain=$gkeluar_lain+$row->keluar_lain;
	           $gtot_keluar=$gtot_keluar+$row->tot_keluar;
	           $gsaldo_akhir=$gsaldo_akhir+$row->saldo_akhir;
	           $gstokopname=$gstokopname+$row->stokopname;
	           $gselisih=$gselisih+$row->selisih;
				
				 }
				 echo "<tr>";
              	  	echo " <td colspan=3><b>TOTAL</td>
                  	<td><b>$gtotsaldoawal</b></td>
                  	<td><b>$gmasuk_bgs</b></td>
                  	<td><b>$gmasuk_prbaikn</b></td>
                  	<td><b>$gmasuk_rtrpacking</b></td>
                  	<td><b>$gmasuk_rtrgdjdi</b></td>
                  	<td><b>$gmasuk_lainother</b></td>
                  	<td><b>$gtot_masuk</b></td>
                  	<td><b>$gkeluar_bgspcking</b></td>
                  	<td><b>$gkeluar_gdjdi</b></td>
                  	<td><b>$gkeluar_rtrprbaikan</b></td>
                  	<td><b>$gkeluar_lain</b></td>
                  	<td><b>$gtot_keluar</b></td>
                  	<td><b>$gsaldo_akhir</b></td>
                  	<td><b>$gstokopname</b></td>
                  	<td><b>$gselisih</b></td>";
              	echo "</tr>";
				}
		?>
	

 	</tbody>
</table>
  	</div>
<br>
<center><span style="font-size: 14px; margin-top: 1px;"><?php echo $this->pagination->create_links(); ?></span></center>
<input name="btnbatal" id="btnbatal" value="Kembali" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/info-mutasi-stok2/cform/wip'">
<input type="button" name="export_excel7" id="export_excel7"  value="Export All" onclick="window.location='<?php echo base_url(); ?>index.php/info-mutasi-stok2/cform/export_excel_mutasi3/<?php echo $date_from ?>/<?php echo $date_to ?>/<?php echo $gudang ?>/'">

  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
