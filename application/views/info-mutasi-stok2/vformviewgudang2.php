<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Mutasi Stok Bahan Baku/Pembantu</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php if ($gudang != '0') { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else { echo "Semua"; } ?><br>
Jenis Laporan: <?php echo $nama_jenis ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok2/cform/export_excel', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >

<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >

<input type="hidden" name="format_harga" value="<?php echo $format_harga ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Kode Brg</th>
		 <th>Nama Barang</th>
		 <th>Sat Awal</th>
		 <?php 
		 	/*$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('username');
			if (($id == 'gudang1') or ($id == 'gudang2') ($id == 'gudang3')) {
			echo '
		 		 <th width="5%">Saldo Awal</th>
				 <th width="5%">Masuk</th>
				 <th width="5%">Masuk Lain</th>
				 <th width="5%">Keluar</th>
				 <th width="5%">Keluar Lain</th>
				 <th width="5%">Saldo Akhir</th>
				 <th width="5%">SO</th>
				 <th width="5%">Selisih</th>';
			}else{
				echo '<th>Satuan Konversi</th>
		 		 <th width="5%">Saldo Awal</th>
				 <th width="5%">Masuk</th>
				 <th width="5%">Masuk Lain</th>
				 <th width="5%">Keluar</th>
				 <th width="5%">Keluar Lain</th>
				 <th width="5%">Saldo Akhir</th>
				 <th width="5%">SO</th>
				 <th width="5%">Selisih</th>';
			}*/
		 ?>
		 <th>Satuan Konversi</th>
 		 <th width="5%">Saldo Awal</th>
		 <th width="5%">Masuk</th>
		 <th width="5%">Masuk Lain</th>
		 <th width="5%">Keluar</th>
		 <th width="5%">Keluar Lain</th>
		 <th width="5%">Saldo Akhir</th>
		 <th width="5%">SO</th>
		 <th width="5%">Selisih</th>
	 </tr>
	</thead>
	<tbody>
		 <?php 
		 if(sizeof($query)>0) {
		 	$i=0;
			foreach($query as $row) {
				$i++;
					echo "<tr>";

					echo "<td>$i</td>";
					echo "<td>$row->kode_brg</td>";
					echo "<td>$row->nama_brg</td>";
					echo "<td>$row->satuan</td>";
					echo "<td>$row->satuan_konversi</td>";
					echo "<td>".number_format($row->saldo_awal,2,',','.')."</td>";
					echo "<td>".number_format($row->jum_masuk,2,',','.')."</td>";
					echo "<td>".number_format($row->jum_masuklain,2,',','.')."</td>";
					echo "<td>".number_format($row->jum_keluar,2,',','.')."</td>";
					echo "<td>".number_format($row->jum_keluarlain,2,',','.')."</td>";
					echo "<td>".number_format($row->saldo_akhir,2,',','.')."</td>";
					echo "<td>".number_format($row->so,2,',','.')."</td>";
					echo "<td>".number_format($row->selisih,2,',','.')."</td>";
					echo "</tr>";
			 		  


			}
		}
		 ?>
 	</tbody>
</table><br>
</div>
