<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Stok Barang WIP (Hasil Jahit) Gudang QC</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel_wip_w', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width='3%' rowspan='2'>No</th>
		 <th width='15%' rowspan='2'>Kode</th>
		 <th width='25%' rowspan='2'>Nama Brg WIP</th>
		 <!--<th width='8%' rowspan='2'>Satuan</th>-->
		 <th width='8%' rowspan='2'>Saldo<br>Awal</th>
		 <th colspan='6'>Masuk</th>
		 <th colspan='5'>Keluar</th>
		 <th width='8%' rowspan='2'>Saldo Akhir</th>
		 <th width='8%' rowspan='2'>Stok Opname</th>
		  <th width='8%' rowspan='2'>Adjustment</th>
		 <th width='8%' rowspan='2'>Selisih</th>
		
	 </tr>
	 <tr class="judulnya">
		<th width='8%'>Bgs</th>
		<th width='8%'>Hsl Perbaikan</th>
			 <th width='8%'>Retur Unit Packing</th>
			 <th width='8%'>Retur Gdg Jadi</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Total</th>
			 <th width='8%'>Bgs Packing</th>
			 <th width='8%'>Bgs Gdg Jadi</th>
			 <th width='8%'>Retur Perbaikan</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Total</th>
	 </tr>
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
		 ?>
					<tr>
						<td colspan="19">&nbsp;<b><?php echo $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ?></b></td>
					</tr>
		 <?php 
				 }
				 /*
				if ($query[$j]['jum_masukback'] == 0  ) {
					
						echo "<tr class=\"record\" bgcolor='#00ff00'>";
				 }
				 elseif ($query[$j]['jum_masukback'] == 0 && $query[$j]['jum_perb_unitback'] == 0 ) {
					
						echo "<tr class=\"record\" bgcolor='#FF0000'>";
				 }
				 else
				 */
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 //echo    "<td>Pieces</td>";
			 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['saldo_sa_warna'])) {
					$detailwarna = $query[$j]['saldo_sa_warna'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['saldo_awal']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['saldo_awal'],0,',','.')." &nbsp;</td>";
			
				  echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjmb'])) {
					$detailwarna = $query[$j]['data_warna_sjmb'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_masuk_bagus_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_masuk'],0,',','.')." &nbsp;</td>";
				
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjmp'])) {
					$detailwarna = $query[$j]['data_warna_sjmp'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_masuk_perbaikan_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_perb_unit'],0,',','.')." &nbsp;</td>";
				
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjmr'])) {
					$detailwarna = $query[$j]['data_warna_sjmr'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_masuk_retpack_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_ret_unit_pack'],0,',','.')." &nbsp;</td>";
				
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjmrg'])) {
					$detailwarna = $query[$j]['data_warna_sjmrg'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_masuk_retgdjd_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_ret_gd_jadi'],0,',','.')." &nbsp;</td>";
				
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjmll'])) {
					$detailwarna = $query[$j]['data_warna_sjmll'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_masuk_retdll_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_masuk_lain'],0,',','.')." &nbsp;</td>";
				
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['totjum_masuk_warna_total'])) {
					$detailwarna = $query[$j]['totjum_masuk_warna_total'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_masuk_warna_total']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_masuk_total'],0,',','.')." &nbsp;</td>";
		
			
			 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjkp'])) {
					$detailwarna = $query[$j]['data_warna_sjkp'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_keluar_bagus_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_keluar_pack'],0,',','.')." &nbsp;</td>";
		
		 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjkgj'])) {
					$detailwarna = $query[$j]['data_warna_sjkgj'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_keluar_gdjadi_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_keluar_gdjadi'],0,',','.')." &nbsp;</td>";
		
		 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjkr'])) {
					$detailwarna = $query[$j]['data_warna_sjkr'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_keluar_retper_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_keluar_perb'],0,',','.')." &nbsp;</td>";
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['data_warna_sjkll'])) {
					$detailwarna = $query[$j]['data_warna_sjkll'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_keluar_dll_warna']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_keluar_lain'],0,',','.')." &nbsp;</td>";
				 
				
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['totjum_keluar_warna_total'])) {
					$detailwarna = $query[$j]['totjum_keluar_warna_total'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_keluar_warna_total']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_keluar_total'],0,',','.')." &nbsp;</td>";
				 
				 $selisih = $query[$j]['jum_stok_opname']-$query[$j]['jum_saldo_akhir']+$query[$j]['jum_adjustment'];
				  
				 
				 
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['totjum_warna_total'])) {
					$detailwarna = $query[$j]['totjum_warna_total'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['jum_warna_total']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_saldo_akhir'],0,',','.')." &nbsp;</td>";
				
				 
				 echo    "	 <td style='white-space:nowrap;'>";
				if (is_array($query[$j]['saldo_so_warna'])) {
					$detailwarna = $query[$j]['saldo_so_warna'];
					for($z=0;$z<count($detailwarna);$z++){
						echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['saldo_so_tot']."<br>";
					}
				}
				echo "Total: ".number_format($query[$j]['jum_stok_opname'],0,',','.')." &nbsp;</td>";
				
				echo    "<td align='center'>".number_format($query[$j]['jum_adjustment'],0,',','.')."&nbsp;</td>";
				//~ 
				//~ echo    "	 <td style='white-space:nowrap;'>";
				//~ if (is_array($query[$j]['totselisih_warna'])) {
					//~ $detailwarna = $query[$j]['totselisih_warna'];
					//~ for($z=0;$z<count($detailwarna);$z++){
						//~ echo $detailwarna[$z]['nama_warna'].": ".$detailwarna[$z]['selisih_warna']."<br>";
					//~ }
				//~ }
				
				echo    "	 <td style='white-space:nowrap;'>";
				//~ if (is_array($query[$j]['totjum_warna_total'] && $query[$j]['saldo_so_warna'])) {
					$detailwarnasa = $query[$j]['totjum_warna_total'];
					$detailwarnaso = $query[$j]['saldo_so_warna'];
					
					for($z=0;$z<count($detailwarnaso);$z++){
						$array_push = array_push($array_push, $detailwarnaso[$z]['saldo_so_tot']); 
					}
					print_r($array_push);
				//~ }
				echo "Total: ".number_format($selisih,0,',','.')." &nbsp;</td>";
				
				 
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
