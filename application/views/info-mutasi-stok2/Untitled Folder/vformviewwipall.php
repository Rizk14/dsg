<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Stok Barang WIP (Hasil Jahit) Gudang QC</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel_wip', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width='3%' rowspan='2'>No</th>
		 <th width='5%' rowspan='2'>Kode</th>
		 <th width='25%' rowspan='2'>Nama Brg WIP</th>
		 <th width='5%' rowspan='2'>Stok Opname</th>
		 <th colspan="5" align="center"><center>Gudang</center> </th>
		
	 </tr>

	 <tr>
	 <th style="width: 5%;"> Cover </th>
	 <th style="width: 5%;"> Plastik </th>
	 <th style="width: 5%;"> Dus </th>
	 <th style="width: 5%;"> Busa </th>
	 <th style="width: 5%;"> Hdp </th>
	 </tr>
	
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
		 ?>
					<tr>
						<td colspan="23">&nbsp;<b><?php echo $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ?></b></td>
					</tr>
		 <?php 
				 }
				 /*
				if ($query[$j]['jum_masukback'] == 0  ) {
					
						echo "<tr class=\"record\" bgcolor='#00ff00'>";
				 }
				 elseif ($query[$j]['jum_masukback'] == 0 && $query[$j]['jum_perb_unitback'] == 0 ) {
					
						echo "<tr class=\"record\" bgcolor='#FF0000'>";
				 }
				 else
				 */
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";		 
				 
				 echo    "<td align='right'>".number_format($query[$j]['jum_stok_opname'],0,',','.')."&nbsp;</td>";
				
				 
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
