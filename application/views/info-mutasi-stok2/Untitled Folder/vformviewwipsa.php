<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Stok Barang WIP (Hasil Jahit) Gudang QC</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel_wip', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	<tr class="judulnya">
		<th width='3%' rowspan='2'>No</th>
		<th width='15%' rowspan='2'>Kode</th>
		<th width='25%' rowspan='2'>Nama Brg WIP</th>
		<!--<th width='8%' rowspan='2'>Satuan</th>-->
		<th width='8%' rowspan='2'>Saldo<br>Awal</th>
		<th colspan='8'>Masuk</th>
		<th colspan='7'>Keluar</th>
		<th width='8%' rowspan='2'>Saldo Akhir</th>
		<th width='8%' rowspan='2'>Stok Opname</th>
		  <th width='8%' rowspan='2'>Adjustment</th>
		<th width='8%' rowspan='2'>Selisih</th>
		
	</tr>
	<tr class="judulnya">
		<th width='8%'>Bgs</th>
		<th width='8%'>Hsl Perbaikan</th>
			<th width='8%'>Retur Unit Packing</th>
			<th width='8%'>Retur Gdg Jadi</th>
			<th width='8%'>Lain2</th>
			  <th width='8%'>Bagus QC Lain</th>
			  <th width='8%'>Retur QC Lain</th>
			<th width='8%'>Total</th>
			<th width='8%'>Bgs Packing</th>
			<th width='8%'>Bgs Gdg Jadi</th>
			<th width='8%'>Retur Perbaikan</th>
			<th width='8%'>Lain2</th>
			<th width='8%'>Bagus QC Lain</th>
			  <th width='8%'>Retur QC Lain</th>
			<th width='8%'>Total</th>
	</tr>
	</thead>
	<tbody>
			
<?php

$totsaldoawal=0;
$msbgs=0;
$mshslperbaikan=0;
$msreturupack=0;
$msreturgdj=0;
$mslain=0;
$msbqcl=0;
$msrqcl=0;
$mstotal=0;
$klbgs=0;
$klbgsgdj=0;
$klrper=0;
$kllain=0;
$klbqcl=0;
$klrqcl=0;
$kltotal=0;
$sakhir=0;
$saop=0;
$adjust=0;
$sel=0;

	if (is_array($query)) {
		$temp_kodekel = "";


		for($j=0;$j<count($query);$j++){


		

			if ($temp_kodekel == "") {

				
					$temp_kodekel = $query[$j]['kode_kel'];

					
		

					}


					if ($temp_kodekel != $query[$j]['kode_kel']) {
					
					
					$temp_kodekel = $query[$j]['kode_kel'];
					

			echo  "<tr>";
				echo "<td align='center' colspan='3'><b>TOTAL</b></td>";
				echo    "<td align='right'>".number_format($totsaldoawal,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msbgs,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($mshslperbaikan,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msreturupack,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msreturgdj,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($mslain,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msbqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msrqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($mstotal,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klbgs,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klbgsgdj,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klrper,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($kllain,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klbqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klrqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($kltotal,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($sakhir,2,',','.')."</td>";	
				echo 	"<td align='right'>".number_format($saop,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($adjust,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($sel,2,',','.')."</td>";	
								

		echo  "</tr>";

			?>
					<tr>
						<td colspan="23">&nbsp;<b><?php echo $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ?></b></td>
					</tr>

		<?php 

		$totsaldoawal=0;
		$msbgs=0;
		$mshslperbaikan=0;
		$msreturupack=0;
		$msreturgdj=0;
		$mslain=0;
		$msbqcl=0;
		$msrqcl=0;
		$mstotal=0;
		$klbgs=0;
		$klbgsgdj=0;
		$klrper=0;
		$kllain=0;
		$klbqcl=0;
		$klrqcl=0;
		$kltotal=0;
		$sakhir=0;
		$saop=0;
		$adjust=0;
		$sel=0;

}

				echo "<tr class=\"record\">";
				echo    "<td width='2%' align='center'>".($j+1)."</td>";
				echo    "<td>".$query[$j]['kode_brg']."</td>";
				echo    "<td>".$query[$j]['nama_brg']."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['saldo_awal'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_masuk'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_perb_unit'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_ret_unit_pack'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_ret_gd_jadi'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_masuk_lain'],0,',','.')."</td>";
				 
				echo    "<td align='center'> ".number_format($query[$j]['jum_masuk_bgs_qc'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_masuk_rtr_qc'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_masuk_total'],0,',','.')."</td>";
				 
				echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_pack'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_gdjadi'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_perb'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_lain'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_bgs_qc'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_rtr_qc'],0,',','.')."</td>";
				echo    "<td align='center'> ".number_format($query[$j]['jum_keluar_total'],0,',','.')."</td>";
				 
				 
				$selisih = $query[$j]['jum_stok_opname']-$query[$j]['jum_saldo_akhir']+$query[$j]['jum_adjustment'];
				echo   "<td align='right'>".number_format($query[$j]['jum_saldo_akhir'],0,',','.')."&nbsp;</td>";
				echo    "<td align='right'>".number_format($query[$j]['jum_stok_opname'],0,',','.')."&nbsp;</td>";
				echo    "<td align='center'>".number_format($query[$j]['jum_adjustment'],0,',','.')."&nbsp;</td>";
				echo    "<td align='right'>".number_format($selisih,0,',','.')."&nbsp;</td>";
				 
				echo  "</tr>";	

			$totsaldoawal=$totsaldoawal+$query[$j]['saldo_awal'];
			$msbgs=$msbgs+$query[$j]['jum_masuk'];
			$mshslperbaikan=$mshslperbaikan+$query[$j]['jum_perb_unit'];
			$msreturupack=$msreturupack+$query[$j]['jum_ret_unit_pack'];
			$msreturgdj=$msreturgdj+$query[$j]['jum_ret_gd_jadi'];
			$mslain=$mslain+$query[$j]['jum_masuk_lain'];
			$msbqcl=$msbqcl+$query[$j]['jum_masuk_bgs_qc'];
			$msrqcl=$msrqcl+$query[$j]['jum_masuk_rtr_qc'];
			$mstotal=$mstotal+$query[$j]['jum_masuk_total'];
			$klbgs=$klbgs+$query[$j]['jum_keluar_pack'];
			$klbgsgdj=$klbgsgdj+$query[$j]['jum_keluar_gdjadi'];
			$klrper=$klrper+$query[$j]['jum_keluar_perb'];
			$kllain=$kllain+$query[$j]['jum_keluar_lain'];
			$klbqcl=$klbqcl+$query[$j]['jum_keluar_bgs_qc'];
			$klrqcl=$klrqcl+$query[$j]['jum_keluar_rtr_qc'];
			$kltotal=$kltotal+$query[$j]['jum_keluar_total'];
			$sakhir=$sakhir+$query[$j]['jum_saldo_akhir'];
			$saop=$saop+$query[$j]['jum_stok_opname'];
			$adjust=$adjust+$query[$j]['jum_adjustment'];
			$sel=$adjust+$selisih;

}
			echo  "<tr>";
				echo "<td align='center' colspan='3'>TOTAL</td>";
				echo    "<td align='right'>".number_format($totsaldoawal,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msbgs,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($mshslperbaikan,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msreturupack,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msreturgdj,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($mslain,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msbqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($msrqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($mstotal,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klbgs,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klbgsgdj,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klrper,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($kllain,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klbqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($klrqcl,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($kltotal,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($sakhir,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($saop,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($adjust,2,',','.')."</td>";
				echo 	"<td align='right'>".number_format($sel,2,',','.')."</td>";


			echo  "</tr>";
		   }
		?>
 	</tbody>
</table><br>
</div>