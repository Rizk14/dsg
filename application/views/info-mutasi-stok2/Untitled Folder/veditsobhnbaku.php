<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Informasi Stok Opname Bahan Baku/Pembantu</h3><br><br>

<div>
Lokasi Gudang: <?php echo "[".$query[0]['nama_lokasi']."] ".$query[0]['kode_gudang']." - ".$query[0]['nama_gudang'] ?><br>
Periode: <?php echo $query[0]['nama_bln']." ".$query[0]['tahun'] ?><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('info-mutasi-stok/cform/updatesobhnbaku', $attributes);
 if ($ctahun == '') $ctahun = "0";
 $url_redirectnya = "info-mutasi-stok/cform/viewsobhnbaku/".$cid_gudang."/".$cbulan."/".$ctahun."/".$cur_page;
 ?>
<input type="hidden" name="jum_data" id="jum_data" value="<?php echo $jum_total ?>">
<input type="hidden" name="id_so" id="id_so" value="<?php echo $id_so ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $query[0]['bulan'] ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $query[0]['tahun'] ?>">
<input type="hidden" name="id_gudang" id="id_gudang" value="<?php echo $query[0]['id_gudang'] ?>">

<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="cid_gudang" value="<?php echo $cid_gudang ?>">
<input type="hidden" name="cbulan" value="<?php echo $cbulan ?>">
<input type="hidden" name="ctahun" value="<?php echo $ctahun ?>">

Tanggal Pencatatan SO: 
      <input name="tgl_so" type="text" id="tgl_so" size="10" value="<?php echo $query[0]['tgl_so'] ?>" readonly="true">
	   <img alt="" id="tgl_so" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_so,'dd-mm-yyyy',this)">
<br><br>

<input type="submit" name="submit" value="Update Stok Opname" onclick="return confirm('Apakah anda yakin simpan data ini? Proses simpan ini otomatis mengupdate stok');">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang</th>
		 <th>Satuan Awal</th>
		 <th>Satuan Konversi</th>
		 <th>Jumlah Fisik Satuan Konversi</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg']."
					 <input type='hidden' name='id_brg_$i' id='id_brg_$i' value='".$query[$j]['id_brg']."'>
					 <input type='hidden' name='id_$i' id='id_$i' value='".$query[$j]['id']."'>
					 </td>";
					 echo    "<td>".$query[$j]['nama_brg']."</td>";
					 echo    "<td>".$query[$j]['satuan']."</td>";
					 echo    "<td>".$query[$j]['nama_satuan_konv']."</td>";
					 
					 echo "<input type='hidden' name='auto_saldo_akhir_$i' id='auto_saldo_akhir_$i' value='".$query[$j]['auto_saldo_akhir']."'>";
					 echo "<td style='white-space:nowrap;' align='center'>";
					?>
					<input type="text" name="stok_fisik_<?php echo $i ?>" value="<?php echo $query[$j]['stok_opname'] ?>" size="5" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
					<input type="hidden" name="stok_<?php echo $i ?>" value="<?php echo $query[$j]['saldo_akhir'] ?>">
					<?php
					 echo "</td>";
					// utk brg baru yg blm ada di SO
		

					/*echo "<td style='white-space:nowrap;' align='right'>";
					if (is_array($query[$j]['detail_harga'])) {
						$detailharga = $query[$j]['detail_harga'];
							for($zz=0;$zz<count($detailharga);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo number_format($detailharga[$zz]['harga'],2,',','.')."&nbsp;"; */
					?>
				<!--	<input type="text" name="stok_fisik_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['stok_opname'] ?>" size="3" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
					<input type="hidden" name="harga_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['harga'] ?>">
					<input type="hidden" name="stok_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['saldo_akhir'] ?>">
					<input type="hidden" name="auto_saldo_akhir_harga_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['auto_saldo_akhir_harga'] ?>">
					<br> -->
					<?php 
						//	}
					?>
					<!--	<input type="hidden" name="ada_harga_<?php echo $i ?>" id="ada_harga_<?php echo $i ?>" value="ada"> -->
					<?php
					//}
					//else {
					?>
						<!--<input type="hidden" name="ada_harga_<?php echo $i ?>" id="ada_harga_<?php echo $i ?>" value="">-->
					<?php //} ?>
					</td>
					 
					 <?php
					 echo  "</tr>";
					 $i++;
				}
			}
				if (is_array($querybrgbaru)) {
				for($j=0;$j<count($querybrgbaru);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$querybrgbaru[$j]['kode_brg']." <b>*</b> 
					 <input type='hidden' name='id_brg_$i' id='id_brg_$i' value='".$querybrgbaru[$j]['id_brg']."'>
					 <input type='hidden' name='id_$i' id='id_$i' value='".$querybrgbaru[$j]['id']."'>
					 </td>";
					 echo    "<td>".$querybrgbaru[$j]['nama_brg']."</td>";
					 echo    "<td>".$querybrgbaru[$j]['satuan']."</td>";
					 echo    "<td>".$querybrgbaru[$j]['nama_satuan_konv']."</td>";
					 echo "<td style='white-space:nowrap;' align='center'>";
					?>
					<input type="text" name="stok_fisik_<?php echo $i ?>" value="<?php echo $querybrgbaru[$j]['stok_opname'] ?>" size="5" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
					<input type="hidden" name="stok_<?php echo $i ?>" value="<?php echo $querybrgbaru[$j]['saldo_akhir'] ?>">
					<?php
					 echo "</td>";
					
					?>
					 
					 <?php
					 echo  "</tr>";
					 $i++;
				}
			}
			
		 ?>
		
 	</tbody>
</table><br>
Keterangan: <i>*) Barang baru yang belum ada di stok opname</i>

<?php echo form_close();  ?>
</div>
