<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Input Stok Opname Bahan Baku/Pembantu Berdasarkan Harga Manual</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function tambahharga(posisi) {
	//inisialisasi untuk id unik
		var no=$("#jum_data_"+posisi).val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku_"+posisi+" tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		
		//*****harga*************************************
		var harga="#harga_"+posisi+"_"+n;
		var new_harga="#harga_"+posisi+"_"+no;
		$(harga, lastRow).attr("id", "harga_"+posisi+"_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+posisi+"_"+no);
		$(new_harga, lastRow).val('0');
		//*****end harga*********************************
												
		//*****is_pkp*************************************
		var is_pkp="#is_pkp_"+posisi+"_"+n;
		var new_is_pkp="#is_pkp_"+posisi+"_"+no;
		$(is_pkp, lastRow).attr("id", "is_pkp_"+posisi+"_"+no);
		$(new_is_pkp, lastRow).attr("name", "is_pkp_"+posisi+"_"+no);
		$(new_is_pkp, lastRow).attr("checked", false);
		//*****end is_pkp*********************************
								
		//*****stok_harga*************************************
		var stok_harga="#stok_harga_"+posisi+"_"+n;
		var new_stok_harga="#stok_harga_"+posisi+"_"+no;
		$(stok_harga, lastRow).attr("id", "stok_harga_"+posisi+"_"+no);
		$(new_stok_harga, lastRow).attr("name", "stok_harga_"+posisi+"_"+no);
		$(new_stok_harga, lastRow).val('0');
		//*****end stok_harga*********************************
												
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku_"+posisi).append(lastRow);
		//jum_data++
		var x=parseInt(no);
		$("#jum_data_"+posisi).val(x+1);
}

function hapusharga(posisi) {
	var x= $("#jum_data_"+posisi).val();	
		if (x>2) {
			$("#tabelku_"+posisi+" tr:last").remove();	 
			var x= $("#jum_data_"+posisi).val();	
			var y= x-1;
			$("#jum_data_"+posisi).val(y);	
		}
}
</script>

<div>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('info-mutasi-stok/cform/submitsoharga', $attributes);
 ?>
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="gudang" id="gudang" value="<?php echo $gudang ?>">
<input type="hidden" name="jum_total" id="jum_total" value="<?php echo $jum_total ?>">

Tanggal Pencatatan SO: <?php echo $tgl_so ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang</th>
		 <th>Satuan Awal</th>
		 <th>Satuan Konversi</th>
		 <th>Jumlah Fisik Total<br>Satuan Konversi</th>
		 <th>Jumlah Fisik Per Harga<br>Satuan Konversi</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg'].
					 "<input type='hidden' name='id_detail_$i' id='id_detail_$i' value='".$query[$j]['id']."'></td>";
					 echo    "<td>".$query[$j]['nama_brg']."</td>";
					 echo    "<td>".$query[$j]['nama_satuan']."</td>";
					 echo    "<td>".$query[$j]['nama_satuan_konv']."</td>";
					 echo    "<td align='right'>".$query[$j]['stok_opname']."</td>";
					 
					 $var_detail = $query[$j]['detail_harga'];
				?>
				<td style="white-space:nowrap;">
					<input type="hidden" name="jum_data_<?php echo $i ?>" id="jum_data_<?php echo $i ?>" value="<?php if(is_array($var_detail)) echo count($var_detail)+1; else echo '2'; ?>">
					<input id="additem" type="button" name="additem" value=" + " title="Tambah Harga" onclick="tambahharga(<?php echo $i ?>)">&nbsp;
					<input id="deleteitem" type="button" name="deleteitem" value=" - " title="Hapus Harga" onclick="hapusharga(<?php echo $i ?>)">
					<br><br>
					<table border="0" cellpadding= "1" cellspacing = "1" width="100%" id="tabelku_<?php echo $i ?>">
					<?php 
					 $z=1;
					 if (is_array($var_detail)) {
						for($k=0;$k<count($var_detail);$k++){
					 ?>
					<tr>
						<td>Harga <input type="text" name="harga_<?php echo $i ?>_<?php echo $z ?>" style="text-align:right;" id="harga_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['harga'] ?>" size="5"> PKP 
						<input type="checkbox" name="is_pkp_<?php echo $i ?>_<?php echo $z ?>" id="is_pkp_<?php echo $i ?>_<?php echo $z ?>" value="t" <?php if ($var_detail[$k]['is_harga_pkp'] == 't') { ?>checked="true"<?php } ?>> &nbsp;&nbsp;&nbsp;Qty 
						<input type="text" name="stok_harga_<?php echo $i ?>_<?php echo $z ?>" id="stok_harga_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['jum_stok_opname'] ?>" size="3" style="text-align:right;">
					</td>
					</tr>
					<?php 	$z++;
						} 
					}
					else {
						?>
						<tr>
						<td>Harga <input type="text" name="harga_<?php echo $i ?>_1" style="text-align:right;" id="harga_<?php echo $i ?>_1" value="0" size="5"> PKP 
						<input type="checkbox" name="is_pkp_<?php echo $i ?>_1" id="is_pkp_<?php echo $i ?>_1" value="t"> &nbsp;&nbsp;&nbsp;Qty 
						<input type="text" name="stok_harga_<?php echo $i ?>_1" id="stok_harga_<?php echo $i ?>_1" value="0" size="3" style="text-align:right;">
					</td>
					</tr>
					<?php } ?>
					</table>
				</td>
				<?php	
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>
<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return confirm('Yakin akan simpan data SO ??')">&nbsp;
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/soharga'">
<?php echo form_close();  ?>
</div>
