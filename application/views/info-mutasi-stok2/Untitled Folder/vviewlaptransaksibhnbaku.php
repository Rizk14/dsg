<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Transaksi Barang Bahan Baku/Pembantu</h3><br><br>

<div>
Lokasi Gudang: <?php if ($id_gudang!= 0) { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else echo "Semua"; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>

<?php
	echo "<b>".$kode_brg." - ".$nama_brg."</b><br>";
?>
	Satuan Awal: <?php echo $satuan_awal."<br>" ?>
	Satuan Konversi: <?php echo $satuan_konv."<br><br>" ?>
	
	<i>* Saldo Awal dan Qty Masuk/Keluar sudah dalam satuan konversi</i>
	<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		 <th width="5%" rowspan="2">Tgl</th>
		 <th width="10%" colspan="2">No Bukti</th>
		 <th width="10%" rowspan="2">Masuk</th>
		 <th width="10%" rowspan="2">Masuk Lain lain</th>
		 <th width="10%" rowspan="2">Keluar</th>
		 
		 <th width="10%" rowspan="2">Keluar Lain lain</th>
		 <th width="10%" rowspan="2">Saldo</th>
	 </tr>
	 <tr class="judulnya">
		 <th>Masuk</th>
		 <th>Keluar</th>
	 </tr>
	</thead>
	<tbody>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right">
			<?php echo $jum_stok_opname ?>
			</td>
		</tr>

<?php
	$tmasuk = 0; $tmasuklain = 0; $tkeluar = 0; $tkeluarlain = 0;
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++){
?>
		<tr>
		<td><?php echo "&nbsp;".$query[$j]['tgl_bonm'] ?></td>
		<td style="white-space:nowrap;"><?php if ($query[$j]['masuk'] == "ya") { echo "No Bon M: ".$query[$j]['no_bonm']."<br>No/Tgl SJ: ".$query[$j]['no_sj']." / ".$query[$j]['tgl_sj']."<br>Supplier: ".$query[$j]['kode_supplier']."-".$query[$j]['nama_supplier'] ; } else echo "&nbsp;"; ?></td>
		<td><?php if ($query[$j]['keluar'] == "ya") echo "&nbsp;".$query[$j]['no_manual']; else echo "&nbsp;"; ?></td>
		<td align="right"><?php if ($query[$j]['masuk'] == "ya" && $query[$j]['is_masuk'] == '1') { echo $query[$j]['qty']; $tmasuk+=$query[$j]['qty']; } else echo "&nbsp;"; ?></td>
		<td align="right"><?php if ($query[$j]['masuk'] == "ya" && $query[$j]['is_masuklain'] == '1') { echo $query[$j]['qty']; $tmasuklain+=$query[$j]['qty']; } else echo "&nbsp;"; ?></td>
		<td align="right"><?php if ($query[$j]['keluar'] == "ya" && $query[$j]['is_keluar'] == '1') { echo $query[$j]['qty']; $tkeluar+=$query[$j]['qty']; } else echo "&nbsp;"; ?></td>
		
		<td align="right"><?php if ($query[$j]['keluar'] == "ya" && $query[$j]['is_keluarlain'] == '1') { echo $query[$j]['qty']; $tkeluarlain+=$query[$j]['qty']; } else echo "&nbsp;"; ?></td>
				 
		<td align="right"><?php echo $query[$j]['saldo']; ?></td>	</tr>
<?php
		}
		
	}
?>
<tr>
	<td colspan="3" align="center">TOTAL</td>
	<td align="right"><?php echo $tmasuk; ?></td>
	<td align="right"><?php echo $tkeluar; ?></td>
	<td align="right"><?php echo $tmasuklain; ?></td>
	<td align="right"><?php echo $tkeluarlain; ?></td>
	<td>&nbsp;</td>
</tr>
	</tbody>
	</table>
</div>
