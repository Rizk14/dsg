<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Hasil Cutting vs SJ Keluar Ke Jahitan</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div>
Total Data = <?php echo $jum_total ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_lap', 'id' => 'f_lap');
echo form_open('info-mutasi-stok/creport/export_excel_hasilcuttingvssjkeluar', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang WIP</th>
		 <th>Jumlah<br>Hasil Cutting</th>
		 <th>Jumlah<br>Kirim Ke Jahitan</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
		?>
				<tr>
					<td align="right"><?php echo $i ?></td>
					<td>&nbsp;<?php echo $query[$j]['kode_brg_wip'] ?></td>
					<td>&nbsp;<?php echo $query[$j]['nama_brg_wip'] ?></td>
					<td align="right"><?php echo $query[$j]['jum_cutting'] ?></td>
					<td align="right"><?php echo $query[$j]['jum_kirim'] ?></td>
				</tr>
		<?php
					$i++;
				}

			}
	?>
	
 	</tbody>
</table><br><br>

</div>
