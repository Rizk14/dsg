<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>

<body onLoad="closeMe();">

<?php
echo "Printer: ".$printer_name;

$get_attributes	= false;
$waktu	= date("h:i:s");
$line	= 80;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php");
} else {
	include_once("printipp_classes/BasicIPP.php");
}

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

$List = "";
for($jj=0;$jj<4;$jj++){
	$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";
}

$List	.= CHR(27).CHR(69).str_repeat(" ",30).$nomorpajak.CHR(27).CHR(70)."\n";

for($a=1;$a<5;$a++){
	$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18).CHR(0);
}

$List	.= CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).strtoupper($nminisial).CHR(27).CHR(70).CHR(18).CHR(0)."\n";
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";

$List	.= CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).strtoupper($almtperusahaan).CHR(27).CHR(70).CHR(18).CHR(0)."\n";
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";

$List	.= CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).strtoupper($npwpperusahaan).CHR(27).CHR(70).CHR(18).CHR(0)."\n";
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";

for($a=1;$a<=3;$a++) {
	$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";
}

$List	.= CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).$nmkostumer.CHR(27).CHR(70).CHR(18).CHR(0)."\n";
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";

$List	.= CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).$almtkostumer.CHR(27).CHR(70).CHR(18).CHR(0)."\n";
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";

$List	.= CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).$npwp.str_repeat(" ",5)."NPPKP : 00.000.000.0.000.000".CHR(27).CHR(70).CHR(18).CHR(0)."\n";
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";

for($a=1;$a<=4;$a++) {
	$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";
}

$no		= 1;
$nolength	= 4;
$brglength	= 78;
$hrglength	= 15;
$arrnolenght	= array();
$arrbrglenght	= array();
$arrhrglenght	= array();
$minusnolength	= array();
$minusbrglength	= array();
$minushrglength	= array();
$tprice		= array();

$j = 0;

foreach($isi as $row) {	
	
	if($no<23) {
		
		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
		
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$tprice[$j]	= $row_value->amount;
		}else{
			$tprice[$j]	= 0;
		}

		$arrnolenght[$no]	= strlen($no);
		$arrbrglenght[$no]	= strlen($row->motifname);
		$arrhrglenght		= strlen(number_format($tprice[$j]));
		$minusnolength[$no]	= ($arrnolenght[$no] <= $nolength)?($nolength-$arrnolenght[$no]):0;
		$minusbrglength[$no]	= $brglength-$arrbrglenght[$no];
		$minushrglength[$no]	= $hrglength-$arrhrglenght;
		
		$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$no]+4)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$no]+45)).str_repeat(" ",$minushrglength[$no])." ".number_format($tprice[$j]).CHR(18).CHR(0)."\n";
		
		$j+=1;
		$no+=1;
			
	}
}

if($no<=29) {
	$akhr	= $no;
	for($loop=$akhr;$loop<=29;$loop++) {	
		$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";
	}
}else{
	$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";
}

$lengthjml	= strlen(number_format($jumlah));
$lengthdis	= strlen(number_format($diskon));
$lengthdpp	= strlen(number_format($dpp));
$lengthppn	= strlen(number_format($nilai_ppn));

$spcjml	= $line-$lengthjml;
$spcdis	= $line-$lengthdis;
$spcdpp	= $line-$lengthdpp;
$spcppn	= $line-$lengthppn;

$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",18)."XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".str_repeat(" ",$spcjml+18).CHR(2).number_format($jumlah)."\n".CHR(18);
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18);

$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcdis+72).number_format($diskon)."\n".CHR(18);
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18);
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18);
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18);

$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcdpp+72).number_format($dpp)."\n".CHR(18);
$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18);

$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcppn+72).number_format($nilai_ppn)."\n".CHR(18);

$footalamat	= strlen($falamat);
$foottgl	= strlen('Tanggal');
$footshtgl	= strlen($ftgl);

for($jj=0;$jj<1;$jj++) {
	$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18).CHR(0);
}

$footsisa_grs	= $line-($footalamat+1+$foottgl+1+1+$footshtgl);

$List	.= CHR(1).CHR(27).CHR(50).str_repeat(" ",($footsisa_grs+5)).CHR(27).CHR(69).$falamat."            ".$tglfaktur."\n".CHR(27).CHR(70);

$footnm	= $line-24;
$footjbt= $line-25;

for($jj=0;$jj<5;$jj++) {
	$List	.= CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";
}

$List	.= CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footnm+8))." ".CHR(27).CHR(69).$TtdPajak01.CHR(27).CHR(70);

$List	.= CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";

for($jj=1;$jj<=2;$jj++) {
	$List	.= CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n";
}

$List	.= CHR(1).CHR(27).CHR(50).str_repeat(" ",($footjbt+7))." ".CHR(27).CHR(69).str_repeat(" ",3)."Adm Penjualan".CHR(27).CHR(70).CHR(0);

$ipp->setData($List);
$ipp->printJob();

/*** Original 

for($jj=0;$jj<4;$jj++){
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(69).str_repeat(" ",30).$nomorpajak.CHR(27).CHR(70)."\n");
$ipp->printJob();

for($a=1;$a<5;$a++){
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18).CHR(0));
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).strtoupper($nminisial).CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).strtoupper($almtperusahaan).CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).strtoupper($npwpperusahaan).CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=3;$a++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).$nmkostumer.CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).$almtkostumer.CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(103).CHR(15).str_repeat(" ",32).CHR(27).CHR(69).$npwp.str_repeat(" ",5)."NPPKP : 00.000.000.0.000.000".CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=4;$a++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$no		= 1;
$nolength	= 4;
$brglength	= 78;
$hrglength	= 15;
$arrnolenght	= array();
$arrbrglenght	= array();
$arrhrglenght	= array();
$minusnolength	= array();
$minusbrglength	= array();
$minushrglength	= array();
$tprice		= array();

$j = 0;

foreach($isi as $row) {	
	
	if($no<23) {
		
		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
		
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$tprice[$j]	= $row_value->amount;
		}else{
			$tprice[$j]	= 0;
		}

		$arrnolenght[$no]	= strlen($no);
		$arrbrglenght[$no]	= strlen($row->motifname);
		$arrhrglenght		= strlen(number_format($tprice[$j]));
		$minusnolength[$no]	= ($arrnolenght[$no] <= $nolength)?($nolength-$arrnolenght[$no]):0;
		$minusbrglength[$no]	= $brglength-$arrbrglenght[$no];
		$minushrglength[$no]	= $hrglength-$arrhrglenght;
		
		$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$no]+4)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$no]+45)).str_repeat(" ",$minushrglength[$no])." ".number_format($tprice[$j]).CHR(18).CHR(0)."\n");
		$ipp->printJob();
		
		$j+=1;
		$no+=1;
			
	}
}

if($no<=29) {
	$akhr	= $no;
	for($loop=$akhr;$loop<=29;$loop++) {	
		$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
		$ipp->printJob();	
	}
}else{
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$lengthjml	= strlen(number_format($jumlah));
$lengthdis	= strlen(number_format($diskon));
$lengthdpp	= strlen(number_format($dpp));
$lengthppn	= strlen(number_format($nilai_ppn));

$spcjml	= $line-$lengthjml;
$spcdis	= $line-$lengthdis;
$spcdpp	= $line-$lengthdpp;
$spcppn	= $line-$lengthppn;

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",18)."XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".str_repeat(" ",$spcjml+18).CHR(2).number_format($jumlah)."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcdis+72).number_format($diskon)."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcdpp+72).number_format($dpp)."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcppn+72).number_format($nilai_ppn)."\n".CHR(18));
$ipp->printJob();

$footalamat	= strlen($falamat);
$foottgl	= strlen('Tanggal');
$footshtgl	= strlen($ftgl);

for($jj=0;$jj<1;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18).CHR(0));
	$ipp->printJob();
}

$footsisa_grs	= $line-($footalamat+1+$foottgl+1+1+$footshtgl);

$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footsisa_grs+5)).CHR(27).CHR(69).$falamat."            ".$tglfaktur."\n".CHR(27).CHR(70));
$ipp->printJob();

$footnm	= $line-24;
$footjbt= $line-25;

for($jj=0;$jj<5;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footnm+8))." ".CHR(27).CHR(69).$TtdPajak01.CHR(27).CHR(70));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($jj=1;$jj<=2;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footjbt+7))." ".CHR(27).CHR(69)."Kasie Accounting".CHR(27).CHR(70).CHR(0));
$ipp->printJob();

***/

/*** 27032012

for($jj=0;$jj<5;$jj++){
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",30).CHR(27).CHR(69).$nomorpajak.CHR(27).CHR(70)."\n");
$ipp->printJob();

for($a=1;$a<=2;$a++){
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).str_repeat(" ",27).CHR(27).CHR(69).strtoupper($nminisial).CHR(27).CHR(70).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77)." ".CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).str_repeat(" ",27).CHR(27).CHR(69).strtoupper($almtperusahaan).CHR(27).CHR(70).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77)." ".CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).str_repeat(" ",27).CHR(27).CHR(69).strtoupper($npwpperusahaan).CHR(27).CHR(70).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77)." ".CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=3;$a++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77)." ".CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).str_repeat(" ",27).CHR(27).CHR(69).$nmkostumer.CHR(27).CHR(70).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77)." ".CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).str_repeat(" ",27).CHR(27).CHR(69).$almtkostumer.CHR(27).CHR(70)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77)." ".CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).str_repeat(" ",27).CHR(27).CHR(69).$npwp.str_repeat(" ",5)."NPPKP : 00.000.000.0.000.000".CHR(27).CHR(70)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77)." ".CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=4;$a++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$no		= 1;
$nolength	= 4;
$brglength	= 78;
$hrglength	= 15;
$arrnolenght	= array();
$arrbrglenght	= array();
$arrhrglenght	= array();
$minusnolength	= array();
$minusbrglength	= array();
$minushrglength	= array();
$tprice		= array();

$j = 0;

foreach($isi as $row) {
	
	if($no<23) {

		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$tprice[$j]	= $row_value->amount;
		} else {
			$tprice[$j]	= 0;
		}

		$arrnolenght[$no]	= strlen($no);
		$arrbrglenght[$no]	= strlen($row->motifname);
		$arrhrglenght		= strlen(number_format($tprice[$j]));
		$minusnolength[$no]	= ($arrnolenght[$no] <= $nolength)?($nolength-$arrnolenght[$no]):0;
		$minusbrglength[$no]	= $brglength-$arrbrglenght[$no];
		$minushrglength[$no]	= $hrglength-$arrhrglenght;
		
		$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$no]+4)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$no]+45)).str_repeat(" ",$minushrglength[$no])." ".number_format($tprice[$j]).CHR(18).CHR(0)."\n");
		$ipp->printJob();
	}
	
	$j+=1;
	$no+=1;
}

if($no<=29) {
	$akhr	= $no;
	for($loop=$akhr;$loop<=29;$loop++) {	
		$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
		$ipp->printJob();	
	}
}else{
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$lengthjml	= strlen(number_format($jumlah));
$lengthdis	= strlen(number_format($diskon));
$lengthdpp	= strlen(number_format($dpp));
$lengthppn	= strlen(number_format($nilai_ppn));

$spcjml	= $line-$lengthjml;
$spcdis	= $line-$lengthdis;
$spcdpp	= $line-$lengthdpp;
$spcppn	= $line-$lengthppn;

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",18)."XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".CHR(18).str_repeat(" ",$spcjml-22).CHR(2).CHR(27).CHR(69).number_format($jumlah).CHR(27).CHR(70).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",$spcdis+10).CHR(2).CHR(27).CHR(69).number_format($diskon).CHR(27).CHR(70).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).CHR(2)." ".CHR(18).CHR(3)."\n"); 
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",$spcdpp+10).CHR(2).CHR(27).CHR(69).number_format($dpp).CHR(27).CHR(70).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",$spcppn+10).CHR(2).CHR(27).CHR(69).number_format($nilai_ppn).CHR(27).CHR(70).CHR(3)."\n");
$ipp->printJob();

$footalamat	= strlen($falamat);
$foottgl	= strlen('Tanggal');
$footshtgl	= strlen($ftgl);

for($jj=0;$jj<1;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$footsisa_grs	= $line-($footalamat+1+$foottgl+1+1+$footshtgl);

$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footsisa_grs+6)).CHR(27).CHR(69).$falamat."            ".$tglfaktur.CHR(27).CHR(70)."\n");
$ipp->printJob();

$footnm	= $line-24;
$footjbt= $line-25;

for($jj=0;$jj<5;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footnm+8))." ".CHR(27).CHR(69).$TtdPajak01.CHR(27).CHR(70));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($jj=1;$jj<=1;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footjbt+7))." ".CHR(27).CHR(69)."Kasie Accounting".CHR(27).CHR(70).CHR(0));
$ipp->printJob();

***/

/* Disabled 01112011
for($jj=0;$jj<12;$jj++){
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." "."\n".CHR(18));
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}
*/

?>

</body>
