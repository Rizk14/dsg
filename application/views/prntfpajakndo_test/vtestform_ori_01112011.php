<html>
<head>
<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>
</head>
<body onLoad="closeMe();">

<?php
echo "Printer: ".$printer_name;
?>

<?php

/*
Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
Italic	= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)
San Serif	= CHR(27).CHR(107).CHR(49)
Start Text	= CHR(2)
End Text	= CHR(3)
Set Bottom Margin	= CHR(27).CHR(78).CHR(2)
Cancel Set Bottom Margin	= CHR(27).CHR(79)
*/

$get_attributes	= false;
$waktu	= date("h:i:s");
$line	= 80;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php");
} else {
	include_once("printipp_classes/BasicIPP.php");
}

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

for($jj=0;$jj<9;$jj++){
	$ipp->setData(CHR(1).CHR(27).CHR(48).CHR(18)."\n"); //ESC 0
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",27).CHR(27).CHR(69).$nomorpajak.CHR(27).CHR(70)."\n");
$ipp->printJob();

for($a=1;$a<=10;$a++) {
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)."\n".CHR(18));	
	$ipp->printJob();
}
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",24).CHR(27).CHR(69).$nmkostumer.CHR(27).CHR(70)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",24).CHR(27).CHR(69).$almtkostumer.CHR(27).CHR(70)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",24).CHR(27).CHR(69).$npwp.str_repeat(" ",5)."NPPKP : 00.000.000.0.000.000".CHR(27).CHR(70)."\n");
$ipp->printJob();
for($a=1;$a<=2;$a++) {
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15)."\n".CHR(18));
	$ipp->printJob();
}

$no			= 1;
$nolength	= 4;
$brglength	= 78;
$hrglength	= 15;
$arrnolenght	= array();
$arrbrglenght	= array();
$arrhrglenght	= array();
$minusnolength	= array();
$minusbrglength	= array();
$minushrglength	= array();

$tprice		= array();

$j = 0;

foreach($isi as $row) {
	if($no<23) {

		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$tprice[$j]	= $row_value->amount;
		} else {
			$tprice[$j]	= 0;		
		}
			
		$arrnolenght[$no]	= strlen($no);
		$arrbrglenght[$no]	= strlen($row->motifname);
		$arrhrglenght		= strlen(number_format($tprice[$j]));
		$minusnolength[$no]	= ($arrnolenght[$no] <= $nolength)?($nolength-$arrnolenght[$no]):0;
		$minusbrglength[$no]	= $brglength-$arrbrglenght[$no];
		$minushrglength[$no]	= $hrglength-$arrhrglenght;
		$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$no]+1)).$row->motifname.str_repeat(" ",($minusbrglength[$no]+25)).str_repeat(" ",$minushrglength[$no])." ".number_format($tprice[$j]).CHR(18).CHR(3)."\n");
		$ipp->printJob();
	}
	$j+=1;
	$no+=1;
}

if($no<=22) {
	$akhr	= $no;
	for($loop=$akhr;$loop<=23;$loop++) {
		$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
		$ipp->printJob();
	}
}else{
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
	$ipp->printJob();
}

$lengthjml	= strlen(number_format($jumlah));
$lengthdis	= strlen(number_format($diskon));
$lengthdpp	= strlen(number_format($dpp));
$lengthppn	= strlen(number_format($nilai_ppn));

$spcjml	= $line-$lengthjml;
$spcdis	= $line-$lengthdis;
$spcdpp	= $line-$lengthdpp;
$spcppn	= $line-$lengthppn;

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",24)."XXXXXXXXXXXXXXXXXXXXXXXXXXXXX".str_repeat(" ",$spcjml-4).CHR(2).number_format($jumlah).CHR(18).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",$spcdis+49).CHR(2).number_format($diskon).CHR(18).CHR(3)."\n");
$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)."\n".CHR(18));
$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",$spcdpp+49).CHR(2).number_format($dpp).CHR(18).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",$spcppn+49).CHR(2).number_format($nilai_ppn).CHR(18).CHR(3)."\n");
$ipp->printJob();

$footalamat	= strlen($falamat);
$foottgl	= strlen('Tanggal');
$footshtgl	= strlen($ftgl);

$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)."\n".CHR(18));
//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
$ipp->printJob();

$footsisa_grs	= $line-($footalamat+1+$foottgl+1+1+$footshtgl);
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footsisa_grs-4)).CHR(27).CHR(69).$falamat.","."       "." ".$tglfaktur.CHR(27).CHR(70)."\n");
$ipp->printJob();

$footnm	= $line-24;
$footjbt= $line-25;

$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).str_repeat(" ",4)." ".CHR(18).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).str_repeat(" ",4)." ".CHR(18).CHR(3)."\n");
$ipp->printJob();

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footnm-4))." ".CHR(27).CHR(69).$TtdPajak01.CHR(27).CHR(70));
$ipp->printJob();
$ipp->setData("\n");
$ipp->printJob();
$ipp->setData("\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footjbt-3))." ".CHR(27).CHR(69)."Kasie Accounting".CHR(27).CHR(70));
$ipp->printJob();
?>
</body>
</html>
