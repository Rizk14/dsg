<style>
.garisbawah { 
		border-bottom:#000000 0.1px solid;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat">Delivery Order (DO) Per Item Barang</td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form Delivery Order (DO) Per Item Barang</td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'opvsdo/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlopvsdoform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="19%">DO dari Tgl </td>
							<td width="0%">&nbsp;</td>
							<td width="81%">
							  <input name="d_op_first" type="text" id="d_op_first" maxlength="10" value="<?=$tglmulai?>"/>
							  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_first,'dd/mm/yyyy',this)">
							s.d 
							<input name="d_last" type="text" id="d_op_last" maxlength="10" value="<?=$tglakhir?>" />
							<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_last,'dd/mm/yyyy',this)">
							</td>
						  </tr>						  
						</table>
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td>
			  	<div id="title-box2">Detail Item Barang</div>
			  	</td>
			  </tr>
			  <tr>
				<td><table width="90%" border="1" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="2%" class="tdatahead">NO</td>
					<td width="10%" align="center" class="tdatahead">KODE BARANG</td>
					<td width="40%" align="center" class="tdatahead" style="white-space: nowrap">NAMA BARANG </td>
					<td width="10%" align="center" class="tdatahead">QTY</td>
					<td width="20%" align="center" class="tdatahead">PER WARNA </td>
				  </tr>
				  
				  <?php
				$no	= 1;
				$cc	= 1;
				$lbrg = "";
				if (is_array($query)) {
				  for($j=0;$j<count($query);$j++){
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
							$lbrg	.= "
								  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
									onMouseOut=\"this.className='$Classnya'\">
									<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
									<td>".$query[$j]['imotif']."</td>
									<td>".$query[$j]['productmotif']."</td>
									<td align=\"right\">".$query[$j]['qty']."</td>
									<td align=\"right\">".$query[$j]['listwarna']."</td>";
							$lbrg.="</tr>";
							
							$no+=1; $cc+=1;
				  }
				}									
					echo $lbrg;
				  
				  ?>

				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>	  

			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	<td align="right">
					<input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listdoperproduk/cform/'">

				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
