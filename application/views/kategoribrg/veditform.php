<script type="text/javascript">

function toUpper1(){
	var e_category_name	= document.getElementById('e_category_name').value;
	var toUpper	= e_category_name.toUpperCase();
	document.getElementById('e_category_name').value = toUpper;
}

</script>

<div id="tabs">
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">FORM KATEGORI BRG</div>
</div>
         
<div class="tab_bdr"></div>

	<div class="panel" id="panel2" style="display:block;">
	    <table class="maintable">
	      <tr>
	    	<td align="left">
		      <?php
		      
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('kategoribrg/cform/actedit', $attributes);?>
			
		<div id="mastercategoriform">
		<div class="effect">
		  <div class="accordion2">
		      <table class="mastertable">
		      	<tr>
			  <td width="150px">Kode Kategori</td>
			  <td width="1px">:</td>
			  <td>
			    <?php
			    $kdkategori = array(
				'name'=>'i_category',
				'id'=>'i_category',
				'value'=>$i_category_code,
				'maxlength'=>'5' );
				echo form_input($kdkategori);
			    ?>		
			  </td>
			</tr>
		      	<tr>
			  <td>Nama Kategori</td>
			  <td>:</td>
			  <td>
			    <?php
			    $nmkategori = array(
				'name'=>'e_category_name',
				'id'=>'e_category_name',
				'value'=>$e_category_name,
				'maxlength'=>'255',
				'onkeyup'=>'toUpper1();');	
			    echo form_input($nmkategori); ?>		
			  </td>
			</tr>
		      	<tr>
			  <td>Kode Kelas</td>
			  <td>:</td>
			  <td>
			    <?php
			    $nmkls = array(
				'name'=>'i_class',
				'id'=>'i_class',
				'onclick'	=>'ckodekls();',
				'value'=>$iclass,
				'maxlength'=>'5');
			    echo form_input($nmkls); ?>
			  </td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			  	<input type="hidden" name="icategory" id="icategory" value="<?php echo $i_category; ?>" />
			  	<input type="hidden" name="iclass" id="iclass" value="<?php echo $iclass;?>" />
			    <input name="tblsimpankbrg" id="tblsimpankbrg" value="Simpan" type="submit">
			    	<input name="btnbatal" id="btnbatal" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/kategoribrg/cform'">

			  </td>
		       </tr>
		      </table>
			</div>
		</div>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
