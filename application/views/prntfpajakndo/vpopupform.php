<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title></title>
<style type="text/css" media="screen, print">
body {
	margin:0;
}

#tbl_utama {
	width:1060px;
}

#tbl_utama2 {
	width:1080px;
	vertical-align:top;
}
</style>

<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>

</head>

<body onload="window.print(); closeMe();">

<table>
  <tr>
    <td><table border="0" cellspacing="0" cellpadding="0" width="1080px;">
      <tr>
        <td align="right"><table width="32%" border="0" cellspacing="0" cellpadding="0" style="border-top:#FFFFFF 1px solid; border-bottom:#FFFFFF 1px solid; border-left:#FFFFFF 1px solid; border-right:#FFFFFF 1px solid; font-family:Arial; font-size:11px;">
            <tr>
              <td style="height:30px;">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" style="height:17px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family:Arial; font-size:14px; line-height:24px; padding-left:350px;"><?php echo $nomorpajak; ?></td>
      </tr>
      <tr>
        <td style="border-bottom:#FFFFFF 1px solid; border-left:#FFFFFF 1px solid; border-right:#FFFFFF 1px solid;"><table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td colspan="3" style="font-family:Arial; font-size:13px;line-height:25px;">&nbsp;</td>
            </tr>
            <tr>
              <td width="21%" style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td width="1%" style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td width="78%" style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
            </tr>
            <tr>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
            </tr>
            <tr>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td colspan="3" style="font-family:Arial; font-size:13px; font-weight:bold; line-height:65px;">&nbsp;</td>
            </tr>
            <tr>
              <td width="21%" style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td width="1%" style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td width="78%" style="font-family:Arial; font-size:13px; line-height:25px;"><span style="padding-left:55px;"><?php echo $nmkostumer; ?></span>&nbsp;</td>
            </tr>
            <tr>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px;"><span style="padding-left:55px;"><?php echo $almtkostumer; ?></span>&nbsp;</td>
            </tr>
            <tr>
              <td style="font-family:Arial; font-size:13px; line-height:25px; color:#FFFFFF;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px; color:#FFFFFF;">&nbsp;</td>
              <td style="font-family:Arial; font-size:13px; line-height:25px;"><span style="padding-left:55px;"><?php echo $npwp; ?></span>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="6%" style="font-family:Arial; font-size:12px; text-align:center; height:45px;">&nbsp;</td>
              <td width="56%" style="font-family:Arial; font-size:12px; text-align:center;">&nbsp;</td>
              <td width="38%" style="font-family:Arial; font-size:12px; text-align:center;">&nbsp;</td>
            </tr>
            <?php
	  $no	= 1;
	  if(count($isi) > 0 ) {
		  foreach($isi as $row) {
		  	if($no < 23) {
	  ?>
            <tr>
              <td style="font-family:Arial; font-size:10px; padding-left:5px; line-height:1.4;"><?php echo $no; ?></td>
              <td style="font-family:Arial; font-size:10px; padding-left:5px;"><?php echo $row->motifname; ?>&nbsp;</td>
              <td style="font-family:Arial; font-size:10px; padding-left:5px; text-align:right;"><?php echo number_format($row->amount); ?>&nbsp;</td>
            </tr>
            <?php
	  		}
		  $no+=1;
		  }
		  
		  if($no<=22) {
			$akhr	= $no;
			for($loop=$akhr;$loop<=27;$loop++) {
				echo "
					<tr>
						<td style=\"font-family:Arial; font-size:10px; padding-left:5px; line-height:1.4;\">&nbsp;</td>
						<td style=\"font-family:Arial; font-size:10px; padding-left:5px;\">&nbsp;</td>
						<td style=\"font-family:Arial; font-size:10px; padding-left:5px; text-align:right;\">&nbsp;</td>
					</tr>";
			}
		  }		  
	  }				  
	  ?>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-left:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="61%" style="line-height:2;">&nbsp;</td>
              <td width="39%" style="text-align:right;"><?php echo number_format($jumlah); ?>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-left:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="61%" style="line-height:1.7;">&nbsp;</td>
              <td width="39%" style="text-align:right;"><?php echo number_format($diskon); ?>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-left:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="61%" style="line-height:1.7;">&nbsp;</td>
              <td width="39%" style="text-align:right; padding-right:1px;">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-left:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="61%" style="line-height:1.7;">&nbsp;</td>
              <td width="39%" style="text-align:right;"><?php echo number_format($dpp); ?>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-left:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="61%" style="line-height:1.7;">&nbsp;</td>
              <td width="39%" style="text-align:right;"><?php echo number_format($nilai_ppn); ?>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
	<td style="line-height:1;">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="60%" style="font-family:Arial; font-size:13px; line-height:24px; padding-left:5px;">&nbsp;</td>
              <td width="40%" style="font-family:Arial; font-size:12px; text-align:center;"><?php echo $falamat; ?> ,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ftgl; ?></td>
            </tr>
            <tr>
              <td align="left" valign="top" style="padding-left:5px;"><table width="86%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="25%" style="font-family:arial;font-size:12px; line-height:22px;">&nbsp;</td>
                    <td width="38%" style="font-family:arial;font-size:12px; text-align:center; line-height:22px;">&nbsp;</td>
                    <td width="37%" style="font-family:arial;font-size:12px; text-align:center; line-height:22px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="2" style="font-family:arial;font-size:12px; text-align:center;">&nbsp;</td>
                    <td style="font-family:arial;font-size:11px; text-align:center; line-height:15px;">&nbsp;</td>
                  </tr>
              </table></td>
              <td align="center" style="font-family:arial; font-size:13px; padding-left:80px;"><br />
                  <br />
                Kristina Oentoro<br />
                <br />
		Kasie. Accounting
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:11px;">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
