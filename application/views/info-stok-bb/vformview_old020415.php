<h3>Laporan Stok Terkini Bahan Baku/Pembantu</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('info-stok-bb/cform/view'); ?>
<table>
	<tr>
		<td>Gudang</td>
		<td><select name="id_gudang" id="id_gudang">
					<option value="0" <?php if ($cgudang == '' || $cgudang == '0') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($gud->id == $cgudang) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><select name="statusnya" id="statusnya">
		<option value="t" <?php if ($cstatus == 't') { ?> selected="true" <?php } ?> >Aktif</option>
		<option value="f" <?php if ($cstatus == 'f') { ?> selected="true" <?php } ?> >Non-Aktif</option>
		</select></td>
	</tr>
	<tr>
		<td>Keyword Pencarian</td>
		<td><input type="text" name="cari" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table>

<?php echo form_close(); ?>
<br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		<th>No</th>
		<th>Kode Barang</th>
		 <th>Nama Barang</th>
		 <th>Satuan</th>
		 <th>Stok</th>
		 <th>Last Update Stok</th>
		 <th>Tgl Stok Opname</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				if ($startnya == '')
					$i=1;
				else
					$i = $startnya+1; 
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_update_stok']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update_stok = $tgl1." ".$nama_bln." ".$thn1;
				
				$bulan = $query[$j]['bulan'];	
					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
					else $nama_bln = "";
				 
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg']."</td>";
				 echo    "<td nowrap>".$query[$j]['nama_brg']."</td>";
				 echo    "<td nowrap>".$query[$j]['satuan']."</td>";
				 echo    "<td align='right'>".$query[$j]['stok']."</td>";
				 echo    "<td align='center'>".$tgl_update_stok."</td>";
				 echo "<td>";
				 if ($query[$j]['tgl_update_so'] != '') {
					 echo "Periode: ".$nama_bln." ".$query[$j]['tahun']." (".$query[$j]['tgl_update_so'].")";
					 if ($query[$j]['status_approve'] == 't')
						echo " OK ";
					else
						echo " Belum Diapprove ";
				  }
				  else echo "&nbsp;";
				 echo "</td>";
				 echo  "</tr>";
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
