<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_piutang; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_piutang; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'expopiutang/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlpiutangform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="80%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_piutang_no_kontrabon; ?> </td>
					<td width="1%">:</td>
					<td width="70%">
					  <input name="no_kontrabon" type="text" id="no_kontrabon" maxlength="14" value="<?=$nokontrabon?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_piutang_tgl_kontrabon; ?></td>
					<td>:</td>
					<td>
					  <input name="d_kontrabon_first" type="text" id="d_kontrabon_first" maxlength="10" value="<?=$tglkontrabonmulai?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_kontrabon_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_kontrabon_last" type="text" id="d_kontrabon_last" maxlength="10" value="<?=$tglkontrabonakhir?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_kontrabon_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
			    <td>
			    <div id="title-box2"><?php echo $form_title_detail_piutang; ?></div></td>	
			  </tr>
			  <tr>
				<td>
				<?php if($template==1) { ?>
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="12%" class="tdatahead"><?php echo $list_piutang_no_kontrabon; ?> </td>
					<td width="14%" class="tdatahead"><?php echo $list_piutang_tgl_kontrabon; ?> </td>
					<td width="20%" class="tdatahead"><?php echo $list_piutang_nilai_kontrabon; ?> </td>
					<td width="20%" class="tdatahead"><?php echo $list_piutang_nilai_piutang; ?> </td>
				  </tr>
				  
				  <?php

				  $no	= 1;
				  $cc	= 1;
				  $arr	= 0;
				  
				  $totalkontrabon = 0;
				  $totalpiutang = 0;
				  
				  if(sizeof($query) > 0 ) {

					  foreach($query as $row) {
					  
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";					
						
						$exp_d_dt = explode("-",$row->d_dt,strlen($row->d_dt));
						
						$lpiutang	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->i_dt_code."</td>
								<td align=\"center\">".$exp_d_dt[2].'/'.$exp_d_dt[1].'/'.$exp_d_dt[0]."</td>
								<td align=\"right\">".number_format($row->v_total_grand,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->piutang,'2','.',',')."</td>
							  </tr>";	
							  
							  $totalkontrabon = $totalkontrabon+$row->v_total_grand;
							  $totalpiutang = $totalpiutang+$row->piutang;
							  
							  $no+=1;
							  $cc+=1;	
							  $arr+=1;
					  }
					  echo $lpiutang;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
				<?php } ?>

				<?php if($template==2) { ?>
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="12%" class="tdatahead"><?php echo $list_piutang_no_faktur; ?> </td>
					<td width="14%" class="tdatahead"><?php echo $list_piutang_tgl_faktur; ?> </td>
					<td width="14%" class="tdatahead"><?php echo $list_piutang_due_date; ?> </td>
					<td width="30%" class="tdatahead"><?php echo $list_piutang_pelanggan; ?> </td>
					<td width="12%" class="tdatahead"><?php echo $list_piutang_nilai_faktur; ?> </td>
					<td width="15%" class="tdatahead"><?php echo $list_piutang_tgl_kontrabon; ?> </td>
				  </tr>
				  
				  <?php

				  $no	= 1;
				  $cc	= 1;
				  $arr	= 0;
				  
				  $totalfaktur = 0;
				  $totalkontrabon = 0;
				  $totalpiutang = 0;
				  
				  if(sizeof($query)>0) {
					  
					  foreach($query as $row) {
						
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";					
						
						$exp_d_nota = explode("-",$row->d_nota,strlen($row->d_nota));
						$exp_d_due = explode("-",$row->d_due_date,strlen($row->d_due_date));
						$exp_d_dt =  explode("-",$row->d_dt,strlen($row->d_dt));
						
						$lpiutang	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->i_faktur_code."</td>
								<td align=\"center\">".$exp_d_nota[2].'/'.$exp_d_nota[1].'/'.$exp_d_nota[0]."</td>
								<td align=\"center\">".$exp_d_due[2].'/'.$exp_d_due[1].'/'.$exp_d_due[0]."</td>
								<td align=\"left\">".$row->e_branch_name."</td>
								<td align=\"right\">".number_format($row->v_total_fppn,'2','.',',')."</td>
								<td align=\"center\">".$exp_d_dt[2].'/'.$exp_d_dt[1].'/'.$exp_d_dt[0]."</td>
							  </tr>";	
							  
							  $totalfaktur = $totalfaktur+$row->v_total_fppn;
							  
							  $no+=1;
							  $cc+=1;	
							  $arr+=1;
					  }
					  echo $lpiutang;
				  }
				  
				  $query	= $this->mclass->totalkontrabon($nokontrabon);
				  if($query->num_rows()>0) {
					  $row_totalkontrabon = $query->row();
					  $totalkontrabon = $row_totalkontrabon->v_total_grand;
					  $totalpiutang = $row_totalkontrabon->piutang;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
				<?php } ?>				
				</td>
			  </tr>
			  <tr>
			  	<td align="center"><?php echo $create_link; ?></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>				  		  
			  <tr>
				<td><table width="80%" border="0" cellspacing="0" cellpadding="0">
				<?php if($template==1) { ?>
				  <tr>	
					<td width="40%" align="right"><?php echo $list_piutang_total_kontrabon; ?></td>
					<td width="1%">:</td>
					<td width="20%">
					  <input name="v_total_kontrabon" type="text" id="v_total_kontrabon" maxlength="50" style="text-align:right;" value="<?php echo number_format($totalkontrabon,'2','.',','); ?>" style="text-align:right;" />
					</td>
					<td width="20%" align="right"><?php echo $list_piutang_total_piutang; ?></td>
					<td width="0%">:</td>
					<td width="20%" align="right">
					  <input name="v_total_piutang" type="text" id="v_total_piutang" maxlength="50" style="text-align:right;" value="<?php echo number_format($totalpiutang,'2','.',','); ?>" style="text-align:right;" />
					</td>
				  </tr>	
				<?php } ?>
				
				<?php if($template==2) { ?>
				  <tr>	
					<td width="60%" align="right"><?php echo $list_piutang_total_faktur; ?></td>
					<td width="1%">:</td>
					<td width="39%">
					  <!-- <input name="v_total_faktur" type="text" id="v_total_faktur" maxlength="50" style="text-align:right;" value="<?php echo number_format($totalfaktur,'2','.',','); ?>" style="text-align:right;" /> -->
					  <input name="v_total_faktur" type="text" id="v_total_faktur" maxlength="50" style="text-align:right;" value="<?php echo number_format($totalfaktur2,'2','.',','); ?>" style="text-align:right;" />	
					</td>
				  </tr>
				  <tr>	
					<td width="60%" align="right"><?php echo $list_piutang_nilai_kontrabon; ?></td>
					<td width="1%">:</td>
					<td width="39%">
					  <input name="v_nilai_kontrabon" type="text" id="v_nilai_kontrabon" maxlength="50" style="text-align:right;" value="<?php echo number_format($totalkontrabon,'2','.',','); ?>" style="text-align:right;" />
					</td>
				  </tr>				  
				  <tr>	
					<td width="60%" align="right"><?php echo $list_piutang_nilai_piutang; ?></td>
					<td width="1%">:</td>
					<td width="39%">
					  <input name="v_nilai_piutang" type="text" id="v_nilai_piutang" maxlength="50" style="text-align:right;" value="<?php echo number_format($totalpiutang,'2','.',','); ?>" style="text-align:right;" />
					</td>
				  </tr>				  
				<?php } ?>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
					 <input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/expopiutang/cform/'">
				
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
