<h3>Laporan Kartu Stok Bahan Hasil Cutting</h3><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	$('#pilih').click(function(){
	  	    var kode_brg_jadi= jQuery('#kode_brg_jadi').val();
			var urlnya = "<?php echo base_url(); ?>index.php/info-mutasi-hsl-cutting/cform/show_popup_brg/"+kode_brg_jadi;
			
			if (kode_brg_jadi == '')
				alert ("Pilih dulu barang jadinya...");
			else
				openCenteredWindow(urlnya);
	  }); 
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_input() {
	var brg_baku= $('#brg_baku').val();
	var brg_jadi= $('#brg_jadi').val();
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();

	if (brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		return false;
	}
	if (brg_baku == '') {
		alert("Bahan Hasil Cutting harus dipilih..!");
		return false;
	}
	
	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	if (date_from > date_to) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
$attributes = array('name' => 'f_mutasi', 'id' => 'f_mutasi');
echo form_open('info-mutasi-hsl-cutting/cform/view', $attributes); ?>
<table width="60%">
	<tr>
		<td>Untuk Barang Jadi</td>
		<td> <input name="brg_jadi" type="text" id="brg_jadi" size="40" readonly="true" value=""/>
			<input type="hidden" name="kode_brg_jadi" id="kode_brg_jadi" value="">
			<input title="browse data brg jadi" name="pilih_brg_jadi" value="..." type="button" id="pilih_brg_jadi" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/info-mutasi-hsl-cutting/cform/show_popup_brg_jadi/');"></td>
	</tr>
	
	<tr>
		<td>Nama Bahan</td>
		<td> <input name="brg_baku" type="text" id="brg_baku" size="40" readonly="true" value=""/>
			<input type="hidden" name="kode_brg" id="kode_brg" value="">
			<input title="browse data bahan" name="pilih" value="..." type="button" id="pilih" ></td>
	</tr>
	<tr>
		<td width="20%">Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
  </tr>

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/info-mutasi-hsl-cutting/cform/'">
<?php echo form_close();  ?>
