<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Transaksi Forecast Di Unit Jahit</h3><br><br>

<div>
Unit Jahit: <?php echo $kode_unit." - ".$nama_unit ?><br>
Periode: <?php echo $bulan ." - "; echo $tahun ;?> <br><br>

<input type="hidden" name="jumbrg" id="jumbrg" value="<?php echo $jumbrg ?>" >
<input name="cmdreset" id="cmdreset" value="Export Ke Excel" type="button">
<?php 
//$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
//echo form_open('wip/creport/export_excel_transaksiunitjahit', $attributes); ?>
<!--<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="kode_unit" value="<?php echo $kode_unit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >
<input type="hidden" name="list_id_brg_wip" value="<?php echo $list_id_brg_wip ?>" >
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">-->
<?php //echo form_close();  ?>
<br><br>

<?php
	$nomor = 1;
	if (is_array($query)) {
		//~ 
		for($a=0;$a<count($query);$a++){
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="100%" id="sitabel_<?php echo $nomor ?>">
	<thead>
	
	
		<th colspan="8"><b><?php echo $query[$a]['kode_brg_wip']." - ".$query[$a]['nama_brg_wip']; ?></b></th>
	</tr>

	 <tr class="judulnya">
		 <th>Tgl</th>
		 <th>Ket</th>
		 <th>No Bukti</th>
		 <th >Bahan Baku (BB)</th>
		 	 <th >Perbaikan (TP)</th>
		 <th >Barang Jadi (KBJ)</th>
	
		 <th >Perbaikan (KP)</th>
		 <th >Retur Bahan Baku</th>
<!--
		 <th>Saldo<br>Per Warna</th>
		 <th>Saldo Global</th>
--->
	 </tr>
	</thead>
	<tbody>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		
<!--
			<td>&nbsp;</td>
			<td>&nbsp;</td>
--->
			
			<td>
			<?php $data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['saldo']."<br>";
				}
			}
		?>
			</td>
			<td align="right">
			<?php //$data_so_warna = $query[$a]['data_so_warna'];
			//if (is_array($data_so_warna)) {
			//	for($zz=0;$zz<count($data_so_warna);$zz++){ 
			//		echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['jum_stok_opname']."<br>";
			//	}
			//}
			//echo $query[$a]['saldo_akhir'];
		//	echo $query[$a]['tot_jum_stok_opname'];
		//~ echo $query[$a]['tot_jum_stok_opname'];
		?>
			</td>
		</tr>
			
<?php
		$data_tabel1 = $query[$a]['data_tabel1'];
		if (is_array($data_tabel1)) {
			for($j=0;$j<count($data_tabel1);$j++){
?>
	<tr>
		<td style="white-space:nowrap;"><?php echo "&nbsp;".$data_tabel1[$j]['tgl_sj'] ?></td>
		<td style="white-space:nowrap;"><?php if ($data_tabel1[$j]['masuk'] == "ya") { 
			if ($data_tabel1[$j]['masuk_bgs'] == '1' || $data_tabel1[$j]['masuk_perb'] == '1' ){
				echo "Masuk"; 
			}
		}
		else {
			echo "Keluar"; 
		}	
		?></td>
		<td><?php echo $data_tabel1[$j]['no_sj']; ?></td>
		<td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_bgs'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_perb'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>	
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_bgs'] == '1' ) {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		<!-- <td><?php /*if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_pengembalian_retur'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			} */
		 ?>
		 </td>-->
	
		 
		  <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_perb'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_bb'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 
		 		 
<!--
		 <td><?php
				$data_tabel1_saldo_perwarna = $data_tabel1[$j]['data_tabel1_saldo_perwarna'];
				if (is_array($data_tabel1_saldo_perwarna)) {
					$totglobal = $query[$a]['tot_jum_stok_opname'];
					$data_so_warna = $query[$a]['data_so_warna'];
					for($z=0;$z<count($data_tabel1_saldo_perwarna);$z++){
					echo "<br>".$data_tabel1_saldo_perwarna[$z]['nama_warna'].": ".$data_so_warna[$z]['saldo']+=$data_tabel1_saldo_perwarna[$z]['saldo']."<br>";
					
					$totglobal+=$data_tabel1_saldo_perwarna[$z]['saldo'];
					}
					echo "<br>Total: ".$totglobal;
				}
				//echo $data_tabel1[$j]['tot_saldo'];
			
			?>
		 </td>
		 <td align="right"><?php
				echo $data_tabel1[$j]['tot_saldo'];
			
			?>
		 </td>
--->
	</tr>
	<?php
			}
		}
	?>
		<tr>
			<td colspan="3" align="center"><b>Total/Bulan</b></td>
			<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_bgs']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_bgs'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
			<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_perb']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_perb'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_bgs']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_bgs'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<!--<td><?php /*$data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_pengembalian_retur1']."<br>";
				}
			} */
		?></td>-->
	
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_perb']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_perb'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_bb']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_bb'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		

<!--
		<td align="right"><?php 
			echo $query[$a]['gtot_saldo'];
		?></td>
-->
		</tr>
	</tbody>
	</table>
			
			<br><br>
	<?php
			$nomor++;
		}
	}
	?>

</div>

<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
	var jumbrg = $("#jumbrg").val();
    //var Contents = $('#sitabel').html();    
    var i = 1; var Contents = '';
    for (i=1; i<=jumbrg; i++) {
		Contents = Contents + $('#sitabel_'+i).html() + '<br><br>';    
	}
    
    //window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent(Contents) +  '</table>' );
  });
</script>
