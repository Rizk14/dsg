<h3>Data Permintaan Bahan Untuk Makloon</h3><br>
<a href="<? echo base_url(); ?>index.php/pb-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/pb-makloon/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	//$("#no").val('2');
	
	//generate_nomor();
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****ket*************************************
		var ket="#keterangan_"+n;
		var new_ket="#keterangan_"+no;
		$(ket, lastRow).attr("id", "keterangan_"+no);
		$(new_ket, lastRow).attr("name", "keterangan_"+no);		
		$(new_ket, lastRow).val('');				
		//*****end ket*************************************	
				
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		// var jenis_bhn= $('#jenis_bhn').val();
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		//var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/pb-makloon/cform/show_popup_brg/"+jenis_bhn+"/"+no+"/');";
		var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pb-makloon/cform/show_popup_brg/'+x+'/"+no+"/');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
	  
	$('#jenis_bhn').change(function(){
	  	    generate_nomor();
	  });
	

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	

function generate_nomor() {
	var jenis_bhn= $('#jenis_bhn').val();
	var jum_detail = $('#no').val()-1;
    $.getJSON("<?php echo base_url(); ?>index.php/pb-makloon/cform/generate_nomor/"+jenis_bhn, function(data) {
		$('#no_request').val(data);
		
    });
    
}

function cek_pb() {
	var no_request= $('#no_request').val();
	var tgl= $('#tgl_request').val();
	if (no_request == '') {
		alert("Nomor Request harus diisi..!");
		return false;
	}
	if (tgl == '') {
		alert("Tanggal Request harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#kode_'+k).val() == '') {
				alert("Data item barang tidak boleh ada yang kosong...!");
				return false;
			}
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pb-makloon/cform/updatedata" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_pb" value="<?php echo $query[0]['id'] ?>">

<?php $detail_pb = $query[0]['detail_pb']; 
	$no = 1;
	foreach ($detail_pb as $hitung) {
		$no++;
	}
?>
Edit Data

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">
<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
    <td width="15%">No Request</td>
    <td>
      <input name="no_request" type="text" id="no_request" size="20" maxlength="20" readonly="true" value="<?php echo $query[0]['no_pb_quilting'] ?>">
    </td>
    
  </tr>
  <tr>
    <td>Tanggal Request</td>
    <td>
	<label>
      <input name="tgl_request" type="text" id="tgl_request" size="10" value="<?php echo $query[0]['tgl'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_request" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_request,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="right">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
	      <th>Qty</th>
          <th>Keterangan</th>
        </tr>
		<?php $i=1;
        if (count($query[0]['detail_pb'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
        <?php
		} else {
			$detail_pb = $query[0]['detail_pb'];
			for($k=0;$k<count($detail_pb);$k++){
			?>
			
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
		  <td><select name="kel_brg_<?php echo $i ?>" id="kel_brg_<?php echo $i ?>">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $detail_pb[$k]['kode_brg'] ?>"/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=<?php echo $i ?>" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" 
           onclick="javascript: var x= $('#kel_brg_<?php echo $i ?>').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/pb-makloon/cform/show_popup_brg/'+x+'/<?php echo $i ?>');" type="button">
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $detail_pb[$k]['nama'] ?>" /></td>
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="10" maxlength="10" value="<?php echo $detail_pb[$k]['jumlah'] ?>" /></td>
          <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="30" maxlength="30" value="<?php echo $detail_pb[$k]['ket'] ?>" /></td>
        </tr>
        <?php $i++; } // end foreach 
		}
		?>
	</table>	
	
	</form>
      <div align="center"><br> 
        
        <input type="submit" name="submit2" value="Simpan" onclick="return cek_pb();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/pb-makloon/cform/view'">

      </div></td>
    </tr>

</table>
</div>
</form>
