<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<h3>Rekap Penerimaan Barang Per Item</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr>
		<th>No</th>
		 <th>No. Perk</th>
		 <th>Nama Barang</th>
		 <th>Satuan</th>
		<!-- <th>Supplier</th> 
		 <th>Harga</th> -->
		 <th>Total Qty</th>
		<!-- <th>Jml Total (Termasuk PPN)</th>
		 <th>PPN</th>
		 <th>Jml Total Diluar PPN</th> -->
	 </tr>
	</thead>
	<tbody>
		 <?php		$totalnya = 0;
				$pajaknya = 0;
				$total_diluarpajak = 0;
			if (is_array($query)) {
				$totalnya = 0;
				$pajaknya = 0;
				$total_diluarpajak = 0;
			 for($j=0;$j<count($query);$j++){
				 $nomor = $j+1;
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$nomor."</td>";
				 echo    "<td>".$query[$j]['kode_perk']."</td>";
				 echo    "<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>";
				 echo    "<td>".$query[$j]['satuan']."</td>";
			/*	 echo    "<td>".$query[$j]['supplier']."</td>";
				 echo    "<td align='right'>".$query[$j]['harga']."</td>"; */
				 echo    "<td align='right'>".$query[$j]['qty']."</td>";
			/*	 $totalnya+= $query[$j]['totalnya'];
				 echo    "<td align='right'>".number_format($query[$j]['totalnya'],2,',','.')."</td>";
				 $pajaknya+= $query[$j]['pajaknya'];
				 echo    "<td align='right'>".number_format($query[$j]['pajaknya'],2,',','.')."</td>";
				 $total_diluarpajak+= $query[$j]['total_diluarpajak'];
				 echo    "<td align='right'>".number_format($query[$j]['total_diluarpajak'],2,',','.')."</td>"; */
				 echo  "</tr>";					
		 	}
		   }
		 ?>
	<!--	 <tr>
			<td colspan="7" align="right"><b>TOTAL</b></td>
			<td align="right"><b><?php echo number_format($totalnya,2,',','.') ?></b></td>
			<td align="right"><b><?php echo number_format($pajaknya,2,',','.') ?></b></td>
			<td align="right"><b><?php echo number_format($total_diluarpajak,2,',','.') ?></b></td>
		 </tr> -->
 	</tbody>
</table><br>
</div>
