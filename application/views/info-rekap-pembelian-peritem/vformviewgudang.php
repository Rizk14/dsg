<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
   
   .judulnya {
		background-color:#DDD;
	}

</style>

<h3>Rekap Penerimaan Barang Per Item Berdasarkan Harga</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else if ($jenis_beli == 2) echo "Kredit"; else echo "Semua"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br>
Acuan Data: <?php if ($acuan == 1) echo "Range Tanggal SJ"; else echo "Range Tanggal Voucher Pelunasan"; ?><br>
Lokasi Gudang: <?php echo $nama_gudang ?> <br><br>
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-rekap-pembelian-peritem/cform/export_excel_gudang', $attributes); ?>
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="hidden" name="acuan" value="<?php echo $acuan ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $id_gudang ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?><br><br>
<i>*) Harga diluar PPN, dan berupa sudah dalam satuan konversi</i>
	<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		 <!--<th>No. Perk</th>-->
		 <th>Nama Barang</th>
		 <th>Harga</th>
		 <th>Satuan</th>
		 <th>Satuan<br>Konversi</th>
		 <th>Total Qty</th>
		 <th>Nilai</th>
		 <th>Supplier</th>
	 </tr>
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
			$totalnya = 0;
			 for($j=0;$j<count($query);$j++){
				 $nomor = $j+1;
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$nomor."</td>";
				 //echo    "<td>".$query[$j]['kode_perk']."</td>";
				 echo    "<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['harga'],4,',','.')."</td>";
				 echo    "<td>".$query[$j]['satuan']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['satuan_konversi']."</td>";
				 echo    "<td align='right'>".$query[$j]['qty']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['nilai'], 2, ',','.')."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['supplier']."</td>";
				 echo  "</tr>";		
				 $totalnya+= $query[$j]['nilai'];
		 	}
		   }
		 ?>
	<tr>
		<td colspan="6" align="right"><b>Grand Total Nilai (Rp.)</b></td>
		<td align="right"><b><?php echo number_format($totalnya, 2, ',','.') ?></b></td>
		<td>&nbsp;</td>
	</tr>
 	</tbody>
</table>
<br>
</div>
