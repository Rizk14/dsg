<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_voucher; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_voucher; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listvoucher/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlvoucherform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_voucher_no; ?> </td>
					<td width="1%">:</td>
					<td width="70%">
					  <input name="no_voucher" type="text" id="no_voucher" maxlength="14" value="<?=$no_voucher?>" readonly />
					  <input name="kode_sumber" type="hidden" id="kode_sumber" value="<?=$kode_sumber?>" />
					  <input name="i_voucher" type="hidden" id="i_voucher" value="<?=$i_voucher?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_kontrabon_tgl_kontrabon; ?> </td>
					<td>:</td>
					<td>
					  <input name="d_voucher_first" type="text" id="d_voucher_first" maxlength="10" value="<?=$tglvouchermulai?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_kontrabon_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_voucher_last" type="text" id="d_voucher_last" maxlength="10" value="<?=$tglvouchermulai?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_kontrabon_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
			    <td>
			    <div id="title-box2"><?php echo $form_title_detail_voucher; ?></div></td>	
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

				  <tr>
					<td width="1%" class="tdatahead">NO</td>
					<td width="18%" class="tdatahead"><?php echo strtoupper($list_voucher_kd_sumber); ?> </td>
					<td width="12%" class="tdatahead"><?php echo strtoupper($list_voucher_no); ?> </td>
					<td width="12%" class="tdatahead"><?php echo strtoupper($list_voucher_tgl); ?> </td>
					<td width="14%" class="tdatahead"><?php echo strtoupper($list_voucher_received); ?> </td>
					<td width="14%" class="tdatahead"><?php echo strtoupper($list_voucher_approved); ?></td>
					<td width="16%" class="tdatahead"><?php echo strtoupper($list_voucher_status_approve); ?></td>
					<td width="16%" class="tdatahead"><?php echo strtoupper($list_voucher_status_cek); ?></td>
					<td width="20%" class="tdatahead"><?php echo strtoupper($list_voucher_total); ?></td>
					<td width="8%" class="tdatahead" align="center"><?php echo strtoupper($link_aksi); ?></td>
				  </tr>
				  
				  <?php
				  
				  $no	= 1;
				  $cc	= 1;
				  $arr	= 0;
				  
				  if(sizeof($query)>0) {

					  foreach($query as $row) {
					  
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";				
	
						$link_act	= "<td align=\"center\" width=\"10%\"><a href=\"javascript:void(0)\" onclick='show(\"listvoucher/cform/edit/".$row->i_voucher."/".$row->i_voucher_no."\",\"#content\")' title=\"Edit Voucher\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Voucher\" border=\"0\"></a>&nbsp;
						<a href=\"javascript:void(0)\" onclick='show(\"listvoucher/cform/undo/".$row->i_voucher."/".$row->i_voucher_no."\",\"#content\")' title=\"Cancel Voucher\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Voucher\" border=\"0\"></a>
						</td>";		
						
						$exp_voucher = explode("-",$row->d_voucher,strlen($row->d_voucher));
						
						$fapprove	= ($row->f_approved=='t')?'telah diapprov':'blm diapprov';
						$fcheck		= ($row->f_checked=='t')?'telah dicek':'blm dicek';

						if($turi1=='0')
							$turi1 = $row->i_voucher;
						
						if($turi2=='0')
							$turi2 = $row->i_voucher_no;
													
						$lvoucher	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no."</td>
								<td>".$row->kodesumber." - ".$row->e_description."</td>
								<td>".$row->i_voucher_no."&nbsp;<a href=\"javascript:void(0)\" onclick='show(\"listvoucher/cform/detail/".$turi1."/".$turi2."/".$turi3."/".$turi4."/".$turi5."\",\"#content\")' title=\"Detail Voucher\"><img src=".base_url()."asset/theme/images/magnifier.png width=16 height=16 alt=\"Detail Voucher\" border=\"0\"></a></td>
								<td>".$exp_voucher[2]."/".$exp_voucher[1]."/".$exp_voucher[0]."</td>
								<td>".$row->e_recieved."</td>
								<td>".$row->e_approved."</td>
								<td align=\"center\">".$fapprove."</td>
								<td align=\"center\">".$fcheck."</td>
								<td align=\"right\">".number_format($row->v_total_voucher,'2','.',',')."</td>
								$link_act
							  </tr>";
							  
							  $no+=1;
							  $cc+=1;
							  $arr+=1;
					  }
					  
					  echo $lvoucher;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>					
				  </tr>

				</table></td>
			  </tr>
			  <tr>
			  	<td align="center"><?php echo $create_link; ?></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>	
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
					 <input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listvoucher/cform/'">

				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
