<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_voucher; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_voucher; ?></td></tr>
   <tr>
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
	    
		<?php
		echo $this->pquery->form_remote_tag(array('url'=>'listvoucher/cform/actedit','update'=>'#content','type'=>'post'));
		?>
		
		<div id="masterlvoucherform">
		
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_voucher_kd_sumber; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="kode_sumber" type="text" id="kode_sumber" maxlength="14" readonly value="<?php echo $kodesumber; ?>" readonly />
					  <input name="i_kode_sumber" id="i_kode_sumber" type="hidden" value="<?php echo $i_voucher_code; ?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_no; ?></td>
					<td>:</td>
					<td>
					  <input name="no_voucher" type="text" id="no_voucher" maxlength="14" value="<?php echo $i_voucher_no; ?>" readonly />
					  <input name="i_voucher" type="hidden" id="i_voucher" value="<?php echo $i_voucher; ?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_tgl; ?></td>
					<td>:</td>
					<td>
					  <input name="tgl_voucher" type="text" id="tgl_voucher" maxlength="10" value="<?php echo $d_voucher; ?>" readonly />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_kd_perusahaan; ?></td>
					<td>:</td>
					<td>
					  <input name="kode_perusahaan" type="text" id="kode_perusahaan" maxlength="100" value="<?php echo $i_company_code; ?>" readonly />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_deskripsi; ?></td>
					<td>:</td>
					<td>
					  <input name="deskripsi_voucher" type="text" id="deskripsi_voucher" maxlength="255" value="<?php echo $e_description; ?>" readonly />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_total; ?></td>
					<td>:</td>
					<td>
					  <input type="text" name="total_nilai_voucher" id="total_nilai_voucher" maxlength="100" value="<?php echo number_format($v_total_voucher,'2','.',','); ?>" readonly />
					  <input type="hidden" name="total_nilai_voucherhidden" id="total_nilai_voucherhidden" value="<?php echo $v_total_voucher; ?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_received; ?></td>
					<td>:</td>
					<td>
					  <input name="recieved_voucher" type="text" id="recieved_voucher" maxlength="200" value="<?php echo $e_recieved; ?>" readonly />
					</td>
				  </tr>				  
				  <tr>
					<td><?php echo $list_voucher_approved; ?></td>
					<td>:</td>
					<td>
					<?php
					$val1 = $f_approve_dept=='t'?'checked':'';
					$val2 = $f_approve_dept=='f'?'checked':'';
					?>
					  <input name="approve_voucher" type="text" id="approve_voucher" maxlength="200" value="<?php echo $e_approved; ?>" readonly />&nbsp;
					  <input name="app_dept" id="app_dept" type="radio" value="t" <?php echo $val1; ?> disabled />Departement&nbsp;
					  <input name="app_dept" id="app_dept" type="radio" value="f" <?php echo $val2; ?> disabled />Services
					</td>
				  </tr>
				</table></td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_voucher; ?></div></td>	
			</tr>
			<tr>
			  <tr>
				<td><table width="70%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="3%" class="tdatahead">NO</td>
					<td width="10%" class="tdatahead"><?php echo $list_voucher_no_kontrabon; ?></td>
					<td width="10%" class="tdatahead"><?php echo $list_voucher_tgl_kontrabon; ?> </td>
					<td width="15%" class="tdatahead"><?php echo $list_voucher_total_kontrabon; ?></td>
					<td width="15%" class="tdatahead"><?php echo $list_voucher_piutang; ?></td>
					<td width="16%" class="tdatahead"><input type="checkbox" name="ckall" id="ckall" <?php if($f_nilai_manual=='t'){ echo "checked"; } ?> readonly />&nbsp;<?php echo $list_voucher_manual_input; ?>
					&nbsp;<?php echo $list_voucher_nilai_voucher_detail; ?>
					</td>
				  </tr>
				  <tr>
					<td colspan="6">
						<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
						<?php
						if(isset($isi) && sizeof($isi)>0){
							echo $isi;
						}
						?>
						</table>				  
					</td>	
				  </tr>
				  <tr>
					<td colspan="6">&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
				  <input name="f_nilai_manual" id="f_nilai_manual" type="hidden" value="<?php echo $f_nilai_manual; ?>" />	

				  <!-- <input name="btnbatal" id="btnbatal" value="<?php //echo $button_batal; ?>" type="button" onclick="show('listvoucher/cform','#content')" /> -->

				  <?php 
				  $uri =  $turi1."/".$turi2."/".$turi3."/".$turi4."/".$turi5."/";						
				  echo "<a href=\"javascript:void(0)\" onclick='show(\"listvoucher/cform/backlistvoucher/".$uri."\",\"#content\")' title=\"Kembali\"><img src=".base_url()."asset/theme/images/icon_exit.png width=32 height=28 alt=\"Kembali\" border=\"0\"></a><br>Kembali";
				  ?>
				  
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
