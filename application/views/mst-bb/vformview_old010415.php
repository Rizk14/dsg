<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Data Bahan Baku/Pembantu</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-bb/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bb/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-bb/cform/cari'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Jenis Bahan  </td>
		<td><select name="jenis_bahan" id="jenis_bahan">
			<option value="0" <?php if ($ejenis_bahan == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php foreach ($jns_bhn as $jns) { ?>
			<option value="<?php echo $jns->id ?>" <?php if ($jns->id == $ejenis_bahan) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nj_brg."] ". $jns->kode."-".$jns->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Lokasi Gudang</td>
		<td><select name="gudang" id="gudang">
		<option value="0" <?php if ($cgudang == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php foreach ($list_gudang as $gud) { ?>
			<option value="<?php echo $gud->id ?>" <?php if ($cgudang == $gud->id) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><select name="statusnya" id="statusnya">
		<option value="0" <?php if ($cstatus == '') { ?> selected="true" <?php } ?> >- All -</option>
		<option value="t" <?php if ($cstatus == 't') { ?> selected="true" <?php } ?> >Aktif</option>
		<option value="f" <?php if ($cstatus == 'f') { ?> selected="true" <?php } ?> >Non-Aktif</option>
		</select></td>
	</tr>
	<tr>
		<td>Keyword Pencarian</td>
		<td><input type="text" name="cari" size="30" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isi">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		 <th>Jenis Bahan</th>
		 <th>Kode Brg</th>
		 <th>Nama Barang</th>
		 <th>Nama Barang ke Supplier</th>
		 <th>Satuan</th>
		 <th>Deskripsi</th>
		 <th>Lokasi Gudang</th>
		 <th>Status</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
		 
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo "<td>$i</td>";
				 echo    "<td>[$row->nj_brg] $row->kode_jenis - $row->nama_jenis</td>";
				 echo    "<td>$row->kode_brg</td>";
				 echo    "<td>$row->nama_brg</td>";
				 echo    "<td>$row->nama_brg_supplier</td>";
				 echo    "<td>$row->nama_satuan</td>";
				 echo    "<td>$row->deskripsi</td>";
				 
				 if ($row->id_gudang != '') {
					 $query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
					$hasilrow = $query3->row();
					$ekode_gudang	= $hasilrow->kode_gudang;
					$eid_gudang	= $hasilrow->id;
					$enama_gudang	= $hasilrow->nama;
					$enama_lokasi	= $hasilrow->nama_lokasi;
					
					 echo    "<td>[$enama_lokasi] $ekode_gudang - $enama_gudang</td>";
				 }
				 else {
					echo "<td>&nbsp;</td>";	 
				 }
				 if ($row->status_aktif == 't')
					echo    "<td>Aktif</td>";
				 else
					echo    "<td>Non-Aktif</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 $ada = 0;
				 // cek apakah data kode sudah dipake di PP, OP, ataupun SJ
				 $query3	= $this->db->query(" SELECT id FROM tm_pp_detail WHERE kode_brg = '$row->kode_brg' ");
				 if ($query3->num_rows() > 0){ 
					//$hasilrow = $query3->row();
					//$jum_stok	= $hasilrow->jstok;
					$ada = 1; 
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_op_detail WHERE kode_brg = '$row->kode_brg' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				
				 $query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE kode_brg = '$row->kode_brg' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				echo "<td align=center><a href=".base_url()."index.php/mst-bb/cform/index/$row->kode_brg/".$cur_page."/".$is_cari."/".$ejenis_bahan."/".$cgudang."/".$cstatus."/".$cari." \" id=\"$row->kode_brg\">Edit</a>";
				if ($ada == 0) {
					echo "&nbsp; <a href=".base_url()."index.php/mst-bb/cform/delete/$row->kode_brg/".$cur_page."/".$is_cari."/".$ejenis_bahan."/".$cgudang."/".$cstatus."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				}
				echo "</td>";

				 echo  "</tr>";
				 $i++;
		 	}
		 ?>
 	</tbody>
</table><br>
<? echo $this->pagination->create_links();?>
</div>
