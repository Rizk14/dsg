<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Data Bahan Baku/Pembantu</h3><br>
<?php if ($this->session->userdata('gid') != 3) { ?><a href="<?php echo base_url(); ?>index.php/mst-bb/cform">Tambah Data</a>&nbsp;&nbsp; <?php } ?><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<p style="color:red;">*Untuk proses export lakukan proses pencarian terlebih dahulu</p>
<?php echo form_open('mst-bb/cform/cari'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Kelompok Barang</td>
		<td><select name="kelompok_barang" id="kelompok_barang">
			<option value="0" <?php if ($ekelompok_barang == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php 
		
		foreach ($kel_brg as $kel) { ?>
			<option value="<?php echo $kel->id ?>" <?php if ($kel->id == $ekelompok_barang) { ?> selected="true" <?php } ?> ><?php echo  $kel->kode."-".$kel->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Jenis Barang</td>
		<td><select name="jenis_barang" id="jenis_barang">
			<option value="0" <?php if ($ejenis_barang == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php foreach ($jns_brg as $jns) { ?>
			<option value="<?php echo $jns->id ?>" <?php if ($jns->id == $ejenis_barang) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nama_kel_brg."] ". $jns->kode."-".$jns->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Lokasi Gudang</td>
		<td><select name="gudang" id="gudang">
		<option value="0" <?php if ($cgudang == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php foreach ($list_gudang as $gud) { ?>
			<option value="<?php echo $gud->id ?>" <?php if ($cgudang == $gud->id) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><select name="statusnya" id="statusnya">
		<option value="0" <?php if ($cstatus == '') { ?> selected="true" <?php } ?> >- All -</option>
		<option value="t" <?php if ($cstatus == 't') { ?> selected="true" <?php } ?> >Aktif</option>
		<option value="f" <?php if ($cstatus == 'f') { ?> selected="true" <?php } ?> >Non-Aktif</option>
		</select></td>
	</tr>
	<tr>
		<td>Keyword Pencarian</td>
		<td><input type="text" name="cari" size="30" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit1" value="Cari"></td>
		<td colspan="2"><input type="submit" name="submit2" value="Export"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isi">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		 <th>Jenis Barang</th>
		 <th>Kode</th>
		 <th>Nama Barang</th>
		 <th>Nama Barang ke Supplier</th>
		 <th>Satuan</th>
		 <th>Satuan Konv</th>
		 <th>Angka Faktor<br>Konv</th>
		 <th>Rumus Konv</th>
		 <th>Deskripsi</th>
		 <th>Lokasi Gudang</th>
		 <th>Status</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
		 
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo "<td>$i</td>";
				 echo    "<td>[$row->nama_kel_brg] $row->kj_brg - $row->nj_brg</td>";
				 echo    "<td>$row->kode_brg</td>";
				 echo    "<td>$row->nama_brg</td>";
				 echo    "<td>$row->nama_brg_supplier</td>";
				 echo    "<td>$row->nama_satuan</td>";
				 // 29-10-2014
				 if ($row->id_satuan_konversi != '0') {
					 $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id='".$row->id_satuan_konversi."' ");
					 if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konversi = $hasilrow->nama;
					 }
				 }
				 else
					$nama_satuan_konversi = '';
				 echo    "<td>".$nama_satuan_konversi."</td>";
				 
				 echo    "<td align='right'>$row->angka_faktor_konversi</td>";
				 
				 //echo    "<td>$row->rumus_konversi</td>";
				 echo "<td>";
				 if ($row->rumus_konversi == '0')
					echo "Tidak Ada";
				 else if ($row->rumus_konversi == '1')
					echo "Dikali (Qty Konv = Qty Sat Awal x Angka Faktor)";
				 else if ($row->rumus_konversi == '2')
					echo "Dibagi (Qty Konv = Qty Sat Awal / Angka Faktor)";
				 else if ($row->rumus_konversi == '3')
					echo "Ditambah (Qty Konv = Qty Sat Awal + Angka Faktor)";
				 else
					echo "Dikurang (Qty Konv = Qty Sat Awal - Angka Faktor)";
				 echo "</td>";
				 
				 echo    "<td>$row->deskripsi</td>";
				 
				 if ($row->id_gudang != '') {
					 $query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row->id_gudang' ");
					$hasilrow = $query3->row();
					$ekode_gudang	= $hasilrow->kode_gudang;
					$eid_gudang	= $hasilrow->id;
					$enama_gudang	= $hasilrow->nama;
					$enama_lokasi	= $hasilrow->nama_lokasi;
					
					 echo    "<td>[$enama_lokasi] $ekode_gudang - $enama_gudang</td>";
				 }
				 else {
					echo "<td>&nbsp;</td>";	 
				 }
				 if ($row->status_aktif == 't')
					echo    "<td>Aktif</td>";
				 else
					echo    "<td>Non-Aktif</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 $ada = 0;
				 // cek apakah data kode sudah dipake di PP, OP, SJ, bonm Keluar, bonm masuk
				 $query3	= $this->db->query(" SELECT id FROM tm_pp_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){ 
					//$hasilrow = $query3->row();
					//$jum_stok	= $hasilrow->jstok;
					$ada = 1; 
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_op_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				
				 $query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // 21-09-2015
				 $query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluar_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasuklain_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukmanual_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				echo "<td align=center>";
				if ($this->session->userdata('gid') != 3) {
					echo "<a href=".base_url()."index.php/mst-bb/cform/index/$row->id/".$cur_page."/".$is_cari."/".$ejenis_barang."/".$cgudang."/".$cstatus."/".$cari." \" id=\"$row->id\">Edit</a>";

					if ($ada == 0) {
						echo "&nbsp; <a href=".base_url()."index.php/mst-bb/cform/delete/$row->id/".$cur_page."/".$is_cari."/".$ejenis_barang."/".$cgudang."/".$cstatus."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					}
				}
				echo "</td>";

				 echo  "</tr>";
				 $i++;
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
