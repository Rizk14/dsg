<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var kode= $('#kode').val();
	var kode2= $('#kode2').val();
	var nama= $('#nama').val();
	var id_jenis= $('#id_jenis').val();
	var id_jenis_bhn= $('#id_jenis_bhn').val();
	var item= $('#item').val();
	var motif= $('#motif').val();
	var warna= $('#warna').val();

	if (id_jenis == '') {
		alert("Jenis Barang harus dipilih..!");
		$('#kode_jenis').focus();
		return false;
	}
	if (id_jenis_bhn == '') {
		alert("Jenis Bahan harus dipilih..!");
		$('#kode_jenis_bhn').focus();
		return false;
	}
	
	if (item == 'xx') {
		alert("Item harus dipilih..!");
		$('#item').focus();
		return false;
	}
	if (motif == 'xx') {
		alert("Motif harus dipilih..!");
		$('#motif').focus();
		return false;
	}
	if (warna == 'xx') {
		alert("Warna harus dipilih..!");
		$('#warna').focus();
		return false;
	}
	if (kode == '') {
		alert("Kode Barang harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (kode2 == '') {
		alert("Digit belakang kode harus diisi..!");
		$('#kode2').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Barang harus diisi..!");
		$('#nama').focus();
		return false;
	}
	if (kode2.length < 4) {
		alert("Digit belakang kode harus 4 digit..!");
		$('#kode2').focus();
		return false;
	}
}

$(function()
{
	var kode_kel = jQuery('#kel_brg').val();
	var kode_brg = jQuery('#kode').val();
	var goedit = jQuery('#goedit').val();
	var gabung = kode_kel;
	if (goedit != '1')
		jQuery('#kode').val(gabung);
	
	$('#pilih_jenis').click(function(){

	  	    var id_kel_brg= jQuery('#kel_brg').val();

			var urlnya = "<?php echo base_url(); ?>index.php/mst-bb/cform/show_popup_jenis/"+id_kel_brg;

			openCenteredWindow(urlnya);

	  });
	 
	 $('#pilih_jenis_bhn').click(function(){
	  	    var id_jns_brg= jQuery('#id_jenis').val();
			var urlnya = "<?php echo base_url(); ?>index.php/mst-bb/cform/show_popup_jenis_bhn/"+id_jns_brg;
			
			if (id_jns_brg == '')
				alert ("Pilih dulu jenis barangnya...");
			else
				openCenteredWindow(urlnya);
	  }); 
	  
	  $('#kel_brg').change(function(){
	  	    var kode_kel = jQuery('#kel_brg').val();
	  	    var kode_brg = jQuery('#kode').val();
	  	    var gabung = kode_kel;
	  	    jQuery('#kode').val(gabung);
	  	    
	  	    $("#id_jenis").val('');	
	  	    $("#kode_jenis").val('');	
	  	    $("#id_jenis_bhn").val('');	
	  	    $("#kode_jenis_bhn").val('');	
	  	    $("#kode2").val('');	
	  	    $("#nama").val('');	
	  	    
	  	    $('#item option[value=xx]').attr('selected', 'selected');
	  	    $('#motif option[value=xx]').attr('selected', 'selected');
	  	    $('#warna option[value=xx]').attr('selected', 'selected');
	  });
	  
	  $('#item').change(function(){
	  	    var kode_jenis = jQuery('#kode_jenis').val();
	  	    var kode_motif = jQuery('#motif').val();
	  	    var kode_item = jQuery('#item').val();
	  	    var kode_brg = jQuery('#kode').val();
	  	    
	  	    var str2= kode_brg+kode_item;
	  	    if (str2.length < 6) {
				alert("Inputkan dulu jenis barang dan jenis bahannya");
				$('#item option[value=xx]').attr('selected', 'selected');
				return false;
			}
			else {
				if (kode_brg.length == 6) {
					var str3 = kode_brg.substr(0,4);
					str3+= kode_item;
					jQuery('#kode').val(str3);
				}
				else if (kode_brg.length == 7) {
					var str3 = kode_brg.substr(0,5);
					str3+= kode_item;
					jQuery('#kode').val(str3);
				}
				else if (kode_brg.length == 4 || (kode_brg.length == 5) ) {
					jQuery('#kode').val(str2);
				}
				else if (kode_brg.length > 7) {
					// jika udh milih motif dan warna tapi item ada yg salah, berarti kode_brg harus disubstr
					// jenis brg ada yg 1 digit, ada yg 2 digit, sampe sini 11:02
					var pkode_jenis = kode_jenis.substr(0,2);
					trimpkode = pkode_jenis.trim(); //alert(trimpkode.length);
					
					if (trimpkode.length == 2 && kode_motif.length == 2) {
						var str3 = kode_brg.substr(0,5);
						str3+= kode_item;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 2 && kode_motif.length == 3) {
						var str3 = kode_brg.substr(0,5);
						str3+= kode_item;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 1 && kode_motif.length == 2) {
						var str3 = kode_brg.substr(0,4);
						str3+= kode_item;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 1 && kode_motif.length == 3) {
						var str3 = kode_brg.substr(0,4);
						str3+= kode_item;
						jQuery('#kode').val(str3);
					}
				}
				
				$('#motif option[value=xx]').attr('selected', 'selected');
				$('#warna option[value=xx]').attr('selected', 'selected');
			}
	  	    
	  	    //===============================
	  	    	  	    	  	    
	  });
	  
	  $('#motif').change(function(){
	  	    var kode_motif = jQuery('#motif').val();
	  	    var kode_brg = jQuery('#kode').val();
	  	    var kode_jenis = jQuery('#kode_jenis').val();
	  	    var pkode_jenis = kode_jenis.substr(0,2);
			var trimpkode = pkode_jenis.trim();
	  	    
	  	    var str2= kode_brg+kode_motif;
	  	    if (str2.length < 8) {
				alert("Inputkan dulu jenis barang, jenis bahan, dan itemnya");
				$('#motif option[value=xx]').attr('selected', 'selected');
				return false;
			}
			else {
				// =====================
				if (trimpkode.length == 2 && kode_motif.length == 3) {
					var str3 = kode_brg.substr(0,7);
					str3+= kode_motif;
					jQuery('#kode').val(str3);
				}
				else if (trimpkode.length == 1 && kode_motif.length == 3) {
					var str3 = kode_brg.substr(0,6);
					str3+= kode_motif;
					jQuery('#kode').val(str3);
				}
				else if (trimpkode.length == 1 && kode_motif.length == 2) {
					var str3 = kode_brg.substr(0,6);
					str3+= kode_motif;
					jQuery('#kode').val(str3);
				}
				else if (trimpkode.length == 2 && kode_motif.length == 2) {
					var str3 = kode_brg.substr(0,7);
					str3+= kode_motif;
					jQuery('#kode').val(str3);
				}
				// =====================
				
			/*	if (kode_brg.length == 8) {
					var str3 = kode_brg.substr(0,6);
					str3+= kode_motif;
					jQuery('#kode').val(str3);
				}
				else if (kode_brg.length == 9) {
					var str3 = kode_brg.substr(0,7);
					str3+= kode_motif;
					jQuery('#kode').val(str3);
				}
				else if (kode_brg.length == 6 || (kode_brg.length == 7) ) {
					jQuery('#kode').val(str2);
				} */
				else if (kode_brg.length > 10) {
					// jika udh milih semua tapi motif ada yg salah, berarti kode_brg harus disubstr
				/*	var pkode_jenis = kode_jenis.substr(0,2);
					
					if (pkode_jenis.length == 2) {
						var str3 = kode_brg.substr(0,6);
						str3+= kode_motif;
						jQuery('#kode').val(str3);
					}
					else {
						var str3 = kode_brg.substr(0,7);
						str3+= kode_motif;
						jQuery('#kode').val(str3);
					} */
					
					if (trimpkode.length == 2 && kode_motif.length == 3) {
						var str3 = kode_brg.substr(0,7);
						str3+= kode_motif;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 1 && kode_motif.length == 3) {
						var str3 = kode_brg.substr(0,6);
						str3+= kode_motif;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 1 && kode_motif.length == 2) {
						var str3 = kode_brg.substr(0,6);
						str3+= kode_motif;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 2 && kode_motif.length == 2) {
						var str3 = kode_brg.substr(0,7);
						str3+= kode_motif;
						jQuery('#kode').val(str3);
					}
				}
				
				$('#warna option[value=xx]').attr('selected', 'selected');
			}
	  	    	  	    	  	    
	  });
	  
	  $('#warna').change(function(){
	  	    var kode_warna = jQuery('#warna').val();
	  	    var kode_brg = jQuery('#kode').val();
	  	    var kode_jenis = jQuery('#kode_jenis').val();
	  	    var kode_motif = jQuery('#motif').val();
	  	    var pkode_jenis = kode_jenis.substr(0,2);
			var trimpkode = pkode_jenis.trim();
	  	    
	  	    var str2= kode_brg+kode_warna;
	  	    if (str2.length < 10) {
				alert("Inputkan dulu jenis barang, jenis bahan, item, dan motifnya");
				$('#warna option[value=xx]').attr('selected', 'selected');
				return false;
			}
			else {
				// ------------------------------
				if (trimpkode.length == 2 && kode_motif.length == 3) {
					var str3 = kode_brg.substr(0,10);
					str3+= kode_warna;
					jQuery('#kode').val(str3);
				}
				else if (trimpkode.length == 1 && kode_motif.length == 3) {
					var str3 = kode_brg.substr(0,9);
					str3+= kode_warna;
					jQuery('#kode').val(str3);
				}
				else if (trimpkode.length == 1 && kode_motif.length == 2) {
					var str3 = kode_brg.substr(0,8);
					str3+= kode_warna;
					jQuery('#kode').val(str3);
				}
				else if (trimpkode.length == 2 && kode_motif.length == 2) {
					var str3 = kode_brg.substr(0,9);
					str3+= kode_warna;
					jQuery('#kode').val(str3);
				}
				
				//-==============================
				
			/*	if (kode_brg.length == 10) {
					var str3 = kode_brg.substr(0,8);
					str3+= kode_warna;
					jQuery('#kode').val(str3);
				}
				else if (kode_brg.length == 11) {
					var str3 = kode_brg.substr(0,9);
					str3+= kode_warna;
					jQuery('#kode').val(str3);
				} */
				/*if (kode_brg.length == 8 || kode_brg.length == 9 || kode_brg.length == 10 ) {
					jQuery('#kode').val(str2);
				}*/
				
				if (kode_brg.length > 12) {
					// jika udh milih semua tapi warna ada yg salah, berarti kode_brg harus disubstr
				/*	var pkode_jenis = kode_jenis.substr(0,2);
					
					if (pkode_jenis.length == 2) {
						var str3 = kode_brg.substr(0,8);
						str3+= kode_warna;
						jQuery('#kode').val(str3);
					}
					else {
						var str3 = kode_brg.substr(0,9);
						str3+= kode_warna;
						jQuery('#kode').val(str3);
					} */
					
					if (trimpkode.length == 2 && kode_motif.length == 3) {
						var str3 = kode_brg.substr(0,10);
						str3+= kode_warna;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 1 && kode_motif.length == 3) {
						var str3 = kode_brg.substr(0,9);
						str3+= kode_warna;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 1 && kode_motif.length == 2) {
						var str3 = kode_brg.substr(0,8);
						str3+= kode_warna;
						jQuery('#kode').val(str3);
					}
					else if (trimpkode.length == 2 && kode_motif.length == 2) {
						var str3 = kode_brg.substr(0,9);
						str3+= kode_warna;
						jQuery('#kode').val(str3);
					}
				}
				
				jQuery('#kode2').focus();
			}	  	    	  	    
	  });
	  
});
	
</script>

<h3>Data Bahan Baku/Pembantu</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-bb/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bb/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
	echo form_open('mst-bb/cform/submit', $attributes); ?>
<?php if ($edit == '1') { ?> 
	<input type="hidden" name="goedit" id="goedit" value="1"> <input type="hidden" name="kodeedit" value="<?php echo $ekode ?>">
	<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
	<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
	<input type="hidden" name="cjenis_bahan" value="<?php echo $cjenis_bahan ?>">
	<input type="hidden" name="cgudang" value="<?php echo $cgudang ?>">
	<input type="hidden" name="cstatus" value="<?php echo $cstatus ?>">
	<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<?php } ?>
	<table>
		<?php
		if ($edit != '1') {
		?>
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kel_brg" id="kel_brg" onkeyup="this.blur();this.focus();">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($kel->kode == $ekode_kel) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td> <input type="text" name="kode_jenis" id="kode_jenis" value="<?php echo $ekode_jenis." - ".$enama_jenis ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="<?php echo $eid_jenis_brg ?>">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang">
			</td>
		</tr>
		<tr>
			<td>Jenis Bahan</td>
			<td> <input type="text" name="kode_jenis_bhn" id="kode_jenis_bhn" value="<?php echo $ekode_jenis_bhn." - ".$enama_jenis_bhn ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis_bhn" id="id_jenis_bhn" value="<?php echo $eid_jenis_bhn ?>">
			<input name="pilih_jenis_bhn" id="pilih_jenis_bhn" value="..." type="button" title="browse data jenis bahan">
			</td>
		</tr>
		<tr>
			<td>Item</td>
			<td> <select name="item" id="item">
				<option value="xx">- Pilih -</option>
				<option value="00" <?php if ($eitem == "00") { ?> selected="true" <?php } ?>>00 - Tidak Ada</option>
				<?php foreach ($item as $itemnya) { ?>
					<option value="<?php echo $itemnya->kode ?>" <?php if ($itemnya->kode == $eitem) { ?> selected="true" <?php } ?> ><?php echo $itemnya->kode." - ".$itemnya->nama ?></option>
				<?php } ?>
				</select></td>
		</tr>
		
		<tr>
			<td>Motif/Ukuran</td>
			<td> <select name="motif" id="motif">
				<option value="xx">- Pilih -</option>
				<option value="00" <?php if ($emotif == "00") { ?> selected="true" <?php } ?> >00 - Tidak Ada</option>
				<?php foreach ($motif as $motifnya) { ?>
					<option value="<?php echo $motifnya->kode ?>" <?php if ($motifnya->kode == $emotif) { ?> selected="true" <?php } ?> ><?php echo $motifnya->kode." - ". $motifnya->nama ?></option>
				<?php } ?>
				</select></td>
		</tr>
		
		<tr>
			<td>Warna</td>
			<td> <select name="warna" id="warna">
				<option value="xx">- Pilih -</option>
				<option value="00" <?php if ($ewarna == "00") { ?> selected="true" <?php } ?>>00 - Tidak Ada</option>
				<?php foreach ($warna as $warnanya) { ?>
					<option value="<?php echo $warnanya->kode ?>" <?php if ($warnanya->kode == $ewarna) { ?> selected="true" <?php } ?> ><?php echo $warnanya->kode." - ". $warnanya->nama ?></option>
				<?php } ?>
				</select></td>
		</tr>
		
		<tr>
			<td>Kode Barang</td>
			<td> <input type="text" name="kode" id="kode" value="<?php echo $ekode1 ?>" maxlength="10" size="10" readonly="true">&nbsp;
			<input type="text" name="kode2" id="kode2" value="<?php echo $ekode2 ?>" maxlength="4" size="4">&nbsp;
			<i>* Ket: teks inputan yg ke-1 ini dari atribut barang, kemudian teks inputan yg ke-2 untuk nomor urutnya</i>
			</td>
		</tr>
		<?php 
		}
		else {
		?>
		<!-- ini jika edit == 1 -->
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kel_brg" id="kel_brg" onkeyup="this.blur();this.focus();" disabled>
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($kel->kode == $ekode_kel) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td> <input type="text" name="kode_jenis" id="kode_jenis" value="<?php echo $ekode_jenis." - ".$enama_jenis ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="<?php echo $eid_jenis_brg ?>">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang" disabled>
			</td>
		</tr>
		<tr>
			<td>Jenis Bahan</td>
			<td> <input type="text" name="kode_jenis_bhn" id="kode_jenis_bhn" value="<?php echo $ekode_jenis_bhn." - ".$enama_jenis_bhn ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis_bhn" id="id_jenis_bhn" value="<?php echo $eid_jenis_bhn ?>">
			<input name="pilih_jenis_bhn" id="pilih_jenis_bhn" value="..." type="button" title="browse data jenis bahan" disabled>
			</td>
		</tr>
		<tr>
			<td>Item</td>
			<td> <select name="item" id="item" disabled>
				<option value="xx">- Pilih -</option>
				<option value="00" <?php if ($eitem == "00") { ?> selected="true" <?php } ?>>00 - Tidak Ada</option>
				<?php foreach ($item as $itemnya) { ?>
					<option value="<?php echo $itemnya->kode ?>" <?php if ($itemnya->kode == $eitem) { ?> selected="true" <?php } ?> ><?php echo $itemnya->kode." - ".$itemnya->nama ?></option>
				<?php } ?>
				</select></td>
		</tr>
		
		<tr>
			<td>Motif/Ukuran</td>
			<td> <select name="motif" id="motif" disabled>
				<option value="xx">- Pilih -</option>
				<option value="00" <?php if ($emotif == "00") { ?> selected="true" <?php } ?> >00 - Tidak Ada</option>
				<?php foreach ($motif as $motifnya) { ?>
					<option value="<?php echo $motifnya->kode ?>" <?php if ($motifnya->kode == $emotif) { ?> selected="true" <?php } ?> ><?php echo $motifnya->kode." - ". $motifnya->nama ?></option>
				<?php } ?>
				</select></td>
		</tr>
		
		<tr>
			<td>Warna</td>
			<td> <select name="warna" id="warna" disabled>
				<option value="xx">- Pilih -</option>
				<option value="00" <?php if ($ewarna == "00") { ?> selected="true" <?php } ?>>00 - Tidak Ada</option>
				<?php foreach ($warna as $warnanya) { ?>
					<option value="<?php echo $warnanya->kode ?>" <?php if ($warnanya->kode == $ewarna) { ?> selected="true" <?php } ?> ><?php echo $warnanya->kode." - ". $warnanya->nama ?></option>
				<?php } ?>
				</select></td>
		</tr>
		
		<tr>
			<td>Kode Barang</td>
			<td> <input readonly="true" type="text" name="kode" id="kode" value="<?php echo $ekode1 ?>" maxlength="10" size="10">&nbsp;
			<input readonly="true" type="text" name="kode2" id="kode2" value="<?php echo $ekode2 ?>" maxlength="4" size="4">&nbsp;
			<i>* Ket: teks inputan yg ke-1 ini dari atribut barang, kemudian teks inputan yg ke-2 untuk nomor urutnya</i>
			</td>
		</tr>
		
		<?php
		}
		?>
		<tr>
			<td>Nama Barang</td>
			<td> <input type="text" name="nama" id="nama" value="<?php echo $enama_brg ?>" maxlength="60" size="40"></td>
		</tr>
		<tr>
			<td>Nama Barang ke Supplier</td>
			<td> <input type="text" name="nama_brg_sup" id="nama_brg_sup" value="<?php echo $enama_brg_sup ?>" maxlength="60" size="40"></td>
		</tr>
		
		<tr>
			<td>Satuan</td>
			<td>
			<?php
			/*	$ada = 0;
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_pp_detail WHERE kode_brg = '$ekode' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_op_detail WHERE kode_brg = '$ekode' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE kode_brg = '$ekode' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail WHERE kode_brg = '$ekode' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 } */
			?>
			 <select name="satuan" id="satuan" <?php //if ($ada == 1) { ?> <!--disabled --> <?php// } ?>>
				<?php foreach ($satuan as $sat) { ?>
					<option value="<?php echo $sat->id ?>" <?php if ($sat->id == $esatuan) { ?> selected="true" <?php } ?> ><?php echo $sat->nama ?></option>
				<?php } ?>
				</select>
				<input type="hidden" name="ada" id="ada" value="<?php //echo $ada ?>">
				<input type="hidden" name="hide_satuan" id="hide_satuan" value="<?php echo $esatuan ?>">
			</td>
		</tr>
		<tr>
			<td>Deskripsi</td>
			<td> <input type="text" name="deskripsi" value="<?php echo $edeskripsi ?>" maxlength="30" size="30"></td>
		</tr>
		<tr>
			<td>Gudang</td>
			<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($gud->id == $eid_gudang) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		
		<?php
			if ($edit == '1') {
		?>
		<tr>
			<td>Status Aktif</td>
			<td> <input type="checkbox" name="status_aktif" id="status_aktif" value="t" <?php if ($estatus_aktif == 't') { ?> checked="true" <?php } ?> ></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb/cform/view'">
			<?php } else { 
				if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_bahan."/".$cgudang."/".$cstatus."/".$carinya."/".$cur_page;
				?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

