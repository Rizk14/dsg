<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function upperCaseF(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
}



function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();
	var id_jenis= $('#id_jenis').val();


	if (id_jenis == '') {
		alert("Jenis Barang harus dipilih..!");
		$('#kode_jenis').focus();
		return false;
	}
	
	if (nama == '') {
		alert("Nama Barang harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
}

$(function()
{
	//var kode_kel = jQuery('#kel_brg').val();
	//var kode_brg = jQuery('#kode').val();
	//var goedit = jQuery('#goedit').val();
	//var gabung = kode_kel;
	//if (goedit != '1')
	//	jQuery('#kode').val(gabung);
	
	$('#pilih_jenis').click(function(){

	  	    var id_kel_brg= jQuery('#kel_brg').val();

			var urlnya = "<?php echo base_url(); ?>index.php/mst-bb/cform/show_popup_jenis/"+id_kel_brg;

			openCenteredWindow(urlnya);

	  });
	 	  
	 /* $('#kel_brg').change(function(){
	  	    var kode_kel = jQuery('#kel_brg').val();
	  	    var kode_brg = jQuery('#kode').val();
	  	    var gabung = kode_kel;
	  	    jQuery('#kode').val(gabung);
	  	    
	  	    $("#id_jenis").val('');	
	  	    $("#kode_jenis").val('');	
	  	    $("#id_jenis_bhn").val('');	
	  	    $("#kode_jenis_bhn").val('');	
	  	    $("#kode2").val('');	
	  	    $("#nama").val('');	
	  	    
	  	    $('#item option[value=xx]').attr('selected', 'selected');
	  	    $('#motif option[value=xx]').attr('selected', 'selected');
	  	    $('#warna option[value=xx]').attr('selected', 'selected');
	  }); */
	  
});
	
</script>

<h3>Data Bahan Baku/Pembantu</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-bb/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i><h1>".$msg."<i></h1><br>"; ?>
<?php 
	$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
	echo form_open('mst-bb/cform/submit', $attributes); ?>
<?php if ($edit == '1') { ?> 
	<input type="hidden" name="goedit" id="goedit" value="1"> 
	<input type="hidden" name="id_barang" value="<?php echo $eid_barang ?>">
	<input type="hidden" name="kode_lama" value="<?php echo $ekode ?>">
	<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
	<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
	<input type="hidden" name="cjenis_barang" value="<?php echo $cjenis_barang ?>">
	<input type="hidden" name="cgudang" value="<?php echo $cgudang ?>">
	<input type="hidden" name="cstatus" value="<?php echo $cstatus ?>">
	<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<?php } ?>
	<table>
		<?php
		if ($edit != '1') {
		?>
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kel_brg" id="kel_brg" onkeyup="this.blur();this.focus();">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($kel->kode == $ekode_kel) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td> <input type="text" name="kode_jenis" id="kode_jenis" value="<?php echo $ekode_jenis." - ".$enama_jenis ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="<?php echo $eid_jenis_brg ?>">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang">
			</td>
		</tr>		
		<tr>
			<td>Kode Barang</td>
			<td> <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="10" size="10" readonly="true">
			</td>
		</tr>
		<?php 
		}
		else { // hilangin aja disablednya 11-06-2015
		?>
		<!-- ini jika edit == 1 -->
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kel_brg" id="kel_brg" onkeyup="this.blur();this.focus();">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($kel->kode == $ekode_kel) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td> <input type="text" name="kode_jenis" id="kode_jenis" value="<?php echo $ekode_jenis." - ".$enama_jenis ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="<?php echo $eid_jenis_brg ?>">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang" >
			</td>
		</tr>		
		<tr>
			<td>Kode Barang</td>
			<td> <input readonly="true" type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="10" size="10">
			</td>
		</tr>
		
		<?php
		}
		?>
		<tr>
			<td>Nama Barang</td>
			<td> <input type="text" name="nama" id="nama" onkeydown="upperCaseF(this)" value="<?php $pos = strpos($enama_brg, "\"");
	  if ($pos > 0)
		echo str_replace("\"", "&quot;", $enama_brg);
	  else
		echo trim($enama_brg) ?>" size="40"></td>
		</tr>
		<tr>
			<td>Nama Barang ke Supplier</td>
			<td> <input type="text" name="nama_brg_sup" id="nama_brg_sup" value="<?php echo $enama_brg_sup ?>" maxlength="60" size="40"></td>
		</tr>
		
		<tr>
			<td>Satuan</td>
			<td>
			 <select name="satuan" id="satuan">
				<?php foreach ($satuan as $sat) { ?>
					<option value="<?php echo $sat->id ?>" <?php if ($sat->id == $esatuan) { ?> selected="true" <?php } ?> ><?php echo $sat->nama ?></option>
				<?php } ?>
				</select>
				<input type="hidden" name="ada" id="ada" value="<?php //echo $ada ?>">
				<input type="hidden" name="hide_satuan" id="hide_satuan" value="<?php echo $esatuan ?>">
			</td>
		</tr>
		
		<!-- 29-10-2014 -->
		<tr>
			<td>Satuan Konversi</td>
			<td>
			 <select name="satuan_konv" id="satuan_konv">
				 <option value="0" <?php if ($esatuan_konv == '0') { ?>selected<?php } ?>>Tidak Ada</option>
				<?php foreach ($satuan as $sat) { ?>
					<option value="<?php echo $sat->id ?>" <?php if ($sat->id == $esatuan_konv) { ?> selected="true" <?php } ?> ><?php echo $sat->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Angka Faktor Konversi</td>
			<td><input type="text" size="3" style="text-align:right;" name="angka_faktor_konversi" id="angka_faktor_konversi" value="<?php if ($esatuan_konv != '0') echo $eangka_faktor_konversi; else echo "0"; ?>"></td>
		</tr>
		<tr>
			<td>Rumus Konversi</td>
			<td>
			 <select name="rumus_konv" id="rumus_konv">
				 <option value="0" <?php if ($erumus_konv == '0') { ?>selected<?php } ?>>Tidak Ada</option>
				 <option value="1" <?php if ($erumus_konv == '1') { ?>selected<?php } ?>>Dikali (Qty Konv = Qty Sat Awal x Angka Faktor)</option>
				 <option value="2" <?php if ($erumus_konv == '2') { ?>selected<?php } ?>>Dibagi (Qty Konv = Qty Sat Awal / Angka Faktor)</option>
				 <option value="3" <?php if ($erumus_konv == '3') { ?>selected<?php } ?>>Ditambah (Qty Konv = Qty Sat Awal + Angka Faktor)</option>
				 <option value="4" <?php if ($erumus_konv == '4') { ?>selected<?php } ?>>Dikurang (Qty Konv = Qty Sat Awal - Angka Faktor)</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Deskripsi</td>
			<td> <input type="text" name="deskripsi" value="<?php echo $edeskripsi ?>" maxlength="30" size="30"></td>
		</tr>
		<tr>
			<td>Gudang</td>
			<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($gud->id == $eid_gudang) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Masuk ke Mutasi Stok Berdasarkan Harga</td>
			<td> <select name="is_mutasi_stok_harga" id="is_mutasi_stok_harga">
				<option value="t" <?php if ($eis_mutasi_stok_harga == 't') { ?>selected<?php } ?>>Ya</option>
				<option value="f" <?php if ($eis_mutasi_stok_harga == 'f') { ?>selected<?php } ?>>Tidak</option>
				</select>
			</td>
		</tr>
		
		<?php
			if ($edit == '1') {
		?>
		<tr>
			<td>Status Aktif</td>
			<td> <input type="checkbox" name="status_aktif" id="status_aktif" value="t" <?php if ($estatus_aktif == 't') { ?> checked="true" <?php } ?> ></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb/cform/view'">
			<?php } else { 
				if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "mst-bb/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "mst-bb/cform/cari/index/".$cjenis_barang."/".$cgudang."/".$cstatus."/".$carinya."/".$cur_page;
				?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>