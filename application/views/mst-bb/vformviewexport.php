<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Export Data Bahan Baku/Pembantu</h3><br>
<?php if ($this->session->userdata('gid') != 3) { ?><a href="<?php echo base_url(); ?>index.php/mst-bb/cform">Tambah Data</a>&nbsp;&nbsp; <?php } ?><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">View Data</a><br><br><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/export">Export Data</a><br><br>

<div>

<?php echo form_open('mst-bb/cform/export_excel'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Jenis Barang</td>
		<td><select name="jenis_barang" id="jenis_barang">
			<option value="0" <?php if ($ejenis_barang == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php foreach ($jns_brg as $jns) { ?>
			<option value="<?php echo $jns->id ?>" <?php if ($jns->id == $ejenis_barang) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nama_kel_brg."] ". $jns->kode."-".$jns->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Lokasi Gudang</td>
		<td><select name="gudang" id="gudang">
		<option value="0" <?php if ($cgudang == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php foreach ($list_gudang as $gud) { ?>
			<option value="<?php echo $gud->id ?>" <?php if ($cgudang == $gud->id) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><select name="statusnya" id="statusnya">
		<option value="0" <?php if ($cstatus == '') { ?> selected="true" <?php } ?> >- All -</option>
		<option value="t" <?php if ($cstatus == 't') { ?> selected="true" <?php } ?> >Aktif</option>
		<option value="f" <?php if ($cstatus == 'f') { ?> selected="true" <?php } ?> >Non-Aktif</option>
		</select></td>
	</tr>
	<tr>
		<td>Keyword Pencarian</td>
		<td><input type="text" name="cari" size="30" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Export"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>


