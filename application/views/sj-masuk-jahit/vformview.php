<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data SJ Masuk Hasil Jahit</h3><br> 
<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('sj-masuk-jahit/cform/cari'); ?>
Unit Jahit: <select name="unit_jahit" id="unit_jahit">
				<option value="0" <?php if ($unit_jahit=='') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_unit as $unitnya) { ?>
					<option value="<?php echo $unitnya->kode_unit ?>" <?php if ($unit_jahit==$unitnya->kode_unit) { ?> 
					selected="true" <?php } ?> ><?php echo $unitnya->kode_unit." - ". $unitnya->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <!-- <th>No/ Tgl SJ Keluar</th> -->
		<!-- <th height="28" width="80px">SJ Masuk</th> -->
		 <th>SJ Masuk</th>
		 <th>Tgl SJ Masuk</th>
		 <th>Unit Jahit</th>
		 <th>SJ Keluar</th>
		 <th>Brg Jadi</th>
		 <th>Ket Qty Per Warna</th>
		 <th>Tot Qty</th>
		 <th>Jml Total (Rp.)</th>
		 <th>Keterangan</th>
		 <th>Status Lunas</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
					
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 // echo "<td>".$query[$j]['no_sj_keluar']." / ".$tgl_sj_keluar."</td>";
				 echo "<td nowrap>".$query[$j]['no_sj']."</td>";
				 echo "<td>".$tgl_sj."</td>";
				 echo "<td>".$query[$j]['nama_unit']."</td>";
				 
				// echo    "<td>".$query[$j]['nama_jenis_makloon']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_masuk_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_masuk_detail'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['tgl_sj_keluar'] != '') {
							$pisah1 = explode("-", $var_detail[$k]['tgl_sj_keluar']);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							if ($bln1 == '01')
								$nama_bln = "Januari";
							else if ($bln1 == '02')
								$nama_bln = "Februari";
							else if ($bln1 == '03')
								$nama_bln = "Maret";
							else if ($bln1 == '04')
								$nama_bln = "April";
							else if ($bln1 == '05')
								$nama_bln = "Mei";
							else if ($bln1 == '06')
								$nama_bln = "Juni";
							else if ($bln1 == '07')
								$nama_bln = "Juli";
							else if ($bln1 == '08')
								$nama_bln = "Agustus";
							else if ($bln1 == '09')
								$nama_bln = "September";
							else if ($bln1 == '10')
								$nama_bln = "Oktober";
							else if ($bln1 == '11')
								$nama_bln = "November";
							else if ($bln1 == '12')
								$nama_bln = "Desember";
							$tgl_sj_keluar = $tgl1." ".$nama_bln." ".$thn1;
							
							echo $var_detail[$k]['no_sj_keluar']." / ".$tgl_sj_keluar;
						}
						else {
							$tgl_sj_keluar = '';
							echo $tgl_sj_keluar;
						}
						if ($k<$hitung-1)
							echo "<br> ";
										
						//echo "(".$var_detail[$k]['no_sj_keluar'].") ".$var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (".$var_detail[$k]['qty'].")";
						/*echo "<tr>
							<td width=\"90px\">".$var_detail[$k]['no_sj_keluar']."</td>
							<td width=\"210px\">".$var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']."</td>
							<td width=\"35px\" align=\"right\">".$var_detail[$k]['qty']."&nbsp;</td>
							</tr>"; */
		 		 						
						//if ($k<$hitung-1)
							//echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_masuk_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_masuk_detail'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_masuk_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_masuk_detail'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['ket_qty_warna'];
						if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['data_masuk_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_masuk_detail'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['qty'];
						if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";

				echo    "<td align='right'>".number_format($query[$j]['gtotal'], 2, ',','.')."</td>";
				echo    "<td>".$query[$j]['keterangan']."</td>";
				echo    "<td>";
					if($query[$j]['status_lunas'] == 't')
						echo "Lunas";
					else
						echo "Belum";
				echo "</td>";
				 echo    "<td>".$tgl_update."</td>";
				 
				 if ($query[$j]['status_edit'] == 'f'){
					echo    "<td align=center>";
					//<a href=".base_url()."index.php/sj-masuk-jahit/cform/edit/".$query[$j]['id']."/"." \">Edit</a>
					echo "<a href=".base_url()."index.php/sj-masuk-jahit/cform/delete/".$query[$j]['id']."/".$query[$j]['id_detailnya']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 }
				 else {
					echo    "<td align=center>
					<a href=".base_url()."index.php/sj-masuk-jahit/cform/delete/".$query[$j]['id']."/".$query[$j]['id_detailnya']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 }
				
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
