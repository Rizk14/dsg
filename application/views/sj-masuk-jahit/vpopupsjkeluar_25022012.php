<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>

<script>
$(function()
{

	$("#pilih").click(function()
	{	
		var id_sj_detail = '';
		var no_sj = '';
		var id_sj	= '';

		opener.document.forms["f_jahit"].no_sj_keluar.value = '';
		opener.document.forms["f_jahit"].id_sj_keluar.value = '';		
		opener.document.forms["f_jahit"].id_proses_jahit_detail.value = '';

		if (document.f_master_brg.idsjprosesjahitdetail.length > 0) {
			for(var i=0; i < document.f_master_brg.idsjprosesjahitdetail.length; i++){
				if(document.f_master_brg.idsjprosesjahitdetail[i].checked==true){
					id_sj_detail+= document.f_master_brg.idsjprosesjahitdetail[i].value + ";";
					no_sj+= document.f_master_brg.no_sj_keluar[i].value + ";";
					id_sj+= document.f_master_brg.id_sj_keluar[i].value + ";";
				}	
			}
			opener.document.forms["f_jahit"].id_proses_jahit_detail.value = id_sj_detail;
			opener.document.forms["f_jahit"].no_sj_keluar.value = no_sj;
			opener.document.forms["f_jahit"].id_sj_keluar.value = id_sj;
			
		}else{
			opener.document.forms["f_jahit"].id_proses_jahit_detail.value = document.getElementById('idsjprosesjahitdetail').value;
			opener.document.forms["f_jahit"].no_sj_keluar.value = document.getElementById('no_sj_keluar').value;
			opener.document.forms["f_jahit"].id_sj_keluar.value = document.getElementById('id_sj_keluar').value;
		}
	
		self.close();
	});
});
</script>

<center><h3>Daftar SJ Keluar Proses Jahit Yg Blm Dipenuhi <br>di <?php echo $nama_unit ?></h3></center>
<div align="center"><br>
<?php echo form_open('sj-masuk-jahit/cform/show_popup_sj_keluar/'.$unit_jahit.'/'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>" />
&nbsp;<input type="submit" name="submit" value="Cari" />
<input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit; ?>" />
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
  <table border="1" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">No/Tgl SJ</th>
      <th bgcolor="#999999">Makloon<br /> Internal</th>
      <th bgcolor="#999999">Nm Barang & Qty</th>
      <th bgcolor="#999999">Last Update</th>
    </tr>
	
	<?php 
	
	$i=1; 
	
	if (is_array($query) && count($query)>0) {
		
		for($j=0;$j<count($query);$j++) {
			
			$pisah1 = explode("-", $query[$j]['tgl_update']);
			
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			
			if ($bln1 == '01')
				$nama_bln = "Januari";
			else if ($bln1 == '02')
				$nama_bln = "Februari";
			else if ($bln1 == '03')
				$nama_bln = "Maret";
			else if ($bln1 == '04')
				$nama_bln = "April";
			else if ($bln1 == '05')
				$nama_bln = "Mei";
			else if ($bln1 == '06')
				$nama_bln = "Juni";
			else if ($bln1 == '07')
				$nama_bln = "Juli";
			else if ($bln1 == '08')
				$nama_bln = "Agustus";
			else if ($bln1 == '09')
				$nama_bln = "September";
			else if ($bln1 == '10')
				$nama_bln = "Oktober";
			else if ($bln1 == '11')
				$nama_bln = "November";
			else if ($bln1 == '12')
				$nama_bln = "Desember";
		
			$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
			$pisah1 = explode("-", $query[$j]['tgl_sj']);
			
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			
			if ($bln1 == '01')
				$nama_bln = "Januari";
			else if ($bln1 == '02')
				$nama_bln = "Februari";
			else if ($bln1 == '03')
				$nama_bln = "Maret";
			else if ($bln1 == '04')
				$nama_bln = "April";
			else if ($bln1 == '05')
				$nama_bln = "Mei";
			else if ($bln1 == '06')
				$nama_bln = "Juni";
			else if ($bln1 == '07')
				$nama_bln = "Juli";
			else if ($bln1 == '08')
				$nama_bln = "Agustus";
			else if ($bln1 == '09')
				$nama_bln = "September";
			else if ($bln1 == '10')
				$nama_bln = "Oktober";
			else if ($bln1 == '11')
				$nama_bln = "November";
			else if ($bln1 == '12')
				$nama_bln = "Desember";
			
			$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['no_sj']." / ".$tgl_sj; ?></td>
      <td><?php 
	  	if($query[$j]['makloon_internal']=='t'){
	  		echo "Ya";
		}else{
			echo "Tdk";
		} ?></td>
      
      <?php
		echo "<td nowrap>";
				 if (is_array($query[$j]['detail_bhn'])) {
				
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bhn'];
					 $hitung = count($var_detail);
					 
					for($k=0;$k<count($var_detail); $k++){
						  
						  if($var_detail[$k]['kode_brg_jd']!=''){
							  $detail_brg_jd	= "(".$var_detail[$k]['kode_brg_jd']."-".$var_detail[$k]['nama_brg_jd'].")";
							  $idbrgjadina	= $var_detail[$k]['kode_brg_jd'];
						  }	else {
							  $detail_brg_jd	= "";
							  $idbrgjadina	= $var_detail[$k]['kode_brg'];
						  }
						  
						  echo "(".$var_detail[$k]['kel_bhn']."-".$var_detail[$k]['nama_kel'].") "." (".$var_detail[$k]['kode_brg'].") ".$detail_brg_jd." (".$var_detail[$k]['qty'].")";
						  echo " <input type='checkbox' name='idsjprosesjahitdetail' id='idsjprosesjahitdetail' value='".$var_detail[$k]['id']."' >";
						  echo " <input type='hidden' name='id_sj_keluar' id='id_sj_keluar' value='".$query[$j]['id']."' >";
						  echo " <input type='hidden' name='no_sj_keluar' id='no_sj_keluar' value='".$query[$j]['no_sj']."' >";
						  
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
      ?>
      
      <td><?php echo $tgl_update; ?></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <br />
  <? echo $this->pagination->create_links();?>
  <br />
  <input type="button" name="pilih" id="pilih" value="Pilih" >
  </form>
</div>

