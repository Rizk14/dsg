<h3>Data SJ Masuk Hasil Jahit</h3><br>
<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">View Data</a><br><br>

<?php if($msg!='') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript" language="javascript">
$(function()
{
	
	$('#unit_packing').change(function(){
		$("#sj_keluar").val('');
		$("#id_sj_keluar").val('');	
	  });
	  
	$('#is_no_sjkeluar').click(function(){
	  	    if ($('input[name=is_no_sjkeluar]').is(':checked')) {
				$('#no_sj_keluar').val('');
				$('#id_sj_keluar').val('');
				$('#id_proses_jahit_detail').val('');
				$("#pilih").attr("disabled", true);
			}
			else {
				$('#no_sj_keluar').val('');
				$('#id_sj_keluar').val('');
				$('#id_proses_jahit_detail').val('');
				$("#pilih").attr("disabled", false);
			}
	});
		
});
</script>

<script type="text/javascript" language="javascript">
function openCenteredWindow(url) {

		var width = 720;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function hitung_total() {
	var jum_detail = $('#no').val()-1;
	
	var i=1;
	var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var biaya=$("#biaya_"+i).val(); 
					if (biaya == '')
						biaya = 0;

					gtotal = parseFloat(gtotal)+parseFloat(biaya);
					gtotal = gtotal.toFixed(2);
					$("#totalnya").val(gtotal);
				}
    
}

function cek_item_brg() {
	var id_brg= $('#id_brg').val();
	var sj_keluar= $('#sj_keluar').val();
	if (sj_keluar == '') {
		alert("Nomor SJ Keluar harus dipilih..!");
		return false;
	}
	if (id_brg == '') {
		alert("item barang di SJ harus dipilih..!");
		return false;
	}
}

function cek_input() {
	var no_sj	= $('#no_sj_masuk').val();
	var tgl		= $('#tgl_sj_masuk').val();
		
	if (no_sj=='') {
		alert("Nomor SJ Masuk harus diisi..!");
		return false;
	}
	if (tgl=='') {
		alert("Tanggal SJ Masuk harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong..!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;	
	
	var i=1; var gtotal = 0;
	for (i=1;i<=jum_detail;i++) {
		var qty=$("#qty_"+i).val();
		var harga=$("#harga_"+i).val();
		var hitung = harga*qty;
		hitung = hitung.toFixed(2);
		$("#total_"+i).val(hitung);
		gtotal = parseFloat(gtotal)+parseFloat(hitung);
	}
	gtotal = gtotal.toFixed(2);
	$("#gtotal").val(gtotal);
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses=='') {
	$attributes = array('name' => 'f_jahit', 'id' => 'f_jahit');
	echo form_open('sj-masuk-jahit/cform/', $attributes);
?>

<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%"> Tidak ada acuan SJ Keluar Proses Jahit </td>
		<td> <input type="checkbox" name="is_no_sjkeluar" id="is_no_sjkeluar" value="t"> &nbsp;
		 </td>
	</tr>

	<tr>
		<td width="20%">Unit Jahit</td>
		<td><select name="unit_jahit" id="unit_jahit" onkeyup="this.blur();this.focus();">
				<?php foreach ($list_unit as $jahit) { ?>
					<option value="<?php echo $jahit->kode_unit ?>"><?php echo $jahit->kode_unit." - ". $jahit->nama ?></option>
				<?php } ?>
			</select></td> 
</tr>
	<tr>
		<td>SJ Proses Jahit</td>
		<td><input name="no_sj_keluar" type="text" id="no_sj_keluar" size="30" readonly="true" />
			<input type="hidden" name="id_sj_keluar" id="id_sj_keluar" />
			<input type="hidden" name="id_proses_jahit_detail" id="id_proses_jahit_detail" />	
			<input title="browse data SJ keluar" name="pilih" value="..." type="button" id="pilih" onclick="javascript: var x= $('#unit_jahit').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/show_popup_sj_keluar/'+ x);" type="button"></td>
	</tr>
	
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/view'">

<?php echo form_close(); 
} else { ?>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_sj_keluar" value="<?php echo $id_sj_keluar ?>">
<input type="hidden" name="is_no_sjkeluar" id="is_no_sjkeluar" value="">
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>">

<?php 

if(!isset($sj_detail)) {
	$sj_detail = array();
}

if(is_array($sj_detail) && count($sj_detail)>0) {
	
	$no=1;
	
	foreach ($sj_detail as $hitung) {
		$no++;
	}
}
else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<div align="center">

<br>

<table class="proit-view" width="100%" cellspacing="2" cellpadding="1"/>
  <tr>
	<td width="15%">Unit Jahit</td>
	<td width="70%"><?php echo $unit_jahit." - ". $nama_unit ?></td> 
  </tr>
  
  <tr>
    <td>SJ Keluar Proses Jahit</td>
    <td>
      <input name="no_sj_keluar" type="text" id="no_sj_keluar" size="20" maxlength="20" readonly="true" value="<?php echo $no_sj_keluar; ?>">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Nomor SJ Masuk</td>
    <td>
	  <input name="no_sj_masuk" type="text" id="no_sj_masuk" size="20" maxlength="20" value=""/>
    </td>
    
  </tr>
  <tr>
    <td>Tgl SJ Masuk</td>
    <td>
	<label>
      <input name="tgl_sj_masuk" type="text" id="tgl_sj_masuk" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj_masuk" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj_masuk,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Keterangan</td>
		<td><input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
	</tr>
	
	<tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku" border="0" align="center" cellpadding="1" cellspacing="2" class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>No/Tgl SJ Keluar</th>
          <th>Kode Bahan</th>
          <th>Kode Brg Jadi</th>
           <th>Nama Brg Jadi</th>
           <th>Ket Qty Per Warna</th>
	      <th>Tot Qty</th>
	      <th>Harga</th>
	      <th>Total</th>
        </tr>

        <?php $i=1;
        if (count($sj_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($sj_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
           <input name="nosj_<?php echo $i ?>" type="text" id="nosj_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $sj_detail[$j]['no_sj']." / ".$sj_detail[$j]['tgl_sj'] ?>"/>
           </td>                    
          <td nowrap="nowrap">
           <input name="kode_bhn_<?php echo $i ?>" type="text" id="kode_bhn_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg'] ?>"/>
           </td>          
          <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg_jadi'] ?>"/>
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['nama_brg_jd'] ?>" /></td>
          
          <td>
			<input name="ket_warna_<?php echo $i ?>" type="text" id="ket_warna_<?php echo $i ?>" size="20" value="" />
		  </td>
          
          <td>
			<input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="5" maxlength="5" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()" />
		  </td>
		  <td>
			<input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="10" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()" value="<?php echo $sj_detail[$j]['harga'] ?>" />
			<input type="hidden" id="harga_lama_<?php echo $i ?>" name="harga_lama_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['harga'] ?>">
		  </td>
		  <td>
			<input name="total_<?php echo $i ?>" type="text" id="total_<?php echo $i ?>" size="10" value="0" readonly="true" />
		  </td>
          <td>
          <!-- <input name="biaya_<?php echo $i ?>" type="text" id="biaya_<?php echo $i ?>" size="10" maxlength="10" value="" onkeyup="hitung_total()" /> -->
		  <input type="hidden" name="id_proses_jahit_<?php echo $i ?>" id="id_proses_jahit_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['idsjprosesjahit'] ?>" >
          <input type="hidden" name="id_proses_jahit_detail_<?php echo $i ?>" id="id_proses_jahit_detail_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['idsjprosesjahitdetail'] ?>" >
          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9">Grand Total: <input type="text" name="gtotal" id="gtotal" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td colspan="9" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/'"></td>
		</tr>
	</table>	
	
	</form>
</td>
    </tr>
</table>
</div>
</form>
<?php } ?>
