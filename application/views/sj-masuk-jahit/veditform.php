<h3>Data SJ Masuk Hasil Jahit</h3><br>
<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>

$(function()

{
	
	$('#unit_packing').change(function(){
		$("#sj_keluar").val('');
		$("#id_sj_keluar").val('');	
	  });
		
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 720;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function hitung_total() {
	var jum_detail = $('#no').val()-1;
	
	var i=1;
	var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var biaya=$("#biaya_"+i).val(); 
					if (biaya == '')
						biaya = 0;

					gtotal = parseFloat(gtotal)+parseFloat(biaya);
					gtotal = gtotal.toFixed(2);
					$("#totalnya").val(gtotal);
				}
    
}

function cek_item_brg() {
	var id_brg= $('#id_brg').val();
	var sj_keluar= $('#sj_keluar').val();
	if (sj_keluar == '') {
		alert("Nomor SJ Keluar harus dipilih..!");
		return false;
	}
	if (id_brg == '') {
		alert("item barang di SJ harus dipilih..!");
		return false;
	}
}

function cek_input() {
	var no_sj= $('#no_sj').val();
	var tgl= $('#tgl_sj').val();
		
	if (no_sj == '') {
		alert("Nomor SJ Masuk harus diisi..!");
		return false;
	}
	if (tgl == '') {
		alert("Tanggal SJ Masuk harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_sj_masuk" value="<?php echo $query[0]['id'] ?>">
<!-- <input type="hidden" name="id_sj_keluar" value="<?php echo $query[0]['id_sj_proses_jahit'] ?>"> -->

<?php 
		if (count($query[0]['detail_sj'])>0) {
			$no=1;
			foreach ($query[0]['detail_sj'] as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
Edit Data<br>
<div align="center">

<br>
<?php
	$pisah1 = explode("-", $query[0]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
?>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
  <tr>
	<td width="15%">Unit Packing</td>
	<td width="70%"><?php echo $query[0]['kode_unit']." - ". $query[0]['nama_unit'] ?></td> 
  </tr>
  <!--
  <tr>
    <td>No SJ Keluar</td>
    <td>
      <input name="no_sj_keluar" type="text" id="no_sj_keluar" size="20" maxlength="20" readonly="true" value="<?php echo $query[0]['no_sj_keluar']; ?>">&nbsp;
    </td>
  </tr>
  -->
  <tr>
    <td>Nomor SJ Masuk</td>
    <td>
      <input name="no_sj_masuk" type="text" id="no_sj_masuk" size="20" maxlength="20" value="<?php echo $query[0]['no_sj'] ?>">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Tgl SJ Masuk</td>
    <td>
	<label>
      <input name="tgl_sj_masuk" type="text" id="tgl_sj_masuk" size="10" value="<?php echo $tgl_sj ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_sj_masuk" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj_masuk,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Keterangan</td>
		<td><input type="text" name="ket" id="ket" value="<?php echo $query[0]['keterangan'] ?>" size="30" maxlength="30"></td>
	</tr>
	
	<tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>SJ Keluar</th>
          <th>Kode Bahan</th>          
          <th>Kode Brg Jadi</th>
          <th>Nama Brg</th>
	      <th>Qty</th>
	      <!-- <th>Biaya (Rp.)</th> -->
        </tr>

        <?php $i=1;
        if (count($query[0]['detail_sj'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
        </tr>
		
		<?php
		} else {
			$sj_detail = $query[0]['detail_sj'];
			for($j=0;$j<count($sj_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
           <input name="nosj_<?php echo $i ?>" type="text" id="nosj_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $sj_detail[$j]['no_sj_keluar'] ?>"/>
           </td>                    
          <td nowrap="nowrap">
           <input name="kode_bhn_<?php echo $i ?>" type="text" id="kode_bhn_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg'] ?>"/>
           </td>                    
          <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg_jadi'] ?>"/>
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['nama'] ?>" /></td>
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="5" maxlength="5" value="<?php echo $sj_detail[$j]['qty'] ?>" />
          <input name="qty_lama_<?php echo $i ?>" type="hidden" id="qty_lama_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['qty'] ?>" />
          </td>
          <!-- <td><input name="biaya_<?php echo $i ?>" type="text" id="biaya_<?php echo $i ?>" size="10" value="<?php echo $sj_detail[$j]['biaya'] ?>" onkeyup="hitung_total()" /> -->
		  <input type="hidden" name="id_hasil_jahit_detail_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['id'] ?>" >
		  <input type="hidden" name="id_proses_jahit_detail_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['id_sj_proses_jahit_detail'] ?>" ></td>
        </tr>
		<?php $i++; }
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<!--
		<tr>
			<td colspan="5"> Total (Rp.): <input type="text" name="totalnya" id="totalnya" value="<?php echo $query[0]['total']; ?>" readonly="true"></td>
		</tr>
		-->
		<tr>
			<td colspan="5" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/view'"></td>
		</tr>
	</table>	
	
	</form>
</td>
    </tr>
</table>
</div>
</form>
