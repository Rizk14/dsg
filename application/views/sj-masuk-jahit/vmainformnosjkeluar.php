<h3>Data SJ Masuk Hasil Jahit</h3><br>
<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tgl_sj = $("#tgl_sj_masuk").val();
	var no_sj = $("#no_sj_masuk").val();
	
	if (no_sj == '') {
		alert ("Nomor SJ harus diisi");
		$("#no_sj").focus();
		return false;
	}
	if (tgl_sj == '') {
		alert ("Tgl SJ harus dipilih");
		$("#no_sj").focus();
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#kode_brg_jadi_'+k).val() == '') {
				alert("Barang jadi harus dipilih...!");
				return false;
			}
						
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka atau desimal..!");
				return false;
			}
			
			if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
				alert("Data harga tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#harga_'+k).val()) ) {
				alert("Harga harus berupa angka atau desimal..!");
				return false;
			}
						
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

//tambah
$(function()
{
	//var goedit = $("#goedit").val();
	//if(goedit!='1')
		$("#no").val('2');
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
						
		//*****nama_brg_jadi*************************************
		var nama_brg_jadi="#nama_brg_jadi_"+n;
		var new_nama_brg_jadi="#nama_brg_jadi_"+no;
		$(nama_brg_jadi, lastRow).attr("id", "nama_brg_jadi_"+no);
		$(new_nama_brg_jadi, lastRow).attr("name", "nama_brg_jadi_"+no);		
		$(new_nama_brg_jadi, lastRow).val('');		
		//*****end nama_brg_jadi*********************************
		
		//*****kode_brg_jadi*************************************
		var kode_brg_jadi="#kode_brg_jadi_"+n;
		var new_kode_brg_jadi="#kode_brg_jadi_"+no;
		$(kode_brg_jadi, lastRow).attr("id", "kode_brg_jadi_"+no);
		$(new_kode_brg_jadi, lastRow).attr("name", "kode_brg_jadi_"+no);		
		$(new_kode_brg_jadi, lastRow).val('');		
		//*****end kode_brg_jadi*********************************
		
		//*****ket_warna*************************************
		var ket_warna="#ket_warna_"+n;
		var new_ket_warna="#ket_warna_"+no;
		$(ket_warna, lastRow).attr("id", "ket_warna_"+no);
		$(new_ket_warna, lastRow).attr("name", "ket_warna_"+no);		
		$(new_ket_warna, lastRow).val('');		
		//*****end ket_warna*********************************
						
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('0');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
		var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('0');
		//*****end harga_lama*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
				
		//button pilih_brg_jadi*****************************************
		 var pilih_brg_jadi="#pilih_brg_jadi_"+n;
		 var new_pilih_brg_jadi="#pilih_brg_jadi_"+no;
		 $(pilih_brg_jadi, lastRow).attr("id","pilih_brg_jadi_"+no);
		
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/show_popup_brg_jadi/"+no+"/<?php echo $unit_jahit ?>');";
		 $(new_pilih_brg_jadi, lastRow).attr("name", "pilih_brg_jadi_"+no);		
		 $(new_pilih_brg_jadi, lastRow).attr("onclick",even_klik);		 
		//end button pilih_brg_jadi
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
			
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;	
	
	var i=1; var gtotal = 0;
	for (i=1;i<=jum_detail;i++) {
		var qty=$("#qty_"+i).val();
		var harga=$("#harga_"+i).val();
		var hitung = harga*qty;
		hitung = hitung.toFixed(2);
		$("#total_"+i).val(hitung);
		gtotal = parseFloat(gtotal)+parseFloat(hitung);
	}
	gtotal = gtotal.toFixed(2);
	$("#gtotal").val(gtotal);
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="is_no_sjkeluar" id="is_no_sjkeluar" value="<?php echo $is_no_sjkeluar ?>">
<input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">
<input name="no_sj_keluar" type="hidden" id="no_sj_keluar" value="">
<input type="hidden" name="no" id="no" >

<div align="center">

<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
		<td width="20%">Tanpa acuan SJ Keluar
		<input name="no_sj_keluar" type="hidden" id="no_sj_keluar" value="<?php echo $no_sj_keluar; ?>">
		</td>
  </tr>
		<tr>
			<td>Unit Makloon</td>
			<td><?php echo $unit_jahit." - ". $nama_unit ?></td> 
		</tr>

  <tr>
    <td>Nomor SJ Masuk</td>
    <td>
      <input name="no_sj_masuk" type="text" id="no_sj_masuk" size="20" maxlength="20" value="">
    </td>
    
  </tr>
  <tr>
    <td>Tgl SJ Masuk</td>
    <td>
	<label>
      <input name="tgl_sj_masuk" type="text" id="tgl_sj_masuk" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj_masuk" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj_masuk,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Keterangan</td>
		<td><input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
	</tr>
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="7" align="right"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode Brg Jadi</th>
          <th>Nama Brg Jadi</th>
          <th>Ket Qty Per Warna</th>
	      <th>Tot Qty</th>
	      <th>Harga</th>
	      <th>Total</th>
        </tr>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap><input name="kode_brg_jadi_1" type="text" id="kode_brg_jadi_1" value="" size="15"> 
			<input title="browse data brg jadi" name="pilih_brg_jadi_1" value="..." type="button" id="pilih_brg_jadi_1" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/show_popup_brg_jadi/1/<?php echo $unit_jahit ?>');" >
          </td>
          <td nowrap="nowrap">
           <input name="nama_brg_jadi_1" type="text" id="nama_brg_jadi_1" size="40" readonly="true"/> 
           </td>
           <td><input name="ket_warna_1" type="text" id="ket_warna_1" size="20" value="" /></td>
           <td><input name="qty_1" type="text" id="qty_1" size="10" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()" /></td>
           <td><input name="harga_1" type="text" id="harga_1" size="10" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()" />
           <input type="hidden" id="harga_lama_1" name="harga_lama_1" value="">
           </td>
         <td><input name="total_1" type="text" id="total_1" size="10" value="0" readonly="true" /></td>
        </tr>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td>Grand Total</td>
			<td>: <input type="text" name="gtotal" id="gtotal" value="0" size="10" readonly="true">
			</td>
		</tr>
	</table>
	
	</form>
      <div align="center"><br><br>
        <input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-masuk-jahit/cform/'">
      </div></td>
    </tr>

</table>
</div>
</form>
