<table class="maintable">
  <tr>
    <td align="left">
		<?php
      $attributes=array('name'=>'f_harga', 'id'=>'f_harga' );
		echo form_open('girokeluar/cform/simpan',$attributes);
      ?>
	
	<div id="mastergiroform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Giro</td>
		<td width="1%">:</td>
		<td width="37%"><input name="igiro" id="igiro" value="" maxlength="10">
						<input readonly name="dgiro" id="dgiro" value="" onclick="showCalendar('',this,this,'','dgiro',0,20,1)"></td>
		<td width="12%">Tanggal JT</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="dgiroduedate" id="dgiroduedate" value="" 
						 onclick="showCalendar('',this,this,'','dgiroduedate',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Payment Voucher</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="ipv" id="ipv" value="" maxlength="6">
						<input readonly name="dpv" id="dpv" value="" onclick="showCalendar('',this,this,'','dpv',0,20,1)"></td>
		<td width="12%">Tanggal Cair</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="dgirocair" id="dgirocair" value="" 
						 onclick="showCalendar('',this,this,'','dgirocair',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Pemasok</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="isupplier" id="isupplier" value="">
						<input readonly name="esuppliername" id="esuppliername" value=""
						 onclick='openCenteredWindow("<?php echo base_url(); ?>index.php/girokeluar/cform/supplier/");'></td>
		<td width="12%">Bank</td>
		<td width="1%">:</td>
		<td width="37%"><input name="egirobank" id="egirobank" value=""></td>
	      </tr>
	      <tr>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" name="vjumlah" id="vjumlah" value="" onkeyup="reformat(this);"></td>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="egirodescription" id="egirodescription" value=""></td>
	      </tr>
	      <tr>
		<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="vsisa" id="vsisa" value=""></td>
		<td colspan=3>&nbsp;</td>
	      </tr>
	      <tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan=4>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="reset" onclick="bersih()">
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script  language="javascript">
	function bersih()
	{
		document.getElementById("pesan").innerHTML='';
	}
	function samain()
	{
		document.getElementById("dgirocair").value=document.getElementById("dgiroduedate").value;
	}
  	function dipales(){
	  	cek='false';
	  	if((document.getElementById("igiro").value!='') &&
	 	   (document.getElementById("dgiroduedate").value!='')) {
			cek='true';	
		} 
		//~ if(cek=='true'){
	  		//~ document.getElementById("login").disabled=true;
    	//~ }
  	}
  	function openCenteredWindow(url) {

	var width = 720;
	var height = 480;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
	myWindow = window.open(url, "subWind", windowFeatures);
}
</script>
