<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_kontrabon; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_kontrabon; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		  <?php 
		    echo $this->pquery->form_remote_tag(array('url'=>'expokontrabon/cform/actedit','update'=>'#content','type'=>'post'));
		  ?>
		<div id="masterlkontrabonform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $list_kontrabon_no_kontrabon; ?> : 
							  <input name="i_kontrabon_code" type="text" id="i_kontrabon_code" maxlength="13" value="<?php echo $idtcode; ?>" readonly />
							  <input type="hidden" name="idt" id="idt" value="<?php echo $idt; ?>" />
							  </td>
							<td width="33%"><?php echo $list_kontrabon_tgl_kontrabon; ?> : 
							  <input name="tgl_kontrabon" type="text" id="tgl_kontrabon" maxlength="10" value="<?php echo $ddt; ?>" />
							  <input name="f_nota_sederhana" type="hidden" id="f_nota_sederhana" value="<?=$fnotasederhana?>" />
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>
					<tr>
					  <td>
						  <div id="title-box2"><?php echo $form_title_detail_kontrabon; ?>					  
						  <div style="float:right;">
						  </div></div>					  
					  </td>
					</tr>
					<tr>
					  <td><table width="80%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="2%" class="tdatahead">NO</td>
							<td width="8%" class="tdatahead"><?php echo $list_kontrabon_no_faktur; ?></td>
							<td width="10%" class="tdatahead"><?php echo $list_kontrabon_tgl_faktur; ?> </td>
							<td width="10%" class="tdatahead"><?php echo $list_kontrabon_tgl_tempo; ?> </td>
							<td width="30%" class="tdatahead"><?php echo $list_kontrabon_pelanggan; ?></td>
							<td width="13%" class="tdatahead"><?php echo $list_kontrabon_jml_tagih; ?></td>
						</tr>
						<tr>
						  <td colspan="6">
						  
						  	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							
							<?php
							
							$iter	= 0;
							$totalkontrabon = 0;
							
							foreach($idtitem as $row3) {
								
								$exp_dfaktur = explode("-",$row3->d_faktur,strlen($row3->d_faktur));
								$exp_d_due_date = explode("-",$row3->d_due_date,strlen($row3->d_due_date));

								if($fnotasederhana=='f') {
									$query2 = $this->mclass->totalfaktur($row3->i_faktur);
								}else{
									$query2 = $this->mclass->totalfakturNONDO($row3->i_faktur);
								}

								$row2	= $query2->row();
								
								$nilai_ppn_perfaktur = (($row2->totalfaktur*11) / 100);
								
								if($row3->v_discount>0){
									$total_sblm_ppn_perfaktur	= round($row2->totalfaktur - $row3->v_discount); // DPP
									$nilai_ppn2_perfaktur	= round((($total_sblm_ppn_perfaktur*11) / 100));
									$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
								}else{
									$total_sblm_ppn_perfaktur	= round($row2->totalfaktur); // DPP
									$nilai_ppn2_perfaktur	= round($nilai_ppn_perfaktur);
									$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
								}
								
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">".($iter*1+1)."</div></td>
									
									<td><DIV ID=\"ajax_i_faktur_code_tblItem_".$iter."\" style=\"width:68px;\">
									<input type=\"text\" ID=\"i_faktur_code_tblItem_".$iter."\" name=\"i_faktur_code_tblItem_".$iter."\" style=\"width:66px;text-align:left;\" value=\"".$row3->i_faktur_code."\" onclick=\"shfakturpenjkontrabon('".$iter."');\" readonly >
									<input type=\"hidden\" ID=\"i_faktur_tblItem_".$iter."\" name=\"i_faktur_tblItem_".$iter."\" value=\"".$row3->i_faktur."\" ></DIV></td>
									
									<td><DIV ID=\"ajax_tgl_faktur_tblItem_".$iter."\" style=\"width:79px;\" >
									<input type=\"text\" ID=\"tgl_faktur_tblItem_".$iter."\" name=\"tgl_faktur_tblItem_".$iter."\" style=\"width:77px;\" value=\"".$exp_dfaktur[2]."/".$exp_dfaktur[1]."/".$exp_dfaktur[0]."\" readonly >
									<input type=\"hidden\" ID=\"tgl_fakturhidden_tblItem_".$iter."\" name=\"tgl_fakturhidden_tblItem_".$iter."\" value=\"".$row3->d_faktur."\"></DIV></td>
									
									<td><DIV ID=\"ajax_due_date_tblItem_".$iter."\" style=\"width:82px;\" >
									<input type=\"text\" ID=\"due_date_tblItem_".$iter."\" name=\"due_date_tblItem_".$iter."\" style=\"width:80px;\" value=\"".$exp_d_due_date[2]."/".$exp_d_due_date[1]."/".$exp_d_due_date[0]."\" readonly >
									<input type=\"hidden\" ID=\"due_datehidden_tblItem_".$iter."\" name=\"due_datehidden_tblItem_".$iter."\" value=\"".$row3->d_due_date."\"></DIV></td>
									
									<td><DIV ID=\"ajax_e_branch_name_tblItem_".$iter."\" style=\"width:272px;\">
									<input type=\"text\" ID=\"e_branch_name_tblItem_".$iter."\" name=\"e_branch_name_tblItem_".$iter."\" style=\"width:270px;\" value=\"".$row3->e_branch_name."\" readonly ></DIV></td>
									
									<td><DIV ID=\"ajax_v_total_plusppn_tblItem_".$iter."\" text-align:right; width:112px;\" >
									<input type=\"text\" ID=\"v_total_plusppn_tblItem_".$iter."\" name=\"v_total_plusppn_tblItem_".$iter."\" style=\"width:106px;text-align:right;\" value=\"".number_format($total_grand_perfaktur,'2','.',',')."\" readonly >
									<input type=\"hidden\" ID=\"i_customer_tblItem_".$iter."\" name=\"i_customer_tblItem_".$iter."\" value=\"".$row3->i_customer."\" >
									<input type=\"hidden\" ID=\"i_branch_code_tblItem_".$iter."\" name=\"i_branch_code_tblItem_".$iter."\" value=\"".$row3->i_branch_code."\" >
									<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\"></DIV></td>
									
								</tr>";
								
								//$totalkontrabon = $totalkontrabon+$row3->v_total_fppn;
								$totalkontrabon = $totalkontrabon+$total_grand_perfaktur;

								$iter+=1;
								
							}
							
							?>
							</table>
						  </td>
						</tr>
						<tr><td colspan="6">&nbsp;</td></tr>
						<tr>
						  <td align="right" colspan="6"><?php echo $list_piutang_nilai_kontrabon; ?>&nbsp;&nbsp;<input name="v_nilai_kontrabon" type="text" id="v_nilai_kontrabon" maxlength="50" style="text-align:right;" value="<?php echo number_format($totalkontrabon,'2','.',','); ?>" style="text-align:right;" />
						  </td>
						</tr>						
                      </table></td>
					</tr>
					<tr align="right">
					  <td>			
						
						<?php 
						$uri =  $turi1."/".$turi2."/".$turi3."/".$turi4."/".$turi5."/";			
						
						echo "<a href='".base_url()."index.php/listkontrabon/cform/backlistkontrabon/".$uri."' title=\"Kembali\"><img src=".base_url()."asset/theme/images/icon_exit.png width=32 height=28 alt=\"Kembali\" border=\"0\"></a><br>Kembali";
						?>
					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
