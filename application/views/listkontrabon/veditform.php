<script language="javascript" type="text/javascript">

function addRowToTable(nItem) {
	
	var f_nota_sederhana = document.getElementById('f_nota_sederhana').value;
	
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	
	tbl.width='100%';
	
	if(iteration%2==0) {
		var bgColorValue = '#FFFFFF';
	} else {
		var bgColorValue = '#FFFFFF';
	}
	
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:18px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_faktur_code_"+nItem+"_"+iteration+"\" style=\"width:68px;\" >" +
	"<input type=\"text\" ID=\"i_faktur_code_"+nItem+"_"+iteration+"\" name=\"i_faktur_code_"+nItem+"_"+iteration+"\" style=\"width:66px;text-align:left;\" onclick=\"shfakturpenjkontrabon('"+iteration+"','"+f_nota_sederhana+"');\" readonly >" +
	"<input type=\"hidden\" ID=\"i_faktur_"+nItem+"_"+iteration+"\" name=\"i_faktur_"+nItem+"_"+iteration+"\" ></DIV>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_tgl_faktur_"+nItem+"_"+iteration+"\" style=\"width:79px;\" >" +
	"<input type=\"text\" ID=\"tgl_faktur_"+nItem+"_"+iteration+"\"  name=\"tgl_faktur_"+nItem+"_"+iteration+"\" style=\"width:77px;\" readonly >" +
	"<input type=\"hidden\" name=\"tgl_fakturhidden_"+nItem+"_"+iteration+"\" id=\"tgl_fakturhidden_"+nItem+"_"+iteration+"\" ></DIV>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_due_date_"+nItem+"_"+iteration+"\" style=\"width:82px;\" >" +
	"<input type=\"text\" ID=\"due_date_"+nItem+"_"+iteration+"\" name=\"due_date_"+nItem+"_"+iteration+"\" style=\"width:80px;\" readonly >" +
	"<input type=\"hidden\" name=\"due_datehidden_"+nItem+"_"+iteration+"\" id=\"due_datehidden_"+nItem+"_"+iteration+"\" ></DIV>";
	
	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_e_branch_name_"+nItem+"_"+iteration+"\" style=\"width:272px;\">" +
	"<input type=\"text\" ID=\"e_branch_name_"+nItem+"_"+iteration+"\" name=\"e_branch_name_"+nItem+"_"+iteration+"\" style=\"width:270px;\" readonly ></DIV>";

	var cell1 = row.insertCell(5);
	cell1.innerHTML = "<DIV ID=\"ajax_v_total_plusppn_"+nItem+"_"+iteration+"\" text-align:right;width:112\" >" +
	"<input type=\"text\" ID=\"v_total_plusppn_"+nItem+"_"+iteration+"\"  name=\"v_total_plusppn_"+nItem+"_"+iteration+"\" style=\"width:106px;text-align:right;\" readonly >" +
	"<input type=\"hidden\" ID=\"i_customer_"+nItem+"_"+iteration+"\" name=\"i_customer_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"hidden\" ID=\"i_branch_code_"+nItem+"_"+iteration+"\" name=\"i_branch_code_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\" >" +
	"</DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}

</script>

<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#tgl_kontrabon").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_kontrabon; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_kontrabon; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
			 <?php
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listkontrabon/cform/actedit', $attributes);?>
		
		<div id="masterlkontrabonform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $list_kontrabon_no_kontrabon; ?> : 
							  <input name="i_kontrabon_code" type="text" id="i_kontrabon_code" maxlength="13" value="<?php echo $idtcode; ?>" readonly />
							  <input type="hidden" name="idt" id="idt" value="<?php echo $idt; ?>" />
							  </td>
							<td width="33%"><?php echo $list_kontrabon_tgl_kontrabon; ?> : 
							  <input name="tgl_kontrabon" type="text" id="tgl_kontrabon" maxlength="10" value="<?php echo $ddt; ?>" />
							  <input name="f_nota_sederhana" type="hidden" id="f_nota_sederhana" value="<?=$fnotasederhana?>" />
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>
					<tr>
					  <td>
						  <div id="title-box2"><?php echo $form_title_detail_kontrabon; ?>					  
						  <div style="float:right;">
						  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
						  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
						  </div></div>					  
					  </td>
					</tr>
					<tr>
					  <td><table width="80%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="2%" class="tdatahead">NO</td>
							<td width="8%" class="tdatahead"><?php echo $list_kontrabon_no_faktur; ?></td>
							<td width="10%" class="tdatahead"><?php echo $list_kontrabon_tgl_faktur; ?> </td>
							<td width="10%" class="tdatahead"><?php echo $list_kontrabon_tgl_tempo; ?> </td>
							<td width="30%" class="tdatahead"><?php echo $list_kontrabon_pelanggan; ?></td>
							<td width="13%" class="tdatahead"><?php echo $list_kontrabon_jml_tagih; ?></td>
						</tr>
						<tr>
						  <td colspan="7">
						  
						  	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							
							<?php
							
							$iter	= 0;
							
							foreach($idtitem as $row3) {
								
								$exp_dfaktur = explode("-",$row3->d_faktur,strlen($row3->d_faktur)); // YYYY-mm-dd
								$exp_d_due_date = explode("-",$row3->d_due_date,strlen($row3->d_due_date));

								if($fnotasederhana=='f') {
									$query2 = $this->mclass->totalfaktur($row3->i_faktur);
								}else{
									$query2 = $this->mclass->totalfakturNONDO($row3->i_faktur);
								}

								$row2	= $query2->row();
								
								$nilai_ppn_perfaktur = (($row2->totalfaktur*11) / 100);
								
								if($row3->v_discount>0){
									$total_sblm_ppn_perfaktur	= round($row2->totalfaktur - $row3->v_discount); // DPP
									$nilai_ppn2_perfaktur	= round((($total_sblm_ppn_perfaktur*11) / 100));
									$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
								}else{
									$total_sblm_ppn_perfaktur	= round($row2->totalfaktur); // DPP
									$nilai_ppn2_perfaktur	= round($nilai_ppn_perfaktur);
									$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
								}
																
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">".($iter*1+1)."</div></td>
									
									<td><DIV ID=\"ajax_i_faktur_code_tblItem_".$iter."\" style=\"width:68px;\">
									<input type=\"text\" ID=\"i_faktur_code_tblItem_".$iter."\" name=\"i_faktur_code_tblItem_".$iter."\" style=\"width:66px;text-align:left;\" value=\"".$row3->i_faktur_code."\" onclick=\"shfakturpenjkontrabon('".$iter."','".$fnotasederhana."');\" readonly >
									<input type=\"hidden\" ID=\"i_faktur_tblItem_".$iter."\" name=\"i_faktur_tblItem_".$iter."\" value=\"".$row3->i_faktur."\" ></DIV></td>
									
									<td><DIV ID=\"ajax_tgl_faktur_tblItem_".$iter."\" style=\"width:79px;\" >
									<input type=\"text\" ID=\"tgl_faktur_tblItem_".$iter."\" name=\"tgl_faktur_tblItem_".$iter."\" style=\"width:77px;\" value=\"".$exp_dfaktur[2]."/".$exp_dfaktur[1]."/".$exp_dfaktur[0]."\" readonly >
									<input type=\"hidden\" ID=\"tgl_fakturhidden_tblItem_".$iter."\" name=\"tgl_fakturhidden_tblItem_".$iter."\" value=\"".$row3->d_faktur."\"></DIV></td>
									
									<td><DIV ID=\"ajax_due_date_tblItem_".$iter."\" style=\"width:82px;\" >
									<input type=\"text\" ID=\"due_date_tblItem_".$iter."\" name=\"due_date_tblItem_".$iter."\" style=\"width:80px;\" value=\"".$exp_d_due_date[2]."/".$exp_d_due_date[1]."/".$exp_d_due_date[0]."\" readonly >
									<input type=\"hidden\" ID=\"due_datehidden_tblItem_".$iter."\" name=\"due_datehidden_tblItem_".$iter."\" value=\"".$row3->d_due_date."\"></DIV></td>
									
									<td><DIV ID=\"ajax_e_branch_name_tblItem_".$iter."\" style=\"width:272px;\">
									<input type=\"text\" ID=\"e_branch_name_tblItem_".$iter."\" name=\"e_branch_name_tblItem_".$iter."\" style=\"width:270px;\" value=\"".$row3->e_branch_name."\" readonly ></DIV></td>
									
									<td><DIV ID=\"ajax_v_total_plusppn_tblItem_".$iter."\" text-align:right; width:112px;\" >
									<input type=\"text\" ID=\"v_total_plusppn_tblItem_".$iter."\" name=\"v_total_plusppn_tblItem_".$iter."\" style=\"width:106px;text-align:right;\" value=\"".$total_grand_perfaktur."\" readonly >
									<input type=\"hidden\" ID=\"i_customer_tblItem_".$iter."\" name=\"i_customer_tblItem_".$iter."\" value=\"".$row3->i_customer."\" >
									<input type=\"hidden\" ID=\"i_branch_code_tblItem_".$iter."\" name=\"i_branch_code_tblItem_".$iter."\" value=\"".$row3->i_branch_code."\" >
									<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\"></DIV></td>
									
								</tr>";
								
								$iter+=1;
								
							}
							
							?>
							</table>
						  </td>
						</tr>						
                      </table></td>
					</tr>	
					<tr>
					 <td>
					  <div id="title-box2">
					  <div style="float:right;">
					  &nbsp;
					  </div></div>
					 </td>	
					</tr>
					<tr align="right">
					  <td>
						<input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit">
						<input name="btnbatal" id="btnbatal" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listkontrabon/cform'" />

					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
