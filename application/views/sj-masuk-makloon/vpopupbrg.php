<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var des=$("#des").val();
		var qty=$("#qty").val();
		var satuan=$("#satuan").val();
		var harga=$("#harga").val();
		var q=$("#q").val();
		
		if (qty == undefined || qty == '')
			qty = 0;
		
		if (q == 'b') {
			opener.document.forms["f_purchase"].bhn_baku_<?php echo $posisi ?>.value=idx+" - "+des;
			opener.document.forms["f_purchase"].kode_bhn_baku_<?php echo $posisi ?>.value=idx;
			self.close();
		}
		else {
			opener.document.forms["f_purchase"].kode_bhn_quilting_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].bhn_quilting_<?php echo $posisi ?>.value=idx+" - "+des;
			opener.document.forms["f_purchase"].satuan_<?php echo $posisi ?>.value=satuan;
			opener.document.forms["f_purchase"].harga_<?php echo $posisi ?>.value=harga;
			self.close();
		}
	});
});
</script>
<center><h3><?php if ($jenisnya == 'b') { ?>Daftar Bahan Baku/Pembantu <?php } else echo "Daftar Bahan Quilting"; ?></h3></center>
<div align="center"><br>
<?php echo form_open('sj-masuk-makloon/cform/show_popup_brg'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="kode_supplier" value="<?php echo $kode_supplier ?>">
<input type="hidden" name="jenisnya" value="<?php echo $jenisnya ?>">
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="des" id="des">
<input type="hidden" name="qty" id="qty">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="harga" id="harga">
<input type="hidden" name="q" id="q" value="<?php echo $jenisnya ?>">
</form>

  <table border="1" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" width="100%" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Stok Terkini</th>
      <th bgcolor="#999999">Fungsi</th>
    </tr>
	<?php 
		//$i=1; 
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
			
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg']; ?></td>
      <td><?php echo $query[$j]['nama_brg']; ?></td>
      <td><?php echo $query[$j]['satuan']; ?></td>
      <td><?php if ($query[$j]['jum_stok']!= 0) echo $query[$j]['jum_stok']; else echo "0"; ?></td>
      <td>
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
	  window.document.f_master_brg.des.value='<?php if($jenisnya == 'q') echo str_replace("\"", "&quot;", $query[$j]['nama_brg']); else echo str_replace("'", "\'", $query[$j]['nama_brg']); ?>'; 
	  window.document.f_master_brg.qty.value='<?php echo $query[$j]['jum_stok'] ?>';
	  window.document.f_master_brg.satuan.value='<?php echo $query[$j]['satuan'] ?>';
	  window.document.f_master_brg.harga.value='<?php echo $query[$j]['harga'] ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
