<h3>Data SJ Masuk Hasil Quilting</h3><br>
<a href="<? echo base_url(); ?>index.php/sj-masuk-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-masuk-makloon/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tgl_sj = $("#tgl_sj").val();
	var no_sj = $("#no_sj").val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	
	if (no_sj == '') {
		alert ("Nomor SJ harus diisi");
		$("#no_sj").focus();
		return false;
	}
	if (tgl_sj == '') {
		alert ("Tgl SJ harus dipilih");
		$("#no_sj").focus();
		return false;
	}
	if (jenis_pembelian == '0') {
		alert("Jenis pembelian harus dipilih..!");
		return false;
	}
	
	/*if(document.getElementById('kode_1').value=='') {
		alert('Maaf, item barangnya harus diisi.');
		document.getElementById('kode_1').focus();
		return false;
	} */
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#bhn_baku_'+k).val() == '') {
				alert("Bahan baku tidak boleh ada yang kosong...!");
				return false;
			}
			if ($('#bhn_quilting_'+k).val() == '') {
				alert("Bahan quilting tidak boleh ada yang kosong...!");
				return false;
			}
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka atau desimal..!");
				return false;
			}
			
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2) { ?>
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Data harga harus berupa angka atau desimal..!");
					return false;
				}
			<?php } ?>
			
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

//tambah
$(function()
{
	//var goedit = $("#goedit").val();
	//if(goedit!='1')
		//$("#no").val('2');
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****id_sj_detail*************************************
		var id_sj_detail="#id_sj_detail_"+n;
		var new_id_sj_detail="#id_sj_detail_"+no;
		$(id_sj_detail, lastRow).attr("id", "id_sj_detail_"+no);
		$(new_id_sj_detail, lastRow).attr("name", "id_sj_detail_"+no);		
		$(new_id_sj_detail, lastRow).val('');				
		//*****end id_sj_detail*************************************	
				
		//*****bhn_baku*************************************
		var bhn_baku="#bhn_baku_"+n;
		var new_bhn_baku="#bhn_baku_"+no;
		$(bhn_baku, lastRow).attr("id", "bhn_baku_"+no);
		$(new_bhn_baku, lastRow).attr("name", "bhn_baku_"+no);		
		$(new_bhn_baku, lastRow).val('');		
		//*****end bhn_baku*********************************
		
		//*****kode_bhn_baku*************************************
		var kode_bhn_baku="#kode_bhn_baku_"+n;
		var new_kode_bhn_baku="#kode_bhn_baku_"+no;
		$(kode_bhn_baku, lastRow).attr("id", "kode_bhn_baku_"+no);
		$(new_kode_bhn_baku, lastRow).attr("name", "kode_bhn_baku_"+no);		
		$(new_kode_bhn_baku, lastRow).val('');		
		//*****end kode_bhn_baku*********************************
		
		//*****kode_bhn_baku_lama*************************************
		var kode_bhn_baku_lama="#kode_bhn_baku_lama_"+n;
		var new_kode_bhn_baku_lama="#kode_bhn_baku_lama_"+no;
		$(kode_bhn_baku_lama, lastRow).attr("id", "kode_bhn_baku_lama_"+no);
		$(new_kode_bhn_baku_lama, lastRow).attr("name", "kode_bhn_baku_lama_"+no);		
		$(new_kode_bhn_baku_lama, lastRow).val('');		
		//*****end kode_bhn_baku_lama*********************************
		
		//*****bhn_quilting*************************************
		var bhn_quilting="#bhn_quilting_"+n;
		var new_bhn_quilting="#bhn_quilting_"+no;
		$(bhn_quilting, lastRow).attr("id", "bhn_quilting_"+no);
		$(new_bhn_quilting, lastRow).attr("name", "bhn_quilting_"+no);		
		$(new_bhn_quilting, lastRow).val('');		
		//*****end bhn_quilting*********************************
		
		//*****kode_bhn_quilting*************************************
		var kode_bhn_quilting="#kode_bhn_quilting_"+n;
		var new_kode_bhn_quilting="#kode_bhn_quilting_"+no;
		$(kode_bhn_quilting, lastRow).attr("id", "kode_bhn_quilting_"+no);
		$(new_kode_bhn_quilting, lastRow).attr("name", "kode_bhn_quilting_"+no);		
		$(new_kode_bhn_quilting, lastRow).val('');		
		//*****end kode_bhn_quilting*********************************
		
		//*****kode_bhn_quilting_lama*************************************
		var kode_bhn_quilting_lama="#kode_bhn_quilting_lama_"+n;
		var new_kode_bhn_quilting_lama="#kode_bhn_quilting_lama_"+no;
		$(kode_bhn_quilting_lama, lastRow).attr("id", "kode_bhn_quilting_lama_"+no);
		$(new_kode_bhn_quilting_lama, lastRow).attr("name", "kode_bhn_quilting_lama_"+no);		
		$(new_kode_bhn_quilting_lama, lastRow).val('');		
		//*****end kode_bhn_quilting_lama*********************************
				
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****qty_lama*************************************
		var qty_lama="#qty_lama_"+n;
		var new_qty_lama="#qty_lama_"+no;
		$(qty_lama, lastRow).attr("id", "qty_lama_"+no);
		$(new_qty_lama, lastRow).attr("name", "qty_lama_"+no);		
		$(new_qty_lama, lastRow).val('');				
		//*****end qty_lama*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		<?php if ($this->session->userdata('gid') == 4) { ?>
			$(new_harga, lastRow).attr("readonly", true);
		<?php } ?>
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
	/*	var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('');				*/
		//*****end harga_lama*************************************	
		
		//*****pajak*************************************
		var pajak="#pajak_"+n;
		var new_pajak="#pajak_"+no;
		$(pajak, lastRow).attr("id", "pajak_"+no);
		$(new_pajak, lastRow).attr("name", "pajak_"+no);		
		$(new_pajak, lastRow).val('0');				
		//*****end pajak*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);	
		
		 var  even_klik= "var kode_sup= $('#unit_makloon').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/show_popup_brg/"+no+"/'+kode_sup+'/b');";
		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		 $(new_pilih, lastRow).attr("disabled",false);
		//end button pilih		
		
		//button pilih_q*****************************************
		 var pilih_q="#pilih_q_"+n;
		 var new_pilih_q="#pilih_q_"+no;
		 $(pilih_q, lastRow).attr("id","pilih_q_"+no);
		
		 // onclick="javascript: var kode_sup = $('#unit_makloon').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/show_popup_brg/1/' + kode_sup+'/q');"
		 var  even_klik= "var kode_sup= $('#unit_makloon').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/show_popup_brg/"+no+"/'+kode_sup+'/q');";
		 $(new_pilih_q, lastRow).attr("name", "pilih_q_"+no);		
		 $(new_pilih_q, lastRow).attr("onclick",even_klik);		 
		 $(new_pilih_q, lastRow).attr("disabled",false);
		//end button pilih_q
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		/*var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		} */
		
		var x= $("#no").val();	
		var jumawal= $("#jumawal").val();	
		//alert(x + " "+ jumawal); alert(x>jumawal);
		if (parseInt(x) > parseInt(jumawal)) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
			
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
	status_pkp = $('#hide_pkp').val();
	status_pajak = $('#hide_tipe_pajak').val();
	
	if (status_pkp == 't') {
		var i=1;
		var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					//var diskon=$("#diskon_"+i).val();
				
					//$('input[name=pkp]').attr('checked', true);
					if (status_pajak == 'I') {
						//$('#tipe_pajak option[value=I]').attr('selected', 'selected');
						var hitung = harga*qty;
						var pi = hitung/1.1;
						new_pajak = hitung-pi;
						new_pajak = new_pajak.toFixed(2);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(hitung);
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					}
					else {
						//$('#tipe_pajak option[value=E]').attr('selected', 'selected');
						var hitung = harga*qty;
						var new_pajak = hitung*0.1;
						new_pajak = new_pajak.toFixed(2);
						new_total = parseFloat(new_pajak)+parseFloat(hitung);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(new_total);
						gtotal = parseFloat(gtotal)+parseFloat(new_total);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					} //end cek item pajak
				}
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(2);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal/1.1;
				dpp = dpp.toFixed(2);
				$("#dpp").val(dpp);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
	}
	else if (status_pkp == 'f') {
				var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var hitung = harga*qty;
					$("#pajak_"+i).val('0');
					$("#total_"+i).val(hitung);
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				$("#gtotal").val(gtotal);
				$("#tot_pajak").val('0');
				$("#dpp").val('0');
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
	}
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="is_no_sjkeluar" id="is_no_sjkeluar" value="<?php if ($is_no_sjkeluar == 1) echo "t"; else echo "f"; ?>">
<input type="hidden" name="id_sj_masuk" id="id_sj_masuk" value="<?php echo $query[0]['id'] ?>">
<input type="hidden" name="unit_makloon" id="unit_makloon" value="<?php echo $query[0]['kode_unit'] ?>">
<input type="hidden" name="hide_pkp" id="hide_pkp" value="<?php echo $query[0]['hide_pkp'] ?>">
<input type="hidden" name="hide_tipe_pajak" id="hide_tipe_pajak" value="<?php echo $query[0]['hide_tipe_pajak'] ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="cunit_makloon" value="<?php echo $cunit_makloon ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

Edit Data

<?php 
		$detail_sj = $query[0]['detail_sj']; 
		$jumawal = count($query[0]['detail_sj'])+1;
		
		if (count($query[0]['detail_sj'])>0) {
			$no=1;
			foreach ($query[0]['detail_sj'] as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">

<div align="center">
<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
		<td width="20%">Tanpa acuan SJ Keluar
		</td>
  </tr>
		<tr>
			<td>Unit Makloon</td>
			<td><?php echo $query[0]['kode_unit']." - ". $query[0]['nama_unit'] ?></td> 
		</tr>

  <tr>
    <td>Nomor SJ Masuk</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" maxlength="20" value="<?php echo $query[0]['no_sj'] ?>">
    </td>
    
  </tr>
  <tr>
    <td>Tgl SJ Masuk</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="<?php echo $query[0]['tgl_sj'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian" >
				<option value="0">-Pilih-</option>
				<option value="1" <?php if ($query[0]['jenis_pembelian'] == '1') { ?> selected <?php } ?> >Cash</option>
				<option value="2" <?php if ($query[0]['jenis_pembelian'] == '2') { ?> selected <?php } ?> >Kredit</option>
			</select>&nbsp;</td> 
  </tr>
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="10" align="right"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode & Nama Bhn Baku</th>
          <th>Kode & Nama Bhn Quilting</th>
          <th>Satuan</th>
	      <th>Qty Bhn Quilting</th>
          <th>Harga</th>
	      <th>PPN</th>
          <th>Total</th>
        </tr>
        <?php $i=1;
        if (count($query[0]['detail_sj'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
			$detail_sj = $query[0]['detail_sj'];
			for($k=0;$k<count($detail_sj);$k++) {
        ?>
         <tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?>
			<input name="id_sj_detail_<?php echo $i ?>" type="hidden" id="id_sj_detail_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['id'] ?>" />
          </td>
          
          <td nowrap="nowrap">
           <input name="bhn_baku_<?php echo $i ?>" type="text" id="bhn_baku_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $detail_sj[$k]['kode_brg']." - ".$detail_sj[$k]['nama_brg'] ?>" />
           <input name="kode_bhn_baku_<?php echo $i ?>" type="hidden" id="kode_bhn_baku_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['kode_brg'] ?>">  
           <input name="kode_bhn_baku_lama_<?php echo $i ?>" type="hidden" id="kode_bhn_baku_lama_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['kode_brg'] ?>">  
           <input title="browse data bhn baku" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" 
           onclick="javascript: var kode_sup = $('#unit_makloon').val();
           openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/show_popup_brg/<?php echo $i ?>/' + kode_sup+'/b');"></td>
         <td><input name="bhn_quilting_<?php echo $i ?>" type="text" id="bhn_quilting_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $detail_sj[$k]['kode_brg_makloon']." - ".str_replace("\"", "&quot;", $detail_sj[$k]['nama_brg_makloon']) ?>"/>
			<input name="kode_bhn_quilting_<?php echo $i ?>" type="hidden" id="kode_bhn_quilting_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['kode_brg_makloon'] ?>">  
			<input name="kode_bhn_quilting_lama_<?php echo $i ?>" type="hidden" id="kode_bhn_quilting_lama_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['kode_brg_makloon'] ?>">
			
			<input title="browse data bhn quilting" name="pilih_q_<?php echo $i ?>" value="..." type="button" id="pilih_q_<?php echo $i ?>" 
           onclick="javascript: var kode_sup = $('#unit_makloon').val();
           openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/show_popup_brg/<?php echo $i ?>/' + kode_sup+'/q');">
         </td>
         <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="7" readonly="true" value="<?php echo $detail_sj[$k]['satuan'] ?>" /></td>
         <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="10" onkeyup="hitungnilai()" onblur="hitungnilai()" value="<?php echo $detail_sj[$k]['qty'] ?>" />
			<input name="qty_lama_<?php echo $i ?>" type="hidden" id="qty_lama_<?php echo $i ?>" value="<?php echo $detail_sj[$k]['qty'] ?>" />
         </td>
         <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="10" value="<?php echo $detail_sj[$k]['harga'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()" <?php if ($this->session->userdata('gid') == 4) { ?> readonly="true" <?php } ?> /></td>
         <td><input name="pajak_<?php echo $i ?>" type="text" id="pajak_<?php echo $i ?>" size="8" readonly="true" value="<?php echo $detail_sj[$k]['pajak'] ?>" /></td>
         <td><input name="total_<?php echo $i ?>" type="text" id="total_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $detail_sj[$k]['biaya'] ?>" /></td>
        </tr>
        <?php $i++; } // end foreach 
		}
		?>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td>DPP</td>
			<td>: <input type="text" name="dpp" id="dpp" value="<?php echo $query[0]['dpp'] ?>" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Total PPN</td>
			<td>: <input type="text" name="tot_pajak" id="tot_pajak" value="<?php echo $query[0]['total_pajak'] ?>" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Grand Total</td>
			<td>: <input type="text" name="gtotal" id="gtotal" value="<?php echo $query[0]['total'] ?>" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Uang Muka</td>
			<td>: <input type="text" name="uang_muka" id="uang_muka" value="<?php echo $query[0]['uang_muka'] ?>" size="10" onkeyup="hitunghutang()"></td>
		</tr>
		<tr>
			<td>Sisa Hutang</td>
			<td>: <input type="text" name="sisa_hutang" id="sisa_hutang" value="<?php echo $query[0]['sisa_hutang'] ?>" size="10" readonly="true"></td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>: <input type="text" name="ket" id="ket" value="<?php echo $query[0]['keterangan'] ?>" size="30" maxlength="30"></td>
		</tr>
	</table>
	</form>
      <div align="center"><br><br> 
        <?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "sj-masuk-makloon/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "sj-masuk-makloon/cform/cari/".$cunit_makloon."/".$carinya."/".$cur_page;
        ?>
        
        <input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
      </div></td>
    </tr>

</table>
</div>
</form>
