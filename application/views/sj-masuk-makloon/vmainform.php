<h3>Data SJ Masuk Hasil Quilting</h3><br>
<a href="<? echo base_url(); ?>index.php/sj-masuk-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-masuk-makloon/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	//$("#no").val('2');
	
	//hitung_total();
	
	$('#is_no_sjkeluar').click(function(){
	  	    if ($('input[name=is_no_sjkeluar]').is(':checked')) {
				$('#id_brg').val('');
				$('#sj_keluar').val('');
				$("#pilih").attr("disabled", true);
			}
			else {
				$('#id_brg').val('');
				$('#sj_keluar').val('');
				$("#pilih").attr("disabled", false);
			}
	  });
	
	$('#unit_makloon').change(function(){
		$("#sj_keluar").val('');
	  	$("#id_brg").val('');	
	  });
	  hitungnilai();
		
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 1200;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

/*function hitung_total() {
	var jum_detail = $('#no').val()-1;
	
	var i=1;
	var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var biaya=$("#biaya_"+i).val(); 
					if (biaya == '')
						biaya = 0;

					gtotal = parseFloat(gtotal)+parseFloat(biaya);
					gtotal = gtotal.toFixed(2);
					$("#totalnya").val(gtotal);
				}
    
} */

function cek_item_brg() {
	var id_brg= $('#id_brg').val();
	var sj_keluar= $('#sj_keluar').val();
	
	if ($('input[name=is_no_sjkeluar]').is(':checked') == false) {
		if (sj_keluar == '') {
			alert("Nomor SJ Keluar harus dipilih..!");
			return false;
		}
		if (id_brg == '') {
			alert("item barang di SJ harus dipilih..!");
			return false;
		}
	}
}

function cek_input() {
	var no_sj= $('#no_sj').val();
	var tgl= $('#tgl_sj').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
		
	if (no_sj == '') {
		alert("Nomor SJ Masuk harus diisi..!");
		return false;
	}
	if (tgl == '') {
		alert("Tanggal SJ Masuk harus dipilih..!");
		return false;
	}
	if (jenis_pembelian == '0') {
		alert("Jenis_pembelian harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#qty_meter_'+k).val() == '0' || $('#qty_meter_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_meter_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			if($('#qty_hasil_'+k).val() == '0' || $('#qty_hasil_'+k).val() == '' ) {				
				alert("Data qty hasil quilting tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_hasil_'+k).val()) ) {
				alert("Qty hasil quilting harus berupa angka..!");
				return false;
			}
			
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;

				var i=1;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_meter_"+i).val();
					
					var qtyyard = parseFloat(qty)/0.91;
					qtyyard = qtyyard.toFixed(2);
					
					$("#qty_hasil_"+i).val(qtyyard);
				}

}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
	$attributes = array('name' => 'f_makloon', 'id' => 'f_makloon');
	echo form_open('sj-masuk-makloon/cform/', $attributes);
?>
<table>
	<tr>
		<td width="20%"> Tidak ada acuan SJ Keluar Proses Quilting </td>
		<td> <input type="checkbox" name="is_no_sjkeluar" id="is_no_sjkeluar" value="t"> &nbsp;
		 </td>
	</tr>
	<tr>
		<td>Unit Makloon</td>
		<td><select name="unit_makloon" id="unit_makloon" onkeyup="this.blur();this.focus();">
				<?php foreach ($list_quilting as $quilting) { ?>
					<option value="<?php echo $quilting->kode_supplier ?>"><?php echo $quilting->kode_supplier." - ". $quilting->nama ?></option>
				<?php } ?>
			</select></td> 
</tr>
	<tr>
		<td>Nomor SJ Keluar</td>
		<td><input name="sj_keluar" type="text" id="sj_keluar" size="40" readonly="true" value=""/>
			<input type="hidden" name="id_brg" id="id_brg" value="">
			
			<input title="browse data SJ keluar" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: var x= $('#unit_makloon').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/show_popup_sj_keluar/'+ x);" type="button"></td>
	</tr>
	
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/view'">
<?php echo form_close(); 
} else { ?>


<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="unit_makloon" value="<?php echo $unit_makloon ?>">

<?php 
		if (count($sj_detail)>0) {
			$no=1;
			foreach ($sj_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<div align="center">

<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
<tr>
			<td width="15%">Unit Makloon</td>
			<td width="70%"><?php echo $unit_makloon." - ". $nama_unit ?></td> 
		</tr>
  <tr>
    <td>No SJ Keluar</td>
    <td>
      <input name="no_sj_keluar" type="text" id="no_sj_keluar" size="40" readonly="true" value="<?php echo $no_sj_keluar; ?>">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Nomor SJ Masuk</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" value="">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Tgl SJ Masuk</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian" >
				<option value="0">-Pilih-</option>
				<option value="1">Cash</option>
				<option value="2">Kredit</option>
			</select>&nbsp;</td> 
  </tr>
  <tr>
		<td>Keterangan</td>
		<td><input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
	</tr>
	
	<tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>No / Tgl SJ Keluar</th>
           <th>Utk Brg Jadi</th>
           <th>Kode & Nama Bhn Baku</th>
           <th>Kode & Nama Bhn Quilting</th>
           <th>Qty Bahan (m)</th>
	      <th>Qty Hsl Quilting (Yard)</th>
	      <th>Detail Pjg Hsl Quilting (Yard)</th>
        </tr>

        <?php $i=1;
        if (count($sj_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($sj_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          
          <td nowrap="nowrap">
           <input name="no_sj_keluar_<?php echo $i ?>" type="text" id="no_sj_keluar_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['no_sj']." / ".$sj_detail[$j]['tgl_sj'] ?>"/>
           </td>
          
          <td nowrap="nowrap">
           <input name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['nama_brg_jadi'] ?>"/>
           <input type="hidden" name="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['kode_brg_jadi'] ?>" >
           </td>
          
          <td nowrap="nowrap">
           <input name="bhn_baku_<?php echo $i ?>" type="text" id="bhn_baku_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg']." - ".$sj_detail[$j]['nama_brg'] ?>"/>
           <input type="hidden" name="id_sj_detail_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['id'] ?>" >
           <input type="hidden" name="id_sj_proses_quilting_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['id_sj_proses_quilting'] ?>" >
           <input type="hidden" name="kode_brg_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['kode_brg'] ?>" >
           </td>
           <td nowrap="nowrap">
           <input name="brg_makloon_<?php echo $i ?>" type="text" id="brg_makloon_<?php echo $i ?>" size="40" readonly="true" value="<?php echo $sj_detail[$j]['kode_brg_makloon']." - ".str_replace("\"", "&quot;", $sj_detail[$j]['nama_brg_makloon']) ?>"/>
           <input type="hidden" name="kode_brg_makloon_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['kode_brg_makloon'] ?>" >
           </td>
           
          <td><input name="qty_meter_<?php echo $i ?>" type="text" id="qty_meter_<?php echo $i ?>" size="5" maxlength="5" 
          value="<?php echo $sj_detail[$j]['qty_meter'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()" readonly="true" /> </td>
          <td><input name="qty_hasil_<?php echo $i ?>" type="text" id="qty_hasil_<?php echo $i ?>" size="5" maxlength="5" 
          value="" readonly="true"  /> </td>
          
          <td nowrap="nowrap">
           <input name="detail_yard_<?php echo $i ?>" type="text" id="detail_yard_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $sj_detail[$j]['detail_pjg_kain_yard'] ?>"/>
          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/'"></td>
		</tr>
	</table>	
	
	</form>
</td>
    </tr>
</table>
</div>
</form>
<?php } ?>
