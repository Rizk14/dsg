<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data SJ Masuk Hasil Quilting</h3><br> 
<a href="<? echo base_url(); ?>index.php/sj-masuk-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-masuk-makloon/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('sj-masuk-makloon/cform/cari'); ?>
Unit Makloon: <select name="unit_makloon" id="unit_makloon">
				<option value="0" <?php if ($unit_makloon == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_unit as $unitnya) { ?>
					<option value="<?php echo $unitnya->kode_supplier ?>" <?php if ($unit_makloon == $unitnya->kode_supplier) { ?> 
					selected="true" <?php } ?> ><?php echo $unitnya->kode_supplier." - ". $unitnya->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>No SJ Keluar</th>
		 <th>No SJ Masuk</th>
		 <th>Tgl SJ Masuk</th>
		 <th>Unit Makloon</th>
		<!-- <th>Jenis Makloon</th> -->
		<th>Utk Barang Jadi</th>
		 <th>List Bahan Baku</th>
		 <th>List Bhn & Qty Quilting</th>
		 <?php
			if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2) {
				echo "<th>Jum Total (Rp.)</th>";
			}
		 ?>
		 <th>Keterangan</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
								 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_sj_keluar']."</td>";
				 echo    "<td nowrap>".$query[$j]['no_sj']."</td>";
				 echo    "<td>".$tgl_sj."</td>";
				 echo    "<td>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 
				// echo    "<td>".$query[$j]['nama_jenis_makloon']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_masuk_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_masuk_detail'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nama_brg_jadi'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_masuk_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_masuk_detail'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['data_masuk_detail'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['data_masuk_detail'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg_makloon']." - ".$var_detail[$k]['nama_brg_makloon']." (".$var_detail[$k]['qty']." Yard)";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				
				if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2) { 
					echo "<td align='right'>";
					echo number_format($query[$j]['total'], 2, ',','.');
					echo "</td>";
				}
				echo    "<td>".$query[$j]['keterangan']."</td>";
				 echo    "<td>".$tgl_update."</td>";
				 
				 if ($query[$j]['status_faktur'] == 'f') {
					echo "<td align=center>";
					echo "<a href=".base_url()."index.php/sj-masuk-makloon/cform/edit/".$query[$j]['id']."/".$query[$j]['is_no_sjkeluar']."/".$cur_page."/".$is_cari."/".$unit_makloon."/".$cari." \">Edit</a>";
					
					//if ($this->session->userdata('gid') == 1) {
					echo "&nbsp;<a href=".base_url()."index.php/sj-masuk-makloon/cform/delete/".$query[$j]['id']."/".$query[$j]['id_detailnya']."/".$query[$j]['is_no_sjkeluar']."/".$cur_page."/".$is_cari."/".$unit_makloon."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
					//}
				 }
				 else {
					echo  "<td>&nbsp;</td> ";
				 }
				
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
