<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:450px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function() {
	$('#filter_brg').click(function(){
	  	    if ($("#filter_brg").is(":checked")) {
				$('#cari_brg').attr('disabled', false);
				$('#cari_brg').val('');
			}
			else {
				$('#cari_brg').attr('disabled', true);
				$('#cari_brg').val('');
			}
	  });
});

</script>

<h3>Data Approve SJ Masuk Barang WIP Ke Gudang QC</h3><br>
<a href="<?php echo base_url(); ?>index.php/app-sj/cform/">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/app-sj/cform/view">View Data</a>&nbsp;<br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('app-sj/cform/carisjmasuk'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td style="white-space:nowrap;">Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;">Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>
	
	<tr>
		<td>Unit Jahit</td>
		<td>: <select name="id_unit_jahit" id="id_unit_jahit">
				<option value="0">- All -</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" <?php if ($jht->id == $id_unit_jahit) { ?> selected <?php } ?> ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Unit Packing</td>
		<td>: <select name="id_unit_packing" id="id_unit_packing">
				<option value="0">- All -</option>
				<?php foreach ($list_unit_packing as $pck) { ?>
					<option value="<?php echo $pck->id ?>" <?php if ($pck->id == $id_unit_packing) { ?> selected <?php } ?> ><?php echo $pck->kode_unit."-".$pck->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	
	<tr>
		<td style="white-space:nowrap;">Lokasi Gudang</td>
		<td style="white-space:nowrap;">: <select name="gudang" id="gudang">
				<option value="0">- All -</option>
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($gud->id == $gudang) { ?> selected <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	
	<tr>
		<td>Jenis Masuk</td>
		<td>: <select name="jenis_masuk" id="jenis_masuk">
				<option value="" <?php if ($jenis_masuk == '') { ?>selected<?php } ?> >- All -</option>
				<option value="1" <?php if ($jenis_masuk == '1') { ?>selected<?php } ?> >Masuk bagus hasil jahit</option>
				<option value="2" <?php if ($jenis_masuk == '2') { ?>selected<?php } ?> >Lain-lain (Perbaikan dari unit jahit)</option>
				<option value="3" <?php if ($jenis_masuk == '3') { ?>selected<?php } ?> >Lain-lain (Retur dari unit packing)</option>
				<option value="4" <?php if ($jenis_masuk == '4') { ?>selected<?php } ?> >Lain-lain (Retur dari gudang jadi)</option>
				<option value="5" <?php if ($jenis_masuk == '5') { ?>selected<?php } ?> >Lain-lain (Lainnya)</option>
				</select>
		</td>
	</tr>
	
	<tr>
		<td style="white-space:nowrap;">No SJ</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;" colspan="2"><input type="checkbox" name="filter_brg" id="filter_brg" value="y" <?php if ($filterbrg == 'y') { ?> checked="true" <?php } ?>>
		Filter berdasarkan kode/nama brg: <input type="text" name="cari_brg" id="cari_brg" size="10" value="<?php echo $caribrg ?>" <?php if ($filterbrg == 'n') { ?>disabled="true" <?php } ?>></td>
	</tr>
	
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" >&nbsp;
		<!--&nbsp;<input type="submit" name="submit2" value="Export ke Excel" >--></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>
<?php 
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;
	
	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
?>
<form id="f_master_brg" name="f_master_brg">
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>Gudang</th>
		<th>No SJ</th>
		 <th>Tgl SJ</th>
		 <th>Jenis Masuk</th>
		 <th>Unit Jahit</th>
		 <th>Unit Packing</th>
		 <th>List Brg WIP</th>
		 <th>Qty</th>		 
		 <th>Qty Ket Warna</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				
				 
			 for($j=0;$j<count($query);$j++){
				 
				  $var_detail_det = $query[$j]['detail_sj'];
				  for($k=0;$k<count($var_detail_det);$k++){
					  $var_detail_det_id=$var_detail_det[0]['id'];
					
				  }
				 
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";

				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;

				$pisah1 = explode("-", $query[$j]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['nama_gudang']."</td>";
				 echo    "<td>".$query[$j]['no_sj']."</td>";
				 echo    "<td>".$tgl_sj."</td>";
				 echo    "<td>".$query[$j]['nama_jenis']."</td>";
				 
				 if ($query[$j]['id_unit_jahit'] != '0')
					echo    "<td style='white-space:nowrap;'>".$query[$j]['kode_unit_jahit']." - ".$query[$j]['nama_unit_jahit']."</td>";
				else
					echo "<td> - </td>";
				
				if ($query[$j]['id_unit_packing'] != '0')
					echo    "<td style='white-space:nowrap;'>".$query[$j]['kode_unit_packing']." - ".$query[$j]['nama_unit_packing']."</td>";
				else
					echo "<td> - </td>";
				 
				 // 24-10-2015
				 if ($cari == '')
					$xcari = "all";
				 else
					$xcari = $cari;
					
				 if ($caribrg == '')
					$xcaribrg = "all";
				 else
					$xcaribrg = $caribrg;
					
				if ($jenis_masuk == '')
					$xjenis_masuk = "all";
				 else
					$xjenis_masuk = $jenis_masuk;
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_sj'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_sj'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg_wip']." - ".$var_detail[$k]['nama_brg_wip'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_sj'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_sj'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_sj'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_sj'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['ket_qty_warna'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				  
				 echo    "<td align='center'>".$tgl_update."</td>";
				   if((($query[$j]['tgl_sj']) < ($query[$j]['tanggal_periode']) )&&  ($query[$j]['status_btb'])=='t'){
				echo "<td><i>* Data SJ sudah di closing  dan telah dibuat BTB</i></td>";
			
				}
				elseif(($query[$j]['tgl_sj']) < ($query[$j]['tanggal_periode'])){
				echo "<td><i>* Data SJ sudah di closing </i></td>";
			}
			elseif(($query[$j]['status_btb'])=='t'){
				echo "<td><i>* Data SJ telah dibuat BTB </i></td>";
			}
			elseif(($query[$j]['periode'])!=null){
				echo "<td><i>* Data sudah di closing periode</i></td>";
				}
				else{
					 echo    "<td align=center>";
					echo "&nbsp;<a href=".base_url()."index.php/app-sj/cform/delete/". $var_detail_det_id."/".$cur_page."/".$is_cari."/".$id_unit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$xcari."/".$xcaribrg."/".$filterbrg." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				echo "	</td>";
				 }
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
