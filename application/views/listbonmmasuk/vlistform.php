<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_inbonm; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_inbonm; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listbonmmasuk/cform/carilistbonmmasuk','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlbonmmasukform">

		  <table width="95%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_tgl_mulai_inbonm; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					  <input name="d_bonmmasuk_first" type="text" id="d_bonmmasuk_first" maxlength="10" value="<?php echo $tglbonmmasukmulai; ?>"/>
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_bonmmasuk_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_bonmmasuk_last" type="text" id="d_bonmmasuk_last" maxlength="10" value="<?php echo $tglbonmmasukakhir; ?>"/>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_bonmmasuk_last,'dd/mm/yyyy',this)"></td>
				  </tr>
				  <tr>
					<td width="33%">Bon M Masuk</td>
					<td width="0%">:</td>
					<td>
					<?php
					$bonmmasuk	= array(
					'name'=>'bonmmasuk',
					'id'=>'bonmmasuk',
					'type'=>'text',
					'maxlength'=>'14',
					'style'=>'width:100px;',
					'value'=>$bonmmasuk
					);
					echo form_input($bonmmasuk);
					?>
					</td>
				  </tr>
				  <tr>
					<td width="33%">Kode Barang</td>
					<td width="0%">:</td>
					<td>
					<?php
					$imotif	= array(
					'name'=>'imotif',
					'id'=>'imotif',
					'type'=>'text',
					'maxlength'=>'14',
					'style'=>'width:100px;',
					'value'=>$imotif_x
					);
					echo form_input($imotif);
					?>
					</td>
				  </tr>					  
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_inbonm; ?></div></td>	
			</tr>
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <?php if($template==1){ ?>
				<tr>
				  <td width="3%" class="tdatahead">NO</td>
				  <td width="15%" class="tdatahead"><?php echo $list_no_inbonm; ?></td>
				  <td width="15%" class="tdatahead"><?php echo $list_tgl_inbonm; ?> </td>
				  <td width="12%" class="tdatahead"><?php echo $list_kd_brg_inbonm; ?></td>
				  <td width="42%" class="tdatahead"><?php echo $list_nm_brg_inbonm; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_qty_brg_inbonm; ?> </td>
				  <td width="20%" class="tdatahead">Per Warna </td>
				  <td width="20%" class="tdatahead"><?php echo $link_aksi; ?> </td>
				</tr>
				<?php
				$no	= 1;	
				$cc	= 1;
				$nomororder	= array();
				$arr	= 0;
				
				$bln	= array(
						'01'=>'Januari', 
						'02'=>'Februari', 
						'03'=>'Maret', 
						'04'=>'April', 
						'05'=>'Mei', 
						'06'=>'Juni', 
						'07'=>'Juli', 
						'08'=>'Agustus', 
						'09'=>'September', 
						'10'=>'Oktober', 
						'11'=>'Nopember', 
						'12'=>'Desember' );
										
				if(count($query)>0) {
					$db2=$this->load->database('db_external', TRUE);
					foreach($query as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

						$jmlBonM	= $this->mclass->jmlBonM($row->inbonmcode);
						$njmlBonM	= $jmlBonM->num_rows();
												
						$nomororder[$arr]	= $row->inbonmcode;
						
						$link_act	= '';
						if(empty($imotif_x)){
							if($cc==1){
								$link_act	= "<td align=\"center\" rowspan='$njmlBonM'>
								
								<a href=".base_url()."index.php/listbonmmasuk/cform/edit/".$row->ibonm." title=\"Edit Bon M Masuk\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Bon M Masuk\"></a>&nbsp;<a href=\"javascript:void(0)\" onclick='show(\"listbonmmasuk/cform/undo/".$row->ibonm."\",\"#content\")' title=\"Cancel Bon M Masuk\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Bon M Masuk\"></a></td>";
							
							
							}elseif($cc>1 && $nomororder[$arr-1]!=$row->inbonmcode){
								$link_act	= "<td align=\"center\" rowspan='$njmlBonM'>
								<a href=".base_url()."index.php/listbonmmasuk/cform/edit/".$row->ibonm." title=\"Edit Bon M Masuk\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Bon M Masuk\"></a>&nbsp;<a href=\"javascript:void(0)\" onclick='show(\"listbonmmasuk/cform/undo/".$row->ibonm."\",\"#content\")' title=\"Cancel Bon M Masuk\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Bon M Masuk\"></a></td>";					
							}else{
								$link_act	= '';
							}
						}
												
						$tgl			= (!empty($row->dinbonm) || strlen($row->dinbonm)!=0)?@explode("-",$row->dinbonm,strlen($row->dinbonm)):"";
						$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
						$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
						
						//--------------08-01-2014 ----------------------------
						// ambil data qty warna dari tr_product_color dan tm_inbonm_item_color
						$sqlxx	= $db2->query(" SELECT a.i_color, b.e_color_name, c.qty FROM tr_product_color a, tr_color b,
												tm_inbonm_item_color c
												 WHERE a.i_color=b.i_color AND c.i_product_color = a.i_product_color
												 AND c.i_inbonm_item = '$row->i_inbonm_item' AND a.i_product_motif = '$row->iproduct' ");
						$listwarna = "";
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $rownya) {
								$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
							}
						}
						
						//-----------------------------------------------------
						
						$lbonmmasuk	.= "
							<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\" >
							  <td class=\"tdatahead2\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
						  	  <td class=\"tdatahead2\">".$row->inbonmcode."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$tanggal[$cc]."</td>
							  <td class=\"tdatahead2\">".$row->iproduct."</td>
							  <td class=\"tdatahead2\">".$row->productname."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->qty."</td>
							  <td class=\"tdatahead2\" style='white-space:nowrap;'>".$listwarna."</td>
							  $link_act
							</tr>";
							
							$no+=1;
							$cc+=1;
							$arr+=1;
					}
					echo $lbonmmasuk;
				}
				?>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<?php } else { ?>
				<tr>
				  <td width="3%" class="tdatahead">NO</td>
				  <td width="20%" class="tdatahead"><?php echo $list_no_inbonm; ?></td>
				   <td width="20%" class="tdatahead"><?php echo $list_no_insj; ?></td>
				  <td width="20%" class="tdatahead"><?php echo $list_tgl_inbonm; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_qty_brg_inbonm; ?> </td>
				  <td width="5%" class="tdatahead"><?php echo $link_aksi; ?> </td>
				</tr>	
				<?php
				$no	= 1;	
				$cc	= 1;
				$nomororder	= array();
				$arr	= 0;
				
				$bln	= array(
						'01'=>'Januari', 
						'02'=>'Februari', 
						'03'=>'Maret', 
						'04'=>'April', 
						'05'=>'Mei', 
						'06'=>'Juni', 
						'07'=>'Juli', 
						'08'=>'Agustus', 
						'09'=>'September', 
						'10'=>'Oktober', 
						'11'=>'Nopember', 
						'12'=>'Desember' );
										
				if(count($query) > 0 ) {
					foreach($query as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$link_act	= "<td align=\"center\">
						 <a href=".base_url()."index.php/listbonmmasuk/cform/edit/".$row->ibonm."
					
						 title=\"Edit Bon M Masuk\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Bon M Masuk\"></a>
						 &nbsp;
						 <a href=".base_url()."index.php/listbonmmasuk/cform/undo/".$row->ibonm." title=\"Cancel Bon M Masuk\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Bon M Masuk\"></a></td>";					
												
						$tgl			= (!empty($row->dinbonm) || strlen($row->dinbonm)!=0)?@explode("-",$row->dinbonm,strlen($row->dinbonm)):"";
						$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
						$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
												
						$lbonmmasuk	.= "
							<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\" >
							  <td class=\"tdatahead2\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
							   <td class=\"tdatahead2\">".$row->inbonmcode."</td>
						  	  <td class=\"tdatahead2\">".$row->i_sj_manual."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$tanggal[$cc]."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->qty."</td>
							  $link_act
							</tr>";
							
							$no+=1;
							$cc+=1;
							$arr+=1;
					}
					echo $lbonmmasuk;
				}
				?>					
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>				
				</tr>		
				<?php } ?>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>			
			<tr>
			  <td align="right">
				      <input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url() ?>index.php/listbonmmasuk/cform'"> 
	
			</tr>
		  </table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
