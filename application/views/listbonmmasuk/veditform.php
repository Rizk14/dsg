<script type="text/javascript" language="javascript">

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka.Terimakasih.");
		angka.value	= 0;
		angka.focus();
	}
}

function ckbonmmasuk(nomor){
	
	var i_inbonm_code_hidden = document.getElementById('i_inbonm_code_hidden').value;
	
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listbonmmasuk/cform/cari_bonmmasuk');?>",
	data:"nobonmmasuk="+nomor+"&i_inbonm_code_hidden="+i_inbonm_code_hidden,
	success: function(data){
		$("#confnobonmmasuk").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function showData(nItem) {
	if (document.getElementById(nItem+'_select').style.display=='none') {		
		document.getElementById(nItem+'_select').style.display='block';	
		document.getElementById(nItem+'_select').style.position='absolute';
	} else {
		document.getElementById(nItem).style.display='block';
		document.getElementById(nItem+'_select').style.display='none';
	}
}
  
function hideData(nItem){
}

function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	tbl.width='100%';
	
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	} else {
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:10px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:110px;\" >" +
	"<input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:80px;\" onclick=\"lshprodukbmm('"+iteration+"','<?=$tBonMmasukH?>');\">" +
	"<input type=\"hidden\" name=\"qty_product_"+nItem+"_"+iteration+"\" id=\"qty_product_"+nItem+"_"+iteration+"\"><img name=\"img_i_product_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"lshprodukbmm('"+iteration+"','<?=$tBonMmasukH?>');\"></DIV>" +
	"<div id=\"ajax_i_product_"+nItem+"_"+iteration+"_select\" style=\"display:none;\">" +
	"</div>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:281px;\"><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:276px;\"></DIV>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_n_count_product_"+nItem+"_"+iteration+"\" style=\"width:85px;\">" +
	//"<input type=\"text\" ID=\"n_count_product_"+nItem+"_"+iteration+"\"  name=\"n_count_product_"+nItem+"_"+iteration+"\" style=\"width:81px;text-align:right;\" onkeyup=\"validNum('n_count_product_tblItem','"+iteration+"')\">" +
	"<input type=\"hidden\" ID=\"f_stp_"+nItem+"_"+iteration+"\" name=\"f_stp_"+nItem+"_"+iteration+"\">" +
	"<input type=\"hidden\" ID=\"iso_"+nItem+"_"+iteration+"\" name=\"iso_"+nItem+"_"+iteration+"\">" +
	"<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\"></DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
	  <td class="tcat"><?php echo $page_title_bmm; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
	 <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_bmm; ?></td></tr>
	<tr>
	 <td class="alt2" style="padding:0px;">

		<table id="table-add-box">
		  <tr>
		    <td align="left">
			      <?php 
			      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listbonmmasuk/cform/actedit', $attributes);?>
				
			<div id="masterbmmform">
			      <table border="0" width="70%">
			      	<tr align="left">
				  <td width="50%" align="right"><?php echo $form_nomor_bmm; ?></td>
				  <td width="1%">:</td>
				  <td width="10%">
					<?php
					 $inbonm = array(
						'name'=>'i_inbonm',
						'id'=>'i_inbonm',
						'value'=>$ibonmmasukcode,
						'maxlength'=>'10',
						'onkeyup'=>'ckbonmmasuk(this.value)'
					 );
					 echo form_input($inbonm);
					?>
					<div id="confnobonmmasuk" style="color:#FF0000;"></div>
				  </td>
				  <td width="10%" align="right"><?php echo $form_tanggal_bmm; ?></td>
				  <td width="1%">:</td>
				  <td width="30%" align="right">
					<?php
					 $tgl = array(
						'name'=>'d_inbonm',
						'id'=>'d_inbonm',
						'value'=>$tBonMmasuk,
						'maxlength'=>'10');
					 echo form_input($tgl);
					?>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_inbonm,'dd/mm/yyyy',this)">
					<input type="hidden" name="d_inbonm_hidden" id="d_inbonm_hidden" value="<?php echo $tBonMmasukH; ?>">
				  </td>
				</tr>
				<tr>
				 <td width="10%" align="right"><?php echo $form_sj_bmm; ?></td>
				 <td width="1%" align="right">:</td>
				 <td colspan="4">
					<?php
					$isj = array(
						'name'=>'i_sj',
						'id'=>'i_sj',
						'maxlength'=>'100',
						'value'=>$isjmanual );
						
						echo form_input($isj);
					?>
				 </td>
				</tr>				
				<tr>
				 <td colspan="6">&nbsp;</td>	
				</tr>
				<tr>
				 <td colspan="6">
					<div id="title-box2"><?php echo $form_title_detail_bmm; ?>
					<div style="float:right;">
					<a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
					<a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
					</div></div>
				 </td>	
				</tr>
				<tr>
				 <td colspan="6">
					<table>
						<tr>
						 <td width="10px" class="tdatahead">NO</td>
						 <td width="100px" class="tdatahead"><?php echo $form_kode_produk_bmm; ?></td>
						 <td width="300px" class="tdatahead"><?php echo $form_nm_produk_bmm; ?></td>
						 <td width="100px" class="tdatahead"><?php echo $form_jml_produk_bmm; ?></td>
						</tr>
						<tr>
						 <td colspan="4">
						 	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							<?php
							$iter	= 0;
							$db2=$this->load->database('db_external', TRUE);
							foreach($bonmmasukitem as $row2) {
								
								$qstp	= $db2->query(" SELECT b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b ON b.i_so=a.i_so WHERE b.i_so='$row2->i_so' AND a.i_product='$row2->i_product' LIMIT 1 ");
								
								if($qstp->num_rows()>0){
									$rstp	= $qstp->row();
									$stp	= $rstp->stp;
								}else{
									$stp	= 'f';
								}
								
								/*
								<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:110px;\" ><input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:80px;\" onclick=\"shprodukbmm('".$iter."');\" value=\"".$row2->i_product."\"><input type=\"hidden\" name=\"qty_product_tblItem_".$iter."\" id=\"qty_product_tblItem_".$iter."\"><img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukbmm('".$iter."');\"></DIV><div id=\"ajax_i_product_tblItem_".$iter."_select\" style=\"display:none;\"></div></td>
								*/
								
								// <input type=\"text\" ID=\"n_count_product_tblItem_".$iter."\" name=\"n_count_product_tblItem_".$iter."\" style=\"width:81px;text-align:right;\" onkeyup=\"validNum('n_count_product_tblItem','".$iter."')\" value=\"".$row2->n_count_product."\">
								
								//--------------08-01-2014 ----------------------------
								// ambil data qty warna dari tr_product_color dan tm_inbonm_item_color
								$sqlxx	= $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name, c.qty FROM tr_product_color a, tr_color b,
														tm_inbonm_item_color c
														 WHERE a.i_color=b.i_color AND c.i_product_color = a.i_product_color
														 AND c.i_inbonm_item = '$row2->i_inbonm_item' AND a.i_product_motif = '$row2->i_product' ");
								
								$listwarna = "";
								if ($sqlxx->num_rows() > 0){
									$hasilxx = $sqlxx->result();
									
									foreach ($hasilxx as $rownya) {
										//$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
										$listwarna .= $rownya->e_color_name." 
										
										<input type='text' name='qty_warna_".$iter."[]' value='".$rownya->qty."'>
										<input type='hidden' name='i_color_".$iter."[]' value='".$rownya->i_color."'>
										<input type='hidden' name='i_product_color_".$iter."[]' value='".$rownya->i_product_color."'><br>";
									}
								}
						
								//-----------------------------------------------------
								
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">".($iter*1+1)."</div></td>
									<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:110px;\" >
									<input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:80px;\" value=\"".$row2->i_product."\" readonly >
									<input type=\"hidden\" name=\"qty_product_tblItem_".$iter."\" id=\"qty_product_tblItem_".$iter."\">
									<img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"lshprodukbmm('".$iter."','".$tBonMmasukH."');\">
									</DIV></td>
									<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:281px;\"><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\"  name=\"e_product_name_tblItem_".$iter."\" style=\"width:276px;\" value=\"".$row2->e_product_motifname."\" readonly ></DIV></td>
									<td style='white-space:nowrap;'><DIV ID=\"ajax_n_count_product_tblItem_".$iter."\" style=\"width:85px;\">
									".$listwarna."
									<input type=\"hidden\" ID=\"f_stp_tblItem_".$iter."\" name=\"f_stp_tblItem_".$iter."\" value=\"".$stp."\">
									<input type=\"hidden\" ID=\"iso_tblItem_".$iter."\" name=\"iso_tblItem_".$iter."\" value=\"".$row2->i_so."\">
									<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$iter."\"></DIV>";
									
									echo "</td>
								</tr>";
								$iter+=1;
							}
							?>
						 	</table>
						 </td>
						</tr>
					</table>
				 </td>
				</tr>
			      	<tr>
				  <td colspan="6" align="right">
				  	<input type="hidden" name="i_inbonm_hidden" id="i_inbonm_hidden" value="<?=$ibonmmasuk?>" />
					<input type="hidden" name="i_inbonm_code_hidden" id="i_inbonm_code_hidden" value="<?=$ibonmmasukcode?>" />
				    <input name="btnsimpan" id="btnsimpan" value="<?php echo $button_update; ?>" type="submit">
				    
				    <input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url() ?>index.php/listbonmmasuk/cform'"> 
				  </td>
			       </tr>
			      </table>
			</div>
			<?php echo form_close(); ?>
		    </td>
		  </tr>
		</table>

	 </td>
	</tr>
</table>
