<h3>Laporan Perubahan Harga Pembelian</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Periode: <?php echo $nama_bln_dari." ".$tahun_dari ?> s/d <?php echo $nama_bln_ke." ".$tahun_ke ?><br>
unit Jahit: <?php echo $nama_unit_jahit->kode_unit ." - ".$nama_unit_jahit->nama ?><br><br> 
<?php 
$attributes = array('name' => 'f_harga', 'id' => 'f_harga');
echo form_open('info-perubahan-harga/cform/submit', $attributes); ?>
<input type="hidden" name="no" id="no" value="<? echo count($query) ?>" >
<input type="hidden" name="bulan" id="bulan" value="<? echo $bulan ?>" >
<input type="hidden" name="tahun" id="tahun" value="<? echo $tahun ?>" >
<input type="hidden" name="is_save" id="is_save" value="<? echo $is_save ?>" >

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		<th>No</th>
		<th>unit_jahit</th>
		 <th>Kode & Nama Brg</th>
		 <th>Satuan</th>
		 <th>Harga (Rp.)</th>
		 <th nowrap>Tgl Input</th>
		<!-- <th>Keterangan</th> -->
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				//if ($startnya == '')
					$i=1;
				//else
				//	$i = $startnya+1; 
			 
			 for($j=0;$j<count($query);$j++){
				
								 
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$i."</td>";
				 echo    "<td>".$query[$j]['kode_unit_jahit']." - ".$query[$j]['nama_unit_jahit']."</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>";
				 echo    "<td>".$query[$j]['satuan']."</td>";
					echo    "<td align='right'>".number_format($query[$j]['harga'],2,',','.')."</td>";
			 echo    "<td>".$query[$j]['tgl_input']."</td>";
			
				 echo  "</tr>";
				
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php // echo $this->pagination->create_links();?>

<?php //if ($jum_total != 0) { ?>
<!--<input type="submit" value="Simpan keterangan" name="submit"> -->
<?php //} ?>

<?php echo form_close();  ?>
</div>
