<h3>Laporan Perubahan Harga Pembelian WIP Jahit</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
/*	var date_from= $('#date_from').val(); // contoh: 02-04-2011
	var date_to= $('#date_to').val();
	var blnfrom = date_from.substr(3,2);
	var blnto = date_to.substr(3,2);
	
	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	if (date_from > date_to) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}
	
	if (blnfrom != blnto) {
		alert("Bulannya harus sama..!");
		return false;
	} */
	
	var tahun = $('#tahun').val();
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
$attributes = array('name' => 'f_harga', 'id' => 'f_harga');
echo form_open('info-perubahan-harga-wip-jht/cform/view', $attributes); ?>
<table width="60%">
<!--	<tr>
		<td width="20%">Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
  </tr> -->
  
  <tr>
		<td width="20%">Periode Dari (bulan-tahun)</td>
		<td>: <select name="bulan_dari" id="bulan_dari">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun_dari" type="text" id="tahun_dari" size="4" value="" maxlength="4">

		</td>
		
  </tr>
   <tr>
		<td width="20%">Periode Ke (bulan-tahun)</td>
		<td>: <select name="bulan_ke" id="bulan_ke">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun_ke" type="text" id="tahun_ke" size="4" value="" maxlength="4">

		</td>
		
  </tr>
  <tr>
  <td>Nama unit_jahit </td>
		<td>:
		<select name='id_unit_jahit' id='id_unit_jahit'>
		<?php foreach ($list_unit_jahit as $lus) {?>
		<option value="<?php echo $lus->id?>" ><?php echo $lus->kode_unit ." - ".$lus->nama ?> </option>		
			<?php }?>
		</select>
		</td>
  </tr>
  

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/info-perubahan-harga-wip-jht/cform/'">
<?php echo form_close();  ?>
