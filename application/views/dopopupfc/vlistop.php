<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>

<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function settextfield(a,b,c) {
	opener.document.getElementById('i_op').value=a;
	opener.document.getElementById('i_op_code').value=b;	
 	opener.document.getElementById('i_product').value=c;
 	
	opener.document.getElementById('i_op_code').focus();
	
	this.close();
}

function checked1() {
	var x = document.getElementById('iteration');

	var banyak	= x.value;
	
	if(document.getElementById('ckall').checked==true) {
		for (i = 1; i <= parseInt(banyak); i++) {
			document.getElementById('f_ck_tblItem'+i).checked = true;
		}
	}else{
		for (i = 1; i <= parseInt(banyak); i++) {
			document.getElementById('f_ck_tblItem'+i).checked = false;
		}
	}
}

function parsi() {
	
	var x = document.getElementById('iteration');
	
	var banyak	= x.value;
	var iter;
	var iter2;
	var iter3;

	var iter_external ;
	var iter_external_trim;
	var i_faktur_external;
	var i_faktur_trim;
	
	var i_prod_ext;
	var i_prod_trim;

	opener.document.getElementById('i_op_code').value	= '';
	opener.document.getElementById('i_op').value		= '';
	opener.document.getElementById('i_product').value	= '';
	
	if(document.getElementById('sum').value<23) {
		
		for(i = 1; i <= banyak; i++) {
			
			if(document.getElementById('f_ck_tblItem'+i).checked==true) {
				
				iter_external		= opener.document.getElementById('i_op_code').value;
				iter_external_trim 	= iter_external.trim();
				i_faktur_external	= opener.document.getElementById('i_op').value;
				i_faktur_trim 		= i_faktur_external.trim();
				i_prod_ext		= opener.document.getElementById('i_product').value;
				i_prod_trim		= i_prod_ext.trim();

				iter = document.getElementById('f_ck_tblItem'+i).value;
				iter2 = document.getElementById('i_op'+i).value;
				iter3 = document.getElementById('i_product'+i).value;
		
				if(iter_external_trim=='') {
					opener.document.getElementById('i_op_code').value = iter.trim();
					opener.document.getElementById('i_op').value = iter2.trim();
					opener.document.getElementById('i_product').value = iter3.trim();
				}else{
					opener.document.getElementById('i_op_code').value = iter_external_trim+'#'+iter.trim();
					opener.document.getElementById('i_op').value = i_faktur_trim+'#'+iter2.trim();
					opener.document.getElementById('i_product').value = i_prod_trim+'#'+iter3.trim(); 
				}
				
			}
		}

		if(i==banyak)
			this.close();
				
	}else{
		alert('item Order tdk dpt lebih dari 22 item barang!');
	}

			
}

function sumcheck() {
	
	var x = document.getElementById('iteration');
	var banyak	= x.value;
	ii = 0;

	if(document.getElementById('f_ck_tblItem1').checked==false)
		document.getElementById('sum').value = 0;
	
	for(i=1; i <=parseInt(banyak); i++) {
		if(document.getElementById('f_ck_tblItem'+i).checked==true) {
			document.getElementById('sum').value = (ii+=1);
		}
	}

}

function klose() {
	this.close();
}

</script>

</head>
<body id="bodylist">
<div id="main">
<br />
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
	<td align="center">
	<input type="button" name="btnkirim" id="btnkirim" value="AMBIL DO" onclick="parsi();">&nbsp;
  	<input type="button" name="btnclose" id="btnclose" value="KELUAR" onclick="klose();"></td>
  </tr>

  <tr>
    <td align="left">
	<?php echo form_open('dopopupfc/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php
		if(!empty($isi) || isset($isi)) {
			
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			
			if(count($isi)) {
				
				$tanggal	= array();
				$cc	= 1;
				
				foreach($isi as $row) {
				
					$tgl			= (!empty($row->dop) || strlen($row->dop)!=0)?@explode("-",$row->dop,strlen($row->dop)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:""; // dd mm YYYY
					
					$iopcode	= trim($row->iopcode);
					$iproduct	= trim($row->iproduct);
					
					$tglfaktur	= $tgl[2]."/".$tgl[1]."/".$tgl[0];
					
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->iop','$iopcode','$iproduct')\">".$iopcode."</a></td> 
					  <td><a href=\"javascript:settextfield('$row->iop','$iopcode','$iproduct')\">".$tanggal[$cc]."</a></td>
					  <td><a href=\"javascript:settextfield('$row->iop','$iopcode','$iproduct')\">".$row->iproduct."</a></td>
					  <td><a href=\"javascript:settextfield('$row->iop','$iopcode','$iproduct')\">".$row->productname."</a></td>
					  <td><a href=\"javascript:settextfield('$row->iop','$iopcode','$iproduct')\">".$row->qtyakhir."</a></td>
					  <td>
					  <input type=\"hidden\" name=\"i_op".$cc."\" id=\"i_op".$cc."\" value=\"".$row->iop."\">
 					  <input type=\"hidden\" name=\"i_product".$cc."\" id=\"i_product".$cc."\" value=\"".$row->iproduct."\">
					  <input type=\"checkbox\" ID=\"f_ck_tblItem".$cc."\" name=\"f_ck_tblItem".$cc."\" value=\"".$iopcode."\" onclick=\"sumcheck();\" /></td>
					 </tr>";
					 
					 $cc+=1;
				}
				echo " <input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$cc."\"> ";
			}
		}
		?>
		
		<table class="listtable2" border="1">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="80px;"><?php echo strtoupper("Nomor OP"); ?></th>
		 <th width="95px;"><?php echo strtoupper("Tgl. OP"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Kode BRG"); ?></th>
		 <th width="200px;"><?php echo strtoupper("Nama Barang"); ?></th>
		 <th width="20px;"><?php echo strtoupper("Qty"); ?></th>
		 <th width="4px;"><input type="checkbox" name="ckall" id="ckall" onclick="checked1();"/></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		<input type="hidden" name="sum" id="sum" value="0" />
		</tbody>
		</table>

  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
