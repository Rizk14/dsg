<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div id="tabs">
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">FORM MEREK BRG</div>
</div>
         
<div class="tab_bdr"></div>

	<div class="panel" id="panel2" style="display:block;">
	    <table class="maintable">
	      <tr>
	    	<td align="left">
		      <?php
			$attributes = array('id'=>'f_master','class'=>'myform');
			echo form_open ('merekbrg/cform/actedit',$attributes);
		    ?>
		<div id="masterbrandform">
		<div class="effect">
		  <div class="accordion2">
		      <table class="mastertable">
		      	<tr>
			  <td width="150px">Kode Brand</td>
			  <td width="1px">:</td>
			  <td>
			    <?php
				$kdbrand = array(
				  'name'=>'ibrand',
				  'id'=>'ibrand',
				  'value'=>$c_brand_code,
				  'maxlength'=>'5' );
			    echo form_input($kdbrand); ?>		
			  </td>
			</tr>
		      	<tr>
			  <td>Nama Brand</td>
			  <td>:</td>
			  <td>
			    <?php
				$nmbrand = array(
				  'name'=>'ebrandname',
				  'id'=>'ebrandname',
				  'value'=>$e_brand_name,
				  'maxlength'=>'50' );
			    echo form_input($nmbrand); ?>		
			 </td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			  	<input type="hidden" name="i_brand" value="<?php echo $i_brand; ?>" id="i_brand" />
			    <input name="btnsimpan" id="btnsimpan" value="Simpan" type="submit">
			    <input name="btnbatal" id="btnbatal" value="Batal" type="button" onclick="window.location='<?php echo base_url() ?>index.php/merekbrg/cform/'">
			   
			  </td>
		       </tr>
		      </table>
			</div>
		</div>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
