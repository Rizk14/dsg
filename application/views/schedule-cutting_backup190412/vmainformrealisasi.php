<h3>Schedule Cutting</h3><br>
<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform/view">View Data</a><br><br>

<?php if ($msg!='') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">
function pemenuhan(iteration) {
	lebar =600;
	tinggi=500;
	eval('window.open("<?php echo site_url(); ?>"+"/schedule-cutting/cform/show_popup_brg_jadi/"+iteration,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}

function ckitem() {
	var tglschedule = $("#tglschedule").val();
	var noschedule = $("#noschedule").val();
	
	if (noschedule == '') {
		alert ("No Schedule harus diisi");
		$("#noschedule").focus();
		return false;
	}
	if (tglschedule == '') {
		alert ("Tgl Schedule harus dipilih");
		$("#tglschedule").focus();
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#imotif_'+k).val() == '') {
				alert("Barang jadi tidak boleh ada yang kosong...!");
				return false;
			}
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data plan qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Plan qty harus berupa angka atau desimal..!");
				return false;
			}
			
			if ($('#jammulai_'+k).val() == '') {
				alert("Jam mulai tidak boleh kosong...!");
				return false;
			}
			if ($('#jamselesai_'+k).val() == '') {
				alert("Jam selesai tidak boleh kosong...!");
				return false;
			}
			if ($('#operator_'+k).val() == '') {
				alert("Data operator tidak boleh kosong...!");
				return false;
			}
									
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}
</script>

<script language="javascript">
//tambah
$(function()
{
	$('#topnya').hide();
	
	// 27-12-2011
	$('#is_dacron').click(function(){
	  	    if ($('input[name=is_dacron]').is(':checked')) {
				$('input[name=is_realisasi]').attr('checked', false);
				$('#no_item').val('');
				$('#id_apply_stok_item').val('');
				$('#id').val('');
				$('#no_pb_cutting').val('');
				$("#tombol").attr("disabled", true);
				$('#item_brg').val('');
				$('#id_realisasi_detail').val('');
				$("#pilih_realisasi").attr("disabled", true);
			}
			else {
				//$('input[name=is_realisasi]').attr('checked', true);
				$('#no_item').val('');
				$('#id_apply_stok_item').val('');
				$('#id').val('');
				$('#no_pb_cutting').val('');
				$("#tombol").attr("disabled", false);
				$('#item_brg').val('');
				$('#id_realisasi_detail').val('');
				$("#pilih_realisasi").attr("disabled", true);
			}
	  });
	  
	  // 18-01-2012
	  $('#is_realisasi').click(function(){ //
	  	    if ($('input[name=is_realisasi]').is(':checked')) {
				$('input[name=is_dacron]').attr('checked', false);
				$('#no_item').val('');
				$('#id_apply_stok_item').val('');
				$('#id').val('');
				$('#no_pb_cutting').val('');
				$("#tombol").attr("disabled", true);
				$('#item_brg').val('');
				$('#id_realisasi_detail').val('');
				$("#pilih_realisasi").attr("disabled", false);
			}
			else {
				//$('input[name=is_dacron]').attr('checked', true);
				$('#no_item').val('');
				$('#id_apply_stok_item').val('');
				$('#id').val('');
				$('#no_pb_cutting').val('');
				$("#tombol").attr("disabled", false);
				$('#item_brg').val('');
				$('#id_realisasi_detail').val('');
				$("#pilih_realisasi").attr("disabled", true);
			}
	  });
	
	$("#additemsc").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****diprint*************************************
		var diprint="#diprint_"+n;
		var new_diprint="#diprint_"+no;
		$(diprint, lastRow).attr("id", "diprint_"+no);
		$(new_diprint, lastRow).attr("name", "diprint_"+no);	
				
		//*****kode_brg_jadi*************************************
		var kode_brg_jadi="#kode_brg_jadi_"+n;
		var new_kode_brg_jadi="#kode_brg_jadi_"+no;
		$(kode_brg_jadi, lastRow).attr("id", "kode_brg_jadi_"+no);
		$(new_kode_brg_jadi, lastRow).attr("name", "kode_brg_jadi_"+no);		
		$(new_kode_brg_jadi, lastRow).val('');
		//*****end kode_brg_jadi*********************************
		
		//*****brg_jadi*************************************
		var brg_jadi="#brg_jadi_"+n;
		var new_brg_jadi="#brg_jadi_"+no;
		$(brg_jadi, lastRow).attr("id", "brg_jadi_"+no);
		$(new_brg_jadi, lastRow).attr("name", "brg_jadi_"+no);		
		$(new_brg_jadi, lastRow).val('');
		//*****end brg_jadi*********************************
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');
		//*****end qty*********************************
		
		//*****plan_qty*************************************
		var plan_qty="#plan_qty_"+n;
		var new_plan_qty="#plan_qty_"+no;
		$(plan_qty, lastRow).attr("id", "plan_qty_"+no);
		$(new_plan_qty, lastRow).attr("name", "plan_qty_"+no);		
		$(new_plan_qty, lastRow).val('0');
		//*****end plan_qty*********************************
		
		//*****nmbrg*************************************
		var nmbrg="#nmbrg_"+n;
		var new_nmbrg="#nmbrg_"+no;
		$(nmbrg, lastRow).attr("id", "nmbrg_"+no);
		$(new_nmbrg, lastRow).attr("name", "nmbrg_"+no);		
		$(new_nmbrg, lastRow).val('');
		//*****end nmbrg*********************************
				
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');
		//*****end kode*************************************	
		
		//*****nmbrgquilting*************************************
		var nmbrgquilting="#nmbrgquilting_"+n;
		var new_nmbrgquilting="#nmbrgquilting_"+no;
		$(nmbrgquilting, lastRow).attr("id", "nmbrgquilting_"+no);
		$(new_nmbrgquilting, lastRow).attr("name", "nmbrgquilting_"+no);		
		$(new_nmbrgquilting, lastRow).val('');
		//*****end nmbrgquilting*********************************
				
		//*****kode_brg_quilting*************************************
		var kode_brg_quilting="#kode_brg_quilting_"+n;
		var new_kode_brg_quilting="#kode_brg_quilting_"+no;
		$(kode_brg_quilting, lastRow).attr("id", "kode_brg_quilting_"+no);
		$(new_kode_brg_quilting, lastRow).attr("name", "kode_brg_quilting_"+no);		
		$(new_kode_brg_quilting, lastRow).val('');
		//*****end kode_brg_quilting*************************************
		
		//*****jammulai*************************************
		var jammulai="#jammulai_"+n;
		var new_jammulai="#jammulai_"+no;
		$(jammulai, lastRow).attr("id", "jammulai_"+no);
		$(new_jammulai, lastRow).attr("name", "jammulai_"+no);		
		$(new_jammulai, lastRow).val('00:00');
		//*****end jammulai*************************************					
		
		//*****jamselesai*************************************
		var jamselesai="#jamselesai_"+n;
		var new_jamselesai="#jamselesai_"+no;
		$(jamselesai, lastRow).attr("id", "jamselesai_"+no);
		$(new_jamselesai, lastRow).attr("name", "jamselesai_"+no);		
		$(new_jamselesai, lastRow).val('00:00');
		//*****end jamselesai*************************************					
		
		//*****keterangan*************************************
		var keterangan="#keterangan_"+n;
		var new_keterangan="#keterangan_"+no;
		$(keterangan, lastRow).attr("id", "keterangan_"+no);
		$(new_keterangan, lastRow).attr("name", "keterangan_"+no);		
		$(new_keterangan, lastRow).val('');
		//*****end keterangan*************************************
		
		//*****operator*************************************
		var operator="#operator_"+n;
		var new_operator="#operator_"+no;
		$(operator, lastRow).attr("id", "operator_"+no);
		$(new_operator, lastRow).attr("name", "operator_"+no);		
		$(new_operator, lastRow).val('');
		//*****end operator*************************************
		
		//*****id_detail*************************************
		var id_detail="#id_detail_"+n;
		var new_id_detail="#id_detail_"+no;
		$(id_detail, lastRow).attr("id", "id_detail_"+no);
		$(new_id_detail, lastRow).attr("name", "id_detail_"+no);		
		$(new_id_detail, lastRow).val('0');
		//*****end id_detail*************************************
		
		//*****idapplystok_detail*************************************
		var idapplystok_detail="#idapplystok_detail_"+n;
		var new_idapplystok_detail="#idapplystok_detail_"+no;
		$(idapplystok_detail, lastRow).attr("id", "idapplystok_detail_"+no);
		$(new_idapplystok_detail, lastRow).attr("name", "idapplystok_detail_"+no);		
		$(new_idapplystok_detail, lastRow).val('0');
		//*****end idapplystok_detail*************************************
		
		//*****idapplystok_detail*************************************
		var idapplystok_detail="#idapplystok_detail_"+n;
		var new_idapplystok_detail="#idapplystok_detail_"+no;
		$(idapplystok_detail, lastRow).attr("id", "idapplystok_detail_"+no);
		$(new_idapplystok_detail, lastRow).attr("name", "idapplystok_detail_"+no);		
		$(new_idapplystok_detail, lastRow).val('0');
		//*****end idapplystok_detail*************************************
		
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg2/"+no+"')";
		 $(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("disabled",false);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		  
		 //end button pilih		
		 
		 //button pilih_brg_jadi*****************************************
		 var pilih_brg_jadi="#pilih_brg_jadi_"+n;
		 var new_pilih_brg_jadi="#pilih_brg_jadi_"+no;
		 $(pilih_brg_jadi, lastRow).attr("id","pilih_brg_jadi_"+no);
		
		 var  even_klik2= "openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg_jadi/"+no+"')";
		 $(new_pilih_brg_jadi, lastRow).attr("name", "pilih_brg_jadi_"+no);		
		 $(new_pilih_brg_jadi, lastRow).attr("disabled",false);
		 $(new_pilih_brg_jadi, lastRow).attr("onclick",even_klik2);
		 //end button pilih_brg_jadi
		
		//----------------END SETTING KONDISI ROW BARU*		
		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();
		var jumawal= $("#jumawal").val();	
		//if (x>2) {
		if (parseInt(x) > parseInt(jumawal)) {
			$("#tabelku tr:last").remove();
			var x= $("#no").val();
			var y= x-1;
			$("#no").val(y);
		}
	});
	
	// Dipake
	$('#tombol').click(function(){
		var urlnya = "<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_pemenuhan/";
		openCenteredWindow(urlnya);
	  });
	  
	$('#pilih_realisasi').click(function(){
		var urlnya = "<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_realisasi/";
		openCenteredWindow(urlnya);
	  });
	 // End 0f Dipake

	$('#tombol2').click(function(){
		var posisi	= $("#posisi").val();	
		var urlnya	= "<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg/"+posisi+"/";
		openCenteredWindow(urlnya);
	  });	  
	
});

function cek_item_brg() {
	var no_item= $('#no_item').val();
	//var is_dacron= $('#is_dacron').val();

		if (no_item == '' && $('input[name=is_dacron]').is(':checked')== false) {
			alert("List bahan dari bon M keluar harus dipilih..!");
			return false;
		}	
		
		if (item_brg == '' && $('input[name=is_realisasi]').is(':checked')== true) {
			alert("List item brg dari realisasi cutting harus dipilih..!");
			return false;
		}	
}
</script>

<script type="text/javascript">
	// Dipake 
	function openCenteredWindow(url) {
		var width = 850;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	// End 0f Dipake
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/schedule-cutting/cform/submit/" method="post" enctype="multipart/form-data">
<input type="hidden" name="is_realisasi" id="is_realisasi" value="<?php echo $is_realisasi ?>">

<?php 
if (count($realisasi_detail)>0) {
	$jumawal = count($realisasi_detail)+1;
	$no=1;
	foreach ($realisasi_detail as $hitung) {
		$no++;
	}
}
else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
<input type="hidden" name="iddata" id="iddata"/>
Schedule cutting dari acuan realisasi yg belum beres
<div align="center">

<label id="status"></label>
<table class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<!-- <tr>
		<td width="20%">No. Permintaan Cutting</td>
		<td>
		  <?php //echo $no_pb_cutting; ?>
    	</td>
  	</tr> -->
	<tr>
		<td width="20%">Nomor Schedule</td>
		<td>
		  <input type="text" name="noschedule" id="noschedule" maxlength="10" size="10" value="<?=$noschedule?>" readonly="true" />
    	</td>
  	</tr>
	<tr>
		<td>Tgl Schedule</td>
		<td>
		  <!-- <input type="hidden" name="no_pb_cutting" id="no_pb_cutting" value="<?=$no_pb_cutting?>" /> -->
		  <input type="text" name="tglschedule" id="tglschedule" maxlength="10" size="10" readonly="true" />
		  <img alt="" id="tgl_sc" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tglschedule,'dd-mm-yyyy',this)">
		</td>
  	</tr>
  	<tr>
		<td>Untuk Eksternal</td>
		<td>
		  <input type="checkbox" name="eksternal" id="eksternal" value="t" > *<i>Ini diceklis jika schedulenya untuk perusahaan luar</i>
    	</td>
  	</tr>
  	
  	<tr>
		<td colspan="2"><br>
		<form name="myform">
		<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
	<!--	<tr>
			<td colspan="10" align="left">
			<input id="additemsc" type="button" name="additemsc" value=" + " title="Tambah Item">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item">
			</td>
		</tr> -->
			<tr>
			  <th width="20">No</th>
			  <th>Nama Barang Jadi</th>
			  <th>Plan Qty di Schedule (Set)</th>
			  <th>Qty Yg Sdh Dipotong (Set)</th>
			  <th>Qty Sisa (Set)</th>
			  <th>Utk Diprint</th>
			  <th>Bhn Baku</th>
			  <th>Bhn Quilting</th>
			  <th>Jam Mulai</th>
			  <th>Jam Selesai</th>
			  <th>Keterangan</th>
			  <th>Operator</th>
			</tr>
			<?php
			//$i=0;
			$i = 1;
			for($j=0;$j<count($realisasi_detail);$j++){
			?>		
			<tr align="center">
			  <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
			  <td nowrap="nowrap">
			   <input name="idbarang_1" type="hidden" id="idbarang_1" />
			   <input readonly="true" name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="40" value="<?php echo $realisasi_detail[$j]['kode_brg_jadi']." - ".$realisasi_detail[$j]['nama_brg_jadi'] ?>" />
			   <input name="kode_brg_jadi_<?php echo $i ?>" type="hidden" id="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $realisasi_detail[$j]['kode_brg_jadi'] ?>" />
			   <input type="hidden" name="posisi" id="posisi" value="<?=$i?>" />
			   
			   <input title="browse data brg jadi" name="pilih_brg_jadi_<?php echo $i ?>" value="..." id="pilih_brg_jadi_<?php echo $i ?>" 
			   onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg_jadi/<?php echo $i ?>/);" type="button" disabled>
			   
			   </td>
			   <td><input name="qty_schedule_<?php echo $i ?>" type="text" id="qty_schedule_<?php echo $i ?>" size="7" value="<?php echo $realisasi_detail[$j]['qty_schedule'] ?>" readonly="true" />
			   </td>
			   <td><input name="qty_potong_<?php echo $i ?>" type="text" id="qty_potong_<?php echo $i ?>" size="7" value="<?php echo $realisasi_detail[$j]['hasil_potong'] ?>" readonly="true" />
			   </td>
			   <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="7" value="<?php echo $realisasi_detail[$j]['selisihnya'] ?>" />
			   </td>
			   <td>
				   <select name="diprint_<?php echo $i ?>" id="diprint_<?php echo $i ?>">
					<option value="t" <?php if ($realisasi_detail[$j]['diprint'] == 't') { ?> selected <?php } ?> >Ya</option>
					<option value="f" <?php if ($realisasi_detail[$j]['diprint'] != 't') { ?> selected <?php } ?> >Tidak</option>
				</select>
				</td>
			   <td nowrap>
			   <input name="nmbrg_<?php echo $i ?>" type="text" id="nmbrg_<?php echo $i ?>" size="20" value="<?php echo $realisasi_detail[$j]['nmbrg'] ?>" readonly="true" />
			   <input name="kode_<?php echo $i ?>" type="hidden" id="kode_<?php echo $i ?>" value="<?php echo $realisasi_detail[$j]['kode_brg'] ?>" />
			   <input title="browse data bhn baku" name="pilih_<?php echo $i ?>" value="..." id="pilih_<?php echo $i ?>" 
			   onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg2/<?php echo $i ?>/);" type="button" disabled>
			  </td>
			  
			  <td>
			   <input name="nmbrgquilting_<?php echo $i ?>" type="text" id="nmbrgquilting_<?php echo $i ?>" size="20" value="<?php echo $realisasi_detail[$j]['nmbrgquilting'] ?>" readonly="true" />
			   <input name="kode_brg_quilting_<?php echo $i ?>" type="hidden" id="kode_brg_quilting_<?php echo $i ?>" value="<?php echo $realisasi_detail[$j]['kode_brg_quilting'] ?>" />
			  </td>
			  
			  <td><input name="jammulai_<?php echo $i ?>" type="text" id="jammulai_<?php echo $i ?>" size="10" value="00:00"/></td>
			  <td><input name="jamselesai_<?php echo $i ?>" type="text" id="jamselesai_<?php echo $i ?>" size="10" value="00:00"/></td>
			  <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="25" /></td>
			  <td><input name="operator_<?php echo $i ?>" type="text" id="operator_<?php echo $i ?>" size="10" />
			  <input type="hidden" name="id_realisasi_detail_<?php echo $i ?>" id="id_realisasi_detail_<?php echo $i ?>" value="<?php echo $realisasi_detail[$j]['id_realisasi_detail'] ?>" />
			  <input type="hidden" name="id_realisasi_cutting_<?php echo $i ?>" id="id_realisasi_cutting_<?php echo $i ?>" value="<?php echo $realisasi_detail[$j]['id_realisasi_cutting'] ?>" />

			  </td>
			</tr>
			<?php $i++; } // end foreach 
			?>
		</table>	
		  </form>
		  <div align="center"><br>        
		  <input type="submit" name="submit" value="Simpan" onclick="return ckitem();" >    
		   &nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/schedule-cutting/cform/'">
		  </div>
		  </td>
	</tr>
</table>
</div>
</form>
