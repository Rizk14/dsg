<h3>Schedule Cutting</h3><br>
<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tglschedule = $("#tglschedule").val();
	var noschedule = $("#noschedule").val();
	var bhn_baku = $("#bhn_baku").val();
	
	if (noschedule == '') {
		alert ("Nomor Schedule harus diisi");
		$("#noschedule").focus();
		return false;
	}
	if (tglschedule == '') {
		alert ("Tgl Schedule harus dipilih");
		$("#tglschedule").focus();
		return false;
	}
	if (bhn_baku == '') {
		alert ("Kode bhn dacron harus dipilih");
		$("#bhn_baku").focus();
		return false;
	}
		
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if ($('#brg_jadi_'+k).val() == '') {
				alert("Barang Jadi tidak boleh kosong...!");
				return false;
			}
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data plan qty cutting tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Data plan qty cutting harus berupa angka atau desimal..!");
				return false;
			}
						
			if ($('#uk_pola_'+k).val() == '') {
				alert("Ukuran pola tidak boleh kosong...!");
				return false;
			}
			
			if ($('#jammulai_'+k).val() == '') {
				alert("Jam mulai tidak boleh kosong...!");
				return false;
			}
			if ($('#jamselesai_'+k).val() == '') {
				alert("Jam selesai tidak boleh kosong...!");
				return false;
			}
		/*	if ($('#operator_'+k).val() == '') {
				alert("Data operator tidak boleh kosong...!");
				return false;
			} */
						
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

//tambah
$(function()
{
	//var goedit = $("#goedit").val();
	//if(goedit!='1')
		//$("#no").val('2');
			
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/schedule-cutting/cform/updatedata" method="post" enctype="multipart/form-data">
<input type="hidden" name="is_dacron" id="is_dacron" value="t">
<input type="hidden" name="id_schedule" id="id_schedule" value="<?php echo $query[0]['id'] ?>">

<?php 
$detail_fb = $query[0]['detail_fb'];
if (count($detail_fb)>0) {
	$no=1;
	foreach ($detail_fb as $hitung) {
		$no++;
	}
}
else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>" >

<div align="center">
Edit Data
<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
		<td width="20%">Schedule Cutting Untuk Dacron
		</td>
  </tr>
  <tr>
		<td width="20%">Nomor Schedule</td>
		<td>
		  <input type="text" name="noschedule" id="noschedule" maxlength="10" size="10" value="<? echo $query[0]['no_schedule'] ?>" />
    	</td>
  	</tr>
	<tr>
		<td>Tgl Schedule</td>
		<td>
		  <!-- <input type="hidden" name="no_pb_cutting" id="no_pb_cutting" value="<?=$no_pb_cutting?>" /> -->
		  <input type="text" name="tglschedule" id="tglschedule" maxlength="10" size="10" value="<?php echo $query[0]['tgl_cutting'] ?>" readonly="true" />
		  <img alt="" id="tgl_sc" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tglschedule,'dd-mm-yyyy',this)">
		</td>
  	</tr>
  	<tr>
		<td>Nama Bahan Dacron</td>
		<td>
		  <input type="text" name="bhn_baku" id="bhn_baku" size="40" value="<?php echo $query[0]['bhn_baku'] ?>" readonly="true" />
		  <input name="kode_bhn_baku" type="hidden" id="kode_bhn_baku" value="<?php echo $query[0]['kode_bhn_baku'] ?>">  
           <input title="browse data bhn baku" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: 
           openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg/');">
    	</td>
  	</tr>
  	
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
	<!--	<tr>
			<td colspan="10" align="left"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr> -->
        <tr>
          <th width="20">No</th>
			  <th>Nama Barang Jadi</th>
			  <th>Plan Qty (Set)</th>
			 <!-- <th>Bhn Baku/Pembantu</th> -->
			  <!--<th>Qty Yg Diklrkan</th> -->
			  <th>Nama Bagian</th>
			  <th>Ukuran Pola</th>
			  <th>Jam Mulai</th>
			  <th>Jam Selesai</th>
			  <th>Keterangan</th>
			<!--  <th>Operator</th> -->
        </tr>
        
        <?php
        $i = 1;
		$detail_fb = $query[0]['detail_fb'];
		for($k=0;$k<count($detail_fb);$k++) {
        ?>
        
         <tr align="center">
          <td align="center" id="num_1"><?php echo $i ?>
          <input type="hidden" name="id_detail_<?php echo $i ?>" id="id_detail_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['idx'] ?>" />
          </td>
          
          <td nowrap="nowrap">
           <input name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $detail_fb[$k]['kode_brg_jadi']." - ".$detail_fb[$k]['nmmotif'] ?>" />
           <input name="kode_brg_jadi_<?php echo $i ?>" type="hidden" id="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['kode_brg_jadi'] ?>">  
           <input title="browse data brg jadi" name="pilih_brg_jadi_<?php echo $i ?>" value="..." type="button" id="pilih_brg_jadi_<?php echo $i ?>" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg_jadi/<?php echo $i ?>/');"></td>
           <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="7" value="<?php echo $detail_fb[$k]['qty_bhn'] ?>" /></td>
         
         <td><select name="bagian_brg_jadi_<?php echo $i ?>" id="bagian_brg_jadi_<?php echo $i ?>" >
				<?php foreach ($bagian_brg_jadi as $bag) { ?>
					<option value="<?php echo $bag->id ?>" <?php if ($detail_fb[$k]['id_bagian_brg_jadi'] == $bag->id) { ?> selected <?php } ?> ><?php echo $bag->nama_bagian ?></option>
				<?php } ?>
				</select>
          </td>
          
          <td><input name="uk_pola_<?php echo $i ?>" type="text" id="uk_pola_<?php echo $i ?>" size="10" value="<?php echo $detail_fb[$k]['ukuran_pola'] ?>" /></td>
         <td><input name="jammulai_<?php echo $i ?>" type="text" id="jammulai_<?php echo $i ?>" size="7" value="<?php echo $detail_fb[$k]['jam_mulai'] ?>"/></td>
		 <td><input name="jamselesai_<?php echo $i ?>" type="text" id="jamselesai_<?php echo $i ?>" size="7" value="<?php echo $detail_fb[$k]['jam_selesai'] ?>"/></td>
         <td><input name="ket_<?php echo $i ?>" type="text" id="ket_<?php echo $i ?>" size="20" value="<?php echo $detail_fb[$k]['keterangan'] ?>" /></td>
         <!--<td><input name="operator_<?php echo $i ?>" type="text" id="operator_<?php echo $i ?>" size="10" value="<?php echo $detail_fb[$k]['operator_cutting'] ?>" /></td> -->
        </tr>
        <?php 
        $i++; } ?>
	</table>	
	</form>
      <div align="center"><br><br> 
        <input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/schedule-cutting/cform/view'">
      </div></td>
    </tr>

</table>
</div>
</form>
