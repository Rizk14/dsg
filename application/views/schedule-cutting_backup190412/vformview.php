<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}

</style>

<h3>Schedule Cutting</h3><br> 
<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform/">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('schedule-cutting/cform/cari/'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		<th height="28px" width="85px">No Schedule</th>
		<th width="60px">Jenis</th>
		<th width="90px">Tgl Schedule</th>
		<th width="200px">Brg Jadi</th>
		<th width="40px">Plan Qty (Set)</th>
		<th width="180px">Bhn Baku/Pembantu</th>
		<th width="85px">Nm Bagian</th>
		<th width="85px">Ukuran Pola</th>
		<th width="120px">Waktu</th>
		<th width="85px">Operator</th>
		<th width="80px">Status Realisasi</th>
		<th width="60px">Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				
			 $realisasi = 0;
			  	
			 for($j=0;$j<count($query);$j++){
				
				if(!empty($query[$j]['tgl_cutting'])){ 
				
					$pisah1 = explode("-", $query[$j]['tgl_cutting']);
					$tgl1	= substr($pisah1[2],0,1)=='0'?substr($pisah1[2],1,1):$pisah1[2];
					
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					if ($bln1=='01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					$tgl_cutting = $tgl1." ".$nama_bln." ".$thn1;
				 }else{
				 	$tgl_cutting = "";
				 }
				  
				 echo "<tr class=\"record\">";
				 echo "<td align=\"center\">".$query[$j]['no_schedule']."</td>";
				 if ($query[$j]['is_dacron'] == 't')
					echo "<td>Dacron</td>";
				 else
					echo "<td>Non-Dacron</td>";
				 
				 echo "<td>".$query[$j]['nama_hari'].", ". $tgl_cutting."</td>";
				// echo "<td nowrap colspan=\"5\">";
				
				echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nm_brg_jadi'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align=right>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['qty_bhn'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				// echo "<table width=\"100%\">";	
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['kode_brg_quilting'] == '')
							echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						else
							echo $var_detail[$k]['kode_brg_quilting']." - ".$var_detail[$k]['nama_brg_quilting'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['nama_bagian_brg_jadi'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['ukuran_pola'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['jam_mulai']." - ".$var_detail[$k]['jam_selesai'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['operator_cutting'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['status_realisasi'] == 'f')
							echo "Belum";
						else
							echo "OK";
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				/* if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  //echo "(".$var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'].")"."  (".$var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nm_brg_jadi'].")"." ( ".$var_detail[$k]['jam_mulai']." s.d ".$var_detail[$k]['jam_selesai'].") (".$var_detail[$k]['qty_bhn'].") (".$var_detail[$k]['operator_cutting'].")";
						  //if ($k<$hitung-1)
						     //echo "<br>";
							 
						  if($var_detail[$k]['status_realisasi']=='t')
						  	$realisasi	= $realisasi+1;
							
					echo "<tr>";
					if ($var_detail[$k]['kode_brg_quilting'] == '')
						echo "<td width=\"100px\">".$var_detail[$k]['kode_brg']."</td>";
					else
						echo "<td width=\"100px\">".$var_detail[$k]['kode_brg_quilting']."</td>";
						
					echo "<td width=\"240px\">".$var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nm_brg_jadi']."</td>
						<td width=\"90px\">".$var_detail[$k]['jam_mulai']."s.d".$var_detail[$k]['jam_selesai']."</td>
						<td width=\"40px\" align=\"right\">".$var_detail[$k]['qty_bhn']."&nbsp;</td>
						<td width=\"90px\">".$var_detail[$k]['operator_cutting']."</td>
						</tr>";
					}
				 } */
				 //echo "</table>";
				 
				 //echo "</td>";

				/* echo "<td>";
				 if($realisasi==$k && $query[$j]['status_realisasi']=='t'){
				 	echo "Sudah";
				 }elseif($realisasi<$k && $query[$j]['status_realisasi']=='t'){
				 	echo "Proses";
				 }else{
				 	echo "Belum";
				 }
				 echo "</td>";	*/
				 
					echo    "<td align=center>";
					if($query[$j]['status_realisasi']=='f'){
						echo "<a href=".base_url()."index.php/schedule-cutting/cform/edit/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a> &nbsp;";
						echo "<a href=".base_url()."index.php/schedule-cutting/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					}else{
						echo "&nbsp;";
					}
					echo "</td>";
				 
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
