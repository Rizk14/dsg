<h3>Schedule Cutting</h3><br>
<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/schedule-cutting/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tglschedule = $("#tglschedule").val();
	var noschedule = $("#noschedule").val();
	var bhn_baku = $("#bhn_baku").val();
	
	if (noschedule == '') {
		alert ("Nomor Schedule harus diisi");
		$("#noschedule").focus();
		return false;
	}
	if (tglschedule == '') {
		alert ("Tgl Schedule harus dipilih");
		$("#tglschedule").focus();
		return false;
	}
	if (bhn_baku == '') {
		alert ("Kode bhn dacron harus dipilih");
		$("#bhn_baku").focus();
		return false;
	}
		
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if ($('#brg_jadi_'+k).val() == '') {
				alert("Barang Jadi tidak boleh kosong...!");
				return false;
			}
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data plan qty cutting tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Data plan qty cutting harus berupa angka atau desimal..!");
				return false;
			}
			
			/*if ($('#bhn_baku_'+k).val() == '') {
				alert("Bahan Dacron tidak boleh kosong...!");
				return false;
			} */
			
		/*	if($('#qty_bhn_'+k).val() == '0' || $('#qty_bhn_'+k).val() == '' ) {				
				alert("Data qty yg dikeluarkan tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_bhn_'+k).val()) ) {
				alert("Data qty yg dikeluarkan harus berupa angka atau desimal..!");
				return false;
			} */
			
			if ($('#uk_pola_'+k).val() == '') {
				alert("Ukuran pola tidak boleh kosong...!");
				return false;
			}
			
			if ($('#jammulai_'+k).val() == '') {
				alert("Jam mulai tidak boleh kosong...!");
				return false;
			}
			if ($('#jamselesai_'+k).val() == '') {
				alert("Jam selesai tidak boleh kosong...!");
				return false;
			}
			/*if ($('#operator_'+k).val() == '') {
				alert("Data operator tidak boleh kosong...!");
				return false;
			} */
						
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

//tambah
$(function()
{
	//var goedit = $("#goedit").val();
	//if(goedit!='1')
		$("#no").val('2');
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
				
		//*****bhn_baku*************************************
	/*	var bhn_baku="#bhn_baku_"+n;
		var new_bhn_baku="#bhn_baku_"+no;
		$(bhn_baku, lastRow).attr("id", "bhn_baku_"+no);
		$(new_bhn_baku, lastRow).attr("name", "bhn_baku_"+no);		
		$(new_bhn_baku, lastRow).val('');		
		//*****end bhn_baku*********************************
		
		//*****kode_bhn_baku*************************************
		var kode_bhn_baku="#kode_bhn_baku_"+n;
		var new_kode_bhn_baku="#kode_bhn_baku_"+no;
		$(kode_bhn_baku, lastRow).attr("id", "kode_bhn_baku_"+no);
		$(new_kode_bhn_baku, lastRow).attr("name", "kode_bhn_baku_"+no);		
		$(new_kode_bhn_baku, lastRow).val('');		*/
		//*****end kode_bhn_baku*********************************
		
		//*****qty_bhn*************************************
		var qty_bhn="#qty_bhn_"+n;
		var new_qty_bhn="#qty_bhn_"+no;
		$(qty_bhn, lastRow).attr("id", "qty_bhn_"+no);
		$(new_qty_bhn, lastRow).attr("name", "qty_bhn_"+no);		
		$(new_qty_bhn, lastRow).val('');				
		//*****end qty_bhn*************************************	
		
		//*****brg_jadi*************************************
		var brg_jadi="#brg_jadi_"+n;
		var new_brg_jadi="#brg_jadi_"+no;
		$(brg_jadi, lastRow).attr("id", "brg_jadi_"+no);
		$(new_brg_jadi, lastRow).attr("name", "brg_jadi_"+no);
		$(new_brg_jadi, lastRow).val('');
		//*****end brg_jadi*********************************
		
		//*****kode_brg_jadi*************************************
		var kode_brg_jadi="#kode_brg_jadi_"+n;
		var new_kode_brg_jadi="#kode_brg_jadi_"+no;
		$(kode_brg_jadi, lastRow).attr("id", "kode_brg_jadi_"+no);
		$(new_kode_brg_jadi, lastRow).attr("name", "kode_brg_jadi_"+no);		
		$(new_kode_brg_jadi, lastRow).val('');		
		//*****end kode_brg_jadi*********************************
						
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****uk_pola*************************************
		var uk_pola="#uk_pola_"+n;
		var new_uk_pola="#uk_pola_"+no;
		$(uk_pola, lastRow).attr("id", "uk_pola_"+no);
		$(new_uk_pola, lastRow).attr("name", "uk_pola_"+no);		
		$(new_uk_pola, lastRow).val('');
		//*****end uk_pola*************************************	
		
		//*****jammulai*************************************
		var jammulai="#jammulai_"+n;
		var new_jammulai="#jammulai_"+no;
		$(jammulai, lastRow).attr("id", "jammulai_"+no);
		$(new_jammulai, lastRow).attr("name", "jammulai_"+no);
		$(new_jammulai, lastRow).val('00:00');
		//*****end jammulai*************************************	
		
		//*****jamselesai*************************************
		var jamselesai="#jamselesai_"+n;
		var new_jamselesai="#jamselesai_"+no;
		$(jamselesai, lastRow).attr("id", "jamselesai_"+no);
		$(new_jamselesai, lastRow).attr("name", "jamselesai_"+no);
		$(new_jamselesai, lastRow).val('00:00');
		//*****end jamselesai*************************************	
		
		//*****bagian_brg_jadi*************************************
		var bagian_brg_jadi="#bagian_brg_jadi_"+n;
		var new_bagian_brg_jadi="#bagian_brg_jadi_"+no;
		$(bagian_brg_jadi, lastRow).attr("id", "bagian_brg_jadi_"+no);
		$(new_bagian_brg_jadi, lastRow).attr("name", "bagian_brg_jadi_"+no);		
		//$(new_ukuran_bisbisan, lastRow).val('');
		
		//*****ket*************************************
		var ket="#ket_"+n;
		var new_ket="#ket_"+no;
		$(ket, lastRow).attr("id", "ket_"+no);
		$(new_ket, lastRow).attr("name", "ket_"+no);		
		$(new_ket, lastRow).val('');
		//*****end ket*************************************	
		
		//*****operator*************************************
		var operator="#operator_"+n;
		var new_operator="#operator_"+no;
		$(operator, lastRow).attr("id", "operator_"+no);
		$(new_operator, lastRow).attr("name", "operator_"+no);		
		$(new_operator, lastRow).val('');
		//*****end operator*************************************	
						
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);	
		
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg/"+no+"/');";
		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		
		//button pilih_brg_jadi*****************************************
		 var pilih_brg_jadi="#pilih_brg_jadi_"+n;
		 var new_pilih_brg_jadi="#pilih_brg_jadi_"+no;
		 $(pilih_brg_jadi, lastRow).attr("id","pilih_brg_jadi_"+no);
		
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg_jadi/"+no+"/');";
		 $(new_pilih_brg_jadi, lastRow).attr("name", "pilih_brg_jadi_"+no);		
		 $(new_pilih_brg_jadi, lastRow).attr("onclick",even_klik);		 
		//end button pilih_brg_jadi
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
			
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/schedule-cutting/cform/submitdacron" method="post" enctype="multipart/form-data">
<input type="hidden" name="is_dacron" id="is_dacron" value="<?php echo $is_dacron ?>">
<input type="hidden" name="no" id="no" >

<div align="center">

<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
		<td width="20%">Schedule Cutting Untuk Dacron
		</td>
  </tr>
  <tr>
		<td width="20%">Nomor Schedule</td>
		<td>
		  <input type="text" name="noschedule" id="noschedule" maxlength="10" size="10" value="<?=$noschedule?>" />
    	</td>
  	</tr>
	<tr>
		<td>Tgl Schedule</td>
		<td>
		  <!-- <input type="hidden" name="no_pb_cutting" id="no_pb_cutting" value="<?=$no_pb_cutting?>" /> -->
		  <input type="text" name="tglschedule" id="tglschedule" maxlength="10" size="10" readonly="true" />
		  <img alt="" id="tgl_sc" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tglschedule,'dd-mm-yyyy',this)">
		</td>
  	</tr>
  	<tr>
		<td>Untuk Eksternal</td>
		<td>
		  <input type="checkbox" name="eksternal" id="eksternal" value="t" > *<i>Ini diceklis jika schedulenya untuk perusahaan luar</i>
    	</td>
  	</tr>
  	<tr>
		<td>Nama Bahan Dacron</td>
		<td>
		  <input type="text" name="bhn_baku" id="bhn_baku" size="40" value="" readonly="true" />
		  <input name="kode_bhn_baku" type="hidden" id="kode_bhn_baku" value="">  
           <input title="browse data bhn baku" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: 
           openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg/');">
    	</td>
  	</tr>
  	
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="10" align="left"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
			  <th>Nama Barang Jadi</th>
			  <th>Plan Qty (Set)</th>
			 <!-- <th>Bhn Baku/Pembantu</th> -->
			  <!--<th>Qty Yg Diklrkan</th> -->
			  <th>Nama Bagian</th>
			  <th>Ukuran Pola</th>
			  <th>Jam Mulai</th>
			  <th>Jam Selesai</th>
			  <th>Keterangan</th>
			 <!-- <th>Operator</th> -->
        </tr>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          
          <td nowrap="nowrap">
           <input name="brg_jadi_1" type="text" id="brg_jadi_1" size="30" readonly="true"/>
           <input name="kode_brg_jadi_1" type="hidden" id="kode_brg_jadi_1" value="">  
           <input title="browse data brg jadi" name="pilih_brg_jadi_1" value="..." type="button" id="pilih_brg_jadi_1" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg_jadi/1/');" type="button"></td>
           <td><input name="qty_1" type="text" id="qty_1" size="7" value="" /></td>
         
		<!--	<td nowrap="nowrap">
           <input name="bhn_baku_1" type="text" id="bhn_baku_1" size="30" readonly="true"/>
           <input name="kode_bhn_baku_1" type="hidden" id="kode_bhn_baku_1" value="">  
           <input title="browse data bhn baku" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: 
           openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg/1/');"></td> -->
           
         
      <!--   <td><input name="bhn_baku_1" type="text" id="bhn_baku_1" size="30" readonly="true"/>
			<input name="kode_bhn_baku_1" type="hidden" id="kode_bhn_baku_1" value="">  
			<input title="browse data bhn" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/schedule-cutting/cform/show_popup_brg/1/');" type="button">
         </td> -->
         
       <!--  <td><input name="qty_bhn_1" type="text" id="qty_bhn_1" size="7" value="" /></td> -->
         
         <td><select name="bagian_brg_jadi_1" id="bagian_brg_jadi_1" >
				<?php foreach ($bagian_brg_jadi as $bag) { ?>
					<option value="<?php echo $bag->id ?>" ><?php echo $bag->nama_bagian ?></option>
				<?php } ?>
				</select>
          </td>
          
          <td><input name="uk_pola_1" type="text" id="uk_pola_1" size="10" value="" /></td>
         <td><input name="jammulai_1" type="text" id="jammulai_1" size="7" value="00:00"/></td>
		 <td><input name="jamselesai_1" type="text" id="jamselesai_1" size="7" value="00:00"/></td>
			  
         <td><input name="ket_1" type="text" id="ket_1" size="20" /></td>
        <!-- <td><input name="operator_1" type="text" id="operator_1" size="10" value="" /></td> -->
        </tr>
	</table>	
	<br>
	</form>
      <div align="center"><br><br> 
        <input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/schedule-cutting/cform/'">
      </div></td>
    </tr>

</table>
</div>
</form>
