<div class="container" id="daftar-sukses">
    <p class="h2">Informasi Untuk Penginputan Master Unit packing</p>
    <hr>

    <div class="alert alert-success alert-dismissible" role="alert">
        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Penginputan Berhasil.
    </div>
    
    <p>Anda dapat kembali ke halaman Master Unit packing di <?php echo anchor('/dashboard/master_unit_packing/view', 'Master Unit packing'); ?>.</p>

</div> <!-- container -->
