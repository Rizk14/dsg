<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_laporansok; ?> <span style="color:#000000; font-weight:bold;"><?php echo $blnso." ".$thnso; ?></span></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_laporansok; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	  	<td>
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'expolaporanstok/cform','update'=>'#content','type'=>'post'));
		?>
		Cari:<input type="text" name="cari" id="cari" maxlength="200" value="<?php echo $cari; ?>" /><input type="checkbox" name="stp" id="stp" value="t" <?php echo $checked; ?> /> STP
		<input type="submit" id="btncari" name="btncari" value="Cari" />
		<?php echo form_close(); ?>	
		</td>
	  </tr>	
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'expolaporanstok/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterldobrgform">

		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_laporansok; ?></div></td>	
			</tr>
			<tr>
			  <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="3%" class="tdatahead">No</td>
				<td width="12%" class="tdatahead"><?php echo $list_laporansok_kd_brg; ?></td>
				<td width="30%" class="tdatahead"><?php echo $list_laporansok_nm_brg; ?></td>
				<td width="10%" class="tdatahead"><?php echo $list_laporansok_awalstok_brg; ?></td>
				<td width="10%" class="tdatahead"><?php echo $list_laporansok_akhirstok_brg; ?></td>
				<td width="15%" class="tdatahead"><?php echo $list_laporansok_ket_brg; ?></td>
			  </tr>
			  <?php
			  if(sizeof($query)>0){
			  	$nom= 1;
				$cc	= 1;
				$j	= 0;
				$bonmkeluar	= array();
				$bonmmasuk	= array();
				$bbk	= array();
				$bbm	= array();
				$do	= array();
				$sj = array();
				
				$x11	= array();
				$x22	= array();
				$z11	= array();
				$saldoakhirnya	= array();
				
			  	foreach($query as $row){
					
					$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
					$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

					$qtyawal	= $row->qtyawal<1?"<span style=\"font-color:#FF0000; font-weight:bold;\">".$row->qtyawal."</span>":$row->qtyawal;
					$qtyakhir	= $row->qtyakhir<1?"<span style=\"font-color:#FF0000; font-weight:bold;\">".$row->qtyakhir."</span>":$row->qtyakhir;		

					$qbonmkeluar	= $this->mclass->lbonkeluar($tglawalbln,$tglakhirbln,$row->imotif);
					if($qbonmkeluar->num_rows()>0) {
						$row_bmkeluar	= $qbonmkeluar->row();
						$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
					} else {
						$bonmkeluar[$j] = 0;
					}
	
					$qbonmmasuk	= $this->mclass->lbonmasuk($tglawalbln,$tglakhirbln,$row->imotif);
					if($qbonmmasuk->num_rows()>0) {
						$row_bmmasuk	= $qbonmmasuk->row();
						$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
					} else {
						$bonmmasuk[$j] = 0;
					}
	
					$qbbk	= $this->mclass->lbbk($tglawalbln,$tglakhirbln,$row->imotif);
					if($qbbk->num_rows()>0) {
						$row_bbk	= $qbbk->row();
						$bbk[$j]	= $row_bbk->jbbk; 
					} else {
						$bbk[$j] = 0;
					}
	
					$qbbm	= $this->mclass->lbbm($tglawalbln,$tglakhirbln,$row->imotif);
					if($qbbm->num_rows()>0) {
						$row_bbm	= $qbbm->row();
						$bbm[$j]	= $row_bbm->jbbm; 
					} else {
						$bbm[$j] = 0;
					}
	
					$qdo	= $this->mclass->ldo($tglawalbln,$tglakhirbln,$row->imotif);
					if($qdo->num_rows()>0) {
						$row_do	= $qdo->row();
						$do[$j]	= $row_do->jdo; 
					} else {
						$do[$j] = 0;
					}

					$qsj	= $this->mclass->lsj($tglawalbln,$tglakhirbln,$row->imotif);
					if($qsj->num_rows()>0) {
						$row_sj	= $qsj->row();
						$sj[$j]	= $row_sj->jsj; 
					} else {
						$sj[$j] = 0;
					}
					
					$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
					$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
					$z11[$j]	= $row->qtyawal+$x11[$j];
					$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
																
			  ?>
				  <tr class="<?=$Classnya?>" onMouseOver="this.className='rowx'"
              	 onMouseOut="this.className='<?=$Classnya?>'" >
					<td class="tdatahead2"><?=$nom?></td>
					<td class="tdatahead2"><?php echo $row->imotif; ?></td>
					<td class="tdatahead2"><?php echo $row->eproductname; ?></td>
					<td class="tdatahead2" align="right"><?php echo $qtyawal; ?></td>
					<!-- <td class="tdatahead2" align="right"><?php echo $qtyakhir; ?></td> -->
					<td class="tdatahead2" align="right"><?php echo $saldoakhirnya[$j]; ?></td>
					<td class="tdatahead2"><?php echo $row->enota; ?></td>
				  </tr>
			  <?php
			  		$j++;
			  		$nom+=1; $cc++;
			  	}
			  }
			  ?>
			</table>
			  </td>
			</tr>
			<tr><td align="center"><?php echo $create_link; ?></td></tr>			
			<tr>
			  <td align="right">
				  
				  <input name="btnkonversi" type="button" id="btnkonversi" value="<?php echo $button_excel; ?>" 
				  onclick="window.location='<?php echo base_url(); ?>index.php/expolaporanstok/cform/expolaporanstok/<?=$uri1?>/<?=$uri2?>'"  /></td>
			</tr>
		  </table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
