<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Rekapitulasi Hutang Dagang</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br>
Jenis Pengambilan Data: <?php echo $namajenisdata ?><br><br>
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-rekap-hutang/cform/export_excel', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenisdata" value="<?php echo $jenisdata ?>" >
<input type="hidden" name="namajenisdata" value="<?php echo $namajenisdata ?>" >
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Supplier</th>
		 <th>Saldo Awal</th>
		 <th>Pembelian<br>Bahan Baku/Pembantu</th>
		 <th>Retur</th>
		 <th>Pembelian<br>Makloon</th>
		 <!--<th>Pembulatan</th>-->
		 <th>Pelunasan</th>
		 <th>Pembulatan</th>
		  <th>C/n</th>
		 <th>Saldo Akhir</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			
			/*if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_hutang += $query[$j]['jumlah'];
					$tot_ppn += $query[$j]['pajaknya'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
							 if ($var_detail[$k]['kode_perk'] == "511.100")
								$tot_baku += $var_detail[$k]['total'];
							 else
								$tot_pembantu += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
			} */
			
			$nomor = 1;
			if (is_array($query)) {
				$tot_hutang = 0;
				$tot_debet = 0;
				$tot_retur = 0;
				$tot_pembelian = 0; $tot_pembelian_makloon = 0;
				$tot_bulat1 = 0;
				$tot_bulat2 = 0;
				$tot_cndn2=0;
				$tot_saldo_akhir = 0;
				
			 for($j=0;$j<count($query);$j++){
				 $tot_hutang+= $query[$j]['tot_hutang'];
				 $tot_debet+= $query[$j]['tot_debet'];
				 $tot_retur+= $query[$j]['tot_retur'];
				 
				 if ($query[$j]['kategori_sup'] == 1)
					$tot_pembelian+= $query[$j]['tot_pembelian'];
				 else
					$tot_pembelian_makloon+= $query[$j]['tot_pembelian'];
				 
				 $tot_saldo_akhir+= $query[$j]['saldo_akhir'];
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$nomor."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['tot_hutang'],4,',','.')."</td>";
				 
				 if ($query[$j]['kategori_sup'] == 1)
					echo    "<td align='right'>".number_format($query[$j]['tot_pembelian'],4,',','.')."</td>";
				 else
					echo    "<td align='right'>".number_format(0,4,',','.')."</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['tot_retur'],4,',','.')."</td>";
				 if ($query[$j]['kategori_sup'] == 2)
					echo    "<td align='right'>".number_format($query[$j]['tot_pembelian'],4,',','.')."</td>";
				 else
					echo    "<td align='right'>".number_format(0,4,',','.')."</td>";
				 /*if ($query[$j]['tot_bulat'] < 0) {
					echo    "<td align='right'>".number_format(abs($query[$j]['tot_bulat']),2,',','.')."</td>";
					$tot_bulat1+= abs($query[$j]['tot_bulat']);
				 }
				 else
					echo    "<td>&nbsp;</td>"; */
								 
				 echo    "<td align='right'>".number_format($query[$j]['tot_debet'],4,',','.')."</td>";
				
				 //if ($query[$j]['tot_bulat'] > 0) {
					echo    "<td align='right'>".number_format($query[$j]['tot_bulat'],4,',','.')."</td>";
					$tot_bulat2+= $query[$j]['tot_bulat'];
				echo    "<td align='right'>".number_format($query[$j]['tot_cndn'],4,',','.')."</td>";
				 $tot_cndn2+= $query[$j]['tot_cndn'];
				 //}
				 //else
				//	echo    "<td>&nbsp;</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['saldo_akhir'],4,',','.')."</td>";
				 				 				 							 
				 echo  "</tr>";
				$nomor++;
		 	}
		 	echo "<tr class=\"record\">";
			
			echo    "<td colspan='2' align='right'><b>GRAND TOTAL</b></td>";
				 echo    "<td align='right'><b>".number_format($tot_hutang,4,',','.')."</b></td>";
				 
				 echo    "<td align='right'><b>".number_format($tot_pembelian,4,',','.')."</b></td>";
				 echo    "<td align='right'><b>".number_format($tot_retur,4,',','.')."</b></td>";
				 echo    "<td align='right'><b>".number_format($tot_pembelian_makloon,4,',','.')."</b></td>";
				 
				 //echo    "<td align='right'><b>".number_format($tot_bulat1,2,',','.')."</b></td>";
								 
				
				 echo    "<td align='right'><b>".number_format($tot_debet,4,',','.')."</b></td>";
				 echo    "<td align='right'><b>".number_format($tot_bulat2,4,',','.')."</b></td>";
				 echo    "<td align='right'><b>".number_format($tot_cndn2,4,',','.')."</b></td>";
				 
				 echo    "<td align='right'><b>".number_format($tot_saldo_akhir,4,',','.')."</b></td>";
		 	
		 	echo "</tr>";
		   }
		 ?>
		 
 	</tbody>
</table><br>
<?php //echo $this->pagination->create_links();?>
</div>
