<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

//function getJatuhTempo(tahun,bulan,hari){ 
function getJatuhTempo(source,destination,range){ // d/m/Y
	var nilai;

	var tsplit	= source.split('/'); // d/m/Y
	var t	= parseInt(tsplit['0'])+parseInt(range);
	//var tanggal = new Date(tahun,bulan,hari+28); 
	var tanggal = new Date(tsplit['2'],tsplit['1'],t,0,0,0); 
	//var tanggal = new Date(tahun,bulan,hari); 
	//tanggal.setDate(tanggal.getDate()+28); 
	var tgl	= tanggal.getDate();
	var bln = tanggal.getMonth();
	var thn = tanggal.getFullYear();

	nilai = tgl+'/'+bln+'/'+thn;
	destination.value	= nilai;
	//return tanggal; 
	//return nilai; 
}

function tgpajak(tfaktur){
	document.getElementById('d_pajak').value = tfaktur;
}

function konfirm(){

	var kon=window.confirm("yakin akan simpan ??");

	if(document.getElementById('i_branch').value=='') {
		alert('Pilih Cabang Pelanggan !!');
		return false;
	}

	if (kon){
		return true;
	}else{
		return false;
	}
}

function ckfpenjualan(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualan/cform/cari_fpenjualan');?>",
	data:"fpenj="+nomor,
	success: function(data){
		$("#confnomorfpenj").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function ckfpajak(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualan/cform/cari_fpajak');?>",
	data:"fpajak="+nomor,
	success: function(data){
		$("#confnomorfpajak").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka!");
		angka.value	= 0;
		angka.focus();
	}
}

/*************************
Faktur Penjualan :D

Kasus :
total nilai = 10000
Diskon 	= 2 % dalam nilai 5000


Perhitungan :
Total 	= 10000 - (5000*2%)
		= 10000 - 4900
		= 5100

PPN 10 %	= 5100 + (5100*10%=510)

Grand Total	= 5610



-----------------------------------------------------
Koreksi kolom "dlm nilai" ===> "total hasil nilai"

Kasus :
total nilai = 10000
Diskon = 2 % hasil nilai = "9800"


Perhitungan :
nilai diskon 2 %	= (10000*2%)
					= 200

Total 		= 10000 - 200
			= 9800
			
PPN 10 %	= 9800*10%)
			= 980

Grand Total	= 9800 + 980
			= 10780
		
**************************/
function tnilaibarang(){

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseInt(unitprice.value);
		}
	}
	totalnilai.value	= totaln;
}

function test() {
	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var nilai	= totalnilai;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;	
	var val		= document.getElementById('n_discount');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			//totaln += parseInt(unitprice.value);
			totaln += parseFloat(unitprice.value);
		}
	}
		
	//totalnilai.value	= totaln.toFixed(2);
	totalnilai.value	= totaln;
	
	/* Format Rupiah */
	/*
	totaln=""+totaln+"";
	var len=totaln.length;
	var mod=len % 3;
	if(mod==0) mod =3;
	var divided= len/3;
	divided=parseInt(divided);
	var priceFormat=totaln.substr(0,mod)+".";
	var pointer=0;	

	for(var pointer=0;pointer<divided; pointer++){
		priceFormat=priceFormat+""+totaln.substr((mod+(pointer*3)),3)+".";
	}
	while(priceFormat.substr(priceFormat.length-1,1)==".") {
		priceFormat=priceFormat.substr(0,priceFormat.length-1);
	}
	while(priceFormat.substr(0,1)==".") {
		priceFormat=priceFormat.substr(1,priceFormat.length-1);
	}
	priceFormat=priceFormat+",00";	
	
	totalnilai.value	= priceFormat;
	*/
	/* End 0f Format Rupiah */
	
	if(parseInt(nilai.value)) {
		hasil 	= ( (parseInt(nilai.value)*val.value) / 100 ); // dikurangi discount
		total_sblm_ppn	= parseInt(nilai.value) - hasil; // DPP
		nilai_ppn	= ( (total_sblm_ppn*10) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;
		/*
		document.getElementById('v_discount').value = hasil.toFixed(2);
		document.getElementById('v_total_faktur').value	= total_sblm_ppn.toFixed(2);
		document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
		document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);
		*/
		document.getElementById('v_discount').value = Math.round(hasil);
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*10) / 100 );
		total_grand	= xtotaln + nilai_ppn;
		/*	
		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= xtotaln.toFixed(2);
		document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
		document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);	
		*/
		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

function test2() {
	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var nilai	= totalnilai;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;	
	
	var aa;
	//var val		= document.getElementById('n_discount');
	var val		= document.getElementById('v_discount');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseFloat(unitprice.value);
		}
	}
		
	totalnilai.value	= totaln;
	
	if(parseInt(nilai.value)) {
		//hasil 	= ( (parseInt(nilai.value)*val.value) / 100 ); // dikurangi discount
		aa	= (val.value / parseInt(nilai.value))*100;
		//total_sblm_ppn	= parseInt(nilai.value) - hasil; // DPP
		total_sblm_ppn	= parseInt(nilai.value) - val.value;
		nilai_ppn	= ( (total_sblm_ppn*10) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;

		//document.getElementById('v_discount').value = Math.round(hasil);
		if(aa>0 && val.value>0){
			document.getElementById('n_discount').value = Math.round(aa*Math.pow(10,2))/Math.pow(10,2);
		}else{
			document.getElementById('n_discount').value = 0;
		}
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*10) / 100 );
		total_grand	= xtotaln + nilai_ppn;

		//document.getElementById('v_discount').value = 0;
		document.getElementById('n_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

function total(iterasi) {
	var	total;
	var	price;
	var	unit0;
	
	price	= document.getElementById('v_hjp_tblItem_'+iterasi);
	unit0	= document.getElementById('n_quantity_tblItem_'+iterasi);
	
	if(parseInt(price.value) && parseInt(unit0.value)) {
		total	= parseInt(price.value) * parseInt(unit0.value);
		document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
	}
}

function getdsk() {
	var nilai;
	var val;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;

	nilai	= document.getElementById('v_total_nilai');
	val		= document.getElementById('n_discount');
		
	/*
	if(parseInt(nilai.value) && parseInt(val.value)) {
	*/
	
	if(parseInt(nilai.value)) {
		hasil 	= ( (parseInt(nilai.value)*val.value) / 100 );
		total_sblm_ppn	= parseInt(nilai.value) - hasil;
		nilai_ppn	= ( (total_sblm_ppn*10) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;
		/*
		document.getElementById('v_discount').value = hasil.toFixed(2);
		document.getElementById('v_total_faktur').value	= total_sblm_ppn.toFixed(2);
		document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
		document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);
		*/
		document.getElementById('v_discount').value = Math.round(hasil);
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*10) / 100 );
		total_grand	= xtotaln + nilai_ppn;
		/*	
		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= xtotaln.toFixed(2);
		document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
		document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);
		*/
		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

/*
function price_format(values) {
	values=""+values+"";
	var len=values.length;
	var mod=len % 3;
	if(mod==0) mod =3;
	var divided= len/3;
	divided=parseInt(divided);
	var priceFormat=values.substr(0,mod)+".";
	var pointer=0;
	
	for(var pointer=0;pointer<divided; pointer++){
		priceFormat=priceFormat+""+values.substr((mod+(pointer*3)),3)+".";
	}
	while(priceFormat.substr(priceFormat.length-1,1)==".") {
		priceFormat=priceFormat.substr(0,priceFormat.length-1);
	}
	while(priceFormat.substr(0,1)==".") {
		priceFormat=priceFormat.substr(1,priceFormat.length-1);
	}
	priceFormat=priceFormat+",00";
	return priceFormat;
}
*/

function ipajak(faktur){
	var ifaktur	= faktur;
	var ccc		= (ifaktur.length)-4;
	var ipajak	= ifaktur.substr(4,ccc);
	document.getElementById('i_faktur_pajak').value	= ipajak;

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualan/cform/cari_fpajak');?>",
	data:"fpajak="+ipajak,
	success: function(data){
		$("#confnomorfpajak").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})	
}

function ipajakck(faktur){
	var ifaktur	= faktur;
	var ccc		= (ifaktur.length)-4;
	var ipajak	= ifaktur.substr(4,ccc);
	var cbox	= document.getElementById('cbox').value;
	
	if(document.getElementById('ckbox').checked==true){
		var satu	= ipajak;
		document.getElementById('i_faktur_pajak').value	= ipajak;	
	}else{
		var satu	= cbox;
		document.getElementById('i_faktur_pajak').value = cbox;
	}

	$.ajax({
		
	type: "POST",
	url: "<?php echo site_url('fakpenjualan/cform/cari_fpajak');?>",
	data:"fpajak="+satu,
	success: function(data){
		$("#confnomorfpajak").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})		
}

function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var brs = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	var cab	= document.getElementById('i_branch');
	
	if(iteration<22){
		tbl.width='100%';
	
		if(iteration%2==0){
			var bgColorValue = '#FFFFFF';
		}else{
			var bgColorValue = '#FFFFFF';
		}
		row.bgColor = bgColorValue;

		var cell1 = row.insertCell(0);
		cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:18px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

		var cell1 = row.insertCell(1);
		cell1.innerHTML = "<DIV ID=\"ajax_i_do_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"i_do_"+nItem+"_"+iteration+"\"  name=\"i_do_"+nItem+"_"+iteration+"\" style=\"width:82px;\" onfocus=\"total("+iteration+");test();\" readonly ><img name=\"img_i_do_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor DO\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualan('"+iteration+"','"+document.getElementById('i_branch').value+"','"+brs+"');\"></DIV>";

		var cell1 = row.insertCell(2);
		cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:120px;\" readonly ></DIV>";

		var cell1 = row.insertCell(3);
		cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:200px;\" readonly ></DIV>";
	
		var cell1 = row.insertCell(4);
		cell1.innerHTML = "<DIV ID=\"ajax_v_hjp_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"v_hjp_"+nItem+"_"+iteration+"\"  name=\"v_hjp_"+nItem+"_"+iteration+"\" style=\"width:90px;text-align:right;\" value=\"0\" readonly ></DIV>";

		var cell1 = row.insertCell(5);
		cell1.innerHTML = "<DIV ID=\"ajax_n_quantity_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"n_quantity_"+nItem+"_"+iteration+"\"  name=\"n_quantity_"+nItem+"_"+iteration+"\" style=\"width:95px;text-align:right;\" onkeyup=\"total("+iteration+");test();validNum('n_quantity_tblItem','"+iteration+"');\" value=\"0\" ></DIV>";

		var cell1 = row.insertCell(6);
		cell1.innerHTML = "<DIV ID=\"ajax_v_unit_price_"+nItem+"_"+iteration+"\" text-align:right;\" ><input type=\"text\" ID=\"v_unit_price_"+nItem+"_"+iteration+"\"  name=\"v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:85px;text-align:right;\" onkeyup=\"total("+iteration+");test();validNum('v_unit_price_tblItem','"+iteration+"');\" value=\"0\" readonly ><input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\" ></DIV>";
	}else{
		alert('item DO tdk dpt lebih dari 22 item !');
		return false;		
	}
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}

</script>

<!--
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#d_faktur").datepicker();
	$("#d_due_date").datepicker();
	$("#d_pajak").datepicker();
});
</script>
-->

<!--
<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
-->

<?php
// berfungsi 
include_once("funcs/1tanggal.php");
$tjthtempo =& dateAdd("d",38,$dateTime);

// berfungsi
function JatuhTempo($tgl,$hari){ // d/m/Y
	list($tgl,$bln,$thn)	= explode("/",$tgl,strlen($tgl));
	$tempstamp	= mktime(0,0,0,$bln,$tgl+$hari,$thn);
	return date('d/m/Y',$tempstamp);
}
$tempo =& JatuhTempo(date('d/m/Y'),38);
?>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_fpenjualando; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_fpenjualando; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		  <?php 
		   $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('fakpenjualan/cform/simpan', $attributes);?>
		   
		<div id="masterfpenjualandoform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $form_nomor_f_fpenjualando; ?> : 
							  <input name="i_faktur" type="text" id="i_faktur" maxlength="13" value="<?php echo $no; ?>" onkeyup="ckfpenjualan(this.value); ipajak(this.value);"/>
							  <input type="hidden" name="cbox" id="cbox" value="<?=$nofakturpajak?>" />
							  <input type="checkbox" name="ckbox" id="ckbox" onclick="ipajakck(document.getElementById('i_faktur').value);" />no.pajak							  
							  <div id="confnomorfpenj" style="color:#FF0000;"></div>
							  </td>
							<td width="33%"><?php echo $form_tgl_f_fpenjualando; ?> : 
							<input name="d_faktur" type="text" id="d_faktur" maxlength="10" value="<?php echo $tgFaktur ?>" readonly="true" onclick="displayCalendar(document.forms[0].d_faktur,'dd/mm/yyyy',this)">
				
							  <!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_faktur,'dd/mm/yyyy',this)"> -->
							  </td>
							<td width="33%"><?php echo $form_cabang_fpenjualando; ?> : 
							  <select name="i_branch" id="i_branch">
								<option value="">[ <?php echo $form_pilih_cab_fpenjualando;?> ]</option>
								<?php
									foreach($opt_cabang as $row) {
										$lcabang	.="<option value=".$row->codebranch.">";
										$lcabang	.= $row->branch." ( ".$row->einitial." ) ";
										$lcabang	.= "</option>";
									}
									echo $lcabang;
								?>
							  </select>
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>
					<tr>
					  <td>
						  <div id="title-box2"><?php echo $form_detail_f_fpenjualando; ?>					  
						  <div style="float:right;">
						  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');test();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
						  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');test();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
						  </div></div>					  
					  </td>
					</tr>
					<tr>
					  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="4%" class="tdatahead">NO</td>
						  <td width="15%" class="tdatahead"><?php echo $form_nomor_do_f_fpenjualando; ?></td>
						  <td width="15%" class="tdatahead"><?php echo $form_kd_brg_fpenjualando; ?></td>
						  <td width="26%" class="tdatahead"><?php echo $form_nm_brg_fpenjualando; ?></td>
						  <td width="14%" class="tdatahead"><?php echo $form_hjp_fpenjualando; ?></td>
						  <td width="14%" class="tdatahead"><?php echo $form_qty_fpenjualando; ?></td>
						  <td width="14%" class="tdatahead"><?php echo $form_nilai_fpenjualando; ?></td>
						</tr>
						<tr>
						  <td colspan="7">
						  	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							</table>
						  </td>
						</tr>						
                      </table></td>
					</tr>	
					<tr>
					 <td>
					  <div id="title-box2">
					  <div style="float:right;">
					  &nbsp;
					  </div></div>
					 </td>	
					</tr>
					<tr>
					  <td><table width="98%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="16%">&nbsp;</td>
						  <td width="1%">&nbsp;</td>
						  <td width="23%">&nbsp;</td>
						  <td width="24%">&nbsp;</td>
						  <td width="14%"><?php echo $form_tnilai_fpenjualando; ?></td>
						  <td width="1%">:</td>
						  <td width="21%">
							<input name="v_total_nilai" type="text" id="v_total_nilai" size="15" maxlength="15" value="0" readonly />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>

						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_diskon_fpenjualando; ?> (%)</td>
						  <td>:</td>
						  <td>
						  <input name="n_discount" type="text" id="n_discount" size="10" maxlength="10" value="0" onkeyup="test();validNum('n_discount');"/>
						  <?php echo $form_dlm_fpenjualando; ?> 
						  <input name="v_discount" type="text" id="v_discount" size="12" maxlength="12" value="0" onkeyup="test2();" />
						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_tgl_jtempo_fpenjualando; ?> </td>
						  <td>:</td>
						  <td>
							<input name="d_due_date" type="text" id="d_due_date" size="10" maxlength="10" value="<?php echo $tjthtempo; ?>" onclick="displayCalendar(document.forms[0].d_due_date,'dd/mm/yyyy',this)"/> 
							<!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_due_date,'dd/mm/yyyy',this)"> -->
						  </td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_total_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_faktur" type="text" id="v_total_faktur" size="15" maxlength="15" value="0" readonly />
						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_no_fpajak_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="i_faktur_pajak" type="text" id="i_faktur_pajak" size="15" maxlength="18" value="<?php echo $nofakturpajak; ?>" onclick="displayCalendar(document.forms[0].d_due_date,'dd/mm/yyyy',this)"/> 
							<div id="confnomorfpajak" style="color:#FF0000;"></div>
						  </td>
						  <td><?php echo $form_tgl_fpajak_fpenjualando; ?> :
							<input name="d_pajak" type="text" id="d_pajak" size="10" maxlength="10" value="<?php echo $tgPajak; ?>"onclick="displayCalendar(document.forms[0].d_pajak,'dd/mm/yyyy',this)"/>
							<!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_pajak,'dd/mm/yyyy',this)"> -->
							</td>
						  <td><?php echo $form_ppn_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="n_ppn" type="text" id="n_ppn" size="15" maxlength="15" value="0" readonly />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>
							<input type="checkbox" name="f_cetak" value="1" />
							&nbsp;<?php echo $form_ket_cetak_fpenjualando; ?></td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_grand_t_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_fppn" type="text" id="v_total_fppn" size="15" maxlength="15" value="0" readonly />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					</tr>
					<tr align="right">
					  <td>
						<input name="btnsimpan" type="submit" id="btnsimpan" value="<?php echo $button_simpan; ?>" onclick="return konfirm();" />
						 <input name="btnbatal" id="btnbatal" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/fakpenjualan/cform/'">

					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
