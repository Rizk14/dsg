<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Harga Barang BS</h3><br>
<a href="<?php echo base_url(); ?>index.php/gradeb/cform/addgradeb">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/gradeb/cform/">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('gradeb/cform/carigradeb'); ?>
<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		 <th>Kode</th>
		 <th>Nama Brg Jadi</th>
		 <th>Harga (Rp.)</th>
		 <th>Bulan</th>
		 <th>Tahun</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
					
					$bulan =$row->bulan;
						if ($bulan == '01')
					$nama_bulan = "Januari";
				else if ($bulan == '02')
					$nama_bulan = "Februari";
				else if ($bulan == '03')
					$nama_bulan = "Maret";
				else if ($bulan == '04')
					$nama_bulan = "April";
				else if ($bulan == '05')
					$nama_bulan = "Mei";
				else if ($bulan == '06')
					$nama_bulan = "Juni";
				else if ($bulan == '07')
					$nama_bulan = "Juli";
				else if ($bulan == '08')
					$nama_bulan = "Agustus";
				else if ($bulan == '09')
					$nama_bulan = "September";
				else if ($bulan == '10')
					$nama_bulan = "Oktober";
				else if ($bulan == '11')
					$nama_bulan = "November";
				else if ($bulan == '12')
					$nama_bulan = "Desember";
					
				
				$pisah1 = explode(" ", $tgl1);
				$tglnya= $pisah1[0];
				$jamnya= $pisah1[1];
				$tgl_update = $tglnya." ".$nama_bln." ".$thn1." ".$jamnya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode_brg_wip</td>";
				  echo    "<td nowrap>$row->nama_brg</td>";
				 
				 echo    "<td align='right'>".number_format($row->harga, 2, ',','.')."</td>";
				 echo    "<td nowrap align=center>".$nama_bulan."</td>";
				 echo    "<td nowrap>$row->tahun</td>";
				 echo    "<td>$tgl_update</td>";
				 echo    "<td align=center><a href=".base_url()."index.php/gradeb/cform/editgradeb/$row->idharga/$row->id_brg_wip/".$cur_page."/".$is_cari."/".$cari." \" >Edit</a>&nbsp;";
				 echo    "<a href=".base_url()."index.php/gradeb/cform/deletegradeb/$row->idharga/$row->id_brg_wip/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
