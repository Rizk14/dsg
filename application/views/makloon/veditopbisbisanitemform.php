<h3>Data Order Pembelian (OP) Makloon Bisbisan</h3><br>
<a href="<?php echo base_url(); ?>index.php/makloon/cform/addopbisbisan">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/makloon/cform/viewopbisbisan">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

 $(function()
 {

 });
 
 function cek_data() {
	var qty= $('#qty').val();

	if (qty == '' || qty == '0') {
		alert("Qty tidak boleh 0 / kosong..!");
		$('#qty').focus();
		return false;
	}
	
	if (isNaN($('#qty').val())) {
		alert("Qty harus berupa angka..!");
		$('#qty').focus();
		return false;
	}
 }	

</script>

<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<?php 
$attributes = array('name' => 'f_op', 'id' => 'f_op');
echo form_open('makloon/cform/editopbisbisanitem', $attributes); ?>
<input type="hidden" name="go_edit" value="1">
<input type="hidden" name="iddetail" value="<?php echo $iddetail ?>">
<input type="hidden" name="id_op_makloon" value="<?php echo $query[0]['id_op_makloon'] ?>">
<input type="hidden" name="id_bonmkeluar_detail" value="<?php echo $query[0]['id_bonmkeluar_detail'] ?>">
<input type="hidden" name="id_brg" value="<?php echo $query[0]['id_brg'] ?>">
<input type="hidden" name="curpage" value="<?php echo $curpage ?>">
<input type="hidden" name="csupplier" value="<?php echo $csupplier ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">

<table width="50%">
		<tr>
			<td colspan="2">Edit Data</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td width="30%">Nomor OP</td>
			<td> <?php echo $query[0]['no_op'] ?>
			</td>
		</tr>
		<tr>
			<td>Tanggal OP</td>
			<td> 
				<?php 
				$pisah1 = explode("-", $query[0]['tgl_op']);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					
					$tgl_op = $tgl1."-".$bln1."-".$thn1;
					
				?>
			
				<input name="tgl_op" type="text" id="tgl_op" size="10" value="<?php echo $tgl_op ?>" readonly="true">
				  <img alt="" id="tgl_op" align="middle"
						title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
						onclick="displayCalendar(document.forms[0].tgl_op,'dd-mm-yyyy',this)">
			</td>
		</tr>
		<tr>
			<td>Supplier</td>
			<td>
			<select name="id_supplier" id="id_supplier">
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($query[0]['id_supplier'] == $sup->id) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>
			
			</td>
		</tr>
		<tr>
			<td>Kode & Nama Brg</td>
			<td> <?php echo $query[0]['kode_brg']." - ".$query[0]['nama_brg'] ?>
			<input type="hidden" name="id_satuan_konversi" id="id_satuan_konversi" value="<?php echo $query[0]['id_satuan_konversi'] ?>">
			<input type="hidden" name="rumus_konversi" id="rumus_konversi" value="<?php echo $query[0]['rumus_konversi'] ?>">
          <input type="hidden" name="angka_faktor_konversi" id="angka_faktor_konversi" value="<?php echo $query[0]['angka_faktor_konversi'] ?>">
			</td>
		</tr>
		
		<tr>
			<td>Qty</td>
			<td> <input type="text" name="qty" id="qty" style="text-align:right;" value="<?php echo $query[0]['qty'] ?>" size="7" onkeyup="javascript: var rumus= $('#rumus_konversi').val(); var angka= $('#angka_faktor_konversi').val(); 
          if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal').val(qtynya); } 
          else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal').val(qtynya); }
          else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal').val(qtynya); }
          else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal').val(qtynya); }
          else { $('#qty_satawal').val(this.value); }"> <?php echo $query[0]['nama_satuan'] ?>
				<input type="hidden" name="qty_lama" id="qty_lama" value="<?php echo $query[0]['qty'] ?>">
			</td>
		</tr>
		<tr>
			<td>Qty Satuan Awal</td>
			<td> <input type="text" readonly="true" name="qty_satawal" id="qty_satawal" style="text-align:right;" value="<?php echo $query[0]['qty_satawal'] ?>" size="7" >
				<?php echo $query[0]['nama_satuan_konv'] ?>
				<input type="hidden" name="qty_lama_satawal" id="qty_lama_satawal" value="<?php echo $query[0]['qty_satawal'] ?>">
			</td>
		</tr>
</table><br>
<?php 
	if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "makloon/cform/viewopbisbisan/index/".$curpage;
	else
		$url_redirectnya = "makloon/cform/cariopbisbisan/".$csupplier."/".$carinya."/".$curpage;
?>
<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<?php echo form_close(); ?>
