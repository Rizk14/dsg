<h3>Data Bukti Terima Barang (BTB) Makloon Bisbisan</h3><br>
<a href="<?php echo base_url(); ?>index.php/makloon/cform/addbtbbisbisan">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/makloon/cform/viewbtbbisbisan">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****id_detail*************************************
		var id_detail="#id_detail_"+n;
		var new_id_detail="#id_detail_"+no;
		$(id_detail, lastRow).attr("id", "id_detail_"+no);
		$(new_id_detail, lastRow).attr("name", "id_detail_"+no);		
		$(new_id_detail, lastRow).val('n');				
		//*****end id_detail*************************************	
		
		//*****jenis_potong*************************************
		var jenis_potong="#jenis_potong_"+n;
		var new_jenis_potong="#jenis_potong_"+no;
		$(jenis_potong, lastRow).attr("id", "jenis_potong_"+no);
		$(new_jenis_potong, lastRow).attr("name", "jenis_potong_"+no);		
		//*****end jenis_potong*********************************
		
		//*****id_ukuran*************************************
		var id_ukuran="#id_ukuran_"+n;
		var new_id_ukuran="#id_ukuran_"+no;
		$(id_ukuran, lastRow).attr("id", "id_ukuran_"+no);
		$(new_id_ukuran, lastRow).attr("name", "id_ukuran_"+no);		
		//*****end id_ukuran*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
				
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****id_brg_lama*************************************
		var id_brg_lama="#id_brg_lama_"+n;
		var new_id_brg_lama="#id_brg_lama_"+no;
		$(id_brg_lama, lastRow).attr("id", "id_brg_lama_"+no);
		$(new_id_brg_lama, lastRow).attr("name", "id_brg_lama_"+no);		
		$(new_id_brg_lama, lastRow).val('');		
		//*****end id_brg_lama*********************************
				
		//*****kode_lama*************************************
		var kode_lama="#kode_lama_"+n;
		var new_kode_lama="#kode_lama_"+no;
		$(kode_lama, lastRow).attr("id", "kode_lama_"+no);
		$(new_kode_lama, lastRow).attr("name", "kode_lama_"+no);		
		$(new_kode_lama, lastRow).val('');
		// **** end kode_lama
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****qty_lama*************************************
		var qty_lama="#qty_lama_"+n;
		var new_qty_lama="#qty_lama_"+no;
		$(qty_lama, lastRow).attr("id", "qty_lama_"+no);
		$(new_qty_lama, lastRow).attr("name", "qty_lama_"+no);		
		$(new_qty_lama, lastRow).val('');				
		//*****end qty_lama*************************************	
		
		//*****qty_op*************************************
		var qty_op="#qty_op_"+n;
		var new_qty_op="#qty_op_"+no;
		$(qty_op, lastRow).attr("id", "qty_op_"+no);
		$(new_qty_op, lastRow).attr("name", "qty_op_"+no);		
		$(new_qty_op, lastRow).val('0');				
		//*****end qty_op*************************************	
		
		//*****jum_beli*************************************
		var jum_beli="#jum_beli_"+n;
		var new_jum_beli="#jum_beli_"+no;
		$(jum_beli, lastRow).attr("id", "jum_beli_"+no);
		$(new_jum_beli, lastRow).attr("name", "jum_beli_"+no);		
		$(new_jum_beli, lastRow).val('0');				
		//*****end jum_beli*************************************	
		
		//*****satuan_konv*************************************
		var satuan_konv="#satuan_konv_"+n;
		var new_satuan_konv="#satuan_konv_"+no;
		$(satuan_konv, lastRow).attr("id", "satuan_konv_"+no);
		$(new_satuan_konv, lastRow).attr("name", "satuan_konv_"+no);		
		$(new_satuan_konv, lastRow).val('');				
		//*****end satuan_konv*************************************	
		
		//*****rumus_konversi*************************************
		var rumus_konversi="#rumus_konversi_"+n;
		var new_rumus_konversi="#rumus_konversi_"+no;
		$(rumus_konversi, lastRow).attr("id", "rumus_konversi_"+no);
		$(new_rumus_konversi, lastRow).attr("name", "rumus_konversi_"+no);		
		$(new_rumus_konversi, lastRow).val('0');				
		//*****end rumus_konversi*************************************	
		
		//*****angka_faktor_konversi*************************************
		var angka_faktor_konversi="#angka_faktor_konversi_"+n;
		var new_angka_faktor_konversi="#angka_faktor_konversi_"+no;
		$(angka_faktor_konversi, lastRow).attr("id", "angka_faktor_konversi_"+no);
		$(new_angka_faktor_konversi, lastRow).attr("name", "angka_faktor_konversi_"+no);		
		$(new_angka_faktor_konversi, lastRow).val('0');				
		//*****end angka_faktor_konversi*************************************	
		
		//*****qty_konv*************************************
		var qty_konv="#qty_konv_"+n;
		var new_qty_konv="#qty_konv_"+no;
		$(qty_konv, lastRow).attr("id", "qty_konv_"+no);
		$(new_qty_konv, lastRow).attr("name", "qty_konv_"+no);		
		$(new_qty_konv, lastRow).val('0');
		//*****end qty_konv*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
		var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('');				
		//*****end harga_lama*************************************	
		
		//*****diskon*************************************
		var diskon="#diskon_"+n;
		var new_diskon="#diskon_"+no;
		$(diskon, lastRow).attr("id", "diskon_"+no);
		$(new_diskon, lastRow).attr("name", "diskon_"+no);		
		$(new_diskon, lastRow).val('0');				
		//*****end diskon*************************************	
		
		//*****pajak*************************************
		var pajak="#pajak_"+n;
		var new_pajak="#pajak_"+no;
		$(pajak, lastRow).attr("id", "pajak_"+no);
		$(new_pajak, lastRow).attr("name", "pajak_"+no);		
		$(new_pajak, lastRow).val('0');				
		//*****end pajak*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
		
		//*****aslitotal*************************************
		var aslitotal="#aslitotal_"+n;
		var new_aslitotal="#aslitotal_"+no;
		$(aslitotal, lastRow).attr("id", "aslitotal_"+no);
		$(new_aslitotal, lastRow).attr("name", "aslitotal_"+no);		
		$(new_aslitotal, lastRow).val('0');				
		//*****end aslitotal*************************************	
		
		//*****id_op_detail*************************************
		var id_op_detail="#id_op_detail_"+n;
		var new_id_op_detail="#id_op_detail_"+no;
		$(id_op_detail, lastRow).attr("id", "id_op_detail_"+no);
		$(new_id_op_detail, lastRow).attr("name", "id_op_detail_"+no);		
		$(new_id_op_detail, lastRow).val('0');				
		//*****end id_op_detail*************************************	
		
		//*****id_pp_detail*************************************
		var id_pp_detail="#id_pp_detail_"+n;
		var new_id_pp_detail="#id_pp_detail_"+no;
		$(id_pp_detail, lastRow).attr("id", "id_pp_detail_"+no);
		$(new_id_pp_detail, lastRow).attr("name", "id_pp_detail_"+no);		
		$(new_id_pp_detail, lastRow).val('0');				
		//*****end id_pp_detail*************************************	
		
		//*****cek*************************************
		var cek="#cek_"+n;
		var new_cek="#cek_"+no;
		$(cek, lastRow).attr("id", "cek_"+no);
		$(new_cek, lastRow).attr("name", "cek_"+no);		
		$(new_cek, lastRow).val('y');				
		//*****end cek*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); var id_sup= $('#id_supplier').val(); var jp= $('#jenis_potong_"+no+ "').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/makloon/cform/show_popup_brg/'+x+'/'+jp+'/"+no+"/'+id_sup);";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		  
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		var jumawal= $("#jumawal").val();	
		//alert(x + " "+ jumawal); alert(x>jumawal);
		if (parseInt(x) > parseInt(jumawal)) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
		
		var i=1;
		var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					
					var rumus=$("#rumus_konversi_"+i).val();
					var angka=$("#angka_faktor_konversi_"+i).val();
					
					/*if (rumus == '1') { var qtynya= parseFloat(qty)*parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
					else if (rumus == '2') { var qtynya= parseFloat(qty)/parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
					else if (rumus == '3') { var qtynya= parseFloat(qty)+parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
					else if (rumus == '4') { var qtynya= parseFloat(qty)-parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
					else $('#qty_konv_'+i).val(qty); */
					
					// ==== 16-10-2015 ===========
					var satuan=$("#satuan_"+i).val();
					var satuan_konv=$("#satuan_konv_"+i).val();
					
					if ((satuan == 'Meter' && satuan_konv == 'Tidak Ada') || satuan == 'Yard') {
						var qtynya= parseFloat(qty)/1.1; qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); 
					}
					else if (satuan == 'Roll') {
						var qtynya= parseFloat(qty)/60; qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); 
					}
					else {
						if (rumus == '1') { var qtynya= parseFloat(qty)*parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '2') { var qtynya= parseFloat(qty)/parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '3') { var qtynya= parseFloat(qty)+parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '4') { var qtynya= parseFloat(qty)-parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else { var qtynya = parseFloat(qty); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); }
					}
					// ===========================
						var hitung = (harga*qty)-diskon;
						hitung = hitung.toFixed(4);
						var dpp = hitung*0.98;
						new_pajak = hitung-dpp;
						new_pajak = new_pajak.toFixed(4);
						$("#pajak_"+i).val(new_pajak);
						$("#aslitotal_"+i).val(hitung);
						
						// cara 2
						//var myMoney=3543.75873;
						//var formattedMoney = myMoney.formatMoney(2,',','.');
						//alert(formattedMoney);
						var hitungformat = formatMoney(hitung, 4,',','.');
						$("#total_"+i).val(hitungformat);
						
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					
				}
				gtotal = gtotal.toFixed(4);
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(4);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal*0.98;
				dpp = dpp.toFixed(4);
				$("#dpp").val(dpp);
				
				//23-01-2013, format utk total
				var hitungformat = formatMoney(gtotal, 4,',','.');
				$("#gtotal").val(hitungformat);
				$("#asligtotal").val(gtotal);
}

function cek_pembelian() {
	var no_sj= $('#no_sj').val();
	var tgl_sj= $('#tgl_sj').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	
	if (no_sj == '') {
		alert("Nomor SJ harus diisi..!");
		return false;
	}
	if (tgl_sj == '') {
		alert("Tanggal SJ harus dipilih..!");
		return false;
	}
	if (jenis_pembelian == '0') {
		alert("Jenis pembelian harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 
	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if($('#kode_'+k).val() == '' ) {				
				alert("Data brg tidak boleh kosong...!");
				return false;
			}
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			
			if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
				alert("Data harga tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#harga_'+k).val()) ) {
				alert("Harga harus berupa angka..!");
				return false;
			}
			
			if ($('#diskon_'+k).val() == '' ) {
				alert("Diskon minimal harus diisi 0..!");
				return false;
			}
			
			if (isNaN($('#diskon_'+k).val()) ) {
				alert("Diskon harus berupa angka..!");
				return false;
			}
		} // end for
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}

function checkAll() {
	var jumdetail= $('#no').val();
	//alert (jumdetail);
	if (jumdetail > 0) {
		for (var k=1; k <= jumdetail; k++) {
				$('input[name=cek_'+k+']').attr('checked', true);
		}
	}
	
}

function uncheckAll() {
	var jumdetail= $('#no').val();
	if (jumdetail > 0) {
		for (var k=1; k <= jumdetail; k++) {
				$('input[name=cek_'+k+']').attr('checked', false);
		}
	}
	
}

// 20-07-2012
function cek_itemnya(){
	//var ceknya = $(":checkbox:checked").length;
	var xx=0;
	var jumdetail= $('#no').val();
	if (jumdetail > 0) {
		for (var k=1; k <= jumdetail; k++) {
			//	$('input[name=cek_'+k+']').attr('checked', true);
			if ($("#cek_"+k).is(":checked")) {
				xx= xx+1;
			}
		}
	}
	
	var kon=window.confirm("yakin akan menghapus item barangnya ??");
	if (kon){
		/*if(document.getElementById('d_delivery_limit').value=='') {
			alert('Tanggal batas kirim hrs dipilih !!');
			return false;
		}
		else if (ceknya == 0) {
			alert('item2 barangnya hrs dipilih !!');
			return false;
		}
		else
			return true; */
		
		//if (ceknya == 0) {
		if (xx ==0) {
			alert('item2 barangnya hrs dipilih !!');
			return false;
		}
		else
			return true;
	}else{
		return false;
	}
}

// 06-06-2015, ambil dari tirai
function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/makloon/cform/updatedatabtbbisbisan" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_pembelian" value="<?php echo $query[0]['id'] ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="csupplier" value="<?php echo $csupplier ?>">
<input type="hidden" name="tgl_awal" value="<?php echo $tgl_awal ?>">
<input type="hidden" name="tgl_akhir" value="<?php echo $tgl_akhir ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<input type="hidden" name="caribrg" value="<?php echo $caribrg ?>">
<input type="hidden" name="filterbrg" value="<?php echo $filterbrg ?>">

<div align="center">
Edit Data
<label id="status"></label>
<br>
<?php $detail_fb = $query[0]['detail_fb']; 
	$jumawal = count($query[0]['detail_fb'])+1;
	$no = 1;
	foreach ($detail_fb as $hitung) {
		$no++;
	}
?>
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">

<?php
		for($j=0;$j<count($query);$j++){
?>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<?php
	if ($query[$j]['no_op'] !='') {
	?>
	<tr>
    <td width="20%">Nomor OP</td>
    <td>
      <?php echo $query[$j]['no_op']; ?>
    </td>
  </tr>
  <?php } else { ?>
	<tr>
    <td width="20%">Tanpa acuan PP ataupun OP</td>
  </tr>
  <?php } 
		?>
		<tr>
			<td>Supplier</td>
			<td><select name="id_supplier" id="id_supplier" onkeyup="this.blur();this.focus();">
				<?php foreach ($supplier as $sup) { ?>
					<option <?php if ($query[$j]['id_supplier'] == $sup->id) { ?> selected="selected" <?php } ?> value="<?php echo $sup->id ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp; <input type="hidden" name="hide_supplier" id="hide_supplier" value="<?php echo $query[$j]['id_supplier'] ?>"></td> 
		</tr>
  <tr>
    <td>Nomor SJ</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" maxlength="20" value="<?php echo $query[$j]['no_sj'] ?>">
      <input name="no_sj_lama" type="hidden" id="no_sj_lama" value="<?php echo $query[$j]['no_sj'] ?>">
    </td>
    
  </tr>
  <tr>
    <td>Tanggal SJ</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="<?php echo $query[$j]['tgl_sj'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian" >
				<option value="0">-Pilih-</option>
				<option value="1" <?php if ($query[$j]['jenis_pembelian'] == '1') { ?> selected <?php } ?> >Cash</option>
				<option value="2" <?php if ($query[$j]['jenis_pembelian'] == '2') { ?> selected <?php } ?> >Kredit</option>
			</select>&nbsp;</td> 
  </tr>
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="13" align="left">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Jenis Potong & Ukuran</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan Awal</th>
	      <th>Qty Sat<br>Awal</th>
	      <th>Satuan Konversi</th>
	      <th>Qty Sat Konversi</th>
          <th>Harga</th>
	      <th>Diskon</th>
	      <th>PPN 2%</th>
          <th>Total</th>
          <th>Pilih Utk Hapus <!--(<input type="checkbox" name="checkall" id="checkall" onclick="if ($('input[name=checkall]').is(':checked')) { checkAll(); } else uncheckAll();"> ) --></th>
        </tr>

        <?php $i=1;
        if (count($query[$j]['detail_fb'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
			$detail_fb = $query[$j]['detail_fb'];
			for($k=0;$k<count($detail_fb);$k++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          
          <td style="white-space:nowrap;"><select name="jenis_potong_<?php echo $i ?>" id="jenis_potong_<?php echo $i ?>">
			  <option value="1" <?php if ($detail_fb[$k]['jenis_potong'] == '1') { ?>selected<?php } ?>>Potong Serong</option>
			  <option value="2" <?php if ($detail_fb[$k]['jenis_potong'] == '2') { ?>selected<?php } ?>>Potong Lurus</option>
			  <option value="3" <?php if ($detail_fb[$k]['jenis_potong'] == '3') { ?>selected<?php } ?>>Potong Spiral</option>
			  </select>
			  <select name="id_ukuran_<?php echo $i ?>" id="id_ukuran_<?php echo $i ?>">
			<?php foreach ($ukuran_bisbisan as $uk) { ?>
					<option value="<?php echo $uk->id ?>" <?php if ($uk->id == $detail_fb[$k]['id_ukuran_bisbisan']) { ?>selected<?php } ?> ><?php echo $uk->nama ?></option>
				<?php } ?>
          </select></td>
          
          <td><select name="kel_brg_<?php echo $i ?>" id="kel_brg_<?php echo $i ?>">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($detail_fb[$k]['kode_kel_brg'] == $kel->kode) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          
          <td nowrap="nowrap">
		   <input name="id_detail_<?php echo $i ?>" type="hidden" id="id_detail_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['id'] ?>" />
		   <input name="id_op_detail_<?php echo $i ?>" type="hidden" id="id_op_detail_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['id_op_detail'] ?>" />
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="10" readonly="true" 
           value="<?php echo $detail_fb[$k]['kode_brg'] ?>" />
           <input name="kode_lama_<?php echo $i ?>" type="hidden" id="kode_lama_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['kode_brg'] ?>" />
           <input name="id_brg_<?php echo $i ?>" type="hidden" id="id_brg_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['id_brg'] ?>" />
           <input name="id_brg_lama_<?php echo $i ?>" type="hidden" id="id_brg_lama_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['id_brg'] ?>" />
           
           <input <?php if ($detail_fb[$k]['id_op_detail'] !='0'){ ?> disabled <?php } ?> title="browse data barang" name="pilih_<?php echo $i ?>" value="..." id="pilih_<?php echo $i ?>" 
           onclick="javascript: var x= $('#kel_brg_<?php echo $i ?>').val(); var id_sup = $('#id_supplier').val(); var jp = $('#jenis_potong_<?php echo $i ?>').val();
           openCenteredWindow('<?php echo base_url(); ?>index.php/makloon/cform/show_popup_brg/'+ x +'/'+jp +'/<?php echo $i ?>/' + id_sup);" type="button">
           
          </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $detail_fb[$k]['nama'] ?>" /></td>
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="5" readonly="true" value="<?php echo $detail_fb[$k]['nama_satuan'] ?>" /></td>
                    
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="3" 
          style="text-align:right;" value="<?php echo $detail_fb[$k]['qty_satawal'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()" />
          <input type="hidden" name="qty_lama_<?php echo $i ?>" id="qty_lama_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['qty_satawal'] ?>" >
          <input type="hidden" name="qty_op_<?php echo $i ?>" id="qty_op_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['qty_op'] ?>" >
          <input type="hidden" name="jum_beli_<?php echo $i ?>" id="jum_beli_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['jum_beli'] ?>" >
          </td>
          
          <td><input name="satuan_konv_<?php echo $i ?>" type="text" id="satuan_konv_<?php echo $i ?>" size="5" readonly="true" value="<?php echo $detail_fb[$k]['nama_satuan_konv'] ?>" />
          <input name="rumus_konversi_<?php echo $i ?>" type="hidden" id="rumus_konversi_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['rumus_konversi'] ?>"/>
          <input name="angka_faktor_konversi_<?php echo $i ?>" type="hidden" id="angka_faktor_konversi_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['angka_faktor_konversi'] ?>"/>
          </td>
          <td><input name="qty_konv_<?php echo $i ?>" type="text" id="qty_konv_<?php echo $i ?>" size="3"  
          style="text-align:right;" value="<?php echo $detail_fb[$k]['qty_konversi'] ?>" readonly="true" /></td>
                    
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="5" maxlength="10" 
          style="text-align:right;" value="<?php echo $detail_fb[$k]['harga'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()" /> 
          <input type="hidden" name="harga_lama_<?php echo $i ?>" id="harga_lama_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['harga'] ?>" ></td>
          <td><input name="diskon_<?php echo $i ?>" type="text" id="diskon_<?php echo $i ?>" size="5" maxlength="8" 
		  style="text-align:right;" value="<?php echo $detail_fb[$k]['diskon'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()" /></td>
          <td><input style="text-align:right;" name="pajak_<?php echo $i ?>" type="text" id="pajak_<?php echo $i ?>" readonly="true" size="8" maxlength="7" value="<?php echo $detail_fb[$k]['pajak'] ?>" /></td>
          <td><input style="text-align:right;" name="total_<?php echo $i ?>" type="text" id="total_<?php echo $i ?>" readonly="true" size="10" maxlength="10" value="<?php echo $detail_fb[$k]['total'] ?>" />
          <input name="aslitotal_<?php echo $i ?>" type="hidden" id="aslitotal_<?php echo $i ?>" value="<?php echo $detail_fb[$k]['total'] ?>" />
          </td>
          
          <td>
			<input name="cek_<?php echo $i; ?>" id="cek_<?php echo $i; ?>" type="checkbox" value="y" />
		</td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1">		
		<tr>
			<td>DPP</td>
			<td>: <input style="text-align:right;" type="text" name="dpp" id="dpp" value="<?php echo $query[$j]['dpp'] ?>" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Total PPN</td>
			<td>: <input style="text-align:right;" type="text" name="tot_pajak" id="tot_pajak" value="<?php echo $query[$j]['total_pajak'] ?>" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Grand Total</td>
			<td>: <input style="text-align:right;" type="text" name="gtotal" id="gtotal" value="<?php echo $query[$j]['total'] ?>" size="10" readonly="true">
			<input type="hidden" name="asligtotal" id="asligtotal" value="<?php echo $query[$j]['total'] ?>" >
			</td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>: <input type="text" name="ket" id="ket" value="<?php echo $query[$j]['keterangan'] ?>" size="30" maxlength="30"></td>
		</tr>
	</table>
	</form>
      <div align="center"><br><br> 
        <?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "makloon/cform/viewbtbbisbisan/index/".$cur_page;
			else
				$url_redirectnya = "makloon/cform/caribtbbisbisan/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
        ?>
        
        <input type="submit" name="submit" value="Edit" onclick="return cek_pembelian();">&nbsp;
        <input type="submit" name="submit2" value="Hapus Item Yg Dipilih" onclick="return cek_itemnya();">&nbsp;
        <input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">

      </div></td>
    </tr>

</table>
<?php } ?>
</div>
</form>
