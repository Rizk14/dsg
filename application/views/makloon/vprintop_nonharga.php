<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
   
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .isinya2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;}

</style>

<style type="text/css" media="print">
    .printable{ 
      page-break-after: always;
    }
    .no_print{
      display: none;
    }
</style>
<!-- media="screen" means these styles will only be used by screen 
  devices (e.g. monitors) -->
<style type="text/css" media="screen">
    .printable{
      display: none;
    }
</style>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">

$(function()
{
	var go_print= jQuery('#go_print').val();
	
	if (go_print == '1')
		window.print();
});


</script>

<?php
	//$tgl = date("d-m-Y");
	// 05-08-2015 ganti jadi tgl OP
	$tgl = $query[0]['tgl_op'];
	
	$pisah1 = explode("-", $tgl);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
?>
<input type="hidden" name="go_print" id="go_print" value="1" >
<table class="isinya2" border='1' align="center" width="100%">
	<tr>
	<td>
	<table class="isinya" border='0' align="center" width="90%">
		<tr>
			<td colspan="3"> <b><?php echo $datasetting['nama'] ?></b> </td>
			<td>&nbsp;</td>
			<td align="right"><?php echo $datasetting['kota'] ?>, <?php echo $tgl; ?></td>
		</tr>
		<tr>
			<td colspan="3"><?php echo $datasetting['alamat'] ?></td>
			<td>Kepada Yth.</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3"><?php echo $datasetting['kota'] ?></td>
			<td><?php echo $query[0]['nama_supplier'] ?> (U/p. <?php echo $query[0]['kontak_person'] ?>)</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center"><b>ORDER PEMBELIAN (Gudang)</b></td>

		</tr>
		<tr>
			<td colspan="3">Dengan hormat,</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="right">Jenis Pembelian: <?php if ($query[0]['jenis_pembelian'] == '1') echo "Cash"; else echo "Kredit"; ?></td>
		</tr>
		<tr>
			<td colspan="3">mohon segera dikirim barang ini:</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="right">Nomor OP: <?php echo $query[0]['no_op'] ?></td>
		</tr>
		<tr>
			<td colspan="5">
				<table border='1' class="isinya2" width="100%" >
					<tr>
				  <th width="5%">No</th>
				  <th width="40%">Nama Barang</th>
				  <th width="5%">Satuan</th>
				  <th width="5%">Qty</th>
				   <th width="10%">Keterangan</th>
				</tr>

			<?php $i=1;
			$halaman = 1; 
			if (count($query[0]['detail_op'])==0) {
			?>
			<tr align="center">
			  <td align="center" id="num_1">1</td>
			  <td nowrap="nowrap">
			   Data tidak ada</td>
			  
			</tr>
			
			<?php
			} else {
				$detailnya = $query[0]['detail_op'];
				for($j=0;$j<count($query[0]['detail_op']);$j++){
				?>
				<tr>
			  <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
			  <td style="white-space:nowrap;"><?php if ($detailnya[$j]['nama_brg_sup'] == '') echo $detailnya[$j]['nama']; else echo $detailnya[$j]['nama_brg_sup']; ?></td>
			  <td style="white-space:nowrap;"><?php echo $detailnya[$j]['satuan']; ?></td>
			  <td style="white-space:nowrap;" align="right"><?php echo number_format($detailnya[$j]['qty'],0,',','.') ?></td>
			  <td nowrap><?php if ($detailnya[$j]['keterangan'] != '') echo $detailnya[$j]['keterangan']; else echo "&nbsp;"; ?></td>
			</tr>
			
			<?php 
			if ($i%30==0) {
			?>
				<tr>
					<td align="right" colspan="9"><?php echo "Hal. ".$halaman; $halaman++; ?></td>
				</tr>
			</table>
			</td></tr></table></td></tr></table>
			
			<br class="printable">	
			<table class="isinya2" border='1' align="center" width="100%">
	<tr>
	<td>
	<table class="isinya" border='0' align="center" width="90%">
		
		<tr>
			<td colspan="5">
				<table border='1' class="isinya2" width="100%" >
					<tr>
				  <th width="5%">No</th>
				  <th width="40%">Nama Barang</th>
				  <th width="5%">Satuan</th>
				  <th width="5%">Qty</th>
				   <th width="10%">Keterangan</th>
				</tr>
			
			<?php
					
				}
				
				$i++; } // end foreach 
			}
			?>
				<tr>
					<td align="right" colspan="9"><?php echo "Hal. ".$halaman; ?></td>
				</tr>
				</table>
			</td>
			</tr>
		</td>
		</tr>
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>


		<tr>
			<td colspan="2" align="center">Hormat Kami,</td>
			<td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">Dibuat<br>Bag. Pembelian</td>
			<td colspan="2" align="center">Bag. Keuangan</td>
			<td colspan="2" align="center">Mengetahui, <br>Kepala Bagian</td>
		</tr>
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		

		<tr>
			<td colspan="2" align="center"><u><?php echo $datasetting['bag_pembelian'] ?></u></td>
			<td colspan="2" align="center"><u><?php echo $datasetting['bag_keuangan'] ?></u></td>
			<td colspan="2" align="center"><u><?php echo $datasetting['kepala_bagian'] ?></u></td>
		</tr>
		</table>
	</td>
	</tr>
</table>
<!--<i><div class="isinya" align="right">Tanggal Cetak: <?php //echo date("d-m-Y H:i:s"); ?></i></div>-->
