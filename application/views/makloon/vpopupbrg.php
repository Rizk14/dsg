<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var id_brg=$("#id_brg").val();
		var des=$("#des").val();
		var satuan=$("#satuan").val();
		var satuan_konv=$("#satuan_konv").val();
		var rumus_konversi=$("#rumus_konversi").val();
		var angka_faktor_konversi=$("#angka_faktor_konversi").val();
		var harga=$("#harga").val();
		var jumdata=$("#jumdata").val();
		
		// 24-08-2015 dikomen. karena item brg boleh sama dgn jenis potong yg berbeda
		/*for(var i=1; i <= jumdata; i++){
			var stringnya = opener.document.getElementById('id_brg_'+i).value;
			if (id_brg == stringnya) {
				alert("Kode barang sudah pernah dipilih...!");
				return false;
			}
		} */
		
		opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
		opener.document.forms["f_purchase"].id_brg_<?php echo $posisi ?>.value=id_brg;
		opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=des;
		opener.document.forms["f_purchase"].satuan_<?php echo $posisi ?>.value=satuan;
		opener.document.forms["f_purchase"].satuan_konv_<?php echo $posisi ?>.value=satuan_konv;
		opener.document.forms["f_purchase"].rumus_konversi_<?php echo $posisi ?>.value=rumus_konversi;
		opener.document.forms["f_purchase"].angka_faktor_konversi_<?php echo $posisi ?>.value=angka_faktor_konversi;
		opener.document.forms["f_purchase"].harga_<?php echo $posisi ?>.value=harga;
		opener.document.forms["f_purchase"].harga_lama_<?php echo $posisi ?>.value=harga;
		opener.document.forms["f_purchase"].qty_<?php echo $posisi ?>.focus();
		self.close();
	});
});
</script>
<center><h3>Daftar Barang Untuk Kelompok <?php echo $nama_kel ?></h3></center>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="id_brg" id="id_brg">
<input type="hidden" name="des" id="des">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="satuan_konv" id="satuan_konv">
<input type="hidden" name="rumus_konversi" id="rumus_konversi">
<input type="hidden" name="angka_faktor_konversi" id="angka_faktor_konversi">
<input type="hidden" name="harga" id="harga">
<input type="hidden" name="jumdata" id="jumdata" value="<?php echo $jumdata ?>">
</form>
<div align="center"><br>
<?php echo form_open('makloon/cform/show_popup_brg'); ?>
Jenis Barang <select name="id_jenis_brg" id="id_jenis_brg">
				<option value="0" <?php if ($cjenis_brg == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($jenis_brg as $jns) { ?>
					<option value="<?php echo $jns->id ?>" <?php if ($cjenis_brg == $jns->id) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nama_kel_brg."] ". $jns->kode." - ".$jns->nama ?></option>
				<?php } ?>
				</select> &nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="kel_brg" value="<?php echo $kel_brg ?>">
<input type="hidden" name="jenis_potong" value="<?php echo $jenis_potong ?>">
<input type="hidden" name="id_supplier" value="<?php echo $id_supplier ?>">
<?php echo form_close(); ?>
  <table border="1" width="100%" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan Awal</th>
      <th bgcolor="#999999">Satuan Konversi</th>
      <th bgcolor="#999999">Action</th>
    </tr>
	<?php 
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
		foreach($query as $brg) {
	
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $brg->kode_brg; ?></td>
      <td nowrap><?php echo $brg->nama_brg; ?></td>
      <td><?php echo $brg->nama_satuan; ?></td>
      <td align="center">
      <?php // cek harga bisbisan
			$query3	= $this->db->query(" SELECT harga FROM tm_harga_bisbisan WHERE jenis_potong = '$jenis_potong' 
				AND id_supplier = '$id_supplier' ");
				if ($query3->num_rows() > 0) {
					$hasilrow = $query3->row();
					$harga = $hasilrow->harga;
				}
				else
					$harga = 0;
					
			// cek satuan konversi
			$query3	= $this->db->query(" SELECT nama as nama_satuan_konv FROM tm_satuan WHERE id = '$brg->id_satuan_konversi' ");
				if ($query3->num_rows() > 0) {
					$hasilrow = $query3->row();
					$nama_satuan_konv = $hasilrow->nama_satuan_konv;
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				echo $nama_satuan_konv;
      ?>
      </td>
      <td>
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $brg->kode_brg ?>';
	  window.document.f_master_brg.id_brg.value='<?php echo $brg->id ?>';
	  window.document.f_master_brg.des.value='<?php 
	  //echo str_replace("'", "\'", $brg->nama_brg) 
	  $pos = strpos($brg->nama_brg, "\"");
	  if ($pos > 0)
		echo str_replace("\"", "&quot;", $brg->nama_brg);
	  else
		echo str_replace("'", "\'", $brg->nama_brg);
	  ?>';
	  
	  window.document.f_master_brg.satuan.value='<?php echo $brg->nama_satuan ?>'; 
	  window.document.f_master_brg.satuan_konv.value='<?php echo $nama_satuan_konv ?>';
	  window.document.f_master_brg.rumus_konversi.value='<?php echo $brg->rumus_konversi ?>'; 
	  window.document.f_master_brg.angka_faktor_konversi.value='<?php echo $brg->angka_faktor_konversi ?>';  
	  window.document.f_master_brg.harga.value='<?php echo $harga ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } ?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
