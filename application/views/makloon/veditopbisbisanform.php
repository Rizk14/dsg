<h3>Data Order Pembelian (OP) Makloon Bisbisan</h3><br>
<a href="<? echo base_url(); ?>index.php/makloon/cform/addopbisbisan">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/makloon/cform/viewopbisbisan">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_op() {
	var no_op= $('#no_op').val();
	var tgl_op= $('#tgl_op').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (no_op == '') {
			alert("Nomor OP harus diisi..!");
			return false;
		}
		if (tgl_op == '') {
			alert("Tanggal OP harus dipilih..!");
			return false;
		}
		if (jenis_pembelian == '0') {
			alert("Jenis pembelian harus dipilih..!");
			return false;
		}
		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka..!");
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
	}
	else
		return false;
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_op" id="f_op" action="<?php echo base_url(); ?>index.php/makloon/cform/submitopbisbisan" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_op_makloon" id="id_op_makloon" value="<?php echo $query[0]['id'] ?>">

<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="csupplier" value="<?php echo $csupplier ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<?php 
	if (is_array($query[0]['detail_op'])) {
		$jumawal = count($query[0]['detail_op'])+1;
		
		if (count($query[0]['detail_op'])>0) {
			$no=1;
			foreach ($query[0]['detail_op'] as $hitung) {
				$no++;
			}
		}
		else $no=2;
	}
	else {
		$no=2;
		$jumawal = 2;
	}
	
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>
<?php
	$pisah1 = explode("-", $query[0]['tgl_op']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tgl_op = $tgl1."-".$bln1."-".$thn1;
?>
<center>Edit Data</center>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
    <td width="15%">Nomor Bon M Keluar</td>
    <td width="70%">
      <?php echo $query[0]['no_bonmkeluar'] ?>
    </td>
  </tr>
	<tr>
			<td>Supplier</td>
			<td><select name="id_supplier" id="id_supplier" >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($sup->id == $query[0]['id_supplier']) { ?> selected <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;
			<input type="hidden" name="id_supplier_lama" value="<?php echo $query[0]['id_supplier'] ?>">	
			</td> 
		</tr>

  <tr>
    <td>No OP</td>
    <td>
      <input name="no_op" type="text" id="no_op" size="20" maxlength="20" readonly="true" value="<?php echo $query[0]['no_op']; ?>">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tgl OP</td>
    <td>
	<label>
      <input name="tgl_op" type="text" id="tgl_op" size="10" value="<?php echo $tgl_op ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_op" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_op,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian" >
				<option value="0">-Pilih-</option>
				<option value="1" <?php if ($query[0]['jenis_pembelian'] == '1') { ?> selected <?php } ?> >Cash</option>
				<option value="2" <?php if ($query[0]['jenis_pembelian'] == '2') { ?> selected <?php } ?> >Kredit</option>
			</select>&nbsp;</td> 
  </tr>
  <tr>
			<td>Keterangan</td>
			<td><input type="text" name="ket" id="ket" value="<?php echo $query[0]['keterangan'] ?>" size="30" maxlength="30"></td>
		</tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
           <th width="20">No</th>
           <th>No/Tgl Bon M Keluar</th>
           <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan<br>Konversi</th>
	       <th>Qty Satuan<br>Konversi</th>
	       <th>Satuan<br>Awal</th>
	       <th>Qty Satuan<br>Awal</th>
	       <th>Jenis Potong & Ukuran</th>
          <th>Keterangan</th>
        </tr>

        <?php $i=1;
        if (count($query[0]['detail_op'])==0 || !is_array($query[0]['detail_op']) ) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap" colspan="8">
		   Data tidak ada, atau data item brg sudah dalam proses SJ penerimaan brg</td>
          
        </tr>
		
		<?php
		} else {
			$detailnya = $query[0]['detail_op'];
			for($j=0;$j<count($query[0]['detail_op']);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td style="white-space:nowrap;">
           <input name="no_bonmkeluar_<?php echo $i ?>" type="text" id="no_bonmkeluar_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $detailnya[$j]['no_bonmkeluar']." / ".$detailnya[$j]['tgl_bonm'] ?>"/>
           </td>
          <td style="white-space:nowrap;">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $detailnya[$j]['kode_brg'] ?>"/>
           <input type="hidden" name="id_brg_<?php echo $i ?>" value="<?php echo $detailnya[$j]['id_brg'] ?>" >
           <input type="hidden" name="id_op_detail_<?php echo $i ?>" value="<?php echo $detailnya[$j]['id'] ?>" >
           <input type="hidden" name="id_bonmkeluar_detail_<?php echo $i ?>" value="<?php echo $detailnya[$j]['id_bonmkeluar_detail'] ?>" >
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="40" readonly="true" value="<?php echo $detailnya[$j]['nama'] ?>" /></td>
          
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="5" readonly="true"
          value="<?php echo $detailnya[$j]['nama_satuan_konv'] ?>" /></td>
          
          <td><input type="hidden" name="rumus_konversi_<?php echo $i ?>" id="rumus_konversi_<?php echo $i ?>" value="<?php echo $detailnya[$j]['rumus_konversi'] ?>">
          <input type="hidden" name="angka_faktor_konversi_<?php echo $i ?>" id="angka_faktor_konversi_<?php echo $i ?>" value="<?php echo $detailnya[$j]['angka_faktor_konversi'] ?>">
		  <input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="3" maxlength="10" 
          style="text-align:right;" value="<?php echo $detailnya[$j]['qty'] ?>" onkeyup="javascript: var rumus= $('#rumus_konversi_<?php echo $i ?>').val(); var angka= $('#angka_faktor_konversi_<?php echo $i ?>').val(); 
          if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal_<?php echo $i ?>').val(qtynya); } 
          else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal_<?php echo $i ?>').val(qtynya); }
          else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal_<?php echo $i ?>').val(qtynya); }
          else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_satawal_<?php echo $i ?>').val(qtynya); }
          else { $('#qty_satawal_<?php echo $i ?>').val(this.value); }" />
          <input name="qty_lama_<?php echo $i ?>" type="hidden" id="qty_lama_<?php echo $i ?>" value="<?php echo $detailnya[$j]['qty'] ?>" />
          </td>
          
          <td><input name="satuan_awal_<?php echo $i ?>" type="text" id="satuan_awal_<?php echo $i ?>" size="5" readonly="true"
          value="<?php echo $detailnya[$j]['satuan'] ?>" /></td>
          <td><input readonly="true" name="qty_satawal_<?php echo $i ?>" type="text" id="qty_satawal_<?php echo $i ?>" size="3" maxlength="10" 
          style="text-align:right;" value="<?php echo $detailnya[$j]['qty_satawal'] ?>" /></td>
          
          <td style="white-space:nowrap;"><select name="jenis_potong_<?php echo $i ?>" id="jenis_potong_<?php echo $i ?>">
			<option value="1" <?php if ($detailnya[$j]['jenis_potong'] == '1') { ?>selected<?php } ?>>Potong Serong</option>
			<option value="2" <?php if ($detailnya[$j]['jenis_potong'] == '2') { ?>selected<?php } ?>>Potong Lurus</option>
			<option value="3" <?php if ($detailnya[$j]['jenis_potong'] == '3') { ?>selected<?php } ?>>Potong Spiral</option>
          </select>&nbsp;
		<select name="id_ukuran_<?php echo $i ?>" id="id_ukuran_<?php echo $i ?>">
			<?php foreach ($ukuran_bisbisan as $uk) { ?>
					<option value="<?php echo $uk->id ?>" <?php if ($uk->id == $detailnya[$j]['id_ukuran_bisbisan']) { ?>selected<?php } ?> ><?php echo $uk->nama ?></option>
				<?php } ?>
          </select>
          </td>
          
          <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="20" value="<?php echo $detailnya[$j]['keterangan'] ?>" /></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
	</table>	
	
	</form>
      <div align="center"><br> 
      <?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "makloon/cform/viewopbisbisan/index/".$cur_page;
			else
				$url_redirectnya = "makloon/cform/cariopbisbisan/".$csupplier."/".$carinya."/".$cur_page;
        ?>
        <?php
			 if (is_array($query[0]['detail_op']) ) {
        ?>
        <input type="submit" name="submit2" value="Edit" onclick="return cek_op();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
		<?php
			}
			else {
		?>
		<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
		<?php
			}
		?>
      </div></td>
    </tr>

</table>
</div>
</form>
