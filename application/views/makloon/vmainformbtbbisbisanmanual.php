<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<h3>Data Bukti Terima Barang (BTB) Makloon Bisbisan</h3><br>
<a href="<?php echo base_url(); ?>index.php/makloon/cform/addbtbbisbisan">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/makloon/cform/viewbtbbisbisan">View Data</a><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tgl_sj = $("#tgl_sj").val();
	var no_sj = $("#no_sj").val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
	
		if (no_sj == '') {
			alert ("Nomor SJ harus diisi");
			$("#no_sj").focus();
			return false;
		}
		if (tgl_sj == '') {
			alert ("Tgl SJ harus dipilih");
			$("#no_sj").focus();
			return false;
		}
		
		if (jenis_pembelian == '0') {
			alert("Jenis pembelian harus dipilih..!");
			return false;
		}
				
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				if ($('#kode_'+k).val() == '') {
					alert("Data item barang tidak boleh ada yang kosong...!");
					return false;
				}
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka atau desimal..!");
					return false;
				}
				
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Data harga harus berupa angka atau desimal..!");
					return false;
				}
				
				if ($('#diskon_'+k).val() == '' ) {
					alert("Diskon minimal harus diisi 0..!");
					return false;
				}
				
				if (isNaN($('#diskon_'+k).val()) ) {
					alert("Diskon harus berupa angka..!");
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
	}
	else
		return false;
	
}

//tambah
$(function()
{
	$("#no").val('2');
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****jenis_potong*************************************
		var jenis_potong="#jenis_potong_"+n;
		var new_jenis_potong="#jenis_potong_"+no;
		$(jenis_potong, lastRow).attr("id", "jenis_potong_"+no);
		$(new_jenis_potong, lastRow).attr("name", "jenis_potong_"+no);		
		//*****end jenis_potong*********************************
		
		//*****id_ukuran*************************************
		var id_ukuran="#id_ukuran_"+n;
		var new_id_ukuran="#id_ukuran_"+no;
		$(id_ukuran, lastRow).attr("id", "id_ukuran_"+no);
		$(new_id_ukuran, lastRow).attr("name", "id_ukuran_"+no);		
		//*****end id_ukuran*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('0');				
		//*****end qty*************************************	
		
		//*****satuan_konv*************************************
		var satuan_konv="#satuan_konv_"+n;
		var new_satuan_konv="#satuan_konv_"+no;
		$(satuan_konv, lastRow).attr("id", "satuan_konv_"+no);
		$(new_satuan_konv, lastRow).attr("name", "satuan_konv_"+no);		
		$(new_satuan_konv, lastRow).val('');				
		//*****end satuan_konv*************************************	
		
		//*****rumus_konversi*************************************
		var rumus_konversi="#rumus_konversi_"+n;
		var new_rumus_konversi="#rumus_konversi_"+no;
		$(rumus_konversi, lastRow).attr("id", "rumus_konversi_"+no);
		$(new_rumus_konversi, lastRow).attr("name", "rumus_konversi_"+no);		
		$(new_rumus_konversi, lastRow).val('0');				
		//*****end rumus_konversi*************************************	
		
		//*****angka_faktor_konversi*************************************
		var angka_faktor_konversi="#angka_faktor_konversi_"+n;
		var new_angka_faktor_konversi="#angka_faktor_konversi_"+no;
		$(angka_faktor_konversi, lastRow).attr("id", "angka_faktor_konversi_"+no);
		$(new_angka_faktor_konversi, lastRow).attr("name", "angka_faktor_konversi_"+no);		
		$(new_angka_faktor_konversi, lastRow).val('0');				
		//*****end angka_faktor_konversi*************************************	
		
		//*****qty_konv*************************************
		var qty_konv="#qty_konv_"+n;
		var new_qty_konv="#qty_konv_"+no;
		$(qty_konv, lastRow).attr("id", "qty_konv_"+no);
		$(new_qty_konv, lastRow).attr("name", "qty_konv_"+no);		
		$(new_qty_konv, lastRow).val('0');
		//*****end qty_konv*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('0');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
		var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('0');				
		//*****end harga_lama*************************************	
		
		//*****diskon*************************************
		var diskon="#diskon_"+n;
		var new_diskon="#diskon_"+no;
		$(diskon, lastRow).attr("id", "diskon_"+no);
		$(new_diskon, lastRow).attr("name", "diskon_"+no);		
		$(new_diskon, lastRow).val('0');				
		//*****end diskon*************************************	
		
		//*****pajak*************************************
		var pajak="#pajak_"+n;
		var new_pajak="#pajak_"+no;
		$(pajak, lastRow).attr("id", "pajak_"+no);
		$(new_pajak, lastRow).attr("name", "pajak_"+no);		
		$(new_pajak, lastRow).val('0');				
		//*****end pajak*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
		
		//*****aslitotal*************************************
		var aslitotal="#aslitotal_"+n;
		var new_aslitotal="#aslitotal_"+no;
		$(aslitotal, lastRow).attr("id", "aslitotal_"+no);
		$(new_aslitotal, lastRow).attr("name", "aslitotal_"+no);		
		$(new_aslitotal, lastRow).val('0');				
		//*****end aslitotal*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); var id_sup= $('#id_supplier').val(); var jp=$('#jenis_potong_"+no+ "').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/makloon/cform/show_popup_brg/'+x+'/'+jp+'/"+no+"/'+id_sup);";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
			  	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

// 06-06-2015, ambil dari tirai
function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
		
		var i=1;
		var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					
					var rumus=$("#rumus_konversi_"+i).val();
					var angka=$("#angka_faktor_konversi_"+i).val();
					
					var satuan=$("#satuan_"+i).val();
					var satuan_konv=$("#satuan_konv_"+i).val();
					
					if ((satuan == 'Meter' && satuan_konv == 'Tidak Ada') || satuan == 'Yard') {
						var qtynya= parseFloat(qty)/1.1; qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); 
					}
					else if (satuan == 'Roll') {
						var qtynya= parseFloat(qty)/60; qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); 
					}
					else {
						if (rumus == '1') { var qtynya= parseFloat(qty)*parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '2') { var qtynya= parseFloat(qty)/parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '3') { var qtynya= parseFloat(qty)+parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '4') { var qtynya= parseFloat(qty)-parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else { var qtynya = parseFloat(qty); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); }
					}
						var hitung = (harga*qty)-diskon;
						hitung = hitung.toFixed(4);
						var dpp = hitung*0.98;
						new_pajak = hitung-dpp;
						new_pajak = new_pajak.toFixed(4);
						$("#pajak_"+i).val(new_pajak);
						$("#aslitotal_"+i).val(hitung);
						
						// cara 2
						//var myMoney=3543.75873;
						//var formattedMoney = myMoney.formatMoney(2,',','.');
						//alert(formattedMoney);
						var hitungformat = formatMoney(hitung, 4,',','.');
						$("#total_"+i).val(hitungformat);
						
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					
				}
				gtotal = gtotal.toFixed(4);
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(4);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal*0.98;
				dpp = dpp.toFixed(4);
				$("#dpp").val(dpp);
				
				//23-01-2013, format utk total
				var hitungformat = formatMoney(gtotal, 4,',','.');
				$("#gtotal").val(hitungformat);
				$("#asligtotal").val(gtotal);
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/makloon/cform/submitbtbbisbisan" method="post" enctype="multipart/form-data">
<input type="hidden" name="is_no_op" id="is_no_op" value="<?php echo $is_no_op ?>">
<input type="hidden" name="id_sup" value="<?php echo $id_supplier ?>">
<input type="hidden" name="no" id="no" >

<div align="center">

<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
		<td width="20%">Tanpa acuan OP Makloon</td>
		<td>
		  
    </td>
  </tr>
		<tr>
			<td>Supplier</td>
			<td><select name="id_supplier" id="id_supplier" onkeyup="this.blur();this.focus();" disabled >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($sup->id == $id_supplier) { ?> selected="true" <?php } ?>><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr>
  <tr>
    <td>Nomor SJ</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" maxlength="20" value="">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tanggal SJ</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  
  <tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian" >
				<option value="0">-Pilih-</option>
				<option value="1">Cash</option>
				<option value="2">Kredit</option>
			</select>&nbsp;</td> 
  </tr>
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="10" align="left"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Jenis Potong & Ukuran</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan Awal</th>
	      <th>Qty Sat Yard</th>
	      <th>Satuan Konversi</th>
	      <th>Qty Sat Konversi</th>
          <th>Harga</th>
	      <th>Diskon</th>
	      <th>PPN 2%</th>
          <th>Total</th>
        </tr>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          <td style="white-space:nowrap;"><select name="jenis_potong_1" id="jenis_potong_1">
			  <option value="1">Potong Serong</option>
			  <option value="2">Potong Lurus</option>
			  <option value="3">Potong Spiral</option>
			  </select>
			  <select name="id_ukuran_1" id="id_ukuran_1">
			<?php foreach ($ukuran_bisbisan as $uk) { ?>
					<option value="<?php echo $uk->id ?>" ><?php echo $uk->nama ?></option>
				<?php } ?>
          </select></td>
          <td><select name="kel_brg_1" id="kel_brg_1">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          <td style="white-space:nowrap;">
           <input name="kode_1" type="text" id="kode_1" size="10" readonly="true"/>
           <input name="id_brg_1" type="hidden" id="id_brg_1" value=""/>
           <input title="browse data barang" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#kel_brg_1').val(); var id_sup = $('#id_supplier').val(); var jp = $('#jenis_potong_1').val();
           openCenteredWindow('<?php echo base_url(); ?>index.php/makloon/cform/show_popup_brg/'+ x +'/'+jp+'/1/' + id_sup);" ></td>
         <td><input name="nama_1" type="text" id="nama_1" size="25" readonly="true"/></td>
         <td><input name="satuan_1" type="text" id="satuan_1" size="5" readonly="true"/></td>
         <td><input style="text-align:right;" name="qty_1" type="text" id="qty_1" size="3" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()" /></td>
         
         <td><input name="satuan_konv_1" type="text" id="satuan_konv_1" size="5" readonly="true"/>
         <input name="rumus_konversi_1" type="hidden" id="rumus_konversi_1" value="0"/>
         <input name="angka_faktor_konversi_1" type="hidden" id="angka_faktor_konversi_1" value="0"/>
         </td>
         <td><input style="text-align:right;" name="qty_konv_1" type="text" id="qty_konv_1" size="3" value="0" /></td>
         
         <td><input style="text-align:right;" name="harga_1" type="text" id="harga_1" size="5" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()" />
			<input name="harga_lama_1" type="hidden" id="harga_lama_1" value="0">  
         </td>
         <td><input style="text-align:right;" name="diskon_1" type="text" id="diskon_1" size="5" onkeyup="hitungnilai()" onblur="hitungnilai()" value="0" /></td>
         <td><input style="text-align:right;" name="pajak_1" type="text" id="pajak_1" size="7" value="0" readonly="true" /></td>
         <td><input style="text-align:right;" name="total_1" type="text" id="total_1" size="10" value="0" readonly="true" />
         <input type="hidden" name="aslitotal_1" id="aslitotal_1" value="0" >
         </td>
        </tr>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td>DPP</td>
			<td>: <input style="text-align:right;" type="text" name="dpp" id="dpp" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Total PPN 2%</td>
			<td>: <input style="text-align:right;" type="text" name="tot_pajak" id="tot_pajak" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Grand Total</td>
			<td>: <input style="text-align:right;" type="text" name="gtotal" id="gtotal" value="0" size="10" readonly="true">
			<input type="hidden" name="asligtotal" id="asligtotal" value="0" >
			</td>
		</tr>
		<input type="hidden" name="uang_muka" id="uang_muka" value="0">
		<input type="hidden" name="sisa_hutang" id="sisa_hutang" value="0">
		<tr>
			<td>Keterangan</td>
			<td>: <input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
	</table>
	</form>
      <div align="center"><br><br> 
        
        <input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/makloon/cform/addbtbbisbisan'">
      </div></td>
    </tr>

</table>
</div>
</form>
