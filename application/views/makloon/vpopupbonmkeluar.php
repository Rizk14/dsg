<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    .judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$("#pilih").click(function()
	{
		opener.document.forms["f_op"].id_brg.value = '';
		opener.document.forms["f_op"].no_bonmkeluar.value='';

		if (document.f_master_brg.brg.length > 0) {
			var no_bonmkeluarnya = "";
			for(var i=0; i < document.f_master_brg.brg.length; i++){
				if(document.f_master_brg.brg[i].checked) {
					opener.document.forms["f_op"].id_brg.value+= document.f_master_brg.brg[i].value + ";";
					
					if (no_bonmkeluarnya != document.f_master_brg.no_bonmkeluar[i].value) {
						opener.document.forms["f_op"].no_bonmkeluar.value+= document.f_master_brg.no_bonmkeluar[i].value + ";";
					}
					no_bonmkeluarnya = document.f_master_brg.no_bonmkeluar[i].value;
				}
			}
		}
		else {
			opener.document.forms["f_op"].id_brg.value = document.f_master_brg.brg.value+";";
			opener.document.forms["f_op"].no_bonmkeluar.value= document.f_master_brg.no_bonmkeluar.value + ";";
		} 
		self.close();
	});
});
</script>

<center><h3>Daftar Barang di Bon M Keluar Untuk Makloon</h3></center>
<div align="center"><br>
<?php echo form_open('makloon/cform/show_popup_bonmkeluar'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<form id="f_master_brg" name="f_master_brg">

	<table border="1" align="center" cellpadding="1" cellspacing="2" width="100%" bordercolor="#666666" nowrap="nowrap">
	<thead>
	 <tr class="judulnya">
		 <th>Nomor Bon M<br>Manual</th>
		 <th>Tanggal Bon M</th>
		 <th>Gudang</th>
		 <th>List Barang</th>
		 <th>Last Update</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				 $pisah1 = explode("-", $query[$j]['tgl_bonm']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_bonm = $tgl1." ".$nama_bln." ".$thn1; 
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_bonm']."</td>";
				 echo    "<td>".$tgl_bonm."</td>";
				 echo    "<td>".$query[$j]['kode_gudang']." - ".$query[$j]['nama_gudang']."</td>";
				 echo "<td style='white-space:nowrap;'>";
				 
				 if (is_array($query[$j]['detail_bonmkeluar'])) {
					 $var_detail = $query[$j]['detail_bonmkeluar'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (".number_format($var_detail[$k]['qty'], 4)." ".$var_detail[$k]['satuan'].")";
						  echo " <input type='checkbox' name='brg' id='brg' value='".$var_detail[$k]['id']."' >";
						  echo " &nbsp; <input type='hidden' name='no_bonmkeluar' id='no_bonmkeluar' value='".$var_detail[$k]['no_bonmkeluar']."' >";
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 echo    "<td>".$tgl_update."</td>"; ?>
				<?php echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table>
</form>
  <? //echo $this->pagination->create_links();?>
  <input type="button" name="pilih" id="pilih" value="Pilih" >
</div>
