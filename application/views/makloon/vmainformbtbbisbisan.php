<h3>Data Bukti Terima Barang (BTB) Makloon Bisbisan</h3><br>
<a href="<?php echo base_url(); ?>index.php/makloon/cform/addbtbbisbisan">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/makloon/cform/viewbtbbisbisan">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{		
	hitungnilai();
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****jenis_potong*************************************
		var jenis_potong="#jenis_potong_"+n;
		var new_jenis_potong="#jenis_potong_"+no;
		$(jenis_potong, lastRow).attr("id", "jenis_potong_"+no);
		$(new_jenis_potong, lastRow).attr("name", "jenis_potong_"+no);		
		//*****end jenis_potong*********************************
		
		//*****id_ukuran*************************************
		var id_ukuran="#id_ukuran_"+n;
		var new_id_ukuran="#id_ukuran_"+no;
		$(id_ukuran, lastRow).attr("id", "id_ukuran_"+no);
		$(new_id_ukuran, lastRow).attr("name", "id_ukuran_"+no);		
		//*****end id_ukuran*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		$(new_kel, lastRow).attr("disabled",false);
		//*****end kel*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****id_op*************************************
		var id_op="#id_op_"+n;
		var new_id_op="#id_op_"+no;
		$(id_op, lastRow).attr("id", "id_op_"+no);
		$(new_id_op, lastRow).attr("name", "id_op_"+no);		
		$(new_id_op, lastRow).val('0');				
		//*****end id_op*************************************	
		
		//*****id_op_detail*************************************
		var id_op_detail="#id_op_detail_"+n;
		var new_id_op_detail="#id_op_detail_"+no;
		$(id_op_detail, lastRow).attr("id", "id_op_detail_"+no);
		$(new_id_op_detail, lastRow).attr("name", "id_op_detail_"+no);		
		$(new_id_op_detail, lastRow).val('0');				
		//*****end id_op_detail*************************************	
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
				
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****qty_lama*************************************
		var qty_lama="#qty_lama_"+n;
		var new_qty_lama="#qty_lama_"+no;
		$(qty_lama, lastRow).attr("id", "qty_lama_"+no);
		$(new_qty_lama, lastRow).attr("name", "qty_lama_"+no);		
		$(new_qty_lama, lastRow).val('0');				
		//*****end qty_lama*************************************	
		
		//*****qty_op*************************************
		var qty_op="#qty_op_"+n;
		var new_qty_op="#qty_op_"+no;
		$(qty_op, lastRow).attr("id", "qty_op_"+no);
		$(new_qty_op, lastRow).attr("name", "qty_op_"+no);		
		$(new_qty_op, lastRow).val('0');				
		//*****end qty_op*************************************	
		
		//*****jum_beli*************************************
		var jum_beli="#jum_beli_"+n;
		var new_jum_beli="#jum_beli_"+no;
		$(jum_beli, lastRow).attr("id", "jum_beli_"+no);
		$(new_jum_beli, lastRow).attr("name", "jum_beli_"+no);		
		$(new_jum_beli, lastRow).val('0');				
		//*****end jum_beli*************************************	
		
		//*****satuan_konv*************************************
		var satuan_konv="#satuan_konv_"+n;
		var new_satuan_konv="#satuan_konv_"+no;
		$(satuan_konv, lastRow).attr("id", "satuan_konv_"+no);
		$(new_satuan_konv, lastRow).attr("name", "satuan_konv_"+no);		
		$(new_satuan_konv, lastRow).val('');				
		//*****end satuan_konv*************************************	
		
		//*****rumus_konversi*************************************
		var rumus_konversi="#rumus_konversi_"+n;
		var new_rumus_konversi="#rumus_konversi_"+no;
		$(rumus_konversi, lastRow).attr("id", "rumus_konversi_"+no);
		$(new_rumus_konversi, lastRow).attr("name", "rumus_konversi_"+no);		
		$(new_rumus_konversi, lastRow).val('0');				
		//*****end rumus_konversi*************************************	
		
		//*****angka_faktor_konversi*************************************
		var angka_faktor_konversi="#angka_faktor_konversi_"+n;
		var new_angka_faktor_konversi="#angka_faktor_konversi_"+no;
		$(angka_faktor_konversi, lastRow).attr("id", "angka_faktor_konversi_"+no);
		$(new_angka_faktor_konversi, lastRow).attr("name", "angka_faktor_konversi_"+no);		
		$(new_angka_faktor_konversi, lastRow).val('0');				
		//*****end angka_faktor_konversi*************************************	
		
		//*****qty_konv*************************************
		var qty_konv="#qty_konv_"+n;
		var new_qty_konv="#qty_konv_"+no;
		$(qty_konv, lastRow).attr("id", "qty_konv_"+no);
		$(new_qty_konv, lastRow).attr("name", "qty_konv_"+no);		
		$(new_qty_konv, lastRow).val('0');
		//*****end qty_konv*************************************	
				
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
		var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('0');				
		//*****end harga_lama*************************************	
		
		//*****pajak*************************************
		var pajak="#pajak_"+n;
		var new_pajak="#pajak_"+no;
		$(pajak, lastRow).attr("id", "pajak_"+no);
		$(new_pajak, lastRow).attr("name", "pajak_"+no);		
		$(new_pajak, lastRow).val('0');				
		//*****end pajak*************************************	
				
		//*****diskon*************************************
		var diskon="#diskon_"+n;
		var new_diskon="#diskon_"+no;
		$(diskon, lastRow).attr("id", "diskon_"+no);
		$(new_diskon, lastRow).attr("name", "diskon_"+no);		
		$(new_diskon, lastRow).val('0');				
		//*****end diskon*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
		
		//*****aslitotal*************************************
		var aslitotal="#aslitotal_"+n;
		var new_aslitotal="#aslitotal_"+no;
		$(aslitotal, lastRow).attr("id", "aslitotal_"+no);
		$(new_aslitotal, lastRow).attr("name", "aslitotal_"+no);		
		$(new_aslitotal, lastRow).val('0');				
		//*****end aslitotal*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 //var nama_for_even="document.f_purchase.iddata.value="+no;	
		 
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); var id_sup= $('#id_supplier').val(); var jp=$('#jenis_potong_"+no+ "').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/makloon/cform/show_popup_brg/'+x+'/'+jp+'/"+no+"/'+id_sup);";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		 $(new_pilih, lastRow).attr("disabled",false);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		var jumawal = $("#jumawal").val();
		//if (x>2) {
		if (parseInt(x) > parseInt(jumawal)) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
	$('#pilih_op').click(function(){
		var cek_pp = 0;
		var id_sup= $('#id_supplier2').val();
		var jenis_pembelian= $('#jenis_pembelian').val();
					
		var urlnya = "<?php echo base_url(); ?>index.php/makloon/cform/show_popup_op/"+id_sup+"/"+jenis_pembelian;
		openCenteredWindow(urlnya);
		
	  });
	  	  
	  $('#is_no_op').click(function(){
	  	    if ($('input[name=is_no_op]').is(':checked')) {
				$('#id_brg').val('');
				$('#kd_brg').val('');
				$("#pilih_op").attr("disabled", true);
			}
			else {
				$("#pilih_op").attr("disabled", false);
			}
	  });
	  
	 // skrip jquery utk save
	$("#button_simpan").click(function(){
		var id_op_cek= $('#id_op').val();
		cek_pembelian();
		
		var jum= $('#no').val()-1; 
		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				var kodenya = $('#kode_'+k).val();
				var qtynya = $('#qty_'+k).val();
				
				var qty_op_detail = 0;
				var qty_pembelian_detail = 0;
				
				// 1. ambil qty di op_detail
				jQuery.post("<?php echo base_url(); ?>index.php/faktur-bb/cform/get_qty_op_detail",
					{ id_op: id_op_cek,
					  kode_brg: kodenya,
					  qty: qtynya
					},
					function(data){	
						//jQuery('#ambil_qty_op_detail').html("<input type='hidden' name='qty_op_detail' id='qty_op_detail' value='"+data+"'> ");
						//alert ($('#qty_lama_'+k).val());
						var qty_lama = $('#qty_lama_'+k).val();
						if (data =='1') {
							//alert (data);
							$('#qty_'+k).val(qty_lama);
							hitungnilai();
						//	jQuery("#f_purchase").submit();
						}
						//else
						//	
					}
				);
				
			} // end for
			jQuery("#f_purchase").submit();
	   }  // end if
		
    });
	 //======================
	 	 
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 900;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

// 06-06-2015, ambil dari tirai
function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
		
		var i=1;
		var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					
					var rumus=$("#rumus_konversi_"+i).val();
					var angka=$("#angka_faktor_konversi_"+i).val();
					
					var satuan=$("#satuan_"+i).val();
					var satuan_konv=$("#satuan_konv_"+i).val();
					
					if ((satuan == 'Meter' && satuan_konv == 'Tidak Ada') || satuan == 'Yard') {
						var qtynya= parseFloat(qty)/1.1; qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); 
					}
					else if (satuan == 'Roll') {
						var qtynya= parseFloat(qty)/60; qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); 
					}
					else {
						if (rumus == '1') { var qtynya= parseFloat(qty)*parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '2') { var qtynya= parseFloat(qty)/parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '3') { var qtynya= parseFloat(qty)+parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else if (rumus == '4') { var qtynya= parseFloat(qty)-parseFloat(angka); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); } 
						else { var qtynya = parseFloat(qty); qtynya = qtynya.toFixed(4); $('#qty_konv_'+i).val(qtynya); }
					}
						var hitung = (harga*qty)-diskon;
						hitung = hitung.toFixed(4);
						var dpp = hitung*0.98;
						// new_pajak = hitung-dpp;
						// new_pajak = new_pajak.toFixed(4);
						// $("#pajak_"+i).val(new_pajak);
						$("#aslitotal_"+i).val(hitung);
						
						// cara 2
						//var myMoney=3543.75873;
						//var formattedMoney = myMoney.formatMoney(2,',','.');
						//alert(formattedMoney);
						var hitungformat = formatMoney(hitung, 4,',','.');
						$("#total_"+i).val(hitungformat);
						
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					
				}
				gtotal = gtotal.toFixed(4);
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(4);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal*0.98;
				dpp = dpp.toFixed(4);
				$("#dpp").val(dpp);
				
				//23-01-2013, format utk total
				var hitungformat = formatMoney(gtotal, 4,',','.');
				$("#gtotal").val(hitungformat);
				$("#asligtotal").val(gtotal);
}

function cek_item_brg() {
	var id_brg= $('#id_brg').val();
	var kd_brg= $('#kd_brg').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	var id_supplier= $('#id_supplier2').val();
	
	if (jenis_pembelian == '0') {
		alert("Jenis pembelian harus dipilih..!");
		return false;
	}
	
	if ($('input[name=is_no_op]').is(':checked') == false) {
		if (kd_brg == '') {
			alert("List barang di OP harus dipilih..!");
			return false;
		}
		if (id_brg == '') {
			alert("List barang di OP harus dipilih..!");
			return false;
		}
	}
}

function cek_pembelian() {
	var no_sj= $('#no_sj').val();
	var tgl_sj= $('#tgl_sj').val();
	var id_op_cek= $('#id_op').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	var id_sup= $('#id_sup').val();
	
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (id_sup == '0') {
			var id_supplierx= $('#id_supplierx').val();
			if (id_supplierx == 'xx') {
				alert("Supplier harus dipilih..!");
				return false;
			}
		}
		if (no_sj == '') {
			alert("Nomor SJ harus diisi..!");
			return false;
		}
		if (tgl_sj == '') {
			alert("Tanggal SJ harus dipilih..!");
			return false;
		}
		if (jenis_pembelian == '0') {
			alert("Jenis pembelian harus dipilih..!");
			return false;
		}
		
		var jum= $('#no').val()-1; 
		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				/*if($('#satuan_lain_'+k).val() != '0') {
					if($('#qty_sat_lain_'+k).val() == '0' || $('#qty_sat_lain_'+k).val() == '' ) {				
						alert("Data qty satuan lain tidak boleh 0 / kosong...!");
						return false;
					}
					else if (isNaN($('#qty_sat_lain_'+k).val()) ) {
						alert("Qty satuan lain harus berupa angka..!");
						return false;
					}
				} */
				
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka..!");
					return false;
				}
				
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Harga harus berupa angka..!");
					return false;
				}
				
				if ($('#diskon_'+k).val() == '' ) {
					alert("Diskon minimal harus diisi 0..!");
					return false;
				}
				
				if (isNaN($('#diskon_'+k).val()) ) {
					alert("Diskon harus berupa angka..!");
					return false;
				}
								
			} // end for
			
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
	}
	else
		return false;
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_btb', 'id' => 'f_btb');
echo form_open('makloon/cform/addbtbbisbisan', $attributes); ?>
<table>
	<tr>
		<td> Tidak ada acuan OP </td>
		<td> : <input type="checkbox" name="is_no_op" id="is_no_op" value="t"> &nbsp;
		 </td>
	</tr>
	<tr>
		<td>Supplier</td>
		<td>: <select name="id_supplier2" id="id_supplier2">
				<?php foreach ($supplier2 as $sup) { ?>
					<option value="<?php echo $sup->id ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>
	<tr>
		<td>Jenis Pembelian</td>
		<td>: <select name="jenis_pembelian" id="jenis_pembelian" >
				<option value="1">Cash</option>
				<option value="2">Kredit</option>
			</select>&nbsp;</td> 
  </tr>

	<tr>
		<td>List Brg di OP</td>
		<td>: 
		<textarea name="kd_brg" id="kd_brg" readonly="true" rows="4" cols="30"></textarea>
		<input type="hidden" name="id_brg" id="id_brg" value="">
		<input name="pilih_op" id="pilih_op" value="..." type="button">
		</td>
	</tr>
	
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/makloon/cform/viewbtbbisbisan'">
<?php echo form_close(); 
} else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/makloon/cform/submitbtbbisbisan" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_op" id="id_op" value="<?php echo $id_op ?>">
<input type="hidden" name="id_sup" id="id_sup" value="<?php echo $id_supplier ?>">

<div id="ambil_qty_op_detail"><input type="hidden" name="qty_op_detail" id="qty_op_detail" value=""></div>
<div id="ambil_qty_pembelian_detail"><input type="hidden" name="qty_pembelian_detail" id="qty_pembelian_detail" value=""></div>

<?php 
		if (count($op_detail)>0) {
			$jumawal = count($op_detail)+1;
			$no=1;
			foreach ($op_detail as $hitung) {
				$no++;
			}
		}
		else { 
			$no=2;
			$jumawal = 2;
		}
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
<div align="center">

<label id="status"></label>
<br>
<table width="100%" cellspacing="2" cellpadding="1" >
	<tr>
    <td width="20%">Nomor OP</td>
    <td>
      <?php echo $no_op; ?>
    </td>
  </tr>
		<tr>
			<td>Supplier</td>
			<td><select name="id_supplier" id="id_supplier" onkeyup="this.blur();this.focus();" disabled >
				<option value="0" <?php if ($id_supplier == '0') { ?> selected="true" <?php } ?> >Lain-lain</option>
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($sup->id == $id_supplier) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr>
  <tr>
    <td>Nomor SJ</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" maxlength="20" value="">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tanggal SJ</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
		<td>Jenis Pembelian</td>
		<td><input type="hidden" name="jenis_pembelian" id="jenis_pembelian" value="<?php echo $jenis_pembelian ?>">
			<select name="jenis_pembelianx" id="jenis_pembelianx" disabled>
				<option value="0">-Pilih-</option>
				<option value="1" <?php if ($jenis_pembelian == '1') { ?>selected<?php } ?>>Cash</option>
				<option value="2" <?php if ($jenis_pembelian == '2') { ?>selected<?php } ?>>Kredit</option>
			</select>&nbsp;</td> 
  </tr>  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="12" align="left">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Jenis Potong & Ukuran</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan Awal</th>
	      <th>Qty Sat Yard</th>
	      <th>Satuan Konversi</th>
	      <th>Qty Sat Konversi</th>
          <th>Harga</th>
	      <th>Diskon</th>
	      <th>PPN 2%</th>
          <th>Total</th>
        </tr>

        <?php $i=1;
        if (count($op_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($op_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td style="white-space:nowrap;"><select name="jenis_potong_<?php echo $i ?>" id="jenis_potong_<?php echo $i ?>">
			  <option value="1" <?php if ($op_detail[$j]['jenis_potong'] == 1) { ?>selected<?php } ?>>Potong Serong</option>
			  <option value="2" <?php if ($op_detail[$j]['jenis_potong'] == 2) { ?>selected<?php } ?>>Potong Lurus</option>
			  <option value="3" <?php if ($op_detail[$j]['jenis_potong'] == 3) { ?>selected<?php } ?>>Potong Spiral</option>
			  </select>
			  <select name="id_ukuran_<?php echo $i ?>" id="id_ukuran_<?php echo $i ?>">
			<?php foreach ($ukuran_bisbisan as $uk) { ?>
					<option value="<?php echo $uk->id ?>" <?php if ($op_detail[$j]['id_ukuran_bisbisan'] == $uk->id) { ?>selected<?php } ?> ><?php echo $uk->nama ?></option>
				<?php } ?>
          </select></td>
          <td><select name="kel_brg_<?php echo $i ?>" id="kel_brg_<?php echo $i ?>" disabled>
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($op_detail[$j]['kode_kel'] == $kel->kode) { ?> selected="true" <?php } ?> > <?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
			</select></td>
          
          <td style="white-space:nowrap;">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $op_detail[$j]['kode_brg'] ?>"/>
           <input name="id_brg_<?php echo $i ?>" type="hidden" id="id_brg_<?php echo $i ?>" value="<?php echo $op_detail[$j]['id_brg'] ?>"/>
           
           <input title="browse data barang" name="pilih_<?php echo $i ?>" value="..." type="button" disabled id="pilih_<?php echo $i ?>" 
           onclick="javascript: var x= $('#kel_brg_<?php echo $i ?>').val(); var id_sup = $('#id_supplier').val(); var jp = $('#jenis_potong_<?php echo $i ?>').val();
           openCenteredWindow('<?php echo base_url(); ?>index.php/makloon/cform/show_popup_brg/'+ x +'/'+jp+'/<?php echo $i ?>/' + id_sup);" >
           
           <input type="hidden" name="id_op_detail_<?php echo $i ?>" id="id_op_detail_<?php echo $i ?>" value="<?php echo $op_detail[$j]['id'] ?>" >
           <input type="hidden" name="id_op_<?php echo $i ?>" id="id_op_<?php echo $i ?>" value="<?php echo $op_detail[$j]['id_op'] ?>" >
          </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="25" readonly="true" value="<?php echo $op_detail[$j]['nama'] ?>" /></td>
          
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="5" readonly="true" value="<?php echo $op_detail[$j]['nama_satuan'] ?>" /></td>
          
          <td><input style="text-align:right;" name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="3" value="<?php echo $op_detail[$j]['qty_satawal']; ?>" onkeyup="hitungnilai()" onblur="hitungnilai()" />
          <input name="qty_lama_<?php echo $i ?>" type="hidden" id="qty_lama_<?php echo $i ?>"
          value="<?php echo $op_detail[$j]['qty'] ?>" />
          <input name="qty_op_<?php echo $i ?>" type="hidden" id="qty_op_<?php echo $i ?>"
          value="<?php echo $op_detail[$j]['qty_op'] ?>" />
          <input name="jum_beli_<?php echo $i ?>" type="hidden" id="jum_beli_<?php echo $i ?>"
          value="<?php echo $op_detail[$j]['jum_beli'] ?>" />
          </td>
          
          <td><input name="satuan_konv_<?php echo $i ?>" type="text" id="satuan_konv_<?php echo $i ?>" size="5" readonly="true" value="<?php echo $op_detail[$j]['nama_satuan_konv']; ?>"/>
          <input name="rumus_konversi_<?php echo $i ?>" type="hidden" id="rumus_konversi_<?php echo $i ?>" value="<?php echo $op_detail[$j]['rumus_konversi']; ?>"/>
          <input name="angka_faktor_konversi_<?php echo $i ?>" type="hidden" id="angka_faktor_konversi_<?php echo $i ?>" value="<?php echo $op_detail[$j]['angka_faktor_konversi']; ?>"/>
          </td>
          <td><input style="text-align:right;" name="qty_konv_<?php echo $i ?>" type="text" id="qty_konv_<?php echo $i ?>" size="3" value="<?php echo $op_detail[$j]['qty']; ?>" /></td>
          
          <td><input style="text-align:right;" name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="5" maxlength="10" 
          value="<?php echo $op_detail[$j]['harga'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()" />
          <input name="harga_lama_<?php echo $i ?>" type="hidden" id="harga_lama_<?php echo $i ?>" value="<?php echo $op_detail[$j]['harga'] ?>" />
          </td>
          <td><input style="text-align:right;" name="diskon_<?php echo $i ?>" type="text" id="diskon_<?php echo $i ?>" size="5" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()" /></td>
          <td><input style="text-align:right;" name="pajak_<?php echo $i ?>" type="text" id="pajak_<?php echo $i ?>" readonly="true" size="7" value="0" /></td>
          <td><input style="text-align:right;" name="total_<?php echo $i ?>" type="text" id="total_<?php echo $i ?>" readonly="true" size="10" value="0" />
          <input name="aslitotal_<?php echo $i ?>" type="hidden" id="aslitotal_<?php echo $i ?>" value="0" />
          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td>DPP</td>
			<td>: <input style="text-align:right;" type="text" name="dpp" id="dpp" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Total PPN</td>
			<td>: <input style="text-align:right;" type="text" name="tot_pajak" id="tot_pajak" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Grand Total</td>
			<td>: <input style="text-align:right;" type="text" name="gtotal" id="gtotal" value="0" size="10" readonly="true">
				<input type="hidden" name="asligtotal" id="asligtotal" value="0" >
			</td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>: <input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
	</table>
      </form>
      <div align="center"><br> 
        
      <input type="submit" name="submit" value="Simpan" onclick="return cek_pembelian(); ">
       
       &nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/makloon/cform/addbtbbisbisan'">

      </div>
      </td>
    </tr>

</table>
</div>
</form>
<?php } ?>
