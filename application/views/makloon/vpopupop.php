<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$("#pilih").click(function()
	{
		
		opener.document.forms["f_btb"].id_brg.value = '';
		opener.document.forms["f_btb"].kd_brg.value = '';

		if (document.f_master_brg.brg.length > 0) {
			for(var i=0; i < document.f_master_brg.brg.length; i++){
				if(document.f_master_brg.brg[i].checked) {
					//total +=document.form1.scripts[i].value + "\n"
					opener.document.forms["f_btb"].id_brg.value+= document.f_master_brg.brg[i].value + ";";
					opener.document.forms["f_btb"].kd_brg.value+= document.f_master_brg.kd_brg[i].value + ";";
				}
			}
		}
		else {
			opener.document.forms["f_btb"].id_brg.value = document.f_master_brg.brg.value;
			opener.document.forms["f_btb"].kd_brg.value = document.f_master_brg.kd_brg.value;
		}
		
		self.close();
	});
});
</script>
<?php 
	if ($jenis_pembelian == '1')
		$jenis1 = "Cash";
	else
		$jenis1 = "Kredit";
?>
	<center><h3>Daftar OP Makloon Bisbisan Yang Belum Realisasi SJ<br>Jenis Pembelian <?php echo $jenis1 ?><br>Supplier <?php if ($kode_supplier != '') echo $kode_supplier." - ".$nama_supplier; else echo $nama_supplier ?></h3></center>
	
<div align="center"><br>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="no_op" id="no_op">

	<table border="1" width="100%" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" nowrap="nowrap">
	<thead>
	 <tr class="judulnya">
		 <th>Nomor OP</th>
		 <th>Tanggal OP</th>
		 <th>Supplier</th>
		 <th>List Barang Sat Konversi</th>
		 <th>Last Update</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				$pisah1 = explode("-", $query[$j]['tgl_op']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_op = $tgl1." ".$nama_bln." ".$thn1; 
				 
				 if ($query[$j]['jenis_pembelian'] == '1')
					$jenis = "Cash";
				 else if ($query[$j]['jenis_pembelian'] == '2')
					$jenis = "Kredit";
				 else
					$jenis = "-";
				 
				 echo "<tr class=\"record\">";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['no_op']." (".$jenis.")</td>";
				 echo    "<td>".$tgl_op."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo "<td style='white-space:nowrap;'>";
				 
				 if (is_array($query[$j]['detail_op'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_op'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$satuannya = $var_detail[$k]['satuan_konv'];
						echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (".$var_detail[$k]['qty']." ".$satuannya.")";
						echo " <input type='checkbox' name='brg' id='brg' value='".$var_detail[$k]['id']."' >";
						echo " &nbsp; <input type='hidden' name='kd_brg' id='kd_brg' value='".$var_detail[$k]['kode_brg']."' >";
						if ($k<$hitung-1)
						   echo "<br>";
					}
				 }
				 echo "</td>";
				 echo    "<td>".$tgl_update."</td>"; ?>
				 
				<?php echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table>
</form>
  <input type="button" name="pilih" id="pilih" value="Pilih" >
</div>
