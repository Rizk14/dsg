<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
$(function() {
	$('#filter_brg').click(function(){
	  	    if ($("#filter_brg").is(":checked")) {
				$('#cari_brg').attr('disabled', false);
				$('#cari_brg').val('');
			}
			else {
				$('#cari_brg').attr('disabled', true);
				$('#cari_brg').val('');
			}
	  });
	
	$(".pilih").click(function()
	{
		var id_sj_print=$("#id_sj_print").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/makloon/cform/print_sj_bisbisan/"+id_sj_print;
		openCenteredWindow(urlnya);
	});
});

function openCenteredWindow(url) {

		var width = 850;
		var height = 550;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

</script>

<h3>Data Bukti Terima Barang (BTB) Makloon Bisbisan</h3><br>
<a href="<?php echo base_url(); ?>index.php/makloon/cform/addbtbbisbisan">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/makloon/cform/viewbtbbisbisan">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('makloon/cform/caribtbbisbisan'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Supplier</td>
		<td>: <select name="supplier" id="supplier">
				<option value="0" <?php if ($csupplier == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($csupplier == $sup->id) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>
	<tr>
		<td>Nomor SJ</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;" colspan="2"><input type="checkbox" name="filter_brg" id="filter_brg" value="y" <?php if ($filterbrg == 'y') { ?> checked="true" <?php } ?>>
		Filter berdasarkan kode/nama brg: <input type="text" name="cari_brg" id="cari_brg" size="10" value="<?php echo $caribrg ?>" <?php if ($filterbrg == 'n') { ?>disabled="true" <?php } ?>></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>		
</fieldset>
<?php echo form_close(); ?>
<br>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_sj_print" id="id_sj_print">
<?php 
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;
	
	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
?>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No OP</th>
		<th>Jenis Pembelian</th>
		<th>No SJ</th>
		 <th>Tgl SJ</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Sat Awal</th>
		 <th>Jenis Potong<br>& Ukuran</th>
		 <th>Qty<br>Sat Yard</th>
		 <th>Qty<br>Sat Konv</th>
		 <th>Harga (Rp.)</th>
		 <th>Diskon (Rp.)</th>
		 <th>Subtotal (Rp.)</th>
		 <th>Jum Total (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				echo    "<td style='white-space:nowrap;'>".$query[$j]['no_op']."</td>";
				 
				if ($query[$j]['jenis_pembelian'] == '1')
					echo    "<td nowrap>Cash</td>";
				else if ($query[$j]['jenis_pembelian'] == '2')
					echo "<td nowrap>Kredit</td>";
				else
					echo "<td nowrap>&nbsp;</td>";
				 echo    "<td nowrap>".$query[$j]['no_sj']."</td>";
				 
				 // 29-09-2015
				if ($cari == '')
					$xcari = "all";
				else
					$xcari = $cari;
					
				if ($caribrg == '')
					$xcaribrg = "all";
				else
					$xcaribrg = $caribrg;
				 
				 echo    "<td nowrap><a href=".base_url()."index.php/makloon/cform/edittglbtbbisbisan/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$xcari."/".$xcaribrg."/".$filterbrg." \" id=\"".$query[$j]['id']."\">".$tgl_sj."</td>";
				  echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['jenis_potong']." ".$var_detail[$k]['nama_ukuran'];
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty_satawal'];
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty_konversi']." ".$var_detail[$k]['satuan_konv'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['diskon'], 2, ',','.');
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['total'], 4, ',','.');
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo  "<td align='right'>".number_format($query[$j]['total'], 4, ',','.')."</td>";
				  
				 echo    "<td>".$tgl_update."</td>";
				 echo "<td align=center>";
				
				if ($query[$j]['cetakbtb'] == '1')
					echo "<a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_sj_print.value=".$query[$j]['id']."'; ><u>Print</u></a>&nbsp;";
				if ($query[$j]['status_faktur'] == 'f') {
					echo    "<a href=".base_url()."index.php/makloon/cform/editbtbbisbisan/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$xcari."/".$xcaribrg."/".$filterbrg." \" id=\"".$query[$j]['id']."\">Edit</a>";
						if ($query[$j]['id_detailnya'] == '')
							$detail_item = 0;
						else
							$detail_item = $query[$j]['id_detailnya'];
						echo "&nbsp;<a href=".base_url()."index.php/makloon/cform/deletebtbbisbisan/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$xcari."/".$xcaribrg."/".$filterbrg." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				}
				
				else {
					echo "&nbsp;";
				}
				 echo "</td>";
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
