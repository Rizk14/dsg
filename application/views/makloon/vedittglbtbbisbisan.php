<h3>Data Bukti Terima Barang (BTB) Makloon Bisbisan</h3><br>
<a href="<?php echo base_url(); ?>index.php/makloon/cform/addbtbbisbisan">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/makloon/cform/viewbtbbisbisan">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">
 $(function()
 {

 });
 
 function cek_data() {
	var tgl= $('#tgl').val();

	if (tgl == '') {
		alert("Tanggal harus diisi..!");
		$('#tgl').focus();
		return false;
	}

 }	

</script>

<?php 
$attributes = array('name' => 'f_purchase', 'id' => 'f_purchase');
echo form_open('makloon/cform/edittglbtbbisbisan', $attributes); ?>
<input type="hidden" name="id_pembelian" value="<?php echo $id_pembelian ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<input type="hidden" name="caribrg" value="<?php echo $caribrg ?>">
<input type="hidden" name="filterbrg" value="<?php echo $filterbrg ?>">
<input type="hidden" name="csupplier" value="<?php echo $csupplier ?>">
<input type="hidden" name="tgl_awal" value="<?php echo $tgl_awal ?>">
<input type="hidden" name="tgl_akhir" value="<?php echo $tgl_akhir ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="is_simpan" value="1">

<table width="50%">
		<tr>
			<td colspan="2">Edit Data Tanggal SJ</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td width="30%">Supplier</td>
			<td> <?php echo $kode_supplier." - ".$nama_supplier ?>
			</td>
		</tr>
		<tr>
			<td>Nomor SJ</td>
			<td> <?php echo $no_sj ?>
			</td>
		</tr>
		<tr>
			<td>Tanggal SJ</td>
			<td> 
				<input name="tgl_sj" type="text" id="tgl_sj" size="10" value="<?php echo $tgl_sj ?>" readonly="true">
				  <img alt="" id="tgl_sj" align="middle"
						title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
						onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
			
			</td>
		</tr>
</table><br>
<?php 
	if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "makloon/cform/viewbtbbisbisan/index/".$cur_page;
	else
		$url_redirectnya = "makloon/cform/caribtbbisbisan/".$csupplier."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
?>
<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<?php echo form_close(); ?>
