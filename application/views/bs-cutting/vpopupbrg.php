<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var des=$("#des").val();
		var qty=$("#qty").val();
		var brg_jadi=$("#brg_jadi").val();
		var kode_brg_jadi=$("#kode_brg_jadi").val();
		var brg_jadi_manual=$("#brg_jadi_manual").val();
		var diprint=$("#diprint").val();
		var is_dacron=$("#is_dacron").val();
		
		if (qty == undefined || qty == '')
			qty = 0;
		
		opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=idx+" - "+des;
		opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
		opener.document.forms["f_purchase"].brg_jadi_<?php echo $posisi ?>.value=brg_jadi;
		opener.document.forms["f_purchase"].kode_brg_jadi_<?php echo $posisi ?>.value=kode_brg_jadi;
		opener.document.forms["f_purchase"].brg_jadi_manual_<?php echo $posisi ?>.value=brg_jadi_manual;
		opener.document.forms["f_purchase"].diprint_<?php echo $posisi ?>.value=diprint;
		opener.document.forms["f_purchase"].is_dacron_<?php echo $posisi ?>.value=is_dacron;
		self.close();
	});
});
</script>
<center>
<h3>Daftar Bahan Baku Hasil Cutting</h3>

</center>
<div align="center"><br>
<?php echo form_open('bs-cutting/cform/show_popup_brg_cutting'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">

<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="des" id="des">
<input type="hidden" name="qty" id="qty">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="brg_jadi" id="brg_jadi">
<input type="hidden" name="kode_brg_jadi" id="kode_brg_jadi">
<input type="hidden" name="brg_jadi_manual" id="brg_jadi_manual">
<input type="hidden" name="diprint" id="diprint">
<input type="hidden" name="is_dacron" id="is_dacron">
</form>

  <table border="1" align="center" cellpadding="1" width="100%" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Utk Brg Jadi</th>
      <th bgcolor="#999999">Bhn Baku Cutting</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Diprint</th>
      <th bgcolor="#999999">Bhn Dacron</th>
      <th bgcolor="#999999">Stok Terkini</th>
      <th bgcolor="#999999">Action</th>
    </tr>
	<?php 
		//$i=1; 
		
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
				
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']; ?></td>
      <td><?php echo $query[$j]['kode_brg']." - ".$query[$j]['nama_brg']; ?></td>
      <td>Set<?php //echo $query[$j]['satuan']; ?></td>
      <td><?php if ($query[$j]['diprint'] == 't') echo "Ya"; else echo "Tidak"; ?></td>
      <td><?php if ($query[$j]['is_dacron'] == 't') echo "Ya"; else echo "Tidak"; ?></td>
      <td align="right"><?php if ($query[$j]['stok']!= 0) echo $query[$j]['stok']; else echo "0"; ?></td>
      <td align="center">
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
	  window.document.f_master_brg.des.value='<?php 
	  $pos = strpos($query[$j]['nama_brg'], "\"");
	  if ($pos > 0)
		echo str_replace("\"", "&quot;", $query[$j]['nama_brg']);
	  else
		echo str_replace("'", "\'", $query[$j]['nama_brg']);
	?>'; 
	  window.document.f_master_brg.brg_jadi.value='<?php echo $query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi'] ?>'; 
	  window.document.f_master_brg.kode_brg_jadi.value='<?php echo $query[$j]['kode_brg_jadi'] ?>'; 
	  window.document.f_master_brg.brg_jadi_manual.value='<?php echo $query[$j]['brg_jadi_manual'] ?>'; 
	  window.document.f_master_brg.diprint.value='<?php echo $query[$j]['diprint'] ?>'; 
	  window.document.f_master_brg.is_dacron.value='<?php echo $query[$j]['is_dacron'] ?>'; 
	  window.document.f_master_brg.qty.value='<?php echo $query[$j]['stok'] ?>'; 
	  window.document.f_master_brg.satuan.value='<?php echo $query[$j]['satuan'] ?>';">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
