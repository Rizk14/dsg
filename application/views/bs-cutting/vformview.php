<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .judulnya {
		background-color:#DDD;
	}

</style>

<h3>Data Pengurangan Stok Hasil Cutting BS</h3><br>
<a href="<? echo base_url(); ?>index.php/bs-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/bs-cutting/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('bs-cutting/cform/cari'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		 <th>Tgl</th>
		 <th>Barang Jadi</th>
		 <th>Bahan Baku</th>
		 <th>Diprint</th>
		 <th>Bhn Dacron</th>
		 <th>Jml Qty (Set)</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_input']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_input = $tgl1." ".$nama_bln." ".$thn1;
								 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$tgl_input."</td>";
				 echo    "<td>".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."</td>";
				 echo    "<td>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>";
				 
				 echo    "<td>";
					if ($query[$j]['diprint'] == 't') echo "Ya"; else echo "Tidak";
				 echo "</td>";
				 echo    "<td>";
					if ($query[$j]['is_dacron'] == 't') echo "Ya"; else echo "Tidak";
				 echo "</td>";
				 echo    "<td>".$query[$j]['jumlah']."</td>";
				 
				 echo    "<td align=center>
					 <a href=".base_url()."index.php/bs-cutting/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
