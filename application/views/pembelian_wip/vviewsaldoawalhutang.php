<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Input Saldo Awal Hutang Dagang WIP</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var jum_data = $('#jum_data').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data saldo awal ??");
	
	if (kon) {
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
				if($('#stok_fisik_'+k).val() == '') {				
					alert("Data saldo tidak boleh kosong, jika kosong maka harus isikan angka 0...!");
					s=1;
					return false;
				}
				if (isNaN($('#stok_fisik_'+k).val()) ) {
					alert("Data saldo harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}

function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

</script>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<i>* Inputkan hanya angka saja tanpa karakter lainnya</i><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('pembelian_wip/cform/submitsaldoawalhutang', $attributes);
$no = count($query);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="jum_data" id="jum_data" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		
		 <th>Nama kelompok Unit</th>
		 <th>Jumlah Saldo Awal (Rp.)</th>
		 <?php //if ($is_new != 1) { ?>
		<!-- <th>Hapus Item</th> -->
		 <?php //} ?>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>&nbsp;".$query[$j]['nama_kelompok_unit']."
					 <input type='hidden' name='id_kelompok_unit_$i' id='id_kelompok_unit_$i' value='".$query[$j]['id_kelompok_unit']."'>
					 </td>";
			
				?>
					 <td align='center'><input type="text" name="temp_stok_fisik_<?php echo $i ?>" id="temp_stok_fisik_<?php echo $i ?>" value="<?php echo number_format($query[$j]['stok_opname'],2,'.',',') ?>" size="10" style="text-align: right;" onblur="javascript: $('#stok_fisik_<?php echo $i ?>').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; ">
						<input type="hidden" name="stok_fisik_<?php echo $i ?>" id="stok_fisik_<?php echo $i ?>" value="<?php echo $query[$j]['stok_opname'] ?>">
				<?php echo	 "</td>";
					 /*if ($is_new != 1) {
						 echo "<td align='center'>";
						 echo "<input name='cek_$i' id='cek_$i' type='checkbox' value='y' />";
						 echo "</td>";
					 } */
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>
<input type="submit" name="submit" value="Simpan Saldo Awal" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Saldo Awal Periode Ini" onclick="return confirm('Yakin akan hapus Saldo Awal periode ini ??')">&nbsp;
<!--<input type="submit" name="submit3" value="Hapus Item Saldo Awal Yang Dipilih" onclick="return confirm('Yakin akan hapus item Saldo Awal ??')">&nbsp;-->
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/pembelian_wip/cform/saldoawalhutang'">
<?php echo form_close();  ?>
</div>
