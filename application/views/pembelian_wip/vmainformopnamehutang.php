<h3>Input Opname Hutang Dagang Manual</h3><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
										
		//*****tgl_bukti*************************************
		var tgl_bukti="#tgl_bukti_"+n;
		var new_tgl_bukti="#tgl_bukti_"+no;
		$(tgl_bukti, lastRow).attr("id", "tgl_bukti_"+no);
		$(new_tgl_bukti, lastRow).attr("name", "tgl_bukti_"+no);		
		$(new_tgl_bukti, lastRow).val('');		
		//*****end tgl_bukti*********************************
		
		//*****imgtgl_bukti*************************************
		var imgtgl_bukti="#imgtgl_bukti_"+n;
		var new_imgtgl_bukti="#imgtgl_bukti_"+no;
		$(imgtgl_bukti, lastRow).attr("id", "imgtgl_bukti_"+no);
		$(new_imgtgl_bukti, lastRow).attr("name", "imgtgl_bukti_"+no);
		var  even_klik= "displayCalendar(document.forms[0].tgl_bukti_"+no+",'dd-mm-yyyy',this)";
		$(new_imgtgl_bukti, lastRow).attr("onclick",even_klik);	
		//*****end imgtgl_bukti*********************************
		
		//*****no_bukti*************************************
		var no_bukti="#no_bukti_"+n;
		var new_no_bukti="#no_bukti_"+no;
		$(no_bukti, lastRow).attr("id", "no_bukti_"+no);
		$(new_no_bukti, lastRow).attr("name", "no_bukti_"+no);		
		$(new_no_bukti, lastRow).val('');		
		//*****end no_bukti*********************************
				
		//*****jumlahx*************************************
		var jumlahx="#jumlahx_"+n;
		var new_jumlahx="#jumlahx_"+no;
		$(jumlahx, lastRow).attr("id", "jumlahx_"+no);
		$(new_jumlahx, lastRow).attr("name", "jumlahx_"+no);
		var even_blur= "$('#jumlah_"+no+"').val(this.value); var angka = formatMoney(this.value, 4,',','.'); this.value= angka;";
		$(new_jumlahx, lastRow).attr("onblur",even_blur);	
		$(new_jumlahx, lastRow).val('');
		//*****end jumlahx*************************************	
		
		//*****jumlah*************************************
		var jumlah="#jumlah_"+n;
		var new_jumlah="#jumlah_"+no;
		$(jumlah, lastRow).attr("id", "jumlah_"+no);
		$(new_jumlah, lastRow).attr("name", "jumlah_"+no);
		$(new_jumlah, lastRow).val('');				
		//*****end jumlah*************************************	
														
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_data() {
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if ($('#tgl_bukti_'+k).val() == '') {
					alert("Tanggal bukti harus diisi...!");
					s = 1;
					return false;
				}
				if ($('#no_bukti_'+k).val() == '') {
					alert("Nomor bukti harus diisi...!");
					s = 1;
					return false;
				}
				if($('#jumlah_'+k).val() == '0' || $('#jumlah_'+k).val() == '' ) {				
					alert("Data jumlah tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#jumlah_'+k).val()) ) {
					alert("Jumlah harus berupa angka..!");
					s = 1;
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}

function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pembelian/cform/submitopnamehutang" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td width="15%">Supplier</td>
		<td> <select name="id_supplier" id="id_supplier">
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td width="15%">Debet Retur</td>
		<td> <select name="isretur" id="isretur">
				<option value="0">Tidak</option>
				<option value="1">Ya</option>
				</select>
		</td>
	</tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku" width="50%" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="4" align="right">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="3%">No</th>
          <th>Tgl Bukti</th>
          <th>Nomor Bukti</th>
          <th>Jumlah</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>		  
          <td style="white-space:nowrap;">
			  <label>
			  <input name="tgl_bukti_1" type="text" id="tgl_bukti_1" size="10" value="" readonly="true">
			</label>
			   <img alt="" id="imgtgl_bukti_1" align="middle"
					title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
					onclick="displayCalendar(document.forms[0].tgl_bukti_1,'dd-mm-yyyy',this)">
           </td>
          <td><input name="no_bukti_1" type="text" id="no_bukti_1" size="10" value="" /></td>
		  <td><input style="text-align:right;" name="jumlahx_1" type="text" id="jumlahx_1" size="10" value="" onblur="javascript: $('#jumlah_1').val(this.value); var angka = formatMoney(this.value, 4,',','.'); this.value= angka; " />
		  <input type="hidden" name="jumlah_1" id="jumlah_1" value="">
		  </td>
          
        </tr>
	</table>	
	
	</form><br>
	<div align="left"><input type="submit" name="submit2" value="Simpan" onclick="return cek_data();"></div>

     </td>
    </tr>

</table>
</div>
</form>
