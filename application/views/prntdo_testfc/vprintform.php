<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_do; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_do; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'prntdo_testfc/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterldobrgform">

		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0"> 
				  <tr>
					<td width="15%"><?php echo $list_no_do; ?></td>
					<td width="1%">:</td>
					<td>
					<?php
					$nomor_do	= array(
					'name'=>'nomor_do',
					'id'=>'nomor_do',
					'type'=>'text',
					'value'=>$nomor,
					'maxlength'=>'14'
					);
					echo form_input($nomor_do);
					?>
					</td>
				  </tr>
				  <tr>
					<td width="15%">Tgl. Delivery Order (DO)</td>
					<td width="1%">:</td>
					<td>
					<?php
					echo $sh_tgl;
					?>
					</td>
				  </tr>
				  <tr>
					<td width="15%">Customer/ Area</td>
					<td width="1%">:</td>
					<td>
					<?php
					echo $nmcabang;
					?>
					</td>
				  </tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_do; ?></div></td>	
			</tr>
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="3%" class="tdatahead">NO</td>
				  <td width="12%" class="tdatahead"><?php echo $list_no_op_do; ?></td>
				  <td width="15%" class="tdatahead"><?php echo $list_kd_brg_do; ?></td>
				  <td width="40%" class="tdatahead"><?php echo $list_nm_brg_do; ?></td>
				  <td width="10%" class="tdatahead"><?php echo $list_qty_do; ?></td>
				</tr>
				
				<?php				
				$total	= 0;
				
				if(sizeof($query) > 0 ) {
				 $db2 = $this->load->database('db_external', TRUE);
					$no	= 1;
					$cc = 1;
					foreach($query as $row) {
						$detailwarna="";
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$sqlxx = " SELECT a.i_color, b.e_color_name, a.qty FROM tm_do_item_color a 
								  INNER JOIN tr_color b ON a.i_color=b.i_color
								  WHERE a.i_do_item = '".$row->i_do_item."' ";
						
						$queryxx = $db2->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->result();
							
							foreach ($hasilxx as $rowxx) {
								if ($rowxx->qty != 0)
									$detailwarna.= $rowxx->e_color_name.": ".$rowxx->qty." ";
							}
						}
						else
							$detailwarna="blm ada qty warna";
											
						$ldobrg	.= "
							<tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
							  <td height=\"22px\" bgcolor=\"$bgcolor\">".$no."&nbsp;</td>
							  <td>".$row->iop."</td>
							  <td>".$row->iproduct."</td>
							  <td>".$row->motifname."</td>
							  <td align=\"right\">".$row->qty." (".$detailwarna.")</td>
							</tr>";
							$no+=1;
							$cc+=1;
					}
				}
				?>
				
				<?php 
				echo $ldobrg;
				?>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table></td>
			</tr>
			<tr>
			  <td></td>
			</tr>			
			<tr>
			  <td align="right">
				  	<input type="button" name="btnkeluar" value="<?php echo $button_keluar; ?>"onclick="window.location='<?php echo base_url(); ?>index.php/prntdo_testfc/cform'"/>
</td>
			</tr>
		  </table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
