<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<h3>Data SJ Keluar Untuk Proses Jahit</h3><br>
<a href="<? echo base_url(); ?>index.php/sj-keluar-jahit-new/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="goedit" id="goedit" <?php if ($edit=='1') { ?> value="1" <?php } else { ?> value="" <?php } ?> >
<input type="hidden" name="nosj" id="nosj" value="<?php echo $eno_sj ?>">
<input type="hidden" name="id" id="id" value="<?php echo $eid ?>">

<?php
if ($edit==1) {
	if (count($jahit_detail)>0) {
		$jumawal = count($jahit_detail)+1;
		$no=1;
		foreach ($jahit_detail as $hitung) {
			$no++;
		}
	}
	else {
		$no=2;
		$jumawal = 2;
	}
}
else
	$jumawal = 2;
?>

<input type="hidden" name="no" id="no" <?php if ($edit==1) { ?> value="<?php echo $no ?>" <?php } ?> >
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
<input type="hidden" name="iddata" id="iddata" />
<input type="hidden" name="iddata_brg" id="iddata_brg" />
<?php 
	if ($edit==1) {
?>
	Edit Data <br><br>
<?php
	}
?>
<div align="center">

<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td width="15%">Unit Jahit</td>
		<td> <select name="kode_unit" id="kode_unit">
			<?php foreach ($kode_unit as $unit) { ?>
				<option value="<?php echo $unit->kode_unit ?>" <?php if ($ekode_unit==$unit->kode_unit) { ?> selected="true" <?php } ?> ><?php echo $unit->kode_unit." - ". $unit->nama ?></option>
			<?php } ?>
			</select>
		</td>
		
	</tr>
	<tr>
		<td width="10%">Nomor SJ </td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="10" maxlength="10" value="<?php echo $eno_sj ?>" onkeyup="ckpp(this.value);" readonly="true">&nbsp;<span id="msgbox" style="display:none;"></span>
    </td>
	</tr>
	
	<tr>
		
		<td>Tanggal SJ </td>
		<td>
		  <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="<?php echo $etgl_sj ?>" readonly="true">
		  <img alt="" id="tgl_retur" align="middle" title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif" onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
		</td>
	</tr>
	
	<tr>
		<!--<td>Makloon Internal Duta </td>
		<td>
		  <input type="checkbox" name="makloon_internal" id="makloon_internal" value="t" <?php echo $makloon; ?> >
		</td> -->
		<td>Keterangan</td>
    <td>
      <input name="ket" type="text" id="ket" size="20" value="<?php echo $ket ?>" >
	</td>
   </tr>		
  <tr>
  <tr>
    <td colspan="4"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<?php 
			if ($edit!=1) {
		?>
		<tr>
			<td colspan="9" align="left"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
		<?php
			}
		?>
        <tr>
          <th width="20">No</th>
          <th>Kel Bhn</th>
          <th>Barang Jadi</th>
          <th>Bahan Baku</th>
	      <th>Qty</th>
	      <th>Ukuran (cm)</th>
	      <th>Qty (pcs)</th>
          <th>Keterangan</th>
        </tr>
        <?php if ($edit=='') { ?>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          
          <td>
				<select name="kel_brg_1" id="kel_brg_1" <?php if ($edit=='1') { ?> disabled <?php } ?> >
				<?php if ($edit!=1) { ?>
					<option value="1">Bisbisan</option>
				<!--	<option value="2">Bordir</option> 
					<option value="3">Print</option> -->
					<option value="4">Makloon Asesoris</option>
					<option value="5">Asesoris Murni</option>
					<option value="6">Hasil Cutting</option>
				</select>
				<?php } else { ?>
					<option value="1" <?php if ($ekel_bhn == 1) { ?> selected="true" <?php } ?> >Bisbisan</option>
					<option value="4" <?php if ($ekel_bhn == 4) { ?> selected="true" <?php } ?> >Makloon Asesoris</option>
					<option value="5" <?php if ($ekel_bhn == 5) { ?> selected="true" <?php } ?> >Asesoris Murni</option>
					<option value="6" <?php if ($ekel_bhn == 6) { ?> selected="true" <?php } ?> >Hasil Cutting</option>
				<?php } ?>
          </td>
          
          <td nowrap><input name="nama_brg_jd_1" type="text" id="nama_brg_jd_1" size="35" value="" readonly="true"/>
          <input name="kode_brg_jd_1" type="hidden" id="kode_brg_jd_1" value=""/>
         <input title="browse data barang jadi" onmouseover="document.f_purchase.iddata_brg.value=1" name="pilih_brg_1" value="..." type="button" id="pilih_brg_1" 
          onclick="javascript: var x= $('#kel_brg_1').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/show_popup_brg_jadi/'+ x +'/1/');" ></td>
          
          <td nowrap="nowrap">
           <input name="kode_bhn_1" type="hidden" id="kode_bhn_1" value="" />
           <input name="nm_brg_1" type="text" id="nm_brg_1" size="35" readonly="true" />
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#kel_brg_1').val();  var zz='xx';
           openCenteredWindow('<?php echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/show_popup_brg/'+ x +'/1/'+zz);" ></td>
         
         <td><input name="qty_1" type="text" id="qty_1" size="10" readonly="true" />
			<input name="diprint_1" type="hidden" id="diprint_1" value="" />
			<input name="is_dacron_1" type="hidden" id="is_dacron_1" value="" />
			<input name="ukuran_1" type="hidden" id="ukuran_1" value="" />
			<input name="id_stok_1" type="hidden" id="id_stok_1" value="" />
		</td>
		<td><input name="ukuranx_1" type="text" id="ukuranx_1" size="10" readonly="true" /></td>
		<td><input name="qtyx_1" type="text" id="qtyx_1" size="10" readonly="true" /></td>
         <td><input name="keterangan_1" type="text" id="keterangan_1" size="25" /><input type="hidden" name="id_1" id="id_1" /></td>
        </tr>
        <?php } else { $i=1;
			for($j=0;$j<count($jahit_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap><select name="kel_brg_<?php echo $i ?>" id="kel_brg_<?php echo $i ?>" <?php if ($edit=='1') { ?> disabled <?php } ?>>
		  		<?php
				$kel_brg_arre	= array(
					'1'=>'Bisbisan',
				//	'2'=>'Bordir',
				//	'3'=>'Print',
					'4'=>'Makloon Asesoris',
					'5'=>'Asesoris Murni',
					'6'=>'Hasil Cutting' );
				?>
				<?php foreach ($kel_brg_arre as $key => $kel) { ?>
					<option value="<?php echo $key ?>" <?php if ($jahit_detail[$j]['kel_bhn'] == $key) { ?> selected="true" <?php } ?> ><?php echo $kel ?></option>
				<?php } ?>
				</select></td>
		
		<td nowrap><input name="nama_brg_jd_<?php echo $i ?>" type="text" id="nama_brg_jd_<?php echo $i ?>" size="35" readonly="true" value="<?php echo $jahit_detail[$j]['kode_brg_jadi']." - ".$jahit_detail[$j]['nm_brg_jadi'] ?>" onclick="getbrgjadi('"<?=$i?>"');" /><input disabled title="browse data barang jadi" onmouseover="document.f_purchase.iddata_brg.value=<?php echo $i ?>" name="pilih_brg_<?php echo $i ?>" value="..." type="button" id="pilih_brg_<?php echo $i ?>" onclick="javascript: var x= $('#kel_brg_<?php echo $i ?>').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-keluar-jahit/cform/show_popup_brg_jadi/'+x+'/<?php echo $i; ?>');" type="button"></td>
		<input name="kode_brg_jd_<?php echo $i ?>" type="hidden" id="kode_brg_jd_<?php echo $i ?>" value="<?php echo $jahit_detail[$j]['kode_brg_jadi'] ?>"/>
          <td nowrap="nowrap">
		   <!-- <input name="idbarang_1" type="hidden" id="idbarang_1" /> -->
		   <input name="kode_bhn_<?php echo $i ?>" type="hidden" id="kode_bhn_<?php echo $i ?>" value="" />
           <input name="nm_brg_<?php echo $i ?>" type="text" id="nm_brg_<?php echo $i ?>" size="35" readonly="true" value="<?php echo $jahit_detail[$j]['nm_brg'] ?>"/><input title="browse data barang" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" disabled
           onclick="javascript: var x= $('#kel_brg_<?php echo $i ?>').val(); var zz='xx'; openCenteredWindow('<?php echo base_url(); ?>index.php/sj-keluar-jahit/cform/show_popup_brg/'+x+'/<?php echo $i; ?>/'+zz);" ></td>
          
         <td><input readonly="true" name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="15" value="<?php echo $jahit_detail[$j]['qty_bhn'] ?>" /><input name="qty_lama_<?php echo $i ?>" type="hidden" id="qty_lama_<?php echo $i ?>" size="10" value="<?php echo $jahit_detail[$j]['qty_bhn'] ?>" />
         <input name="diprint_<?php echo $i ?>" type="hidden" id="diprint_<?php echo $i ?>" value="<?php echo $jahit_detail[$j]['diprint'] ?>" />
			<input name="is_dacron_<?php echo $i ?>" type="hidden" id="is_dacron_<?php echo $i ?>" value="<?php echo $jahit_detail[$j]['is_dacron'] ?>" />
			<input name="ukuran_<?php echo $i ?>" type="hidden" id="ukuran_<?php echo $i ?>" value="<?php echo $jahit_detail[$j]['ukuran'] ?>" />
			<input name="id_stok_<?php echo $i ?>" type="hidden" id="id_stok_<?php echo $i ?>" value="<?php echo $jahit_detail[$j]['id_stok'] ?>" />
         </td>
          <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="25" value="<?php echo $jahit_detail[$j]['keterangan'] ?>" /><input type="hidden" name="id_<?php echo $i ?>" id="id_<?php echo $i ?>" value="<?php echo $jahit_detail[$j]['id'] ?>" /></td>
        </tr>
		<?php $i++; } // end foreach ?>
        <?php } ?>
	</table>	
	</form>
      <div align="center"><br><br> 
        
        <?php if ($edit=='') { ?><input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-keluar-jahit/cform/view'"><?php } ?>
			
      </div></td>
    </tr>

</table>
</div>
</form>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function getbrgjadi(x){
	var	kel_brg	= document.getElementById('kel_brg_'+x).value;
	//if(kel_brg==5){
		alert(kel_brg);
	//}
}

function ckitem() {
	var tgl_sj = $("#tgl_sj").val();
	
	if (tgl_sj=='') {
		alert ("Tanggal SJ harus dipilih");
		$("#tgl_sj").focus();
		return false;
	}
	
	var jum	= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#kode_bhn_'+k).val() == '') {
				alert("Data item bhn baku tidak boleh ada yang kosong...!");
				return false;
			}
			if ($('#kode_brg_jd_'+k).val() == '') {
				alert("Data brg jadi tidak boleh ada yang kosong...!");
				return false;
			}
			if($('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh kosong...!");
				return false;
			}
			
			/*if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka atau desimal..!");
				return false;
			} */
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

function ckpp(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('sj-keluar-jahit/cform/cari_sj');?>",
	data:"nomorsj="+nomor,
	success: function(data){
		$("#confnomorpp").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

$(function()
{
	var goedit = $("#goedit").val();
	if(goedit!='1')
		$("#no").val('2');

	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();

		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		

		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);

		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		

		var kode_bhn="#kode_bhn_"+n;
		var new_kode_bhn="#kode_bhn_"+no;
		$(kode_bhn, lastRow).attr("id", "kode_bhn_"+no);
		$(new_kode_bhn, lastRow).attr("name", "kode_bhn_"+no);		
		$(new_kode_bhn, lastRow).val('');				

		var nm_brg="#nm_brg_"+n;
		var new_nm_brg="#nm_brg_"+no;
		$(nm_brg, lastRow).attr("id", "nm_brg_"+no);
		$(new_nm_brg, lastRow).attr("name", "nm_brg_"+no);		
		$(new_nm_brg, lastRow).val('');				

		var kode_brg_jd="#kode_brg_jd_"+n;
		var new_kode_brg_jd="#kode_brg_jd_"+no;
		$(kode_brg_jd, lastRow).attr("id", "kode_brg_jd_"+no);
		$(new_kode_brg_jd, lastRow).attr("name", "kode_brg_jd_"+no);		
		$(new_kode_brg_jd, lastRow).val('');
		
		//nama_brg_jd_1		
		var nama_brg_jd="#nama_brg_jd_"+n;
		var new_nama_brg_jd="#nama_brg_jd_"+no;
		$(nama_brg_jd, lastRow).attr("id", "nama_brg_jd_"+no);
		$(new_nama_brg_jd, lastRow).attr("name", "nama_brg_jd_"+no);		
		$(new_nama_brg_jd, lastRow).val('');

		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');			

		var qty_lama="#qty_lama_"+n;
		var new_qty_lama="#qty_lama_"+no;
		$(qty_lama, lastRow).attr("id", "qty_lama_"+no);
		$(new_qty_lama, lastRow).attr("name", "qty_lama_"+no);		
		$(new_qty_lama, lastRow).val('');
		
		var subtotal="#keterangan_"+n;
		var new_subtotal="#keterangan_"+no;
		$(subtotal, lastRow).attr("id", "keterangan_"+no);
		$(new_subtotal, lastRow).attr("name", "keterangan_"+no);		
		$(new_subtotal, lastRow).val('');				

		var subtotal="#id_"+n;
		var new_subtotal="#id_"+no;
		$(subtotal, lastRow).attr("id", "id_"+no);
		$(new_subtotal, lastRow).attr("name", "id_"+no);		
		$(new_subtotal, lastRow).val('');
		
		var diprint="#diprint_"+n;
		var new_diprint="#diprint_"+no;
		$(diprint, lastRow).attr("id", "diprint_"+no);
		$(new_diprint, lastRow).attr("name", "diprint_"+no);		
		$(new_diprint, lastRow).val('');
		
		var is_dacron="#is_dacron_"+n;
		var new_is_dacron="#is_dacron_"+no;
		$(is_dacron, lastRow).attr("id", "is_dacron_"+no);
		$(new_is_dacron, lastRow).attr("name", "is_dacron_"+no);		
		$(new_is_dacron, lastRow).val('');
		
		var ukuran="#ukuran_"+n;
		var new_ukuran="#ukuran_"+no;
		$(ukuran, lastRow).attr("id", "ukuran_"+no);
		$(new_ukuran, lastRow).attr("name", "ukuran_"+no);		
		$(new_ukuran, lastRow).val('0');
		
		var ukuranx="#ukuranx_"+n;
		var new_ukuranx="#ukuranx_"+no;
		$(ukuranx, lastRow).attr("id", "ukuranx_"+no);
		$(new_ukuranx, lastRow).attr("name", "ukuranx_"+no);		
		$(new_ukuranx, lastRow).val('');
		
		var qtyx="#qtyx_"+n;
		var new_qtyx="#qtyx_"+no;
		$(qtyx, lastRow).attr("id", "qtyx_"+no);
		$(new_qtyx, lastRow).attr("name", "qtyx_"+no);		
		$(new_qtyx, lastRow).val('');
		
		var id_stok="#id_stok_"+n;
		var new_id_stok="#id_stok_"+no;
		$(id_stok, lastRow).attr("id", "id_stok_"+no);
		$(new_id_stok, lastRow).attr("name", "id_stok_"+no);		
		$(new_id_stok, lastRow).val('');
		
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		// var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		 var  even_klik= "var x= $('#kel_brg_"+no+"').val(); var zz= 'xx'; openCenteredWindow('<?php echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/show_popup_brg/'+x+'/"+no+"/'+zz);";

		 $(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 //$(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 

		//
		 var pilih_brg="#pilih_brg_"+n;
		 var new_pilih_brg="#pilih_brg_"+no;
		 $(pilih_brg, lastRow).attr("id","pilih_brg_"+no);
		// var nama_for_even_brg="document.f_purchase.iddata2.value="+no;	
		
		 var  even_klik_brg= "var x= $('#kel_brg_"+no+"').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/show_popup_brg_jadi/'+x+'/"+no+"/');";

		 $(new_pilih_brg, lastRow).attr("name", "pilih_brg_"+no);		
		// $(new_pilih_brg, lastRow).attr("onmouseover",nama_for_even_brg);
		 $(new_pilih_brg, lastRow).attr("onclick",even_klik_brg);
		 			
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		var goedit = $("#goedit").val();
		var jumawal = $("#jumawal").val();
		
		if (goedit != 1) {
			if (x>2) {
				$("#tabelku tr:last").remove();	 
				var x= $("#no").val();	
				var y= x-1;
				$("#no").val(y);	
			}
		}
		else {
			if (parseInt(x) > parseInt(jumawal)) {
				$("#tabelku tr:last").remove();	 
				var x= $("#no").val();	
				var y= x-1;
				$("#no").val(y);	
			}
		}
	});
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 1000;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
