<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .judulnya {
		background-color:#DDD;
	}

</style>

<h3>Data SJ Keluar Untuk Proses Jahit</h3><br>
<a href="<?php echo base_url(); ?>index.php/sj-keluar-jahit-new/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('sj-keluar-jahit-new/cform/cari'); ?>
Unit Jahit <select name="kode_unit" id="kode_unit">
		<option value="0" <?php if ($kode_unit=="0") { ?> selected="true" <?php } ?> >- All -</option>
			<?php foreach ($list_unit as $unit) { ?>
				<option value="<?php echo $unit->kode_unit ?>" <?php if ($kode_unit==$unit->kode_unit) { ?> selected="true" <?php } ?> ><?php echo $unit->kode_unit." - ". $unit->nama ?></option>
			<?php } ?>
	</select>	
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <!--<th height="28px">Nomor SJ</th>-->
		 <th>Nomor SJ</th>
		 <th>Tanggal SJ</th>
		 <th>Nama Unit</th>
		 <th>Kel Bhn</th>
		 <th>Utk Barang Jadi</th>
		 <th width="147px">Bhn Baku</th>
		 <th>Qty Bhn</th>
		 <th>Ket Detail</th>
		 <th>Keterangan</th>
		 <th>Last Update</th>
		 <th width="71px">Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {
						 
			 	 $bl	= array(
				 	'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember'
				 );
				 
			 	 $etgl	= explode("-",$query[$j]['tgl_sj'],strlen($query[$j]['tgl_sj'])); // YYYY-mm-dd
				 $hr	= substr($etgl[2],0,1)==0?substr($etgl[2],1,1):$etgl[2];
				 $nbl	= $etgl[1];
				 $ntgl	= $hr." ".$bl[$nbl]." ".$etgl[0];
				 
				 $etgl_last	= explode("-",$query[$j]['tgl_update'],strlen($query[$j]['tgl_update'])); // YYYY-mm-dd
				 $hr_last	= substr($etgl_last[2],0,1)==0?substr($etgl_last[2],1,1):$etgl_last[2];
				 $nbl_last	= $etgl_last[1];
				 $ntgl_last	= $hr_last." ".$bl[$nbl_last]." ".$etgl_last[0];
				 
				 echo "<tr class=\"record\">";
				 echo "<td nowrap>".$query[$j]['no_sj']."</td>";
				 echo "<td>".$ntgl."</td>";
				 echo "<td>".$query[$j]['nama_unit']."</td>";
				//29-02-2012 echo "<td>".$query[$j]['nm_kel_brg']."</td>";
				// echo "<td nowrap colspan=\"4\" valign=\"top\">";

				 if (is_array($query[$j]['detail_jahit'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_jahit'];
					 $hitung = count($var_detail);
					
					echo "<td nowrap>";
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['nm_kel_brg'];
						if ($k<$hitung-1)
						     echo "<br> ";
					}
					echo "</td>";
					
					echo "<td nowrap>";
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nm_brg'];
						if ($k<$hitung-1)
						     echo "<br> ";
					}
					echo "</td>";
					
					echo "<td nowrap>";
					for($k=0;$k<count($var_detail); $k++){
						//echo $var_detail[$k]['kode_brg']."<br>";
						$list_kode_brg = explode(";", $var_detail[$k]['kode_brg']);
						$list_ukuran = explode(";", $var_detail[$k]['ukuran']);
						$list_qty_makloon = explode(";", $var_detail[$k]['qty_makloon_asesoris']);
						$list_diprint = explode(";", $var_detail[$k]['diprint']);
						$list_is_dacron = explode(";", $var_detail[$k]['is_dacron']);
						$jumlah_list = count($list_kode_brg)-1;
						
					for($zz=0; $zz<$jumlah_list; $zz++) {
						//if ($query[$j]['kel_bhn'] != '1') {
						if ($var_detail[$k]['kel_bhn'] != '1') {
							
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$list_kode_brg[$zz]' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$list_kode_brg[$zz]' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$nama_brg = '';
									$satuan = '';
								} // 
							}
						}
						else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$list_kode_brg[$zz]' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
						}
							
							if ($list_ukuran[$zz] != 0)
								$ukurannya = " (ukuran ".$list_ukuran[$zz]." cm, ".$list_qty_makloon[$zz]." pcs) ";
							else
								$ukurannya="";
							
							if ($list_is_dacron[$zz] == 't' && $var_detail[$k]['kel_bhn'] == 6)
								$dacronnya = "(Dacron) ";
							else if ($list_is_dacron[$zz] == 'f' && $var_detail[$k]['kel_bhn'] == 6)
								$dacronnya = "(Non-Dacron) ";
							else
								$dacronnya = "";
								
							if ($list_diprint[$zz] == 't' && $var_detail[$k]['kel_bhn'] == 6)
								$printnya = "(Print) ";
							else if ($list_diprint[$zz] == 'f' && $var_detail[$k]['kel_bhn'] == 6)
								$printnya = "";
							else
								$printnya="";
							
							echo $dacronnya.$printnya.$list_kode_brg[$zz]."-".$nama_brg.$ukurannya.";";
						
						} echo "<br>";
					}
					echo "</td>";
					
					echo "<td>";
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['qty_bhn']."<br>";
					}
					echo "</td>";
					
					echo "<td>";
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['keterangan']."<br>";
					}
					echo "</td>";
					
					/*echo "<table width=\"100%\">"; 
					for($k=0;$k<count($var_detail); $k++){
						if ($query[$j]['ukuran'] != 0)
							$ukurannya = " (ukuran ".$var_detail[$k]['ukuran']." cm) ";
												
						  if($query[$j]['kel_bhn']==5){
							if($var_detail[$k]['kode_brg_jadi']!=''){
								$detail_brg_jadi	= $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nm_brg'];
							}else{
								$detail_brg_jadi	= '';
							}
						  }else{
						  	$detail_brg_jadi	= $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nm_brg'];
						  }
						  
						  //echo "(".$var_detail[$k]['kel_bhn']."-".$var_detail[$k]['nm_kel_brg'].") (".$var_detail[$k]['kode_brg'].") (".$var_detail[$k]['qty_bhn'].") ".$detail_brg_jadi;
						  echo "<tr>
						  	<td width=\"119px\">".$var_detail[$k]['kel_bhn']."-".$var_detail[$k]['nm_kel_brg'];
						  	if($var_detail[$k]['kel_bhn']==6) {
								if ($var_detail[$k]['is_dacron'] == 't')
									echo " (Dacron) ";
								else
									echo " (Non-Dacron) ";
							}
						  	echo "</td>
							<td width=\"147px\">".$var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
							if ($query[$j]['kel_bhn'] == 6)
								echo " (Print) ";
							else if ($query[$j]['kel_bhn'] == 4)
								echo $ukurannya;
							echo "</td>
							<td width=\"64px\" align=\"right\">".$var_detail[$k]['qty_bhn']."&nbsp;&nbsp;&nbsp;</td>
							<td>".$detail_brg_jadi."</td>
						  </tr>";
				  
						 // if ($k<$hitung-1)
						    // echo "<br> ";
					}
					echo "</table>"; */
				 } // end if is array
				 
				// echo "</td>";
				 echo "<td>".$query[$j]['keterangan']."</td>";
				 echo "<td>".$ntgl_last."</td>";
				 echo "<td align=center>";

				if($query[$j]['status_jahit']=='f') {
					 echo "<a href=".base_url()."index.php/sj-keluar-jahit/cform/index/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a>";
					 echo "&nbsp; <a href=".base_url()."index.php/sj-keluar-jahit/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 }else{
				 	//echo "&nbsp; <a href=".base_url()."index.php/sj-keluar-jahit/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 	echo "&nbsp;";
				 }
				 echo "</td>";
				 echo "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links(); ?>
</div>
