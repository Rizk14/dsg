<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>

<body onLoad="closeMe();">

<?php
echo "Printer: ".$printer_name;

/***
Referensi 

ESC P = select 10.5 point, 10 cpi ======== CHR(27) CHR(80)
ESC M = select 10.5 point, 12 cpi ======== CHR(27) CHR(77)
ESC g = select 10.5 point, 15 cpi ======== CHR(27) CHR(103)
ESC p 1/0 = turn proportional mode on/ off  ======== CHR(27) CHR(112) CHR(1),, CHR(27) CHR(112) CHR(0)

Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
Italic		= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)
San Serif	= CHR(27).CHR(107).CHR(49)
Start Text	= CHR(2)
End Text	= CHR(3)
Set Bottom Margin			= CHR(27).CHR(78).CHR(2)
Cancel Set Bottom Margin	= CHR(27).CHR(79)

Paper Feeding :

ESC 0	: 48 : 1/8
ESC 2	: 50 : 1/6
ESC 3 n	: 51 : n/180
ESC + n	: 43 : n/360

ESC ! n : 33

n = 0 10 cpi
* = 1 12 cpi
* = 2 prop
* = 4 cond
* = 8 bold
* = 16 double-strike
* = 32 double-width
* = 64 italic
* = 128 underline

bilangan dari 0 s.d 9

0 CHR(48)
1 CHR(49)
2 CHR(50)
3 CHR(51)
4 CHR(52)
5 CHR(53)
6 CHR(54)
7 CHR(56)
8 CHR(57)
9 CHR(58)
***/

$get_attributes	= false;
$waktu	= date("H:i:s");
$line	= 80;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php");
}else{
	include_once("printipp_classes/BasicIPP.php");
}

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

for($jj=0; $jj<4; $jj++) {
    $ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18).CHR(0));
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",48).CHR(27).CHR(69).$nomorpajak."\n".CHR(27).CHR(70).CHR(18).CHR(0)); // ASALNYA 30
$ipp->printJob();

for($a=1;$a<5;$a++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18).CHR(0));
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).strtoupper($nminitial).CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).strtoupper($almtperusahaan).CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).strtoupper($npwpperusahaan).CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=3;$a++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).$nmkostumer.CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).$almtkostumer.CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).$npwp.CHR(27).CHR(70).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=4;$a++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$no			= 1;
$nolength	= 4;
$brglength	= 78;
$hrglength	= 15;
$arrnolenght	= array();
$arrbrglenght	= array();
$arrhrglenght	= array();
$minusnolength	= array();
$minusbrglength	= array();
$minushrglength	= array();
$tprice		= array();

$j = 0;

foreach($isi as $row) {

	if($no<23 && $row->motifname!='') {

		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);

		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$tprice[$j]	= $row_value->amount;
		} else {
			$tprice[$j]	= 0;
		}

		$jj	= $j+1;

		$arrnolenght[$j]	= strlen($jj);
		$arrbrglenght[$j]	= strlen($row->motifname);
		$arrhrglenght		= strlen(number_format($tprice[$j]));
		$minusnolength[$j]	= ($arrnolenght[$j] <= $nolength)?($nolength-$arrnolenght[$j]):0;
		$minusbrglength[$j]	= $brglength-$arrbrglenght[$j];
		$minushrglength[$j]	= $hrglength-$arrhrglenght;

		$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$j]+4)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$j]+45)).str_repeat(" ",$minushrglength[$j])." ".number_format($tprice[$j]).CHR(18).CHR(0)."\n"); // 6 4 45
		$ipp->printJob();
		$j+=1;
		$no+=1;
	}
}

if($no<=29) {

	$akhr	= $no;

	for($loop=$akhr;$loop<=29;$loop++) {
		$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
		$ipp->printJob();
	}
}else{
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$lengthjml	= strlen(number_format($jumlah));
$lengthdis	= strlen(number_format($diskon));
$lengthdpp	= strlen(number_format($dpp));
$lengthppn	= strlen(number_format($nilai_ppn));

$spcjml	= $line-$lengthjml;
$spcdis	= $line-$lengthdis;
$spcdpp	= $line-$lengthdpp;
$spcppn	= $line-$lengthppn;

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",18)."XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".str_repeat(" ",$spcjml+18).CHR(2).number_format($jumlah)."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcdis+72).number_format($diskon)."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcdpp+72).number_format($dpp)."\n".CHR(18));
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15).str_repeat(" ",$spcppn+72).number_format($nilai_ppn)."\n".CHR(18));
$ipp->printJob();

$footalamat	= strlen($falamat);
$foottgl	= strlen('Tanggal');
$footshtgl	= strlen($tglfaktur);

for($jj=0;$jj<1;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." "."\n".CHR(18).CHR(0));
	$ipp->printJob();
}

$footsisa_grs	= $line-($footalamat+1+$foottgl+1+1+$footshtgl);
$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footsisa_grs+5)).CHR(27).CHR(69).$falamat."            ".$tglfaktur."\n".CHR(27).CHR(70));
$ipp->printJob();

$footnm	= $line-24;
$footjbt= $line-25;

for($jj=0;$jj<5;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footnm+8))." ".CHR(27).CHR(69).$TtdPajak01.CHR(27).CHR(70));
$ipp->printJob();

$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18)."\n".CHR(0));
$ipp->printJob();

for($jj=1;$jj<=2;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footjbt+7))." ".CHR(27).CHR(69)."Kasie Accounting".CHR(27).CHR(70).CHR(0));
$ipp->printJob();

/* Disabled 01112011
for($jj=0;$jj<12;$jj++) {
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." "."\n".CHR(18));
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}
*/

?>
</body>
