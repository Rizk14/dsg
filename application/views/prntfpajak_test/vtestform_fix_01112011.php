<script language="javascript" type="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>

<body onLoad="closeMe();">

<?php
echo "Printer: ".$printer_name;

/*
Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
Italic	= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)
San Serif	= CHR(27).CHR(107).CHR(49)
Start Text	= CHR(2)
End Text	= CHR(3)
Set Bottom Margin	= CHR(27).CHR(78).CHR(2)
Cancel Set Bottom Margin	= CHR(27).CHR(79)

Paper Feeding :
ESC 0	: 48 : 1/8
ESC 2	: 50 : 1/6
ESC 3 n	: 51 : n/180
ESC + n	: 43 : n/360

ESC ! n : 33 

n = 0 10 cpi
* = 1 12 cpi
* = 2 prop
* = 4 cond
* = 8 bold
* = 16 double-strike
* = 32 double-width
* = 64 itaic
* = 128 underline
*/

$get_attributes	= false;
$waktu	= date("H:i:s");
$line	= 80;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php");
} else {
	include_once("printipp_classes/BasicIPP.php");
}

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

for($jj=0;$jj<4;$jj++){
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." ".CHR(18)."\n");
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",30).CHR(27).CHR(69).$nomorpajak.CHR(27).CHR(70)."\n");
$ipp->printJob();

for($a=1;$a<=2;$a++){
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." ".CHR(18)."\n");
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
	//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).CHR(2)." "."\n".CHR(18).CHR(3));
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3)); // asal 4 ubah 6
	//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",24).CHR(27).CHR(69)." "."\n".CHR(27).CHR(70));
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",29).CHR(27).CHR(69).strtoupper($nminitial).CHR(27).CHR(70)."\n");
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).strtoupper($nminitial).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",29).CHR(27).CHR(69).strtoupper($almtperusahaan).CHR(27).CHR(70)."\n");
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).strtoupper($almtperusahaan).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",29).CHR(27).CHR(69).strtoupper($npwpperusahaan).CHR(27).CHR(70)."\n");
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).strtoupper($npwpperusahaan).CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=3;$a++) {
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." "."\n".CHR(18));
	//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).CHR(2)." "."\n".CHR(18).CHR(3));
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",29).CHR(27).CHR(69).$nmkostumer.CHR(27).CHR(70)."\n");
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).$nmkostumer.CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",29).CHR(27).CHR(69).$almtkostumer.CHR(27).CHR(70)."\n");
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).$almtkostumer.CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",29).CHR(27).CHR(69).$npwp.CHR(27).CHR(70)."\n");
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15).str_repeat(" ",43).CHR(27).CHR(69).$npwp.CHR(18).CHR(0)."\n");
$ipp->printJob();
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

for($a=1;$a<=4;$a++) {
	//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).CHR(2)." "."\n".CHR(18).CHR(3));
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$no		= 1;
$nolength	= 4;
$brglength	= 78;
$hrglength	= 15;
$arrnolenght	= array();
$arrbrglenght	= array();
$arrhrglenght	= array();
$minusnolength	= array();
$minusbrglength	= array();
$minushrglength	= array();
$tprice		= array();

$j = 0;
foreach($isi as $row) {
	if($no<23 && $row->motifname!='') {
		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
		
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$tprice[$j]	= $row_value->amount;
		} else {
			$tprice[$j]	= 0;		
		}

		$jj	= $j+1;
		
		$arrnolenght[$j]	= strlen($jj);
		$arrbrglenght[$j]	= strlen($row->motifname);
		$arrhrglenght	= strlen(number_format($tprice[$j]));
		$minusnolength[$j]	= ($arrnolenght[$j] <= $nolength)?($nolength-$arrnolenght[$j]):0;
		$minusbrglength[$j]	= $brglength-$arrbrglenght[$j];
		$minushrglength[$j]	= $hrglength-$arrhrglenght;
		//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$j]+1)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$j]+25)).str_repeat(" ",$minushrglength[$j])." ".number_format($tprice[$j]).CHR(18).CHR(3)."\n");
		//$ipp->setData(CHR(1).CHR(27).CHR(80).CHR(15).CHR(27).CHR(83).CHR(49).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$j]+1)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$j]+25)).str_repeat(" ",$minushrglength[$j])." ".number_format($tprice[$j]).CHR(84).CHR(18).CHR(0)."\n");
		//$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(80).CHR(27).CHR(83).CHR(49).str_repeat(" ",6).strtoupper($row->motifname).CHR(84).CHR(0)."\n");
		//$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(27).CHR(83).CHR(49).str_repeat(" ",6).strtoupper($row->motifname).CHR(84).CHR(0)."\n");
		//$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).str_repeat(" ",6).strtoupper($row->motifname).CHR(0)."\n");
		//$ipp->setData(CHR(1).CHR(27).CHR(33).CHR(0).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$j]+1)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$j]+25)).str_repeat(" ",$minushrglength[$j])." ".number_format($tprice[$j]).CHR(0)."\n");
		//$ipp->setData(CHR(1).CHR(27).CHR(33).CHR(0).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$j]+1)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$j]+25)).str_repeat(" ",$minushrglength[$j])." ".number_format($tprice[$j]).CHR(18).CHR(0)."\n");
		$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15).str_repeat(" ",6).$no.str_repeat(" ",($minusnolength[$j]+4)).strtoupper($row->motifname).str_repeat(" ",($minusbrglength[$j]+45)).str_repeat(" ",$minushrglength[$j])." ".number_format($tprice[$j]).CHR(18).CHR(0)."\n");
		$ipp->printJob();
		$j+=1;
		$no+=1;
	}
}

//if($no<=22) {
if($no<=29) {
	$akhr	= $no;
	//for($loop=$akhr;$loop<=22;$loop++) {
	for($loop=$akhr;$loop<=29;$loop++) {	
		//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
		//$ipp->printJob();
		$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
		$ipp->printJob();
			
	}
}else{
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
	//$ipp->printJob();

	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$lengthjml	= strlen(number_format($jumlah));
$lengthdis	= strlen(number_format($diskon));
$lengthdpp	= strlen(number_format($dpp));
$lengthppn	= strlen(number_format($nilai_ppn));

$spcjml	= $line-$lengthjml;
$spcdis	= $line-$lengthdis;
$spcdpp	= $line-$lengthdpp;
$spcppn	= $line-$lengthppn;

//$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)."                         ".CHR(18).CHR(0)."\n");
//$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",18)."XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".str_repeat(" ",$spcjml+19).CHR(2).number_format($jumlah).CHR(18).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",$spcdis+71).CHR(2).number_format($diskon).CHR(18).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).CHR(2)." ".CHR(18).CHR(3)."\n"); 
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",$spcdpp+71).CHR(2).number_format($dpp).CHR(18).CHR(3)."\n");
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).str_repeat(" ",$spcppn+71).CHR(2).number_format($nilai_ppn).CHR(18).CHR(3)."\n");
$ipp->printJob();

$footalamat	= strlen($falamat);
$foottgl	= strlen('Tanggal');
$footshtgl	= strlen($tglfaktur);

for($jj=0;$jj<1;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$footsisa_grs	= $line-($footalamat+1+$foottgl+1+1+$footshtgl);
$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footsisa_grs+5)).CHR(27).CHR(69).$falamat." "."         "." ".$tglfaktur.CHR(27).CHR(70)."\n");
$ipp->printJob();

$footnm	= $line-24;
$footjbt= $line-25;

//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "." ".CHR(18).CHR(3)."\n");
//$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "." ".CHR(18).CHR(3)."\n");
//$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "." ".CHR(18).CHR(3)."\n");
//$ipp->printJob();

for($jj=0;$jj<5;$jj++) {
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." ".CHR(18)."\n");
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footnm+8))." ".CHR(27).CHR(69).$TtdPajak01.CHR(27).CHR(70));
$ipp->printJob();

//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." ".CHR(18)."\n");
//$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",24).CHR(27).CHR(69)." ".CHR(27).CHR(70)."\n");
//$ipp->printJob();
//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
$ipp->printJob();

//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3)); 
for($jj=1;$jj<=2;$jj++) {
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(50).str_repeat(" ",($footjbt+7))." ".CHR(27).CHR(69)."Kasie Accounting".CHR(27).CHR(70));
$ipp->printJob();

/* Disabled 01112011
for($jj=0;$jj<12;$jj++){
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(15)." "."\n".CHR(18));
	//$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
	$ipp->setData(CHR(27).CHR(48).CHR(1).CHR(27).CHR(77).CHR(15)." ".CHR(18).CHR(0)."\n");
	$ipp->printJob();
}
*/

?>
</body>
