<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_penjualanndo; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_penjualanndo; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'expopenjualanndo/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlpenjualanndoform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_penjualanndo_no_faktur; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="no_faktur" type="text" id="no_faktur" maxlength="14" value="<?=$nofaktur?>"/>
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_penjualanndo_tgl_faktur_mulai; ?> </td>
					<td>:</td>
					<td>
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?=$tgldomulai?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?=$tgldoakhir?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>				  
				  <!--
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><?php //echo $list_penjualanndo_jenis_brg; ?></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>
					  <select name="e_category">
					  <?php
					  /*
					  foreach($opt_jns_brg as $row) {
					  	$selected = $kategori==$row->i_class?" selected ":"";
					  	$ljnsbrg .= "<option value=".$row->i_class." ".$selected.">".$row->e_class_name."</option>";
					  }
					  echo $ljnsbrg;
					  */
					  ?>
					  </select>					  
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>
					<input type="checkbox" name="f_stop_produksi" value="1" <?php //echo $sproduksi; ?>/>
					<?php //echo $list_penjualanndo_s_produksi; ?></td>
				  </tr>	
				  -->			  				  
				</table></td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_penjualanndo; ?></div></td>	
			</tr>
			<tr>
			  <tr>
				<td><table class="table table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="11%" class="tdatahead"><?php echo $list_penjualanndo_no_do; ?> </td>
					<td width="11%" class="tdatahead"><?php echo strtoupper($list_penjualanndo_kd_brg); ?> </td>
					<td width="32%" class="tdatahead"><?php echo $list_penjualanndo_nm_brg; ?> </td>
					<td width="8%" class="tdatahead"><?php echo $list_penjualanndo_qty; ?> </td>
					<td width="12%" class="tdatahead"><?php echo $list_penjualanndo_hjp; ?></td>
					<td width="14%" class="tdatahead"><?php echo $list_penjualanndo_amount; ?></td>
					<td width="10%" class="tdatahead"><?php echo 'Disc %'; ?></td>
					<td width="10%" class="tdatahead"><?php echo 'Disc Rp.'; ?></td>
				  </tr>
				  <?php
				  $totalqty	= 0;
				  $totalpenjualan	= 0;
				  				  
				  if(sizeof($query) > 0 ) {
					  $no	= 1;
					  $cc	= 1;
					  foreach($query as $row) {
						
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						/* 16112011				  
						$lpenjndo .= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no."</td>
								<td>".$row->imotif."</td>
								<td>".$row->motifname."</td>
								<td align=\"right\">".$row->qty."</td>
								<td align=\"right\">".number_format($row->unitprice,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->amount,'2','.',',')."</td>								
							  </tr>";	
							  $no+=1;	
							  $cc++;
							  $totalqty+=$row->qty;
							  $totalpenjualan+=$row->amount;
					  }
					  echo $lpenjndo;
					  */

						$lpenjndo	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->sjcode."</td>
								<td>".$row->imotif."</td>
								<td>".$row->motifname."</td>
								<td align=\"right\">".$row->qty."</td>
								<td align=\"right\">".number_format($row->unitprice,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->amount,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->ndisc,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->vdisc,'2','.',',')."</td>
							  </tr>";	
							  $no+=1;
							  $cc+=1;
							  $totalqty+=$row->qty;
							  $totalpenjualan+=$row->amount;	  
					  }
					  echo $lpenjndo;
					  					   
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <!-- 
			  <tr>
				<td align="right"><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="40%" align="right"><?php //echo $list_penjualanndo_total_pengiriman; ?> </td>
					<td width="1%">:</td>
					<td width="20%">
					  <input name="v_t_pengiriman" type="text" id="v_t_pengiriman" maxlength="50" value="<?php //echo $totalqty; ?>"/>
					</td>
					<td width="20%" align="right"><?php //echo $list_penjualanndo_total_penjualan; ?></td>
					<td width="1%">:</td>
					<td width="20%" align="right">
					  <input name="v_t_penjualan" type="text" id="v_t_penjualan" maxlength="50" value="<?php //echo number_format($totalpenjualan,'2','.',','); ?>" style="text-align:right;" />
					</td>
					</tr>
				</table></td>
			  </tr>
			  -->
			  <tr>
				<td align="center"><?php echo $create_link; ?></td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
				  <input name="btnlaporan" type="button" id="btnlaporan" value="Laporan Penjualan Tertentu" onclick="window.location='<?php echo base_url(); ?>index.php/expopenjualanndo/cform/gexportpenjualannondo/<?=$nofak?>/<?=$tfirst?>/<?=$tlast?>'" />
				   <input name="btnlaporan" type="button" id="btnlaporan" value="Laporan Penjualan Tertentu as Nomor Faktur Pajak" onclick="window.location='<?php echo base_url(); ?>index.php/expopenjualanndo/cform/gexportpenjualannondoasnofakpajak/<?=$nofak?>/<?=$tfirst?>/<?=$tlast?>'" />
				  <!--
				  <input name="btnglobal" type="button" id="btnglobal" value="Global" />
				  <input name="btnrealisasi" type="button" id="btnrealisasi" value="Realisasi Target" /> 
				  -->
			
				<input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/expopenjualanndo/cform/'">

				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close()?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
