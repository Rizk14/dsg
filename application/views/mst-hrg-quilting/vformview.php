<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Harga Bahan Quilting Berdasarkan Unit Makloon</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-hrg-quilting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-hrg-quilting/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-hrg-quilting/cform/cari'); ?>
Unit Makloon
				<select name="supplier" id="supplier">
					<option value="0" <?php if ($esupplier == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($sup->id == $esupplier) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ".$sup->nama ?></option>
				<?php } ?>
				</select>
<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		 <th>Unit Makloon</th>
		 <th>Kode & Nama Bhn Quilting</th>
		 <th>Satuan</th>
		 <th>Harga (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode_supplier - $row->nama_sup</td>";
				 echo    "<td nowrap>$row->kode_brg - $row->nama_brg</td>";
				 
				 $query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE a.satuan = b.id AND a.id = '$row->id_brg_quilting' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$satuan	= '';
				}
						
				 echo    "<td>$satuan</td>";
				 echo    "<td align='right'>".number_format($row->harga, 2, ',','.')."</td>";
				 echo    "<td>$tgl_update</td>";
				 echo    "<td align=center><a href=".base_url()."index.php/mst-hrg-quilting/cform/edit/$row->id/".$cur_page."/".$is_cari."/".$esupplier."/".$cari." \" >Edit</a>&nbsp;";
				 echo    "<a href=".base_url()."index.php/mst-hrg-quilting/cform/delete/$row->id/".$cur_page."/".$is_cari."/".$esupplier."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table><br>
<? echo $this->pagination->create_links();?>
</div>
