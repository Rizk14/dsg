<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function cek_input() {
	var angka_faktor_konversi= $('#angka_faktor_konversi').val();
	var id_satuan_konversi= $('#id_satuan_konversi').val();
	var rumus_konversi= $('#rumus_konversi').val();
	var konversi_harga_mutasi_stok= $('#konversi_harga_mutasi_stok').val();
	
	if (id_satuan_konversi == '0') {
		alert("Satuan konversi harus dipilih..!");
		return false;
	}
	if (angka_faktor_konversi == '0' || angka_faktor_konversi == '') {
		alert("Angka faktor konversi tidak boleh kosong / 0..!");
		return false;
	}
	if (isNaN(angka_faktor_konversi)) {
		alert("Angka faktor konversi harus berupa angka atau desimal..!");
		return false;
	}
	
	if (rumus_konversi == '0') {
		alert("Rumus konversi harus dipilih..!");
		return false;
	}
	if (konversi_harga_mutasi_stok == '0') {
		alert("Konversi Harga Di Laporan Mutasi Stok harus dipilih..!");
		return false;
	}
}
</script>

<h3>Data Setting Konversi Satuan</h3><br>
<?php echo form_open('setting/cform/submitkonversisatuan'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="id_konversi" value="<?php echo $eid ?>"><?php } ?>
	<table>
		<tr>
			<td>Satuan</td>
			<td>
			 <select name="id_satuan_awal" id="id_satuan_awal">
				<?php foreach ($satuan as $sat) { ?>
					<option value="<?php echo $sat->id ?>" <?php if ($sat->id == $eid_satuan_awal) { ?> selected="true" <?php } ?> ><?php echo $sat->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Satuan Konversi</td>
			<td>
			 <select name="id_satuan_konversi" id="id_satuan_konversi">
				 <option value="0" <?php if ($eid_satuan_konversi == '0') { ?>selected<?php } ?>>-Pilih-</option>
				<?php foreach ($satuan as $sat) { ?>
					<option value="<?php echo $sat->id ?>" <?php if ($sat->id == $eid_satuan_konversi) { ?> selected="true" <?php } ?> ><?php echo $sat->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Angka Faktor Konversi</td>
			<td><input type="text" size="3" style="text-align:right;" name="angka_faktor_konversi" id="angka_faktor_konversi" value="<?php if ($eid_satuan_konversi != '0') echo $eangka_faktor_konversi; else echo "0"; ?>"></td>
		</tr>
		<tr>
			<td>Rumus Konversi</td>
			<td>
			 <select name="rumus_konversi" id="rumus_konversi">
				 <option value="0" <?php if ($erumus_konversi == '0') { ?>selected<?php } ?>>-Pilih-</option>
				 <option value="1" <?php if ($erumus_konversi == '1') { ?>selected<?php } ?>>Dikali (Qty Konv = Qty Sat Awal x Angka Faktor)</option>
				 <option value="2" <?php if ($erumus_konversi == '2') { ?>selected<?php } ?>>Dibagi (Qty Konv = Qty Sat Awal / Angka Faktor)</option>
				 <option value="3" <?php if ($erumus_konversi == '3') { ?>selected<?php } ?>>Ditambah (Qty Konv = Qty Sat Awal + Angka Faktor)</option>
				 <option value="4" <?php if ($erumus_konversi == '4') { ?>selected<?php } ?>>Dikurang (Qty Konv = Qty Sat Awal - Angka Faktor)</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Konversi Harga Di Laporan Mutasi Stok</td>
			<td>
			 <select name="konversi_harga_mutasi_stok" id="konversi_harga_mutasi_stok">
				 <option value="0" <?php if ($ekonversi_harga_mutasi_stok == '0') { ?>selected<?php } ?>>-Pilih-</option>
				 <option value="1" <?php if ($ekonversi_harga_mutasi_stok == '1') { ?>selected<?php } ?>>Dikali (Harga Konv = Harga x Angka Faktor)</option>
				 <option value="2" <?php if ($ekonversi_harga_mutasi_stok == '2') { ?>selected<?php } ?>>Dibagi (Harga Konv = Harga / Angka Faktor)</option>
				 <option value="3" <?php if ($ekonversi_harga_mutasi_stok == '3') { ?>selected<?php } ?>>Ditambah (Harga Konv = Harga + Angka Faktor)</option>
				 <option value="4" <?php if ($ekonversi_harga_mutasi_stok == '4') { ?>selected<?php } ?>>Dikurang (Harga Konv = Harga - Angka Faktor)</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_input();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/setting/cform/konversisatuan'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

<div>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		 <th>Satuan Awal</th>
		 <th>Satuan Konversi</th>
		 <th>Angka Faktor Konversi</th>
		 <th>Rumus Konversi</th>
		 <th>Konversi Harga<br>Di Laporan Mutasi Stok</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				$pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;				 
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 
				 $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id='".$row->id_satuan_awal."' ");
				 if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan_awal = $hasilrow->nama;
				 }
				 echo    "<td>".$nama_satuan_awal."</td>";
				 
				 if ($row->id_satuan_konversi != '0') {
					 $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id='".$row->id_satuan_konversi."' ");
					 if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konversi = $hasilrow->nama;
					 }
				 }
				 else
					$nama_satuan_konversi = 'Tidak Ada';
				 echo    "<td>".$nama_satuan_konversi."</td>";
				 
				 echo    "<td align='right'>$row->angka_faktor_konversi</td>";
				 
				 echo "<td>";
				 if ($row->rumus_konversi == '0')
					echo "Tidak Ada";
				 else if ($row->rumus_konversi == '1')
					echo "Dikali (Qty Konv = Qty Sat Awal x Angka Faktor)";
				 else if ($row->rumus_konversi == '2')
					echo "Dibagi (Qty Konv = Qty Sat Awal / Angka Faktor)";
				 else if ($row->rumus_konversi == '3')
					echo "Ditambah (Qty Konv = Qty Sat Awal + Angka Faktor)";
				 else
					echo "Dikurang (Qty Konv = Qty Sat Awal - Angka Faktor)";
				 echo "</td>";
				 
				 echo "<td>";
				 if ($row->konversi_harga_mutasi_stok == '0')
					echo "Tidak Ada";
				 else if ($row->konversi_harga_mutasi_stok == '1')
					echo "Dikali (Harga Konv = Harga x Angka Faktor)";
				 else if ($row->konversi_harga_mutasi_stok == '2')
					echo "Dibagi (Harga Konv = Harga / Angka Faktor)";
				 else if ($row->konversi_harga_mutasi_stok == '3')
					echo "Ditambah (Harga Konv = Harga + Angka Faktor)";
				 else
					echo "Dikurang (Harga Konv = Harga - Angka Faktor)";
				 echo "</td>";
				 
				 echo    "<td>$tgl_update</td>";
				 
				 echo    "<td align=center><a href=".base_url()."index.php/setting/cform/konversisatuan/$row->id \" >Edit</a>";
				 echo    "&nbsp; <a href=".base_url()."index.php/setting/cform/deletekonversisatuan/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table>
</div>
