<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function cek_data() {
	var nama= $('#nama').val();
	var alamat= $('#alamat').val();
	var kepala_bagian= $('#kepala_bagian').val();
	var bag_pembelian= $('#bag_pembelian').val();
	var bag_keuangan= $('#bag_keuangan').val();
	var bag_admstok= $('#bag_admstok').val();

	if (nama == '') {
		alert("Nama perusahaan harus diisi..!");
		$('#nama').focus();
		return false;
	}
	if (alamat == '') {
		alert("Alamat perusahaan harus diisi..!");
		$('#nama').focus();
		return false;
	}
	if (kepala_bagian == '') {
		alert("Kepala bagian harus diisi..!");
		$('#kepala_bagian').focus();
		return false;
	}
	if (bag_pembelian == '') {
		alert("Staf bagian pembelian harus diisi..!");
		$('#bag_pembelian').focus();
		return false;
	}
	if (bag_keuangan == '') {
		alert("Staf bagian keuangan harus diisi..!");
		$('#bag_keuangan').focus();
		return false;
	}
	if (bag_admstok == '') {
		alert("Staf bagian adm stok harus diisi..!");
		$('#bag_admstok').focus();
		return false;
	}
	
}
	
</script>

<h3>Setting Perusahaan</h3><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_setting', 'id' => 'f_setting');
	echo form_open('setting/cform/', $attributes); ?>
	<input type="hidden" name="id" value="<?php echo $datasetting['id'] ?>">
	<table>
		<tr>
			<td>Nama Perusahaan</td>
			<td> : <input type="text" name="nama" id="nama" value="<?php echo $datasetting['nama'] ?>" size="20"></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td> : <input type="text" name="alamat" id="alamat" value="<?php echo $datasetting['alamat'] ?>" size="70"></td>
		</tr>
		<tr>
			<td>Kota</td>
			<td> : <input type="text" name="kota" id="kota" value="<?php echo $datasetting['kota'] ?>" size="10"></td>
		</tr>
		<tr>
			<td>Nama Kepala Bagian</td>
			<td> : <input type="text" name="kepala_bagian" id="kepala_bagian" value="<?php echo $datasetting['kepala_bagian'] ?>" size="30"></td>
		</tr>
		<tr>
			<td>Nama Staf Bagian Pembelian</td>
			<td> : <input type="text" name="bag_pembelian" id="bag_pembelian" value="<?php echo $datasetting['bag_pembelian'] ?>" size="30"></td>
		</tr>
		<tr>
			<td>Nama Staf Bagian Pembelian 2</td>
			<td> : <input type="text" name="bag_pembelian2" id="bag_pembelian2" value="<?php echo $datasetting['bag_pembelian2'] ?>" size="30"></td>
		</tr>
		<tr>
			<td>Nama Staf Bagian Keuangan</td>
			<td> : <input type="text" name="bag_keuangan" id="bag_keuangan" value="<?php echo $datasetting['bag_keuangan'] ?>" size="30"></td>
		</tr>
		<tr>
			<td>Nama Spv Bagian Admin Stok</td>
			<td> : <input type="text" name="spv_bag_admstok" id="spv_bag_admstok" value="<?php echo $datasetting['spv_bag_admstok'] ?>" size="30"></td>
		</tr>
		<tr>
			<td>Nama Staf Bagian Adm Stok 1</td>
			<td> : <input type="text" name="bag_admstok" id="bag_admstok" value="<?php echo $datasetting['bag_admstok'] ?>" size="30"></td>
		</tr>
		<tr>
			<td>Gudang Bagian Adm Stok 1</td>
			<td> <?php 
					foreach ($list_gudang as $gud) {
				?>
						<input type="checkbox" name="id_gudang_admstok[]" value="<?php echo $gud->id ?>" 
						<?php
							if (is_array($list_id_gudang_admstok)) {
								foreach($list_id_gudang_admstok as $a1) {
									if ($a1 != '') {
										$a1 = trim($a1);
										if ($gud->id == $a1) {
											echo "checked='true'";
										}
									}
								}
							}
						?>
						> <?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama."<br>"; ?>
				
				<?php
					}
			 ?></td>
		</tr>
		
		<tr>
			<td>Nama Staf Bagian Adm Stok 2</td>
			<td> : <input type="text" name="bag_admstok2" id="bag_admstok2" value="<?php echo $datasetting['bag_admstok2'] ?>" size="30"></td>
		</tr>
		
		<tr>
			<td>Gudang Bagian Adm Stok 2</td>
			<td> <?php 
					foreach ($list_gudang as $gud) {
				?>
						<input type="checkbox" name="id_gudang_admstok2[]" value="<?php echo $gud->id ?>" 
						<?php
							if (is_array($list_id_gudang_admstok2)) {
								foreach($list_id_gudang_admstok2 as $a2) {
									if ($a2 != '') {
										$a2 = trim($a2);
										if ($gud->id == $a2) {
											echo "checked='true'";
										}
									}
								}
							}
						?>
						> <?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama."<br>"; ?>
				
				<?php
					}
			 ?></td>
		</tr>
		<tr>
			<td>Nama Staf Bagian Adm Stok 3</td>
			<td> : <input type="text" name="bag_admstok2" id="bag_admstok2" value="<?php echo $datasetting['bag_admstok2'] ?>" size="30"></td>
		</tr>
		<tr>
			<td>Gudang Bagian Adm Stok 3</td>
			<td> <?php 
					foreach ($list_gudang as $gud) {
				?>
						<input type="checkbox" name="id_gudang_admstok2[]" value="<?php echo $gud->id ?>" 
						<?php
							if (is_array($list_id_gudang_admstok2)) {
								foreach($list_id_gudang_admstok2 as $a2) {
									if ($a2 != '') {
										$a2 = trim($a2);
										if ($gud->id == $a2) {
											echo "checked='true'";
										}
									}
								}
							}
						?>
						> <?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama."<br>"; ?>
				
				<?php
					}
			 ?></td>
		</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset"></td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

