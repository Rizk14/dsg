<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif;
}
   
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    
} 
    .isinya2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;

}

</style>

<style type="text/css" media="print">
    printable {page-break-after: always;}
</style>
<!-- media="screen" means these styles will only be used by screen 
  devices (e.g. monitors) -->
<style type="text/css" media="screen">
    .printable{
        page-break-after: always;
margin: 0;
    }
</style>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">

$(function()
{
	var go_print= jQuery('#go_print').val();
	
	if (go_print == '1')
		window.print();
});


</script>

<?php
	//print_r($query);
	$tgl =date("j F Y");;

?>
<input type="hidden" name="go_print" id="go_print" value="1" >

<table class="isinya2" border='1' align="center" width="100%">
	<tr>
		<td>
			<table>
				<tr>
				<td colspan ="3" align ="left"><img src="<?php echo base_url().'images/logo.jpg'; ?>" style="width:84px;height:68px;" border="0"></td>
				<td ><p align='center' ><font size="3"><b> NOTA RETUR </b></font></p></td>
				<td>&nbsp;</td>
			
		
				</tr>
	<tr>
			<td colspan="3"> <b><?php echo $datasetting['nama'] ?></b> </td>
			<td>&nbsp;</td>
			<td align="right" style="white-space:nowrap;"><?php echo $datasetting['kota'] ?>, <?php echo $tgl; ?></td>
		</tr>
		<tr>
			<td colspan="3"><?php echo $datasetting['alamat'] ?></td>
			<td>Kepada Yth.</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3"><?php echo $datasetting['kota'] ?></td>
			<td><?php echo $query[0]['perusahaan'] ?> (U/p. <?php echo $query[0]['kontak_person'] ?>)</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" align="center"><b></b></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">Dengan hormat,</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">Kami akan mengembalikan barang ini:</td> 
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	<tr>
			<td colspan="5">
				<table border='1' class="isinya2" width="90%" >
				<thead>
	 <tr class="judulnya">
		 <th rowspan="2" width="1%">No</th>
		<!-- <th rowspan="2" width="10%">Perusahaan</th> --->
		 <th rowspan="2" width="5%">No Faktur</th>
		 <th rowspan="2" width="9%">Tgl Faktur</th>
		 <th rowspan="2" width="40%">No Surat Jalan / Tanggal SJ</th>  
		 <th colspan="6" width="35%">List Brg Jadi</th>		 
		 <th rowspan="2" width="10%">Grand Total</th>
	 </tr>
	 <tr class="judulnya">
		<th>Kode Brg Jadi</th>
		<th>Nama Brg Jadi</th>
		<th>Sat</th>
		<th>Qty</th>
		<th>Harga (Rp.)</th>
	<!--	<th>Diskon</th> --->
		<th>Subtotal</th>
	 </tr>
	 </thead>
	 <tbody>
	<?php 
				
			if (is_array($query)) {
				
				$tot_grandtotal = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					//$tot_grandtotal += $query[$j]['grandtotal'];
				} // end header
				
			}
			else {
				$tot_grandtotal = 0;
			} 
			
	
			$i=0;
			$halaman = 1; 
			if (count($query[0]['detail_beli'])==0) {
				
			?>
			<tr align="center">
			  <td align="center" id="num_1">1</td>
			  <td nowrap="nowrap">
			   Data tidak ada</td>
			  
			</tr>
			
			<?php
			}
			else {
				if (is_array($query)) {
				$no = 1;
				$detailnya = count($query[0]['detail_beli']);
				
				for($j=0;$j<count($query);$j++){
				echo "<tr class=\"record\">";
				echo    "<td align='center'>".$no."</td>";
				echo    "<td nowrap>".$query[$j]['no_faktur']."</td>";
				echo    "<td>".$query[$j]['tgl_faktur']."</td>";
				echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_sj']."/".$var_detail[$k]['tgl_sjkeluar'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				// echo    "<td></td>";
				
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
 
				 echo "<td align='center'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
					 echo number_format($var_detail[$k]['qty'], 0,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
			//	 echo    "<td></td>";
				 echo "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga'], 2,',','.');
						echo  "" ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['subtotal'],2,',','.');
						 echo ""   ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo    "<td align='right'>".number_format($query[$j]['grandtotal'],2,',','.')."</td>";
				$tot_grandtotal+=$query[$j]['grandtotal'];

				 echo  "</tr>";
				 
				 $no++;
			$i++; 	
			
			 }
				 ?>
			 	</tbody>
		<?php
			}		
				} 
			
			?>
		</table>
			<tr>
			<td colspan="2" align="center" >Hormat Kami,</td>
			
		</tr>
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">Dibuat<br>Bag. Pembelian</td>
			<td colspan="2" align="center">Penerima</td>
			<td colspan="2" align="center">Mengetahui, <br>Bag. Keuangan</td>
		</tr>
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><u><?php //echo $datasetting['bag_pembelian'] 
			// ambil nama usernya
		$sqlxx = " SELECT nama FROM tm_user WHERE uid = '$uid_update_by' ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_user = $hasilxx->nama;
			echo $nama_user;
		}
		else
			echo '';
			?>
			
			</u></td>
			
		<td colspan="2" align="center"><u><?php echo"(". $query[0]['penanggungjawab'] .")" ?></u></td>
			<td colspan="2" align="center"><u><?php echo $datasetting['bag_keuangan'] ?></u></td>
		</tr>
		</td>
	</tr>
	</table>
		</td>
	</tr>	
	</table>
	



<!--<i><div class="isinya" align="right">Tanggal Cetak: <?php //echo date("d-m-Y H:i:s"); ?></i></div>-->
