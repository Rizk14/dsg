<style type="text/css">
	@page {
		size: US Letter;
		margin: 0.29in 0.29in 0.29in 0.70in;
	}

	.isinya {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 12px;

	}

	.kepala {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 18px;
	}

	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 12px;
	}

	.kepala2 {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 12px;
	}

	.kotak {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 10px;
		border-collapse: collapse;
		border: 1px solid black;
	}

	.tabelheader {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.subtotal {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.garisbawah {
		border-bottom: #000000 0.1px solid;
	}
</style>

<!-- media="screen" means these styles will only be used by screen 
  devices (e.g. monitors) -->

<?php include_once("funcs/terbilang.php"); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.3.2.js"></script>
<!-- <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script> -->
<script type="text/javascript">
	$(function() {
		//printpr();
		//window.print();
	});

	window.onafterprint = function() {
		var faktur = <?php echo $nomorfaktur ?>;
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('khususprntfpenjualannondo/cform/update'); ?>",
			data: "faktur=" + faktur,
			success: function(data) {
				opener.window.refreshview();
				setTimeout(window.close, 0);
			},
			error: function(XMLHttpRequest) {
				alert('fail');
			}
		});
	}
</script>

<table border="0" width="730px" class="tabelheader">
	<tr>
		<td align="left" colspan="2"> =============================================================================================== </td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<div class="kepala">&nbsp;&nbsp;<b>Faktur Penjualan</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<b class="kepalaxx"><?php echo $nminitial ?></b>
			</div>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2"> =============================================================================================== </td>
	</tr>

	<tr class="kepala2">
		<td>Nomor Faktur : <b><?php echo $nomorfaktur ?> </b></td>
		<td align="right">Tanggal Faktur : <b><?php echo $tglfaktur ?></b></td>
	</tr>
	<tr>
	</tr>
	<tr>
		<td>Kepada :</td>
		<td rowspan="3">
			<table border="2" width="70%" class="kotak">
				<tr>
					<td>Keterangan:<br>
						<?php echo $enote ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><b><?php echo $nmcabang ?></b></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><b><?php echo $alamatcabang  ?></b></td>
		<td>&nbsp;</td>
	</tr>
</table>

<table border="0" width="730px" class="isinya" cellspacing="0">

	<thead>
		<tr>
			<td colspan="8">
				<hr>
			</td>
		</tr>
		<tr class="kepala2">
			<th width="3%">No</th>
			<th width="10%" style="white-space:nowrap;">Kode Barang</th>
			<th width="30%" style="white-space:nowrap;">Nama Barang</th>
			<th width="4%">Unit</th>
			<th width="8%">Harga</th>
			<th width="8%">Jumlah</th>
			<th width="5%">Disc(%)</th>
			<th width="5%">Disc(Rp.)</th>
		</tr>
		<tr>
			<td colspan="8">
				<hr>
			</td>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		$lup	= 0;
		$grandtotal2 = 0;
		$grandtotal = 0;
		$totaldiskon = 0;
		foreach ($isi as $row) {
			if ($lup < 22) {

				$qvalue	= $this->mclass->jmlitemharga($row->i_faktur, $row->imotif);

				if ($qvalue->num_rows() > 0) {
					$row_value	= $qvalue->row();
					$jmlqty[$lup]	= $row_value->qty;
					$uprice[$lup]	= $row_value->unitprice;
					$tprice[$lup]	= $row_value->amount;
					$tprice2[$lup]	= $row_value->amount2;
					$ndisc[$lup]	= $row_value->ndisc;
					$vdisc[$lup]	= $row_value->vdisc;
				} else {
					$jmlqty[$lup]	= 0;
					$uprice[$lup]	= 0;
					$tprice[$lup]	= 0;
				}
				if ($ndisc[$lup] == 0 && $vdisc[$lup] > 0) {
					$vtdisc = $vdisc[$lup];
				} else {
					$vtdisc = $vdisc[$lup] * $jmlqty[$lup];
				}

				echo "<tr class='detailbrg'>
					<td align='center'>$no</td>
					<td>&nbsp;$row->imotif</td>
					<td>&nbsp;$row->motifname</td>
					<td align='right'>" . number_format($jmlqty[$lup]) . "&nbsp;</td>
					<td align='right'>" . number_format($uprice[$lup]) . "&nbsp;</td>
					<td align='right'>" . number_format($tprice2[$lup]) . "&nbsp;</td>
					<td align='right'>" . number_format($ndisc[$lup]) . "&nbsp;</td>
					<td align='right'>" . number_format($vtdisc) . "&nbsp;</td>
			</tr>";

				$grandtotal2 += $tprice2[$lup];
				$grandtotal += $tprice[$lup];
				$totaldiskon += $vtdisc;
				/**********1 juni 2022*************/
				$grandtotal_new = $grandtotal2 - $totaldiskon;
				$no++;
				$lup++;
			}
		}
		?>
		<tr>
			<td colspan="8">
				<hr>
			</td>
		</tr>
	</tbody>
</table>

<table width="730px" border="0" class="kepala2">

	<tr class="detailbrg2">
		<td colspan="2" style="white-space:nowrap;">Tgl Jatuh Tempo : <b><?php echo $tgljthtempo ?></b></td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
	</tr>

	<!-- 
	<tr class="detailbrg2">
		<td width="3%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%" colspan="2" align="right">Jumlah Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;</td>
		<!--<td width="20%">Rp. </td>
		<td align="right"><b><?php echo number_format($grandtotal) ?></b>&nbsp;</td>
		-->
	<tr class="detailbrg2">
		<td width="3%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%" colspan="2" align="right">Total Jumlah Rp. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td width="20%">Rp. </td>-->
		<td align="right"><b><?php echo number_format($grandtotal2) ?></b>&nbsp;</td>
	</tr>


	<tr class="detailbrg2">
		<td width="3%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%" colspan="2" align="right">Total Diskon Rp. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td width="20%">Rp. </td>-->
		<td align="right"><b><?php echo number_format($totaldiskon) ?></b>&nbsp;</td>
	</tr>

	<tr>
		<td rowspan="7" colspan="3">
			<table border="2" width="100%" class="kotak">
				<tr>
					<td style="white-space:nowrap;">&nbsp;1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kecuali ada perjanjian terlebih dahulu<br>
						&nbsp;2. Faktur asli merupakan bukti pembayaran yg sah<br>
						&nbsp;3. Pembayaran dgn cek/giro baru dianggap sah setelah diuangkan
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="2">Total Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td>Rp.</td>-->
		<!-- <td align="right"><b><?php echo number_format($grandtotal) ?></b>&nbsp;</td> -->

		<!-- 1 Juni 2022 -->
		<td align="right"><b><?php echo number_format($grandtotal_new) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td>&nbsp;</td>
		<td colspan="2">
			<hr>
		</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="2">DPP Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($dpp) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="2">PPN Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($nilai_ppn) ?></b>&nbsp;</td>
	</tr>
	<? if ($nilai_faktur > 4999999) { ?>
		<tr class="detailbrg2">
			<!-- <td align="right" colspan="2">Bea Materai Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
			<!--<td>Rp.</td>-->
			<!-- <td align="right"><b><?php echo number_format($materai) ?></b>&nbsp;</td> -->
		</tr>
	<? } ?>

	<tr class="detailbrg2">
		<td>&nbsp;</td>
		<td colspan="2">
			<hr>
		</td>
	</tr>
	<? if ($nilai_faktur > 4999999) { ?>
		<tr class="detailbrg2">
			<td align="right" colspan="5">Nilai Faktur Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;</td>
			<!--<td>Rp.</td>-->
			<td align="right"><b><?php echo number_format($nilai_faktur) ?></b>&nbsp;</td>
		</tr>
	<? } ?>
	<tr class="detailbrg2">
		<td align="right" colspan="2">Nilai Faktur Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($nilai_faktur) ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td colspan="5" align="center" style="white-space:nowrap;"><?php $terbil = &terbilangxx($nilai_faktur, 3); ?>(Terbilang: <?php echo $terbil ?> Rupiah)</td>
	</tr>
</table>
<div class="kepala2"><b>SE & O<br><br><br><br><br><br><br>


		Adm Penjualan</b></div>