<style type="text/css">
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 10px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	window.print();
});

/*function printpr()
	{
		var OLECMDID = 7; */
		/* OLECMDID values:
		* 6 - print
		* 7 - print preview
		* 1 - open window
		* 4 - Save As
		*/
/*		var PROMPT = 1; // 2 DONTPROMPTUSER
		var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
		WebBrowser1.ExecWB(OLECMDID, PROMPT);
		WebBrowser1.outerHTML = "";
	} */
</script>

<?php if ($query[0]['is_dacron'] == 'f') {

?>
<table border="0" width="100%" class="tabelheader">
	<tr>
		<td><b>REALISASI CUTTING</b></td>
	</tr>
</table><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isinya">
	<thead>
		<tr>
			<th rowspan="2" >Tanggal</td>
			<th rowspan="2" >Operator</td>
			<th rowspan="2" >Barang Jadi</td>
			<th rowspan="2" >Bahan Baku</td>
			<th rowspan="2" >Jam Kerja</td>
			<th rowspan="2" >Lot/Rol</td>
			<th rowspan="2">Pjg Kain (m)</td>
			<th rowspan="2">Gelaran</td>
			<th rowspan="2">Set</td>
			<th rowspan="2">Jml Gelar</td>
			<th rowspan="2">Hsl Potong</td>
			<th colspan="4">Sisa Kain</td>
			<th rowspan="2">Keterangan</td>
			
		</tr>
		<tr>
			<th >Planning(m)</td>
			<th >Aktual(m)</td>
			<th >Allow(m)</td>
			<th >Selisih(m)</td>
		</tr>
	</thead>
	<tbody>
	<?php
			if (is_array($query)) {
				$tgl_temp = ""; $hari_temp = "";
				$operator_temp = "";
				for($j=0;$j<count($query);$j++){
					
		?>
			<tr>
				<td><?php 
					if ($tgl_temp != $query[$j]['tgl_realisasi']) {
						$tgl_temp = $query[$j]['tgl_realisasi'];
						echo $query[$j]['tgl_realisasi'];  
					}
					else
						echo "&nbsp;";
					?></td>

				<td nowrap><?php 
					if ($operator_temp != $query[$j]['operator_cutting']) {
						$operator_temp = $query[$j]['operator_cutting'];
						echo $query[$j]['operator_cutting']; 
					}
					else
						echo "&nbsp;";
					
					?></td>
				<td><?php echo $query[$j]['kode_brg_jadi']." - ".$query[$j]['nmmotif'] ?></td>
				
				<td><?php echo $query[$j]['nmbrg'];
					 ?></td>
				<td nowrap><?php echo $query[$j]['jam_mulai']." - ".$query[$j]['jam_selesai'] ?></td>
				<td nowrap><?php echo $query[$j]['jum_lot'] ?></td>
				<td nowrap><?php echo $query[$j]['detail_panjang_kain_mtr'] ?></td>
				<td nowrap align="right"><?php echo $query[$j]['gelaran'] ?></td>
				<td nowrap align="right"><?php echo $query[$j]['set'] ?></td>
				<td nowrap><?php echo $query[$j]['detail_jum_gelar'] ?></td>
				<td nowrap><?php echo $query[$j]['detail_hasil_potong'] ?></td>
				<td nowrap><?php echo $query[$j]['sisa_planning_mtr'] ?></td>
				<td nowrap><?php echo $query[$j]['sisa_aktual_mtr'] ?></td>
				<td nowrap><?php echo $query[$j]['sisa_allow_mtr'] ?></td>
				<td nowrap><?php echo $query[$j]['sisa_selisih_mtr'] ?></td>
				
				<td><?php echo $query[$j]['keterangan'] ?></td>
			</tr>
		<?php			
				}
			}
		?>
	</tbody>
</table>

<?php 
}
else {
?>
<table border="0" width="100%" class="tabelheader">
	<tr>
		<td><b>REALISASI CUTTING DACRON</b></td>
	</tr>
</table><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isinya">
	<thead>
		<tr>
			<th>Tanggal</td>
			<th>Ket. Pekerjaan</td>
			<th>Operator</td>
			<th>Barang Jadi</td>
			<th>Nama Bahan</td>
			<th>Uk Pola</td>
			<th>Nama Bagian</td>
			<th>Jml Gelar</td>
			<th>Hsl Potong</td>
			<th>Jam Kerja</td>
		</tr>
	
	</thead>
	<tbody>
	<?php
			if (is_array($query)) {
				$tgl_temp = ""; $ket_pekerjaan_temp = "";
				$operator_temp = "";
				for($j=0;$j<count($query);$j++){
					
		?>
			<tr>
				<td><?php 
					if ($tgl_temp != $query[$j]['tgl_realisasi']) {
						$tgl_temp = $query[$j]['tgl_realisasi'];
						echo $query[$j]['tgl_realisasi'];  
					}
					else
						echo "&nbsp;";
					?></td>
				
				<td><?php 
					if ($ket_pekerjaan_temp != $query[$j]['ket_pekerjaan']) {
						$ket_pekerjaan_temp = $query[$j]['ket_pekerjaan'];
						echo $query[$j]['ket_pekerjaan'];  
					}
					else
						echo "&nbsp;";
					?></td>
				
				<td><?php 
					if ($operator_temp != $query[$j]['operator_cutting']) {
						$operator_temp = $query[$j]['operator_cutting'];
						echo $query[$j]['operator_cutting']; 
					}
					else
						echo "&nbsp;";
					
					?></td>
				<td><?php echo $query[$j]['kode_brg_jadi']." - ".$query[$j]['nmmotif'] ?></td>
				
				<td><?php 	echo $query[$j]['nmbrg']; ?></td>
				<td><?php echo $query[$j]['ukuran_pola'] ?></td>
				 <td><?php echo $query[$j]['nama_bagian_brg_jadi'] ?></td>
				 <td align='right'><?php echo $query[$j]['jum_gelar'] ?></td>
				 <td align='right'><?php echo $query[$j]['hasil_potong'] ?></td>
				
				<td nowrap><?php echo $query[$j]['jam_mulai']." - ".$query[$j]['jam_selesai'] ?></td>
			</tr>
		<?php			
				}
			}
		?>
	</tbody>
</table>
<?php 
	}
?>
