<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
    .fieldsetdemo {
		background-color:#DDD;
		max-width:300px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function openCenteredWindow(url) {

		var width = 1200;
		var height = 600;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function()
{
	$(".pilih").click(function()
	{
		var id_rc=$("#id_rc").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/realisasi-cutting/cform/print_rc/"+id_rc;
		openCenteredWindow(urlnya);
	});
});
</script>


<h3>Realisasi Cutting</h3><br> 
<a href="<? echo base_url(); ?>index.php/realisasi-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/realisasi-cutting/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('realisasi-cutting/cform/cari'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Nomor Realisasi</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>
</fieldset>
<?php echo form_close(); ?>
<br>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_rc" id="id_rc">

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th width="90px">No / Tgl Realisasi</th>
		<th width="40px">Jenis</th>
		<th width="10px">Qty Dacron</th>
		<th width="250px">Brg Jadi</th>
		<th width="205px">Bhn Baku</th>
		<th>Jml Lot</th>
		<th>Pjg Kain (m)</th>
		<th>Gelaran</th>
		<th>Set</th>
		<th width="30px">Jml Gelar</th>
		<th width="50px">Qty Hsl Potong</th>
		<th>Sisa Planning</th>
		<th>Sisa Aktual</th>
		<th>Allowance</th>
		<th>Sisa Allow</th>
		<th>Selisih Sisa</th>
		<th width="70px">Jam Kerja</th>
		<th>Operator</th>
		<th>Status Beres</th>
		<th>Last Update</th>
		<th width="40px">Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
					$pisah1 = explode("-", $query[$j]['tgl_realisasi']);
					$tgl1	= substr($pisah1[2],0,1)=='0'?substr($pisah1[2],1,1):$pisah1[2];
					
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					if ($bln1=='01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					//$tgl_realisasi = $tgl1." ".$nama_bln." ".$thn1;
				 $tgl_realisasi = $tgl1." ".$nama_bln." ".$thn1;
				 
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				 echo "<tr class=\"record\">";
				 
				 echo "<td>".$query[$j]['no_realisasi']." / ".$query[$j]['nama_hari'].", ".$tgl_realisasi."</td>";
				 if ($query[$j]['is_dacron'] == 't')
					echo "<td>Dacron</td>";
				 else
					echo "<td>Non-Dacron</td>";
				 
				 echo "<td align=right>".$query[$j]['qty_dacron']."</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nm_brg_jadi'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['jum_lot'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align=right>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['panjang_kain_mtr'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align=right>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['gelaran'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align=right>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['set'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align=right>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['jum_gelar'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align=right>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['hasil_potong'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['sisa_planning_mtr'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['sisa_aktual_mtr'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['allowance'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['sisa_allow_mtr'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['sisa_selisih_mtr'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['jam_mulai']." - ".$var_detail[$k]['jam_selesai'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['operator_cutting'];
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";			 
				 if (is_array($query[$j]['detail_fb'])) {
					$var_detail = array();
					$var_detail = $query[$j]['detail_fb'];
					$hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['status_beres'] == 'f')
						echo "Belum";
						else echo "OK";
						 if ($k<$hitung-1)
							echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>".$tgl_update."</td>";
				 
					echo "<td align=center>";
					echo "<a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_rc.value=".$query[$j]['id']."'; ><u>Print</u></a>&nbsp;";
					if($query[$j]['status_apply_stok']=='f'){
						echo "<a href=".base_url()."index.php/realisasi-cutting/cform/edit/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a> &nbsp;";
						echo "<a href=".base_url()."index.php/realisasi-cutting/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					}
					echo "</td>";

				 echo  "</tr>"; 
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
