<h3>Realisasi Cutting</h3><br>
<a href="<? echo base_url(); ?>index.php/realisasi-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/realisasi-cutting/cform/view">View Data</a><br><br>

<?php if ($msg!='') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">
function pemenuhan(iteration) {
	lebar =450;
	tinggi=400;
	eval('window.open("<?php echo site_url(); ?>"+"/realisasi-cutting/cform/show_popup_brg/"+iteration,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
</script>

<script language="javascript">
//tambah
$(function()
{
	$('#topnya').hide();
		
	$('#tombol').click(function(){
		var jenis_schedule	= $("#jenis_schedule").val();	
		var urlnya = "<?php echo base_url(); ?>index.php/realisasi-cutting/cform/show_popup_schedule/"+jenis_schedule;
		openCenteredWindow(urlnya);
	  });

	$('#tombol2').click(function(){
		var posisi	= $("#posisi").val();	
		var urlnya	= "<?php echo base_url(); ?>index.php/realisasi-cutting/cform/show_popup_brg/"+posisi+"/";
		openCenteredWindow(urlnya);
	  });	  
});

function cek_item_brg() {
	var no_item= $('#no_item').val();
	
		if (no_item == '') {
			alert("Schedule cutting harus dipilih..!");
			return false;
		}	
}

function ckitem() {
	var norealisasi = $("#norealisasi").val();
	var tglrealisasi = $("#tglrealisasi").val();
	var is_dacron = $("#is_dacron").val();
	
	if (norealisasi == '') {
		alert ("No Realisasi harus diisi");
		$("#norealisasi").focus();
		return false;
	}
	if (tglrealisasi == '') {
		alert ("Tgl Realisasi harus dipilih");
		$("#tglrealisasi").focus();
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#operator_'+k).val() == '') {
				alert("Data operator tidak boleh kosong...!");
				return false;
			}
			if (is_dacron == 'f') {
				if ($('#pjg_kain_'+k).val() == '') {
					alert("Panjang kain tidak boleh kosong...!");
					return false;
				}
				if ($('#gelaran_'+k).val() == '') {
					alert("Gelaran tidak boleh kosong...!");
					return false;
				}
				if ($('#set_'+k).val() == '') {
					alert("Set tidak boleh kosong...!");
					return false;
				}
				if ($('#planning_'+k).val() == '') {
					alert("Sisa planning tidak boleh kosong...!");
					return false;
				}
				if ($('#aktual_'+k).val() == '') {
					alert("Sisa aktual tidak boleh kosong...!");
					return false;
				}
				if ($('#allow_'+k).val() == '') {
					alert("Sisa allow tidak boleh kosong...!");
					return false;
				}
				if ($('#selisih_'+k).val() == '') {
					alert("Selisih sisa tidak boleh kosong...!");
					return false;
				}
			}
			
			if ($('#jml_gelar_'+k).val() == '') {
				alert("Jml Gelar tidak boleh kosong...!");
				return false;
			}
			if ($('#hsl_potong_'+k).val() == '') {
				alert("Hasil potong tidak boleh kosong...!");
				return false;
			}
			
			if (is_dacron == 't') {
				if (isNaN($('#jml_gelar_'+k).val()) ) {
					alert("Jml gelar harus berupa angka atau desimal...!");
					return false;
				}
				if (isNaN($('#hsl_potong_'+k).val()) ) {
					alert("Hasil potong harus berupa angka atau desimal...!");
					return false;
				}
			}
			
			if ($('#jammulai_'+k).val() == '') {
				alert("Jam mulai tidak boleh kosong...!");
				return false;
			}
			if ($('#jamselesai_'+k).val() == '') {
				alert("Jam selesai tidak boleh kosong...!");
				return false;
			}
									
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;

				var i=1;
				for (i=1;i<=jum_detail;i++) {
					var jml_gelar=$("#jml_gelar_"+i).val();
					var jum_set=$("#set_"+i).val();

					var hsl_potong = parseFloat(jml_gelar)*parseFloat(jum_set);
					hsl_potong = hsl_potong.toFixed(2);					
					
					$("#hsl_potong_"+i).val(hsl_potong);
				}

}
</script>

<script type="text/javascript"> 
	function openCenteredWindow(url) {
		var width = 1100;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if($go_proses==''){
$attributes = array('name'=>'f_op', 'id'=>'f_op');
echo form_open('realisasi-cutting/cform/', $attributes); ?>
<table>
	<tr>
		<td>Jenis</td>
		<td>: <select name="jenis_schedule" id="jenis_schedule" onchange="$('#no_item').val(''); $('#id').val(''); $('#id_detail').val('');" onkeyup="this.blur();this.focus();">
					<option value="t" >Dacron</option>
					<option value="f" >Non-Dacron</option>
				</select></td>
	</tr>
	<tr>
		<td>No Schedule Cutting</td>
		<td>: <input type="text" name="no_item" id="no_item" size="15" readonly="true" />
		<input type="hidden" name="id" id="id" />
		<input type="hidden" name="id_detail" id="id_detail" />
		<!-- <input type="hidden" name="ischheader" id="ischheader" /> -->
		<input name="tombol" id="tombol" value="..." type="button" />
		</td>
	</tr>
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();" />&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/realisasi-cutting/cform/view'">
<?php echo form_close(); ?>
<?php }else{ ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/realisasi-cutting/cform/submit/" method="post" enctype="multipart/form-data">

<?php 
if (count($schedule_detail)>0) {
	$no=1;
	foreach ($schedule_detail as $hitung) {
		$no++;
	}
}
else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="is_dacron" id="is_dacron" value="<?php echo $jenis_schedule ?>">
<input type="hidden" name="nobonm" id="nobonm" value="bonm_rc">
<input type="hidden" name="eksternal" id="eksternal" value="<?php echo $schedule_detail[0]['eksternal'] ?>">

<div align="center">
<label id="status"></label>
<br>
<table class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<?php 
		if ($jenis_schedule == 't') {
	?>
	<tr>
		<td colspan="2">Khusus Dacron</td>
	</tr>
	<tr>
		<td width="14%">Qty Dacron Yg Dikeluarkan</td>
		<td> <input type="text" name="qty_dacron" id="qty_dacron" size="5" value="" /></td>
	</tr>
	<?php } else { ?>
		<input type="hidden" name="qty_dacron" id="qty_dacron" value="0">
	<?php } ?>
	<tr>
		<td width="14%">Nomor Realisasi</td>
		<td>
		  <input type="text" name="norealisasi" id="norealisasi" maxlength="10" size="10" value="<?=$norealisasi?>" readonly="true" />
		  <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
    	</td>
  	</tr>
  	<tr>
		<td>Tgl Realisasi</td>
		<td>
		  <input type="text" name="tglrealisasi" id="tglrealisasi" size="10" readonly="true" />
		  <img alt="" id="tglrealisasi" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tglrealisasi,'dd-mm-yyyy',this)">
		</td>
  	</tr>
  	<tr>
		<td width="14%" valign="top">Ket Pekerjaan Operator</td>
		<td>
		  <textarea name="ket_pekerjaan" id="ket_pekerjaan" cols="40" rows="7"></textarea>
    	</td>
  	</tr>
	<!--<tr>
		<td width="14%">Nomor Bon M</td>
		<td>
		  <input type="text" name="nobonm" id="nobonm" maxlength="10" size="10" value="" />
    	</td>
  	</tr> -->	
  	<tr>
		<td colspan="2"><br>
		<form name="myform">
		<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
			<tr>
			  <th width="20">No</th>
			  <th>Operator</th>
			  <th>Brg Jadi</th>
			  <!-- <th>Qty</th> -->
			  <th>Bhn Baku/Pembantu</th>
			  <th>Sat Dasar</th>
			  <th>Qty Yg Diklrkan</th>
			  <th>Dari Sisa</th>
			  <th>Gelaran</th>
			  <th>Set</th>
			  <?php if ($jenis_schedule != 't') { ?>
			  <th>Input Detail</th>
			  <?php } ?>
			  <th>Jml lot</th>
			  <th>Pjg Kain(m)</th>
			  <th>Jml Gelar</th>
			  <th>Nama Bagian</th>
			  <th>Ukuran Pola</th>
			  <th>Hsl Potong</th>
			  <th>Sisa Planning(m)</th>
			  <th>Sisa Aktual(m)</th>
			  <th>Sisa Allow(m)</th>
			  <th>Selisih Sisa(m)</th>
			  <th>Jam Mulai</th>
			  <th>Jam Selesai</th>
			  <th>Ket.</th>
			</tr>
			<?php
			$i=1;
			for($j=0;$j<count($schedule_detail);$j++){
			//echo $xx[$j]."-";
			?>		
			<tr align="center">
			  <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
			  <td><input name="operator_<?php echo $i ?>" type="text" id="operator_<?php echo $i ?>" size="15" value="<?php echo $schedule_detail[$j]['operator_cutting'] ?>" /></td>
			  <td nowrap="nowrap">
			   <input readonly="true" name="nmbrgjadi_<?php echo $i ?>" type="text" id="nmbrgjadi_<?php echo $i ?>" size="30" value="<?php echo $schedule_detail[$j]['kode_brg_jadi']." - ".$schedule_detail[$j]['nmmotif'] ?>" />
			   <input name="imotif_<?php echo $i ?>" type="hidden" id="imotif_<?php echo $i ?>" value="<?php echo $schedule_detail[$j]['kode_brg_jadi'] ?>"/>
			   <input name="nama_brg_jadi_other_<?php echo $i ?>" type="hidden" id="nama_brg_jadi_other_<?php echo $i ?>" value="<?php echo $schedule_detail[$j]['nama_brg_jadi_other'] ?>"/>
			   <input type="hidden" name="posisi" id="posisi" value="<?=$i?>" />
			   <input type="hidden" name="diprint" id="diprint" value="<? echo $schedule_detail[$j]['diprint'] ?>" />
			   <!-- <input name="tombolx" id="tombolx" value="..." type="button" onclick="pemenuhan(<?=$i?>);"/> -->
			   <input name="qty_<?php echo $i ?>" type="hidden" id="qty_<?php echo $i ?>" value="<?php echo $schedule_detail[$j]['qty_bhn'] ?>" />
			   </td>
			   <td>
			   <input name="nmbrg_<?php echo $i ?>" type="text" id="nmbrg_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $schedule_detail[$j]['nmbrg'] ?>" />
			   <input name="kode_<?php echo $i ?>" type="hidden" id="kode_<?php echo $i ?>" value="<?php echo $schedule_detail[$j]['kode_brg'] ?>" />
			  </td>
			  
			  <td>
			   <input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="6" readonly="true" value="<?php echo $schedule_detail[$j]['satuan'] ?>" />
			  </td>
			  <td>
			   <input name="qty_klr_<?php echo $i ?>" type="text" id="qty_klr_<?php echo $i ?>" size="5" <?php if ($schedule_detail[$j]['id_apply_stok_detail'] != '0') { ?> readonly="true" <?php } ?> value="" />
			  </td>
			  
			  <td nowrap>
			   <input type="checkbox" name="sisa_<?php echo $i ?>" id="sisa_<?php echo $i ?>" <?php if ($schedule_detail[$j]['id_apply_stok_detail'] != '0') { ?> disabled <?php } ?> value="t" >
			  </td>
			  
			  <td><input <?php if ($jenis_schedule == 't') { ?> readonly="true" <?php } ?> name="gelaran_<?php echo $i ?>" type="text" id="gelaran_<?php echo $i ?>" size="5" value="<?php echo $schedule_detail[$j]['gelaran'] ?>" /></td>
			  <td><input <?php if ($jenis_schedule == 't') { ?> readonly="true" <?php } ?> name="set_<?php echo $i ?>" type="text" id="set_<?php echo $i ?>" size="5" value="<?php echo $schedule_detail[$j]['jum_set'] ?>" /></td>
			  
			  <?php if ($jenis_schedule != 't') { ?>
			  <td>
			  <input title="input detail" name="pilih_detail_<?php echo $i ?>" value="Input Detail" type="button" id="pilih_detail_<?php echo $i ?>" 
           onclick="javascript: var gelaran= $('#gelaran_<?php echo $i ?>').val(); var jum_set= $('#set_<?php echo $i ?>').val(); 
           if(gelaran == '' || jum_set == '') { alert('input dulu gelaran atau setnya...'); return false; } openCenteredWindow('<?php echo base_url(); ?>index.php/realisasi-cutting/cform/show_popup_detail2/<?php echo $i ?>/'+gelaran+'/'+jum_set+'/');">
			  </td>
			  <?php } ?>
			  
			  <td nowrap><input readonly="true" name="jml_lot_<?php echo $i ?>" type="text" id="jml_lot_<?php echo $i ?>" size="7" <?php if ($jenis_schedule == 't') { ?> readonly="true" <?php } ?> />
			<!--  <input title="input detail" name="pilih_jml_lot_<?php echo $i ?>" value="..." type="button" id="pilih_jml_lot_<?php echo $i ?>" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/realisasi-cutting/cform/show_popup_detail/<?php echo $i ?>/1/0');"> -->
			  </td>
			  <td nowrap><input readonly="true" name="pjg_kain_<?php echo $i ?>" type="text" id="pjg_kain_<?php echo $i ?>" size="7" <?php if ($jenis_schedule == 't') { ?> readonly="true" <?php } ?> />
			 <!-- <input type="hidden" name="hide_pjg_kain_<?php echo $i ?>" id="hide_pjg_kain_<?php echo $i ?>" value=""> -->
			<!--  <input title="input detail" name="pilih_pjg_kain_<?php echo $i ?>" value="..." type="button" id="pilih_pjg_kain_<?php echo $i ?>" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/realisasi-cutting/cform/show_popup_detail/<?php echo $i ?>/2/0');"> -->
           </td>
			  
			  <td nowrap><input <?php if ($jenis_schedule != 't') { ?> readonly="true" <?php } ?> name="jml_gelar_<?php echo $i ?>" type="text" id="jml_gelar_<?php echo $i ?>" size="5" />
			  <input type="hidden" name="hide_jml_gelar_<?php echo $i ?>" id="hide_jml_gelar_<?php echo $i ?>" value="">
			<!--  <input title="input detail" name="pilih_jml_gelar_<?php echo $i ?>" value="..." type="button" id="pilih_jml_gelar_<?php echo $i ?>" 
           onclick="javascript: var jum_set= $('#set_<?php echo $i ?>').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/realisasi-cutting/cform/show_popup_detail/<?php echo $i ?>/3/'+jum_set);"> -->
			  </td>
			  
			  <td><input name="bagian_brg_jadi_<?php echo $i ?>" type="text" id="bagian_brg_jadi_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $schedule_detail[$j]['nama_bagian_brg_jadi'] ?>" />
				<input type="hidden" name="id_bagian_brg_jadi_<?php echo $i ?>" id="id_bagian_brg_jadi_<?php echo $i ?>" value="<?php echo $schedule_detail[$j]['id_bagian_brg_jadi'] ?>"></td>
			  <td><input name="ukuran_pola_<?php echo $i ?>" type="text" id="ukuran_pola_<?php echo $i ?>" size="7" value="<?php echo $schedule_detail[$j]['ukuran_pola'] ?>" readonly="true" /></td>
			  <td nowrap><input <?php if ($jenis_schedule != 't') { ?> readonly="true" <?php } ?> name="hsl_potong_<?php echo $i ?>" type="text" id="hsl_potong_<?php echo $i ?>" size="5" />
			  <input type="hidden" name="hide_hsl_potong_<?php echo $i ?>" id="hide_hsl_potong_<?php echo $i ?>" value="">
			  </td>
			  
			  <td nowrap><input readonly="true" name="planning_<?php echo $i ?>" type="text" id="planning_<?php echo $i ?>" size="5" /></td>
			  
			  <td nowrap><input readonly="true" name="aktual_<?php echo $i ?>" type="text" id="aktual_<?php echo $i ?>" size="5" />
			<!--  <input title="input detail" name="pilih_aktual_<?php echo $i ?>" value="..." type="button" id="pilih_aktual_<?php echo $i ?>" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/realisasi-cutting/cform/show_popup_detail/<?php echo $i ?>/5/0');"> -->
			  </td>
			  
			  <td nowrap><input readonly="true" name="allow_<?php echo $i ?>" type="text" id="allow_<?php echo $i ?>" size="5" />
			  <input name="allowance_<?php echo $i ?>" type="hidden" id="allowance_<?php echo $i ?>" value="" />
			  </td>
			  <td nowrap><input readonly="true" name="selisih_<?php echo $i ?>" type="text" id="selisih_<?php echo $i ?>" size="5" /></td>
			  
			  <td><input name="jammulai_<?php echo $i ?>" type="text" id="jammulai_<?php echo $i ?>" size="8" value="00:00" /></td>
			  <td><input name="jamselesai_<?php echo $i ?>" type="text" id="jamselesai_<?php echo $i ?>" size="8" value="00:00" />
			  <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="10" />
			  <input type="hidden" name="id_detail_<?php echo $i ?>" id="id_detail_<?php echo $i ?>" value="<?php echo $schedule_detail[$j]['id'] ?>" />
			  <input type="hidden" name="iterasi" id="iterasi" value="<?=$i?>" />
			  </td>
			</tr>
			<?php $i++; } // end foreach 
			?>
		</table>	
		  </form>
		  <div align="center"><br>        
		  <input type="submit" name="submit" value="Simpan" onclick="return ckitem();" />    
		   &nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/realisasi-cutting/cform/'" />
		  </div>
		  </td>
	</tr>
</table>
</div>
</form>
<?php } ?>
