<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{	<?php if ($is_edit == 0) { ?>
	$("#no").val('2');
	<?php } ?>
	
	$("#pilih").click(function()
	{
		
		var jum= $('#no').val()-1; 
		// 30-01-2012
		opener.document.forms["f_purchase"].jml_lot_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].pjg_kain_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].jml_gelar_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].hsl_potong_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].planning_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].aktual_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].allow_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].selisih_<?php echo $posisi ?>.value='';
		
		opener.document.forms["f_purchase"].hide_jml_gelar_<?php echo $posisi ?>.value='';
		opener.document.forms["f_purchase"].hide_hsl_potong_<?php echo $posisi ?>.value='';
				
		//var gabung = ""; var gabung2 = ""; var j_gelar = 0; var j_potong = 0;
		
		var gjum_lot = ""; var gpjg_kain = ""; var gjml_gelar = ""; var ghsl_potong = "";
		var gplanning = ""; var gaktual = ""; var gallow = ""; var gselisih = ""; var gallowance = "";
		var j_gelar = 0; var j_potong = 0;
		
		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				if($('#pjg_kain_'+k).val() == '0' || $('#pjg_kain_'+k).val() == '' ) {				
					alert("Pjg kain tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#pjg_kain_'+k).val()) ) {
					alert("Pjg kain harus berupa angka..!");
					return false;
				}
				
				if($('#jml_gelar_'+k).val() == '0' || $('#jml_gelar_'+k).val() == '' ) {				
					alert("Jml gelar tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#jml_gelar_'+k).val()) ) {
					alert("Jml gelar harus berupa angka..!");
					return false;
				}
				
				if($('#hsl_potong_'+k).val() == '0' || $('#hsl_potong_'+k).val() == '' ) {				
					alert("Hasil potong tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#hsl_potong_'+k).val()) ) {
					alert("Hasil potong harus berupa angka..!");
					return false;
				}
				
				if($('#sisa_planning_'+k).val() == '0' || $('#sisa_planning_'+k).val() == '' ) {				
					alert("Sisa planning tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#sisa_planning_'+k).val()) ) {
					alert("Sisa planning harus berupa angka..!");
					return false;
				}
				
				if($('#sisa_aktual_'+k).val() == '0' || $('#sisa_aktual_'+k).val() == '' ) {				
					alert("Sisa aktual tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#sisa_aktual_'+k).val()) ) {
					alert("Sisa aktual harus berupa angka..!");
					return false;
				}
				
				if($('#allowance_'+k).val() == '0' || $('#allowance_'+k).val() == '' ) {				
					alert("Allowance tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#allowance_'+k).val()) ) {
					alert("Allowance harus berupa angka..!");
					return false;
				}
				
				if($('#sisa_allow_'+k).val() == '0' || $('#sisa_allow_'+k).val() == '' ) {				
					alert("Sisa allow tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#sisa_allow_'+k).val()) ) {
					alert("Sisa allow harus berupa angka..!");
					return false;
				}
				
				if($('#selisih_'+k).val() == '0' || $('#selisih_'+k).val() == '' ) {				
					alert("Selisih sisa tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#selisih_'+k).val()) ) {
					alert("Selisih sisa harus berupa angka..!");
					return false;
				}
				
				gjum_lot += $('#jml_lot_'+k).val()+";";
				gpjg_kain += $('#pjg_kain_'+k).val()+";";
				gjml_gelar += $('#jml_gelar_'+k).val()+";";
				ghsl_potong += $('#hsl_potong_'+k).val()+";";
				j_gelar += parseFloat($('#jml_gelar_'+k).val());
				j_potong += parseFloat($('#hsl_potong_'+k).val());
				gplanning += $('#sisa_planning_'+k).val()+";";
				gaktual += $('#sisa_aktual_'+k).val()+";";
				gallow += $('#sisa_allow_'+k).val()+";";
				gallowance += $('#allowance_'+k).val()+";";
				gselisih += $('#selisih_'+k).val()+";";
				
				opener.document.forms["f_purchase"].jml_lot_<?php echo $posisi ?>.value= gjum_lot;
				opener.document.forms["f_purchase"].pjg_kain_<?php echo $posisi ?>.value= gpjg_kain; 
				opener.document.forms["f_purchase"].jml_gelar_<?php echo $posisi ?>.value= gjml_gelar;
				opener.document.forms["f_purchase"].hsl_potong_<?php echo $posisi ?>.value= ghsl_potong;
				opener.document.forms["f_purchase"].planning_<?php echo $posisi ?>.value=gplanning;
				opener.document.forms["f_purchase"].aktual_<?php echo $posisi ?>.value=gaktual;
				opener.document.forms["f_purchase"].allow_<?php echo $posisi ?>.value=gallow;
				opener.document.forms["f_purchase"].allowance_<?php echo $posisi ?>.value=gallowance;
				opener.document.forms["f_purchase"].selisih_<?php echo $posisi ?>.value=gselisih;
				
				opener.document.forms["f_purchase"].hide_jml_gelar_<?php echo $posisi ?>.value= j_gelar;
				opener.document.forms["f_purchase"].hide_hsl_potong_<?php echo $posisi ?>.value=j_potong;
				
				// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
				
			}
			
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
		
		self.close();
	});
	
	
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
				
		//*****jml_lot*************************************
		var jml_lot="#jml_lot_"+n;
		var new_jml_lot="#jml_lot_"+no;
		$(jml_lot, lastRow).attr("id", "jml_lot_"+no);
		$(new_jml_lot, lastRow).attr("name", "jml_lot_"+no);		
		$(new_jml_lot, lastRow).val('');				
		//*****end jml_lot*************************************	
		
		//*****pjg_kain*************************************
		var pjg_kain="#pjg_kain_"+n;
		var new_pjg_kain="#pjg_kain_"+no;
		$(pjg_kain, lastRow).attr("id", "pjg_kain_"+no);
		$(new_pjg_kain, lastRow).attr("name", "pjg_kain_"+no);		
		$(new_pjg_kain, lastRow).val('');
		//*****end pjg_kain*************************************	
		
		//*****gelaran*************************************
		var gelaran="#gelaran_"+n;
		var new_gelaran="#gelaran_"+no;
		$(gelaran, lastRow).attr("id", "gelaran_"+no);
		$(new_gelaran, lastRow).attr("name", "gelaran_"+no);		
		//$(new_gelaran, lastRow).val('');
		//*****end gelaran*************************************	
		
		//*****set*************************************
		var set="#set_"+n;
		var new_set="#set_"+no;
		$(set, lastRow).attr("id", "set_"+no);
		$(new_set, lastRow).attr("name", "set_"+no);		
		//$(new_set, lastRow).val(is_set);
		//*****end set*************************************	
		
		//*****jml_gelar*************************************
		var jml_gelar="#jml_gelar_"+n;
		var new_jml_gelar="#jml_gelar_"+no;
		$(jml_gelar, lastRow).attr("id", "jml_gelar_"+no);
		$(new_jml_gelar, lastRow).attr("name", "jml_gelar_"+no);		
		$(new_jml_gelar, lastRow).val('');
		//*****end jml_gelar*************************************	
		
		//*****hsl_potong*************************************
		var hsl_potong="#hsl_potong_"+n;
		var new_hsl_potong="#hsl_potong_"+no;
		$(hsl_potong, lastRow).attr("id", "hsl_potong_"+no);
		$(new_hsl_potong, lastRow).attr("name", "hsl_potong_"+no);		
		$(new_hsl_potong, lastRow).val('');	
		//*****end hsl_potong*************************************	
		
		//*****sisa_planning*************************************
		var sisa_planning="#sisa_planning_"+n;
		var new_sisa_planning="#sisa_planning_"+no;
		$(sisa_planning, lastRow).attr("id", "sisa_planning_"+no);
		$(new_sisa_planning, lastRow).attr("name", "sisa_planning_"+no);		
		$(new_sisa_planning, lastRow).val('');	
		//*****end sisa_planning*************************************	
		
		//*****sisa_aktual*************************************
		var sisa_aktual="#sisa_aktual_"+n;
		var new_sisa_aktual="#sisa_aktual_"+no;
		$(sisa_aktual, lastRow).attr("id", "sisa_aktual_"+no);
		$(new_sisa_aktual, lastRow).attr("name", "sisa_aktual_"+no);		
		$(new_sisa_aktual, lastRow).val('');	
		//*****end sisa_aktual*************************************	
		
		//*****allowance*************************************
		var allowance="#allowance_"+n;
		var new_allowance="#allowance_"+no;
		$(allowance, lastRow).attr("id", "allowance_"+no);
		$(new_allowance, lastRow).attr("name", "allowance_"+no);		
		$(new_allowance, lastRow).val('');	
		//*****end allowance*************************************	
		
		//*****sisa_allow*************************************
		var sisa_allow="#sisa_allow_"+n;
		var new_sisa_allow="#sisa_allow_"+no;
		$(sisa_allow, lastRow).attr("id", "sisa_allow_"+no);
		$(new_sisa_allow, lastRow).attr("name", "sisa_allow_"+no);		
		$(new_sisa_allow, lastRow).val('');	
		//*****end sisa_allow*************************************	
		
		//*****selisih*************************************
		var selisih="#selisih_"+n;
		var new_selisih="#selisih_"+no;
		$(selisih, lastRow).attr("id", "selisih_"+no);
		$(new_selisih, lastRow).attr("name", "selisih_"+no);		
		$(new_selisih, lastRow).val('');	
		//*****end selisih*************************************	
				
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});

function hitunghasil() {
	var jum_detail= $('#no').val()-1;

		for (i=1;i<=jum_detail;i++) {
			var jum_set=$("#set_"+i).val();
			var jml_gelar=$("#jml_gelar_"+i).val();
			var pjg_kain=$("#pjg_kain_"+i).val();
			var gelaran=$("#gelaran_"+i).val();
					
			var hsl_potong = parseFloat(jum_set)*parseFloat(jml_gelar);
			hsl_potong = hsl_potong.toFixed(2);
			$("#hsl_potong_"+i).val(hsl_potong);
			
			var sisa_planning = parseFloat(pjg_kain)-(jml_gelar*gelaran);
			sisa_planning = sisa_planning.toFixed(2);
			$("#sisa_planning_"+i).val(sisa_planning);
		}
}

function hitungsisa() {
	var jum_detail= $('#no').val()-1;

		for (i=1;i<=jum_detail;i++) {
			var jml_gelar=$("#jml_gelar_"+i).val();
			var allowance=$("#allowance_"+i).val();
			var sisa_planning=$("#sisa_planning_"+i).val();
			var sisa_aktual=$("#sisa_aktual_"+i).val();
					
			var sisa_allow = (parseFloat(jml_gelar)*parseFloat(allowance))/100;
			sisa_allow = sisa_allow.toFixed(2);
			$("#sisa_allow_"+i).val(sisa_allow);
			
			var sisa_allow=$("#sisa_allow_"+i).val();
			var selisihnya = parseFloat(sisa_planning)-parseFloat(sisa_aktual)-parseFloat(sisa_allow);
			selisihnya = selisihnya.toFixed(2);
			$("#selisih_"+i).val(selisihnya);
		}
}

</script>
<center><h3>Input Data-Data Detail</h3></center>
<div align="center">

<form id="f_master_brg" name="f_master_brg">

<?php 	
		if ($is_edit == 1) {
			// query utk ambil detailnya berdasarkan id_realisasi_detail
			$query1	= $this->db->query(" SELECT * FROM tt_realisasi_cutting_detail where id='$id_realisasi_detail' ");
			//if ($query1->num_rows()>0) { 
				$hasil1 = $query1->row();
				$jml_lot = $hasil1->jum_lot;
				$pjg_kain = $hasil1->detail_panjang_kain_mtr;
				$jml_gelar = $hasil1->detail_jum_gelar;
				$hsl_potong = $hasil1->detail_hasil_potong;
				$sisa_planning = $hasil1->sisa_planning_mtr;
				$sisa_aktual = $hasil1->sisa_aktual_mtr;
				$sisa_allow = $hasil1->sisa_allow_mtr;
				$sisa_selisih = $hasil1->sisa_selisih_mtr;
				$allowance = $hasil1->allowance;
				
				$list_jml_lot = explode(";", $jml_lot);
				$list_pjg_kain = explode(";", $pjg_kain);
				$list_jml_gelar = explode(";", $jml_gelar);
				$list_hsl_potong = explode(";", $hsl_potong);
				$list_sisa_planning = explode(";", $sisa_planning);
				$list_sisa_aktual = explode(";", $sisa_aktual);
				$list_sisa_allow = explode(";", $sisa_allow);
				$list_sisa_selisih = explode(";", $sisa_selisih);
				$list_allowance = explode(";", $allowance);
				$jumlah_list = count($list_hsl_potong); 
			//}	
		 ?>
<input type="hidden" name="no" id="no" value="<?php echo $jumlah_list ?>">
<?php } else { ?>
<input type="hidden" name="no" id="no" value="2">
<?php } ?>
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
</form>

  <table border="0" id="tabelku" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
  <?php if ($is_edit == 0) { ?>
  <tr>
		<td colspan="11" align="left">
		<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item ">&nbsp;
		<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item ">
		</td>
	</tr>
	<?php } ?>

    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Jml Lot</th>
      <th bgcolor="#999999">Pjg Kain (m)</th>
      <th bgcolor="#999999">Gelaran</th>
      <th bgcolor="#999999">Set</th>
      <th bgcolor="#999999">Jml Gelar</th>
      <th bgcolor="#999999">Hsl Potong</th>
      <th bgcolor="#999999">Sisa Planning(m)</th>
      <th bgcolor="#999999">Sisa Aktual(m)</th>
      <th bgcolor="#999999">Allowance(cm)</th>
      <th bgcolor="#999999">Sisa Allow(m)</th>
      <th bgcolor="#999999">Selisih sisa(m)</th>

    </tr>
    <?php if ($is_edit == 0) { ?>
	<tr align="center">
		<td align="center" id="num_1">1</td>
		<td><input type="text" name="jml_lot_1" id="jml_lot_1" size="5" value=""></td>
		<td><input type="text" name="pjg_kain_1" id="pjg_kain_1" size="5" value=""></td>
		<td><input type="text" name="gelaran_1" id="gelaran_1" size="5" value="<?php echo $gelaran ?>" readonly="true"></td>
		<td><input type="text" name="set_1" id="set_1" size="5" value="<?php echo $jum_set ?>" readonly="true"></td>
		<td><input type="text" name="jml_gelar_1" id="jml_gelar_1" size="5" value="" onkeyup="hitunghasil()" onblur="hitunghasil()"></td>
		<td><input type="text" name="hsl_potong_1" id="hsl_potong_1" size="5" value=""></td>
		<td><input type="text" name="sisa_planning_1" id="sisa_planning_1" size="5" value=""></td>
		<td><input type="text" name="sisa_aktual_1" id="sisa_aktual_1" size="5" value=""></td>
		<td><input type="text" name="allowance_1" id="allowance_1" size="5" value="" onkeyup="hitungsisa()" onblur="hitungsisa()"></td>
		<td><input type="text" name="sisa_allow_1" id="sisa_allow_1" size="5" value=""></td>
		<td><input type="text" name="selisih_1" id="selisih_1" size="5" value=""></td>
	</tr>
	<?php } else { 
		
			$jumlah_list = count($list_hsl_potong)-1;
			$i=1;
			for($j=0; $j<$jumlah_list; $j++) {
			?>
			
			<tr align="center">
				<td align="center" id="num_<?php echo $i; ?>"><?php echo $i; ?></td>
				<td><input type="text" name="jml_lot_<?php echo $i; ?>" id="jml_lot_<?php echo $i; ?>" size="5" value="<?php echo $list_jml_lot[$j] ?>"></td>
				<td><input type="text" name="pjg_kain_<?php echo $i; ?>" id="pjg_kain_<?php echo $i; ?>" size="5" value="<?php echo $list_pjg_kain[$j] ?>"></td>
				<td><input type="text" name="gelaran_<?php echo $i; ?>" id="gelaran_<?php echo $i; ?>" size="5" value="<?php echo $gelaran ?>" readonly="true"></td>
				<td><input type="text" name="set_<?php echo $i; ?>" id="set_<?php echo $i; ?>" size="5" value="<?php echo $jum_set ?>" readonly="true"></td>
				<td><input type="text" name="jml_gelar_<?php echo $i; ?>" id="jml_gelar_<?php echo $i; ?>" size="5" value="<?php echo $list_jml_gelar[$j] ?>" onkeyup="hitunghasil()" onblur="hitunghasil()"></td>
				<td><input type="text" name="hsl_potong_<?php echo $i; ?>" id="hsl_potong_<?php echo $i; ?>" size="5" value="<?php echo $list_hsl_potong[$j] ?>"></td>
				<td><input type="text" name="sisa_planning_<?php echo $i; ?>" id="sisa_planning_<?php echo $i; ?>" size="5" value="<?php echo $list_sisa_planning[$j] ?>"></td>
				<td><input type="text" name="sisa_aktual_<?php echo $i; ?>" id="sisa_aktual_<?php echo $i; ?>" size="5" value="<?php echo $list_sisa_aktual[$j] ?>"></td>
				<td><input type="text" name="allowance_<?php echo $i; ?>" id="allowance_<?php echo $i; ?>" size="5" value="<?php echo $list_allowance[$j] ?>" onkeyup="hitungsisa()" onblur="hitungsisa()"></td>
				<td><input type="text" name="sisa_allow_<?php echo $i; ?>" id="sisa_allow_<?php echo $i; ?>" size="5" value="<?php echo $list_sisa_allow[$j] ?>"></td>
				<td><input type="text" name="selisih_<?php echo $i; ?>" id="selisih_<?php echo $i; ?>" size="5" value="<?php echo $list_sisa_selisih[$j] ?>"></td>
			</tr>
			
			<?php	
			$i++;
			}
		
		?>
		
	<?php } ?>
  </table>
  <input type="button" id="pilih" name="pilih" value="OK">
</div>
