<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$("#no").val('2');
	
	$("#pilih").click(function()
	{
		
		var jum= $('#no').val()-1; 
		var proses= $('#proses').val(); 
		//opener.document.forms["f_purchase"].gelar_pjg_kain_<?php echo $posisi ?>.value='';
		if (proses == 1)
			opener.document.forms["f_purchase"].jml_lot_<?php echo $posisi ?>.value='';
		
		var gabung = ""; var gabung2 = ""; var j_gelar = 0; var j_potong = 0;
		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				if($('#detailnya_'+k).val() == '0' || $('#detailnya_'+k).val() == '' ) {				
					alert("Data detailnya tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#detailnya_'+k).val()) ) {
					alert("Data detailnya harus berupa angka..!");
					return false;
				}
				
				gabung += $('#detailnya_'+k).val()+";";
				j_gelar += parseFloat($('#detailnya_'+k).val());
				j_potong += parseFloat($('#hsl_potong_'+k).val());
				
				if (proses == 1)
					opener.document.forms["f_purchase"].jml_lot_<?php echo $posisi ?>.value= gabung;
				else if (proses == 2) {
					opener.document.forms["f_purchase"].pjg_kain_<?php echo $posisi ?>.value= gabung;
					//opener.document.forms["f_purchase"].hide_pjg_kain_<?php echo $posisi ?>.value= j_gelar;
				}
				else if (proses == 3) {
					opener.document.forms["f_purchase"].jml_gelar_<?php echo $posisi ?>.value= gabung;
					gabung2 += $('#hsl_potong_'+k).val()+";";
					opener.document.forms["f_purchase"].hsl_potong_<?php echo $posisi ?>.value= gabung2;
					opener.document.forms["f_purchase"].hide_jml_gelar_<?php echo $posisi ?>.value= j_gelar;
					opener.document.forms["f_purchase"].hide_hsl_potong_<?php echo $posisi ?>.value= j_potong;
				}
				else if (proses == 5) {
					opener.document.forms["f_purchase"].aktual_<?php echo $posisi ?>.value= gabung;
					//opener.document.forms["f_purchase"].hide_aktual_<?php echo $posisi ?>.value= $('#detailnya_'+k).val();
				}
			}
			
		}
		else {
			alert("Data detail tidak ada");
			return false;
		}
		
		self.close();
	});
	
	
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var is_set=$("#is_set").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
				
		//*****detailnya*************************************
		var detailnya="#detailnya_"+n;
		var new_detailnya="#detailnya_"+no;
		$(detailnya, lastRow).attr("id", "detailnya_"+no);
		$(new_detailnya, lastRow).attr("name", "detailnya_"+no);		
		$(new_detailnya, lastRow).val('');				
		//*****end detailnya*************************************	
		
		if (is_set != '') {
			//*****set*************************************
			var set="#set_"+n;
			var new_set="#set_"+no;
			$(set, lastRow).attr("id", "set_"+no);
			$(new_set, lastRow).attr("name", "set_"+no);		
			$(new_set, lastRow).val(is_set);
			//*****end set*************************************	
			
			//*****hsl_potong*************************************
			var hsl_potong="#hsl_potong_"+n;
			var new_hsl_potong="#hsl_potong_"+no;
			$(hsl_potong, lastRow).attr("id", "hsl_potong_"+no);
			$(new_hsl_potong, lastRow).attr("name", "hsl_potong_"+no);		
			$(new_hsl_potong, lastRow).val('');	// sampe sini, yg perhitungan utk hsl potong (set x jml gelar) lanjut besok lagii
			//*****end hsl_potong*************************************	
		}
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});

function hitunghasil() {
	var jum_detail= $('#no').val()-1;

				var i=1;
				//var gelaran=$("#gelaran").val(); 
				for (i=1;i<=jum_detail;i++) {
					var jum_set=$("#set_"+i).val();
					var detailnya=$("#detailnya_"+i).val();
					
					var hsl_potong = parseFloat(jum_set)*parseFloat(detailnya);
					hsl_potong = hsl_potong.toFixed(2);
					
					$("#hsl_potong_"+i).val(hsl_potong);
				}
}

</script>
<center><h3>Input Data Detail <?php echo $nama_proses; ?></h3></center>
<div align="center">

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="proses" id="proses" value="<?php echo $proses ?>">
<input type="hidden" name="is_set" id="is_set" value="<?php echo $is_set ?>">
</form>

  <table border="0" id="tabelku" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
  <tr>
			<td colspan="4" align="right">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item ">
			</td>
		</tr>

    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999"><?php echo $nama_proses ?></th>
      <?php if ($proses == '3') { ?>
      <th bgcolor="#999999">Set</th>
      <th bgcolor="#999999">Hasil Potong</th>
      <?php } ?>

    </tr>
	<tr align="center">
		<td align="center" id="num_1">1</td>
		<td><input type="text" name="detailnya_1" id="detailnya_1" size="5" value="" <?php if ($proses == 3) { ?> onkeyup="hitunghasil()" onblur="hitunghasil()" <?php } ?> ></td>
		<?php if ($proses == '3') { ?>
		<td><input type="text" name="set_1" id="set_1" size="5" value="<?php echo $is_set ?>" readonly="true" ></td>
		<td><input type="text" name="hsl_potong_1" id="hsl_potong_1" size="5" value="" ></td>
		<?php } ?>
	</tr>
  </table>
  <input type="button" id="pilih" name="pilih" value="OK">
</div>
