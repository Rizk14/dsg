<h3>Input Periode di gudang Bahan Baku/Pembantu</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<a href="<?php echo base_url(); ?>index.php/periode/cform/view">View Data</a>
<?php 
$attributes = array('name' => 'f_periode', 'id' => 'f_periode');
echo form_open('periode/cform/submit', $attributes); ?>

<table >
	<tr>
    <td>Tgl Periode : </td>
    <td>
	<label>
      <input name="tgl_periode" type="text" id="tgl_periode" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_periode" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_periode,'dd-mm-yyyy',this)">
	</td>
  </tr>
		
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/periode/cform'">
<?php echo form_close();  ?>
