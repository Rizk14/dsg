<h3>Data Harga Bahan Baku/Pembantu Berdasarkan Supplier</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-bb-sup/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-bb-sup/cform/view">View Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-bb-sup/cform/print_harga">Print/Export Excel</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

 $(function()
 {
		
	$('#pilih_jenis').click(function(){

	  	    var id_kel_brg= jQuery('#kel_brg').val();

			var urlnya = "<?php echo base_url(); ?>index.php/mst-bb-sup/cform/show_popup_jenis/"+id_kel_brg;

			openCenteredWindow(urlnya);

	  });
	 	  
	  $('#kel_brg').change(function(){
	  	    $("#id_jenis").val('');	
	  	    $("#kode_jenis").val('');		  	    
	  });

 });
 
 function cek_data() {
	var id_jenis= $('#id_jenis').val();

/*	if (id_jenis == '') {
		alert("Jenis Barang harus dipilih..!");
		$('#kode_jenis').focus();
		return false;
	}
	if (id_jenis_bhn == '') {
		alert("Jenis Bahan harus dipilih..!");
		$('#kode_jenis_bhn').focus();
		return false;
	} */	
 }	

 function cek_cb() {
	var ceknya = $(":checkbox:checked").length;
	var hitung = $('#jum').val();

	if (ceknya == 0) {
		alert("Data-data barangnya harus dipilih..!");
		return false;
	}
	else {
		for (var k=1; k <= hitung; k++) {
			if ($('input[name=cek_'+k+']').is(':checked')== true) {
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Harga harus berupa angka..!");
					return false;
				}
			} // end if
		}
	}
	
}
</script>

<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
echo form_open('mst-bb-sup/cform/proseslistbrg', $attributes); ?>
<table>
		<tr>
			<td>Supplier</td>
			<td> <select name="supplier" id="supplier">
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kel_brg" id="kel_brg">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td> <input type="text" name="kode_jenis" id="kode_jenis" value="" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang">
			</td>
		</tr>
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb-sup/cform/view'">
<?php echo form_close(); 
}
else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/mst-bb-sup/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_supplier" value="<?php echo $id_supplier ?>">
<input type="hidden" name="pkp" value="<?php echo $pkp ?>">
<input type="hidden" name="jum" id="jum" value="<?php echo $jum ?>">

<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td width="15%">Supplier</td>
		<td width="70%">
		  <?php echo $nama_supplier; ?>
		</td>
    </tr>
	<tr>
		<td>Kelompok Barang</td>
		<td>
		  <?php echo $kel_brg." - ".$nama_kel; ?>
		</td>
	</tr>
	<tr>
		<td>Jenis Barang</td>
		<td>
		  <?php echo $nama_jenis; ?>
		</td>
	</tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
	      <th>Harga (Rp.)</th>
	      <th>Pilih</th>
        </tr>

        <?php $i=1;
        if (!is_array($list_brg) ) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap" colspan="3">
		   Data barang tidak ada atau sudah ada harganya</td>
          
        </tr>
        <tr>
			<td colspan="5"><input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb-sup/cform/'"></td>
        </tr>
		
		<?php
		} else {
			$i= 1;
		  if (is_array($list_brg)) {
			 for($j=0;$j<count($list_brg);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $list_brg[$j]['kode_brg'] ?>"/>
           <input name="id_brg_<?php echo $i ?>" type="hidden" id="id_brg_<?php echo $i ?>" value="<?php echo $list_brg[$j]['id_brg'] ?>"/>
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="35" readonly="true" value="<?php echo $list_brg[$j]['nama_brg'] ?>" /></td>
          <td><input name="nama_satuan_<?php echo $i ?>" type="text" id="nama_satuan_<?php echo $i ?>" size="7" readonly="true" value="<?php echo $list_brg[$j]['nama_satuan'] ?>" />
           <input name="id_satuan_<?php echo $i ?>" type="hidden" id="id_satuan_<?php echo $i ?>" value="<?php echo $list_brg[$j]['id_satuan'] ?>"/>
            <input name="id_satuan_konversi_<?php echo $i ?>" type="hidden" id="id_satuan_konversi_<?php echo $i ?>" value="<?php echo $list_brg[$j]['id_satuan_konversi'] ?>"/>
          </td>
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="7" maxlength="7" value="<?php echo $list_brg[$j]['harga'] ?>" /></td>
          <td><input name="cek_<?php echo $i ?>" type="checkbox" id="cek_<?php echo $i ?>" value="y" /></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		
		?>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_cb();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-bb-sup/cform/'"></td>
	</tr>
	<?php } ?>
	</table>	
	
	</form>
		</td>
    </tr>

</table>
</div>
</form>
<?php } ?>
