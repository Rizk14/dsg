<style type="text/css">
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 14px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	window.print();
});

</script>

<table border="0" width="100%" class="tabelheader">
	<tr>
		<td colspan="2" align="center"><b>DATA HARGA PEMBELIAN BAHAN BAKU/PEMBANTU <br>
		Supplier: <?php if ($supplier != '0') echo $kode_supplier." - ".$nama_supplier; else echo "All"; ?>
		</b>
		</td>
	</tr>

</table>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isinya">
	<thead>
	 <tr>
		<th width="20%">Supplier</th>
		<th width="10%">Kelompok Brg</th>
		<th width="30%">Nama Barang</th>
		<th width="7%">Satuan</th>
		<th width="7%">Harga (Rp.)</th>
		<th width="7%">Harga Sebelum PPn (Rp.)</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			if (is_array($query)) {
				$sup_temp = "";
				for($j=0;$j<count($query);$j++){
					
		?>
			<tr>
				<td><?php 
					if ($sup_temp != $query[$j]['kode_supplier']) {
						$sup_temp = $query[$j]['kode_supplier'];
						echo $query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']; 
					}
					else
						echo "&nbsp;";
					
					?></td>
				<td style="white-space:nowrap;"><?php echo $query[$j]['nama_kel_brg'] ?></td>
				<td style="white-space:nowrap;"><?php echo $query[$j]['kode_brg']." - ".$query[$j]['nama_brg'] ?></td>
				<td><?php echo $query[$j]['satuan'] ?></td>
				<td align="right"><?php echo number_format($query[$j]['harga'], 2,',','.') ?></td>
				<td align="right"><?php if ($query[$j]['pkp'] == 't') echo number_format($query[$j]['harga']/1.1, 2,',','.'); else echo "0"; ?></td>
			</tr>
		<?php			
				}
			}
		?>
	</tbody>
</table>
