<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    .judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var id_jenis=$("#id_jenis").val(); 
		var kodenya=$("#kode").val(); 
		var des=$("#nama").val();
		opener.document.forms["f_bb"].kode_jenis.value=kodenya+" - "+des;
		opener.document.forms["f_bb"].id_jenis.value = id_jenis;
		
		self.close();
	});
});
</script>

<center><h3>Daftar Jenis Barang Untuk Kelompok <?php echo $nama_kel; ?> </h3></center>
<div align="center"><br>
<?php echo form_open('mst-bb-sup/cform/show_popup_jenis'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">
<input type="hidden" name="kel_brg" value="<?php echo $kel_brg ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_jenis" id="id_jenis">
<input type="hidden" name="kode" id="kode">
<input type="hidden" name="nama" id="nama">

	<table border="1" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" nowrap="nowrap" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th>Kode</th>
		 <th>Nama Jenis</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$row->kode."</td>";
				 echo    "<td>".$row->nama."</td>";
				 echo    "<td>".$tgl_update."</td>"; ?>
				 <td align=center><a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.id_jenis.value='<?php echo $row->id ?>';
				 window.document.f_master_brg.kode.value='<?php echo $row->kode ?>'; 
				 window.document.f_master_brg.nama.value='<?php echo $row->nama ?>'">Pilih</a></td>
				<?php echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table>
</form>
  <?php echo $this->pagination->create_links();?>
</div>
