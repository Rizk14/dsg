<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">


function konfirm() {
	
	var jmlpebayaran = document.getElementById('jmlpebayaran');
	
	if(document.getElementById('wajibpajak').value==''){
		alert("Maaf, Kolom Nama WP hrs diisi!");
		document.getElementById('wajibpajak').focus();
		return false;		
	}
	
	if(document.getElementById('kdakunpajak').value==''){
		alert("Maaf, Kode Akun Pajak hrs diisi!");
		document.getElementById('kdakunpajak').focus();
		return false;		
	}	

	if(document.getElementById('kdjnssetor').value==''){
		alert("Maaf, Kode Jenis Setoran hrs diisi!");
		document.getElementById('kdjnssetor').focus();
		return false;		
	}

	if(document.getElementById('uraipembayaran').value==''){
		alert("Maaf, Kolom Uraian Pembayaran hrs diisi!");
		document.getElementById('uraipembayaran').focus();
		return false;		
	}	

	if(!parseInt(jmlpebayaran.value)){
		alert("Maaf, Kolom Pembayaran hrs angka!");
		jmlpebayaran.focus();
		return false;
	}
	
	if(document.getElementById('tmptwp').value==''){
		alert("Maaf, Kolom Tempat WP hrs diisi!");
		document.getElementById('tmptwp').focus();
		return false;		
	}	

	if(document.getElementById('tglwp').value==''){
		alert("Maaf, Kolom Tanggal WP hrs diisi!");
		document.getElementById('tglwp').focus();
		return false;		
	}	

	if(document.getElementById('penyetor').value==''){
		alert("Maaf, Kolom Penyetor hrs diisi!");
		document.getElementById('penyetor').focus();
		return false;		
	}			
}
</script>

<?php
$tgsekarang	= date('d/m/Y');
?>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
	  <td class="tcat"><?php echo $page_title_ssp; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
	 <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> FORM <?php echo $page_title_ssp; ?></td></tr>
	<tr>
	 <td class="alt2" style="padding:0px;">
		<table id="table-add-box">
		  <tr>
			<td align="left">
				  <?php 
				   $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('ssp/cform/simpan', $attributes);?>
				
				<div id="mastersspform">
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="21%"><?php echo $form_nama_wp_ssp; ?></td>
						<td width="1%">&nbsp;</td>
						<td width="78%"><input name="wajibpajak" type="text" id="wajibpajak" maxlength="200" onclick="cWP();" readonly="true"/><input type="hidden" name="i_initial" id="i_initial" /></td>
					  </tr>
					  <tr>
						<td><?php echo $form_kd_akun_pajak_ssp; ?></td>
						<td>&nbsp;</td>
						<td><input name="kdakunpajak" type="text" id="kdakunpajak" maxlength="100" onclick="ckodejnssetorpajak();" readonly="true"/></td>
					  </tr>
					  <tr>
						<td><?php echo $form_kd_jns_setor_ssp; ?></td>
						<td>&nbsp;</td>
						<td><input name="kdjnssetor" type="text" id="kdjnssetor" maxlength="100" readonly="true"/></td>
					  </tr>
					  <tr>
						<td><?php echo $form_uraian_pembayaran_spp; ?></td>
						<td>&nbsp;</td>
						<td><input name="uraipembayaran" type="text" id="uraipembayaran" maxlength="250" readonly="true"/></td>
					  </tr>
					  <tr>
						<td><?php echo $form_masa_pajak_ssp; ?></td>
						<td>&nbsp;</td>
						<td>
						  <?php echo $form_masa_pajak_bln_ssp; ?>
						  <?php
						  $arrbl	= array(
						  	'01'=>'Januari',
							'02'=>'Februari',
							'03'=>'Maret',
							'04'=>'April',
							'05'=>'Mei',
							'06'=>'Juni',
							'07'=>'Juli',
							'08'=>'Agustus',
							'09'=>'September',
							'10'=>'Oktober',
							'11'=>'Nopember',
							'12'=>'Desember');
						  ?>
						  
						  <select name="bln" id="bln">
						  <?php
						  $bl	= date('m');
						  foreach($arrbl as $key => $val){
						  	$sel	= ($bl==$key)?("selected"):("");
						  	echo "<option value=\"$key\" $sel >{$val}</option>";
						  }
						  ?>
						  </select>
						  
						  <?php echo $form_masa_pajak_thn_ssp; ?>
						  <select name="thn" id="thn">
						  <?php
						  $thn=date('Y');
						  for(($thn1=$thn-5);$thn1<=$thn+5; $thn1++){
						  	$sel	= ($thn1==$thn)?("selected"):("");
						  	echo "<option value=\"$thn1\" $sel >{$thn1}</option>";
						  }
						  ?>
						  </select>
						  </td>
					  </tr>
					  <tr>
						<td><?php echo $form_jml_pembayaran_ssp; ?></td>
						<td>&nbsp;</td>
						<td><input name="jmlpebayaran" type="text" id="jmlpebayaran" maxlength="100" /></td>
					  </tr>
					  <tr>
						<td colspan="3">&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan="3"><?php echo strtoupper($form_wjb_pajak_penyetor_ssp); ?></td>
					  </tr>
					  <tr>
						<td><?php echo $form_wjb_pajak_penyetor_tempat_ssp; ?></td>
						<td>&nbsp;</td>
						<td><input name="tmptwp" type="text" id="tmptwp" maxlength="200" value="Bandung" /></td>
					  </tr>
					  <tr>
						<td><?php echo $form_wjb_pajak_penyetor_tgl_ssp; ?></td>
						<td>&nbsp;</td>
						<td><input name="tglwp" type="text" id="tglwp" maxlength="10" value="<?=$tgsekarang?>"onclick="displayCalendar(document.forms[0].tglwp,'dd/mm/yyyy',this)"/></td>
					  </tr>
					  <tr>
						<td><?php echo $form_wjb_pajak_penyetor_penyetor_ssp; ?></td>
						<td>&nbsp;</td>
						<td><input name="penyetor" type="text" id="penyetor" maxlength="200" onclick="csetor();" /><input type="hidden" name="ipenyetor" id="ipenyetor" /></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><input name="btnsimpan" type="submit" id="btnsimpan" value="Simpan" onclick="return konfirm();" />
						<input name="btnbatal" id="btnbatal" value="Batal" type="reset" onclick="window.location='<?php echo base_url(); ?>index.php/ssp/cform/'">
					
					  </tr> 
					</table>
			  	</div>
			<?php echo form_close();?>
			</td>
		  </tr> 
		</table>

	 </td>
	</tr>
</table>
