<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var username= $('#username').val();
	var passwd= $('#passwd').val();
	var nama= $('#nama').val();

	if (username == '') {
		alert("Username harus diisi..!");
		$('#username').focus();
		return false;
	}
	if (passwd == '') {
		alert("Password harus diisi..!");
		$('#passwd').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
}

$(function()
{
		
});
	
</script>

<h3>Data User Program</h3><br>
<a href="<?php echo base_url(); ?>index.php/users-admin-user/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/users-admin-user/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
	echo form_open('users-admin-user/cform/submit', $attributes); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> 
<input type="hidden" name="uid_user" value="<?php echo $eid ?>">
 <?php } ?>
	<table>
		<tr>
			<td>Group User</td>
			<td> : <select name="group_user" id="group_user">
				<?php foreach ($list_group as $groupnya) { ?>
					<option value="<?php echo $groupnya->gid ?>" <?php if ($groupnya->gid == $egid) { ?> selected="true" <?php } ?> ><?php echo $groupnya->nama_group ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>

		<tr>
			<td>Username</td>
			<td> : <input type="text" name="username" id="username" value="<?php echo $eusername ?>" <?php if ($edit == '1') { ?> readonly="true" <?php } ?> maxlength="20" size="20">
			</td>
		</tr>
		<tr>
			<td>Password</td>
			<td> : <input type="password" name="passwd" id="passwd" value="<?php echo $epasswd ?>" maxlength="20" size="20"></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td> : <input type="text" name="nama" id="nama" value="<?php echo $enama ?>" maxlength="30" size="30">
			</td>
		</tr>
		
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/users-admin-user/cform/view'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

