<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 3600;

t = null;
function closeMe() {
	t = setTimeout("self.close()",howLong);
}

</script>

</head>

<body onLoad="closeMe();">
<!--<body> -->

<!--
<span style="margin-top:10px; text-align:center; color:#FF0000; font-size:11px; font-weight:bold; font-family:Verdana, Arial, Helvetica, sans-serif; text-decoration:blink;">
Mohon utk tidak ditutup !!<br />
Sedang melakukan pencetakan, silahkan tunggu....
</span>
-->

<?php
echo "Printer: ".$printer_name;
?>

<?php

$get_attributes	= false;
$waktu	= date("H:i:s");
$line	= 80;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php"); 
}else{
	include_once("printipp_classes/BasicIPP.php");
}

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

$List = "";

$range	= $line-(strlen('delivery order')+strlen($nminitial)+14);

$List	.= CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(14).CHR(27).CHR(69)."Delivery Order".CHR(27).CHR(69).CHR(0).CHR(20).CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0).str_repeat(" ",$range).CHR(27).CHR(69).$nminitial.CHR(27).CHR(70)."\n\n";

$range0	= $line-2;
$List	.= CHR(218).str_repeat(CHR(196),($line-2)).CHR(191)."\n";

$range1	= ( $line-(1+strlen('nomor do')+2+1+2+strlen($nomor))-(strlen('nomor op :')+strlen($nomorop)+1) );
$List	.= CHR(179).CHR(27).CHR(52)."Nomor DO".CHR(27).CHR(53).str_repeat(" ",2).":".str_repeat(" ",2).CHR(27).CHR(69).$nomor.CHR(27).CHR(70).str_repeat(" ",$range1).CHR(27).CHR(52)."Nomor OP :".CHR(27).CHR(53).CHR(27).CHR(69).$nomorop.CHR(27).CHR(70).CHR(179).CHR(0)."\n";
	
$range2	= $line-2;

$List	.= CHR(192).str_repeat(CHR(196),$range2).CHR(217)."\n";
	
$List	.= "\n";

$rangetgldo0	= (strlen('customer/area')+2)-strlen('tanggal do');

$List	.= "Tanggal DO".str_repeat(" ",$rangetgldo0).":".str_repeat(" ",2).$dotgl." ".$dobln." ".$dothn."\n";

$List	.= "Customer/Area".str_repeat(" ",2).":".str_repeat(" ",2).$nmcabang."\n";

$List	.= "\n";

$range3	= ($line-(strlen('no')+1+strlen('no.op')+2+strlen('kode')+4+strlen('jumlah')+1+strlen('keterangan')+1+strlen('nama barang')))-10; // +10

$List	.= CHR(27).CHR(69)."No".str_repeat(" ",1)."No.OP".str_repeat(" ",2)."Kode".str_repeat(" ",4)."Nama Barang".str_repeat(" ",$range3)."Jumlah"." "."Keterangan".str_repeat(" ",1).CHR(27).CHR(70)."\n";
$List	.= str_repeat(CHR(196),$line)."\n";

$no	= 1;
$spcno	= 5;
$spcop	= 12;
$spckb	= 14;
$spcnb	= 58;
$spcjml	= 10;
$spcket	= 30;

$arrno	= array();
$arrop	= array();
$arrproc= array();
$arrnm	= array();
$arrqty	= array();
$arrket	= array();

$lup	= 0;

foreach($isi as $row) {
		 $db2 = $this->load->database('db_external', TRUE);
		$detailwarna= "";
		// 08-09-2014 --------
		$sqlxx = " SELECT a.i_color, b.e_color_name, a.qty FROM tm_do_item_color a 
				  INNER JOIN tr_color b ON a.i_color=b.i_color
				  WHERE a.i_do_item = '".$row->i_do_item."' ";
						
		$queryxx = $db2->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			
			$detailwarna = "(";
			foreach ($hasilxx as $rowxx) {
				if ($rowxx->qty != 0) {
					// 27-11-2015
					if (trim($rowxx->e_color_name) == 'MERAH MAROON')
						$rowxx->e_color_name = 'MRH MRN';
					
					$detailwarna.= $rowxx->e_color_name.":".$rowxx->qty." ";
				}
			}
			$detailwarna .= ")";
		}
		else
			$detailwarna="";
		// -------------------
		
		$nmmotif	= $row->motifname;
		$arrno[$lup]	= (strlen($no)<=$spcno)?$spcno-strlen($no):strlen($no);
		$trimop	= trim($row->iop);
		$test	= strlen($trimop);
		$arrop[$lup]	= $test<=$spcop?($spcop-$test):$test;
		$arrproc[$lup]	= (strlen($row->iproduct)<=$spckb)?$spckb-strlen($row->iproduct):strlen($row->iproduct);
		$arrnm[$lup]	= strlen($row->motifname)<=$spcnb?$spcnb-(strlen($row->motifname)):strlen($row->motifname);
		$arrqty[$lup]	= (strlen($row->qty)<=$spcjml)?$spcjml-strlen($row->qty):strlen($row->qty);
		$arrket[$lup]	= (strlen($row->keterangan)<=$spcket)?$spcket-strlen($row->keterangan):strlen($row->keterangan);
		
		$List	.= CHR(15);
		
		// 27-11-2015
		if (trim($row->keterangan) != '')
			$teksket = $row->keterangan.str_repeat(" ",1);
		else
			$teksket = "";
		
		$List	.= $no.str_repeat(" ",$arrno[$lup]).$row->iop.str_repeat(" ",$arrop[$lup]).$row->iproduct.str_repeat(" ",$arrproc[$lup]).trim($nmmotif).str_repeat(" ",$arrnm[$lup]).str_repeat(" ",$arrqty[$lup]).$row->qty.str_repeat(" ",2).$teksket.$detailwarna.str_repeat(" ",1)."\n";
		
		$List	.= CHR(18);
		
		$no+=1;
		$lup++;
		
}

$List	.= str_repeat(CHR(196),$line)."\n";

$List	.= "\n";

$List	.= CHR(27).CHR(69).str_repeat(" ",11)."Penerima".str_repeat(" ",15)."Yang Membawa".str_repeat(" ",12)."Kepala Gudang".CHR(27).CHR(70)."\n";
	
$List	.= str_repeat("\n",2);
$List .= "\n";

$List	.= str_repeat(" ",7)."(".str_repeat(" ",13).")".str_repeat(" ",10)."(".str_repeat(" ",13).")".str_repeat(" ",11)."("."Manda Adi Eso".")"."\n";

$tglprnt	= explode("-",$tglprint,strlen($tglprint));
$tg_print	= html_entity_decode($tglprnt[0]);
$bl_print	= html_entity_decode($tglprnt[1]);
$th_print	= html_entity_decode($tglprnt[2]);

$List	.= CHR(15).$tg_print." ".$bl_print." ".$th_print." ".$waktu.CHR(18)."\n";

$List	.= str_repeat(CHR(196),$line)."\n";
//echo $List; die();
$ipp->setData($List);
$ipp->printJob();

/*** Original 

// Function 

// Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
// Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
// Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
// Italic	= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)

$range	= $line-(strlen('delivery order')+strlen($nminitial)+14);
$ipp->setData(CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(14).CHR(27).CHR(69)."Delivery Order".CHR(27).CHR(69).CHR(0).CHR(20).CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0).str_repeat(" ",$range).CHR(27).CHR(69).$nminitial.CHR(27).CHR(70)."\n\n");
$ipp->printJob();

$range0	= $line-2;
$ipp->setData(chr(218).str_repeat(chr(196),($line-2)).chr(191)."\n");
$ipp->printJob();
	
$range1	= ( $line-(1+strlen('nomor do')+2+1+2+strlen($nomor))-(strlen('nomor op :')+strlen($nomorop)+1) );
$ipp->setData(CHR(179).CHR(27).CHR(52)."Nomor DO".CHR(27).CHR(53).str_repeat(" ",2).":".str_repeat(" ",2).CHR(27).CHR(69).$nomor.CHR(27).CHR(70).str_repeat(" ",$range1).CHR(27).CHR(52)."Nomor OP :".CHR(27).CHR(53).CHR(27).CHR(69).$nomorop.CHR(27).CHR(70).CHR(179).CHR(0));
$ipp->printJob();
	
$range2	= $line-2;
$ipp->setData(chr(192).str_repeat(chr(196),$range2).chr(217)."\n");
$ipp->printJob();
	
$ipp->setData("\n");
$ipp->printJob();
	
$rangetgldo0	= (strlen('customer/area')+2)-strlen('tanggal do');
$ipp->setData("Tanggal DO".str_repeat(" ",$rangetgldo0).":".str_repeat(" ",2).$dotgl." ".$dobln." ".$dothn."\n");
$ipp->printJob();

$ipp->setData("Customer/Area".str_repeat(" ",2).":".str_repeat(" ",2).$nmcabang."\n");
$ipp->printJob();

$ipp->setData("\n");
$ipp->printJob();

$range3	= ($line-(strlen('no')+1+strlen('no.op')+2+strlen('kode')+4+strlen('jumlah')+1+strlen('keterangan')+1+strlen('nama barang')))-10; // +10
$ipp->setData(CHR(27).CHR(69)."No".str_repeat(" ",1)."No.OP".str_repeat(" ",2)."Kode".str_repeat(" ",4)."Nama Barang".str_repeat(" ",$range3)."Jumlah"." "."Keterangan".str_repeat(" ",1).CHR(27).CHR(70)."\n");
$ipp->printJob();

$ipp->setData(str_repeat(chr(196),$line)."\n");
$ipp->printJob();

$no	= 1;
$spcno	= 5; // sesuaikan dgn header No + 2
$spcop	= 12; // +5
$spckb	= 14; // +5
//$spcnb	= (11+$range3)-0;
$spcnb	= 58;
$spcjml	= 10;
$spcket	= 30;

$arrno	= array();
$arrop	= array();
$arrproc= array();
$arrnm	= array();
$arrqty	= array();
$arrket	= array();

$lup	= 0;

foreach($isi as $row) {
	//if($lup<9) {
		$nmmotif	= $row->motifname;
		$arrno[$lup]	= (strlen($no)<=$spcno)?$spcno-strlen($no):strlen($no);
		//$arrop[$lup]	= (strlen(trim($row->iop))<=$spcop)?$spcop-strlen(trim($row->iop)):strlen(trim($row->iop));
		$trimop	= trim($row->iop);
		$test	= strlen($trimop);
		$arrop[$lup]	= $test<=$spcop?($spcop-$test):$test;
		$arrproc[$lup]	= (strlen($row->iproduct)<=$spckb)?$spckb-strlen($row->iproduct):strlen($row->iproduct);
		//$arrnm[$lup]	= (strlen($row->motifname)-0)<=$spcnb?($spcnb-(strlen($row->motifname)))-0:(strlen($row->motifname)-0);
		$arrnm[$lup]	= strlen($row->motifname)<=$spcnb?$spcnb-(strlen($row->motifname)):strlen($row->motifname);
		$arrqty[$lup]	= (strlen($row->qty)<=$spcjml)?$spcjml-strlen($row->qty):strlen($row->qty);
		$arrket[$lup]	= (strlen($row->keterangan)<=$spcket)?$spcket-strlen($row->keterangan):strlen($row->keterangan);
		
		$ipp->setData(CHR(15));
		$ipp->printJob();
		
		$ipp->setData($no.str_repeat(" ",$arrno[$lup]).$row->iop.str_repeat(" ",$arrop[$lup]).$row->iproduct.str_repeat(" ",$arrproc[$lup]).trim($nmmotif).str_repeat(" ",$arrnm[$lup]).str_repeat(" ",$arrqty[$lup]).$row->qty.str_repeat(" ",2).$row->keterangan.str_repeat(" ",$arrket[$lup])."\n");
		$ipp->printJob();
		
		$ipp->setData(CHR(18));
		$ipp->printJob();
		
		$no+=1;
		$lup++;	
	//}
}

$ipp->setData(str_repeat(chr(196),$line)."\n");
$ipp->printJob();

$ipp->setData("\n");
$ipp->printJob();

// Function
// Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
// 10 cpi	= CHR(27).CHR(33).CHR(48)

$ipp->setData(CHR(27).CHR(69).str_repeat(" ",11)."Penerima".str_repeat(" ",15)."Yang Membawa".str_repeat(" ",12)."Factory Manager".CHR(27).CHR(70)."\n");
$ipp->printJob();
	
$ipp->setData(str_repeat("\n",2));
$ipp->printJob();
	
$ipp->setData(str_repeat(" ",7)."(".str_repeat(" ",13).")".str_repeat(" ",10)."(".str_repeat(" ",13).")".str_repeat(" ",11)."(".str_repeat(" ",13).")"."\n");
$ipp->printJob();

$tglprnt	= explode("-",$tglprint,strlen($tglprint));
$tg_print	= html_entity_decode($tglprnt[0]);
$bl_print	= html_entity_decode($tglprnt[1]);
$th_print	= html_entity_decode($tglprnt[2]);

// subscript-bold : CHR(27).CHR(83).CHR(49).CHR(27).CHR(69) - CHR(27).CHR(70).CHR(27).CHR(84)
$ipp->setData(CHR(15).$tg_print." ".$bl_print." ".$th_print." ".$waktu.CHR(18)."\n");
//$ipp->setData(CHR(2).CHR(27).CHR(107).CHR(50).$tg_print." ".$bl_print." ".$th_print." ".$waktu.CHR(3)."\n");
$ipp->printJob();

$ipp->setData(str_repeat(chr(196),$line)."\n");
$ipp->printJob();

***/

?>
