<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<?php
	$query3	= $this->db->query(" SELECT id, nama, alamat, kota FROM tm_perusahaan ");

	if ($query3->num_rows() > 0) {
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
	} else {
		$id	= '';
		$nama = '';
		$kota = '';
		$alamat = '';
	}
	?>
	<title><?php echo $nama ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="Tom@Lwis (http://www.lwis.net/free-css-drop-down-menu/)" />
	<meta name="keywords" content=" css, dropdowns, dropdown menu, drop-down, menu, navigation, nav, horizontal, vertical left-to-right, vertical right-to-left, horizontal linear, horizontal upwards, cross browser, internet explorer, ie, firefox, safari, opera, browser, lwis" />
	<meta name="description" content="Clean, standards-friendly, modular framework for dropdown menus" />
	<link href="<?php echo base_url(); ?>css/dropdown/themes/default/helper.css" media="screen" rel="stylesheet" type="text/css" />

	<!-- Beginning of compulsory code below -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>images/logo_duta.jpg" />
	<link href="<?php echo base_url(); ?>css/dropdown/dropdown.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>css/dropdown/themes/default/default.ultimate.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>css/style.css" media="screen" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.4.min.js"></script>

	<script>
		function cek_data() {
			var username = $('#username').val();
			var passwd = $('#passwd').val();

			if (username == '') {
				alert("Username harus diisi..!");
				$('#username').focus();
				return false;
			}
			if (passwd == '') {
				alert("Password harus diisi..!");
				$('#passwd').focus();
				return false;
			}
		}
	</script>

	<!--[if lt IE 7]>
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<![endif]-->

	<!-- / END -->

</head>

<body>

	<h1>Sistem Informasi <?php echo $nama ?> v4.0</h1>


	<!-- / END -->
	<br><br><br><br>

	<center>
		<h3>Form Login</h3>
	</center>
	<div align="center"><br>
		<?php if ($pesan != "") echo "<i>" . $pesan . "</i><br>"; ?>
		<?php echo form_open('users-login/cform/proseslogin'); ?>

		<table>
			<tr>
				<td>Username</td>
				<td> : <input type="text" name="username" id="username" value="" maxlength="20" size="20">
				</td>
			</tr>
			<tr>
				<td>Password</td>
				<td> : <input type="password" name="passwd" id="passwd" value="" maxlength="20" size="20"></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="submit" value="Login" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset">
				</td>
			</tr>
		</table>

		<?php echo form_close(); ?>

	</div>

	<?php echo $this->load->view('footer'); ?>

</body>

</html>