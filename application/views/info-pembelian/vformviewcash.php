<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
    
    .fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}

</style>

<h3>Laporan Pembelian</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Supplier: <?php if ($supplier != '0') { echo $supplier." - ".$nama_supplier; } else { echo "All"; } ?><br>
Gudang: <?php if ($nama_gudang != '0') { echo $nama_gudang." - ".$kode_gudang; } else { echo "All"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br> 
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-pembelian/cform/export_excel_cash', $attributes); ?>
<input type="hidden" name="gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="hidden" name="kode_supplier" value="<?php echo $supplier ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Supplier</th>
		 <th>No Faktur</th>
		 <th>Tgl Faktur</th>
		 <th>No F Pajak</th>
		 <th>No / Tgl SJ</th>
		 <th>Kode & Nama Barang</th>
		 <th>Satuan</th>
		 <th>Harga</th>
		 <th>Qty</th>
		 <th>Subtotal</th>
		 <th>Total</th>
		 <th>No/Tgl Voucher <br>Pelunasan</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_jumlah = 0; 
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_jumlah += $query[$j]['jumlah'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_jumlah = 0;
			}
		 
			if (is_array($query)) {
				$no=1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$no."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td>".$query[$j]['no_faktur']."</td>";
				 echo    "<td>".$query[$j]['tgl_faktur']."</td>";
				 echo    "<td>".$query[$j]['no_faktur_pajak']."</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga'], 2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['total'],2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['jumlah'],2,',','.')."</td>";
				 echo    "<td>".$query[$j]['no_voucher']." / ".$query[$j]['tgl_voucher']."</td>";
				 echo  "</tr>";

				$no++;
		 	}
		   }
		 ?>
		 <tr>
			<td colspan="9" align="right"><b>TOTAL</b></td>
			<td align="right"><b><?php echo number_format($tot_jumlah_detail,2,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_jumlah,2,',','.')  ?></b></td>
			<td>&nbsp;</td>
		 </tr>
 	</tbody>
</table><br>
<?php //echo $this->pagination->create_links();?>
</div>
