<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
    
    .fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}

</style>

<h3>Laporan Pembelian</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Kategori Pembelian: <?php if ($kategori == 0) echo "All"; else if ($kategori == 1) echo "Pembelian Bahan Baku/Pembantu"; else echo "Pembelian Makloon"; ?><br>
Supplier: <?php if ($supplier != '0') { echo $supplier." - ".$nama_supplier; } else { echo "All"; } ?><br>
Gudang: <?php if ($nama_gudang != '0') { echo $nama_gudang." - ".$kode_gudang; } else { echo "All"; } ?><br>
Kelompok: <?php if ($kelompok != '0') { echo $kode_perkiraan." - ".$nama_kelompok; } else { echo "All"; } ?><br>
Jenis: <?php if ($jenis != '0') { echo $kode_jenis." - ".$nama_jenis; } else { echo "All"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br> 
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-pembelian/cform/export_excel', $attributes); ?>
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="hidden" name="gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kategori" value="<?php echo $kategori ?>" >
<input type="hidden" name="id_supplier" value="<?php echo $supplier ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" name="kelompok" value="<?php echo $kelompok ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel ">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS ">
<?php echo form_close(); ?>
<br>
<?php
echo form_open('info-pembelian/cform/export_excel_khusus', $attributes); ?>
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="hidden" name="gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kategori" value="<?php echo $kategori ?>" >
<input type="hidden" name="id_supplier" value="<?php echo $supplier ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" name="kelompok" value="<?php echo $kelompok ?>" >
<!-- <input type="submit" name="export_excel_khusus" id="export_excel_khusus" value="Export ke Excel Khusus ">
<input type="submit" name="export_ods_khusus" id="export_ods_khusus" value="Export ke ODS Khusus"> -->
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<!--<th>Tgl Faktur</th>
		 <th>Nomor Faktur</th> -->
		 <th>Kode Supplier</th>
		 <th>Nama Supplier</th>
		 <th>No SJ</th>
		 <th>Tgl SJ</th>
		 <th>No Faktur</th>
		 <th>Tgl Faktur</th>
		 <th>No F Pajak</th>
		 <th>List Barang</th>
		 <th>No Perk</th>
		 <th>Harga Satuan</th>
		 <th>Qty</th>
		 <th>Satuan</th>
		<?php if ($kategori == 0 || $kategori == 2) { ?>
			 <th>Jenis Potong & Ukuran</th>
		<?php } ?>
		 <th>Jumlah</th>
		 <th>Total<br>Hutang Dagang</th>
		 <th>DPP</th>
		 <?php if ($kategori == 1) { ?>
		 <th>PPN</th>
		 <?php } else { ?>
			 <th>Total<br>Pajak/PPN</th>
		 <?php } ?>
		 <th>Bahan<br>Baku</th>
		 <th>Bahan<br>Pembantu</th>
		 <?php if ($kategori == 0 || $kategori == 2) { ?>
		 <th>Makloon</th>
		 <?php } ?>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_dpp = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				$tot_makloon = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_hutang += $query[$j]['jumlah'];
					$tot_dpp += $query[$j]['dpp'];
					$tot_ppn += $query[$j]['pajaknya'];
					
					if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 
						for($k=0;$k<count($var_detail); $k++){
							 $tot_jumlah_detail += $var_detail[$k]['total'];
							 //if ($var_detail[$k]['kode_perk'] == "511.100")
							 if ($var_detail[$k]['kode_perk'] == "510-10100")
								$tot_baku += $var_detail[$k]['total'];
							 //else if ($var_detail[$k]['kode_perk'] == "512.100")
							 else if ($var_detail[$k]['kode_perk'] == "510-10200")
								$tot_pembantu += $var_detail[$k]['total'];
							 else
								$tot_makloon += $var_detail[$k]['total'];
						}
					 } // end detail
				} // end header
			}
			else {
				$tot_jumlah_detail = 0;
				$tot_hutang = 0;
				$tot_dpp = 0;
				$tot_ppn = 0;
				$tot_baku = 0;
				$tot_pembantu = 0;
				$tot_makloon = 0;
			}
		 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['kode_supplier']."</td>";
				 echo    "<td>".$query[$j]['nama_supplier']."</td>";
				 echo    "<td>".$query[$j]['no_sj']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['tgl_sj']."</td>";
				 echo    "<td>".$query[$j]['no_faktur']."</td>";
				 echo    "<td>".$query[$j]['tgl_faktur']."</td>";
				 echo    "<td>".$query[$j]['no_faktur_pajak']."</td>";
				 /* echo    "<td style='white-space:nowrap;'>".$query[$j]['tgl_sj']."</td>"; */
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						 // echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_perk'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga'], 4,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['kode_perk'] != "523.100")
						  echo $var_detail[$k]['satuan'];
						else
						  echo "Yard";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 if ($kategori == 0 || $kategori == 2) {
					 echo "<td style='white-space:nowrap;'>";
					 if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							echo $var_detail[$k]['nama_jenis_potong']." ".$var_detail[$k]['nama_ukuran_bisbisan'];
							if ($k<$hitung-1)
								echo "<br> ";
						}
					 }
					 echo "</td>";
				 }
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['total'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['jumlah'],4,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['dpp'],4,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['pajaknya'],4,',','.')."</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "510-10100") 
							echo number_format($var_detail[$k]['total'],4,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "510-10200") 
							echo number_format($var_detail[$k]['total'],4,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 if ($kategori == 0 || $kategori == 2) {
					 echo "<td align='right'>";
					 if (is_array($query[$j]['detail_beli'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_beli'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							 if ($var_detail[$k]['kode_perk'] == "523.100") 
								echo number_format($var_detail[$k]['total'],4,',','.');
							 if ($k<$hitung-1)
								echo "<br> ";
						}
					 }
					 echo "</td>";
				}
				 
				echo  "</tr>";

					
		 	}
		   }
		 ?>
		 <tr>
			 <?php
				 if ($kategori == 0 || $kategori == 2) {
			echo "<td colspan='12' align='center'>TOTAL</td>";
		 }
		 else
			echo "<td colspan='13' align='center'>TOTAL</td>";
			?>
			<td align="right"><b><?php echo number_format($tot_jumlah_detail,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_hutang,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_dpp,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_ppn,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_baku,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_pembantu,4,',','.')  ?></b></td>
			<?php if ($kategori == 0 || $kategori == 2) { ?>
			<td align="right"><b><?php echo number_format($tot_makloon,4,',','.')  ?></b></td>
			<?php } ?>
		 </tr>
 	</tbody>
</table><br>
<?php //echo $this->pagination->create_links();?>
</div>
