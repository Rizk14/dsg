<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
   
   .judulnya {
		background-color:#DDD;
	}

</style>

<h3>Laporan Faktur Pembelian Makloon Hasil Jahit (WIP)</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Unit Jahit: <?php if ($kode_unit != '0') { echo $kode_unit." - ".$nama_unit; } else { echo "All"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br> 
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-pembelian/cform/export_excel_lapfakturwip', $attributes); ?>
<input type="hidden" name="kode_unit" value="<?php echo $kode_unit ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis_masuk" value="<?php echo $jenis_masuk ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th rowspan="2">No</th>
		 <th rowspan="2">Unit Jahit</th>
		 <th rowspan="2">No Faktur</th>
		 <th rowspan="2">Tgl Faktur</th>
		 <th rowspan="2">Tgl SJ</th>
		 <th colspan="6">List Brg Jadi</th>		 
		 <th rowspan="2">Grand Total (Setelah dikurangi pajak)</th>
	 </tr>
	 <tr class="judulnya">
		<th>Kode & Nama Brg Jadi</th>
		<th>Satuan</th>
		<th>Quantity</th>
		<th>Harga (Rp.)</th>
		<th>Diskon</th>
		<th>Subtotal</th>
		
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				$tot_grandtotal = 0;
				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					//$tot_grandtotal += $query[$j]['grandtotal'];
				} // end header
				
			}
			else {
				$tot_grandtotal = 0;
			}
		 
			if (is_array($query)) {
				$no = 1;
			 for($j=0;$j<count($query);$j++){
				 				 
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$no."</td>";
				 echo    "<td>".$query[$j]['nama_unit']."</td>";
				 
				 
				// echo    "<td>".$query[$j]['no_faktur']."</td>";
				echo    "<td></td>";
				// echo    "<td>".$query[$j]['tgl_faktur']."</td>";
				echo    "<td></td>";
				echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='center'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						//  echo number_format($var_detail[$k]['harga'], 2,',','.');
						echo  "" ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 // echo number_format($var_detail[$k]['diskon'], 2,',','.');
						 echo ""   ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						//  echo number_format($var_detail[$k]['subtotal'],2,',','.');
						 echo ""   ;
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";

				

			//	 echo    "<td align='right'>".number_format($query[$j]['grandtotal'],2,',','.')."</td>";
				 echo    "<td align='right'>";
				 echo "</td>";
				/* echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "511.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 if ($var_detail[$k]['kode_perk'] == "512.100") 
							echo number_format($var_detail[$k]['total'],2,',','.');
						 if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>"; */
				 
				 echo  "</tr>";

				$no++;
		 	} // end for
		   }
		 ?>
		 <tr>
			<td colspan="11" align="right"><b>TOTAL</b></td>
			<td align="right"><b><?php echo number_format($tot_grandtotal,2,',','.') ?></b></td>
		 </tr>
 	</tbody>
</table><br>
</div>
