<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}

</style>

<h3>SJ Yang Tidak Sinkron (Total Di Header Dgn Di Detail)</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		 <th>Supplier</th>
		 <th>No / Tgl SJ</th>
		 <th>List Barang</th>
		 <th>Harga Satuan</th>
		 <th>Qty</th>
		 <th>Satuan</th>
		 <th>Jumlah</th>
		 <th>Total Header</th>
		 <th>Status Faktur</th>
		 <th>Status Lunas</th>
		 <th>Tgl Input</th>
		 <th>Tgl Update</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 				 
				 echo "<tr class=\"record\">";
				echo    "<td>".$query[$j]['nama_supplier']."</td>";
				 
				 echo "<td nowrap>";
				 echo $query[$j]['no_sj']." / ".$query[$j]['tgl_sj'];
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 echo $query[$j]['nama_brg'];
				 echo "</td>";
				 				 
				 echo "<td align='right' nowrap>";
				 echo number_format($query[$j]['harga'], 2,',','.');
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 echo $query[$j]['qty'];
				 echo "</td>";
				 
				 echo "<td>";
				 echo $query[$j]['satuan'];
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 echo number_format($query[$j]['total'],2,',','.');
				 echo "</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['total_header'],2,',','.')."</td>";
				  echo    "<td align='center'>";
				  if ($query[$j]['status_faktur'] == 't')
					echo "Sudah";
				  else
					echo "Belum";
				  echo "</td>";
				  
				  echo    "<td align='center'>";
				  if ($query[$j]['status_lunas'] == 't')
					echo "Sudah";
				  else
					echo "Belum";
				  echo "</td>";
				  
				  echo "<td nowrap>";
				 echo $query[$j]['tgl_input'];
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 echo $query[$j]['tgl_update'];
				 echo "</td>";
				 
				 echo  "</tr>";

		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
