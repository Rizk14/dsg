<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Gudang</h3><br>
<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php echo form_open('mst-gudang/cform/submit'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> 
	<input type="hidden" name="kodeeditlok" value="<?php echo $eid_lokasi ?>">
	<input type="hidden" name="kodeeditgud" value="<?php echo $ekode_gudang ?>">
	<input type="hidden" name="id_gudang" value="<?php echo $eid ?>">
	<?php } ?>
<input type="hidden" id="kode_lokasi" name="kode_lokasi" value="<?php echo $ekode_lokasi ?>">
	<table width="40%">
		<tr>
			<td>Lokasi</td>
			<td>: <select name="id_lokasi" id="id_lokasi" onClick="document.getElementById('kode_lokasi').value = document.getElementById('id_lokasi').value);">
				<?php foreach ($lokasi as $lok) { ?>
					<option value="<?php echo $lok->id ?>" <?php if ($lok->id == $eid_lokasi) { ?> selected="true" <?php } ?> ><?php echo $lok->kode_lokasi." - ". $lok->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kode Gudang</td>
			<td>: <input type="text" name="kode_gudang" value="<?php echo $ekode_gudang ?>" maxlength="3" size="3" <?php if ($edit == '1') { ?> readonly="true" <?php } ?> ></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>: <input type="text" name="nama" value="<?php echo $enama ?>" maxlength="60"></td>
		</tr>
		
		<tr>
			<td>Jenis Penyimpanan</td>
			<td>: <select name="jenis" id="jenis">
				<?php foreach($list_jenis as $lj) { ?>
					<option value="<?php echo $lj->id ; ?>" <?php if($ejenis == $lj->id){?> selected <?php } ?> ><?php echo $lj->kode_jenis." - ".$lj->nama_jenis ;?> </option>
				<?php	} ?>
					 
			</td>
		</tr>
		
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-gudang/cform/'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

<div>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th>Kode & Nm Lokasi</th>
		 <th>Kode Gudang</th>
		 <th>Nama</th>
		 <th>Jenis<br>Penyimpanan</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				if ($row->jenis == '1')
					$nama_jenis = "Bahan Baku/Pembantu";
				else if ($row->jenis == '2')
					$nama_jenis = "Barang WIP";
				else
					$nama_jenis = "Barang Jadi";
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode_lokasi - $row->nama_lokasi</td>";
				 echo    "<td>$row->kode_gudang</td>";
				 echo    "<td>$row->nama</td>";
				 echo    "<td>$nama_jenis</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 $ada = 0;
				 // cek apakah data kode sudah dipake di tabel tm_barang				 
				 $query3	= $this->db->query(" SELECT kode_brg FROM tm_barang WHERE id_gudang = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 echo    "<td align=center><a href=".base_url()."index.php/mst-gudang/cform/index/$row->id \" id=\"$row->id\">Edit</a>";
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/mst-gudang/cform/delete/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table>
</div>
