<html>
<head>
<script language="javascript">
	// (C) 2001 www.CodeLifter.com
	// http://www.codelifter.com
	// Free for all users, but leave in this header

	var howLong = 10000;
	t = null;
	
	function closeMe(){
		t = setTimeout("self.close()",howLong);
	}
	
</script>

</head>

<body onLoad="closeMe();">

<?php

echo "Printer: ".$printer_name;

/*
Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
Italic	= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)
San Serif	= CHR(27).CHR(107).CHR(49)
Start Text	= CHR(2)
End Text	= CHR(3)
Set Bottom Margin	= CHR(27).CHR(78).CHR(2)
Cancel Set Bottom Margin	= CHR(27).CHR(79)
*/

include_once ("funcs/terbilang.php");

$get_attributes	= false;
$waktu	= date("H:i:s");
$line	= 80;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php");
} else {
	include_once("printipp_classes/BasicIPP.php");
}

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

for($jj=0;$jj<10;$jj++){	
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).CHR(27).CHR(69)." ".CHR(18).CHR(3)."\n");
	$ipp->printJob();
}

$no	= 1;
$j	= 0;

foreach($isi as $row) {

	$qpenyetor	= $this->mclass->penyetor($row->i_penyetor);
	$row_setor	= $qpenyetor->row();
	$qinisial	= $this->mclass->inisal($row->e_initial_npwp);
	$row_inisial= $qinisial->row();
	
	$npwp		= $row_inisial->e_initial_npwp;
	
	$npwp_gk_pake_ti2k	= str_replace('.','',$npwp);
	$npwp_bnyknya_char	= strlen($npwp_gk_pake_ti2k);
	
	$dimulai_dari = 0;
	$elemen_npwp = array();
	
	while($dimulai_dari<$npwp_bnyknya_char){
		$elemen_npwp[$dimulai_dari]	= substr($npwp_gk_pake_ti2k,$dimulai_dari,1);
		$dimulai_dari++;
	}
	
	$terbil	=&Terbilang($row->v_jml_bayar);
	
	$bglobal	= array(
			'01'=>'Januari',
			'02'=>'Februari',
			'03'=>'Maret',
			'04'=>'April',
			'05'=>'Mei',
			'06'=>'Juni',
			'07'=>'Juli',
			'08'=>'Agustus',
			'09'=>'September',
			'10'=>'Oktober',
			'11'=>'Nopember',
			'12'=>'Desember'
	);
	
	$tanggal= explode("-",$row->d_wp,strlen($row->d_wp));// Y-m-d
	$tgl	= substr($tanggal[2],0,1)=='0'?substr($tanggal[2],1,1):$tanggal[2];
	$nw_tgl	= $tgl;
	$bln	= $tanggal[1];
	$blnnya	= $bglobal[$bln];
		
	$falamat= $row->e_tempat_wp;
	
	/* 05/11/2011
	$ftgl	= $nw_tgl." ".$blnnya." ".$tanggal[0];
	*/
	$ftgl	= $tanggal[2]."/".$tanggal[1]."/".$tanggal[0];
	
	$bl	= (substr($row->d_bln,0,1)=='0')?(substr($row->d_bln,1,1)):($row->d_bln);
	
	$jml= 1;
	$arr= 0;
	$arr_bln = array();

	while($jml<=12){
		if($jml==$bl){
			$arr_bln[$arr]	= "X";
		}else{
			$arr_bln[$arr]	= " ";			
		}
		$jml+=1;
		$arr+=1;
	}
	
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).str_repeat(" ",35).CHR(27).CHR(69).$elemen_npwp[0]."   ".$elemen_npwp[1].str_repeat(" ",7).$elemen_npwp[2]."   ".$elemen_npwp[3]."   ".$elemen_npwp[4].str_repeat(" ",7).$elemen_npwp[5]."   ".$elemen_npwp[6]."   ".$elemen_npwp[7].str_repeat(" ",7).$elemen_npwp[8].str_repeat(" ",7).$elemen_npwp[9]."   ".$elemen_npwp[10]."   ".$elemen_npwp[11].str_repeat(" ",7).$elemen_npwp[12]."   ".$elemen_npwp[13]."   ".$elemen_npwp[14].CHR(18).CHR(3)."\n");
	$ipp->printJob();	
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(27).CHR(69)." ".CHR(27).CHR(70)."\n");
	$ipp->printJob();		
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",20).CHR(27).CHR(69).strtoupper($row_inisial->e_initial_name).CHR(27).CHR(70)."\n");
	$ipp->printJob();
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",20).CHR(27).CHR(69).$row_inisial->e_initial_address.CHR(27).CHR(70)."\n");
	$ipp->printJob();
 
	for($jj=0;$jj<8;$jj++) {	
		$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." "."\n".CHR(18).CHR(3));
		$ipp->printJob();
	}
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0)." "."\n".CHR(27).CHR(70));
	$ipp->printJob();
		
	//misal : PPH Pasal 21
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).str_repeat(" ",90)." ".$row->e_jsetor_pajak."\n".CHR(18).CHR(3));
	$ipp->printJob();
	
	$iakunpajak_bnyknya_char	= strlen($row->i_akun_pajak);
	$iakun_jml = 0;
	$elemen_iakunpajak = array();
	
	while($iakun_jml<$iakunpajak_bnyknya_char){
		$elemen_iakunpajak[$iakun_jml]	= substr($row->i_akun_pajak,$iakun_jml,1);
		$iakun_jml++;
	}
	
	$ijsetorpajak_bnyknya_char	= strlen($row->i_jsetor_pajak);
	$ijsetorpajak_jml = 0;
	$elemen_ijsetorpajak = array();
	
	while($ijsetorpajak_jml<$ijsetorpajak_bnyknya_char){
		$elemen_ijsetorpajak[$ijsetorpajak_jml]	= substr($row->i_jsetor_pajak,$ijsetorpajak_jml,1);
		$ijsetorpajak_jml++;
	}
	
	//misal : 411121 411
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).str_repeat(" ",10).$elemen_iakunpajak[0]."   ".$elemen_iakunpajak[1]."   ".$elemen_iakunpajak[2]."   ".$elemen_iakunpajak[3]."   ".$elemen_iakunpajak[4]."   ".$elemen_iakunpajak[5].str_repeat(" ",12).$elemen_ijsetorpajak[0]."   ".$elemen_ijsetorpajak[1]."   ".$elemen_ijsetorpajak[2].CHR(18).CHR(3));
	$ipp->printJob();
	
	for($jj=0;$jj<4;$jj++){ //5
		$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
		$ipp->printJob();
	}
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0)." "."\n".CHR(27).CHR(70));
	$ipp->printJob();
			
	/***
	CHR(27).CHR(99). n1. n2
	CHR(27).CHR(32). n
	CHR(27).CHR(40).CHR(85). n=10/20/30/40/50/60
	***/
	
	$thn_skrng = date("Y");
	$pjg_thn_skrng = strlen($thn_skrng);
	$elemen_thn = array();
	for($t=0;$t<$pjg_thn_skrng;$t++){
		$elemen_thn[$t] = substr($thn_skrng,$t,1);
	}
		
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).CHR(27).CHR(69).str_repeat(" ",9).$arr_bln[0].str_repeat(" ",6).$arr_bln[1].str_repeat(" ",7).$arr_bln[2].str_repeat(" ",7).$arr_bln[3].str_repeat(" ",7).$arr_bln[4].str_repeat(" ",7).$arr_bln[5].str_repeat(" ",7).$arr_bln[6].str_repeat(" ",7).$arr_bln[7].str_repeat(" ",7).$arr_bln[8].str_repeat(" ",7).$arr_bln[9].str_repeat(" ",7).$arr_bln[10].str_repeat(" ",7).$arr_bln[11].str_repeat(" ",14).$elemen_thn[0]."   ".$elemen_thn[1]."   ".$elemen_thn[2]."   ".$elemen_thn[3].CHR(18).CHR(3)."\n");
	$ipp->printJob();
	
	for($jj=0;$jj<5;$jj++){ //7
		$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
		$ipp->printJob();
	}

	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).CHR(27).CHR(69)." ".CHR(18).CHR(3)."\n");
	$ipp->printJob();
				
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",22)."Rp. ".CHR(27).CHR(69).number_format($row->v_jml_bayar).CHR(27).CHR(70)."\n");
	$ipp->printJob();
	
	$exp_terbilang	= explode(" ",$terbil,strlen($terbil));
	$jjumlah = 1;

	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",12).CHR(27).CHR(69)." ".CHR(27).CHR(70));
	$ipp->printJob();
	for($x=0; $x<count($exp_terbilang); $x++){
		$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(27).CHR(69).$exp_terbilang[$x]." ".CHR(27).CHR(70));
		$ipp->printJob();

		if($jjumlah==9){
			$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(27).CHR(69)."\n".CHR(27).CHR(70));
			$ipp->printJob();
			
			$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",12).CHR(27).CHR(69)." ".CHR(27).CHR(70));
			$ipp->printJob();			
		}
		$jjumlah+=1;
	}
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(27).CHR(69)." Rupiah.".CHR(27).CHR(70)."\n");
	$ipp->printJob();	

	if($jjumlah<9){
			$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(27).CHR(69)." "."\n".CHR(27).CHR(70));
			$ipp->printJob();		
	}
					
	$footalamat	= strlen($falamat);
	$foottgl	= strlen('Tanggal');
	$footshtgl	= strlen($ftgl);
	
	for($jj=0;$jj<2;$jj++){
		$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).CHR(15).CHR(18)."\n");
		$ipp->printJob();
	}
	
	$footsisa_grs	= $line-($footalamat+1+$foottgl+1+1+$footshtgl);
	
	/* 05112011
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15).CHR(27).CHR(69).str_repeat(" ",($footsisa_grs+39)).$falamat."                ".$ftgl.CHR(27).CHR(70).CHR(18).CHR(3)."\n");
	*/
	
	$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(27).CHR(69).str_repeat(" ",($footsisa_grs-6)).$falamat."          ".$ftgl.CHR(27).CHR(70).CHR(3)."\n");
	$ipp->printJob();
	
	$footnm	= $line-24;
	$footjbt= $line-25;
	
	for($b=0;$b<4;$b++){
		$ipp->setData(CHR(1).CHR(27).CHR(50).CHR(0).CHR(2).CHR(15)." ".CHR(18).CHR(3)."\n");
		$ipp->printJob();
	}
	
	$ipp->setData(CHR(1).CHR(27).CHR(51).CHR(54).CHR(0).str_repeat(" ",($footnm-2))." ".CHR(27).CHR(69).$row_setor->e_penyetor.CHR(27).CHR(70));
	$ipp->printJob();
	
	$j+=1;
	$no+=1;
}
?>

</body>
</html>
