<style type='text/css'>

table{
	font-family: elvetica, Geneva, Arial, SunSans-Regular, sans-serif; 
	font_size:font-size: 12px;
	}
.judulnya{
	background-color: #DDD;
}
</style>

<h3> Laporan Harga Unit Jahit + Packing Berdasarkan BTB Jahit Packing </h3>

<div>
	
Total Data : <?php echo $jumlah ?><br>

Unit Jahit : <?php if ($id_unit_jahit != '0')
 {
	 echo $kode_unit_jahit ." - ". $nama_unit_jahit ;
	 }
 else { echo "ALL";
}
 ?>
 <br>
 Unit packing : <?php if ($id_unit_packing != '0')
 {
	 echo $kode_unit_packing ." - ". $nama_unit_packing ;
	 }
 else { echo "ALL";
}
 ?>
 <br>
Periode : <?php echo $date_from ?> s/d <?php echo $date_to ?>

<table border="1" cellpadding="1" cellspacing="2">
<thead>
<tr class="judulnya">
	<th>No</th>

	<th>No SJ Pembelian</th>
	<th>Tanggal SJ</th>
	<th>Unit Jahit</th>
	<th>Unit Packing</th>
	<th>Kode Barang</th>
	<th>Nama Barang</th>
	<th>Qty</th>
	<th>Harga Jahit</th>
	<th>Harga Packing</th>
	<th>Diskon</th>
	<th>Sub total</th>
	<th>Jumlah Total</th>
</tr>
</thead>
<tbody>
<?php

if(is_array($query)){
	$total_grandtotal= 0;
	for($i=0;$i<count($query);$i++ ){
		}
	}
	else{
		$total_grandtotal=0;
		}
		
if(is_array($query)){
	//print_r($query);
	$no=1;
	for($j=0;$j<count($query);$j++){
		echo "<tr class=\"record\">";
		echo "<td align='center'>".$no."</td>";
	
		echo "<td align='center'>".$query[$j]['no_sjmasukpembelianpackjht']."</td>";
		echo "<td align='center'>".$query[$j]['tgl_sj']."</td>";
		echo "<td align='center'>".$query[$j]['nama_unit_jahit']."</td>";
		echo "<td align='center'>".$query[$j]['nama_unit_packing']."</td>";

		echo "<td>";
		if (is_array($query[$j]['detail_beli'])){
			$var_detail=array();
			//print_r($query);
			$var_detail=$query[$j]['detail_beli'];
			$hitung=count($var_detail);
			for($k=0;$k<$hitung;$k++){
				echo $var_detail[$k]['kode_brg_wip'] ;
				if($k<$hitung)
				echo "<br>";
				}
			}
			
		echo "</td>";
		
		echo "<td size='200%'>";
		if (is_array($query[$j]['detail_beli'])){
			$var_detail=array();
			//print_r($query);
			$var_detail=$query[$j]['detail_beli'];
			$hitung=count($var_detail);
			for($k=0;$k<$hitung;$k++){
				echo $var_detail[$k]['nama_brg_wip'];
				if($k<$hitung)
				echo "<br>";
				}
			}
			
		echo "</td>";
		
		echo "<td>";
		if (is_array($query[$j]['detail_beli'])){
			$var_detail=array();
				//print_r($query);
			$var_detail=$query[$j]['detail_beli'];
			$hitung=count($var_detail);
			for($k=0;$k<$hitung;$k++){
				//echo $var_detail[$k]['qty'];
					echo number_format($var_detail[$k]['qty']),'','';
				if($k<$hitung)
				echo "<br>";
				}
			}
		echo "</td>";

		echo "<td>";
		if(is_array($query[$j]['detail_beli'])){
			$var_detail=array();
			$var_detail=$query[$j]['detail_beli'];
			$hitung=count($var_detail);
			for($k=0;$k<$hitung;$k++){
				echo $var_detail[$k]['harga_j'];
				if ($k<$hitung)
				echo"<br>";
			}
		}
		echo "</td>";
		
		echo "<td>";
		if(is_array($query[$j]['detail_beli'])){
			$var_detail=array();
			$var_detail=$query[$j]['detail_beli'];
			$hitung=count($var_detail);
			for($k=0;$k<$hitung;$k++){
				echo $var_detail[$k]['harga_p'];
				if ($k<$hitung)
				echo"<br>";
			}
		}
		echo "</td>";
	
		echo "<td>";
		if(is_array($query[$j]['detail_beli'])){
			$var_detail=array();
			$var_detail=$query[$j]['detail_beli'];
			$hitung=count($var_detail);
			for($k=0;$k<$hitung;$k++){
				echo $var_detail[$k]['diskon'];
				if ($k<$hitung)
				echo"<br>";
			}
		}
		echo "</td>";
		
			echo "<td>";
		if(is_array($query[$j]['detail_beli'])){
			$var_detail=array();
			$var_detail=$query[$j]['detail_beli'];
			$hitung=count($var_detail);
			for($k=0;$k<$hitung;$k++){
				echo $var_detail[$k]['subtotal'];
				if ($k<$hitung)
				echo"<br>";
			}
		}
		echo "</td>";
		if(is_array($query[$j]['detail_beli'])){
		echo "<td >".number_format($query[$j]['total'],2,',','.')."</td>";
	}
		else
		echo "<td></td>";
	
		$total_grandtotal+= $query[$j]['total'];
		echo "</tr>";
		$no++;
		}
		echo "<tr>";
		echo "<td align='center' colspan='12'>Grand Total</td>";
		
		echo "<td align='center'>".number_format($total_grandtotal,2,',','.')."</td>";
		echo "</tr>";	
			
	}
	
?>

</tbody>
</table>
<br>
<?php
$attributes= array('name'=>'f_pembelian','id'=>'f_pembelian');
	echo form_open('info-btb-jhtpck/cform/export',$attributes);
	?>
<input type='hidden' name='id_unit_jahit' value='<?php echo $id_unit_jahit ?>' >
<input type='hidden' name='id_unit_packing' value='<?php echo $id_unit_packing ?>' >
<input type='hidden' name='date_from' value='<?php echo $date_from ?>' >
<input type='hidden' name='date_to' value='<?php echo $date_to ?>' >
<input type='submit' name='export_excel' value='Export ke Excel Khusus Windows' >
<input type='submit' name='export_ods' value='EXport ODS khusus Linux' >
<?php
echo form_close();
?>
</div>
