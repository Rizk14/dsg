<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div id="tabs">
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">FORM LAYOUT BRG</div>
</div>
         
<div class="tab_bdr"></div>

	<div class="panel" id="panel2" style="display:block;">
	    <table class="maintable">
	      <tr>
	    	<td align="left">
				<?php
			$attributes = array('id'=>'f_master','class'=>'myform');
			echo form_open ('layoutbrg/cform/actedit',$attributes);
		    ?>
		     
		<div id="masterlayoutform">
		<div class="effect">
		  <div class="accordion2">
		      <table class="mastertable">
		      	<tr>
			  <td width="150px">Kode Layout</td>
			  <td width="1px">:</td>
			  <td>
			    <?php
			    $kdlayout = array(
					'name'=>'ilayout',
					'id'=>'ilayout',
					'value'=>$i_layout,
					'maxlength'=>'5',
					'readonly'=>true
				);
			    echo form_input($kdlayout);
			    ?>
			  </td>
			</tr>
		      	<tr>
			  <td>Nama Layout</td>
			  <td>:</td>
			  <td>
			    <?php
			     $nmlayout = array(
				'name'=>'elayoutname',
				'id'=>'elayoutname',
				'value'=>$e_layout_name,
				'maxlength'=>'50'
				);
			     echo form_input($nmlayout);	
			    ?>
			  </td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			    <input name="btnsimpan" id="btnsimpan" value="Simpan" type="submit">
			      <input name="tblreset" id="tblreset" value="Batal" type="button" onclick="window.location='<?php echo base_url() ?>index.php/layoutbrg/cform/'">

			  </td>
		       </tr>
		      </table>
			</div>
		</div>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
