<style>
.garisbawah { 
		border-bottom:#000000 0.1px solid;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><h3>Laporan Masuk Barang WIP Link Gudang Jadi</h3></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
  
     <td class="alt2" style="padding:0px;">
		<?php 
		$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
		echo form_open('listbonmmasukperprodukbaru/cform/export_excel_wip', $attributes); ?>
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'opvsdo/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlopvsdoform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								Total
							</td>
							<td>
								:
							</td>
							<td>
								<?php
								$no	= 0;
								$cc	= 0;
									for($j=0;$j<count($query);$j++){
										
										

										$no+=1; $cc+=1;


									}
									echo "$no";
								?>
							</td>
						</tr>
						<tr>
							<td>
								Lokasi Gudang
							</td>
							<td>
								: 
							</td>
							<td>
								[Duta] A8-Gudang Jadi
							</td>
						</tr>
						  <tr>
							<td width="19%">Periode</td>
							<td> : </td>
							<td> <?php echo $tglmulai; ?> 
							 s.d.
							  <?php echo $tglakhir; ?> 
							</td>
						  </tr>						  
						</table>
				</td>
			  </tr>
			  <tr>
			  	<td>
			  		&nbsp;
			  	</td>
			  </tr>
			  <tr>
			  	<td>
			  		<input type="hidden" name="date_from" value="<?php echo $tglmulai; ?>">
			  		<input type="hidden" name="date_to" value="<?php echo $tglakhir; ?>">
			  		<input type="hidden" name="nama_gudang" value="[Duta] A8-Gudang Jadi">
			  		<input type="submit" value="Export ke Excel (Khusus Window)" name="export_excel"> &nbsp; <input type="submit" name="export_ods" value="Export ke ODS (Khusus Linux)">
			  	</td>
			  </tr>
			 <tr>
			 	<td>
			 		&nbsp;
			 	</td>
			 </tr>
			  <tr>
				<td><table width="90%" border="1" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="2%" class="tdatahead" bgcolor="#E4E4E4">NO</td>
					<td width="10%" align="center" class="tdatahead" bgcolor="#E4E4E4">KODE BARANG</td>
					<td width="40%" align="center" class="tdatahead" bgcolor="#E4E4E4" style="white-space: nowrap">NAMA BARANG </td>
					<td width="10%" align="center" class="tdatahead" bgcolor="#E4E4E4">QTY</td>
					<td width="20%" align="center" class="tdatahead" bgcolor="#E4E4E4">PER WARNA </td>
				  </tr>
				  
				  <?php
				$no	= 1;
				$cc	= 1;
				$lbrg = "";
				if (is_array($query)) {
				  for($j=0;$j<count($query);$j++){
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
							$lbrg	.= "
								  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
									onMouseOut=\"this.className='$Classnya'\">
									<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
									<td>".$query[$j]['imotif']."</td>
									<td>".$query[$j]['productmotif']."</td>
									<td align=\"right\">".$query[$j]['qty']."</td>
									<td align=\"right\">".$query[$j]['listwarna']."</td>";
							$lbrg.="</tr>";
							
							$no+=1; $cc+=1;
				  }
				}									
					echo $lbrg;
				  
				  ?>

				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>	  

			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	<td align="right">
					  <input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" 
					  onclick="window.location='<?php echo base_url() ?>index.php/listbonmmasukperprodukbaru/cform'"> 

				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
