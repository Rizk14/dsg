<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 10px;}

</style>

<h3>Laporan Rekap Mutasi Stok Bahan Baku/Pembantu</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Kelompok Barang: <?php echo $nama_kel_brg ?><br><br>
Periode: <?php echo $nama_bulan." ".$tahun ?> <br><br>
*harga diluar pajak (untuk pkp)
<!-- <table border="1">
<caption>ROWSPAN Example</caption>

<tr>
<td rowspan="2">Nama Brg</td>
<td colspan="2">Saldo Awal</td>
<td colspan="2">Masuk</td>
<td colspan="2">Masuk Lain-lain</td>
</tr>

<tr>
<td>Qty</td>
<td>Jumlah</td>
<td>Qty</td>
<td>Jumlah</td>
</tr>
</table><br><br> -->

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		<th rowspan="2">No</th>
		 <th rowspan="2">Kode & Nama Brg</th>
		 <th rowspan="2">Satuan</th>
		 <th rowspan="2">Harga</th>
		 <th rowspan="2">Stok Di DB</th>
		 <th colspan="2">Saldo Awal</th>
		 <th colspan="2">Masuk</th>
		 <th colspan="2">Masuk Lain-lain</th>
		<!-- <th colspan="2">Total</th> -->
		 <th colspan="2">Keluar</th>
		 <th colspan="2">Keluar Lain-lain</th>
		<!-- <th colspan="2">Total</th> -->
		 <th colspan="2">Saldo Akhir</th>
		 <th colspan="2">SO</th>
		 <th colspan="2">Selisih</th>
	 </tr>
	 <tr>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
	</tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
				//if ($startnya == '')
					$i=1;
				//else
				//	$i = $startnya+1; 
			 
			 for($j=0;$j<count($query);$j++){
								 
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$i."</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>";
				 echo    "<td>".$query[$j]['satuan']."</td>";
				 echo    "<td>". number_format($query[$j]['harga'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['stok_db'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['saldo_awal'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_saldo_awal'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['jum_masuk'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_jum_masuk'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['jum_masuk_lain'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_jum_masuk_lain'],2,',','.')."</td>";
			//	 echo    "<td align='right'>".number_format($query[$j]['tot_masuk'],2,',','.')."</td>";
				 
				 echo    "<td align='right' nowrap>".number_format($query[$j]['jum_keluar'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_jum_keluar'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['jum_keluar_lain'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_jum_keluar_lain'],2,',','.')."</td>";
			//	 echo    "<td align='right'>".number_format($query[$j]['tot_keluar'],2,',','.')."</td>";
				 
				 echo    "<td align='right' nowrap>".number_format($query[$j]['saldo_akhir'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_saldo_akhir'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['jum_stok_opname'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_jum_stok_opname'],2,',','.')."</td>";

				 //$selisih = $query[$j]['jum_stok_opname']- $query[$j]['saldo_akhir'];
				 echo    "<td align='right' nowrap>".number_format($query[$j]['selisih'],2,',','.')."</td>";
				 echo    "<td align='right' nowrap>".number_format($query[$j]['harga_selisih'],2,',','.')."</td>";
				 echo  "</tr>";
				
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php // echo $this->pagination->create_links();?>


</div>
