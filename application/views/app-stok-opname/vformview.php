<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Approval Stok Opname Bahan Baku/Pembantu</h3><br><br>

<div>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
Tanggal Pencatatan SO: <?php echo $tgl_so ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('app-stok-opname/cform/submit', $attributes);
if(is_array($query))
	$no = count($query);
else $no = 0;
 ?>
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="gudang" id="gudang" value="<?php echo $gudang ?>">
<input type="hidden" name="id_header" id="id_header" value="<?php echo $query[0]['id_header'] ?>">
<input type="hidden" name="tgl_so" id="tgl_so" value="<?php echo $tgl_so ?>">

<input type="submit" name="submit" value="Update Stok Opname" onclick="return confirm('Yakin akan approve data SO ini ? Data stok terkini akan otomatis terupdate')">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/app-stok-opname/cform'">
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang</th>
		 <th>Satuan Awal</th>
		 <th>Satuan Konversi</th>
		 <th>Total Stok Akhir<br>Satuan Konversi</th>
		 <th>Jumlah Fisik Satuan Konversi</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg']."
					 <input type='hidden' name='id_brg_$i' id='id_brg_$i' value='".$query[$j]['id_brg']."'>
					 <input type='hidden' name='id_$i' id='id_$i' value='".$query[$j]['id']."'>
					 </td>";
					 echo    "<td>".$query[$j]['nama_brg']."</td>";
					 echo    "<td>".$query[$j]['nama_satuan']."</td>";
					 echo    "<td>".$query[$j]['nama_satuan_konv']."</td>";
					 
					 echo "<input type='hidden' name='auto_saldo_akhir_$i' id='auto_saldo_akhir_$i' value='".$query[$j]['auto_saldo_akhir']."'>";
					 
					 /*if ($query[$j]['id_satuan'] == 2) // yard
						echo "<td>Meter</td>";
					 else if ($query[$j]['id_satuan'] == 7) // lusin
						echo "<td>Pieces</td>";
					 else
						echo "<td>&nbsp;</td>"; */
					 
				/*	 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_harga'])) {
						$detailharga = $query[$j]['detail_harga'];
							for($zz=0;$zz<count($detailharga);$zz++){ */ ?>
					<!--		<span style="white-space:nowrap;"> -->
							<?php //echo "<b>".number_format($detailharga[$zz]['harga'],2,',','.')."</b>&nbsp;: ".$detailharga[$zz]['saldo_akhir']."<br>";
						//}
					//}
					// echo "</td>";
					 
				/*	 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_harga'])) {
						$detailharga = $query[$j]['detail_harga'];
						for($zz=0;$zz<count($detailharga);$zz++){ */ ?>
						<!--	<span style="white-space:nowrap;"> -->
							<?php //echo "<b>".number_format($detailharga[$zz]['harga'],2,',','.')."</b>&nbsp;: ".$detailharga[$zz]['stok_opname']."<br>";
						?>
				<!--		<input type="hidden" name="harga_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['harga'] ?>">
						<input type="hidden" name="stok_fisik_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['stok_opname'] ?>">
						<input type="hidden" name="jum_stok_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['saldo_akhir'] ?>">
						<input type="hidden" name="auto_saldo_akhir_harga_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['auto_saldo_akhir_harga'] ?>"> -->
						<?php
						//}
					//}
					 echo "</td>";
					 
					 echo    "<td align='right'>".$query[$j]['stok']."</td>";
					 echo    "<td align='right'>".$query[$j]['stok_opname'].
					 " <input type='hidden' name='stok_fisik_$i' id='stok_fisik_$i' value='".$query[$j]['stok_opname']."'>
					 <input type='hidden' name='jum_stok_$i' id='jum_stok_$i' value='".$query[$j]['stok']."'>
					 
					 </td>";
					 // <input type='hidden' name='satuan_$i' id='satuan_$i' value='".$query[$j]['id_satuan']."'>
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>

<?php echo form_close();  ?>
</div>
