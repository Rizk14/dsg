<h3>Approval Stok Opname Bahan Baku/Pembantu</h3><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('app-stok-opname/cform/view', $attributes); ?>
<table width="60%">
	<tr>
			<td width="20%">Lokasi Gudang</td>
			<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		
	<tr>
		<td width="20%">Periode (bulan-tahun)</td>
		<td> <!-- <select name="bulan" id="bulan">
				<option value="01" <?php if ($bulan_skrg == '01') { ?> selected="true" <?php } ?> >Januari</option>
				<option value="02" <?php if ($bulan_skrg == '02') { ?> selected="true" <?php } ?> >Februari</option>
				<option value="03" <?php if ($bulan_skrg == '03') { ?> selected="true" <?php } ?>>Maret</option>
				<option value="04" <?php if ($bulan_skrg == '04') { ?> selected="true" <?php } ?>>April</option>
				<option value="05" <?php if ($bulan_skrg == '05') { ?> selected="true" <?php } ?>>Mei</option>
				<option value="06" <?php if ($bulan_skrg == '06') { ?> selected="true" <?php } ?>>Juni</option>
				<option value="07" <?php if ($bulan_skrg == '07') { ?> selected="true" <?php } ?>>Juli</option>
				<option value="08" <?php if ($bulan_skrg == '08') { ?> selected="true" <?php } ?>>Agustus</option>
				<option value="09" <?php if ($bulan_skrg == '09') { ?> selected="true" <?php } ?>>September</option>
				<option value="10" <?php if ($bulan_skrg == '10') { ?> selected="true" <?php } ?>>Oktober</option>
				<option value="11" <?php if ($bulan_skrg == '11') { ?> selected="true" <?php } ?>>November</option>
				<option value="12" <?php if ($bulan_skrg == '12') { ?> selected="true" <?php } ?>>Desember</option> 
			</select> -->
			<select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>	
		
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/app-stok-opname/cform'">
<?php echo form_close();  ?>
