<style type="text/css">
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .halaman {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
	}
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 12px;}
    
    .subtotal {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;
	}

</style>

<style type="text/css" media="print">
    .printable{ 
      page-break-after: always;
    }
    .no_print{
      display: none;
    }
</style>
<!-- media="screen" means these styles will only be used by screen 
  devices (e.g. monitors) -->
<style type="text/css" media="screen">
    .printable{
      display: none;
    }
</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	//window.print();
});

</script>

<table border="0" width="100%" class="tabelheader" align="center">
	<tr>
		<td align="center"><b>DAFTAR KONTRA BON<br><?php echo $pelanggan ?><br>SP030 CV DUTA SETIA GARMEN<br>
		Dari Faktur No: <?php echo $faktur_awal; ?> s/d <?php echo $faktur_akhir; ?><br>
		Nomor Kontrabon: <?php echo $nokontrabon ?></b>
		</td>
	</tr>

</table>

<table border="0" width="100%" class="halaman" align="center">
	<tr>
		<td colspan="7" align="right"><?php $halaman = 1; echo "Hal. ".$halaman; ?></td>
	</tr>
</table>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isinya" align="center">
	<thead>
	 <tr>
		<th rowspan="2" width="3%">No</th>
		<th rowspan="2" width="10%">No Faktur</th>
		<th rowspan="2" width="10%">No Faktur Pajak</th>
		<th colspan="2" width="22%">Tanggal</th>
		<th rowspan="2" width="15%">Kota</th>
		<th rowspan="2" width="30%">Nomor DO</th>
		<!-- <th rowspan="2" width="30%">BEA METERAI</th> -->
		<th rowspan="2" width="20%">Jumlah</th>
	 </tr>
	 <tr>
		<th width="11%">Faktur</th>
		<th width="11%">Jth Tempo</th>
	 </tr>
	</thead>
	<tbody>
		<?php $i=1;
				$subtotal = 0;
				$grandtotal = 0;
				foreach($query as $row3) {
						$db2=$this->load->database('db_external', TRUE);
					$pisah1 = explode("-", $row3->d_faktur);
								$tgl1= $pisah1[2];
								$bln1= $pisah1[1];
								$thn1= $pisah1[0];
								
							/*	if ($bln1 == '01')
									$nama_bln = "Januari";
								else if ($bln1 == '02')
									$nama_bln = "Februari";
								else if ($bln1 == '03')
									$nama_bln = "Maret";
								else if ($bln1 == '04')
									$nama_bln = "April";
								else if ($bln1 == '05')
									$nama_bln = "Mei";
								else if ($bln1 == '06')
									$nama_bln = "Juni";
								else if ($bln1 == '07')
									$nama_bln = "Juli";
								else if ($bln1 == '08')
									$nama_bln = "Agustus";
								else if ($bln1 == '09')
									$nama_bln = "September";
								else if ($bln1 == '10')
									$nama_bln = "Oktober";
								else if ($bln1 == '11')
									$nama_bln = "November";
								else if ($bln1 == '12')
									$nama_bln = "Desember"; */
								//$d_faktur = $tgl1." ".$nama_bln." ".$thn1; 
								$d_faktur = $tgl1."/".$bln1."/".$thn1; 
					
					$pisah1 = explode("-", $row3->d_due_date);
								$tgl1= $pisah1[2];
								$bln1= $pisah1[1];
								$thn1= $pisah1[0];
								
								/*if ($bln1 == '01')
									$nama_bln = "Januari";
								else if ($bln1 == '02')
									$nama_bln = "Februari";
								else if ($bln1 == '03')
									$nama_bln = "Maret";
								else if ($bln1 == '04')
									$nama_bln = "April";
								else if ($bln1 == '05')
									$nama_bln = "Mei";
								else if ($bln1 == '06')
									$nama_bln = "Juni";
								else if ($bln1 == '07')
									$nama_bln = "Juli";
								else if ($bln1 == '08')
									$nama_bln = "Agustus";
								else if ($bln1 == '09')
									$nama_bln = "September";
								else if ($bln1 == '10')
									$nama_bln = "Oktober";
								else if ($bln1 == '11')
									$nama_bln = "November";
								else if ($bln1 == '12')
									$nama_bln = "Desember";
								$d_due_date = $tgl1." ".$nama_bln." ".$thn1; */
								$d_due_date = $tgl1."/".$bln1."/".$thn1;
					
							$query2 = $this->mclass->totalfaktur($row3->i_faktur);
								$row2	= $query2->row();								
								$nilai_ppn_perfaktur = (($row2->totalfaktur*11) / 100);
								
								if($row3->v_discount>0){
									$total_sblm_ppn_perfaktur	= round($row2->totalfaktur - $row3->v_discount); // DPP
									$nilai_ppn2_perfaktur	= round((($total_sblm_ppn_perfaktur*11) / 100));
									$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur+$row3->v_materai);
								}else{
									$total_sblm_ppn_perfaktur	= round($row2->totalfaktur); // DPP
									$nilai_ppn2_perfaktur	= round($nilai_ppn_perfaktur);
									$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur+$row3->v_materai);
								}
								
								$nomor_do = "";
								// ambil data2 DO
								$query3	= $db2->query(" SELECT distinct c.i_sj, a.i_sj_code FROM tm_sj_bhnbaku a, tm_faktur_bhnbaku b, 
														tm_faktur_bhnbaku_item c 
														WHERE a.i_sj = c.i_sj AND b.i_faktur = c.i_faktur 
														AND b.i_faktur = '$row3->i_faktur' ");
								//$hasilrow = $query3->row();
								if ($query3->num_rows() > 0) {
									/*$exp_ddt = explode("-",$hasilrow->d_dt,$hasilrow->d_dt);
									$data['ddt'] = $exp_ddt[2]."/".$exp_ddt[1]."/".$exp_ddt[0]; */
									$hasil3 = $query3->result();
									foreach ($hasil3 as $rowdo) {
										$nomor_do.= trim($rowdo->i_sj_code)."; ";
									}
								}
					
					// 21-12-2015
					$row3->i_faktur_code = trim($row3->i_faktur_code);
					//$query3	= $db2->query(" SELECT nomor_pajak FROM tm_nomor_pajak_faktur WHERE i_faktur_code = '$row3->i_faktur_code' ");
					$query3	= $db2->query(" SELECT nomor_pajak FROM tm_nomor_pajak_faktur WHERE i_faktur_code like  '%$row3->i_faktur_code%' ");
					
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nomor_pajak	= $hasilrow->nomor_pajak;
						}
						else {
							$nomor_pajak = '';
						}
		?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td><?php echo $row3->i_faktur_code; ?></td>
				<td><?php echo $nomor_pajak; ?></td>
				<td><?php echo $d_faktur; ?></td>
				<td><?php echo $d_due_date; ?></td>
				<td><?php if ($row3->i_code == 'PB') echo $row3->e_branch_city." (".$row3->i_code.")"; else echo $row3->e_branch_city; ?></td>
				<td><?php echo $nomor_do; ?></td>
				<!-- <td><?php// echo $row3->v_materai; ?></td> -->
				<td align="right"><?php echo number_format($total_grand_perfaktur,'2','.',','); ?></td>
			</tr>
		<?php 
			$subtotal+= $total_grand_perfaktur;
			$grandtotal+= $total_grand_perfaktur;
			if ($i%30==0 && $i!=$total_semua) {
				$halaman++;
				echo "<tr>
					<td colspan='6' align='right' class='subtotal'><b>Jumlah Sub Total</b></td>
					<td align='right' class='subtotal'><b>".number_format($subtotal,'2','.',',')."</b></td>
				</tr></tbody>
				</table>
				
				<br class='printable'>";
				
				//echo "<div align='right' class='halaman'>Hal. $halaman</div>";
				 ?>
				<table border="0" width="100%" class="halaman" align="center">
				<tr>
					<td colspan="7" align="right"><?php echo "Hal. ".$halaman; ?></td>
				</tr>
			</table>
				<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isinya" align="center">
				<thead>
				 	 <tr>
		<th rowspan="2" width="3%">No</th>
		<th rowspan="2" width="10%">No Faktur</th>
		<th colspan="2" width="22%">Tanggal</th>
		<th rowspan="2" width="15%">Kota</th>
		<th rowspan="2" width="30%">Nomor DO</th>
		<th rowspan="2" width="20%">Jumlah</th>
	 </tr>
	 <tr>
		<th width="11%">Faktur</th>
		<th width="11%">Jth Tempo</th>
	 </tr>
				 </thead><tbody>
				<?php
				$subtotal = 0;
			}
			$i++;
		} 
		?>
				<tr>
					<td colspan="7" align="right" class="subtotal"><b>Jumlah Sub Total</b></td>
					<td align="right" class="subtotal"><b><?php echo number_format($subtotal,'2','.',',') ?></b></td>
				</tr>
				<tr>
					<td colspan="7" align="right" class="subtotal"><b>Jumlah GRAND TOTAL</b></td>
					<td align="right" class="subtotal"><b><?php echo number_format($grandtotal,'2','.',',') ?></b></td>
				</tr>
				</tbody>
				</table><br>
				<table border="0" width="70%" class="halaman">
				<tbody>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penerima</td>
						<td>&nbsp;Yang Menyerahkan</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mengetahui</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>

						<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
						<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
					</tr>
					</tbody>
				</table>

	</tbody>
</table>
