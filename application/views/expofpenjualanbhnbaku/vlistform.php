<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_penjualanbhnbaku; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_penjualanbhnbaku; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'expofpenjualanbhnbaku/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlpenjualanndoform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_fpenjualanbhnbaku_no_faktur; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="no_faktur" type="text" id="no_faktur" maxlength="14" value="<?=$nofaktur?>"/>
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_penjualanbhnbaku_tgl_faktur_mulai; ?> </td>
					<td>:</td>
					<td>
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?=$tgldomulai?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?=$tgldoakhir?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>
				</table></td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_penjualanbhnbaku; ?></div></td>	
			</tr>
			<tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="8%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_no_faktur; ?> </td>
					<td width="13%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_no_pajak; ?> </td>
					<td width="25%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_nm_pelanggan; ?> </td>
					<td width="13%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_npwp; ?> </td>
					<td width="8%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_tgl_faktur; ?> </td>
					<td width="10%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_tgl_bts_kirim; ?></td>
					<td width="8%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_discount; ?></td>
					<td width="18%" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_total_faktur; ?></td>
					<td width="18" class="tdatahead"><?php echo $list_fpenjualanbhnbaku_grand_total; ?></td>
				  </tr>
				  <?php

				  $bruto	= array();
				  $dpp		= array();
				  $ppn		= array();
				  $total	= array();

				  $no	= 1;
				  $cc	= 1;
				  $jml	= 0;
				    				  				  				  
				  if(sizeof($query) > 0 ) {											  
					  
					  foreach($query as $row) {

					   $bruto[$jml]	= $row->totalfaktur;
					   $dpp[$jml]	= ($bruto[$jml]-($row->discountnilai));
						
					   // Pembulantan ke atas
					   $ppn[$jml]	= ceil(($dpp[$jml]*10)/100);
					   $total[$jml]	= ceil($dpp[$jml]+$ppn[$jml]);
					  						
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

						$row->dfaktur	= explode("-",$row->dfaktur,strlen($row->dfaktur)); // Y-m-d
						$tfaktur	= $row->dfaktur[2];
						$bfaktur	= $row->dfaktur[1];
						$thfaktur	= $row->dfaktur[0];
						$tglfaktur	= $tfaktur."/".$bfaktur."/".$thfaktur;
						
						$row->dduedate	= explode("-",$row->dduedate,strlen($row->dduedate)); // Y-m-d
						$tduedate	= $row->dduedate[2];
						$bduedate	= $row->dduedate[1];
						$thduedate	= $row->dduedate[0];
						$tglduedate	= $tduedate."/".$bduedate."/".$thduedate;

						$row->dpajak	= explode("-",$row->dpajak,strlen($row->dpajak)); // Y-m-d
						$thpajak	= $row->dpajak[0];
						$tahunpajak	= substr($thpajak,2,2);
						
					 	/* Nomor Pajak */
						$nopajak		= $this->mclass->pajak($row->ifakturcode);
						if($nopajak->num_rows() > 0) {
							$row_pajak	= $nopajak->row();			
							$nomorpajak	= $row_pajak->ifakturpajak;			
							
							switch(strlen($nomorpajak)) {
								case "1":
									$nfaktur	= '0000000'.$nomorpajak;
								break;
								case "2":
									$nfaktur	= '000000'.$nomorpajak;
								break;
								case "3":
									$nfaktur	= '00000'.$nomorpajak;
								break;
								case "4":
									$nfaktur	= '0000'.$nomorpajak;
								break;
								case "5":
									$nfaktur	= '000'.$nomorpajak;
								break;
								case "6":
									$nfaktur	= '00'.$nomorpajak;
								break;
								case "7":
									$nfaktur	= '0'.$nomorpajak;
								break;
								default:
									$nfaktur	= $nomorpajak;
							}
							$nomorpajak	= "010"."."."000".".".$tahunpajak.".".$nfaktur;			
						} else {
							$nomorpajak	= "";
						}				
						/* End 0f Nomer Pajak */
																  
						$lpenjndo .= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".trim($row->ifakturcode)."</td>
								<td>".trim($nomorpajak)."</td>
								<td>".trim($row->customername)."</td>
								<td>".trim($row->npwp)."</td>
								<td align=\"right\">".$tglfaktur."</td>
								<td align=\"right\">".$tglduedate."</td>
								<td align=\"right\">".$row->discountpersen."</td>
								<td align=\"right\">".number_format($row->totalfaktur,'2','.',',')."</td>
								<td align=\"right\">".number_format($total[$jml],'2','.',',')."</td>
							  </tr>";
							  $no+=1;	
							  $cc++;
							  $jml+=1;
					  }
					  echo $lpenjndo;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><? //echo $nofak; ?> <? //echo $tfirst; ?> <? //echo $tlast; ?></td>
			  </tr>
			  <tr>
				<td align="center"><?php echo $create_link; ?></td>
			  </tr>
			  <tr>
				<td align="right">
				  <input name="btnlaporan" type="button" id="btnlaporan" value="Lap. Faktur Penjualan Tertentu" onclick="window.location='<?php echo base_url(); ?>index.php/expofpenjualanbhnbaku/cform/gexportpenjualannondo/<?=$nofak?>/<?=$tfirst?>/<?=$tlast?>'" />
									<input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/expofpenjualanbhnbaku/cform/'">

				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close()?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
