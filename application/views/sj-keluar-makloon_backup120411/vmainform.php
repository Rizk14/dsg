<h3>Data SJ Keluar Untuk Proses Makloon</h3><br> 
<a href="<? echo base_url(); ?>index.php/sj-keluar-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-keluar-makloon/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	$('#pilih_bonm').click(function(){

			var urlnya = "<?php echo base_url(); ?>index.php/sj-keluar-makloon/cform/show_popup_bonm/";
			openCenteredWindow(urlnya);

	  });
	  
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_item_brg() {
	var id_brg= $('#id_brg').val();

	if (id_brg == '') {
		alert("item barang di Bon M harus dipilih..!");
		return false;
	}
}

function cek_input() {
	var tgl_sj= $('#tgl_sj').val();
	if (tgl_sj == '') {
		alert("Tanggal SJ harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_quilting', 'id' => 'f_quilting');
echo form_open('sj-keluar-makloon/cform/', $attributes); ?>
<table>

	<tr>
		<td>List Barang dari Bon M Keluar</td>
		<td> <input type="text" name="id_brg" id="id_brg" value="" size="40" maxlength="40" readonly="true">
		<input type="hidden" name="id_apply_stok_detail" value="">&nbsp;
		<input name="pilih_bonm" id="pilih_bonm" value="..." type="button" title="browse data Bon M Keluar">
		</td>
	</tr>

</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-keluar-makloon/cform/view'">
<?php echo form_close(); 
} else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sj-keluar-makloon/cform/submit" method="post" enctype="multipart/form-data">


<?php 
		if (count($apply_stok_detail)>0) {
			$no=1;
			foreach ($apply_stok_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
		<tr>
			<td width="15%">Unit Makloon</td>
			<td width="70%"><select name="unit_quilting" id="unit_quilting">
				<?php foreach ($list_quilting as $quilting) { ?>
					<option value="<?php echo $quilting->kode_unit ?>"><?php echo $quilting->kode_unit." - ". $quilting->nama ?></option>
				<?php } ?>
				</select></td> 
		</tr>
  <tr>
    <td>Nomor SJ Keluar</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" maxlength="20" readonly="true" value="<?php echo $no_sj; ?>">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tgl SJ</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
			<td>Keterangan</td>
			<td><input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="right">
			<!-- <input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang"> -->
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
	      <th>Qty</th>
        </tr>

        <?php $i=1;
        if (count($apply_stok_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($apply_stok_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $apply_stok_detail[$j]['kode_brg'] ?>"/>
           <input type="hidden" name="id_apply_stok_<?php echo $i ?>" value="<?php echo $apply_stok_detail[$j]['id_apply_stok'] ?>" >
           <input type="hidden" name="id_<?php echo $i ?>" value="<?php echo $apply_stok_detail[$j]['id'] ?>" >
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $apply_stok_detail[$j]['nama'] ?>" /></td>
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="10" readonly="true" value="<?php echo $apply_stok_detail[$j]['satuan'] ?>" /></td>
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $apply_stok_detail[$j]['qty'] ?>" /></td>

        </tr>
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sj-keluar-makloon/cform/'"></td>
		</tr>
	</table>	
	
	</form>
</td>
    </tr>

</table>
</div>
</form>
<?php } ?>
