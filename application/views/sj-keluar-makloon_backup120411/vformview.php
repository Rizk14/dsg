<h3>Data SJ Keluar Untuk Proses Makloon</h3><br> 
<a href="<? echo base_url(); ?>index.php/sj-keluar-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/sj-keluar-makloon/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('sj-keluar-makloon/cform/cari'); ?>
Unit Makloon: <select name="unit_quilting" id="unit_quilting">
				<option value="0" <?php if ($unit_quilting == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_quilting as $quilting) { ?>
					<option value="<?php echo $quilting->kode_unit ?>" <?php if ($unit_quilting == $quilting->kode_unit) { ?> 
					selected="true" <?php } ?> ><?php echo $quilting->kode_unit." - ". $quilting->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr>
		 <th>No SJ</th>
		 <th>Tgl SJ</th>
		 <th>Unit Makloon</th>
		 <th>Bon M Keluar/Gudang</th>
		 <th>List Barang</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_sj']."</td>";
				 echo    "<td>".$tgl_sj."</td>";
				 echo    "<td>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_bonm']." / [".$var_detail[$k]['nama_lokasi']."] ".$var_detail[$k]['kode_gudang']."-".$var_detail[$k]['nama_gudang'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (".$var_detail[$k]['qty']." ".$var_detail[$k]['satuan'].")";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";

				 echo    "<td>".$tgl_update."</td>";
				 
				 //if ($query[$j]['status_edit'] == 'f')
					echo    "<td align=center>
					<a href=".base_url()."index.php/sj-keluar-makloon/cform/edit/".$query[$j]['id']." \">Edit</a>
					<a href=".base_url()."index.php/sj-keluar-makloon/cform/delete/".$query[$j]['id']."/".$query[$j]['id_detailnya']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";

				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
