<?
  include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
*/
*{
size: landscape;
}
.pagebreak {
    page-break-before: always;
}
.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%; 
  border-style: solid;
  border-width:3px;
  border-collapse: collapse;
  cellspacing:3px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:3px;
	font-size: 18px;
  FONT-WEIGHT: normal; 
}
.judul {
  font-size: 30px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 26px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 18px;
  font-weight:normal;
}
.eusi {
  font-size: 18px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
      <table width="1160px" border="0">
        <tr>
          <td width="112"><img src="<? echo base_url().'img/Logo_DGU.png'; ?>" width="115" height="45" alt="" /></td>
          <td width="20" valign="top"><strong>PT. </strong></td>
          <td width="358" valign="top"><strong><span class="miring judul huruf">D</span><span class="nmper huruf">IALOGUE</span>&nbsp;<span class="miring judul huruf">G</span><span class="nmper huruf">ARMINDO</span>&nbsp;<span class="miring judul huruf">U</span><span class="nmper huruf">TAMA</span></strong>  </td>
          <td width="320"></td>
          <td width="250">&nbsp;</td>
        </tr>
        <tr>
          <td colspan=5 align=center><strong class="judul huruf">Kas Masuk(KB)</strong></td>
        </tr>

        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
<?
  if($isi){
		foreach($isi as $row){
    ?>
          <td class="huruf isi">No. <? echo $row->i_rv; ?> </td>
          <?
			      $tmp=explode("-",$row->d_rv);
			      $th =$tmp[0];
			      $bl =$tmp[1];
			      $hr =$tmp[2];
			      $bl=mbulan($bl);
			      break;
			      }
			    }
		      ?>
          <td align="right" class="huruf isi"><? echo $hr.' '.$bl.' '.$th; ?> </td>
        </tr>
      </table>
      <table width="960px" cellspacing="0.1px" class="garis huruf">
        <tr>
          <td align="center" class="huruf isi">KETERANGAN</td>
          <td align="center" class="huruf isi">Jumlah</td>
        </tr>
        <?
        
        $i=0;
          if($isi){
            $tot=0;
            foreach($isi as $row){
              $tot=$tot+$row->v_rv;
            }
		        foreach($isi as $row){
              $i++;
              $tmp=explode("-",$row->d_rv);
		          $th=$tmp[0];
		          $bl=$tmp[1];
		          $hr=$tmp[2];
		          $row->d_rv=$hr."-".$bl."-".$th;
		          if($iarea!='00'){
	              $coa=substr($row->i_coa,0,5).$iarea;
	            }else{
	              $coa=$row->i_coa;
	            }
              echo "<tr height=27>
                 <td>$row->e_remark</td>
                 <td align='right'>".number_format($row->v_rv)."</td></tr>";
            }
            for($x=$i+1;$x<10;$x++){
              echo "<tr height=27><td></td><td></td></tr>";
            }
            echo "<tr height=27>
               <td align=right>Total</td>
               <td align='right'>".number_format($tot)."</td></tr>";
		        $bilangan = new Terbilang;
		        $kata=ucwords($bilangan->eja($tot));	
            echo "<tr height=27><td colspan=2>Terbilang : ".$kata." Rupiah</td></tr>";
            echo "</table>";
            
            echo "<table width=\"100%\" border=0>
                  <tr><td colspan=4>&nbsp;</td></tr>
                  <tr>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">Dibuat Oleh</td>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">Diperiksa Oleh</td>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">Disetujui Oleh</td>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">Diterima Oleh</td>
                  </tr>
                  <tr><td colspan=4>&nbsp;</td></tr><tr><td colspan=4>&nbsp;</td></tr><tr><td colspan=4>&nbsp;</td></tr>
                  <tr>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
                    <td width=\"25%\" align=\"center\" class=\"huruf isi\">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
                  </tr>
                  ";
          }
          echo "</table>";
      ?>
      </table>
    <?
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
</BODY>
</html>
