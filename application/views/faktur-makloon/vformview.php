<h3>Data Faktur Jasa Makloon</h3><br>
<a href="<? echo base_url(); ?>index.php/faktur-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/faktur-makloon/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('faktur-makloon/cform/cari'); ?>
Unit Makloon <select name="supplier" id="supplier">
				<option value="0" <?php if ($csupplier == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" <?php if ($csupplier == $sup->kode_supplier) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr>
		<th>Jenis Makloon </th>
		 <th>No/Tgl Faktur </th>
		 <th>No/Tgl SJ</th>
		 <th>Unit Makloon</th>
		 <th>Jum Total (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['nama_makloon']."</td>";
				 echo    "<td nowrap>".$query[$j]['no_faktur']." / ".$query[$j]['tgl_faktur']."</td>";
				 
				 
				 echo "<td nowrap>";
				 
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['totalnya'], 2, ',','.')."</td>";
				 echo    "<td>".$query[$j]['tgl_update']."</td>";
				
				if ($query[$j]['status_faktur_pajak'] == 'f') {
					 echo    "<td align=center><a href=".base_url()."index.php/faktur-makloon/cform/edit/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a>";
					 echo    "&nbsp; <a href=".base_url()."index.php/faktur-makloon/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				}
				else {
					echo "<td align=center> </td>";
				}
					 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
