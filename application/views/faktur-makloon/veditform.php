<h3>Data Faktur Jasa Makloon</h3><br>
<a href="<? echo base_url(); ?>index.php/faktur-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/faktur-makloon/cform/view">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	$('#pilih_sj').click(function(){
		var jenis_makloon= jQuery('#jenis_makloon').val();
		var id_sup= jQuery('#unit_makloon').val();
		var no_fp= jQuery('#no_fp').val();
		var urlnya = "<?php echo base_url(); ?>index.php/faktur-makloon/cform/show_popup_makloon/E/"+id_sup+"/"+jenis_makloon+"/"+no_fp;
			openCenteredWindow(urlnya);
	  });
	  
	  $('#jenis_makloon').change(function(){
	  	    $("#no_sj").val('');	
	  	    $("#jum").val('0');	
	  });
	  
	  $('#unit_makloon').change(function(){
	  	    $("#no_sj").val('');	
	  	    $("#jum").val('0');	
	  });
	  
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_faktur() {
	var no_fp= $('#no_fp').val();
	var tgl_fp= $('#tgl_fp').val();
	var no_sj= $('#no_sj').val();
	var jum= $('#jum').val();
	
	if (no_sj == '') {
		alert("Nomor SJ harus dipilih..!");
		$('#no_sj').focus();
		return false;
	}
	if (no_fp == '') {
		alert("No Faktur harus diisi..!");
		$('#no_fp').focus();
		return false;
	}
	if (tgl_fp == '') {
		alert("Tanggal Faktur harus dipilih..!");
		$('#tgl_fp').focus();
		return false;
	}
	
	if (jum == '' || jum == '0') {
		$('#jum').focus();
		alert("Jumlah Total harus diisi..!");
		return false;
	}	
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-makloon/cform/updatedata" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_faktur" value="<?php echo $id_faktur ?>">
<input type="hidden" name="no_faktur_lama" value="<?php echo $query[0]['no_faktur'] ?>">
<br>
Edit Data<br><br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
<tr>
	<td>Jenis Makloon </td>
	<td><select name="jenis_makloon" id="jenis_makloon" onkeyup="this.blur();this.focus();">
				
				<?php foreach ($jenis_makloon as $jm) { ?>
					<option value="<?php echo $jm->id ?>" <?php if ($query[0]['jenis_makloon'] == $jm->id) { ?> selected="selected" <?php } ?> ><?php echo $jm->id." - ". $jm->nama ?></option>
				<?php } ?>
				</select></td>
</tr>
		
<tr>
	<td>Unit Makloon </td>
	<td><select name="unit_makloon" id="unit_makloon" onkeyup="this.blur();this.focus();">
				
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" <?php if ($query[0]['kode_unit'] == $sup->kode_supplier) { ?> selected="selected" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select></td>
</tr>
  <tr>
		<td width="15%">Nomor SJ Masuk Makloon</td>
		<td width="70%"> <input type="text" name="no_sj" id="no_sj" value="<?php echo $query[0]['no_sj'] ?>" size="40" maxlength="40" readonly="true">
		&nbsp; <input type="hidden" name="no_sj_lama" id="no_sj_lama" value="<?php echo $query[0]['no_sj'] ?>" size="40" maxlength="40">
		<input name="pilih_sj" id="pilih_sj" value="..." type="button" title="browse data sj makloon">
		</td>
	</tr>
  <tr>
    <td>Nomor Faktur</td>
    <td>
      <input name="no_fp" type="text" id="no_fp" size="20" maxlength="20" value="<?php echo $query[0]['no_faktur'] ?>">
    </td>
  </tr>
  <tr>
    <td>Tanggal Faktur</td>
    <td>
	<label>
      <input name="tgl_fp" type="text" id="tgl_fp" size="10" value="<?php echo $query[0]['tgl_faktur'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_fp" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_fp,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td>Jumlah Total</td>
    <td>
      <input name="jum" type="text" id="jum" size="10" maxlength="10" value="<?php echo $query[0]['jumlah'] ?>" >
    </td>
  </tr>
  
  <tr><td>&nbsp;</td>
	<td><input type="submit" name="submit" value="Edit" onclick="return cek_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-makloon/cform/view'"></td>
  </tr>

</table>
</form>
