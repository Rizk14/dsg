<script type="text/javascript">

$(document).ready(function() {
 
 $(".xhapus").click(function(){
 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var hps = element.attr("id");
 
 //Built a url to send
 var info = 'pid=' + hps;
 if(confirm("Yakin akan menghapus data dgn id: "+hps+" ?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('/listssp/cform/actdelete')?>",
 data: info,
 success: function(){
 	redirect('listssp/cform/');
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
 
})

</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_spp; ?> </td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_spp; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	  	<td>
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listssp/cform','update'=>'#content','type'=>'post'));
		?>
		Cari:<input type="text" name="cari" id="cari" maxlength="200" value="<?php echo $cari; ?>" /><input type="submit" id="btncari" name="btncari" value="Cari" />
		<?php echo form_close(); ?>
		</td>
	  </tr>
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listssp/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterldobrgform">

		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_ssp; ?></div></td>	
			</tr>
			<tr>
			  <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>			  
				<td width="3%" class="tdatahead">No</td>
				<td width="20%" class="tdatahead"><?php echo $list_laporansok_nmwp_ssp; ?></td>
				<td width="10%" class="tdatahead"><?php echo $list_laporansok_akunpajak_ssp; ?></td>
				<td width="10%" class="tdatahead"><?php echo $list_laporansok_jns_setor_spp; ?></td>
				<td width="15%" class="tdatahead"><?php echo $list_laporansok_urai_spp; ?></td>
				<td width="10%" class="tdatahead"><?php echo $list_laporansok_jmlbayar_spp; ?></td>
				<td width="20%" class="tdatahead"><?php echo $list_laporansok_penyetor_ssp; ?></td>
				<td width="5%" class="tdatahead">Aksi</td>
			  </tr>
			  <?php
			  if(sizeof($query)>0){
				  			$db2=$this->load->database('db_external', TRUE);
				$cc	=1;
			  	foreach($query as $row){
					
					$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
					$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";		
					
					$qpenyetor	= $db2->query(" SELECT * FROM tr_penyetor WHERE i_penyetor='$row->i_penyetor' ");	
					if($qpenyetor->num_rows()>0){
						$rpenyetor	= $qpenyetor->row();
						$penyetor	= $rpenyetor->e_penyetor;
					}else{
						$penyetor	= "";
					}
			  ?>
				  <tr class="<?=$Classnya?>" onMouseOver="this.className='rowx'"
              	 onMouseOut="this.className='<?=$Classnya?>'">
				 	<td class="tdatahead2"><?=$cc?></td>
					<td class="tdatahead2"><?php echo $row->e_initial_npwp; ?></td>
					<td class="tdatahead2"><?php echo $row->i_akun_pajak; ?></td>
					<td class="tdatahead2"><?php echo $row->i_jsetor_pajak; ?></td>
					<td class="tdatahead2"><?php echo $row->e_jsetor_pajak; ?></td>
					<td class="tdatahead2" align="right"><?php echo number_format($row->v_jml_bayar,'2','.',','); ?></td>
					<td class="tdatahead2"><?php echo $penyetor; ?></td>
					<td class="tdatahead2" align="center">
					<?php echo
					 "
					 <a href=".base_url()."index.php/listssp/cform/actdelete/".$row->i_ssp." title=\"Hapus SSP\"><img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13 alt=\"Hapus SSP\"></a>"; ?></td>
				  </tr>
			  <?php
			  		$cc++;
			  	}
			  }
			  ?>
			</table>
			  </td>
			</tr>
			<tr><td align="center"><?php echo $create_link; ?></td></tr>			
			<tr>
			  <td align="right"></td>
			</tr>
		  </table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
