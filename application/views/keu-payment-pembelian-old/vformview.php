<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Pembayaran Hutang Pembelian</h3><br> 
<a href="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('keu-payment-pembelian/cform/cari'); ?>
<!--Supplier <select name="supplier" id="supplier">
				<option value="0" <?php //if ($csupplier == '') { ?> selected="true" <?php //} ?> >- All -</option>
				<?php //foreach ($list_supplier as $sup) { ?>
					<option value="<?php //echo $sup->kode_supplier ?>" <?php //if ($csupplier == $sup->kode_supplier) { ?> selected="true" <?php //} ?> ><?php //echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php //} ?>
				</select>&nbsp; -->
Jenis Pembelian <select name="jenis_pembelian" id="jenis_pembelian">
				<option value="0" <?php if ($jenis_pembelian == '0') { ?> selected <?php } ?> >-All-</option>
				<option value="1" <?php if ($jenis_pembelian == '1') { ?> selected <?php } ?>>Cash</option>
				<option value="2" <?php if ($jenis_pembelian == '2') { ?> selected <?php } ?>>Kredit</option>
			</select>
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No Voucher</th>
		 <th>Tgl Voucher</th>
		 <th>Jenis Pembelian</th>
		 <th>Supplier</th>
		 <th>Deskripsi</th>
		 <th>Subtotal (Rp.)</th>
		 <th>Jml Bayar (Rp.)</th>
		 <th>Jml Pembulatan (Rp.)</th>
		 <th>Jns Pembulatan</th>
		 <th>CnDn</th>
		 <th>Ket CnDn</th>
		 <th>Total Jml Bayar (Rp.)</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_voucher = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_voucher']."</td>";
				 echo    "<td>".$tgl_voucher."</td>";
				 echo    "<td>";
				 if ($query[$j]['jenis_pembelian'] == '1')
					echo "Cash";
				 else
					echo "Kredit";
				 echo "</td>";
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_supplier']." - ".$var_detail[$k]['nama_supplier'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['deskripsi'];
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['subtotal'], 2, ',','.');
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jumlah_bayar'], 2, ',','.');
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						//if ($var_detail[$k]['jenis_pembulatan'] != '2')
						  echo number_format($var_detail[$k]['pembulatan'], 2, ',','.');
						//else
						//  echo "(".number_format($var_detail[$k]['pembulatan'], 2, ',','.').")";
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='center'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['jenis_pembulatan'] == '0' || $var_detail[$k]['jenis_pembulatan'] == null)
						  echo "Tidak Ada";
						else if ($var_detail[$k]['jenis_pembulatan'] == '1')
						  echo "Keatas";
						else if ($var_detail[$k]['jenis_pembulatan'] == '2')
						  echo "Kebawah";
						  
						  if ($k<$hitung-1)
						     //echo ", ";
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['cndn'], 2, ',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['keterangan_cndn'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['totjumbayar'], 2, ',','.')."</td>";
				 echo    "<td>".$tgl_update."</td>";
				
					echo    "<td align=center>";
					// edit udh ga dipake
					//echo	"<a href=".base_url()."index.php/keu-payment-pembelian/cform/edit/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a> &nbsp;";
					echo "<a href=".base_url()."index.php/keu-payment-pembelian/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					echo "</td>";

				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
