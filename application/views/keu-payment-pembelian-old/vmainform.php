<h3>Data Pembayaran Hutang Pembelian</h3><br> 
<a href="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
	
	//get_data_pkp();
	$('#topnya').hide();
	
	 $('#is_makloon').click(function(){
		 if ($('input[name=is_makloon]').is(':checked')) {
			$('#tipe_makloon').show();
		}
			else{
				$('#tipe_makloon').hide();
		}
	  });	
	  
	   $('#jenis_makloon').change(function(){
		   
		 if ($('#jenis_makloon').val()==2) {
			$('#unit_jahit').show();
			$('#unit_packing').hide();
			
		}
		else if ($('#jenis_makloon').val()==3) {
			$('#unit_packing').show();
			$('#unit_jahit').hide();
		}
		else if ($('#jenis_makloon').val()==4) {
			$('#unit_packing').show();
			$('#unit_jahit').show();
		}
			else{
				$('#unit_packing').hide();
				$('#unit_jahit').hide();
		}
	  });	
			
	$('#pilih_faktur').click(function(){
			var id_supplier= $("#id_supplier").val();	
			var jenis_pembelian= $("#jenis_pembelian").val();	
			var jenis_makloon= $("#jenis_makloon").val(); 
			var unit_makloon = 0;
			var unit_jahit =document.getElementById("id_unit_jahit");
			var list_unit_jahit = unit_jahit.options[unit_jahit.selectedIndex].value;
			
			var unit_packing =document.getElementById("id_unit_packing");
			var list_unit_packing = unit_packing.options[unit_packing.selectedIndex].value;
			
			
			
			if (jenis_makloon == 2){
			unit_makloon = list_unit_jahit;
				}
			else if (jenis_makloon == 3){
				unit_makloon = list_unit_packing;
			}
			if ($('input[name=is_makloon]').is(':checked'))
				var makloon = 't';
			else
				var makloon = 'f';
				
			
			if (jenis_makloon == 4){
				var urlnya = "<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/show_popup_faktur/"+jenis_pembelian+"/"+makloon+"/"+jenis_makloon+"/"+list_unit_jahit+"/"+list_unit_packing;
			}
			else{
			var urlnya = "<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/show_popup_faktur/"+jenis_pembelian+"/"+makloon+"/"+jenis_makloon+"/"+unit_makloon;
		}
			openCenteredWindow(urlnya);
	  });
	  
	$('#id_supplier').change(function(){
	  	    $("#no_faktur").val('');	
	  });
	
	// 13-07-2015
	$('#khusus').click(function(){
	  	    if ($('input[name=khusus]').is(':checked')) {
				$("#pilih_faktur").attr("disabled", true);
				$('#no_faktur').val('');
				$('#id_faktur').val('');
			}
			else {
				$("#pilih_faktur").attr("disabled", false);
				$('#no_faktur').val('');
				$('#id_faktur').val('');
			}
	  });
	  
	// 28-07-2015
	$('#ada_manual').click(function(){
	  	    if ($('input[name=ada_manual]').is(':checked')) {
				$("#tabelku").attr("style", "display: '';");
			}
			else {
				$("#tabelku").attr("style", "display:none;");
			}
	  });
	 
	// 28-07-2015
	$("#no2").val('2');
	
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no2").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num2_"+n;
		var new_num="#num2_"+no;
		$(num, lastRow).attr("id", "num2_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****id_supplier*************************************
		var id_supplier="#id_supplier2x_"+n;
		var new_id_supplier="#id_supplier2x_"+no;
		$(id_supplier, lastRow).attr("id", "id_supplier2x_"+no);
		$(new_id_supplier, lastRow).attr("name", "id_supplier2x_"+no);		
		//*****end id_supplier*********************************
		
		//*****jum_bayar*************************************
		var jum_bayar="#jum_bayar2_"+n;
		var new_jum_bayar="#jum_bayar2_"+no;
		$(jum_bayar, lastRow).attr("id", "jum_bayar2_"+no);
		$(new_jum_bayar, lastRow).attr("name", "jum_bayar2_"+no);		
		$(new_jum_bayar, lastRow).val('0');		
		//*****end jum_bayar*********************************
		
		//*****jum_bayarx*************************************
		var jum_bayarx="#jum_bayarx2_"+n;
		var new_jum_bayarx="#jum_bayarx2_"+no;
		$(jum_bayarx, lastRow).attr("id", "jum_bayarx2_"+no);
		$(new_jum_bayarx, lastRow).attr("name", "jum_bayarx2_"+no);	
		var  even_blur= "$('#jum_bayar2_"+no+ "').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; ";
		$(new_jum_bayarx, lastRow).val('0');
		$(new_jum_bayarx, lastRow).attr("onblur",even_blur);		
		//*****end jum_bayarx*********************************
		
		//*****subtotal*************************************
		var subtotal="#subtotal2_"+n;
		var new_subtotal="#subtotal2_"+no;
		$(subtotal, lastRow).attr("id", "subtotal2_"+no);
		$(new_subtotal, lastRow).attr("name", "subtotal2_"+no);		
		$(new_subtotal, lastRow).val('0');		
		//*****end subtotal*********************************
		
		//*****subtotalx*************************************
		var subtotalx="#subtotalx2_"+n;
		var new_subtotalx="#subtotalx2_"+no;
		$(subtotalx, lastRow).attr("id", "subtotalx2_"+no);
		$(new_subtotalx, lastRow).attr("name", "subtotalx2_"+no);	
		var  even_blur= "$('#subtotal2_"+no+ "').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; ";
		$(new_subtotalx, lastRow).val('0');
		$(new_subtotalx, lastRow).attr("onblur",even_blur);		
		//*****end subtotalx*********************************
				
		//*****deskripsi*************************************
		var deskripsi="#deskripsi2_"+n;
		var new_deskripsi="#deskripsi2_"+no;
		$(deskripsi, lastRow).attr("id", "deskripsi2_"+no);
		$(new_deskripsi, lastRow).attr("name", "deskripsi2_"+no);		
		$(new_deskripsi, lastRow).val('');				
		//*****end deskripsi*************************************	
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no2").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no2").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no2").val();	
			var y= x-1;
			$("#no2").val(y);	
		}
	});
	
	// --------------------------------------------
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 670;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_item_faktur() {
	var no_faktur= $('#no_faktur').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	if (jenis_pembelian == '0') {
		alert("Jenis pembelian harus dipilih..!");
		return false;
	}
	if ($('input[name=khusus]').is(':checked')) {
		alert("Anda akan menginput di form khusus untuk data pelunasan tanpa acuan faktur. Silahkan klik OK untuk melanjutkan");
	}
	else {
		if (no_faktur == '') {
			alert("Nomor Faktur harus dipilih..!");
			return false;
		}
	}
}

function cek_payment() {
	var no_voucher= $('#no_voucher').val();
	var tgl_voucher= $('#tgl_voucher').val();
	
	var kon=window.confirm("yakin akan simpan data ??");
	if (kon){
		if (no_voucher == '') {
			alert("Nomor Voucher Pembayaran harus diisi..!");
			return false;
		}
		if (tgl_voucher == '') {
			alert("Tanggal Voucher Pembayaran harus dipilih..!");
			return false;
		}
		
		//var jum= $('#no').val()-1; 
		var jum_subtotal= $('#jum_subtotal').val();

		if (jum_subtotal > 0) {
			for (var k=1; k <= jum_subtotal; k++) {
				if ($('#jum_bayar_'+k).val() == '' || $('#jum_bayar_'+k).val() == '0') {
					alert("Data pembayaran tidak boleh 0 / kosong...!");
					return false;
				}
				
				if ($('#deskripsi_'+k).val() == '') {
					alert("Deskripsi harus diisi...!");
					return false;
				}
				
				//alert($('#sisa_bayar_'+k).val());
				//alert($('#jum_bayar_'+k).val());
			} //return false;
		}
		else {
			alert("Data faktur tidak ada");
			return false;
		}
		
		if ($('input[name=ada_manual]').is(':checked')) {
			var jum= $('#no2').val()-1;
			for (var k=1; k <= jum; k++) {
				if ($('#subtotal2_'+k).val() == '' || $('#subtotal2_'+k).val() == '0') {
					alert("Data subtotal tagihan tidak boleh 0 / kosong...!");
					return false;
				}
				if ($('#jum_bayar2_'+k).val() == '' || $('#jum_bayar2_'+k).val() == '0') {
					alert("Data pembayaran tidak boleh 0 / kosong...!");
					return false;
				}
							
				if ($('#deskripsi2_'+k).val() == '') {
					alert("Deskripsi harus diisi...!");
					return false;
				}
			} 
		} // end if
	}
	else
		return false;
	
}

function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_payment', 'id' => 'f_payment');
echo form_open('keu-payment-pembelian/cform/', $attributes); ?>
<table>
	
	<tr>
		<td>Input Khusus Tanpa Pilih Faktur</td>
		<td><input type="checkbox" name="khusus" id="khusus" value="t"></td>
	</tr>
	<tr>
		<td>Pembelian Makloon</td>
		<td><input type="checkbox" name="is_makloon" id="is_makloon" value="t"></td>
	</tr>
	<tr style="display:none;" id="tipe_makloon">
		<td>Jenis Makloon</td>
		<td><select name="jenis_makloon" id="jenis_makloon">
				<option value="0">-Pilih-</option>
				<option value="1">Makloon Bisbisan</option>
				<option value="2">Makloon Jahit</option>
				<option value="3">Makloon Packing</option>
				<option value="4">Makloon Jahit + Packing</option>
			</select>&nbsp;</td> 
	</tr>
	
	<tr style="display:none;" id="unit_jahit">
		<td>Unit Jahit</td>
		<td><select name="id_unit_jahit" id="id_unit_jahit">
				<option value="0">-Pilih-</option>
				<?php
				foreach ($unit_jahit as $ujh){ ?>
					<option value="<?php echo $ujh->id?>"><?php echo $ujh->kode_supplier." - ".$ujh->nama  ?></option>
					<?php
					}
				?>
			</select>&nbsp;</td> 
	</tr>
	
	<tr style="display:none;" id="unit_packing">
		<td>Unit Packing</td>
		<td><select name="id_unit_packing" id="id_unit_packing">
				<option value="0">-Pilih-</option>
				<?php
				foreach ($unit_packing as $upk){ ?>
					<option value="<?php echo $upk->id?>"><?php echo $upk->kode_unit." - ".$upk->nama  ?></option>
					<?php
					}
				?>
			</select>&nbsp;</td> 
	</tr>
	
	<tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian">
				<option value="0">-Pilih-</option>
				<option value="1">Cash</option>
				<option value="2">Kredit</option>
			</select>&nbsp;</td> 
	</tr>
	
	<tr>
		<td>Nomor Faktur</td>
		<td><input type="text" name="no_faktur" id="no_faktur" value="" size="30" maxlength="30" readonly="true">
		<input type="hidden" name="id_faktur" id="id_faktur" value=""> &nbsp;
		<input type="hidden" name="is_quilting" id="is_quilting" value="">
		<input type="hidden" name="is_jahit" id="is_jahit" value="">
		<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data faktur">
		</td>
	</tr>
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/view'">
<?php echo form_close(); 
} else { ?>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/submit" method="post" enctype="multipart/form-data">

<input type="hidden" name="jenis_pembelian" value="<?php echo $jenis_pembelian ?>">
<input type="hidden" name="is_makloon" value="<?php echo $is_makloon ?>">
<input type="hidden" name="jenis_makloon" value="<?php echo $jenis_makloon ?>">

<?php 
		if (count($faktur_detail)>0) {
			$no=1;
			foreach ($faktur_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="no2" id="no2" value="">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td width="10%">Jenis Pembelian </td>
		<td><?php if ($is_makloon == 't') $makloon = "(Pembelian Makloon)"; else $makloon=""; if ($jenis_pembelian == '1') echo "Cash ".$makloon; else echo "Kredit ".$makloon; ?></td>
	</tr>
	
	<tr>
		<td>No Voucher </td>
		<td>
		<input name="no_voucher" type="text" id="no_voucher" size="10" maxlength="20" value="">&nbsp;<span id="msgbox" style="display:none"></span>
		</td>
	</tr>
  <tr>
    <td>Tgl Voucher</td>
    <td>
	<label>
      <input name="tgl_voucher" type="text" id="tgl_voucher" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_voucher" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_voucher,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td colspan="2"><br>
	
	<table id="tabelkux"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td align="right">
			&nbsp;
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Supplier</th>
          <th>Nomor Faktur</th>
           <th>Tgl Faktur</th>
	      <th>Nominal Faktur</th>
	      <th>Jml Retur</th>
	      <th>Jml Bayar</th>
	      <th>Deskripsi</th>
	      <th>Jml CnDn</th>
	      <th>Keterangan CnDn</th>
	      <!--<th>Jml Yg Sdh Dibayar</th> 
	      <th>Jml Pembulatan</th>
	      <th>Jns Pembulatan</th>
	      <th>Jml Bayar</th> -->
        </tr>

        <?php $i=1;
        if (count($faktur_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
			$zz=1; $total_persupplier = 0; $item_faktur = ""; $item_is_quilting = ""; $item_is_jahit = "";
			$item_tgl_akhir = ""; $grandtotal=0;
			for($j=0;$j<count($faktur_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
			<input type="hidden" name="id_faktur_<?php echo $i ?>" id="id_faktur_<?php echo $i ?>" value="<?php echo $faktur_detail[$j]['id'] ?>">
		<?php
		if($jenis_makloon==4){
			?>
					 <input name="unit_jahit_<?php echo $i ?>" type="text" id="unit_jahit_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $faktur_detail[$j]['kode_unit_jahit']." - ".$faktur_detail[$j]['nama_unit_jahit'] ?>"/>
            <input type="hidden" name="id_unit_jahit_<?php echo $i ?>" id="id_unit_jahit_<?php echo $i ?>" value="<?php echo $faktur_detail[$j]['id_unit_jahit'] ?>">
            <input name="unit_packing_<?php echo $i ?>" type="text" id="unit_packing_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $faktur_detail[$j]['kode_unit_packing']." - ".$faktur_detail[$j]['nama_unit_packing'] ?>"/>
            <input type="hidden" name="id_unit_packing_<?php echo $i ?>" id="id_unit_packing_<?php echo $i ?>" value="<?php echo $faktur_detail[$j]['id_unit_packing'] ?>">
<?php
					}
					else{
?>
            <input name="supplier_<?php echo $i ?>" type="text" id="supplier_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $faktur_detail[$j]['kode_supplier']." - ".$faktur_detail[$j]['nama_supplier'] ?>"/>
            <input type="hidden" name="id_supplier_<?php echo $i ?>" id="id_supplier_<?php echo $i ?>" value="<?php echo $faktur_detail[$j]['id_supplier'] ?>">
	<?php
}
		?>
		 </td>
          <td nowrap="nowrap">
            <input name="no_faktur_<?php echo $i ?>" type="text" id="no_faktur_<?php echo $i ?>" size="15" readonly="true" value="<?php echo $faktur_detail[$j]['no_faktur'] ?>"/>
		 </td>
		 <?php
			//if ($i == 1) {
				$tgl_akhir = $faktur_detail[$j]['tgl_faktur'];
				$pisah1 = explode("-", $tgl_akhir);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				$tgl_akhir = $thn1."-".$bln1."-".$tgl1;
				$item_tgl_akhir.= $tgl_akhir.";";
			//}
		 ?>
          <td><input name="tgl_faktur_<?php echo $i ?>" type="text" id="tgl_faktur_<?php echo $i ?>" size="7" readonly="true" value="<?php echo $faktur_detail[$j]['tgl_faktur'] ?>" /></td>
          <td><input style="text-align:right;" name="jum_hutang_<?php echo $i ?>" type="text" id="jum_hutang_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $faktur_detail[$j]['jum_hutang'] ?>" readonly="true" /></td>
          
          <td><input style="text-align:right;" name="jum_retur_<?php echo $i ?>" type="text" id="jum_retur_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $faktur_detail[$j]['tot_retur'] ?>" readonly="true" /></td>
          
         
        </tr>
        <?php 
			$item_faktur.= $faktur_detail[$j]['id'].";";
			//$item_is_quilting.= $faktur_detail[$j]['is_quilting'].";";
			//$item_is_jahit.= $faktur_detail[$j]['is_jahit'].";";
			$total_persupplier = $total_persupplier + $faktur_detail[$j]['jum_hutang'] - $faktur_detail[$j]['tot_retur'] ;
			if (isset($faktur_detail[$j+1]['id_supplier']) && ($faktur_detail[$j]['id_supplier'] != $faktur_detail[$j+1]['id_supplier'])) {
				//$total_persupplier = number_format($total_persupplier, 2, '.','');
				
				// 1 nov 2011, hitung jml retur utk kode brg yg diluar range tgl faktur
				/* alurnya: cek tanggal awal dan tanggal akhir dari faktur2 yg dipilih, 
				 * apakah ada yg sama dgn yg di data retur. Kalo ada, maka cek di tabel retur_beli_detail 
				 * apakah ada kode brg yg is_luar_range = 't'. Jika ada maka masukkan ke jumlah returnya
				 * 
				 * */
				 $tgl_awal = $faktur_detail[$j]['tgl_faktur'];
				 $pisah1 = explode("-", $tgl_awal);
				 $tgl1= $pisah1[0];
				 $bln1= $pisah1[1];
				 $thn1= $pisah1[2];
				 $tgl_awal = $thn1."-".$bln1."-".$tgl1;
				 
				 $list_item_tgl_akhir = explode(";", $item_tgl_akhir);
				 $ambil_tgl_akhir = $list_item_tgl_akhir[0]; // ambil tgl akhir (thn-bln-tgl) yg dari awal
				 
		
				$tot_retur_lain = 0;
			
				// 05-06-2012
				$pisah1 = explode("-", $tgl_awal);
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tgl_awal_modif = $thn1."-".$bln1."-01";
				
				//07-02-2013
				$pisah1 = explode("-", $ambil_tgl_akhir);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$bln1,1,$thn1);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				$tgl_akhir_modif = $thn1."-".$bln1."-".$lastDay;
				
					// 11-04-2012. range query diubah dari faktur_date_from = '$tgl_awal' menjadi faktur_date_from >= '$tgl_awal' AND faktur_date_to <= '$tgl_akhir'
					// 07-02-2013, $ambil_tgl_akhir ganti jadi $tgl_akhir_modif
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli 
										WHERE id_supplier = '".$faktur_detail[$j]['id_supplier']."'
										AND faktur_date_from >= '$tgl_awal_modif' AND faktur_date_to <= '$tgl_akhir_modif' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$id_retur	= $hasilrow->id;
						
					
						
						$query3	= $this->db->query(" SELECT id FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
						//echo "SELECT id FROM tm_retur_beli_detail 
						//				WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur'";
						if ($query3->num_rows() > 0){
							//$hasilrow = $query3->row();
							//$tot_retur_lain	= $hasilrow->tot_retur;
							$query31	= $this->db->query(" SELECT id_pembelian_nofaktur FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$id_retur' ");
							if ($query31->num_rows() > 0){
								$hasilrow31 = $query31->row();
								$id_fakturcek	= $hasilrow31->id_pembelian_nofaktur;
							
								$query32	= $this->db->query(" SELECT status_lunas FROM tm_pembelian_nofaktur 
											WHERE id = '$id_fakturcek' AND id_supplier='".$faktur_detail[$j]['id_supplier']."' ");
								if ($query32->num_rows() > 0){ // ini utk faktur pembelian non-quilting
									$hasilrow32 = $query32->row();
									$status_lunas	= $hasilrow32->status_lunas;
									
									if ($status_lunas == 'f') {
										$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
														WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
										if ($query33->num_rows() > 0){
											$hasilrow33 = $query33->row();
											$tot_retur_lain	= $hasilrow33->tot_retur;
										}
										else
											$tot_retur_lain = 0;
									}
									else
										$tot_retur_lain = 0;
								}
								else { // ini faktur makloon quilting
									$query32	= $this->db->query(" SELECT status_lunas FROM tm_faktur_makloon 
												WHERE no_faktur = '$no_fakturcek' AND kode_unit='".$faktur_detail[$j]['id_supplier']."' ");
									if ($query32->num_rows() > 0){
										$hasilrow32 = $query32->row();
										$status_lunas	= $hasilrow32->status_lunas;
										
										if ($status_lunas == 'f') {
											$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
															WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
											if ($query33->num_rows() > 0){
												$hasilrow33 = $query33->row();
												$tot_retur_lain	= $hasilrow33->tot_retur;
											}
											else
												$tot_retur_lain = 0;
										}
										else
											$tot_retur_lain = 0;
									} // ENDDDD
								}
							}
						}
						else
							$tot_retur_lain = 0;
							
						// ================================== end 07-02-2013 ====================================
					}
					else {
						$id_retur	= 0;
						$tot_retur_lain = 0;
					}
			
				// ===========================================================================================
					$tot_retur_lain = number_format($tot_retur_lain, 2, '.','');
					
					$total_persupplier = $total_persupplier-$tot_retur_lain;
					// 27-04-2015
					$grandtotal+=$total_persupplier;
					
					$total_persupplier = number_format($total_persupplier, 2, '.','');
		?>
			<tr>
				<td colspan="4" align="right">Jml Retur Diluar Range Tgl satu<input style="text-align:right;" readonly="true" name="retur_lain_<?php echo $zz ?>" type="text" id="retur_lain_<?php echo $zz ?>" size="15" value="<?php echo $tot_retur_lain ?>" /> <?php echo $faktur_detail[$j]['nama_supplier'] ?> TOTAL</td>
				<td colspan="2"><input style="text-align:right;" readonly="true" name="subtotal_<?php echo $zz ?>" type="text" id="subtotal_<?php echo $zz ?>" size="15" value="<?php echo $total_persupplier ?>" /></td>
				<input type="hidden" name="id_supplier2_<?php echo $zz ?>" id="id_supplier2_<?php echo $zz ?>" value="<?php echo $faktur_detail[$j]['id_supplier'] ?>">
				<input type="hidden" name="item_faktur_<?php echo $zz ?>" id="item_faktur_<?php echo $zz ?>" value="<?php echo $item_faktur ?>">
				<input type="hidden" name="item_is_quilting_<?php echo $zz ?>" id="item_is_quilting_<?php echo $zz ?>" value="<?php echo $item_is_quilting ?>">
				<input type="hidden" name="item_is_jahit_<?php echo $zz ?>" id="item_is_jahit_<?php echo $zz ?>" value="<?php echo $item_is_jahit ?>">
				
				<td><input style="text-align:right;" name="jum_bayar_<?php echo $zz ?>" type="text" id="jum_bayar_<?php echo $zz ?>" size="15" maxlength="20" value="<?php echo $total_persupplier ?>" /></td>
				<td><input name="deskripsi_<?php echo $zz ?>" type="text" id="deskripsi_<?php echo $zz ?>" size="25" value="" /></td>
				<td><input style="text-align:right;" name="jum_cndn_<?php echo $zz ?>" type="text" id="jum_cndn_<?php echo $zz ?>" size="10" maxlength="20" value="0" /></td>
				<td><input name="ket_cndn_<?php echo $zz ?>" type="text" id="ket_cndn_<?php echo $zz ?>" size="20" value="" /></td>
			</tr>
		<?php		
				$total_persupplier = 0;
				$item_faktur = "";
				$item_is_quilting = "";
				$item_is_jahit = "";
				$item_tgl_akhir = "";
				$zz++;
			}
			
			if (!isset($faktur_detail[$j+1]['id_supplier'])) {
				//echo "woi 07-02-2013 ";
				//$total_persupplier = number_format($total_persupplier, 2, '.','');
				
				 $tgl_awal = $faktur_detail[$j]['tgl_faktur'];
				 $pisah1 = explode("-", $tgl_awal);
				 $tgl1= $pisah1[0];
				 $bln1= $pisah1[1];
				 $thn1= $pisah1[2];
				 $tgl_awal = $thn1."-".$bln1."-".$tgl1;
				 
				 $list_item_tgl_akhir = explode(";", $item_tgl_akhir);
				 $ambil_tgl_akhir = $list_item_tgl_akhir[0]; // ambil tgl akhir (thn-bln-tgl) yg dari awal
				 				 
				/* $query2	= $this->db->query(" SELECT id FROM tm_retur_beli 
										WHERE id_supplier = '".$faktur_detail[$j]['id_supplier']."'
										AND faktur_date_from = '$tgl_awal' AND faktur_date_to = '$tgl_akhir' "); */
					
				// ===========================================================================================
				// 11-04-2012 perlu ada pengecekan apakah returnya itu pake acuan faktur atau bukan.
				$tot_retur_lain = 0;
				/*$sqlcek = "SELECT count(b.id) FROM tm_retur_beli a, tm_retur_beli_faktur b 
							WHERE a.id = b.id_retur_beli AND b.no_faktur = '".$faktur_detail[$j]['no_faktur']."' 
							AND a.id_supplier = '".$faktur_detail[$j]['id_supplier']."' "; echo $sqlcek; 
				$querycek = $this->db->query($sqlcek);
				if ($querycek->num_rows() > 0){ // ini utk data lama yg nota debetnya pake acuan faktur
				*/
				
				// 05-06-2012
				$pisah1 = explode("-", $tgl_awal);
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tgl_awal_modif = $thn1."-".$bln1."-01";
				/*$sqlnya = " SELECT id FROM tm_retur_beli 
										WHERE id_supplier = '".$faktur_detail[$j]['id_supplier']."'
										AND faktur_date_from >= '$tgl_awal' AND faktur_date_to <= '$ambil_tgl_akhir' ";
				echo $sqlnya; */
				
				//07-02-2013
				$pisah1 = explode("-", $ambil_tgl_akhir);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$bln1,1,$thn1);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				$tgl_akhir_modif = $thn1."-".$bln1."-".$lastDay;
				
				//ini yg lama pake $ambil_tgl_akhir. skrg pake variabel baru: $tgl_akhir_modif
				/*echo "SELECT id FROM tm_retur_beli 
										WHERE id_supplier = '".$faktur_detail[$j]['id_supplier']."'
										AND faktur_date_from >= '$tgl_awal_modif' AND faktur_date_to <= '$ambil_tgl_akhir'"; */
						if ($is_makloon == '') {				
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli 
										WHERE id_supplier = '".$faktur_detail[$j]['id_supplier']."'
										AND faktur_date_from >= '$tgl_awal_modif' AND faktur_date_to <= '$tgl_akhir_modif' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$id_retur	= $hasilrow->id;
						
						/*
						 * SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
							tm_retur_beli_faktur b, tm_retur_beli_detail c WHERE a.id = b.id_retur_beli
							AND b.id = c.id_retur_beli_faktur 
							AND b.no_faktur = '$no_faktur' AND a.id_supplier = '$id_supplier'
						 * 
						 * */
						 
						 //31-01-2013, cek di retur_beli_detail apakah ada item brg yg is_luar_range='t'. 
						//jika ada, maka cek acuan fakturnya dan liat status lunasnya 't' atau 'f'. 
					/*	echo "SELECT id FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur'"; */
						$query3	= $this->db->query(" SELECT id FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
						if ($query3->num_rows() > 0){
							//$hasilrow = $query3->row();
							//$tot_retur_lain	= $hasilrow->tot_retur;
							$query31	= $this->db->query(" SELECT id_pembelian_nofaktur FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$id_retur' ");
							if ($query31->num_rows() > 0){
								$hasilrow31 = $query31->row();
								$id_fakturcek	= $hasilrow31->id_pembelian_nofaktur;
							
								$query32	= $this->db->query(" SELECT status_lunas FROM tm_pembelian_nofaktur 
											WHERE id = '$id_fakturcek' AND id_supplier='".$faktur_detail[$j]['id_supplier']."' ");
								if ($query32->num_rows() > 0){ // ini utk faktur pembelian non-quilting
									$hasilrow32 = $query32->row();
									$status_lunas	= $hasilrow32->status_lunas;
									
									if ($status_lunas == 'f') {
										$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
														WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
										if ($query33->num_rows() > 0){
											$hasilrow33 = $query33->row();
											$tot_retur_lain	= $hasilrow33->tot_retur;
										}
										else
											$tot_retur_lain = 0;
									}
									else
										$tot_retur_lain = 0;
								}
							
							} //
							else {
								// 09-02-2013, jika ga ada acuan faktur sama sekali, maka kita anggap status lunasnya f
								$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
														WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
								if ($query33->num_rows() > 0){
									$hasilrow33 = $query33->row();
									$tot_retur_lain	= $hasilrow33->tot_retur;
								}
								else
									$tot_retur_lain = 0;
							}
						}
						else
							$tot_retur_lain = 0;
						
					}
					else {
						$id_retur	= 0;
						$tot_retur_lain = 0;
					}
			
			}
				$tot_retur_lain = number_format($tot_retur_lain, 2, '.','');
					
					$total_persupplier = $total_persupplier-$tot_retur_lain;
					// 27-04-2015
					$grandtotal+=$total_persupplier;
					
					$total_persupplier = number_format($total_persupplier, 2, '.','');
        ?>
			<tr>
				<td colspan="4" align="right">Jml Retur Diluar Range Tgl dua<input style="text-align:right;" readonly="true" name="retur_lain_<?php echo $zz ?>" type="text" id="retur_lain_<?php echo $zz ?>" size="15" value="<?php echo $tot_retur_lain ?>" /> 
				<?php
			if($jenis_makloon==4){
				echo $faktur_detail[$j]['nama_unit_jahit']." - Packing ".$faktur_detail[$j]['nama_unit_packing'];
				}
				else
				 echo $faktur_detail[$j]['nama_supplier'] ?> TOTAL</td>
				<td colspan="2"><input style="text-align:right;" readonly="true" name="subtotal_<?php echo $zz ?>" type="text" id="subtotal_<?php echo $zz ?>" size="15" value="<?php echo $total_persupplier ?>" /></td>
				<?php
				if($jenis_makloon==4){
					?>
					<input type="hidden" name="id_unit_jahit2_<?php echo $zz ?>" id="id_unit_jahit2_<?php echo $zz ?>" value="<?php echo $faktur_detail[$j]['id_unit_jahit'] ?>">
					<input type="hidden" name="id_unit_packing2_<?php echo $zz ?>" id="id_unit_packing2_<?php echo $zz ?>" value="<?php echo $faktur_detail[$j]['id_unit_packing'] ?>">
					<?php
					}
					else{
				?>
				<input type="hidden" name="id_supplier2_<?php echo $zz ?>" id="id_supplier2_<?php echo $zz ?>" value="<?php echo $faktur_detail[$j]['id_supplier'] ?>">
				<?php
			}
				?>
				<input type="hidden" name="item_faktur_<?php echo $zz ?>" id="item_faktur_<?php echo $zz ?>" value="<?php echo $item_faktur ?>">
				<input type="hidden" name="item_is_quilting_<?php echo $zz ?>" id="item_is_quilting_<?php echo $zz ?>" value="<?php echo $item_is_quilting ?>">
				<input type="hidden" name="item_is_jahit_<?php echo $zz ?>" id="item_is_jahit_<?php echo $zz ?>" value="<?php echo $item_is_jahit ?>">
				
				<td><input style="text-align:right;" name="jum_bayar_<?php echo $zz ?>" type="text" id="jum_bayar_<?php echo $zz ?>" size="15" maxlength="20" value="<?php echo $total_persupplier ?>" /></td>
				<td><input name="deskripsi_<?php echo $zz ?>" type="text" id="deskripsi_<?php echo $zz ?>" size="25" value="" /></td>
				<td><input style="text-align:right;" name="jum_cndn_<?php echo $zz ?>" type="text" id="jum_cndn_<?php echo $zz ?>" size="10" maxlength="20" value="0" /></td>
				<td><input name="ket_cndn_<?php echo $zz ?>" type="text" id="ket_cndn_<?php echo $zz ?>" size="20" value="" /></td>
			</tr>
        <?php 
				$total_persupplier = 0;
				$item_faktur = "";
				$item_is_quilting = "";
				$item_is_jahit = "";
				$item_tgl_akhir = "";
				$zz++;
			}
        ?>
        
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td colspan="4" align="right">GRAND TOTAL Yang Harus Dibayar</td>
			<td colspan="2" align="center"><b><?php echo number_format($grandtotal, 2, '.',','); ?></b></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<input type="hidden" name="jum_subtotal" id="jum_subtotal" value="<?php echo $zz-1 ?>">
			<td colspan="9" align="center"><!--<input type="submit" name="submit" value="Simpan" onclick="return cek_payment();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/'">--></td>
		</tr>
	</table>	
	<br>
      </td>
    </tr>
	<tr>
		<td colspan="2"><input type="checkbox" name="ada_manual" id="ada_manual" value="t">Tambahkan Input Manual</td>
	</tr>
	<tr>
		<td colspan="2"><br>
		<!--<form name="myform">-->
		<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2" style="display:none;">
			<tr>
				<td colspan="5" align="right"><input id="addrow" type="button" name="addrow" value=" + ">&nbsp;
				<input id="deleterow" type="button" name="deleterow" value=" - ">
				</td>
			</tr>
			<tr>
			  <th width="3%">No</th>
			  <th>Supplier</th>
			  <th>Subtotal</th>
			  <th>Jml Bayar</th>
			  <th>Deskripsi</th>
			</tr>
			
			<tr align="center">
			  <td align="center" id="num2_1">1</td>
			  <td><select name="id_supplier2x_1" id="id_supplier2x_1">
					<?php foreach ($supplier as $sup) { ?>
						<option value="<?php echo $sup->id ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
					<?php } ?>
					</select></td>
			 <td style="white-space:nowrap;">
			   <input name="subtotalx2_1" type="text" id="subtotalx2_1" size="10" style="text-align:right;" value="0" onblur="javascript: $('#subtotal2_1').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; " />
			   <input type="hidden" name="subtotal2_1" id="subtotal2_1" value="0">
			 </td>
			 <td style="white-space:nowrap;">
			   <input name="jum_bayarx2_1" type="text" id="jum_bayarx2_1" size="10" style="text-align:right;" value="0" onblur="javascript: $('#jum_bayar2_1').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; " />
			   <input type="hidden" name="jum_bayar2_1" id="jum_bayar2_1" value="0">
			 </td>
			 <td><input name="deskripsi2_1" type="text" id="deskripsi2_1" size="25" value="" /></td>
			</tr>
			
		</table>	
      <!--</form>-->
      </td>
	</tr>
	<tr>
				<td colspan="2" align="center"><br>
				<input type="submit" name="submit" value="Simpan" onclick="return cek_payment();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/'">
				</td>
			</tr>
</table>
</div>
</form>
<?php } ?>
