<h3>Input kelompok Jahit dan Packing</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-hub-unit/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-hub-unit/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url){
	
	var width = 640;
	var height = 480;
	var left =parseInt((screen.availWidth/2) -(width/2));
	var top =parseInt((screen.availHeight/2) -(height/2));
	
	var windowFeatures= "width=" + width + ",height=" + height 
	+ ",status,resizeable,toolbar,scrollbars,left=" + left 
	+ ",top=" + top + ",screenX=" + left + ",screenY=" + top ;
	
	myWindow = window.open(url,"subWind",windowFeatures);
	
	}

function cek_data(){
	var nama_kelompok=$('#nama_kelompok').val();
	var id_unit_jahit=$('#id_unit_jahit').val();
	var id_unit_packing=$('#id_unit_packing').val();
	
	kon = window.confirm("Yakin akan simpan ini !!");
	
	if(kon){
		if (nama_kelompok==0){
			alert("Nama Kelompok harus diisi");
			return false;
			}
		}
	}

$(function(){
$('#pilih_unit_packing').click(function(){
	
	var urlnya = "<?php echo base_url();?>index.php/mst-hub-unit/cform/show_popup_packing";
	
	openCenteredWindow(urlnya);
	
	});
	
$('#pilih_unit_jahit').click(function(){

	var urlnya = "<?php echo base_url();?>index.php/mst-hub-unit/cform/show_popup_jahit";
	
	openCenteredWindow(urlnya);
})

});

</script>
<?php
$attributes= array('name'=>'f_master_unit','id'=>'f_master_unit');
echo form_open('mst-hub-unit/cform/submit',$attributes);
?>

<table>
<tr>
<td>Nama Kelompok : </td>
<td><input type='text' name='nama_kelompok' id='nama_kelompok'  ></td>
</tr>

<tr>
<td>Unit Jahit: </td>
<td><input type="text" name="nama_unit_jahit" id="nama_unit_jahit"></td>
<td><input type='hidden' name='id_unit_jahit' id='id_unit_jahit'  ></td>
<td><input type="hidden" name='kode_unit_jahit' id='kode_unit_jahit'></td>
<td><input name="pilih_unit_jahit" id="pilih_unit_jahit" value="..." type ="button" title="browse data Unit Jahit"</td>
</tr>

<tr>
<td>Unit Packing: </td>
<td><input type="text" name="nama_unit_packing" id="nama_unit_packing"></td>
<td><input type='hidden' name='id_unit_packing' id='id_unit_packing'  ></td>
<td><input type="hidden" name='kode_unit_packing' id='kode_unit_packing'></td>
<td><input name="pilih_unit_packing" id="pilih_unit_packing" value="..." type ="button" title="browse data Unit Packing"</td>
</tr>

<tr>
<td>
<input type="submit" id="simpan" type="button" value="Simpan" onClick="return cek_data()">
<input type="button" id="batal "name="batal" value="Batal" onClick="window.location='<?php echo base_url();?>index.php/mst-hub-unit/cform/view'">
</td>
</tr>
</table>

<?php
echo form_close();
?>
