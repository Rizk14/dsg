<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	.uraian {
    font-size: 10px;}

</style>

<h3>Kartu Hutang</h3><br><br>

<div>
Supplier: <?php if ($supplier != '0') { echo $kode_supplier." - ".$nama_supplier; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br> 
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('kartuhutang/cform/export_excel', $attributes); ?>
<input type="hidden" name="id_supplier" value="<?php echo $supplier ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>Tanggal</th>
		 <th width="30%">Uraian</th>
		 <th>Nomor Bukti</th>
		 <th>Saldo Awal</th>
		 <th>Pembelian</th>
		 <th>Pelunasan</th>
		 <th>C/n</th>
		 <th>Pembulatan</th>
		 <th>Saldo Akhir</th>
	 </tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $date_from ?></td>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td align="right"><?php echo number_format($saldo_awal,4,',','.') ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<th>&nbsp;</th>
		</tr>
		 <?php
			if (is_array($query)) {
				$tot_pembelian = 0;
				$tot_pelunasan = 0;
				$tot_pembulatan = 0;

				for($j=0;$j<count($query);$j++){
					// hitung jumlah total masing2 field
					$tot_pembelian+= $query[$j]['pembelian'];
					$tot_pelunasan+= $query[$j]['pelunasan'];
					$tot_pembulatan+= $query[$j]['pembulatan'];
					
				} // end header
			}
			else {
				$tot_saldoawal = 0;
				$tot_pembelian = 0;
				$tot_pelunasan = 0;
				$tot_pembulatan = 0;
			}
			
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['tgl_bukti']."</td>";
				 if ($query[$j]['is_pembelian'] == 1) {
					 if ($query[$j]['status_lunas'] == 't')
						echo    "<td class='uraian'>Pembelian (SUDAH LUNAS): <br>".$query[$j]['list_brg']."</td>";
					else
						echo    "<td class='uraian'>Pembelian (BELUM LUNAS): <br>".$query[$j]['list_brg']."</td>";
				}
				 else
					echo    "<td class='uraian'>Pelunasan: <br>".$query[$j]['deskripsi_pelunasan']."</td>";
				 echo    "<td>".$query[$j]['no_bukti']."</td>";
				 echo    "<td>&nbsp;</td>";
				 
				 if ($query[$j]['pembelian'] != '')
					echo    "<td align='right'>".number_format($query[$j]['pembelian'],4,',','.')."</td>";
				 else
					echo    "<td>&nbsp;</td>";
					
				 if ($query[$j]['pelunasan'] != '')
					echo    "<td align='right'>".number_format($query[$j]['pelunasan'],4,',','.')."</td>";
				 else
					echo    "<td>&nbsp;</td>";
				 echo    "<td>&nbsp;</td>";
				 
				 if ($query[$j]['pelunasan'] != '')
					echo    "<td align='right'>".number_format($query[$j]['pembulatan'],4,',','.')."</td>";
				 else
					echo    "<td>&nbsp;</td>";
				 echo    "<td>&nbsp;</td>";
				 echo  "</tr>";
		 	}
		   }
		   
		   $saldo_akhir = $saldo_awal+$tot_pembelian-$tot_pelunasan+$tot_pembulatan;
		 ?>
		 <tr>
			<td><?php echo $date_to ?></td>
			<td>Saldo Akhir</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right"><?php echo number_format($saldo_akhir,4,',','.') ?></td>
		</tr>
		 
		 <tr>
			<td colspan="3" align="right"><b>TOTAL</b></td>
			<td align="right"><b><?php echo number_format($saldo_awal,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_pembelian,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($tot_pelunasan,4,',','.')  ?></b></td>
			<td align="right">&nbsp;</td>
			<td align="right"><b><?php echo number_format($tot_pembulatan,4,',','.')  ?></b></td>
			<td align="right"><b><?php echo number_format($saldo_akhir,4,',','.')  ?></b></td>
		 </tr>
 	</tbody>
</table><br>
<?php //echo $this->pagination->create_links();?>
</div>
