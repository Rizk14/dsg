<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function toUpper(val){
	var imotif	= val.value;
	var toUpper	= imotif.toUpperCase();
	val.value	= toUpper;
}

function settextfield(a,b,c,d,e) {

   	var ada	= false;
	var z	= "<?=$iterasi?>";
	var y	= parseInt(z)-1;
	
  	for(i=0;i<=y;i++){
		if(
			(a==opener.document.getElementById("i_product_tblItem_"+i).value)
		){
			alert ("kode Barang "+a+" sdh ada!");
			ada=true;
			break;
		}else{
			ada=false;
		}
    }
    
    if(!ada){	
		opener.document.getElementById('i_product_tblItem_'+'<?php echo $iterasi; ?>').value=a;
		opener.document.getElementById('e_product_name_tblItem_'+'<?php echo $iterasi; ?>').value=b;
		opener.document.getElementById('v_product_price_tblItem_'+'<?php echo $iterasi; ?>').value=c;
		opener.document.getElementById('f_stp_tblItem_'+'<?php echo $iterasi; ?>').value=d;
		opener.document.getElementById('i_so_tblItem_'+'<?php echo $iterasi; ?>').value=e;
		opener.document.getElementById('n_unit_tblItem_'+'<?php echo $iterasi; ?>').focus();
		this.close();
	}
}

function getInfo(txt){

var dsj = document.getElementById('dsj').value;

$.ajax({

type: "POST",
url: "<?php echo site_url('sj/cform/flistbarangjadi');?>",
data:"key="+txt+"&dsj="+dsj,
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};

function carifocus() {
	document.getElementById('txtcari').focus();
}
</script>

</head>
<body id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="toUpper(document.getElementById('txtcari'));getInfo(this.value);"/>
  	  <input type="hidden" name="dsj" id="dsj" value="<?=$dsj?>">
  	</td>
  </tr>
  <tr>
    <td align="left">
	<?php echo form_open('fakpenjualan/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php

		if(sizeof($isi)==0){
			$isi = $isiopsi;
		}
					
		if(!empty($isi) || isset($isi)) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			
			if(count($isi)) {
				$cc	= 1;
				
				foreach($isi as $row) {
					
					$iproduct	= trim($row->iproduct);
					
					$list .= "
					 <tr>
					  <td>".$cc."</td>	 
					  <td><a href=\"javascript:settextfield('$iproduct','$row->productname','$row->price','$row->stp','$row->iso');\">".$row->iproduct."</a></td>
					  <td><a href=\"javascript:settextfield('$iproduct','$row->productname','$row->price','$row->stp','$row->iso');\">".$row->productname."</a></td>
					  <td><a href=\"javascript:settextfield('$iproduct','$row->productname','$row->price','$row->stp','$row->iso');\">".$row->price."</a></td>
					 </tr>";
					 $cc+=1;
				}
			}
		}
		?>
		
		<table class="listtable2" border="1">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th><?php echo strtoupper("Kode BRG"); ?></th>
		 <th><?php echo strtoupper("Nama Barang"); ?></th>
		 <th><?php echo strtoupper("Harga BRG"); ?></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>"; ?>
  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
