<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Jenis Bahan</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-jns-bhn/cform/cari'); ?>
Jenis Barang 
				<select name="kel_jns_brg" id="kel_jns_brg">
					<option value="0" <?php if ($ekode_kel_jns == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($kel_jns_brg as $kel) { ?>
					<option value="<?php echo $kel->id ?>" <?php if ($kel->id == $ekode_kel_jns) { ?> selected="true" <?php } ?> ><?php echo "[".$kel->nama_kel."] ".$kel->kode."-".$kel->nama ?></option>
				<?php } ?>
				</select>
<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th>Kel & Jenis Barang</th>
		 <th>Kode Jenis Bhn</th>
		 <th>Nama Jenis Bhn</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>[$row->nama_kel] $row->kode_jenis - $row->nama_jenis</td>";
				 echo    "<td>$row->kode</td>";
				 echo    "<td>$row->nama</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 // cek apakah data kode sudah dipake di tm_barang
				 $query3	= $this->db->query(" SELECT kode_brg FROM tm_barang WHERE id_jenis_bahan = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 else
					$ada = 0;
					
				 echo    "<td align=center><a href=".base_url()."index.php/mst-jns-bhn/cform/index/$row->id \" id=\"$row->id\">Edit</a>";
				 
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/mst-jns-bhn/cform/delete/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table><br>
<? echo $this->pagination->create_links();?>
</div>
