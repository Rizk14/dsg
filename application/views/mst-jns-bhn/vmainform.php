<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();
	var kode_jenis= $('#kode_jenis').val();

	if (kode == '') {
		alert("Kode harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Jenis harus diisi..!");
		$('#nama').focus();
		return false;
	}
	if (kode_jenis == '') {
		alert("Jenis Barang harus dipilih..!");
		$('#kode_jenis').focus();
		return false;
	}
	
}

$(function()
{
		
	$('#pilih_jenis').click(function(){

	  	    var id_kel_brg= jQuery('#kel_brg').val();

			var urlnya = "<?php echo base_url(); ?>index.php/mst-jns-bhn/cform/show_popup_jenis/"+id_kel_brg;

			openCenteredWindow(urlnya);

	  });
	  

});
	
</script>

<h3>Data Jenis Bahan</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
	echo form_open('mst-jns-bhn/cform/submit', $attributes); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> 
<input type="hidden" name="id_jenis_bhn" value="<?php echo $eid ?>">
<input type="hidden" name="kodeeditjnsbhn" value="<?php echo $ekode ?>">
<input type="hidden" name="kodeeditjns" value="<?php echo $ekode_jenis ?>">
 <?php } ?>
	<table>
		<tr>
			<td>Kelompok Barang</td>
			<td> <select name="kel_brg" id="kel_brg">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" <?php if ($kel->kode == $ekode_kel) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td> <input type="text" name="kode_jenis" id="kode_jenis" value="<?php echo $ekode_jenis." - ".$enama_jenis ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="<?php echo $eid_jenis ?>">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang">
			</td>
		</tr>
		<tr>
			<td>Kode Jenis Bahan</td>
			<td> <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="2" size="2" <?php if ($edit == '1') { ?> readonly="true" <?php } ?> >
			</td>
		</tr>
		<tr>
			<td>Nama Jenis Bahan</td>
			<td> <input type="text" name="nama" id="nama" value="<?php echo $enama ?>" maxlength="30" size="30"></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-jns-bhn/cform/view'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

