
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#d_forcast_first").datepicker();
	$("#d_forcast_last").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_expo_forcast; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_expo_forcast; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
			<?php 
		$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('expoforcast/cform/carilistforcast', $attributes);?>
		
		<div id="masterexpoforcastform">

			<table width="50%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td><?php echo $list_no_forcast_expo_forcast; ?> </td>
							<td>:</td>
							<td><label>
							  <input name="i_forcast" type="text" id="i_forcast" maxlength="100"/>
							  <img name="img_nomor_do" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Kode Forcast" class="imgLink" align="absmiddle" onclick="infoexpoforcast();" >
							</label></td>
						  </tr>	
						  <tr>
							<td><?php echo $list_drop_forcast_expo_forcast; ?> </td>
							<td>:</td>
							<td><label>
							  <input name="i_drop_forcast" type="text" id="i_drop_forcast" maxlength="100"/>
							  <img name="img_nomor_do" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Kode Forcast" class="imgLink" align="absmiddle" onclick="infoexpodropforcast();" >
							</label></td>
						  </tr>							  
						  <tr>
							<td width="24%"><?php echo $list_tgl_forcast_expo_forcast; ?> </td>
							<td width="0%">&nbsp;</td>
							<td>
							  <input name="d_forcast_first" type="text" id="d_forcast_first" maxlength="10" />
							s.d 
							<input name="d_forcast_last" type="text" id="d_forcast_last" maxlength="10" />
							<span style="color:#FF0000">*</span>
							</td>
						  </tr>
						</table>				
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
			  </tr>
			  <tr>
			  	<td align="right">
				<input type="hidden" name="stop" id="stop" value="0" />
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" />
				<input name="btnbatal" type="reset" id="btnbatal" value="<?php echo $button_batal; ?>" />
				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
