<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
 
<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_expo_forcast; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> FORM <?php echo $page_title_expo_forcast; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'','update'=>'#content','type'=>'post'));
		?>
		<div id="masterexpoforcastform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td><?php echo $list_no_forcast_expo_forcast; ?> </td>
							<td>:</td>
							<td><label>
							  <input name="i_forcast" type="text" id="i_forcast" maxlength="100" value="" />
							  <img name="img_nomor_do" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Kode Forcast" class="imgLink" align="absmiddle" >
							</label></td>
						  </tr>	
						  <tr>
							<td><?php echo $list_drop_forcast_expo_forcast; ?> </td>
							<td>:</td>
							<td><label>
							  <input name="i_drop_forcast" type="text" id="i_drop_forcast" maxlength="100" value="" />
							  <img name="img_nomor_do" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Kode Drop Forcast" class="imgLink" align="absmiddle" >
							</label></td>
						  </tr>							  
						  <tr>
							<td width="24%"><?php echo $list_tgl_forcast_expo_forcast; ?> </td>
							<td width="0%">&nbsp;</td>
							<td>
							  <input name="d_forcast_first" type="text" id="d_forcast_first" maxlength="10" value="<?php echo $tglforcastmulai; ?>"/>
							s.d 
							<input name="d_forcast_last" type="text" id="d_forcast_last" maxlength="10" value="<?php echo $tglforcastakhir; ?>" />
							<span style="color:#FF0000">*</span>
							</td>
						  </tr>
						</table>
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td>
			  	<div id="title-box2"><?php echo $form_title_detail_expo_forcast; ?></div>
			  	</td>
			  </tr>
			  <tr>
				<td>
				<?php
				if($template==0){
				?>
				<?php }else{ ?>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="2%" class="tdatahead">NO</td>
					<td width="10%" align="center" class="tdatahead"><?php echo $list_kd_brg_expo_forcast; ?> </td>
					<td width="40%" align="center" class="tdatahead"><?php echo $list_nm_brg_expo_forcast; ?></td>
					<td width="8%" align="center" class="tdatahead"><?php echo $list_jml_forcast_expo_forcast; ?> </td>
					<td width="8%" align="center" class="tdatahead"><?php echo $list_jml_sj_expo_forcast; ?> </td>
					<td width="8%" align="center" class="tdatahead"><?php echo $list_sisa_forcast_expo_forcast; ?></td>
				  </tr>
				  
				  <?php
				  
				  $lfrcst = "";
				  
				  if(sizeof($query) > 0){
					  
				  	$no	= 1;
					$cc	= 1;
					
				  	foreach($query as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$qtotalforcast = $this->mclass->totalforcast($row->i_product,$tforcastfirst,$tforcastlast);
						if($qtotalforcast->num_rows()>0){
							$rtotalforcast 		= $qtotalforcast->row();							
							$totalforcast 		= $rtotalforcast->totalforcast;
							$totalsisaforcast	= $rtotalforcast->totalsisaforcast;
							$totalsj			= $totalforcast-$totalsisaforcast;
						}else{
							$totalforcast 		= 0;
							$totalsisaforcast	= 0;
							$totalsj			= 0;
						}
						
						$lfrcst	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\" onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->i_product."</td>
								<td>".$row->e_product_name."</td>
								<td align=\"right\">".$totalforcast."</td>
								<td align=\"right\">".$totalsj."</td>
								<td align=\"right\">".$totalsisaforcast."</td>
							  </tr>";							  
						$no+=1; 
						$cc+=1;
					}
					
					echo $lfrcst;
				  }
				  
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
								
				<?php } ?>
				</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>	  
			  
			  <tr>
			  	<td align="right">
					<input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/expoforcast/cform/'">

				
				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
