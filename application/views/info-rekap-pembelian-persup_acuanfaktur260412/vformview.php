<h3>Rekap Pembelian Per Supplier</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
Jenis Pembelian: <?php if ($jenis_beli == 1) echo "Cash"; else echo "Kredit"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr>
		 <th>No</th>
		 <th>Nama Supplier</th>
		 <th>Bahan Baku</th>
		 <th>Bahan Pembantu</th>
		 <th>Jumlah</th>
	 </tr>
	</thead>
	<tbody>
		 <?		$total_baku = 0;
				$total_pembantu = 0;
				$total_semua = 0;
			if (is_array($query)) {
				$total_baku = 0;
				$total_pembantu = 0;
				$total_semua = 0;
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['nama_supplier']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_total_baku'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_total_pembantu'],2,',','.')."</td>";

				 $totalnya = $query[$j]['jum_total_baku'] + $query[$j]['jum_total_pembantu'];
				 echo    "<td align='right'>".number_format($totalnya,2,',','.')."</td>";
				 $total_baku += $query[$j]['jum_total_baku'];
				 $total_pembantu += $query[$j]['jum_total_pembantu'];
				 $total_semua += $totalnya;
				 echo  "</tr>";					
		 	}
		   }
		 ?>
		 <tr>
			<td>&nbsp;</td>
			<td><b>TOTAL</b></td>
			<td align="right"><b><?php echo number_format($total_baku,2,',','.') ?></b></td>
			<td align="right"><b><?php echo number_format($total_pembantu,2,',','.') ?></b></td>
			<td align="right"><b><?php echo number_format($total_semua,2,',','.') ?></b></td>
		 </tr>
 	</tbody>
</table><br>
</div>
