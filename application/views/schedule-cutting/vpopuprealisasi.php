<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>

<script>
$(function()
{
	$("#pilih").click(function()
	{
		/* var idx=$("#idx").val(); 
		var des=$("#no_pb_cutting").val(); */
				
		var kode_bhn_baku	= '';
		var id_realisasi_detail = '';
		
		opener.document.forms["f_op"].item_brg.value = '';
		opener.document.forms["f_op"].id_realisasi_detail.value = '';
		
		if (document.f_master_brg.cek.length>0){
			for(var i=0; i < document.f_master_brg.cek.length; i++){
				if(document.f_master_brg.cek[i].checked==true){
					kode_bhn_baku+= document.f_master_brg.kode_bhn_baku[i].value + ";";
					id_realisasi_detail+= document.f_master_brg.id_realisasi_detail[i].value + ";";
				}
				
			}
			opener.document.forms["f_op"].item_brg.value = kode_bhn_baku;
			opener.document.forms["f_op"].id_realisasi_detail.value = id_realisasi_detail;
		}else{
			opener.document.forms["f_op"].item_brg.value = document.getElementById('kode_bhn_baku').value;
			opener.document.forms["f_op"].id_realisasi_detail.value = document.getElementById('id_realisasi_detail').value;
		}	
		self.close();
	});
});
</script>
	<center><h3>List Data Realisasi Cutting Yang Belum Beres</h3></center>
<div align="center"><br>

<form id="f_master_brg" name="f_master_brg">

	<table border="1" width="100%" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" nowrap="nowrap">
	<thead>
	 <tr>
		 <th>No / Tgl Realisasi</th>
		 <th>Jenis</th>
		 <th>List Bahan (Plan Qty Schedule-Jml Gelar-Hsl Potong)</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if(is_array($query)){

			 for($j=0;$j<count($query);$j++){

				$var_detail = array();
				$var_detail = $query[$j]['detail_realisasi'];
				$hitung = count($var_detail);
						
				if(is_array($var_detail)>0){
					$pisah1 = explode("-", $query[$j]['tgl_realisasi']);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					$tgl_realisasi = $tgl1." ".$nama_bln." ".$thn1;
					
					 echo "<tr class=\"record\">";
					 echo "<td>".$query[$j]['no_realisasi']." / ".$tgl_realisasi."</td>";
					 if ($query[$j]['is_dacron'] == 't')
						echo "<td>Dacron</td>";
					else
						echo "<td>Non-Dacron</td>";
						
					 echo "<td>";
					 
					 if(is_array($query[$j]['detail_realisasi'])){
						for($k=0;$k<count($var_detail); $k++){
							if ($var_detail[$k]['kode_brg_quilting'] == '') {
								$kode_brgnya = $var_detail[$k]['kode_brg'];
								echo "[".$var_detail[$k]['kode_brg_jadi']."-".$var_detail[$k]['nama_brg_jadi']."] ". $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama_brg']." (".$var_detail[$k]['qty_schedule']." - ".$var_detail[$k]['jum_gelar']." - ".$var_detail[$k]['hasil_potong'].")";
							}
							else {
								//$kode_brgnya = $var_detail[$k]['kode_brg_quilting'];
								echo "[".$var_detail[$k]['kode_brg_jadi']."-".$var_detail[$k]['nama_brg_jadi']."] ". $var_detail[$k]['kode_brg_quilting']." - ".$var_detail[$k]['nama_brg_quilting']." (".$var_detail[$k]['qty_schedule']." - ".$var_detail[$k]['jum_gelar']." - ".$var_detail[$k]['hasil_potong'].")";
							}
							
							  echo "<input type='checkbox' name='cek' id='cek' value='y' />";
							  echo "<input type='hidden' name='kode_bhn_baku' id='kode_bhn_baku' value='".$kode_brgnya."' />";
							  echo "<input type='hidden' name='id_realisasi_detail' id='id_realisasi_detail' value='".$var_detail[$k]['id']."' />";
							  echo "<input type='hidden' name='id_realisasi' id='id_realisasi' value='".$var_detail[$k]['id_realisasi_cutting']."' />";

							  if ($k<$hitung-1)
								 echo "<br>";
						}
					 }
					 echo "</td>";
					 ?>
					 
					<?php echo  "</tr>";
				}
		 	}
		   }else{
		   }
		 ?>
 	</tbody>
</table>

  <? //echo $this->pagination->create_links();?>
  <br />
  <input type="button" name="pilih" id="pilih" value="Pilih" >
</div>

</form>
