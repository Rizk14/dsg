<style>
.vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
}

</style>

<div class="container">
<h2>Makloon Baju Gudang Jadi WIP</h2>
<hr>

<?php 

echo form_open('/dashboard/makloon_baju_gudang_jadi_wip/updatedata', array('id'=>'myform', 'class'=>'form-vertical', 'role'=>'form', 'method'=>'post')) ?>
	  
	   <input type="hidden" name="no" id="no" value="">
	   
	   <input type="hidden" name="id" id="id" value=<?php echo $values[0]['id'] ; ?>>
	   <div id='my_div_1'>   
			<?php echo form_input('num',1,'id="num" class="form-control" maxlength="3" size="1" align="center" disabled' ) ?> 
           
           <span class="label label-info">No SJ</span>
           <input type="hidden" name="no_sj_wip_lama" id="no_sj_wip_lama" value="<?php echo $values[0]['no_sj'] ?>">
            <?php echo form_input('no_sj', $values[0]['no_sj'], 'id="no_sj" class="form-control" placeholder="No SJ" maxlength="10" disabled') ?>            
            
            <span class="label label-info">Tanggal SJ</span>
			<div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_sj', $values[0]['tanggal_sj'], 'id="tanggal_sj" class="form-control" placeholder="Tanggal SJ" maxlength="10" disabled') ?>
			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar" ></i></span>
            </div>
            
           <span class="label label-info">Jenis Masuk</span>
           
           
            <select class="form-control" id="unit_jahit" readonly="TRUE">
			<?php	if($values[0]['jenis_masuk'] == 1){ ?>
			 <option value = '1'> Masuk Bagus dari Unit Jahit</option> 
			 <?} elseif($values[0]['jenis_masuk'] == 2){ ?>
			 <option value = '2'> Masuk Bagus dari Unit Packing</option> 
			  <?} elseif($values[0]['jenis_masuk'] == 3){ ?>
			 <option value = '3'> Masuk Lain - lain</option> 
				<?} ?>
  </select>
			
			
 <span class="label label-info">Unit Jahit</span>
  <select class="form-control" id="unit_jahit" readonly="TRUE">
   <?php
		foreach ($list_unit_jahit as $luj){
			?>
			<option value=''> - </option>
			 <option  
			 <?php if ($values[0]['id_unit_jahit'] == $luj->id) { ?>  selected <?php } ?> value="<?php echo $luj->id ?>">
				<?php echo $luj->nama_unit_jahit ?> 
			 </option disabled>
	<?php } ?>
  </select>
      
        
       
 <span class="label label-info">Unit Packing</span>
  <select class="form-control" id="unit_packing" readonly="TRUE">
   <?php
		foreach ($list_unit_packing as $lup){
			?>
			<option value=''> - </option>
			 <option 
			 <?php if ($values[0]['id_unit_packing'] == $lup->id) { ?>  selected <?php } ?> value="<?php echo $values[0]['id_unit_packing'] ?>">
				<?php echo $lup->nama_unit_packing ?> 
			 </option disabled>
	<?php } ?>
  </select>
       
 


<span class="label label-info">Gudang :</span>
  <select class="form-control" id="gudang" readonly="TRUE">
   <?php
		foreach ($list_gudang as $lug){
			?>	
			 <option 
			 <?php if ($values[0]['id_gudang'] == $lug->id) { ?>  selected <?php } ?> value="<?php echo $lug->id ?>">
				<?php $lug->nama_gudang ?> 
			 </option>
	<?php } ?>
  </select>
</div>

<span class="label label-info">Keterangan Header</span>
           <input type="hidden" name="keterangan_header_lama" id="keterangan_header_lama" value="<?php echo $values[0]['keterangan_header'] ?>" >
            <?php echo form_input('keterangan_header', $values[0]['keterangan_header'], 'id="keterangan_header" class="form-control" placeholder="Keterangan Header" maxlength="10" disabled' ) ?>      
       
        
        
        <br>

                 <div id='my_div_1'>
				  <?php 
				  $i =1 ;
         $detail_data =$values[0]['detail_data'];
         for($k=0;$k<count($detail_data);$k++){
			 ?>
				<input type="text" name="no" id="no"  class="form-control input-group-lg" value="<?php echo $i ?> " readonly="true">
				
				<input type="hidden" name="id_barang_wip_<?php echo $i ?>" id="id_barang_wip_<?php echo $i ?>" value="<?php echo $detail_data[$k]['id_barang_wip'] ?>" >
                <input id="kode_barang_wip_<?php echo $i ?>" class="col-lg-3 " type="text" name="kode_barang_wip_<?php echo $i ?>"
                       title="Masukkan Kode Barang Wip"  value="<?php echo $detail_data[0]['kode_barang_wip'] ?>"
                       placeholder="Kode Barang WIP" onclick= "javascript: openCenteredWindows('<?php echo base_url(); ?>index.php/dashboard/makloon_baju_gudang_jadi_wip/barang_wip/<?php echo $i ;?>'disabled);"
                       disabled/>
	
                <input id="nama_barang_wip_<?php echo $i ?>" class="col-lg-3" type="text" name="nama_barang_wip_<?php echo $i ?>"
                       title="Masukkan Nama Barang Wip"  value="<?php echo $detail_data[$k]['nama_barang_wip'] ?>"
                       placeholder="Nama Barang WIP" disabled/ >
		
                <input id="qty_<?php echo $i ?>" class="col-lg-3" type="text" name="qty_<?php echo $i ?>"
                       title="Masukkan Quantity" value="<?php echo $detail_data[$k]['qty'] ?>"
                       placeholder="Quantity" disabled/>
                       
               <input id="keterangan_detail_<?php echo $i ?>" class="col-lg-3" type="text" name="keterangan_detail_<?php echo $i ?>"
                       title="Masukkan keterangan detail" value="<?php echo $detail_data[$k]['keterangan_detail'] ?>"
                       placeholder="Keterangan detail" disabled/>   

<?php $i++ ;
                       	 }
         
         
         ?>     
        </div>
<?php echo form_close() ?>
 <br>
  <br>
  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 
 <?php echo anchor('/dashboard/approve_makloon_baju/view', 'Keluar', 'class="btn btn-danger " role="button" '); ?> 


</div>


