<?php
// Nomor urut data di tabel.
$per_page = 2;

// Posisi nomor halaman (untuk paging) jika user login / tidak.
$login_status = $this->session->userdata('login_status');
$user_bagian = $this->session->userdata('user_bagian');
$page = $this->uri->segment(4);

// Nomor urut data di tabel.
// Ini karena library pagination menggunakan option 'use_page_numbers' => true.
if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Data Status Makloon Baju Dari Unit Packing</h2>
    <hr>

    <!-- Paging dan form pencarian -->
    <div class="row navigasi_cari">
        <!-- Paging -->
        <div class="col-xs-12 col-md-6">
            <?php echo (!empty($paging)) ? $paging : ''?>
        </div>
        <!-- /Paging -->

        <!-- Form Pencarian -->
        <div class="col-xs-12 col-md-4 pull-right">
            <form method="get" action="<?php echo site_url('dashboard/approve_makloon_baju/cari');?>" role="form" class="form-horizontal">
                <div class="input-group">
                    <input type="text" name="kata_kunci" class="form-control" placeholder="Masukkan Username pada pencarian" id="kata_kunci" value="<?php echo $this->input->get('kata_kunci')?>">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /Form Pencarian -->
    </div>
    <!-- /Paging dan form pencarian -->

    <?php if (!empty($approve_makloon_baju) && is_array($approve_makloon_baju)): ?>
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>No SJ</th>
					<th>Tanggal SJ</th>
					<th>Jenis Masuk</th>
					<th>Gudang Masuk</th>
					<th>Unit Packing</th>
					<th>Unit Jahit</th>
                    <th>Status Verifikasi</th>
					<th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
             
                for($i=0;$i<count($approve_makloon_baju);$i++){ 
                   
                    // Link edit, hapus, cetak
                    $link_detail = anchor('dashboard/approve_makloon_baju/link_detail/'.$approve_makloon_baju[$i]['id'], '<span class="glyphicon glyphicon-link"></span>', array('title' => 'Detail'));
               
                    // Status verifikasi
                    if ($approve_makloon_baju[$i]['status_approve'] == 'f') {	
                        $verifikasi = '<td class="status-danger">' . anchor('dashboard/approve_makloon_baju/ubah_status_verifikasi/'.$approve_makloon_baju[$i]['id'], format_status_approve($approve_makloon_baju[$i]['status_approve']), 'data-confirm="Mengubah status verifikasi ke SUDAH?"') . '</td>';
                    } else {
                        $verifikasi = '<td class="status-biasa">' . anchor('dashboard/approve_makloon_baju/ubah_status_verifikasi/'.$approve_makloon_baju[$i]['id'], format_status_approve($approve_makloon_baju[$i]['status_approve']), 'data-confirm="Mengubah status verifikasi ke BELUM?"')  . '</td>';
                    }
                    ?>
                    <tr>
                        <td><?php echo ++$offset ?></td>
                        <td><?php echo $approve_makloon_baju[$i]['no_sj'] ?></td>
                        <td><?php echo  $approve_makloon_baju[$i]['tanggal_sj']?></td>
                        <td><?php if($approve_makloon_baju[$i]['jenis_masuk'] == 1)
							echo "Masuk Bagus dari Unit Jahit" ;
							elseif ($approve_makloon_baju[$i]['jenis_masuk'] == 2)
							echo "Masuk Bagus dari Unit Packing " ;
							elseif ($approve_makloon_baju[$i]['jenis_masuk'] == 3)
							echo "Masuk Lain - Lain " ;
						 ?></td>
						<td><?php echo $approve_makloon_baju[$i]['nama_gudang_masuk'] ?></td>
						<td><?php echo $approve_makloon_baju[$i]['nama_unit_packing'] ?></td>
						<td><?php echo $approve_makloon_baju[$i]['nama_unit_jahit'] ?></td>
							<?php echo $verifikasi ?>
						<td> <?php echo $user_bagian == '2' ? $link_detail : $link_detail ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $approve_makloon_baju ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->
