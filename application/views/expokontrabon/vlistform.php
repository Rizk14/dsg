<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_kontrabon; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_kontrabon; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'expokontrabon/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlkontrabonform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_kontrabon_no_kontrabon; ?> </td>
					<td width="1%">:</td>
					<td width="70%">
					  <input name="no_kontrabon" type="text" id="no_kontrabon" maxlength="14" value="<?=$nokotrabon?>" />
					  <input name="i_kontrabon" type="hidden" id="i_kontrabon" value="<?=$ikontrabon?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_kontrabon_tgl_kontrabon; ?> </td>
					<td>:</td>
					<td>
					  <input name="d_kontrabon_first" type="text" id="d_kontrabon_first" maxlength="10" value="<?=$tglkontrabonmulai?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_kontrabon_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_kontrabon_last" type="text" id="d_kontrabon_last" maxlength="10" value="<?=$tglkontrabonakhir?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_kontrabon_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>
				  
				  <tr>
					<td width="20%"><?php echo $list_kontrabon_nota_sederhana; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="f_nota_sederhana" type="checkbox" id="f_nota_sederhana" value="<?=$tf_nota_sederhana?>" <?=$checked?> />
					  <input name="tf_nota_sederhana" type="hidden" id="tf_nota_sederhana" value="<?=$tf_nota_sederhana?>" />
					</td>
				  </tr>
				  				  
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
			    <td>
			    <div id="title-box2"><?php echo $form_title_detail_kontrabon; ?></div></td>	
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<?php if($templates==2) { ?>
				  <tr>
					<td width="4%" class="tdatahead">NO</td>
					<td width="12%" class="tdatahead"><?php echo strtoupper($list_kontrabon_no_kontrabon); ?> </td>
					<td width="10%" class="tdatahead"><?php echo strtoupper($list_kontrabon_tgl_kontrabon); ?> </td>
					<td width="15%" class="tdatahead"><?php echo strtoupper($list_kontrabon_nilai); ?> </td>
					<td width="15%" class="tdatahead"><?php echo strtoupper($list_kontrabon_discount); ?> </td>
					<td width="15%" class="tdatahead"><?php echo strtoupper($list_kontrabon_ppn); ?></td>
					<td width="16%" class="tdatahead"><?php echo strtoupper($list_kontrabon_total_tagih); ?></td>
					<td width="30%" class="tdatahead"><?php echo strtoupper($list_kontrabon_status_pelunasan); ?></td>
				  </tr>
				  <?php

				  $no	= 1;
				  $cc	= 1;
				  $arr	= 0;
				  
				  if(sizeof($query) > 0 ) {

					  foreach($query as $row) {
					  
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";					
						
						$show = 0;
						
						if(($row->f_pelunasan=='t') && 
							($row->v_grand_sisa < $row->v_total_grand) && 
							($row->v_grand_sisa>0))
						{
							$flunas = "Proses";
						} else if($row->f_pelunasan=='t' && $row->v_grand_sisa==0) {
							$flunas = "Sudah";
						} else {
							$flunas = "Belum";
							$show	= 1;
						}
						
						/*
						if($show==1) {
							$link_act	= "<td align=\"center\" ><a href=\"javascript:void(0)\" onclick='show(\"expokontrabon/cform/edit/".$row->i_dt."/".$row->i_dt_code."/".$tf_nota_sederhana."\",\"#content\")' title=\"Edit Kontra Bon\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Kontra Bon\" border=\"0\"></a>
							&nbsp;<a href=\"javascript:void(0)\" onclick='show(\"expokontrabon/cform/undo/".$row->i_dt."/".$row->i_dt_code."/".$tf_nota_sederhana."\",\"#content\")' title=\"Cancel Kontra Bon\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Kontra Bon\" border=\"0\"></a>
							</td>";
						} else {
							$link_act	= "<td align=\"center\" >&nbsp;</td>";
						}		
						*/
						
						$exp_ddt		= explode("-",$row->d_dt,strlen($row->d_dt));// yyyy-mm-dd
						
						$lkontrabon	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\" onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->i_dt_code."&nbsp;<a href=\"javascript:void(0)\" onclick='show(\"expokontrabon/cform/detail/".$row->i_dt_code."/".$row->i_dt."/".$turi3."/".$turi4."/".$tf_nota_sederhana."\",\"#content\")' title=\"Detail Kontra Bon\"><img src=".base_url()."asset/theme/images/magnifier.png width=16 height=16 alt=\"Detail Kontra Bon\" border=\"0\"></a></td>
								<td>".$exp_ddt[2]."/".$exp_ddt[1]."/".$exp_ddt[0]."</td>
								<td align=\"right\">".number_format($row->v_total,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->v_discount,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->v_ppn,'2','.',',')."</td>
								<td align=\"right\">".number_format($row->v_total_grand,'2','.',',')."</td>
								<td>".$flunas."</td>
							  </tr>";
							  
							  $no+=1;
							  $cc+=1;
							  $arr+=1;
					  }
					  
					  echo $lkontrabon;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <?php } else if($templates==1) { ?>
				  <tr>
					<td width="2%" class="tdatahead">NO</td>
					<td width="8%" class="tdatahead"><?php echo strtoupper($list_kontrabon_no_kontrabon); ?> </td>
					<td width="9%" class="tdatahead"><?php echo strtoupper($list_kontrabon_tgl_kontrabon); ?> </td>					
					<td width="8%" class="tdatahead"><?php echo strtoupper($list_kontrabon_no_faktur); ?> </td>
					<td width="9%" class="tdatahead"><?php echo strtoupper($list_kontrabon_tgl_faktur); ?> </td>
					<td width="25%" class="tdatahead"><?php echo strtoupper($list_kontrabon_pelanggan); ?> </td>
					<td width="8%" class="tdatahead"><?php echo strtoupper($list_kontrabon_kd_brg); ?> </td>
					<td width="40%" class="tdatahead"><?php echo strtoupper($list_kontrabon_nm_brg); ?></td>
				  </tr>
				  <?php 
				  $no	= 1;
				  $cc	= 1;
				  
				  if(sizeof($query) > 0 ) {

					  foreach($query as $row) {
					  
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";							
						
						$exp_nota	= explode("-",$row->d_nota,strlen($row->d_nota));// yyyy-mm-dd
						$exp_dt		= explode("-",$row->d_dt,strlen($row->d_dt));// yyyy-mm-dd
						
						$lkontrabon	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->i_dt_code."</td>
								<td>".$exp_dt[2]."/".$exp_dt[1]."/".$exp_dt[0]."</td>
								<td>".$row->i_faktur_code."</td>
								<td>".$exp_nota[2]."/".$exp_nota[1]."/".$exp_nota[0]."</td>
								<td align=\"left\">".$row->e_branch_name."</td>
								<td align=\"center\">".$row->i_product."</td>
								<td align=\"left\">".$row->e_product_name."</td>
							  </tr>";
							  
							  $no+=1;
							  $cc+=1;
					  }
					  
					  echo $lkontrabon;
				  }
				  ?>		  
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>				  
				  <?php } ?>
				</table></td>
			  </tr>
			  <tr>
			  	<td align="center"><?php echo $create_link; ?></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>	
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <?php
			  if($templates==1) {
				  $qtotalkontrabon = $this->mclass->totalkontrabon($turi3,$turi4,$turi5);
				  $rtotalkontrabon = $qtotalkontrabon->row();
			  ?>
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="60%" align="right"><?php echo $list_kontrabon_total_tagih; ?></td>
					<td width="2%">:</td>
					<td width="20%" align="right">
					  <input name="v_total_tagihan_all" type="text" id="v_total_tagihan_all" maxlength="50" style="text-align:right;" value="<?php echo number_format($rtotalkontrabon->totalkontrabon,'2','.',','); ?>" style="text-align:right;" />
					</td>
					</tr>
				</table></td>
			  </tr>
			  <?php
			  }
			  ?>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
				  <?php if($templates==2) {	?>
				  <input name="btnkonversi" type="button" id="btnkonversi" value="<?php echo $button_excel; ?>" onclick='show("expokontrabon/cform/expokontrabon/<?=$turi1?>/<?=$turi2?>/<?=$turi3?>/<?=$turi4?>/<?=$turi5?>/","#content");' <?php if($disabled=='t') echo "disabled"; ?> /> 	
				  <?php } ?>
				  <input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/expokontrabon/cform/'">

				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
