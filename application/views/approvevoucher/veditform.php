
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#tgl_voucher").datepicker();
});

function checked1() {
	var x = document.getElementById('jml');
	var banyak	= x.value;
	
	if(document.getElementById('ckall').checked==true) {
		for (i = 0; i <= parseInt(banyak); i++) {
			document.getElementById('nilai_voucher_tblItem_'+i).disabled	= false;
			if(document.getElementById('nilai_voucher_tblItem_'+i).value=='0' || document.getElementById('nilai_voucher_tblItem_'+i).value==''){
				document.getElementById('nilai_voucher_tblItem_'+i).value		= 0;
			}
		}
		document.getElementById('f_nilai_manual').value = 't';
	}else{
		for (i = 0; i <= parseInt(banyak); i++){
			document.getElementById('nilai_voucher_tblItem_'+i).disabled	= true;
			document.getElementById('piutang_tblItem_'+i).value = document.getElementById('piutanghidden_tblItem_'+i).value;
			if(document.getElementById('nilai_voucher_tblItem_'+i).value=='' || document.getElementById('nilai_voucher_tblItem_'+i).value=='0') {
				document.getElementById('nilai_voucher_tblItem_'+i).value		= '';
			}
		}
		document.getElementById('f_nilai_manual').value = 'f';
	}
	
}

function assign_voucher() {
	
	var totalpiutang = parseInt(document.getElementById('totalpiutang').value);
	var totalbayar = parseInt(document.getElementById('totalbayar').value);
	var total_nilai_voucher	= parseInt(document.getElementById('total_nilai_voucher').value);
	var nilai = totalpiutang+totalbayar;
	var total_nilai_voucher_hidden = document.getElementById('total_nilai_voucherhidden');
	
	if(total_nilai_voucher > nilai) {
		
		alert('Nilai voucher '+total_nilai_voucher+' melebihi (piutang='+totalpiutang+'+total voucher='+totalbayar+') ');
		
		document.getElementById('total_nilai_voucher').value = total_nilai_voucher_hidden.value;
		document.getElementById('total_nilai_voucher').focus();
	}
}

function hitung(coloum,posisi) {

	var jml_voucher	= document.getElementById('nilai_voucher_tblItem_'+posisi).value;
	var jml_voucherhidden = document.getElementById('nilai_voucherhidden_tblItem_'+posisi).value;
	var piutang = document.getElementById('piutanghidden_tblItem_'+posisi).value;
	
	var sisa_piutang = (parseInt(piutang)+parseInt(jml_voucherhidden)) - parseInt(jml_voucher);
	
	var total_voucher = document.getElementById('total_nilai_voucher').value;
	var jml = 0;
	var sisa;
	var coloum_next = posisi+1;
	
	var piutangsisa;
	
	piutangsisa = (parseInt(piutang)+parseInt(jml_voucherhidden))-parseInt(jml_voucher);
	
	if(total_voucher!='' || total_voucher!=0) {
		
		for(j=0;j<coloum;j++) {
			
			var nilai_voucher = parseInt(document.getElementById('nilai_voucher_tblItem_'+j).value);
			
			if(nilai_voucher=='') {
				nilai_voucher = 0;
			}
			
			jml = jml+nilai_voucher;
		}
		
		sisa = parseInt(total_voucher)-parseInt(jml);
		
		if(parseInt(jml)>parseInt(total_voucher)) {
			alert('Nilai voucher '+jml+'\nmelebihi Total voucher ('+total_voucher+') yg dimasukan!');
		}
		
		document.getElementById('piutang_tblItem_'+posisi).value = piutangsisa;

	}else{
		alert('Total nilai voucher tdk boleh kosong!');
		document.getElementById('nilai_voucher_tblItem_'+posisi).value = 0;
		document.getElementById('total_nilai_voucher').focus();
	}
}

function ckapprove2() {
	if(document.getElementById('ckapprove').checked==true) {
		document.getElementById('ckapprovehidden').value = 't';
	}else{
		document.getElementById('ckapprovehidden').value = 'f';
	}
}
	
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_voucher; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_voucher; ?></td></tr>
   <tr>
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
	    
		<?php
		echo $this->pquery->form_remote_tag(array('url'=>'approvevoucher/cform/actedit','update'=>'#content','type'=>'post'));
		?>
		
		<div id="masterlvoucherform">
		
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_voucher_kd_sumber; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="kode_sumber" type="text" id="kode_sumber" maxlength="14" onclick="kodesumbervoucher();" readonly value="<?php echo $kodesumber; ?>" />
					  <input name="i_kode_sumber" id="i_kode_sumber" type="hidden" value="<?php echo $i_voucher_code; ?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_no; ?></td>
					<td>:</td>
					<td>
					  <input name="no_voucher" type="text" id="no_voucher" maxlength="14" value="<?php echo $i_voucher_no; ?>" readonly />
					  <input name="i_voucher" type="hidden" id="i_voucher" value="<?php echo $i_voucher; ?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_tgl; ?></td>
					<td>:</td>
					<td>
					  <input name="tgl_voucher" type="text" id="tgl_voucher" maxlength="10" value="<?php echo $d_voucher; ?>" disabled />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_kd_perusahaan; ?></td>
					<td>:</td>
					<td>
					  <input name="kode_perusahaan" type="text" id="kode_perusahaan" maxlength="100" value="<?php echo $i_company_code; ?>" readonly />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_deskripsi; ?></td>
					<td>:</td>
					<td>
					  <input name="deskripsi_voucher" type="text" id="deskripsi_voucher" maxlength="255" value="<?php echo $e_description; ?>" readonly />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_total; ?></td>
					<td>:</td>
					<td>
					  <input type="text" name="total_nilai_voucher" id="total_nilai_voucher" maxlength="100" value="<?php echo number_format($v_total_voucher,'2','.',','); ?>" onkeyup="assign_voucher();" readonly />
					  <input type="hidden" name="total_nilai_voucherhidden" id="total_nilai_voucherhidden" value="<?php echo $v_total_voucher; ?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_received; ?></td>
					<td>:</td>
					<td>
					  <input name="recieved_voucher" type="text" id="recieved_voucher" maxlength="200" value="<?php echo $e_recieved; ?>" readonly />
					</td>
				  </tr>				  
				  <tr>
					<td><?php echo $list_voucher_approved; ?></td>
					<td>:</td>
					<td>
					<?php
					$val1 = $f_approve_dept=='t'?'checked':'';
					$val2 = $f_approve_dept=='f'?'checked':'';
					?>
					  <input name="approve_voucher" type="text" id="approve_voucher" maxlength="200" value="<?php echo $e_approved; ?>" readonly />&nbsp;
					  <input name="app_dept" id="app_dept" type="radio" value="t" <?php echo $val1; ?> disabled />Departement&nbsp;
					  <input name="app_dept" id="app_dept" type="radio" value="f" <?php echo $val2; ?> disabled />Services
					</td>
				  </tr>
				</table></td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_voucher; ?></div></td>	
			</tr>
			<tr>
			  <tr>
				<td><table width="70%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="3%" class="tdatahead">NO</td>
					<td width="10%" class="tdatahead"><?php echo $list_voucher_no_kontrabon; ?></td>
					<td width="10%" class="tdatahead"><?php echo $list_voucher_tgl_kontrabon; ?> </td>
					<td width="15%" class="tdatahead"><?php echo $list_voucher_total_kontrabon; ?></td>
					<td width="15%" class="tdatahead"><?php echo $list_voucher_piutang; ?></td>
					<td width="16%" class="tdatahead"><input type="checkbox" name="ckall" id="ckall" onclick="checked1();" <?php if($f_nilai_manual=='t'){ echo "checked"; } ?> disabled />&nbsp;<?php echo $list_voucher_manual_input; ?>
					&nbsp;<?php echo $list_voucher_nilai_voucher_detail; ?>
					</td>
				  </tr>
				  <tr>
					<td colspan="6">
						<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
						<?php
						if(isset($isi) && sizeof($isi)>0) {
							echo $isi;
						}
						?>
						</table>
					</td>	
				  </tr>
				  <tr>
					<td colspan="6">&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
				  <input name="f_nilai_manual" id="f_nilai_manual" type="hidden" value="<?php echo $f_nilai_manual; ?>" />
				  <input type="hidden" id="ckapprovehidden" name="ckapprovehidden" value="f" />
				  <input type="checkbox" id="ckapprove" name="ckapprove" onclick="ckapprove2();" <?php if($f_approved=='t') echo "checked"; ?> /> Approve &nbsp;
				  <input name="btnupdate" id="btnupdate" value="<?php echo $button_approve; ?>" type="submit" />&nbsp;	
				  <input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="show('approvevoucher/cform','#content')" />
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
