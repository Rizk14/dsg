
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		$("#d_voucher_first").datepicker();
		$("#d_voucher_last").datepicker();
	});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_voucher; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_voucher; ?></td></tr>
   <tr>
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php
		 $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('approvevoucher/cform/carilistvoucher', $attributes);?>
		
		<div id="masterlvoucherform">
		
			<table width="60%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_voucher_no; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="no_voucher" type="text" id="no_voucher" maxlength="14" readonly />
					  <input name="kode_sumber" type="hidden" id="kode_sumber" />
					  <input name="i_voucher" type="hidden" id="i_voucher" />
					  <img name="img_i_fpenjualan_" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Tampilkan Nomor Voucher" class="imgLink" align="absmiddle" onclick="infolistvoucherpembayaran();">
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_tgl; ?> </td>
					<td>:</td>
					<td>
					  <input name="d_voucher_first" type="text" id="d_voucher_first" maxlength="10" />
					s.d 
					<input name="d_voucher_last" type="text" id="d_voucher_last" maxlength="10" />
					<span style="color:#FF0000">*</span>
					</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" />
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
