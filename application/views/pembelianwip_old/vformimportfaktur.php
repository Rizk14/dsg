<h3>Import Data Faktur Pembelian WIP (Hasil Jahit)</h3><br>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
	var fileName= $('#userfile').val();

	if (fileName == '') {
		alert("file yg akan diupload harus dipilih..!");
		return false;
	}
	
	if (fileName.split(".")[1].toUpperCase() != "XLS") {
		alert('Jenis file yg diperbolehkan adalah .xls !');
		return false;
	}
	
	var re = /(?:\.([^.]+))?$/;
	var ext = re.exec(fileName)[1];   // "txt"
	/*var ext = re.exec("file.txt")[1];                  // "txt"
	var ext = re.exec("file")[1];                      // undefined
	var ext = re.exec("")[1];                          // undefined
	var ext = re.exec(null)[1];                        // undefined
	*/
    //var xx1 = ext.toLowerCase(); // ini utk supaya huruf kecil semua. kalo hrf besar, toUpperCase();
    
    /*var yy1 = jenisfile.toLowerCase();
    var yy2 = jenistabel.toLowerCase();
    var gabung = yy2+"."+yy1;
    var xx1 = fileName.toLowerCase();
	
	if (gabung!=xx1) {
		alert("nama file dan/atau ext file tidak sama");
		return false;
	} */

	$('#gambarnya').show();
	$('#btnsubmit').attr("disabled", true);
	
}

</script>

<?php 
//$attributes = array('name' => 'f_baca', 'id' => 'f_baca');
//echo form_open('bacadbf/cform/', $attributes); 
echo form_open_multipart('pembelianwip/cform/doimport',array('id' => 'f_baca', 'name'=>'f_baca')); ?>

<table width="60%">
	<tr>
		<td width="15%">Unit Jahit</td>
		<td>: <select name="kode_unit" id="kode_unit">
				<?php foreach ($list_unit_jahit as $unit) { ?>
					<option value="<?php echo $unit->kode_unit ?>" ><?php echo $unit->kode_unit." - ". $unit->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>

<tr>
		<td style="white-space:nowrap;">Pilih file yang akan diupload *</td>
		<td>:
				<input type="file" name="userfile" id="userfile" size="20" /><br /><br />
						
		</td>
</tr>
<tr>
	<td colspan="2"><i>* Tipe file harus Excel (.xls), dan hanya data saja TANPA JUDUL KOLOM. Jika ada judul kolom, silahkan dihapus dulu <br>
					 * Isi datanya (dari kiri ke kanan) adalah: Tanggal, No SJ, Kode Brg, Nama Brg, Qty, Harga, Total, No Nota, Jumlah<br>
					 * Jika ada data yang sudah pernah diimport sebelumnya, maka tidak akan disimpan ke database</i></td>
</tr>

</table>
<input type="submit" name="btnsubmit" id="btnsubmit" value="Import" onclick="return cek_input();"> <img id="gambarnya" src="<?php echo base_url();?>images/loading.gif" style="display:none">
<?php echo form_close();  ?>
