<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:350px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

</script>

<h3>Data Faktur Pembelian Makloon Hasil Jahit (WIP)</h3><br>
<a href="<?php echo base_url(); ?>index.php/pembelianwip/cform/fakturwipadd">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/pembelianwip/cform/fakturwip">View Data</a>&nbsp;<br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('pembelianwip/cform/fakturwip'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Unit Jahit</td>
		<td>: <select name="cunit" id="cunit">
			<option value="xx" <?php if ($cunit == 'xx') { ?> selected <?php } ?>>- All -</option>
				<?php foreach ($list_unit_jahit as $unit) { ?>
					<option value="<?php echo $unit->kode_unit ?>" <?php if ($unit->kode_unit == $cunit) { ?> selected <?php } ?> ><?php echo $unit->kode_unit." - ".$unit->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Nomor Faktur</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>
<?php 
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;
	
	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
?>
<form id="f_master_brg" name="f_master_brg">
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th rowspan="2">No</th>
		 <th rowspan="2">No Faktur</th>
		 <th rowspan="2">Tgl Faktur</th>
		 <th rowspan="2">Unit Jahit</th>
		 <th colspan="6">List Brg Jadi</th>		 
		 <th rowspan="2">Grand Total</th>
		 <th rowspan="2">Last Update</th>
		 <th rowspan="2">Action</th>
	 </tr>
	 <tr class="judulnya">
		<th>Kode & Nama Brg Jadi</th>
		<th>Qty</th>
		<th>Harga (Rp.)</th>
		<th>Diskon</th>
		<th>Subtotal</th>
		<th>No/Tgl SJ</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
				
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";

				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;

				$pisah1 = explode("-", $query[$j]['tgl_faktur']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td align='right'>".$i."</td>";
				 				 
				 echo    "<td>".$query[$j]['no_faktur']."</td>";
				 echo    "<td>".$tgl_faktur."</td>";
				 
				echo    "<td>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_faktur'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_faktur'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nama_brg_jadi'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_faktur'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_faktur'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_faktur'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_faktur'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_faktur'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_faktur'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['diskon'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right' nowrap>";
				 if (is_array($query[$j]['detail_faktur'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_faktur'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['subtotal'], 2, ',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_faktur'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_faktur'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if($var_detail[$k]['tgl_sj'] != '')
						  echo $var_detail[$k]['no_sj']." / ".$var_detail[$k]['tgl_sj'];
						else
							echo "&nbsp;";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td align='right' nowrap>".number_format($query[$j]['grandtotal'], 2, ',','.')."</td>";
				 echo    "<td align='center'>".$tgl_update."</td>";
				 
					 echo    "<td align=center>";
					 echo "<a href=".base_url()."index.php/pembelianwip/cform/fakturwipedit/".$query[$j]['id']."/".$cur_page."/".$cunit."/".$tgl_awal."/".$tgl_akhir."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>&nbsp;";
					 echo "<a href=".base_url()."index.php/pembelianwip/cform/fakturwipdelete/".$query[$j]['id']."/".$cur_page."/".$cunit."/".$tgl_awal."/".$tgl_akhir."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					 
					 echo "</td>";
				 
				 echo  "</tr>";
				 $i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
