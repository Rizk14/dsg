<h3>Laporan Stok Terkini Bahan Hasil Makloon</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('info-stok-makloon/cform/view'); ?>
Gudang
				<select name="id_gudang" id="id_gudang">
					<option value="0" <?php if ($cgudang == '' || $cgudang == '0') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($gud->id == $cgudang) { ?> selected="true" <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr>
		<th>No</th>
		<th>Kode Brg Makloon</th>
		 <th>Nama Brg Makloon</th>
		 <th>Satuan</th>
		 <th>Stok</th>
		 <th>Last Update Stok</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
				if ($startnya == '')
					$i=1;
				else
					$i = $startnya+1; 
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_update_stok']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update_stok = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg']."</td>";
				 echo    "<td nowrap>".$query[$j]['nama_brg']."</td>";
				 echo    "<td nowrap>".$query[$j]['satuan']."</td>";
				 echo    "<td align='right'>".$query[$j]['stok']."</td>";
				 echo    "<td align='center'>".$tgl_update_stok."</td>";
				 echo  "</tr>";
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
