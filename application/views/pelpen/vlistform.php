
<script type="text/javascript" language="javascript">

$(document).ready(function() {
	$("#tgl_voucher").datepicker();
});

function checked1() {
	var x = document.getElementById('jml');
	var banyak	= x.value;
	
	if(document.getElementById('ckall').checked==true) {
		for (i = 0; i <= parseInt(banyak); i++) {
			document.getElementById('nilai_voucher_tblItem_'+i).disabled	= false;
			document.getElementById('nilai_voucher_tblItem_'+i).value		= 0;
		}
		document.getElementById('f_nilai_manual').value = 't';
	}else{
		for (i = 0; i <= parseInt(banyak); i++) {
			document.getElementById('nilai_voucher_tblItem_'+i).disabled	= true;
			document.getElementById('piutang_tblItem_'+i).value = document.getElementById('piutanghidden_tblItem_'+i).value;
			document.getElementById('nilai_voucher_tblItem_'+i).value		= '';
		}
		document.getElementById('f_nilai_manual').value = 'f';
	}
	
}

function assign_voucher() {
	
	var totalpiutang = parseInt(document.getElementById('totalpiutang').value);
	var total_nilai_voucher	= parseInt(document.getElementById('total_nilai_voucher').value);
	
	if(total_nilai_voucher > totalpiutang) {
		
		alert('Nilai voucher melebihi total piutang!');
		
		document.getElementById('total_nilai_voucher').value = 0;
		document.getElementById('total_nilai_voucher').focus();
	}
}

function hitung(coloum) {
	
	var tblItem = document.getElementById('tblItem');
	var lastRow = tblItem.rows.length;
	var jmlcoloum = parseInt(document.getElementById('jml').value)+1;
	
	var jml_voucher		= document.getElementById('nilai_voucher_tblItem_'+coloum).value;
	var piutang			= document.getElementById('piutanghidden_tblItem_'+coloum).value;
	var sisa_piutang 	= parseInt(piutang)-parseInt(jml_voucher);
	
	var total_voucher	= document.getElementById('total_nilai_voucher').value;
	var jml;
	var sisa;
	var coloum_next = coloum+1;
	
	if(total_voucher!='' || total_voucher!=0) {		
		
		if(coloum_next<jmlcoloum) {
			jml = 0;	
			for(j=0;j<=coloum;j++){		
				var nilai_voucher = parseInt(document.getElementById('nilai_voucher_tblItem_'+j).value);
				if(nilai_voucher=='') {
					nilai_voucher = 0;
				}
				jml = jml+nilai_voucher;
			}
			sisa = parseInt(total_voucher)-parseInt(jml);
					
			document.getElementById('nilai_voucher_tblItem_'+coloum_next).value = sisa;
			if(jml_voucher!='') {	
				document.getElementById('piutang_tblItem_'+coloum).value = sisa_piutang;
				document.getElementById('piutang_tblItem_'+coloum_next).value = document.getElementById('piutanghidden_tblItem_'+coloum_next).value - sisa;
			}else{
				document.getElementById('piutang_tblItem_'+coloum).value = piutang;
			}
			
		}else{
			
			jml = 0;
			
			for(j=0;j<coloum;j++){		
				var nilai_voucher = parseInt(document.getElementById('nilai_voucher_tblItem_'+j).value);
				if(nilai_voucher=='') {
					nilai_voucher = 0;
				}
				jml = jml+nilai_voucher;
			}
			//sisa = parseInt(total_voucher)-parseInt(jml);			

			if(jml_voucher!='') {	
				document.getElementById('piutang_tblItem_'+coloum).value = sisa_piutang;
			}else{
				document.getElementById('piutang_tblItem_'+coloum).value = piutang;
			}
		}
		
	}else{
		alert('Total nilai voucher tdk boleh kosong!');
		document.getElementById('nilai_voucher_tblItem_'+coloum).value = 0;
		document.getElementById('total_nilai_voucher').focus();
	}	
	
}

function ckisi() {
	
	var jmlcoloum = parseInt(document.getElementById('jml').value);
	var total_voucher	= document.getElementById('total_nilai_voucher').value;	
	jml = 0;
	
	if(document.getElementById('ckall').checked==true) {

		for(j=0;j<=jmlcoloum;j++){		
			var nilai_voucher = parseInt(document.getElementById('nilai_voucher_tblItem_'+j).value);
			if(nilai_voucher=='') {
				nilai_voucher = 0;
			}
			jml = jml+nilai_voucher;
		}
			
		if(jml<total_voucher) {
			alert("Jumlah item voucher kurang dari total nilai voucher!");
			return false;
		}

		if(total_voucher<jml) {
			alert("Jumlah item voucher melebihi total nilai voucher yg ada!");
			return false;
		}
	}
	
	if(document.getElementById('kode_sumber').value=='') {
		alert("Kode Sumber hrs diisi !");
		document.getElementById('kode_sumber').focus();
		return false;
	}

	if(document.getElementById('no_voucher').value==''){
		alert("Nomor voucher hrs diisi !");
		document.getElementById('no_voucher').focus();
		return false;
	}

	if(document.getElementById('deskripsi_voucher').value==''){
		alert("Keterangan voucher hrs diisi !");
		document.getElementById('deskripsi_voucher').focus();
		return false;
	}

	if(document.getElementById('deskripsi_voucher').value==''){
		alert("Keterangan voucher hrs diisi !");
		document.getElementById('deskripsi_voucher').focus();
		return false;
	}		
	
	if(document.getElementById('total_nilai_voucher').value==''){
		alert("Total voucher hrs diisi !");
		document.getElementById('total_nilai_voucher').focus();
		return false;
	}

	if(document.getElementById('approve_voucher').value==''){
		alert("Kolom Approve voucher hrs diisi !");
		document.getElementById('approve_voucher').focus();
		return false;
	}
}
	
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_voucher; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_voucher; ?></td></tr>
   <tr>
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
	     <?php 
		 $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('pelpen/cform/simpan', $attributes);?>
		
		
		<div id="masterlvoucherform">
		
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_voucher_kd_sumber; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="kode_sumber" type="text" id="kode_sumber" maxlength="14" value="" onclick="kodesumbervoucher();" readonly />
					  <input name="i_kode_sumber" id="i_kode_sumber" type="hidden" >
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_no; ?></td>
					<td>:</td>
					<td>
					  <input name="no_voucher" type="text" id="no_voucher" maxlength="14" value="" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_tgl; ?></td>
					<td>:</td>
					<td>
					  <input name="tgl_voucher" type="text" id="tgl_voucher" maxlength="10" value="<?php echo date("d/m/Y"); ?>" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_nm_pelanggan; ?></td>
					<td>:</td>
					<td>
					<input name="nama_pelanggan" type="text" id="nama_pelanggan" maxlength="100" value="" onclick="namasumberpelanggan();" readonly />
					  <input name="i_nama_pelanggan" id="i_nama_pelanggan" type="hidden" >

					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_deskripsi; ?></td>
					<td>:</td>
					<td>
					  <input name="deskripsi_voucher" type="text" id="deskripsi_voucher" maxlength="255" value="" />
					</td>
				  </tr>
				  <tr>
					<td><?php echo $list_voucher_total; ?></td>
					<td>:</td>
					<td>
					  <input name="total_nilai_voucher" type="text" id="total_nilai_voucher" maxlength="100" value="" onkeyup="assign_voucher();" />
					</td>
				  </tr>
				  				  
				  <tr>
					<td><?php echo $list_voucher_approved; ?></td>
					<td>:</td>
					<td>
					  <input name="approve_voucher" type="text" id="approve_voucher" maxlength="200" value="<?php echo $approve_voucher[0]['fullname']; ?> " onclick="namasumberapprove();" readonly />&nbsp;
					  <input name="i_approve_voucher" id="i_approve_voucher" type="hidden" value='<?php echo $approve_voucher[0]['i_user_id']; ?>' >
					  <!-- <input type="checkbox" name="c_dept" id="c_dept" value="0" />&nbsp;
					  <input type="checkbox" name="c_dept" id="c_dept" value="0" />
					  -->
					  <input name="app_dept" id="app_dept" type="radio" value="t" />Departement&nbsp;
					  <input name="app_dept" id="app_dept" type="radio" value="f" checked />Services
					</td>
				  </tr>
				</table></td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_voucher; ?></div></td>	
			</tr>
			<tr>
			  <tr>
				<td><table width="70%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="3%" class="tdatahead">NO</td>
					<td width="10%" class="tdatahead"><?php echo $list_voucher_no_faktur; ?></td>
					<td width="10%" class="tdatahead"><?php echo $list_voucher_tgl_faktur; ?> </td>
					<td width="15%" class="tdatahead"><?php echo $list_voucher_total_faktur; ?></td>
					<td width="15%" class="tdatahead"><?php echo $list_voucher_piutang; ?></td>
					<td width="16%" class="tdatahead"><input type="checkbox" name="ckall" id="ckall" onclick="checked1();"/>&nbsp;<?php echo $list_voucher_manual_input; ?> 
					&nbsp;<?php echo $list_voucher_nilai_voucher_detail; ?> <span style="color:#FF0000">*</span>
					</td>
				  </tr>
<!--
				  <tr>
					<td colspan="6">
						<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
-->
						<?php
						if(isset($query) && sizeof($query)>0) {
							echo $query;
						}
						?>
<!--
						</table>				  
					</td>	
				  </tr>
-->
				  <tr>
					<td colspan="6">&nbsp;</td>
				  </tr>
				  <tr>
				    <td colspan="3" align="right"><?php echo strtoupper($list_voucher_total_nilai); ?> :</td>
				    <td align="right"><input type="text" ID="total_faktur" name="total_faktur" size="20px;" style="text-align:right;" readonly value="<?php echo number_format($totalfaktur,'2','.',','); ?>" ></td>
				    <td align="right"><input type="text" ID="piutang" name="piutang" size="20px;" style="text-align:right;" readonly value="<?php echo number_format($totalpiutang,'2','.',','); ?>" ></td>
				    <td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:295px; padding:5px;"><span style="color:#FF0000">*</span>) Klik, jika penginputan nilai pembayarannya manual.</div></td>
			  </tr>			  
			  <tr>
				<td align="right">
				  <input name="f_nilai_manual" id="f_nilai_manual" type="hidden" value="f" />	
				  <input name="btnsimpan" id="btnsimpan" value="<?php echo $button_simpan; ?>" type="submit" onclick="return ckisi();" />&nbsp;	
				  <input name="btnkeluar" type="button" id="btnkeluar" value="<?php echo $button_keluar; ?>" onclick="show('pelpen/cform','#content')" />
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
