<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function settextfield(a,b,c) {
	
	opener.document.getElementById('no_faktur').value=a;
	opener.document.getElementById('i_faktur').value=b;
	opener.document.getElementById('f_nota_sederhana').value=c;
	
	this.close();
}

function parsi() {
	
	var x = document.getElementById('iteration');
	var banyak	= x.value;
	var iter;
	var iter2;
	var iter3;
	var iter_external ;
	var iter_external_trim;
	var i_faktur_external;
	var i_faktur_trim;
	var f_nota_sederhana_external;
	
	opener.document.getElementById('no_faktur').value		= '';
	opener.document.getElementById('i_faktur').value		= '';
	opener.document.getElementById('f_nota_sederhana').value	= '';
	
	for(i = 1; i <= parseInt(banyak); i++) {
		if(document.getElementById('f_ck_tblItem'+i).checked==true) {			
			iter_external		= opener.document.getElementById('no_faktur').value;
			iter_external_trim 	= iter_external.trim();
			i_faktur_external	= opener.document.getElementById('i_faktur').value;
			i_faktur_trim 		= i_faktur_external.trim();
			f_nota_sederhana_external	= opener.document.getElementById('f_nota_sederhana').value;
			
			iter  = document.getElementById('f_ck_tblItem'+i).value;
			iter2 = document.getElementById('i_faktur'+i).value;
			iter3 = document.getElementById('f_nota_sederhana'+i).value;
			
			if(iter_external_trim=='') {
				opener.document.getElementById('no_faktur').value = iter.trim();
				opener.document.getElementById('i_faktur').value = iter2.trim();
				opener.document.getElementById('f_nota_sederhana').value = iter3.trim();
			} else {
				opener.document.getElementById('no_faktur').value = iter_external_trim+'#'+iter.trim();
				opener.document.getElementById('i_faktur').value = i_faktur_trim+'#'+iter2.trim();
				opener.document.getElementById('f_nota_sederhana').value = f_nota_sederhana_external+'#'+iter3.trim();
			}
		}
	}
	
	if(i==banyak)
		this.close();
		
}

function ck(ck){
	if(ck.checked==true){
	}else{
	}
}

function getInfo(txt){

$.ajax({

type: "POST",
url: "<?php echo site_url('pelpen/cform/flistfaktur'); ?>",
data:"key="+txt,
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};

function carifocus() {
	document.getElementById('txtcari').focus();
}

function klose(){
	this.close();
}

function checked1(){
	var x = document.getElementById('iteration');

	var banyak	= x.value;
	
	if(document.getElementById('ckall').checked==true){
		for (i = 1; i <= parseInt(banyak); i++){
			document.getElementById('f_ck_tblItem'+i).checked = true;
		}
	}else{
		for (i = 1; i <= parseInt(banyak); i++){
			document.getElementById('f_ck_tblItem'+i).checked = false;
		}
	}
}

</script>

</head>
<body id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h3>$page_title</h3></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
  </tr>  
  <tr>
  	<td align="center"><input type="button" name="btnkirim" id="btnkirim" value="AMBIL FAKTUR" onclick="parsi();">&nbsp;
  	<input type="button" name="btnclose" id="btnclose" value="KELUAR" onclick="klose();"></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
  </tr>  
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="getInfo(this.value);"/>
  	</td>
  </tr>
  <tr>
    <td align="left">
	<?php echo form_open('pelpen/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php
		if( !empty($isi) || isset($isi) ) {
			$cc	= 1; 
			if(count($isi)) {
				foreach($isi as $row) {
					
					$f_nota_sederhana = ($row->f_nota_sederhana=='t')?'Faktur Non DO':'Faktur DO';
					
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$row->i_dt_code."</a></td>
					  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$row->d_dt."</a></td>
					  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$row->v_total."</a></td>
					  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$row->v_sisa."</a></td>
					  
					  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt','$row->f_nota_sederhana')\">".$f_nota_sederhana."</a></td>
					  <td>
					  <input type=\"hidden\" name=\"i_faktur".$cc."\" id=\"i_faktur".$cc."\" value=\"".$row->i_dt."\">
					  <input type=\"hidden\" name=\"f_nota_sederhana".$cc."\" id=\"f_nota_sederhana".$cc."\" value=\"".$row->f_nota_sederhana."\">
					  <input type=\"checkbox\" ID=\"f_ck_tblItem".$cc."\" name=\"f_ck_tblItem".$cc."\" value=\"".$row->i_dt_code."\" onclick=\"ck(document.getElementById('f_ck_tblItem'+$cc));\"></td>
					 </tr>";
					 
					 $cc+=1;
				}
				echo " <input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$cc."\"> ";
			}
		}
		?>
		
		<table class="listtable2">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="90px;"><?php echo strtoupper("No. Faktur"); ?></th>
		 <th><?php echo strtoupper("Tgl. Faktur"); ?></th>
		 <th><?php echo strtoupper("Jumlah Tagihan"); ?></th>
		 <th><?php echo strtoupper("Sisa Tagihan"); ?></th>
		 <th><?php echo strtoupper("Keterangan"); ?></th>
		 <th><?php echo strtoupper("Pilih"); ?><input type="checkbox" name="ckall" id="ckall" onclick="checked1();"/></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>"; ?>
  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
