<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var id_brg=$("#id_brg").val();
		var des=$("#des").val();
		var qty=$("#qty").val();
		var satuan=$("#satuan").val();
		// 29-10-2014
		var nama_satuan_konv=$("#nama_satuan_konv").val();
		
		// 06-07-2015
		var jumdata=$("#jumdata").val();
		for(var i=1; i <= jumdata; i++){
			var stringnya = opener.document.getElementById('id_brg_'+i).value;
			if (id_brg == stringnya) {
				alert("Kode barang sudah pernah dipilih...!");
				return false;
			}
		}
		
		var id_so=$("#id_so").val();
		
		$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/stok-opname/cform/cekadabrg', 
				data: 'id_so='+id_so+'&id_brg='+id_brg, success: function(response) {
					//alert(response);
					if (response == "ada") {
						alert("Kode barang sudah pernah diinput...!");
						return false;
					}
					
					opener.document.forms["f_opname"].id_brg_<?php echo $posisi ?>.value=id_brg;
					opener.document.forms["f_opname"].kode_brg_<?php echo $posisi ?>.value=idx;
					opener.document.forms["f_opname"].nama_<?php echo $posisi ?>.value=des;
					opener.document.forms["f_opname"].satuan_<?php echo $posisi ?>.value=satuan;
					opener.document.forms["f_opname"].satuan_konv_<?php echo $posisi ?>.value=nama_satuan_konv;
					self.close();
			}});
		
	});
});
</script>
<center>

<h3>Daftar Barang Di <?php echo $nama_gudang ?></h3>

</center>
<div align="center"><br>
<?php echo form_open('stok-opname/cform/show_popup_brg'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="id_gudang" value="<?php echo $id_gudang ?>">
<input type="hidden" name="id_so" id="id_so" value="<?php echo $id_so ?>">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="id_brg" id="id_brg">
<input type="hidden" name="des" id="des">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="nama_satuan_konv" id="nama_satuan_konv">
<input type="hidden" name="stok" id="stok">
<input type="hidden" name="jumdata" id="jumdata" value="<?php echo $jumdata ?>">

</form>

  <table border="1" align="center" cellpadding="1" width="100%" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Satuan<br>Konversi</th>
      <th bgcolor="#999999">Action</th>
    </tr>
	<?php 
		//$i=1; 
		
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
				
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg']; ?></td>
      <td nowrap><?php echo $query[$j]['nama_brg']; ?></td>
      <td><?php echo $query[$j]['satuan']; ?></td>
      
      <td><?php if ($query[$j]['id_satuan_konversi'] == '0') { $nama_satuan_konversi = 'Tidak Ada'; echo "Tidak Ada"; } else {
				$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '".$query[$j]['id_satuan_konversi']."' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan_konversi	= $hasilrow->nama;
				}
				else
					$nama_satuan_konversi = '';
				
				echo $nama_satuan_konversi;
		}
		   ?></td>
      
      <td align="center">
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
		window.document.f_master_brg.id_brg.value='<?php echo $query[$j]['id_brg'] ?>';
	  window.document.f_master_brg.des.value='<?php 
	  $pos = strpos($query[$j]['nama_brg'], "\"");
	  if ($pos > 0)
		echo str_replace("\"", "&quot;", $query[$j]['nama_brg']);
	  else
		echo str_replace("'", "\'", $query[$j]['nama_brg']);
		?>'; 
	  
	  window.document.f_master_brg.satuan.value='<?php echo $query[$j]['satuan'] ?>'; 
	  window.document.f_master_brg.nama_satuan_konv.value='<?php echo $nama_satuan_konversi ?>';
	  ">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
