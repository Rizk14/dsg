<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Stok Opname Bahan Baku/Pembantu</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var jum_data = $('#jum_data').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data SO ??");
	
	if (kon) {
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
				if($('#stok_fisik_'+k).val() == '') {				
					alert("Data stok tidak boleh kosong, jika kosong maka harus isikan angka 0...!");
					s=1;
					return false;
				}
				if (isNaN($('#stok_fisik_'+k).val()) ) {
					alert("Data stok harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}
</script>

<div>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<i>Keterangan: untuk satuan awal Lusin, masukkan data dalam satuan Pieces. untuk satuan awal Yard, masukkan data dalam satuan Meter</i><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('stok-opname/cform/submit', $attributes);
$no = count($query);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="gudang" id="gudang" value="<?php echo $gudang ?>">
<input type="hidden" name="jum_data" id="jum_data" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang</th>
		 <th>Satuan Awal</th>
	<!--	<th>Jumlah Stok</th> -->
	<!--	<th>Konversi Ke</th> -->
		 <th>Jumlah Fisik</th>
		 <?php if ($is_new != 1) { ?>
		 <th>Hapus Item</th>
		 <?php } ?>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg']."
					 <input type='hidden' name='kode_$i' id='kode_$i' value='".$query[$j]['kode_brg']."'></td>";
					 echo    "<td>".$query[$j]['nama_brg']."</td>";
					 echo    "<td>".$query[$j]['satuan']."</td>";
				//	 echo    "<td align='right'>".$query[$j]['stok']."</td>";
			/*		if ($query[$j]['id_satuan'] == 2) // yard
						echo "<td>Meter</td>";
					else if ($query[$j]['id_satuan'] == 7) // meter
						echo "<td>Pieces</td>";
					else
						echo "<td>&nbsp;</td>"; */
					 echo    "<td align='center'><input type='text' name='stok_fisik_$i' id='stok_fisik_$i' value='".$query[$j]['stok_opname']."' size='10' style='text-align: right;'>
					 
					 <input type='hidden' name='stok_$i' id='stok_$i' value='".$query[$j]['stok']."'>
					 </td>";
					 if ($is_new != 1) {
						 echo "<td align='center'>";
						 echo "<input name='cek_$i' id='cek_$i' type='checkbox' value='y' />";
						 echo "</td>";
					 }
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>
<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Stok Opname Periode Ini" onclick="return confirm('Yakin akan hapus SO periode ini ??')">&nbsp;
<input type="submit" name="submit3" value="Hapus Item SO Yang Dipilih" onclick="return confirm('Yakin akan hapus item SO ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/stok-opname/cform'">
<?php echo form_close();  ?>
</div>
