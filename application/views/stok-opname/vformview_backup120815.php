<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Stok Opname Bahan Baku/Pembantu</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var jum_data = $('#jum_data').val();
	var tgl_so = $('#tgl_so').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data SO ??");
	
	if (kon) {
		if(tgl_so == '') {				
			alert("Tanggal SO harus dipilih...!");
			s=1;
			return false;
		}
		
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
				if($('#stok_fisik_'+k).val() == '') {				
					alert("Data stok tidak boleh kosong, jika kosong maka harus isikan angka 0...!");
					s=1;
					return false;
				}
				if (isNaN($('#stok_fisik_'+k).val()) ) {
					alert("Data stok harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}
</script>

<div>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<!--<i>Keterangan: untuk satuan awal Lusin, masukkan data dalam satuan Pieces. untuk satuan awal Yard, masukkan data dalam satuan Meter</i><br><br>-->
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('stok-opname/cform/submit', $attributes);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="gudang" id="gudang" value="<?php echo $gudang ?>">
<input type="hidden" name="jum_data" id="jum_data" value="<?php echo $jum_total ?>">
<input type="hidden" name="is_pertamakali" id="is_pertamakali" value="">

Tanggal Pencatatan SO: 
      <input name="tgl_so" type="text" id="tgl_so" size="10" value="<?php echo $tgl_so ?>" readonly="true">
	   <img alt="" id="tgl_so" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_so,'dd-mm-yyyy',this)">
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang</th>
		 <th>Satuan Awal</th>
		 <th>Satuan Konversi</th>
		 <th>Jumlah Fisik Satuan Konversi</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg']."
					 <input type='hidden' name='id_brg_$i' id='id_brg_$i' value='".$query[$j]['id_brg']."'>
					 <input type='hidden' name='brgbaru_$i' id='brgbaru_$i' value='0'>
					 </td>";
					 echo    "<td>".$query[$j]['nama_brg']."</td>";
					 echo    "<td>".$query[$j]['nama_satuan']."</td>";
					 echo    "<td>".$query[$j]['nama_satuan_konv']."</td>";
					 
					 echo "<input type='hidden' name='auto_saldo_akhir_$i' id='auto_saldo_akhir_$i' value='".$query[$j]['auto_saldo_akhir']."'>";
					 
/*					 echo "<td style='white-space:nowrap;' align='right'>";
				if (is_array($query[$j]['detail_harga'])) {
					$detailharga = $query[$j]['detail_harga'];
						for($zz=0;$zz<count($detailharga);$zz++){ ?>
						<span style="white-space:nowrap;">
						<?php echo number_format($detailharga[$zz]['harga'],2,',','.')."&nbsp;"; */
				?>
			<!--	<input type="text" name="stok_fisik_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['stok_opname'] ?>" size="3" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
				<input type="hidden" name="harga_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['harga'] ?>">
				<input type="hidden" name="stok_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['saldo_akhir'] ?>">
				<input type="hidden" name="auto_saldo_akhir_harga_<?php echo $i ?>[]" value="<?php echo $detailharga[$zz]['auto_saldo_akhir_harga'] ?>"><br> -->
				<?php 
					//	}
				?>
				<!--	<input type="hidden" name="ada_harga_<?php echo $i ?>" id="ada_harga_<?php echo $i ?>" value="ada"> -->
				<?php
				//}
				//else {
				?>
				<!--	<input type="hidden" name="ada_harga_<?php echo $i ?>" id="ada_harga_<?php echo $i ?>" value=""> -->
				<?php //} ?>
				</td>
					 
				<?php	echo    "<td align='center'><input type='text' name='stok_fisik_$i' id='stok_fisik_$i' value='".$query[$j]['stok_opname']."' size='5' style='text-align: right;'>
					 
					 <input type='hidden' name='stok_$i' id='stok_$i' value='".$query[$j]['saldo_akhir']."'>
					 </td>";

					 echo  "</tr>";
					 $i++;
				}
			}
			
			if ($is_new != 1) {
				// utk brg baru yg blm ada di SO
				if (is_array($querybrgbaru)) {
					for($j=0;$j<count($querybrgbaru);$j++){
						 echo "<tr class=\"record\">";
						 echo "<td align='center'>$i</td>";
						 echo    "<td>".$querybrgbaru[$j]['kode_brg']." <b>*</b> 
						 <input type='hidden' name='id_brg_$i' id='id_brg_$i' value='".$querybrgbaru[$j]['id_brg']."'>
						 <input type='hidden' name='brgbaru_$i' id='brgbaru_$i' value='1'>
						 </td>";
						 echo    "<td>".$querybrgbaru[$j]['nama_brg']."</td>";
						 echo    "<td>".$querybrgbaru[$j]['satuan']."</td>";
						 echo    "<td>".$querybrgbaru[$j]['nama_satuan_konv']."</td>";
						 echo "<td style='white-space:nowrap;' align='center'>";
						?>
						<input type="text" name="stok_fisik_<?php echo $i ?>" id="stok_fisik_<?php echo $i ?>" value="<?php echo $querybrgbaru[$j]['stok_opname'] ?>" size="5" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
						<input type="hidden" name="stok_<?php echo $i ?>" value="<?php echo $querybrgbaru[$j]['saldo_akhir'] ?>">
						<?php
						 echo "</td>";
						
						?>
						 
						 <?php
						 echo  "</tr>";
						 $i++;
					}
				}
			}
		 ?>
		
 	</tbody>
</table><br><?php if ($is_new != 1) { ?>Keterangan: <i>*) Barang baru yang belum ada di stok opname</i><br><br><?php } ?>
<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Stok Opname Periode Ini" onclick="return confirm('Yakin akan hapus SO periode ini ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/stok-opname/cform'">
<?php echo form_close();  ?>
</div>
