<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Stok Opname Bahan Baku/Pembantu</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">
 $(function() {
	
	$("#no").val('2');
	//generate_nomor();		
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
								
		//*****kode_brg*************************************
		var kode_brg="#kode_brg_"+n;
		var new_kode_brg="#kode_brg_"+no;
		$(kode_brg, lastRow).attr("id", "kode_brg_"+no);
		$(new_kode_brg, lastRow).attr("name", "kode_brg_"+no);		
		$(new_kode_brg, lastRow).val('');		
		//*****end kode_brg*********************************
		
		//*****id_brg*************************************
		var id_brg="#id_brg_"+n;
		var new_id_brg="#id_brg_"+no;
		$(id_brg, lastRow).attr("id", "id_brg_"+no);
		$(new_id_brg, lastRow).attr("name", "id_brg_"+no);		
		$(new_id_brg, lastRow).val('');		
		//*****end id_brg*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');
		//*****end satuan*************************************	
				
		//*****satuan_konv*************************************
		var satuan_konv="#satuan_konv_"+n;
		var new_satuan_konv="#satuan_konv_"+no;
		$(satuan_konv, lastRow).attr("id", "satuan_konv_"+no);
		$(new_satuan_konv, lastRow).attr("name", "satuan_konv_"+no);	
		$(new_satuan_konv, lastRow).val('');
		//*****end satuan_konv*************************************	
		
		//*****stok_fisik*************************************
		var stok_fisik="#stok_fisik_"+n;
		var new_stok_fisik="#stok_fisik_"+no;
		$(stok_fisik, lastRow).attr("id", "stok_fisik_"+no);
		$(new_stok_fisik, lastRow).attr("name", "stok_fisik_"+no);	
		$(new_stok_fisik, lastRow).val('0');
		//*****end stok_fisik*************************************	
		
		//*****stok*************************************
		var stok="#stok_"+n;
		var new_stok="#stok_"+no;
		$(stok, lastRow).attr("id", "stok_"+no);
		$(new_stok, lastRow).attr("name", "stok_"+no);	
		$(new_stok, lastRow).val('0');
		//*****end stok*************************************	
										
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
            
		 var  even_klik= "var x= $('#gudang').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/stok-opname/cform/show_popup_brg/'+ x+'/<?php echo $id_so ?>/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
	    $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});

function cek_input() {
	var jum_data = $('#no').val();
	var tgl_so = $('#tgl_so').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data SO ??");
	
	if (kon) {
		if(tgl_so == '') {				
			alert("Tanggal SO harus dipilih...!");
			s=1;
			return false;
		}
		
			for (var k=1; k <= jum_data-1; k++) {
				if($('#id_brg_'+k).val() == '') {				
					alert("Data barang harus dipilih...!");
					s=1;
					return false;
				}
				if($('#stok_fisik_'+k).val() == '') {				
					alert("Data stok tidak boleh kosong, jika kosong maka harus isikan angka 0...!");
					s=1;
					return false;
				}
				if (isNaN($('#stok_fisik_'+k).val()) ) {
					alert("Data stok harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
			}
		if (s == 0)
			return true;
	}
	else
		return false;
	
}

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
</script>

<div>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang." - ".$nama_gudang." (<i>* Belum ada data stok opname sama sekali</i>)"; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
<!--<i>Keterangan: untuk satuan awal Lusin, masukkan data dalam satuan Pieces. untuk satuan awal Yard, masukkan data dalam satuan Meter</i><br><br>-->
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('stok-opname/cform/submit', $attributes);
 ?>
<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="jum_data" id="jum_data" value="0">
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="gudang" id="gudang" value="<?php echo $gudang ?>">

Tanggal Pencatatan SO: 
      <input name="tgl_so" type="text" id="tgl_so" size="10" value="<?php echo $tgl_so ?>" readonly="true">
	   <img alt="" id="tgl_so" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_so,'dd-mm-yyyy',this)">
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="80%" id="tabelku">
	<tr>
			<td colspan="6" align="right">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
	</tr>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Barang</th>
		 <th>Nama Barang</th>
		 <th>Satuan Awal</th>
		 <th>Satuan Konversi</th>
		 <th>Jumlah Fisik Satuan Konversi</th>
	 </tr>
	<tr align="center">
          <td align="center" id="num_1">1</td>
          <td style="white-space:nowrap;">
          <input name="kode_brg_1" type="text" id="kode_brg_1" size="10" readonly="true" value="" />
           <input name="id_brg_1" type="hidden" id="id_brg_1" value=""/>
           <input title="browse data barang" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#gudang').val();
            openCenteredWindow('<?php echo base_url(); ?>index.php/stok-opname/cform/show_popup_brg/'+ x+'/<?php echo $id_so ?>/1');" >
           </td>
			<td><input name="nama_1" type="text" id="nama_1" size="40" readonly="true" value="" /></td>
			<td><input name="satuan_1" type="text" id="satuan_1" size="5" readonly="true" value="" /></td>
			<td><input name="satuan_konv_1" type="text" id="satuan_konv_1" size="5" readonly="true" value="" /></td>
			<td align="center"><input type="text" name="stok_fisik_1" id="stok_fisik_1" value='0' size='7' style="text-align: right;">
			<input name="stok_1" type="hidden" id="stok_1" value="0"/>
			</td>
    </tr>
</table><br>
<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return cek_input();">&nbsp;
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/stok-opname/cform'">
<?php echo form_close();  ?>
</div>
