

<?php
// Nomor urut data di tabel.
$per_page = 10;

// Posisi nomor halaman (untuk paging) jika user login / tidak.
$login_status = $this->session->userdata('login_status');
$user_bagian = $this->session->userdata('user_bagian');

if ($login_status ==  true && $user_bagian == '2') {
    $page = $this->uri->segment(4);
} else {
    $page = $this->uri->segment(3);
}


if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}

?>

<div class="container">

    <h2>First Data Stock Opname Jahit periode <?php echo $bulan; ?> - <?php echo $tahun;  ?> 
    <?php //echo $query[0]['nama_unit_jahit']; ?>
    </h2>
    <hr>
    <div class="row">
		
        
    <?php if (!empty($query) && is_array($query)): ?>
   
        <div class="col-md-12">
			<form action ="submit" name="myform" id='myform' class="form-inline" method="post">
				<?php echo form_label('Tanggal SO ', 'tanggal_so', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback <?php set_validation_style('tanggal_so')?>">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_so', date_to_id($values->tanggal_so), 'id="tanggal_so" class="form-control" placeholder="Tanggal SO" maxlength="10"') ?>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        
		<div class="form-group form-group-sm">
        <?php echo form_label('Jenis Hitung ', 'tanggal_so', array('class' => 'control-label ')) ?>
        <select name="jenis_hitung" id="jenis_hitung" class="form-control">
		<option value="1">1. Sudah menghitung barang masuk dan barang keluar</option>
		<option value="2">2. Sudah menghitung barang masuk, barang keluar belum</option>
		<option value="3">3. Belum menghitung barang masuk, barang keluar sudah</option>
		<option value="4">4. Belum menghitung barang masuk dan barang keluar</option>
		</select>
		</div>
		</div>
		<div class="row">
        <div class="col-xs-12 col-md-4 pull-right">
			<br>
		<?php echo form_button(array('id'=>'addRowx','content'=>'+', 'type'=>'button', 'class'=>'btn btn-primary'  )) ?>
		<?php echo form_button(array( 'id'=>"deleteRow",'content'=>'-', 'type'=>'button', 'class'=>'btn btn-primary')) ?>
         </div>
         </div>
		<input type='hidden' id='id_unit_jahit' name='id_unit_jahit' value =  '<?php echo $query[0]['id_unit_jahit']; ?>'>
		<input type='hidden' id='bulan' name='bulan' value =  '<?php echo $bulan ?>'>
		<input type='hidden' id='tahun' name='tahun' value =  '<?php echo $tahun ?>'>
		<input type='hidden' id='no' name='no' >
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Barang BB</th>
                    <th>Nama Barang BB</th>
                    <th>Qty</th>
                </tr>
                </thead>
                <tbody>		  
				<br>
                <?php 


                for($i=0;$i<count($query);$i++){
					 $data_detail=$query[0]['data_detail'];

			    for($j=0;$j<count($data_detail);$j++){
                 ?>
                    <tr>
                        <td><?php echo ++$offset ?></td>
                        <input type='hidden' value='<?php echo $data_detail[$j]['id_barang_bb'] ?>' id='id_barang_bb[]' name='id_barang_bb[]' >
                        <input type='hidden' value='<?php echo $data_detail[$j]['kode_barang_bb'] ?>' id='kode_barang_bb[]' name='kode_barang_bb[]' >
						<td><?php echo $data_detail[$j]['kode_barang_bb'] ?></td>
						<td><?php echo $data_detail[$j]['nama_barang_bb'] ?></td>
						<td>  <input type='text' value='' id='qty[]' name='qty[]' placeholder='Masukkan Qty' class='form-control'> </td>  
                    </tr>
                    
                <?php 
					}
					?>
					
					<?php
                } ?>
                  
				</tbody>
           	</table>
       	
				 <table id="tabelku" class="table table-striped table-bordered table-hover table-condensed">
				
					<tr >
		
					<input type='hidden' id='no' name='no' >
					          <td align="center" id="num_1">1</td>
					<td>
				<input type="hidden" name="id_barang_bb_1[]" id="id_barang_bb_1" value="">
                <input id="kode_barang_bb_1" class="form-control input-group-lg" type="text" name="kode_barang_bb_1"
                       title="Masukkan Kode Barang BB" value=""
                       placeholder="Kode Barang BB" onclick= "openCenteredWindows('../item_makloon/barang_bb/1');"
                       />
				</td>
				<td>
                <input id="nama_barang_bb_1" class="form-control input-group-lg" type="text" name="nama_barang_bb_1"
                       title="Masukkan Nama Barang BB" value=""
                       placeholder="Nama Barang BB"/ disabled>
				</td>
				<td>
                <input id="qty_1" class="form-control input-group-lg" type="text" name="qty_1[]"
                       title="Masukkan Quantity" value=""
                       placeholder="Quantity"/>
             </td>  
            	
                     </tr>
                     </table>   
                      
					</div>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					 <?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-success', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
				</div>
      
			
			</form>
			
		  </div> 
		   
					
	</div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $query ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



