

<?php
// Nomor urut data di tabel.
$per_page = 10;

// Posisi nomor halaman (untuk paging) jika user login / tidak.
$login_status = $this->session->userdata('login_status');
$user_bagian = $this->session->userdata('user_bagian');

if ($login_status ==  true && $user_bagian == '2') {
    $page = $this->uri->segment(4);
} else {
    $page = $this->uri->segment(3);
}


if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Data Stok Opname</h2>
    <hr>

    <!-- Paging dan form pencarian -->
    <div class="row navigasi_cari">
        <!-- Paging -->
        <div class="col-xs-12 col-md-6">
            <?php echo (!empty($paging)) ? $paging : ''?>
        </div>
        <!-- /Paging -->

        <!-- Form Pencarian -->
        <div class="col-xs-12 col-md-4 pull-right">
            <form method="get" action="<?php echo $form_action;?>" role="form" class="form-horizontal">
                <div class="input-group">
                    <input type="text" name="kata_kunci" class="form-control" placeholder="Masukkan Bulan atau Tahun pencarian" id="kata_kunci" value="<?php echo $this->input->get('kata_kunci')?>">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /Form Pencarian -->
    </div>
    <!-- /Paging dan form pencarian -->

    <?php if (!empty($stokopname_unit) && is_array($stokopname_unit)): 

    ?>
    
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal SO</th>
                    <th>Tahun SO</th>
                    <th>Bulan SO</th>
                    <th>Nama Gudang Wip</th>
                    <th>Nama Gudang BB</th>
                    <th>Nama Unit Jahit</th>
                    <th>Nama Unit Packing</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
              
                for($i=0 ;$i < count($stokopname_unit);$i++) {
				echo form_hidden('id', $stokopname_unit[$i]['id']) ;
				
                 $link_edit = anchor('dashboard/stokopname_unit/edit/'.$stokopname_unit[$i]['id'].'/'.$stokopname_unit[$i]['id_gudang_wip'].'/'.$stokopname_unit[$i]['id_gudang_bb'].'/'.$stokopname_unit[$i]['id_unit_jahit'].'/'.$stokopname_unit[$i]['id_unit_packing'], '<span class="glyphicon glyphicon-edit"></span>', array('title' => 'Edit'));
                 $link_hapus = anchor('dashboard/stokopname_unit/hapus/'.$stokopname_unit[$i]['id'].'/'.$stokopname_unit[$i]['id_gudang_wip'].'/'.$stokopname_unit[$i]['id_gudang_bb'].'/'.$stokopname_unit[$i]['id_unit_jahit'].'/'.$stokopname_unit[$i]['id_unit_packing'],'<span class="glyphicon glyphicon-trash"></span>', array('title' => 'Hapus', 'data-confirm' => 'Anda yakin akan menghapus data ini?'));
                 $link_cetak = anchor('dashboard/stokopname_unit/cetak/'.$stokopname_unit[$i]['id'],'<span class="glyphicon glyphicon-print"></span>', array('title' => 'Cetak'));
                 ?>
                    <tr>
						
                        <td><?php echo ++$offset ?></td>
						
						<td><?php echo $stokopname_unit[$i]['tanggal_so'] ?></td>
						<td><?php echo $stokopname_unit[$i]['tahun'] ?></td>
						<td><?php echo $stokopname_unit[$i]['bulan'] ?></td>
						<input type='hidden' id='id_gudang_wip' name='id_gudang_wip' value='<?php echo $stokopname_unit[$i]['id_gudang_wip'] ; ?>'>
						<td><?php echo $stokopname_unit[$i]['nama_gudang_wip'] ?></td>
						<input type='hidden' id='id_gudang_bb' name='id_gudang_bb' value='<?php echo $stokopname_unit[$i]['id_gudang_bb'] ; ?>'>
						<td><?php echo $stokopname_unit[$i]['nama_gudang_bb'] ?></td>
						<input type='hidden' id='id_unit_jahit' name='id_unit_jahit' value='<?php echo $stokopname_unit[$i]['id_unit_jahit'] ; ?>'>
						<td><?php echo $stokopname_unit[$i]['nama_unit_jahit'] ?></td>
						<input type='hidden' id='id_unit_packing' name='id_unit_packing' value='<?php echo $stokopname_unit[$i]['id_unit_packing'] ; ?>'>
						<td><?php echo $stokopname_unit[$i]['nama_unit_packing'] ?></td>
                        <td>
                            <?php echo $user_bagian == '2' ? $link_edit.'&nbsp;&nbsp;&nbsp;&nbsp;'.$link_hapus : "" ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
	
	<div class="col-sm-offset-5">
    
                    <div class="btn-group" >
                     <?php echo anchor('/dashboard/stokopname_unit/', 'Tambah Item', 'class="btn btn-warning " role="button" '); ?>
                    </div>
           
        </div>
	
        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $stokopname_unit ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



