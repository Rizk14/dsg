

<?php
// Nomor urut data di tabel.
$per_page = 10;

// Posisi nomor halaman (untuk paging) jika user login / tidak.
$login_status = $this->session->userdata('login_status');
$user_bagian = $this->session->userdata('user_bagian');

if ($login_status ==  true && $user_bagian == '2') {
    $page = $this->uri->segment(9);
} else {
    $page = $this->uri->segment(10);
}


if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}

?>

<div class="container">

    <h2>Edit Data Stock Opname Gudang WIP
    <?php //echo $query[0]['nama_gudang']; ?>
    </h2>
    <hr>
    <div class="row">
		
        
    <?php if (!empty($query) && is_array($query)): ?>
   
        <div class="col-md-12">
			<form action ="<?php echo base_url('index.php/dashboard/stokopname_unit/update_data') ?> " name="myform" id='myform' class="form-inline" method="post">
				<?php echo form_label('Tanggal SO ', 'tanggal_so', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback <?php set_validation_style('tanggal_so')?>">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_so', $query[0]['tanggal_so'], 'id="tanggal_so" class="form-control" placeholder="Tanggal SO" maxlength="10"') ?>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        
		<div class="form-group form-group-sm">
        <?php echo form_label('Jenis Hitung ', 'jenis_hitung', array('class' => 'control-label ')) ?>
        <select name="jenis_hitung" id="jenis_hitung" class="form-control">
	<option value="1" <?php if ($query[0]['jenis_hitung'] == '1') { ?>selected<?php } ?>>1. Sudah menghitung barang masuk dan barang keluar</option>
	<option value="2" <?php if ($query[0]['jenis_hitung'] == '2') { ?>selected<?php } ?>>2. Sudah menghitung barang masuk, barang keluar belum</option>
	<option value="3" <?php if ($query[0]['jenis_hitung'] == '3') { ?>selected<?php } ?>>3. Belum menghitung barang masuk, barang keluar sudah</option>
	<option value="4" <?php if ($query[0]['jenis_hitung'] == '4') { ?>selected<?php } ?>>4. Belum menghitung barang masuk dan barang keluar</option>
		</select>
		</div>
		</div>
		<div class="row">
        <div class="col-xs-12 col-md-4 pull-right">
			<br>
		<?php echo form_button(array('id'=>'addRowx','content'=>'+', 'type'=>'button', 'class'=>'btn btn-primary'  )) ?>
		<?php echo form_button(array( 'id'=>"deleteRow",'content'=>'-', 'type'=>'button', 'class'=>'btn btn-primary')) ?>
         </div>
         </div>
        <input type='hidden' id='id_so' name='id_so' value =  '<?php echo $query[0]['id_so']; ?>'>
		<input type='hidden' id='id_gudang' name='id_gudang' value =  '<?php echo $query[0]['id_gudang']; ?>'>
	
		<input type='hidden' id='no' name='no' >
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Barang WIP</th>
                    <th>Nama Barang WIP</th>
                    <th>Qty</th>
                </tr>
                </thead>
                <tbody>		  
				<br>
                <?php 

                for($i=0;$i<count($query);$i++){
					 $data_detail=$query[0]['data_detail'];

			    for($j=0;$j<count($data_detail);$j++){
                 ?>
                    <tr>
                        <td><?php echo ++$offset ?></td>
                         <input type='hidden' value='<?php echo $data_detail[$j]['id_detail_so'] ?>' id='id_detail_so[]' name='id_detail_so[]' >
                        <input type='hidden' value='<?php echo $data_detail[$j]['id_barang_wip'] ?>' id='id_barang_wip[]' name='id_barang_wip[]' >
                        <input type='hidden' value='<?php echo $data_detail[$j]['kode_barang_wip'] ?>' id='kode_barang_wip[]' name='kode_barang_wip[]' >
						<td><?php echo $data_detail[$j]['kode_barang_wip'] ?></td>
						<td><?php echo $data_detail[$j]['nama_barang_wip'] ?></td>
						<td> <input type='text' id='qty[]' name='qty[]' placeholder='Masukkan Qty' class='form-control' value='<?php echo $data_detail[$j]['v_stok_opname'] ?>'> </td>  
                    </tr>
                    
                <?php 
					}
					?>
					
					<?php
                } ?>
                  
				</tbody>
           	</table>
       	
				 <table id="tabelku" class="table table-striped table-bordered table-hover table-condensed">
				
					<tr >
		
					<input type='hidden' id='no' name='no' >
					          <td align="center" id="num_1">1</td>
					<td>
				<input type="hidden" name="id_barang_wip_1[]" id="id_barang_wip_1" value="">
                <input id="kode_barang_wip_1" class="form-control input-group-lg" type="text" name="kode_barang_wip_1[]"
                       title="Masukkan Kode Barang WIP" value=""
                        placeholder="Kode Barang WIP" onclick= "openCenteredWindows('<?php echo base_url('index.php/dashboard/item_makloon/barang_wip/1') ?>');"
                       />
				</td>
				<td>
                <input id="nama_barang_wip_1" class="form-control input-group-lg" type="text" name="nama_barang_wip_1"
                       title="Masukkan Nama Barang WIP" value=""
                       placeholder="Nama Barang WIP"/ disabled>
				</td>
				<td>
                <input id="qty_1" class="form-control input-group-lg" type="text" name="qty_1[]"
                       title="Masukkan Quantity" value=""
                       placeholder="Quantity"/>
             </td>  
            	
                     </tr>
                     </table>   
                      
					</div>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <?php echo anchor('/dashboard/stokopname_unit/list_all', 'Batal', 'class="btn btn-danger " role="button" '); ?> 
					 <?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-success', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
				</div>
      
			
			</form>
			
		  </div> 
		   
					
	</div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $query ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



