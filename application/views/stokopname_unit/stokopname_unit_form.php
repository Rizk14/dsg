<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<div class="container">
<h2>Stock Opname</h2>
<hr>

    <form action ="stokopname_unit/view" name="myform" id='myform' class="form-inline" method="post">

        <div class="row">
          <?php echo form_label('Periode SO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;', 'Periode SO', array('class' => 'control-label ')) ?>
            <div class="input-group date" data-date-format="mm-yyyy">
               
               <input id='periode_so' name='periode_so' value='' class='form-control'
				placeholder="Periode SO" maxlength="10" onclick="displayCalendar(document.forms[0].periode_so,'yyyy-mm',this)"></input>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    
   
        
      
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="unit_jahit" class="control-label">Unit Jahit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="unit_jahit" name="unit_jahit">
			<option value="0"> - Pilih -</option>
		<?php foreach ($unit_jahit as $unj) {?> 
    <option value=<?php echo $unj->id ?>><?php echo $unj->nama_unit_jahit?></option>
    <?php } ?>
  </select>
        </div>
        </div>
                <div class="row">
        <div class="form-group has-feedback ">
            <label for="unit_packing" class="control-label">Unit packing &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="unit_packing" name="unit_packing">
			<option value="0"> - Pilih -</option>
		<?php foreach ($unit_packing as $unp) {?> 
    <option value=<?php echo $unp->id ?>><?php echo $unp->nama_unit_packing?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="gudang" class="control-label">Gudang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="gudang" name="gudang">
			<option value="0"> - Pilih -</option>
		<?php foreach ($gudang as $ung) {?> 
    <option value=<?php echo $ung->id.":".$ung->id_jenis_so ?>><?php echo $ung->nama_gudang?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        <br>
        <?php echo form_button(array('content'=>'View', 'type'=>'submit', 'class'=>'btn btn-info')) ?>
        <?php echo anchor('/dashboard/stokopname_unit/list_all', 'Batal', 'class="btn btn-danger " role="button" '); ?> 

</form>
        </div>
