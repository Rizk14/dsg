<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_listtrasferdo; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_listtrasferdo; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listtransferdo/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterltransferdoform">

		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_tgl_mulai_do; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?php echo $tgldomulai; ?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)">
					<?php echo $list_tgl_akhir_do; ?> 
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?php echo $tgldoakhir; ?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)"></td>
				  </tr>
				  <tr>
					<td width="33%"><?php echo $list_no_do; ?></td>
					<td width="0%">:</td>
					<td>
					<?php
					$nomor_do	= array(
					'name'=>'nomor_do',
					'id'=>'nomor_do',
					'type'=>'text',
					'value'=>$nomordo,
					'maxlength'=>'14'
					);
					echo form_input($nomor_do);
					?>
					</td>
				  </tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_listtrasferdo; ?></div></td>	
			</tr>
			<tr>
			  <td>
			  <? if($flag=='f'){?>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="2%" class="tdatahead">NO</td>
				  <td width="9%" class="tdatahead"><?php echo $list_kodeop_listtrasferdo; ?></td>
				  <td width="8%" class="tdatahead"><?php echo $list_tglop_listtrasferdo; ?> </td>
				  <td width="9%" class="tdatahead"><?php echo $list_kodedo_listtrasferdo; ?></td>
				  <td width="8%" class="tdatahead"><?php echo $list_tgldo_listtrasferdo; ?> </td>
				  <td width="2%" class="tdatahead"><?php echo $list_area_listtrasferdo; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_kodebrg_listtrasferdo; ?> </td>
				  <td width="40%" class="tdatahead"><?php echo $list_nmbrg_listtrasferdo; ?> </td>
				  <td width="4%" class="tdatahead"><?php echo $list_jml_listtrasferdo; ?> </td>
				</tr>
				<?php				
				
				$lopbrg	= "";
								
				if(sizeof($isi_f) > 0 ) {

					$no	= 1;
					$cc	= 1;
					
					foreach($isi_f as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$exp_ddo	= explode("-",$row->d_do,strlen($row->d_do)); // YYYY-mm-dd
						$ddo	= $exp_ddo[2]."/".$exp_ddo[1]."/".$exp_ddo[0];

						$exp_dop	= explode("-",$row->d_op,strlen($row->d_op)); // YYYY-mm-dd
						$dop	= $exp_dop[2]."/".$exp_dop[1]."/".$exp_dop[0];
						
						$lopbrg	.= "
							<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\" >
							  <td class=\"tdatahead2\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
							  <td class=\"tdatahead2\">".$row->i_op_code."</td>
							  <td class=\"tdatahead2\">".$dop."</td>
							  <td class=\"tdatahead2\">".$row->i_do_code."</td>
							  <td class=\"tdatahead2\">".$ddo."</td>
							  <td class=\"tdatahead2\" align=\"center\">".$row->area."</td>
							  <td class=\"tdatahead2\">".$row->iproduct."</td>
							  <td class=\"tdatahead2\">".$row->eproductname."</td>
							  <td class=\"tdatahead2\">".$row->jmldo."</td>
							</tr>";
							$no+=1; $cc+=1;
					}
				}
				?>
				
				<?php 
				echo $lopbrg;
				?>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table>
			  <? } elseif($flag=='t') { ?>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					  <td width="2%" class="tdatahead">NO</td>
					  <td width="9%" class="tdatahead"><?php echo $list_kodeop_listtrasferdo; ?></td>
					  <td width="8%" class="tdatahead"><?php echo $list_tglop_listtrasferdo; ?> </td>
					  <td width="9%" class="tdatahead"><?php echo $list_kodedo_listtrasferdo; ?></td>
					  <td width="8%" class="tdatahead"><?php echo $list_tgldo_listtrasferdo; ?> </td>
					  <td width="2%" class="tdatahead"><?php echo $list_area_listtrasferdo; ?> </td>
					  <td width="10%" class="tdatahead"><?php echo $list_kodebrg_listtrasferdo; ?> </td>
					  <td width="40%" class="tdatahead"><?php echo $list_nmbrg_listtrasferdo; ?> </td>
					  <td width="4%" class="tdatahead"><?php echo $list_jml_listtrasferdo; ?> </td>
				  </tr>
				  <?
					
					$lopbrg	= "";
							
					if(sizeof($isi_t) > 0 ) {
						
						$no	= 1;
						$cc	= 1;
						
						foreach($isi_t as $row) {
	
						  $Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						  $bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";						

						  $exp_ddo	= explode("-",$row->d_do,strlen($row->d_do)); // YYYY-mm-dd
						  $ddo	= $exp_ddo[2]."/".$exp_ddo[1]."/".$exp_ddo[0];

						  $exp_dop	= explode("-",$row->d_op,strlen($row->d_op)); // YYYY-mm-dd
						  $dop	= $exp_dop[2]."/".$exp_dop[1]."/".$exp_dop[0];
							  
						  $lopbrg	.=
						  "<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\">
							  <td class=\"tdatahead2\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
							  <td class=\"tdatahead2\">".$row->i_op_code."</td>
							  <td class=\"tdatahead2\">".$dop."</td>
							  <td class=\"tdatahead2\">".$row->i_do_code."</td>
							  <td class=\"tdatahead2\">".$ddo."</td>
							  <td class=\"tdatahead2\"  align=\"center\">".$row->area."</td>
							  <td class=\"tdatahead2\">".$row->iproduct."</td>
							  <td class=\"tdatahead2\">".$row->eproductname."</td>
							  <td class=\"tdatahead2\">".$row->jmldo."</td>
						  </tr>";
							$no+=1; $cc+=1;

					}
					echo $lopbrg;
				}
				?>						  
			  </table>			  
			  <? } ?>
			  </td>
			</tr>
			<tr><td align="center"><?php if($flag=='t') echo $create_link; ?></td></tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Jika ada koreksi DO dan sebelumnya sdh di transfer.</div></td>	
			</tr>
			<tr>
			  <td align="right">
			  <?php
			  if($flag=='f') {
			  ?>
			  <input name="btntransferulang" id="btntransferulang" type="button" value="<?php echo $button_transfer_ulang; ?>" onclick="show('listtransferdo/cform/transferulang/<?php echo $nomordo; ?>/','#content')" />&nbsp;<span style="color:#FF0000">*</span>&nbsp;&nbsp;
			  <?php } ?>
			  <input name="btnkeluar" id="btnkeluar" type="button" value="<?php echo $button_keluar; ?>" onclick="show('listtransferdo/cform','#content')" /></td>
			</tr>
		  </table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
