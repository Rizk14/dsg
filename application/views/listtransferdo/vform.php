<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#d_do_first").datepicker();
	$("#d_do_last").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_listtrasferdo; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_listtrasferdo; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listtransferdo/cform/carilisttransferdo','update'=>'#content','type'=>'post'));
		?>
		<div id="masterltransferdoform">
		  <table width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_tgl_mulai_do; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" />
					<?php echo $list_tgl_akhir_do; ?>
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" />
					<span style="color:#FF0000">*</span></td>
				  </tr>
				  <tr>
					<td width="33%"><?php echo $list_no_do; ?></td>
					<td width="0%">:</td>
					<td>
					<?php
					$nomor_do	= array(
					'name'=>'nomor_do',
					'id'=>'nomor_do',
					'type'=>'text',
					'maxlength'=>'14'
					);
					echo form_input($nomor_do);
					?>
					<img name="img_i_op_" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Tampilkan Nomor DO" class="imgLink" align="absmiddle" onclick="infotransferdo();">
					</td>
				  </tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>			
			<tr>
			  <td align="right">
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" />
				<input name="btnbatal" type="reset" id="btnbatal" value="<?php echo $button_batal; ?>" />			  
			  </td>
			</tr>
		  </table>

		</div>
		<?php echo form_close()?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
