<script type="text/javascript">

function getUser(lepel) {
	
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('printer/cform/cari_user');?>",
	data:"ilepel="+lepel,
	success: function(data){
	$("#cboUser").html(data);
	
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_addprinter; ?></td></tr>
</table>
<a href="<?php echo base_url(); ?>index.php/printer/cform/tambah">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/printer/cform">View Data</a>&nbsp;<br><br>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_addprinter; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<div id="masteraddprinterform">

			<div id="tabs">
				<div id="tab1" class="tab_sel_printer" align="center" onClick="javascript: displayPanelPrinter('1');">DAFTAR PRINTER</div>
				<div id="tab2" class="tab_printer" style="margin-left:1px;" align="center" onClick="javascript: displayPanelPrinter('2');">FORM TAMBAH PRINTER</div>

			<div class="tab_bdr_printer"></div>
			<div class="panel_printer" id="panel1" style="display: block">
				<?php			
				if( !empty($query) || isset($query) ) {
					$simgedit	= "<img src=".base_url()."asset/theme/images/edit.gif"."width=12 height=13>";
					$simgdelete	= "<img src=".base_url()."asset/theme/images/delete.gif"."width=12 height=13>";
					$cc = 1;
					foreach($query as $row) {
						
						$Classnya = $cc % 2 == 0 ? "row" :"row1";
							
						
						$list .= "
						 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
						  <td>".$cc."</td>		 
						  <td>".$row->e_printer_name."&nbsp;</td>
						  <td>".$row->ip."&nbsp;</td>
						  <td align=\"center\">".$row->e_uri."&nbsp;</td>
						  <td align=center>
						   <a href='".base_url()."index.php/printer/cform/edit/".$row->i_printer."'>
						<img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;
						   <a href='".base_url()."index.php/printer/cform/actdelete/".$row->i_printer."'><img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13></a>
						  </td>
						 </tr>
						";
						$cc+=1;	
					}
				}
				/******
				onclick=\"return confirm('Anda yakin akan menghapus?')\"
				*******/
				?>
				<table class="listtable_user" border="1">
				 <th width="4%">No</th>
				 <th width="40%"><?php echo $form_tbl_printer_name; ?></th>
				 <th width="15%"><?php echo $form_tbl_printer_ip; ?></th>
				 <th width="15%"><?php echo $form_tbl_printer_uri; ?></th>
				 <th width="10%" align="center"><?php echo $link_aksi; ?></th>
				<tbody>
				<?php
				if( !empty($list) && !isset($not_defined) ) {
					echo $list;
				} else {
					echo $list."</tbody></table>".$not_defined;
				}
				?>
			</div>
			
			<div class="panel_printer" id="panel2" style="display: none">
				<table>
				  <tr>
					<td align="left">
					  <?php 
					  echo $this->pquery->form_remote_tag(array('url'=>'printer/cform/simpan','update'=>'#content','type'=>'post'));
					  ?>
				  	  <div class="accordion2">
					  <table>
						<tr>
					  		<td width="150px"><?php echo $form_name_printer; ?></td>
					  		<td width="1px">:</td>
					  		<td>
							<?php
								$eprintername	= array(
									'name'=>'e_printer_name',
									'id'=>'e_printer_name',
									'value'=>'',
									'maxlength'=>'100'
								);
								echo form_input($eprintername);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_uri_printer; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$euri	= array(
									'name'=>'e_uri',
									'id'=>'e_uri',
									'value'=>'/printers/',
									'maxlength'=>'200'
								);
								echo form_input($euri);
							?>
							</td>
					    </tr>
						<tr>
					  		<td><?php echo $form_ip_printer; ?></td>
					  		<td>:</td>
					  		<td>
							<?php
								$ip	= array(
									'name'=>'ip',
									'id'=>'ip',
									'value'=>'',
									'maxlength'=>'15'
								);
								echo form_input($ip);
							?>
							</td>
					    </tr>						
						<tr>
					  		<td><?php echo $form_level_user_printer; ?></td>
					  		<td>:</td>
					  		<td>
							<select name="ilevel" onchange="getUser(this.value);">
							<option value="">[ <?php echo $form_pilihan_level_printer; ?> ]</option>
							<?php 
							foreach($opt_level as $row) {
								$llevel	.= "<option value=";
								$llevel	.= $row->i_level.">";
								$llevel	.= $row->e_level;
								$llevel	.= "</option>";
							}
							echo $llevel;
							?>
							</select>
							</td>
					    </tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<?php echo "<td width=\"%\" id=\"cboUser\" align=\"left\"></td>"; ?>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>
					    <tr>
					  		<td>&nbsp;</td>
					  		<td>&nbsp;</td>
					  		<td>
							<input name="tblsimpan" id="tblsimpan" value="<?php echo $button_simpan; ?>" type="submit">
							<input name="tblreset" id="tblreset" value="<?php echo $button_batal; ?>" type="button" onclick='show("printer/cform/","#content");'>
					  		</td>
					    </tr>
					  </table>
				      </div>
					  <?php echo form_close()?>
					</td>
				  </tr> 
				</table>
			</div>
			
		</div>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
