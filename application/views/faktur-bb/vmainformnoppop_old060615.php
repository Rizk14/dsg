<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<h3>Data Bukti Penerimaan Barang</h3><br>
<a href="<? echo base_url(); ?>index.php/faktur-bb/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/faktur-bb/cform/view">View Data</a><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	var tgl_sj = $("#tgl_sj").val();
	var no_sj = $("#no_sj").val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	
	if (no_sj == '') {
		alert ("Nomor SJ harus diisi");
		$("#no_sj").focus();
		return false;
	}
	if (tgl_sj == '') {
		alert ("Tgl SJ harus dipilih");
		$("#no_sj").focus();
		return false;
	}
	
	if (jenis_pembelian == '0') {
		alert("Jenis pembelian harus dipilih..!");
		return false;
	}
	
	/*if(document.getElementById('kode_1').value=='') {
		alert('Maaf, item barangnya harus diisi.');
		document.getElementById('kode_1').focus();
		return false;
	} */
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#kode_'+k).val() == '') {
				alert("Data item barang tidak boleh ada yang kosong...!");
				return false;
			}
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka atau desimal..!");
				return false;
			}
			
			if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
				alert("Data harga tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#harga_'+k).val()) ) {
				alert("Data harga harus berupa angka atau desimal..!");
				return false;
			}
			
			if ($('#diskon_'+k).val() == '' ) {
				alert("Diskon minimal harus diisi 0..!");
				return false;
			}
			
			if (isNaN($('#diskon_'+k).val()) ) {
				alert("Diskon harus berupa angka..!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
}

function ckpp(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('pp-new/cform/cari_pp');?>",
	data:"nomorpp="+nomor,
	success: function(data){
		$("#confnomorpp").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

//tambah
$(function()
{
	//var goedit = $("#goedit").val();
	//if(goedit!='1')
		$("#no").val('2');
		get_data_pkp();
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****harga_lama*************************************
		var harga_lama="#harga_lama_"+n;
		var new_harga_lama="#harga_lama_"+no;
		$(harga_lama, lastRow).attr("id", "harga_lama_"+no);
		$(new_harga_lama, lastRow).attr("name", "harga_lama_"+no);		
		$(new_harga_lama, lastRow).val('');				
		//*****end harga_lama*************************************	
		
		//*****diskon*************************************
		var diskon="#diskon_"+n;
		var new_diskon="#diskon_"+no;
		$(diskon, lastRow).attr("id", "diskon_"+no);
		$(new_diskon, lastRow).attr("name", "diskon_"+no);		
		$(new_diskon, lastRow).val('0');				
		//*****end diskon*************************************	
		
		//*****pajak*************************************
		var pajak="#pajak_"+n;
		var new_pajak="#pajak_"+no;
		$(pajak, lastRow).attr("id", "pajak_"+no);
		$(new_pajak, lastRow).attr("name", "pajak_"+no);		
		$(new_pajak, lastRow).val('0');				
		//*****end pajak*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); var kode_sup= $('#kode_supplier').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/faktur-bb/cform/show_popup_brg/'+x+'/"+no+"/'+kode_sup);";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
	$('#kode_supplier').change(function(){
	  	    get_data_pkp();
	});
	
	$('#pkp').click(function(){
	  	    if ($("#pkp").is(":checked")) {
				$('#hide_pkp').val('t');
				hitungnilai();
			}
			else {
				$('#hide_pkp').val('f');
				hitungnilai();
			}
	  });
	  
	$('#tipe_pajak').change(function(){
	  	    if ($("#tipe_pajak").val() == 'I') {
				$('#hide_tipe_pajak').val('I');
				hitungnilai();
			}
			else {
				$('#hide_tipe_pajak').val('E');
				hitungnilai();
			}
	  });
	  
	  $('#lain_cash').click(function(){
			if ($("#lain_cash").is(":checked")) {
				if ($("#lain_kredit").is(":checked")) {
					alert ("Tidak boleh diceklis dua-duanya, harus salah satu saja");
					$('input[name=lain_cash]').attr('checked', false);
				}
			}
	  });
	  
	  $('#lain_kredit').click(function(){
			if ($("#lain_kredit").is(":checked")) {
				if ($("#lain_cash").is(":checked")) {
					alert ("Tidak boleh diceklis dua-duanya, harus salah satu saja");
					$('input[name=lain_kredit]').attr('checked', false);
				}
			}
	  });
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function get_data_pkp() {
	var kode_sup= $('#kode_supplier').val();
	var jum_detail = $('#no').val()-1;
    $.getJSON("<?php echo base_url(); ?>index.php/faktur-bb/cform/get_pkp_tipe_pajak/"+kode_sup, function(data) {

		$(data).each(function(index, item) {
			$("#topnya").html("T.O.P : "+item.top+" Hari");
			$("#topnya").show();

			if (item.pkp == 't') {
				//alert($("input:checked").length);
				$('#hide_pkp').val('t');
				var i=1;
				var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
				
					$('input[name=pkp]').attr('checked', true);
					if (item.tipe_pajak == 'I') {
						$('#tipe_pajak option[value=I]').attr('selected', 'selected');
						$('#hide_tipe_pajak').val('I');
						var hitung = (harga*qty)-diskon;
						hitung = hitung.toFixed(2);
						var pi = hitung/1.1;
						new_pajak = hitung-pi;
						new_pajak = new_pajak.toFixed(2);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(hitung);
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					}
					else {
						$('#tipe_pajak option[value=E]').attr('selected', 'selected');
						$('#hide_tipe_pajak').val('E');
						var hitung = (harga*qty)-diskon;
						hitung = hitung.toFixed(2);
						var new_pajak = hitung*0.1;
						new_pajak = new_pajak.toFixed(2);
						new_total = parseFloat(new_pajak)+parseFloat(hitung);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(new_total);
						gtotal = parseFloat(gtotal)+parseFloat(new_total); 
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					} //end cek item pajak
				}
				gtotal = gtotal.toFixed(2);
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(2);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal/1.1;
				dpp = dpp.toFixed(2);
				$("#dpp").val(dpp);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
			}
			else if (item.pkp == 'f') {
				$('input[name=pkp]').attr('checked', false);
				$('#tipe_pajak option[value=I]').attr('selected', 'selected');
				$('#hide_pkp').val('f');
				$('#hide_tipe_pajak').val('I');
				var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					var hitung = (harga*qty)-diskon;
					hitung = hitung.toFixed(2);
					$("#pajak_"+i).val('0');
					$("#total_"+i).val(hitung);
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				gtotal = gtotal.toFixed(2);
				$("#gtotal").val(gtotal);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
				$("#tot_pajak").val('0');
				$("#dpp").val('0');
			}
				
        });
    });
    
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
	status_pkp = $('#hide_pkp').val();
	status_pajak = $('#hide_tipe_pajak').val();
	
	if (status_pkp == 't') {
		var i=1;
		var gtotal = 0; var tot_pajak = 0; var dpp = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
				
					//$('input[name=pkp]').attr('checked', true);
					if (status_pajak == 'I') {
						//$('#tipe_pajak option[value=I]').attr('selected', 'selected');
						var hitung = (harga*qty)-diskon;
						hitung = hitung.toFixed(2);
						var pi = hitung/1.1;
						new_pajak = hitung-pi;
						new_pajak = new_pajak.toFixed(2);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(hitung);
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					}
					else {
						//$('#tipe_pajak option[value=E]').attr('selected', 'selected');
						var hitung = (harga*qty)-diskon;
						hitung = hitung.toFixed(2);
						var new_pajak = hitung*0.1;
						new_pajak = new_pajak.toFixed(2);
						new_total = parseFloat(new_pajak)+parseFloat(hitung);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(new_total);
						gtotal = parseFloat(gtotal)+parseFloat(new_total);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					} //end cek item pajak
				}
				gtotal = gtotal.toFixed(2);
				$("#gtotal").val(gtotal);
				tot_pajak = tot_pajak.toFixed(2);
				$("#tot_pajak").val(tot_pajak);
				dpp = gtotal/1.1;
				dpp = dpp.toFixed(2);
				$("#dpp").val(dpp);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
	}
	else if (status_pkp == 'f') {
				var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					var hitung = (harga*qty)-diskon;
					hitung = hitung.toFixed(2);
					$("#pajak_"+i).val('0');
					$("#total_"+i).val(hitung);
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				gtotal = gtotal.toFixed(2);
				$("#gtotal").val(gtotal);
				$("#tot_pajak").val('0');
				$("#dpp").val('0');
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
	}
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/faktur-bb/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="is_no_ppop" id="is_no_ppop" value="<?php echo $is_no_ppop ?>">
<input type="hidden" name="kode_sup" value="<?php echo $kode_supplier ?>">
<!--<input type="hidden" name="lain_cash" id="lain_cash" value="">
<input type="hidden" name="lain_kredit" id="lain_kredit" value=""> -->

<input type="hidden" name="no" id="no" >
<input type="hidden" name="iddata" id="iddata" />

<div align="center">

<label id="status"></label>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
	<tr>
		<td width="20%">Tanpa acuan PP ataupun OP</td>
		<td>
		  
    </td>
  </tr>
		<tr>
			<td>Supplier</td>
			<td><select name="kode_supplier" id="kode_supplier" onkeyup="this.blur();this.focus();" disabled >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" <?php if ($sup->kode_supplier == $kode_supplier) { ?> selected="true" <?php } ?>><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr>
		<tr>
			<td>
			&nbsp;
			</td>
			<td>PKP <input type="checkbox" name="pkp" id="pkp" value="t" checked="checked">&nbsp;&nbsp;&nbsp;
			Tipe Pajak <select name="tipe_pajak" id="tipe_pajak" onkeyup="this.blur();this.focus();">
				<option value="I">Include</option>
				<option value="E" >Exclude</option>
			</select>&nbsp;&nbsp;&nbsp;<span id="topnya"></span>
			<input type="hidden" name="hide_pkp" id="hide_pkp" value=""><input type="hidden" name="hide_tipe_pajak" id="hide_tipe_pajak" value="">
			</td>
		</tr>
  <tr>
    <td>Nomor SJ</td>
    <td>
      <input name="no_sj" type="text" id="no_sj" size="20" maxlength="20" value="">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tanggal SJ</td>
    <td>
	<label>
      <input name="tgl_sj" type="text" id="tgl_sj" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_sj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_sj,'dd-mm-yyyy',this)">
	</td>
  </tr>
  
  <tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian" >
				<option value="0">-Pilih-</option>
				<option value="1">Cash</option>
				<option value="2">Kredit</option>
			</select>&nbsp;</td> 
  </tr>
  
  <tr>
    <td nowrap>Pembelian cash lain-lain</td>
    <td>
		<input type="checkbox" name="lain_cash" id="lain_cash" value="t">
	</td>
  </tr>
  <tr>
    <td nowrap>Pembelian kredit lain-lain</td>
    <td>
		<input type="checkbox" name="lain_kredit" id="lain_kredit" value="t">
	</td>
  </tr>
  
 <!-- <tr>
    <td nowrap>Masukkan stok ke tipe lain-lain (cash)</td>
    <td>
		<input type="checkbox" name="lain_cash" id="lain_cash" value="t">
	</td>
  </tr>
  <tr>
    <td nowrap>Masukkan stok ke tipe lain-lain (kredit)</td>
    <td>
		<input type="checkbox" name="lain_kredit" id="lain_kredit" value="t">
	</td>
  </tr> -->

  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="10" align="right"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
	      <th>Qty</th>
          <th>Harga</th>
	      <th>Diskon</th>
	      <th>PPN</th>
          <th>Total</th>
        </tr>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          <td><select name="kel_brg_1" id="kel_brg_1">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select></td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_1" type="text" id="kode_1" size="15" readonly="true"/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#kel_brg_1').val(); var kode_sup = $('#kode_supplier').val();
           openCenteredWindow('<?php echo base_url(); ?>index.php/faktur-bb/cform/show_popup_brg/'+ x +'/1/' + kode_sup);" ></td>
         <td><input name="nama_1" type="text" id="nama_1" size="25" readonly="true"/></td>
         <td><input name="satuan_1" type="text" id="satuan_1" size="10" readonly="true"/></td>
         <td><input name="qty_1" type="text" id="qty_1" size="10" onkeyup="hitungnilai()" onblur="hitungnilai()" /></td>
         <td><input name="harga_1" type="text" id="harga_1" size="10" onkeyup="hitungnilai()" onblur="hitungnilai()" />
			<input name="harga_lama_1" type="hidden" id="harga_lama_1" value="">  
         </td>
         <td><input name="diskon_1" type="text" id="diskon_1" size="8" onkeyup="hitungnilai()" onblur="hitungnilai()" value="0" /></td>
         <td><input name="pajak_1" type="text" id="pajak_1" size="8" value="0" readonly="true" /></td>
         <td><input name="total_1" type="text" id="total_1" size="10" value="0" readonly="true" /></td>
        </tr>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td>DPP</td>
			<td>: <input type="text" name="dpp" id="dpp" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Total PPN</td>
			<td>: <input type="text" name="tot_pajak" id="tot_pajak" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Grand Total</td>
			<td>: <input type="text" name="gtotal" id="gtotal" value="0" size="10" readonly="true">
			</td>
		</tr>
		<tr>
			<td>Uang Muka</td>
			<td>: <input type="text" name="uang_muka" id="uang_muka" value="0" size="10" onkeyup="hitunghutang()"></td>
		</tr>
		<tr>
			<td>Sisa Hutang</td>
			<td>: <input type="text" name="sisa_hutang" id="sisa_hutang" value="0" size="10" readonly="true"></td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>: <input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
	</table>
	</form>
      <div align="center"><br><br> 
        
        <input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/faktur-bb/cform/'">
      </div></td>
    </tr>

</table>
</div>
</form>
