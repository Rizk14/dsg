<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
   
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .isinya2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">

$(function()
{
	var go_print= jQuery('#go_print').val();
	
	//if (go_print == '1')
	//	window.print();
	
});


</script>

<?php
	$tgl = date("d-m-Y");
	$pisah1 = explode("-", $tgl);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;

	//if ($proses != '') {
?>
<input type="hidden" name="go_print" id="go_print" value="1" >
<center><h3>BUKTI PENERIMAAN BARANG PEMBELIAN</h3></center>
<table class="isinya" border='0' align="center" width="90%">
	<tr>
		<td width="10%" style="white-space:nowrap;">Nomor SJ</td>
		<td style="white-space:nowrap;">: <?php echo $query[0]['no_sj'] ?></td>
		<td width="30%">&nbsp;</td>
		<td width="20%" style="white-space:nowrap;">Supplier</td>
		<td style="white-space:nowrap;">: <?php echo $query[0]['kode_supplier']." - ".$query[0]['nama_supplier'] ?></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;">Tanggal SJ</td>
		<td style="white-space:nowrap;">: <?php echo $query[0]['tgl_sj'] ?></td>
		<td width="30%">&nbsp;</td>
		<td width="15%" style="white-space:nowrap;">Jenis Pembelian</td>
		<td style="white-space:nowrap;">: <?php if ($query[0]['jenis_pembelian'] == '1') echo "Cash"; else echo "Kredit"; ?></td>
	</tr>
	<tr>
		<td colspan="5">
			<table border='1' class="isinya2" width="100%" >
				<tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th>Satuan</th>
          <th>Qty</th>
          <th>Harga (Rp.)</th>
          <th>Subtotal (Rp.)</th>
        </tr>

        <?php $i=1;
        if (count($query[0]['detail_fb'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
			$detailnya = $query[0]['detail_fb'];
			for($j=0;$j<count($query[0]['detail_fb']);$j++){
			?>
			<tr>
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td style="white-space:nowrap;"><?php echo $detailnya[$j]['kode_brg']; ?></td>
          <td style="white-space:nowrap;"><?php echo $detailnya[$j]['nama']; ?></td>
          <td style="white-space:nowrap;"><?php echo $detailnya[$j]['nama_satuan']; ?></td>
          <td style="white-space:nowrap;" align="right"><?php echo number_format($detailnya[$j]['qty'],0,',','.') ?></td>
          <td style="white-space:nowrap;" align="right"><?php echo number_format($detailnya[$j]['harga'],2,',','.') ?></td>
          <td style="white-space:nowrap;" align="right"><?php echo number_format($detailnya[$j]['total'],2,',','.') ?></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
			</table><br>
			<div align="left">
			DPP = Rp. <?php echo number_format($query[0]['dpp'], 2, ',','.'); ?><br>
			PPN = Rp. <?php echo number_format($query[0]['total_pajak'], 2, ',','.'); ?><br>
			Grand Total = Rp. <?php echo number_format($query[0]['total'], 2, ',','.'); ?><br>
			</div>
		</td>
		</tr>
	</td>
	</tr>
	<tr>
		<td colspan="5"><hr><i><div align="right">Tanggal Cetak: <?php echo date("d-m-Y H:i:s"); ?></i></div></td>
	</tr>

</table>
<?php 
	//}
	
?>

<?php 
	//echo form_close(); 
	//}
?>
