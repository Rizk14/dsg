<div id="tmpx">
<?php
	if(
		($periode=='')
	  )
	{
		$this->load->view('akt-neraca/vform');
	}else{
		$data['periode']	= $periode;
		$data['namabulan']	= $namabulan;
		$data['tahun']		= $tahun;
		$data['dfrom']		= $dfrom;
		$data['dto']		= $dto;
		$data['kas']		= $kas;
		$data['piutang']	= $piutang;
		$data['hutangdagang']= $hutangdagang;
		$data['modal']		= $modal;
		$data['giromundur']	= $giromundur;
		$data['piutangkaryawan']		= $piutangkaryawan;
		$data['piutanglainlain']		= $piutanglainlain;
		$data['biayadibayardimuka']		= $biayadibayardimuka;
		$data['persediaanbarangdagang']	= $persediaanbarangdagang;
		$data['possementaradebet']		= $possementaradebet;
		$data['aktivatetapkel1']		= $aktivatetapkel1;
		$data['aktivatetapkel2']		= $aktivatetapkel2;
		$data['akumaktiva1']			= $akumaktiva1;
		$data['akumamortisasi']			= $akumamortisasi;
		$data['biayayangditangguhkan']	= $biayayangditangguhkan;
		$data['akumamortisasiyangditangguhkan']	= $akumamortisasiyangditangguhkan;
		$data['hutangbank']	= $hutangbank;
		$data['hutangpajakpph21']= $hutangpajakpph21;
		$data['hutangpajakpph22']= $hutangpajakpph22;
		$data['hutangpajakpph23']= $hutangpajakpph23;
		$data['hutangpajakpph24']= $hutangpajakpph24;
		$data['hutangpajakppn']= $hutangpajakppn;
		$data['hutanglain']	= $hutanglain;
		$data['possementara']= $possementara;
		$data['hutangbankjangkapanjang']= $hutangbankjangkapanjang;
		$data['modalyangdisetor']= $modalyangdisetor;
		$data['labarugiyangditahantahunberjalan']= $labarugiyangditahantahunberjalan;
		$data['labarugiyangditahan']= $labarugiyangditahan;
		$data['labarugiyangditahanbulanberjalan']= $labarugiyangditahanbulanberjalan;
		$data['pajakpph21ydm']= $pajakpph21ydm;
		$data['pajakpph22ydm']= $pajakpph22ydm;
		$data['pajakpph23ydm']= $pajakpph23ydm;
		$data['pajakpph25ydm']= $pajakpph25ydm;
		$data['uangmukaleasing']= $uangmukaleasing;
		$data['yhmdibayar']	= $yhmdibayar;
		$data['tanggal']	= $tanggal;
		$this->load->view('akt-neraca/vformview',$data);
	}
?>
</div>
