<?
 	include ("php/fungsi.php");
   	echo "<center><h2>$page_title</h2></center>";
	echo "<center><h3>$namabulan $tahun</h3></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" width="750px">
	   	    <th width="250px">Aktiva</th>
			<th width="125px">&nbsp;</th>
			<th width="250px">Passiva</th>
			<th width="125px">&nbsp;</th>
		<?
		echo "<tr valign=top><td colspan=2><table width=\"100%\">";
		$aktiva	= 0;
		$passiva= 0;
		echo "<table width=\"100%\">
 		<tr valign=top> 
		<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">A K T I V A - A K T I V A :</td>";
		echo "</tr>";
		#----------------
		echo "<table width=\"100%\">
 		<tr valign=top> 
		<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Aktiva Lancar :</td>";
		echo "</tr>";

		if($kas){
			$this->load->model('akt-bb/mmaster');
			$totalkas=0;
			foreach($kas as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalkas=$totalkas+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($giromundur){
			$totalgiro=0;
			foreach($giromundur as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalgiro=$totalgiro+$tmp->v_saldo_akhir;

			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($piutang){
			$totalpiutang=0;
			foreach($piutang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpiutang=$totalpiutang+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($piutangkaryawan){
			$totalpiutangkaryawan=0;
			foreach($piutangkaryawan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpiutangkaryawan=$totalpiutangkaryawan+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($piutanglainlain){
			$totalpiutanglainlain=0;
			foreach($piutanglainlain as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpiutanglainlain=$totalpiutanglainlain+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($biayadibayardimuka){
			$totalbiayadibayardimuka=0;
			foreach($biayadibayardimuka as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalbiayadibayardimuka=$totalbiayadibayardimuka+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		$totalaktivasub=$totalbiayadibayardimuka+$totalpiutanglainlain+$totalpiutangkaryawan+$totalpiutang+$totalgiro+$totalkas;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">&nbsp;</td>";
		  if($totalaktivasub){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalaktivasub)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";

	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Pajak Yang Dibayar Dimuka :</td>";
	echo "</tr>";echo "</table>";


			echo "<table width=\"100%\">";
		if($pajakpph21ydm){
			$totalpajakpph21ydm=0;
			foreach($pajakpph21ydm as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpajakpph21ydm=$totalpajakpph21ydm+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

			echo "<table width=\"100%\">";
		if($pajakpph22ydm){
			$totalpajakpph22ydm=0;
			foreach($pajakpph22ydm as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpajakpph22dm=$totalpajakpph22ydm+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		if($pajakpph23ydm){
			$totalpajakpph23ydm=0;
			foreach($pajakpph23ydm as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpajakpph23ydm=$totalpajakpph23ydm+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		if($pajakpph25ydm){
			$totalpajakpph25ydm=0;
			foreach($pajakpph25ydm as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpajakpph25ydm=$totalpajakpph25ydm+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		$totalpajakdibayardimuka1=$totalpajakpph21ydm+$totalpajakpph25ydm+$totalpajakpph23ydm+$totalpajakpph22ydm;
		echo "<tr valign=top> 
			  <td width=\"234px\"colspan=2 style=\"font-weight: bold;\">Pajak Yang Dibayar Dimuka :</td>";
		  if($totalpajakdibayardimuka1){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalpajakdibayardimuka1)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "</table>";

	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">&nbsp;</td>";
	echo "</tr>";echo "</table>";

			echo "<table width=\"100%\">";
		if($persediaanbarangdagang){
			$totalpersediaandagang=0;
			foreach($persediaanbarangdagang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpersediaandagang=$totalpersediaandagang+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

	echo "<table width=\"100%\">";
		if($uangmukaleasing){
			$totaluangmukaleasing=0;
			foreach($uangmukaleasing as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totaluangmukaleasing=$totaluangmukaleasing+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

			echo "<table width=\"100%\">";
		if($possementaradebet){
			$totalpossementaradebet=0;
			foreach($possementaradebet as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpossementaradebet=$totalpossementaradebet+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		$totalaktiva=$totalpajakdibayardimuka1+$totalgiro+$totalkas+$totalpiutang+$totalpiutangkaryawan+$totalpiutanglainlain+$totalbiayadibayardimuka+$totalpersediaandagang+$totalpossementaradebet+$totaluangmukaleasing;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Aktiva Lancar :</td>";
		  if($totalaktiva){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalaktiva)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";

	echo "<table width=\"100%\">
 	 <tr valign=top> 
		<td width=\"450px\" align=right>&nbsp;</td>";
	echo "</tr>";
	echo "</table>";

	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Aktiva Tetap :</td>";
	echo "</tr>";echo "</table>";

		echo "<table width=\"100%\">";
		if($aktivatetapkel1){
			$totalaktivatetapkel1=0;
			foreach($aktivatetapkel1 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalaktivatetapkel1=$totalaktivatetapkel1+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		if($akumaktiva1){
			$totalakumaktiva11=0;
			foreach($akumaktiva1 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalakumaktiva11=$totalakumaktiva11+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		if($aktivatetapkel2){
			$totalaktivatetapkel2=0;
			foreach($aktivatetapkel2 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalaktivatetapkel2=$totalaktivatetapkel2+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

			echo "<table width=\"100%\">";
		if($akumaktiva2){
			$totalakumaktiva2=0;
			foreach($akumaktiva2 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalakumaktiva2=$totalakumaktiva2+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		$totalaktivatetap=$totalaktivatetapkel1+$totalakumaktiva11+$totalaktivatetapkel2+$totalpiutangkaryawan;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Aktiva Tetap :</td>";
		  if($totalaktivatetap){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalaktivatetap)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";

	echo "<table width=\"100%\">
	<tr valign=top> 
	<td width=\"450px\" align=right>&nbsp;</td>";
	echo "</tr>";
	echo "</table>";

	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Aktiva Lain-Lain :</td>";
	echo "</tr>";echo "</table>";

	echo "<table width=\"100%\">";
		if($akumamortisasi){
			$totalakumamortisasi=0;
			foreach($akumamortisasi as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalakumamortisasi=$totalakumamortisasi+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

	echo "<table width=\"100%\">";
		if($biayayangditangguhkan){
			$totalbiayayangditangguhkan=0;
			foreach($biayayangditangguhkan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalbiayayangditangguhkan=$totalbiayayangditangguhkan+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

			echo "<table width=\"100%\">";
		if($akumamortisasiyangditangguhkan){
			$totalakumamortisasiyangditangguhkan=0;
			foreach($akumamortisasiyangditangguhkan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalakumamortisasiyangditangguhkan=$totalakumamortisasiyangditangguhkan+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		$totalaktivatetap=$totalakumamortisasi+$totalbiayayangditangguhkan+$totalakumamortisasiyangditangguhkan;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Aktiva Lain-Lain :</td>";
		  if($totalaktivatetap){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalaktivatetap)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";

	echo "</td><td colspan=2><table width=\"100%\">";
	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">HUTANG-HUTANG DAN MODAL :</td>";
	echo "</tr>";
	#------------------------
	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Hutang Jangka Pendek :</td>";
	echo "</tr>";

		if($hutangdagang){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangdagang = 0;
			foreach($hutangdagang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangdagang=$totalhutangdagang+$tmp->v_saldo_akhir;

			}
		}
		if($hutangbank){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangbank = 0;
			foreach($hutangbank as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangbank=$totalhutangbank+$tmp->v_saldo_akhir;

			}
		}	
				echo "
 				<tr valign=top> 
				<td width=\"234px\" align=right>&nbsp;</td>
				<td width=\"116px\" align=right>&nbsp;</td>
				<td width=\"116px\" align=right>&nbsp;</td>";
				echo "</tr>";
	#----------------------------------	
				echo "
 				<tr valign=top> 
				<td width=\"450px\" colspan=3 style=\"font-weight: bold;\">Hutang Pajak</td>";
				echo "</tr>";
	
			if($hutangpajakpph21){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangpajakpph21 = 0;
			foreach($hutangpajakpph21 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangpajakpph21=$totalhutangpajakpph21+$tmp->v_saldo_akhir;

			}
		}
			if($hutangpajakpph22){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangpajakpph22 = 0;
			foreach($hutangpajakpph22 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangpajakpph22=$totalhutangpajakpph22+$tmp->v_saldo_akhir;

			}
		}
			if($hutangpajakpph23){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangpajakpph23 = 0;
			foreach($hutangpajakpph23 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangpajakpph23=$totalhutangpajakpph23+$tmp->v_saldo_akhir;

			}
		}
			if($hutangpajakpph24){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangpajakpph24 = 0;
			foreach($hutangpajakpph24 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangpajakpph24=$totalhutangpajakpph24+$tmp->v_saldo_akhir;

			}
		}
			if($hutangpajakppn){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangpajakppn = 0;
			foreach($hutangpajakppn as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangpajakppn=$totalhutangpajakppn+$tmp->v_saldo_akhir;

			}
		}
				echo "";
		$totalhutangpajakall=$totalhutangpajakppn+$totalhutangpajakpph21+$totalhutangpajakpph22+$totalhutangpajakpph23+$totalhutangpajakpph24;
				echo "<tr valign=top> 
			  <td width=\"234px\"colspan=2 style=\"font-weight: bold;\">Hutang Pajak :</td>";
		 if($totalhutangpajakall){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalhutangpajakall)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	#------------
				echo "
 				<tr valign=top> 
				<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">&nbsp;</td>";
				echo "</tr>";
	#---------------
		

			if($yhmdibayar){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalyhmdibayar = 0;
			foreach($yhmdibayar as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalyhmdibayar=$totalyhmdibayar+$tmp->v_saldo_akhir;

			}
		}
			if($hutanglain){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutanglain = 0;
			foreach($hutanglain as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutanglain=$totalhutanglain+$tmp->v_saldo_akhir;

			}
		}

			if($possementara){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalpossementara = 0;
			foreach($possementara as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalpossementara=$totalpossementara+$tmp->v_saldo_akhir;

			}
		}
		$totalhutangjangkapendek=$totalhutangdagang+$totalhutangbank+$totalhutanglain+$totalpossementara+$totalyhmdibayar+$totalhutangpajakppn+$totalhutangpajakpph21+$totalhutangpajakpph22+$totalhutangpajakpph23+$totalhutangpajakpph24;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Hutang Jangka Pendek :</td>";
		  if($totalhutangjangkapendek){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalhutangjangkapendek)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}

	echo "
 	<tr valign=top> 
	<td width=\"234px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>";
	echo "</tr>";

	echo "
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Hutang Jangka Panjang :</td>";
	echo "</tr>";

			if($hutangbankjangkapanjang){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangbankjangkapanjang = 0;
			foreach($hutangbankjangkapanjang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangbankjangkapanjang=$totalhutangbankjangkapanjang+$tmp->v_saldo_akhir;

			}
		}

		$totalhutangbankjangkapanjang=$totalhutangbankjangkapanjang;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Hutang Jangka Panjang :</td>";
		  if($totalhutangbankjangkapanjang){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalhutangbankjangkapanjang)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}

	echo "
 	<tr valign=top> 
	<td width=\"234px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>";
	echo "</tr>";

	echo "
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Modal :</td>";
	echo "</tr>";

			if($modalyangdisetor){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalmodalyangdisetor = 0;
			foreach($modalyangdisetor as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalmodalyangdisetor=$totalmodalyangdisetor+$tmp->v_saldo_akhir;

			}
		}
		$totalmodalyangdisetor=$totalmodalyangdisetor;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Modal Yang Disetor :</td>";
		  if($totalmodalyangdisetor){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalmodalyangdisetor)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}

		echo "
 	<tr valign=top> 
	<td width=\"234px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>";
	echo "</tr>";
	#------------
	echo " 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Laba/Rugi Yang Ditahan :</td>";
	echo "</tr>";

			if($labarugiyangditahan){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totallabarugiyangditahan = 0;
			foreach($labarugiyangditahan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totallabarugiyangditahan=$totallabarugiyangditahan+$tmp->v_saldo_akhir;

			}
		}

			if($labarugiyangditahantahunberjalan){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totallabarugiyangditahantahunberjalan = 0;
			foreach($labarugiyangditahantahunberjalan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totallabarugiyangditahantahunberjalan=$totallabarugiyangditahantahunberjalan+$tmp->v_saldo_akhir;

			}
		}

			if($labarugiyangditahanbulanberjalan){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totallabarugiyangditahanbulanberjalan = 0;
			foreach($labarugiyangditahanbulanberjalan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totallabarugiyangditahanbulanberjalan=$totallabarugiyangditahanbulanberjalan+$tmp->v_saldo_akhir;

			}
		}

		$totallabarugi=$totallabarugiyangditahanbulanberjalan+$totallabarugiyangditahantahunberjalan+$totallabarugiyangditahan;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Laba/Rugi Yang Ditahan :</td>";
		  if($totallabarugi){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totallabarugi)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}

	echo "</tr></table></td></tr>";
	/*echo "<tr><td colspan=2>&nbsp;</td><td colspan=2><table width=\"100%\">";
		if($modal){
			$totaldebet	 = 0;
			$totalkredit = 0;
			foreach($modal as $tmp)
			{
				echo "<tr valign=top>
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
			}
		}
		echo "</tr></table></td></tr>"; */
		?>
		  <tr>
			<td width="250px" style="font-weight: bold;">Jumlah</td>
			<td width="125px"	 align=right style="font-weight: bold;"><? echo number_format($aktiva); ?></td>
			<td width="250px" style="font-weight: bold;">Jumlah</td>
			<td width="125px" align=right style="font-weight: bold;"><? echo number_format($passiva); ?></td>
		  </tr>
		</table>
      </tbody>
      </div>
	</div>
    </td>
  </tr>
</table>
