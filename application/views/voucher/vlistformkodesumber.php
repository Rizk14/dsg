<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function settextfield(a,b,c) {
	opener.document.getElementById('i_kode_sumber').value = a;
	opener.document.getElementById('kode_sumber').value = b;
	opener.document.getElementById('deskripsi_voucher').value = c;
	this.close();
}

function getInfo(txt){

$.ajax({

type: "POST",
url: "<?php echo site_url('voucher/cform/flistkodesumbervoucher'); ?>",
data:"key="+txt,
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};

function carifocus() {
	document.getElementById('txtcari').focus();
}

</script>

</head>
<body id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h3>$page_title</h3></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="getInfo(this.value);"/>
  	</td>
  </tr>
  <tr>
    <td align="left">
	<?php echo form_open('voucher/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php
		if( !empty($isi) || isset($isi) ) {
			$cc	= 1; 
			if(count($isi)) {
				foreach($isi as $row) {
				
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->i_voucher','$row->i_voucher_code','$row->e_description')\">".$row->i_voucher_code."</a></td>
					  <td><a href=\"javascript:settextfield('$row->i_voucher','$row->i_voucher_code','$row->e_description')\">".$row->e_voucher_name."</a></td>
					  <td><a href=\"javascript:settextfield('$row->i_voucher','$row->i_voucher_code','$row->e_description')\">".$row->e_description."</a></td>
					 </tr>";
					 
					 $cc+=1;
				}
			}
		}
		?>
		
		<table class="listtable2">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Kode Sumber"); ?></th>
		 <th><?php echo strtoupper("Nama Kode Sumber"); ?></th>
		 <th><?php echo strtoupper("Keterangan"); ?></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>"; ?>
  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
