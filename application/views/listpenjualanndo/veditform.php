<script language="javascript" type="text/javascript">
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

function getJatuhTempo(source,destination,range) { // d/m/Y

	var nilai;
	var tsplit	= source.split('/');
	var baru = tsplit['2']+'/'+tsplit['1']+'/'+tsplit['0'];
	var firstDay = new Date(baru);
	var test = firstDay.getTime() + parseInt(30 * 24 * 60 * 60 * 1000);
	var tanggal = new Date(test);
	var tgl	= tanggal.getDate();
	var bln = tanggal.getMonth()+1; // +1 karena getMonth itu startnya dari 0 (Januari), 1 (Februari), dst
	var thn = tanggal.getFullYear();

	nilai = tgl+'/'+bln+'/'+thn;
	destination.value	= nilai;
}

function tgpajak(tfaktur) {
	document.getElementById('d_pajak').value=tfaktur;
}

function konfirm() {

	var kon=window.confirm("yakin akan simpan ??");

	if(document.getElementById('i_branch').value=='') {
		alert('Pilih Cabang Pelanggan !!');
		return false;
	}

	if (kon) {
		return true;
	}else{
		return false;
	}
}

function ckfpenjualan(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualannondodisc/cform/cari_fpenjualan');?>",
	data:"fpenj="+nomor,
	success: function(data){
		$("#confnomorfpenj").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function ckfpajak(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualannondodisc/cform/cari_fpajak');?>",
	data:"fpajak="+nomor,
	success: function(data){
		$("#confnomorfpajak").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka!");
		angka.value	= 0;
		angka.focus();
	}
}

function tnilaibarang(){

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('n_total_nilai');

	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++){
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		unitdisc   = document.getElementById('n_discount_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseInt(unitprice.value);
		}
	}
	totalnilai.value	= Math.round(totaln);
	
}
function total(iterasi) {
	var	total;
	var	price;
	var	unit0;
	var ndisc;
	
	price	= document.getElementById('v_hjp_tblItem_'+iterasi);
	unit0	= document.getElementById('n_quantity_tblItem_'+iterasi);
	vdisc	= document.getElementById('v_discount_tblItem_'+iterasi).value;

	if(parseInt(price.value) && parseInt(unit0.value)) {
		total	= (parseInt(price.value) * parseInt(unit0.value))-(parseInt(vdisc) * parseInt(unit0.value));
    ndisc = (parseInt(vdisc)/parseInt(price.value))*100;
		document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
		document.getElementById('n_discount_tblItem_'+iterasi).value=ndisc;
	}
} //menghitung discrupiah

function totalndisc(iterasi) {
	var	total;
	var	price;
	var	unit0;
	var ndisc;
	
	price	= document.getElementById('v_hjp_tblItem_'+iterasi);
	unit0	= document.getElementById('n_quantity_tblItem_'+iterasi);
	ndisc	= document.getElementById('n_discount_tblItem_'+iterasi).value;

	if(parseInt(price.value) && parseInt(unit0.value)) {
		total	= (parseInt(price.value) * parseInt(unit0.value))-((parseInt(ndisc) * (parseInt(price.value) * parseInt(unit0.value)))/100);
    vdisc = (parseInt(ndisc)*parseInt(price.value))/100;
		document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
		document.getElementById('v_discount_tblItem_'+iterasi).value=vdisc;
	}
} //menghitung persen
function getdsk() {
	var nilai;
	var val;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;
	/*
	v_total_faktur
	n_ppn
	*/
	nilai	= document.getElementById('v_total_nilai');
	val		= document.getElementById('n_discount');

	if(parseInt(nilai.value) && (parseInt(val.value) || parseFloat(val.value))) {
		hasil 	= ( (parseInt(nilai.value)*parseFloat(val.value)) / 100 );
		total_sblm_ppn	= parseInt(nilai.value) - hasil;
		nilai_ppn	= ( (total_sblm_ppn*11) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;
		/*
		document.getElementById('v_discount').value = hasil;
		document.getElementById('v_total_faktur').value	= total_sblm_ppn;
		document.getElementById('n_ppn').value	= nilai_ppn;
		document.getElementById('v_total_fppn').value	= total_grand;
		*/
		//document.getElementById('v_discount').value = Math.round(hasil);
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= Math.round(xtotaln);
		
		nilai_ppn	= ((xtotaln*11) / 100);
		total_grand	= xtotaln + nilai_ppn;
		/*
		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= xtotaln;
		document.getElementById('n_ppn').value	= nilai_ppn;
		document.getElementById('v_total_fppn').value	= total_grand;
		*/		
		//document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}

	{
	
		var ptotaln=0;
		var punitprice;
		var ptotalnilai	= document.getElementById('v_discount2');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			punitprice  = document.getElementById('v_discount_tblItem_'+c);
			if(punitprice.value=='') {
				ptotaln += 0;
			} else {
				ptotaln += parseInt(punitprice.value);
			}
		}
		ptotalnilai.value	= Math.round(ptotaln);
		document.getElementById('v_discount').value = 0;

	}

}


function getdsk2() {
	
		var ztotaln=0;
		var zunitprice;
		var ztotalnilai	= document.getElementById('n_discount2');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			zunitprice  = document.getElementById('n_discount_tblItem_'+c);
			if(zunitprice.value=='') {
				ztotaln += 0; 
			} else {
				ztotaln += parseFloat(zunitprice.value);
			}
		}
		ztotalnilai.value	= ztotaln.toFixed(3) / lastRow;
		document.getElementById('n_discount').value = 0;
	}



function addRowToTable(nItem) {
	
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	
	tbl.width='100%';
										
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	}else{
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:24px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:120px;\"><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:100px;\" onfocus=\"total("+iteration+");tnilaibarang();getdsk();getdsk2();\"><img name=\"img_i_product_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualanndo('"+iteration+"','"+document.getElementById('i_branch').value+"', '"+document.getElementById('is_pakai_sj').value+"');\"></DIV>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:240px;\" ><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:200px;\"></DIV>";
	
	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_v_hjp_"+nItem+"_"+iteration+"\" style=\"width:110px;\" ><input type=\"text\" ID=\"v_hjp_"+nItem+"_"+iteration+"\"  name=\"v_hjp_"+nItem+"_"+iteration+"\" style=\"width:130px;text-align:right;\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_n_quantity_"+nItem+"_"+iteration+"\" style=\"width:80px;\" ><input type=\"text\" ID=\"n_quantity_"+nItem+"_"+iteration+"\"  name=\"n_quantity_"+nItem+"_"+iteration+"\" style=\"width:60px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();getdsk();getdsk2();validNum('n_quantity_tblItem','"+iteration+"');\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(5);
	cell1.innerHTML = "<DIV ID=\"ajax_v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:110px;text-align:right;\" ><input type=\"text\" ID=\"v_unit_price_"+nItem+"_"+iteration+"\"  name=\"v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:140px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();validNum('v_unit_price_tblItem','"+iteration+"');\" value=\"0\" ><input type=\"hidden\" name=\"iteration\" value=\""+iteration+"\" ><input type=\"hidden\" ID=\"isjcode_"+nItem+"_"+iteration+"\"  name=\"isjcode_"+nItem+"_"+iteration+"\"><input type=\"hidden\" ID=\"n_quantity_hidden_"+nItem+"_"+iteration+"\"  name=\"n_quantity_hidden_"+nItem+"_"+iteration+"\" ></DIV>";


	var cell1 = row.insertCell(6);
	cell1.innerHTML = "<DIV ID=\"ajax_n_discount_"+nItem+"_"+iteration+"\" style=\"width:110px;\" ><input type=\"text\" ID=\"n_discount_"+nItem+"_"+iteration+"\"  name=\"n_discount_"+nItem+"_"+iteration+"\" style=\"width:80px;text-align:right;\" onkeyup=\"totalndisc("+iteration+");getdsk();getdsk2();\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(7);
	cell1.innerHTML = "<DIV ID=\"ajax_v_discount_"+nItem+"_"+iteration+"\" style=\"width:80px;\" ><input type=\"text\" ID=\"v_discount_"+nItem+"_"+iteration+"\"  name=\"v_discount_"+nItem+"_"+iteration+"\" style=\"width:80px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();getdsk();getdsk2();validNum('n_quantity_tblItem','"+iteration+"');getdsk();getdsk2();\" value=\"0\" ></DIV>";




}


function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}

</script>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#d_faktur").datepicker();
	$("#d_due_date").datepicker();
	$("#d_pajak").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_fpenjualanndo; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_fpenjualanndo; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		  <?php 
		  	$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listpenjualanndo/cform/actedit', $attributes);?>

		  <input type="hidden" name="is_pakai_sj" id="is_pakai_sj" value="<?php echo $is_pakai_sj ?>">
		<div id="masterfpenjualandoform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $form_nomor_f_fpenjualanndo; ?> : 
							  <input name="i_faktur" type="text" id="i_faktur" maxlength="13" value="<?php echo $ifakturcode; ?>" onkeyup="ckfpenjualan(this.value);"/>
							  <div id="confnomorfpenj" style="color:#FF0000;"></div>
							  </td>
							<td width="33%"><?php echo $form_tgl_f_fpenjualanndo; ?> : 
							  <input name="d_faktur" type="text" id="d_faktur" maxlength="10" value="<?php echo $tgFAKTUR; ?>" onchange="getJatuhTempo(this.value,document.getElementById('d_due_date'),29); tgpajak(this.value);" />
							  <!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_faktur,'dd/mm/yyyy',this)"> -->
							  </td>
							<td width="33%"><?php echo $form_cabang_fpenjualanndo; ?> : 
							  <!-- <select name="i_branch" id="i_branch" onchange="show_pel_manual();"> -->
							  <select name="i_branch" id="i_branch" >
								<option value="">[ <?php echo $form_pilih_cab_fpenjualanndo;?> ]</option>
								<!-- <option value="1">[ <?php echo $form_pilih_cab_manual_fpenjualanndo;?> ]</option> -->
								<?php
									foreach($opt_cabang as $row) {
										$sel	= $ibranchname==$row->einitial?"selected":"";
										$lcabang	.= "<option value=".$row->einitial." $sel >";
										$lcabang	.= $row->branch." ( ".$row->einitial." ) ";
										$lcabang	.= "</option>";
									}
									echo $lcabang;
								?>								
							  </select>&nbsp;&nbsp;<span style="color:#FF0000">*</span>
							 <!--  <input type="text" name="txt_pel_manual" id="txt_pel_manual" style="width:165px; display:none;" readonly="true" onclick="shpelangganfpenjualanndo();"/> -->
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>					
					<tr>
					 <td>
					  <div id="title-box2"><?php echo $form_detail_f_fpenjualanndo; ?>
					  <div style="float:right;">
					  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');total(document.getElementById('tblItem').rows.length);getdsk();getdsk2();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
					  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');total((document.getElementById('tblItem').rows.length)-1);getdsk();getdsk2();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
					  </div></div>
					 </td>	
					</tr>
					<tr>
			<td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="3%" class="tdatahead">NO</td>
                          <td width="10%" class="tdatahead"><?php echo $form_kd_brg_fpenjualanndo; ?></td>
                          <td width="15%" class="tdatahead"><?php echo $form_nm_brg_fpenjualanndo; ?></td>
                          <td width="9%" class="tdatahead"><?php echo $form_hjp_fpenjualanndo; ?></td>
                          <td width="5%" class="tdatahead"><?php echo $form_qty_fpenjualanndo; ?></td>
                          <td width="10%" class="tdatahead"><?php echo $form_nilai_fpenjualanndo; ?></td>
			  <td width="6%" class="tdatahead"><?php echo $form_ndiscount_fpenjualanndo; ?></td>
			  <td width="7%" class="tdatahead"><?php echo $form_vdiscount_fpenjualanndo; ?></td>

			  

                          
                        </tr>
						<tr>
						 <td colspan="8">
						  <table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
							<?php
							$iter	= 0;
							$xtotaln	= 0;
							foreach($fakturitem as $row3) {
								
								$xtotaln+=$row3->amount;	
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:25px;margin-right:0px;\">".($iter*1+1)."</div></td>
									<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:150px;\"><input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:120px;\" onfocus=\"total('".$iter."');tnilaibarang();getdsk();getdsk2();\" value=\"".$row3->i_product."\"><img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualanndox('".$iter."',document.getElementById('i_branch').value, document.getElementById('is_pakai_sj').value);\"></td>
									
									<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:200px;margin-right:5px\" ><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\" name=\"e_product_name_tblItem_".$iter."\" style=\"width:200px;\" value=\"".$row3->e_product_name."\"></DIV></td>
									
									<td><DIV ID=\"ajax_v_hjp_tblItem_".$iter."\" style=\"width:130px;margin-right:5px\" ><input type=\"text\" ID=\"v_hjp_tblItem_".$iter."\"  name=\"v_hjp_tblItem_".$iter."\" style=\"width:130px;text-align:right;\" value=\"".$row3->v_unit_price."\" onkeyup=\"total('".$iter."');tnilaibarang();getdsk();getdsk2();\" ></DIV></td>
									
									<td><DIV ID=\"ajax_n_quantity_tblItem_".$iter."\" style=\"width:60px;margin-right:5px\" ><input type=\"text\" ID=\"n_quantity_tblItem_".$iter."\"  name=\"n_quantity_tblItem_".$iter."\" style=\"width:60px;text-align:right;\" onkeyup=\"total('".$iter."');tnilaibarang();validNum('n_quantity_tblItem','".$iter."');getdsk();getdsk2();\" value=\"".$row3->n_quantity."\" ></DIV></td>

<td>

<DIV ID=\"ajax_v_unit_price_tblItem_".$iter."\" style=\"width:140px;margin-right:5px;\">

<input type=\"text\" ID=\"v_unit_price_tblItem_".$iter."\"  name=\"v_unit_price_tblItem_".$iter."\" style=\"width:140px;text-align:right;\" onkeyup=\"total('".$iter."');tnilaibarang();validNum('v_unit_price_tblItem','".$iter."');getdsk();getdsk2();\" value=\"".$row3->amount."\">

<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\">

<input type=\"hidden\" ID=\"isjcode_tblItem_".$iter."\"  name=\"isjcode_tblItem_".$iter."\" value=\"".$row3->i_sj."\"><input type=\"hidden\" ID=\"n_quantity_hidden_tblItem_".$iter."\"  name=\"n_quantity_hidden_tblItem_".$iter."\">


</DIV>
</td>

<td><DIV ID=\"ajax_n_discount_tblItem_".$iter."\" style=\"width:100px;\" ><input type=\"text\" ID=\"n_discount_tblItem_".$iter."\"  name=\"n_discount_tblItem_".$iter."\" style=\"width:80px;text-align:right;\" value=\"".$row3->n_discount."\" onkeyup=\"totalndisc('".$iter."');getdsk();getdsk2();\" ></DIV></td>

<td><DIV ID=\"ajax_v_discount_tblItem_".$iter."\" style=\"width:85px;margin-right:20px\" ><input type=\"text\" ID=\"v_discount_tblItem_".$iter."\"  name=\"v_discount_tblItem_".$iter."\" style=\"width:80px;text-align:right;\" value=\"".$row3->v_discount."\" onkeyup=\"total('".$iter."');getdsk();getdsk2();\" ></DIV></DIV></td>

								</tr>";
								$iter++;
							}		
							
							$nilai_ppn	= ( ($xtotaln*11) / 100 );
							$total_grand	= $xtotaln + $nilai_ppn;
							
							if($n_discount>0 || $n_discount!=0){
								$hasil 	= ( ($nilai_ppn*$n_discount) / 100 ); // dikurangi discount
								$total_sblm_ppn	= round($nilai_ppn - $hasil); // DPP
								$nilai_ppn2	= round((($total_sblm_ppn*11) / 100 ));
								$total_grand2	= round($total_sblm_ppn + $nilai_ppn2);
							}else{
								$total_sblm_ppn	= round($xtotaln); // DPP
								$nilai_ppn2	= round($nilai_ppn);
								$total_grand2	= round($total_grand);							
							}
										
							?>						  
						  </table>
						</tr>
                      </table></td>
					</tr>					
					<tr>
					 <td>
					  <div id="title-box2">
					  <div style="float:right;">
					  &nbsp;
					  </div></div>
					 </td>	
					</tr>					
					<tr>
					  <td valign="top"><table width="98%" border="0" cellpadding="0" cellspacing="0">
						<tr valign="top">
						  <td width="16%" rowspan="2"><?php echo $form_ket_f_fpenjualanndo; ?></td>
						  <td width="1%">:</td>
						  <td colspan="2" rowspan="2">
						  <?php
						  	$enotefaktur = array(
								'name'=>'e_note_faktur',
								'id'=>'e_note_faktur',
								'cols'=>2,
								'rows'=>1,
								'value'=>$e_note_faktur
							);
							echo form_textarea($enotefaktur);
						  ?></td>
						  <td width="14%"><?php echo $form_tnilai_fpenjualanndo; ?></td>
						  <td width="1%">:</td>
						  <td width="21%">
							<input name="v_total_nilai" type="text" id="v_total_nilai" size="15" maxlength="15" value="<?=$xtotaln?>" />						  </td>
						</tr>
<!-- ======================================================================================================================================= -->
						
						
						<tr>
						  <td>&nbsp;</td>
						  <td><?php echo $form_diskon_fpenjualanndo; ?> Total Diskon Rp.</td>
						  <td>:</td>
						  <td>

						  <input name="v_discount" type="hidden" id="v_discount" size="10" maxlength="10" value="0" />

						 <input name="v_discount" type="text" id="v_discount2" size="10" maxlength="10" value="<?php echo $v_discount; ?>" readonly />
						 <input name="n_discount" type="hidden" id="n_discount" size="10" maxlength="10" value="0" />

						 <input name="n_discount" type="text" id="n_discount2" size="10" maxlength="10" on value="<?php echo $n_discount; ?>" readonly />
						</tr>

						<tr>

<!-- ======================================================================================================================================= -->





						<tr>
						  <td><?php echo $form_tgl_jtempo_fpenjualanndo; ?> </td>
						  <td>:</td>
						  <td>
							<input name="d_due_date" type="text" id="d_due_date" size="10" maxlength="10" value="<?php echo $tjthtempo; ?>"/>
							<!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_due_date,'dd/mm/yyyy',this)"> --></td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_total_fpenjualanndo; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_faktur" type="text" id="v_total_faktur" size="15" maxlength="15" value="<?=$total_sblm_ppn?>" />						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_no_fpajak_fpenjualanndo; ?></td>
						  <td>:</td>
						  <td>
							<input name="i_faktur_pajak" type="text" id="i_faktur_pajak" size="15" maxlength="18" value="<?php echo $i_faktur_pajak; ?>"/>						  </td>
						  <td><?php echo $form_tgl_fpajak_fpenjualanndo; ?> :
							<input name="d_pajak" type="text" id="d_pajak" size="10" maxlength="10"  value="<?php echo $tgPajak; ?>"/>
							<!-- <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_pajak,'dd/mm/yyyy',this)"> --></td> 
						  <td><?php echo $form_ppn_fpenjualanndo; ?></td>
						  <td>:</td>
						  <td>
							<input name="n_ppn" type="text" id="n_ppn" size="15" maxlength="15" value="<?=$nilai_ppn2?>" /></td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>
							<input type="checkbox" name="f_cetak" value="1" <?=$f_printed?>/>
							&nbsp;<?php echo $form_ket_cetak_fpenjualanndo; ?></td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_grand_t_fpenjualanndo; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_fppn" type="text" id="v_total_fppn" size="15" maxlength="15" value="<?=$total_grand2?>" /></td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td colspan="7"><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:245px; padding:5px;">Dari input Surat Jalan (SJ)</div></td>
						</tr>
					  </table></td>
					</tr>
					<tr align="right">
					  <td>
					  	<!-- <input type="hidden" name="id_pel_manual" id="id_pel_manual"/> -->
						<!-- <input name="btnsimpan" type="submit" id="btnsimpan" value="<?php // echo $button_simpan; ?>" onclick="return ck_pel_manual();" />
						<input name="btnbatal" type="reset" id="btnbatal" value="<?php // echo $button_batal; ?>" onclick="return batal_pel_manual();" /> -->
						<input type="hidden" name="ifakturhiden" id="ifakturhiden" value="<?=$i_faktur?>" />
						<input type="hidden" name="ifakturcodehiden" id="ifakturcodehiden" value="<?=$ifakturcode?>" />
						
						<input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit" />
						<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listpenjualanndo/cform/'">

					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
