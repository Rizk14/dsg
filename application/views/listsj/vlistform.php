<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_sj; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_sj; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listsj/cform/carilistsj','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlsjform">

		  <table width="95%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_tgl_mulai_sj; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					  <input name="d_sj_first" type="text" id="d_sj_first" maxlength="10" value="<?php echo $tglsjmulai; ?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_sj_first,'dd/mm/yyyy',this)">
					s.d
					<input name="d_sj_last" type="text" id="d_sj_last" maxlength="10" value="<?php echo $tglsjakhir; ?>"/>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_sj_last,'dd/mm/yyyy',this)">            		</td>
				  </tr>
				  <tr>
					<td width="33%">Nomor Surat Jalan (SJ)</td>
					<td width="0%">:</td>
					<td>
					<?php
					$sj	= array(
					'name'=>'isj',
					'id'=>'isj',
					'type'=>'text',
					'maxlength'=>'14',
					'style'=>'width:100px;',
					'value'=>$isj
					);
					echo form_input($sj);
					?>
					</td>
				  </tr>	  
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_sj; ?></div></td>	
			</tr>
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="3%" class="tdatahead">NO</td>
				  <td width="10%" class="tdatahead"><?php echo $list_no_sj; ?></td>
				  <td width="15%" class="tdatahead"><?php echo $list_kd_brg_sj; ?> </td>
				  <td width="30%" class="tdatahead"><?php echo $list_nm_brg_sj; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_unit_sj; ?></td>
				  <td width="10%" class="tdatahead"><?php echo $list_unit_belumfaktur_sj; ?></td>
				  <td width="4%" class="tdatahead"><?php echo $list_hjp_sj; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_harga_sj; ?> </td>
				  <td width="20%" class="tdatahead"><?php echo $link_aksi; ?> </td>
				</tr>
				<?php
				$no	= 1;
				$cc	= 1;

				$bln	= array(
						'01'=>'Januari', 
						'02'=>'Februari', 
						'03'=>'Maret', 
						'04'=>'April', 
						'05'=>'Mei', 
						'06'=>'Juni', 
						'07'=>'Juli', 
						'08'=>'Agustus', 
						'09'=>'September', 
						'10'=>'Oktober', 
						'11'=>'Nopember', 
						'12'=>'Desember' );
						
				
				if(count($query) > 0 ) {
					$xx = 'f';
					foreach($query as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

						$tgl			= (!empty($row->dsj) || strlen($row->dsj)!=0)?@explode("-",$row->dsj,strlen($row->dsj)):"";
						$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
						$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
						
						if($tglsjmulai==''){
							
							if($row->sjfaktur=='f') {
								if($xx=='f'){
									$laksi	= "<td class=\"tdatahead2\" align=\"center\">
										<a href=\"javascript:void(0)\" onclick='show(\"listsj/cform/edit/".$row->isj."/".$row->dsj."\",\"#content\")' title=\"Edit SJ\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit SJ\"></a>&nbsp;
										<a href=\"javascript:void(0)\" onclick='show(\"index.php/listsj/cform/undo/".$row->isj."\",\"#content\")' title=\"Cancel SJ\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel SJ\"></a>
									</td>";
									$xx='t';
								}else{
									$laksi	= "<td class=\"tdatahead2\" align=\"center\"></td>";
								}
							} else {
								$laksi	= "<td class=\"tdatahead2\" align=\"center\"></td>";
							}

						}else{
							$laksi	= "<td class=\"tdatahead2\" align=\"center\"></td>";	
						}

						$lsj	.= "
							<tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
							  <td class=\"tdatahead2\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
							  <td class=\"tdatahead2\">".$row->isjcode."</td>
							  <td class=\"tdatahead2\">".$row->iproduct."</td>
							  <td class=\"tdatahead2\">".$row->productname."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->unit."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->belumfaktur."</td>
							  <td class=\"tdatahead2\" align=\"right\">".number_format($row->hjp,'2','.',',')."</td>
							  <td class=\"tdatahead2\" align=\"right\">".number_format($row->harga,'2','.',',')."</td>
							  ".$laksi."
							</tr>";
							
							$no+=1;
							$cc+=1;
					}
					echo $lsj;
				}
				?>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>			
			<tr>
				
			  <td align="right">
				   <input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listsj/cform/'">
				  </td>
			</tr>
		  </table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
