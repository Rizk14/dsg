<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_opvsdo; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_opvsdo; ?></td></tr>
   <tr>
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php
		echo $this->pquery->form_remote_tag(array('url'=>'expoopvsdogrosir/cform/','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlopvsdoform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td><?php echo $list_opvsdo_kd_brg; ?></td>
							<td>:</td>
							<td>
							  <input name="i_product" type="text" id="i_product" maxlength="100" value="<?=$kproduksi?>" />
							</td>
						  </tr>
						  <tr>
							<td width="19%"><?php echo $list_opvsdo_tgl_mulai_op; ?> </td>
							<td width="0%">&nbsp;</td>
							<td width="81%">
							  <input name="d_op_first" type="text" id="d_op_first" maxlength="10" value="<?=$tglopmulai?>"/>
							  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_first,'dd/mm/yyyy',this)">
							s.d 
							<input name="d_op_last" type="text" id="d_op_last" maxlength="10" value="<?=$tglopakhir?>" />
							<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_last,'dd/mm/yyyy',this)">
							</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							  <input name="f_stop_produksi" type="checkbox" id="f_stop_produksi" value="1" <?=$stop?> />
							<?php echo $list_opvsdo_stop_produk; ?></td>
						  </tr>
						  
						  <tr>
							<td>Pelanggan</td>
							<td></td>
							<td>
							<select name="customer" id="customer">
							 <option value="">[ PILIH PELANGGAN ]</option>
							 <?php
							 foreach($customer as $rowcustomer) {
								 $Sel = ($icustomer==$rowcustomer->i_customer)?("selected"):("");
								 echo "<option value=\"$rowcustomer->i_customer\" $Sel >".$rowcustomer->e_customer_name."</option>";
							 }
							 ?>
							</select>
							</td>
						  </tr>

						  <tr>
							<td>OP Drop Forcast</td>
							<td></td>
							<td>
							<select name="fdropforcast" id="fdropforcast">
							 <option value="0" <?php if($fdropforcast=='0') { echo "selected"; } ?> >Tampilan Semua</option>
							 <option value="1" <?php if($fdropforcast=='1') { echo "selected"; } ?> >Ya</option>
							 <option value="2" <?php if($fdropforcast=='2') { echo "selected"; } ?> >Bukan</option>
							</select>
							</td>
						  </tr>
						  						  
						</table>
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td>
			  	<div id="title-box2"><?php echo $form_title_detail_opvsdo; ?></div>
			  	</td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="2%" class="tdatahead">NO</td>
					<td width="3%" align="center" class="tdatahead"><?php echo strtoupper($list_opvsdo_stop_produk); ?> </td>
					<td width="7%" align="center" class="tdatahead"><?php echo strtoupper($list_opvsdo_kd_brg); ?> </td>
					<td width="25%" align="center" class="tdatahead"><?php echo $list_opvsdo_nm_brg; ?> </td>
					<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_unit_price; ?> </td>
					<td width="5%" align="center" class="tdatahead"><?php echo $list_opvsdo_op; ?></td>
					<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_n_op; ?> </td>
					<td width="5%" align="center" class="tdatahead"><?php echo $list_opvsdo_do; ?></td>
					<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_n_do; ?> </td>
					<td width="5%" align="center" class="tdatahead"><?php echo $list_opvsdo_selisih_opdo; ?> </td>
					<td width="8%" align="center" class="tdatahead"><?php echo $list_opvsdo_n_selisih; ?></td>
					<td width="8%" align="center" class="tdatahead">QTY TOTAL DI FAKTUR</td>
				  </tr>
				  
				  <?php
				  
				  $totalpermintaan	= 0;
				  $nilaipermintaan	= 0;
				  $totalpengiriman	= 0;
				  $nilaipenjualan	= 0;
				  $selisihtotal	= 0;
				  $selisihnilai	= 0;
				  $totalqtyfaktur	= 0;
				  
				  if(sizeof($isi) > 0) {
				  	$no	= 1;
					$cc	= 1;
				  	foreach($isi as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$sproduct	= $row->stopproduct=='t'?"checked":"";
						
						// 12-07-2012
						// ambil jml OP
						$qjmlopall	= $this->mclass->jumopall($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$row->imotif,$icustomer,$fdropforcast);
						if($qjmlopall->num_rows()>0) {
							$rjmlopall	= $qjmlopall->row();
							$jmlorderall	= $rjmlopall->jmlorder;
						}
						else
							$jmlorderall = 0;

						$qjmlopgrosir	= $this->mclass->jmlorder_new($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$row->imotif,$icustomer,$fdropforcast, 1);
						/*if($qjmlopgrosir->num_rows()>0) {
							$rjmlopgrosir	= $qjmlopgrosir->row();
							$jmlordergrosir	= $rjmlopgrosir->jmlorder;
						}
						else
							$jmlordergrosir = 0; */
						if (is_array($qjmlopgrosir))
							$jmlordergrosir = $qjmlopgrosir['jmlorder'];
						else
							$jmlordergrosir = 0;
							
						if ($is_grosir == '2')
							$selisihnya = $jmlorderall-$jmlordergrosir;
						else
							$selisihnya = $jmlordergrosir;
						
						$qjmlorderpemenuhan	= $this->mclass->jmlorder_new($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$row->imotif,$icustomer,$fdropforcast,$is_grosir);
						/*if($qjmlorderpemenuhan->num_rows()>0) {
							$rjmlorderpemenuhan	= $qjmlorderpemenuhan->row(); */

							/*$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row->imotif,$rjmlorderpemenuhan->i_customer);
							$qhargadefault 	    = $this->mclass->hargadefault($row->imotif);
							
							if($qhargaperpelanggan->num_rows()>0) {
								$rhargaperpelanggan = $qhargaperpelanggan->row();
								
								$hargaperunit = $rhargaperpelanggan->v_price;
							
							}elseif($qhargadefault->num_rows()>0) {
								$rhargadefault = $qhargadefault->row();
								
								$hargaperunit = $rhargadefault->v_price;
								
							}else{ */
							//	$hargaperunit = $row->unitprice;
							//}
							
							//$jmlorder	= $rjmlorderpemenuhan->jmlorder;
						/*	if ($is_grosir == '1')
								$jmlorder	= $rjmlorderpemenuhan->jmlorder;
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $rjmlorderpemenuhan->pemenuhan;
							//$nilaiorder	= $rjmlorderpemenuhan->jmlorder*$hargaperunit;
							$nilaiorder	= $jmlorder*$hargaperunit;
							$nilaipemenuhan	= $rjmlorderpemenuhan->pemenuhan*$hargaperunit;
							//$selisihopdo	= $rjmlorderpemenuhan->jmlorder - $rjmlorderpemenuhan->pemenuhan;
							$selisihopdo	= $jmlorder - $rjmlorderpemenuhan->pemenuhan;
							$nilaiselisih	= $selisihopdo*$hargaperunit;
						}*/
						if (is_array($qjmlorderpemenuhan)) {
							$hargaperunit = $row->unitprice;
							
							//$jmlorder	= $rjmlorderpemenuhan->jmlorder;
							if ($is_grosir == '1')
								$jmlorder	= $qjmlorderpemenuhan['jmlorder'];
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $qjmlorderpemenuhan['pemenuhan'];
							//$nilaiorder	= $rjmlorderpemenuhan->jmlorder*$hargaperunit;
							$nilaiorder	= $jmlorder*$hargaperunit;
							$nilaipemenuhan	= $qjmlorderpemenuhan['pemenuhan']*$hargaperunit;
							//$selisihopdo	= $rjmlorderpemenuhan->jmlorder - $rjmlorderpemenuhan->pemenuhan;
							$selisihopdo	= $jmlorder - $qjmlorderpemenuhan['pemenuhan'];
							$nilaiselisih	= $selisihopdo*$hargaperunit;
						}
						else{
							$jmlorder	= 0;
							$pemenuhan = 0;
							$nilaiorder	= 0;
							$nilaipemenuhan	= 0;
							$selisihopdo	= 0;
							$nilaiselisih	= 0;
							$hargaperunit = $row->unitprice;
						}						
						// ==== end 12-07-2012 =====
						
						/*$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, 
								tm_faktur_do_item_t b, tr_product_base c, tr_product_motif d where 
								a.i_faktur = b.i_faktur AND a.f_faktur_cancel = 'f'
								AND c.i_product_base = d.i_product
								AND d.i_product_motif = b.i_product";

						$sqlxx.= " AND c.f_stop_produksi = '$row->stopproduct'
								AND a.d_faktur >='$var_ddofirst' AND a.d_faktur <='$var_ddolast' AND b.i_product ='$row->imotif'"; */
						
						$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, tm_faktur_do_item_t b, tm_do c,
								tr_product_base d, tr_product_motif e  
								where a.i_faktur = b.i_faktur AND c.i_do = b.i_do
								AND d.i_product_base = e.i_product AND e.i_product_motif = b.i_product
								AND ((a.d_faktur >= '$var_ddofirst' AND a.d_faktur <= '$var_ddolast') OR (c.d_do >='$var_ddofirst' 
								AND c.d_do<='$var_ddolast')) AND b.i_product = '".$row->imotif."' AND a.f_faktur_cancel = 'f' 
								AND c.f_do_cancel = 'f' AND d.f_stop_produksi = '".$row->stopproduct."' ";
						if ($icustomer != '')
							$sqlxx.=" AND c.i_customer = '$icustomer' ";
							
						$query3	= $this->db->query($sqlxx);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_faktur	= $hasilrow->jumnya;
							if ($qty_faktur == '')
								$qty_faktur = 0;
						}
											 
						$lopvsdo	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td><input type=\"checkbox\" value=\"1\" $sproduct></td>
								<td>".$row->imotif."</td>
								<td>".$row->productmotif."</td>
								<td align=\"right\">".number_format($hargaperunit,'2','.',',')."</td>
								<td align=\"right\">".$jmlorder."</td>
								<td align=\"right\">".number_format($nilaiorder,'2','.',',')."</td>
								<td align=\"right\">".$pemenuhan."</td>
								<td align=\"right\">".number_format($nilaipemenuhan,'2','.',',')."</td>
								<td align=\"right\">".$selisihopdo."</td>
								<td align=\"right\">".number_format($nilaiselisih,'2','.',',')."</td>
								<td align=\"right\">".$qty_faktur."</td>
							  </tr>";	
							  						  
						$no+=1; $cc+=1;
						/*
						$totalpermintaan+=$rjmlorderpemenuhan->jmlorder;
						$nilaipermintaan+=$nilaiorder;
						$totalpengiriman+=$rjmlorderpemenuhan->pemenuhan;
						$nilaipenjualan+=$nilaipemenuhan;
						*/ 

						$totalpermintaan+=$jmlorder;
						$nilaipermintaan+=$nilaiorder;
						$totalpengiriman+=$pemenuhan;
						$nilaipenjualan+=$nilaipemenuhan;
						$totalqtyfaktur+= $qty_faktur;	
						
					}
					
					// 13-08-2012
					if (is_array($isixx)) {
						for($j=0;$j<count($isixx);$j++){
					//foreach($isixx as $rowxx) {
						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

						$sproduct	= $isixx[$j]['stopproduct']=='t'?"checked":"";
						
						// ambil jml OP
						$qjmlopall	= $this->mclass->jumopall($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$isixx[$j]['imotif'],$icustomer,$fdropforcast);
						if($qjmlopall->num_rows()>0) {
							$rjmlopall	= $qjmlopall->row();
							$jmlorderall	= $rjmlopall->jmlorder;
						}
						else
							$jmlorderall = 0;
						//if ($rowxx->imotif == 'TPT070100')
						//	echo "dodol ".$jmlorderall;
						
						$qjmlopgrosir	= $this->mclass->jmlorder_new($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$isixx[$j]['imotif'],$icustomer,$fdropforcast, 1);
												
						if (is_array($qjmlopgrosir))
							$jmlordergrosir = $qjmlopgrosir['jmlorder'];
						else
							$jmlordergrosir = 0;

						if ($is_grosir == '2')
							$selisihnya = $jmlorderall-$jmlordergrosir;
						else
							$selisihnya = $jmlordergrosir; 
												
						$qjmlorderpemenuhan	= $this->mclass->jmlorder_new($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$isixx[$j]['imotif'],$icustomer,$fdropforcast,$is_grosir);
						if (is_array($qjmlorderpemenuhan)) {
							$hargaperunit = $isixx[$j]['unitprice'];
							if ($is_grosir == '1')
								$jmlorder	= $qjmlorderpemenuhan['jmlorder'];
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $qjmlorderpemenuhan['pemenuhan'];
							$nilaiorder	= $jmlorder*$hargaperunit;
							$nilaipemenuhan	= $qjmlorderpemenuhan['pemenuhan']*$hargaperunit;
							$selisihopdo	= $jmlorder - $qjmlorderpemenuhan['pemenuhan'];
							$nilaiselisih	= $selisihopdo*$hargaperunit;
						}else{
							$hargaperunit = $isixx[$j]['unitprice'];
							$jmlorder	= $selisihnya;
							$pemenuhan = 0;
							$nilaiorder	= $selisihnya*$hargaperunit;
							$nilaipemenuhan	= 0;
							$selisihopdo	= $selisihnya;
							$nilaiselisih	= $selisihnya*$hargaperunit;
							
						}						
						
					/*	$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, 
								tm_faktur_do_item_t b, tr_product_base c, tr_product_motif d where 
								a.i_faktur = b.i_faktur AND a.f_faktur_cancel = 'f'
								AND c.i_product_base = d.i_product
								AND d.i_product_motif = b.i_product";

						$sqlxx.= " AND c.f_stop_produksi = '".$var_stopproduct."'
								AND a.d_faktur >='$var_ddofirst' AND a.d_faktur <='$var_ddolast' AND b.i_product ='".$isixx[$j]['imotif']."'";
					*/
					
					$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, tm_faktur_do_item_t b, tm_do c,
								tr_product_base d, tr_product_motif e  
								where a.i_faktur = b.i_faktur AND c.i_do = b.i_do
								AND d.i_product_base = e.i_product AND e.i_product_motif = b.i_product
								AND ((a.d_faktur >= '$var_ddofirst' AND a.d_faktur <= '$var_ddolast') OR (c.d_do >='$var_ddofirst' 
								AND c.d_do<='$var_ddolast')) AND b.i_product = '".$isixx[$j]['imotif']."' AND a.f_faktur_cancel = 'f' 
								AND c.f_do_cancel = 'f' AND d.f_stop_produksi = '".$var_stopproduct."' ";
						if ($icustomer != '')
							$sqlxx.=" AND c.i_customer = '$icustomer' ";
								
						$query3	= $this->db->query($sqlxx);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_faktur	= $hasilrow->jumnya;
							
							if ($qty_faktur == '')
								$qty_faktur = 0;
						}
						
						$lopvsdo	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td><input type=\"checkbox\" value=\"1\" $sproduct></td>
								<td>".$isixx[$j]['imotif']."</td>
								<td>".$isixx[$j]['productmotif']."</td>
								<td align=\"right\">".number_format($hargaperunit,'2','.',',')."</td>
								<td align=\"right\">".$jmlorder."</td>
								<td align=\"right\">".number_format($nilaiorder,'2','.',',')."</td>
								<td align=\"right\">".$pemenuhan."</td>
								<td align=\"right\">".number_format($nilaipemenuhan,'2','.',',')."</td>
								<td align=\"right\">".$selisihopdo."</td>
								<td align=\"right\">".number_format($nilaiselisih,'2','.',',')."</td>
								<td align=\"right\">".$qty_faktur."</td>
							  </tr>";
						
						$no+=1; $cc+=1;
						
						$totalpermintaan+=$jmlorder;
						$nilaipermintaan+=$nilaiorder;
						$totalpengiriman+=$pemenuhan;
						$nilaipenjualan+=$nilaipemenuhan;
						$totalqtyfaktur+= $qty_faktur;
					}
				}
					$selisihtotal	= ($totalpermintaan - $totalpengiriman);
					$selisihnilai	= ($nilaipermintaan - $nilaipenjualan);
					
					echo $lopvsdo;
				  }
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>	  
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="15%"><?php echo $list_opvsdo_t_permintaan; ?> </td>
							<td width="1%">:</td>
							<td width="35%">
							  <input name="t_permintaan" type="text" id="t_permintaan" maxlength="20" value="<?php echo $totalpermintaan; ?>"/>
							</td>
							<td width="15%"><?php echo $list_opvsdo_t_pengiriman; ?> </td>
							<td width="1%">:</td>
							<td width="%">
							  <input name="t_pengiriman" type="text" id="t_pengiriman" maxlength="20" value="<?php echo $totalpengiriman; ?>"/>
								<br>Total Di Faktur : <?php echo $totalqtyfaktur; ?>
							</td>
						  </tr>
						  <tr>
							<td><?php echo $list_opvsdo_n_permintaan; ?></td>
							<td>:</td>
							<td>
							  <input name="n_permintaan" type="text" id="n_permintaan" maxlength="20" value="<?php echo number_format($nilaipermintaan,'2','.',','); ?>"/>
							</td>
							<td><?php echo $list_opvsdo_n_penjualan; ?> </td>
							<td>:</td>
							<td>
							  <input name="n_penjualan" type="text" id="n_penjualan" maxlength="20" value="<?php echo number_format($nilaipenjualan,'2','.',','); ?>"/>
							</td>
						  </tr>
						  <tr>
							<td><?php echo $list_opvsdo_selisih; ?></td>
							<td>:</td>
							<td>
							  <input name="selisih" type="text" id="selisih" maxlength="20" value="<?php echo $selisihtotal; ?>"/>
							</td>
							<td><?php echo $list_opvsdo_t_n_selisih; ?></td>
							<td>:</td>
							<td>
							  <input name="t_n_selisih" type="text" id="t_n_selisih" maxlength="20" value="<?php echo number_format($selisihnilai,'2','.',','); ?>"/>
							</td>
						  </tr>
						</table>
				</td>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	<td align="right">
				<input type="button" name="btnkeluar" value="<?php echo $button_keluar; ?>" onclick="show('expoopvsdogrosir/cform','#content')"/>
				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
