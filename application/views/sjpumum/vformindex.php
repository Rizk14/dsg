<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:400px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

.legend {
    display: block;
    width: 100%;
    padding: 5px 15px;
    margin-bottom: unset !important;
    font-size: 16px;
    line-height: inherit;
    color: #333;
    border: 0;
    border: 1px solid #e5e5e5;
    background: white;
    border-radius: 5px;
}

.print2 {
    padding-top: 2px; padding-left: 5px; height: 24px; width: 24px
}



</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>


<link rel="stylesheet" href="https://cdn.datatables.net/1.13.5/css/dataTables.bootstrap.min.css">
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-3.7.0.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap.min.js"></script>



<script type="text/javascript"> 
$(function() {
	$('#filter_brg').click(function(){
	  	    if ($("#filter_brg").is(":checked")) {
				$('#cari_brg').attr('disabled', false);
				$('#cari_brg').val('');
			}
			else {
				$('#cari_brg').attr('disabled', true);
				$('#cari_brg').val('');
			}
	  });
});

function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function validateFilterDate() {
    let [d, m, y] = $('#date_from').val().split('-');
    let date_from = new Date(+y, +m - 1, +d);

    [d, m, y] = $('#date_to').val().split('-');
    let date_to = new Date(+y, +m - 1, +d);
    
    if (date_from > date_to) {
        alert('Periode tanggal tidak valid');
    }
    
    return;
}

</script>

<h3>Surat Jalan Pengantar (Umum)</h3><br> 
    <div style="margin-bottom: 1rem;">
        <a href="<?php echo base_url(); ?>index.php/sjpumum/cform/add" class="btn btn-sm btn-primary">Tambah Data</a>
    </div>
<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('sjpumum/cform/index'); ?>
    <fieldset class="fieldsetdemo" style="margin-bottom: 2rem; border: 1px solid #e5e5e5">
        <legend class="legend">Filter Pencarian Data</legend>
    <table class="table table-responsive table-hovered" style="margin-bottom: unset !important;">
        <tr>
            <td>Dari Tanggal</td>
            <td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?= date('d-m-Y', strtotime($date_from)) ?>" />
            <img alt="" id="date1" align="middle"
                title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
                onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
        </tr>
        <tr>
            <td>Sampai Tanggal</td>
            <td>: 
                <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?= date('d-m-Y', strtotime($date_to)) ?>" />
            <img alt="" id="date2" align="middle"
                title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
                onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
        </tr>
        <tr>
            <td><button type="submit" name="submit" onclick="return validateFilterDate()" class="btn btn-sm btn-primary" style="padding: 0px 5px;">Cari</button></td>
        </tr>
    </table>
    </fieldset>

    <?php echo form_close(); ?>    

    <form id="f_master_brg" name="f_master_brg">
        <input type="hidden" name="id_sjpumum" id="id_sjpumum">
        <table class="table table-responsive table-hover" border="1" cellpadding= "1" cellspacing = "1" width="100%" id="mytable">
            <thead>
                <tr class="judulnya">
                    <th>No SJP</th>
                    <th>Area</th>
                    <th>Tanggal SJP</th>
                    <th>Tanggal Terima</th>
                    <th>Pengirim</th>
                    <th>Tujuan</th>
                    <th>Keterangan</th>
                    <th>Status SJP</th>
                    <th>Status Cetak</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($query->result() as $row) { ?>
                    <tr>
                        <td><?= $row->i_sjp ?></td>
                        <td><?= $row->e_area_name ?></td>
                        <td><?= $row->d_sjp ?></td>
                        <td><?= $row->d_sjp_receive ?></td>
                        <td><?= $row->e_sender ?></td>
                        <td><?= $row->e_recipient ?></td>
                        <td><?= $row->e_remark ?></td>
                        <td><?= $row->status_sjp?></td>
                        <td><?= $row->n_print ?></td>
                        <td>
                            <!-- Lihat -->
                            <a href="<?= base_url() . "index.php/sjpumum/cform/view?id=" . $row->id ?>" 
                                    class="btn btn-sm btn-success" style="padding: 5px" title="Lihat">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>

                            <!-- edit -->
                            <?php 
                            $url_edit = base_url() . "index.php/sjpumum/cform/edit?id=" . $row->id; 
                            $html = '<a href="'.$url_edit.'" class="btn btn-sm btn-warning" style="padding: 5px" title="Edit">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>';

                            if (!($row->f_sjp_cancel == 'f' and $row->d_approve == null)) {
                                $html = '';
                            } 
                            
                            echo $html;
                            ?>
                            

                            <!-- kondisi delete, jika belum approve & status masih aktif -->
                            <?php 
                            $url_delete = base_url() . "index.php/sjpumum/cform/delete?id=" . $row->id;
                            $html = '<a href="'.$url_delete.'" class="btn btn-sm btn-danger" style="padding: 5px" title="Delete">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>';

                            if (!($row->f_sjp_cancel == 'f' and $row->d_approve == null)) {
                                $html = '';
                            }

                            echo $html;
                            ?>
                            
                            
                            <!-- kondisi cetak, jika f_printed = false -->
                            <?php
                            $url_cetak = base_url() . "index.php/sjpumum/cform/printsjp?id=" . $row->id;
                            $html = '<a href="'.$url_cetak.'" class="btn btn-sm btn-warning glyphicon glyphicon-print print2" target="_blank" title="Cetak" onclick="return sjpPrint()">
                            </a>';

                            if (!($row->f_printed == 'f' and $row->d_approve != null)) {
                                $html = '';
                            }

                            echo $html;
                            ?>                   

                            <!-- kondisi approve -->
                            <?php 
                            $url_approve = base_url() . "index.php/sjpumum/cform/approve?id=" . $row->id;
                            $html = '<a href="'.$url_approve.'" class="btn btn-sm btn-primary" style="padding: 5px" title="Approve">
                                <span class="glyphicon glyphicon-ok"></span>
                            </a>';

                            $validApproval = $this->session->userdata('uid') == $row->uid_approve;
                            if (!(
                                    $row->d_approve == null and $row->d_sjp_receive == null 
                                    and $row->f_sjp_cancel == 'f' and $row->d_notapprove == null
                                    and $validApproval
                                )) {
                                $html = '';
                            }

                            echo $html;
                            ?>                            

                            <!-- kondisi terima -->
                            <?php 
                            $url_terima = base_url() . "index.php/sjpumum/cform/accept?id=" . $row->id;
                            $html = '<a href="'.$url_terima.'"
                                class="btn btn-sm btn-success" style="padding: 5px" title="Terima">
                                <span class="glyphicon glyphicon-share"></span>
                            </a>';

                            if (!($row->d_approve != null and $row->d_sjp_receive == null)) {
                                $html = '';
                            }
                            
                            echo $html;
                            ?>                            

                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <br>
    </form>
    <?php echo $this->pagination->create_links();?>
</div>

<script type="text/javascript">
    function sjpPrint() {
        event.preventDefault();
        const url = event.target.getAttribute('href');
        
        window.open(url, 'Cetak SJP', '_blank', 'titlebar=no,toolbar=no,location=no,status=no,menubar=no');

        window.location.reload();
    }


    $(document).ready(function() {
        $('#mytable').DataTable({
            order: [[2, 'desc'], [0, 'desc']],
        })
    })
</script>