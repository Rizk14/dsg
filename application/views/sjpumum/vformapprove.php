<h3>Surat Jalan Pengantar (Umum)</h3><br>
    <a href="javascript:void(0);">Approve Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<?php /* <script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script> */ ?>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-3.7.0.min.js"></script>

<script>


//tambah
$(function()
{

    // $("#no").val('1');

    const createItem = () => {   
        var index = $("#no").val();
        index++;

        $("#no").val(index);

        return `<tr key="${index}">
                    <td id="num_${index}" class="no">${index}</td>		  
                    <td nowrap=""><input name="kode_${index}" type="text" id="kode_${index}" size="20" value="" required /></td>
                    <td nowrap=""><input name="nama_${index}" type="text" id="nama_${index}" size="40" value=""  /></td>
                    <td nowrap=""><input name="qty_${index}" type="number" id="qty_${index}" size="10" value="0" required  /></td>
                    <td><input name="satuan_${index}" type="text" id="satuan_${index}" size="15" value="" /></td>   
                    <td><textarea name="remark_${index}" id="remark_${index}" placeholder="Keterangan" size="20"></textarea></td>
                    <td><button type="button" class="btn btn-sm iBtnDel">X</button></td>
                </tr>`;
    }

    $("#addrow2").click(function() {
        const newItem = createItem();
        let wrapper = $('#array-item');
        $(wrapper).append(newItem);
    });  

    $('#array-item').on('click', '.iBtnDel', function() {
        $(this).closest('tr').remove();

        let count = 1;
        var obj = $('#array-item tr:visible').find('.no');
        $.each(obj, function(key, value) {
            $(this).text(count);
            count++;
        });
        
        $('#no').val(count-1);
    });   
    
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

	var width = 720;
	var height = 480;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
	myWindow = window.open(url, "subWind", windowFeatures);
}

function cari(posisi, kodebrgwip) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/bonmkeluar/cform/caribrgwip', 
				data: 'kode_brg_wip='+kodebrgwip+'&posisi='+posisi, success: function(response) {
					$("#infobrgwip_"+posisi).html(response);
			}}); 
}
	
function confirmApproval()
{
    const c = confirm('Konfirmasi Approval ?');

    if (!c) {
        return false;
    }
}

function confirmReject()
{
    const c = prompt('Alasan Reject');

    if (!c) {
        return false;
    }

    $('input[name="e_remark_notapprove"]').val(c);;
}

function generate_nomor_dokumen() {
    $.ajax({
        type:'post', 
        url: '<?php echo base_url();?>index.php/sjpumum/cform/generate_nomor_dokumen', 
        data: {
            date: $('#d_sjp').val(),
            i_area: $('#i_area').val()
        },
        success: function(response) {
            $('#i_sjp').val(response);
        }
    })
}


</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sjpumum/cform/approve_submit" method="post" enctype="multipart/form-data">

<input type="hidden" name="iddata" id="iddata"/>
<input type="hidden" name="id" id="id" value="<?= $header->id ?>">
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
    <tr>
        <td>Perusahaan</td>
        <td><input name="e_sender_company" type="text" id="e_sender_company" maxlength="30" value="<?= $header->e_sender_company ?>" disabled></td>
    </tr>
    <tr>
        <td>Pengirim</td>
        <td><input name="e_sender" type="text" id="e_sender" maxlength="30" value="<?= $header->e_sender ?>" disabled></td>
    </tr>
    <tr>
        <td>Area</td>
        <td>
            <select name="i_area" id="i_area" onchange="generate_nomor_dokumen()" disabled>
                <?php foreach ($list_area as $area) { ?>
                    <?php $selected = ($area->i_area == $header->i_area) ? 'selected' : '' ?>
					<option value="<?= $area->i_area ?>" <?= $selected ?>><?= "$area->e_area_city - $area->e_area_name" ?></option>
				<?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><br/></td>
    </tr>
    <tr>
        <td>Tanggal SJP</td>
        <td>
            <input name="d_sjp" type="text" id="d_sjp" value="<?= date('d-m-Y', strtotime($header->d_sjp)) ?>" onchange="generate_nomor_dokumen()" disabled>
            <img alt="" src="<?php echo base_url();?>images/calendar.gif">
        </td>
    </tr> 
    <tr>
        <td width="15%">Nomor SJP</td>
        <td>
            <input name="i_sjp" type="text" id="i_sjp" size="20" maxlength="20" value="<?= $header->i_sjp ?>" disabled>
        </td>
    </tr> 
    <tr>
        <td>Perusahaan Tujuan</td>
        <td>
        <input name="e_recepient_company" type="text" id="e_recepient_company" maxlength="50" value="<?= $header->e_recipient_company ?>" disabled>
        </td>
    </tr> 
    <tr>
        <td>Tujuan</td>
        <td>
        <input name="e_recepient" type="text" id="e_recepient" maxlength="50" value="<?= $header->e_recipient ?>" disabled>
        </td>
    </tr> 
    <tr>
        <td>Keterangan</td>
        <td>
            <textarea name="e_remark_header" id="e_remark_header" cols="30" rows="2" disabled><?= $header->e_remark ?></textarea>
        </td>  
    </tr>
    <tr>
        <td colspan="2"><br>
            <form name="myform">
                <table id="tabelku" width="90%" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="table table-responsive">
                    <thead>
                        <tr>
                            <td colspan="8" align="left">
                            <?php /* <input id="addrow2" type="button" name="addrow2" value="&nbsp&nbsp+&nbsp&nbsp" title="Tambah Item Barang">&nbsp; */ ?>
                            <?php /* <input id="deleterow" type="button" name="deleterow" value="&nbsp&nbsp-&nbsp&nbsp " title="Hapus Item Barang"> */ ?>
                            </td>
                        </tr>
                        <tr>
                            <th width="5%">No</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                        <tbody id="array-item">
                            <?php $no = 1; ?>   
                            <?php foreach ($detail as $row) { ?>
                            <tr>
                                <td id="num_<?= $no ?>" class="no"><?= $no ?></td>
                                <td nowrap="">
                                    <input name="kode_<?= $no ?>" type="text" id="kode_<?= $no ?>" size="20" value="<?= $row->i_product ?>" disabled/>
                                </td>
                                <td nowrap="">
                                    <input name="nama_<?= $no ?>" type="text" id="nama_<?= $no ?>" size="40" value="<?= $row->e_product_name ?>" disabled/>
                                </td>
                                <td nowrap="">
                                    <input name="qty_<?= $no ?>" type="number" id="qty_<?= $no ?>" size="10" value="<?= $row->n_quantity ?>" disabled/>
                                </td>
                                <td>
                                    <input name="satuan_<?= $no ?>" type="text" id="satuan_<?= $no ?>" size="15" value="<?= $row->n_satuan ?>" disabled/></td>
                                <td>
                                    <textarea name="remark_<?= $no ?>" id="remark_<?= $no ?>" placeholder="Keterangan" size="20" disabled><?= $row->e_remark ?></textarea>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                    </tbody>
                </table>	
                <input type="hidden" name="no" id="no" value="<?= $no -1 ?>">
            </form>
            <br>
            <div align="center">
                <input type="submit" class="btn btn-sm btn-success" name="approve" value="approve" onclick="return confirmApproval();">&nbsp;
                <input type="submit" class="btn btn-sm btn-danger" name="reject" value="reject" onclick="return confirmReject()">
                <input type="hidden" name="e_remark_notapprove" value="">
                <input type="button" class="btn btn-sm btn-light" name="batal" value="kembali" onClick="window.location='<?php echo base_url(); ?>index.php/sjpumum/cform/index'">
            </div>
        </td>
    </tr>

</table>
</div>
</form>
