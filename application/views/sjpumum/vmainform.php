<h3>Surat Jalan Pengantar (Umum)</h3><br>
    <a href="javascript:void(0)">Tambah Data</a>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<?php /* <script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script> */ ?>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-3.7.0.min.js"></script>

<script>


//tambah
$(function()
{

    $("#no").val('1');

    const createItem = () => {   
        var index = $("#no").val();
        index++;

        $("#no").val(index);

        return `<tr key="${index}">
                    <td id="num_${index}" class="no">${index}</td>		  
                    <td nowrap=""><input name="kode_${index}" type="text" id="kode_${index}" size="20" value=""/></td>
                    <td nowrap=""><input name="nama_${index}" type="text" id="nama_${index}" size="40" value=""/></td>
                    <td nowrap=""><input name="qty_${index}" type="number" id="qty_${index}" size="10" value="0" required  /></td>
                    <td><input name="satuan_${index}" type="text" id="satuan_${index}" size="15" value=""/></td>   
                    <td><textarea name="remark_${index}" id="remark_${index}" placeholder="Keterangan" size="20"></textarea></td>
                    <td><button type="button" class="btn btn-sm iBtnDel"><span class="glyphicon glyphicon-trash"></span></button></td>
                </tr>`;
    }

    $("#addrow2").click(function() {
        const newItem = createItem();
        let wrapper = $('#array-item');
        $(wrapper).append(newItem);
    });  

    $('#array-item').on('click', '.iBtnDel', function() {
        $(this).closest('tr').remove();

        let count = 1;
        var obj = $('#array-item tr:visible').find('.no');
        $.each(obj, function(key, value) {
            $(this).text(count);
            count++;
        });
        
        $('#no').val(count-1);
    });   
    
    




















	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>1) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
	// 11-07-2015
	$('#dibebankan').click(function(){
	  	    if ($("#dibebankan").is(":checked")) {
				$('#ket').attr('disabled', true);
				$('#ket').val('');
			}
			else {
				$('#ket').attr('disabled', false);
				$('#ket').val('');
			}
	  });
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

	var width = 720;
	var height = 480;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
	myWindow = window.open(url, "subWind", windowFeatures);
}

function cari(posisi, kodebrgwip) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/bonmkeluar/cform/caribrgwip', 
				data: 'kode_brg_wip='+kodebrgwip+'&posisi='+posisi, success: function(response) {
					$("#infobrgwip_"+posisi).html(response);
			}}); 
}

function validateForm() {
    let valid = true;
    let elQtys = $('input[name*="qty"]');

    elQtys.each(function() {
        const t = $(this);

        if (t.val() == null || t.val() === undefined || t.val() == '' || parseFloat(t.val()) == 0) {
            alert('Jumlah tidak valid, harap periksa kembali..')
            valid = false;
        }
    });

    if (!valid) {
        return false;
    }
}

function generate_nomor_dokumen() {
    $.ajax({
        type:'post', 
        url: '<?php echo base_url();?>index.php/sjpumum/cform/generate_nomor_dokumen', 
        data: {
            date: $('#d_sjp').val(),
            i_area: $('#i_area').val()
        },
        success: function(response) {
            $('#i_sjp').val(response);
        }
    })
}


</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/sjpumum/cform/submit" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<input type="hidden" name="no_bonm" id="no_bonm" value="">
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
    <tr>
        <td>Perusahaan</td>
        <td><input name="e_sender_company" type="text" id="e_sender_company" maxlength="30" value="CV. Duta Setia Garment"></td>
    </tr>
    <tr>
        <td>Pengirim</td>
        <td><input name="e_sender" type="text" id="e_sender" maxlength="30" value=""></td>
    </tr>
    <tr>
        <td>Area</td>
        <td>
            <select name="i_area" id="i_area" onchange="generate_nomor_dokumen()">
                <?php foreach ($list_area as $area) { ?>
					<option value="<?= $area->i_area ?>" ><?= "$area->e_area_city - $area->e_area_name" ?></option>
				<?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><br/></td>
    </tr>
  <tr>
    <td>Tanggal SJP</td>
    <td>
	<label>
      <input name="d_sjp" type="text" id="d_sjp" size="10" value="<?= date('d-m-Y') ?>" readonly="true" onchange="generate_nomor_dokumen()">
    </label>
	   <img alt="" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].d_sjp,'dd-mm-yyyy',this);">
	</td>
  </tr> 
  <tr>
    <td width="15%">Nomor SJP</td>
    <td>
       <input name="i_sjp" type="text" id="i_sjp" size="20" maxlength="20" value="">
    </td>
  </tr> 
  <tr>
    <td>Perusahaan Tujuan</td>
    <td>
       <input name="e_recepient_company" type="text" id="e_recepient_company" maxlength="50" value="">
    </td>
  </tr> 
  <tr>
    <td>Tujuan</td>
    <td>
       <input name="e_recepient" type="text" id="e_recepient" maxlength="50" value="">
    </td>
  </tr> 
  <?php /*
  <tr>
		<td>Tujuan</td>
		<td> <select name="tujuan" id="tujuan">
				<option value="1" >Cutting</option>
				<option value="2" >Unit Jahit</option>
				<option value="3" >Lain-Lain (Retur)</option>
				<option value="4" >Lain-Lain (Peminjaman)</option>
				<option value="5" >Lain-Lain (Lainnya)</option>
				<option value="6" >Pengadaan</option>
				<option value="7" >QC/WIP</option>
				<option value="8" >Makloon</option>
				<option value="9" >Unit Packing</option>
			<!--	<option value="6" >Pengadaan</option>
				<option value="7" >Quilting</option>
				<option value="8" >WIP</option>
				<option value="9" >RND</option>
				<option value="10" >BLN Kedungreja</option> -->
				</select>
		</td>
	</tr>
	
	<tr>
		<td>Unit Jahit</td>
		<td> <select name="id_unit_jahit" id="id_unit_jahit">
				<option value="0">Tidak Ada</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Unit Packing</td>
		<td> <select name="id_unit_packing" id="id_unit_packing">
				<option value="0">Tidak Ada</option>
				<?php foreach ($list_unit_packing as $pck) { ?>
					<option value="<?php echo $pck->id ?>" ><?php echo $pck->kode_unit."-".$pck->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
    <tr>
        <td>Dibebankan</td>
        <td>
            <input name="ket" type="text" id="ket" size="30" value=""> 
            <input type="checkbox" name="dibebankan" id="dibebankan" value="DIBEBANKAN">
        </td>  
    </tr>
    */ ?>
    <tr>
        <td>Keterangan</td>
        <td>
            <textarea name="e_remark_header" id="e_remark_header" cols="30" rows="2"></textarea>
        </td>  
    </tr>
<!--  <tr>
	<td colspan="2"><i>*) Untuk bahan yg satuannya Yard, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Meter (kecuali quilting, tetap Yard)<br>
	*) Untuk bahan yg satuannya Lusin, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Pieces<br>
	*) Untuk bahan lain yg memerlukan konversi satuan, silahkan pilih di kolom Satuan Konversi<br></i></td>
  </tr> -->
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
        <table id="tabelku" width="100%" border="0" align="center"  cellpadding="1"  cellspacing="2" class="table table-responsive">
            <thead>
                <tr>
                    <td colspan="8" align="left">
                    <input id="addrow2" type="button" name="addrow2" value="&nbsp&nbsp+&nbsp&nbsp" title="Tambah Item Barang">&nbsp;
                    <?php /* <input id="deleterow" type="button" name="deleterow" value="&nbsp&nbsp-&nbsp&nbsp " title="Hapus Item Barang"> */ ?>
                    </td>
                </tr>
                <tr>
                    <th width="2%">No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
			<tbody id="array-item">
            <tr>
                <td id="num_1" class="no">1</td>		  
                <td nowrap="">
                    <input name="kode_1" type="text" id="kode_1" size="20" value=""/>
                </td>
                <td nowrap="">
                    <input name="nama_1" type="text" id="nama_1" size="40" value=""/>
                </td>
                <td nowrap="">
                    <input name="qty_1" type="number" id="qty_1" size="10" value="0" required/>
                </td>
                <td>
                    <input name="satuan_1" type="text" id="satuan_1" size="15" value=""/></td>
                <td>
                    <textarea name="remark_1" id="remark_1" placeholder="Keterangan" size="20"></textarea>
                </td>
                <td>
                    <button class="btn btn-sm iBtnDel" id="">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </td>
	     	</tr>
	    </tbody>
	</table>	
	
	</form>
        <br>
        <div align="center">
            <input type="submit" name="submit2" class="btn btn-sm btn-primary" value="Simpan" onclick="return validateForm();">
            <input type="button" name="batal" class="btn btn-sm btn-light" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/sjpumum/cform/index'">
        </div>
    </td>
</tr>

</table>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        generate_nomor_dokumen();
    })
</script>