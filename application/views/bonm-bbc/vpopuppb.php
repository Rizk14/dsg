<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var des=$("#no_pb").val();
		var id_pb=$("#id_pb").val();
		opener.document.forms["f_pb"].no_pb_cutting.value=des;
		opener.document.forms["f_pb"].id_pb.value=id_pb;
		
		opener.document.forms["f_pb"].id_brg.value = '';
		//alert(document.f_master_brg.brg.value);
		/*
			var len = document.f_master_brg.brg.length;
			if(len == undefined) len = 1;
		*/
		
		if (document.f_master_brg.brg.length > 0) {
			for(var i=0; i < document.f_master_brg.brg.length; i++){
				if(document.f_master_brg.brg[i].checked)
					//total +=document.form1.scripts[i].value + "\n"
					opener.document.forms["f_pb"].id_brg.value+= document.f_master_brg.brg[i].value + ";";
			}
		}
		else
			opener.document.forms["f_pb"].id_brg.value = document.f_master_brg.brg.value;
		
		self.close();
	});
});
</script>

<center><h3>Daftar Permintaan Bahan Baku Yang Belum Dipenuhi</h3></center>
<div align="center"><br>
<?php echo form_open('bonm-bbc/cform/show_popup_pb_cutting'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="no_pb" id="no_pb">
<input type="hidden" name="id_pb" id="id_pb">

	<table border="1" align="center" cellpadding="1" width="100%" cellspacing="2" bordercolor="#666666" nowrap="nowrap">
	<thead>
	 <tr>
		 <th>No Request</th>
		 <th>Tgl Request</th>
		 <th>Utk Barang Jadi</th>
		 <th>Jenis Proses & List Bhn Baku</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				 $pisah1 = explode("-", $query[$j]['tgl']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_pp = $tgl1." ".$nama_bln." ".$thn1; 
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_pb_cutting']."</td>";
				 echo    "<td>".$tgl_pp."</td>";
				 echo    "<td>".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['jenis_proses'] == 1) {
						  $nama_proses = "Dicutting";
						  if ($var_detail[$k]['for_kekurangan_cutting'] == 't')
							$nama_proses.= " (utk kekurangan bhn cutting)";
						  else if ($var_detail[$k]['for_bs_cutting'] == 't')
							$nama_proses.= " (utk ganti BS cutting)";
						}
						else if ($var_detail[$k]['jenis_proses'] == 2) {
						  if ($var_detail[$k]['bordir'] == 'f' ) {
							  $nama_proses = "Diprint";
							  if ($var_detail[$k]['for_kekurangan_cutting'] == 't')
								$nama_proses.= " (utk kekurangan bhn cutting)";
							  else if ($var_detail[$k]['for_bs_cutting'] == 't')
								$nama_proses.= " (utk ganti BS cutting)";
						  }
						  else {
							  $nama_proses = "Bordir";
							  if ($var_detail[$k]['for_kekurangan_cutting'] == 't')
								$nama_proses.= " (utk kekurangan bhn cutting)";
							  else if ($var_detail[$k]['for_bs_cutting'] == 't')
								$nama_proses.= " (utk ganti BS cutting)";
						  }
						}
						else if ($var_detail[$k]['jenis_proses'] == 3)
						  $nama_proses = "Diquilting";
						else if ($var_detail[$k]['jenis_proses'] == 4)
						  $nama_proses = "Dibisbisan";
						else if ($var_detail[$k]['jenis_proses'] == 5) {
						  $nama_proses = "Diquilting+print/bordir";
						  if ($var_detail[$k]['for_kekurangan_cutting'] == 't')
							$nama_proses.= " (utk kekurangan bhn cutting)";
						  else if ($var_detail[$k]['for_bs_cutting'] == 't')
							$nama_proses.= " (utk ganti BS cutting)";
						}
						
						  echo "[".$nama_proses."] ". $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (Jml Gelar ".number_format($var_detail[$k]['jum_gelar'],0,',','.').", Pjg Kain ".$var_detail[$k]['panjang_kain']." m)";
						  echo " <input type='checkbox' name='brg' id='brg' value='".$var_detail[$k]['id']."' >";
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 				 
				 echo    "<td>".$tgl_update."</td>"; ?>
				 <td align=center><a class="pilih" style="cursor:pointer" id="pilih" 
				 onMouseOver="window.document.f_master_brg.no_pb.value='<?php echo $query[$j]['no_pb_cutting'] ?>';
				 window.document.f_master_brg.id_pb.value='<?php echo $query[$j]['id'] ?>'">Pilih</a></td>
				<?php echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table>
</form>
  <? echo $this->pagination->create_links();?>
</div>
