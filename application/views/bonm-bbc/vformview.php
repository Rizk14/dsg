<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
    
    .fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 900;
		var height = 550;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function()
{
	
});


</script>

<h3>Data Pemenuhan Permintaan Bahan Baku</h3><br>
<a href="<? echo base_url(); ?>index.php/bonm-bbc/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/bonm-bbc/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('bonm-bbc/cform/cari'); ?>

<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_op_print" id="id_op_print">

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No Request Bahan</th>
		 <th>No Pemenuhan</th>
		 <th>Tgl Pemenuhan</th>
		 <th>Untuk Brg Jadi</th>
		 <th>Jenis Proses</th>
		 <th>Bhn Baku/Pembantu</th>
		 <th>Plan Qty (Set)</th>
		 <th>Qty Yg Diklrkan</th>
		 <th>Detail Gelar</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl_bonm']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_bonm = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>".$query[$j]['no_pb_cutting']."</td>";
				 echo    "<td nowrap>".$query[$j]['no_bonm']."</td>";
				 echo    "<td>".$tgl_bonm."</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nama_brg_jadi'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['jenis_proses'] == 1)
						  echo "Dicutting";
						else if ($var_detail[$k]['jenis_proses'] == 2)
						  echo "Diprint";
						else if ($var_detail[$k]['jenis_proses'] == 3)
						  echo "Diquilting";
						else if ($var_detail[$k]['jenis_proses'] == 4)
						  echo "Dibisbisan";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['plan_qty_cutting'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty_pjg_kain'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['gelar_pjg_kain'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";

				 echo    "<td>".$tgl_update."</td>";
					
				 if ($query[$j]['status_edit'] == 'f')
				
					echo    "<td align=center>
					<a href=".base_url()."index.php/bonm-bbc/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \">Edit</a>
					<a href=".base_url()."index.php/bonm-bbc/cform/delete/".$query[$j]['id']."/".$query[$j]['id_detailnya']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 else
					echo "<td>&nbsp;</td>";
				 
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
