<h3>Data Pemenuhan Permintaan Bahan Baku</h3><br>
<a href="<? echo base_url(); ?>index.php/bonm-bbc/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/bonm-bbc/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
	
	$('#pilih_pb').click(function(){
			var urlnya = "<?php echo base_url(); ?>index.php/bonm-bbc/cform/show_popup_pb_cutting/";
			openCenteredWindow(urlnya);

	  });
	  
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 1000;
		var height = 500;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_item_brg() {
	var id_brg= $('#id_brg').val();
	var no_pb_cutting= $('#no_pb_cutting').val();
	var id_pb= $('#id_pb').val();
	if (no_pb_cutting == '' && id_pb == '') {
		alert("Nomor Request Bahan harus dipilih..!");
		return false;
	}
	if (id_brg == '') {
		alert("item barang di Permintaan Bahan harus dipilih..!");
		return false;
	}
}

function cek_bonm() {
	var no_bonm= $('#no_bonm').val();
	var tgl_bonm= $('#tgl_bonm').val();
	if (no_bonm == '') {
		alert("Nomor Pemenuhan harus diisi..!");
		return false;
	}
	if (tgl_bonm == '') {
		alert("Tanggal Pemenuhan harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#pjg_kain_'+k).val() == '0' || $('#pjg_kain_'+k).val() == '' ) {				
				alert("Data Panjang Kain tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#pjg_kain_'+k).val()) ) {
				alert("Panjang Kain harus berupa angka..!");
				return false;
			}
			
			if($('#gelar_pjg_kain_'+k).val() == '' ) {				
				alert("Data Detail Gelar tidak boleh 0 / kosong...!");
				return false;
			}
			
			if ($('#stok_lama_m_'+k).val() - $('#pjg_kain_'+k).val() < 0 ) {
				alert("Permintaan melebihi stok...!");
				return false;
			}
			
		/*	alert($('#stok_lama_m_'+k).val());
			alert($('#pjg_kain_'+k).val());
			alert($('#stok_lama_m_'+k).val() - $('#pjg_kain_'+k).val()); 
			return false; */

		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_pb', 'id' => 'f_pb');
echo form_open('bonm-bbc/cform/', $attributes); ?>
<table>

	<tr>
		<td>No Request Bahan</td>
		<td> <input type="text" name="no_pb_cutting" id="no_pb_cutting" value="" size="10" readonly="true">&nbsp;
		<input type="hidden" name="id_brg" id="id_brg" value="">
		<input type="hidden" name="id_pb" id="id_pb" value="">
		<input name="pilih_pb" id="pilih_pb" value="..." type="button" title="browse data permintaan bahan baku">
		</td>
	</tr>
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/bonm-bbc/cform/view'">
<?php echo form_close(); 
} else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/bonm-bbc/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="no_pb_cutting" value="<?php echo $no_pb_cutting ?>">
<input type="hidden" name="id_pb" value="<?php echo $id_pb ?>">

<?php 
		if (count($pb_detail)>0) {
			$no=1;
			foreach ($pb_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
    <td width="10%">No Request Bahan</td>
    <td width="70%">
      <?php echo $no_pb_cutting; ?>
    </td>
  </tr>
  <tr>
    <td>No Pemenuhan</td>
    <td>
      <input name="no_bonm" type="text" id="no_bonm" size="10" maxlength="20" readonly="true" value="<?php echo $no_bonm; ?>">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tgl Pemenuhan</td>
    <td>
	<label>
      <input name="tgl_bonm" type="text" id="tgl_bonm" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_bonm" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_bonm,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
	<td colspan="2"><i>*) Untuk bahan yg satuannya Yard, maka Qty yg dikeluarkan itu dlm bentuk Meter<br>
	*) Untuk bahan yg satuannya selain Yard, maka Qty yg dikeluarkan itu tidak ada konversi satuan</i></td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="right">
			<!-- <input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang"> -->
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Utk Barang Jadi</th>
          <th>Jenis Proses</th>
          <th>Bhn Quilting</th>
          <th>Bhn Bis-Bisan</th>
          <th>Uk Bis-Bisan</th>
          <th>Kode Bhn</th>
           <th>Nama Bhn</th>
           <th>Satuan</th>
	      <th>Plan Qty (Set)</th>
	      <th>Gelaran/Set</th>
	      <th>Jml Gelar</th>
	      <th>Qty Yg Diklrkan</th>
	      <th>Detail Per Gelar</th>
	      <th>Utk Kekurangan Schd Cutting</th>
        </tr>

        <?php $i=1;
        if (count($pb_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($pb_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          
          <td><input name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $pb_detail[$j]['kode_brg_jadi']." - ".$pb_detail[$j]['nama_brg_jadi'] ?>" />
          <input type="hidden" name="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['kode_brg_jadi'] ?>" >
          <input type="hidden" name="nama_brg_jadi_other_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['nama_brg_jadi_other'] ?>" >
          </td>
          
          <td nowrap>
          <?php 
				if ($pb_detail[$j]['jenis_proses'] == 1) {
					$jenis_proses = "Dicutting";

					if ($pb_detail[$j]['kcutting'] == 't')
						$jenis_proses.= " (utk kekurangan bhn cutting)";
					else if ($pb_detail[$j]['bscutting'] == 't')
						$jenis_proses.= " (utk ganti BS cutting)";
				}
				else if ($pb_detail[$j]['jenis_proses'] == 2) {
					if ($pb_detail[$j]['bordir'] == 'f' ) {
						$jenis_proses = "Diprint";
						if ($pb_detail[$j]['kcutting'] == 't')
							$jenis_proses.= " (utk kekurangan bhn cutting)";
						else if ($pb_detail[$j]['bscutting'] == 't')
							$jenis_proses.= " (utk ganti BS cutting)";
					}
					else {
						$jenis_proses = "Bordir";
						if ($pb_detail[$j]['kcutting'] == 't')
							$jenis_proses.= " (utk kekurangan bhn cutting)";
						else if ($pb_detail[$j]['bscutting'] == 't')
							$jenis_proses.= " (utk ganti BS cutting)";
					}
				}
				else if ($pb_detail[$j]['jenis_proses'] == 3) 
					$jenis_proses = "Diquilting";
				else if ($pb_detail[$j]['jenis_proses'] == 4) 
					$jenis_proses = "Dibisbisan";
				else if ($pb_detail[$j]['jenis_proses'] == 5) { 
					$jenis_proses = "Diquilting+print/bordir";
					if ($pb_detail[$j]['kcutting'] == 't')
						$jenis_proses.= " (utk kekurangan bhn cutting)";
					else if ($pb_detail[$j]['bscutting'] == 't')
						$jenis_proses.= " (utk ganti BS cutting)";
				}
			?>
          <input name="nama_jenis_proses_<?php echo $i ?>" type="text" id="nama_jenis_proses_<?php echo $i ?>" size="15" readonly="true" value="<?php echo $jenis_proses; ?>"/>
          <input name="jenis_proses_<?php echo $i ?>" type="hidden" value="<?php echo $pb_detail[$j]['jenis_proses'] ?>"/>
          <input type="hidden" name="bordir_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['bordir'] ?>" >
          <input type="hidden" name="kcutting_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['kcutting'] ?>" >
          <input type="hidden" name="bscutting_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['bscutting'] ?>" >
          </td>
          
          <td><input name="brg_quilting_<?php echo $i ?>" type="text" id="brg_quilting_<?php echo $i ?>" size="30" readonly="true" value="<?php if($pb_detail[$j]['kode_brg_quilting'] != '') echo $pb_detail[$j]['kode_brg_quilting']." - ".str_replace("\"", "&quot;", $pb_detail[$j]['nama_brg_quilting']) ?>" />
          <input type="hidden" name="kode_brg_quilting_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['kode_brg_quilting'] ?>" >
          </td>
          
          <td><input name="brg_bisbisan_<?php echo $i ?>" type="text" id="brg_bisbisan_<?php echo $i ?>" size="30" readonly="true" value="<?php if($pb_detail[$j]['kode_brg_bisbisan'] != '') echo $pb_detail[$j]['kode_brg_bisbisan']." - ".str_replace("\"", "&quot;", $pb_detail[$j]['nama_brg_bisbisan']) ?>" />
          <input type="hidden" name="kode_brg_bisbisan_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['kode_brg_bisbisan'] ?>" >
          </td>
          
          <td><select disabled name="ukbis_<?php echo $i ?>" id="ukbis_<?php echo $i ?>" >
				<?php foreach ($ukuran_bisbisan as $uk) { ?>
					<option value="<?php echo $uk->id ?>" <?php if ($pb_detail[$j]['ukuran_bisbisan'] == $uk->id) { ?> selected="true" <?php } ?> ><?php echo $uk->nama ?></option>
				<?php } ?>
				</select>
				<input type="hidden" name="ukuran_bisbisan_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['ukuran_bisbisan'] ?>" >
          </td>
          
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="15" readonly="true" value="<?php echo $pb_detail[$j]['kode_brg'] ?>"/>
           <input type="hidden" name="id_pb_detail_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['id'] ?>" >
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $pb_detail[$j]['nama'] ?>" /></td>
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="5" readonly="true"
          value="<?php echo $pb_detail[$j]['nama_satuan'] ?>" /></td>
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="6" maxlength="10" 
          value="<?php echo $pb_detail[$j]['qty'] ?>" readonly="true" /></td>
          
          <td nowrap><input name="gelaran_<?php echo $i ?>" type="text" id="gelaran_<?php echo $i ?>" size="5" value="<?php echo $pb_detail[$j]['gelaran'] ?>" readonly="true" /> / <input name="set_<?php echo $i ?>" type="text" id="set_<?php echo $i ?>" size="5" value="<?php echo $pb_detail[$j]['jum_set'] ?>" readonly="true" /> </td>
          <td><input name="jum_gelar_<?php echo $i ?>" type="text" id="jum_gelar_<?php echo $i ?>" size="5" value="<?php echo $pb_detail[$j]['jum_gelar'] ?>" readonly="true" /></td>
          <td><input name="pjg_kain_<?php echo $i ?>" type="text" id="pjg_kain_<?php echo $i ?>" size="5" value="<?php echo $pb_detail[$j]['panjang_kain'] ?>" />
          <input type="hidden" name="stok_lama_m_<?php echo $i ?>" id="stok_lama_m_<?php echo $i ?>" value="<?php echo $pb_detail[$j]['stok_lama'] ?>" >
          </td>
          <td nowrap><input readonly="true" name="gelar_pjg_kain_<?php echo $i ?>" type="text" id="gelar_pjg_kain_<?php echo $i ?>" size="20" value="" />
          <input title="input detail" name="pilih_gelar_pjg_kain_<?php echo $i ?>" value="..." type="button" id="pilih_gelar_pjg_kain_<?php echo $i ?>" 
           onclick="javascript: var x= $('#gelaran_<?php echo $i ?>').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/bonm-bbc/cform/show_popup_gelar_pjg_kain/<?php echo $i ?>/'+x);" >
          </td>
          
          <td nowrap><input readonly="true" name="sc_<?php echo $i ?>" type="text" id="sc_<?php echo $i ?>" size="10" value="" />
          <input name="id_sc_detail_<?php echo $i ?>" type="hidden" id="id_sc_detail_<?php echo $i ?>" value="0" />
          <input title="input schd cutting" name="pilih_sc_<?php echo $i ?>" value="..." type="button" id="pilih_sc_<?php echo $i ?>" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/bonm-bbc/cform/show_popup_sched_cutting/<?php echo $i ?>/');"
           <?php if ($pb_detail[$j]['kcutting'] == 'f') { ?> disabled <?php } ?> >
          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
	</table>	
	
	</form>
      <div align="center"><br> 
        
        <input type="submit" name="submit2" value="Simpan" onclick="return cek_bonm();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/bonm-bbc/cform/'">

      </div></td>
    </tr>

</table>
</div>
</form>
<?php } ?>
