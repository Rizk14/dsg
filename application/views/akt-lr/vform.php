<?php echo "<h2>$page_title</h2>";?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('akt-lr/cform/view', $attributes);?>
	
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="19%">Periode</td>
		<td width="1%">:</td>
		<td width="80%"><input type="hidden" id="iperiode" name="iperiode" value="">
						<select name="bulan" id="bulan" onmouseup="buatperiode()">
							<option></option>
							<option value='01'>Januari</option>
							<option value='02'>Pebruari</option>
							<option value='03'>Maret</option>
							<option value='04'>April</option>
							<option value='05'>Mei</option>
							<option value='06'>Juni</option>
							<option value='07'>Juli</option>
							<option value='08'>Agustus</option>
							<option value='09'>September</option>
							<option value='10'>Oktober</option>
							<option value='11'>November</option>
							<option value='12'>Desember</option>
						</select>
						<select name="tahun" id="tahun" onMouseUp="buatperiode()">
							<option></option>
                     <?php
                        $tahun1 = date('Y')-3;
                        $tahun2 = date('Y');
                        for($i=$tahun1;$i<=$tahun2;$i++)
                        {
                           echo "<option value='$i'>$i</option>";
                        }
                     ?>
						</select>
			</td>
	      </tr>
	      <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="View" type="button" onclick="view()">
		  <input name="cmdreset" id="cmdreset" value="Reset" type="reset"
		   onclick="window.location='<?php echo base_url(); ?>index.php/akt-lr/cform'" />
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script languge=javascript type=text/javascript>
  function buatperiode(){
	  periode=document.getElementById("tahun").value+document.getElementById("bulan").value;
	  document.getElementById("iperiode").value=periode;
  }
	function view()
	{
		if(
			(document.getElementById("iperiode").value!='')
		  )
		{
			periode	= document.getElementById("iperiode").value;
			window.location="cform/view/"+periode+"/";
		}
	}
</script>
