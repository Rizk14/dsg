<h3>Import Excel Data Bahan Baku/Pembantu</h3><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var userfile= $('#userfile').val();
	
	if (userfile == '') {
		alert("File excel yang mau diimport harus dipilih..!");
		return false;
	}
	
	$('#gambarnya').show();
	$('#btnimport').attr("disabled", true);
	
}

/*$("#btnexport").click(function(){    
	$('#gambarnya').show();
	$('#btnexport').attr("disabled", true);
}); */
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
$attributes = array('name' => 'f_import', 'id' => 'f_import');
echo form_open_multipart('importdata/cform/doimportbhnbaku',array('id' => 'f_import', 'name'=>'f_import')); ?>

<table width="60%">
	<tr>
        <td width="20%">Pilih File Excel</td>
        <td><input type="file" id="userfile" name="userfile"></td>
     </tr>

</table><br>
<i>Format kolom-kolomnya: <b>Nama Item Barang | ID Satuan | Kode Kelompok | ID Jenis Barang</b></i><br><br>
<input type="submit" name="btnimport" id="btnimport" value="Import Data" onclick="return cek_input();">&nbsp;<img id="gambarnya" src="<?php echo base_url();?>images/loading.gif" style="display:none">
<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/importdata/cform/importbhnbaku'">
<?php echo form_close();  ?>
