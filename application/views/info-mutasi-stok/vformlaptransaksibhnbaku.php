<h3>Laporan Transaksi Barang Bahan Baku/Pembantu</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	var kode_brg = $('#kode_brg').val();
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}	
	
	if (kode_brg == '') {
		alert("Bahan Baku/Pembantu harus dipilih ..!");
		return false;
	}	
}

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<?php 
$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
echo form_open('info-mutasi-stok/creport/viewtransaksibhnbaku', $attributes); ?>
<table width="60%">
	<tr>
			<td width="20%">Lokasi Gudang</td>
			<td> <select name="id_gudang" id="id_gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>"><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
	
	<tr>
		<td>Periode (bulan-tahun)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>	
  <tr>
	<td>Bahan Baku/Pembantu</td>
	<td><input type="text" name="bhnbaku" id="bhnbaku" value="" size="40" readonly="true"> &nbsp;
	<input name="id_brg" type="hidden" id="id_brg" value=""/>
	<input name="kode_brg" type="hidden" id="kode_brg" value=""/>
           <input title="browse data barang" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: var x= $('#id_gudang').val(); 
            openCenteredWindow('<?php echo base_url(); ?>index.php/info-mutasi-stok/creport/show_popup_brg/'+x);" >
	</td>
  </tr>

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/info-mutasi-stok/creport/transaksibhnbaku'">
<?php echo form_close();  ?>
