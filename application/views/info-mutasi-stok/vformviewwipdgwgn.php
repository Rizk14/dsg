<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Stok Barang WIP (Hasil Jahit) Gudang QC</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel_wip_w', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width='1%' rowspan='2'>No</th>
		 <th width='3%' rowspan='2'>Kode</th>
		 <th width='13%' rowspan='2'>Nama Brg WIP</th>
		 <!--<th width='8%' rowspan='2'>Satuan</th>-->
		 <th width='11%' rowspan='2'>Saldo<br>Awal</th>
		 <th colspan='6'>Masuk</th>
		 <th colspan='5'>Keluar</th>
		 <th width='8%' rowspan='2'>Saldo Akhir</th>
		 <th width='8%' rowspan='2'>Stok Opname</th>
		  <th width='8%' rowspan='2'>Adjustment</th>
		 <th width='8%' rowspan='2'>Selisih</th>
		
	 </tr>
	 <tr class="judulnya">
			<th width='11%'>Bgs</th>
			<th width='11%'>Hsl Perbaikan</th>
			 <th width='11%'>Retur Unit Packing</th>
			 <th width='11%'>Retur Gdg Jadi</th>
			 <th width='11%'>Lain2</th>
			 <th width='11%'>Total</th>
			 <th width='11%'>Bgs Packing</th>
			 <th width='11%'>Bgs Gdg Jadi</th>
			 <th width='11%'>Retur Perbaikan</th>
			 <th width='11%'>Lain2</th>
			 <th width='11%'>Total</th>
	 </tr>
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				//~ print_r($query);
				$temp_kodekel = "";
			 for($j=0;$j<count($query);$j++){
				 if ($temp_kodekel != $query[$j]['kode_kel']) {
					 $temp_kodekel = $query[$j]['kode_kel'];
		 ?>
					<tr>
						<td colspan="19">&nbsp;<b><?php echo $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ?></b></td>
					</tr>
		 <?php 
				 }
				
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_saldo_awal_warna=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_saldo_awal_warna += $var_detail_warna[$k]['saldo_awal_warna'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['saldo_awal_warna'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_saldo_awal_warna;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_masuk=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_masuk += $var_detail_warna[$k]['jum_masuk'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_masuk'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_masuk;
					}	
				echo "</td>";
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_masuk_perb=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_masuk_perb += $var_detail_warna[$k]['jum_perb_unit'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_perb_unit'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_masuk_perb;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_ret_unit_pack=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_ret_unit_pack += $var_detail_warna[$k]['jum_ret_unit_pack'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_ret_unit_pack'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_ret_unit_pack;
					}	
				echo "</td>";
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_ret_gd_jadi=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_ret_gd_jadi += $var_detail_warna[$k]['jum_ret_gd_jadi'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_ret_gd_jadi'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_ret_gd_jadi;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_masuk_lain=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_masuk_lain += $var_detail_warna[$k]['jum_masuk_lain'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_masuk_lain'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_masuk_lain;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_saldo_masuk_warna=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_saldo_masuk_warna += $var_detail_warna[$k]['saldo_masuk_warna'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['saldo_masuk_warna'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_saldo_masuk_warna;
					}	
				echo "</td>";
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_pack=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_pack += $var_detail_warna[$k]['jum_keluar_pack'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_pack'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_keluar_pack;
					}	
				echo "</td>";
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_gdjadi=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_gdjadi += $var_detail_warna[$k]['jum_keluar_gdjadi'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_gdjadi'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_keluar_gdjadi;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_perb=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_perb += $var_detail_warna[$k]['jum_keluar_perb'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_perb'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_keluar_perb;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_lain=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_lain += $var_detail_warna[$k]['jum_keluar_lain'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_lain'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_keluar_lain;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_keluar_total=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_keluar_total += $var_detail_warna[$k]['jum_keluar_total'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_keluar_total'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_keluar_total;
					}	
				echo "</td>";
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_saldo_akhir=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_saldo_akhir += $var_detail_warna[$k]['jum_saldo_akhir'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_saldo_akhir'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_saldo_akhir;
					}	
				echo "</td>";
				
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_stok_opname=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_stok_opname += $var_detail_warna[$k]['jum_stok_opname'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_stok_opname'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_stok_opname;
					}	
				echo "</td>";
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_jum_adjustment_b=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_jum_adjustment_b += $var_detail_warna[$k]['jum_adjustment_b'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['jum_adjustment_b'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_jum_adjustment_b;
					}	
				echo "</td>";
				 echo    "<td>";
				if(is_array($query[$j]['data_warna'])){
					$var_detail_warna=$query[$j]['data_warna'];
					$total_selisih=0;
					for($k=0;$k<count($var_detail_warna);$k++){
						$total_selisih += $var_detail_warna[$k]['selisih'];
						 echo $var_detail_warna[$k]['nama_warna'] ." : ". $var_detail_warna[$k]['selisih'] ;
						 if(count($var_detail_warna) > 0 ){
							 echo "<br>";
							 } 
						}
						echo "TOTAL = ". $total_selisih;
					}	
				echo "</td>";
			
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
