<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Transaksi Barang Hasil Cutting</h3><br><br>

<div>
Barang WIP: <b><?php echo $kode_brg_wip." - ".$nama_brg_wip; ?></b><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>

<?php 
// 17-10-2015, sementara ga usah pake fitur export excel transaksi
//$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
//echo form_open('info-mutasi-stok/creport/export_excel_transaksihasilcutting', $attributes); ?>
<!--<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="id_brg_wip" value="<?php echo $id_brg_wip ?>" >
<input type="hidden" name="kode_brg_wip" value="<?php echo $kode_brg_wip ?>" >
<input type="hidden" name="nama_brg_wip" value="<?php echo $nama_brg_wip ?>" >
<input type="hidden" name="nama_bulan" value="<?php echo $nama_bulan ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">-->
<?php //echo form_close();  ?>
<!--<br><br>-->
<?php
	$nomor = 1;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			//echo "<b>".$query[$a]['kode_brg_jadi']." - ".$query[$a]['nama_brg_jadi']."</b>"."<br>";
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		 <th width="5%">Tgl</th>
		 <th width="10%">Ket</th>
		 <th width="10%">No Bukti</th>
		 <th width="10%">Masuk Bagus</th>
		 <th width="10%">Masuk Lain</th>
		 <th width="10%">Masuk Retur</th>
		 <th width="10%">Keluar Bagus</th>
		 <th width="10%">Keluar Lain</th>
		 <th width="10%">Keluar Retur</th>
		 <th width="10%">Saldo</th>
	 </tr>
	</thead>
	<tbody>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
			<?php $data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['jum_stok_opname']."<br>";
				}
			}
		?>
			</td>
		</tr>
			
<?php
		$data_tabel1 = $query[$a]['data_tabel1'];
		if (is_array($data_tabel1)) {
			for($j=0;$j<count($data_tabel1);$j++){
?>
	<tr>
		<td><?php echo "&nbsp;".$data_tabel1[$j]['tgl_sj'] ?></td>
		<td style="white-space:nowrap;"><?php if ($data_tabel1[$j]['masuk'] == "ya") { 
			if ($data_tabel1[$j]['masuk_bgs'] == '1' || $data_tabel1[$j]['masuk_lain'] == '1') {
				echo "&nbsp;Masuk"; 
				if ($data_tabel1[$j]['keterangan'] != '')
					echo " / ".$data_tabel1[$j]['keterangan'];
			}
			else if ($data_tabel1[$j]['masuk_retur'] == '1') {
				echo "Masuk"; 
				if ($data_tabel1[$j]['keterangan'] != '')
					echo " / ".$data_tabel1[$j]['keterangan'];
				echo "<br>Unit Jahit: ".$data_tabel1[$j]['kode_unit']."-".$data_tabel1[$j]['nama_unit'];
			}
		}
		else {
			echo "Keluar"; 
			if ($data_tabel1[$j]['keterangan'] != '')
				echo " / ".$data_tabel1[$j]['keterangan'];
			echo "<br>Unit Jahit: ".$data_tabel1[$j]['kode_unit']."-".$data_tabel1[$j]['nama_unit'];
		}	
		?></td>
		<td><?php echo "&nbsp;".$data_tabel1[$j]['no_sj']; ?></td>
		<td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_bgs'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_lain'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_retur'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_bgs'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_lain'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_retur'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 		 
		 <td><?php
				$data_tabel1_saldo_perwarna = $data_tabel1[$j]['data_tabel1_saldo_perwarna'];
				if (is_array($data_tabel1_saldo_perwarna)) {
					for($z=0;$z<count($data_tabel1_saldo_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_saldo_perwarna[$z]['nama_warna'].": ".$data_tabel1_saldo_perwarna[$z]['saldo']."<br>";
					}
				}
			
			?>
		 </td>
	</tr>
	<?php
			}
		}
	?>
		<tr>
			<td colspan="3" align="center"><b>Total/Bulan</b></td>
			<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk1']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_lain1']."<br>";
				}
			}
		?></td>
		
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_retur1']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar1']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_lain1']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_retur1']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['saldo']."<br>";
				}
			}
		?></td>
		</tr>
	</tbody>
	</table>
			
			<br><br>
	<?php
		}
	}
	?>

</div>
