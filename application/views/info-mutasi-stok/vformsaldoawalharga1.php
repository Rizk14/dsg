<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Mutasi Stok Bahan Baku/Pembantu</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php if ($gudang != '0') { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else { echo "Semua"; } ?><br>
Kelompok: <?php if ($kelompok != '0') { echo $kode_perkiraan." - ".$nama_kelompok; } else { echo "All"; } ?><br>
Jenis Barang: <?php if ($jns_brg != '0') { echo $kode_jenis_brg." - ".$nama_jenis_brg; } else { echo "All"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
Jenis Laporan: <?php echo $nama_jenis ?><br>
<i>*) Data yang ditampilkan sudah dalam satuan konversi<br>
*) Data harga adalah diluar pajak<br><br></i>
<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" name="format_harga" value="<?php echo $format_harga ?>" >
<input type="hidden" name="kalkulasi_ulang_so" value="<?php echo $kalkulasi_ulang_so ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="hidden" name="kelompok" value="<?php echo $kelompok ?>" >
<input type="hidden" name="jenis_brg" value="<?php echo $jns_brg ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<?
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/update_saldoawalharga', $attributes); ?>
<input type="submit" name="update_saldoawalharga" id="update_saldoawalharga" value="Update Saldo Awal">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th rowspan="2">No</th>
		 <th rowspan="2">Kode Brg</th>
		 <th rowspan="2">Nama Barang</th>
		 <th rowspan="2">Sat Awal</th>
		 <th rowspan="2">Sat Konversi</th>
		 <th rowspan="2">Harga</th>
		 <th rowspan="2">PKP</th>
		 <th width="5%" colspan="2">Saldo Awal</th>
		 <th width="5%" colspan="2">Masuk</th>
		 <th width="5%" colspan="2">Masuk Lain</th>
		 <th width="5%" colspan="2">Keluar</th>
		 <th width="5%" colspan="2">Keluar Lain</th>
		 <th width="5%" colspan="2">Saldo Akhir</th>
		 <th width="5%" colspan="2">SO</th>
		 <th width="5%" colspan="2">Selisih</th>
	 </tr>
	 <tr class="judulnya">
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
	 </tr>
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				$tkodegudang="";
				$totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
				$totsaldoakhir=0; $totso = 0; $totselisih=0;
				$gtotsaldoawal = 0; $gtotmasuk = 0; $gtotmasuklain = 0; $gtotkeluar=0; $gtotkeluarlain=0;
				$gtotsaldoakhir=0; $gtotso = 0; $gtotselisih=0;
			 for($j=0;$j<count($query);$j++){
				 
				 if ($tkodegudang != $query[$j]['kode_gudang']) {
					 $tkodegudang = $query[$j]['kode_gudang'];
			?>
					<tr class="judulnya">
						<td colspan="23"><b><?php echo $query[$j]['kode_gudang']." - ".$query[$j]['nama_gudang'] ?></b></td>
					</tr>
			<?php
				 }
				 
				/* if ($query[$j]['pkp'] == 't') {
					$dpp = $query[$j]['jum_masuk_rp']/1.1;
					//$pajaknya = $total-$dpp;
				}
				else {
					$dpp = 0; //$pajaknya=0;
				} */
				 
				 // 08-10-2015
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					 if ($var_detail[0]['harga'] == 0 && $var_detail[0]['jum_stok_opname'] != 0)
						//$warna = "bgcolor="#FF0000"";
						echo "<tr class=\"record\" bgcolor='#FF0000'>";
				 }
				 else
					echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 echo    "<td style='white-space:nowrap;'>";
				 //<input type='hidden' name='id_satuan' value='".$query[$j]['id_satuan']."'>".$query[$j]['satuan'].
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_satuan_konv'];
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 //echo    "<td style='white-space:nowrap;'>".$query[$j]['nama_satuan_konv']."</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['harga'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='center'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_pkp'];
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['saldo_awal'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totsaldoawal+= $var_detail[$k]['saldo_awal_rp'];
						  echo number_format($var_detail[$k]['saldo_awal_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "</td>";
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_masuk'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totmasuk+= $var_detail[$k]['jum_masuk_rp'];
						$gtotmasuk+= $var_detail[$k]['jum_masuk_rp'];
						  echo number_format($var_detail[$k]['jum_masuk_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_masuk_lain'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totmasuklain+= $var_detail[$k]['jum_masuk_lain_rp'];
						  echo number_format($var_detail[$k]['jum_masuk_lain_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_keluar'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totkeluar+= $var_detail[$k]['jum_keluar_rp'];
						  echo number_format($var_detail[$k]['jum_keluar_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_keluar_lain'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totkeluarlain+= $var_detail[$k]['jum_keluar_lain_rp'];
						  echo number_format($var_detail[$k]['jum_keluar_lain_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['saldo_akhir'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						 $totsaldoakhir+= $var_detail[$k]['saldo_akhir_rp'];
						 $gtotsaldoakhir+= $var_detail[$k]['saldo_akhir_rp'];
						  echo number_format($var_detail[$k]['saldo_akhir_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_stok_opname'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totso+= $var_detail[$k]['jum_stok_opname_rp'];
						  echo number_format($var_detail[$k]['jum_stok_opname_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['selisih'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['data_perharga'])) {
					 $var_detail = $query[$j]['data_perharga'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$totselisih+= $var_detail[$k]['selisih_rp'];
						  echo number_format($var_detail[$k]['selisih_rp'],4,',','.');
						  if ($k<$hitung-1)
						     echo "<br>";
					}
				 }
				 echo "</td>";
				 
				 echo  "</tr>";
				 
				 
				 if (isset($query[$j+1]['kode_gudang']) && ($query[$j]['kode_gudang'] != $query[$j+1]['kode_gudang'])) {
					 echo "<tr>
						<td colspan='7' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoawal,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuk,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuklain,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluar,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluarlain,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoakhir,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totso,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totselisih,4,',','.')."</b></td>
					 </tr>";
					 
					 $totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
					$totsaldoakhir=0; $totso = 0; $totselisih=0;
				 }
				 else if (!isset($query[$j+1]['kode_gudang'])) {
					 echo "<tr>
						<td colspan='7' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoawal,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuk,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuklain,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluar,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluarlain,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoakhir,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totso,4,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totselisih,4,',','.')."</b></td>
					 </tr>";
				 }
		 	}
		 ?>
		<tr>
			<td colspan="7" align="center">GRAND TOTAL</td>
			<td colspan="2" align="right"><?php //echo $gtotsaldoawal ?></td>
			<td colspan="2" align="right"><?php echo number_format($gtotmasuk,4,',','.') ?></td>
			<td colspan="2" align="right"><?php //echo $gtotmasuklain ?></td>
			<td colspan="2" align="right"><?php //echo $gtotkeluar ?></td>
			<td colspan="2" align="right"><?php //echo $gtotkeluarlain ?></td>
			<td colspan="2" align="right"><?php echo number_format($gtotsaldoakhir,4,',','.') ?></td>
			<td colspan="2" align="right"><?php //echo $gtotso ?></td>
			<td colspan="2" align="right"><?php //echo $gtotselisih ?></td>
		</tr>
		 <?php
		   }
		 ?>
 	</tbody>
</table><br>
</div>
