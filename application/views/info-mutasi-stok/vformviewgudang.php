<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<script>
function updatesaldo(){
	var sahir = document.querySelectorAll('.updatesawal');
	var data = '';
	sahir.forEach(function(a,b){//a =element(variabel) b = index(line)
		data += a.innerHTML + '||';
		// data.push(JSON.stringify(a.innerHTML))
	})
	$.ajax({
		type : 'post',
		url  : '<?php echo site_url('info-mutasi-stok/cform/updatesawal')?>',
		data : {
			key : data
		},
		success:function(){
			alert("Sukses");
		}
	});
}

function updatesaldo_X(){
	var sahir = document.querySelectorAll('.updatesawal');
	var data = [];
	sahir.forEach(function(a,b){//a =element(variabel) b = index(line)
		data.push(a.innerHTML)
		// data.push(JSON.stringify(a.innerHTML))
	})
	$.ajax({
		type : 'post',
		url  : '<?php echo site_url('info-mutasi-stok/cform/updatesawal')?>',
		data : {
			key : JSON.stringify(data)
		},
		success:function(){
			alert("Sukses");
		}
	});
}
</script>	

<h3>Laporan Mutasi Stok Bahan Baku/Pembantu</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php if ($gudang != '0') { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else { echo "Semua"; } ?><br>
Jenis Laporan: <?php echo $nama_jenis ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >

<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >

<input type="hidden" name="format_harga" value="<?php echo $format_harga ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<input type="button" name="saldo" id="saldo" value="Update Saldo Awal Bulan Berikutnya" onclick='updatesaldo();'>
<?php echo form_close();  ?>
<?
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/update_saldoawal', $attributes); ?>
<!-- <input type="submit" name="update_saldoawal" id="update_saldoawal" value="Update Saldo Awal"> -->
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Kode Brg</th>
		 <th>Nama Barang</th>
		 <th>Sat Awal</th>
		 <?php 
		 	/*$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('username');
			if (($id == 'gudang1') or ($id == 'gudang2') ($id == 'gudang3')) {
			echo '
		 		 <th width="5%">Saldo Awal</th>
				 <th width="5%">Masuk</th>
				 <th width="5%">Masuk Lain</th>
				 <th width="5%">Keluar</th>
				 <th width="5%">Keluar Lain</th>
				 <th width="5%">Saldo Akhir</th>
				 <th width="5%">SO</th>
				 <th width="5%">Selisih</th>';
			}else{
				echo '<th>Satuan Konversi</th>
		 		 <th width="5%">Saldo Awal</th>
				 <th width="5%">Masuk</th>
				 <th width="5%">Masuk Lain</th>
				 <th width="5%">Keluar</th>
				 <th width="5%">Keluar Lain</th>
				 <th width="5%">Saldo Akhir</th>
				 <th width="5%">SO</th>
				 <th width="5%">Selisih</th>';
			}*/
		 ?>
		 <th>Satuan Konversi</th>
 		 <th width="5%">Saldo Awal</th>
		 <th width="5%">Masuk Sat Awal</th>
		 <th width="5%">Masuk Konversi</th>
		 <th width="5%">Masuk Lain</th>
		 <th width="5%">Keluar</th>
		 <th width="5%">Keluar Lain</th>
		 <th width="5%">Saldo Akhir</th>
		 <th width="5%">SO</th>
		 <th width="5%">Selisih</th>
		 <th  hidden='true'></th>
	 </tr>
	</thead>
	<tbody>
		 <?		
			if (is_array($query)) {
				$tkodegudang="";
				$totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
				$totsaldoakhir=0; $totso = 0; $totselisih=0;
			 for($j=0;$j<count($query);$j++){
				 
				 if ($tkodegudang != $query[$j]['kode_gudang']) {
					 $tkodegudang = $query[$j]['kode_gudang'];
			?>
					<tr class="judulnya">
						<td colspan="22"><b><?php echo $query[$j]['kode_gudang']." - ".$query[$j]['nama_gudang'] ?></b></td>
					</tr>
			<?php
				 }
				 				 
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 echo    "<td><input type='hidden' name='id_satuan' value='".$query[$j]['id_satuan']."'>".$query[$j]['satuan']."</td>";
				 
				/*$sess=$this->session->userdata('session_id');	
				$id=$this->session->userdata('username');
					if (($id == 'gudang1') or ($id == 'gudang2') ($id == 'gudang3')) {
				 		echo "";
				 	}else {
				 		echo "<td>".$query[$j]['nama_satuan_konv']."</td>";
				 	}*/
				 echo "<td>".$query[$j]['nama_satuan_konv']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['saldo_awal'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_konv'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_lain'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar_lain'],2,',','.')."</td>";
				 $saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				 echo    "<td align='right'>".number_format($saldo_akhir,2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_stok_opname'],2,',','.')."</td>";
				 $selisih = $query[$j]['jum_stok_opname']-$saldo_akhir;
				 $selisih = round($selisih, 2);
				 echo    "<td align='right'>".number_format($selisih,2,',','.')."</td>";

				 $sawal = json_encode(
					array(
						$query[$j]['kode_brg'], 
						$query[$j]['kode_gudang'],
						$saldo_akhir,
						$date_to,
						$query[$j]['id_satuan'],
				)
				);
				 
				 echo  "
				 <td ><span hidden='true' class='updatesawal'>$sawal</span></td>
				 </tr>";
				 
				 				 
			/*	 if (isset($query[$j+1]['kode_gudang']) && ($query[$j]['kode_gudang'] != $query[$j+1]['kode_gudang'])) {
					 echo "<tr>
						<td colspan='6' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoawal,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuk,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuklain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluar,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluarlain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoakhir,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totso,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totselisih,2,',','.')."</b></td>
					 </tr>";
					 
					 $totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
					$totsaldoakhir=0; $totso = 0; $totselisih=0;
				 }
				 else if (!isset($query[$j+1]['kode_gudang'])) {
					 echo "<tr>
						<td colspan='6' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoawal,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuk,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuklain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluar,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluarlain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoakhir,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totso,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totselisih,2,',','.')."</b></td>
					 </tr>";
				 } */
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
