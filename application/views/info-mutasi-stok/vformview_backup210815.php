<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Mutasi Stok Bahan Baku/Pembantu</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php if ($gudang != '0') { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else { echo "Semua"; } ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>
<!--<i>*) Untuk bahan yg satuannya Yard, maka Qty yg dikeluarkan itu dlm bentuk Meter (kecuali quilting, tetap Yard)<br>
	*) Untuk bahan yg satuannya Lusin, maka Qty yg dikeluarkan itu dlm bentuk Pieces<br>
	*) Untuk satuan lainnya, maka Qty yg dikeluarkan itu tidak ada konversi satuan</i><br><br> -->
<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" name="format_harga" value="<?php echo $format_harga ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th rowspan="2">No</th>
		 <th rowspan="2">Kode Brg</th>
		 <th rowspan="2">Nama Barang</th>
		 <th rowspan="2">Sat Awal</th>
		 <th rowspan="2">Sat Konversi</th>
		 <th rowspan="2">Harga</th>
		 <th width="5%" colspan="2">Saldo Awal</th>
		 <th width="5%" colspan="2">Masuk</th>
		 <th width="5%" colspan="2">Masuk Lain</th>
		 <th width="5%" colspan="2">Keluar</th>
		 <th width="5%" colspan="2">Keluar Lain</th>
		 <th width="5%" colspan="2">Saldo Akhir</th>
		 <th width="5%" colspan="2">SO</th>
		 <th width="5%" colspan="2">Selisih</th>
	 </tr>
	 <tr class="judulnya">
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<!--<th>DPP</th>-->
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
		<th>Qty</th>
		<th>Jumlah</th>
	 </tr>
	</thead>
	<tbody>
		 <?		
			if (is_array($query)) {
				$tkodegudang="";
				$totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
				$totsaldoakhir=0; $totso = 0; $totselisih=0;
			 for($j=0;$j<count($query);$j++){
				 
				 if ($tkodegudang != $query[$j]['kode_gudang']) {
					 $tkodegudang = $query[$j]['kode_gudang'];
			?>
					<tr class="judulnya">
						<td colspan="22"><b><?php echo $query[$j]['kode_gudang']." - ".$query[$j]['nama_gudang'] ?></b></td>
					</tr>
			<?php
				 }
				 
				/* if ($query[$j]['pkp'] == 't') {
					$dpp = $query[$j]['jum_masuk_rp']/1.1;
					//$pajaknya = $total-$dpp;
				}
				else {
					$dpp = 0; //$pajaknya=0;
				} */
				 
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg']."</td>";
				 echo    "<td>".$query[$j]['nama_brg']."</td>";
				 echo    "<td><input type='hidden' name='id_satuan' value='".$query[$j]['id_satuan']."'>".$query[$j]['satuan']."</td>";
				 
				 echo    "<td>".$query[$j]['nama_satuan_konv']."</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['harga'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['saldo_awal'],2,',','.')."</td>";
				 //echo    "<td align='right'>".number_format($query[$j]['saldo_awal_rp'],2,',','.')."</td>";
				 echo    "<td align='right'>&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_rp'],2,',','.')."</td>";
				// echo    "<td align='right'>".number_format($dpp,2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_lain'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_lain_rp'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar_rp'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar_lain'],2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar_lain_rp'],2,',','.')."</td>";
				 $saldo_akhir = $query[$j]['saldo_awal']+$query[$j]['jum_masuk']+$query[$j]['jum_masuk_lain']-$query[$j]['jum_keluar']-$query[$j]['jum_keluar_lain'];
				 $saldo_akhir_rp = $saldo_akhir*$query[$j]['harga'];
				 echo    "<td align='right'>".number_format($saldo_akhir,2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($saldo_akhir_rp,2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_stok_opname'],2,',','.')."</td>";
				 $selisih = abs($query[$j]['jum_stok_opname']-$saldo_akhir);
				 $selisih_rp = $selisih*$query[$j]['harga'];
				 //echo    "<td align='right'>".number_format($query[$j]['jum_stok_opname_rp'],2,',','.')."</td>";
				 echo    "<td align='right'>&nbsp;</td>";
				 echo    "<td align='right'>".number_format($selisih,2,',','.')."</td>";
				 echo    "<td align='right'>".number_format($selisih_rp,2,',','.')."</td>";
				 echo  "</tr>";
				 
				 //$totsaldoawal+= $query[$j]['saldo_awal_rp'];
				 $totmasuk+= $query[$j]['jum_masuk_rp'];
				 $totmasuklain+= $query[$j]['jum_masuk_lain_rp'];
				 $totkeluar+= $query[$j]['jum_keluar_rp'];
				 $totkeluarlain+= $query[$j]['jum_keluar_lain_rp'];
				 $totsaldoakhir+= $saldo_akhir_rp;
				 //$totso+= $query[$j]['jum_stok_opname_rp'];
				 $totselisih+= $selisih_rp;
				 
				 if (isset($query[$j+1]['kode_gudang']) && ($query[$j]['kode_gudang'] != $query[$j+1]['kode_gudang'])) {
					 echo "<tr>
						<td colspan='6' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoawal,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuk,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuklain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluar,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluarlain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoakhir,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totso,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totselisih,2,',','.')."</b></td>
					 </tr>";
					 
					 $totsaldoawal = 0; $totmasuk = 0; $totmasuklain = 0; $totkeluar=0; $totkeluarlain=0;
					$totsaldoakhir=0; $totso = 0; $totselisih=0;
				 }
				 else if (!isset($query[$j+1]['kode_gudang'])) {
					 echo "<tr>
						<td colspan='6' align='center'><b>TOTAL</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoawal,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuk,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totmasuklain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluar,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totkeluarlain,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totsaldoakhir,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totso,2,',','.')."</b></td>
						<td colspan='2' align='right'><b>".number_format($totselisih,2,',','.')."</b></td>
					 </tr>";
				 }
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
