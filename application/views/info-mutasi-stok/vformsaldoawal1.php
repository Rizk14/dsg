<style type="text/css">
table {
    font-family: Helvetica, Geneva, Arial,
        SunSans-Regular, sans-serif;
    font-size: 11px;
}

.fieldsetdemo {
    background-color: #DDD;
    max-width: 500px;
    padding: 14px;
}

.judulnya {
    background-color: #DDD;
}
</style>

<h3>Laporan Mutasi Stok Bahan Baku/Pembantu</h3><br><br>

<div>

    Lokasi Gudang:
    <?php if ($gudang != '0') { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else { echo "Semua"; } ?><br>
    Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

    <?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel', $attributes); ?>

    <input type="hidden" name="date_to" value="<?php echo $date_to ?>">

    <input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>">
    <input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>">
    <input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>">

    <!--<input type="hidden" name="format_harga" value="<?php echo $format_harga ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
-->
    <?php echo form_close();  ?>

    <?php
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/savesaldoawal', $attributes); ?>
    <input type="submit" name="savesaldoawal" id="savesaldoawal" value="Update to Saldo Awal">
    <input type="hidden" name="date_from" value="<?php echo $date_from ?>">
    <input type="hidden" name="id_gudang" value="<?php echo $gudang ?>">
    <br><br>
    <table border="1" cellpadding="1" cellspacing="1" width="100%">
        <thead>
            <tr class="judulnya">
                <th>No</th>
                <th>Kode Brg</th>
                <th>Nama Barang</th>
                <th>Nama Gudang</th>
                <th>Satuan Awal</th>
                <th>Saldo Awal</th>
                <th>Masuk</th>
                <th>Masuk Lain</th>
                <th>Keluar</th>
                <th>Keluar Lain</th>
                <th>Saldo Akhir</th>
                <th>SO</th>
                <th>Selisih</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0;
         //var_dump($brg);
            foreach ($brg->result() as $row) {
        //var_dump(count($brg));
            $i++;?>
            <td>
                <?php echo $i?>
            </td>
            <td>
                <input type="text" id="id_brg<?=$i;?>" style="width:70px" name="id_brg<?=$i;?>"
                    value="<?php echo $row->id_brg ?>" readonly>
                <input type="text" id="kode<?=$i;?>" style="width:70px" name="kode<?=$i;?>"
                    value="<?php echo $row->kode ?>" readonly>
            </td>
            <td>
                <input type="text" id="barang<?=$i;?>" style="width:250px" name="barang<?=$i;?>"
                    value="<?php echo $row->barang; ?>" readonly>
            </td>
            <td>
                <input type="text" id="gudang<?=$i;?>" style="width:120px" name="gudang<?=$i;?>"
                    value="<?php echo $row->gudang; ?>" readonly>
            </td>
            <td>
                <input type="text" id="satuan<?=$i;?>" style="width:70px" name="satuan<?=$i;?>"
                    value="<?php echo $row->satuan; ?>" readonly>
            </td>
            <td>
                <input type="text" id="saldoawalfake<?=$i;?>" style="width:70px" name="saldoawalfake<?=$i;?>"
                    value="<?php echo number_format($row->saldoawal,2); ?>" readonly>
                <input type="hidden" id="saldoawal<?=$i;?>" style="width:70px" name="saldoawal<?=$i;?>"
                    value="<?php echo $row->saldoawal; ?>" readonly>
            </td>
            <td>
                <input type="text" id="bonmasuk1<?=$i;?>" style="width:70px" name="bonmasuk1<?=$i;?>"
                    value="<?php echo number_format($row->bonmasuk1,2); ?>" readonly>
            </td>
            <td>
                <input type="text" id="bonmasuklain<?=$i;?>" style="width:70px" name="bonmasuklain<?=$i;?>"
                    value="<?php echo number_format($row->bonmasuklain,2); ?>" readonly>
            </td>
            <td>
                <input type="text" id="bonkeluar<?=$i;?>" style="width:70px" name="bonkeluar<?=$i;?>"
                    value="<?php echo number_format($row->bonkeluar,2); ?>" readonly>
            </td>
            <td>
                <input type="text" id="bonkeluarlain<?=$i;?>" style="width:70px" name="bonkeluarlain<?=$i;?>"
                    value="<?php echo number_format($row->bonkeluarlain,2); ?>" readonly>
            </td>
            <td>
                <input type="text" id="saldoakhir<?=$i;?>" style="width:70px" name="saldoakhir<?=$i;?>"
                    value="<?php echo number_format($row->saldoakhir,2); ?>" readonly>
            </td>
            <td>
                <input type="text" id="so<?=$i;?>" style="width:70px" name="so<?=$i;?>"
                    value="<?php echo number_format($row->so,2); ?>" readonly>
            </td>
            <td>
                <input type="text" id="selisih<?=$i;?>" style="width:70px" name="selisih<?=$i;?>"
                    value="<?php echo number_format($row->selisih,2); ?>" readonly>
            </td>
            <!--<td>
                <?php echo $i?>
            </td>
            <td>            
                <?php echo $row->kode ?>   
            </td> 
            <td>                  
                <?php echo $row->barang; ?>
            </td>
            <td>                  
                <?php echo $row->gudang; ?>
            </td>
            <td>                  
                <?php echo $row->satuan; ?>
            </td> 
            <td>                  
                <?php echo $row->saldoawal; ?>    
            </td> 
            <td>                  
                <?php echo $row->bonmasuk1; ?>
            </td>
            <td>                  
                <?php echo $row->bonmasuklain; ?>
            </td>
            <td>                  
                <?php echo $row->bonkeluar; ?>
            </td>   
            <td>                  
                <?php echo $row->bonkeluarlain; ?>    
            </td> 
            <td>                  
                <?php echo $row->saldoakhir; ?>
            </td>
            <td>                  
                <?php echo $row->so; ?>
            </td>
            <td>                  
                <?php echo $row->selisih; ?>
            </td>-->
            <td>

            </td>
            </tr>

            <?php }?>
            <input type="text" name="jml" id="jml" value="<?php echo $i; ?>" readonly> <br>
        </tbody>
    </table>
    <?php echo form_close();  ?>
</div>