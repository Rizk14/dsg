<h3>Update Saldo Awal</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<!--<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>-->
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-mutasi-stok/cform/saldoawal', $attributes); ?>
<table width="60%">
	<tr>
			<td width="20%">Lokasi Gudang</td>
			<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
  <tr>
		<td>Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
		</td>
  </tr>
		
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/stok-opname/cform'">
<?php echo form_close();  ?>