<h3>Input Stok Opname Berdasarkan Harga Manual</h3><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
										
		//*****tgl_bukti*************************************
		var tgl_bukti="#tgl_bukti_"+n;
		var new_tgl_bukti="#tgl_bukti_"+no;
		$(tgl_bukti, lastRow).attr("id", "tgl_bukti_"+no);
		$(new_tgl_bukti, lastRow).attr("name", "tgl_bukti_"+no);		
		$(new_tgl_bukti, lastRow).val('');		
		//*****end tgl_bukti*********************************
		
		//*****imgtgl_bukti*************************************
		var imgtgl_bukti="#imgtgl_bukti_"+n;
		var new_imgtgl_bukti="#imgtgl_bukti_"+no;
		$(imgtgl_bukti, lastRow).attr("id", "imgtgl_bukti_"+no);
		$(new_imgtgl_bukti, lastRow).attr("name", "imgtgl_bukti_"+no);
		var  even_klik= "displayCalendar(document.forms[0].tgl_bukti_"+no+",'dd-mm-yyyy',this)";
		$(new_imgtgl_bukti, lastRow).attr("onclick",even_klik);	
		//*****end imgtgl_bukti*********************************
		
		//*****no_bukti*************************************
		var no_bukti="#no_bukti_"+n;
		var new_no_bukti="#no_bukti_"+no;
		$(no_bukti, lastRow).attr("id", "no_bukti_"+no);
		$(new_no_bukti, lastRow).attr("name", "no_bukti_"+no);		
		$(new_no_bukti, lastRow).val('');		
		//*****end no_bukti*********************************
				
		//*****jumlahx*************************************
		var jumlahx="#jumlahx_"+n;
		var new_jumlahx="#jumlahx_"+no;
		$(jumlahx, lastRow).attr("id", "jumlahx_"+no);
		$(new_jumlahx, lastRow).attr("name", "jumlahx_"+no);
		var even_blur= "$('#jumlah_"+no+"').val(this.value); var angka = formatMoney(this.value, 4,',','.'); this.value= angka;";
		$(new_jumlahx, lastRow).attr("onblur",even_blur);	
		$(new_jumlahx, lastRow).val('');
		//*****end jumlahx*************************************	
		
		//*****jumlah*************************************
		var jumlah="#jumlah_"+n;
		var new_jumlah="#jumlah_"+no;
		$(jumlah, lastRow).attr("id", "jumlah_"+no);
		$(new_jumlah, lastRow).attr("name", "jumlah_"+no);
		$(new_jumlah, lastRow).val('');				
		//*****end jumlah*************************************	
														
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_data() {
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if ($('#kode_'+k).val() == '') {
					alert("Barang harus dipilih...!");
					s = 1;
					return false;
				}
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Data harga harus berupa angka..!");
					s = 1;
					return false;
				}
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Data qty harus berupa angka..!");
					s = 1;
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}

function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pembelian/cform/submitopnamehutang" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
			<td width="20%">Lokasi Gudang</td>
			<td> <select name="gudang" id="gudang">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		
	<tr>
		<td width="20%">Periode (bulan-tahun)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>	
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku" width="90%" border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="8" align="right">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="3%">No</th>
          <th>Kode</th>
          <th>Nama Barang</th>
          <th>Satuan Awal</th>
          <th>Satuan Konversi</th>
          <th>Harga</th>
          <th>PKP</th>
          <th>Qty</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td> 
          <td style="white-space:nowrap;"><input name="kode_1" type="text" id="kode_1" size="7" value="" readonly="true" /> 
          <input title="browse data barang" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#gudang').val();
            openCenteredWindow('<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/show_popup_brg/'+ x+'/1');" >
          </td>
          <td><input name="nama_1" type="text" id="nama_1" size="40" value="" readonly="true" /> </td>
          <td><input name="satuan_1" type="text" id="satuan_1" size="7" value="" readonly="true" /> </td>
          <td><input name="satuan_konv_1" type="text" id="satuan_konv_1" size="7" value="" readonly="true" /> </td>
		  <td><input style="text-align:right;" name="harga_1" type="text" id="harga_1" size="10" value="" /></td>
		  <td><input type="checkbox" name="is_pkp_1" id="is_pkp_1" value="t"></td>
		  <td><input style="text-align:right;" name="qty_1" type="text" id="qty_1" size="10" value="" /></td>
          
        </tr>
	</table>	
	
	</form><br>
	<div align="left"><input type="submit" name="submit2" value="Simpan" onclick="return cek_data();"></div>

     </td>
    </tr>

</table>
</div>
</form>
