<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Stok Bahan Hasil Cutting</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-mutasi-stok/cform/export_excel_hasilcutting', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width='3%' rowspan='2'>No</th>
		 <th width='15%' rowspan='2'>Kode</th>
		 <th width='25%' rowspan='2'>Nama Brg WIP</th>
		 <th width='8%' rowspan='2'>Satuan</th>
		 <th width='8%' rowspan='2'>Saldo<br>Awal</th>
		 <th colspan="3">Masuk</th>
		 <th width='8%' rowspan='2'>Total<br>Masuk</th>
		 <th colspan="3">Keluar</th>
		 <th width='8%' rowspan='2'>Total<br>Keluar</th>
		 
		 <th width='8%' rowspan='2'>Saldo Akhir</th>
		 <th width='8%' rowspan='2'>Stok Opname</th>
		 <th width='8%' rowspan='2'>Selisih</th>
	 </tr>
	 <tr class="judulnya">
			 <th width='8%'>Gudang<br>Pengadaan</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Retur</th>
			 <th width='8%'>Kirim<br>Unit</th>
			 <th width='8%'>Lain2</th>
			 <th width='8%'>Retur</th>
	 </tr>
	</thead>
	<tbody>
		 <?php		
			if (is_array($query)) {
				
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td width='2%' align='center'>".($j+1)."</td>";
				 echo    "<td>".$query[$j]['kode_brg_wip']."</td>";
				 echo    "<td>".$query[$j]['nama_brg_wip']."</td>";
				 echo    "<td>Set</td>";
				 echo    "<td align='right'>".number_format($query[$j]['saldo_awal'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_bgs'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_lain'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_masuk_retur'],0,',','.')."&nbsp;</td>";
				 
				 $tot_masuk = $query[$j]['jum_masuk_bgs']+$query[$j]['jum_masuk_lain']+$query[$j]['jum_masuk_retur'];
				 echo    "<td align='right'>".number_format($tot_masuk,0,',','.')."&nbsp;</td>";
				 
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar_bgs'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar_lain'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_keluar_retur'],0,',','.')."&nbsp;</td>";
				 
				 $tot_keluar = $query[$j]['jum_keluar_bgs']+$query[$j]['jum_keluar_lain']+$query[$j]['jum_keluar_retur'];
				 echo    "<td align='right'>".number_format($tot_keluar,0,',','.')."&nbsp;</td>";
				 
				 $saldo_akhir = $query[$j]['saldo_awal']+$tot_masuk-$tot_keluar;
				 $selisih = $query[$j]['jum_stok_opname']-$saldo_akhir;
				 
				 echo    "<td align='right'>".number_format($saldo_akhir,0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($query[$j]['jum_stok_opname'],0,',','.')."&nbsp;</td>";
				 echo    "<td align='right'>".number_format($selisih,0,',','.')."&nbsp;</td>";
				 echo  "</tr>";					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</div>
