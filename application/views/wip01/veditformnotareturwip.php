<h3>Data Nota Retur Barang WIP (Hasil Jahit)</h3><br>
<a href="<?php echo base_url(); ?>index.php/wip/cform/notareturwipadd">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/wip/cform/notareturwipview">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

$(function()
{	  
	  hitungnilai();
		
});

function cek_nota() {
	var no_nota= $('#no_nota').val();
	var tgl_nota= $('#tgl_nota').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		if (no_nota == '') {
			alert("Nomor Nota harus diisi..!");
			s = 1;
			return false;
		}
		if (tgl_nota == '') {
			alert("Tanggal Nota harus dipilih..!");
			s = 1;
			return false;
		}
		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if ($('#kode_'+k).val() == '') {
					alert("Data item barang tidak boleh ada yang kosong...!");
					s = 1;
					return false;
				}
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Harga harus berupa angka..!");
					s = 1;
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // end if kon
	else
		return false;
}

function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
	var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty_hasil=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					var hitung = (harga*qty_hasil)-diskon;
					
					// 24-07-2013
					$("#total_"+i).val(hitung);
					var hitungformat = formatMoney(hitung, 2,',','.');
					$("#total2_"+i).val(hitungformat);
					
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				
				$("#gtotal").val(gtotal);
				var hitungformat = formatMoney(gtotal, 2,',','.');
				$("#gtotal2").val(hitungformat);
}

</script>

<form name="f_wip" id="f_wip" action="<?php echo base_url(); ?>index.php/wip/cform/notareturwipsubmit" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_nota" value="<?php echo $query[0]['id'] ?>">
<input type="hidden" name="unit_jahit" value="<?php echo $query[0]['kode_unit_jahit'] ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="tgl_awal" value="<?php echo $tgl_awal ?>">
<input type="hidden" name="tgl_akhir" value="<?php echo $tgl_akhir ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<?php $detail_nota = $query[0]['detail_nota']; 
	$no = 1;
	$jumawal = count($detail_nota)+1;
	
	foreach ($detail_nota as $hitung) {
		$no++;
	}
?>
Edit Data

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
<div align="center">
<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
		<td>Unit Jahit</td>
		<td><?php echo $query[0]['kode_unit_jahit']." - ".$query[0]['nama_unit_jahit'] ?></td> 
	</tr>

  <tr>
    <td>Nomor Nota</td>
    <td>
      <input name="no_nota" type="text" id="no_nota" size="10" maxlength="20" value="<?php echo $query[0]['no_nota'] ?>" >&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Tgl Nota</td>
    <td>
	<label>
      <input name="tgl_nota" type="text" id="tgl_nota" size="10" value="<?php echo $query[0]['tgl_nota'] ?>" readonly="true">
    </label>
	   <img alt="" id="tgl_nota" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_nota,'dd-mm-yyyy',this)">
	</td>
  </tr>
  
  <tr>
    <td>Keterangan</td>
    <td>
      <input name="ket" type="text" id="ket" size="30" maxlength="60" value="<?php echo $query[0]['keterangan'] ?>">&nbsp;
    </td>    
  </tr>

  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>No / Tgl SJ Keluar</th>
           <th>Kode & Nama Barang Jadi</th>
	      <th>Qty (Pcs)</th>
	      <th>Harga (Rp.)</th>
	      <th>Diskon (Rp.)</th>
	      <th>Total (Rp.)</th>
        </tr>
		<?php $i=1;
        if (count($query[0]['detail_nota'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
        <?php
		} else {
			$detail_nota = $query[0]['detail_nota'];
			for($k=0;$k<count($detail_nota);$k++){
			?>
			
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          
          <td nowrap="nowrap">
           <input name="no_sj_keluar_<?php echo $i ?>" type="text" id="no_sj_keluar_<?php echo $i ?>" size="25" readonly="true" value="<?php echo $detail_nota[$k]['no_sj']." / ".$detail_nota[$k]['tgl_sj'] ?>"/>
           <input type="hidden" name="id_nota_retur_detail_<?php echo $i ?>" value="<?php echo $detail_nota[$k]['id'] ?>" >
           <input type="hidden" name="id_sj_keluar_detail_<?php echo $i ?>" value="<?php echo $detail_nota[$k]['id_sjkeluarwip_detail'] ?>" >
           </td>
          
          <td nowrap="nowrap">
           <input name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="40" readonly="true" value="<?php echo  $detail_nota[$k]['kode_brg_jadi']." - ".$detail_nota[$k]['nama_brg_jadi'] ?>"/>
           <input type="hidden" name="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $detail_nota[$k]['kode_brg_jadi'] ?>" >
           </td>
          
          <td><input name="qty_<?php echo $i ?>" type="text" style="text-align:right" id="qty_<?php echo $i ?>" size="5" maxlength="5" 
          value="<?php echo $detail_nota[$k]['qty'] ?>" readonly="true"  /> </td>
          
          <td nowrap="nowrap">
           <input name="harga_<?php echo $i ?>" type="text" style="text-align:right" id="harga_<?php echo $i ?>" size="10" value="<?php echo $detail_nota[$k]['harga'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()"/>
           <input name="harga_lama_<?php echo $i ?>" type="hidden" id="harga_lama_<?php echo $i ?>" value="<?php echo $detail_nota[$k]['harga'] ?>" />
          </td>
          <td nowrap="nowrap">
           <input name="diskon_<?php echo $i ?>" type="text" style="text-align:right" id="diskon_<?php echo $i ?>" size="10" value="<?php echo $detail_nota[$k]['diskon'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()"/>
          </td>
          
          <td nowrap="nowrap">
           <input readonly="true" name="total2_<?php echo $i ?>" type="text" style="text-align:right" id="total2_<?php echo $i ?>" size="10" value="<?php echo $detail_nota[$k]['total'] ?>"/>
           <input name="total_<?php echo $i ?>" type="hidden" id="total_<?php echo $i ?>" value="<?php echo $detail_nota[$k]['total'] ?>"/>
          </td>
          
        </tr>
        <?php $i++; } // end foreach 
		}
		?>
	</table>	
	
	</form>
      </td>
    </tr>
    
    <tr>
		<td colspan="2">Grand Total: &nbsp;&nbsp;<input type="text" name="gtotal2" id="gtotal2" value="<?php echo $query[0]['gtotal'] ?>" size="10" readonly="true">
			<input type="hidden" name="gtotal" id="gtotal" value="<?php echo $query[0]['gtotal'] ?>">
		</td>
	</tr>
	<?php
			if ($carinya == '') $carinya = "all";
				$url_redirectnya = "wip/cform/notareturwipview/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
        ?>
    <tr>
		<td colspan="2" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_nota();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
	</tr>

</table>
</div>
</form>
