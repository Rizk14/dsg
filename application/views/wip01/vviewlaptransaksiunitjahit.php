<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Transaksi Barang WIP (Hasil Jahit) Di Unit Jahit</h3><br><br>

<div>
Unit Jahit: <?php echo $kode_unit." - ".$nama_unit ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<input type="hidden" name="jumbrg" id="jumbrg" value="<?php echo $jumbrg ?>" >
<input name="cmdreset" id="cmdreset" value="Export Ke Excel" type="button">
<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('wip/creport/export_excel_transaksiunitjahit', $attributes); ?>
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="kode_unit" value="<?php echo $kode_unit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >
<input type="hidden" name="list_id_brg_wip" value="<?php echo $list_id_brg_wip ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel SO (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS SO (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<?php
	$nomor = 1;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="100%" id="sitabel_<?php echo $nomor ?>">
	<thead>
	<?php
	$total_masuk_bagus_back=	0;
		$data_tabel7 = $query[$a]['data_tabel7'];
	
		if (is_array($data_tabel7)){
			for($v=0;$v<count($data_tabel7);$v++){
			$total_masuk_bagus_back += $data_tabel7[$v]['masuk_bgsback'];
		//print_r($total_masuk_bagus_back);
	}
}

	if($total_masuk_bagus_back == 0   ){
	echo "<tr class=\"record\"  bgcolor='#ff3333'>";
	}
		else
	echo "<tr class=\"record\">";	
		
	?>
	
		<th colspan="14"><b><?php echo $query[$a]['kode_brg_wip']." - ".$query[$a]['nama_brg_wip']; ?></b></th>
	</tr>

	 <tr class="judulnya">
		 <th>Tgl</th>
		 <th>Ket</th>
		 <th>No Bukti</th>
		 <th>Masuk Bagus</th>
		 <th>Masuk<br>Retur Perbaikan</th>
		 <th>Masuk Lain</th>
		 <!--<th>Masuk<br>Pengembalian Retur Bhn Baku</th>-->
		 <th>Keluar Bagus<br>QC</th>
		 <th>Keluar Bagus<br>Gdg Jadi</th>
		 <th>Keluar<br>Hasil Perbaikan</th>
		 <th>Keluar Lain</th>
		 <th>Keluar<br>Retur Bhn Baku</th>
		 <th>Keluar<br>ke Unit Packing</th>
		 <th>Saldo<br>Per Warna</th>
		 <th>Saldo Global</th>
	 </tr>
	</thead>
	<tbody>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
			<?php $data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['saldo']."<br>";
				}
			}
		?>
			</td>
			<td align="right">
			<?php //$data_so_warna = $query[$a]['data_so_warna'];
			//if (is_array($data_so_warna)) {
			//	for($zz=0;$zz<count($data_so_warna);$zz++){ 
			//		echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['jum_stok_opname']."<br>";
			//	}
			//}
			//echo $query[$a]['saldo_akhir'];
		//	echo $query[$a]['tot_jum_stok_opname'];
		echo $query[$a]['tot_jum_stok_opname'];
		?>
			</td>
		</tr>
			
<?php
		$data_tabel1 = $query[$a]['data_tabel1'];
		if (is_array($data_tabel1)) {
			for($j=0;$j<count($data_tabel1);$j++){
?>
	<tr>
		<td style="white-space:nowrap;"><?php echo "&nbsp;".$data_tabel1[$j]['tgl_sj'] ?></td>
		<td style="white-space:nowrap;"><?php if ($data_tabel1[$j]['masuk'] == "ya") { 
			if ($data_tabel1[$j]['masuk_bgs'] == '1' || $data_tabel1[$j]['masuk_retur_perb'] == '1' || $data_tabel1[$j]['masuk_lain'] == '1' || $data_tabel1[$j]['masuk_lain2'] == '1' || $data_tabel1[$j]['masuk_lain3'] == '1' || $data_tabel1[$j]['masuk_pengembalian_retur'] == '1' || $data_tabel1[$j]['masuk_other'] == '1'){
				echo "Masuk"; 
			}
		}
		else {
			echo "Keluar"; 
		}	
		?></td>
		<td><?php echo $data_tabel1[$j]['no_sj']; ?></td>
		<td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_bgs'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_retur_perb'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && ($data_tabel1[$j]['masuk_lain'] == '1' || $data_tabel1[$j]['masuk_lain2'] == '1' || $data_tabel1[$j]['masuk_lain3'] == '1'|| $data_tabel1[$j]['masuk_other'] == '1')) {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		<!-- <td><?php /*if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_pengembalian_retur'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			} */
		 ?>
		 </td>-->
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_bgs'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_hasil_perb'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && ($data_tabel1[$j]['keluar_lain'] == '1' || $data_tabel1[$j]['keluar_lain2'] == '1' || $data_tabel1[$j]['keluar_other'] == '1')) {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_retur_bhnbaku'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		  <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_unit_packing'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 		 
		 <td><?php
				$data_tabel1_saldo_perwarna = $data_tabel1[$j]['data_tabel1_saldo_perwarna'];
				if (is_array($data_tabel1_saldo_perwarna)) {
					$totglobal = $query[$a]['tot_jum_stok_opname'];
					$data_so_warna = $query[$a]['data_so_warna'];
				
					for($z=0;$z<count($data_tabel1_saldo_perwarna);$z++){
						$data_so_warnasaldo=$data_so_warna[$z]['saldo'];
					echo "<br>".$data_tabel1_saldo_perwarna[$z]['nama_warna'].": ".$data_so_warnasaldo+=$data_tabel1_saldo_perwarna[$z]['saldo']."<br>";
					
					$totglobal+=$data_tabel1_saldo_perwarna[$z]['saldo'];
					}
					echo "<br>Total: ".$totglobal;
				}
				//echo $data_tabel1[$j]['tot_saldo'];
			
			?>
		 </td>
		 <td align="right"><?php
				echo $data_tabel1[$j]['tot_saldo'];
			
			?>
		 </td>
	</tr>
	<?php
			}
		}
	?>
		<tr>
			<td colspan="3" align="center"><b>Total/Bulan</b></td>
			<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_retur_perb1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_retur_perb1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_lain1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_lain1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<!--<td><?php /*$data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_pengembalian_retur1']."<br>";
				}
			} */
		?></td>-->
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_retur_bhnbaku1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_retur_bhnbaku1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_hasil_perb1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_hasil_perb1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_lain1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_lain1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_gdgjadi1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_gdgjadi1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_packing']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_packing'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['saldo']."<br>";
				}
				
			}
		?></td>
		<td align="right"><?php 
			echo $query[$a]['gtot_saldo'];
		?></td>
		</tr>
	</tbody>
	</table>
			
			<br><br>
	<?php
			$nomor++;
		}
	}
	?>

</div>

<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
	var jumbrg = $("#jumbrg").val();
    //var Contents = $('#sitabel').html();    
    var i = 1; var Contents = '';
    for (i=1; i<=jumbrg; i++) {
		Contents = Contents + $('#sitabel_'+i).html() + '<br><br>';    
	}
    
    //window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent(Contents) +  '</table>' );
  });
</script>
<font color="red">* <strong ><font color="black">jika dibulan sebelumnya tidak ada Masuk Bagus Bahan Baku.</font></strong></font><br>
