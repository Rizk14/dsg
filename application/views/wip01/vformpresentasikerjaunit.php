<h3>Presentasi Kerja Unit</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}
	
}
</script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<?php 
$attributes = array('name' => 'f_unit', 'id' => 'f_unit');
echo form_open('wip/creport/addpresentasikerjaunit', $attributes); ?>
<table width="60%">
	<tr>
		<td width="20%">Unit Jahit</td>
		<td> <select name="unit_jahit" id="unit_jahit">
				<?php foreach ($list_unit as $jht) { ?>
					<option value="<?php echo $jht->kode_unit ?>" ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Periode (bulan-tahun)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>	
		
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/viewpresentasikerjaunit'">
<?php echo form_close();  ?>
