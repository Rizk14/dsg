<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Presentasi Kerja Unit</h3><br><br>

<div>
Unit Jahit: <?php if ($unit_jahit!= 0) { echo $unit_jahit."-".$nama_unit; } else echo "Semua"; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>

<?php 
$attributes = array('name' => 'f_presentasi', 'id' => 'f_presentasi');
echo form_open('wip/creport/export_excel_presentasikerjaunit', $attributes); ?>
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >
<input type="hidden" name="nama_bulan" value="<?php echo $nama_bulan ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<?php
	$nomor = 1;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			//echo "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b>"."<br>";
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
		<tr class="judulnya">
			<td colspan="10" align="center"><?php echo "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b>" ?></td>
		</tr>
	 <tr class="judulnya">
		 <th width='3%'>No</th>
		 <th width='5%'>Hari</th>
		 <th width='20%'>Nama Barang</th>
		 <th width='5%'>Kapasitas</th>
		 <th width='5%'>Minggu 1</th>
		 <th width='5%'>Minggu 2</th>
		 <th width='5%'>Minggu 3</th>
		 <th width='5%'>Minggu 4</th>
		 <th width='8%'>Total/Bulan</th>
		 <th width='8%'>Persentase</th>
	 </tr>
	</thead>
	<tbody>
			
<?php
			$detail_grup = $query[$a]['data_grup_jahit'];
			if (is_array($detail_grup)) {
				for($j=0;$j<count($detail_grup);$j++){
?>
			<tr>
				<td colspan="10" align="center"><?php echo $detail_grup[$j]['nama_grup_jahit'] ?></td>
			</tr>
			
<?php
			$detail_item = $detail_grup[$j]['data_item_brg'];
			if (is_array($detail_item)) {
				for($z=0;$z<count($detail_item);$z++){
?>
			<tr>
				<td align="center"><?php echo $nomor ?></td>
				<td align="right"><?php echo $detail_item[$z]['hari'] ?>&nbsp;</td>
				<td>&nbsp;<?php echo $detail_item[$z]['kode_brg_jadi']." - ".$detail_item[$z]['nama_brg_jadi'] ?></td>
				<td align="right"><?php echo $detail_item[$z]['kapasitas'] ?>&nbsp;</td>
				<td align="right"><?php echo $detail_item[$z]['minggu1'] ?>&nbsp;</td>
				<td align="right"><?php echo $detail_item[$z]['minggu2'] ?>&nbsp;</td>
				<td align="right"><?php echo $detail_item[$z]['minggu3'] ?>&nbsp;</td>
				<td align="right"><?php echo $detail_item[$z]['minggu4'] ?>&nbsp;</td>
				<td align="right"><?php echo $detail_item[$z]['total'] ?>&nbsp;</td>
				<td align="right"><?php echo $detail_item[$z]['persentase'] ?>&nbsp;</td>
			</tr>
			
<?php				$nomor++;
				} // end for2x
			} // end if2x
			
		} // end for2
	} // end if2
?>
		<tr>
			<td colspan="9" align="center"><b>Rata-Rata Presentasi Schedule / Bulan</b></td>
			<td align="right"><?php echo $query[$a]['total_rata'] ?>&nbsp;</td>
		</tr>
			</tbody>
			</table><br><br>
<?php
		} //end for1
	} // end if1
?>


</div>
