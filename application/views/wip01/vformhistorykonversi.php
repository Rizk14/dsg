<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo
{
	background-color:#DDD;
	max-width:350px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<h3>History Konversi Stok Barang WIP</h3><br><br>

<div>

Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('wip/cgudang/historykonversistok'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	
	<tr>
		<td style="white-space:nowrap;">Lokasi Gudang</td>
		<td style="white-space:nowrap;">: <select name="gudang" id="gudang">
				<option value="0">- All -</option>
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if ($gud->id == $cgudang) { ?> selected <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td style="white-space:nowrap;">Kode Brg/Nama Brg</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		<th>Brg Jadi Awal</th>
		 <th>Gudang Awal</th>
		 <th>Brg Jadi Tujuan</th>
		 <th>Gudang Tujuan</th>
		 <th>Qty</th>
		 <th>Qty Per Warna</th>
		 <th>Tgl Konversi</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				if ($startnya == '')
					$i=1;
				else
					$i = $startnya+1; 
			 for($j=0;$j<count($query);$j++){
				if ($query[$j]['tgl_input'] != '') {
					$pisah1 = explode("-", $query[$j]['tgl_input']);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					//$tgl_update_stok = $tgl1." ".$nama_bln." ".$thn1;
					
					$exptgl1 = explode(" ", $tgl1);
					$tgl1nya= $exptgl1[0];
					$jam1nya= $exptgl1[1];
					$tgl_input = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				}
				else
					$tgl_update_stok = "&nbsp;";
				 
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['kode_brg_jadi1']." - ".$query[$j]['nama_brg_jadi1']."</td>";
				 echo    "<td style='white-space:nowrap;'>[".$query[$j]['nama_lokasi1']."] ".$query[$j]['kode_gudang1']." - ".$query[$j]['nama_gudang1']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['kode_brg_jadi2']." - ".$query[$j]['nama_brg_jadi2']."</td>";
				 echo    "<td style='white-space:nowrap;'>[".$query[$j]['nama_lokasi2']."] ".$query[$j]['kode_gudang2']." - ".$query[$j]['nama_gudang2']."</td>";
				 
				 echo    "<td align='right'>".$query[$j]['qty']."</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailwarna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_warna']." : ".$var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td align='center'>".$tgl_input."</td>";
				 echo  "</tr>";
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
