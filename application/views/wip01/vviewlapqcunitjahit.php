<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan QC Barang Unit Jahit</h3><br><br>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>

<?php 
$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
echo form_open('wip/creport/export_excel_qcunitjahit', $attributes); ?>
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="nama_bulan" value="<?php echo $nama_bulan ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	<tr class="judulnya">
		<th colspan="9" align="center">BARANG BAGUS</td>
	</tr>
	
	<?php
		
		if (is_array($query)) {
			$jum1=0; $jum2=0; $cunit1=0; $temp_kel_unit = "";
			
			$i = 0;
			$jumlah=0; $cbrg=0; $jumlah2=0;
			$temp_kode_unit = "";
			
			for($a=0;$a<count($query);$a++){
				if ($query[$a]['kode'] != $temp_kel_unit) {
					$temp_kel_unit = $query[$a]['kode'];
	?>
					<tr class="judulnya">
						<td colspan="9"><b><?php echo $query[$a]['kode']." - ".$query[$a]['nama'] ?></b></td>
					</tr>
					<tr class="judulnya">
					 <th>No </th>
					 <th>Unit Jahit</th>
					 <th>Kode</th>
					 <th>Nama Barang Jadi</th>
					<th>Qty</th>
					<th>Retur</th>
					<th>Grade A</th>
					<th>% Grade A</th>
					<th>% Retur</th>
				 </tr>
				</thead>
	
	<?php
					$databagus = $query[$a]['data_stok'];
						
					if (is_array($databagus)) {
						for($j=0;$j<count($databagus);$j++){					
							 echo "<tr class=\"record\">";
							 
							 if ($databagus[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $databagus[$j]['kode_unit_jahit'];
								$i++;
								echo "<td align='center'>$i</td>";
								echo "<td>&nbsp;".$databagus[$j]['kode_unit_jahit']." - ".$databagus[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 echo "<td align='center'>&nbsp;</td>";
								 echo "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $databagus[$j]['persen_retur'];
							 $jumlah2+= $databagus[$j]['persena'];
							 $cbrg++;
				?>
							
							<td>&nbsp;<?php echo $databagus[$j]['kode_brg_jadi'] ?></td>
							<td>&nbsp;<?php echo $databagus[$j]['nama_brg_jadi'] ?></td>
							 <td align="right"><?php echo $databagus[$j]['qty'] ?>&nbsp;</td>
							 <td align="right"><?php echo $databagus[$j]['retur'] ?>&nbsp;</td>
							 <td align="right"><?php echo $databagus[$j]['gradea'] ?>&nbsp;</td>
							 <td align="right"><?php echo number_format($databagus[$j]['persena'], 2, '.',',') ?> %&nbsp;</td>
							 <td align="right"><?php echo number_format($databagus[$j]['persen_retur'], 2, '.',',') ?> %&nbsp;</td>
							</tr>
				<?php
						
							if (isset($databagus[$j+1]['kode_unit_jahit']) && ($databagus[$j]['kode_unit_jahit'] != $databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
							<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $databagus[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
			<?php
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
								<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $databagus[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
				<?php			
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query[$a+1]['kode']) && ($query[$a]['kode'] != $query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
				}
				else {
					$databagus = $query[$a]['data_stok'];
					
					//$i = 0;
					//$jumlah=0; $cbrg=0; $jumlah2=0;
					$temp_kode_unit = "";
						
					if (is_array($databagus)) {
						for($j=0;$j<count($databagus);$j++){					
							 echo "<tr class=\"record\">";
							 
							 if ($databagus[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $databagus[$j]['kode_unit_jahit'];
								$i++;
								echo "<td align='center'>$i</td>";
								echo "<td>&nbsp;".$databagus[$j]['kode_unit_jahit']." - ".$databagus[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 echo "<td align='center'>&nbsp;</td>";
								 echo "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $databagus[$j]['persen_retur'];
							 $jumlah2+= $databagus[$j]['persena'];
							 $cbrg++;
				?>
							
							<td>&nbsp;<?php echo $databagus[$j]['kode_brg_jadi'] ?></td>
							<td>&nbsp;<?php echo $databagus[$j]['nama_brg_jadi'] ?></td>
							 <td align="right"><?php echo $databagus[$j]['qty'] ?>&nbsp;</td>
							 <td align="right"><?php echo $databagus[$j]['retur'] ?>&nbsp;</td>
							 <td align="right"><?php echo $databagus[$j]['gradea'] ?>&nbsp;</td>
							 <td align="right"><?php echo number_format($databagus[$j]['persena'], 2, '.',',') ?> %&nbsp;</td>
							 <td align="right"><?php echo number_format($databagus[$j]['persen_retur'], 2, '.',',') ?> %&nbsp;</td>
							</tr>
				<?php
						
							if (isset($databagus[$j+1]['kode_unit_jahit']) && ($databagus[$j]['kode_unit_jahit'] != $databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
							<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $databagus[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
			<?php
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($databagus[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
								<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $databagus[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
				<?php			
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query[$a+1]['kode']) && ($query[$a]['kode'] != $query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
				} // end if header
	?>
				
	<?php			
			}
		} // end if barangbagus
	?>
	
 	</tbody>
</table><br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	<tr class="judulnya">
		<th colspan="9" align="center">BARANG PERBAIKAN</td>
	</tr>
	
	<?php
		
		if (is_array($query2)) {
			$jum1=0; $jum2=0; $cunit1=0; $temp_kel_unit = "";
			
			$i = 0;
			$jumlah=0; $cbrg=0; $jumlah2=0;
			$temp_kode_unit = "";
			
			for($a=0;$a<count($query2);$a++){
				if ($query2[$a]['kode'] != $temp_kel_unit) {
					$temp_kel_unit = $query2[$a]['kode'];
	?>
					<tr class="judulnya">
						<td colspan="9"><b><?php echo $query2[$a]['kode']." - ".$query2[$a]['nama'] ?></b></td>
					</tr>
					<tr class="judulnya">
					 <th>No </th>
					 <th>Unit Jahit</th>
					 <th>Kode</th>
					 <th>Nama Barang Jadi</th>
					<th>Qty</th>
					<th>Retur</th>
					<th>Grade A</th>
					<th>% Grade A</th>
					<th>% Retur</th>
				 </tr>
				</thead>
	
	<?php
					$dataperbaikan = $query2[$a]['data_stok2'];
						
					if (is_array($dataperbaikan)) {
						for($j=0;$j<count($dataperbaikan);$j++){					
							 echo "<tr class=\"record\">";
							 
							 if ($dataperbaikan[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $dataperbaikan[$j]['kode_unit_jahit'];
								$i++;
								echo "<td align='center'>$i</td>";
								echo "<td>&nbsp;".$dataperbaikan[$j]['kode_unit_jahit']." - ".$dataperbaikan[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 echo "<td align='center'>&nbsp;</td>";
								 echo "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $dataperbaikan[$j]['persen_retur'];
							 $jumlah2+= $dataperbaikan[$j]['persena'];
							 $cbrg++;
				?>
							
							<td>&nbsp;<?php echo $dataperbaikan[$j]['kode_brg_jadi'] ?></td>
							<td>&nbsp;<?php echo $dataperbaikan[$j]['nama_brg_jadi'] ?></td>
							 <td align="right"><?php echo $dataperbaikan[$j]['qty'] ?>&nbsp;</td>
							 <td align="right"><?php echo $dataperbaikan[$j]['retur'] ?>&nbsp;</td>
							 <td align="right"><?php echo $dataperbaikan[$j]['gradea'] ?>&nbsp;</td>
							 <td align="right"><?php echo number_format($dataperbaikan[$j]['persena'], 2, '.',',') ?> %&nbsp;</td>
							 <td align="right"><?php echo number_format($dataperbaikan[$j]['persen_retur'], 2, '.',',') ?> %&nbsp;</td>
							</tr>
				<?php
						
							if (isset($dataperbaikan[$j+1]['kode_unit_jahit']) && ($dataperbaikan[$j]['kode_unit_jahit'] != $dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
							<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $dataperbaikan[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
			<?php
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
								<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $dataperbaikan[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
				<?php			
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query2[$a+1]['kode']) && ($query2[$a]['kode'] != $query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
				}
				else {
					$dataperbaikan = $query2[$a]['data_stok2'];
					
					//$i = 0;
					//$jumlah=0; $cbrg=0; $jumlah2=0;
					$temp_kode_unit = "";
						
					if (is_array($dataperbaikan)) {
						for($j=0;$j<count($dataperbaikan);$j++){					
							 echo "<tr class=\"record\">";
							 
							 if ($dataperbaikan[$j]['kode_unit_jahit'] != $temp_kode_unit) {
								$temp_kode_unit = $dataperbaikan[$j]['kode_unit_jahit'];
								$i++;
								echo "<td align='center'>$i</td>";
								echo "<td>&nbsp;".$dataperbaikan[$j]['kode_unit_jahit']." - ".$dataperbaikan[$j]['nama_unit_jahit']."</td>";
							 }
							 else {
								 echo "<td align='center'>&nbsp;</td>";
								 echo "<td>&nbsp;</td>";
							 }
							 
							 $jumlah+= $dataperbaikan[$j]['persen_retur'];
							 $jumlah2+= $dataperbaikan[$j]['persena'];
							 $cbrg++;
				?>
							
							<td>&nbsp;<?php echo $dataperbaikan[$j]['kode_brg_jadi'] ?></td>
							<td>&nbsp;<?php echo $dataperbaikan[$j]['nama_brg_jadi'] ?></td>
							 <td align="right"><?php echo $dataperbaikan[$j]['qty'] ?>&nbsp;</td>
							 <td align="right"><?php echo $dataperbaikan[$j]['retur'] ?>&nbsp;</td>
							 <td align="right"><?php echo $dataperbaikan[$j]['gradea'] ?>&nbsp;</td>
							 <td align="right"><?php echo number_format($dataperbaikan[$j]['persena'], 2, '.',',') ?> %&nbsp;</td>
							 <td align="right"><?php echo number_format($dataperbaikan[$j]['persen_retur'], 2, '.',',') ?> %&nbsp;</td>
							</tr>
				<?php
						
							if (isset($dataperbaikan[$j+1]['kode_unit_jahit']) && ($dataperbaikan[$j]['kode_unit_jahit'] != $dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
							<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $dataperbaikan[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
			<?php
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
							else if ( !isset($dataperbaikan[$j+1]['kode_unit_jahit'])) {
								$rataperunit = $jumlah/$cbrg;
								$ratagradea = $jumlah2/$cbrg;
								
								$jum1+= $rataperunit;
								$jum2+= $ratagradea;
								$cunit1++;
				?>
								<tr>
								<td colspan="7" align="right"><b>Rata-Rata Unit <?php echo $dataperbaikan[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($ratagradea, 2, '.',',') ?> %&nbsp;</b></td>
								<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
							</tr>
				<?php			
								$jumlah=0; $jumlah2=0; $cbrg=0;
							}
						}
					}
					
					if (isset($query2[$a+1]['kode']) && ($query2[$a]['kode'] != $query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
					else if ( !isset($query2[$a+1]['kode'])) {
						$ratareturperkel = $jum1/$cunit1;
						$ratagradeaperkel = $jum2/$cunit1;
			?>
					<tr>
						<th colspan="7">TOTAL RATA-RATA</th>
						<th align="right"><b><?php echo number_format($ratagradeaperkel, 2, '.',',') ?> %&nbsp;</b></th>
						<th align="right"><b><?php echo number_format($ratareturperkel, 2, '.',',') ?> %&nbsp;</b></th>
					</tr>
			<?php
						$jum1=0; $jum2=0; $cunit1=0;
					}
				} // end if header
	?>
				
	<?php			
			}
		} // end if barangbagus
	?>
	
 	</tbody>
</table>

</div>
