<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Mutasi Stok Barang WIP (Hasil Jahit) Unit Packing</h3><br><br>

<div>
Unit Packing: <?php if ($unit_packing!= 0) { echo $kode_unit."-".$nama_unit; } else echo "Semua"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('wip/creport/export_excel_mutasiunitpacking', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="unit_packing" value="<?php echo $unit_packing ?>" >
<input type="hidden" name="kode_unit" value="<?php echo $kode_unit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<?php

	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			echo "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b>"."<br>";
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width='3%' rowspan='2'>No</th>
		 <th width='15%' rowspan='2'>Kode</th>
		 <th width='25%' rowspan='2'>Nama Brg WIP</th>
		 <th width='8%' rowspan='2'>HPP</th>
		 <th rowspan="2">Saldo<br>Awal</th>
		<th colspan="4">Masuk</th>
		<th colspan='4'>Keluar</th>
		<th rowspan="2">Saldo Akhir</th>
		<th rowspan="2">Stok Opname</th>
		
		<th width='3%' rowspan='2'>Adjustment</th>
		<th width='3%' rowspan='2'>Selisih</th>
	 </tr>
	 <tr class="judulnya">
		 <th width='8%'>Bgs</th>
		 <th width='8%'>Unit Jahit</th>
		 <th width='8%'>Lain2</th>
		 <th width='8%'>Total</th>
		 <th width='8%'>Bgs</th>
		 <th width='8%'>Retur</th>
		 <th width='8%'>Lain2</th>
		 <th width='8%'>Total</th>
	 </tr>
	</thead>
	<tbody>
			
<?php
			$detail_stok = $query[$a]['data_stok'];
			if (is_array($detail_stok)) {
				for($j=0;$j<count($detail_stok);$j++){
?>
			<tr>
				<td align="center"><?php echo ($j+1) ?></td>
				<td>&nbsp;<?php echo $detail_stok[$j]['kode_brg_wip'] ?></td>
				<td>&nbsp;<?php echo $detail_stok[$j]['nama_brg_wip'] ?></td>
				<td align="center"><?php echo $detail_stok[$j]['hpp'] ?></td>
				<td align="right"><?php echo number_format($detail_stok[$j]['jum_saldo_awal'],0,',','.') ?>&nbsp;</td>
				<td align="right"><?php echo number_format($detail_stok[$j]['masuk_bgs'],0,',','.') ?>&nbsp;</td>
				<td align="right"><?php echo number_format($detail_stok[$j]['masuk_dr_unitjahit'],0,',','.') ?>&nbsp;</td>
				<td align="right"><?php  if ($detail_stok[$j]['masuk_other'] )
				echo number_format($detail_stok[$j]['masuk_other'],0,',','.');
				else number_format($detail_stok[$j]['masuk_lain'],0,',','.');
				?></td>
				<td align="right"><?php echo number_format($detail_stok[$j]['jum_masuk'],0,',','.') ?>&nbsp;</td>
				<?php
				$keluar_gd_jadi=$detail_stok[$j]['keluar_bgs'] + $detail_stok[$j]['keluar_dr_unitpacking'] ;
				?>
				<td align="right"><?php echo number_format($keluar_gd_jadi,0,',','.')  ?>&nbsp;</td>
				<td align="right"><?php echo number_format($detail_stok[$j]['keluar_retur'],0,',','.') ?>&nbsp;</td>
				<td align="right"><?php  if ($detail_stok[$j]['keluar_other'] )
				echo number_format($detail_stok[$j]['keluar_other'],0,',','.');
				else number_format($detail_stok[$j]['keluar_lain'],0,',','.');
				?></td>
				<td align="right"><?php echo number_format($detail_stok[$j]['jum_keluar'],0,',','.') ?>&nbsp;</td>
				<td align="right"><?php echo number_format($detail_stok[$j]['jum_saldo_akhir'],0,',','.') ?>&nbsp;</td>
				<td align="right"><?php echo number_format($detail_stok[$j]['jum_so'],0,',','.') ?>&nbsp;</td>
				
				<td align="right"><?php echo number_format($detail_stok[$j]['jum_adjustment'],0,',','.') ?>&nbsp;</td>
				
				<td align="right"><?php echo number_format($detail_stok[$j]['selisih'],0,',','.') ?>&nbsp;</td>
			</tr>
<?php
				} // end for2
			} // end if2
?>			
		<!--	<tr>
				<td colspan="21" align="center">TOTAL</td>
				<td align="right"><?php echo number_format($query[$a]['total_so_rupiah'],0,',','.') ?>&nbsp;</td>
				<td colspan="4">&nbsp;</td>
				<td align="right"><?php echo number_format($query[$a]['total_sisa_stok_rupiah'],0,',','.') ?>&nbsp;</td>
			</tr> -->
			</tbody>
			</table><br><br>
<?php
		} //end for1
	} // end if1
?>
</div>
