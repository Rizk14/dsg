<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:400px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

</script>

<h3>Input Stok Awal WIP (Hasil Jahit)</h3><br>
<a href="<? echo base_url(); ?>index.php/wip/cform/addstokawal">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawal">View Data</a>&nbsp;<br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('wip/cform/viewstokawal'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td style="white-space:nowrap">Kode / Nama Barang Jadi</td>
		<td style="white-space:nowrap">: <input type="text" name="cari" size="20" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>
<?php 

?>
<form id="f_master_brg" name="f_master_brg">
<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		<th>Gudang</th>
		<th>Kode</th>
		 <th>Nama Barang Jadi</th>
		 <th>Stok per Warna</th>		 
		 <th>Last Update Stok</th>
	<!--	 <th>Action</th> -->
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
				if ($startnya == '')
					$no=1;
				else
					$no = $startnya+1; 
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update_stok']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td align='center'>".$no."</td>";
				 echo    "<td>".$query[$j]['nama_gudang']."</td>";
				 echo    "<td>".$query[$j]['kode_brg_jadi']."</td>";
				 echo    "<td>".$query[$j]['nama_brg_jadi']."</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailwarna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_warna']." : ".$var_detail[$k]['stok'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				// echo    "<td align='right'>".$query[$j]['stok']."</td>";				  
				 echo    "<td align='center'>".$tgl_update."</td>";
				 
				/*	 echo    "<td align=center>";
					 echo "<a href=".base_url()."index.php/wip/cform/editstokawal/".$query[$j]['id']."/".$cur_page."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a></td>"; */
				 
				 echo  "</tr>";
				 $no++;
		 	} // end for
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
