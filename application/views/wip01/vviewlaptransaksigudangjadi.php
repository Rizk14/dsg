<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Transaksi Barang WIP  Di Gudang Jadi</h3><br><br>

<div>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<input type="hidden" name="jumbrg" id="jumbrg" value="<?php echo $jumbrg ?>" >
<input name="cmdreset" id="cmdreset" value="Export Ke Excel" type="button">
<?php 
//$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
//echo form_open('wip/creport/export_excel_transaksiunitjahit', $attributes); ?>
<!--<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="kode_unit" value="<?php echo $kode_unit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >
<input type="hidden" name="list_id_brg_wip" value="<?php echo $list_id_brg_wip ?>" >
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">-->
<?php //echo form_close();  ?>
<br><br>

<?php
//print_r($query);
	$nomor = 1;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="100%" id="sitabel_<?php echo $nomor ?>">
	<thead>
	<tr class="judulnya">
		<th colspan="13"><b><?php echo $query[$a]['kode_brg_wip']." - ".$query[$a]['nama_brg_wip']; ?></b></th>
	</tr>
	 <tr class="judulnya">
		 <th>Tgl</th>
		 <th>Unit Packing</th>
		  <th>Unit Jahit</th>
		 <th>Ket</th>
		 <th>No Bukti</th>
		 <th>Masuk Bagus<br>QC</th>
		 <th>Masuk Bagus<br>Unit Jahit</th>
		 <th>Masuk Bagus<br>Unit Packing</th>
		  <th>Masuk Bagus<br>Unit Jahit + Packing</th>
		 <th>Keluar<br>Retur QC</th>
		 <th>Saldo<br>Per Warna</th>
		 <th>Saldo Global</th>
	 </tr>
	</thead>
	<tbody>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
			<?php $data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['saldo']."<br>";
				}
			}
		?>
			</td>
			<td align="right">
			<?php //$data_so_warna = $query[$a]['data_so_warna'];
			//if (is_array($data_so_warna)) {
			//	for($zz=0;$zz<count($data_so_warna);$zz++){ 
			//		echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['jum_stok_opname']."<br>";
			//	}
			//}
			echo $query[$a]['saldo_akhir'];
		?>
			</td>
		</tr>
			
<?php
		$data_tabel1 = $query[$a]['data_tabel1'];
		if (is_array($data_tabel1)) {
			for($j=0;$j<count($data_tabel1);$j++){
?>
	<tr>
		<td style="white-space:nowrap;"><?php echo "&nbsp;".$data_tabel1[$j]['tgl_sj'] ?></td>
		<td style="white-space:nowrap;"><?php echo "&nbsp;".$data_tabel1[$j]['kode_unit_packing']." - ".$data_tabel1[$j]['nama_unit_packing'] ?></td>
		<td style="white-space:nowrap;"><?php echo "&nbsp;".$data_tabel1[$j]['kode_unit_jahit']." - ".$data_tabel1[$j]['nama_unit_jahit'] ?></td>
		<td style="white-space:nowrap;"><?php if ($data_tabel1[$j]['masuk'] == "ya") { 
			if ($data_tabel1[$j]['masuk_qc'] == '1' || $data_tabel1[$j]['masuk_unit_jahit'] == '1' || $data_tabel1[$j]['masuk_unit_packing'] == '1'|| $data_tabel1[$j]['masuk_unit_packing_jahit'] == '1') {
				echo "Masuk"; 
			}
		}
		else {
			echo "Keluar"; 
		}	
		?></td>
		<td><?php echo $data_tabel1[$j]['no_sj']; ?></td>
		<td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_qc'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_unit_jahit'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_unit_packing'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		  <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_unit_packing_jahit'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_retur_qc'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					$totglobal = 0;
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					echo "Total: ".$totglobal;
				}
			}
		 ?>
		 </td>
		 		 
		 <td><?php
				$data_tabel1_saldo_perwarna = $data_tabel1[$j]['data_tabel1_saldo_perwarna'];
				if (is_array($data_tabel1_saldo_perwarna)) {
					for($z=0;$z<count($data_tabel1_saldo_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_saldo_perwarna[$z]['nama_warna'].": ".$data_tabel1_saldo_perwarna[$z]['saldo']."<br>";
					}
				}
				//echo $data_tabel1[$j]['tot_saldo'];
			
			?>
		 </td>
		 <td align="right"><?php
				echo $data_tabel1[$j]['tot_saldo'];
			
			?>
		 </td>
	</tr>
	<?php
			}
		}
	?>
		<tr>
			<td colspan="5" align="center"><b>Total/Bulan</b></td>
			<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_qc1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_qc1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_unit_jahit1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_unit_jahit1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_unit_packing1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_unit_packing1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
			<td><?php $data_warna = $query[$a]['data_warna'];
			
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk_unit_packing_jahit1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_masuk_unit_packing_jahit1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				$totglobal=0;
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar_retur_qc1']."<br>";
					$totglobal+=$data_warna[$zz]['tot_keluar_retur_qc1'];
				}
				echo "Total: ".$totglobal;
			}
		?></td>

		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['saldo']."<br>";
				}
			}
		?></td>
		<td align="right"><?php 
			echo $query[$a]['gtot_saldo'];
		?></td>
		</tr>
	</tbody>
	</table>
			
			<br><br>
	<?php
			$nomor++;
		}
	}
	?>

</div>

<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
	var jumbrg = $("#jumbrg").val();
    //var Contents = $('#sitabel').html();    
    var i = 1; var Contents = '';
    for (i=1; i<=jumbrg; i++) {
		Contents = Contents + $('#sitabel_'+i).html() + '<br><br>';    
	}
    
    //window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent(Contents) +  '</table>' );
  });
</script>
