<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Approval Stok Opname Barang WIP (Hasil Jahit) di Unit Jahit</h3><br><br>

<div>
Unit Jahit: <?php echo $unit_jahit." - ".$nama_unit ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br>
Total Data = <?php echo $jum_total ?><br><br>

Tanggal Pencatatan SO: <?php echo $tgl_so ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('gudangwip/cform/submitappsounitjahit', $attributes);
if(is_array($query))
	$no = count($query);
else $no = 0;
 ?>
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">
<input type="hidden" name="id_header" id="id_header" value="<?php echo $query[0]['id_header'] ?>">
<input type="hidden" name="tgl_so" id="tgl_so" value="<?php echo $tgl_so ?>">

<input type="submit" name="submit" value="Update Stok Opname" onclick="return confirm('Yakin akan approve data SO ini ? Data stok terkini akan otomatis terupdate')">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/gudangwip/cform/approvalsounitjahit'">
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Brg Jadi</th>
		 <th>Nama Brg Jadi</th>
		 <th>Satuan</th>
		 <th>Total Stok Akhir</th>
		 <th>Jml Fisik Bagus</th>
		 <th>Jml Fisik Perbaikan</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg_jadi']."
					 <input type='hidden' name='kode_brg_jadi_$i' id='kode_brg_jadi_$i' value='".$query[$j]['kode_brg_jadi']."'>
					 <input type='hidden' name='id_$i' id='id_$i' value='".$query[$j]['id']."'>
					 </td>";
					 echo    "<td>".$query[$j]['nama_brg_jadi']."</td>";
					 echo    "<td>Pieces</td>";
					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
							for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp; : ".$detailwarna[$zz]['saldo_akhir']."<br>";
						}
					}
					 echo "</td>";
					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
						for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp; : ".$detailwarna[$zz]['stok_opname_bgs']."<br>";
						?>
						<input type="hidden" name="kode_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['kode_warna'] ?>">
						<input type="hidden" name="stok_fisik_bgs_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['stok_opname_bgs'] ?>">
						<input type="hidden" name="stok_fisik_perbaikan_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['stok_opname_perbaikan'] ?>">
						<input type="hidden" name="jum_stok_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['saldo_akhir'] ?>">
						<?php
						}
					}
					 echo "</td>";
					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
						for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp; : ".$detailwarna[$zz]['stok_opname_perbaikan']."<br>";
						}
					}
					 echo "</td>";
					
					/* echo    "<td align='right'>".$query[$j]['stok']."</td>";
					 echo    "<td align='right'>".$query[$j]['stok_opname'].
					
					 " <input type='hidden' name='stok_fisik_$i' id='stok_fisik_$i' value='".$query[$j]['stok_opname']."'>
					 <input type='hidden' name='jum_stok_$i' id='jum_stok_$i' value='".$query[$j]['stok']."'>
					 </td>"; */
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>

<?php echo form_close();  ?>
</div>
