<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Stok Opname Barang WIP (Hasil Jahit) di Unit Jahit</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function cek_input() {
	var jum_data = $('#jum_data').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data SO ??");
	
	/*if (jum_data == 0) {
		alert("Data barang tidak ada..!");
		return false;
	} */
	
	if (kon) {
		if (jum_data > 0) {
			for (var k=1; k <= jum_data; k++) {
				if($('#ada_warna_'+k).val() == '') {				
					alert("Ada data barang yang belum ada warnanya, silahkan input dulu di menu Master Warna Brg Jadi WIP...!");
					s=1;
					return false;
				}
			/*	if($('#stok_fisik_'+k).val() == '') {				
					alert("Data stok tidak boleh kosong, jika kosong maka harus isikan angka 0...!");
					s=1;
					return false;
				}
				if (isNaN($('#stok_fisik_'+k).val()) ) {
					alert("Data stok harus berupa angka atau desimal..!");
					s=1;
					return false;
				} */
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}
</script>

<div>
Unit Jahit: <?php echo $unit_jahit." - ".$nama_unit; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_so', 'id' => 'f_so');
echo form_open('gudangwip/cform/submitsounitjahit', $attributes);
$no = count($query);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">

Tanggal Pencatatan SO: 
      <input name="tgl_so" type="text" id="tgl_so" size="10" value="<?php echo $tgl_so ?>" readonly="true">
	   <img alt="" id="tgl_so" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_so,'dd-mm-yyyy',this)">
<br><br>

<input type="hidden" name="jum_data" id="jum_data" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Brg Jadi</th>
		 <th>Nama Brg Jadi</th>
		 <th>Satuan</th>
		<!-- <th>Jumlah Stok</th> -->
		 <th>Jml Fisik Bagus</th>
		 <th>Jml Fisik Perbaikan</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif
									WHERE i_product_motif = '".$query[$j]['kode_brg_jadi']."' ");
					
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
					}
					else {
						$nama_brg_jadi	= '';
					}
					
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";					 
					 echo    "<td>".$query[$j]['kode_brg_jadi']."
					 <input type='hidden' name='kode_brg_jadi_$i' id='kode_brg_jadi_$i' value='".$query[$j]['kode_brg_jadi']."'></td>";
					 echo    "<td>".$nama_brg_jadi."</td>";
					 echo    "<td>Pieces</td>";
				//	 echo    "<td align='right'>".$query[$j]['stok']."</td>";
				/*	 echo    "<td align='center'><input type='text' name='stok_fisik_$i' id='stok_fisik_$i' value='".$query[$j]['stok_opname']."' size='10' style='text-align: right;'>
					 <input type='hidden' name='stok_$i' id='stok_$i' value='".$query[$j]['saldo_akhir']."'>
					 </td>"; */
				echo "<td style='white-space:nowrap;' align='right'>";
				if (is_array($query[$j]['detail_warna'])) {
					$detailwarna = $query[$j]['detail_warna'];
						for($zz=0;$zz<count($detailwarna);$zz++){ ?>
						<span style="white-space:nowrap;">
						<?php echo $detailwarna[$zz]['nama_warna']."&nbsp;";
				?>
				<input type="text" name="stok_fisik_bgs_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['stok_opname_bgs'] ?>" size="3" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
				<input type="hidden" name="kode_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['kode_warna'] ?>">
				<input type="hidden" name="stok_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['saldo_akhir'] ?>"><br>
				<?php 
						}
				?>
					<input type="hidden" name="ada_warna_<?php echo $i ?>" id="ada_warna_<?php echo $i ?>" value="ada">
				<?php
				}
				else {
				?>
					<input type="hidden" name="ada_warna_<?php echo $i ?>" id="ada_warna_<?php echo $i ?>" value="">
				<?php } ?>
				</td>
				
				<!-- 24-03-2014 -->
				<?php
					echo "<td style='white-space:nowrap;' align='right'>";
				if (is_array($query[$j]['detail_warna'])) {
					$detailwarna = $query[$j]['detail_warna'];
						for($zz=0;$zz<count($detailwarna);$zz++){ ?>
						<span style="white-space:nowrap;">
						<?php echo $detailwarna[$zz]['nama_warna']."&nbsp;";
				?>
				<input type="text" name="stok_fisik_perbaikan_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['stok_opname_perbaikan'] ?>" size="3" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
				<br>
				<?php 
						}

				}
				?>
				</td>
				
				<?php
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>
<input type="submit" name="submit" value="Simpan Stok Opname" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Stok Opname Periode Ini" onclick="return confirm('Yakin akan hapus SO periode ini ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/gudangwip/cform/sounitjahit'">
<?php echo form_close();  ?>
</div>
