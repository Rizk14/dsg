<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Masuk Barang WIP Gudang Jadi</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br>
Lokasi Gudang: <?php echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('trial-info-masuk-gd-jadi/cform/export_excel_wip', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_gudang" value="<?php echo $gudang ?>" >
<input type="hidden" name="kode_gudang" value="<?php echo $kode_gudang ?>" >
<input type="hidden" name="nama_gudang" value="<?php echo $nama_gudang ?>" >
<input type="hidden" name="nama_lokasi" value="<?php echo $nama_lokasi ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><? echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<? echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<? echo $this->pquery->form_remote_tag(array('url'=>'akt-kk-multi/cform/pv','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="area" name="area" value="<? echo $area; ?>"><input type="hidden" id="periode" name="periode" value="<? echo $periode; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Voucher</th>
    		    <th>Nilai</th>			
	    <tbody>
	      <?
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_pv','$area')\">$row->i_pv</a></td>
				  <td align=right><a href=\"javascript:setValue('$row->i_pv','$area')\">".number_format($row->v_pv)."</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <? echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    lebar =1366;
    tinggi=768;
 	  pv=a;
 	  area=b;
    eval('window.open("<?php echo site_url(); ?>"+"/akt-kk-multi/cform/cetak/"+pv+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  	jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
</div>
