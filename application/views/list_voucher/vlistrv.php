<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Voucher</h3>

<div>
Area: <?php echo $area ?> <br>
Jenis Data: <?php echo $nama_jenis ?> <br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br>
Total Data: <?php echo $total ?> <br>


<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('list_voucher/cform/export_excel_voucher', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="area" value="<?php echo $area ?>" >
<input type="hidden" id='jenis' name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" id='nama_jenis' name="nama_jenis" value="<?php echo $nama_jenis ?>" >
<!--
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
-->
<?php echo form_close();  ?>
<br>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><? echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<table class="maintable">
  <tr>
    <td align="left">
		<?php echo form_open('list_voucher/cform/voucher'); ?>
		<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="area" value="<?php echo $area ?>" >
<input type="hidden" id='jenis' name="jenis" value="<?php echo $jenis ?>" >
<input type="hidden" id='nama_jenis' name="nama_jenis" value="<?php echo $nama_jenis ?>" >
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="area" name="area" value="<? echo $area; ?>">
		<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Voucher</th>
    		    <th>Nilai</th>			
	    <tbody>
	      <?
		if($query){
			foreach($query as $row){ 
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_rv','$area')\">$row->i_rv</a></td>
				  <td align=right><a href=\"javascript:setValue('$row->i_rv','$area')\">".number_format($row->v_rv)."</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <? echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
<!--
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
--->
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
	jenis	= document.getElementById("jenis").value;
	if(jenis ==3){
    lebar =1366;
    tinggi=768;
 	  rv=a;
 	  area=b;
    eval('window.open("<?php echo site_url(); ?>"+"/list_voucher/cform/cetak3/"+rv+"/"+area+"/3","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  	jsDlgHide("#konten *", "#fade", "#light");
	}
	
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
</div>
