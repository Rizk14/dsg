<h3>Laporan Voucher</h3><br><br>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$('#jenis').change(function(){
	  	if ($("#jenis").val() == '4' || $("#jenis").val() == '5') {
			$('#bank').show();
		}
		else {
			$('#bank').hide();
		
		}
	  });
});
</script>
<script type="text/javascript">

function cek_input() {
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();

	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	var tgl_dari = date_from.substr(0,2);
	var bln_dari = date_from.substr(3,2);
	var thn_dari = date_from.substr(6,4);
	var dari = new Date(thn_dari, bln_dari, tgl_dari);
	
	var tgl_ke = date_to.substr(0,2);
	var bln_ke = date_to.substr(3,2);
	var thn_ke = date_to.substr(6,4);
	var ke = new Date(thn_ke, bln_ke, tgl_ke);
	
	if (dari > ke) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
//$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('list_voucher/cform/voucher' ); ?>
<table width="60%">
	<tr>
		<td>AREA</td>
		<td>
		<select id='area' name='area'>
		<?php foreach ($area as $as){
			?>
			<option value='<?php echo $as->id ?>'><?php echo "[" .$as->kode_area ."]  ".$as->nama ?> </option>
			<?php
			}
		?>
		</select>
		</td>
  </tr>
  <tr style="display:none;"  id='bank' name='bank'>
		<td>Bank</td>
		<td>
		<select id='i_coa_bank' name='i_coa_bank' >
		<?php foreach ($bank as $bk){
			?>
			<option value='<?php echo $bk->id_coa ?>'><?php echo "[" .$bk->kode ."]  ".$bk->nama ?> </option>
			<?php
			}
		?>
		</select>
		</td>
  </tr>
 <tr>	
		<td>Dari tanggal</td>
		<td>
		<label>
		  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'yyyy-mm-dd',this)">
		</td>
  </tr>
  <tr>
		<td>Sampai tanggal</td>
		<td>
		<label>
		  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'yyyy-mm-dd',this)">
		</td>
  </tr>
   <tr>
		<td>Jenis</td>
		<td>
		<select id='jenis' name='jenis'>
		
			<option value='1'>Kas Kecil</option>
			<option value='2'>Kas Besar Keluar</option>
			<option value='3'>Kas Besar Masuk</option>
			<option value='4'>Kas Bank Keluar</option>
			<option value='5'>Kas Bank Masuk</option>
		</select>
		</td>
  </tr>

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/wip'">
<?php echo form_close();  ?>
