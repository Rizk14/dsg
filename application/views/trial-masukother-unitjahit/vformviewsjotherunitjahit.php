<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:450px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function() {
	$('#filter_brg').click(function(){
	  	    if ($("#filter_brg").is(":checked")) {
				$('#cari_brg').attr('disabled', false);
				$('#cari_brg').val('');
			}
			else {
				$('#cari_brg').attr('disabled', true);
				$('#cari_brg').val('');
			}
	  });
});

</script>

<h3>Data SJ masuk Barang WIP ke Unit Jahit</h3><br>
<a href="<?php echo base_url(); ?>index.php/trial-masukother-unitjahit/ctest/addsjotherunitjahit">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/trial-masukother-unitjahit/ctest/viewsjotherunitjahit">View Data</a>&nbsp;<br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('trial-masukother-unitjahit/ctest/carisjotherunitjahit'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td style="white-space:nowrap;">Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;">Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>
	
	<tr>
		<td>Unit Jahit tujuan</td>
		<td>: <select name="id_unit_tujuan" id="id_unit_tujuan">
				<option value="0">- All -</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" <?php if ($jht->id == $id_unit_tujuan) { ?> selected <?php } ?> ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	
	<tr>
		<td style="white-space:nowrap;">No SJ</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	
	<tr>
		<td style="white-space:nowrap;" colspan="2"><input type="checkbox" name="filter_brg" id="filter_brg" value="y" <?php if ($filterbrg == 'y') { ?> checked="true" <?php } ?>>
		Filter berdasarkan kode/nama brg: <input type="text" name="cari_brg" id="cari_brg" size="10" value="<?php echo $caribrg ?>" <?php if ($filterbrg == 'n') { ?>disabled="true" <?php } ?>></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" >
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>
<?php 
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;
	
	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
?>
<form id="f_master_brg" name="f_master_brg">
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No SJ</th>
		<th>No BP</th>
		 <th>Tgl SJ</th>
		 <th>Unit Jahit tujuan</th>
		 <th>List Brg WIP</th>
		 <th>Qty</th>		 
		 <th>Qty Ket Warna</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";

				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;

				$pisah1 = explode("-", $query[$j]['tgl_sj']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_sj']."</td>";
				  echo    "<td>".$query[$j]['no_bp']."</td>";
				 echo    "<td>".$tgl_sj."</td>";
				 echo    "<td>".$query[$j]['kode_unit_tujuan']." - ".$query[$j]['nama_unit_tujuan']."</td>";				
							
				// 27-10-2015
				 if ($cari == '')
					$xcari = "all";
				 else
					$xcari = $cari;
					
				 if ($caribrg == '')
					$xcaribrg = "all";
				 else
					$xcaribrg = $caribrg;
									 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_sj'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_sj'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg_wip']." - ".$var_detail[$k]['nama_brg_wip'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_sj'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_sj'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_sj'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_sj'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['ket_qty_warna'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 				  
				 echo    "<td align='center'>".$tgl_update."</td>";
			


			if(($query[$j]['periode'])==null){
			echo    "<td align=center>";
				 echo "<a href=".base_url()."index.php/trial-masukother-unitjahit/ctest/editsjotherunitjahit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$tgl_awal."/".$tgl_akhir."/".$id_unit_tujuan."/".$xcari."/".$xcaribrg."/".$filterbrg." \" id=\"".$query[$j]['id']."\">Edit</a>&nbsp;
				 <a href=".base_url()."index.php/trial-masukother-unitjahit/ctest/deletesjotherunitjahit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$tgl_awal."/".$tgl_akhir."/".$id_unit_tujuan."/".$xcari."/".$xcaribrg."/".$filterbrg." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>&nbsp;";
			}
			else
			{
				 echo "<td><i>* Data sudah di closing periode </i></td>";
			 
			}



					 echo "</td>";
				 // <a href='javascript:void(0)' onclick='cetak();'>Cetak</a>
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
