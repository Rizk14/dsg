<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

/*function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();

	if (kode == '') {
		alert("Kode Barang harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Barang harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
	if (kode.search(" ") > 0) {
		alert("Kode barang tidak boleh ada spasi kosong..!");
		$('#kode').focus();
		return false;
	}
} */

function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();
	var id_jenis= $('#id_jenis').val();

	if (id_jenis == '') {
		alert("Jenis Barang harus dipilih..!");
		$('#kode_jenis').focus();
		return false;
	}
	
	if (nama == '') {
		alert("Nama Barang harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
}

$(function()
{	
	$('#pilih_jenis').click(function(){

	  	    var id_kel_brg= jQuery('#kel_brg_wip').val();
			var urlnya = "<?php echo base_url(); ?>index.php/brgwip/cform/show_popup_jenis/"+id_kel_brg;
			openCenteredWindow(urlnya);
	  });
});
	
</script>

<h3>Data Barang WIP</h3><br>
<a href="<? echo base_url(); ?>index.php/brgwip/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/brgwip/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
	echo form_open('brgwip/cform/submit', $attributes); ?>
<?php if ($edit == '1') { ?> 
	<input type="hidden" name="goedit" id="goedit" value="1"> 
	<input type="hidden" name="id_brg" value="<?php echo $eid ?>">
	<input type="hidden" name="kode_lama" value="<?php echo $ekode ?>">
	<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
	<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
	<input type="hidden" name="cjenis_brg_wip" value="<?php echo $cjenis_brg_wip ?>">
	<input type="hidden" name="cstatus" value="<?php echo $cstatus ?>">
	<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<?php } ?>

	<table>
		<tr>
			<td>Kelompok Brg WIP</td>
			<td> : <select name="kel_brg_wip" id="kel_brg_wip"> 
						<?php
						foreach($list_kelbrgwip as $kel) {
						?>
							<option value="<?php echo $kel->id ?>" <?php if ($eid_kel_brg_wip == $kel->id) { ?> selected <?php } ?>><?php echo $kel->kode." - ".$kel->nama ?></option>
						<?php
						}
						?>
					</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Barang</td>
			<td>: <input type="text" name="kode_jenis" id="kode_jenis" value="<?php echo $ekode_jenis." - ".$enama_jenis ?>" size="20" readonly="true">
			<input type="hidden" name="id_jenis" id="id_jenis" value="<?php echo $eid_jenis_brg_wip ?>">
			<input name="pilih_jenis" id="pilih_jenis" value="..." type="button" title="browse data jenis barang WIP">
			</td>
		</tr>		
		<tr>
			<td>Kode Barang</td>
			<td> : <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="8" size="10" >
			</td>
		</tr>
		<tr>
			<td>Nama Barang</td>
			<td> : <input type="text" name="nama" id="nama" value="<?php echo str_replace("\"", "&quot;", $enama_brg) ?>" maxlength="100" size="50" onkeyup="var nama=this.value; var ganti= nama.toUpperCase(); this.value=ganti;"></td>
		</tr>
		<?php
			if ($edit == '1') {
		?>
		<tr>
			<td>Status Aktif</td>
			<td> <input type="checkbox" name="status_aktif" id="status_aktif" value="t" <?php if ($estatus_aktif == 't') { ?> checked="true" <?php } ?> ></td>
		</tr>
		<?php
			}
		?>
		
		
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgjadi/cform/view'">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgwip/cform/view'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>
