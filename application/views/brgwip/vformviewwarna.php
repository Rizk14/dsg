<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Warna Barang WIP</h3><br>
<a href="<?php echo base_url(); ?>index.php/brgwip/cform/addwarnabrgwip">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('brgwip/cform/cariwarnabrgwip'); ?>

<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Kode</th>
		 <th>Nama Barang WIP</th>
		 <th>Warna</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
			
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td align='right'>$i &nbsp;</td>";
				 echo    "<td>".$query[$j]['kode_brg']."&nbsp;</td>";
				 echo    "<td>".$query[$j]['nama_brg']."&nbsp;</td>";
				 echo "<td>";
				  
				   if (is_array($query[$j]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailwarna'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_warna'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					 }
				  }
				  echo "</td>";
				  
				 $ada = 0;
				 
				 // tabel bon M masuk hasil cutting
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail WHERE id_brg_wip = '".$query[$j]['id_brg_wip']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj keluar ke jahitan
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE id_brg_wip = '".$query[$j]['id_brg_wip']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj masuk retur bhn baku dari jahitan
				 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail WHERE id_brg_wip = '".$query[$j]['id_brg_wip']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj masuk WIP
				 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail WHERE id_brg_wip = '".$query[$j]['id_brg_wip']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // tabel Sj keluar WIP
				 $query3	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail WHERE id_brg_wip = '".$query[$j]['id_brg_wip']."' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				  				
				 echo    "<td align=center><a href=".base_url()."index.php/brgwip/cform/editwarnabrgwip/".$query[$j]['id_brg_wip']." \" >Edit</a>";
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/brgwip/cform/deletewarnabrgwip/".$query[$j]['id_brg_wip']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 
				 echo "</td>";
				 echo  "</tr>";
				 $i++;
		 	} // end for
		 }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
