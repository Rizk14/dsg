<h3>Data Material Barang Jadi</h3><br>
<a href="<?php echo base_url(); ?>index.php/brgwip/cform/addmaterialbrgwip">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewmaterialbrgwip">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****is_quilting*************************************
		var is_quilting="#is_quilting_"+n;
		var new_is_quilting="#is_quilting_"+no;
		$(is_quilting, lastRow).attr("id", "is_quilting_"+no);
		$(new_is_quilting, lastRow).attr("name", "is_quilting_"+no);		
		$(new_is_quilting, lastRow).val('t');
		$(new_is_quilting, lastRow).attr("checked", false);
		//*****end is_quilting*********************************
								
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');
		//*****end satuan*************************************	
		
		//******div listwarna*************************************
		var listwarna="#listwarna_"+n;
		var new_listwarna="#listwarna_"+no;
		$(listwarna, lastRow).attr("id", "listwarna_"+no);
		
		//*****kode_warna*************************************
		var kode_warna="#kode_warna_"+n;
		var new_kode_warna="#kode_warna_"+no;
		$(kode_warna, lastRow).attr("id", "kode_warna_"+no);
		$(new_kode_warna, lastRow).attr("name", "kode_warna_"+no);	
		$(new_kode_warna, lastRow).val('');	
		//*****end kode_warna*************************************	
										
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
            
		 var  even_klik= "var q=''; if ($('input[name=is_quilting_"+no+"]').is(':checked')) { q= 't'; } else { q='f'; } openCenteredWindow('<?php echo base_url(); ?>index.php/brgwip/cform/show_popup_brg/'+q+'/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_input() {
	var brg_jadi= $('#brg_jadi').val();
	
	if (brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#kode_'+k).val() == '') {
				alert("Data item bhn baku tidak boleh ada yang kosong...!");
				return false;
			}
			if ($('#kode_warna_'+k).val() == '') {
				alert("Warna untuk brg jadi harus dipilih...!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}	
}

</script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/brgwip/cform/submitmaterialbrgwip" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
	<td width="10%">Barang Jadi</td>
	<td><input name="brg_jadi" type="text" id="brg_jadi" size="50" readonly="true" value=""/>
           <input name="kode_brg_jadi" type="hidden" id="kode_brg_jadi" value=""/>
           <input title="browse data brg jadi" name="pilih_brg_jadi" value="..." type="button" id="pilih_brg_jadi" 
           onclick="javascript: var jumdata = $('#no').val()-1; openCenteredWindow('<?php echo base_url(); ?>index.php/brgwip/cform/show_popup_brg_jadi/'+jumdata);" type="button"></td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="11" align="left">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Quilting</th>
          <th>Kode & Nama Bhn Baku/Pembantu</th>
          <th>Satuan</th>
          <th>Warna Utk Brg Jadi</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>		  
		  <td>
			<input type="checkbox" name="is_quilting_1" id="is_quilting_1" value="t" >
		  </td>
          <td nowrap="nowrap">
          <input name="nama_1" type="text" id="nama_1" size="40" readonly="true" value="" />
           <input name="kode_1" type="hidden" id="kode_1" value=""/>
           <input title="browse data barang" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var q='';
           if ($('input[name=is_quilting_1]').is(':checked')) { q= 't'; } else { q='f'; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/brgwip/cform/show_popup_brg/'+q+'/1');" >
           </td>
          <td><input name="satuan_1" type="text" id="satuan_1" size="8" readonly="true" value="" /></td>
          <td><div id="listwarna_1" ><select name="kode_warna_1" id="kode_warna_1">
					<option value=""> -Pilih- </option>
					<?php
					foreach($list_warna as $color) {
						echo "<option value=".$color->kode.">".$color->nama."</option>";
					}
					?>
				</select></div></td>
        </tr>
	</table>	
	
	</form>
      <div align="left"><br> 
        
        <input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgwip/cform/viewmaterialbrgwip'">

      </div></td>
    </tr>

</table>
</div>
</form>
