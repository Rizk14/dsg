<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">
function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();

	if (kode == '') {
		alert("Kode harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (kode.length < 3) {
		alert("Kode harus 3 digit..!");
		$('#kode').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Jenis harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
}
</script>

<h3>Data Jenis Barang WIP</h3><br>
<?php echo form_open('brgwip/cform/submitjenisbrgwip'); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> 
<input type="hidden" name="id_jenis" value="<?php echo $eid ?>">
<input type="hidden" name="kodeeditjenis" value="<?php echo $ekode ?>">
<?php } ?>
	<table>
		<tr>
			<td>Kelompok Barang WIP</td>
			<td>: <select name="kel_brg_wip" id="kel_brg_wip">
				<?php foreach ($kel_brg_wip as $kel) { ?>
					<option value="<?php echo $kel->id ?>" <?php if ($kel->id == $eid_kel_brg_wip) { ?> selected="true" <?php } ?> ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kode Jenis</td>
			<td>: <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="4" size="4"> <i>*) 4 digit. Contoh: DGT7</i>
			</td>
		</tr>
		<tr>
			<td>Nama Jenis</td>
			<td>: <input type="text" name="nama" id="nama" value="<?php echo $enama ?>" maxlength="30"></td>
		</tr>
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgwip/cform/jenisbrgwip'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br><br>
Total Data = <?php echo $jumdata; ?>

<div>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th>Kelompok Brg WIP</th>
		 <th>Kode</th>
		 <th>Nama Jenis</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 foreach ($query as $row){
				 echo "<tr class=\"record\">";
				 echo    "<td>$row->kode_kel - $row->nama_kel</td>";
				 echo    "<td>$row->kode</td>";
				 echo    "<td>$row->nama</td>";
				 
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo    "<td>$tgl_update</td>";
				 
				 // cek apakah data id sudah dipake di tm_barang
				 $query3	= $this->db->query(" SELECT id FROM tm_barang_wip WHERE id_jenis_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 else
					$ada = 0;
				 
				 echo    "<td align=center><a href=".base_url()."index.php/brgwip/cform/jenisbrgwip/$row->id \" id=\"$row->id\">Edit</a>";
				 
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/brgwip/cform/deletejenisbrgwip/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table>
</div>
