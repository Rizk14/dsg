<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var brg_wip= $('#brg_wip').val();
	var id_brg_wip= $('#id_brg_wip').val();

	if (id_brg_wip == '') {
		alert("Barang WIP harus dipilih..!");
		$('#brg_wip').focus();
		return false;
	}
}

function addElement() {
  var ni = document.getElementById('myDiv');
  var newdiv = document.createElement('div');
  
  var jumwarna= $('#jumwarna').val();
  var jumwarnanew = parseInt(jumwarna)+1;
  $('#jumwarna').val(jumwarnanew);
    
  newdiv.innerHTML = "<select name='id_warna_"+jumwarnanew+"' id='id_warna_"+jumwarnanew+"'>"+
					"<option value=''> -Pilih- </option>"+
					<?php
					foreach($list_warna as $color) {
					?>
					"<option value='<?php echo $color->id ?>'><?php echo $color->nama ?></option>"+
					<?php }?>
					"</select>";
  
  ni.appendChild(newdiv);
}

function hapusElement() {
  var jumwarna= $('#jumwarna').val();
  var jumwarna_lama= $('#jumwarna_lama').val();
  var ada= $('#ada').val();
	
	// 04-12-2015 DIMODIF, JADI WARNANYA BOLEH DIGANTI walaupun sudah pernah ada transaksi. 
	//karena di tiap2 transaksi langsung pake acuan id_warna, bukan id_warna_brg_jadi
	
	/*if (ada == 1) {
		var kurangi = parseInt(jumwarna)-1;
		if (kurangi >= jumwarna_lama) {
			$("#id_warna_"+jumwarna).remove(); 
			var jumwarnanew = parseInt(jumwarna)-1;
			$('#jumwarna').val(jumwarnanew);
		}
	} */
	//else {
		$("#id_warna_"+jumwarna).remove(); 
		var jumwarnanew = parseInt(jumwarna)-1;
		$('#jumwarna').val(jumwarnanew);
	//}
}

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
</script>

<h3>Data Warna Barang WIP</h3><br>
<a href="<? echo base_url(); ?>index.php/brgwip/cform/addwarnabrgwip">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_warna', 'id' => 'f_warna');
	echo form_open('brgwip/cform/updatewarnabrgwip', $attributes); ?>
	<table>
		<tr>
			<td>Barang Jadi</td>
			<td> <input type="text" name="brg_wip" id="brg_wip" value="<?php echo $query[0]['kode_brg']." - ".$query[0]['nama_brg'] ?>" size="40" readonly="true" >
			<input type="hidden" name="id_brg_wip" id="id_brg_wip" value="<?php echo $query[0]['id_brg_wip'] ?>">
			<input type="hidden" name="ada" id="ada" value="<?php echo $ada ?>">
			<input title="browse data barang" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/brgwip/cform/show_popup_brgwip/');" disabled >
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			  <td>Warna</td>
			  <td> <input type="hidden" name="jumwarna" id="jumwarna" value="<?php echo count($query[0]['detailwarna']) ?>"> 
					<input type="hidden" name="jumwarna_lama" id="jumwarna_lama" value="<?php echo count($query[0]['detailwarna']) ?>"> 
			  <div id="myDiv">
				<a href="javascript:addElement()">Tambah</a>&nbsp;<?php //if($ada == 0) { ?> <a href="javascript:hapusElement()">Hapus</a> <?php //} ?><br />
				
				<?php
					if (is_array($query[0]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[0]['detailwarna'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
						 $lcolor='';
						  ?>
						<select name="id_warna_<?php echo $k+1 ?>" id="id_warna_<?php echo $k+1 ?>" <?php //if($ada == 1) { ?> <!--disabled--> <?php //} ?> >
							<option value=""> -Pilih- </option>
							<?php
							foreach($list_warna as $color) {
								$lcolor	.= "<option value='".$color->id."'";
								if ($color->id == $var_detail[$k]['id_warna'])
									$lcolor	.= " selected";
								$lcolor.=">".$color->nama."</option>";
							}
							echo $lcolor;
							?>
						</select>
					<?php
						  if ($k<$hitung-1)
						     echo "<br> ";
					 }
				  }
				?>
				
				</div>
			  </td>
			</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip'">
			
		</tr>
	</table>
<?php echo form_close(); ?> <br>
