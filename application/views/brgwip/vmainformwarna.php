<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var brg_wip= $('#brg_wip').val();
	var id_brg_wip= $('#id_brg_wip').val();

	if (id_brg_wip == '') {
		alert("Barang WIP harus dipilih..!");
		$('#id_brg_wip').focus();
		return false;
	}
}

function addElement() {
  var ni = document.getElementById('myDiv');
  var newdiv = document.createElement('div');
  
  var jumwarna= $('#jumwarna').val();
  var jumwarnanew = parseInt(jumwarna)+1;
  $('#jumwarna').val(jumwarnanew);
    
  newdiv.innerHTML = "<select name='id_warna_"+jumwarnanew+"' id='id_warna_"+jumwarnanew+"'>"+
					"<option value=''> -Pilih- </option>"+
					<?php
					foreach($list_warna as $color) {
					?>
					"<option value='<?php echo $color->id ?>'><?php echo $color->nama ?></option>"+
					<?php }?>
					"</select>";
  
  ni.appendChild(newdiv);
}

function hapusElement() {
  var jumwarna= $('#jumwarna').val();
  $("#id_warna_"+jumwarna).remove(); 
  var jumwarnanew = parseInt(jumwarna)-1;
  $('#jumwarna').val(jumwarnanew);
}

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
</script>

<h3>Data Warna Barang WIP</h3><br>
<a href="<?php echo base_url(); ?>index.php/brgwip/cform/addwarnabrgwip">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_warna', 'id' => 'f_warna');
	echo form_open('brgwip/cform/submitwarnabrgwip', $attributes); ?>
	<table>
		<tr>
			<td>Barang WIP</td>
			<td> <input type="text" name="brg_wip" id="brg_wip" value="" size="40" readonly="true" >
			<input type="hidden" name="id_brg_wip" id="id_brg_wip" value="">
			<input title="browse data barang" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/brgwip/cform/show_popup_brgwip/');" >
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			  <td>Warna</td>
			  <td> <input type="hidden" name="jumwarna" id="jumwarna" value="1"> 
			  <div id="myDiv">
				<a href="javascript:addElement()">Tambah</a>&nbsp;<a href="javascript:hapusElement()">Hapus</a><br />
				<select name="id_warna_1" id="id_warna_1">
					<option value=""> -Pilih- </option>
					<?php
					foreach($list_warna as $color) {
						echo "<option value=".$color->id.">".$color->nama."</option>";
					}
					?>
				</select>
				</div>
			  </td>
			</tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip'">
			
		</tr>
	</table>
<?php echo form_close(); ?> <br>
