<h3>Data Material Barang Jadi</h3><br>
<a href="<? echo base_url(); ?>index.php/brgjadi/cform/addmaterialbrgjadi">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	//$("#no").val('2');
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****is_quilting*************************************
		var is_quilting="#is_quilting_"+n;
		var new_is_quilting="#is_quilting_"+no;
		$(is_quilting, lastRow).attr("id", "is_quilting_"+no);
		$(new_is_quilting, lastRow).attr("name", "is_quilting_"+no);		
		$(new_is_quilting, lastRow).val('t');
		$(new_is_quilting, lastRow).attr("checked", false);
		//*****end is_quilting*********************************
								
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');
		//*****end satuan*************************************	
		
		//*****kode_warna*************************************
		var kode_warna="#kode_warna_"+n;
		var new_kode_warna="#kode_warna_"+no;
		$(kode_warna, lastRow).attr("id", "kode_warna_"+no);
		$(new_kode_warna, lastRow).attr("name", "kode_warna_"+no);		
		$(new_kode_warna, lastRow).val('');
		//*****end kode_warna*************************************	
										
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
            
		 var  even_klik= "var q=''; if ($('input[name=is_quilting_"+no+"]').is(':checked')) { q= 't'; } else { q='f'; } openCenteredWindow('<?php echo base_url(); ?>index.php/brgjadi/cform/show_popup_brg/'+q+'/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		var jumdata_lama= $("#jumdata_lama").val();	
		var ada= $('#ada').val();
		if (ada == 1) {
			if (x>jumdata_lama) {
				$("#tabelku tr:last").remove();	 
				var x= $("#no").val();	
				var y= x-1;
				$("#no").val(y);	
			}
		}
		else {
			if (x>2) {
				$("#tabelku tr:last").remove();	 
				var x= $("#no").val();	
				var y= x-1;
				$("#no").val(y);	
			}
		}
	});	

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_input() {
	var brg_jadi= $('#brg_jadi').val();
	
	if (brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#kode_'+k).val() == '') {
				alert("Data item bhn baku tidak boleh ada yang kosong...!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}	
}

</script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/brgjadi/cform/updatedatamaterialbrgjadi" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="<?php echo count($query[0]['detailbrg'])+1 ?>"> 
<input type="hidden" name="jumdata_lama" id="jumdata_lama" value="<?php echo count($query[0]['detailbrg'])+1 ?>"> 
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
	<td width="10%">Barang Jadi</td>
	<td><input name="brg_jadi" type="text" id="brg_jadi" size="50" readonly="true" value="<?php echo $query[0]['kode_brg_jadi']." - ".$query[0]['e_product_motifname'] ?>"/>
           <input name="kode_brg_jadi" type="hidden" id="kode_brg_jadi" value="<?php echo $query[0]['kode_brg_jadi'] ?>"/>
           <input type="hidden" name="ada" id="ada" value="<?php echo $ada ?>">
           <input title="browse data brg jadi" name="pilih_brg_jadi" value="..." type="button" id="pilih_brg_jadi" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/brgjadi/cform/show_popup_brg_jadi/');" type="button" disabled></td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="11" align="right">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Quilting</th>
          <th>Kode & Nama Bhn Baku/Pembantu</th>
          <th>Satuan</th>
          <th>Warna Utk Brg Jadi</th>
        </tr>
		<?php
			if (is_array($query[0]['detailbrg'])) {
					 $var_detail = array();
					 $var_detail = $query[0]['detailbrg'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
		?>
		<tr align="center">
          <td align="center" id="num_<?php echo $k+1 ?>"><?php echo $k+1 ?></td>		  
		  <td>
			<input type="checkbox" name="is_quilting_<?php echo $k+1 ?>" id="is_quilting_<?php echo $k+1 ?>" value="t" <?php if ($var_detail[$k]['quilting'] == 't') { ?> checked="checked" <?php } ?> >
		  </td>
          <td nowrap="nowrap">
          <input name="nama_<?php echo $k+1 ?>" type="text" id="nama_<?php echo $k+1 ?>" size="40" readonly="true" value="<?php echo $var_detail[$k]['kode_brg']." - ".str_replace("\"", "&quot;", $var_detail[$k]['nama_brg']) ?>" />
           <input name="kode_<?php echo $k+1 ?>" type="hidden" id="kode_<?php echo $k+1 ?>" value="<?php echo $var_detail[$k]['kode_brg'] ?>"/>
           <input <?php if ($ada == 1) { ?> disabled <?php } ?> title="browse data barang" name="pilih_<?php echo $k+1 ?>" value="..." type="button" id="pilih_<?php echo $k+1 ?>" 
           onclick="javascript: var q='';
           if ($('input[name=is_quilting_1]').is(':checked')) { q= 't'; } else { q='f'; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/brgjadi/cform/show_popup_brg/'+q+'/<?php echo $k+1 ?>');" >
           </td>
          <td><input name="satuan_<?php echo $k+1 ?>" type="text" id="satuan_<?php echo $k+1 ?>" size="8" readonly="true" value="<?php echo $var_detail[$k]['satuan'] ?>" /></td>
          
          <td><select name="kode_warna_<?php echo $k+1 ?>" id="kode_warna_<?php echo $k+1 ?>">
					<option value=""> -Pilih- </option>
					<?php
					foreach($list_warna as $color) {
						if ($color->kode_warna == $var_detail[$k]['kode_warna'])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=".$color->kode_warna." ".$selected.">".$color->nama."</option>";
					}
					?>
				</select></td>
          
        </tr>
        <?php } 
		}
		?>
        
	</table>	
	
	</form>
      <div align="left"><br> 
        
        <input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi'">
      </div></td>
    </tr>

</table>
</div>
</form>
