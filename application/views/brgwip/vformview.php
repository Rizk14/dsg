<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Data Barang Jadi WIP</h3><br>
<a href="<?php echo base_url(); ?>index.php/brgwip/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/brgwip/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('brgwip/cform/cari'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Jenis Barang</td>
		<td><select name="jenis_brg_wip" id="jenis_brg_wip">
			<option value="0" <?php if ($ejenis_brg_wip == '') { ?> selected="true" <?php } ?> >- All -</option>
		<?php foreach ($jns_brg_wip as $jns) { ?>
			<option value="<?php echo $jns->id ?>" <?php if ($jns->id == $ejenis_brg_wip) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nama_kel."] ". $jns->kode."-".$jns->nama ?></option>
		<?php } ?>
		</select></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><select name="statusnya" id="statusnya">
		<option value="0" <?php if ($cstatus == '') { ?> selected="true" <?php } ?> >- All -</option>
		<option value="t" <?php if ($cstatus == 't') { ?> selected="true" <?php } ?> >Aktif</option>
		<option value="f" <?php if ($cstatus == 'f') { ?> selected="true" <?php } ?> >Non-Aktif</option>
		</select></td>
	</tr>
	<tr>
		<td>Keyword Pencarian</td>
		<td><input type="text" name="cari" size="30" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th>No</th>
		 <th>Jenis Barang WIP</th>
		 <th>Kode</th>
		 <th>Nama Barang</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
			 foreach ($query as $row){				 
				 echo "<tr class=\"record\">";
				 echo    "<td align='right'>$i &nbsp;</td>";
				 
				 $query3	= $this->db->query(" SELECT nama FROM tm_kel_brg_wip WHERE id = '$row->id_kel_brg_wip' ");
				 if ($query3->num_rows() > 0){
					$hasil3 = $query3->row();
					$nama_kel = $hasil3->nama;
				 }
				 else {
					$nama_kel = "";
				 }
				 
				 $query3	= $this->db->query(" SELECT kode, nama FROM tm_jenis_brg_wip WHERE id = '$row->id_jenis_brg_wip' ");
				 if ($query3->num_rows() > 0){
					$hasil3 = $query3->row();
					$kode_jenis = $hasil3->kode;
					$nama_jenis = $hasil3->nama;
				 }
				 else {
					 $kode_jenis = "";
					 $nama_jenis = "";
				 }
				 echo    "<td>[$nama_kel] $kode_jenis - $nama_jenis</td>";
				 
				 echo    "<td>&nbsp; $row->kode_brg</td>";
				 echo    "<td>&nbsp; $row->nama_brg</td>";
				 				 
				 $ada = 0;
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_warna_brg_wip WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // SEMENTARA DIKOMEN 11-09-2015, PERLU DIPERBAIKI FIELD2 DI TABEL2 TSB
				 
				 /*$query3	= $this->db->query(" SELECT id FROM tm_material_brg_jadi WHERE kode_brg_jadi = '$row->i_product_motif' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 } */
				 /*$query3	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 $query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 $query3	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail WHERE id_brg_wip = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 } */
				
				 echo    "<td align=center><a href=".base_url()."index.php/brgwip/cform/index/$row->id/".$cur_page."/".$is_cari."/".$ejenis_brg_wip."/".$cstatus."/".$cari." \" id=\"$row->id\">Edit</a>";
				 if ($ada == 0)
					echo    "&nbsp; <a href=".base_url()."index.php/brgwip/cform/delete/$row->id/".$cur_page."/".$is_cari."/".$ejenis_brg_wip."/".$cstatus."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 
				 echo "</td>";
				 echo  "</tr>";
				 $i++;
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
