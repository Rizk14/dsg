<style type="text/css">
	table {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 12px;
	}

	.fieldsetdemo {
		background-color: #DDD;
		max-width: 500px;
		padding: 14px;
	}

	.judulnya {
		background-color: #DDD;
	}
</style>

<h3>Laporan Opname Hutang Dagang</h3><br><br>

<div>
	Total Data = <?php echo $jum_total ?><br>
	Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br>
	<!--Jenis Laporan: <?php echo $namalap ?><br>-->
	Jenis Pengambilan Data: <?php echo $namajenisdata ?><br><br>
	<?php
	$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
	echo form_open('info-opname-hutang/cform/export_excel', $attributes); ?>
	<input type="hidden" name="date_from" value="<?php echo $date_from ?>">
	<input type="hidden" name="date_to" value="<?php echo $date_to ?>">
	<input type="hidden" name="jenisdata" value="<?php echo $jenisdata ?>">
	<input type="hidden" name="namajenisdata" value="<?php echo $namajenisdata ?>">
	<input type="hidden" name="jenislap" value="<?php echo $jenislap ?>">
	<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
	<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
	<?php echo form_close();  ?>
	<br>
	<table border="1" cellpadding="1" cellspacing="1" width="80%">
		<thead>
			<tr class="judulnya">
				<th>No</th>
				<th>Tgl Faktur</th>
				<th>Nomor Faktur</th>
				<th>Supplier</th>
				<th>Jenis</th>
				<th>Jumlah (Rp.)</th>
				<!-- <th>Last Update</th> -->
			</tr>
		</thead>
		<tbody>
			<?php
			if (is_array($query)) {
				$total_persupplier = 0;
				$grand_total = 0;
				$urut_per_supplier = 0;
				$tot_retur = 0;
				for ($j = 0; $j < count($query); $j++) {

					/*
				 * 1. query semua data retur berdasarkan tanggal awal dari data yg dihasilkan di query opname. 
				 * ( faktur_date_from >= tgl_awal). Tempatkan query di awal perulangan pada form view.
				   2. Hitung sum retur detailnya tiap2 data hasil query di tm_retur_beli. Hasil sum tiap2 data retur_beli dijumlahkan.
				   3. Dari hasil sum itu, bandingkan dgn tiap2 nominal faktur. Jika selisihnya dgn faktur masih ada sisa 100.000 di fakturnya, maka nilai sum retur berkurang. 
				   * Begitu seterusnya sampe nilai retur = 0
				 * 
				 * 
				 * */
					// echo $urut_per_supplier."<br>";
					if (isset($query[$j + 1]['id_supplier']) && ($query[$j]['id_supplier'] == $query[$j + 1]['id_supplier'])) {
						// 11-08-2015 DIKOMEN, KARENA RETUR SKRG DITAMPILKAN JUGA
						/*if ($urut_per_supplier == 0) {
						$pisah1 = explode("-", $query[$j]['tgl_faktur']);
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$query2	= $this->db->query(" SELECT id FROM tm_retur_beli WHERE id_supplier = '".$query[$j]['id_supplier']."'
										AND faktur_date_from >= '$thn1-$bln1-01' ");
						
						if ($query2->num_rows() > 0){
							$hasil2 = $query2->result();
							foreach ($hasil2 as $row2) {
								$query3	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
												INNER JOIN tm_retur_beli_detail c ON a.id = c.id_retur_beli
												WHERE c.id_retur_beli = '$row2->id' ");
									
								$hasilrow = $query3->row();
								$tot_retur+= $hasilrow->tot_retur;
							}
						
							$selisih = $query[$j]['total']-$tot_retur; 
							
							if ($selisih <=0) {
								$tot_retur = $tot_retur - $query[$j]['total']+100000;
								$query[$j]['total'] = 100000; // kasih batas minimal 100.000
							}
							else {
								// 11:03
								//if ($tot_retur < $query[$j]['total']) {
								$tot_retur = 0;
								$query[$j]['total'] = $selisih;
								//}
								
								//$tot_retur = $tot_retur - $query[$j]['total'] + 100000; 
								//$query[$j]['total'] = $query[$j]['total'] + 100000;
							}
							//misal tot_retur = 10.000.000
							// jml faktur = 2000000
							// selisih = 2 jt - 10 jt = -8 jt. jika kurang atau = 0, maka 2jt - 2jt + 100000
						} // end if ada data returnya
						
						//$urut_per_supplier++;
					} // end if $urut_per_supplier = 0
					else {
					  if ($tot_retur != 0) {
						$selisih = $query[$j]['total']-$tot_retur;
						
						if ($selisih <=0) {
							$tot_retur = $tot_retur - $query[$j]['total']+100000;
							$query[$j]['total'] = 100000; // kasih batas minimal 100.000
						}
						else {
							$tot_retur = 0;
							$query[$j]['total'] = $selisih;
							
							//$tot_retur = $tot_retur - $query[$j]['total'] + 100000;
							//$query[$j]['total'] = $query[$j]['total'] + 100000;
						}
					  }
						//$urut_per_supplier++;
					} */
					} // end if
					$urut_per_supplier++;
					$pisah1 = explode("-", $query[$j]['tgl_faktur']);
					$tgl1 = $pisah1[2];
					$bln1 = $pisah1[1];
					$thn1 = $pisah1[0];
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					$tgl_faktur = $tgl1 . " " . $nama_bln . " " . $thn1;

					/*$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya; */

					echo "<tr class=\"record\">";
					echo "<td>$urut_per_supplier</td>";
					echo    "<td>" . $tgl_faktur . "</td>";
					echo    "<td>" . $query[$j]['no_faktur'] . "</td>";
					echo    "<td>" . $query[$j]['kode_supplier'] . " - " . $query[$j]['nama_supplier'] . "</td>";

					if ($query[$j]['ismakloon'] == '1')
						echo    "<td>Makloon</td>";
					else if ($query[$j]['ismakloon'] == '0') {
						if ($query[$j]['isretur'] == '0')
							echo    "<td>Pembelian</td>";
						else
							echo    "<td>Retur</td>";
					}

					if ($query[$j]['isretur'] == '0')
						echo    "<td align='right'>" . number_format($query[$j]['total'], 4, ',', '.') . "</td>";
					else
						echo    "<td align='right'>-" . number_format($query[$j]['total'], 4, ',', '.') . "</td>";

					//	 echo    "<td align='center'>".$tgl_update."</td>";
					echo  "</tr>";
					if ($query[$j]['isretur'] == '0')
						$total_persupplier = $total_persupplier + $query[$j]['total'];
					else
						$total_persupplier = $total_persupplier - $query[$j]['total'];

					if (isset($query[$j + 1]['id_supplier']) && ($query[$j]['id_supplier'] != $query[$j + 1]['id_supplier'])) {
						echo "<tr class=\"record\">";
						echo    "<td colspan='5' align='right'>" . $query[$j]['nama_supplier'] . " TOTAL </td>";
						echo    "<td align='right'>" . number_format($total_persupplier, 4, ',', '.') . "</td>";
						//echo    "<td>&nbsp;</td>";
						echo  "</tr>";
						$grand_total = $grand_total + $total_persupplier;
						$total_persupplier = 0;
						$urut_per_supplier = 0;
						$tot_retur = 0;
					}
					/*	else {
					echo "<tr class=\"record\">";
					 echo    "<td colspan='3' align='right'>".$query[$j]['nama_supplier']." TOTAL </td>";
					 echo    "<td align='right'>".number_format($total_persupplier,2,',','.')."</td>";
					 echo    "<td>&nbsp;</td>";
					 echo  "</tr>";
				} */
					if (!isset($query[$j + 1]['id_supplier'])) {
						echo "<tr class=\"record\">";
						echo    "<td colspan='5' align='right'>" . $query[$j]['nama_supplier'] . " TOTAL </td>";
						echo    "<td align='right'>" . number_format($total_persupplier, 4, ',', '.') . "</td>";
						//echo    "<td>&nbsp;</td>";
						echo  "</tr>";
						$grand_total = $grand_total + $total_persupplier;
						$total_persupplier = 0;
						$urut_per_supplier = 0;
						$tot_retur = 0;
					}
				} // end for
				echo "<tr>
				<td colspan='5' align='center'><b>GRAND TOTAL</b></td>
				<td align='center'><b>" . number_format($grand_total, 4, ',', '.') . "</b></td>
			</tr>";
			}
			?>
		</tbody>
	</table><br>
	<?php //echo $this->pagination->create_links();
	?>
</div>