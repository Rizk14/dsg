<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>

<body onLoad="closeMe();">

<?php
echo "Printer: ".$printer_name;
?>

<?php
include_once ("funcs/terbilang.php");

$get_attributes	= false;
$waktu	= date("h:i:s");
$line	= 80;
$line_first	= 2;
$line_last	= 2;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php"); 
} else {
	include_once("printipp_classes/BasicIPP.php");
}

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

//$ipp->setFidelity();
//$ipp->unsetFidelity();

$List = "";

$List	.= CHR(1).str_repeat(CHR(205),($line-2)).CHR(0)."\n";

$List	.= "\n";

//Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
//Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
//Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
//Italic	= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)
//San Serif	= CHR(27).CHR(107).CHR(49)
//Start Text	= CHR(2)
//End Text	= CHR(3)
//Set Bottom Margin	= CHR(27).CHR(78).CHR(2)
//Cancel Set Bottom Margin	= CHR(27).CHR(79)

$range	= $line-($line_first+strlen('faktur penjualan')+strlen($nminitial)+$line_last+1);
$List	.= str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(107).CHR(48).CHR(27).CHR(119).CHR(1). CHR(27).CHR(69)."Faktur Penjualan".CHR(27).CHR(70).CHR(27).CHR(119).CHR(0).CHR(27).CHR(107).CHR(48).CHR(0).str_repeat(" ",($range)).CHR(27).CHR(69).$nminitial.CHR(27).CHR(70).str_repeat(" ",$line_last)."\n";

$List	.= str_repeat(CHR(205),($line-2))."\n";

$range0	= $line-($line_first+strlen('nomor faktur')+2+strlen($nomorfaktur)+strlen('tanggal faktur')+2+strlen($tglfaktur)+$line_last);
$List	.= str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(69)."Nomor Faktur".str_repeat(" ",2).$nomorfaktur.CHR(27).CHR(70).CHR(0).str_repeat(" ",$range0).CHR(1).CHR(27).CHR(69)."Tanggal Faktur".CHR(27).CHR(70).CHR(0).str_repeat(" ",2).$tglfaktur.str_repeat(" ",$line_last)."\n";

$List	.= "\n";

$bts	= 5;
$kalimat1	= strlen($nmcabang);
$kalimat2	= strlen($alamatcabang);
$bhn_grs1	= ($line-(2+$kalimat1+2))+$bts;
$bhn_grs2	= ($line-(2+$kalimat2+2))+$bts;
if($kalimat1 > $kalimat2) {
	$selisih	= $bhn_grs2-$bhn_grs1;
	$grske1		= $bts;
	$grske2		= $bts+$selisih;
	$btskpd		= ($kalimat1-(strlen('kepada :')))+$grske1;
	$s1			=  ($line-($kalimat1+2))+9;
} else {
	$selisih	= $bhn_grs1-$bhn_grs2;
	$grske1		= $bts+$selisih;
	$grske2		= $bts;
	$btskpd		= ($kalimat2-(strlen('kepada :')))+$grske2;
	$s1			=  ($line-($kalimat2+2))+9;
}

$patok	= $s1;
$enotenya	= $enote;
$pjg_enote	= strlen($enotenya);

$temp	= array();
$a=0;
$b=0;
$jml=0;
$kata1	= "";
$kata2	= "";
$kata3	= "";

while($a<$pjg_enote){
	$temp[$a]	= substr($enotenya,$a,1);
	$a+=1;
}

foreach($temp as $kunci => $karakter){
	if(($jml%$patok)==0)
		$b=$b+1;
	
	if($b==1)
	$kata1.=$karakter;
	
	if($b==2)
	$kata2.=$karakter;
	
	if($b==3)
	$kata3.=$karakter;
	
	$jml+=1;
}

if(!empty($kata1)){
	$isikata1=$kata1;
}else{
	$isikata1=str_repeat(" ",$s1);
}

if(!empty($kata2)){
	$isikata2=$kata2;
}else{
	$isikata2=str_repeat(" ",$s1);
}

if(!empty($kata3)){
	$isikata3=$kata3;
}else{
	$isikata3=str_repeat(" ",$s1);
}


$List	.= str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(69)."Kepada :".CHR(27).CHR(70).CHR(0).str_repeat(" ",$btskpd).CHR(1).CHR(15).CHR(218).str_repeat(CHR(196),$s1+1).CHR(191).CHR(18).CHR(0)."\n";

$List	.= str_repeat(" ",$line_first).$nmcabang.str_repeat(" ",$grske1).CHR(15).CHR(179)."keterangan:".str_repeat(" ",($s1-strlen('keterangan:'))+1).CHR(179).CHR(18)."\n";

$List	.= str_repeat(" ",$line_first).$alamatcabang.str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).$isikata1.str_repeat(" ",($s1-strlen($isikata1))+1).CHR(179).CHR(18).CHR(0)."\n";

$List	.= str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).$isikata2." ".CHR(179).CHR(18).CHR(0)."\n";

if(!empty($kata3)){
	$List	.= str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).$isikata3." ".CHR(179).CHR(18).CHR(0)."\n";
}

$List	.= str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(192).str_repeat(CHR(196),$s1+1).CHR(217).CHR(18).CHR(0)."\n";

$List	.= "\n";

$List	.= str_repeat(CHR(196),($line-2))."\n";

$range4	= $line-($line_first+strlen('no')+1+strlen('kode barang')+1+strlen('nama barang')+strlen('unit')+1+strlen('harga')+1+strlen('jumlah')+1);
$List	.= CHR(1).CHR(27).CHR(69).str_repeat(" ",$line_first)."No".str_repeat(" ",2)."Kode Barang".str_repeat(" ",1)."Nama Barang".str_repeat(" ",($range4-11))."Unit".str_repeat(" ",4)."Harga".str_repeat(" ",5)."Jumlah".str_repeat(" ",2).CHR(27).CHR(70).CHR(0)."\n";

$List	.= str_repeat(CHR(196),($line-2))."\n";

$no	= 1;
$spcno	= 6; // sesuaikan dgn header No + 2
$spckb	= 20;
$spcnb	= 58;
$spcunit= 9;
$spcprice	= 12;
$spcjml	= 15;

$arrno	= array();
$arrkb	= array();
$arrnb	= array();
$arrunit= array();
$arrprice= array();
$arrjml	= array();

$spunit	= array();
$spprice	= array();
$spamount	= array();

$lup	= 0;
$jmlqty		= array();
$uprice		= array();
$tprice		= array();

foreach($isi as $row) {
	if($lup<22) {		
		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$jmlqty[$lup]	= $row_value->qty;
			$uprice[$lup]	= $row_value->unitprice;
			$tprice[$lup]	= $row_value->amount;
		} else {
			$jmlqty[$lup]	= 0;
			$uprice[$lup]	= 0;
			$tprice[$lup]	= 0;		
		}
		$arrno[$lup]	= (strlen($no)<=$spcno)?$spcno-strlen($no):0;
		$arrkb[$lup]	= (strlen(trim($row->imotif))<=$spckb)?$spckb-strlen($row->imotif):0;
		$arrnb[$lup]	= (strlen($row->motifname)<=$spcnb)?$spcnb-strlen($row->motifname):0;
		$arrunit[$lup]	= (strlen($jmlqty[$lup])<=$spcunit)?$spcunit-strlen($jmlqty[$lup]):strlen($jmlqty[$lup]);
		$arrprice[$lup]	= (strlen($uprice[$lup])<=$spcprice)?$spcprice-strlen($uprice[$lup]):strlen($uprice[$lup]);
		$arrjml[$lup]	= (strlen($tprice[$lup])<=$spcjml)?$spcjml-strlen($tprice[$lup]):strlen($tprice[$lup]);
	
		$spunit[$lup]	= strlen(number_format($jmlqty[$lup]))<=$spcunit?$spcunit-strlen(number_format($jmlqty[$lup])):0;
		$spprice[$lup]	= strlen(number_format($uprice[$lup]))<=$spcprice?$spcprice-strlen(number_format($uprice[$lup])):0;
		$spamount[$lup]	= strlen(number_format($tprice[$lup]))<=$spcjml?$spcjml-strlen(number_format($tprice[$lup])):0;

		$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",3).$no.str_repeat(" ",$arrno[$lup]).trim($row->imotif).str_repeat(" ",$arrkb[$lup]).$row->motifname.str_repeat(" ",$arrnb[$lup]).str_repeat(" ",$spunit[$lup]).number_format($jmlqty[$lup]).str_repeat(" ",3).str_repeat(" ",$spprice[$lup]).number_format($uprice[$lup]).str_repeat(" ",3).str_repeat(" ",$spamount[$lup]).number_format($tprice[$lup]).CHR(18)."\n";
		
	}
	
	$no+=1;
	$lup+=1;
	
}

$List	.= CHR(1).str_repeat(CHR(196),($line-2)).CHR(0)."\n";

$teks1	= 66; //"1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,";
$teks2	= 39; //"kecuali ada perjanjian terlebih dahulu.";
$teks3	= 48; //"2. Faktur asli merupakan bukti pembayaran yg sah";
$teks4	= 63; //"3. Pembayaran dgn cek/ giro baru dianggap sah setelah diuangkan";

$bts_rp_nilai_uang	= 20;
$space_awal_rp	= 1;
$bts_kotak_dg_jml	= 3;

$xx1	= $bts_rp_nilai_uang-strlen(number_format($jumlah));
$xx2	= $bts_rp_nilai_uang-strlen(number_format($diskon));
$xx3	= $bts_rp_nilai_uang-strlen(number_format($dpp));
$xx4	= $bts_rp_nilai_uang-strlen(number_format($nilai_ppn));
$xx5	= $bts_rp_nilai_uang-strlen(number_format($nilai_faktur));

$left1	= $line-($line_first+1+strlen('tgl. jatuh tempo :')+strlen($tgljthtempo)+1+strlen('jumlah')+1+strlen('rp.')+$xx1+strlen(number_format($jumlah)));
$left2	= ( $line-(1+strlen('diskon')+1+strlen('rp.')+$xx2+strlen(number_format($diskon))) )-($bts_kotak_dg_jml+24);
$left3	= ( $line-($line_first+strlen('dpp')+1+strlen('rp.')+$xx3+strlen(number_format($dpp))) )-($bts_kotak_dg_jml+24);
$left4	= ( $line-($line_first+strlen('nilai faktur')+1+strlen('rp.')+$xx5+strlen(number_format($nilai_faktur))) )-($bts_kotak_dg_jml+24);
$left5	= ( $line-($line_first+strlen('ppn')+1+strlen('rp.')+$xx4+strlen(number_format($nilai_ppn))) )-($bts_kotak_dg_jml+24);

$left6	= ( $line-($line_first+strlen('jumlah')+1+strlen('rp.')+$xx1+strlen(number_format($jumlah))) )-($bts_kotak_dg_jml+24);

$right4	= $left4;
$left44	= ($line-($right4+1+$bts_kotak_dg_jml))-24;

$repeat1	= 1;
$repeat2	= strlen($teks1)-strlen($teks2);
$repeat3	= strlen($teks1)-strlen($teks3);
$repeat4	= strlen($teks1)-strlen($teks4);

$grs_atas	= (2+strlen($teks1)+2)-2;
$grs_bwh	= (2+strlen($teks1)+2)-2;

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",77).str_repeat(" ",$bts_kotak_dg_jml-2).str_repeat(" ",$left6).CHR(27).CHR(69)."Jumlah".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx1).number_format($jumlah).CHR(0)."\n";

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).str_repeat(" ",$line_first).CHR(27).CHR(69)."Tgl. Jatuh Tempo :".CHR(27).CHR(70).$tgljthtempo."\n";

$List	.= CHR(1).CHR(15).CHR(218).str_repeat(CHR(196),71).CHR(191).CHR(18).CHR(0)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,".str_repeat(" ",$repeat1).str_repeat(" ",3).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+1).str_repeat(" ",$left2).CHR(27).CHR(69)."Diskon".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx2).number_format($diskon).CHR(18)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0).str_repeat(" ",3)."kecuali ada perjanjian terlebih dahulu.".str_repeat(" ",$repeat2).str_repeat(" ",28).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",($bts_kotak_dg_jml+15))." ".str_repeat(CHR(196),$left44).CHR(18)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."2. Faktur asli merupakan bukti pembayaran yg sah".str_repeat(" ",$repeat3).str_repeat(" ",22).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+2).str_repeat(" ",$left3).CHR(27).CHR(69)."DPP".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx3).number_format($dpp).CHR(18)."\n";

$List	.= CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."3. Pembayaran dgn cek/ giro baru dianggap sah setelah diuangkan".str_repeat(" ",$repeat4).str_repeat(" ",7).CHR(179).CHR(18);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+2).str_repeat(" ",$left5).CHR(27).CHR(69)."PPN".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx4).number_format($nilai_ppn).CHR(18)."\n";

$List	.= CHR(1).CHR(15).CHR(192).str_repeat(CHR(196),71).CHR(217).CHR(18).CHR(0);

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",($bts_kotak_dg_jml+15))." ".str_repeat(CHR(196),$left44).CHR(18)."\n";

$List	.= CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",77).str_repeat(" ",$bts_kotak_dg_jml-2).str_repeat(" ",$left4).CHR(27).CHR(69)."Nilai Faktur".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx5).number_format($nilai_faktur).CHR(0)."\n";

$terbil=&Terbilang($nilai_faktur);
$List	.= CHR(1).CHR(15).str_repeat(" ",$line-(strlen($terbil)) )."(Terbilang :".$terbil." Rupiah.)".CHR(18).CHR(0)."\n";

$List	.= CHR(11); // Vertical Tab

$List	.= CHR(11);

$List	.= CHR(1).CHR(27).CHR(88).CHR(28).CHR(0).CHR(27).CHR(69).str_repeat(" ",$line_first)."SE"." & "."0".CHR(27).CHR(70)."\n";

for($j=1;$j<=4;$j++) {
  $List	.= "\n";
}

$List	.= CHR(1).CHR(27).CHR(88).CHR(28).CHR(0).CHR(27).CHR(69).str_repeat(" ",$line_first)."Adm Penjualan".CHR(27).CHR(70)."\n";

if($lup < 22) {
 for($jj=$lup;$jj < 24;$jj++) {
   $List	.= "\n";
 }
}

$List	.= str_repeat(CHR(205),($line-2))."\n";

$ipp->setData($List);
$ipp->printJob();

/*** Original

$ipp->setData(CHR(1).str_repeat(CHR(205),($line-2)).CHR(0)."\n");
$ipp->printJob();

$ipp->setData("\n");
$ipp->printJob();

//Bold	= CHR(27).CHR(69), End 0f Bold = CHR(27).CHR(70)
//Double Height	= CHR(27).CHR(119).CHR(1), End 0f Height : CHR(27).CHR(119).CHR(0)
//Double Width	= CHR(27).CHR(87).CHR(1), End 0f Width : CHR(27).CHR(87).CHR(0)
//Italic	= CHR(27).CHR(52), End 0f italic	= CHR(27).CHR(53)
//San Serif	= CHR(27).CHR(107).CHR(49)
//Start Text	= CHR(2)
//End Text	= CHR(3)
//Set Bottom Margin	= CHR(27).CHR(78).CHR(2)
//Cancel Set Bottom Margin	= CHR(27).CHR(79)

$range	= $line-($line_first+strlen('faktur penjualan')+strlen($nminitial)+$line_last+1);
$ipp->setData(str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(107).CHR(48).CHR(27).CHR(119).CHR(1). CHR(27).CHR(69)."Faktur Penjualan".CHR(27).CHR(70).CHR(27).CHR(119).CHR(0).CHR(27).CHR(107).CHR(48).CHR(0).str_repeat(" ",($range)).CHR(27).CHR(69).$nminitial.CHR(27).CHR(70).str_repeat(" ",$line_last)."\n");
$ipp->printJob();

$ipp->setData(str_repeat(CHR(205),($line-2))."\n");
$ipp->printJob();

$range0	= $line-($line_first+strlen('nomor faktur')+2+strlen($nomorfaktur)+strlen('tanggal faktur')+2+strlen($tglfaktur)+$line_last);
$ipp->setData(str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(69)."Nomor Faktur".str_repeat(" ",2).$nomorfaktur.CHR(27).CHR(70).CHR(0).str_repeat(" ",$range0).CHR(1).CHR(27).CHR(69)."Tanggal Faktur".CHR(27).CHR(70).CHR(0).str_repeat(" ",2).$tglfaktur.str_repeat(" ",$line_last)."\n");
$ipp->printJob();

$ipp->setData("\n");
$ipp->printJob();


// Misalkan 

//bts	= 5
//line	= 80
//kalimat1= 60
//kalimat2= 50

//garis1	= (line-(2+60+2))-5
//	= 11
//garis2	= (line-(2+50+2))-5
//	= 21

//selisih	= 21-11
//	= 10	
//grske1	= bts
//grske2	= bts+selisih			

$bts	= 5;
$kalimat1	= strlen($nmcabang);
$kalimat2	= strlen($alamatcabang);
$bhn_grs1	= ($line-(2+$kalimat1+2))+$bts;
$bhn_grs2	= ($line-(2+$kalimat2+2))+$bts;
if($kalimat1 > $kalimat2) {
	$selisih	= $bhn_grs2-$bhn_grs1;
	$grske1		= $bts;
	$grske2		= $bts+$selisih;
	//$btskpd	= ($kalimat1-(2+strlen('kepada :')+2))+$grske1;
	$btskpd		= ($kalimat1-(strlen('kepada :')))+$grske1;
	$s1		=  ($line-($kalimat1+2))+9;
} else {
	$selisih	= $bhn_grs1-$bhn_grs2;
	$grske1		= $bts+$selisih;
	$grske2		= $bts;
	//$btskpd	= ($kalimat2-(2+strlen('kepada :')+2))+$grske2;
	$btskpd		= ($kalimat2-(strlen('kepada :')))+$grske2;
	$s1		=  ($line-($kalimat2+2))+9;
}

$patok	= $s1;
$enotenya	= $enote;
$pjg_enote	= strlen($enotenya);

$temp	= array();
$a=0;
$b=0;
$jml=0;
$kata1	= "";
$kata2	= "";
$kata3	= "";

while($a<$pjg_enote){
	$temp[$a]	= substr($enotenya,$a,1);
	$a+=1;
}

foreach($temp as $kunci => $karakter){
	if(($jml%$patok)==0)
		$b=$b+1;
	
	if($b==1)
	$kata1.=$karakter;
	
	if($b==2)
	$kata2.=$karakter;
	
	if($b==3)
	$kata3.=$karakter;
	
	$jml+=1;
}

if(!empty($kata1)){
	$isikata1=$kata1;
}else{
	$isikata1=str_repeat(" ",$s1);
}

if(!empty($kata2)){
	$isikata2=$kata2;
}else{
	$isikata2=str_repeat(" ",$s1);
}

if(!empty($kata3)){
	$isikata3=$kata3;
}else{
	$isikata3=str_repeat(" ",$s1);
}


$ipp->setData(str_repeat(" ",$line_first).CHR(1).CHR(27).CHR(69)."Kepada :".CHR(27).CHR(70).CHR(0).str_repeat(" ",$btskpd).CHR(1).CHR(15).CHR(218).str_repeat(CHR(196),$s1+1).CHR(191).CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(str_repeat(" ",$line_first).$nmcabang.str_repeat(" ",$grske1).CHR(15).CHR(179)."keterangan:".str_repeat(" ",($s1-strlen('keterangan:'))+1).CHR(179).CHR(18)."\n");
$ipp->printJob();

//$ipp->setData(str_repeat(" ",$line_first).$alamatcabang.str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).str_repeat(" ",$s1).CHR(179).CHR(18).CHR(0)."\n");
$ipp->setData(str_repeat(" ",$line_first).$alamatcabang.str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).$isikata1.str_repeat(" ",($s1-strlen($isikata1))+1).CHR(179).CHR(18).CHR(0)."\n");
$ipp->printJob();

//$ipp->setData(str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).str_repeat(" ",$s1).CHR(179).CHR(18).CHR(0)."\n");
$ipp->setData(str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).$isikata2." ".CHR(179).CHR(18).CHR(0)."\n");
$ipp->printJob();

if(!empty($kata3)){
	//$ipp->setData(str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).str_repeat(" ",$s1).CHR(179).CHR(18).CHR(0)."\n");
	$ipp->setData(str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(179).$isikata3." ".CHR(179).CHR(18).CHR(0)."\n");
	$ipp->printJob();
}

//$ipp->setData(str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(192).str_repeat(CHR(196),$s1).CHR(217).CHR(18).CHR(0)."\n");
$ipp->setData(str_repeat(" ",$line_first).str_repeat(" ",strlen($alamatcabang)).str_repeat(" ",$grske2).CHR(1).CHR(15).CHR(192).str_repeat(CHR(196),$s1+1).CHR(217).CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData("\n");
$ipp->printJob();

$ipp->setData(str_repeat(CHR(196),($line-2))."\n");
$ipp->printJob();

$range4	= $line-($line_first+strlen('no')+1+strlen('kode barang')+1+strlen('nama barang')+strlen('unit')+1+strlen('harga')+1+strlen('jumlah')+1);
$ipp->setData(CHR(1).CHR(27).CHR(69).str_repeat(" ",$line_first)."No".str_repeat(" ",2)."Kode Barang".str_repeat(" ",1)."Nama Barang".str_repeat(" ",($range4-11))."Unit".str_repeat(" ",4)."Harga".str_repeat(" ",5)."Jumlah".str_repeat(" ",2).CHR(27).CHR(70).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(str_repeat(CHR(196),($line-2))."\n");
$ipp->printJob();

$no	= 1;

$spcno	= 6; // sesuaikan dgn header No + 2
//$spckb	= 13; // +1
//$spcnb	= 11+$range4;
$spckb	= 20;
$spcnb	= 58;
$spcunit= 9;
$spcprice	= 12;
$spcjml	= 15;

$arrno	= array();
$arrkb	= array();
$arrnb	= array();
$arrunit= array();
$arrprice= array();
$arrjml	= array();

$spunit	= array();
$spprice	= array();
$spamount	= array();

$lup	= 0;
$jmlqty		= array();
$uprice		= array();
$tprice		= array();

foreach($isi as $row) {
	if($lup<22) {		
		$qvalue	= $this->mclass->jmlitemharga($row->i_faktur,$row->imotif);
		if($qvalue->num_rows()>0) {
			$row_value	= $qvalue->row();
			$jmlqty[$lup]	= $row_value->qty;
			$uprice[$lup]	= $row_value->unitprice;
			$tprice[$lup]	= $row_value->amount;
		} else {
			$jmlqty[$lup]	= 0;
			$uprice[$lup]	= 0;
			$tprice[$lup]	= 0;		
		}
		$arrno[$lup]	= (strlen($no)<=$spcno)?$spcno-strlen($no):0;
		$arrkb[$lup]	= (strlen(trim($row->imotif))<=$spckb)?$spckb-strlen($row->imotif):0;
		$arrnb[$lup]	= (strlen($row->motifname)<=$spcnb)?$spcnb-strlen($row->motifname):0;
		$arrunit[$lup]	= (strlen($jmlqty[$lup])<=$spcunit)?$spcunit-strlen($jmlqty[$lup]):strlen($jmlqty[$lup]);
		$arrprice[$lup]	= (strlen($uprice[$lup])<=$spcprice)?$spcprice-strlen($uprice[$lup]):strlen($uprice[$lup]);
		$arrjml[$lup]	= (strlen($tprice[$lup])<=$spcjml)?$spcjml-strlen($tprice[$lup]):strlen($tprice[$lup]);
	
		$spunit[$lup]	= strlen(number_format($jmlqty[$lup]))<=$spcunit?$spcunit-strlen(number_format($jmlqty[$lup])):0;
		$spprice[$lup]	= strlen(number_format($uprice[$lup]))<=$spcprice?$spcprice-strlen(number_format($uprice[$lup])):0;
		$spamount[$lup]	= strlen(number_format($tprice[$lup]))<=$spcjml?$spcjml-strlen(number_format($tprice[$lup])):0;

		$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",3).$no.str_repeat(" ",$arrno[$lup]).trim($row->imotif).str_repeat(" ",$arrkb[$lup]).$row->motifname.str_repeat(" ",$arrnb[$lup]).str_repeat(" ",$spunit[$lup]).number_format($jmlqty[$lup]).str_repeat(" ",3).str_repeat(" ",$spprice[$lup]).number_format($uprice[$lup]).str_repeat(" ",3).str_repeat(" ",$spamount[$lup]).number_format($tprice[$lup]).CHR(18)."\n");
		$ipp->printJob();
	}
	$no+=1;
	$lup+=1;
}

$ipp->setData(CHR(1).str_repeat(CHR(196),($line-2)).CHR(0)."\n");
$ipp->printJob();

$teks1	= 66; //"1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,";
$teks2	= 39; //"kecuali ada perjanjian terlebih dahulu.";
$teks3	= 48; //"2. Faktur asli merupakan bukti pembayaran yg sah";
$teks4	= 63; //"3. Pembayaran dgn cek/ giro baru dianggap sah setelah diuangkan";

$bts_rp_nilai_uang	= 20;
$space_awal_rp	= 1;
$bts_kotak_dg_jml	= 3;

$xx1	= $bts_rp_nilai_uang-strlen(number_format($jumlah));
$xx2	= $bts_rp_nilai_uang-strlen(number_format($diskon));
$xx3	= $bts_rp_nilai_uang-strlen(number_format($dpp));
$xx4	= $bts_rp_nilai_uang-strlen(number_format($nilai_ppn));
$xx5	= $bts_rp_nilai_uang-strlen(number_format($nilai_faktur));

$left1	= $line-($line_first+1+strlen('tgl. jatuh tempo :')+strlen($tgljthtempo)+1+strlen('jumlah')+1+strlen('rp.')+$xx1+strlen(number_format($jumlah)));
$left2	= ( $line-(1+strlen('diskon')+1+strlen('rp.')+$xx2+strlen(number_format($diskon))) )-($bts_kotak_dg_jml+24);
$left3	= ( $line-($line_first+strlen('dpp')+1+strlen('rp.')+$xx3+strlen(number_format($dpp))) )-($bts_kotak_dg_jml+24);
$left4	= ( $line-($line_first+strlen('nilai faktur')+1+strlen('rp.')+$xx5+strlen(number_format($nilai_faktur))) )-($bts_kotak_dg_jml+24);
$left5	= ( $line-($line_first+strlen('ppn')+1+strlen('rp.')+$xx4+strlen(number_format($nilai_ppn))) )-($bts_kotak_dg_jml+24);

$left6	= ( $line-($line_first+strlen('jumlah')+1+strlen('rp.')+$xx1+strlen(number_format($jumlah))) )-($bts_kotak_dg_jml+24);

$right4	= $left4;
$left44	= ($line-($right4+1+$bts_kotak_dg_jml))-24;

$repeat1	= 1;
$repeat2	= strlen($teks1)-strlen($teks2);
$repeat3	= strlen($teks1)-strlen($teks3);
$repeat4	= strlen($teks1)-strlen($teks4);

$grs_atas	= (2+strlen($teks1)+2)-2;
$grs_bwh	= (2+strlen($teks1)+2)-2;

$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",77).str_repeat(" ",$bts_kotak_dg_jml-2).str_repeat(" ",$left6).CHR(27).CHR(69)."Jumlah".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx1).number_format($jumlah).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).str_repeat(" ",$line_first).CHR(27).CHR(69)."Tgl. Jatuh Tempo :".CHR(27).CHR(70).$tgljthtempo."\n");
$ipp->printJob();

$ipp->setData(CHR(1).CHR(15).CHR(218).str_repeat(CHR(196),71).CHR(191).CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,".str_repeat(" ",$repeat1).str_repeat(" ",3).CHR(179).CHR(18));
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+1).str_repeat(" ",$left2).CHR(27).CHR(69)."Diskon".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx2).number_format($diskon).CHR(18)."\n");
$ipp->printJob();

$ipp->setData(CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0).str_repeat(" ",3)."kecuali ada perjanjian terlebih dahulu.".str_repeat(" ",$repeat2).str_repeat(" ",28).CHR(179).CHR(18));
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",($bts_kotak_dg_jml+15))." ".str_repeat(CHR(196),$left44).CHR(18)."\n");
$ipp->printJob();

$ipp->setData(CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."2. Faktur asli merupakan bukti pembayaran yg sah".str_repeat(" ",$repeat3).str_repeat(" ",22).CHR(179).CHR(18));
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+2).str_repeat(" ",$left3).CHR(27).CHR(69)."DPP".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx3).number_format($dpp).CHR(18)."\n");
$ipp->printJob();

$ipp->setData(CHR(15).CHR(179).str_repeat(" ",1).CHR(1).CHR(27).CHR(32).CHR(0).CHR(0).CHR(1).CHR(27).CHR(88).CHR(0)."3. Pembayaran dgn cek/ giro baru dianggap sah setelah diuangkan".str_repeat(" ",$repeat4).str_repeat(" ",7).CHR(179).CHR(18));
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",$bts_kotak_dg_jml+2).str_repeat(" ",$left5).CHR(27).CHR(69)."PPN".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx4).number_format($nilai_ppn).CHR(18)."\n");
$ipp->printJob();

$ipp->setData(CHR(1).CHR(15).CHR(192).str_repeat(CHR(196),71).CHR(217).CHR(18).CHR(0));
$ipp->printJob();
$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",($bts_kotak_dg_jml+15))." ".str_repeat(CHR(196),$left44).CHR(18)."\n");
$ipp->printJob();

$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(32).CHR(0).CHR(15).str_repeat(" ",77).str_repeat(" ",$bts_kotak_dg_jml-2).str_repeat(" ",$left4).CHR(27).CHR(69)."Nilai Faktur".CHR(27).CHR(70)." "."Rp.".str_repeat(" ",$xx5).number_format($nilai_faktur).CHR(0)."\n");
$ipp->printJob();

$terbil=&Terbilang($nilai_faktur);
$ipp->setData(CHR(1).CHR(15).str_repeat(" ",$line-(strlen($terbil)) )."(Terbilang :".$terbil." Rupiah.)".CHR(18).CHR(0)."\n");
$ipp->printJob();

$ipp->setData(CHR(11)); // Vertical Tab
$ipp->printJob();

$ipp->setData(CHR(11));
$ipp->printJob();

//$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(28).CHR(0).CHR(27).CHR(69).str_repeat(" ",$line_first)."SE".CHR(128)."0".CHR(27).CHR(70)."\n");
$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(28).CHR(0).CHR(27).CHR(69).str_repeat(" ",$line_first)."SE"." & "."0".CHR(27).CHR(70)."\n");
$ipp->printJob();

for($j=1;$j<=4;$j++) {
  $ipp->setData("\n");
  $ipp->printJob();
}

$ipp->setData(CHR(1).CHR(27).CHR(88).CHR(28).CHR(0).CHR(27).CHR(69).str_repeat(" ",$line_first)."Kasie Accounting".CHR(27).CHR(70)."\n");
$ipp->printJob();

if($lup < 22) {
 for($jj=$lup;$jj < 24;$jj++) {
   $ipp->setData("\n");
   $ipp->printJob();
 }
}

$ipp->setData(str_repeat(CHR(205),($line-2))."\n");
$ipp->printJob();

***/

?>
</body>
