<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function settextfield(a,b,c,d,e,f,g,h,i,j) {
	
	var iter	= '<?=$iterasi?>';
	var banyak	= iter-1;
	var x;
	var ada = 0;
	
	var dfaktur	= c.split('-'); // YYYY-mm-dd
	var ddue	= e.split('-');
	var dftr	= dfaktur[2]+'/'+dfaktur[1]+'/'+dfaktur[0]; // dd-mm-YYYY
	var dduedate= ddue[2]+'/'+ddue[1]+'/'+ddue[0];
	
	for(x=0;x<=banyak;x++){
		if(opener.document.getElementById('i_faktur_code_tblItem_'+x).value==a.trim()){
			ada = 1;
			break;
		}
	}
	
	if(ada==0) {
		opener.document.getElementById('i_faktur_code_tblItem_'+iter).value	= a.trim();
		opener.document.getElementById('i_faktur_tblItem_'+iter).value	= b;
		opener.document.getElementById('tgl_faktur_tblItem_'+iter).value	= dftr;
		opener.document.getElementById('tgl_fakturhidden_tblItem_'+iter).value	= d;
		opener.document.getElementById('due_date_tblItem_'+iter).value	= dduedate;
		opener.document.getElementById('due_datehidden_tblItem_'+iter).value	= f;
		opener.document.getElementById('e_branch_name_tblItem_'+iter).value	= g;
		opener.document.getElementById('v_total_plusppn_tblItem_'+iter).value	= h;
		opener.document.getElementById('i_customer_tblItem_'+iter).value	= i;
		opener.document.getElementById('i_branch_code_tblItem_'+iter).value	= j;	
		opener.document.getElementById('i_faktur_code_tblItem_'+iter).focus();
			
		this.close();
		
	}else{
		alert('Nomor faktur '+a.trim()+' sudah ada!');
	}
}

function getInfo(txt) {
	
$.ajax({
	
type: "POST",
url: "<?php echo site_url('approvevoucher/cform/flistfaktur');?>",
data:"key="+txt,
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest) {
	alert(XMLHttpRequest.responseText);
}

})
};

function carifocus() {
	document.getElementById('txtcari').focus();
}

</script>

</head>
<body id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td align="right">Pencarian :
	  <form>	
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="getInfo(this.value);"/>
	  </form>
  	</td>
  </tr>
  <tr>
    <td align="left">
	<?php echo form_open('approvevoucher/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php
		if( !empty($isi) || isset($isi) ) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			
			if(count($isi)) {
				$cc	= 1;				
				foreach($isi as $row) {
				
					$tgl			= (!empty($row->d_faktur) || strlen($row->d_faktur)!=0)?@explode("-",$row->d_faktur,strlen($row->d_faktur)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal		= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
					
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur','$row->d_faktur','$row->d_faktur','$row->d_due_date','$row->d_due_date','$row->e_branch_name','$row->v_total_fppn','$row->i_customer','$row->i_branch_code')\">".$row->i_faktur_code."</a></td>
					  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur','$row->d_faktur','$row->d_faktur','$row->d_due_date','$row->d_due_date','$row->e_branch_name','$row->v_total_fppn','$row->i_customer','$row->i_branch_code')\">".$tanggal."</a></td>
					 </tr>";
					 $cc+=1;
				}
			}
		}
		?>
		
		<table class="listtable2" border="1">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Nomor Faktur"); ?></th>
		 <th width="130px;"><?php echo strtoupper("Tgl. Faktur"); ?></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>"; ?>
  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
