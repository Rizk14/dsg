<h3>Set Stok Awal Berdasarkan Harga Pembelian</h3><br>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

 $(function()
 {
		
 });
 
 function cek_cb() {
	var hitung = $(":checkbox:checked").length;
	var jum_data = $('#jum_data').val();

	if (hitung == 0) {
		alert("Data-data barangnya harus dipilih..!");
		return false;
	}
	else {
		for (var k=1; k <= jum_data; k++) {
			if ($('input[name=cek_'+k+']').is(':checked')) {
				if($('#stok_'+k).val() == '0' || $('#stok_'+k).val() == '' ) {				
					alert("Data stok tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#stok_'+k).val()) ) {
					alert("Stok harus berupa angka..!");
					return false;
				}
				
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Harga harus berupa angka..!");
					return false;
				}
				
				if($('#supplier_'+k).val() == '0') {				
					alert("Supplier harus dipilih...!");
					return false;
				}
			}// end if
		} // end for
	} // end else
}
</script>

<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/set-stok-harga/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="jum_data" id="jum_data" value="<?php if (is_array($list_brg)) echo count($list_brg); else echo "0" ?>">
<div align="center">

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
		<td colspan="2">Daftar bahan baku/pembantu yang belum ada data stok harga</td>
    </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th>Jml Stok</th>
	      <th>Harga (Rp.)</th>
	      <th>Supplier</th>
	      <th>Pilih</th>
        </tr>

        <?php $i=1;
        if (!is_array($list_brg) ) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap" colspan="3">
		   Data barang tidak ada atau sudah ada stok harga</td>
          
        </tr>
        <tr>
			<td colspan="7"><input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/set-stok-harga/cform/'"></td>
        </tr>
		
		<?php
		} else {
			$i= 1;
		  if (is_array($list_brg)) {
			 for($j=0;$j<count($list_brg);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $list_brg[$j]['kode_brg'] ?>"/>
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="40" readonly="true" value="<?php echo $list_brg[$j]['nama_brg'] ?>" /></td>
          <td><input name="stok_<?php echo $i ?>" type="text" id="stok_<?php echo $i ?>" size="7" maxlength="7" value="" /></td>
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="7" maxlength="7" value="" /></td>
          <td><select name="supplier_<?php echo $i ?>" id="supplier_<?php echo $i ?>">
					<option value="0" >-Pilih-</option>
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select></td>
          <td><input name="cek_<?php echo $i ?>" type="checkbox" id="cek_<?php echo $i ?>" value="y" /></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		
		?>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="7" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_cb();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/set-stok-harga/cform/'"></td>
	</tr>
	<?php } ?>
	</table>	
	
	</form>
		</td>
    </tr>

</table>
</div>
</form>
