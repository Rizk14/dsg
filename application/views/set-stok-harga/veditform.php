<h3>Data Harga Bahan Baku/Pembantu Berdasarkan Supplier</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-bb-sup/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-bb-sup/cform/view">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

 $(function()
 {
		
	

 });
 
 function cek_data() {
	var harga= $('#harga').val();


	if (harga == '' || harga == '0') {
		alert("Harga tidak boleh 0 / kosong..!");
		$('#harga').focus();
		return false;
	}
	
	if (isNaN($('#harga').val())) {
		alert("Harga harus berupa angka..!");
		$('#harga').focus();
		return false;
	}
	
 }	

</script>

<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<?php 
$attributes = array('name' => 'f_bb', 'id' => 'f_bb');
echo form_open('mst-bb-sup/cform/edit', $attributes); ?>
<input type="hidden" name="go_edit" value="1">
<input type="hidden" name="idharga" value="<?php echo $idharga ?>">
<input type="hidden" name="kode_supplier" value="<?php echo $query[0]['kode_supplier'] ?>">
<input type="hidden" name="kode_brg" value="<?php echo $query[0]['kode_brg'] ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="esupplier" value="<?php echo $esupplier ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<table width="50%">
		<tr>
			<td colspan="2">Edit Data</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td width="30%">Supplier</td>
			<td> <?php echo $query[0]['kode_supplier']." - ".$query[0]['nama_supplier'] ?>
			</td>
		</tr>
		<tr>
			<td>Kode & Nama Brg</td>
			<td> <?php echo $query[0]['kode_brg']." - ".$query[0]['nama_brg'] ?>
			</td>
		</tr>
		<tr>
			<td>Harga (Rp.)</td>
			<td> <input type="text" name="harga" id="harga" value="<?php echo $query[0]['harga'] ?>" size="10" >
				<input type="hidden" name="harga_lama" id="harga_lama" value="<?php echo $query[0]['harga'] ?>">
			</td>
		</tr>
</table><br>
		<?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "mst-bb-sup/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "mst-bb-sup/cform/cari/index/".$esupplier."/".$carinya."/".$cur_page;
        ?>
<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<?php echo form_close(); ?>
