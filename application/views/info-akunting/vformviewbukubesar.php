
<h3>Laporan Buku Besar</h3>

Periode : <?php echo $date_from ." s/d " .$date_to  ?>
<br>
Coa    : <?php echo $coa[0]['nama'] ?>
<br><br>
<form action='exportbukubesar' method='post'>
<input type='hidden' id='date_to' name='date_to' value='<?php echo $date_to ?>'>
<input type='hidden' id='date_from' name='date_from' value='<?php echo $date_from ?>'>
<input type='hidden' id='id_coa' name='id_coa' value='<?php echo $coa[0]['id'] ?>'>
<input type='submit' id='export_ods' name='export_ods' value='Export ke ODS'>
<input type='submit' id='export_excel' name='export_excel' value='Export ke Excel'>
</form>
<br><br>
<table border='1' class="table table-condensed">
<tr>
<thead>
<th>No</th>
<th>Tanggal</th>
<th>No Transaksi</th>
<th>Deskripsi</th>
<th>Debet</th>
<th>Kredit</th>
<th>Saldo Akhir</th>
</thead>
</tr>	
	
<tr>
<thead>

<th colspan='4'><?php echo$coa[0]['kode']; ?></th>
<th colspan='2'>Saldo Awal</th>
<th><?php echo $saldoawal; ?></th>
</thead>
</tr>



<?php 
if(is_array($query)){
	
$no=1;
$sakhir=0;
$totdeb=0;$totcre=0;

	for($a=0;$a<count($query);$a++){
		echo "<tr>";
		echo "<td>".$no."</td>" ;
		echo "<td>".$query[$a]['d_mutasi']."</td>";
		echo "<td>".$query[$a]['i_refference']."</td>";
		echo "<td>".$query[$a]['deskripsi']."</td>";
		 switch($query[$a]['f_debet'])
						    {
							    case 't' :
								    if($query[$a]['flagvmutasidebet']<$query[$a]['v_mutasi_kredit'])
								    {
									   $query[$a]['v_mutasi_kredit']=$query[$a]['flagvmutasidebet'];
								    }
								    break;
							    case 'f' :
								    if($query[$a]['flagvmutasikredit']<$query[$a]['v_mutasi_debet'])
								    {
									   $query[$a]['v_mutasi_debet']=$query[$a]['flagvmutasikredit'];
								    }
								    break;
						    }
		 $saldoawal+=  $query[$a]['v_mutasi_kredit'] - $query[$a]['v_mutasi_debet'];
		echo "<td>".number_format($query[$a]['v_mutasi_kredit'],2,',','.')."</td>";
		$totdeb+=$query[$a]['v_mutasi_kredit'];
		echo "<td>".number_format($query[$a]['v_mutasi_debet'],2,',','.')."</td>";
		$totcre+=$query[$a]['v_mutasi_debet'];
		
		echo "<td>".number_format($saldoawal,2,',','.')."</td>";
		echo "</tr>";
		$no++;
		}


		echo "<tr>";
		echo "<td colspan='4' align ='center'>TOTAL.</td>";
		echo "<td>".number_format($totdeb,2,',','.'). "</td>";
		echo "<td>".number_format($totcre,2,',','.'). "</td>";
		echo "<td>".number_format($saldoawal,2,',','.')."</td>";
		echo "</tr>";
}
else
{
		echo "<tr>";
		echo "<td colspan='4' align ='center'>TOTAL.</td>";
		echo "<td>".number_format(0). "</td>";
		echo "<td>".number_format(0). "</td>";
		echo "<td>".number_format(0). "</td>";
		echo "</tr>";
		}
?>


</table>
