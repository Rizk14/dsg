<h3>Laporan Kas Besar</h3>

Periode : <?php echo $date_from ." s/d " .$date_to  ?>
<br>
Total   : <?php echo $jumlah  ?>
<br>
Area    : <?php echo $nama_area ?>
<br><br>
<form action='exportkasbesar' method='post'>
	<input type='hidden' id='date_to' name='date_to' value='<?php echo $date_to ?>'>
	<input type='hidden' id='date_from' name='date_from' value='<?php echo $date_from ?>'>
	<input type='hidden' id='id_area' name='id_area' value='<?php echo $id_area ?>'>
	<input type='submit' id='export_ods' name='export_ods' value='Export ke ODS'>
	<input type='submit' id='export_excel' name='export_excel' value='Export ke Excel'>
</form>
<br><br>
<table border='1'>
	<tr>
		<thead>
			<th>No</th>
			<th>No Transaksi</th>
			<th>Tanggal Transaksi</th>
			<th>No. Voucher</th>
			<th>Keterangan</th>
			<th>CoA</th>
			<th>Debet</th>
			<th>Kredit</th>
			<th>Saldo</th>
			<th>Keterangan</th>
		</thead>
	</tr>
	<tr>

		<?php 
			if(is_array($query)){	
				$no=1;
				$totalsaldo = 0;
				$debet=0;
				$kredit=0;

				$tot_debet = 0;
				$tot_kredit = 0;
				$tot_saldo = 0;
				$total =0;
				$total_a=0;
				$total_b=0;

				echo "<td align='center' colspan='8'><b>Saldo Awal</td>";
				echo "<td><b>".number_format($totsaldo,2,',','.')."</td>";

				for($i=0;$i<count($query);$i++){

					$debet 	= $query[$i]['debet'];
					$kredit = $query[$i]['kredit'];

					$tot_debet  += $query[$i]['debet'];
					$tot_kredit += $query[$i]['kredit'];
					echo "<tr>";
					echo "<td>".$no."</td>" ;
					echo "<td>".$query[$i]['no_transaksi']."</td>";
					echo "<td>".$query[$i]['tanggal']."</td>";
					echo "<td>".$query[$i]['irv']."</td>";
					echo "<td>".$query[$i]['keterangan']."</td>";
					echo "<td>".$query[$i]['kode_coa']."</td>";
					echo "<td>".$query[$i]['debet']."</td>";
					echo "<td>".$query[$i]['kredit']."</td>";
					echo "<td>".number_format($totalsaldo=$totsaldo+$debet-$kredit,2,',','.')."</td>";
					//echo "<td>".number_format($query[$i]['value'],2,',','.')."</td>";
					echo "<td>".$query[$i]['nama_coa']."</td>";
					echo "</tr>";
					$total_a += $totalsaldo;	
					$no++;
				}
				$total_b = $totsaldo + $total_a;			
				echo "<td align='center' colspan='6'><b>Total</td>";
				echo "<td><b>".number_format($tot_debet,2,',','.')."</td>";
				echo "<td><b>".number_format($tot_kredit,2,',','.')."</td>";
				echo "<td><b>".number_format($total_b,2,',','.')."</td>";
			}
		?>
	</tr>
</table>