<style>
.vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
}

</style>

<div class="container">
<h2>Edit Input Makloon Baju dari Pengadaan ke Unit Jahit</h2>
<hr>

<?php 

echo form_open('/makloon_pengadaan_baju_wip/makloon_pengadaan_baju_wip/updatedata', array('id'=>'myform', 'class'=>'form-vertical', 'role'=>'form', 'method'=>'post')) ?>
	  
	 
	   <input type="hidden" name="no" id="no">
	   <input type="hidden" name="id" id="id" value=<?php echo $values[0]['id'] ; ?>>
	   <div id='my_div_1'>   
			<?php echo form_input('num',1,'id="num" class="form-control" maxlength="3" size="1" align="center" disabled' ) ?> 
           
           <span class="label label-info">No SJ</span>
           <input type="hidden" name="no_sj_wip_lama" id="no_sj_wip_lama" value="<?php echo $values[0]['no_sj'] ?>">
            <?php echo form_input('no_sj', $values[0]['no_sj'], 'id="no_sj" class="form-control" placeholder="No SJ" maxlength="10"') ?>            
            
            <span class="label label-info">Tanggal SJ</span>
			<div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_sj', $values[0]['tanggal_sj'], 'id="tanggal_sj" class="form-control" placeholder="Tanggal SJ" maxlength="10"') ?>
			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
            
           <span class="label label-info" >Jenis Masuk</span>
            <?php
                 
                $jenis_masuk = array(
                '' => '- Pilih -',
                 '1' => 'Masuk Bagus',
                 '2' => 'Masuk Lain-lain', 
            );
                    $atribut_jenis_masuk = 'class="form-control"' ;
            echo form_dropdown('jenis_masuk' , $jenis_masuk, $values[0]['jenis_masuk'], $atribut_jenis_masuk );
            ?>
		 

		
 <span class="label label-info">Unit Jahit</span>
  <select class="form-control" id="unit_jahit" name="unit_jahit" disabled>
	  <option value="0"> - Pilih -</option>
   <?php
		foreach ($list_unit_jahit as $luj){
			?>
			 <option  
			 <?php if ($values[0]['id_unit_jahit'] == $luj->id) { ?>  selected <?php } ?> value="<?php echo $luj->id ?>">
				<?php echo $luj->nama_unit_jahit ?> 
			 </option>
	<?php } ?>
  </select>
     <input type="hidden" name="id_unit_jahit" id="id_unit_jahit" value='<?php echo $values[0]['id_unit_jahit'] ?>'> 
        
       
 <span class="label label-info">Gudang</span>
  <select class="form-control" id="gudang" ="gudang" disabled>
	  <option value="0"> - Pilih -</option>
   <?php
		foreach ($list_gudang as $lgu){
			?>
			 <option 
			 <?php if ($values[0]['id_gudang'] == $lgu->id) { ?>  selected <?php } ?> value="<?php echo $values[0]['id_gudang'] ?>">
				<?php echo $lgu->nama_gudang ?> 
			 </option disabled>
	<?php } ?>
  </select>
     <input type="hidden" name="id_gudang" id="id_gudang" value='<?php echo $values[0]['id_gudang'] ?>'>   
 
<span class="label label-info">Keterangan Header</span>
           <input type="hidden" name="keterangan_header_lama" id="keterangan_header_lama" value="<?php echo $values[0]['keterangan_header'] ?>">
            <?php echo form_input('keterangan_header', $values[0]['keterangan_header'], 'id="keterangan_header" class="form-control" placeholder="Keterangan Header" maxlength="10"') ?>      
       
        
        
        <br>

                 <div id='my_div_1'>
				  <?php 
				  $i=1;
         $detail_data =$values[0]['detail_data'];
         for($k=0;$k<count($detail_data);$k++){
			 ?>
				<input type="text" name="num_<?php echo $i ?>" id="num_<?php echo $i ?>"  class="form-control input-group-lg" value="<?php echo $i ?> " readonly="true">
				
				
				<input type="hidden" name="id_detail_<?php echo $i ?>" id="id_detail_<?php echo $i ?>" value="<?php echo $detail_data[$k]['id_detail'] ?>">
				<input type="hidden" name="id_barang_wip_<?php echo $i ?>" id="id_barang_wip_<?php echo $i ?>" value="<?php echo $detail_data[$k]['id_barang_wip'] ?>">
                <input id="kode_barang_wip_<?php echo $i ?>" class="col-lg-3 " type="text" name="kode_barang_wip_<?php echo $i ?>"
						title="Masukkan Kode Barang Wip"  value="<?php echo $detail_data[$k]['kode_barang_wip'] ?>"
                       placeholder="Kode Barang WIP" onclick= "javascript: openCenteredWindows('<?php echo base_url(); ?>index.php/makloon_pengadaan_baju_wip/makloon_pengadaan_baju_wip/barang_wip/<?php echo $i ;?>');"
                      disabled />
	
                <input id="nama_barang_wip_<?php echo $i ?>" class="col-lg-3" type="text" name="nama_barang_wip_<?php echo $i ?>"
                       title="Masukkan Nama Barang Wip"  value="<?php echo $detail_data[$k]['nama_barang_wip'] ?>"
                       placeholder="Nama Barang WIP"/ disabled>
				
				<input type="hidden" name="qty_lama_<?php echo $i ?>" id="qty_lama_<?php echo $i ?>" value="<?php echo $detail_data[$k]['qty'] ?>">
                <input id="qty_<?php echo $i ?>" class="col-lg-3" type="text" name="qty_<?php echo $i ?>"
                       title="Masukkan Quantity" value="<?php echo $detail_data[$k]['qty'] ?>"
                       placeholder="Quantity"/>
                       
               <input id="keterangan_detail_<?php echo $i ?>" class="col-lg-3" type="text" name="keterangan_detail_<?php echo $i ?>"
                       title="Masukkan keterangan detail" value="<?php echo $detail_data[$k]['keterangan_detail'] ?>"
                       placeholder="Keterangan detail"/>   
                       <?php 
                      
				  
         $detail_motif_data =$detail_data[$k]['detail_motif_data'];
         ?>
          <span class="label label-info" >Detail Motif</span>
         <?php
        for($x=0;$x<count($detail_motif_data);$x++){
			?>
			
			  <input  type='hidden' name="id_detail_motif_<?php echo $i ?>[]" id="id_detail_motif_<?php echo $i ?>[]" value="<?php echo $detail_motif_data[$x]['id_detail_motif'] ?>">
              <input  name="nama_barang_bb_<?php echo $i ?>[]" id="nama_barang_bb_<?php echo $i ?>[]" value="<?php echo $detail_motif_data[$x]['nama_barang_bb'] ?>" disabled>          
               <input  type='hidden' name="id_barang_bb_<?php echo $i ?>[]" id="id_barang_bb_<?php echo $i ?>[]" value="<?php echo $detail_motif_data[$x]['id_barang_bb'] ?>">
               
                       <?php 
                     
                      
			}
			
			 $i++;
          }
         ?>   
         
        </div>
       
         <input type='hidden' id='jum_data_detail' name='jum_data_detail' value='<?php echo $i?>'>
         <br>
  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 
 <?php echo anchor('/makloon_pengadaan_baju_wip/makloon_pengadaan_baju_wip/view', 'Batal', 'class="btn btn-danger " role="button" '); ?> 
<?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-success', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>

<?php echo form_close() ?>
 
</div>


