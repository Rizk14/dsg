
<div class="container">
<h2>Input Makloon Baju dari Pengadaan ke Unit Jahit</h2>
<hr>
<?php echo anchor('/dashboard/makloon_pengadaan_baju_wip/view', 'View', 'class="btn btn-info " role="button"'); ?>

<br>
<br>

    <form action ="makloon_pengadaan_baju_wip/submit" name="myform" id='myform' class="form-inline" method="post">
        
        <div class="row">
        <div class="form-group has-feedback <?php set_validation_style('no_sj')?>">            
            <label for="no_sj" class="control-label '">Nomor SJ &nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>
            <input id="no_sj" class="form-control input-group-lg" type="text" autocapitalize='off' name="no_sj"
                   title="Masukkan Nomor SJ"
                   placeholder="Nomor SJ"/>
        </div>
        </div>
        
         <div class="row">
        <?php echo form_label('Tanggal SJ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;', 'tanggal_sj', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback <?php set_validation_style('tanggal_sj')?>">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_sj', date_to_id($values->tanggal_sj), 'id="tanggal_sj" class="form-control" placeholder="Tanggal SJ" maxlength="10"') ?>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <?php if (form_error('tanggal_sj')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tanggal_sj', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
        
        
        
        <div class="row">
        <div class="form-group has-feedback <?php set_validation_style('jenis_masuk')?>">
            <label for="jenis_masuk" class="control-label">Jenis Masuk 
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>
            <?php
                   $jenis_masuk = array(
                '0' => '- Pilih -',
                '1' => 'Masuk Bagus ',
                '2' => 'Masuk Lain-lain', 
            );
                    $atribut_jenis_masuk = 'class="form-control"';
            echo form_dropdown('jenis_masuk', $jenis_masuk, $values->jenis_masuk, $atribut_jenis_masuk);
            ?>
        </div>
        </div>
        
         <div class="row">
        <div class="form-group has-feedback <?php set_validation_style('unit_jahit')?>">
            <label for="unit_jahit" class="control-label">Unit Masuk  
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="unit_jahit" name="unit_jahit">
		
		<?php foreach ($unit_jahit as $ujh) {?> 
    <option value=<?php echo $ujh->id ?>><?php echo $ujh->nama_unit_jahit?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        
          <div class="row">
        <div class="form-group has-feedback <?php set_validation_style('gudang')?>">
            <label for="gudang" class="control-label">Gudang Keluar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="gudang" name="gudang">
			
		<?php foreach ($gudang as $gud) {?> 
    <option value=<?php echo $gud->id ?>><?php echo $gud->nama_gudang?></option>
    <?php } ?>
  </select>
        </div>
        </div>
     
       
        
         
        <div class="row">
        <div class="form-group has-feedback <?php set_validation_style('keterangan_header')?>">
            <label for="keterangan_header" class="control-label">Keterangan Header  : &nbsp;&nbsp;</label>
		<input id="keterangan_header" class="form-control input-group-lg" type="text" autocapitalize='off' name="keterangan_header"
                   title="Masukkan Keterangan Header"
                   placeholder="Keterangan Header"/>
        
        </div>
        </div>
        <br>
        
         <div class="row">
        <div class="col-xs-12 col-md-4 pull-right">
		<?php echo form_button(array('id'=>'addRow','content'=>'+', 'type'=>'button', 'class'=>'btn btn-primary'  )) ?>
		<?php echo form_button(array( 'id'=>"deleteRow",'content'=>'-', 'type'=>'button', 'class'=>'btn btn-primary')) ?>
         </div>
         </div>
         <br>
<input type="hidden" name="no" id="no" value="">
			
            <div id='my_div_1'>
				
				<input type="hidden" name="id_barang_bb_1" id="id_barang_bb_1" value="">
                <input id="kode_barang_bb_1" class="form-control input-group-lg" type="text" name="kode_barang_bb_1"
                       title="Masukkan Kode Barang BB" value=""
                       placeholder="Kode Barang BB" onclick= "openCenteredWindows('item_makloon/barang_bb/1');"
                       />
	
                <input id="nama_barang_bb_1" class="form-control input-group-lg" type="text" name="nama_barang_bb_1"
                       title="Masukkan Nama Barang BB" value=""
                       placeholder="Nama Barang BB"/ disabled>
		
                <input id="qty_1" class="form-control input-group-lg" type="text" name="qty_1"
                       title="Masukkan Quantity" value=""
                       placeholder="Quantity"/>
                       
               <input id="keterangan_detail_1" class="form-control input-group-lg" type="text" name="keterangan_detail_1"
                       title="Masukkan keterangan detail" value=""
                       placeholder="Keterangan detail"/>        
                      
        </div>
</form>
 <br>
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-success', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>

        </div>
