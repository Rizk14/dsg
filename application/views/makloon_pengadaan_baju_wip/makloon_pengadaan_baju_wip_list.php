

<?php
// Nomor urut data di tabel.
$per_page = 10;



if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Data Makloon Baju dari Pengadaan ke Unit Jahit</h2>
    <hr>

    <!-- Paging dan form pencarian -->
    <div class="row navigasi_cari">
        <!-- Paging -->
        <div class="col-xs-12 col-md-6">
            <?php echo (!empty($paging)) ? $paging : ''?>
        </div>
        <!-- /Paging -->

        <!-- Form Pencarian -->
        <div class="col-xs-12 col-md-4 pull-right">
            <form method="get" action="<?php echo $form_action;?>" role="form" class="form-horizontal">
                <div class="input-group">
                    <input type="text" name="kata_kunci" class="form-control" placeholder="Masukkan No SJ pencarian" id="kata_kunci" value="<?php echo $this->input->get('kata_kunci')?>">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /Form Pencarian -->
    </div>
    <!-- /Paging dan form pencarian -->

    <?php if (!empty($makloon_pengadaan_baju_wip) && is_array($makloon_pengadaan_baju_wip)): ?>
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>NO SJ</th>
                    <th>Tanggal SJ</th>
                    <th>Jenis Masuk</th>
                     <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
                foreach($makloon_pengadaan_baju_wip as $row):

				echo form_hidden('id', $row->id) ;

				echo form_hidden('id_unit_jahit', $row->id_unit_jahit) ;
                 $link_edit = anchor('makloon_pengadaan_baju_wip/makloon_pengadaan_baju_wip/edit/'.$row->id, '<span class="glyphicon glyphicon-edit"></span>', array('title' => 'Edit'));
                 $link_hapus = anchor('makloon_pengadaan_baju_wip/makloon_pengadaan_baju_wip/hapus/'.$row->id.'/'.$row->id_gudang.'/'.$row->id_unit_jahit,'<span class="glyphicon glyphicon-trash"></span>', array('title' => 'Hapus', 'data-confirm' => 'Anda yakin akan menghapus data ini?'));
                 $link_cetak = anchor('makloon_pengadaan_baju_wip/makloon_pengadaan_baju_wip/cetak/'.$row->id,'<span class="glyphicon glyphicon-print"></span>', array('title' => 'Cetak'));
                 ?>
                    <tr>
						
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $row->no_sj ?></td>
						<td><?php echo $row->tanggal_sj ?></td>
						<td><?php if($row->jenis_masuk == 1)
							echo "Masuk Bagus" ;
							elseif ($row->jenis_masuk == 2)
							echo "Keluar Lain-lain" ;
							
               
						 ?></td>
						 
						 <td><?php echo $row->keterangan ?></td>    
                        <td>
                            <?php echo $link_edit.'&nbsp;&nbsp;&nbsp;&nbsp;'.$link_hapus  ?>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
	
	<div class="col-sm-offset-5">
    
                    <div class="btn-group" >
                     <?php echo anchor('/makloon_pengadaan_baju_wip/makloon_pengadaan_baju_wip/', 'Tambah Item', 'class="btn btn-warning " role="button" '); ?>
                    </div>
           
        </div>
	
        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $makloon_pengadaan_baju_wip ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



