<!--
narr = nama array,
cpelanggan = counter pelanggan
-->
<script type="text/javascript" language="javascript">

function fopdropforcast() {
	if(document.getElementById('f_op_dropforcast').checked==true) {
		document.getElementById('f_op_dropforcast').value = 1;
	}else{
		document.getElementById('f_op_dropforcast').value = 0;
	}
	
}

function validNum(iterasi) {
	var angka	= document.getElementById('v_count_tblItem_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka. Terimakasih.");
		angka.value	= 0;
		angka.focus();
	}
}

function validStok(iterasi) {
	var qty		= document.getElementById('i_qty_hidden_tblItem_'+iterasi);
	var masukan	= document.getElementById('v_count_tblItem_'+iterasi);
	var xx	= masukan.value;
	if(masukan.value > parseInt(qty.value)) {
		alert("Maaf, quantity melebihi stok yg ada! Terimakasih.");
		masukan.value	= xx;
	}
}

function cItem(nItem) {
	if(!(document.getElementById('ajax_v_count_'+nItem+'_'+'0'))) {
		alert('Maaf, item OP harus terisi. Terimakasih.');
		return false;
	}
}

function getCabang(nilai) {
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listop/cform/cari_cabang');?>",
	data:"ibranch="+nilai,
	success: function(data) {
	$("#cboPelanggan").html(data);

	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function ckop(nomor){
	var i = document.getElementById('i_op_code_hidden').value;
	var icustomer	= document.getElementById('i_customer').value;
	
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listop/cform/cari_op');?>",
	data:"nop="+nomor+"&i="+i+"&icustomer="+icustomer,
	success: function(data){
		$("#confnomorop").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function cksop(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listop/cform/cari_sop');?>",
	data:"nsop="+nomor,
	success: function(data){
		$("#confnomorsop").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function showData(nItem) {
	if (document.getElementById(nItem+'_select').style.display=='none') {			
		document.getElementById(nItem+'_select').style.display='block';		
		document.getElementById(nItem+'_select').style.position='absolute';	
	} else {
		document.getElementById(nItem).style.display='block';
		document.getElementById(nItem+'_select').style.display='none';
	} 
}
  
function hideData(nItem){
}

function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	var kelompok;
	tbl.width='100%';
	
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	}else{
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:20px;margin-right:0px;\">"+(iteration*1+1)+".</div>";
	/* 19052011
	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_v_count_"+nItem+"_"+iteration+"\" style=\"width:74px;\"><input type=\"text\" ID=\"v_count_"+nItem+"_"+iteration+"\"  name=\"v_count_"+nItem+"_"+iteration+"\" style=\"width:94px;text-align:right;\" onkeyup=\"validNum('"+iteration+"'); validStok('"+iteration+"');\"></DIV>";
	*/
	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_v_count_"+nItem+"_"+iteration+"\" style=\"width:74px;\"><input type=\"text\" ID=\"v_count_"+nItem+"_"+iteration+"\"  name=\"v_count_"+nItem+"_"+iteration+"\" style=\"width:94px;text-align:right;\" onkeyup=\"validNum('"+iteration+"'); \"></DIV>";	
	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:130px;\" ><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:100px;\" onclick=\"shprodukop('"+iteration+"');\"><img name=\"img_i_product_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukop('"+iteration+"');\"></DIV>" +
	"<div id=\"ajax_i_product_"+nItem+"_"+iteration+"_select\" style=\"display:none;\">" +
	"</div>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:170px;\"><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:220px;\"></DIV>";

	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_e_note_"+nItem+"_"+iteration+"\" style=\"width:152px;\"><input type=\"text\" ID=\"e_note_"+nItem+"_"+iteration+"\"  name=\"e_note_"+nItem+"_"+iteration+"\" style=\"width:204px;\"><input type=\"hidden\" name=\"iteration\" value=\""+iteration+"\"><input type=\"hidden\" name=\"i_qty_hidden_"+nItem+"_"+iteration+"\" id=\"i_qty_hidden_"+nItem+"_"+iteration+"\"></DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dsg.js"></script>
<script src="<?php echo base_url();?>asset/javascript/jquery-1.4.3.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
	  <td class="tcat"><?php echo $page_title; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
	 <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title; ?></td></tr>
	<tr>
	 <td class="alt2" style="padding:0px;">
		<table id="table-add-box">
		  <tr>
			<td align="left">
				  <?php 
				  	$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listop/cform/actedit', $attributes);?>
			
				<div id="masteropform">
				<table border="0">
				<tr>
				  <td width="16%"><?php echo $form_nomor_op; ?></td>
				  <td width="1%">:</td>
				  <td width="25%">
					<?php
					 $noop = array(
						'name'=>'i_op',
						'id'=>'i_op',
						'value'=>$no,
						'maxlength'=>'9',
						'onkeyup'=>'ckop(this.value)'
						);
					 echo form_input($noop);
					?>
					<div id="confnomorop" style="color:#FF0000;"></div>
				  </td>
				  <td width="12%"><?php echo $form_cabang_op; ?></td>
				  <td width="1%">:</td>
				  <td width="16%">
					<?php
					$lpelanggan	.= "<select name=\"i_customer\" id=\"i_customer\" onchange=\"getCabang(this.value);\" disabled >"; 
					  $lpelanggan	.= "<option value=\"\">[".$form_option_pel_op."]</option>";
					  foreach($opt_pelanggan as $key => $row) {	
					  	$Sel	= $icust==$row->code?"selected":"";
						$lpelanggan .= "<option value=\"$row->code\" $Sel>".$row->customer."</option>";
					  }
					$lpelanggan	.= "</select>";
					echo $lpelanggan;
					?>
				  </td>
				  <?php //echo "<td width=\"%\" id=\"cboPelanggan\"></td>"; ?>
				  <td>
				  <select name="i_branch" id="i_branch">
				  	<?php
					$lcabang	= "";
					foreach($opt_cabang as $row2) {
						$Sel2	= $ibranch==$row2->ibranchcode?"selected":"";
						$lcabang .= "<option value=\"$row2->ibranchcode\" $Sel2 >".$row2->ebranchname."</option>";	
					}
					echo $lcabang;
					?>
				  </select>
				  </td>
				</tr>
				<tr>
				  <td><?php echo $form_tgl_op; ?></td>
				  <td>:</td>
				  <td>
					<?php
					 $tglop = array(
						'name'=>'d_op',
						'id'=>'d_op',
						'value'=>$tgOP,
						'maxlength'=>'10'
						);
					 echo form_input($tglop);
					?>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op,'dd/mm/yyyy',this)">
				  </td>
				  <td><?php echo $form_tgl_bts_kirim_op; ?></td>
				  <td>:</td>
				  <td>
					<?php

					 $tgldeliverylimit = array(
						'name'=>'d_delivery_limit',
						'id'=>'d_delivery_limit',
						'value'=>$tBtsKirim,
						'maxlength'=>'10'
						);
					 echo form_input($tgldeliverylimit);
					?>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_delivery_limit,'dd/mm/yyyy',this)">	
				  </td>
				  <td></td>
				</tr>
				<tr>
				  <td><?php echo "OP Drop Forcast"; ?></td>
				  <td>:</td>
				  <td><input type="checkbox" id="f_op_dropforcast" name="f_op_dropforcast" value="<?=(($f_op_dropforcast=='t')?"1":"0")?>" <? if($f_op_dropforcast=='t'){ echo "checked"; } ?> onclick="fopdropforcast();"/><?php echo "Ya"; ?></td>
				  <td><?php echo $form_nomor_sop_op; ?></td>
				  <td>:</td>
				  <td>
					<?php
					 $nosop = array(
						'name'=>'i_sop',
						'id'=>'i_sop',
						'value'=>$sop,
						'maxlength'=>'50',
						'readonly'=>'true',
						'onkeyup'=>'cksop(this.value)'
						);
					 echo form_input($nosop);
					?>
					<div id="confnomorsop" style="color:#FF0000;"></div>
				  </td>
				  <td></td>
				</tr>
				
				<tr>
				  <td><?php echo "Jenis OP"; ?></td>
				  <td>:</td>
				  <td><select name="jenis_op" id="jenis_op">
						<option value="0" <?php if ($jenis_op == '0') { ?>selected<?php } ?> >-- PILIH --</option>
						<option value="1" <?php if ($jenis_op == '1') { ?>selected<?php } ?> >Stok</option>
						<option value="2" <?php if ($jenis_op == '2') { ?>selected<?php } ?> >Konsinyasi</option>
						<option value="3" <?php if ($jenis_op == '3') { ?>selected<?php } ?> >Toko</option>
					</select>
				  </td>
				  
				</tr>
				
				<tr>
				 <td colspan="7"><div id="light"></div></td>	
				</tr>
				<tr>
				 <td colspan="7">
				  <div id="title-box2"><?php echo $form_title_detail_op; ?>
				  <div style="float:right;">
				  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
				  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
				  </div></div>
 				 </td>	
				</tr>
				<tr>
				 <td colspan="7">
					<table width="90%" border="0">
						<tr>
						 <td width="6%" class="tdatahead">NO</td>	
						 <td width="14%" class="tdatahead"><?php echo $form_jml_op; ?></td>
						 <td width="21%" class="tdatahead"><?php echo $form_kode_produk_op; ?></td>
						 <td width="30%" class="tdatahead"><?php echo $form_nm_produk_op; ?></td>
						 <td width="29%" class="tdatahead"><?php echo $form_ket_op; ?></td>
						</tr>
						<tr>
						 <td colspan="5">
						 	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
						 	<?php
							$iter	= 0;
							foreach($opitem as $row3) {
								echo "
								<tr>
								<td><DIV style=\"font:11px;text-align:right;width:24px;margin-right:0px;\">".($iter*1+1)."</DIV></td>
								<td><DIV ID=\"ajax_v_count_tblItem_".$iter."\" style=\"width:74px;\"><input type=\"text\" ID=\"v_count_tblItem_".$iter."\"  name=\"v_count_tblItem_".$iter."\" style=\"width:94px;text-align:right;\" onkeyup=\"validNum('".$iter."'); validStok('".$iter."');\" value=\"".$row3->qty."\"></DIV></td>
								<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:130px;\" ><input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:100px;\" onclick=\"shprodukop('".$iter."');\" value=\"".$row3->iproduct."\"><img name=\"img_i_product_tblItem_".$iter."\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukop('".$iter."');\"></DIV><DIV id=\"ajax_i_product_tblItem_".$iter."_select\" style=\"display:none;\"></DIV></td>
								<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:170px;\"><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\"  name=\"e_product_name_tblItem_".$iter."\" style=\"width:220px;\" value=\"".$row3->productname."\"></DIV></td>
								<td><DIV ID=\"ajax_e_note_tblItem_".$iter."\" style=\"width:152px;\"><input type=\"text\" ID=\"e_note_tblItem_".$iter."\"  name=\"e_note_tblItem_".$iter."\" style=\"width:204px;\" value=\"".$row3->e_note."\"><input type=\"hidden\" name=\"iteration\" value=\"".$iter."\"><input type=\"hidden\" name=\"i_qty_hidden_tblItem_".$iter."\" id=\"i_qty_hidden_tblItem_".$iter."\"  value=\"".$row3->qty_akhir."\"></DIV></td>
								</tr>";
								$iter++;
							}
							?>
						 	</table>
						 </td>
						</tr>
					</table>
				 </td>
				</tr>
				<tr>
				  <td colspan="7" align="right">
				  	<input type="hidden" name="i_customer_hidden" id="i_customer_hidden" value="<?=$icust?>" >
					<input type="hidden" name="i_op_hidden" id="i_op_hidden" value="<?=$iop?>" >
					<input type="hidden" name="i_op_code_hidden" id="i_op_code_hidden" value="<?=$no?>" >
					<input type="hidden" name="i_sop_hidden" id="i_sop_hidden" value="<?=$sop?>" >
					<input name="btnkoreksi" type="button" id="btnkoreksi" value="<?php echo "Koreksi Nilai Sisa Pemenuhan"; ?>" onclick="show('listop/cform/koreksijmlorder/<?=$iop?>/<?=$no?>/<?=$icust?>/','#content')" />
					<input name="btnbatal" id="btnbatal" value="<?php echo $button_update; ?>" type="submit">
					<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick='show("listop/cform/","#content")'>
				  </td>
				</tr>
				</table>
			  	</div>
			<?php echo form_close(); ?>
			</td>
		  </tr>
		</table>

	 </td>
	</tr>
</table>
