<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_op; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_op; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listop/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlopbrgform">

		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_tgl_mulai_op; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					  <input name="d_op_first" type="text" id="d_op_first" maxlength="10" value="<?php echo $tglopmulai; ?>" />
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_first,'dd/mm/yyyy',this)">
					<?php echo $list_tgl_akhir_op; ?> 
					<input name="d_op_last" type="text" id="d_op_last" maxlength="10" value="<?php echo $tglopakhir; ?>" />
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_last,'dd/mm/yyyy',this)"></td>
				  </tr>
				  <tr>
					<td width="33%"><?php echo $list_no_op; ?></td>
					<td width="0%">:</td>
					<td>
					<?php
					$nomor_op	= array(
					'name'=>'nomor_op',
					'id'=>'nomor_op',
					'type'=>'text',
					'value'=>$nomorop,
					'maxlength'=>'14'
					);
					echo form_input($nomor_op);
					?>
					</td>
				  </tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_op; ?></div></td>	
			</tr>
			<tr>
			  <td>
			  <?php if($flag=='f') { ?>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="2%" class="tdatahead">NO</td>
				  <td width="10%" class="tdatahead"><?php echo $list_no_op; ?></td>
				  <td width="25%" class="tdatahead"><?php echo $list_nm_cab_op; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_kd_brg_op; ?></td>
				  <td width="30%" class="tdatahead"><?php echo $list_nm_brg_op; ?> </td>
				  <td width="4%" class="tdatahead"><?php echo $list_qty_op; ?> </td>
				  <td width="4%" class="tdatahead"><?php echo $list_sisa_order_op; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_limit_date_op; ?> </td>
				  <td width="10%" class="tdatahead"><?php echo $list_no_sop_op; ?> </td>
				  <td width="40%" class="tdatahead"><?php echo $link_aksi; ?> </td>
				</tr>
				
				<?php
						
				$total	= 0;
				
				if(sizeof($isi_f) > 0) {
					
					$arr= 0;
					$no	= 1;
					$cc	= 1;
					
					$nomororder	= array();
					$db2=$this->load->database('db_external', TRUE);
					foreach($isi_f as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
						
						$jmlop	= $this->mclass->jmlop($row->iopcode);
						$njmlop	= $jmlop->num_rows();
						
						$query	= $this->mclass->cekstatusdo($row->iopcode);
						
						if($query->num_rows()>0) {
							
							$rdiDO	= $query->row();
							
							$query2 = $this->mclass->cekstatusdo2($rdiDO->i_op);
							$num2	= $query2->num_rows();
							$j2		= 0;
							
							if($num2>0) {
								//$diDO2 = $query2->num_rows();
								foreach($query2->result() as $rdiDO2) {
									$q = $db2->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$rdiDO->i_op' AND i_product='$rdiDO2->i_product' ");
									$r = $q->row();
									if($rdiDO2->n_count==$r->n_residual) {
											$j2+=1;
									}
								}
								if($num2==$j2) { // belum ada do
									$diDO2 = 0;
								}else{
									$diDO2 = $num2;
								}
							}else{ // tdk akan pernah masuk ke blok ini
								$diDO2 = 0;
							}
							
						}else{
							$query	= $this->mclass->cekstatusdo3($row->iopcode);
							
							if($query->num_rows()>0) {
								$rdiDO	= $query->row();
								//
								$query2 = $this->mclass->cekstatusdo2($rdiDO->i_op);
								$num2	= $query2->num_rows();
								$j2		= 0;
								
								if($num2>0) {
									//$diDO2 = $query2->num_rows();
									foreach($query2->result() as $rdiDO2) {
										$q = $db2->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$rdiDO->i_op' AND i_product='$rdiDO2->i_product' ");
										$r = $q->row();
										if($rdiDO2->n_count==$r->n_residual) {
												$j2+=1;
										}
									}
									if($num2==$j2) { // belum ada do
										$diDO2 = 0;
									}else{
										$diDO2 = $num2;
									}
								}else{ // tdk akan pernah masuk ke blok ini
									$diDO2 = 0;
								}								
								//
							}else{ // tdk akan pernah masuk ke blok ini
								$diDO2 = 0;
							}
							
						}
						
						$nomororder[$arr]	= $row->iopcode;
						
						if($cc==1) {
							if($diDO2==0 && $row->f_op_cancel=='f') {
								$link_act	= "<td class=\"tdatahead2\" rowspan='$njmlop' valign='top'>
								<a href=".base_url()."index.php/listop/cform/edit/".$row->iopcode."/".$icustomer."
								 title=\"Edit Order Pembelian (OP)\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Order Pembelian (OP)\"></a>&nbsp;
								<a href=".base_url()."index.php/listop/cform/undo/".$row->iopcode."/".$icustomer."
								
								 title=\"Cancel Order Pembelian (OP)\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Order Pembelian (OP)\"></a>
								</td>";
							}else{
								$link_act	= "<td class=\"tdatahead2\" rowspan='$njmlop' valign='top'></td>";
							}
						}elseif($cc>1 && $nomororder[$arr-1]!=$row->iopcode) {
							if($diDO2==0 && $row->f_op_cancel=='f'){
								$link_act	= "<td class=\"tdatahead2\" rowspan='$njmlop' valign='top'>
								<a href=".base_url()."index.php/listop/cform/edit/".$row->iopcode."/".$icustomer."
								
								 title=\"Edit Order Pembelian (OP)\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Order Pembelian (OP)\"></a>&nbsp;
								<a href=".base_url()."index.php/listop/cform/undo/".$row->iopcode."/".$icustomer."
								 
								title=\"Cancel Order Pembelian (OP)\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Order Pembelian (OP)\"></a>
								</td>";
							}else{
								$link_act	= "<td class=\"tdatahead2\" rowspan='$njmlop' valign='top'></td>";
							}
						}else{
							if($diDO2==0 && $row->f_op_cancel=='f') {
								$link_act	= '';
							}else{
								$link_act	= '';
							}
						}
					
						$lopbrg	.= "
							<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\" >
							  <td class=\"tdatahead2\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
							  <td class=\"tdatahead2\">".$row->iopcode."</td>
							  <td class=\"tdatahead2\">".$row->ebranchname."</td>
							  <td class=\"tdatahead2\">".$row->iproduct."</td>
							  <td class=\"tdatahead2\">".$row->motifname."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->qty."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->sisaorder."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->deliver."</td>
							  <td class=\"tdatahead2\">".$row->isop."</td>
							  ".$link_act."
							</tr>";
							
							$no+=1; $cc+=1;
							$arr+=1;
							
					}
				}
				?>
				
				<?php
				echo $lopbrg;
				?>
				
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table>
			  
			  <?php } elseif($flag=='t') { ?>
			  
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">No</td>
					<td width="12%" class="tdatahead">Kode OP </td>
					<td width="11%" class="tdatahead">Tanggal OP </td>
					<td width="35%" class="tdatahead">Pelanggan Cabang </td>
					<td width="10%" class="tdatahead">Status OP </td>
					<td width="10%" class="tdatahead">Status DO </td>
					<td width="5%" class="tdatahead">Aksi</td>
				  </tr>
				  
				  <?php
				  
					$total	= 0;	
								
					if(sizeof($isi_t)>0) {
						$db2=$this->load->database('db_external', TRUE);
						$arr= 0;
						$no	= 1;
						$cc	= 1;
						$nomororder	= array();
						$lopbrg	= "";
						
						foreach($isi_t as $row) {
	
							$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
							$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
							
							$jmlop	= $this->mclass->jmlop($row->iopcode);
							$njmlop	= $jmlop->num_rows();
							
							/* 27022012
							$query	= $this->mclass->cekstatusdo($row->iopcode);
							if($query->num_rows()>0){
								$diDO	= $query->num_rows();
							}else{
								$diDO	= 0;
							}
							*/

							//
							$query	= $this->mclass->cekstatusdo($row->iopcode);
							
							if($query->num_rows()>0) {
								
								$rdiDO	= $query->row();
								
								$query2 = $this->mclass->cekstatusdo2($rdiDO->i_op);
								$num2	= $query2->num_rows();
								$j2		= 0;
								
								if($num2>0) {
									//$diDO2 = $query2->num_rows();
									foreach($query2->result() as $rdiDO2) {
										$q = $db2->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$rdiDO->i_op' AND i_product='$rdiDO2->i_product' ");
										$r = $q->row();
										if($rdiDO2->n_count==$r->n_residual) {
												$j2+=1;
										}
									}
									if($num2==$j2) { // belum ada do
										$diDO2 = 0;
									}else{
										$diDO2 = $num2;
									}
								}else{ // tdk akan pernah masuk ke blok ini
									$diDO2 = 0;
								}
								
							}else{
								$query	= $this->mclass->cekstatusdo3($row->iopcode);
								
								if($query->num_rows()>0) {
									$rdiDO	= $query->row();
									//
									$query2 = $this->mclass->cekstatusdo2($rdiDO->i_op);
									$num2	= $query2->num_rows();
									$j2		= 0;
									
									if($num2>0) {
										//$diDO2 = $query2->num_rows();
										foreach($query2->result() as $rdiDO2) {
											$q = $db2->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$rdiDO->i_op' AND i_product='$rdiDO2->i_product' ");
											$r = $q->row();
											if($rdiDO2->n_count==$r->n_residual) {
													$j2+=1;
											}
										}
										if($num2==$j2) { // belum ada do
											$diDO2 = 0;
										}else{
											$diDO2 = $num2;
										}
									}else{ // tdk akan pernah masuk ke blok ini
										$diDO2 = 0;
									}								
									//
								}else{ // tdk akan pernah masuk ke blok ini
									$diDO2 = 0;
								}
								
							}							
							//
						
							if($diDO2==0 && $row->f_op_cancel=='f') {
								$link_act	= "<td class=\"tdatahead2\" valign='top'>
								<a href=".base_url()."index.php/listop/cform/edit/".$row->iopcode."/".$icustomer." title=\"Edit Order Pembelian (OP)\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Order Pembelian (OP)\"></a>&nbsp;
								<a href=".base_url()."index.php/listop/cform/undo/".$row->iopcode."/".$icustomer." title=\"Cancel Order Pembelian (OP)\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Order Pembelian (OP)\"></a>
									</td>";
							}else{
								$link_act	= "<td class=\"tdatahead2\" valign='top'></td>";
							}
	  
							$batal		= $row->f_op_cancel=='f'?'':'Batal';	
							$createdo	= $row->f_do_created=='t'?'Ya':'Tidak';

							$exp_dop	= explode("-",$row->dop,strlen($row->dop)); // YYYY-mm-dd
							$dop		= $exp_dop[2]."/".$exp_dop[1]."/".$exp_dop[0];
						  						  
							$lopbrg	.=
							  "<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
					 onMouseOut=\"this.className='$Classnya'\">
								<td class=\"tdatahead2\" bgcolor=\"$bgcolor\">{$cc}</td>
								<td class=\"tdatahead2\">{$row->iopcode}</td>
								<td class=\"tdatahead2\">{$dop}</td>
								<td class=\"tdatahead2\">{$row->ebranchname}</td>
								<td class=\"tdatahead2\">".$batal."</td>
								<td class=\"tdatahead2\">".$createdo."</td>
								".$link_act."
							  </tr>";
						  
							$no+=1; $cc+=1;
							$arr+=1;
							
					}
					
					echo $lopbrg;
					
				}
				
				?>
			  </table>
			  <?php } ?>
			  </td>
			</tr>
			<tr><td align="center"><?php if($flag=='t') echo $create_link; ?></td></tr>
			<tr>
			  <td><div style="border-top:#E4E4E4 0px solid; border-bottom:#E4E4E4 0px solid; border-left:#E4E4E4 0px solid; border-right:#E4E4E4 0px solid; color:#990000; width:245px;">Order Pembelian (OP) yg telah di DO-kan, <br />tdk dapat diubah atau dibatalkan!</div></td>
			</tr>			
			<tr>
			  <td align="right">
			  <?php if($flag=='f') { ?>
			  <input name="btnkoreksi" type="button" id="btnkoreksi" value="<?php echo "Koreksi Nilai Sisa Pemenuhan"; ?>" onclick="show('listop/cform/koreksijmlorder2/<?=$nomorop?>/<?=$icustomer?>','#content')" />
			  <span style="color:#FF0000;">*</span>&nbsp;&nbsp;&nbsp;&nbsp;
			  <?php } ?>
			  <input name="btnkeluar" id="btnkeluar"  value="<?php echo $button_keluar; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listop/cform/'">
			  
	</td>
			</tr>
			<?php if($flag=='f') { ?>
			<tr><td><div style="border-top:#E4E4E4 0px solid; border-bottom:#E4E4E4 0px solid; border-left:#E4E4E4 0px solid; border-right:#E4E4E4 0px solid; color:#990000; width:245px;">*) Jika nilai sisa order minus (ada selisih dgn DO)</div></td></tr>
			<?php } ?>
		  </table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
