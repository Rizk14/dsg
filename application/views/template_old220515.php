<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>CV Duta Setia Garmen</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Tom@Lwis (http://www.lwis.net/free-css-drop-down-menu/)" />
<meta name="keywords" content=" css, dropdowns, dropdown menu, drop-down, menu, navigation, nav, horizontal, vertical left-to-right, vertical right-to-left, horizontal linear, horizontal upwards, cross browser, internet explorer, ie, firefox, safari, opera, browser, lwis" />
<meta name="description" content="Clean, standards-friendly, modular framework for dropdown menus" />
<link href="<? echo base_url(); ?>css/dropdown/themes/default/helper.css" media="screen" rel="stylesheet" type="text/css" />

<!-- Beginning of compulsory code below -->

<link href="<? echo base_url(); ?>css/dropdown/dropdown.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<? echo base_url(); ?>css/dropdown/themes/default/default.ultimate.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<? echo base_url(); ?>css/style.css" media="screen" rel="stylesheet" type="text/css" />
	
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.4.4.min.js"></script>

<!--[if lt IE 7]>
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<!-- / END -->

</head>
<body>

<h1>Sistem Informasi Duta</h1>

<ul id="nav" class="dropdown dropdown-horizontal" nowrap>
	<li><a href="<? echo base_url(); ?>index.php">Home</a></li>
	<?php if ($this->session->userdata('gid') == 1) { ?>
		<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-item/cform">Item</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<? echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-bb-sup/cform/view">Harga Bahan Baku/Pembantu Berdasarkan Supplier</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-hrg-quilting/cform/view">Harga Bahan Quilting Berdasarkan Unit Makloon</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>
				<li><span class="dir">Produksi</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-marker/cform/view">Marker Gelaran/Set</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-ukuran-bisbisan/cform">Ukuran Bis-Bisan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">Kebutuhan Bis-Bisan Per Pcs</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-bagian-brg-jadi/cform">Bagian Dari Brg Jadi Utk Schedule Cutting Dacron</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-motif-brg-jadi/cform">Motif Brg Jadi</a></li>
					</ul>
				</li>
				<li><span class="dir">Gudang</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-lok-gudang/cform">Master Lokasi Gudang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-gudang/cform">Master Gudang</a></li>
					</ul>
				</li>
				<li><span class="dir">Unit Luar</span>
					<ul>
					<!--	<li><a href="<? echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->
						<li><a href="<? echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
		<!--		<li><a href="<? echo base_url(); ?>index.php/mst-bb/cform/view">Bahan Baku</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-acc/cform/view">Accessories</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-perl/cform/view">Alat Perlengkapan</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-bp/cform/view">Bahan Pendukung</a></li> -->
			<!--	<li><a href="<? echo base_url(); ?>index.php/mst-makloon/cform">Jenis Makloon</a></li> -->
			
				<li><span class="dir">WIP</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/view">Barang Jadi WIP</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) Jahit</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewwarnabrgjadi">Warna Barang Jadi WIP</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi">Material Barang Jadi</a></li>
					</ul>
				</li>
				
				<li><a href="<? echo base_url(); ?>index.php/mst-department/cform">Departemen</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-supplier/cform/view">Supplier</a></li>
				
			</ul>
		</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 2) { ?>
		<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-item/cform">Item</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<? echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-bb-sup/cform/view">Harga Bahan Baku/Pembantu Berdasarkan Supplier</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-hrg-quilting/cform/view">Harga Bahan Quilting Berdasarkan Unit Makloon</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>
				<li><span class="dir">Gudang</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-lok-gudang/cform">Master Lokasi Gudang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-gudang/cform">Master Gudang</a></li>
					</ul>
				</li>
				<li><span class="dir">Unit Luar</span>
					<ul>
					<!--	<li><a href="<? echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->
						<li><a href="<? echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
				<li><a href="<? echo base_url(); ?>index.php/mst-department/cform">Departemen</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-supplier/cform/view">Supplier</a></li>
				
			</ul>
		</li>
	<?php } ?>
	
	<!-- 15-09-2014 adopt from tirai -->
	<?php if ($this->session->userdata('gid') == 3) { ?>
		<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-item/cform">Item</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<? echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><span class="dir">Gudang</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-lok-gudang/cform">Master Lokasi Gudang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-gudang/cform">Master Gudang</a></li>
					</ul>
				</li>
				<li><a href="<? echo base_url(); ?>index.php/mst-department/cform">Departemen</a></li>				
			</ul>
		</li>
	<?php } ?>
	<!-- -->
	
	<?php if ($this->session->userdata('gid') == 4 || $this->session->userdata('gid') == 5) { ?>
		<li><span class="dir">Master</span>
			<ul>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>
				<li><span class="dir">Produksi</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-marker/cform/view">Marker Gelaran/Set</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-ukuran-bisbisan/cform">Ukuran Bis-Bisan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">Kebutuhan Bis-Bisan Per Pcs</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-bagian-brg-jadi/cform">Bagian Dari Brg Jadi Utk Schedule Cutting Dacron</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-motif-brg-jadi/cform">Motif Brg Jadi</a></li>
					</ul>
				</li>
			</ul>
		</li>
	<?php } ?>
	
	<?php // staf WIP / PIC
	if ($this->session->userdata('gid') == 6) { ?>
		<li><span class="dir">Master</span>
			<ul>
			<li><a href="<? echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
			<li><span class="dir">Unit Luar</span>
					<ul>
					<!--	<li><a href="<? echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->
						<li><a href="<? echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-unit-jahit/cform/viewgrupjahit">Grup Jahit di Unit Jahit</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
			<!--<li><a href="<? echo base_url(); ?>index.php/syncbrgjadi/cform">Sinkronisasi Barang Jadi</a></li> -->
			<li><span class="dir">WIP</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/view">Barang Jadi WIP</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/kelbrgjadi">Kelompok Barang Jadi</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) WIP</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewwarnabrgjadi">Warna Barang Jadi WIP</a></li>
						<li><a href="<? echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi">Material Barang Jadi</a></li>
					</ul>
				</li>
			</ul>
		</li>
	<?php } ?>
	
<!--	<li><span class="dir">PP</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/pp-bb/cform/view">Bahan Baku</a></li>
			<li><a href="<? echo base_url(); ?>index.php/pp-asesoris/cform/view">Accessories</a></li>
			<li><a href="<? echo base_url(); ?>index.php/pp-bp/cform/view">Bahan Pendukung</a></li>
			<li><a href="<? echo base_url(); ?>index.php/pp-perl/cform/view">Alat Perlengkapan</a></li>
		</ul>
	</li> -->
	
	<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2) { ?>
	<li><span class="dir">Pembelian</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/pp-new/cform/view">Permintaan Pembelian (PP)</a></li>
			<li><a href="<? echo base_url(); ?>index.php/op/cform/view">Order Pembelian (OP)</a></li>
			<!-- <li>Order Pembelian (OP)</li> -->
			<li><a href="<? echo base_url(); ?>index.php/faktur-bb/cform/view">Bukti Penerimaan Barang</a></li>
			<li><a href="<? echo base_url(); ?>index.php/bonmmasuk/cform/view">Update Stok (Bon M Masuk) Hasil Pembelian</a></li>
			
		<!-- Ini disiapkan jika 1 faktur penjualan utk beberapa SJ	-->
		<li><a href="<? echo base_url(); ?>index.php/faktur-jual/cform/view">Faktur Pembelian</a></li>
		
		<!--	<li><a href="<? echo base_url(); ?>index.php/faktur-bb/cform/view2">Input No Faktur Pembelian</a></li> -->
			<li><a href="<? echo base_url(); ?>index.php/faktur-pajak/cform/view">Faktur Pajak</a></li>
			<li><a href="<? echo base_url(); ?>index.php/retur-beli/cform/view">Nota Debet Retur</a></li>
			<li><a href="<? echo base_url(); ?>index.php/retur-beli/cform/viewnotaretur">Nota Retur</a></li>
			<!-- sementara dinonaktifkan dulu <li><a href="<? echo base_url(); ?>index.php/faktur-makloon/cform/view">Input Faktur Jasa Makloon</a></li> -->
			<li><a href="<? echo base_url(); ?>index.php/sj-masuk-makloon/cform/view">SJ Masuk Hasil Quilting</a></li>
			<li><a href="<? echo base_url(); ?>index.php/faktur-quilting/cform/view">Faktur Quilting</a></li>
			
			<li><a href="<? echo base_url(); ?>index.php/pembelian/cform/saldoawalhutang">Input Saldo Awal Hutang Dagang</a></li>
		<!--	sementara dinonaktifkan dulu
		<li><a href="<? echo base_url(); ?>index.php/set-stok-harga/cform/">Set Stok Awal Berdasarkan Harga Pembelian</a></li> -->
			
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 3) { // 20-11-2012. update 01-04-2015 ga usah ?>
	<!--<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<? echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-item/cform">Item</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
						<li><a href="<? echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<? echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<? echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>				
			</ul>
		</li> -->
	
	<!--<li><span class="dir">Pembelian</span>
		<ul>

			<li><a href="<? echo base_url(); ?>index.php/faktur-bb/cform/view">Bukti Penerimaan Barang</a></li>
			
		</ul>
	</li> -->
	<?php } ?>
	
	<!-- 15-09-2014 admin -->
	<?php if ($this->session->userdata('gid') == 1) { ?>
	<li><span class="dir">Gudang</span>
		<ul>
			<li><span class="dir">Bon M Masuk</span>
				<ul>
					<!--<li><a href="<? echo base_url(); ?>index.php/bonmmasuk-hsl-cutting/cform/view">Hasil Cutting</a></li>-->
					<li><a href="<? echo base_url(); ?>index.php/bonmmasuk-hsl-cutting/cform/viewbonm">Hasil Cutting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/bonmmasuk/cform/view">Hasil Pembelian</a></li>
				<!--	<li><a href="<? echo base_url(); ?>index.php/bonmmasukkredit/cform/view">Hasil Pembelian Untuk Stok Lain-lain</a></li> -->
					<li><a href="<? echo base_url(); ?>index.php/bonmmasukquilting/cform/view">Hasil Pembelian Bahan Quilting</a></li>
				<!--	<li><a href="<? echo base_url(); ?>index.php/bonmmasukcash/cform/view">Revisi Tipe Stok Untuk Pembelian Cash Lain-lain</a></li> -->
					<li><a href="<? echo base_url(); ?>index.php/bonmmasuklain/cform/view">Masuk Lain-Lain</a></li>
				</ul>
			</li>
			<li><span class="dir">Bon M Keluar</span>
				<ul>
				<!--	<li><a href="<? echo base_url(); ?>index.php/bonm-bbc/cform/view">Pemenuhan Bahan Baku Dari Permintaan Bhn Baku</a></li> -->
					<li><a href="<? echo base_url(); ?>index.php/bonmkeluar/cform/view">Pengeluaran Bahan Baku/Pembantu</a></li>
				</ul>
			</li>
			
			<!--<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawal">Input Stok Awal WIP (hasil jahit)</a></li>-->
			<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawal">Input Stok Awal WIP (hasil jahit)</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawalunit">Input Stok Awal WIP di Unit Jahit</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/cgudang/konversistok">Konversi Stok Barang WIP</a></li>
			
			<li><span class="dir">Input Stok Opname</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname-quilting/cform/">Bahan Quilting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/gudangwip/cform/sounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
				</ul>
			</li>
			
			<li><span class="dir">Approval Stok Opname</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-quilting/cform/">Bahan Quilting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/gudangwip/cform/approvalsounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
					
				</ul>
			</li>

		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 3) { ?>
	<li><span class="dir">Gudang</span>
		<ul>
			<li><span class="dir">Bon M Masuk</span>
				<ul>
					<!--<li><a href="<? echo base_url(); ?>index.php/bonmmasuk-hsl-cutting/cform/view">Hasil Cutting</a></li>-->
					
					<li><a href="<? echo base_url(); ?>index.php/bonmmasuk/cform/view">Hasil Pembelian</a></li>
				<!--	<li><a href="<? echo base_url(); ?>index.php/bonmmasukkredit/cform/view">Hasil Pembelian Untuk Stok Lain-lain</a></li> -->
					<li><a href="<? echo base_url(); ?>index.php/bonmmasukquilting/cform/view">Hasil Pembelian Bahan Quilting</a></li>
				<!--	<li><a href="<? echo base_url(); ?>index.php/bonmmasukcash/cform/view">Revisi Tipe Stok Untuk Pembelian Cash Lain-lain</a></li> -->
					<li><a href="<? echo base_url(); ?>index.php/bonmmasuklain/cform/view">Masuk Lain-Lain</a></li>
				</ul>
			</li>
			<li><span class="dir">Bon M Keluar</span>
				<ul>
				<!--	<li><a href="<? echo base_url(); ?>index.php/bonm-bbc/cform/view">Pemenuhan Bahan Baku Dari Permintaan Bhn Baku</a></li> -->
					<li><a href="<? echo base_url(); ?>index.php/bonmkeluar/cform/view">Pengeluaran Bahan Baku/Pembantu</a></li>
				<!--	<li><a href="<? echo base_url(); ?>index.php/bonmkeluar/cform/viewbonmkeluarcutting">Pengeluaran Barang Hasil Cutting ke Jahitan</a></li> -->
				</ul>
			</li>
			<li><span class="dir">Input Stok Opname</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname-quilting/cform/">Bahan Quilting</a></li>
					<!--01-04-2015 dikomen -->
					<!--<li><a href="<? echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/gudangwip/cform/sounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li> -->
					<!--<li><a href="<? echo base_url(); ?>index.php/stok-opname-hsl-packing/cform/">Bahan Hasil Packing</a></li>-->
					
				</ul>
			</li>
			
			<li><span class="dir">Approval Stok Opname</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-quilting/cform/">Bahan Quilting</a></li>
					<!--<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/gudangwip/cform/approvalsounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>-->
					<!--<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-hsl-packing/cform/">Bahan Hasil Packing</a></li>-->
					
				</ul>
			</li>
			<li><a href="<? echo base_url(); ?>index.php/set-stok-harga/cform/viewstokawal">Input Stok Awal Bahan Baku/Pembantu Berdasarkan Harga</a></li>
		</ul>
	</li>
	<?php } ?>
		
		<!--  15-09-2014 dikoment, ini utk login gudang, udh dibikin diatas -->
	<?php //if ($this->session->userdata('gid') == 2) { ?>
	<!--<li><span class="dir">Gudang</span>
		<ul>
			<li><span class="dir">Bon M Masuk</span>
				<ul>
					<li><a href="<? //echo base_url(); ?>index.php/bonmmasuk/cform/view">Hasil Pembelian</a></li>
					<li><a href="<? //echo base_url(); ?>index.php/bonmmasukkredit/cform/view">Hasil Pembelian Untuk Stok Lain-lain</a></li>
					<li><a href="<? //echo base_url(); ?>index.php/bonmmasukquilting/cform/view">Hasil Pembelian Bahan Quilting</a></li>
				<!--	<li><a href="<? //echo base_url(); ?>index.php/bonmmasukcash/cform/view">Revisi Tipe Stok Untuk Pembelian Cash Lain-lain</a></li> -->
	<!--			</ul>
			</li>
		</ul>
	</li> -->
	<?php //} ?>
	
	 <!-- this is not f**kin necessary anymore -->
	<?php //if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 4 || $this->session->userdata('gid') == 5 ) { ?>
	<!--<li><span class="dir">Makloon</span>
		<ul> -->
		<!--	<li><a href="<? echo base_url(); ?>index.php/pb-makloon/cform/view">Permintaan Bahan Baku/Pembantu Ke Gudang</a></li> -->
			
		<!--	<li><span class="dir">Quilting</span>
						<ul>
							<li><a href="<? //echo base_url(); ?>index.php/sj-keluar-makloon/cform/view">SJ Keluar</a></li>
							<li><a href="<? //echo base_url(); ?>index.php/sj-masuk-makloon/cform/view">SJ Masuk</a></li>
						</ul>
			</li>
			<li><span class="dir">Bis-Bisan</span>
						<ul>
							<li><a href="<? //echo base_url(); ?>index.php/sj-keluar-bisbisan/cform/view">SJ Keluar</a></li>
							<li><a href="<? //echo base_url(); ?>index.php/sj-masuk-bisbisan/cform/view">SJ Masuk</a></li>
						</ul>
			</li>
			<li><span class="dir">Bordir</span>
						<ul>
							<li><a href="<? //echo base_url(); ?>index.php/sj-keluar-bordir/cform/view">SJ Keluar</a></li>
						</ul>
			</li>
			<li><span class="dir">Print</span>
						<ul>
							<li><a href="<? //echo base_url(); ?>index.php/sj-keluar-print/cform/view">SJ Keluar</a></li>
						</ul>
			</li>
			<li><span class="dir">Makloon Accessories</span>
						<ul>
							<li><a href="<? //echo base_url(); ?>index.php/sj-keluar-asesoris/cform/view">SJ Keluar</a></li>
							<li><a href="<? //echo base_url(); ?>index.php/sj-masuk-asesoris/cform/view">SJ Masuk</a></li>
						</ul>
			</li> -->
			
			<!-- <li><a href="<? echo base_url(); ?>index.php/faktur-pajak-makloon/cform/view">Input Faktur Pajak Makloon</a></li> -->
		<!--sementara dihide dulu	<li><a href="<? //echo base_url(); ?>index.php/retur-makloon/cform/view">Claim Produksi (Nota Debet Retur)</a></li> -->
			
	<!--	</ul>
	</li> -->
	<?php //} ?>
	
	<!--  15-09-2014 dikoment, cutting diskip dulu -->
	<?php //if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 5) { ?>
	<!--<li><span class="dir">Cutting</span>
		<ul>
			<li><a href="<? //echo base_url(); ?>index.php/pb-cutting/cform/view">Permintaan Bahan Baku Ke Gudang</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/bonm-bbc/cform/view">Pemenuhan Permintaan Bhn Baku</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/schedule-cutting/cform/view">Schedule Cutting</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/realisasi-cutting/cform/view">Realisasi Cutting</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/bs-cutting/cform/view">Pengurangan Stok Hasil Cutting BS</a></li>
		</ul>
	</li> -->
	<?php //} ?>
	
	<!-- 15-09-2014, this is no longer used anymore -->
	<?php //if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 3) { ?>
	<!--<li><span class="dir">WIP/Jahit</span> -->
		<!--sementara dikomen<ul>
			<li><a href="<? //echo base_url(); ?>index.php/jahit-schedule/cform/view">Schedule Jahit</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/view">SJ Keluar (Proses Ke Unit Jahit)</a></li>
			<li><a href="<? //echo base_url(); ?>">SJ Keluar (Proses Retur Hasil Jahit Ke Unit Jahit)</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">SJ Masuk Hasil Jahit</a></li>
		</ul> -->
	<!--	<ul>
			<li><a href="<? //echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">SJ Masuk Hasil Jahit Pembelian</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/wip/cform/viewsjmasuk">SJ Masuk</a></li>
			<li><a href="<? //echo base_url(); ?>index.php/wip/cform/viewsjkeluar">SJ Keluar</a></li>
		</ul>
	</li> -->
	<?php //} ?>
	
	<?php //if ($this->session->userdata('gid') == 2) { ?>
	<!--<li><span class="dir">WIP/Jahit</span>
		<ul>
			<li><a href="<? //echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">SJ Masuk Hasil Jahit Pembelian</a></li>
		</ul>
	</li> -->
	<?php //} ?>
	
	<!-- ini staf WIP 15-09-2014 -->
	<?php if ($this->session->userdata('gid') == 6) { ?>
	<li><span class="dir">Gudang</span>
		<ul>
			<!--<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawal">Input Stok Awal WIP (hasil jahit)</a></li>-->
			<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawal">Input Stok Awal WIP (hasil jahit)</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawalunit">Input Stok Awal WIP di Unit Jahit</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/cgudang/konversistok">Konversi Stok Barang WIP</a></li>
			<li><span class="dir">Input Stok Opname</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/gudangwip/cform/sounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
					
				</ul>
			</li>
			
			<li><span class="dir">Approval Stok Opname</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/app-stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/gudangwip/cform/approvalsounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
					
				</ul>
			</li>
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6) { ?>
	<li><span class="dir">PIC</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/pic/cform/viewsjmasuk">SJ Masuk Bahan Baku dari Jahitan</a></li>
			<li><a href="<? echo base_url(); ?>index.php/bonmkeluar/cform/viewbonmkeluarcutting">SJ Keluar Bahan Baku (Hasil Cutting) ke Jahitan</a></li>
			<li><a href="<? echo base_url(); ?>index.php/pic/cform/editwarnabrgjadi">Pengubahan Data Warna Barang Jadi (PIC / WIP)</a></li>
		</ul>
	</li>
	<li><span class="dir">WIP</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewsjmasuk">SJ Masuk Barang WIP</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/cform/viewsjkeluar">SJ Keluar Barang WIP</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/creport/addpresentasikerjaunit">Input Presentasi Kerja Unit</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/creport/addforecast">Input Forecast</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/creport/addstokschedule">Input Stok Schedule</a></li>
			<li><a href="<? echo base_url(); ?>index.php/wip/creport/addreturqc">Input Retur QC</a></li>
		</ul>
	</li>
	<?php } ?>
	
	
	<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 8) { ?>
	<li><span class="dir">Keuangan</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">Pembayaran Hutang Pembelian</a></li>
			<!--<li><a href="<? echo base_url(); ?>index.php/keu-payment-makloon/cform/view">Pembayaran Biaya Makloon</a></li>-->
		</ul>
	</li>
	
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 2) { ?>
	<li><span class="dir">Keuangan</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">Pembayaran Hutang Pembelian</a></li>
			<!--<li><a href="<? echo base_url(); ?>index.php/keu-payment-makloon/cform/view">Pembayaran Biaya Makloon</a></li>-->
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('username') != '') {  ?>
	<li><span class="dir">Informasi</span>
		<ul>
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2) { ?>
			<li><span class="dir">Pembelian</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/info-perubahan-harga/cform/">Laporan Perubahan Harga Pembelian</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-opname-hutang/cform/">Laporan Opname Hutang Dagang</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-rekap-pembelian-persup/cform">Rekap Pembelian Per Supplier</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-rekap-pembelian-peritem/cform">Rekap Penerimaan Barang Per Item</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-pembelian/cform/">Laporan Pembelian</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-rekap-hutang/cform/">Rekapitulasi Hutang Dagang</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-rekap-stok-bb/cform">Rekap Mutasi Stok Bahan Baku/Pembantu</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-opvssj/cform">OP vs SJ Pembelian</a></li>
				<!--	<li><a href="#">Rekapitulasi Pembayaran Hutang Dagang</a></li> -->
				</ul>
			</li>
			<?php } ?>
			
			<?php //if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 3) { ?>
			<!--<li><span class="dir">Gudang</span>
				<ul>
					<li><a href="<? //echo base_url(); ?>index.php/info-mutasi-stok/cform/">Laporan Mutasi Stok Bahan Baku/Pembantu</a></li>
					<li><a href="<? //echo base_url(); ?>index.php/info-mutasi-stok/cform/wip">Laporan Mutasi Stok WIP</a></li>
				</ul>
			</li> -->
			<?php //} ?>
			
			<?php if ($this->session->userdata('gid') != 8) { ?>
			<li><span class="dir">Gudang</span>
				<ul>
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2 || $this->session->userdata('gid') == 3) { ?>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-stok/cform/viewsobhnbaku">Stok Opname Bahan Baku/Pembantu</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-stok/cform/">Laporan Mutasi Stok Bahan Baku/Pembantu</a></li>
					<!--<li><a href="<? echo base_url(); ?>index.php/info-mutasi-stok/cform/wip">Laporan Mutasi Stok WIP</a></li> -->
					<!--<li><a href="<? echo base_url(); ?>index.php/info-mutasi-stok/cform/hasilcutting">Laporan Mutasi Stok Bahan Hasil Cutting</a></li>-->
			<?php } ?>
			
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6) { ?>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/viewsohasiljahit">Stok Opname Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/viewsounitjahit">Stok Opname Barang WIP (hasil jahit) di Unit Jahit</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-stok/cform/wip">Laporan Mutasi Stok WIP</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/transaksihasiljahit">Laporan Transaksi Barang WIP di Perusahaan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/cgudang/historykonversistok">History Konversi Stok Barang WIP</a></li>
			<?php } ?>
				</ul>
			</li>
			<?php } ?>
			
			<!-- 15-09-2014, samain dgn tirai -->
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6) { ?>
			<li><span class="dir">WIP</span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/presentasikerjaunit">Laporan Presentasi Kerja Unit</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/rekappresentasikerjaunit">Rekapitulasi Presentasi Kerja Unit Tahunan</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/forecastvsschedule">Laporan Forecast vs Schedule</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/mutasiunit">Laporan Mutasi Unit Jahit</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/transaksiunitjahit">Laporan Transaksi Barang Unit Jahit</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/stokmingguanunit">Laporan Stok Mingguan Unit Jahit</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/bhnbakuunit">Laporan Bahan Baku Unit Jahit</a></li>
					<li><a href="<? echo base_url(); ?>index.php/wip/creport/qcunitjahit">Laporan QC Unit Jahit</a></li>
				</ul>
			</li>
			<?php } ?>
			<!-- -->
			
			<!-- 15-09-2014 -->
			<li><span class="dir">Stok Terkini </a></span>
				<ul>
					<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 8) { ?>
						<li><a href="<? echo base_url(); ?>index.php/info-stok-bb/cform/view">Bahan Baku/Pembantu</a></li>
							<li><a href="<? echo base_url(); ?>index.php/info-stok-quilting/cform/view">Bahan Hasil Quilting</a></li>
						<li><a href="<? echo base_url(); ?>index.php/info-stok-hsl-cutting/cform/view">Bahan Hasil Cutting</a></li>
						<li><a href="<? echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/view">Barang WIP (hasil jahit) di Perusahaan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/viewstokunit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
						<!--<li><a href="<? echo base_url(); ?>index.php/info-stok-hsl-packing/cform/view">Barang Jadi (hasil packing)</a></li>-->
					<?php }
					else if ($this->session->userdata('gid') == 2 || $this->session->userdata('gid') == 3) {
					 ?>
						<li><a href="<? echo base_url(); ?>index.php/info-stok-bb/cform/view">Bahan Baku/Pembantu</a></li>
							<li><a href="<? echo base_url(); ?>index.php/info-stok-quilting/cform/view">Bahan Hasil Quilting</a></li>
						<!--<li><a href="<? echo base_url(); ?>index.php/info-stok-hsl-cutting/cform/view">Bahan Hasil Cutting</a></li>-->
					 <?php } else if ($this->session->userdata('gid') == 6) { ?>
						<li><a href="<? echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/view">Barang WIP (hasil jahit) di Perusahaan</a></li>
						<li><a href="<? echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/viewstokunit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
					 <?php } ?>
				</ul>
			
		<!--	<li><span class="dir">Kartu Stok </a></span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-bb/cform">Bahan Baku/Pembantu</a></li>				-->
				<!--	<li><a href="<? echo base_url(); ?>index.php/info-mutasi-makloon/cform">Bahan Hasil Makloon</a></li> -->
		<!--			<li><a href="<? echo base_url(); ?>index.php/info-mutasi-quilting/cform">Bahan Hasil Quilting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-hsl-bisbisan/cform">Bahan Hasil Bis-Bisan</a></li>		-->
				<!--	<li><a href="<? echo base_url(); ?>index.php/info-mutasi-hsl-bordir/cform">Bahan Hasil Bordir</a></li> -->
		<!--			<li><a href="<? echo base_url(); ?>index.php/info-mutasi-hsl-print/cform">Bahan Hasil Print</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-hsl-asesoris/cform">Bahan Hasil Makloon Accessories</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-hsl-cutting/cform">Bahan Hasil Cutting</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-hsl-jahit/cform">Barang Hasil Jahit</a></li>
					<li><a href="<? echo base_url(); ?>index.php/info-mutasi-hsl-packing/cform">Barang Hasil Packing</a></li>
				</ul>
			</li> -->
			
			<!-- 15-09-2014, not necessary anymore -->
			<?php //if ($this->session->userdata('gid') == 5 || $this->session->userdata('gid') == 1 ) { ?>
		<!--	<li><span class="dir">Cutting </a></span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/exp-schedule-cutting/cform">Realisasi Cutting Per Operator</a></li>
				</ul>
			</li> -->
			<?php //} ?>
			
		<!--	<li><span class="dir">Rekap Mutasi Stok </a></span>
				<ul>
					<li><a href="<? echo base_url(); ?>index.php/info-rekap-stok-bb/cform">Bahan Baku/Pembantu</a></li> -->
				<!--	<li><a href="<? echo base_url(); ?>index.php/info-mutasi-makloon/cform">Bahan Hasil Makloon</a></li> -->
				<!--	<li><a href="<? echo base_url(); ?>#">Bahan Hasil Cutting</a></li>
					<li><a href="<? echo base_url(); ?>#">Barang Hasil Jahit</a></li>
					<li><a href="<? echo base_url(); ?>#">Barang Hasil Packing</a></li>
				</ul>
			</li> -->
			
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 1 && $this->session->userdata('username') != '') { ?>
	<li><span class="dir">Setting</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/users-group/cform">Group User</a></li>
			<li><a href="<? echo base_url(); ?>index.php/users-admin-user/cform/view">Admin User</a></li>
			<li><a href="<? echo base_url(); ?>index.php/users-ganti-passwd/cform">Ganti Password</a></li>
			<li><a href="<? echo base_url(); ?>index.php/info-pembelian/cform/cek_sj_nonsinkron">Cek SJ Yg Angkanya Tdk Cocok</a></li>
			<li><a href="<? echo base_url(); ?>index.php/bacadbf/cform/">Tes Baca DBF</a></li>
		</ul>
	</li>
	<?php } else if ($this->session->userdata('gid') != 1 && $this->session->userdata('username') != '') { ?>
		<li><span class="dir">Setting</span>
		<ul>
			<li><a href="<? echo base_url(); ?>index.php/users-ganti-passwd/cform">Ganti Password</a></li>
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('username') != '') { ?>
	<li><a href="<? echo base_url(); ?>index.php/users-login/cform/logout">Logout</a></li>
	<?php } ?>
</ul>
</div>
<!-- / END -->
<br><br><br><br>

<? echo $this->load->view($isi); ?>

<? echo $this->load->view('footer'); ?>

</body>
</html>
