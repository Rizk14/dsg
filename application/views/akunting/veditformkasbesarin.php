<h3>Data Kas Besar (Masuk)</h3><br>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/addkasbesarin">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewkasbesarin">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">
	
function cari(posisi, kodecoa) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/akunting/cform/caricoa2', 
				data: 'kode_coa='+kodecoa+'&posisi='+posisi, success: function(response) {
					$("#infocoa").html(response);
			}});
}

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_data() {
	if ($('#id_coa').val() == '') {
		alert("Kode CoA tidak boleh kosong...!");
		s = 1;
		return false;
	}
	if ($('#dkk').val() == '') {
		alert("Tanggal tidak boleh kosong...!");
		s = 1;
		return false;
	}
	if ($('#deskripsi').val() == '') {
		alert("Deskripsi tidak boleh kosong...!");
		s = 1;
		return false;
	}
	if($('#jumlah').val() == '0' || $('#jumlah').val() == '' ) {				
		alert("Data jumlah nominal tidak boleh 0 / kosong...!");
		s = 1;
		return false;
	}
	if (isNaN($('#jumlah').val()) ) {
		alert("Jumlah nominal harus berupa angka..!");
		s = 1;
		return false;
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_saldo" id="f_saldo" action="<?php echo base_url(); ?>index.php/akunting/cform/updatedatakasbesarin" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_kb" value="<?php echo $eid_kb ?>">
<input type="hidden" name="bulan" value="<?php echo $ebulan ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<br>
Edit Data<br><br>
<table width="100%" cellspacing="2" cellpadding="1" >
  <tr>
		<td width="10%">Periode (bulan-tahun)</td>
		<td>: <select name="bulanx" id="iperiodebl" disabled>
				<option value="01" <?php if($ebulan == '01') { ?>selected<?php } ?>>Januari</option>
				<option value="02" <?php if($ebulan == '02') { ?>selected<?php } ?>>Februari</option>
				<option value="03" <?php if($ebulan == '03') { ?>selected<?php } ?>>Maret</option>
				<option value="04" <?php if($ebulan == '04') { ?>selected<?php } ?>>April</option>
				<option value="05" <?php if($ebulan == '05') { ?>selected<?php } ?>>Mei</option>
				<option value="06" <?php if($ebulan == '06') { ?>selected<?php } ?>>Juni</option>
				<option value="07" <?php if($ebulan == '07') { ?>selected<?php } ?>>Juli</option>
				<option value="08" <?php if($ebulan == '08') { ?>selected<?php } ?>>Agustus</option>
				<option value="09" <?php if($ebulan == '09') { ?>selected<?php } ?>>September</option>
				<option value="10" <?php if($ebulan == '10') { ?>selected<?php } ?>>Oktober</option>
				<option value="11" <?php if($ebulan == '11') { ?>selected<?php } ?>>November</option>
				<option value="12" <?php if($ebulan == '12') { ?>selected<?php } ?>>Desember</option>
			</select> -

		  <input name="tahun" type="text" id="iperiodeth" readonly="true" size="4" value="<?php echo $etahun ?>" maxlength="4">
			<input type="hidden" name="bulan" value="<?php echo $ebulan ?>" >
		</td>
  </tr>	
  <tr>
		<td>Kode Akun / CoA </td>
		<td width="70%" style="white-space:nowrap;">: <input name="kode_coa" type="text" id="kode_coa" size="5" value="<?php echo $ekode_coa ?>" onkeyup="cari('0',this.value);" />
		<input name="id_coa_lama" type="hidden" id="id_coa_lama" value="<?php echo $eid_coa ?>"/>
		<label id="infocoa">
          <input name="nama_coa_0" type="text" id="nama_coa" size="40" readonly="true" value="<?php echo $enama_coa ?>" />
           <input name="id_coa_0" type="hidden" id="id_coa" value="<?php echo $eid_coa ?>"/>
         </label>            
		</td>
	</tr>
  <tr>
    <td>Tanggal</td>
    <td>:
      <label>
      <input name="tgl" type="text" id="dkk" size="10" value="<?php echo $etgl_trans ?>" readonly="true">
    </label>
	   <img alt="" id="imgtgl" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl,'dd-mm-yyyy',this)">
    </td>
  </tr>
  <tr>
    <td>Deskripsi</td>
    <td>:
      <input name="deskripsi" type="text" id="deskripsi" size="30" value="<?php echo $edeskripsi ?>" />
    </td>
  </tr>
  <tr>
    <td>Jumlah</td>
    <td>:
      <input name="jumlah" type="text" id="jumlah" size="10" value="<?php echo $ejumlah ?>" style="text-align:right;" />
      <input type="hidden" name="jumlah_lama" value="<?php echo $ejumlah ?>">
    </td>
  </tr>
		<?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "akunting/cform/viewkasbesarin/".$cur_page;
			else
				$url_redirectnya = "akunting/cform/carikasbesarin/".$carinya."/".$cur_page;
        ?>
  
  <tr><td>&nbsp;</td>  	
	<td>
		<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;
			<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
		
  </tr>

</table>
</form>
</script>
<!-- <?php
			if($tgl_dok <= $batas){ ?>
				<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
		<?}else{?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;
			<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
		<?}?> -->