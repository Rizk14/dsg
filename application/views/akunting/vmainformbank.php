<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">
	
function cari(posisi, kodecoa) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/akunting/cform/caricoa2', 
				data: 'kode_coa='+kodecoa+'&posisi='+posisi, success: function(response) {
					$("#infocoa").html(response);
			}});
}

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();
	
	if (kode == '') {
		alert("Kode Bank harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Bank harus diisi..!");
		$('#nama').focus();
		return false;
	}
	if ($('#id_coa').val() == '') {
		alert("Kode CoA tidak boleh kosong...!");
		s = 1;
		return false;
	}
	
}

$(function()
{
	  
});
	
</script>

<h3>Data Bank</h3><br>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/addbank">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbank">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_bank', 'id' => 'f_bank');
	echo form_open('akunting/cform/submitbank', $attributes); ?>
<?php if ($edit == '1') { ?> 
	<input type="hidden" name="goedit" id="goedit" value="1"> 
	<input type="hidden" name="id_bank" value="<?php echo $eid_bank ?>">
	<input type="hidden" name="kode_lama" value="<?php echo $ekode ?>">
	<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
	<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
	<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<?php } ?>
	<table>
		<?php
		if ($edit != '1') {
		?>
		<tr>
			<td>Kode Bank</td>
			<td>: <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="7" size="10">
			</td>
		</tr>
		<?php 
		}
		else {
		?>
		<!-- ini jika edit == 1 -->
		<tr>
			<td>Kode Bank</td>
			<td>: <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="7" size="10">
			</td>
		</tr>
		
		<?php
		}
		?>
		<tr>
			<td>Nama Bank</td>
			<td>: <input type="text" name="nama" id="nama" value="<?php $pos = strpos($enama, "\"");
	  if ($pos > 0)
		echo str_replace("\"", "&quot;", $enama);
	  else
		echo $enama ?>" size="40"></td>
		</tr>
		
		<tr>
		<td>Kode Akun / CoA </td>
		<td width="70%" style="white-space:nowrap;">: <input name="kode_coa" type="text" id="kode_coa" size="5" value="<?php echo $ekode_coa ?>" onkeyup="cari('0',this.value);" />
		<input name="id_coa_lama" type="hidden" id="id_coa_lama" value="<?php echo $eid_coa ?>"/>
		<label id="infocoa">
          <input name="nama_coa_0" type="text" id="nama_coa" size="40" readonly="true" value="<?php echo $enama_coa ?>" />
           <input name="id_coa_0" type="hidden" id="id_coa" value="<?php echo $eid_coa ?>"/>
         </label>            
		</td>
	</tr>
		
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/akunting/cform/viewbank'">
			<?php } else { 
				if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "akunting/cform/viewbank/".$cur_page;
			else
				$url_redirectnya = "akunting/cform/caribank/".$carinya."/".$cur_page;
				?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>

