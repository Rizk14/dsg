<h3>Data Master Saldo Akun</h3><br>
<a href="<? echo base_url(); ?>index.php/akunting/cform/addsaldoakun">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/akunting/cform/viewsaldoakun">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	$('#pilih_coa').click(function(){
		var urlnya = "<?php echo base_url(); ?>index.php/akunting/cform/show_popup_coa/";
			openCenteredWindow(urlnya);
	  });
	  
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_data() {
	var id_coa= $('#id_coa').val();
	var saldo_awal= $('#saldo_awal').val();
	var debet= $('#debet').val();
	var kredit= $('#kredit').val();
	var saldo_akhir= $('#saldo_akhir').val();
	
	if (id_coa == '') {
		alert("Kode Akun (CoA) harus dipilih..!");
		$('#nama_coa').focus();
		return false;
	}
	
	if (saldo_awal == '') {
		alert("Saldo awal tidak boleh kosong..!");
		$('#saldo_awal').val('0');
		return false;
	}
	if (debet == '') {
		alert("Mutasi debet tidak boleh kosong..!");
		$('#debet').val('0');
		return false;
	}
	if (kredit == '') {
		alert("Mutasi kredit tidak boleh kosong..!");
		$('#kredit').val('0');
		return false;
	}
	if (saldo_akhir == '') {
		alert("Saldo akhir tidak boleh kosong..!");
		$('#saldo_akhir').val('0');
		return false;
	}
	
	if (isNaN($('#saldo_awal').val()) ) {
		alert("Saldo awal harus berupa angka..!");
		$('#saldo_awal').val('0');
		return false;
	}
	if (isNaN($('#debet').val()) ) {
		alert("Mutasi debet harus berupa angka..!");
		$('#debet').val('0');
		return false;
	}
	if (isNaN($('#kredit').val()) ) {
		alert("Mutasi kredit harus berupa angka..!");
		$('#kredit').val('0');
		return false;
	}
	if (isNaN($('#saldo_akhir').val()) ) {
		alert("Saldo akhir harus berupa angka..!");
		$('#saldo_akhir').val('0');
		return false;
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_saldo" id="f_saldo" action="<?php echo base_url(); ?>index.php/akunting/cform/updatedatasaldoakun" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_saldo" value="<?php echo $eid_saldo ?>">
<input type="hidden" name="id_coa_lama" value="<?php echo $eid_coa ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<br>
Edit Data<br><br>
<table width="100%" cellspacing="2" cellpadding="1" >
  <tr>
		<td width="10%">Periode (bulan-tahun)</td>
		<td>: <select name="bulanx" id="bulanx" disabled>
				<option value="01" <?php if($ebulan == '01') { ?>selected<?php } ?>>Januari</option>
				<option value="02" <?php if($ebulan == '02') { ?>selected<?php } ?>>Februari</option>
				<option value="03" <?php if($ebulan == '03') { ?>selected<?php } ?>>Maret</option>
				<option value="04" <?php if($ebulan == '04') { ?>selected<?php } ?>>April</option>
				<option value="05" <?php if($ebulan == '05') { ?>selected<?php } ?>>Mei</option>
				<option value="06" <?php if($ebulan == '06') { ?>selected<?php } ?>>Juni</option>
				<option value="07" <?php if($ebulan == '07') { ?>selected<?php } ?>>Juli</option>
				<option value="08" <?php if($ebulan == '08') { ?>selected<?php } ?>>Agustus</option>
				<option value="09" <?php if($ebulan == '09') { ?>selected<?php } ?>>September</option>
				<option value="10" <?php if($ebulan == '10') { ?>selected<?php } ?>>Oktober</option>
				<option value="11" <?php if($ebulan == '11') { ?>selected<?php } ?>>November</option>
				<option value="12" <?php if($ebulan == '12') { ?>selected<?php } ?>>Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" readonly="true" size="4" value="<?php echo $etahun ?>" maxlength="4"> <i>*Contoh: 2015</i>
			<input type="hidden" name="bulan" value="<?php echo $ebulan ?>" >
		</td>
  </tr>	
  <tr>
		<td>Kode Akun / CoA </td>
		<td width="70%">: <input type="text" name="nama_coax" id="nama_coax" value="<?php echo $ekode_coa." - ".$enama_coa ?>" size="40" readonly="true">
		<input type="hidden" name="id_coa" id="id_coa" value="<?php echo $eid_coa ?>">
		<input type="hidden" name="nama_coa" id="nama_coa" value="<?php echo $enama_coa ?>">
		<input name="pilih_coa" id="pilih_coa" value="..." type="button" title="browse data CoA">
		</td>
	</tr>
  <tr>
    <td>Saldo Awal</td>
    <td>:
      <input name="saldo_awal" type="text" id="saldo_awal" size="10" value="<?php echo $esaldo_awal ?>" style="text-align:right;">
    </td>
  </tr>
  <tr>
    <td>Mutasi Debet</td>
    <td>:
      <input name="debet" type="text" id="debet" size="10" value="<?php echo $edebet ?>" style="text-align:right;">
    </td>
  </tr>
  <tr>
    <td>Mutasi Kredit</td>
    <td>:
      <input name="kredit" type="text" id="kredit" size="10" value="<?php echo $ekredit ?>" style="text-align:right;">
    </td>
  </tr>
  <tr>
    <td>Saldo Akhir</td>
    <td>:
      <input name="saldo_akhir" type="text" id="saldo_akhir" size="10" value="<?php echo $esaldo_akhir ?>" style="text-align:right;">
    </td>
  </tr>
		<?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "akunting/cform/viewsaldoakun/".$cur_page;
			else
				$url_redirectnya = "akunting/cform/carisaldoakun/".$carinya."/".$cur_page;
        ?>
  
  <tr><td>&nbsp;</td>
	<td><input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
  </tr>

</table>
</form>
