<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Data Bank (Keluar)</h3><br>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/addbankout">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbankout">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('akunting/cform/caribankout'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td width="45%">Masukkan Kode / Nama CoA</td>
		<td><input type="text" name="cari" size="20" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isi">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		 <th>Periode (Bulan-Tahun)</th>
		 <th>CoA Bank</th>
		  <th>Area</th>
		 <th>No Transaksi</th>
		 <th>CoA</th>
		 <th>Tanggal</th>
		 <th>Deskripsi</th>
		 <th>Jumlah</th>
		 <th>Tanggal Input</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
		 
			 foreach ($query as $row){
				$pisah1 = explode("-", $row->tgl);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tgl_trans = $tgl1."-".$bln1."-".$thn1;
				 
				 $pisah1 = explode("-", $row->tgl_input);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td>".$row->bulan."-".$row->tahun."</td>";
				 
				 $query3	= $this->db->query(" SELECT b.kode, b.nama FROM tt_kas_bank a INNER JOIN tm_coa b ON a.id_coa_bank = b.id
								WHERE a.id_coa_bank = '$row->id_coa_bank' ");
				if ($query3->num_rows() > 0){
					$hasilrow3 = $query3->row();
					$kode_coa_bank	= $hasilrow3->kode;
					$nama_coa_bank	= $hasilrow3->nama;
				}
				else {
					$kode_coa_bank = '';
					$nama_coa_bank = '';
				}
				 $query3	= $this->db->query(" SELECT * from tm_area where id = '$row->id_area' ");
				if ($query3->num_rows() > 0){
					$hasilrow3 = $query3->row();
					$kode_area	= $hasilrow3->kode_area;
					$nama_area	= $hasilrow3->nama;
				}
				else {
					$kode_area = '';
					$nama_area = '';
				}
				 
				 echo    "<td>$kode_coa_bank - $nama_coa_bank</td>";
				 echo    "<td>$kode_area - $nama_area</td>";
				 echo    "<td>$row->no_transaksi</td>";
				 echo    "<td>$row->kode - $row->nama</td>";
				 echo    "<td>$tgl_trans</td>";
				 echo    "<td>$row->deskripsi</td>";
				 echo    "<td align='right'>".number_format($row->jumlah, 2, ',','.')."</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 $ada = 0;
				 
				echo "<td align=center>";
				echo "<a href=".base_url()."index.php/akunting/cform/editbankout/$row->id/".$cur_page."/".$is_cari."/".$cari." \" id=\"$row->id\">Edit</a>";

				if ($ada == 0) {
					echo "&nbsp; <a href=".base_url()."index.php/akunting/cform/deletebankout/$row->id/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				}
				echo "</td>";

				 echo  "</tr>";
				 $i++;
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
