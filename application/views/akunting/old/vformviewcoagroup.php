<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Data Kode Akun Group (CoA)</h3><br>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/addcoagroup">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoagroup">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('akunting/cform/caricoagroup'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td width="45%">Masukkan Kode / Nama Akun</td>
		<td><input type="text" name="cari" size="20" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%" class="isi">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		 <th>Kode CoA</th>
		 <th>Nama CoA</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
		 
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				 echo "<tr class=\"record\">";
				 echo "<td>$i</td>";
				 echo    "<td>$row->kode</td>";
				 echo    "<td>$row->nama</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 $ada = 0;
				 // 16-12-2015 cek apakah data kode sudah dipake di transaksi keuangan. ini belakangan aja
				/* $query3	= $this->db->query(" SELECT id FROM tm_pp_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){ 
					//$hasilrow = $query3->row();
					//$jum_stok	= $hasilrow->jstok;
					$ada = 1; 
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_op_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				
				 $query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 // 21-09-2015
				 $query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmkeluar_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasuklain_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_bonmmasukmanual_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 $query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail WHERE id_brg = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 } */
				 
				echo "<td align=center>";
				echo "<a href=".base_url()."index.php/akunting/cform/addcoagroup/$row->id/".$cur_page."/".$is_cari."/".$cari." \" id=\"$row->id\">Edit</a>";

				if ($ada == 0) {
					echo "&nbsp; <a href=".base_url()."index.php/akunting/cform/deletecoagroup/$row->id/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				}
				echo "</td>";

				 echo  "</tr>";
				 $i++;
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
