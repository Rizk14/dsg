<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Data Kas kecil (Keluar)</h3><br>

<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewkaskecil">View Data</a>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/cetakvoucherkk">Kembali</a><br><br>


<div>
<!-- Total Data = <#?php echo $jum_total ?><br><br> -->
<?php echo form_open('akunting/cform/carikasbesarout/'.$periode.'/'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td width="45%">Masukkan Nomor Voucher</td>
		<td><input type="text" name="cari" size="20" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isi">
	Total Data = <?php echo $jum_total;?><br><br>
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		 <th>Kode Voucher</th>
		 <th>Jumlah</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1;

			foreach ($query as $row){
				 //$i = $i+1;
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td>$row->i_pv</td>";
				 echo    "<td align='right'>".number_format($row->v_pv, 2, ',','.')."</td>";
				 $ada = 0;
				 
				echo "<td align=center>";
				// echo "<a href=".base_url()."index.php/akunting/cform/cetak/$row->i_rv/$row->kode_area>Cetak</a>";
				if($row->f_cancel_kk == 'f'){
				echo "<a href='#' onclick='eval(window.open(\"".site_url()."/akunting/cform/cetak/$row->i_pv/$row->kode_area/00/00\",\"_blank\",\"width=1366px,height=768px,resizable=1,scrollbars=1,top=(screen.height-768)/2,left=(screen.width-1366)/2\"))' id='$row->i_pv'>Cetak</a>";
				}
				echo  "</tr>";
				 $i++;
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links($periode);?>
</div>
