<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Data Jurnal Umum</h3><br>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/addjurnalumum">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewjurnalumum">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('akunting/cform/viewjurnalumum'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td width="45%">Masukkan No Jurnal</td>
		<td><input type="text" name="cari" size="20" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%" class="isi">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		 <th>No jurnal</th>
		 <th>Tgl jurnal</th>
		  <th>CoA</th>
		  <th>Deskripsi</th>
		  <th>Area</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if ($startnya == '')
				$i=1;
			else
				$i = $startnya+1; 
		 if(is_array($query)){
					 for($j=0;$j<count($query);$j++){

				 $pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update= $tgl1."-".$nama_bln."-".$thn1 ;
		
				$query3	= $this->db->query(" SELECT * from tm_area where id = '".$query[$j]['id_area']."' ");
				if ($query3->num_rows() > 0){
					$hasilrow3 = $query3->row();
					$kode_area	= $hasilrow3->kode_area;
					$nama_area	= $hasilrow3->nama;
				}
				else {
					$kode_area = '';
					$nama_area = '';
				}
	
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td>".$query[$j]['no_jurnal']."</td>";
				 echo    "<td>".$query[$j]['tgl_jurnal']."</td>";
				
				 echo "<td nowrap>";
				 
				 if (is_array($query[$j]['detail_ju'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_ju'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						
						  echo $var_detail[$k]['kode_coa']." / ".$var_detail[$k]['nama_coa'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				  echo "<td nowrap>";
				  if (is_array($query[$j]['detail_ju'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_ju'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						
						  echo $var_detail[$k]['deskripsi'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				echo    "<td>$kode_area - $nama_area</td>";
				 echo    "<td>$tgl_update</td>";
				 
				echo "<td align=center>";
				echo "<a href=".base_url()."index.php/akunting/cform/editjurnalumum/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>";
				echo "&nbsp; <a href=".base_url()."index.php/akunting/cform/deletejurnalumum/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";

				echo "</td>";

				 echo  "</tr>";
				 $i++;
		 	}
		 	
		}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
