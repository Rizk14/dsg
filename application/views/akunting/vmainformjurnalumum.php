<h3>Data Jurnal Umum</h3><br>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/addjurnalumum">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewjurnalumum">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode_coa*************************************
		var kode_coa="#kode_coa_"+n;
		var new_kode_coa="#kode_coa_"+no;
		$(kode_coa, lastRow).attr("id", "kode_coa_"+no);
		$(new_kode_coa, lastRow).attr("name", "kode_coa_"+no);		
		$(new_kode_coa, lastRow).attr("onkeyup", "cari('"+no+"', this.value);");		
		$(new_kode_coa, lastRow).val('');		
		//*****end kode_coa*********************************
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_akunting.iddata.value="+no;			 
		 var  even_klik= " openCenteredWindow('<?php echo base_url(); ?>index.php/akunting/cform/show_popup_coa/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		
		
		//******div infocoa*************************************
		var infocoa="#infocoa_"+n;
		var new_infocoa="#infocoa_"+no;
		$(infocoa, lastRow).attr("id", "infocoa_"+no);

		$(new_infocoa, lastRow).html("<input type='hidden' id='id_coa_"+no+"' name='id_coa_"+no+"' value=''>"+
		"<input type='text' id='nama_coa_"+no+"' name='nama_coa_"+no+"' value='' readonly='true' size='40'>");
		
		//*****imgtgl*************************************
		var imgtgl="#imgtgl_"+n;
		var new_imgtgl="#imgtgl_"+no;
		$(imgtgl, lastRow).attr("id", "imgtgl_"+no);
		$(new_imgtgl, lastRow).attr("name", "imgtgl_"+no);
		var  even_klik= "displayCalendar(document.forms[0].tgl_"+no+",'dd-mm-yyyy',this)";
		$(new_imgtgl, lastRow).attr("onclick",even_klik);	
		//*****end imgtgl*********************************
		
		//*****tgl*************************************
		var tgl="#tgl_"+n;
		var new_tgl="#tgl_"+no;
		$(tgl, lastRow).attr("id", "tgl_"+no);
		$(new_tgl, lastRow).attr("name", "tgl_"+no);		
		$(new_tgl, lastRow).val('');		
		//*****end tgl*********************************
		
		//*****deskripsi*************************************
		var deskripsi="#deskripsi_"+n;
		var new_deskripsi="#deskripsi_"+no;
		$(deskripsi, lastRow).attr("id", "deskripsi_"+no);
		$(new_deskripsi, lastRow).attr("name", "deskripsi_"+no);		
		$(new_deskripsi, lastRow).val('');				
		//*****end deskripsi*************************************	
		
		//*****debet*************************************
		var debet="#debet_"+n;
		var new_debet="#debet_"+no;
		$(debet, lastRow).attr("id", "debet_"+no);
		$(new_debet, lastRow).attr("name", "debet_"+no);		
		$(new_debet, lastRow).val('0');				
		//*****end debet*************************************	
		
		//*****kredit*************************************
		var kredit="#kredit_"+n;
		var new_kredit="#kredit_"+no;
		$(kredit, lastRow).attr("id", "kredit_"+no);
		$(new_kredit, lastRow).attr("name", "kredit_"+no);		
		$(new_kredit, lastRow).val('0');				
		//*****end kredit*************************************	
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
		
});
</script>

<script type="text/javascript">
	
function openCenteredWindow(url) {

	var width = 720;
	var height = 480;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
	myWindow = window.open(url, "subWind", windowFeatures);
}	
	

function cari(posisi, kodecoa) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/akunting/cform/caricoa2', 
				data: 'kode_coa='+kodecoa+'&posisi='+posisi, success: function(response) {
					$("#infocoa_"+posisi).html(response);
			}});
}

function cek_data() {
	var tahun= $('#tahun').val();
	var s = 0;
	
	kon = window.confirm("Yakin akan simpan data ini ??");
	if (kon) {
		if (tahun == '') {
			alert("Tahun harus diisi..!");
			$('#nama_coa').focus();
			return false;
		}
		
		var jum= $('#no').val()-1; 
		
		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
					
				if ($('#id_coa_'+k).val() == '') {
					alert("Kode CoA tidak boleh ada yang kosong...!");
					s = 1;
					return false;
				}
				if ($('#tgl_'+k).val() == '') {
					alert("Tanggal tidak boleh ada yang kosong...!");
					s = 1;
					return false;
				}
				if ($('#deskripsi_'+k).val() == '') {
					alert("Deskripsi tidak boleh ada yang kosong...!");
					s = 1;
					return false;
				}
				if($('#debet_'+k).val() == '' || $('#kredit_'+k).val() == '' ) {				
					alert("Data debet / kredit nominal tidak boleh kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#debet_'+k).val()) || isNaN($('#kredit_'+k).val()) ) {
					alert("debet / kredit nominal harus berupa angka..!");
					s = 1;
					return false;
				}
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
	
		if (s != 1)
			document.f_akunting.submit();
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<?php if ($msg != '') echo "<i>".$msg."</i><br>"; 
$attributes = array('name' => 'f_akunting', 'id' => 'f_akunting');
echo form_open('akunting/cform/submitjurnalumum', $attributes); ?>
<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<table width="100%" cellspacing="2" cellpadding="1" >	
<tr>
	<td>Jurnal</td>
    <td> :
	 <input name="no_jurnal" type="text" id="no_jurnal" size="15" maxlength="15" value="">
	</td>
  </tr>

  <tr>
	<td>Tanggal Jurnal</td>
    <td> :
	<label>
      <input name="tgl_jurnal" type="text" id="tgl_jurnal" size="15" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_jurnal" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_jurnal,'dd-mm-yyyy',this)">
	</td>
  </tr>

   <tr>
		<td>Area</td>
	<td>: <select name="id_area" id="id_area">
				<?php foreach ($list_area as $area) { ?>
					<option value=<?php echo $area->id ?>  ><?php echo $area->kode_area." - ". $area->nama ?></option>
				<?php } ?>
			</select>
	</td>
	</tr>
	<tr>
	<td>Keterangan</td>
    <td> :
	 <input name="keterangan" type="text" id="keterangan" size="40" maxlength="40" value="">
	</td>
  </tr>
  	<tr>
	<td>Debet</td>
    <td> :
	 <input name="totdebet" type="text" id="debet" size="15" maxlength="15" value="" readonly="true">
	</td>
  </tr>
   	<tr>
	<td>Kredit</td>
    <td> :
	 <input name="totkredit" type="text" id="kredit" size="15" maxlength="15" value="" readonly="true">
	</td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku" width="100%" border="0" align="center"  cellpadding="1"  cellspacing="2">
		<tr>
			<td colspan="6" align="left">
			<input id="addrow" type="button" name="addrow" value=" + " title="Tambah data CoA">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus data CoA">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode CoA</th>
          <th>Nama CoA</th>
            <th></th>
         <!-- <th>Pilih Coa</th> --->

	      <th>Deskripsi</th>
	      <th>Debet</th>
	      <th>Kredit</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>		
          <td><input name="kode_coa_1" type="text" id="kode_coa_1" size="10" value="" onkeyup="cari('1',this.value);" /></td>  
          <td style="white-space:nowrap;"><div id="infocoa_1">
          <input name="nama_coa_1" type="text" id="nama_coa_1" size="40" readonly="true" value="" />
           <input name="id_coa_1" type="hidden" id="id_coa_1" value=""/>
            </div>
           </td>
          <td> <input title="browse data coa" onmouseover="document.f_akunting.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript:  
            openCenteredWindow('<?php echo base_url(); ?>index.php/akunting/cform/show_popup_coa/1');" ></td>
	
          <td><input name="deskripsi_1" type="text" id="deskripsi_1" size="40" value="" /></td>
          <td><input name="debet_1" type="text" id="debet_1" size="10" value="0" style="text-align:right;" /></td>
           <td><input name="kredit_1" type="text" id="kredit_1" size="10" value="0" style="text-align:right;" /></td>
          
        </tr>
	</table>	
	
	</form><br>
	<div align="center">
	<input type="button" name="simpan_data" id="simpan_data" value="Simpan" onclick="cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/akunting/cform/viewjurnalumum'"></div>
     </td>
    </tr>
  
</table>
</form>
