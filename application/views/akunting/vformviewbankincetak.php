<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>
<h3>Cetak Voucher Bank (Masuk)</h3><br>

<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbankin">View Data</a><br><br>
<div>
<?php echo form_open('akunting/cform/printvoucherkbin'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data Voucher</legend>
<table>
	<tr>
	<td width="13%">Periode (bulan-tahun)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> <br>
		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4"> <i>*Contoh: 2020</i>
		</td>
	</tr>
	<tr>
		<td width="45%">Bank</td>
		<td> <select name="ibank" id="ibank">
				 <option value="" selected = "true" disabled>----</option> 
				<?php foreach ($list_bank as $bank) { ?>
					<option value="<?php echo $bank->kode_coa ?>"><?php echo $bank->kode."-".$bank->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="View" onclick = "return dipales()"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
</div>

<script language="javascript" type="text/javascript">
function dipales() {
    if ((document.getElementById("tahun").value == '')) {
		alert('Tahun tidak boleh kosong !!!');
		return false;
    }else if((document.getElementById("ibank").value == '')) {
		alert('Pilih Kode Bank !!!');
		return false;
	}else{
		return true;
	}
}
</script>