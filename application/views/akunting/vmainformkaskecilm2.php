<h3>Data Kas kecil (Masuk)</h3><br>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/addkaskecilm">Kembali</a>&nbsp;&nbsp;
<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewkaskecilmasuk">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();		
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode_coa*************************************
		var kode_coa="#kode_coa_"+n;
		var new_kode_coa="#kode_coa_"+no;
		$(kode_coa, lastRow).attr("id", "kode_coa_"+no);
		$(new_kode_coa, lastRow).attr("name", "kode_coa_"+no);		
		$(new_kode_coa, lastRow).attr("onkeyup", "cari('"+no+"', this.value);");		
		$(new_kode_coa, lastRow).val('');		
		//*****end kode_coa*********************************
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_akunting.iddata.value="+no;			 
		 var  even_klik= " openCenteredWindow('<?php echo base_url(); ?>index.php/akunting/cform/show_popup_coa/"+no+"');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		
		
		//******div infocoa*************************************
		var infocoa="#infocoa_"+n;
		var new_infocoa="#infocoa_"+no;
		$(infocoa, lastRow).attr("id", "infocoa_"+no);

		$(new_infocoa, lastRow).html("<input type='hidden' id='id_coa_"+no+"' name='id_coa_"+no+"' value=''>"+
		"<input type='text' id='nama_coa_"+no+"' name='nama_coa_"+no+"' value='' readonly='true' size='40'>");
		
		//*****imgtgl*************************************
		var imgtgl="#imgtgl_"+n;
		var new_imgtgl="#imgtgl_"+no;
		$(imgtgl, lastRow).attr("id", "imgtgl_"+no);
		$(new_imgtgl, lastRow).attr("name", "imgtgl_"+no);
		var  even_klik= "displayCalendar(document.forms[0].tgl_"+no+",'dd-mm-yyyy',this)";
		$(new_imgtgl, lastRow).attr("onclick",even_klik);	
		//*****end imgtgl*********************************
		
		//*****tgl*************************************
		var tgl="#tgl_"+n;
		var new_tgl="#tgl_"+no;
		$(tgl, lastRow).attr("id", "tgl_"+no);
		$(new_tgl, lastRow).attr("name", "tgl_"+no);		
		$(new_tgl, lastRow).val('');		
		//*****end tgl*********************************
		
		//*****deskripsi*************************************
		var deskripsi="#deskripsi_"+n;
		var new_deskripsi="#deskripsi_"+no;
		$(deskripsi, lastRow).attr("id", "deskripsi_"+no);
		$(new_deskripsi, lastRow).attr("name", "deskripsi_"+no);		
		$(new_deskripsi, lastRow).val('');				
		//*****end deskripsi*************************************	
			//*****ibukti*************************************
		var ibukti="#ibukti_"+n;
		var new_ibukti="#ibukti_"+no;
		$(ibukti, lastRow).attr("id", "ibukti_"+no);
		$(new_ibukti, lastRow).attr("name", "ibukti_"+no);		
		$(new_ibukti, lastRow).val('');				
		//*****end new_ibukti*************************************	
		
		//*****jumlah*************************************
		var jumlah="#jumlah_"+n;
		var new_jumlah="#jumlah_"+no;
		$(jumlah, lastRow).attr("id", "jumlah_"+no);
		$(new_jumlah, lastRow).attr("name", "jumlah_"+no);		
		$(new_jumlah, lastRow).val('0');				
		//*****end jumlah*************************************	
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
		
});
</script>

<script type="text/javascript">
	
function openCenteredWindow(url) {

	var width = 720;
	var height = 480;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
	myWindow = window.open(url, "subWind", windowFeatures);
}	
	

function cari(posisi, kodecoa) {
	$.ajax({ 
				type:'post', url: '<?php echo base_url();?>index.php/akunting/cform/caricoa2', 
				data: 'kode_coa='+kodecoa+'&posisi='+posisi, success: function(response) {
					$("#infocoa_"+posisi).html(response);
			}});
}

function cek_data() {
	var jumlah= $('#jumlah').val();
	
	kon = window.confirm("Yakin akan simpan data ini ??");
	if (kon) {
		if (jumlah == '0') {
			alert("Jumlah Belum di Isi..!");
			$('#jumlah').focus();
			return false;
		}
			document.f_akunting.submit();
	}
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_akunting" id="f_akunting" action="<?php echo base_url(); ?>index.php/akunting/cform/simpan" method="post" enctype="multipart/form-data">
<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<input type="hidden" name="ipvtype" id="ipvtype" value="00">
<table width="100%" cellspacing="2" cellpadding="1" >	
  <tr>
		<td width="13%">Periode (bulan-tahun)</td>
		<td>: <select  readonly name="bulan" id="bulan">
							<option <? if($bulan=='01') echo 'selected'; ?>>Januari</option>
							<option <? if($bulan=='02') echo 'selected'; ?>>Februari</option>
							<option <? if($bulan=='03') echo 'selected'; ?>>Maret</option>
							<option <? if($bulan=='04') echo 'selected'; ?>>April</option>
							<option <? if($bulan=='05') echo 'selected'; ?>>Mei</option>
							<option <? if($bulan=='06') echo 'selected'; ?>>Juni</option>
							<option <? if($bulan=='07') echo 'selected'; ?>>Juli</option>
							<option <? if($bulan=='08') echo 'selected'; ?>>Agustus</option>
							<option <? if($bulan=='09') echo 'selected'; ?>>September</option>
							<option <? if($bulan=='10') echo 'selected'; ?>>Oktober</option>
							<option <? if($bulan=='11') echo 'selected'; ?>>November</option>
							<option <? if($bulan=='12') echo 'selected'; ?>>Desember</option>
			</select>
		  <input name="tahun" type="text" id="tahun" size="4" value="<?= $tahun?>" readonly> <i>*Contoh: 2020</i>
		</td>
	 <tr>
		<td>Area</td>
		<td>:
			<input name="area" type="text" id="area" size="10" value="<?=$area?>" readonly="true">
		</td>
	</tr>	
  </tr>
  <tr>
	  <td>Tanggal Kas Kecil</td>
  <td>: 
	<label>
      <input name="dkk" type="text" id="dkk" size="10" value="<?=$dkk?>" readonly="true">
    </label>
	</td>
	</tr>

	<tr>
	  <td>Tanggal Bukti</td>
  <td>: 
	<label>
      <input name="dkb" type="text" id="dkb" size="10" value="<?=$dkb?>" readonly="true">
    </label>
	</td>
	</tr>
	
	<tr>
	  <td>Saldo</td>
  <td>: 
	<label>
      <input name="saldo" type="text" id="saldo" size="10" value="<?=$saldo?>" readonly="true">
    </label>
	</td>
	</tr>

	<tr>
	  <td>Jumlah</td>
  <td>: 
	<label>
      <input name="jumlah" type="text" id="jumlah" size="10" value="0">
    </label>
	</td>
	</tr>
	
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<input type="button" name="simpan_data" id="simpan_data" value="Simpan" onclick="cek_data();">&nbsp;
	<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/akunting/cform/viewkaskecilmasuk'">
	
</form>
