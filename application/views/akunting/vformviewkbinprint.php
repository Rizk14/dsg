<style type="text/css">
  table.isi {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
	}
	
</style>

<h3>Cetak Bank (Masuk)</h3><br>

<a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbankin">View Data</a>
<a href="<?php echo base_url(); ?>index.php/akunting/cform/cetakvoucherkbin">Kembali</a><br><br>
<div>
<!-- Total Data = <#?php echo $jum_total ?><br><br> -->
<?php echo form_open('akunting/cform/caribankin/'.$periode.'/'.$bank.'/'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td width="45%">Masukkan Nomor Voucher</td>
		<td><input type="text" name="cari" size="20" value="<?php echo $cari; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
	</tr>
</table></fieldset>

<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="39%" class="isi">
	<thead>
	 <tr class="judulnya">
		<th style ="text-align:center">No</th>
		<th style ="text-align:center">Kode Voucher</th>
		<th style ="text-align:center">Tgl Voucher</th>
		<th style ="text-align:center">Jumlah</th>
		<th style ="text-align:center">Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
		 $i = 1;
			 foreach ($query as $row){
				$ada = 0;
				echo "<tr class=\"record\">
						<td align='center'>".$i."</td>
						<td>".$row->i_rv."</td>
						<td>".$row->d_rv."</td>
						<td align='right'>".number_format($row->v_rv, 2, ',','.')."</td>
						<td align=center>
							<a href='#' onclick='eval(window.open(\"".site_url()."/akunting/cform/cetak/$row->i_rv/$row->kode_area/02/01\",\"_blank\",\"width=1366px,height=768px,resizable=1,scrollbars=1,top=(screen.height-768)/2,left=(screen.width-1366)/2\"))' id='$row->i_rv'>Cetak
							</a>
						</td>
					</tr>";
				$i++;
		 	}
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
