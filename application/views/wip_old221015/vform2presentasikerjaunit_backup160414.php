<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Presentasi Kerja Unit</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function hitungnilai() {
	var jum_detail= $('#jum_data').val();
	var i=1; var gtotal = 0;
		for (i=1;i<=jum_detail;i++) {
			var hari=$("#hari_"+i).val();
			var kapasitas=$("#kapasitas_"+i).val();
			var minggu1=$("#minggu1_"+i).val();
			var minggu2=$("#minggu2_"+i).val();
			var minggu3=$("#minggu3_"+i).val();
			var minggu4=$("#minggu4_"+i).val();

			var total = parseInt(minggu1)+parseInt(minggu2)+parseInt(minggu3)+parseInt(minggu4);
			var persentase = ((parseInt(hari)-parseInt(total))*parseInt(kapasitas))/(parseInt(hari)*parseInt(kapasitas));
			persentase = persentase.toFixed(2);
			$("#total_"+i).val(total);
			$("#persentase_"+i).val(persentase);
										
			gtotal = parseFloat(gtotal)+parseFloat(persentase);
		}
		var gtotalx = parseFloat(gtotal/jum_detail);
		gtotalx = gtotalx.toFixed(2);
		$("#gtotal").val(gtotalx);
}

function cek_input() {
	var jum_data = $('#jum_data').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data presentasi kerja unit ??");
		
	if (kon) {
		if (jum_data > 0) {
			for (var i=1; i <= jum_data; i++) {
				var hari=$("#hari_"+i).val();
				var kapasitas=$("#kapasitas_"+i).val();
				var minggu1=$("#minggu1_"+i).val();
				var minggu2=$("#minggu2_"+i).val();
				var minggu3=$("#minggu3_"+i).val();
				var minggu4=$("#minggu4_"+i).val();
				var total=$("#total_"+i).val();
				var persentase=$("#persentase_"+i).val();
				
				if (isNaN($('#hari_'+k).val() || $('#hari_'+k).val() == '') ) {
					alert("Data hari harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				
				if (isNaN($('#kapasitas_'+k).val() || $('#kapasitas_'+k).val() == '') ) {
					alert("Data kapasitas harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				
				if (isNaN($('#minggu1_'+k).val() || $('#minggu1_'+k).val() == '') ) {
					alert("Data minggu 1 harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				
				if (isNaN($('#minggu2_'+k).val() || $('#minggu2_'+k).val() == '') ) {
					alert("Data minggu 2 harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				
				if (isNaN($('#minggu3_'+k).val() || $('#minggu3_'+k).val() == '') ) {
					alert("Data minggu 3 harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				
				if (isNaN($('#minggu4_'+k).val() || $('#minggu4_'+k).val() == '') ) {
					alert("Data minggu 4 harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				if (isNaN($('#total_'+k).val() || $('#total_'+k).val() == '') ) {
					alert("Data total harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
				if (isNaN($('#persentase_'+k).val() || $('#persentase_'+k).val() == '') ) {
					alert("Data persentase harus berupa angka atau desimal..!");
					s=1;
					return false;
				}
			}
			if (isNaN($('#gtotal').val()) ) {
				alert("Data total rata-rata harus berupa angka atau desimal..!");
				s=1;
				return false;
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
	
}
</script>

<div>
Unit Jahit: <?php echo $unit_jahit." - ".$nama_unit; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_unit', 'id' => 'f_unit');
echo form_open('wip/creport/submitpresentasikerjaunit', $attributes);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">
<input type="hidden" name="jum_data" id="jum_data" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Hari</th>
		 <th>Kode & Nama Barang Jadi</th>
		 <th>Grup Jahit</th>
		 <th>Kapasitas</th>
		 <th>Minggu 1</th>
		 <th>Minggu 2</th>
		 <th>Minggu 3</th>
		 <th>Minggu 4</th>
		 <th>Total/Bulan</th>
		 <th>Persentase</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){					
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td><input type='text' name='hari_$i' id='hari_$i' value='".$query[$j]['hari']."' size='2' style='text-align: right;' onkeyup='hitungnilai()' onblur='hitungnilai();'></td>";
					 echo    "<td >".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."
					 <input type='hidden' name='kode_brg_jadi_$i' id='kode_brg_jadi_$i' value='".$query[$j]['kode_brg_jadi']."'>
					 <input type='hidden' name='iddetail_$i' id='iddetail_$i' value='".$query[$j]['id']."'>
					 </td>"; ?>
					<td>
						<select name="nama_grup_<?php echo $i ?>" id="nama_grup_<?php echo $i ?>">
							<option value="" <?php if($query[$j]['nama_grup_jahit'] == '') { ?> selected <?php } ?> >- Pilih - </option>
						<?php foreach ($list_grup_jahit as $grp) { ?>
							<option value="<?php echo $grp->nama_grup ?>" <?php if($query[$j]['nama_grup_jahit'] == $grp->nama_grup) { ?> selected <?php } ?> ><?php echo $grp->nama_grup ?></option>
						<?php } ?>
						</select>
					</td>
			<?php
					 echo    "<td><input type='text' name='kapasitas_$i' id='kapasitas_$i' value='".$query[$j]['kapasitas']."' size='2' style='text-align: right;' onkeyup='hitungnilai()' onblur='hitungnilai();'></td>";
					echo    "<td><input type='text' name='minggu1_$i' id='minggu1_$i' value='".$query[$j]['minggu1']."' size='2' style='text-align: right;' onkeyup='hitungnilai()' onblur='hitungnilai();'></td>";
					echo    "<td><input type='text' name='minggu2_$i' id='minggu2_$i' value='".$query[$j]['minggu2']."' size='2' style='text-align: right;' onkeyup='hitungnilai()' onblur='hitungnilai();'></td>";
					echo    "<td><input type='text' name='minggu3_$i' id='minggu3_$i' value='".$query[$j]['minggu3']."' size='2' style='text-align: right;' onkeyup='hitungnilai()' onblur='hitungnilai();'></td>";
					echo    "<td><input type='text' name='minggu4_$i' id='minggu4_$i' value='".$query[$j]['minggu4']."' size='2' style='text-align: right;' onkeyup='hitungnilai()' onblur='hitungnilai();'></td>";
					echo    "<td><input type='text' name='total_$i' id='total_$i' value='".$query[$j]['total']."' size='2' style='text-align: right;' readonly='true'></td>";
					echo    "<td><input type='text' name='persentase_$i' id='persentase_$i' value='".$query[$j]['persentase']."' size='5' style='text-align: right;' readonly='true'></td>";
				
					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		<tr>
			<td colspan="10" align="right"><b>Rata-rata Persentase Schedule / Bulan</b></td>
			<td><input type="text" name="gtotal" id="gtotal" size="5" value="<?php if (is_array($query)) { echo $query[0]['total_rata']; } else echo "0"; ?>" readonly="true" style="text-align:right;"></td>
		</tr>
 	</tbody>
</table><br>
<input type="submit" name="submit" value="Simpan" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Data Periode Ini" onclick="return confirm('Yakin akan hapus data periode ini ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/addpresentasikerjaunit'">
<?php echo form_close();  ?>
</div>
