<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Presentasi Kerja Unit</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">
	
// 16-04-2014
function tambahgrup(posisi) {
	//inisialisasi untuk id unik
		var no=$("#jum_data_"+posisi).val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku_"+posisi+" tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		
		//*****nama_grup*************************************
		var nama_grup="#nama_grup_"+posisi+"_"+n;
		var new_nama_grup="#nama_grup_"+posisi+"_"+no;
		$(nama_grup, lastRow).attr("id", "nama_grup_"+posisi+"_"+no);
		$(new_nama_grup, lastRow).attr("name", "nama_grup_"+posisi+"_"+no);
		//*****end nama_grup*********************************
												
		//*****hari*************************************
		var hari="#hari_"+posisi+"_"+n;
		var new_hari="#hari_"+posisi+"_"+no;
		$(hari, lastRow).attr("id", "hari_"+posisi+"_"+no);
		$(new_hari, lastRow).attr("name", "hari_"+posisi+"_"+no);
		$(new_hari, lastRow).val('0');
		//*****end hari*********************************
								
		//*****kapasitas*************************************
		var kapasitas="#kapasitas_"+posisi+"_"+n;
		var new_kapasitas="#kapasitas_"+posisi+"_"+no;
		$(kapasitas, lastRow).attr("id", "kapasitas_"+posisi+"_"+no);
		$(new_kapasitas, lastRow).attr("name", "kapasitas_"+posisi+"_"+no);
		$(new_kapasitas, lastRow).val('0');
		//*****end kapasitas*********************************
		
		//*****minggu1*************************************
		var minggu1="#minggu1_"+posisi+"_"+n;
		var new_minggu1="#minggu1_"+posisi+"_"+no;
		$(minggu1, lastRow).attr("id", "minggu1_"+posisi+"_"+no);
		$(new_minggu1, lastRow).attr("name", "minggu1_"+posisi+"_"+no);
		$(new_minggu1, lastRow).val('0');
		//*****end minggu1*********************************
		
		//*****minggu2*************************************
		var minggu2="#minggu2_"+posisi+"_"+n;
		var new_minggu2="#minggu2_"+posisi+"_"+no;
		$(minggu2, lastRow).attr("id", "minggu2_"+posisi+"_"+no);
		$(new_minggu2, lastRow).attr("name", "minggu2_"+posisi+"_"+no);
		$(new_minggu2, lastRow).val('0');
		//*****end minggu2*********************************
		
		//*****minggu3*************************************
		var minggu3="#minggu3_"+posisi+"_"+n;
		var new_minggu3="#minggu3_"+posisi+"_"+no;
		$(minggu3, lastRow).attr("id", "minggu3_"+posisi+"_"+no);
		$(new_minggu3, lastRow).attr("name", "minggu3_"+posisi+"_"+no);
		$(new_minggu3, lastRow).val('0');
		//*****end minggu3*********************************
		
		//*****minggu4*************************************
		var minggu4="#minggu4_"+posisi+"_"+n;
		var new_minggu4="#minggu4_"+posisi+"_"+no;
		$(minggu4, lastRow).attr("id", "minggu4_"+posisi+"_"+no);
		$(new_minggu4, lastRow).attr("name", "minggu4_"+posisi+"_"+no);
		$(new_minggu4, lastRow).val('0');
		//*****end minggu4*********************************
		
		//*****total*************************************
		var total="#total_"+posisi+"_"+n;
		var new_total="#total_"+posisi+"_"+no;
		$(total, lastRow).attr("id", "total_"+posisi+"_"+no);
		$(new_total, lastRow).attr("name", "total_"+posisi+"_"+no);
		$(new_total, lastRow).val('0');
		//*****end total*********************************
		
		//*****persentase*************************************
		var persentase="#persentase_"+posisi+"_"+n;
		var new_persentase="#persentase_"+posisi+"_"+no;
		$(persentase, lastRow).attr("id", "persentase_"+posisi+"_"+no);
		$(new_persentase, lastRow).attr("name", "persentase_"+posisi+"_"+no);
		$(new_persentase, lastRow).val('0');
		//*****end persentase*********************************
												
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku_"+posisi).append(lastRow);
		//jum_data++
		var x=parseInt(no);
		$("#jum_data_"+posisi).val(x+1);	
}

function hapusgrup(posisi) {
	var x= $("#jum_data_"+posisi).val();	
		if (x>2) {
			$("#tabelku_"+posisi+" tr:last").remove();	 
			var x= $("#jum_data_"+posisi).val();	
			var y= x-1;
			$("#jum_data_"+posisi).val(y);	
		}
}

function hitungnilai() {
	
	// new 17-04-2014 =====================
	var jum_total= $('#jum_total').val();
	var gtotal = 0;
	var i=1; var j=1; var xx=0;
	
	for (i=1; i<=jum_total;i++) {
		var jum_detail= $('#jum_data_'+i).val()-1;
		for (j=1;j<=jum_detail;j++) {
			var hari=$("#hari_"+i+"_"+j).val();
			var kapasitas=$("#kapasitas_"+i+"_"+j).val();
			var minggu1=$("#minggu1_"+i+"_"+j).val();
			var minggu2=$("#minggu2_"+i+"_"+j).val();
			var minggu3=$("#minggu3_"+i+"_"+j).val();
			var minggu4=$("#minggu4_"+i+"_"+j).val();

			var total = parseInt(minggu1)+parseInt(minggu2)+parseInt(minggu3)+parseInt(minggu4);
			var persentase = ((parseInt(hari)-parseInt(total))*parseInt(kapasitas))/(parseInt(hari)*parseInt(kapasitas));
			if (isNaN(persentase))
				persentase=0;
			persentase = persentase*100;
			persentase = persentase.toFixed(2);
			$("#total_"+i+"_"+j).val(total);
			$("#persentase_"+i+"_"+j).val(persentase);
			gtotal = parseFloat(gtotal)+parseFloat(persentase);
			
			if (persentase != 0)
				xx++;
		}
	}
	//alert(gtotal); alert(xx);
	var gtotalx = parseFloat(gtotal/xx);
	if (isNaN(gtotalx))
		gtotalx=0;
	gtotalx = gtotalx.toFixed(2);
	$("#gtotal").val(gtotalx);
	// ===============================================
		
	//----------------------- eksperimen gagal pake array, karena bingung output ke htmlnya, dikomen aja --------------------------------------
/*	var harix = new Array();
	var kapasitasx = new Array();
	var minggu1x = new Array();
	var minggu2x = new Array();
	var minggu3x = new Array();
	var minggu4x = new Array();
	var totalx = new Array();
	var persentasex = new Array();
	
	var xx=0;
	$('.hari').each(function(){ 
		harix[xx]= $(this).val();
		xx=xx+1;
	});
	
	xx=0;
	$('.kapasitas').each(function(){ 
		kapasitasx[xx]= $(this).val();
		xx=xx+1;
	});
	
	xx=0;
	$('.minggu1').each(function(){ 
		minggu1x[xx]= $(this).val();
		xx=xx+1;
	});
	
	xx=0;
	$('.minggu2').each(function(){ 
		minggu2x[xx]= $(this).val();
		xx=xx+1;
	});
	
	xx=0;
	$('.minggu3').each(function(){ 
		minggu3x[xx]= $(this).val();
		xx=xx+1;
	});
	
	xx=0;
	$('.minggu4').each(function(){ 
		minggu4x[xx]= $(this).val();
		xx=xx+1;
	});
	
	//for (i=0; i<$(".hari").length; i++) {
	for (i=0; i<harix.length; i++) {
		var total = parseInt(minggu1x[i])+parseInt(minggu2x[i])+parseInt(minggu3x[i])+parseInt(minggu4x[i]);
		var persentase = ((parseInt(harix[i])-parseInt(totalx[i]))*parseInt(kapasitasx[i]))/(parseInt(harix[i])*parseInt(kapasitasx[i]));
		persentase = persentase.toFixed(2);
		
		//$("#total_"+i).val(total);
		//$("#persentase_"+i).val(persentase);
									
		gtotal = parseFloat(gtotal)+parseFloat(persentase);
	} */
}

function cek_input() {
	var jum_total= $('#jum_total').val();
	var s = 0;
	kon = window.confirm("Yakin akan simpan data presentasi kerja unit ??");
		
	if (kon) {
		// new 17-04-2014 =====================
		if (jum_total > 0) {
			for (var i=1; i <= jum_total; i++) {
				var jum_detail= $('#jum_data_'+i).val()-1;
				for (j=1;j<=jum_detail;j++) {
					var hari=$("#hari_"+i+"_"+j).val();
					var kapasitas=$("#kapasitas_"+i+"_"+j).val();
					var minggu1=$("#minggu1_"+i+"_"+j).val();
					var minggu2=$("#minggu2_"+i+"_"+j).val();
					var minggu3=$("#minggu3_"+i+"_"+j).val();
					var minggu4=$("#minggu4_"+i+"_"+j).val();
					var total=$("#total_"+i+"_"+j).val();
					var persentase=$("#persentase_"+i+"_"+j).val();
					
					if (isNaN(hari) || hari == '' ) {
						alert("Data hari harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
					
					if (isNaN(kapasitas) || kapasitas == '' ) {
						alert("Data kapasitas harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
					
					if (isNaN(minggu1) || minggu1 == '' ) {
						alert("Data minggu 1 harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
					
					if (isNaN(minggu2) || minggu2 == '' ) {
						alert("Data minggu 2 harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
					
					if (isNaN(minggu3) || minggu3 == '' ) {
						alert("Data minggu 3 harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
					
					if (isNaN(minggu4) || minggu4 == '' ) {
						alert("Data minggu 4 harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
					if (isNaN(total) || total == '' ) {
						alert("Data total harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
					if (isNaN(persentase) || persentase == '' ) {
						alert("Data persentase harus berupa angka atau desimal..!");
						s=1;
						return false;
					}
				}
								
				
			}
			if (isNaN($('#gtotal').val()) ) {
				alert("Data total rata-rata harus berupa angka atau desimal..!");
				s=1;
				return false;
			}
		}
		else {
			alert("Data barang tidak ada");
			s=1;
			return false;
		}
		
		if (s == 0)
			return true;
	} // didieu
	else
		return false;
	
}
</script>

<div>
Unit Jahit: <?php echo $unit_jahit." - ".$nama_unit; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_unit', 'id' => 'f_unit');
echo form_open('wip/creport/submitpresentasikerjaunit', $attributes);
 ?>
<input type="hidden" name="is_new" id="is_new" value="<?php echo $is_new; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>">
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun ?>">
<input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">
<input type="hidden" name="jum_total" id="jum_total" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode & Nama Barang Jadi</th>
		 <th>Detail Per Grup Jahit</th>
		<!-- <th>Grup Jahit</th>
		 <th>Hari</th>
		 <th>Kapasitas</th>
		 <th>Minggu 1</th>
		 <th>Minggu 2</th>
		 <th>Minggu 3</th>
		 <th>Minggu 4</th>
		 <th>Total/Bulan</th>
		 <th>Persentase</th> -->
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){					
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td >".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."
					 <input type='hidden' name='kode_brg_jadi_$i' id='kode_brg_jadi_$i' value='".$query[$j]['kode_brg_jadi']."'>
					 <input type='hidden' name='iddetail_$i' id='iddetail_$i' value='".$query[$j]['id']."'>
					 </td>
					 ";
					 
					$var_detail = $query[$j]['datapergrup'];

		?>	
		<td>
			<input type="hidden" name="jum_data_<?php echo $i ?>" id="jum_data_<?php echo $i ?>" value="<?php if(is_array($var_detail)) echo count($var_detail)+1; else echo '2'; ?>">
		<input id="additem" type="button" name="additem" value=" + " title="Tambah Grup Jahit" onclick="tambahgrup(<?php echo $i ?>)">&nbsp;
		<input id="deleteitem" type="button" name="deleteitem" value=" - " title="Hapus Grup Jahit" onclick="hapusgrup(<?php echo $i ?>)">
		<br><br>
			<table border="1" cellpadding= "1" cellspacing = "1" width="100%" id="tabelku_<?php echo $i ?>">
				<thead>
				 <tr class="judulnya">
					<th>Grup Jahit</th>
					 <th>Hari</th>
					 <th>Kapasitas</th>
					 <th>Minggu 1</th>
					 <th>Minggu 2</th>
					 <th>Minggu 3</th>
					 <th>Minggu 4</th>
					 <th>Total/Bulan</th>
					 <th>Persentase</th>
				 </tr>
				</thead>
				<tbody>	
					 <!-- disini isinya -->
					 <?php 
					 $z=1;
					 if (is_array($var_detail)) {
						for($k=0;$k<count($var_detail);$k++){
					 ?>
					 <tr>
						 <td>
							<select name="nama_grup_<?php echo $i ?>_<?php echo $z ?>" id="nama_grup_<?php echo $i ?>_<?php echo $z ?>">
								<option value="" <?php if($var_detail[$k]['nama_grup_jahit'] == '') { ?> selected <?php } ?> >- Pilih - </option>
							<?php foreach ($list_grup_jahit as $grp) { ?>
								<option value="<?php echo $grp->nama_grup ?>" <?php if($var_detail[$k]['nama_grup_jahit'] == $grp->nama_grup) { ?> selected <?php } ?> ><?php echo $grp->nama_grup ?></option>
							<?php } ?>
							</select>
						</td>
						<td><input type="text" class="hari" name="hari_<?php echo $i ?>_<?php echo $z ?>" id="hari_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['hari'] ?>" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();" ></td>
						<td><input type="text" class="kapasitas" name="kapasitas_<?php echo $i ?>_<?php echo $z ?>" id="kapasitas_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['kapasitas'] ?>" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu1" name="minggu1_<?php echo $i ?>_<?php echo $z ?>" id="minggu1_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['minggu1'] ?>" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu2" name="minggu2_<?php echo $i ?>_<?php echo $z ?>" id="minggu2_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['minggu2'] ?>" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu3" name="minggu3_<?php echo $i ?>_<?php echo $z ?>" id="minggu3_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['minggu3'] ?>" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu4" name="minggu4_<?php echo $i ?>_<?php echo $z ?>" id="minggu4_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['minggu4'] ?>" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="total" name="total_<?php echo $i ?>_<?php echo $z ?>" id="total_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['total'] ?>" size="2" style="text-align: right;" readonly="true"></td>
						<td><input type="text" class="persentase" name="persentase_<?php echo $i ?>_<?php echo $z ?>" id="persentase_<?php echo $i ?>_<?php echo $z ?>" value="<?php echo $var_detail[$k]['persentase'] ?>" size="5" style="text-align: right;" readonly="true"></td>
					
					</tr>
					<?php 
								$z++;
							}
						}
						else {
					?>
					
					<tr>
						 <td>
							<select name="nama_grup_<?php echo $i ?>_1" id="nama_grup_<?php echo $i ?>_1">
								<option value="" >- Pilih - </option>
							<?php foreach ($list_grup_jahit as $grp) { ?>
								<option value="<?php echo $grp->nama_grup ?>" ><?php echo $grp->nama_grup ?></option>
							<?php } ?>
							</select>
						</td>
						<td><input type="text" class="hari" name="hari_<?php echo $i ?>_1" id="hari_<?php echo $i ?>_1" value="0" size="2" style="text-align: right;" onkeyup="hitungnilai();" ></td>
						<td><input type="text" class="kapasitas" name="kapasitas_<?php echo $i ?>_1" id="kapasitas_<?php echo $i ?>_1" value="0" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu1" name="minggu1_<?php echo $i ?>_1" id="minggu1_<?php echo $i ?>_1" value="0" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu2" name="minggu2_<?php echo $i ?>_1" id="minggu2_<?php echo $i ?>_1" value="0" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu3" name="minggu3_<?php echo $i ?>_1" id="minggu3_<?php echo $i ?>_1" value="0" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="minggu4" name="minggu4_<?php echo $i ?>_1" id="minggu4_<?php echo $i ?>_1" value="0" size="2" style="text-align: right;" onkeyup="hitungnilai();" onblur="hitungnilai();"></td>
						<td><input type="text" class="total" name="total_<?php echo $i ?>_1" id="total_<?php echo $i ?>_1" value="0" size="2" style="text-align: right;" readonly="true"></td>
						<td><input type="text" class="persentase" name="persentase_<?php echo $i ?>_1" id="persentase_<?php echo $i ?>_1" value="0" size="5" style="text-align: right;" readonly="true"></td>
					
					</tr>
					
					<?php
						}
					?>
				</tbody>
			</table><br>
		</td>		 
		
			<?php
					 $i++;
				}
			}
		 ?>
		<tr>
			<td colspan="3" align="right"><b>Rata-rata Persentase Schedule / Bulan</b> &nbsp;&nbsp;<input type="text" name="gtotal" id="gtotal" size="5" value="<?php if (is_array($query)) { echo $query[0]['total_rata']; } else echo "0"; ?>" readonly="true" style="text-align:right;">&nbsp;&nbsp;</td>
		</tr>
 	</tbody>
</table><br>
<input type="submit" name="submit" value="Simpan" onclick="return cek_input();">&nbsp;
<?php if ($is_new != 1) { ?>
<input type="submit" name="submit2" value="Hapus Data Periode Ini" onclick="return confirm('Yakin akan hapus data periode ini ??')">&nbsp;
<?php } ?>
<input type="button" name="batal" value="Kembali" onClick="window.location='<?php echo base_url(); ?>index.php/wip/creport/addpresentasikerjaunit'">
<?php echo form_close();  ?>
</div>
