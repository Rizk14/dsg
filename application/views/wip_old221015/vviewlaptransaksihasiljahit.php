<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Transaksi Barang Unit Jahit</h3><br><br>

<div>
Lokasi Gudang: <?php if ($id_gudang!= 0) { echo "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang; } else echo "Semua"; ?><br>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>

<?php 
//$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
//echo form_open('wip/creport/export_excel_transaksiunitjahit', $attributes); ?>
<!--<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >
<input type="hidden" name="nama_bulan" value="<?php echo $nama_bulan ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)"> -->
<?php //echo form_close();  ?>
<!--<br><br>-->
<?php
	$nomor = 1;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			echo "<b>".$query[$a]['kode_brg_jadi']." - ".$query[$a]['nama_brg_jadi']."</b>"."<br>";
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		 <th width="5%" rowspan="2">Tgl</th>
		 <th width="5%" rowspan="2">Ket</th>
		 <th width="10%" colspan="2">No Bukti</th>
		 <th width="10%" colspan="2">Unit Jahit</th>
		 <th width="10%" colspan="2">Packing/Gdg Jadi</th>
		 <th width="10%" rowspan="2">Saldo</th>
	 </tr>
	 <tr class="judulnya">
		<th>Masuk</th>
		<th>Keluar</th>
		<th>Masuk</th>
		<th>Keluar</th>
		<th>Masuk</th>
		<th>Keluar</th>

	</tr>
	</thead>
	<tbody>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
			<?php $data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					echo "&nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['jum_stok_opname']."<br>";
				}
			}
		?>
			</td>
		</tr>
			
<?php
		$data_tabel1 = $query[$a]['data_tabel1'];
		if (is_array($data_tabel1)) {
			for($j=0;$j<count($data_tabel1);$j++){
?>
	<tr>
		<td><?php echo "&nbsp;".$data_tabel1[$j]['tgl_sj'] ?></td>
		<td><?php if ($data_tabel1[$j]['masuk'] == "ya") echo "&nbsp;Masuk"; else echo "&nbsp;Keluar"; ?></td>
		<td><?php if ($data_tabel1[$j]['masuk'] == "ya") echo "&nbsp;".$data_tabel1[$j]['no_sj']; else echo "&nbsp;"; ?></td>
		<td><?php if ($data_tabel1[$j]['keluar'] == "ya") echo "&nbsp;".$data_tabel1[$j]['no_sj']; else echo "&nbsp;"; ?></td>
		<td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['is_masukwipunit'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['is_keluarwipunit'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 
		 <td><?php if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['is_masukwippacking'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 <td><?php if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['is_keluarwippacking'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
					}
				}
			}
		 ?>
		 </td>
		 
		 <td><?php
				$data_tabel1_saldo_perwarna = $data_tabel1[$j]['data_tabel1_saldo_perwarna'];
				if (is_array($data_tabel1_saldo_perwarna)) {
					for($z=0;$z<count($data_tabel1_saldo_perwarna);$z++){
						echo "&nbsp;".$data_tabel1_saldo_perwarna[$z]['nama_warna'].": ".$data_tabel1_saldo_perwarna[$z]['saldo']."<br>";
					}
				}
			
			?>
		 </td>
	</tr>
	<?php
			}
		}
	?>
		<tr>
			<td colspan="2" align="center"><b>Total/Bulan</b></td>
			<td colspan="2" align="center"><b>Total</b></td>
			<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk1']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar1']."<br>";
				}
			}
		?></td>
		
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_masuk2']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['tot_keluar2']."<br>";
				}
			}
		?></td>
		<td><?php $data_warna = $query[$a]['data_warna'];
			if (is_array($data_warna)) {
				for($zz=0;$zz<count($data_warna);$zz++){ 
					echo "&nbsp;".$data_warna[$zz]['nama_warna'].": ".$data_warna[$zz]['saldo']."<br>";
				}
			}
		?></td>
		</tr>
	</tbody>
	</table>

			
			<br><br>
	<?php
		}
	}
	?>

</div>
