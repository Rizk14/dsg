<h3>Input Stok Awal WIP (Hasil Jahit)</h3><br>
<a href="<? echo base_url(); ?>index.php/wip/cform/addstokawal">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/wip/cform/viewstokawal">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">
 
 function cek_data() {
	var stok= $('#stok').val();

	if (stok == '') {
		alert("Stoknya harus diisi..!");
		$('#stok').focus();
		return false;
	}

 }	

</script>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('wip/cform/editstokawal', $attributes); ?>
<input type="hidden" name="id_stok" value="<?php echo $id_stok ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<input type="hidden" name="is_simpan" value="1">

<table width="50%">
		<tr>
			<td colspan="2">Edit Data Stok Awal</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td width="30%">Gudang</td>
			<td>: <?php echo $gudang ?>
			</td>
		</tr>
		<tr>
			<td>Barang Jadi</td>
			<td>: <?php echo $brg_jadi ?>
			</td>
		</tr>
		<tr>
			<td>Stok</td>
			<td>: <input size="5" type="text" name="stok" id="stok" value="<?php echo $stok ?>"> Pieces
			</td>
		</tr>

</table><br>
<?php 
	$url_redirectnya = "wip/cform/viewstokawal/".$carinya."/".$cur_page;
?>
<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<?php echo form_close(); ?>
