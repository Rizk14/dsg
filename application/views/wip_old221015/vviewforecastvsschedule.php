<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Forecast vs Schedule</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_forecast', 'id' => 'f_forecast');
echo form_open('wip/creport/export_excel_forecastvsschedule', $attributes); ?>
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="nama_bulan" value="<?php echo $nama_bulan ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode</th>
		 <th>Nama Barang Jadi</th>
		 <th>FC Produksi</th>
		 <th>Schedule</th>
		 <th>Barang Masuk</th>
		 <th>%Produksi <br>vs Schedule</th>
		 <th>%Produksi <br>vs Forecast</th>
		 <th>Keterangan</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){					
					 echo "<tr class=\"record\">";
					 echo "<td align='center' id='num_$i'>$i</td>";
		?>
					<td>&nbsp;<?php echo $query[$j]['kode_brg_jadi'] ?></td>
					<td>&nbsp;<?php echo $query[$j]['nama_brg_jadi'] ?></td>
					 <td align="right"><?php echo $query[$j]['fc'] ?>&nbsp;</td>
					 <td align="right"><?php echo $query[$j]['schedule'] ?>&nbsp;</td>
					 <td align="right"><?php echo $query[$j]['brgmasuk'] ?>&nbsp;</td>
					 <td align="right"><?php echo number_format($query[$j]['persenschedule'], 2, '.',',') ?> %&nbsp;</td>
					 <td align="right"><?php echo number_format($query[$j]['persenforecast'], 2, '.',',') ?> %&nbsp;</td>
					 <td><?php echo $query[$j]['keterangan'] ?></td>
					</tr>
		<?php
					 $i++;
				}
			}
			
	?>
	
 	</tbody>
</table><br>
</div>
