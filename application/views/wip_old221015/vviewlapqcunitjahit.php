<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan QC Barang Unit Jahit</h3><br><br>

<div>
Periode: <?php echo $nama_bulan." ".$tahun ?><br><br>

<?php 
$attributes = array('name' => 'f_transaksi', 'id' => 'f_transaksi');
echo form_open('wip/creport/export_excel_qcunitjahit', $attributes); ?>
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="nama_bulan" value="<?php echo $nama_bulan ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	<tr class="judulnya">
		<th colspan="9" align="center">BARANG BAGUS</td>
	</tr>
	 <tr class="judulnya">
		 <th>No </th>
		 <th>Unit Jahit</th>
		 <th>Kode</th>
		 <th>Nama Barang Jadi</th>
		<th>Qty</th>
		<th>Retur</th>
		<th>Grade A</th>
		<th>% Grade A</th>
		<th>% Retur</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 0;
			$jumlah=0; $cbrg=0;
			$temp_kode_unit = "";
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){					
					 echo "<tr class=\"record\">";
					 
					 if ($query[$j]['kode_unit_jahit'] != $temp_kode_unit) {
						$temp_kode_unit = $query[$j]['kode_unit_jahit'];
						$i++;
						echo "<td align='center'>$i</td>";
						echo "<td>&nbsp;".$query[$j]['kode_unit_jahit']." - ".$query[$j]['nama_unit_jahit']."</td>";
					 }
					 else {
						 echo "<td align='center'>&nbsp;</td>";
						 echo "<td>&nbsp;</td>";
					 }
					 
					 $jumlah+= $query[$j]['persen_retur'];
					 $cbrg++;
		?>
					
					<td>&nbsp;<?php echo $query[$j]['kode_brg_jadi'] ?></td>
					<td>&nbsp;<?php echo $query[$j]['nama_brg_jadi'] ?></td>
					 <td align="right"><?php echo $query[$j]['qty'] ?>&nbsp;</td>
					 <td align="right"><?php echo $query[$j]['retur'] ?>&nbsp;</td>
					 <td align="right"><?php echo $query[$j]['gradea'] ?>&nbsp;</td>
					 <td align="right"><?php echo number_format($query[$j]['persena'], 2, '.',',') ?> %&nbsp;</td>
					 <td align="right"><?php echo number_format($query[$j]['persen_retur'], 2, '.',',') ?> %&nbsp;</td>
					</tr>
		<?php
				
					if (isset($query[$j+1]['kode_unit_jahit']) && ($query[$j]['kode_unit_jahit'] != $query[$j+1]['kode_unit_jahit'])) {
						$rataperunit = $jumlah/$cbrg;
		?>
					<tr>
						<td colspan="8" align="right"><b>Rata-Rata Retur Unit <?php echo $query[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
					</tr>
	<?php
						$jumlah=0; $cbrg=0;
					}
					else if ( !isset($query[$j+1]['kode_unit_jahit'])) {
						$rataperunit = $jumlah/$cbrg;
		?>
						<tr>
						<td colspan="8" align="right"><b>Rata-Rata Retur Unit <?php echo $query[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
					</tr>
		<?php			
						$jumlah=0; $cbrg=0;
					}
				}
			} // end if
	?>
	
 	</tbody>
</table><br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	<tr class="judulnya">
		<th colspan="9" align="center">BARANG PERBAIKAN</td>
	</tr>
	 <tr class="judulnya">
		 <th>No </th>
		 <th>Unit Jahit</th>
		 <th>Kode</th>
		 <th>Nama Barang Jadi</th>
		<th>Qty</th>
		<th>Retur</th>
		<th>Grade A</th>
		<th>% Grade A</th>
		<th>% Retur</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 0;
			$jumlah=0; $cbrg=0;
			$temp_kode_unit = "";
			if (is_array($query2)) {
				for($j=0;$j<count($query2);$j++){					
					 echo "<tr class=\"record\">";
					 if ($query2[$j]['kode_unit_jahit'] != $temp_kode_unit) {
						$temp_kode_unit = $query2[$j]['kode_unit_jahit'];
						$i++;
						echo "<td align='center'>$i</td>";
						echo "<td>&nbsp;".$query2[$j]['kode_unit_jahit']." - ".$query2[$j]['nama_unit_jahit']."</td>";
					 }
					 else {
						 echo "<td align='center'>&nbsp;</td>";
						 echo "<td>&nbsp;</td>";
					 }
					 
					 $jumlah+= $query2[$j]['persen_retur'];
					 $cbrg++;
		?>
					
					<td>&nbsp;<?php echo $query2[$j]['kode_brg_jadi'] ?></td>
					<td>&nbsp;<?php echo $query2[$j]['nama_brg_jadi'] ?></td>
					 <td align="right"><?php echo $query2[$j]['qty'] ?>&nbsp;</td>
					 <td align="right"><?php echo $query2[$j]['retur'] ?>&nbsp;</td>
					 <td align="right"><?php echo $query2[$j]['gradea'] ?>&nbsp;</td>
					 <td align="right"><?php echo number_format($query2[$j]['persena'], 2, '.',',') ?> %&nbsp;</td>
					 <td align="right"><?php echo number_format($query2[$j]['persen_retur'], 2, '.',',') ?> %&nbsp;</td>
					</tr>
	<?php
	
				
					if (isset($query2[$j+1]['kode_unit_jahit']) && ($query2[$j]['kode_unit_jahit'] != $query2[$j+1]['kode_unit_jahit'])) {
						$rataperunit = $jumlah/$cbrg;
		?>
					<tr>
						<td colspan="8" align="right"><b>Rata-Rata Retur Unit <?php echo $query2[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
					</tr>
	<?php
						$jumlah=0; $cbrg=0;
					}
					else if ( !isset($query2[$j+1]['kode_unit_jahit'])) {
						$rataperunit = $jumlah/$cbrg;
		?>
						<tr>
						<td colspan="8" align="right"><b>Rata-Rata Retur Unit <?php echo $query2[$j]['nama_unit_jahit'] ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($rataperunit, 2, '.',',') ?> %&nbsp;</b></td>
					</tr>
		<?php			
						$jumlah=0; $cbrg=0;
					}
				}
			} // end if
	?>
	
 	</tbody>
</table>

</div>
