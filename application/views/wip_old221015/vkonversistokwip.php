<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 680;
		var height = 580;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var gudang1= $('#gudang1').val();
	var gudang2= $('#gudang2').val();
	var adawarna1= $('#adawarna1').val();
	var adawarna2= $('#adawarna2').val();
	var kode_brg_jadi1= $('#kode_brg_jadi1').val();
	var kode_brg_jadi2= $('#kode_brg_jadi2').val();
	var idwarna1= $('#idwarna1').val();
	var idwarna2= $('#idwarna2').val();
	var s = 0;
	kon = window.confirm("Yakin akan lakukan konversi stok ??");
	if (kon) {
		if (gudang1 == '') {
			alert("Gudang Awal harus dipilih..!");
			$('#gudang1').focus();
			s = 1;
			return false;
		}
		if (kode_brg_jadi1 == '') {
			alert("Barang Jadi Awal harus dipilih..!");
			$('#kode_brg_jadi1').focus();
			s = 1;
			return false;
		}
		if (adawarna1 == '0') {
			alert("Barang Jadi Awal blm ada warnanya, silahkan input dulu di Master warna barang jadi..!");
			s = 1;
			return false;
		}
		
		if (gudang2 == '') {
			alert("Gudang Tujuan harus dipilih..!");
			$('#gudang2').focus();
			s = 1;
			return false;
		}
		if (kode_brg_jadi2 == '') {
			alert("Barang Jadi Tujuan harus dipilih..!");
			$('#kode_brg_jadi2').focus();
			s = 1;
			return false;
		}
		
		if ((kode_brg_jadi1 == kode_brg_jadi2) && (gudang1 == gudang2)) {
			alert("Barang Jadi Awal harus beda dgn Barang Jadi Tujuan jika gudangnya sama. Atau Gudang Awal harus beda dgn Gudang Tujuan jika kode barang awal dan tujuan sama ..!");
			$('#kode_brg_jadi2').focus();
			s = 1;
			return false;
		}
		
		if (adawarna2 == '0') {
			alert("Barang Jadi Tujuan blm ada warnanya, silahkan input dulu di Master warna barang jadi..!");
			s = 1;
			return false;
		}
		
		if (idwarna1 != idwarna2) {
			alert("Warna Barang Jadi Awal dan Warna Barang Jadi Tujuan tidak sama. Silahkan samakan dulu di Master warna barang jadi..!");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	}
	else
		return false;
}

	
</script>

<h3>Konversi Stok Barang WIP di Perusahaan</h3><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_konversi', 'id' => 'f_konversi');
	echo form_open('wip/cgudang/submitkonversistok', $attributes); ?>
	<table>
		<tr>
			<td colspan="2"><i>Keterangan: <br>Konversi Stok ini untuk memindahkan semua stok dari kode barang yg lama ke yg baru (dgn lokasi gudang yg sama),<br>
			atau bisa juga untuk memindahkan semua stok dari satu gudang ke gudang lain (dgn kode barang yg sama)
			</i></td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td width="15%">Nomor Bukti Konversi</td>
			<td>
			  <input name="no_bukti" type="text" id="no_bukti" size="10" maxlength="20" readonly="true" value="<?php echo $no_bukti ?>">
			</td>			
		  </tr>
		  
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
		<td width="15%" style="white-space:nowrap;">Lokasi Gudang Awal</td>
		<td> <select name="gudang1" id="gudang1">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if($gud->id == '22') { ?> selected <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	
		<tr>
			<td>Barang Jadi Awal</td>
			<td> <input type="text" name="brg_jadi1" id="brg_jadi1" value="" size="40" readonly="true" >
			<input type="hidden" name="kode_brg_jadi1" id="kode_brg_jadi1" value="">
			<input type="hidden" name="adawarna1" id="adawarna1" value="0">
			<input type="hidden" name="idwarna1" id="idwarna1" value="">
			<input title="browse data barang" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: var gudang1= $('#gudang1').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/wip/cgudang/show_popup_brgjadi/1/'+gudang1);" >
			</td>
		</tr>
		
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
		<td width="15%" style="white-space:nowrap;">Lokasi Gudang Tujuan</td>
		<td> <select name="gudang2" id="gudang2">
				<?php foreach ($list_gudang as $gud) { ?>
					<option value="<?php echo $gud->id ?>" <?php if($gud->id == '22') { ?> selected <?php } ?> ><?php echo "[".$gud->nama_lokasi."] ". $gud->kode_gudang."-".$gud->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	
		<tr>
			<td>Barang Jadi Tujuan</td>
			<td> <input type="text" name="brg_jadi2" id="brg_jadi2" value="" size="40" readonly="true" >
			<input type="hidden" name="kode_brg_jadi2" id="kode_brg_jadi2" value="">
			<input type="hidden" name="adawarna2" id="adawarna2" value="0">
			<input type="hidden" name="idwarna2" id="idwarna2" value="">
			<input title="browse data barang" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: var gudang2= $('#gudang2').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/wip/cgudang/show_popup_brgjadi/2/'+gudang2);" >
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Simpan Konversi" onclick="return cek_data();">
		</tr>
		
	</table>
<?php echo form_close(); ?> <br>
