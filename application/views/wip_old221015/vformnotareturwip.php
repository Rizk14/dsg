<h3>Data Nota Retur Barang WIP (Hasil Jahit)</h3><br>
<a href="<? echo base_url(); ?>index.php/wip/cform/notareturwipadd">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/wip/cform/notareturwipview">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	//$("#no").val('2');
	
	//hitung_total();
	
	$('#unit_jahit').change(function(){
		$("#sj_keluar").val('');
	  	$("#id_sj").val('');	
	  });
	  
	  hitungnilai();
		
});
</script>
<script type="text/javascript">

// 24-07-2013
function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

function openCenteredWindow(url) {

		var width = 1200;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_item_brg() {
	var sj_keluar= $('#sj_keluar').val();
		
	if (sj_keluar == '') {
		alert("Nomor SJ Keluar harus dipilih..!");
		return false;
	}
}

function cek_input() {
	var no_nota= $('#no_nota').val();
	var tgl= $('#tgl_nota').val();
	
	var s = 0;
	kon = window.confirm("Yakin akan simpan data ini ??");
	
	if (kon) {
		
		if (no_nota == '') {
			alert("Nomor Nota Retur harus diisi..!");
			s = 1;
			return false;
		}
		if (tgl == '') {
			alert("Tanggal Nota Retur harus dipilih..!");
			s = 1;
			return false;
		}
		
		var jum= $('#no').val()-1; 

		if (jum > 0) {
			for (var k=1; k <= jum; k++) {
				
				if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
					alert("Data harga tidak boleh 0 / kosong...!");
					s = 1;
					return false;
				}
				if (isNaN($('#harga_'+k).val()) ) {
					alert("Harga harus berupa angka..!");
					s = 1;
					return false;
				}
				
			}
		}
		else {
			alert("Data detail tidak ada");
			s = 1;
			return false;
		}
		
		if (s == 0)
			return true;
	} //
	else
		return false;
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;
	var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty_hasil=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					var hitung = (harga*qty_hasil)-diskon;
					
					// 24-07-2013
					$("#total_"+i).val(hitung);
					var hitungformat = formatMoney(hitung, 2,',','.');
					$("#total2_"+i).val(hitungformat);
					
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				
				$("#gtotal").val(gtotal);
				var hitungformat = formatMoney(gtotal, 2,',','.');
				$("#gtotal2").val(hitungformat);
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
	$attributes = array('name' => 'f_wip', 'id' => 'f_wip');
	echo form_open('wip/cform/notareturwipadd', $attributes);
?>
<table>
	<tr>
		<td>Unit Jahit</td>
		<td>: <select name="unit_jahit" id="unit_jahit" onkeyup="this.blur();this.focus();">
				<?php foreach ($list_unit_jahit as $uj) { ?>
					<option value="<?php echo $uj->kode_unit ?>"><?php echo $uj->kode_unit." - ". $uj->nama ?></option>
				<?php } ?>
			</select></td> 
</tr>
	<tr>
		<td>Nomor SJ Keluar</td>
		<td>: <input name="sj_keluar" type="text" id="sj_keluar" size="40" readonly="true" value=""/>
			<input name="id_sj" type="hidden" id="id_sj" value=""/>
			<input title="browse data SJ keluar WIP" name="pilih" value="..." type="button" id="pilih" 
           onclick="javascript: var x= $('#unit_jahit').val(); openCenteredWindow('<?php echo base_url(); ?>index.php/wip/cform/show_popup_sj_keluar/'+ x);" type="button"></td>
	</tr>
	
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/wip/cform/notareturwipview'">
<?php echo form_close(); 
} else { ?>


<form name="f_wip" id="f_wip" action="<?php echo base_url(); ?>index.php/wip/cform/notareturwipsubmit" method="post" enctype="multipart/form-data">
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>">

<?php 
		if (count($sj_detail)>0) {
			$no=1;
			foreach ($sj_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<div align="center">

<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
<tr>
		<td>Unit Jahit</td>
		<td><?php echo $unit_jahit." - ". $nama_unit ?></td> 
</tr>
  <tr>
    <td>No SJ Keluar</td>
    <td>
      <input name="no_sj_keluar" type="text" id="no_sj_keluar" size="30" readonly="true" value="<?php echo $no_sj_keluar; ?>">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Nomor Nota</td>
    <td>
      <input name="no_nota" type="text" id="no_nota" size="10" maxlength="20" value="<?php echo $no_nota ?>">&nbsp;
    </td>
    
  </tr>
  <tr>
    <td>Tgl Nota</td>
    <td>
	<label>
      <input name="tgl_nota" type="text" id="tgl_nota" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_nota" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_nota,'dd-mm-yyyy',this)">
	</td>
  </tr>
  
  <tr>
    <td>Keterangan</td>
    <td>
      <input name="ket" type="text" id="ket" size="30" maxlength="60" value="">&nbsp;
    </td>
    
  </tr>
	
	<tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
        <tr>
          <th width="20">No</th>
          <th>No / Tgl SJ Keluar</th>
           <th>Kode & Nama Barang Jadi</th>
	      <th>Qty (Pcs)</th>
	      <th>Harga (Rp.)</th>
	      <th>Diskon (Rp.)</th>
	      <th>Total (Rp.)</th>
        </tr>

        <?php $i=1;
        if (count($sj_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($sj_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          
          <td nowrap="nowrap">
           <input name="no_sj_keluar_<?php echo $i ?>" type="text" id="no_sj_keluar_<?php echo $i ?>" size="25" readonly="true" value="<?php echo $sj_detail[$j]['no_sj']." / ".$sj_detail[$j]['tgl_sj'] ?>"/>
           <input type="hidden" name="id_sj_keluar_detail_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['id'] ?>" >
           </td>
          
          <td nowrap="nowrap">
           <input name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="40" readonly="true" value="<?php echo  $sj_detail[$j]['kode_brg_jadi']." - ".$sj_detail[$j]['nama_brg_jadi'] ?>"/>
           <input type="hidden" name="kode_brg_jadi_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['kode_brg_jadi'] ?>" >
           </td>
          
          <td><input name="qty_<?php echo $i ?>" type="text" style="text-align:right" id="qty_<?php echo $i ?>" size="5" maxlength="5" 
          value="<?php echo $sj_detail[$j]['qty'] ?>" readonly="true"  /> </td>
          
          <td nowrap="nowrap">
           <input name="harga_<?php echo $i ?>" type="text" style="text-align:right" id="harga_<?php echo $i ?>" size="10" value="<?php echo $sj_detail[$j]['harganya'] ?>" onkeyup="hitungnilai()" onblur="hitungnilai()"/>
           <input name="harga_lama_<?php echo $i ?>" type="hidden" id="harga_lama_<?php echo $i ?>" value="<?php echo $sj_detail[$j]['harganya'] ?>" />
          </td>
          <td nowrap="nowrap">
           <input name="diskon_<?php echo $i ?>" type="text" style="text-align:right" id="diskon_<?php echo $i ?>" size="10" value="0" onkeyup="hitungnilai()" onblur="hitungnilai()"/>
          </td>
          
          <td nowrap="nowrap">
           <input readonly="true" name="total2_<?php echo $i ?>" type="text" style="text-align:right" id="total2_<?php echo $i ?>" size="10" value=""/>
           <input name="total_<?php echo $i ?>" type="hidden" id="total_<?php echo $i ?>" value=""/>
          </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
	</table>	
	
	</form>
</td>
    </tr>
    <tr>
		<td colspan="2">Grand Total: &nbsp;&nbsp;<input type="text" name="gtotal2" id="gtotal2" value="0" size="10" readonly="true">
			<input type="hidden" name="gtotal" id="gtotal" value="0">
		</td>
	</tr>
    <tr>
		<td colspan="2" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/wip/cform/notareturwipadd'"></td>
	</tr>
	
</table>
</div>
</form>
<?php } ?>
