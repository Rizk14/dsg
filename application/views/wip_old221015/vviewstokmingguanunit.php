<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Stok Mingguan Unit Jahit</h3><br><br>

<div>
Unit Jahit: <?php if ($unit_jahit!= 0) { echo $unit_jahit."-".$nama_unit; } else echo "Semua"; ?><br>
Tanggal Sekarang: <?php echo date("d-m-Y"); ?>
<br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('wip/creport/export_excel_stokmingguanunit', $attributes); ?>
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<?php
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			echo "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b>"."<br>";
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="60%">
	<thead>
	 <tr class="judulnya">
		 <th width='3%'>No</th>
		 <th width='15%'>Kode</th>
		 <th width='35%'>Nama Brg</th>
		<th width='5%'>Stok Bagus</th>
		<th width='5%'>Stok Perbaikan</th>
	 </tr>
	</thead>
	<tbody>
			
<?php
			$detail_stok = $query[$a]['data_stok'];
			if (is_array($detail_stok)) {
				for($j=0;$j<count($detail_stok);$j++){
?>
			<tr>
				<td align="center"><?php echo ($j+1) ?></td>
				<td>&nbsp;<?php echo $detail_stok[$j]['kode_brg'] ?></td>
				<td>&nbsp;<?php echo $detail_stok[$j]['nama_brg'] ?></td>
				<td align="right"><?php echo number_format($detail_stok[$j]['stok_bagus'],0,',','.') ?>&nbsp;</td>
				<td align="right"><?php echo number_format($detail_stok[$j]['stok_perbaikan'],0,',','.') ?>&nbsp;</td>
			</tr>
<?php
				} // end for2
			} // end if2
?>
			</tbody>
			</table><br><br>
<?php
		} //end for1
	} // end if1
?>

</div>
