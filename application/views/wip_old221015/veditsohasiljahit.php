<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Informasi Stok Opname Barang WIP (Hasil Jahit) di Perusahaan</h3><br><br>

<div>
Lokasi Gudang: <?php echo "[".$query[0]['nama_lokasi']."] ".$query[0]['kode_gudang']." - ".$query[0]['nama_gudang'] ?><br>
Periode: <?php echo $query[0]['nama_bln']." ".$query[0]['tahun'] ?><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('wip/creport/updatesohasiljahit', $attributes);
$no = count($query);
 if ($ctahun == '') $ctahun = "0";
 $url_redirectnya = "wip/creport/viewsohasiljahit/".$cgudang."/".$cbulan."/".$ctahun."/".$cur_page;
 ?>
<input type="hidden" name="jum_data" id="jum_data" value="<?php if(is_array($query)) echo count($query); else echo '0'; ?>">
<input type="hidden" name="id_so" id="id_so" value="<?php echo $id_so ?>">

<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="cgudang" value="<?php echo $cgudang ?>">
<input type="hidden" name="cbulan" value="<?php echo $cbulan ?>">
<input type="hidden" name="ctahun" value="<?php echo $ctahun ?>">

Tanggal Pencatatan SO: 
      <input name="tgl_so" type="text" id="tgl_so" size="10" value="<?php echo $query[0]['tgl_so'] ?>" readonly="true">
	   <img alt="" id="tgl_so" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_so,'dd-mm-yyyy',this)">
<br><br>

<input type="submit" name="submit" value="Update Stok Opname" onclick="return confirm('Apakah anda yakin simpan data ini? Setelah disimpan, selanjutnya anda harus lakukan approval stok opname untuk mengupdate stok');">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="70%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode Brg Jadi</th>
		 <th>Nama Brg Jadi</th>
		 <th>Satuan</th>
		 <th>Stok Akhir</th>
		 <th>Jumlah Fisik</th>
	 </tr>
	</thead>
	<tbody>
		<?
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg_jadi']."
					 <input type='hidden' name='kode_brg_jadi_$i' id='kode_brg_jadi_$i' value='".$query[$j]['kode_brg_jadi']."'>
					 <input type='hidden' name='id_$i' id='id_$i' value='".$query[$j]['id']."'>
					 </td>";
					 echo    "<td>".$query[$j]['nama_brg_jadi']."</td>";
					 echo    "<td>Pieces</td>";
					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
							for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp; : ".$detailwarna[$zz]['saldo_akhir']."<br>";
						}
					}
					 echo "</td>";
					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
						for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp;";
						?>
						<input type="hidden" name="kode_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['kode_warna'] ?>">
						
						<input type="text" name="stok_fisik_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['stok_opname'] ?>" size="3" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
						
						<input type="hidden" name="jum_stok_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['saldo_akhir'] ?>"><br>
						<?php
						}
					}
					 echo "</td>";

					 echo  "</tr>";
					 $i++;
				}
			}
		 ?>
		
 	</tbody>
</table><br>

<?php echo form_close();  ?>
</div>
