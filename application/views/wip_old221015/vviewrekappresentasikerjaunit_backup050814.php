<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Rekapitulasi Presentasi Kerja Unit Tahunan</h3><br><br>

<div>
Tahun: <?php echo $tahun ?><br><br>

<?php 
$attributes = array('name' => 'f_presentasi', 'id' => 'f_presentasi');
echo form_open('wip/creport/export_excel_rekappresentasikerjaunit', $attributes); ?>
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width="3%" rowspan="2">No</th>
		 <th width="10%" rowspan="2">Unit</th>
		 <th width="10%" rowspan="2">Grup</th>
		 <th colspan="12">Bulan</th>
		 <th width="10%" rowspan="2">Rata-Rata</th>
	 </tr>
	 <tr class="judulnya">
		<th width="10%">Januari</th>
		<th width="10%">Februari</th>
		<th width="10%">Maret</th>
		<th width="10%">April</th>
		<th width="10%">Mei</th>
		<th width="10%">Juni</th>
		<th width="10%">Juli</th>
		<th width="10%">Agustus</th>
		<th width="10%">September</th>
		<th width="10%">Oktober</th>
		<th width="10%">November</th>
		<th width="10%">Desember</th>
	 </tr>
	</thead>
	<tbody>
<?php
	$nomor = 1; $temp_unit = ""; $totalperunit=0; //$jumgrupperunit=0;
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
?>
	<tr>
		<td align="center"><?php echo $nomor; ?></td>
		<td><?php 
			if ($temp_unit != $query[$a]['kode_unit']) {
				$temp_unit = $query[$a]['kode_unit'];
				echo "&nbsp;".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']; 
			}
			else
				echo "&nbsp;";
			?>
		</td>
		<td>&nbsp;<?php echo $query[$a]['nama_grup_jahit'] ?></td>
		<?php
			$listratabulan = $query[$a]['listratabulan'];
			
			$bulan=1; $total = 0;
			if (is_array($listratabulan)) {
				for($j=0;$j<count($listratabulan);$j++){
					//echo $bulan." ";
					$total+=$listratabulan[$j][$bulan];
		?>
		<td align="right"><?php echo number_format($listratabulan[$j][$bulan], 2, '.',',') ?>&nbsp;</td>
		<?php		$bulan++;
				}
				$totrata = $total/12;
				$totalperunit+=$totrata;
			}
		?>
		<td align="right"><?php echo number_format($totrata, 2, '.',',') ?>&nbsp;</td>
	</tr>
			
	<?php 		$nomor++;
				
				if ( isset($query[$a+1]['kode_unit']) && ($query[$a]['kode_unit'] != $query[$a+1]['kode_unit']) ) {
					
	?>
			<tr>
				<td colspan="15" align="right">&nbsp;Subtotal Unit <?php echo $query[$a]['nama_unit'] ?>&nbsp;</td>
				<td align="right"><?php echo number_format($totalperunit, 2, '.',',') ?>&nbsp;</td>
			</tr>
	<?php				
					$totalperunit = 0;
				}
				else if ( !isset($query[$a+1]['kode_unit'])) {
	?>
			<tr>
				<td colspan="15" align="right">&nbsp;Subtotal Unit <?php echo $query[$a]['nama_unit'] ?>&nbsp;</td>
				<td align="right"><?php echo number_format($totalperunit, 2, '.',',') ?>&nbsp;</td>
			</tr>
	<?php			$totalperunit = 0;
				}
			}
	} 
	?>
	</tbody>
	</table>
</div>
