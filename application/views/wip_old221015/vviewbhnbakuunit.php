<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Bahan Baku Unit Jahit</h3><br><br>

<div>
Periode Tanggal SJ: <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('wip/creport/export_excel_bhnbakuunit', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>
<?php
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="50%">
	<thead>
	 <tr class="judulnya">
		 <th colspan="2" style="white-space:nowrap;"><?php echo "<b>".$query[$a]['kode_brg_jadi']." - ".$query[$a]['nama_brg_jadi']."<br> (Forecast ".$query[$a]['bln_forecast']." ".$query[$a]['thn_forecast'].": ".$query[$a]['qty_forecast']." ; Sisa: ".$query[$a]['sisa_forecast'].") </b>" ?></th>
	 </tr>
	</thead>
	<tbody>
			
<?php
			$detail_stok = $query[$a]['data_stok'];
			if (is_array($detail_stok)) {
				for($j=0;$j<count($detail_stok);$j++){
?>
			<tr>
				<td width="50%">&nbsp;<?php echo $detail_stok[$j]['kode_unit']." - ".$detail_stok[$j]['nama_unit'] ?></td>
				<td width="50%" align="right"><?php echo number_format($detail_stok[$j]['jum'],0,',','.') ?>&nbsp;</td>
			</tr>
<?php
				} // end for2
			} // end if2
?>
			</tbody>
			</table><br>
<?php
		} //end for1
	} // end if1
?>

</div>
