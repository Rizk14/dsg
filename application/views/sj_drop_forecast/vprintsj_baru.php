<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
   
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .isinya2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;}

</style>

<style type="text/css" media="print">
    .printable{ 
      page-break-after: always;
    }
    .no_print{
      display: none;
    }
</style>
<!-- media="screen" means these styles will only be used by screen 
  devices (e.g. monitors) -->
<style type="text/css" media="screen">
    .printable{
      display: none;
    }
</style>

<link href="<?php echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">

$(function()
{
	var go_print= jQuery('#go_print').val();
	
	if (go_print == '1')
		window.print();
});


</script>

<?php
	//$tgl = date("d-m-Y");
	// 05-08-2015 ganti jadi tgl OP
	$tgl = $query[0]['tgl_sj'];
	$pisah1 = explode("-", $tgl);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
?>
<input type="hidden" name="go_print" id="go_print" value="1" >
<table class="isinya2" border='1' align="center" width="100%">
	<tr>
	<td>
	<table class="isinya" border='0' align="center" width="90%">
		<tr>
			<td colspan="2"> <b>SURAT JALAN DROP FORECAST</b> </td>
			<td>&nbsp;</td>
			
			<td colspan="2" align ="right"><img src="<?php echo base_url().'images/logo.jpg'; ?>" style="width:84px;height:68px;" border="0"></td>
			<td colspan="2" align ='left'> <b><?php echo $datasetting['nama'] ?></b> </td>
		</tr>
		<tr>
		
			<td colspan="2">No SJ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo $query[0]['no_sj']?></td>
			<td>&nbsp;</td>
			
			<td>&nbsp;</td>
		</tr>
		
		<tr>
		<td colspan="1">Tanggal SJ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo $query[0]['tgl_sj']?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
		
		<tr>
			<td colspan="1">Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
		<tr>
			<td colspan="1">Keterangan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo $query[0]['keterangan']?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
		
		<tr>
			<td colspan="6">
				<table border='1' class="isinya2" width="100%" >
				<tr>
				  <th width="5%">No</th>
				   <th width="5%">Kode Barang</th>
				  <th width="40%">Nama Barang</th>
				  <th width="10%">Qty</th>
				   <th width="20%">Keterangan</th>
				</tr>
				<?php $i=1;
			$halaman = 1; 
			if (count($query[0]['detail_sj'])==0) {
			?>
			<tr align="center">
			  <td align="center" id="num_1">1</td>
			  <td nowrap="nowrap">
			   Data tidak ada</td>
			  
			</tr>
			
			<?php
			} else {
				$detailnya = $query[0]['detail_sj'];
				for($j=0;$j<count($query[0]['detail_sj']);$j++){
				?>
				<tr>
			  <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
			   <td style="white-space:nowrap;" align ='center'><?php echo $detailnya[$j]['kode_brg'];  ?></td>
			  <td style="white-space:nowrap;" align ='center'><?php echo $detailnya[$j]['nama'];  ?></td>
			  <td style="white-space:nowrap;" align="center"><?php echo number_format($detailnya[$j]['qty'],0,',','.') ?></td>
			  <?php 
			  
			   echo "<td nowrap>";
				 if (is_array($query[0]['detail_sj'])) {
					 $var_detail = array();
					 $var_detail = $query[0]['detail_sj'];
					 $hitung = count($var_detail);
					
						 $var_detail_warna = array();
						  $var_detail_warna = $var_detail[$j]['detail_sj_warna'];
						   $hitung_warna = count($var_detail_warna);
						   
						  for($l=0;$l<$hitung_warna; $l++){
						echo $var_detail_warna[$l]['warna']." : ". $var_detail_warna[$l]['qtywarna']."&nbsp;";
						  if ($l<$hitung_warna)
						   echo "<br> ";
					}
				 
				} 
				 echo "</td>";
			  
			  ?>
			  
			</tr>
			
			<?php 
			if ($i%30==0) {
			?>
				<tr>
					<td align="right" colspan="7"><?php echo "Hal. ".$halaman; $halaman++; ?></td>
				</tr>
			</table>
			</td></tr></table></td></tr></table>
			
			<br class="printable">	
			<table class="isinya2" border='1' align="center" width="100%">
	<tr>
	<td>
	<table class="isinya" border='0' align="center" width="90%">
		
		<tr>
			<td colspan="5">
				<table border='1' class="isinya2" width="100%" >
					<tr>
				  <th width="5%">No</th>
				  <th width="40%">Nama Barang</th>
				  <th width="5%">Satuan</th>
				  <th width="5%">Qty</th>
				  <th width="5%">Harga (Rp.)</th>
				  <!--<th width="10%">Diskon</th>
				  <th width="10%">PPN</th>-->
				  <th width="10%">Subtotal (Rp.)</th>
				   <th width="10%">Keterangan</th>
				</tr>
			
			<?php
					
				}
				
				$i++; } // end foreach 
			}
			?>
				
				</table>
		</td>
		</tr>
		<tr>
			
			<td colspan="6" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td  align="center">Tanda Terima</td>
			<td  align="center" colspan="2" align="left">Mengetahui</td>
			<td  align="center" colspan="2" align="left">Pembawa </td>
			<td  align="center">Hormat Kami </td>
		</tr>
		
		<tr>
			<td colspan="6" align="right">&nbsp;</td>
		</tr>
		
		<tr>
			<td  align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
			<td  align="center" colspan="2" align="left">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
			<td  align="center" colspan="2" align="left">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
			<td  align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
		</tr>
		</table>
	</td>
	</tr>
</table>
<!--<i><div class="isinya" align="right">Tanggal Cetak: <?php //echo date("d-m-Y H:i:s"); ?></i></div>-->
