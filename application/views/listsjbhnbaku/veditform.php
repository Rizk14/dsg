<script type="text/javascript" language="javascript">

function piproductitem(nItem) {
	
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	var iprocitem	= document.getElementById('i_product_hidden');
	var iproduct	= document.getElementById('i_product_tblItem_'+iteration);

}

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka! Terimakasih.");
		angka.value	= 0;
		angka.focus();
	}
}

function validNum2(iterasi) {
	var angka	= document.getElementById('n_unit_tblItem_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka. Terimakasih.");
		angka.value	= 0;
		angka.focus();
	}
}

function cksj(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listsjbhnbaku/cform/carisj');?>",
	data:"nsj="+nomor,
	success: function(data){
		$("#confnomorsj").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function getCabang(nilai) {
	var icustomer	= document.getElementById('i_customer_hidden');
	
	icustomer.value	= nilai;
	//alert(icustomer.value);
	
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listsjbhnbaku/cform/cari_cabang');?>",
	data:"ibranch="+nilai,
	success: function(data){
	$("#cboPelanggan").html(data);
	
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function showData(nItem) {
	if (document.getElementById(nItem+'_select').style.display=='none') {		
		document.getElementById(nItem+'_select').style.display='block';	
		document.getElementById(nItem+'_select').style.position='absolute';
	} else {
		document.getElementById(nItem).style.display='block';
		document.getElementById(nItem+'_select').style.display='none';
	}
}

function formatcemua(input){
	var num = input.toString();
	if(!isNaN(num)){
		if(num.indexOf('.') > -1){
			num = num.split('.');
			num[0] = num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
			if(num[1].length > 2){
				while(num[1].length > 2){
					num[1] = num[1].substring(0,num[1].length-1);
				}
			}
			input = num[0];  
		}else{
			input = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'')
		};
	}
	return input;
}

function tnilaibarang(){

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_sj_total');

	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseFloat(unitprice.value);
		}
	}
	//totalnilai.value	= totaln;
	//totalnilai.value	= formatcemua(totaln);
	//totalnilai.value	= Math.round(totaln);
	totalnilai.value	= totaln.toFixed(2);
}

function total(iterasi) {
	
	var	total;
	var	price;
	var	unit0;
	
	price	= document.getElementById('v_product_price_tblItem_'+iterasi);
	unit0	= document.getElementById('n_unit_tblItem_'+iterasi);
	
	if(parseFloat(price.value) && parseFloat(unit0.value)) {
		//total	= parseFloat(price.value) * parseFloat(unit0.value);
		total	= parseFloat(price.value) * parseFloat(unit0.value);
		//document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
		//document.getElementById('v_unit_price_tblItem_'+iterasi).value=formatcemua(total);		
		//document.getElementById('v_unit_price_tblItem_'+iterasi).value = Math.round(total);
		document.getElementById('v_unit_price_tblItem_'+iterasi).value = total.toFixed(2);
	}
}

function addRowToTable(nItem) {

	var d_sj	= document.getElementById('d_sj').value;
	var tspit	= d_sj.split('/');
	var d_sj_hidden = tspit[2]+'-'+tspit[1]+'-'+tspit[0];
		
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);

	var noiterasisjpbhnbaku = document.getElementById('noiterasisjpbhnbaku').value;
	var noiterasisjpbhnbaku2;
	var noiterasisj;
	
	if(iteration==0) {
		noiterasisj = noiterasisjpbhnbaku;
		
		document.getElementById('noiterasisjpbhnbaku').value = noiterasisj;
	}else{
		noiterasisj = parseInt(noiterasisjpbhnbaku)+1;
		
		document.getElementById('noiterasisjpbhnbaku').value = noiterasisj;
	}
	
	tbl.width='100%';
	
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	} else {
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;
	var gudang = document.querySelector('.getgudang').value;
	/* console.log(gudang); */
	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:10px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:192px;\" >" +
	"<input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\" name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:160px;\" value=\""+noiterasisj+"\" readonly >" +
	"<img name=\"img_i_do_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Item Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shproduksjpbhnbaku('"+iteration+"');\"></DIV>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:292px;\">" +
	"<input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:284px;\"></DIV>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_v_product_price_"+nItem+"_"+iteration+"\" style=\"width:125px;\">" +
	"<input type=\"text\" ID=\"v_product_price_"+nItem+"_"+iteration+"\"  name=\"v_product_price_"+nItem+"_"+iteration+"\" style=\"width:120px;text-align:right;\" value=\"0\"></DIV>";

	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_n_unit_"+nItem+"_"+iteration+"\" style=\"width:91px;\" >" +
	"<input type=\"text\" ID=\"n_unit_"+nItem+"_"+iteration+"\"  name=\"n_unit_"+nItem+"_"+iteration+"\" style=\"width:90px;text-align:right;\" value=\"0\" onkeyup=\"total("+iteration+");tnilaibarang();validNum('n_unit_tblItem','"+iteration+"');\" ></DIV>";

	var cell1 = row.insertCell(5);
	cell1.innerHTML = "<DIV ID=\"ajax_e_satuan_"+nItem+"_"+iteration+"\" style=\"width:91px;\" >" +
	"<input type=\"text\" ID=\"e_satuan_"+nItem+"_"+iteration+"\" name=\"e_satuan_"+nItem+"_"+iteration+"\" style=\"width:90px;text-align:right;\" onclick=\"shsatuanbhnbaku('"+iteration+"');\" readonly ></DIV>";
	
	var cell1 = row.insertCell(6);
	cell1.innerHTML = "<DIV ID=\"ajax_v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:91px;\">" +
	"<input type=\"text\" ID=\"v_unit_price_"+nItem+"_"+iteration+"\" name=\"v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:90px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();\" value=\"0\">" +
	"<input type=\"hidden\" ID=\"i_satuan_hidden_"+nItem+"_"+iteration+"\" name=\"i_satuan_hidden_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"hidden\" ID=\"i_code_references_"+nItem+"_"+iteration+"\" name=\"i_code_references_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"hidden\" ID=\"i_gudang_"+nItem+"_"+iteration+"\" name=\"i_gudang_"+nItem+"_"+iteration+"\" value = \""+gudang+"\" >" +
	"<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\"></DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}
</script>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#d_sj").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_sj; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_sj; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		  <?php 
		   
		  	$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listsjbhnbaku/cform/actedit', $attributes);?>
		  

		<div id="mastersjform">
		      <table border="0">
		      	<tr align="left">
			  <td width="16%"><?php echo $form_nomor_sj; ?></td>
			  <td width="1%">:</td>
			  <td width="33%">
				<?php
				 $nosj = array(
					'name'=>'i_sj',
					'id'=>'i_sj',
					'value'=>$isjcode,
					'maxlength'=>'14',
					'readonly'=>'true',
					'onkeyup'=>'cksj(this.value)'
					);
				 echo form_input($nosj);
				?>
				<div id="confnomorsj" style="color:#FF0000;"></div>
			  </td>
			  <td width="12%"><?php echo $form_tanggal_sj; ?></td>
			  <td width="1%">:</td>
			  <td width="36%">
				<?php
				 $tglsj = array(
					'name'=>'d_sj',
					'id'=>'d_sj',
					'value'=>$tsj,
					'maxlength'=>'10'
					);
				 echo form_input($tglsj);
				?>
			  </td>
			</tr>
			<tr>
			 <td colspan="6" width="100%">
			 	<table width="100%">
					<tr>
					  <td width="16%"><?php echo $form_cabang_sj; ?></td>
					  <td width="1%"></td>
					  <td width="25%">
						<?php
						$lpelanggan	.= "<select name=\"i_customer\" id=\"i_customer\" onchange=\"getCabang(this.value);\" disabled >"; 
						  $lpelanggan	.= "<option value=\"\" >[".$form_option_pel_sj."]</option>";
						  foreach($opt_pelanggan as $key => $row) {	
						  	$sel = $icustomer == $row->code? " selected " : "";
							$lpelanggan .= "<option value=\"$row->code\" $sel >".$row->customer."</option>";
						  }
						$lpelanggan	.= "</select>";
						echo $lpelanggan;
						?>					
						<input type="hidden" name="icustomer" id="icustomer" value="<?=$icustomer?>">	
					  </td>
					  <td colspan="3">
						<select name="i_branch" id="i_branch" disabled >
						<?php
						$lcabang	= "";
						$lcabang	.= "<option value=\"\">[Pilih Cabang Pelanggan]</option>";
						foreach($opt_cabang as $row2) {
							$Sel2	= $ibranch == $row2->ibranchcode ? " selected " : "";
							$lcabang .= "<option value=\"$row2->ibranchcode\" $Sel2 >".$row2->ebranchname."</option>";	
						}
						echo $lcabang;
						?>
						<input type="hidden" name="ibranch" id="ibranch" value="<?=$ibranch?>">
						</select>					  
					  </td>
					</tr>				
				</table>
			 </td>
			</tr>
			<tr><td colspan="6">&nbsp;</td></tr>
			<tr>
			 <td colspan="6">
				<div id="title-box2"><?php echo $form_title_detail_sj; ?>
				<div style="float:right;">
				<a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');tnilaibarang();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
				<a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');tnilaibarang();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
				</div></div>
			 </td>	
			</tr>
			<tr>
			 <td colspan="6">
				<table>
					<tr>
					 <td width="2%" class="tdatahead">NO</td>
					 <td width="20%" class="tdatahead"><?php echo $form_kode_produk_sjp_bhnbaku; ?></td>
					 <td width="30%" class="tdatahead"><?php echo $form_nm_produk_sjp_bhnbaku; ?></td>
					 <td width="13%" class="tdatahead"><?php echo $form_hjp_sjp_bhnbaku; ?></td>
					 <td width="10%" class="tdatahead"><?php echo $form_unit_sjp_bhnbaku; ?></td>
					 <td width="10%" class="tdatahead"><?php echo $form_satuan_sjp_bhnbaku; ?></td>
					 <td width="10%" class="tdatahead"><?php echo $form_total_sjp_bhnbaku; ?></td>		 					 
					</tr>
					<tr>
					 <td colspan="7">
					 	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
					 	
						<?php
						
						$total	= 0;
						$iter	= 0;
						$gudang	= '';
						
						foreach($sjitem as $row3) {
							
							echo "
							<tr>
								<td><div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">".($iter*1+1)."</div></td>
								<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:192px;\" >
								<input type=\"text\" ID=\"i_product_tblItem_".$iter."\" name=\"i_product_tblItem_".$iter."\" style=\"width:160px;\" value=\"".$row3->iproduct."\">
								<img name=\"img_i_do_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Item Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shproduksjpbhnbaku('".$iter."');\"></DIV></td>
								<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:292px;\">
								<input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\" name=\"e_product_name_tblItem_".$iter."\" style=\"width:284px;\" value=\"".$row3->productname."\"></DIV></td>
								<td><DIV ID=\"ajax_v_product_price_tblItem_".$iter."\" style=\"width:125px;\">
								<input type=\"text\" ID=\"v_product_price_tblItem_".$iter."\" name=\"v_product_price_tblItem_".$iter."\" style=\"width:120px;text-align:right;\" value=\"".$row3->hjp."\" onkeyup=\"total('".$iter."');tnilaibarang();\" ></DIV></td>
								<td><DIV ID=\"ajax_n_unit_tblItem_".$iter."\" style=\"width:91px;\" >
								<input type=\"text\" ID=\"n_unit_tblItem_".$iter."\"  name=\"n_unit_tblItem_".$iter."\" style=\"width:90px;text-align:right;\" value=\"".$row3->qtyakhir."\" onkeyup=\"validNum2('".$iter."');total('".$iter."');tnilaibarang();\" onfocus=\"total('".$iter."');tnilaibarang();\"></DIV></td>
								<td><DIV ID=\"ajax_e_satuan_tblItem_".$iter."\" style=\"width:91px;\" >
								<input type=\"text\" ID=\"e_satuan_tblItem_".$iter."\"  name=\"e_satuan_tblItem_".$iter."\" value=\"".$row3->esatuan."\" style=\"width:90px;text-align:right;\" onclick=\"shsatuanbhnbaku('".$iter."');\" readonly ></DIV></td>
								<td><DIV ID=\"ajax_v_unit_price_tblItem_".$iter."\" style=\"width:91px;\">
								<input type=\"text\" ID=\"v_unit_price_tblItem_".$iter."\" name=\"v_unit_price_tblItem_".$iter."\" style=\"width:90px;text-align:right;\" value=\"".round(($row3->hjp * $row3->qtyakhir),2)."\">
								<input type=\"hidden\" ID=\"i_satuan_hidden_tblItem_".$iter."\" name=\"i_satuan_hidden_tblItem_".$iter."\" value=\"".$row3->isatuan."\">
								<input type=\"hidden\" ID=\"i_code_references_tblItem_".$iter."\" name=\"i_code_references_tblItem_".$iter."\" value=\"".$row3->iprod."\">
								<input type=\"hidden\" ID=\"i_gudang_tblItem_".$iter."\" name=\"i_gudang_tblItem_".$iter."\" class =\"getgudang\" value=\"".$row3->i_gudang."\">
								<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$iter."\"></DIV></td></tr>
							";
							
							$harga = ($row3->hjp * $row3->qtyakhir);
							
							$gudang = $row3->i_gudang;
							
							
							$iter++;
							$ipro	= $row3->iproduct;
							$total += $harga;
						}
						?>
					 	</table>
					 </td>
					</tr>
					<tr>
					 <td colspan="7" align="right"><?php echo $form_total_nilai_brg_sj; ?> : Rp.&nbsp;&nbsp;&nbsp;
						<?php
						 $sj_total = array(
							'name'=>'v_sj_total',
							'id'=>'v_sj_total',
							'value'=>$total,
							'readonly'=>'true',
							'style'=>'text-align:right;'
							);
						 echo form_input($sj_total);
						?>
					 </td>
					</tr>
				</table>
			 </td>
			</tr>
			<tr valign="top">
			  <td width="16%"><?php echo $form_ket_sj; ?></td>
			  <td width="1%">:</td>
			  <td width="33%" colspan="5">
				<?php
				 $ket = array(
					'name'=>'e_note',
					'id'=>'e_note',
					'value'=>$enote,
					'cols'=>'2',
					'rows'=>'1'
					);
				 echo form_textarea($ket);
				?>
			  </td>
			</tr>
		      	<tr>
			  <td colspan="7" align="right">
			    <input type="hidden" name="isj" id="isj" value="<?=$isj?>" />
			    <input type="hidden" name="noiterasisjpbhnbaku" id="noiterasisjpbhnbaku" value="<?php echo $noiterasisjpbhnbaku; ?>" />
			    <input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit" />
			   <input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listpenjualanndo/cform/'">
</td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
