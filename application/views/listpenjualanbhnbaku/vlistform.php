<script type="text/javascript" src="<?php echo base_url(); ?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
		<td class="tcat"><?php echo $page_title_penjualanbhnbaku; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
		<td class="thead"><img src="<?php echo base_url(); ?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_penjualanbhnbaku; ?></td>
	</tr>
	<tr>
		<td class="alt2" style="padding:0px;">

			<table id="table-add-box">
				<tr>
					<td align="left">
						<?php
						echo $this->pquery->form_remote_tag(array('url' => 'listpenjualanbhnbaku/cform/aksi', 'update' => '#content', 'type' => 'post'));
						?>
						<div id="masterlpenjualanndoform">

							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<table width="98%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="20%"><?php echo $list_penjualanbhnbaku_no_faktur; ?></td>
												<td width="1%">:</td>
												<td>
													<input name="no_faktur" type="text" id="no_faktur" maxlength="14" value="<?= $nofaktur ?>" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
										<div id="title-box2"><?php echo $form_title_detail_penjualanbhnbaku; ?></div>
									</td>
								</tr>
								<tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="5%" class="tdatahead">NO</td>
												<td width="12%" class="tdatahead"><?php echo $list_penjualanbhnbaku_kd_brg; ?></td>
												<td width="35%" class="tdatahead"><?php echo $list_penjualanbhnbaku_nm_brg; ?> </td>
												<td width="10%" class="tdatahead"><?php echo $list_penjualanbhnbaku_qty; ?> </td>
												<td width="11%" class="tdatahead"><?php echo $list_penjualanbhnbaku_hjp; ?></td>
												<td width="13%" class="tdatahead"><?php echo $list_penjualanbhnbaku_amount; ?></td>
												<td width="2%" class="tdatahead" align="center"><?php echo $link_aksi; ?></td>
											</tr>

											<?php

											$totalqty	= 0;
											$totalpenjualan	= 0;
											$nomororder	= array();
											$arr	= 0;

											if (sizeof($query) > 0) {

												$no	= 1;
												$cc	= 1;

												foreach ($query as $row) {

													$Classnya	= $cc % 2 == 0 ? "row1" : "row2";
													$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

													$jmlFaktur	= $this->mclass->jmlFaktur($row->i_faktur_code);
													$njmlFaktur	= $jmlFaktur->num_rows();

													$nomororder[$arr]	= $row->i_faktur_code;

													if ($cc == 1) {
														$link_act	= "	<td align=\"center\" rowspan='$njmlFaktur'>";

														if ($row->f_printed == 't' && (($this->session->userdata('departemen') == "7" && $this->session->userdata('level') == "4") ||
															($this->session->userdata('departemen') == "8" && $this->session->userdata('level') == "4") ||
															$this->session->userdata('level') == "0" || $this->session->userdata('level') == "3")) {
															$link_act	.=	"	<a href=\"javascript:void(0)\" title=\"Cancel Print (Cetak Ulang)\" onclick='reprint(\"listpenjualanbhnbaku/cform/reprint/" . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . "\",\"#content\");'>
																					<img src=" . base_url() . "asset/images/button_print_cancel.png width=\"12\" height=\"15\" alt=\"Cancel Print\">
																				</a>&nbsp;";
														}

														if ($row->f_kontrabon == 'f') {
															$link_act	.= " <a href=" . base_url() . "index.php/listpenjualanbhnbaku/cform/edit/" . $row->i_faktur_code . trim($row->i_faktur_code) . "/" . trim($row->i_faktur) . " title=\"Edit Faktur\">
																				<img src=" . base_url() . "asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Faktur\" border=\"0\">
																			</a>&nbsp;
																			
																			<a href=" . base_url() . "index.php/listpenjualanbhnbaku/cform/undo/" . trim($row->i_faktur) . "/" . trim($row->i_faktur_code) . " title=\"Cancel faktur\">
																				<img src=" . base_url() . "asset/theme/images/cross.png width=12 height=13 alt=\"Cancel faktur\" border=\"0\">
																			</a>";
														}
													} elseif ($cc > 1 && $nomororder[$arr - 1] != $row->i_faktur_code) {
														$link_act	= "	<td align=\"center\" rowspan='$njmlFaktur' >";
														if ($row->f_kontrabon == 'f') {
															$link_act	.= "<a href=" . base_url() . "index.php/listpenjualanbhnbaku/cform/edit/" . $row->i_faktur_code . " title=\"Edit Faktur\">
																				<img src=" . base_url() . "asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Faktur\" border=\"0\">
																			</a>&nbsp;
																			
																			<a href=" . base_url() . "index.php/listpenjualanbhnbaku/cform/undo/" . trim($row->i_faktur) . "/" . trim($row->i_faktur_code)  . " title=\"Cancel faktur\">
																				<img src=" . base_url() . "asset/theme/images/cross.png width=12 height=13 alt=\"Cancel faktur\" border=\"0\">
																			</a>
																		</td>";
														}
													} else {
														$link_act	= '';
													}

													$lpenjndo .= "	<tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\" onMouseOut=\"this.className='$Classnya'\">
																		<td height=\"22px\" bgcolor=\"$bgcolor\">" . $no . "</td>
																		<td>" . $row->imotif . "/" . $row->i_code_references . "</td>
																		<td>" . $row->motifname . "</td>
																		<td align=\"right\">" . $row->qty . "</td>
																		<td align=\"right\">" . number_format($row->unitprice, '2', '.', ',') . "</td>
																		<td align=\"right\">" . number_format($row->amount, '2', '.', ',') . "</td>
																		$link_act								
																	</tr>";

													$no += 1;
													$cc++;
													$totalqty += $row->qty;
													$totalpenjualan += $row->amount;
													$arr += 1;
												}

												echo $lpenjndo;
											}

											?>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="right">
										<table width="98%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="40%" align="right"><?php echo $list_penjualanbhnbaku_total_pengiriman; ?> </td>
												<td width="1%">:</td>
												<td width="20%">
													<input name="v_t_pengiriman" type="text" id="v_t_pengiriman" maxlength="50" value="<?php echo $totalqty; ?>" />
												</td>
												<td width="20%" align="right"><?php echo $list_penjualanbhnbaku_total_penjualan; ?></td>
												<td width="1%">:</td>
												<td width="20%" align="right">
													<input name="v_t_penjualan" type="text" id="v_t_penjualan" maxlength="50" value="<?php echo number_format($totalpenjualan, '2', '.', ','); ?>" style="text-align:right;" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="right">
										<input name="btnkeluar" type="button" id="btnkeluar" value="<?php echo $button_keluar; ?>" onclick="show('listpenjualanbhnbaku/cform','#content')" />
									</td>
								</tr>
							</table>

						</div>
						<?php echo form_close() ?>
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>