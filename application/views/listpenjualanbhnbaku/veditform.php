<script language="javascript" type="text/javascript">

function fincludeppn() {
	if(document.getElementById('include_ppn').checked==true) {
		document.getElementById('f_include_ppn').value = 't';
	}else{
		document.getElementById('f_include_ppn').value = 'f';
	}
}

function getJatuhTempo(source,destination,range){ // d/m/Y
	var nilai;

	var tsplit	= source.split('/'); // d/m/Y
	var t	= parseInt(tsplit['0'])+parseInt(range);
	var tanggal = new Date(tsplit['2'],tsplit['1'],t,0,0,0); 

	var tgl	= tanggal.getDate();
	var bln = tanggal.getMonth();
	var thn = tanggal.getFullYear();

	nilai = tgl+'/'+bln+'/'+thn;
	destination.value	= nilai;
}

function tgpajak(tfaktur){
	document.getElementById('d_pajak').value=tfaktur;
}

function ckfpenjualan(nomor){	
	var ifak	= document.getElementById('i_faktur').value;
	
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listpenjualanndo/cform/cari_fpenjualan');?>",
	data:"fpenj="+nomor+"&foriginal="+ifak,
	success: function(data){
		$("#confnomorfpenj").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka!");
		angka.value	= 0;
		angka.focus();
	}
}

/*************************
Faktur Penjualan :D

Kasus :
total nilai = 10000
Diskon 	= 2 % dalam nilai 5000


Perhitungan :
Total 	= 10000 - (5000*2%)
		= 10000 - 4900
		= 5100

PPN 10 %	= 5100 + (5100*10%=510)

Grand Total	= 5610


-----------------------------------------------------
Koreksi kolom "dlm nilai" ===> "total hasil nilai"

Kasus :
total nilai = 10000
Diskon = 2 % hasil nilai = "9800"


Perhitungan :
nilai diskon 2 %	= (10000*2%)
					= 200

Total 		= 10000 - 200
			= 9800
			
PPN 10 %	= 9800*10%)
			= 980

Grand Total	= 9800 + 980
			= 10780
		
**************************/

function tnilaibarang(){

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');

	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseFloat(unitprice.value);
		}
	}
	//totalnilai.value	= Math.round(totaln);
	totalnilai.value	= totaln.toFixed(2);
}

function total(iterasi) {

	var	total;
	var	price;
	var	unit0;
	
	price	= document.getElementById('v_hjp_tblItem_'+iterasi);
	unit0	= document.getElementById('n_quantity_tblItem_'+iterasi);
	
	if(parseFloat(price.value) && parseFloat(unit0.value)) {
		total	= parseFloat(price.value) * parseFloat(unit0.value);
		//document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
		document.getElementById('v_unit_price_tblItem_'+iterasi).value = total.toFixed(2);
	}
}

function getdsk() {
	
	var nilai;
	var val;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;

	nilai	= document.getElementById('v_total_nilai');
	val		= document.getElementById('n_discount');

	if(parseFloat(nilai.value) && (parseInt(val.value) || parseFloat(val.value))) {
		
		if(document.getElementById('f_include_ppn').value=='t') {
			
			hasil 	= ((parseFloat(nilai.value)*parseFloat(val.value)) / 100);
			total_sblm_ppn	= parseFloat(nilai.value) - hasil;
			nilai_ppn	= ((total_sblm_ppn*11) / 100);
			total_grand	= total_sblm_ppn + nilai_ppn;
			
			/*
			document.getElementById('v_discount').value = hasil;
			document.getElementById('v_total_faktur').value	= total_sblm_ppn;
			document.getElementById('n_ppn').value	= nilai_ppn;
			document.getElementById('v_total_fppn').value	= total_grand;
			*/
			
			//document.getElementById('v_discount').value = Math.round(hasil);
			//document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
			//document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
			//document.getElementById('v_total_fppn').value	= Math.round(total_grand);

			document.getElementById('v_discount').value = hasil.toFixed(2);
			document.getElementById('v_total_faktur').value	= total_sblm_ppn.toFixed(2);
			document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
			document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);
		}else{
			
			hasil 	= ((parseFloat(nilai.value)*parseFloat(val.value)) / 100);
			total_sblm_ppn	= parseFloat(nilai.value) - hasil;
			nilai_ppn	= 0;
			total_grand	= total_sblm_ppn + nilai_ppn;

			document.getElementById('v_discount').value = hasil.toFixed(2);
			document.getElementById('v_total_faktur').value	= total_sblm_ppn.toFixed(2);
			document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
			document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);			
		}	
	} else {
		
		var xtotaln = 0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseFloat(xunitprice.value);
			}
		}
		//xtotalnilai.value	= Math.round(xtotaln);
		xtotalnilai.value	= xtotaln.toFixed(2);
		
		if(document.getElementById('f_include_ppn').value=='t') {
			
			nilai_ppn	= ((xtotaln*11) / 100);
			total_grand	= xtotaln + nilai_ppn;
			
			/*			
			document.getElementById('v_discount').value = 0;
			document.getElementById('v_total_faktur').value	= xtotaln;
			document.getElementById('n_ppn').value	= nilai_ppn;
			document.getElementById('v_total_fppn').value	= total_grand;
			*/
			
			//document.getElementById('v_discount').value = 0;
			//document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
			//document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
			//document.getElementById('v_total_fppn').value	= Math.round(total_grand);

			document.getElementById('v_discount').value = 0;
			document.getElementById('v_total_faktur').value	= xtotaln.toFixed(2);
			document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
			document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);
		}else{
			nilai_ppn	= 0;
			total_grand	= xtotaln + nilai_ppn;

			document.getElementById('v_discount').value = 0;
			document.getElementById('v_total_faktur').value	= xtotaln.toFixed(2);
			document.getElementById('n_ppn').value	= nilai_ppn.toFixed(2);
			document.getElementById('v_total_fppn').value	= total_grand.toFixed(2);			
		}						
	}

}

function addRowToTable(nItem) {
	
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	
	tbl.width='100%';
										
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	}else{
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:12px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:120px;\">" +
	"<input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:80px;\" onfocus=\"total("+iteration+");tnilaibarang();getdsk();\" readonly >" +
	"<img name=\"img_i_product_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualanbhnbakux('"+iteration+"','"+document.getElementById('i_branch').value+"');\"></DIV>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:200px;\" ><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:240px;\"></DIV>";
	
	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_v_hjp_"+nItem+"_"+iteration+"\" style=\"width:95px;\" ><input type=\"text\" ID=\"v_hjp_"+nItem+"_"+iteration+"\"  name=\"v_hjp_"+nItem+"_"+iteration+"\" style=\"width:100px;text-align:right;\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_n_quantity_"+nItem+"_"+iteration+"\" style=\"width:95px;\" ><input type=\"text\" ID=\"n_quantity_"+nItem+"_"+iteration+"\"  name=\"n_quantity_"+nItem+"_"+iteration+"\" style=\"width:100px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();validNum('n_quantity_tblItem','"+iteration+"');getdsk();\" value=\"0\" ></DIV>";

	var cell1 = row.insertCell(5);
	cell1.innerHTML = "<DIV ID=\"ajax_v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:95px;text-align:right;\" >" +
	"<input type=\"text\" ID=\"v_unit_price_"+nItem+"_"+iteration+"\"  name=\"v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:100px;text-align:right;\" onkeyup=\"total("+iteration+");tnilaibarang();validNum('v_unit_price_tblItem','"+iteration+"')getdsk();\" value=\"0\" >" +
	"<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\" >" +
	"<input type=\"hidden\" ID=\"isjcode_"+nItem+"_"+iteration+"\"  name=\"isjcode_"+nItem+"_"+iteration+"\">" +
	"<input type=\"hidden\" ID=\"n_quantity_hidden_"+nItem+"_"+iteration+"\"  name=\"n_quantity_hidden_"+nItem+"_"+iteration+"\">" +
	"<input type=\"hidden\" ID=\"e_satuan_hidden_"+nItem+"_"+iteration+"\" name=\"e_satuan_hidden_"+nItem+"_"+iteration+"\" ></DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}

</script>

<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#d_faktur").datepicker();
	$("#d_due_date").datepicker();
	$("#d_pajak").datepicker();
});
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_fpenjualanbhnbaku; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_fpenjualanbhnbaku; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
			<?php 
		  	$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listpenjualanbhnbaku/cform/actedit', $attributes);?>
		  
		<div id="masterfpenjualandoform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $form_nomor_f_fpenjualanbhnbaku; ?> : 
							  <input name="i_faktur" type="text" id="i_faktur" maxlength="13" value="<?php echo $ifakturcode; ?>" onkeyup="ckfpenjualan(this.value);"/>
							  <div id="confnomorfpenj" style="color:#FF0000;"></div>
							  </td>
							<td width="33%"><?php echo $form_tgl_f_fpenjualanbhnbaku; ?> : 
							  <input name="d_faktur" type="text" id="d_faktur" maxlength="10" value="<?php echo $tgFAKTUR; ?>" onchange="getJatuhTempo(this.value,document.getElementById('d_due_date'),29); tgpajak(this.value);" />
							  </td>
							<td width="33%"><?php echo $form_cabang_fpenjualanbhnbaku; ?> : 
							  <select name="i_branch" id="i_branch" >
								<option value="">[ <?php echo $form_pilih_cab_fpenjualanbhnbaku;?> ]</option>
								<?php
									foreach($opt_cabang as $row) {
										
										$sel		= $ibranchname==$row->einitial?"selected":"";
										
										$lcabang	.= "<option value=".$row->einitial." $sel >";
										$lcabang	.= $row->branch." ( ".$row->einitial." ) ";
										$lcabang	.= "</option>";
									}
									echo $lcabang;
								?>								
							  </select>&nbsp;&nbsp;<span style="color:#FF0000">*</span>
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>					
					<tr>
					 <td>
					  <div id="title-box2"><?php echo $form_detail_f_fpenjualanbhnbaku; ?>
					  <div style="float:right;">
					  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');total(document.getElementById('tblItem').rows.length);getdsk();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
					  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');total((document.getElementById('tblItem').rows.length)-1);getdsk();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
					  </div></div>
					 </td>	
					</tr>
					<tr>
					  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="4%" class="tdatahead">NO</td>
                          <td width="15%" class="tdatahead"><?php echo $form_kd_brg_fpenjualanbhnbaku; ?></td>
                          <td width="26%" class="tdatahead"><?php echo $form_nm_brg_fpenjualanbhnbaku; ?></td>
                          <td width="14%" class="tdatahead"><?php echo $form_hjp_fpenjualanbhnbaku; ?></td>
                          <td width="14%" class="tdatahead"><?php echo $form_qty_fpenjualanbhnbaku; ?></td>
                          <td width="14%" class="tdatahead"><?php echo $form_nilai_fpenjualanbhnbaku; ?></td>
                        </tr>
						<tr>
						 <td colspan="6">
						  <table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
						  
							<?php
							
							$iter	= 0;
							$xtotaln	= 0;
							
							foreach($fakturitem as $row3) {
								
								$xtotaln += $row3->amount;
								
								echo "
								<tr>
									<td><div style=\"font:24px;text-align:right;width:12px;margin-right:0px;\">".($iter*1+1).".</div></td>
									<td><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:120px;\">
									<input type=\"text\" ID=\"i_product_tblItem_".$iter."\"  name=\"i_product_tblItem_".$iter."\" style=\"width:80px;\" onfocus=\"total('".$iter."');tnilaibarang();getdsk();\" value=\"".$row3->i_product."\" readonly >
									<img name=\"img_i_product_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Kode Barang\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualanbhnbakux('".$iter."',document.getElementById('i_branch').value);\"></td>
									
									<td><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:200px;\" ><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\" name=\"e_product_name_tblItem_".$iter."\" style=\"width:240px;\" value=\"".$row3->e_product_name."\"></DIV></td>
									
									<td><DIV ID=\"ajax_v_hjp_tblItem_".$iter."\" style=\"width:95px;\" ><input type=\"text\" ID=\"v_hjp_tblItem_".$iter."\"  name=\"v_hjp_tblItem_".$iter."\" style=\"width:100px;text-align:right;\" value=\"".$row3->v_unit_price."\" onkeyup=\"total('".$iter."');tnilaibarang();getdsk();\" ></DIV></td>
									
									<td><DIV ID=\"ajax_n_quantity_tblItem_".$iter."\" style=\"width:95px;\" ><input type=\"text\" ID=\"n_quantity_tblItem_".$iter."\"  name=\"n_quantity_tblItem_".$iter."\" style=\"width:100px;text-align:right;\" onkeyup=\"total('".$iter."');tnilaibarang();validNum('n_quantity_tblItem','".$iter."');getdsk();\" value=\"".$row3->n_quantity."\" ></DIV></td>
									<td><DIV ID=\"ajax_v_unit_price_tblItem_".$iter."\" style=\"width:95px;text-align:right;\" >
									<input type=\"text\" ID=\"v_unit_price_tblItem_".$iter."\"  name=\"v_unit_price_tblItem_".$iter."\" style=\"width:100px;text-align:right;\" onkeyup=\"total('".$iter."');tnilaibarang();validNum('v_unit_price_tblItem','".$iter."');getdsk();\" value=\"".round(($row3->n_quantity * $row3->v_unit_price),2)."\" >
									<input type=\"hidden\" ID=\"iteration\" name=\"iteration\" value=\"".$iter."\">
									<input type=\"hidden\" ID=\"isjcode_tblItem_".$iter."\" name=\"isjcode_tblItem_".$iter."\" value=\"".$row3->i_sj."\">
									<input type=\"hidden\" ID=\"n_quantity_hidden_tblItem_".$iter."\" name=\"n_quantity_hidden_tblItem_".$iter."\" >
									<input type=\"hidden\" ID=\"e_satuan_hidden_tblItem_".$iter."\" name=\"e_satuan_hidden_tblItem_".$iter."\" value=\"".$row3->i_satuan."\" ></DIV></td>
								</tr>";
								
								$iter++;
							}
							
							if($f_include_ppn=='t') {
								
								$nilai_ppn		= (($xtotaln*11) / 100);
								$total_grand	= $xtotaln + $nilai_ppn;
								
								if($n_discount>0 || $n_discount!=0) {
									$hasil 	= (($nilai_ppn*$n_discount) / 100); // dikurangi discount
									$total_sblm_ppn	= round(($nilai_ppn - $hasil),2); // DPP
									$nilai_ppn2		= round((($total_sblm_ppn*11) / 100),2);
									$total_grand2	= round(($total_sblm_ppn + $nilai_ppn2),2);
								}else{
									$total_sblm_ppn	= round($xtotaln,2); // DPP
									$nilai_ppn2		= round($nilai_ppn,2);
									$total_grand2	= round($total_grand,2);							
								}
							}else{
								$nilai_ppn		= 0;
								$total_grand	= $xtotaln + $nilai_ppn;
								
								if($n_discount>0 || $n_discount!=0) {
									$hasil 	= (($nilai_ppn*$n_discount) / 100); // dikurangi discount
									$total_sblm_ppn	= round(($nilai_ppn - $hasil),2); // DPP
									$nilai_ppn2		= 0;
									$total_grand2	= round(($total_sblm_ppn + $nilai_ppn2),2);
								}else{
									$total_sblm_ppn	= round($xtotaln,2); // DPP
									$nilai_ppn2		= round($nilai_ppn,2);
									$total_grand2	= round($total_grand,2);							
								}	
							}

							/*
							if($f_include_ppn=='t') {
								$nilai_ppn		= (($xtotaln*10) / 100);
								$total_grand	= $xtotaln + $nilai_ppn;
								
								if($n_discount>0 || $n_discount!=0) {
									$hasil 	= (($nilai_ppn*$n_discount) / 100); // dikurangi discount
									$total_sblm_ppn	= ($nilai_ppn - $hasil); // DPP
									$nilai_ppn2		= (($total_sblm_ppn*10) / 100);
									$total_grand2	= ($total_sblm_ppn + $nilai_ppn2);
								}else{
									$total_sblm_ppn	= $xtotaln; // DPP
									$nilai_ppn2		= $nilai_ppn;
									$total_grand2	= $total_grand;							
								}
							}else{
								$nilai_ppn		= 0;
								$total_grand	= $xtotaln + $nilai_ppn;
								
								if($n_discount>0 || $n_discount!=0) {
									$hasil 	= (($nilai_ppn*$n_discount) / 100); // dikurangi discount
									$total_sblm_ppn	= ($nilai_ppn - $hasil); // DPP
									$nilai_ppn2		= 0;
									$total_grand2	= ($total_sblm_ppn + $nilai_ppn2);
								}else{
									$total_sblm_ppn	= $xtotaln; // DPP
									$nilai_ppn2		= $nilai_ppn;
									$total_grand2	= $total_grand;							
								}	
							}
							*/
												
							?>				  
						  </table>
						</tr>
                      </table></td>
					</tr>		
					<tr>
					 <td>
					  <div id="title-box2">
					  <div style="float:right;">
					  &nbsp;
					  </div></div>
					 </td>	
					</tr>					
					<tr>
					  <td valign="top"><table width="98%" border="0" cellpadding="0" cellspacing="0">
						<tr valign="top">
						  <td width="16%" rowspan="2"><?php echo $form_ket_f_fpenjualanbhnbaku; ?></td>
						  <td width="1%">:</td>
						  <td colspan="2" rowspan="2">
						  <?php
						  	$enotefaktur = array(
								'name'=>'e_note_faktur',
								'id'=>'e_note_faktur',
								'cols'=>2,
								'rows'=>1,
								'value'=>$e_note_faktur
							);
							echo form_textarea($enotefaktur);
						  ?></td>
						  <td width="14%"><?php echo $form_tnilai_fpenjualanbhnbaku; ?></td>
						  <td width="1%">:</td>
						  <td width="21%">
							<input name="v_total_nilai" type="text" id="v_total_nilai" size="15" maxlength="15" value="<?=$xtotaln?>" />						  </td>
						</tr>
						
						<tr>
						  <td>&nbsp;</td>
						  <td><?php echo $form_diskon_fpenjualanbhnbaku; ?> (%)</td>
						  <td>:</td>
						  <td>
						  <input name="n_discount" type="text" id="n_discount" size="10" maxlength="10" value="0" onkeyup="getdsk();" value="<?=$n_discount?>"/>
						  <?php echo $form_dlm_fpenjualanbhnbaku; ?> 
						  <input name="v_discount" type="text" id="v_discount" size="12" maxlength="12" value="<?=$v_discount?>"/>						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_tgl_jtempo_fpenjualanbhnbaku; ?> </td>
						  <td>:</td>
						  <td>
							<input name="d_due_date" type="text" id="d_due_date" size="10" maxlength="10" value="<?php echo $tjthtempo; ?>"/>
						  <td>&nbsp;</td>
						  <td><?php echo $form_total_fpenjualanbhnbaku; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_faktur" type="text" id="v_total_faktur" size="15" maxlength="15" value="<?=$total_sblm_ppn?>" />						  </td>
						</tr>

						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td> 
						  <td><?php echo "Include PPN"; ?></td>
						  <td>:</td>
						  <td>
							<input type="checkbox" name="include_ppn" id="include_ppn" onclick="fincludeppn();getdsk();" <?php if($f_include_ppn=='t'){ echo "checked"; } ?> /></td>
						</tr>
												
						<tr>
						  <td><?php echo $form_no_fpajak_fpenjualanbhnbaku; ?></td>
						  <td>:</td>
						  <td>
							<input name="i_faktur_pajak" type="text" id="i_faktur_pajak" size="15" maxlength="18" value="<?php echo $i_faktur_pajak; ?>"/>						  </td>
						  <td><?php echo $form_tgl_fpajak_fpenjualanbhnbaku; ?> :
							<input name="d_pajak" type="text" id="d_pajak" size="10" maxlength="10"  value="<?php echo $tgPajak; ?>"/>
							</td> 
						  <td><?php echo $form_ppn_fpenjualanbhnbaku; ?></td>
						  <td>:</td>
						  <td>
							<input name="n_ppn" type="text" id="n_ppn" size="15" maxlength="15" value="<?=$nilai_ppn2?>" /></td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>
							<input type="checkbox" name="f_cetak" value="1" <?=$f_printed?>/>
							&nbsp;<?php echo $form_ket_cetak_fpenjualanbhnbaku; ?></td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_grand_t_fpenjualanbhnbaku; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_fppn" type="text" id="v_total_fppn" size="15" maxlength="15" value="<?=$total_grand2?>" /></td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td colspan="7"><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:245px; padding:5px;">Dari input Surat Jalan (SJ)</div></td>
						</tr>
					  </table></td>
					</tr>
					<tr align="right">
					  <td>
						<input type="hidden" name="f_include_ppn" id="f_include_ppn" value="<?=$f_include_ppn;?>" />
						
						<input type="hidden" name="ifakturhiden" id="ifakturhiden" value="<?=$i_faktur?>" />
						<input type="hidden" name="ifakturcodehiden" id="ifakturcodehiden" value="<?=$ifakturcode?>" />
						
						<input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit">
						<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listpenjualanbhnbaku/cform/'">

					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
