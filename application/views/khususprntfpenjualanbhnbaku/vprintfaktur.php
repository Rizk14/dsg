<style type="text/css">
	@page {
		size: US Letter;
		margin: 0.29in 0.29in 0.29in 0.70in;
	}

	.isinya {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;

	}

	.kepala {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 19px;
	}

	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.kepala2 {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.kotak {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 11px;
		border-collapse: collapse;
		border: 1px solid black;
	}

	.tabelheader {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 13px;
	}

	.subtotal {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 14px;
	}
</style>

<!-- media="screen" means these styles will only be used by screen 
  devices (e.g. monitors) -->

<?php include_once("funcs/terbilang.php"); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.3.2.js"></script>
<!-- <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script> -->
<script type="text/javascript">
	$(function() {
		//printpr();
		//window.print();
	});

	window.onafterprint = function() {
		var faktur = <?php echo $nomorfaktur ?>;
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('khususprntfpenjualanbhnbaku/cform/update'); ?>",
			data: "faktur=" + faktur,
			success: function(data) {
				opener.window.refreshview();
				setTimeout(window.close, 0);
			},
			error: function(XMLHttpRequest) {
				alert('fail');
			}
		});
	}
</script>

<table border="0" width="730px" class="tabelheader">
	<tr>
	</tr>
	<tr>
		<td align="left" colspan="2"> =============================================================================================== </td>
	</tr>
	<tr>
		<td align="left" colspan="2">
			<div class="kepala">&nbsp;&nbsp;<b>Faktur Penjualan</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<b class="kepalaxx"><?php echo $nminitial ?></b>
			</div>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2"> =============================================================================================== </td>
	</tr>

	<tr class="kepala2">
		<td>Nomor Faktur : <b><?php echo $nomorfaktur ?> </b></td>
		<td align="right">Tanggal Faktur : <b><?php echo $tglfaktur ?></b></td>
	</tr>
	<tr>
	</tr>
	<tr>
		<td>Kepada :</td>
		<td rowspan="3">
			<table border="2" width="70%" class="kotak">
				<tr>
					<td>Keterangan:<br>
						<?php echo $enote ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><b><?php echo $nmcabang ?></b></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><b><?php echo $alamatcabang  ?></b></td>
		<td>&nbsp;</td>
	</tr>
</table>

<table border="0" width="730px" class="isinya">

	<thead>
		<tr>
			<td colspan="6">
				<hr>
			</td>
		</tr>
		<tr class="kepala2">
			<th width="3%">No</th>
			<th width="20%" style="white-space:nowrap;">Nama Barang</th>
			<th width="4%">Qty</th>
			<th width="7%">Satuan</th>
			<th width="8%" align="right">Harga</th>
			<th width="10%" align="right">Jumlah</th>
		</tr>
		<tr>
			<td colspan="6">
				<hr>
			</td>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		$lup	= 0;
		$grandtotal = 0;

		foreach ($isi as $row) {
			if ($lup < 22) {
				$esatuan = $row->esatuan;
				$qvalue	= $this->mclass->jmlitemharga($row->i_faktur, $row->imotif);

				if ($qvalue->num_rows() > 0) {
					$row_value	= $qvalue->row();
					$jmlqty[$lup]	= $row_value->qty;
					$uprice[$lup]	= $row_value->unitprice;
					$tprice[$lup]	= $row_value->amount;
				} else {
					$jmlqty[$lup]	= 0;
					$uprice[$lup]	= 0;
					$tprice[$lup]	= 0;
				}
				echo "<tr class='detailbrg'>
					<td align='center'>$no</td>
					<td>&nbsp;$row->motifname</td>
					<td align='right'>" . number_format($jmlqty[$lup], 2, '.', ',') . "&nbsp;</td>
					<td align='center'>&nbsp;$esatuan</td>
					<td align='right'>" . number_format($uprice[$lup], 2, '.', ',') . "&nbsp;</td>
					<td align='right'>" . number_format($tprice[$lup], 2, '.', ',') . "&nbsp;</td>
			</tr>";
				$grandtotal += $tprice[$lup];
				$no++;
				$lup++;
			}
		}
		?>
		<tr>
			<td colspan="6">
				<hr>
			</td>
		</tr>
	</tbody>
</table>

<table width="730px" border="0" class="kepala2">
	<tr class="detailbrg2">
		<td width="3%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%" colspan="2" align="right">Jumlah Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td width="20%">Rp. </td>-->
		<td align="right"><b><?php echo number_format($grandtotal, 2, '.', ',') ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td colspan="2" style="white-space:nowrap;">Tgl Jatuh Tempo : <b><?php echo $tgljthtempo ?></b></td>
		<td width="10%">&nbsp;</td>
		<td width="30%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
	</tr>
	<tr>
		<td rowspan="7" colspan="3">
			<table border="2" width="100%" class="kotak">
				<tr>
					<td style="white-space:nowrap;">&nbsp;1. Barang-barang yg sudah dibeli tidak dapat ditukar/dikembalikan,<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kecuali ada perjanjian terlebih dahulu<br>
						&nbsp;2. Faktur asli merupakan bukti pembayaran yg sah<br>
						&nbsp;3. Pembayaran dgn cek/giro baru dianggap sah setelah diuangkan
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="2">Diskon Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($diskon, 2, '.', ',') ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td>&nbsp;</td>
		<td colspan="2">
			<hr>
		</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="2">DPP Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<!--<td>Rp.</td>-->
		<td align="right"><b><?php echo number_format($dpp, 2, '.', ',') ?></b>&nbsp;</td>
	</tr>
	<tr class="detailbrg2">
		<td align="right" colspan="2">PPN Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;</td>
		<td align="right"><b><?php echo number_format($nilai_ppn, 2, '.', ',') ?></b>&nbsp;</td>
	</tr>

	<? if ($nilai_faktur > 4999999) { ?>
		<tr class="detailbrg2">
			<!-- <td align="right" colspan="2">BEA MATERAI Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
			<!-- <td align="right"><b><?php echo number_format($materai, 2, '.', ',') ?></b>&nbsp;</td> -->
		</tr>
	<? } ?>
	<tr class="detailbrg2">
		<td>&nbsp;</td>
		<td colspan="2">
			<hr>
		</td>
	</tr>
	<? if ($nilai_faktur > 4999999) { ?>
		<tr class="detailbrg2">
			<td align="right" colspan="5">Nilai Faktur Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;</td>
			<!--<td>Rp.</td>-->
			<td align="right"><b><?php echo number_format($nilai_faktur, 2, '.', ',') ?></b>&nbsp;</td>
		</tr>
	<? } else { ?>
		<tr class="detailbrg2">
			<td align="right" colspan="2">Nilai Faktur Rp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;</td>
			<!--<td>Rp.</td>-->
			<td align="right"><b><?php echo number_format($nilai_faktur, 2, '.', ',') ?></b>&nbsp;</td>
		</tr>
	<? } ?>
	<tr class="detailbrg2">
		<td colspan="6" align="center" style="white-space:nowrap;"><?php $terbil = &terbilangxx($nilai_faktur, 3); ?>(Terbilang: <?php echo $terbil ?> Rupiah)</td>
	</tr>
</table>
<br>
<div class="kepala2"><b>SE & O<br><br><br><br><br><br>
		<br>

		Adm Penjualan</b></div>