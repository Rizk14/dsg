<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php
$query3	= $this->db->query(" SELECT id, nama, alamat, kota FROM tm_perusahaan ");
				
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
	}
	else {
		$id	= '';
		$nama = '';
		$kota = '';
		$alamat = '';
	}
?>
<title><?php echo $nama ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Tom@Lwis (http://www.lwis.net/free-css-drop-down-menu/)" />
<meta name="keywords" content=" css, dropdowns, dropdown menu, drop-down, menu, navigation, nav, horizontal, vertical left-to-right, vertical right-to-left, horizontal linear, horizontal upwards, cross browser, internet explorer, ie, firefox, safari, opera, browser, lwis" />
<meta name="description" content="Clean, standards-friendly, modular framework for dropdown menus" />
<link href="<?php echo base_url(); ?>css/dropdown/themes/default/helper.css" media="screen" rel="stylesheet" type="text/css" />

<!-- Beginning of compulsory code below -->

<link href="<?php echo base_url(); ?>css/dropdown/dropdown.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>css/dropdown/themes/default/default.ultimate.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>css/style.css" media="screen" rel="stylesheet" type="text/css" />
	
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.4.4.min.js"></script>

<!--[if lt IE 7]>
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<!-- / END -->

</head>
<body>

<h1>Sistem Informasi <?php echo $nama ?> v2.0</h1>

<ul id="nav" class="dropdown dropdown-horizontal" nowrap>
	<li><a href="<?php echo base_url(); ?>index.php">Home</a></li>
	<?php if ($this->session->userdata('gid') == 1 )  { ?>
		<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>-->
						<!--04-06-2015 dinonaktifkan <li><a href="<?php echo base_url(); ?>index.php/mst-item/cform">Item</a></li>-->
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>-->
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>-->
						<li><a href="<?php echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-sup/cform/view">Harga Bahan Baku/Pembantu Berdasarkan Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-wip/cform/view">Harga Barang WIP Berdasarkan Unit Jahit</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-pack/cform/view">Harga Barang WIP Berdasarkan Unit Packing</a></li>
				<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
				<!--<li><a href="<?php echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-hrg-quilting/cform/view">Harga Bahan Quilting Berdasarkan Unit Makloon</a></li>-->
				
				<!-- 04-06-2015 sementara dinonaktifkan aja dulu <li><a href="<?php echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>-->
				<!-- 04-06-2015 sementara dinonaktifkan aja dulu <li><span class="dir">Produksi</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-marker/cform/view">Marker Gelaran/Set</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-ukuran-bisbisan/cform">Ukuran Bis-Bisan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">Kebutuhan Bis-Bisan Per Pcs</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-bagian-brg-jadi/cform">Bagian Dari Brg Jadi Utk Schedule Cutting Dacron</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-motif-brg-jadi/cform">Motif Brg Jadi</a></li>
					</ul>
				</li> -->
				<li><span class="dir">Makloon</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/ukuranbisbisan">Ukuran Bisbisan</a></li>
						<!--<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/jenispotongbisbisan">Jenis Potong Bisbisan</a></li>-->
						<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/hargabisbisan">Harga Bisbisan</a></li>
					</ul>
				</li>
				
				<li><span class="dir">Akunting</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-area/cform">Master Area Akunting</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbank">Bank</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoa">Kode Akun (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoagroup">Kode Akun Group (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoasubledger">Kode Akun Subledger (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoaledger">Kode Akun Ledger (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoageneralledger">Kode Akun General Ledger (CoA)</a></li>
						
					</ul>
				</li>
				
				<li><span class="dir">Gudang</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-lok-gudang/cform">Master Lokasi Gudang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-gudang/cform">Master Jenis Gudang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-gudang/cform">Master Gudang</a></li>
					</ul>
				</li>
				<li><span class="dir">Pelanggan</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/pelanggan/cformgroup">Group Pelanggan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/pelanggan/cform">Pelanggan Pusat</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/pelanggan/cformcabang">Pelanggan Cabang</a></li>
						
						<li><a href="<?php echo base_url(); ?>index.php/pelanggan/cformcodetransfer">Kode Transfer Pelanggan</a></li>
					</ul>
				</li>
				<li><span class="dir">Unit Luar</span>
					<ul>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->
					<li><a href="<?php echo base_url(); ?>index.php/mst-kel-unit/cform/view">Pengkelompokan Unit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
		<!--		<li><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">Bahan Baku</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-acc/cform/view">Accessories</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-perl/cform/view">Alat Perlengkapan</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bp/cform/view">Bahan Pendukung</a></li> -->
			<!--	<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform">Jenis Makloon</a></li> -->
			
				<li><span class="dir">HPP</span>
					<ul>
				<!--		<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/view">Barang Jadi WIP</a></li> --->
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewwarnabrgjadi">Warna Barang Jadi WIP</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi">Material Barang Jadi</a></li> --->
					</ul>
				</li>
			<li><span class="dir">WIP</span>
							<ul>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/kelbrgwip">Kelompok Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/jenisbrgwip">Jenis Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/view">Barang WIP</a></li>
							<!-- 13-07-2015 SEMENTARA DINONAKTIFKAN	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li> -->
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">Warna Barang WIP</a></li>
<!--
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewmaterialbrgwip">Material/Bahan Baku-Pembantu Pembentuk Barang WIP</a></li>
-->
						</ul>
				</li>
				<?php
				if ($this->session->userdata('gid') == 7) { ?>
		
			<li><span class="dir">Marketing</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/marketing/cform/kelbrgjadi">Kelompok Barang Jadi</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/marketing/cform/viewbrgjadi">Barang Jadi</a></li>
					</ul>
				</li>
			
	<?php } ?>
				
				<li><a href="<?php echo base_url(); ?>index.php/mst-department/cform">Departemen</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-supplier/cform/view">Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/setting/cform/konversisatuan">Setting Konversi Satuan</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importbhnbaku">Import Excel Data Bahan Baku/Pembantu</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importsupplier">Import Excel Data Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importhargasupplier">Import Excel Data Harga Berdasarkan Supplier</a></li>
				
			</ul>
		</li>
	<?php } ?>
	<?php if (($this->session->userdata('gid') == 7))  { ?>
		<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>-->
						<!--04-06-2015 dinonaktifkan <li><a href="<?php echo base_url(); ?>index.php/mst-item/cform">Item</a></li>-->
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>-->
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>-->
						<li><a href="<?php echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-sup/cform/view">Harga Bahan Baku/Pembantu Berdasarkan Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-wip/cform/view">Harga Barang WIP Berdasarkan Unit Jahit</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-pack/cform/view">Harga Barang WIP Berdasarkan Unit Packing</a></li>
				<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
				<!--<li><a href="<?php echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-hrg-quilting/cform/view">Harga Bahan Quilting Berdasarkan Unit Makloon</a></li>-->
				
				<!-- 04-06-2015 sementara dinonaktifkan aja dulu <li><a href="<?php echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>-->
				<!-- 04-06-2015 sementara dinonaktifkan aja dulu <li><span class="dir">Produksi</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-marker/cform/view">Marker Gelaran/Set</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-ukuran-bisbisan/cform">Ukuran Bis-Bisan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">Kebutuhan Bis-Bisan Per Pcs</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-bagian-brg-jadi/cform">Bagian Dari Brg Jadi Utk Schedule Cutting Dacron</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-motif-brg-jadi/cform">Motif Brg Jadi</a></li>
					</ul>
				</li> -->
				<li><span class="dir">Makloon</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/ukuranbisbisan">Ukuran Bisbisan</a></li>
						<!--<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/jenispotongbisbisan">Jenis Potong Bisbisan</a></li>-->
						<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/hargabisbisan">Harga Bisbisan</a></li>
					</ul>
				</li>
				
				<li><span class="dir">Akunting</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-area/cform">Master Area Akunting</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbank">Bank</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoa">Kode Akun (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoagroup">Kode Akun Group (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoasubledger">Kode Akun Subledger (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoaledger">Kode Akun Ledger (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoageneralledger">Kode Akun General Ledger (CoA)</a></li>
						
					</ul>
				</li>
				
				<li><span class="dir">Gudang</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-lok-gudang/cform">Master Lokasi Gudang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-gudang/cform">Master Jenis Gudang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-gudang/cform">Master Gudang</a></li>
					</ul>
				</li>
				
				<li><span class="dir">Unit Luar</span>
					<ul>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->
					<li><a href="<?php echo base_url(); ?>index.php/mst-kel-unit/cform/view">Pengkelompokan Unit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
		<!--		<li><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">Bahan Baku</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-acc/cform/view">Accessories</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-perl/cform/view">Alat Perlengkapan</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bp/cform/view">Bahan Pendukung</a></li> -->
			<!--	<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform">Jenis Makloon</a></li> -->
			
				<li><span class="dir">HPP</span>
					<ul>
				<!--		<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/view">Barang Jadi WIP</a></li> --->
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewwarnabrgjadi">Warna Barang Jadi WIP</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi">Material Barang Jadi</a></li> --->
					</ul>
				</li>
			<li><span class="dir">WIP</span>
							<ul>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/kelbrgwip">Kelompok Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/jenisbrgwip">Jenis Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/view">Barang WIP</a></li>
							<!-- 13-07-2015 SEMENTARA DINONAKTIFKAN	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li> -->
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">Warna Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewmaterialbrgwip">Material/Bahan Baku-Pembantu Pembentuk Barang WIP</a></li>
						</ul>
				</li>
				<?php
				if ($this->session->userdata('gid') == 1) { ?>
		
			<li><span class="dir">Marketing</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/marketing/cform/kelbrgjadi">Kelompok Barang Jadi</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/marketing/cform/viewbrgjadi">Barang Jadi</a></li>
					</ul>
				</li>
			
	<?php } ?>
				
				<li><a href="<?php echo base_url(); ?>index.php/mst-department/cform">Departemen</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-supplier/cform/view">Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/setting/cform/konversisatuan">Setting Konversi Satuan</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importbhnbaku">Import Excel Data Bahan Baku/Pembantu</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importsupplier">Import Excel Data Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importhargasupplier">Import Excel Data Harga Berdasarkan Supplier</a></li>
				
			</ul>
		</li>
	<?php } ?>
	<?php if ($this->session->userdata('gid') == 2) { ?>
		<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>-->
						<!--04-06-2015 dinonaktifkan <li><a href="<?php echo base_url(); ?>index.php/mst-item/cform">Item</a></li>-->
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>-->
						<!--11-06-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>-->
						<li><a href="<?php echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-sup/cform/view">Harga Bahan Baku/Pembantu Berdasarkan Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb-wip/cform/view">Harga Bahan Baku/Pembantu Berdasarkan Unit Jahit</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/mst-bb-pack/cform/view">Harga Bahan Baku/Pembantu Berdasarkan Unit Packing</a></li>
				
				<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
				<!--<li><a href="<?php echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-hrg-quilting/cform/view">Harga Bahan Quilting Berdasarkan Unit Makloon</a></li>-->
				
				<li><span class="dir">Makloon</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/ukuranbisbisan">Ukuran Bisbisan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-makloon/cform/hargabisbisan">Harga Bisbisan</a></li>
					</ul>
				</li>
				
				<!-- 04-06-2015 sementara dinonaktifkan aja dulu<li><a href="<?php echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>-->
				<li><span class="dir">Gudang</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-lok-gudang/cform">Master Lokasi Gudang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-gudang/cform">Master Gudang</a></li>
					</ul>
				</li>
				<li><span class="dir">Unit Luar</span>
					<ul>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-department/cform">Departemen</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-supplier/cform/view">Supplier</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/setting/cform/konversisatuan">Setting Konversi Satuan</a></li>
			<li><span class="dir">WIP</span>
							<ul>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/kelbrgwip">Kelompok Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/jenisbrgwip">Jenis Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/view">Barang WIP</a></li>
							<!-- 13-07-2015 SEMENTARA DINONAKTIFKAN	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li> -->
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">Warna Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewmaterialbrgwip">Material/Bahan Baku-Pembantu Pembentuk Barang WIP</a></li>
						</ul>
				</li>
			
			</ul>
		</li>
		
		
		
	<?php } ?>
	
	<!-- 15-09-2014 adopt from tirai -->
	<?php if ($this->session->userdata('gid') == 3) { ?>
		<li><span class="dir">Master</span>
			<ul>
				<!--<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>-->
						<!--<li><a href="<?php echo base_url(); ?>index.php/mst-item/cform">Item</a></li>-->
						<!--<li><a href="<?php echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>-->
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
				<!--<li><a href="<?php echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>-->
				<li><span class="dir">Gudang</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-lok-gudang/cform">Master Lokasi Gudang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-gudang/cform">Master Jenis Gudang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-gudang/cform">Master Gudang</a></li>
					</ul>
				</li>
				<li><span class="dir">Unit Luar</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-department/cform">Departemen</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/setting/cform/konversisatuan">Setting Konversi Satuan</a></li>
				<li><span class="dir">HPP</span>
					<ul>
				<!--		<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/view">Barang Jadi WIP</a></li> --->
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewwarnabrgjadi">Warna Barang Jadi WIP</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi">Material Barang Jadi</a></li> --->
					</ul>
				</li>
			</ul>
		</li>
	<?php } ?>
	<!-- -->
	
	<?php if ( $this->session->userdata('gid') == 5) { ?>

	<li><span class="dir">Master</span>
			<ul>
				<li><a href="<?php echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
				<li><span class="dir">WIP</span>
							<ul>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/kelbrgwip">Kelompok Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/jenisbrgwip">Jenis Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/view">Barang WIP</a></li>

							<!-- 13-07-2015 SEMENTARA DINONAKTIFKAN	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li> -->

								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">Warna Barang WIP</a></li>

						<!-- 06-07-2015 sementara dinonaktifkan. ini rencananya di ppic aja		<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewmaterialbrgjadi">Material Barang Jadi</a></li> -->

						</ul>
				</li>
				<li><span class="dir">Unit Luar</span>
					<ul>
<!---
	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->

						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>

					<!-- 06-07-2015 sementara dinonaktifkan	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform/viewgrupjahit">Grup Jahit di Unit Jahit</a></li> -->
					<!--	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li> -->

					</ul>
				</li>

			<!--<li><a href="<?php echo base_url(); ?>index.php/syncbrgjadi/cform">Sinkronisasi Barang Jadi</a></li> -->
			
			

			</ul>
	</li>

		
	<!-- 06-07-2015 ditutup aja	<li><span class="dir">Master</span>
			<ul> -->
				<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
				<!--<li><a href="<?php echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>-->
				<!--<li><a href="<?php echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>-->
			<!-- 04-06-2015 sementara dinonaktifkan	<li><span class="dir">Produksi</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-marker/cform/view">Marker Gelaran/Set</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-ukuran-bisbisan/cform">Ukuran Bis-Bisan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">Kebutuhan Bis-Bisan Per Pcs</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-bagian-brg-jadi/cform">Bagian Dari Brg Jadi Utk Schedule Cutting Dacron</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-motif-brg-jadi/cform">Motif Brg Jadi</a></li>
					</ul>
				</li> -->
		<!--	</ul>
		</li> -->
	<?php } ?>
	
	<?php // staf WIP / PIC
	if ($this->session->userdata('gid') == 6) { ?>
		<li><span class="dir">Master</span>
			<ul>
<li><span class="dir">WIP</span>
							<ul>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/kelbrgwip">Kelompok Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/jenisbrgwip">Jenis Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/view">Barang WIP</a></li>
							<!-- 13-07-2015 SEMENTARA DINONAKTIFKAN	<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppengadaan">Harga Pokok Penjualan (HPP) Gudang Pengadaan</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpp">Harga Pokok Penjualan (HPP) WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhpppacking">Harga Pokok Penjualan (HPP) Packing</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgjadi/cform/viewhppgudangjadi">Harga Pokok Penjualan (HPP) Gudang Jadi</a></li> -->
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewwarnabrgwip">Warna Barang WIP</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/brgwip/cform/viewmaterialbrgwip">Material/Bahan Baku-Pembantu Pembentuk Barang WIP</a></li>
						</ul>
				</li>
			<li><span class="dir">Unit Luar</span>
					<ul>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-makloon/cform">Unit Makloon</a></li> -->
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform">Unit Jahit</a></li>
					<!-- 06-07-2015 sementara dinonaktifkan	<li><a href="<?php echo base_url(); ?>index.php/mst-unit-jahit/cform/viewgrupjahit">Grup Jahit di Unit Jahit</a></li> -->
						<li><a href="<?php echo base_url(); ?>index.php/mst-unit-packing/cform">Unit Packing</a></li>
					</ul>
				</li>
			<!--<li><a href="<?php echo base_url(); ?>index.php/syncbrgjadi/cform">Sinkronisasi Barang Jadi</a></li> -->
			
			</ul>
		</li>
	<?php } ?>
	
<!--	<li><span class="dir">PP</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/pp-bb/cform/view">Bahan Baku</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/pp-asesoris/cform/view">Accessories</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/pp-bp/cform/view">Bahan Pendukung</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/pp-perl/cform/view">Alat Perlengkapan</a></li>
		</ul>
	</li> -->
	
	<?php if ($this->session->userdata('gid') == 8|| $this->session->userdata('gid') == 4) { ?>
		<li><span class="dir">Master</span>
			<ul>
				
				<li><span class="dir">Akunting</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-area/cform">Master Area Akunting</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbank">Bank</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoa">Kode Akun (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoagroup">Kode Akun Group (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoasubledger">Kode Akun Subledger (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoaledger">Kode Akun Ledger (CoA)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewcoageneralledger">Kode Akun General Ledger (CoA)</a></li>
					
					</ul>
				</li>
				
			</ul>
		</li>
	<?php } ?>
	
	<?php // 23-04-2015 staf MARKETING
	if ($this->session->userdata('gid') == 10  ) { ?>
		<li><span class="dir">Master</span>
			<ul>
			<li><span class="dir">Marketing</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/marketing/cform/kelbrgjadi">Kelompok Barang Jadi</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/marketing/cform/viewbrgjadi">Barang Jadi</a></li>
					</ul>
				</li>
			</ul>
		</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 1 ||$this->session->userdata('gid') == 7) { ?>
	
	<!-- <li><a href="<?php echo base_url(); ?>index.php/credit_note/cform/">Credit Note Alokasi</a></li> -->
	
		<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2 || $this->session->userdata('gid') == 9||$this->session->userdata('gid') == 7) { ?>
	<li><span class="dir">Pembelian</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/pp-new/cform/view">Permintaan Pembelian (PP)</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/op/cform/view">Order Pembelian (OP)</a></li>
			<!-- <li>Order Pembelian (OP)</li> -->
			<li><a href="<?php echo base_url(); ?>index.php/faktur-bb/cform/view">Bukti Penerimaan Barang</a></li>
			<!--04-06-2015 sementara dinonaktifkan <li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/view">Update Stok (Bon M Masuk) Hasil Pembelian</a></li>-->
			
		<!-- Ini disiapkan jika 1 faktur pembelian utk beberapa SJ	-->
		<li><a href="<?php echo base_url(); ?>index.php/faktur-pembelian/cform/view">Faktur Pembelian</a></li>

		<!--	<li><a href="<?php echo base_url(); ?>index.php/faktur-bb/cform/view2">Input No Faktur Pembelian</a></li> -->
			<li><a href="<?php echo base_url(); ?>index.php/faktur-pajak/cform/view">Faktur Pajak</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/retur-beli/cform/view">Nota Debet Retur</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/retur-beli/cform/viewnotaretur">Nota Retur</a></li>
			<!-- sementara dinonaktifkan dulu <li><a href="<?php echo base_url(); ?>index.php/faktur-makloon/cform/view">Input Faktur Jasa Makloon</a></li> -->
			
			<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
			<!--<li><a href="<?php echo base_url(); ?>index.php/sj-masuk-makloon/cform/view">SJ Masuk Hasil Quilting</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/faktur-quilting/cform/view">Faktur Quilting</a></li> 
			<li><a href="<?php echo base_url(); ?>index.php/pembelianwip/cform/view">Faktur Pembelian Makloon Hasil Jahit (WIP)</a></li>-->
			<!--penginputan 1 kali
			<li><a href="<?php echo base_url(); ?>index.php/pembelian/cform/saldoawalhutang">Input Saldo Awal Hutang Dagang</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/pembelian/cform/opnamehutang">Input Opname Hutang Dagang Manual</a></li> -->
			<!--	sementara dinonaktifkan dulu
		<li><a href="<?php echo base_url(); ?>index.php/set-stok-harga/cform/">Set Stok Awal Berdasarkan Harga Pembelian</a></li> -->
		
		</ul>
	</li>
	

	<?php 
}
	
	if ($this->session->userdata('gid') == 8 || $this->session->userdata('gid') == 2|| $this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 9||$this->session->userdata('gid') == 7) { ?>
	
	
	
	
	<li><span class="dir">Makloon Lainnya</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/makloon/cform/viewopbisbisan">Order Pembelian (OP) Makloon Bisbisan</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/makloon/cform/viewbtbbisbisan">Bukti Terima Barang (BTB) Makloon Bisbisan</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/makloon/cform/viewfakturbisbisan">Faktur Pembelian Makloon Bisbisan</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/faktur-bb-makloon/cform/view">Bukti Terima Barang (BTB) Makloon Bordir</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/faktur-pembelian-bordir/cform/view">Faktur Pembelian Makloon Bordir</a></li>
			
			<li><a href="<?php echo base_url(); ?>index.php/faktur-bb-aplikasi/cform/view">Bukti Terima Barang (BTB) Makloon Aplikasi</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/faktur-pembelian-aplikasi/cform/view">Faktur Pembelian Makloon Aplikasi</a></li>
			
			<li><a href="<?php echo base_url(); ?>index.php/importcsv/cform/makloon_baju">Import CSV Data Makloon Baju</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/faktur-btb-baju/cform/view">Bukti Terima Barang (BTB) Makloon Baju</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/faktur-pembelian-baju/cform/view">Faktur Pembelian Makloon Baju</a></li>
				
		</ul>
	</li>
	<li><span class="dir">Makloon Packing + Jahit</span>
		<ul>
			
					<li><a href="<?php echo base_url(); ?>index.php/faktur-btb-wip/cform/view">Bukti Terima Barang (BTB) Makloon Jahit WIP</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/faktur-pembelian-wip/cform/view">Faktur Pembelian Makloon Jahit WIP</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/faktur-btbpack-wip/cform/view">Bukti Terima Barang (BTB) Makloon Packing WIP</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/faktur-pembelianpack-wip/cform/view">Faktur Pembelian Makloon Packing WIP</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/faktur-btb-packjht-wip/cform/view">Bukti Terima Barang (BTB) Makloon Packing + Jahit WIP</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/faktur-pembelianpackjht-wip/cform/view">Faktur Pembelian Makloon Packing + Jahit WIP</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/faktur-btb-retur-wip/cform/view">Faktur Retur Makloon Jahit WIP</a></li>
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 3) { // 20-11-2012. update 01-04-2015 ga usah ?>
	<!--<li><span class="dir">Master</span>
			<ul>
				<li><span class="dir">Atribut Bahan Baku/Pembantu</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/mst-kel-barang/cform">Kelompok Barang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bb/cform">Jenis Barang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-jns-bhn/cform/view">Jenis Bahan</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-item/cform">Item</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-motif/cform">Motif / Ukuran</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-warna/cform">Warna</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/mst-satuan/cform/">Satuan</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-bb/cform/view">Data Bahan Baku/Pembantu</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-brg-makloon/cform/view">Data Bahan Quilting</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/mst-brg-bisbisan/cform/view">Data Bahan Bis-Bisan</a></li>				
			</ul>
		</li> -->
	
	<!--<li><span class="dir">Pembelian</span>
		<ul>

			<li><a href="<?php echo base_url(); ?>index.php/faktur-bb/cform/view">Bukti Penerimaan Barang</a></li>
			
		</ul>
	</li> -->
	<?php } ?>
	
		<!-- 08-04-2016  -->
	<?php if ($this->session->userdata('gid') == 3) { ?>
	<li><span class="dir">Pembelian</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/faktur-bb/cform/view">Bukti Penerimaan Barang</a></li>
		</ul>
	</li>
		<?php } ?>
	
	<!-- 15-09-2014 admin -->
	<?php if ($this->session->userdata('gid') == 1||$this->session->userdata('gid') == 7) { ?>
	<li><span class="dir">Gudang</span>
		<ul>
			<li><span class="dir">Bon M Masuk</span>
				<ul>
					<!--<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk-hsl-cutting/cform/view">Hasil Cutting</a></li>-->
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk-hsl-cutting/cform/viewbonm">Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/view">Hasil Pembelian</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/manual">Hasil Pembelian (Manual)</a></li>
				<!--	<li><a href="<?php echo base_url(); ?>index.php/bonmmasukkredit/cform/view">Hasil Pembelian Untuk Stok Lain-lain</a></li> -->
					
					<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/bonmmasukquilting/cform/view">Hasil Pembelian Bahan Quilting</a></li>-->
				
				<!--	<li><a href="<?php echo base_url(); ?>index.php/bonmmasukcash/cform/view">Revisi Tipe Stok Untuk Pembelian Cash Lain-lain</a></li> -->
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuklain/cform/view">Masuk Lain-Lain</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewbisbisan">Hasil Pembelian Makloon Bisbisan</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewbordir">Hasil Pembelian Makloon Bordir</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewaplikasi">Hasil Pembelian Makloon Aplikasi</a></li>
				</ul>
			</li>
			<li><span class="dir">Bon M Keluar</span>
				<ul>
				<!--	<li><a href="<?php echo base_url(); ?>index.php/bonm-bbc/cform/view">Pemenuhan Bahan Baku Dari Permintaan Bhn Baku</a></li> -->
					<li><a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/view">Pengeluaran Bahan Baku/Pembantu</a></li>
				</ul>
			</li>
			
			<!--<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewstokawal">Input Stok Awal WIP (hasil jahit)</a></li>-->
			<!-- 06-07-2015 dinonaktifkan, ini dari SO aja <li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewstokawal">Input Stok Awal WIP (hasil jahit)</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewstokawalunit">Input Stok Awal WIP di Unit Jahit</a></li> -->
			<li><a href="<?php echo base_url(); ?>index.php/wip/cgudang/konversistok">Konversi Stok Barang WIP</a></li>
			
			<li><span class="dir">Input Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/stok-opname-quilting/cform/">Bahan Quilting</a></li>-->
					<li><a href="<?php echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/stok-opname-hsl-jahit/cform/">Barang WIP (Hasil Jahit) Di Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/sounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/sounitpacking">Barang WIP (hasil jahit) di Unit Packing</a></li> 
				</ul>
			</li>
			
			<li><span class="dir">Approval Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-quilting/cform/">Bahan Quilting</a></li>-->
					<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-hsl-jahit/cform/">Barang WIP (Hasil Jahit) Di Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/approvalsounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/approvalsounitpacking">Barang WIP (hasil jahit) di Unit Packing</a></li>
				</ul>
			</li>
			<!-- 07-08-2015 -->
			<li><a href="<?php echo base_url(); ?>index.php/gudangbb/cform/kalkulasiulangstok">Kalkulasi Ulang Stok Terkini Bahan Baku/Pembantu</a></li>
			<!-- 25-08-2015 -->
			<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/soharga">Input Stok Opname Bahan Baku/Pembantu Berdasarkan Harga Manual</a></li>
		<li><a href="<?php echo base_url(); ?>index.php/periode/cform/view">Data Input Periode Closing Bahan Baku</a></li>
		<li><a href="<?php echo base_url(); ?>index.php/periode-wip/cform/view">Data Input Periode Closing WIP</a></li>
		</ul>
	</li>
	
	
	<?php } ?>
	
		
	<?php if ($this->session->userdata('gid') == 3) { ?>
	<li><span class="dir">Gudang</span>
		<ul>
			<li><span class="dir">Bon M Masuk</span>
				<ul>
					<!--<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk-hsl-cutting/cform/view">Hasil Cutting</a></li>-->
					
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/view">Hasil Pembelian</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewmanual">Hasil Pembelian (Manual)</a></li>
				<!--	<li><a href="<?php echo base_url(); ?>index.php/bonmmasukkredit/cform/view">Hasil Pembelian Untuk Stok Lain-lain</a></li> -->
				
					<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/bonmmasukquilting/cform/view">Hasil Pembelian Bahan Quilting</a></li>-->
				
				<!--	<li><a href="<?php echo base_url(); ?>index.php/bonmmasukcash/cform/view">Revisi Tipe Stok Untuk Pembelian Cash Lain-lain</a></li> -->
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuklain/cform/view">Masuk Lain-Lain</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewbisbisan">Hasil Pembelian Makloon Bisbisan</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewbordir">Hasil Pembelian Makloon Bordir</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk/cform/viewaplikasi">Hasil Pembelian Makloon Aplikasi</a></li>
				</ul>
			</li>
			<li><span class="dir">Bon M Keluar</span>
				<ul>
				<!--	<li><a href="<?php echo base_url(); ?>index.php/bonm-bbc/cform/view">Pemenuhan Bahan Baku Dari Permintaan Bhn Baku</a></li> -->
					<li><a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/view">Pengeluaran Bahan Baku/Pembantu</a></li>
				<!--	<li><a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/viewbonmkeluarcutting">Pengeluaran Barang Hasil Cutting ke Jahitan</a></li> -->
				</ul>
			</li>
			<li><span class="dir">Input Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/stok-opname-quilting/cform/">Bahan Quilting</a></li>-->
					
					<!--01-04-2015 dikomen -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/sounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li> -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/stok-opname-hsl-packing/cform/">Bahan Hasil Packing</a></li>-->
					
				</ul>
			</li>
			
			<li><span class="dir">Approval Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname/cform/">Bahan Baku/Pembantu</a></li>
					
					<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-quilting/cform/">Bahan Quilting</a></li>-->
					
					<!--<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-hsl-jahit/cform/">Barang WIP (hasil jahit) di Perusahaan</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/approvalsounitjahit">Barang WIP (hasil jahit) di Unit Jahit</a></li>-->
					<!--<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-hsl-packing/cform/">Bahan Hasil Packing</a></li>-->
					
				</ul>
			</li>
			<!-- 07-08-2015 -->
			<li><a href="<?php echo base_url(); ?>index.php/gudangbb/cform/kalkulasiulangstok">Kalkulasi Ulang Stok Bahan Baku/Pembantu</a></li>
			<!-- 25-08-2015 -->
			<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/soharga">Input Stok Opname Bahan Baku/Pembantu Berdasarkan Harga Manual</a></li>
		<!-- 12-06-2015 dinonaktifkan	<li><a href="<?php echo base_url(); ?>index.php/set-stok-harga/cform/viewstokawal">Input Stok Awal Bahan Baku/Pembantu Berdasarkan Harga</a></li>-->
		<li><a href="<?php echo base_url(); ?>index.php/periode/cform/">Input Periode Closing Bahan Baku</a></li>
		</ul>
	</li>
	
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6|| $this->session->userdata('gid') == 7) { ?>
	<li><span class="dir">QC</span>
		<ul>
			<li><span class="dir">Approve Surat Jalan</span>
				<ul>
					
			<li><a href="<?php echo base_url(); ?>index.php/app-sj/cform/view">Approve SJ Masuk Barang WIP ke Gudang QC</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/app-sj/cform/viewsjkeluar">Approve SJ Keluar Barang WIP ke Gudang QC</a></li>
				</ul>
		</ul>
	</li>
	
	<?php } ?>
		
		<!--  15-09-2014 dikoment, ini utk login gudang, udh dibikin diatas -->
	<?php //if ($this->session->userdata('gid') == 2) { ?>
	<!--<li><span class="dir">Gudang</span>
		<ul>
			<li><span class="dir">Bon M Masuk</span>
				<ul>
					<li><a href="<?php //echo base_url(); ?>index.php/bonmmasuk/cform/view">Hasil Pembelian</a></li>
					<li><a href="<?php //echo base_url(); ?>index.php/bonmmasukkredit/cform/view">Hasil Pembelian Untuk Stok Lain-lain</a></li>
					<li><a href="<?php //echo base_url(); ?>index.php/bonmmasukquilting/cform/view">Hasil Pembelian Bahan Quilting</a></li>
				<!--	<li><a href="<?php //echo base_url(); ?>index.php/bonmmasukcash/cform/view">Revisi Tipe Stok Untuk Pembelian Cash Lain-lain</a></li> -->
	<!--			</ul>
			</li>
		</ul>
	</li> -->
	<?php //} ?>
	
	 <!-- this is not f**kin necessary anymore -->
	<?php //if ($this->session->userdata('gid') == 1 ||  $this->session->userdata('gid') == 5 ) { ?>
	<!--<li><span class="dir">Makloon</span>
		<ul> -->
		<!--	<li><a href="<?php echo base_url(); ?>index.php/pb-makloon/cform/view">Permintaan Bahan Baku/Pembantu Ke Gudang</a></li> -->
			
		<!--	<li><span class="dir">Quilting</span>
						<ul>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-keluar-makloon/cform/view">SJ Keluar</a></li>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-masuk-makloon/cform/view">SJ Masuk</a></li>
						</ul>
			</li>
			<li><span class="dir">Bis-Bisan</span>
						<ul>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-keluar-bisbisan/cform/view">SJ Keluar</a></li>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-masuk-bisbisan/cform/view">SJ Masuk</a></li>
						</ul>
			</li>
			<li><span class="dir">Bordir</span>
						<ul>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-keluar-bordir/cform/view">SJ Keluar</a></li>
						</ul>
			</li>
			<li><span class="dir">Print</span>
						<ul>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-keluar-print/cform/view">SJ Keluar</a></li>
						</ul>
			</li>
			<li><span class="dir">Makloon Accessories</span>
						<ul>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-keluar-asesoris/cform/view">SJ Keluar</a></li>
							<li><a href="<?php //echo base_url(); ?>index.php/sj-masuk-asesoris/cform/view">SJ Masuk</a></li>
						</ul>
			</li> -->
			
			<!-- <li><a href="<?php echo base_url(); ?>index.php/faktur-pajak-makloon/cform/view">Input Faktur Pajak Makloon</a></li> -->
		<!--sementara dihide dulu	<li><a href="<?php //echo base_url(); ?>index.php/retur-makloon/cform/view">Claim Produksi (Nota Debet Retur)</a></li> -->
			
	<!--	</ul>
	</li> -->
	<?php //} ?>
	
	<!--  15-09-2014 dikoment, cutting diskip dulu -->
	<?php //if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 5) { ?>
	<!--<li><span class="dir">Cutting</span>
		<ul>
			<li><a href="<?php //echo base_url(); ?>index.php/pb-cutting/cform/view">Permintaan Bahan Baku Ke Gudang</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/bonm-bbc/cform/view">Pemenuhan Permintaan Bhn Baku</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/schedule-cutting/cform/view">Schedule Cutting</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/realisasi-cutting/cform/view">Realisasi Cutting</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/bs-cutting/cform/view">Pengurangan Stok Hasil Cutting BS</a></li>
		</ul>
	</li> -->
	<?php //} ?>
	
	<!-- 15-09-2014, this is no longer used anymore -->
	<?php //if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 3) { ?>
	<!--<li><span class="dir">WIP/Jahit</span> -->
		<!--sementara dikomen<ul>
			<li><a href="<?php //echo base_url(); ?>index.php/jahit-schedule/cform/view">Schedule Jahit</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/sj-keluar-jahit-new/cform/view">SJ Keluar (Proses Ke Unit Jahit)</a></li>
			<li><a href="<?php //echo base_url(); ?>">SJ Keluar (Proses Retur Hasil Jahit Ke Unit Jahit)</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">SJ Masuk Hasil Jahit</a></li>
		</ul> -->
	<!--	<ul>
			<li><a href="<?php //echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">SJ Masuk Hasil Jahit Pembelian</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/wip/cform/viewsjmasuk">SJ Masuk</a></li>
			<li><a href="<?php //echo base_url(); ?>index.php/wip/cform/viewsjkeluar">SJ Keluar</a></li>
		</ul>
	</li> -->
	<?php //} ?>
	
	<?php //if ($this->session->userdata('gid') == 2) { ?>
	<!--<li><span class="dir">WIP/Jahit</span>
		<ul>
			<li><a href="<?php //echo base_url(); ?>index.php/sj-masuk-jahit/cform/view">SJ Masuk Hasil Jahit Pembelian</a></li>
		</ul>
	</li> -->
	<?php //} ?>
	
	<!-- ini staf adm pengadaan 06-07-2015 -->
	<?php if ($this->session->userdata('gid') == 5) { ?>
	<li><span class="dir">Gudang</span>
		<ul>
			<li><span class="dir">Bon M Masuk</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/bonmmasuk-hsl-cutting/cform/viewbonm">Hasil Cutting</a></li>
				</ul>
			</li>
			
			<li><span class="dir">Input Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
				</ul>
			</li>
			
			<li><span class="dir">Approval Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-hsl-cutting/cform/">Bahan Hasil Cutting</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<?php } ?>
	
	<!-- ini staf WIP 15-09-2014 -->
	<?php if ($this->session->userdata('gid') == 6) { ?>
	<li><span class="dir">Gudang</span>
		<ul>
			<!-- 06-07-2015 dinonaktifkan<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewstokawal">Input Stok Awal WIP (hasil jahit)</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewstokawalunit">Input Stok Awal WIP di Unit Jahit</a></li>-->
			<!-- 02-11-2015 sementara dihide -->
			<!--<li><a href="<?php echo base_url(); ?>index.php/wip/cgudang/konversistok">Konversi Stok BaranStok Opname Barang WIP (Hasil Jahit) Di g WIP di Perusahaan</a></li>-->
			<li><span class="dir">Input Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/stok-opname-hsl-jahit/cform/">Barang WIP (Hasil Jahit) Di Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/sounitjahit">Barang WIP (Hasil Jahit) Di Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/trial-gudangwip-packing/cform/sounitpacking">Barang WIP (Hasil Jahit) Di Unit Packing</a></li>
					
				<!--	SEMENTARA DINONAKTIFKAN 16/02/2016			
				<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/sounitpacking">Barang WIP (Hasil Jahit) Di Unit Packing</a></li>
					-->	
								
				</ul>
			</li>
		<!--	SEMENTARA DINONAKTIFKAN 16/02/2016
		 <li><span class="dir">Approval Stok Opname</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/app-stok-opname-hsl-jahit/cform/">Barang WIP (Hasil Jahit) Di Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/approvalsounitjahit">Barang WIP (Hasil Jahit) Di Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/gudangwip/cform/approvalsounitpacking">Barang WIP (Hasil Jahit) Di Unit Packing</a></li>
				</ul>
			</li> -->
		</ul>
	</li>
	<?php } ?>
	
	<?php 
	// 06-07-2015 UTK ADM PENGADAAN
	if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 5) { ?>
	<li><span class="dir">Pengadaan</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/pengadaan/cform/viewsjmasuk">SJ Masuk Retur Bahan Baku Dari Jahitan</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/pengadaan/cform/viewbonmkeluarcutting">SJ Keluar Bahan Baku (Hasil Cutting) Ke Jahitan</a></li>
			<!--06-07-2015 INI DIPINDAH KE LOGIN WIP AJA <li><a href="<?php echo base_url(); ?>index.php/pic/cform/editwarnabrgjadi">Pengubahan Data Warna Barang Jadi (PIC / WIP)</a></li>-->
		</ul>
	</li>
	<?php } ?>
	
	<?php 
	// 11-12-2015
	if ($this->session->userdata('gid') == 6) { ?>
	<li><span class="dir">Pengadaan</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/pengadaan/cform/viewsjmasuk">SJ Masuk Retur Bahan Baku Dari Jahitan</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/pengadaan/cform/viewbonmkeluarcutting">SJ Keluar Bahan Baku (Hasil Cutting) Ke Jahitan</a></li>
			<!--06-07-2015 INI DIPINDAH KE LOGIN WIP AJA <li><a href="<?php echo base_url(); ?>index.php/pic/cform/editwarnabrgjadi">Pengubahan Data Warna Barang Jadi (PIC / WIP)</a></li>-->
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 11 ) { ?>
	
	<li><span class="dir">WIP</span>
		<ul>

			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewsjmasuk">SJ Masuk Barang WIP Ke Gudang QC</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewsjkeluar">SJ Keluar Barang WIP Dari Gudang QC</a></li>		
		</ul>
	</li>
		<?php } ?>
	
	
	<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6||$this->session->userdata('gid') == 7) { ?>
	
	<li><span class="dir">WIP</span>
		<ul>

			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewsjmasuk">SJ Masuk Barang WIP Ke Gudang QC</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewsjkeluar">SJ Keluar Barang WIP Dari Gudang QC</a></li>

			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewsjantarunit">SJ Bahan Baku (Hasil Cutting) Antar Unit Jahit</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/cform/viewsjmasukjahitpacking">SJ Masuk Barang WIP Dari Unit Jahit/Packing Ke Gudang Jadi</a></li>

			<li><span class="dir">Adjustment WIP</span>
			<ul>
			<li><a href="<?php echo base_url(); ?>index.php/adjustment-hsl-jahit/cform/">Adjusment WIP di Gudang QC</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/adjustment-unit-jahit/cform/">Adjusment WIP di unit Jahit</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/adjustment-unit-packing/cform/">Adjusment WIP di unit Packing</a></li>
			
			</ul>
			</li>
	
		<!-- 06-07-2015 sementara dinonaktifkan	<li><a href="<?php echo base_url(); ?>index.php/wip/creport/addpresentasikerjaunit">Input Presentasi Kerja Unit</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/creport/addforecast">Input Forecast</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/creport/addstokschedule">Input Stok Schedule</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/wip/creport/addreturqc">Input Retur QC</a></li> -->

<!--dibuka sewaktu-waktu-->
		<li><span class="dir">Lain-lain</span>
			<ul>
				<li><span class="dir">Unit Jahit</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/trial-masukother-unitjahit/ctest/viewsjotherunitjahit">Masuk Lain lain ke Unit Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/trial-keluarother-unitjahit/ctest/viewsjotherunitjahit">Keluar Lain lain dari Unit Jahit</a></li>
					</ul>
				</li> 
				<li><span class="dir">Unit Packing</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/trial-masukother-unitpacking/ctest/viewsjotherunitpacking">Masuk Lain lain ke Unit packing</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/trial-keluarother-unitpacking/ctest/viewsjotherunitpacking">Keluar Lain lain dari Unit Packing</a></li>
					</ul>
				</li> 
				<li><span class="dir">Gudang QC</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/trial-masukother-gudangqc/cform/viewsjmasuk">Masuk Lain lain ke Gudang QC</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/trial-keluarother-gudangqc/cform/viewsjkeluar">Keluar Lain lain dari Gudang QC</a></li>
					</ul>
				</li> 	
			</ul>	
			</li>  
		</ul>
	</li>

	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 8 ||$this->session->userdata('gid') == 7) { ?>
	<li><span class="dir">Keuangan</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">Pembayaran Hutang Pembelian</a></li>
		<li><a href="<?php echo base_url(); ?>index.php/keu-alokasi/cform/viewalo">Alokasi Bank Masuk</a></li>
		<!---	<li><a href="<?php echo base_url(); ?>index.php/keu-payment-pembelian-wip/cform/view">Pembayaran Hutang Pembelian WIP</a></li> --->
			<li><a href="<?php echo base_url(); ?>index.php/exp-htg-dgg/cform"> EXPORT Pembayaran Hutang Pembelian</a></li>
			
			<!--<li><a href="<?php echo base_url(); ?>index.php/keu-payment-makloon/cform/view">Pembayaran Biaya Makloon</a></li>-->
		</ul>
	</li>
	
	<?php }// 22-04-2016
	if ($this->session->userdata('gid') == 10 || $this->session->userdata('gid') == 1  || $this->session->userdata('gid') == 8||$this->session->userdata('gid') == 7) { ?>
	<li><span class="dir">Marketing</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/marketing/creport/addforecastdistributor">Input Forecast Berdasarkan Pelanggan/Distributor</a></li>
		</ul>
	</li>
	<?php } 
	if( $this->session->userdata('gid') == 4 || $this->session->userdata('gid') == 8 || $this->session->userdata('gid') == 1||$this->session->userdata('gid') == 7 ){ ?>
	<li><span class="dir">Akunting</span>
		<ul>
			
			<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewsaldoakun">Master Saldo Akun</a></li>
			
			<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewjurnalumum">Jurnal Umum</a></li>
			<li><span class="dir">Kas</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewkasbesarin">Kas Besar (Masuk)</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewkasbesarout">Kas Besar (Keluar)</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbankin">Bank (Masuk)</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewbankout">Bank (Keluar)</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/akunting/cform/viewkaskecil">Kas Kecil</a></li>
				</ul>
			</li>
		</ul>
	</li>
	
	<?php }

	 ?>
	
	<?php if ($this->session->userdata('gid') == 2) { ?>
	<li><span class="dir">Keuangan</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">Pembayaran Hutang Pembelian</a></li>
			<!--<li><a href="<?php echo base_url(); ?>index.php/keu-payment-makloon/cform/view">Pembayaran Biaya Makloon</a></li>-->
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('username') != '') {  ?>
	<li><span class="dir">Informasi</span>
		<ul>
			
			<?php // 20-04-2015
				if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 10 || $this->session->userdata('gid') == 7) {
			?>
					<li><span class="dir">Marketing</span>
						<ul>
							<li><a href="<?php echo base_url(); ?>index.php/marketing/creport/laprealisasi">Laporan Data Realisasi FC vs OP vs DO berdasarkan Pelanggan/Distributor</a></li>
							<li><a href="<?php echo base_url(); ?>index.php/marketing/creport/laprealisasiperiode">Laporan Data Realisasi FC vs OP vs DO Periode</a></li>
							<li><a href="<?php echo base_url(); ?>index.php/marketing/creport/laprealisasibrand">Laporan Data Realisasi FC vs OP vs DO berdasarkan Brand</a></li>
						</ul>
					</li>
			<?php } ?>
			
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2|| $this->session->userdata('gid') == 8|| $this->session->userdata('gid') == 9|| $this->session->userdata('gid') == 7) { ?>
			<li><span class="dir">Pembelian</span>
				<ul>
				<?php
			}
			 if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2|| $this->session->userdata('gid') == 9|| $this->session->userdata('gid') == 7) { 
				?>
					<li><span class="dir">Rekap Pembelian</span>
					<ul>
						
						<li><a href="<?php echo base_url(); ?>index.php/kartuhutang/cform/">Kartu Hutang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/kartuhutang-makloon/cform/">Kartu Hutang Makloon</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-rekap-pembelian-persup/cform">Rekap Pembelian Per Supplier</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-rekap-pembelian-peritem/cform">Rekap Penerimaan Barang Per Item</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-rekap-pelunasan-persup/cform">Rekap Pembayaran Pembelian Per Supplier</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-rekap-hutang/cform/">Rekapitulasi Hutang Dagang</a></li>
				<!-- 04-06-2015 sementara dinonaktifkan	<li><a href="<?php echo base_url(); ?>index.php/info-rekap-stok-bb/cform">Rekap Mutasi Stok Bahan Baku/Pembantu</a></li>-->
					<li><a href="<?php echo base_url(); ?>index.php/info-opvssj/cform">OP vs SJ Pembelian</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-rekap-op/cform/">Rekap Order Pembelian (OP)</a></li>
				<!--	<li><a href="#">Rekapitulasi Pembayaran Hutang Dagang</a></li> -->
					</ul>
					</li>
					<?php 
				}
					if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2|| $this->session->userdata('gid') == 8|| $this->session->userdata('gid') == 9|| $this->session->userdata('gid') == 7) {
					
					 ?>
					
					<li><span class="dir">Laporan Pembelian</span>
					<ul>
						<li><a href="<?php echo base_url(); ?>index.php/info-opname-hutang/cform/">Laporan Opname Hutang Dagang</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian/cform/">Laporan Pembelian</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian/cform/lappelunasan">Laporan Pembayaran Pembelian</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-perubahan-harga/cform/">Laporan Perubahan Harga Pembelian Bahan Baku / Pembantu</a></li> 
						<li><a href="<?php echo base_url(); ?>index.php/info-perubahan-harga-wip-jht/cform/">Laporan Perubahan Harga Pembelian WIP Jahit</a></li> 
						<li><a href="<?php echo base_url(); ?>index.php/info-perubahan-harga-wip-pck/cform/">Laporan Perubahan Harga Pembelian WIP Packing</a></li> 
					</ul>
			</li>
			<li><span class="dir">Laporan Makloon Jahit + Packing </span>
					<ul>
					<!--	<li><a href="<?php echo base_url(); ?>index.php/info-pembelian/cform/lapfakturwip">Laporan Faktur Pembelian Makloon Hasil Jahit (WIP)</a></li>  --->
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian-wip-faktur/cform/lapfakturwip">Laporan Faktur Pembelian Makloon Hasil Jahit (WIP)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian-packing/cform/lapfakturwip">Laporan Faktur Pembelian Makloon Hasil Packing (WIP)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian-jhtpack-faktur/cform/lapfakturwip">Laporan Faktur Pembelian Makloon Hasil Jahit + Packing (WIP)</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian-retur-makloon/cform/lapfakturwip">Laporan Faktur Retur Makloon (WIP) </a></li>
						
						<li><a href="<?php echo base_url(); ?>index.php/info-btb-jht/cform/">Laporan Harga Unit Jahit Berdasarkan BTB Jahit </a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-btb-pck/cform/">Laporan Harga Unit Packing Berdasarkan BTB Packing </a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-btb-jhtpck/cform/">Laporan Harga Unit Packing & Jahit Berdasarkan BTB Jahit Packing </a></li>
						
				<!--		<li><a href="<?php echo base_url(); ?>index.php/info-perubahan-harga/cform/">Laporan Perubahan Harga Pembelian</a></li> -->
					</ul>
			</li>
			
			<li><span class="dir">Laporan Makloon Lainnya</span>
					<ul>

						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian-makloon-baju/cform/lapfakturwip">Laporan Faktur Makloon Baju (WIP) </a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian-makloon-bordir/cform/lapfakturwip">Laporan Faktur Makloon Bordir (WIP) </a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-pembelian-makloon-aplikasi/cform/lapfakturwip">Laporan Faktur Makloon Aplikasi (WIP) </a></li>
				
						<li><a href="<?php echo base_url(); ?>index.php/info-btb-baju/cform/">Laporan Harga Unit Packing & Jahit Berdasarkan BTB Baju </a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-btb-bordir/cform/">Laporan Harga Berdasarkan BTB Bordir </a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-btb-aplikasi/cform/">Laporan Harga Berdasarkan BTB Aplikasi </a></li>
					
					</ul>
			</li>
		</ul>
	</li>
			<?php } ?>
			
			<?php //if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 3) { ?>
			<!--<li><span class="dir">Gudang</span>
				<ul>
					<li><a href="<?php //echo base_url(); ?>index.php/info-mutasi-stok/cform/">Laporan Mutasi Stok Bahan Baku/Pembantu</a></li>
					<li><a href="<?php //echo base_url(); ?>index.php/info-mutasi-stok/cform/wip">Laporan Mutasi Stok WIP</a></li>
				</ul>
			</li> -->
			<?php //} ?>
			
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 3 || $this->session->userdata('gid') == 5 || $this->session->userdata('gid') == 7 || $this->session->userdata('gid') == 11) { ?>
			<li><span class="dir">Gudang</span>
				<ul>
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 3 || $this->session->userdata('gid') == 7 ) { ?>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/viewsobhnbaku">Stok Opname Bahan Baku/Pembantu</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/creport/transaksibhnbaku">Laporan Transaksi Bahan Baku/Pembantu</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-rekap-pembelian-peritem/cform/formgudang">Rekap Penerimaan Barang Per Item Berdasarkan Harga</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/">Laporan Mutasi Stok Bahan Baku/Pembantu</a></li>
					<!--<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/wip">Laporan Mutasi Stok WIP</a></li> -->
					<!--<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/hasilcutting">Laporan Mutasi Stok Bahan Hasil Cutting</a></li>-->
			<?php } ?>
			
			<?php 
					// 06-07-2015
			if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 5 ) { ?>
					<li><a href="<?php echo base_url(); ?>index.php/hasilcutting/cform/viewsohasilcutting">Stok Opname Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/creport/transaksihasilcutting">Laporan Transaksi Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/hasilcutting">Laporan Mutasi Stok Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/transaksiunitjahit">Laporan Transaksi Barang WIP (Hasil Jahit) Di Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/creport/hasilcuttingvssjkeluar">Laporan Hasil Cutting vs SJ Keluar Ke Jahitan</a></li>
			<?php } ?>
			
			
				</ul>
			</li>
			<?php } ?>
			
			
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6|| $this->session->userdata('gid') == 7||$this->session->userdata('gid') == 11) { ?>
			<li><span class="dir">Gudang QC</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/viewsohasiljahit">Stok Opname Barang WIP (Hasil Jahit) Di Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-stok/cform/wip">Laporan Mutasi Stok WIP (Hasil Jahit) Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/transaksihasiljahit">Laporan Transaksi Barang WIP (Hasil Jahit) Di Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-stok-app/cform/wip">Laporan Approve Mutasi Stok WIP (Hasil Jahit) Gudang QC</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-stok-app/cform/transaksihasiljahit">Laporan Approve Transaksi Barang WIP (Hasil Jahit) Di Gudang QC</a></li>
			
			<!--		<li><a href="<?php echo base_url(); ?>index.php/info-adjustment-hasil-jahit/creport/viewadjhasiljahit">Laporan Adjustment Barang WIP (Hasil Jahit) Di Gudang QC</a></li> -->
					<!--28-10-2015 sementara dinonaktifkan <li><a href="<?php echo base_url(); ?>index.php/wip/cgudang/historykonversistok">History Konversi Stok Barang WIP di Gudang QC</a></li>-->
				</ul>
			</li>
			<?php } ?>
			
			
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6 || $this->session->userdata('gid') == 7)  { ?>
			
			<li><span class="dir">Gudang Jadi</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/transaksigudangjadi">Laporan Transaksi Barang WIP di Gudang Jadi</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/trial-info-masuk-gd-jadi/cform/wip">Laporan Masuk Barang WIP di Gudang Jadi</a></li>
				</ul>
			</li>
			<?php } ?>
			

			<!-- 15-09-2014, samain dgn tirai -->
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 6|| $this->session->userdata('gid') == 7) { ?>
			<li><span class="dir">Unit Jahit</span>
				<ul>
					<!-- 06-07-2015 dinonaktifkan sementara <li><a href="<?php echo base_url(); ?>index.php/wip/creport/presentasikerjaunit">Laporan Presentasi Kerja Unit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/rekappresentasikerjaunit">Rekapitulasi Presentasi Kerja Unit Tahunan</a></li>-->
					<!-- 06-07-2015 dinonaktifkan, sepertinya ini utk PPIC<li><a href="<?php echo base_url(); ?>index.php/wip/creport/forecastvsschedule">Laporan Forecast vs Schedule</a></li> -->
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/viewsounitjahit">Stok Opname Barang WIP (Hasil Jahit) Di Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/mutasiunit">Laporan Mutasi Stok WIP (Hasil Jahit) di Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/transaksiunitjahit">Laporan Transaksi Barang WIP (Hasil Jahit) Di Unit Jahit</a></li>
				<!-- 06-07-2015 dikomen	<li><a href="<?php echo base_url(); ?>index.php/wip/creport/stokmingguanunit">Laporan Stok Mingguan Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/bhnbakuunit">Laporan Bahan Baku Unit Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/qcunitjahit">Laporan QC Unit Jahit</a></li> -->
				<!--	<li><a href="<?php echo base_url(); ?>index.php/info-adjustment-unit-jahit/creport/viewadjunitjahit">Laporan Adjustment Barang WIP (Unit Jahit) </a></li> -->
				</ul>
			</li>
			
			<li><span class="dir">Unit Packing</span>
				<ul>
				<!--	SEMENTARA DINONAKTIFKAN 16/02/2016
				<li><a href="<?php echo base_url(); ?>index.php/wip/creport/viewsounitpacking">Stok Opname Barang WIP (Hasil Jahit) Di Unit Packing</a></li> -->
				<li><a href="<?php echo base_url(); ?>index.php/trial-wip-packing/creport/viewsounitpacking">Stok Opname Barang WIP (Hasil Jahit) Di Unit Packing</a></li> 
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/mutasiunitpacking">Laporan Mutasi Stok WIP (Hasil Jahit) Unit Packing</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/wip/creport/transaksiunitpacking">Laporan Transaksi Barang WIP (Hasil Jahit) Di Unit Packing</a></li>
			<!--		<li><a href="<?php echo base_url(); ?>index.php/info-adjustment-unit-packing/creport/viewadjunitpacking">Laporan Adjustment Barang WIP (Unit Packing) </a></li> -->
				</ul>
			</li>
			<?php } ?>
			<!-- -->
			
			<!-- 15-09-2014 -->
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2 || $this->session->userdata('gid') == 3 || $this->session->userdata('gid') == 5 || $this->session->userdata('gid') == 6 || $this->session->userdata('gid') == 7) { ?>
			<li><span class="dir">Stok Terkini </a></span>
				<ul>
					<?php if ($this->session->userdata('gid') == 1|| $this->session->userdata('gid') == 7) { ?>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-bb/cform/view">Bahan Baku/Pembantu</a></li>
						
						<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
						<!--<li><a href="<?php echo base_url(); ?>index.php/info-stok-quilting/cform/view">Bahan Hasil Quilting</a></li>-->
						
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-cutting/cform/view">Bahan Hasil Cutting</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/view">Barang WIP (Hasil Jahit) di Gudang QC</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/viewstokunit">Barang WIP (Hasil Jahit) di Unit Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/viewstokunitpacking">Barang WIP (Hasil Jahit) di Unit Packing</a></li>
						<!--<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-packing/cform/view">Barang Jadi (hasil packing)</a></li>-->
					<?php }
					else if ($this->session->userdata('gid') == 2 || $this->session->userdata('gid') == 3) {
					 ?>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-bb/cform/view">Bahan Baku/Pembantu</a></li>
						
						<!-- 23-06-2015 quilting dikomen karena mau digabung ke master bhn baku/pembantu -->
						<!--<li><a href="<?php echo base_url(); ?>index.php/info-stok-quilting/cform/view">Bahan Hasil Quilting</a></li>-->
						
						<!--<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-cutting/cform/view">Bahan Hasil Cutting</a></li>-->
					 <?php } 
					 else if ($this->session->userdata('gid') == 6) { ?>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/view">Barang WIP (Hasil Jahit) di Gudang QC</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/viewstokunit">Barang WIP (Hasil Jahit) di Unit Jahit</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-jahit/cform/viewstokunitpacking">Barang WIP (Hasil Jahit) di Unit Packing</a></li>
					 <?php } 
					 else if ($this->session->userdata('gid') == 5) { ?>
						<li><a href="<?php echo base_url(); ?>index.php/info-stok-hsl-cutting/cform/view">Bahan Hasil Cutting</a></li>
					 <?php } ?>
				</ul>
			</li>
			<?php } ?>
			
			<!-- 09-06-2015 UNTUK NGETES EXPORT CSV. 15-12-2015 dikomen yg export CSVnya -->
			<?php if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 8 || $this->session->userdata('gid') == 4|| $this->session->userdata('gid') == 7) { ?>
			<!--<li><span class="dir">Export CSV Untuk e-faktur</a></span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/exportpenjualan/cform/fakturdo">Faktur Penjualan Berdasarkan DO</a></li>
				</ul>
			</li>-->
			
			<li><span class="dir">Akunting</span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/bukubesar">Buku Besar</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/neracasaldo">Neraca Saldo</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/labarugi">Laba Rugi</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/neraca">Neraca</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/ju">Jurnal Umum</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/kk">Kas Kecil</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/kb">Kas Besar</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-akunting/cform/bank">Bank</a></li>
				</ul>
			</li>
			
			<?php } ?>
			
		<!--	<li><span class="dir">Kartu Stok </a></span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-bb/cform">Bahan Baku/Pembantu</a></li>				-->
				<!--	<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-makloon/cform">Bahan Hasil Makloon</a></li> -->
		<!--			<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-quilting/cform">Bahan Hasil Quilting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-hsl-bisbisan/cform">Bahan Hasil Bis-Bisan</a></li>		-->
				<!--	<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-hsl-bordir/cform">Bahan Hasil Bordir</a></li> -->
		<!--			<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-hsl-print/cform">Bahan Hasil Print</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-hsl-asesoris/cform">Bahan Hasil Makloon Accessories</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-hsl-cutting/cform">Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-hsl-jahit/cform">Barang Hasil Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-hsl-packing/cform">Barang Hasil Packing</a></li>
				</ul>
			</li> -->
			
			<!-- 15-09-2014, not necessary anymore -->
			<?php //if ($this->session->userdata('gid') == 5 || $this->session->userdata('gid') == 1 ) { ?>
		<!--	<li><span class="dir">Cutting </a></span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/exp-schedule-cutting/cform">Realisasi Cutting Per Operator</a></li>
				</ul>
			</li> -->
			<?php //} ?>
			
		<!--	<li><span class="dir">Rekap Mutasi Stok </a></span>
				<ul>
					<li><a href="<?php echo base_url(); ?>index.php/info-rekap-stok-bb/cform">Bahan Baku/Pembantu</a></li> -->
				<!--	<li><a href="<?php echo base_url(); ?>index.php/info-mutasi-makloon/cform">Bahan Hasil Makloon</a></li> -->
				<!--	<li><a href="<?php echo base_url(); ?>#">Bahan Hasil Cutting</a></li>
					<li><a href="<?php echo base_url(); ?>#">Barang Hasil Jahit</a></li>
					<li><a href="<?php echo base_url(); ?>#">Barang Hasil Packing</a></li>
				</ul>
			</li> -->
			
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('gid') == 1 && $this->session->userdata('username') != '') { ?>
	<li><span class="dir">Setting</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/setting/cform">Setting Perusahaan</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/users-group/cform">Group User</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/users-admin-user/cform/view">Admin User</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/users-ganti-passwd/cform">Ganti Password</a></li>
			<!--<li><a href="<?php echo base_url(); ?>index.php/info-pembelian/cform/cek_sj_nonsinkron">Cek SJ Yg Angkanya Tdk Cocok</a></li>-->
			<!-- 06-07-2015 dikomen<li><a href="<?php echo base_url(); ?>index.php/bacadbf/cform/">Tes Baca DBF</a></li>-->
			<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importtrxpembelian">Import Excel Data Laporan Pembelian (Taskhub)</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importtrxsupplier">Import Excel Data Supplier (Taskhub)</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/importtrxpenjualan">Import Data Laporan Penjualan (Taskhub)</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/exporttrxpembelian">Export Excel Laporan Pembelian Untuk Taskhub</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/importdata/cform/exporttrxpenjualan">Export Excel Laporan Penjualan Untuk Taskhub</a></li>
		</ul>
	</li>
	<?php } else if ($this->session->userdata('gid') != 1 && $this->session->userdata('username') != '') { ?>
		<li><span class="dir">Setting</span>
		<ul>
			<li><a href="<?php echo base_url(); ?>index.php/users-ganti-passwd/cform">Ganti Password</a></li>
			
		</ul>
	</li>
	<?php } ?>
	
	<?php if ($this->session->userdata('username') != '') { ?>
	<li><a href="<?php echo base_url(); ?>index.php/users-login/cform/logout">Logout</a></li>
	<?php } ?>
</ul>
</div>
<!-- / END -->
<br><br><br><br>

<?php echo $this->load->view($isi); ?>

<?php echo $this->load->view('footer'); ?>

</body>
</html>
