<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title; ?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function toUpper(val){
	var imotif	= val.value;
	var toUpper	= imotif.toUpperCase();
	val.value	= toUpper;
}

function settextfield(a,b,c,d,e,f,g,h,i,j,k) {

   	var ada	= false;
	var z	= "<?=$iterasi?>";
	var y	= parseInt(z);
	
  	for(ii=1;ii<=y;ii++){
		if(
			(a==opener.document.getElementById("i_op_tblItem_"+ii).value) && 
			(b==opener.document.getElementById("i_product_tblItem_"+ii).value)
		){
			alert ("kode Barang "+a+" sdh ada!");
			ada=true;
			break;
		}else{
			ada=false;
		}
    }
    
    if(!ada){		
		opener.document.getElementById('i_op_tblItem_'+'<?php echo $iterasi; ?>').value=a;
		opener.document.getElementById('i_product_tblItem_'+'<?php echo $iterasi; ?>').value=b;
		opener.document.getElementById('e_product_name_tblItem_'+'<?php echo $iterasi; ?>').value=c;
		opener.document.getElementById('n_deliver_tblItem_'+'<?php echo $iterasi; ?>').value=d;
		opener.document.getElementById('v_do_gross_tblItem_'+'<?php echo $iterasi; ?>').value=e;
		opener.document.getElementById('qty_product_tblItem_'+'<?php echo $iterasi; ?>').value=f;
		opener.document.getElementById('qty_op_tblItem_'+'<?php echo $iterasi; ?>').value=g;
		opener.document.getElementById('price_tblItem_'+'<?php echo $iterasi; ?>').value=h;
		opener.document.getElementById('i_op_sebunyi_tblItem_'+'<?php echo $iterasi; ?>').value=i;
		opener.document.getElementById('f_stp_tblItem_'+'<?php echo $iterasi; ?>').value=j;
		opener.document.getElementById('grosir_tblItem_'+'<?php echo $iterasi; ?>').value=k;
		opener.document.getElementById('n_deliver_tblItem_'+'<?php echo $iterasi; ?>').focus();
		this.close();
	}	
}


function getInfo(txt){

$.ajax({

type: "POST",
url: "<?php echo site_url('listdo/cform/flistop');?>",
data:"key="+txt+"&icust=<?php echo $icust; ?>",
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};

function carifocus() {
	document.getElementById('txtcari').focus();
}
</script>

</head>
<body id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h3>$page_title</h3></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="toUpper(document.getElementById('txtcari'));getInfo(this.value);"/>
  	</td>
  </tr>	
  <tr>
    <td align="left">
	<?php echo form_open('listdo/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	
	<div class="effect">
	  <div class="accordion2">

		<?php
		if( !empty($isi) || isset($isi) ) {
			if(count($isi)) {
				$cc	= 1; 
				foreach($isi as $row){
					$qtmso	= $this->mclass->cari_tm_so($row->iproduct);
					if($qtmso->num_rows()>0) {
						
						$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row->iproduct,$icust);
						$qhargadefault 		= $this->mclass->hargadefault($row->iproduct);
						
						if($qhargaperpelanggan->num_rows()>0){
							$rhargaperpelanggan = $qhargaperpelanggan->row();
							
							$hargaperunit = $rhargaperpelanggan->v_price;
							$harga = $row->qtyakhir*$hargaperunit;
							
						}elseif($qhargadefault->num_rows()>0){
							$rhargadefault = $qhargadefault->row();
							
							$hargaperunit = $rhargadefault->v_price;
							$harga = $row->qtyakhir*$hargaperunit;
							
						}else{
							$hargaperunit = $row->unitprice;
							$harga = $row->qtyakhir*$hargaperunit;
						}
						
						$list .= "
						 <tr>
						  <td>".$cc."</td>
						  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp', '$row->harga_grosir')\">".$row->iopcode."</a></td>	 
						  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp', '$row->harga_grosir')\">".$row->iproduct."</a></td>
						  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp', '$row->harga_grosir')\">".$row->qtyproduk."</a></td>
						  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp', '$row->harga_grosir')\">".$row->productname."</a></td>
						  <td><a href=\"javascript:settextfield('$row->iopcode','$row->iproduct','$row->productname','$row->qtyakhir','$harga','$row->qtyproduk','$row->qtyakhir','$hargaperunit','$row->iop','$row->stp', '$row->harga_grosir')\">".$row->qtyakhir."</a></td>
						 </tr>";
						 $cc+=1;					
					}
				}
			}
		}
		?>
		<table class="listtable2" border="1">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Nomor OP"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Kode BRG"); ?></th>
		 <th width="40px;"><?php echo strtoupper("Stok BRG"); ?></th>
		 <th><?php echo strtoupper("Nama Barang"); ?></th>
		 <th width="40px;"><?php echo strtoupper("Qty OP"); ?></th>
		<tbody>
		<div id="listItem"></div>		
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>";?>
  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
