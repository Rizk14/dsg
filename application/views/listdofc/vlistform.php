<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_do; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_do; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php
		echo $this->pquery->form_remote_tag(array('url'=>'listdofc/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterldobrgform">

		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_tgl_mulai_do; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?php echo $tgldomulai; ?>"/>
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)">
					<?php echo $list_tgl_akhir_do; ?> 
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?php echo $tgldoakhir; ?>"/>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)"></td>
				  </tr>	  
				  <tr>
					<td width="33%"><?php echo $list_no_do; ?></td>
					<td width="0%">:</td>
					<td>
					<?php
					$nomor_do	= array(
					'name'=>'nomor_do',
					'id'=>'nomor_do',
					'type'=>'text',
					'maxlength'=>'14',
					'value'=>$nomordo
					);
					echo form_input($nomor_do);
					?>
					</td>
				  </tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_do; ?></div></td>	
			</tr>
			<tr>
			  <td>
			  <? if($flag=='f'){?>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="2%" class="tdatahead">NO</td>
				  <td width="10%" class="tdatahead"><?php echo $list_no_op_do; ?></td>
				  <td width="25%" class="tdatahead"><?php echo $list_nm_cab_do; ?> </td>
				  <td width="11%" class="tdatahead"><?php echo $list_kd_brg_do; ?></td>
				  <td width="40%" class="tdatahead"><?php echo $list_nm_brg_do; ?> </td>
				  <td width="6%" class="tdatahead"><?php echo $list_qty_do; ?> </td>
				  <td width="8%" class="tdatahead"><?php echo $list_sisa_delivery_do; ?> </td>
				  <td width="15%" class="tdatahead" align="center"><?php echo $link_aksi; ?></td>
				</tr>
				<?php				
				$total	= 0;				
				if(sizeof($isi_f) > 0 ) {
					$arr= 0;
					$no	= 1;
					$cc	= 1;
					$nomororder	= array();
					
					foreach($isi_f as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

						$jmlDO	= $this->mclass->jmlDO($row->idocode);
						$njmlDO	= $jmlDO->num_rows();
												
						$query	= $this->mclass->cekstatusfaktur($row->idocode);
						if($query->num_rows()>0) {
							$diFaktur	= $query->num_rows();
						} else {
							$diFaktur	= 0;
						}
						
						$nomororder[$arr]	= $row->idocode;
						
						if($cc==1){
							if($diFaktur==0) {
								$link_act	= "
								<td align=\"center\" rowspan='$njmlDO'>
								 <a href=".base_url()."index.php/listdofc/cform/edit/".$row->idocode."
								 title=\"Edit Delivery Order (DO)\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Delivery Order (DO)\" border=\"0\"></a>&nbsp;
								<a href=".base_url()."index.php/listdofc/cform/undo/".$row->idocode." title=\"Cancel Edit Delivery Order (DO)\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Edit Delivery Order (DO)\" border=\"0\"></a></td>";
							} else {
								$link_act	= "<td align=\"center\" rowspan='$njmlDO'></td>";
							}
						}elseif($cc>1 && $nomororder[$arr-1]!=$row->idocode){
							if($diFaktur==0) {
								$link_act	= "
								<td align=\"center\" rowspan='$njmlDO'> <a href=".base_url()."index.php/listdofc/cform/edit/".$row->idocode."
								title=\"Edit Delivery Order (DO)\"><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Delivery Order (DO)\" border=\"0\"></a>&nbsp;
								<a href=".base_url()."index.php/listdofc/cform/undo/".$row->idocode." title=\"Cancel Edit Delivery Order (DO)\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Edit Delivery Order (DO)\" border=\"0\"></a></td>";
							} else {
								$link_act	= "<td align=\"center\" rowspan='$njmlDO'></td>";
							}						
						}else{
							if($diFaktur==0){
								$link_act	= '';
							}else{
								$link_act	= '';
							}
						}
																							
						$ldobrg	.= "
							<tr class=\"$Classnya\" onMouseOver=\"this.className='rowx'\"
              	 onMouseOut=\"this.className='$Classnya'\" >
							  <td class=\"tdatahead2\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
							  <td class=\"tdatahead2\">".$row->iopcode."</td>
							  <td class=\"tdatahead2\">".$row->ebranchname."</td>
							  <td class=\"tdatahead2\">".$row->iproduct."</td>
							  <td class=\"tdatahead2\">".$row->motifname."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->qty."</td>
							  <td class=\"tdatahead2\" align=\"right\">".$row->sisadelivery."</td>
							  ".$link_act."
							</tr>";
							$no+=1; $cc+=1;
							$arr+=1;
					}
				}
				?>
				
				<?php 
				echo $ldobrg;
				?>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table>
			  <? }else{ ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="5%" class="tdatahead">No</td>
				<td width="12%" class="tdatahead">Tanggal DO</td>
				<td width="13%" class="tdatahead">Nomor DO</td>
				<td width="36%" class="tdatahead">Pelanggan Cabang</td>
				<td width="13%" class="tdatahead">Status DO</td>
				<td width="12%" class="tdatahead">Status Faktur</td>
				<td width="9%" class="tdatahead">Aksi</td>
			  </tr>
			  <?php
			  if(sizeof($isi_t)>0){
			  	$nom=1;
			  	foreach($isi_t as $row){
					$query	= $this->mclass->cekstatusfaktur($row->idocode);
					if($query->num_rows()>0) {
						$diFaktur	= $query->num_rows();
					} else {
						$diFaktur	= 0;
					}

					if($diFaktur==0) {
						$link_act	= "
						<td align=\"center\">
						<a href=".base_url()."index.php/listdofc/cform/edit/".$row->idocode."
								title=\"Edit Delivery Order (DO)\">
	<img src=".base_url()."asset/theme/images/edit.gif width=12 height=13 alt=\"Edit Delivery Order (DO)\" border=\"0\"></a>&nbsp;
	<a href=".base_url()."index.php/listdofc/cform/undo/".$row->idocode." title=\"Cancel Edit Delivery Order (DO)\"><img src=".base_url()."asset/theme/images/cross.png width=12 height=13 alt=\"Cancel Edit Delivery Order (DO)\" border=\"0\"></a></td>";
					} else {
						$link_act	= "<td align=\"center\"></td>";
					}
																	
					$docancel	= $row->f_do_cancel=='t'?'Batal':'';
					$dofaktur	= $row->f_faktur_created=='t'?'Difakturkan':'Belum Faktur';

					$exp_ddo	= explode("-",$row->ddo,strlen($row->ddo)); // YYYY-mm-dd
					$ddo	= $exp_ddo[2]."/".$exp_ddo[1]."/".$exp_ddo[0];
						  					
			  ?>
				  <tr>
					<td class="tdatahead2"><?=$nom?></td>
					<td class="tdatahead2"><?php echo $ddo; ?></td>
					<td class="tdatahead2"><?php echo $row->idocode; ?></td>
					<td class="tdatahead2"><?php echo $row->ebranchname; ?></td>
					<td class="tdatahead2"><?php echo $docancel; ?></td>
					<td class="tdatahead2"><?php echo $dofaktur; ?></td>
					<?=$link_act?>
				  </tr>
			  <?php
			  		$nom+=1;
			  	}
			  }
			  ?>
			</table>
			  <? } ?>
			  </td>
			</tr>
			<tr><td align="center"><?php if($flag=='t') echo $create_link; ?></td></tr>
			<tr>
			  <td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:245px;">Delivery Order (DO) yg telah di buat faktur, <br />tdk dapat diubah atau dibatalkan!</div></td>
			</tr>			
			<tr>
				<td align="right">
				<input name="btnkeluar" id="btnkeluar" value="<?php echo $button_keluar; ?>" type="reset" onclick="window.location='<?php echo base_url(); ?>index.php/listdofc/cform/'">
</td>
			  
			</tr>
		  </table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
