<script type="text/javascript" language="javascript">

function totalharga(jml,iterasi) {
	
	var	harga;
	var	totalharga;
	
	harga	= document.getElementById('price_tblItem_'+iterasi);
	if(jml>0){
		totalharga	= parseInt(harga.value)*jml;
		if(isNaN(totalharga)){
			document.getElementById('v_do_gross_tblItem_'+iterasi).value=totalharga;
		}else{
			totalharga	= parseInt(harga.value)*jml;
			document.getElementById('v_do_gross_tblItem_'+iterasi).value=totalharga;
		}
	}else{
		document.getElementById('v_do_gross_tblItem_'+iterasi).value=harga.value;
	}
	
}

function validNum(column,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka.Terimakasih.");
		angka.value	= 0;
		angka.focus();
	}
}

function validStok(iterasi) {
	/* dgn qty op */
	var qty		= document.getElementById('qty_op_tblItem_'+iterasi);
	var masukan	= document.getElementById('n_deliver_tblItem_'+iterasi);
	if(masukan.value > parseInt(qty.value)) {
		alert("Maaf, quantity melebihi quantity OP yg ada!");
		masukan.value	= qty.value;
		masukan.focus();
	}
}

function validStok2(nilai,iterasi) {
	
	var qtyop	= document.getElementById('qty_op_tblItem_'+iterasi);
	var qty		= document.getElementById('qty_product_tblItem_'+iterasi);
	var masukan	= document.getElementById('n_deliver_tblItem_'+iterasi);
	
	if(parseInt(nilai) > parseInt(qty.value)) {	

		alert("Maaf, quantity melebihi stok yg ada!");

		masukan.focus();
	}
}

function getCabang(nilai){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listdofc/cform/cari_cabang');?>",
	data:"ibranch="+nilai,
	success: function(data){
	$("#cboPelanggan").html(data);
	
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function ckdo(nomor,code){
	var codelama	= code.value;
	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listdofc/cform/cari_do');?>",
	data:"ndo="+nomor+"&code="+codelama,
	success: function(data){
		$("#confnomordo").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function showData(nItem) {
	if (document.getElementById(nItem+'_select').style.display=='none') {			
		document.getElementById(nItem+'_select').style.display='block';		
		document.getElementById(nItem+'_select').style.position='absolute';	
	} else {
		document.getElementById(nItem).style.display='block';
		document.getElementById(nItem+'_select').style.display='none';
	} 
}
  
function hideData(nItem){
}

function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	
	var cab	= document.getElementById('i_branch');
	
	tbl.width='100%';
	
	if(iteration%2==0){
		var bgColorValue = '#FFFFFF';
	}else{
		var bgColorValue = '#FFFFFF';
	}
	row.bgColor = bgColorValue;

	var cell1 = row.insertCell(0);
	cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:10px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<DIV ID=\"ajax_i_op_"+nItem+"_"+iteration+"\" style=\"width:90px;\">" +
	"<input type=\"hidden\" ID=\"i_product_item_"+nItem+"_"+iteration+"\"  name=\"i_product_item_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"text\" ID=\"i_op_"+nItem+"_"+iteration+"\"  name=\"i_op_"+nItem+"_"+iteration+"\" style=\"width:55px;\" onclick=\"shproduklistdo('"+iteration+"','<?=$ibranch?>','<?=$icust?>');\"\"><img name=\"img_i_op_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor OP\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shproduklistdo('"+iteration+"','<?=$ibranch?>','<?=$icust?>');\"\">" +
	"<input type=\"hidden\" name=\"qty_product_"+nItem+"_"+iteration+"\" id=\"qty_product_"+nItem+"_"+iteration+"\">" +
	"<input type=\"hidden\" name=\"qty_op_"+nItem+"_"+iteration+"\" id=\"qty_op_"+nItem+"_"+iteration+"\"></DIV>";

	var cell1 = row.insertCell(2);
	cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" style=\"width:100px;\" ><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:95px;\"></DIV>" +
	"<div id=\"ajax_i_product_"+nItem+"_"+iteration+"_select\" style=\"display:none;\">" +
	"</div>";

	var cell1 = row.insertCell(3);
	cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" style=\"width:245px;\"><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:240px;\"></DIV>";

	var cell1 = row.insertCell(4);
	cell1.innerHTML = "<DIV ID=\"ajax_n_deliver_"+nItem+"_"+iteration+"\" style=\"width:85px;\">" +
	"<input type=\"text\" ID=\"n_deliver_"+nItem+"_"+iteration+"\" name=\"n_deliver_"+nItem+"_"+iteration+"\" style=\"width:81px;text-align:right;\" onkeyup=\"validNum('n_deliver_tblItem','"+iteration+"');validStok("+iteration+");validStok2(this.value,"+iteration+");totalharga(this.value,"+iteration+");\"></DIV>";

	var cell1 = row.insertCell(5);
	cell1.innerHTML = "<DIV ID=\"ajax_v_do_gross_"+nItem+"_"+iteration+"\" style=\"width:85px;\"><input type=\"text\" ID=\"v_do_gross_"+nItem+"_"+iteration+"\"  name=\"v_do_gross_"+nItem+"_"+iteration+"\" style=\"width:80px;text-align:right;\" onkeyup=\"validNum('v_do_gross_tblItem','"+iteration+"')\"></DIV>";

	var cell1 = row.insertCell(6);
	cell1.innerHTML = "<DIV ID=\"ajax_e_note_"+nItem+"_"+iteration+"\" style=\"width:130px;\"><input type=\"text\" ID=\"e_note_"+nItem+"_"+iteration+"\"  name=\"e_note_"+nItem+"_"+iteration+"\" style=\"width:100px;\">" +
	"<input type=\"hidden\" ID=\"price_"+nItem+"_"+iteration+"\"  name=\"price_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"hidden\" ID=\"i_op_sebunyi_"+nItem+"_"+iteration+"\"  name=\"i_op_sebunyi_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"hidden\" ID=\"f_stp_"+nItem+"_"+iteration+"\"  name=\"f_stp_"+nItem+"_"+iteration+"\" >" +
	"<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\"></DIV>";
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}

function hitungnilai(j) {
			if ($('input[name=is_grosir_'+j+']').is(':checked')) {
				//v_do_gross_tblItem_
				var harga_grosir= $('#grosir_tblItem_'+j).val();
				var qty = $('#n_deliver_tblItem_'+j).val();
				var subtotal = parseInt(harga_grosir)*parseInt(qty);
				$('#v_do_gross_tblItem_'+j).val(subtotal);
			}
			else {
				var hjp= $('#price_tblItem_'+j).val();
				var qty = $('#n_deliver_tblItem_'+j).val();
				var subtotal = parseInt(hjp)*parseInt(qty);
				//alert(qty);
				$('#v_do_gross_tblItem_'+j).val(subtotal);
			}
	
}

function adddata() {
	var no=$("#no").val();		
	var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html("<div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">"+no+"</div>");		
		//******end no*********************************
				
		//*****i_op_tblItem_*************************************
	/*	var i_op_tblItem="#i_op_tblItem_"+n;
		var new_i_op_tblItem="#i_op_tblItem_"+no;
		$(i_op_tblItem, lastRow).attr("id", "i_op_tblItem_"+no);
		$(new_i_op_tblItem, lastRow).attr("name", "i_op_tblItem_"+no);	
		$(new_i_op_tblItem, lastRow).val(''); */
		//*****end new_i_op_tblItem*********************************
				
		//*****td2*************************************
		var td2="#td2_"+n;
		var new_td2="#td2_"+no;
		$(td2, lastRow).attr("id", "td2_"+no);
		$(new_td2, lastRow).html("<DIV ID=\"ajax_i_op_tblItem_"+no+"\" style=\"width:90px;\">"+
								"<input type=\"hidden\" ID=\"i_product_item_tblItem_"+no+"\" name=\"i_product_item_tblItem_"+no+"\" style=\"width:95px;\" value=''>"+
								"<input type=\"hidden\" ID=\"n_deliver_item_tblItem_"+no+"\"  name=\"n_deliver_item_tblItem_"+no+"\" value=''>"+
								"<input type=\"text\" ID=\"i_op_tblItem_"+no+"\" name=\"i_op_tblItem_"+no+"\" style=\"width:65px;\" onclick=\"shproduklistdo('"+no+"','<?php echo $ibranch ?>','<?php echo $icust ?>');\" value=''><img name=\"img_i_op_\" src=\"<?php echo base_url() ?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor OP\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shproduklistdo('"+no+"','<?php echo $ibranch ?>','<?php echo $icust ?>')\">"+
								"<input type=\"hidden\" name=\"qty_product_tblItem_"+no+"\" id=\"qty_product_tblItem_"+no+"\" value=''>"+
								"<input type=\"hidden\" name=\"qty_op_tblItem_"+no+"\" id=\"qty_op_tblItem_"+no+"\" value=''></DIV>");	
		//*****end td2*********************************
		
		//*****td3*************************************
		var td3="#td3_"+n;
		var new_td3="#td3_"+no;
		$(td3, lastRow).attr("id", "td3_"+no);
		$(new_td3, lastRow).html("<DIV ID=\"ajax_i_product_tblItem_"+no+"\" style=\"width:100px;\" ><input type=\"text\" ID=\"i_product_tblItem_"+no+"\" name=\"i_product_tblItem_"+no+"\" style=\"width:95px;\" value=''></DIV><div id=\"ajax_i_product_tblItem_"+no+"_select\" style=\"display:none;\"></div>");	
		//*****end td3*********************************
		
		//*****td4*************************************
		var td4="#td4_"+n;
		var new_td4="#td4_"+no;
		$(td4, lastRow).attr("id", "td4_"+no);
		$(new_td4, lastRow).html("<DIV ID=\"ajax_e_product_name_tblItem_"+no+"\" style=\"width:245px;\"><input type=\"text\" ID=\"e_product_name_tblItem_"+no+"\"  name=\"e_product_name_tblItem_"+no+"\" style=\"width:240px;\" value=''></DIV>");	
		//*****end td4*********************************
		
		//*****td5*************************************
		var td5="#td5_"+n;
		var new_td5="#td5_"+no;
		$(td5, lastRow).attr("id", "td5_"+no);
		$(new_td5, lastRow).html("<DIV ID=\"ajax_n_deliver_tblItem_"+no+"\" style=\"width:55px;\"><input type=\"text\" ID=\"n_deliver_tblItem_"+no+"\" name=\"n_deliver_tblItem_"+no+"\" style=\"width:55px;text-align:right;\" onkeyup=\"validNum('n_deliver_tblItem','"+no+"');validStok('"+no+"');validStok2(this.value,'"+no+"');totalharga(this.value,'"+no+"');\" value=''></DIV>");	
		//*****end td5*********************************
		
		//*****td6*************************************
		var td6="#td6_"+n;
		var new_td6="#td6_"+no;
		$(td6, lastRow).attr("id", "td6_"+no);
		$(td6, lastRow).attr("style", "display:none");
		$(new_td6, lastRow).html("<input type=\"hidden\" ID=\"price_tblItem_"+no+"\"  name=\"price_tblItem_"+no+"\" value='' style=\"width:60px;text-align:right;\">");	
		//*****end td6*********************************
		
		//*****td7*************************************
		var td7="#td7_"+n;
		var new_td7="#td7_"+no;
		$(td7, lastRow).attr("id", "td7_"+no);
		$(td7, lastRow).attr("style", "display:none");
		$(new_td7, lastRow).html("<input type='hidden' name='is_grosir_"+no+"' id='is_grosir_"+no+"' value=''> <input type=\"hidden\" ID=\"grosir_tblItem_"+no+"\"  name=\"grosir_tblItem_"+no+"\" value='0' readonly='true' style=\"width:60px;text-align:right;\">");	
		//*****end td7********************************* 
		
		//*****td8*************************************
		var td8="#td8_"+n;
		var new_td8="#td8_"+no;
		$(td8, lastRow).attr("id", "td8_"+no);
		$(td8, lastRow).attr("style", "display:none");
		$(new_td8, lastRow).html("<DIV ID=\"ajax_v_do_gross_tblItem_"+no+"\" style=\"width:85px;\"><input type=\"hidden\" ID=\"v_do_gross_tblItem_"+no+"\"  name=\"v_do_gross_tblItem_"+no+"\" style=\"width:80px;text-align:right;\" onkeyup=\"validNum('v_do_gross_tblItem','"+no+"')\" value='0'></DIV>");	
		//*****end td8*********************************
		
		//*****td9*************************************
		var td9="#td9_"+n;
		var new_td9="#td9_"+no;
		$(td9, lastRow).attr("id", "td9_"+no);
		$(new_td9, lastRow).html("<DIV ID=\"ajax_e_note_tblItem_"+no+"\" style=\"width:130px;\">"+
								"<input type=\"text\" ID=\"e_note_tblItem_"+no+"\"  name=\"e_note_tblItem_"+no+"\" style=\"width:100px;\">"+
								"<input type=\"hidden\" ID=\"price_tblItem_"+no+"\"  name=\"price_tblItem_"+no+"\" value=''>"+
								"<input type=\"hidden\" ID=\"i_op_sebunyi_tblItem_"+no+"\" name=\"i_op_sebunyi_tblItem_"+no+"\" value=''>"+
								"<input type=\"hidden\" ID=\"f_stp_tblItem_"+no+"\" name=\"f_stp_tblItem_"+no+"\" value=''>"+
								"<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\""+no+"\"></DIV>");	
		//*****end td9*********************************
		
		//*****td10*************************************
		var td10="#td10_"+n;
		var new_td10="#td10_"+no;
		$(td10, lastRow).attr("id", "td10_"+no);
		//$(td10, lastRow).attr("style", "display:none");
		$(new_td10, lastRow).html("<DIV ID=\"adaboneka_tblItem_"+no+"\" ><select id=\"adaboneka_tblItem_"+no+"\" name=\"adaboneka_tblItem_"+no+"\"><option value='0'>Tidak</option><option value='1'>Ya</option></select></DIV>");	
		//*****end td10*********************************
		
		// 06-09-2014
		//*****td11*************************************
		var td11="#td11_"+n;
		var new_td11="#td11_"+no;
		$(td11, lastRow).attr("id", "td11_"+no);
		$(new_td11, lastRow).html("<DIV ID=\"perwarna_tblItem_"+no+"\" >&nbsp;</DIV>");	
		//*****end td11*********************************
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
}

function deldata() {
	var x= $("#no").val();	
		var jumawal = $("#jumawal").val();
		//if (x>2) {
		if (parseInt(x) > parseInt(jumawal)) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
}

// 09-09-2014
function konfirm() {
	var kon;
	var j = 1;
	var s = 0;
	
	kon = window.confirm("Yakin akan update data ini ??");
	
	if (kon) {
		if(document.getElementById('i_customer').value=='') {
			alert('Pilih Pelanggan !!');
			return false;
		}
		
		if(document.getElementById('i_branch').value=='') {
			alert('Pilih Cabang Pelanggan !!');
			return false;
		}
		
		var iter = document.getElementById('no');
		
		if(iter.value!='') {
			var jumdata = parseInt(iter.value-1);
			while(j<=iter.value) {
				var n_deliver = document.getElementById('n_deliver_tblItem_'+j).value;
				if(n_deliver==0 || n_deliver=='') {
					s = 1;
					alert('Jml DO tdk boleh nol!');
					return false;
				}
							
				var kodebrg= $('#i_product_tblItem_'+j).val();
				var qtytotal= $('#n_deliver_tblItem_'+j).val();
				var qty_warna = document.forms[0].elements["qty_warna_"+j+"[]"];
				//alert(qty_warna.length);
				var totqty = 0;
				for (var i = 0, len = qty_warna.length; i < len; i++) {
				  
				  totqty+=parseInt(qty_warna[i].value);
				}
				
				//alert(totqty);
				if (parseInt(totqty) > parseInt(qtytotal)) {
					alert("qty warna utk barang "+kodebrg+" melebihi qty total, silahkan dikoreksi dulu ya");
					return false;
				}
				/*else if (parseInt(totqty) < parseInt(qtytotal)) {
					alert("qty warna utk barang "+kodebrg+" kurang dari qty total, silahkan dikoreksi dulu ya");
					return false;
				} */
				
				j+=1;
			} // end while
	}
	else {
		alert("Item barang di DO tidak ada");
		return false;
	}	
		//alert(iter.value);
		//return false;
	}
	else
		return false;
}

</script>
<?php $jumawal = count($doitem)+1; ?>
<input type="hidden" name="no" id="no" value="<?php echo $jumawal ?>">
<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_do; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_do; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		    <?php 
		       $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listdofc/cform/actedit', $attributes);?>
			
		<div id="masterdoform">

		      <table>
		      	<tr>
			  <td width="10%"><?php echo $form_nomor_do; ?></td>
			  <td width="1%">:</td>
			  <td width="18%">
				<?php
				 $nodo = array(
					'name'=>'i_do',
					'id'=>'i_do',
					'value'=>$no,
					'maxlength'=>'20',
					'onkeyup'=>"ckdo(this.value,document.getElementById('i_do_code_hidden'))"
					);
				 echo form_input($nodo);
				?>	
				<div id="confnomordo" style="color:#FF0000;"></div>
			  </td>
			  <td width="6%"><?php echo $form_tgl_do; ?></td>
			  <td width="1%">:</td>
			  <td width="18%">
				<?php
				 $tgldo = array(
					'name'=>'d_do',
					'id'=>'d_do',
					'value'=>$tgDO,
					'maxlength'=>'10'
					);
				 echo form_input($tgldo);
				?>
				<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do,'dd/mm/yyyy',this)">
			  </td>
			  <td width="10%"><?php echo $form_cabang_do; ?></td>
			  <td width="1%">:</td>
			  <td width="18%">
				<?php
				$lpelanggan	.= "<select name=\"i_customer\" onchange=\"getCabang(this.value);\" id=\"i_customer\" disabled >"; 
				  $lpelanggan	.= "<option value=\"\">[".$form_option_pel_do."]</option>";
				  foreach($opt_pelanggan as $key => $row) {	
				  	$Sel	= $icust==$row->code?"selected":"";
					$lpelanggan .= "<option value=\"$row->code\" $Sel >".$row->customer."</option>";
				  }
				$lpelanggan	.= "</select>";
				echo $lpelanggan;
				?>
				&nbsp;
			  </td>
			  <td>
			  <select name="i_branch" id="i_branch">
			  	<?php
				$lcabang	= "";
				foreach($opt_cabang as $row2) {
					$Sel2	= $ibranch==$row2->ibranchcode?"selected":"";
					$lcabang .= "<option value=\"$row2->ibranchcode\" $Sel2 >".$row2->ebranchname."</option>";	
				}
				echo $lcabang;
				?>
			  </select>	
			  </td>
			</tr>
			<tr>
			 <td colspan="10">&nbsp;</td>
			</tr>
			
			<tr>
			 <td colspan="10">
				<table width="100%" id="tabelku">
				<tr>
					 <td colspan="10">
					  <div id="title-box2"><?php echo $form_title_detail_do; ?>
					  <div style="float:right;">

					  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();adddata();"><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
					  <a class="button"  href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();deldata();" ><span><img src="<?php echo base_url();?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
					  </div></div>
					 </td>	
					</tr>
					<tr>
					 <td width="2%" class="tdatahead">NO</td>
					 <td width="12%" class="tdatahead"><?php echo $form_nomor_op_do; ?></td>
					 <td width="12%" class="tdatahead"><?php echo $form_kode_produk_do; ?></td>
					 <td width="30%" class="tdatahead"><?php echo $form_nm_produk_do; ?></td>
					 <td width="6%" class="tdatahead"><?php echo $form_jml_product_do; ?></td>
					 <td width="5%" class="tdatahead" nowrap>QTY<br>PERWARNA</td>
					 <td width="8%" class="tdatahead">ADA BONEKA?</td>
				<!--	 <td width="8%" class="tdatahead">HJP</td>
					 <td width="8%" class="tdatahead">HARGA GROSIR</td>
					 <td width="10%" class="tdatahead">SUBTOTAL</td> -->
					 <td width="20%" class="tdatahead"><?php echo $form_ket_do; ?></td>
					</tr>
					<?php			
						$db2=$this->load->database('db_external', TRUE);		    
						$iter	= 1;
					  	foreach($doitem as $row3) {
							
							$qcari_qty_order = $this->mclass->cari_qtyorder($row3->iop,$row3->iproduct);
							$rcari_qty_order = $qcari_qty_order->row();
							
							// 27-06-2012, ambil harga
							$qhargaperpelanggan = $this->mclass->hargaperpelanggan($row3->iproduct,$icust);
							$qhargadefault 	    = $this->mclass->hargadefault($row3->iproduct);
								
							if($qhargaperpelanggan->num_rows()>0){
								$rhargaperpelanggan = $qhargaperpelanggan->row();
									
								$hargaperunit = $rhargaperpelanggan->v_price;
								//$harga = $row->qtyakhir*$hargaperunit;
									
							}elseif($qhargadefault->num_rows()>0){
								$rhargadefault = $qhargadefault->row();
									
								$hargaperunit = $rhargadefault->v_price;
								//$harga = $row->qtyakhir*$hargaperunit;
									
							}else{
								$hargaperunit = $row3->unitprice;
								//$harga = $row->qtyakhir*$hargaperunit;
							}
							// -------------------------
							$hargaperunit = round($hargaperunit, 2);
							
							$detailwarna = array();
							// 05-09-2014, warna brg jadi
							// query ambil data2 warna berdasarkan kode brgnya
							$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name, c.qty FROM tr_product_color a, tr_color b, tm_dofc_item_color c
														WHERE a.i_color = b.i_color AND c.i_color = b.i_color
														AND c.i_do_item = '".$row3->i_do_item."'
														AND a.i_product_motif = '".$row3->iproduct."' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->result();
								foreach ($hasilxx as $rowxx) {
									$detailwarna[] = array(	'i_product_color'=> $rowxx->i_product_color,
															'i_color'=> $rowxx->i_color,
															'e_color_name'=> $rowxx->e_color_name,
															'qty'=> $rowxx->qty
														);
								}
							}
							else
								$detailwarna = '';
						
							echo "
							<tr>
								<td id='num_".$iter."'><div style=\"font:24px;text-align:right;width:10px;margin-right:0px;\">".$iter."</div></td>
								<td id='td2_".$iter."'><DIV ID=\"ajax_i_op_tblItem_".$iter."\" style=\"width:90px;\">
								<input type=\"hidden\" ID=\"i_product_item_tblItem_".$iter."\" name=\"i_product_item_tblItem_".$iter."\" style=\"width:95px;\" value=\"".$row3->iproduct."\">
								<input type=\"hidden\" ID=\"n_deliver_item_tblItem_".$iter."\"  name=\"n_deliver_item_tblItem_".$iter."\" value=\"".$row3->qty."\">
								<input type=\"text\" ID=\"i_op_tblItem_".$iter."\" name=\"i_op_tblItem_".$iter."\" style=\"width:65px;\" onclick=\"shproduklistdo('".$iter."','".$ibranch."','".$icust."');\" value=\"".$row3->iopcode."\"><img name=\"img_i_op_\" src=\"".base_url()."asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor OP\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shproduklistdo('".$iter."','".$ibranch."','".$icust."')\">
								<input type=\"hidden\" name=\"qty_product_tblItem_".$iter."\" id=\"qty_product_tblItem_".$iter."\" value=\"".$row3->qtyproduk."\">
								<input type=\"hidden\" name=\"qty_op_tblItem_".$iter."\" id=\"qty_op_tblItem_".$iter."\" value=\"".$rcari_qty_order->n_count."\"></DIV></td>
								<td id='td3_".$iter."'><DIV ID=\"ajax_i_product_tblItem_".$iter."\" style=\"width:100px;\" ><input type=\"text\" ID=\"i_product_tblItem_".$iter."\" name=\"i_product_tblItem_".$iter."\" style=\"width:95px;\" value=\"".$row3->iproduct."\"></DIV><div id=\"ajax_i_product_tblItem_".$iter."_select\" style=\"display:none;\"></div></td>
								<td id='td4_".$iter."'><DIV ID=\"ajax_e_product_name_tblItem_".$iter."\" style=\"width:245px;\"><input type=\"text\" ID=\"e_product_name_tblItem_".$iter."\"  name=\"e_product_name_tblItem_".$iter."\" style=\"width:240px;\" value=\"".$row3->motifname."\"></DIV></td>
								<td id='td5_".$iter."'><DIV ID=\"ajax_n_deliver_tblItem_".$iter."\" style=\"width:55px;\"><input type=\"text\" ID=\"n_deliver_tblItem_".$iter."\" name=\"n_deliver_tblItem_".$iter."\" style=\"width:55px;text-align:right;\" onkeyup=\"validNum('n_deliver_tblItem','".$iter."');validStok('".$iter."');validStok2(this.value,'".$iter."');totalharga(this.value,'".$iter."');\" value=\"".$row3->qty."\"></DIV></td>".
								
								"<td id='td11_".$iter."' style='white-space:nowrap;' align='right'><DIV ID=\"perwarna_tblItem_".$iter."\" style=\"width:140px;\" >";
								if (is_array($detailwarna)) {
									for($j=0;$j<count($detailwarna);$j++) {
									
										echo $detailwarna[$j]['e_color_name']."&nbsp;
											<input type='text' name='qty_warna_".$iter."[]' value='".$detailwarna[$j]['qty']."' style=\"width:40px; text-align:right;\">
											<input type='hidden' name='i_color_".$iter."[]' value='".$detailwarna[$j]['i_color']."'><br>
											
											";
									}
								}
								echo "<hr></DIV></td>";
								
								echo "<td id='td10_".$iter."'><DIV ID=\"adaboneka_tblItem_".$iter."\" >
							<select id=\"adaboneka_tblItem_".$iter."\" name=\"adaboneka_tblItem_".$iter."\">
								<option value='0' ";
								if ($row3->is_adaboneka == '0') {
									echo "selected"; }
								echo ">Tidak</option>
								<option value='1' ";
								if ($row3->is_adaboneka == '1') {
									echo "selected"; }
								echo ">Ya</option>
							</select></DIV></td>
								
								<td id='td6_".$iter."' style='display:none'><input type=\"hidden\" ID=\"price_tblItem_".$iter."\"  name=\"price_tblItem_".$iter."\" value=\"".$hargaperunit."\" style=\"width:60px;text-align:right;\"></td>
								<td id='td7_".$iter."' nowrap style='display:none'><input type='hidden' name='is_grosir_".$iter."' id='is_grosir_".$iter."' value='' onclick='hitungnilai(".$iter.")' onblur='hitungnilai(".$iter.")'";
								if ($row3->is_grosir == 't') {
								 echo "checked='true'";
								}
								echo ">
								<input type=\"hidden\" ID=\"grosir_tblItem_".$iter."\"  name=\"grosir_tblItem_".$iter."\" readonly='true' value=\"".$row3->harga_grosir."\" style=\"width:60px;text-align:right;\"></td>
								
								<td id='td8_".$iter."' style='display:none'><DIV ID=\"ajax_v_do_gross_tblItem_".$iter."\" style=\"width:85px;\"><input type=\"hidden\" ID=\"v_do_gross_tblItem_".$iter."\"  name=\"v_do_gross_tblItem_".$iter."\" style=\"width:80px;text-align:right;\" onkeyup=\"validNum('v_do_gross_tblItem','".$iter."')\" value=\"".$row3->harga."\"></DIV></td>
								<td id='td9_".$iter."'><DIV ID=\"ajax_e_note_tblItem_".$iter."\" style=\"width:130px;\">
								<input type=\"text\" ID=\"e_note_tblItem_".$iter."\"  name=\"e_note_tblItem_".$iter."\" style=\"width:100px;\">
								<input type=\"hidden\" ID=\"price_tblItem_".$iter."\"  name=\"price_tblItem_".$iter."\" value=\"".$row3->unitprice."\">
								<input type=\"hidden\" ID=\"i_op_sebunyi_tblItem_".$iter."\" name=\"i_op_sebunyi_tblItem_".$iter."\" value=\"".$row3->iop."\">
								<input type=\"hidden\" ID=\"f_stp_tblItem_".$iter."\" name=\"f_stp_tblItem_".$iter."\" value=\"".$row3->stp."\">
								<input type=\"hidden\" id=\"iteration\" name=\"iteration\" value=\"".$iter."\"></DIV></td>
							</tr>";
							$iter++;
						}
						
						$stpnya	= '';
						
						foreach($doitem as $row4) {
							$stpnya.=$row4->stp.";";
						}
						
					  ?>

					<!--<tr>
					 <td colspan="7">
					  <table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
					  					  
					  </table>
					</tr> -->
				</table>
			 </td>
			</tr>
		      	<tr>
			  <td colspan="10" align="right">
				<input type="hidden" name="stpnya" id="stpnya" value="<?=$stpnya?>" />
			  	<input type="hidden" name="i_do_code_hidden" id="i_do_code_hidden" value="<?=$no?>" />
				<input type="hidden" name="i_do_hidden" id="i_do_hidden" value="<?=$ido?>" />
				<input type="hidden" name="i_branch_hidden" id="i_branch_hidden" value="<?=$ibranch?>" />
				<input type="hidden" name="i_customer_hidden" id="i_customer_hidden" value="<?=$icust?>" />
				<!--<input name="btnrefresh" type="button" id="btnrefresh" value="Refresh Harga Barang" onclick="show('listdofc/cform/refreshitembrg/<?=$ido?>/<?=$icust?>/','#content');" <?php if($disabled=='t'){?> disabled <? } ?> /> -->
			    <input name="btnupdate" id="btnupdate" value="<?php echo $button_update; ?>" type="submit" onclick="return konfirm();">
			   	<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listdofc/cform/'">

			  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
