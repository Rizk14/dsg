<h3>Data Permintaan Pembelian (PP) Bahan Baku</h3><br>
<a href="<?php echo base_url(); ?>index.php/pp-bb/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/pp-bb/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('pp-bb/cform/cari'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="75%">
	<thead>
	 <tr>
		 <th>Nomor PP</th>
		 <th>Tanggal PP</th>
		 <th>List Barang</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			 
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++) {

				$qstatusop	= $this->mmaster->statusop($query[$j]['id']);
				$jstatusop	= $qstatusop->num_rows();
				if($jstatusop>0) {
					$jmlop	= $jstatusop;
				} else {
					$jmlop	= 0;
				}
						 
			 	 $bl	= array(
				 	'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'Nopermber',
					'12'=>'Desember'
				 );
				 
			 	 $etgl	= explode("-",$query[$j]['tgl_pp'],strlen($query[$j]['tgl_pp'])); // YYYY-mm-dd
				 $hr	= substr($etgl[2],0,1)==0?substr($etgl[2],1,1):$etgl[2];
				 $nbl	= $etgl[1];
				 $ntgl	= $hr." ".$bl[$nbl]." ".$etgl[0];
				 
				 $etgl_last	= explode("-",$query[$j]['tgl_update'],strlen($query[$j]['tgl_update'])); // YYYY-mm-dd
				 $hr_last	= substr($etgl_last[2],0,1)==0?substr($etgl_last[2],1,1):$etgl_last[2];
				 $nbl_last	= $etgl_last[1];
				 $ntgl_last	= $hr_last." ".$bl[$nbl_last]." ".$etgl_last[0];
				 
				 echo "<tr class=\"record\">";
				 echo "<td>".$query[$j]['no_pp']."</td>";
				 echo "<td>".$ntgl."</td>";
				 echo "<td>";
				 
				 if (is_array($query[$j]['detail_pp'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pp'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (".$var_detail[$k]['qty']." ".$var_detail[$k]['satuan'].")";
						  if ($k<$hitung-1)
						     echo ", ";
					}
				 }
				 
				 echo "</td>";
				 echo "<td>".$ntgl_last."</td>";
				 echo "<td align=center>";
				 if($jmlop==0) {
					 echo "<a href=".base_url()."index.php/pp-bb/cform/index/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a>";
					 echo "&nbsp; <a href=".base_url()."index.php/pp-bb/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 }
				 echo "</td>";
				 echo "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links(); ?>
</div>
