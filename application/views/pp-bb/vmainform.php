<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<h3>Data Permintaan Pembelian (PP) Bahan Baku</h3><br>
<a href="<? echo base_url(); ?>index.php/pp-bb/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/pp-bb/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script language="javascript" type="text/javascript">

function ckitem() {
	if(document.getElementById('kode_1').value=='') {
		alert('Maaf, item Bahan Baku hrs diisi.');
		document.getElementById('kode_1').focus();
		return false;
	}
}

function ckpp(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('pp-bb/cform/cari_pp');?>",
	//data:"nomorpp="+nomor+'&ccc=',
	data:"nomorpp="+nomor,
	success: function(data){
		$("#confnomorpp").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

//tambah
$(function()
{
	var goedit = $("#goedit").val();
	if(goedit!='1')
		$("#no").val('2');
	
	$("#no_faktur").blur(function()
		{
		 var invoice_number=$("#no_faktur").val();
		
		 $("#msgbox").removeClass().addClass('messagebox').html('<img src="modules/ProitInventory/pnimages/spinner_small.gif">').fadeIn("slow");
		
		 $.post("index.php?module=ProitInventory&type=admin&func=cek_invoice_penjualan",
		 { 
		 	invoice_number:invoice_number } ,function(data)
		 { 	
		 
		 
		 var cek=$("status",data).html()
		 var tgl=$("tgl",data).html()
		 $('#tgl_retur').val(tgl);
		 
		 	
		  if(cek=='0') 
		  {
		   $("#msgbox").fadeTo(200,0.1,function() 
		   {			
			$(this).html('No. Invoice yang anda masukkan tidak valid').addClass('messageboxerror').fadeTo(300,1);
		   });
		  }
		  else
		  {
		   $("#msgbox").fadeTo(200,0.1,function()  
		   {			
			$(this).html('<img src="modules/ProitInventory/pnimages/checkbullet.gif">').addClass('messageboxok').fadeTo(300,1);
		   });
		  }
		  
		 });
		});
	
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****jumlah*************************************
		var jumlah="#jumlah_"+n;
		var new_jumlah="#jumlah_"+no;
		$(jumlah, lastRow).attr("id", "jumlah_"+no);
		$(new_jumlah, lastRow).attr("name", "jumlah_"+no);		
		$(new_jumlah, lastRow).val('');				
		//*****end jumlah*************************************	
		
		//*****subtotal*************************************
		var subtotal="#keterangan_"+n;
		var new_subtotal="#keterangan_"+no;
		$(subtotal, lastRow).attr("id", "keterangan_"+no);
		$(new_subtotal, lastRow).attr("name", "keterangan_"+no);		
		$(new_subtotal, lastRow).val('');				
		//*****end subtotal*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/pp-bb/cform/show_popup_brg/"+no+"/');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>
<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pp-bb/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="goedit" id="goedit" <?php if ($edit == '1') { ?> value="1" <?php } else { ?> value="" <?php } ?> >
<input type="hidden" name="kodeeditnopp" id="kodeeditnopp" value="<?php echo $eno_pp ?>">
<input type="hidden" name="id_pp" id="id_pp" value="<?php echo $eid ?>">

<?php
if ($edit==1) {
	if (count($pp_detail)>0) {
		$no=1;
		foreach ($pp_detail as $hitung) {
			$no++;
		}
	}
	else $no=2;
}
?>

<input type="hidden" name="no" id="no" <?php if ($edit==1) { ?> value="<?php echo $no ?>" <?php } ?> >
<input type="hidden" name="iddata" id="iddata" />

<div align="center">

<br>
<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1">
  <tr>
    <td width="15%">Nomor PP </td>
    <td>
      <input name="no_faktur" type="text" id="no_faktur" size="10" maxlength="10" value="<?php echo $eno_pp ?>" onkeyup="ckpp(this.value);">&nbsp;<span id="msgbox" style="display:none;"></span>
	  <div id="confnomorpp" style="color:#FF0000; font-size:10px;"></div>
    </td>
  </tr>
  <tr>
    <td>Tanggal PP </td>
    <td>
      <input name="tgl_retur" type="text" id="tgl_retur" size="10" value="<?php echo $etgl_pp ?>">
	  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].tgl_retur,'dd-mm-yyyy',this)">
	  <!-- <img alt="" id="tgl_retur" align="middle"
			title="Pick date.." src="<?php echo base_url();?>images/calendar.gif"
			onmouseover="if (timeoutId) clearTimeout(timeoutId);return true;"
			onclick="g_Calendar.show(event,'f_purchase.tgl_retur',false,'dd-mm-yyyy');return false;"
			onmouseout="if (timeoutDelay) calendarTimeout();"> -->
	</td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="5" align="right"><input id="returpurchase" type="button" name="returpurchase" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
	      <th>Qty</th>
          <th>Keterangan</th>
        </tr>
        <?php if ($edit == '') { ?>
         <tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_1" type="text" id="kode_1" size="20" readonly="true"/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" onclick="javascript:openCenteredWindow('<?php echo base_url(); ?>index.php/pp-bb/cform/show_popup_brg/1/');" type="button"></td>
         <td><input name="nama_1" type="text" id="nama_1" size="30" readonly="true"/></td>
         <td><input name="jumlah_1" type="text" id="jumlah_1" size="10" /></td>
         <td><input name="keterangan_1" type="text" id="keterangan_1" size="30" /></td>
        </tr>
        <?php } else { $i=1;
        if (count($pp_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_1" type="text" id="kode_1" size="20" readonly="true"/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" onclick="javascript:openCenteredWindow('<?php echo base_url(); ?>index.php/pp-bb/cform/show_popup_brg/1/');" type="button"></td>
          <td><input name="nama_1" type="text" id="nama_1" size="30" readonly="true"/></td>
         <td><input name="jumlah_1" type="text" id="jumlah_1" size="10" /></td>
          <td><input name="keterangan_1" type="text" id="keterangan_1" size="30" /></td>
        </tr>
		
		<?php
		}
			for($j=0;$j<count($pp_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $pp_detail[$j]['kode_brg'] ?>"/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=<?php echo $i ?>" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" onclick="javascript:openCenteredWindow('<?php echo base_url(); ?>index.php/pp-bb/cform/show_popup_brg/<?php echo $i ?>/<?php echo $jenis_brg ?>');" type="button"></td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $pp_detail[$j]['nama'] ?>" /></td>
         <td><input name="jumlah_<?php echo $i ?>" type="text" id="jumlah_<?php echo $i ?>" size="10" value="<?php echo $pp_detail[$j]['qty'] ?>" /></td>
          <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="30" value="<?php echo $pp_detail[$j]['keterangan'] ?>" /></td>
        </tr>
		<?php $i++; } // end foreach ?>
        <?php } ?>
	</table>	
	</form>
      <div align="center"><br><br> 
        
        <?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return ckitem();">&nbsp;<input type="reset" name="batal" value="Reset">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/pp-bb/cform/view'"><?php } ?>
      </div></td>
    </tr>

</table>
</div>
</form>
