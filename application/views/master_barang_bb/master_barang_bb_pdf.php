<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Data Master Barang WIP</title>
    <style type="text/css">
        h1 {text-align:center; font-size:18px;}
        h2 {font-size:14px;}
        .tengah {text-align:center;	}
        .kiri {padding-left:10px;}
        table.record {border-collapse: collapse;}
        table.record td {border: 1px solid #000000}
    </style>
</head>

<body>
<h1>Data Master Barang BB </h1>
<hr />
<table width="540" class="record" align='center'> 
			 <tr>
			 <td class="tengah">No</td>
			 <td class="tengah">Kode Barang</td>
			 <td class="tengah">Nama Barang</td>
			 <td class="tengah">Jenis Barang WIP</td>
			 <td class="tengah">Kelompok Barang WIP</td>
 </tr>
  <?php
			if (is_array($master_barang_bb)) {
				
				$i=1;
		
foreach ($master_barang_bb as $mst_brg){

  echo  "<tr>";
  echo  "<td class=\"tengah\" width=\"20\">".$i."</td>";
  echo  "<td class=\"tengah\" width=\"80\">".$mst_brg['kode_barang_bb']."</td>";
  echo  "<td class=\"tengah\" width=\"150\">".$mst_brg['nama_barang_bb']."</td>";
  echo  "<td class=\"tengah\" width=\"150\">".$mst_brg['kode_barang_wip']." - ".$mst_brg['nama_barang_wip']."</td>";
  echo  "<td class=\"tengah\" width=\"150\">".$mst_brg['kode_kelompok_bb']." - ".$mst_brg['nama_kelompok_bb']."</td>";
  echo  "</tr>";
	$i++;
	}
}
?>
    
     
	 

</table>

</body>
</html>
