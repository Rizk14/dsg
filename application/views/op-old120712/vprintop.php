<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
   
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .isinya2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">

$(function()
{
	var go_print= jQuery('#go_print').val();
	
	if (go_print == '1')
		window.print();
	
	$('#chkpj').click(function(){
	  	    if ($('input[name=chkpj]').is(':checked')) {
				$("#penanggungjwb").attr("readonly", false);
			}
			else {
				$("#penanggungjwb").attr("readonly", true);
			}
	  });
});


</script>

<?php
	$tgl = date("d-m-Y");
	$pisah1 = explode("-", $tgl);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;

	if ($proses != '') {
?>
<input type="hidden" name="go_print" id="go_print" value="1" >
<table class="isinya" border='0' align="center" width="70%">
	<tr>
		<td colspan="3"> CV Duta Setia Garment </td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>Bandung, <?php echo $tgl; ?></td>
	</tr>
	<tr>
		<td colspan="3">Jl. Salendro Timur X No. 1A</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Bandung</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><?php echo $query[0]['nama_supplier'] ?></td>
	</tr>
	<tr>
		<td colspan="3"><h3>ORDER PEMBELIAN</h3></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>U/p. <?php echo $nama_up ?> </td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Nomor: <?php echo $query[0]['no_op'] ?></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5">
			<table border='1' class="isinya2" width="100%" >
				<tr>
          <th width="20">No</th>
          <th>Banyak</th>
          <th>Harga (Rp.)</th>
           <th>Nama Barang</th>
           <th>Keterangan</th>
        </tr>

        <?php $i=1;
        if (count($query[0]['detail_op'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
			$detailnya = $query[0]['detail_op'];
			for($j=0;$j<count($query[0]['detail_op']);$j++){
			?>
			<tr>
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap"><?php echo number_format($detailnya[$j]['qty'],0,',','.')." ".$detailnya[$j]['satuan'] ?></td>
          <td nowrap align="right"><?php echo number_format($detailnya[$j]['harga'],2,',','.') ?></td>
          <td nowrap><?php if ($detailnya[$j]['nama_brg_sup'] == '') echo $detailnya[$j]['nama']; else echo $detailnya[$j]['nama_brg_sup']; ?></td>
          <td nowrap><?php if ($detailnya[$j]['keterangan'] != '') echo $detailnya[$j]['keterangan']; else echo "&nbsp;"; ?></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
			</table>
		</td>
		</tr>
	</td>
	</tr>
	<tr>
		<td colspan="5" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5" align="right">&nbsp;</td>
	</tr>
	<?php if ($chkpj == '') { ?>
	<tr>
		<td colspan="5" align="right">Hormat Kami</td>
	</tr>
	<?php } else { ?>
	<tr>
		<td colspan="2" align="left">Penanggung Jawab</td>
		<td colspan="3" align="right">Hormat Kami</td>
	</tr>
	<?php } ?>
	<tr>
		<td colspan="5" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5" align="right">&nbsp;</td>
	</tr>

	<?php if ($chkpj == '') { ?>
	<tr>
		<td colspan="5" align="right"><u><?php echo $an_op ?></u></td>
	</tr>
	<?php } else { ?>
	<tr>
		<td colspan="3" align="left"><u><?php echo $penanggungjwb ?></u></td>
		<td colspan="2" align="right"><u><?php echo $an_op ?></u></td>
	</tr>
	<?php } ?>

</table>
<?php 
	}
	else { 
		$attributes = array('name' => 'f_op', 'id' => 'f_op');
		echo form_open('op/cform/print_op', $attributes);
?>
	<input type="hidden" name="proses" value="1" >
	<input type="hidden" name="id_op" value="<?php echo $id_op ?>" >
<table>
	<tr>
		<td>Nama U/p</td>
		<td>: <input type="text" name="nama_up" id="nama_up" value="" size="30" >

		</td>
	</tr>
	<tr>
		<td>Nama Penandatangan OP</td>
		<td>: <input type="text" name="an_op" id="an_op" value="" size="30" >
		</td>
	</tr>
	<tr>
		<td>Pakai Penanggung Jawab</td>
		<td>: <input type="checkbox" name="chkpj" id="chkpj" value="t" >
		</td>
	</tr>
	<tr>
		<td>Penanggung Jawab</td>
		<td>: <input type="text" name="penanggungjwb" id="penanggungjwb" value="" size="30" readonly="true" >
		</td>
	</tr>
	
	<tr>
		<td><input type="submit" name="submit" value="Go Print"></td>
	</tr>
</table><br>
<?php 
	echo form_close(); 
	}
?>
