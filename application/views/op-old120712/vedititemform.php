<h3>Data Order Pembelian (OP)</h3><br> 
<a href="<? echo base_url(); ?>index.php/op/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/op/cform/view">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

 $(function()
 {

 });
 
 function cek_data() {
	var qty= $('#qty').val();

	if (qty == '' || qty == '0') {
		alert("Qty tidak boleh 0 / kosong..!");
		$('#qty').focus();
		return false;
	}
	
	if (isNaN($('#qty').val())) {
		alert("Qty harus berupa angka..!");
		$('#qty').focus();
		return false;
	}
 }	

</script>

<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

</script>

<?php 
$attributes = array('name' => 'f_op', 'id' => 'f_op');
echo form_open('op/cform/edititem', $attributes); ?>
<input type="hidden" name="go_edit" value="1">
<input type="hidden" name="iddetail" value="<?php echo $iddetail ?>">
<input type="hidden" name="id_op" value="<?php echo $query[0]['id_op'] ?>">
<input type="hidden" name="curpage" value="<?php echo $curpage ?>">
<input type="hidden" name="csupplier" value="<?php echo $csupplier ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">

<table width="50%">
		<tr>
			<td colspan="2">Edit Data</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td width="30%">Nomor OP</td>
			<td> <?php echo $query[0]['no_op'] ?>
			</td>
		</tr>
		<tr>
			<td>Tanggal OP</td>
			<td> 
				<?php 
				$pisah1 = explode("-", $query[0]['tgl_op']);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					
					$tgl_op = $tgl1."-".$bln1."-".$thn1;
					
				?>
			
				<input name="tgl_op" type="text" id="tgl_op" size="10" value="<?php echo $tgl_op ?>" readonly="true">
				  <img alt="" id="tgl_op" align="middle"
						title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
						onclick="displayCalendar(document.forms[0].tgl_op,'dd-mm-yyyy',this)">
			</td>
		</tr>
		<tr>
			<td>Supplier</td>
			<td>
			<select name="kode_supplier" id="kode_supplier">
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" <?php if ($query[0]['kode_supplier'] == $sup->kode_supplier) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>
			
			</td>
		</tr>
		<tr>
			<td>Kode & Nama Brg</td>
			<td> <?php echo $query[0]['kode_brg']." - ".$query[0]['nama_brg']." (".$query[0]['nama_satuan'].")" ?>
			</td>
		</tr>
		
		<tr>
			<td>Qty</td>
			<td> <input type="text" name="qty" id="qty" value="<?php echo $query[0]['qty'] ?>" size="7" >
				<input type="hidden" name="qty_lama" id="qty_lama" value="<?php echo $query[0]['qty'] ?>">
			</td>
		</tr>
</table><br>
<?php 
	if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "op/cform/view/index/".$curpage;
	else
		$url_redirectnya = "op/cform/cari/".$csupplier."/".$carinya."/".$curpage;
?>
<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<?php echo form_close(); ?>
