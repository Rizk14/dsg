<h3>Data Order Pembelian (OP)</h3><br>
<a href="<? echo base_url(); ?>index.php/op/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/op/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
	
	get_data_pkp();
	$('#topnya').hide();
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****harga*************************************
		var harga="#harga_"+n;
		var new_harga="#harga_"+no;
		$(harga, lastRow).attr("id", "harga_"+no);
		$(new_harga, lastRow).attr("name", "harga_"+no);		
		$(new_harga, lastRow).val('');				
		//*****end harga*************************************	
		
		//*****pajak*************************************
		var pajak="#pajak_"+n;
		var new_pajak="#pajak_"+no;
		$(pajak, lastRow).attr("id", "pajak_"+no);
		$(new_pajak, lastRow).attr("name", "pajak_"+no);		
		$(new_pajak, lastRow).val('0');				
		//*****end pajak*************************************	
				
		//*****diskon*************************************
		var diskon="#diskon_"+n;
		var new_diskon="#diskon_"+no;
		$(diskon, lastRow).attr("id", "diskon_"+no);
		$(new_diskon, lastRow).attr("name", "diskon_"+no);		
		$(new_diskon, lastRow).val('0');				
		//*****end diskon*************************************	
		
		//*****total*************************************
		var total="#total_"+n;
		var new_total="#total_"+no;
		$(total, lastRow).attr("id", "total_"+no);
		$(new_total, lastRow).attr("name", "total_"+no);		
		$(new_total, lastRow).val('0');				
		//*****end total*************************************	
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		
		var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/pp-bb/cform/show_popup_brg/"+no+"/');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
	$('#pilih_pp').click(function(){

			var urlnya = "<?php echo base_url(); ?>index.php/op/cform/show_popup_pp/";
			openCenteredWindow(urlnya);

	  });
	  
	$('#kode_supplier').change(function(){
	  	    get_data_pkp();
	  });
	
	$('#pkp').click(function(){
	  	    if ($("#pkp").is(":checked")) {
				$('#hide_pkp').val('t');
				hitungnilai();
			}
			else {
				$('#hide_pkp').val('f');
				hitungnilai();
			}
	  });
	  
	$('#tipe_pajak').change(function(){
	  	    if ($("#tipe_pajak").val() == 'I') {
				$('#hide_tipe_pajak').val('I');
				hitungnilai();
			}
			else {
				$('#hide_tipe_pajak').val('E');
				hitungnilai();
			}
	  });
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 900;
		var height = 500;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	

function get_data_pkp() {
	var kode_sup= $('#kode_supplier').val();
	var jum_detail = $('#no').val()-1;
    $.getJSON("<?php echo base_url(); ?>index.php/op/cform/get_pkp_tipe_pajak/"+kode_sup, function(data) {

		$(data).each(function(index, item) {
			$("#topnya").html("T.O.P : "+item.top+" Hari");
			$("#topnya").show();
                    //alert(item.pkp);
                   
            if (item.tipe_pajak == 'I') {
				$('#tipe_pajak option[value=I]').attr('selected', 'selected');
				$('#hide_tipe_pajak').val('I');
			}
			else {
				$('#tipe_pajak option[value=E]').attr('selected', 'selected');
				$('#hide_tipe_pajak').val('E');
			} 
                   
			if (item.pkp == 't') {
				//alert($("input:checked").length);
				$('#hide_pkp').val('t');
				$('input[name=pkp]').attr('checked', true);
				
				var i=1;
				var gtotal = 0; var tot_pajak = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
				
					$('input[name=pkp]').attr('checked', true);
					if (item.tipe_pajak == 'I') {
					//	$('#tipe_pajak option[value=I]').attr('selected', 'selected');
					//	$('#hide_tipe_pajak').val('I');
						var hitung = (harga*qty)-diskon;
						var pi = hitung/1.1;
						new_pajak = hitung-pi;
						new_pajak = new_pajak.toFixed(2);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(hitung);
						gtotal = parseFloat(gtotal)+parseFloat(hitung);
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					}
					else {
					//	$('#tipe_pajak option[value=E]').attr('selected', 'selected');
					//	$('#hide_tipe_pajak').val('E');
						var hitung = (harga*qty)-diskon;
						var new_pajak = hitung*0.1;
						new_pajak = new_pajak.toFixed(2);
						new_total = parseFloat(new_pajak)+parseFloat(hitung);
						$("#pajak_"+i).val(new_pajak);
						$("#total_"+i).val(new_total);
						gtotal = parseFloat(gtotal)+parseFloat(new_total); 
						tot_pajak = parseFloat(tot_pajak)+parseFloat(new_pajak);
					} //end cek item pajak
				}
				$("#gtotal").val(gtotal);
				$("#tot_pajak").val(tot_pajak);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
			}
			else if (item.pkp == 'f') {
				$('input[name=pkp]').attr('checked', false);
				$('#tipe_pajak option[value=I]').attr('selected', 'selected');
				$('#hide_pkp').val('f');
				$('#hide_tipe_pajak').val('I');
				var i=1; var gtotal = 0;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var harga=$("#harga_"+i).val();
					var diskon=$("#diskon_"+i).val();
					var hitung = (harga*qty)-diskon;
					$("#pajak_"+i).val('0');
					$("#total_"+i).val(hitung);
					gtotal = parseFloat(gtotal)+parseFloat(hitung);
				}
				$("#gtotal").val(gtotal);
				var uang_muka = $("#uang_muka").val();
				$("#sisa_hutang").val(gtotal-uang_muka);
				$("#tot_pajak").val('0');
			}
				
        });
    });
    
}

function cek_item_brg() {
	var id_brg= $('#id_brg').val();
	var no_pp= $('#no_pp').val();
	if (no_pp == '') {
		alert("Nomor PP harus dipilih..!");
		return false;
	}
	if (id_brg == '') {
		alert("item barang di PP harus dipilih..!");
		return false;
	}
}

function cek_op() {
	var no_faktur= $('#no_faktur').val();
	var tgl_retur= $('#tgl_retur').val();
	if (no_faktur == '') {
		alert("Nomor OP harus diisi..!");
		return false;
	}
	if (tgl_retur == '') {
		alert("Tanggal OP harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			
			if($('#harga_'+k).val() == '0' || $('#harga_'+k).val() == '' ) {				
				alert("Data harga tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#harga_'+k).val()) ) {
				alert("Harga harus berupa angka..!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_pp', 'id' => 'f_pp');
echo form_open('op/cform/', $attributes); ?>
<table>

	<tr>
		<td>Nomor PP</td>
		<td> <input type="text" name="no_pp" id="no_pp" value="" size="10" maxlength="10" readonly="true"><input type="hidden" name="id_pp" value="">&nbsp;
		<input type="hidden" name="id_brg" id="id_brg" value="">
		<input name="pilih_pp" id="pilih_pp" value="..." type="button" title="browse data PP">
		</td>
	</tr>
	<tr>
			<td>Supplier</td>
			<td><select name="kode_supplier" id="kode_supplier" onkeyup="this.blur();this.focus();" >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr>
		<tr>
			<td>
			&nbsp;
			</td>
			<td>PKP <input type="checkbox" name="pkp" id="pkp" value="t" checked="checked" disabled>&nbsp;&nbsp;&nbsp;
			Tipe Pajak <select name="tipe_pajak" id="tipe_pajak" onkeyup="this.blur();this.focus();" disabled="true">
				<option value="I">Include</option>
				<option value="E" >Exclude</option>
			</select>&nbsp;&nbsp;&nbsp;<span id="topnya"></span>
			<input type="hidden" name="hide_pkp" id="hide_pkp" value=""><input type="hidden" name="hide_tipe_pajak" id="hide_tipe_pajak" value="">
			</td>
		</tr>
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/op/cform/view'">
<?php echo form_close(); 
} else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/op/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_pp" value="<?php echo $id_pp ?>">
<input type="hidden" name="kode_supplier" value="<?php echo $kode_supplier ?>">

<?php 
		if (count($pp_detail)>0) {
			$no=1;
			foreach ($pp_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
    <td width="15%">Nomor PP</td>
    <td width="70%">
      <?php echo $no_pp; ?>
    </td>
  </tr>
<!--		<tr>
			<td>Supplier</td>
			<td><select name="kode_supplier" id="kode_supplier" onkeyup="this.blur();this.focus();" >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr> -->
		<tr>
			<td>Supplier</td>
			<td><?php echo $kode_supplier." - ". $nama_supplier ?></td> 
		</tr>
<!--		<tr>
			<td>
			&nbsp;
			</td>
			<td>PKP <input type="checkbox" name="pkp" id="pkp" value="t" checked="checked" disabled>&nbsp;&nbsp;&nbsp;
			Tipe Pajak <select name="tipe_pajak" id="tipe_pajak" onkeyup="this.blur();this.focus();" disabled="true">
				<option value="I">Include</option>
				<option value="E" >Exclude</option>
			</select>&nbsp;&nbsp;&nbsp;<span id="topnya"></span>
			<input type="hidden" name="hide_pkp" id="hide_pkp" value=""><input type="hidden" name="hide_tipe_pajak" id="hide_tipe_pajak" value="">
			</td>
		</tr> -->
  <tr>
    <td>No OP</td>
    <td>
      <input name="no_faktur" type="text" id="no_faktur" size="20" maxlength="20" readonly="true" value="<?php echo $no_op; ?>">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tgl OP</td>
    <td>
	<label>
      <input name="tgl_retur" type="text" id="tgl_retur" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_retur" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_retur,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
			<td>Keterangan</td>
			<td><input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="right">
			<!-- <input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang"> -->
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Satuan</th>
           <th>Satuan Lain</th>
	      <th>Qty</th>
	      <th>Harga (Rp.)</th>
          <th>Keterangan</th>
        </tr>

        <?php $i=1;
        if (count($pp_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($pp_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
		   <input name="idbarang_1" type="hidden" id="idbarang_1" />
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $pp_detail[$j]['kode_brg'] ?>"/>
           <input type="hidden" name="id_pp_detail_<?php echo $i ?>" value="<?php echo $pp_detail[$j]['id'] ?>" >
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $pp_detail[$j]['nama'] ?>" /></td>
          <td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="10" readonly="true"
          value="<?php echo $pp_detail[$j]['nama_satuan'] ?>" /></td>
          
          <td><input name="nama_satuan_lain_<?php echo $i ?>" type="text" id="nama_satuan_lain_<?php echo $i ?>" size="10" readonly="true"
          value="<?php echo $pp_detail[$j]['nama_sat_lain'] ?>" />
          <input type="hidden" name="satuan_lain_<?php echo $i ?>" value="<?php echo $pp_detail[$j]['sat_lain'] ?>">
          </td>
          
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $pp_detail[$j]['qty'] ?>" /></td>
          
          <td><input name="harga_<?php echo $i ?>" type="text" id="harga_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $pp_detail[$j]['harga'] ?>" />
          <input type="hidden" name="harga_lama_<?php echo $i ?>" id="harga_lama_<?php echo $i ?>" value="<?php echo $pp_detail[$j]['harga'] ?>">
          </td>
          <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="25" value="" /></td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
	</table>	
	
	</form>
      <div align="center"><br> 
        
        <input type="submit" name="submit2" value="Simpan" onclick="return cek_op();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/op/cform/'">

      </div></td>
    </tr>

</table>
</div>
</form>
<?php } ?>
