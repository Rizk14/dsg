<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript">
$(document).ready(function() {
 
 $(".xhapus").click(function(){
 
 //Save the link in a variable called element
 var element = $(this);
 
 //Find the id of the link that was clicked
 var hps = element.attr("id");
 
 //Built a url to send
 var info = 'pid=' + hps;
 if(confirm("Yakin akan menghapus data dgn id: "+hps+" ?"))
 {
 $.ajax({
 type: "POST",
 url : "<?php echo site_url('/klsbrg/cform/actdelete')?>",
 data: info,
 success: function(){
 }
 });
 
 $(this).parents(".record").animate({ opacity: "hide" }, "slow");
 
 }
 
 return false;
 
 });
 
})
</script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<!--
tb = textbox
cc = cacah
-->
<div id="tabs">
	<div id="tab1" class="tab_sel_kls" align="center" onClick="javascript: displayPanelKls('1');">DAFTAR KELAS BRG</div>
<!--
	<div id="tab2" class="tab_kls" style="margin-left:1px;" align="center" onClick="javascript: displayPanelKls('2');">FORM KELAS BRG </div>
	<div id="tab3" class="tab_kls" style="margin-left:1px;" align="center" onClick="javascript: displayPanelKls('3');">CARI KELAS BRG</div>
-->
</div>
   
<div class="tab_bdr_kls"></div>
	<div class="panel" id="panel1" style="display:none;">
	<?php
	if( !empty($query) || isset($query) ) {
		$cc = 1;
		foreach($query as $row) {
			
			$Classnya = $cc % 2 == 0 ? "row" :"row1";
			/* 20052011
			<a href=\"#\" class=\"xhapus\" id=\"".$row->i_class."\" ><img src=".base_url()."asset/theme/images/delete.gif width=12 height=13></a>
			*/
			$list .= "
			 <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
			  <td width=5>".$cc."</td>	
			  <td width=40>".$row->i_class."&nbsp;</td>	 
			  <td width=100>".$row->e_class_name."&nbsp;</td>
			  <td align=center><a href=".base_url()."index.php/klsbrg/cform/edit/".$row->i_class."><img src=".base_url()."asset/theme/images/edit.gif width=12 height=13></a>&nbsp;&nbsp;<a href=\"javascript:void(0)\" onclick='hapus(\"klsbrg/cform/actdelete/".$row->i_class."\",\"#content\")'><img src=".base_url()."asset/theme/images/delete.gif"." "."width=12 height=13></a></td>
			 </tr>";
			$cc+=1;
		}
	}
	?>
        <table>
   	 <!-- <tr>
	  <td><?php echo $page_title;?></td>
	 </tr> -->
	</table>
	<table class="listtable_kls">
	 <th width="5">No</th>
	 <th width="40">Kode Kelas</th>
	 <th width="100">Nama Kelas</th>
	 <th width="10" class="action">Aksi</th>
	<tbody>
	<?php
	if( !empty($list) && !isset($not_defined) ) {
		echo $list."</tbody></table>";
	} else {
		echo $list."</tbody></table>
		<div style=\"font-family:arial; font-size:11px; color:#FF0000;\">".strtoupper($not_defined)."</div>";
	}
	?>
	</div>

	<div class="panel" id="panel2" style="display:block;">
	    <table>
	      <tr>
	    	<td align="left">
				 <?php 
		      $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('klsbrg/cform/simpan', $attributes);?>
		     
		<div id="masterclassform">
		      <table>
		      	<tr>
			  <td width="150px">Kode Kelas</td>
			  <td width="1px">:</td>
			  <td>
				<?php 
				$kdkls = array(
			              'name'        => 'iclass',
			              'id'          => 'iclass',
			              'value'       => $iclass,
						  'readonly'	=> 'true',
			              'maxlength'   => '5');
				echo form_input($kdkls); ?>
			  </td>
			</tr>
		      	<tr>
			  <td>Nama Kelas</td>
			  <td>:</td>
			  <td>
				<?php
				$nmkls = array(
			              'name'        => 'eclassname',
			              'id'          => 'eclassname',
			              'value'       => '',
			              'maxlength'   => '100');
				echo form_input($nmkls); ?>
			  </td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			    <input name="tblsimpan" id="tblsimpan" value="Simpan" type="submit">
			    <input name="tblreset" id="tblreset" value="Batal" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/klsbrg/cform/'">
			  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>

	<div class="panel_kls" id="panel3" style="display:none;">
		<table>
			<tr>
			  <td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url'=>'klsbrg/cform/cari','update'=>'#content','type'=>'post'));?>
				<table class="listtable">
				<thead>
			      	 <tr>
				   <td width="160px">Kode Kelas</td>
				   <td width="1px">:</td>
				   <td>
				    <input type="text" name="txt_i_class" id="txt_i_class" maxlength='5'></td>
				 </tr>
			      	 <tr>
				   <td>Nama Kelas</td>
				   <td>:</td>
				   <td>
				    <input type="text" name="txt_e_class_name" id="txt_e_class_name" maxlength='50'></td>
				 </tr>
				 <tr>
				  <td colspan="2">
				  </td>
				  <td align="left"><input type="submit" id="btncari" name="btncari" value="Pencarian"></td>
				 </tr>
				</thead>
				</table>
			  </td>
			</tr>
		</table>
	</div>
