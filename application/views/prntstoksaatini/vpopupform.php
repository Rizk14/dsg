<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>

<style type="text/css" media="screen, print">
body {
	background:#FFFFFF;
	color:black;
	margin:0;
}

#tbl_utama {
	width:1100px;
}

</style>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 18000;

t = null;
function closeMe() {
t = setTimeout("self.close()",howLong);
}

</script>

</head>

<body onload="window.print();closeMe();">

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td style="font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#000000; font-weight:bold;"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Laporan Stok Barang Berdasarkan DO Per-Tanggal :</td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'prntstoksaatini/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlstoksaatiniform">


		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="23%" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; font-weight:bold;"><?php echo $list_stoksaatini_tgl_do_mulai; ?></td>
					<td width="1%" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; font-weight:bold;">:</td>
					<td width="76%" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; font-weight:bold;">
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?php echo $tgldomulai; ?>"/>
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?php echo $tgldoakhir; ?>"/>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; font-weight:bold;">
					  <input name="f_stop_produksi" type="checkbox" <?php echo $sproduksi; ?> />
					<?php echo $list_stoksaatini_s_produk; ?></td>
					<td>&nbsp;</td>
					<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; font-weight:bold;"><?php echo $list_stoksaatini_j_produk; ?> : 
					<select name="i_class">
					<?php
					foreach($opt_jns_brg as $row) {
						$selected = $kproduksi==$row->i_class ? " selected ":" ";
						$ljnsbrg .= "
							<option value=\"$row->i_class\" $selected >$row->e_class_name</option>
						";
					} 
					echo $ljnsbrg;
					?>
					</select>					
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; font-weight:bold;">
			  <div id="title-box2"><?php echo $form_title_detail_stoksaatini; ?></div>
			  </td>	
			</tr>
			<tr>
			  <td>
			  	<table border="1" style="border:thin #000000;">
					<tr>
						<td>
						  <table width="100%" border="1" cellspacing="0" cellpadding="0" style="border:thin #000000;">
							<tr>
							  <td align="center" width="3%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;">No</td>
							  <td align="center" width="10%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_kd_brg); ?> </td>
							  <td align="center" width="25%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_nm_brg); ?> </td>
							  <td align="center" width="6%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_s_awal); ?> </td>
							  <td align="center" width="6%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_s_akhir); ?> </td>
							  <td align="center" width="6%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_bmm); ?> </td>
							  <td align="center" width="6%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_bbm); ?></td>
							  <td align="center" width="6%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_do); ?></td>
							  <td align="center" width="6%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000; border-right:thin #000000;"><?php echo strtoupper($list_stoksaatini_bmk); ?> </td>
							  <td align="center" width="6%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000066; font-weight:bold; border-bottom:thin #000000;"><?php echo strtoupper($list_stoksaatini_bbk); ?></td>
							</tr>
							<?php
							echo $isi;
							?>
						  </table>						
						</td>
					</tr>
				</table>
			  </td>
			</tr>
		  </table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
</body>
</html>