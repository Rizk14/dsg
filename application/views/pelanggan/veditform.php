<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript">

function toUpper1(){
	var i_customer_code	= document.getElementById('i_customer_code').value;
	var toUpper	= i_customer_code.toUpperCase();
	document.getElementById('i_customer_code').value = toUpper;
}

</script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div id="tabs">
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');"><?php echo $form_form_pelanggan; ?></div>
</div>

	<div class="panel" id="panel2" style="display:block;">
	    <table width="90%">
	      <tr>
	    	<td align="left">
				<?php 
				$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('pelanggan/cform/actedit', $attributes);?>
		<div id="mastercustomerform">
		      <table>
		      	<tr>
			  <td width="20%"><?php echo ucfirst($form_kode_pelanggan); ?>&nbsp;[<?php echo $id; ?>]</td>
			  <td width="1%">:</td>
			  <td>
			    <input type="text" name="i_customer_code" id="i_customer_code" maxlength='15' value="<?php echo $icustomercode; ?>" onkeyup="toUpper1();" ></td>
			</tr>
		      	<tr>
			  <td><?php echo ucfirst($form_nm_pelanggan); ?></td>
			  <td>:</td>
			  <td>
			    <input type="text" name="e_customer_name" id="e_customer_name" maxlength='200' value="<?php echo $ecustomername; ?>"></td>
			</tr>
		      	<tr>
			  <td><?php echo ucfirst($form_nm_group_pelanggan); ?></td>
			  <td>:</td>
			  <td>
			  	<select name="igroup" id="igroup">
					<option value=""><?php echo $form_option_group_pelanggan; ?></option>
					<?php echo $opt_grp_pel; ?>
				</select>
			  </td>
			</tr>				
			<tr valign="top">
			  <td><?php echo $form_alamat_pelanggan; ?></td>
			  <td>:</td>
			  <td>
			    <textarea name="e_customer_address" id="e_customer_address" cols="2" rows="2"><?php echo $ecustomeraddress; ?></textarea></td>
		      	</tr>
			<tr>
			  <td><?php echo $form_pkp_pelanggan; ?></td>
			  <td>:</td>
			  <td>
			    <input type="checkbox" name="f_customer_pkp" id="f_customer_pkp" value="1" <?php echo $fcustpkp; ?> ></td>
		      	</tr>
		      	<tr>
			  <td><?php echo $form_npwp_pelanggan; ?></td>
			  <td>:</td>
			  <td>
			    <input type="text" name="e_customer_npwp" id="e_customer_npwp" maxlength='30' value="<?php echo $ecustomernpwp; ?>"></td>
		      	</tr>
		      	<tr>
			  <td><?php echo $form_top_pelanggan; ?></td>
			  <td>:</td>
			  <td>
			    <input type="text" name="n_customer_top" id="n_customer_top" maxlength='50' value="<?php echo $ncustomertop; ?>"></td>
			</tr>
			<tr>
			  <td><?php echo $form_konsinyasi_pelanggan; ?></td>
			  <td>:</td>
			  <td>
			    <input type="checkbox" name="f_customer_konsinyasi" id="f_customer_konsinyasi" value="1" <?php echo $fcustkonsinyasi; ?> ></td>
			</tr>
		      	<tr>
			  <td><?php echo $form_telpon_pelanggan; ?></td>
			  <td>:</td>
			  <td>
			    <input type="text" name="e_customer_phone" id="e_customer_phone" maxlength='17' value="<?php echo $ecustomerphone; ?>"></td>
			</tr>
		      	<tr>
			  <td><?php echo $form_fax_pelanggan; ?></td>
			  <td>:</td>
			  <td>
			    <input type="text" name="e_customer_fax" id="e_customer_fax" maxlength='17' value="<?php echo $ecustomerfax; ?>"></td>
			</tr>
		      	<tr>
			  <td><?php echo ucfirst($form_kontak_pelanggan); ?></td>
			  <td>:</td>
			  <td>
			    <input type="text" name="e_customer_contact" id="e_customer_contact" maxlength='50' value="<?php echo $ecustomercontact; ?>"></td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			  	<input type="hidden" name="i_customer2" value="<?php echo $icustomer; ?>" id="i_customer2">
				<input type="hidden" name="i_customer_code2" value="<?php echo $icustomercode; ?>" id="i_customer_code2">
			    <input name="btnsimpan" id="btnsimpan" value="<?php echo $button_simpan; ?>" type="submit">
			    <input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button"  onclick="window.location='<?php echo base_url(); ?>index.php/pelanggan/cform/'">
			  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
