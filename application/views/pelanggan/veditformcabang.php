
<div id="tabs">
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');"><?php echo $form_panel_daftar_pelcab; ?></div>
         
<div class="tab_bdr"></div>

	<div class="panel" id="panel2" style="display:block;">
	    <table width="90%">
	      <tr>
	    	<td align="left">
				<?php 
				$attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('pelanggan/cformcabang/actedit', $attributes);?>
		      
		<div id="mastercustomercabform">
		      <table>
		      	<tr>
			  <td width="20%"><?php echo ucfirst($form_kode_cab); ?></td>
			  <td width="1%">:</td>
			  <td>
			   <?php
			    $kdcabang = array(
				'name'=>'i_branch',
				'id'=>'i_branch',
				'value'=>$ibranchcode,
				'maxlength'=>'5',
				'readonly'=>'true' );
			   echo form_input($kdcabang);
			   ?>	
			  </td>
			</tr>
		      	<tr>
			  <td width="20%"><?php echo "Kode Area"; ?></td>
			  <td width="1%">:</td>
			  <td>
			   <?php
			    $iarea = array(
				'name'=>'i_area',
				'id'=>'i_area',
				'value'=>$iarea,
				'maxlength'=>'5' );
			   echo form_input($iarea);
			   ?>	
			  </td>
			</tr>			
		      	<tr>
			  <td><?php echo ucfirst($form_kode_pelcab); ?></td>
			  <td>:</td>
			  <td>
			   <?php
			    $kdpelanggan = array(
				'name'	=>'i_customer',
				'id'	=>'i_customer',
				'value'		=>$customername,
				'onclick'	=>'ckodepelanggan();',
				'maxlength'	=>'10'
				);
			    echo form_input($kdpelanggan);
			   ?>		
			  </td>
			</tr>
		      	<tr>
			  <td><?php echo ucfirst($form_nm_cab); ?></td>
			  <td>:</td>
			  <td>
			   <?php
			    $nmpelanggan = array(
				'name'=>'e_branch_name',
				'id'=>'e_branch_name',
				'value'=>$ebranchname,
				'maxlength'=>'50'
				);
			    echo form_input($nmpelanggan);	
			   ?>	
			  </td>
			</tr>
			<tr valign="top">
			  <td>Alamat</td>
			  <td>:</td>
			  <td>
			   <textarea name="e_branch_address" id="e_branch_address" cols="2" rows="2"><?php echo $ebranchaddress; ?></textarea>	
			  </td>
		      	</tr>
		      	<tr>
			  <td><?php echo $form_kota_cab; ?></td>
			  <td>:</td>
			  <td>
			   <?php
			    $nmkota = array(
				'name'=>'e_branch_city',
				'id'=>'e_branch_city',
				'value'=>$ebranchcity,
				'maxlength'=>'40'
				);
			    echo form_input($nmkota);	
			   ?>
			  </td>
		      	</tr>
		      	<tr>
			  <td><?php echo $form_inisial_cab; ?></td>
			  <td>:</td>
			  <td>
			   <?php
			    $nminisial = array(
				'name'=>'e_initial',
				'id'=>'e_initial',
				'value'=>$einitial,
				'maxlength'=>'40'
				);
			    echo form_input($nminisial);	
			   ?>
			  </td>
			</tr>
		      	<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>
			  	<input type="hidden" name="icustomer" id="icustomer" value="<?php echo $icustomer; ?>" />
			  	<input type="hidden" name="ibranch" id="ibranch" value="<?php echo $ibranch; ?>" />
			    <input name="btnsimpan" id="btnsimpan" value="<?php echo $button_simpan; ?>" type="submit">
			    <input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/pelanggan/cformcabang/'">
			  </td>
		       </tr>
		      </table>
		</div>
		<?php echo form_close(); ?>
		</td>
	      </tr> 
	    </table>
	</div>
