
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>asset/js/jquery-1.11.1.min.js"></script>
<script>

function choose() 
	{
		
		var id_barang_bb= $( "#id_barang_bb" ).val();
		var kode_barang_bb=$("#kode_barang_bb").val();
		var nama_barang_bb=$("#nama_barang_bb").val();

		opener.document.forms["myform"].id_barang_bb_<?php echo $posisi ?>.value = id_barang_bb;
		opener.document.forms["myform"].kode_barang_bb_<?php echo $posisi ?>.value = kode_barang_bb;
		opener.document.forms["myform"].nama_barang_bb_<?php echo $posisi ?>.value = nama_barang_bb;

		self.close();
		

	
	};
</script>

<?php
// Nomor urut data di tabel.
$per_page = 10;

// Posisi nomor halaman (untuk paging) jika user login / tidak.
$login_status = $this->session->userdata('login_status');
$user_bagian = $this->session->userdata('user_bagian');

if ($login_status ==  true && $user_bagian == '2') {
    $page = $this->uri->segment(5);
} else {
    $page = $this->uri->segment(5);
}


if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">

    <h2>Data Barang BB</h2>
    <hr>

    <!-- Paging dan form pencarian -->
    <div class="row navigasi_cari">
        <!-- Paging -->
        <div class="col-xs-12 col-md-6">
            <?php echo (!empty($paging)) ? $paging : ''?>
            
        </div>
       
        <!-- Form Pencarian -->
        <div class="col-xs-12 col-md-4 pull-right">
            <form method="post" action="<?php echo $form_action;?>" role="form" class="form-horizontal">
            
                <div class="input-group">
                    <input type="text" name="kata_kunci" class="form-control" placeholder="Masukkan kode / nama barang pencarian" id="kata_kunci" >
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /Form Pencarian -->
    </div>
    <!-- /Paging dan form pencarian -->

    <?php if (!empty($item_makloon_barang_bb) && is_array($item_makloon_barang_bb)): ?>
    <div class="row">
    <div class="col-md-12">
	<form role="form" class="form-horizontal" id="f_master_brg" name="f_master_brg">
	 <input type="hidden"  id="id_barang_bb" value="">
	 <input type="hidden" id="kode_barang_bb" value="">
	 <input type="hidden" id="nama_barang_bb" value="">
	 
	 </form>
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>   
                    <th>Barang WIP</th> 
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
                foreach($item_makloon_barang_bb as $row){
         
    
                 ?>
         
                    <tr>
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $row->kode_barang_bb ?></td>
						<td><?php echo $row->nama_barang_bb ?></td>    
						<td><?php echo $row->kode_barang_wip." - ".$row->nama_barang_wip ?></td>    
						<td>
                           <a class="glyphicon glyphicon-share" style="cursor:pointer" id="pilih" 
                           onClick="choose()"
                            onMouseOver=
                           "window.document.f_master_brg.id_barang_bb.value='<?php echo $row->id ?>';
							window.document.f_master_brg.kode_barang_bb.value='<?php echo $row->kode_barang_bb ?>';
							window.document.f_master_brg.nama_barang_bb.value='<?php 
						  $pos = strpos($row->nama_barang_bb, "\"");
						  if ($pos > 0)
							echo str_replace("\"", "&quot;",$row->nama_barang_bb);
						  else
							echo str_replace("'", "\'", $row->nama_barang_bb); ?>'; "></a>
                        </td>
                    </tr>
                    
                <?php  } ?>
                </tbody>
            </table>

	</div>
    </div>

    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $item_makloon_barang_bb ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->


