

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>

function choose() 
	{
		
		var id_barang_wip= $( "#id_barang_wip" ).val();
		var kode_barang_wip=$("#kode_barang_wip").val();
		var nama_barang_wip=$("#nama_barang_wip").val();

		opener.document.forms["myform"].id_barang_wip_<?php echo $posisi ?>.value = id_barang_wip;
		opener.document.forms["myform"].kode_barang_wip_<?php echo $posisi ?>.value = kode_barang_wip;
		opener.document.forms["myform"].nama_barang_wip_<?php echo $posisi ?>.value = nama_barang_wip;

		self.close();
		

	
	};
</script>

<?php
// Nomor urut data di tabel.
$per_page = 10;

if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">

    <h2>Data Barang WIP</h2>
    <hr>

    <!-- Paging dan form pencarian -->
    <div class="row navigasi_cari">
        <!-- Paging -->
        <div class="col-xs-12 col-md-6">
            <?php echo (!empty($paging)) ? $paging : ''?>
       
        </div>
       
        <!-- Form Pencarian -->
        <div class="col-xs-12 col-md-4 pull-right">
            <form method="post" action="<?php echo $form_action;?>" role="form" class="form-horizontal">
            
                <div class="input-group">
                    <input type="text" name="kata_kunci" class="form-control" placeholder="Masukkan kode / nama barang pencarian" id="kata_kunci" >
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /Form Pencarian -->
    </div>
    <!-- /Paging dan form pencarian -->

    <?php if (!empty($item_makloon_barang_wip) && is_array($item_makloon_barang_wip)): ?>
    <div class="row">
    <div class="col-md-12">
	<form role="form" class="form-horizontal" id="f_master_brg" name="f_master_brg">
	 <input type="hidden"  id="id_barang_wip" value="">
	 <input type="hidden" id="kode_barang_wip" value="">
	 <input type="hidden" id="nama_barang_wip" value="">
	 
	 </form>
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>   
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
                foreach($item_makloon_barang_wip as $row){
                 ?>
         
                    <tr>
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $row->kode_barang_wip ?></td>
						<td><?php echo $row->nama_barang_wip ?></td>    
						<td>
			<input type="button" name="pilih" value="pilih" onclick="choose();"  onMouseOver=
                           "window.document.f_master_brg.id_barang_wip.value='<?php echo $row->id ?>';
							window.document.f_master_brg.kode_barang_wip.value='<?php echo $row->kode_barang_wip ?>';
							window.document.f_master_brg.nama_barang_wip.value='<?php 
						  $pos = strpos($row->nama_barang_wip, "\"");
						  if ($pos > 0)
							echo str_replace("\"", "&quot;",$row->nama_barang_wip);
						  else
							echo str_replace("'", "\'", $row->nama_barang_wip); ?>'; ">

                        
                        </td>
                    </tr>
                    
                <?php  } ?>
                </tbody>
            </table>

	</div>
    </div>

    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $item_makloon_barang_wip ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->


