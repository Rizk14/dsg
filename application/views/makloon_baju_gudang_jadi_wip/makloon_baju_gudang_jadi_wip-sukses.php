<div class="container" id="daftar-sukses">
    <p class="h2">Informasi Untuk Penginputan Makloon baju gudang Jadi WIP</p>
    <hr>

    <div class="alert alert-success alert-dismissible" role="alert">
        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Penginputan Berhasil.
    </div>
    
    <p>Anda dapat kembali ke halaman Makloon baju gudang Jadi WIP di <?php echo anchor('/makloon_baju_gudang_jadi_wip/makloon_baju_gudang_jadi_wip/view', 'Makloon baju gudang Jadi WIP'); ?>.</p>

</div> <!-- container -->

