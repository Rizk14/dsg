<h3>Laporan Stok Terkini Barang Hasil Jahit</h3><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('info-stok-hsl-jahit/cform/view'); ?>
<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr>
		<th>No</th>
		<th>Kode Brg Jadi</th>
		 <th>Nama Brg Jadi</th>
		 <th>Stok (Pcs)</th>
		 <th>Last Update Stok</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				if ($startnya == '')
					$i=1;
				else
					$i = $startnya+1; 
			 for($j=0;$j<count($query);$j++){
				if ($query[$j]['tgl_update_stok'] != '') {
					$pisah1 = explode("-", $query[$j]['tgl_update_stok']);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					$tgl_update_stok = $tgl1." ".$nama_bln." ".$thn1;
				}
				else
					$tgl_update_stok = "&nbsp;";
				 
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg_jadi']."</td>";
				 echo    "<td nowrap>".$query[$j]['nama_brg_jadi']."</td>";
				 echo    "<td align='right'>".$query[$j]['stok']."</td>";
				 echo    "<td align='center'>".$tgl_update_stok."</td>";
				 echo  "</tr>";
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
