<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<h3>Data Faktur Pembelian WIP Makloon Jahit</h3><br>
<a href="<?php echo base_url(); ?>index.php/faktur-pembelian-wip/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/faktur-pembelian-wip/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('faktur-pembelian-wip/cform/cari'); ?>

<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>

	<tr>
		<td>Status Lunas</td>
		<td>: <select name="status_lunas" id="status_lunas">
				<option value="0" <?php if ($cstatus_lunas == '0') { ?> selected="true" <?php } ?> >- All -</option>
				<option value="t" <?php if ($cstatus_lunas == 't') { ?> selected="true" <?php } ?> >Lunas</option>
				<option value="f" <?php if ($cstatus_lunas == 'f') { ?> selected="true" <?php } ?>>Belum</option>
				</select></td>
	</tr>
	<tr>
		<td>Unit Jahit</td>
		<td>: <select name="unit_jahit" id="unit_jahit">
				<option value="0" <?php if ($cunit_jahit == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($list_unit_jahit as $luj) { ?>
					<option value="<?php echo $luj->id ?>" <?php if ($cunit_jahit == $luj->id) { ?> selected="true" <?php } ?> ><?php echo $luj->kode_unit." - ". $luj->nama ?></option>
				<?php } ?>
				</select></td>
	</tr>
	<tr>
		<td>Nomor Faktur</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>		
</fieldset>

<?php echo form_close(); ?>
<br>
<?php 
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;
	
	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
?>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>Jenis Pembelian </th>
		 <th>No/Tgl Faktur </th>
		 <th>No/Tgl BTB Makloon Jahit</th>
		 <th>unit_jahit</th>
		 <th>Jum Total (Rp.)</th>
		 <th>Status Lunas</th>
		 <th>Jenis Masuk</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {

			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>";
					if ($query[$j]['jenis_pembelian'] == 1) echo "Cash"; else echo "Kredit";
				 echo "</td>";
				 echo    "<td nowrap>".$query[$j]['no_faktur']." / ".$query[$j]['tgl_faktur']."</td>";
				 
				 echo "<td nowrap>";
				 
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						
						  echo $var_detail[$k]['no_sjmasukpembelian']." / ".$var_detail[$k]['tgl_sjpembelian'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td>".$query[$j]['kode_unit_jahit']." - ".$query[$j]['nama_unit_jahit']."</td>";
				 echo    "<td align='right'>".number_format($query[$j]['totalnya'], 4, ',','.')."</td>";
				 if ($query[$j]['status_lunas'] == 't')
					echo "<td>Lunas</td>";
				else
					echo "<td>Belum</td>";
					 echo    "<td nowrap>";
					if ($query[$j]['jenis_masuk'] == 1) echo "Masuk Bagus Hasil Jahit"; else echo "Masuk Perbaikan";
				 echo "</td>";
					
				 echo    "<td>".$query[$j]['tgl_update']."</td>";
				
				if ($query[$j]['status_faktur_pajak'] == 'f') {
				  if ($query[$j]['status_lunas'] == 'f') {
					 echo    "<td align=center><a href=".base_url()."index.php/faktur-pembelian-wip/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cstatus_lunas."/".$cunit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>";
					 //if ($this->session->userdata('gid') == 1)
						echo    "&nbsp; <a href=".base_url()."index.php/faktur-pembelian-wip/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cstatus_lunas."/".$cunit_jahit."/".$tgl_awal."/".$tgl_akhir."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
					echo "</td>";
				  }
				  else
					echo "<td align=center>&nbsp;</td>";
				}
				else {
					echo "<td align=center>&nbsp;</td>";
				}
					 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
