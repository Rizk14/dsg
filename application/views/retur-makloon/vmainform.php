<h3>Data Nota Debet Claim Produksi</h3><br>
<a href="<? echo base_url(); ?>index.php/retur-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/retur-makloon/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
	
	
	$('#pilih_faktur').click(function(){
			var supplier= $('#kode_supplier').val();
			var urlnya = "<?php echo base_url(); ?>index.php/retur-makloon/cform/show_popup_faktur/"+supplier;
			openCenteredWindow(urlnya);

	  });

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	
    
}

function cek_item_brg() {
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();
	if (date_from == '') {
		alert("Tanggal awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal akhir harus dipilih..!");
		return false;
	}
	
	if (date_to < date_from) {
		alert("Tanggal akhir harus lebih besar dari tanggal awal..!");
		return false;
	}

}

function cek_retur() {
	var tgl_retur= $('#tgl_retur').val();
	var jum= $('#no').val()-1;
	//alert(jum);

	if (tgl_retur == '') {
		alert("Tanggal nota debet harus dipilih..!");
		return false;
	}
	
	//if (document.f_master_brg.sj.length > 0) {
		var xx = 0;
		for(var i=1; i <= jum; i++){
			//if ($('#cek_'+i).val() == 'y')
			if ($('#cek_'+i).attr('checked') )
				xx= xx + 1;
		}
		//alert (xx);
		if (xx == 0) {
			alert("Item barang yg mau diretur harus dipilih..!");
			return false;
		}
		//alert(jum); return false;
	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#cek_'+k).attr('checked') ) {
			
				if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
					alert("Data qty tidak boleh 0 / kosong...!");
					return false;
				}
				if (isNaN($('#qty_'+k).val()) ) {
					alert("Qty harus berupa angka..!");
					return false;
				}
				
				var qty_faktur = parseFloat($('#qty_faktur_'+k).val());
				var jum_retur = parseFloat($('#jum_retur_'+k).val());
				var qty = parseFloat($('#qty_'+k).val());
				var cek_jumlah = 0;
				
				cek_jumlah = qty_faktur-jum_retur-qty;
				if (cek_jumlah < 0) {
					alert("Jumlah retur melebihi qty di faktur...!");
					return false;
				}
				
			} // end checkbox
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
		
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_pp', 'id' => 'f_pp');
echo form_open('retur-makloon/cform/', $attributes); ?>
<table>
		<tr>
			<td>Jenis Makloon </td>
			<td><select name="jenis_makloon" id="jenis_makloon" onkeyup="this.blur();this.focus();">
						
						<?php foreach ($jenis_makloon as $jm) { ?>
							<option value="<?php echo $jm->id ?>" ><?php echo $jm->id." - ". $jm->nama ?></option>
						<?php } ?>
						</select></td>
		</tr>

		<tr>
			<td>Unit Makloon</td>
			<td><select name="kode_supplier" id="kode_supplier" onkeyup="this.blur();this.focus();" >
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->kode_supplier ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;</td> 
		</tr>
		<tr>
			<td>Faktur Dari tanggal</td>
			<td>
			<label>
			  <input name="date_from" type="text" id="date_from" size="10" value="" readonly="true">
			</label>
			   <img alt="" id="date_from" align="middle"
					title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
					onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
			</td>
	  </tr>
	  <tr>
			<td>Faktur Sampai tanggal</td>
			<td>
			<label>
			  <input name="date_to" type="text" id="date_to" size="10" value="" readonly="true">
			</label>
			   <img alt="" id="date_to" align="middle"
					title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
					onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
			</td>
	  </tr>
		
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_brg();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/retur-makloon/cform/view'">
<?php echo form_close(); 
} else { ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/retur-makloon/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="date_from" value="<?php echo $date_from ?>">
<input type="hidden" name="date_to" value="<?php echo $date_to ?>">
<input type="hidden" name="kode_supplier" value="<?php echo $kode_supplier ?>">
<input type="hidden" name="jenis_makloon" value="<?php echo $jenis_makloon ?>">

<?php 
		if (count($brg_detail)>0) {
			$no=1;
			foreach ($brg_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
		<tr>
			<td width="15%">Range Tgl Faktur</td>
			<td width="70%">
			  <?php echo $date_from. " s.d. ".$date_to; ?>
			</td>
	  </tr>
		<tr>
			<td>Unit Makloon</td>
			<td><?php echo $kode_supplier." - ". $nama_supplier ?></td> 
		</tr>

	<tr>
			<td>No Nota Debet</td>
			<td><input type="text" name="no_dn" id="no_dn" value="<?php echo $nomordn ?>" size="20" readonly="true"></td>
		</tr>
  <tr>
  <tr>
    <td>Tgl Nota Debet</td>
    <td>
	<label>
      <input name="tgl_retur" type="text" id="tgl_retur" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_retur" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_retur,'dd-mm-yyyy',this)">
	</td>
  </tr>
  
  <tr>
			<td>Keterangan</td>
			<td><input type="text" name="ket" id="ket" value="" size="30" maxlength="30"></td>
		</tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="right">
			<!-- <input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang"> -->
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <?php
			if ($jenis_makloon != '1') {
          ?>
			<th>Barang Jadi</th>
		  <?php
			}
		  ?>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
           <th>Biaya (Rp.)</th>
	       <th>Qty di Faktur</th>
	       <th>Qty Pernah Diretur</th>
	       <th>Qty Yg Diretur</th>
	      <th>Pilih</th>
        </tr>

        <?php $i=1;
        if (count($brg_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap" colspan="6">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
		
			for($j=0;$j<count($brg_detail);$j++){
			?>
          <input name="faktur_<?php echo $i ?>" type="hidden" id="faktur_<?php echo $i ?>" 
          value="<?php echo $brg_detail[$j]['no_faktur'] ?>" />
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          
          <?php
			if ($jenis_makloon != '1') {
          ?>
          <td><input name="brg_jadi_<?php echo $i ?>" type="text" id="brg_jadi_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $brg_detail[$j]['kode_brg_jadi']."-".$brg_detail[$j]['nama_brg_jadi'] ?>" /></td>
          <input name="kode_brg_jadi_<?php echo $i ?>" type="hidden" id="kode_brg_jadi_<?php echo $i ?>" 
          value="<?php echo $brg_detail[$j]['kode_brg_jadi'] ?>" />
          <?php
			}
          ?>
          
          <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="20" readonly="true" value="<?php echo $brg_detail[$j]['kode_brg'] ?>"/>
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $brg_detail[$j]['nama'] ?>" /></td>
          <td><input name="biaya_<?php echo $i ?>" type="text" id="biaya_<?php echo $i ?>" size="7" readonly="true" value="<?php echo $brg_detail[$j]['biaya'] ?>" /></td>
          <td><input name="qty_faktur_<?php echo $i ?>" type="text" id="qty_faktur_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $brg_detail[$j]['qty_faktur'] ?>" readonly="true" /></td>
         <td><input name="jum_retur_<?php echo $i ?>" type="text" id="jum_retur_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $brg_detail[$j]['jum_retur'] ?>" readonly="true" /></td>
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="10" maxlength="10" value="0" /></td>
          <td><input name="cek_<?php echo $i ?>" type="checkbox" id="cek_<?php echo $i ?>" value="y" /> </td>
        </tr>
		<?php $i++; } // end foreach 
		}
		?>
		
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="9" align="center"><input type="submit" name="submit2" value="Simpan" onclick="return cek_retur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/retur-makloon/cform/'"></td>
	</tr>	
		
	</table>	
	
	</form>
      </td>
    </tr>

</table>
</div>
</form>
<?php } ?>
