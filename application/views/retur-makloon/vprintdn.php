<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }

</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	window.print();
});
</script>
<?php
	include ("php/fungsi.php");

	//$tgl = date("d-m-Y");
	$pisah1 = explode("-", $query[0]['tgl_retur']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
				
		$bilangan = new Terbilang;
		$kata=ucwords($bilangan->eja($tot_retur));
?>

<table border='0' align="center">
	<tr>
		<td colspan="3" align="center"> <h2><i>Nota Claim Produksi</i></h2> </td>
	</tr>
	<tr>
		<td>No. <?php echo $query[0]['no_dn_retur'] ?> </td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td nowrap>Bandung, <?php echo $tgl ?></td>
	</tr>

	<tr>
		<td>Kepada : <?php echo $query[0]['nama_supplier'] ?></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>Kami telah mendebet faktur Saudara tanggal <?php echo $tgl ?> <br> Sejumlah : Rp. <?php echo number_format($tot_retur,2,',','.') ?> 
		untuk faktur tanggal <?php echo $faktur_date_from ?> - <?php echo $faktur_date_to ?> </td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Terbilang : <b><i> <?php echo $kata ?> Rupiah </i></b></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Ket : Retur Claim Produksi</td>
	</tr>
	<?php $i=1;
        if (count($query[0]['detail_retur'])==0) {
		?>
		<tr align="center">
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
	<?php
		} else {
			$detailnya = $query[0]['detail_retur'];
			for($j=0;$j<count($query[0]['detail_retur']);$j++){
			?>
			<tr>
			<td nowrap><?php echo $detailnya[$j]['nama']." = ".number_format($detailnya[$j]['qty'],0,',','.')." x Rp.". number_format($detailnya[$j]['biaya'],2,',','.')." = "; 
				$perkalian = $detailnya[$j]['qty'] * $detailnya[$j]['biaya'];
				echo number_format($perkalian,2,',','.');
			?> </td>
          
        </tr>
		<?php $i++; } // end foreach 
		}
		?>

	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami,</td>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;Penerima,</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td>(<font color="#FFFFFF">TESTESTTEST</font>)</td>
		<td>&nbsp;</td>
		<td>(<font color="#FFFFFF">TESTESTTEST</font>)</td>
	</tr>
	

</table>
