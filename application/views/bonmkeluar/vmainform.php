<h3>Data Pengeluaran Bahan Baku/Pembantu</h3><br>
<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/view">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>" . $msg . "</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>

<script>
	//tambah
	$(function() {
		$("#no").val('2');
		//generate_nomor();		
		$("#addrow").click(function() {
			//inisialisasi untuk id unik
			var no = $("#no").val();
			var n = parseInt(no) - 1;
			//copy last row
			var lastRow = $("#tabelku tr:last").clone();
			//----------------SETTING KONDISI ROW BARU*****
			//******no*************************************
			var num = "#num_" + n;
			var new_num = "#num_" + no;
			$(num, lastRow).attr("id", "num_" + no);
			$(new_num, lastRow).html(no);
			//******end no*********************************

			//*****is_quilting*************************************
			/*	var is_quilting="#is_quilting_"+n;
				var new_is_quilting="#is_quilting_"+no;
				$(is_quilting, lastRow).attr("id", "is_quilting_"+no);
				$(new_is_quilting, lastRow).attr("name", "is_quilting_"+no);		
				$(new_is_quilting, lastRow).val('t');
				$(new_is_quilting, lastRow).attr("checked", false); */
			//*****end is_quilting*********************************

			//*****untuk_bisbisan*************************************
			var untuk_bisbisan = "#untuk_bisbisan_" + n;
			var new_untuk_bisbisan = "#untuk_bisbisan_" + no;
			$(untuk_bisbisan, lastRow).attr("id", "untuk_bisbisan_" + no);
			$(new_untuk_bisbisan, lastRow).attr("name", "untuk_bisbisan_" + no);
			$(new_untuk_bisbisan, lastRow).val('t');
			$(new_untuk_bisbisan, lastRow).attr("checked", false);
			//*****end untuk_bisbisan*********************************

			//*****kode*************************************
			var kode = "#kode_" + n;
			var new_kode = "#kode_" + no;
			$(kode, lastRow).attr("id", "kode_" + no);
			$(new_kode, lastRow).attr("name", "kode_" + no);
			$(new_kode, lastRow).val('');
			//*****end kode*********************************

			//*****id_brg*************************************
			var id_brg = "#id_brg_" + n;
			var new_id_brg = "#id_brg_" + no;
			$(id_brg, lastRow).attr("id", "id_brg_" + no);
			$(new_id_brg, lastRow).attr("name", "id_brg_" + no);
			$(new_id_brg, lastRow).val('');
			//*****end id_brg*********************************

			//*****nama*************************************
			var nama = "#nama_" + n;
			var new_nama = "#nama_" + no;
			$(nama, lastRow).attr("id", "nama_" + no);
			$(new_nama, lastRow).attr("name", "nama_" + no);
			$(new_nama, lastRow).val('');
			//*****end nama*************************************

			//*****satuan*************************************
			var satuan = "#satuan_" + n;
			var new_satuan = "#satuan_" + no;
			$(satuan, lastRow).attr("id", "satuan_" + no);
			$(new_satuan, lastRow).attr("name", "satuan_" + no);
			$(new_satuan, lastRow).val('');
			//*****end satuan*************************************	

			//*****tsatuan_konv*************************************
			var tsatuan_konv = "#tsatuan_konv_" + n;
			var new_tsatuan_konv = "#tsatuan_konv_" + no;
			$(tsatuan_konv, lastRow).attr("id", "tsatuan_konv_" + no);
			$(new_tsatuan_konv, lastRow).attr("name", "tsatuan_konv_" + no);
			$(new_tsatuan_konv, lastRow).val('0');
			//*****end tsatuan_konv*************************************	

			//*****satuan_konv*************************************
			var satuan_konv = "#satuan_konv_" + n;
			var new_satuan_konv = "#satuan_konv_" + no;
			$(satuan_konv, lastRow).attr("id", "satuan_konv_" + no);
			$(new_satuan_konv, lastRow).attr("name", "satuan_konv_" + no);
			$(new_satuan_konv, lastRow).val('0');
			//*****end satuan_konv*************************************	

			//*****rumus_konv*************************************
			var rumus_konv = "#rumus_konv_" + n;
			var new_rumus_konv = "#rumus_konv_" + no;
			$(rumus_konv, lastRow).attr("id", "rumus_konv_" + no);
			$(new_rumus_konv, lastRow).attr("name", "rumus_konv_" + no);
			$(new_rumus_konv, lastRow).val('0');
			//*****end rumus_konv*************************************	

			//*****angka_faktor_konversi*************************************
			var angka_faktor_konversi = "#angka_faktor_konversi_" + n;
			var new_angka_faktor_konversi = "#angka_faktor_konversi_" + no;
			$(angka_faktor_konversi, lastRow).attr("id", "angka_faktor_konversi_" + no);
			$(new_angka_faktor_konversi, lastRow).attr("name", "angka_faktor_konversi_" + no);
			$(new_angka_faktor_konversi, lastRow).val('0');
			//*****end angka_faktor_konversi*************************************	

			//*****stok*************************************
			var stok = "#stok_" + n;
			var new_stok = "#stok_" + no;
			$(stok, lastRow).attr("id", "stok_" + no);
			$(new_stok, lastRow).attr("name", "stok_" + no);
			$(new_stok, lastRow).val('');
			//*****end stok*************************************	

			//*****nama_brg_wip_*************************************
			var nama_brg_wip = "#nama_brg_wip_" + n;
			var new_nama_brg_wip = "#nama_brg_wip_" + no;
			$(nama_brg_wip, lastRow).attr("id", "nama_brg_wip_" + no);
			$(new_nama_brg_wip, lastRow).attr("name", "nama_brg_wip_" + no);
			$(new_nama_brg_wip, lastRow).val('');
			//*****end nama_brg_wip_*************************************

			//*****qty_satawal*************************************
			var qty_satawal = "#qty_satawal_" + n;
			var new_qty_satawal = "#qty_satawal_" + no;
			$(qty_satawal, lastRow).attr("id", "qty_satawal_" + no);
			$(new_qty_satawal, lastRow).attr("name", "qty_satawal_" + no);
			//$(new_qty_satawal, lastRow).attr("onkeyup", "javascript: var x= $('#satuan_konv_"+no+"').val(); if (x == 0) { $('#qty_"+no+"').val(this.value); }");
			// onkeyup="javascript: var x= $('#satuan_konv_1').val(); if (x == 0) { $('#qty_1').val(this.value); }"
			$(new_qty, lastRow).attr("readonly", true);
			$(new_qty_satawal, lastRow).val('0');
			//*****end qty_satawal*************************************	

			//*****qty*************************************
			var qty = "#qty_" + n;
			var new_qty = "#qty_" + no;
			$(qty, lastRow).attr("id", "qty_" + no);
			$(new_qty, lastRow).attr("name", "qty_" + no);
			$(new_qty, lastRow).attr("onkeyup", "javascript: var rumus= $('#rumus_konv_" + no + "').val(); var angka= $('#angka_faktor_konversi_" + no + "').val(); " +
				"if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else { $('#qty_satawal_" + no + "').val(this.value); }");
			/*
		onkeyup="javascript: var rumus= $('#rumus_konv_1').val(); var angka= $('#angka_faktor_konversi_1').val(); 
          if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); $('#qty_1').val(qtynya); } 
          else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); $('#qty_1').val(qtynya); }
          else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); $('#qty_1').val(qtynya); }
          else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); $('#qty_1').val(qtynya); }
          else { $('#qty_1').val(this.value); }"
		*/
			$(new_qty, lastRow).val('0');

			//*****end qty*************************************	

			//*****ket_detail*************************************
			var ket_detail = "#ket_detail_" + n;
			var new_ket_detail = "#ket_detail_" + no;
			$(ket_detail, lastRow).attr("id", "ket_detail_" + no);
			$(new_ket_detail, lastRow).attr("name", "ket_detail_" + no);
			$(new_ket_detail, lastRow).val('');
			//*****end ket_detail*************************************	

			//button pilih*****************************************
			var pilih = "#pilih_" + n;
			var new_pilih = "#pilih_" + no;
			$(pilih, lastRow).attr("id", "pilih_" + no);
			var nama_for_even = "document.f_purchase.iddata.value=" + no;

			var even_klik = "var x= $('#gudang').val(); var q='f'; openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg/'+ x+'/'+q+'/" + no + "');";

			$(new_pilih, lastRow).attr("name", "pilih_" + no);
			$(new_pilih, lastRow).attr("onmouseover", nama_for_even);
			$(new_pilih, lastRow).attr("onclick", even_klik);
			//end button pilih		

			//button pilih*****************************************
			var pilih_wip = "#pilih_wip_" + n;
			var new_pilih_wip = "#pilih_wip_" + no;
			$(pilih_wip, lastRow).attr("id", "pilih_wip_" + no);
			var nama_for_even_wip = "document.f_purchase.iddata.value=" + no;

			var even_klik_wip = "openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg_wip/" + no + "');";

			$(new_pilih_wip, lastRow).attr("name", "pilih_" + no);
			$(new_pilih_wip, lastRow).attr("onmouseover", nama_for_even_wip);
			$(new_pilih_wip, lastRow).attr("onclick", even_klik_wip);
			//end button pilih		

			// 04-11-2015
			//*****kode_brg_wip*************************************
			var kode_brg_wip = "#kode_brg_wip_" + n;
			var new_kode_brg_wip = "#kode_brg_wip_" + no;
			$(kode_brg_wip, lastRow).attr("id", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("name", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("onkeyup", "cari('" + no + "', this.value);");
			$(new_kode_brg_wip, lastRow).val('');
			//*****end kode_brg_wip*********************************

			//******div infobrgwip*************************************
			var infobrgwip = "#infobrgwip_" + n;
			var new_infobrgwip = "#infobrgwip_" + no;
			$(infobrgwip, lastRow).attr("id", "infobrgwip_" + no);

			$(new_infobrgwip, lastRow).html("<input type='hidden' id='id_brg_wip_" + no + "' name='id_brg_wip_" + no + "' value=''>");

			//----------------END SETTING KONDISI ROW BARU*		
			//tambahin row nya sesuai settingan
			$("#tabelku").append(lastRow);
			//no++
			var x = parseInt(no);
			$("#no").val(x + 1);

		});

		$("#deleterow").click(function() {
			var x = $("#no").val();
			if (x > 2) {
				$("#tabelku tr:last").remove();
				var x = $("#no").val();
				var y = x - 1;
				$("#no").val(y);
			}
		});

		// 11-07-2015
		$('#dibebankan').click(function() {
			if ($("#dibebankan").is(":checked")) {
				$('#ket').attr('disabled', true);
				$('#ket').val('');
			} else {
				$('#ket').attr('disabled', false);
				$('#ket').val('');
			}
		});

	});
</script>
<script type="text/javascript">
	function openCenteredWindow(url) {

		var width = 720;
		var height = 480;
		var left = parseInt((screen.availWidth / 2) - (width / 2));
		var top = parseInt((screen.availHeight / 2) - (height / 2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

	function cari(posisi, kodebrgwip) {
		$.ajax({
			type: 'post',
			url: '<?php echo base_url(); ?>index.php/bonmkeluar/cform/caribrgwip',
			data: 'kode_brg_wip=' + kodebrgwip + '&posisi=' + posisi,
			success: function(response) {
				$("#infobrgwip_" + posisi).html(response);
			}
		});
	}

	function cek_bonm() {
		var no_bonm = $('#no_bonm_manual').val();
		var tgl_bonm = $('#tgl_bonm').val();
		var tujuan = $('#tujuan').val();
		var id_unit_jahit = $('#id_unit_jahit').val();
		var id_unit_packing = $('#id_unit_packing').val();
		var s = 0;
		kon = window.confirm("Yakin akan simpan data ini ??");

		if (kon) {
			if (no_bonm == '') {
				alert("Nomor Bon M manual harus diisi..!");
				s = 1;
				return false;
			}
			if (tgl_bonm == '') {
				alert("Tanggal Bon M harus dipilih..!");
				s = 1;
				return false;
			}

			if (tujuan == 2 && id_unit_jahit == 0) {
				alert("Unit Jahit harus Dipilih");
				s = 1;
				return false;
			}
			if (tujuan == 9 && id_unit_packing == 0) {
				alert("Unit Packing harus Dipilih");
				s = 1;
				return false;
			}

			var jum = $('#no').val() - 1;

			if (jum > 0) {
				for (var k = 1; k <= jum; k++) {

					if ($('#kode_' + k).val() == '') {
						alert("Data item bahan baku/pembantu tidak boleh ada yang kosong...!");
						s = 1;
						return false;
					}
					if ($('#qty_' + k).val() == '0' || $('#qty_' + k).val() == '') {
						alert("Data qty tidak boleh 0 / kosong...!");
						s = 1;
						return false;
					}
					if (isNaN($('#qty_' + k).val())) {
						alert("Qty harus berupa angka..!");
						s = 1;
						return false;
					}
					/*if ($('#id_brg_wip_'+k).val() == '') {
						alert("Kode barang WIP harus diisi..!");
						s = 1;
						return false;
					} */
					// 150113 disabled aja, biarin ngaruh ke stok harga juga
					/*if ($('#stok_'+k).val() - $('#qty_'+k).val() < 0 ) {
						alert("Pengeluaran barang "+$('#nama_'+k).val()+" melebihi stok...!");
						s = 1;
						return false;
					} */

				}
			} else {
				alert("Data detail tidak ada");
				s = 1;
				return false;
			}

			if (s == 0)
				return true;
		} // end if kon
		else
			return false;
	}
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/bonmkeluar/cform/submit" method="post" enctype="multipart/form-data">

	<input type="hidden" name="no" id="no" value="">
	<input type="hidden" name="iddata" id="iddata" />
	<input type="hidden" name="no_bonm" id="no_bonm" value="">
	<div align="center">

		<label id="status"></label>
		<br>

		<table class="proit-view" width="100%" cellspacing="2" cellpadding="1">
			<tr>
				<td width="15%">Lokasi Gudang</td>
				<td> <select name="gudang" id="gudang">
						<?php foreach ($list_gudang as $gud) { ?>
							<option value="<?php echo $gud->id ?>"><?php echo "[" . $gud->nama_lokasi . "] " . $gud->kode_gudang . "-" . $gud->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<!--<tr>
    <td width="15%">Nomor Bon M / SJ</td>
    <td>
      <input name="no_bonm" type="text" id="no_bonm" size="10" maxlength="20" value="<?php //echo $no_bonm 
																						?>" readonly="true">
      &nbsp; Nomor Manual <input name="no_bonm_manual" type="text" id="no_bonm_manual" size="15" maxlength="15" value="">
    </td>
  </tr> -->
			<tr>
				<td width="15%">Nomor Bon M / SJ Manual</td>
				<td>
					<input name="no_bonm_manual" type="text" id="no_bonm_manual" size="15" maxlength="15" value="">
				</td>
			</tr>
			<tr>
				<td>Tanggal Bon M / SJ</td>
				<td>
					<label>
						<input name="tgl_bonm" type="text" id="tgl_bonm" size="10" value="" readonly="true">
					</label>
					<img alt="" id="tgl_bonm" align="middle" title="Pilih tgl.." src="<?php echo base_url(); ?>images/calendar.gif" onclick="displayCalendar(document.forms[0].tgl_bonm,'dd-mm-yyyy',this)">
				</td>
			</tr>

			<tr>
				<td>Dari</td>
				<td>
					<input name="esender" type="text" id="esender" value="" placeholder="Nama Penyerah" maxlength="125"> /
					<input style="width:200px" name="esendercompany" type="text" id="esendercompany" maxlength="125" value="" placeholder="Nama Perusahaan/Departemen">
				</td>
			</tr>

			<tr>
				<td>Untuk</td>
				<td>
					<input name="erecipient" type="text" id="erecipient" maxlength="125" value="" placeholder="Nama Penerima"> /
					<input style="width:200px" name="erecipientcompany" type="text" id="erecipientcompany" placeholder="Nama Perusahaan/Departemen" maxlength="125" value="">
				</td>
			</tr>

			<tr>
				<td>Tujuan</td>
				<td> <select name="tujuan" id="tujuan">
						<option value="1">Cutting</option>
						<option value="2">Unit Jahit</option>
						<option value="3">Lain-Lain (Retur)</option>
						<option value="4">Lain-Lain (Peminjaman)</option>
						<option value="5">Lain-Lain (Lainnya)</option>
						<option value="6">Pengadaan</option>
						<option value="7">QC/WIP</option>
						<option value="8">Makloon</option>
						<option value="9">Unit Packing</option>
						<!--	<option value="6" >Pengadaan</option>
				<option value="7" >Quilting</option>
				<option value="8" >WIP</option>
				<option value="9" >RND</option>
				<option value="10" >BLN Kedungreja</option> -->
					</select>
				</td>
			</tr>

			<tr>
				<td>Unit Jahit</td>
				<td> <select name="id_unit_jahit" id="id_unit_jahit">
						<option value="0">Tidak Ada</option>
						<?php foreach ($list_unit_jahit as $jht) { ?>
							<option value="<?php echo $jht->id ?>"><?php echo $jht->kode_unit . "-" . $jht->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Unit Packing</td>
				<td> <select name="id_unit_packing" id="id_unit_packing">
						<option value="0">Tidak Ada</option>
						<?php foreach ($list_unit_packing as $pck) { ?>
							<option value="<?php echo $pck->id ?>"><?php echo $pck->kode_unit . "-" . $pck->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td>
					<input name="ket" type="text" id="ket" size="30" value="">&nbsp;<input type="checkbox" name="dibebankan" id="dibebankan" value="DIBEBANKAN">Dibebankan
				</td>
			</tr>
			<!--  <tr>
	<td colspan="2"><i>*) Untuk bahan yg satuannya Yard, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Meter (kecuali quilting, tetap Yard)<br>
	*) Untuk bahan yg satuannya Lusin, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Pieces<br>
	*) Untuk bahan lain yg memerlukan konversi satuan, silahkan pilih di kolom Satuan Konversi<br></i></td>
  </tr> -->

			<tr>
				<td colspan="2"><br>
					<form name="myform">
						<table id="tabelku" width="90%" border="0" align="center" cellpadding="1" cellspacing="2" class="proit-view">
							<tr>
								<td colspan="8" align="left">
									<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
									<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
								</td>
							</tr>
							<tr>
								<th width="20">No</th>
								<!--<th>Kelompok Brg</th> -->
								<!--   <th>Quilting</th> -->
								<th>Kode & Nama Bhn Baku/Pembantu</th>
								<th>Satuan Awal</th>
								<th>Satuan Konversi</th>
								<th>Qty<br>Sat Awal</th>
								<th>Qty<br>Sat Konv</th>
								<th>Acuan Barang<br>WIP</th>
								<th>Keterangan</th>
							</tr>

							<tr align="center">
								<td align="center" id="num_1">1</td>
								<!-- <td><select name="kel_brg_1" id="kel_brg_1">
				<?php //foreach ($kel_brg as $kel) { 
				?>
					<option value="<?php //echo $kel->kode 
									?>" ><?php //echo $kel->kode." - ". $kel->nama 
											?></option>
				<?php //} 
				?>
				</select>
		  </td> -->
								<!-- <td>
			<input type="checkbox" name="is_quilting_1" id="is_quilting_1" value="t" >
		  </td> -->
								<td nowrap="nowrap">
									<input name="nama_1" type="text" id="nama_1" size="40" readonly="true" value="" />
									<input name="kode_1" type="hidden" id="kode_1" value="" />
									<input name="id_brg_1" type="hidden" id="id_brg_1" value="" />
									<input name="stok_1" type="hidden" id="stok_1" value="" />
									<input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" onclick="javascript: var x= $('#gudang').val(); var q='f'; openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg/'+ x+'/'+q+'/1');">
								</td>
								<td>
									<input name="satuan_1" type="text" id="satuan_1" size="5" readonly="true" value="" />
								</td>
								<td>
									<select name="tsatuan_konv_1" id="tsatuan_konv_1" disabled>
										<option value="0">Tidak Ada</option>
										<?php foreach ($list_satuan as $sat) { ?>
											<option value="<?php echo $sat->id ?>"><?php echo $sat->nama ?></option>
										<?php } ?>
									</select>
									<input type="hidden" name="satuan_konv_1" id="satuan_konv_1" value="0">
									<input type="hidden" name="rumus_konv_1" id="rumus_konv_1" value="0">
									<input type="hidden" name="angka_faktor_konversi_1" id="angka_faktor_konversi_1" value="0">
								</td>
								<td>
									<input style="text-align:right;" name="qty_satawal_1" type="text" id="qty_satawal_1" size="3" value="0" readonly="true" />
								</td>

								<td>
									<input style="text-align:right;" name="qty_1" type="text" id="qty_1" size="3" value="0" onkeyup="javascript: var rumus= $('#rumus_konv_1').val(); var angka= $('#angka_faktor_konversi_1').val(); 
									if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_1').val(qtynya); } 
									else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_1').val(qtynya); }
									else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_1').val(qtynya); }
									else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_1').val(qtynya); }
									else { $('#qty_satawal_1').val(this.value); }" />
								</td>

								<td style="white-space:nowrap;">
									<input name="kode_brg_wip_1" type="text" id="kode_brg_wip_1" size="10" value="" onkeyup="cari('1',this.value);" /><br>

									<input name="nama_brg_wip_1" type="text" id="nama_brg_wip_1" value="" />

									<div id="infobrgwip_1">
										<input name="id_brg_wip_1" type="hidden" id="id_brg_wip_1" value="" />
									</div>
									<input title="browse data barang wip" onmouseover="document.f_purchase.iddata.value=1" name="pilih_wip_1" value="..." type="button" id="pilih_wip_1" onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg_wip/1');" />
								</td>

								<td style="white-space:nowrap;">
									<input name="ket_detail_1" type="text" id="ket_detail_1" size="20" value="" />
									<input type="checkbox" name="untuk_bisbisan_1" id="untuk_bisbisan_1" value="t">Untuk Makloon Bisbisan
								</td>
							</tr>
						</table>
					</form><br>
					<div align="center">
						<input type="submit" name="submit2" value="Simpan" onclick="return cek_bonm();">&nbsp;
						<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/bonmkeluar/cform/view'">
					</div>
				</td>
			</tr>
		</table>
	</div>
</form>