<style type="text/css">
	table {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 12px;
	}

	.fieldsetdemo {
		background-color: #DDD;
		max-width: 400px;
		padding: 14px;
	}

	.judulnya {
		background-color: #DDD;
	}

	.legend {
		display: block;
		width: 100%;
		padding: 5px 15px;
		margin-bottom: unset !important;
		font-size: 16px;
		line-height: inherit;
		color: #333;
		border: 0;
		border: 1px solid #e5e5e5;
		background: white;
		border-radius: 5px;
	}

	.print2 {
		padding-top: 2px;
		padding-left: 5px;
		height: 24px;
		width: 24px
	}
</style>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>


<link rel="stylesheet" href="https://cdn.datatables.net/1.13.5/css/dataTables.bootstrap.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-3.7.0.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap.min.js"></script>

<style type="text/css">
	table {
		font-family: Helvetica, Geneva, Arial,
			SunSans-Regular, sans-serif;
		font-size: 12px;
	}

	.fieldsetdemo {
		background-color: #DDD;
		max-width: 400px;
		padding: 14px;
	}

	.judulnya {
		background-color: #DDD;
	}
</style>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>

<script type="text/javascript">
	$(function() {
		$('#filter_brg').click(function() {
			if ($("#filter_brg").is(":checked")) {
				$('#cari_brg').attr('disabled', false);
				$('#cari_brg').val('');
			} else {
				$('#cari_brg').attr('disabled', true);
				$('#cari_brg').val('');
			}
		});
	});

	function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth / 2) - (width / 2));
		var top = parseInt((screen.availHeight / 2) - (height / 2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

	function stbPrint() {
		event.preventDefault();
		const url = event.target.getAttribute('href');

		window.open(url, 'Cetak STB', '_blank', 'titlebar=no,toolbar=no,location=no,status=no,menubar=no');

		window.location.reload();
	}
</script>

<h3>Data Pengeluaran Bahan Baku/Pembantu</h3><br>
<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/view">View Data</a>&nbsp;<br><br>

<div>
	Total Data = <?php echo $jum_total ?><br><br>
	<?php echo form_open('bonmkeluar/cform/cari'); ?>
	<fieldset class="fieldsetdemo">
		<legend>Filter Pencarian Data</legend>
		<table>
			<tr>
				<td>Dari Tanggal</td>
				<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
					<img alt="" id="date1" align="middle" title="Pilih tgl.." src="<?php echo base_url(); ?>images/calendar.gif" onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)">
				</td>
			</tr>
			<tr>
				<td>Sampai Tanggal</td>
				<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
					<img alt="" id="date2" align="middle" title="Pilih tgl.." src="<?php echo base_url(); ?>images/calendar.gif" onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
				</td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">Lokasi Gudang</td>
				<td style="white-space:nowrap;">: <select name="gudang" id="gudang">

						<?php
						if ($this->session->userdata('gid') != 16) {
						?>
							<option value="0" <?php if ($cgudang == '0') { ?> selected="true" <?php } ?>>- All -</option>
						<?php
						}
						?>


						<?php foreach ($list_gudang as $gud) { ?>
							<option value="<?php echo $gud->id ?>" <?php if ($cgudang == $gud->id) { ?> selected="true" <?php } ?>><?php echo "[" . $gud->nama_lokasi . "] " . $gud->kode_gudang . "-" . $gud->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Tujuan</td>
				<td>: <select name="tujuan" id="tujuan">
						<option value="0" <?php if ($ctujuan == '0') { ?> selected="true" <?php } ?>>- All -</option>
						<option value="1" <?php if ($ctujuan == '1') { ?> selected="true" <?php } ?>>Cutting</option>
						<option value="2" <?php if ($ctujuan == '2') { ?> selected="true" <?php } ?>>Unit</option>
						<option value="3" <?php if ($ctujuan == '3') { ?> selected="true" <?php } ?>>Lain-Lain (Retur)</option>
						<option value="4" <?php if ($ctujuan == '4') { ?> selected="true" <?php } ?>>Lain-Lain (Peminjaman)</option>
						<option value="5" <?php if ($ctujuan == '5') { ?> selected="true" <?php } ?>>Lain-Lain (Lainnya)</option>
						<option value="6" <?php if ($ctujuan == '6') { ?> selected="true" <?php } ?>>Pengadaan</option>
						<option value="7" <?php if ($ctujuan == '7') { ?> selected="true" <?php } ?>>QC/WIP</option>
						<option value="8" <?php if ($ctujuan == '8') { ?> selected="true" <?php } ?>>Cutting (Lainnya)</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>No Bon M / SJ Manual</td>
				<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
			</tr>
			<tr>
				<td style="white-space:nowrap;" colspan="2"><input type="checkbox" name="filter_brg" id="filter_brg" value="y" <?php if ($filterbrg == 'y') { ?> checked="true" <?php } ?>>
					Filter berdasarkan kode/nama brg: <input type="text" name="cari_brg" id="cari_brg" size="20" value="<?php echo $caribrg ?>" <?php if ($filterbrg == 'n') { ?>disabled="true" <?php } ?>></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="Cari"></td>
			</tr>
		</table>
	</fieldset>

	<?php echo form_close(); ?>
	<br>
	<?php
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;

	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
	?>
	<form id="f_master_brg" name="f_master_brg">
		<input type="hidden" name="id_bonmkeluar" id="id_bonmkeluar">
		<table border="1" cellpadding="1" cellspacing="1" width="100%">
			<thead>
				<tr class="judulnya">
					<th>Gudang</th>
					<th>No Bon M / SJ</th>
					<th>No Bon M / SJ Manual</th>
					<th>Tgl Bon M</th>
					<th>Tujuan</th>
					<th>Keterangan Header</th>
					<th>List Bhn Baku/Pembantu & Sat Awal</th>
					<th>Keterangan Detail</th>
					<th>Qty<br>Sat Awal</th>
					<th>Satuan Keluar</th>
					<th>Qty Keluar</th>
					<th>Acuan Brg WIP</th>
					<!-- <th>Ket</th> -->
					<th>Last Update</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if (is_array($query)) {
					for ($j = 0; $j < count($query); $j++) {

						$pisah1 = explode("-", $query[$j]['tgl_update']);
						$tgl1 = $pisah1[2];
						$bln1 = $pisah1[1];
						$thn1 = $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;

						$exptgl1 = explode(" ", $tgl1);
						$tgl1nya = $exptgl1[0];
						$jam1nya = $exptgl1[1];
						$tgl_update = $tgl1nya . " " . $nama_bln . " " . $thn1 . " " . $jam1nya;

						$pisah1 = explode("-", $query[$j]['tgl_bonm']);
						$tgl1 = $pisah1[2];
						$bln1 = $pisah1[1];
						$thn1 = $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_bonm = $tgl1 . " " . $nama_bln . " " . $thn1;

						echo "<tr class=\"record\">";
						echo    "<td>" . $query[$j]['nama_gudang'] . "</td>";
						echo    "<td>" . $query[$j]['no_bonm'] . "</td>";
						echo    "<td>" . $query[$j]['no_manual'] . "</td>";
						echo    "<td>" . $tgl_bonm . "</td>";
						echo    "<td>" . $query[$j]['nama_tujuan'] . "</td>";
						echo    "<td>" . $query[$j]['keterangan'] . "</td>";

						// 04-11-2015
						if ($cari == '')
							$xcari = "all";
						else
							$xcari = $cari;

						if ($caribrg == '')
							$xcaribrg = "all";
						else
							$xcaribrg = $caribrg;

						echo "<td nowrap>";
						if (is_array($query[$j]['detail_bonm'])) {
							$var_detail = array();
							$var_detail = $query[$j]['detail_bonm'];
							$hitung = count($var_detail);
							for ($k = 0; $k < count($var_detail); $k++) {
								echo $var_detail[$k]['kode_brg'] . " - " . $var_detail[$k]['nama'] . " (" . $var_detail[$k]['satuan'] . ")";
								if ($k < $hitung - 1)
									echo "<br> ";
							}
						}
						echo "</td>";

						echo "<td nowrap align='right'>";
						if (is_array($query[$j]['detail_bonm'])) {
							$var_detail = array();
							$var_detail = $query[$j]['detail_bonm'];
							$hitung = count($var_detail);
							for ($k = 0; $k < count($var_detail); $k++) {
								echo $var_detail[$k]['keterangan'];
								if ($k < $hitung - 1)
									echo "<br> ";
							}
						}
						echo "</td>";

						echo "<td nowrap align='right'>";
						if (is_array($query[$j]['detail_bonm'])) {
							$var_detail = array();
							$var_detail = $query[$j]['detail_bonm'];
							$hitung = count($var_detail);
							for ($k = 0; $k < count($var_detail); $k++) {
								echo $var_detail[$k]['qty_satawal'];
								if ($k < $hitung - 1)
									echo "<br> ";
							}
						}
						echo "</td>";

						echo "<td style='white-space:nowrap;'>";
						if (is_array($query[$j]['detail_bonm'])) {
							$var_detail = array();
							$var_detail = $query[$j]['detail_bonm'];
							$hitung = count($var_detail);

							for ($k = 0; $k < count($var_detail); $k++) {
								if ($var_detail[$k]['konversi_satuan'] == 't') {
									if ($var_detail[$k]['id_satuan'] == '2')
										$var_konv = "Meter";
									else if ($var_detail[$k]['id_satuan'] == '7')
										$var_konv = "Pieces";
									else {
										if ($var_detail[$k]['id_satuan_konversi'] != '0')
											$var_konv = $var_detail[$k]['nama_satuan_konversi'];
										else
											$var_konv = $var_detail[$k]['satuan'];
									}
								} else
									$var_konv = $var_detail[$k]['satuan'];
								echo $var_konv;
								if ($k < $hitung - 1)
									echo "<br> ";
							}
						}
						echo "</td>";

						echo "<td nowrap align='right'>";
						if (is_array($query[$j]['detail_bonm'])) {
							$var_detail = array();
							$var_detail = $query[$j]['detail_bonm'];
							$hitung = count($var_detail);
							for ($k = 0; $k < count($var_detail); $k++) {
								echo $var_detail[$k]['qty'];
								if ($k < $hitung - 1)
									echo "<br> ";
							}
						}
						echo "</td>";

						/* echo "<td nowrap>";
				 if (is_array($query[$j]['detail_bonm'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_bonm'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['keterangan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>"; */

						echo "<td style='white-space:nowrap;'>";
						if (is_array($query[$j]['detail_bonm'])) {
							$var_detail = array();
							$var_detail = $query[$j]['detail_bonm'];
							$hitung = count($var_detail);
							for ($k = 0; $k < count($var_detail); $k++) {
								if ($var_detail[$k]['kode_brg_wip'] != '')
									echo $var_detail[$k]['kode_brg_wip'] . " - " . $var_detail[$k]['nama_brg_wip'];
								else
									echo " ";
								if ($k < $hitung - 1)
									echo "<br> ";
							}
						}
						echo "</td>";

						echo    "<td align='center'>" . $tgl_update . "</td>";
						if (($query[$j]['tanggal_periode']) > ($query[$j]['tgl_bonm'])) {
							echo "<td><i>* bonM Keluar sudah di closing </i></td>";
						} else {
							echo    "<td align=center>";

							//<a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_pb_cutting.value=".$query[$j]['id']."'; ><u>Print</u></a> &nbsp;
							// echo "	<a href=" . base_url() . "index.php/bonmkeluar/cform/edit/" . $query[$j]['id'] . "/" . $cur_page . "/" . $is_cari . "/" . $tgl_awal . "/" . $tgl_akhir . "/" . $cgudang . "/" . $ctujuan . "/" . $xcari . "/" . $xcaribrg . "/" . $filterbrg . " \" id=\"" . $query[$j]['id'] . "\">
							// 			Edit
							// 		</a>&nbsp;
							// 		<a href=" . base_url() . "index.php/bonmkeluar/cform/delete/" . $query[$j]['id'] . "/" . $cur_page . "/" . $is_cari . "/" . $tgl_awal . "/" . $tgl_akhir . "/" . $cgudang . "/" . $ctujuan . "/" . $xcari . "/" . $xcaribrg . "/" . $filterbrg . " \" onclick=\"return confirm('Hapus data ini?')\">
							// 			Delete
							// 		</a>";
				?>
							<!-- Lihat -->
							<a href="<?= base_url() . "index.php/bonmkeluar/cform/viewdetail/" . $query[$j]['id'] ?>" class="btn btn-sm btn-success" style="padding: 5px" title="Lihat">
								<span class="glyphicon glyphicon-eye-open"></span>
							</a>
				<?php
							/* <!-- edit --> */
							$url_edit = base_url() . "index.php/bonmkeluar/cform/edit/" . $query[$j]['id'] . "/" . $cur_page . "/" . $is_cari . "/" . $tgl_awal . "/" . $tgl_akhir . "/" . $cgudang . "/" . $ctujuan . "/" . $xcari . "/" . $xcaribrg . "/" . $filterbrg . " \" id=\"" . $query[$j]['id'] . "\"";
							$html = '<a href="' . $url_edit . '" class="btn btn-sm btn-warning" style="padding: 5px" title="Edit">
										<span class="glyphicon glyphicon-pencil"></span>
									</a>';

							if (!($query[$j]['f_bon_cancel'] == 'f' and $query[$j]['d_approve'] == null)) {
								$html = '';
							}

							echo $html;

							/* <!-- kondisi delete, jika belum approve & status masih aktif --> */
							$url_delete = base_url() . "index.php/bonmkeluar/cform/delete/" . $query[$j]['id'] . "/" . $cur_page . "/" . $is_cari . "/" . $tgl_awal . "/" . $tgl_akhir . "/" . $cgudang . "/" . $ctujuan . "/" . $xcari . "/" . $xcaribrg . "/" . $filterbrg . " \"";
							$html = '<a href="' . $url_delete . '" class="btn btn-sm btn-danger" style="padding: 5px" title="Delete">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>';

							if (!($query[$j]['f_bon_cancel'] == 'f' and $query[$j]['d_approve'] == null)) {
								$html = '';
							}

							echo $html;

							/* <!-- kondisi cetak, jika f_printed = false --> */
							$url_cetak = base_url() . "index.php/bonmkeluar/cform/printstb?id=" .  $query[$j]['id'];
							$html = '<a href="' . $url_cetak . '" class="btn btn-sm btn-warning glyphicon glyphicon-print print2" target="_blank" title="Cetak" onclick="return stbPrint()"></a>';

							if (!($query[$j]['f_printed'] == 'f' && $query[$j]['d_approve'] != null)) {
								$html = '';
							}

							echo $html;

							/* <!-- kondisi approve --> */
							$url_approve = base_url() . "index.php/bonmkeluar/cform/approve/" . $query[$j]['id'] . "/" . $cur_page . "/" . $is_cari . "/" . $tgl_awal . "/" . $tgl_akhir . "/" . $cgudang . "/" . $ctujuan . "/" . $xcari . "/" . $xcaribrg . "/" . $filterbrg . " \"";
							// $html = '<a href="' . $url_approve . '">Approve</a>';
							$html = '<a href="' . $url_approve . '" class="btn btn-sm btn-primary" style="padding: 5px" title="Approve">
										<span class="glyphicon glyphicon-ok"></span>
									</a>';

							$validApproval = $this->session->userdata('uid') == $query[$j]['uid_approve'];
							if (!($query[$j]['d_approve'] == null && $query[$j]['d_stb_receive'] == null
								&& $query[$j]['f_bon_cancel'] == 'f' && $query[$j]['d_notapprove'] == null
								&& $validApproval
							)) {
								$html = '';
							}

							echo $html;

							/* <!-- kondisi terima --> */
							$url_terima = base_url() . "index.php/bonmkeluar/cform/accept/" . $query[$j]['id'];
							$html = '<a href="' . $url_terima . '"
										class="btn btn-sm btn-success" style="padding: 5px" title="Terima">
										<span class="glyphicon glyphicon-share"></span>
									</a>';

							if (!($query[$j]['d_approve'] != null and $query[$j]['d_stb_receive'] == null)) {
								$html = '';
							}

							echo $html;

							echo "</td>";
						}
						echo  "</tr>";
					}
				}
				?>
			</tbody>
		</table><br>
	</form>
	<?php echo $this->pagination->create_links(); ?>
</div>