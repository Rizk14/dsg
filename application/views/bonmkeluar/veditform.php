<h3>Data Pengeluaran Bahan Baku/Pembantu</h3><br>
<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/bonmkeluar/cform/view">View Data</a>&nbsp;<br><br>

<?php if ($msg != '') echo "<i>" . $msg . "</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>

<script>
	//tambah
	$(function() {
		//$("#no").val('2');

		//generate_nomor();

		$("#addrow").click(function() {
			//inisialisasi untuk id unik
			var no = $("#no").val();
			var n = parseInt(no) - 1;
			//copy last row
			var lastRow = $("#tabelku tr:last").clone();
			//----------------SETTING KONDISI ROW BARU*****
			//******no*************************************
			var num = "#num_" + n;
			var new_num = "#num_" + no;
			$(num, lastRow).attr("id", "num_" + no);
			$(new_num, lastRow).html(no);
			//******end no*********************************

			//*****is_quilting*************************************
			/*	var is_quilting="#is_quilting_"+n;
				var new_is_quilting="#is_quilting_"+no;
				$(is_quilting, lastRow).attr("id", "is_quilting_"+no);
				$(new_is_quilting, lastRow).attr("name", "is_quilting_"+no);		
				$(new_is_quilting, lastRow).val('t');
				$(new_is_quilting, lastRow).attr("checked", false); */
			//*****end is_quilting*********************************

			//*****untuk_bisbisan*************************************
			var untuk_bisbisan = "#untuk_bisbisan_" + n;
			var new_untuk_bisbisan = "#untuk_bisbisan_" + no;
			$(untuk_bisbisan, lastRow).attr("id", "untuk_bisbisan_" + no);
			$(new_untuk_bisbisan, lastRow).attr("name", "untuk_bisbisan_" + no);
			$(new_untuk_bisbisan, lastRow).val('t');
			$(new_untuk_bisbisan, lastRow).attr("checked", false);
			//*****end untuk_bisbisan*********************************

			//*****kode*************************************
			var kode = "#kode_" + n;
			var new_kode = "#kode_" + no;
			$(kode, lastRow).attr("id", "kode_" + no);
			$(new_kode, lastRow).attr("name", "kode_" + no);
			$(new_kode, lastRow).val('');
			//*****end kode*********************************

			//*****id_brg*************************************
			var id_brg = "#id_brg_" + n;
			var new_id_brg = "#id_brg_" + no;
			$(id_brg, lastRow).attr("id", "id_brg_" + no);
			$(new_id_brg, lastRow).attr("name", "id_brg_" + no);
			$(new_id_brg, lastRow).val('');
			//*****end id_brg*********************************

			//*****nama*************************************
			var nama = "#nama_" + n;
			var new_nama = "#nama_" + no;
			$(nama, lastRow).attr("id", "nama_" + no);
			$(new_nama, lastRow).attr("name", "nama_" + no);
			$(new_nama, lastRow).val('');
			//*****end nama*************************************

			//*****satuan*************************************
			var satuan = "#satuan_" + n;
			var new_satuan = "#satuan_" + no;
			$(satuan, lastRow).attr("id", "satuan_" + no);
			$(new_satuan, lastRow).attr("name", "satuan_" + no);
			$(new_satuan, lastRow).val('');
			//*****end satuan*************************************	

			//*****tsatuan_konv*************************************
			var tsatuan_konv = "#tsatuan_konv_" + n;
			var new_tsatuan_konv = "#tsatuan_konv_" + no;
			$(tsatuan_konv, lastRow).attr("id", "tsatuan_konv_" + no);
			$(new_tsatuan_konv, lastRow).attr("name", "tsatuan_konv_" + no);
			$(new_tsatuan_konv, lastRow).val('0');
			//*****end tsatuan_konv*************************************	

			//*****satuan_konv*************************************
			var satuan_konv = "#satuan_konv_" + n;
			var new_satuan_konv = "#satuan_konv_" + no;
			$(satuan_konv, lastRow).attr("id", "satuan_konv_" + no);
			$(new_satuan_konv, lastRow).attr("name", "satuan_konv_" + no);
			$(new_satuan_konv, lastRow).val('0');
			//*****end satuan_konv*************************************	

			//*****rumus_konv*************************************
			var rumus_konv = "#rumus_konv_" + n;
			var new_rumus_konv = "#rumus_konv_" + no;
			$(rumus_konv, lastRow).attr("id", "rumus_konv_" + no);
			$(new_rumus_konv, lastRow).attr("name", "rumus_konv_" + no);
			$(new_rumus_konv, lastRow).val('0');
			//*****end rumus_konv*************************************	

			//*****angka_faktor_konversi*************************************
			var angka_faktor_konversi = "#angka_faktor_konversi_" + n;
			var new_angka_faktor_konversi = "#angka_faktor_konversi_" + no;
			$(angka_faktor_konversi, lastRow).attr("id", "angka_faktor_konversi_" + no);
			$(new_angka_faktor_konversi, lastRow).attr("name", "angka_faktor_konversi_" + no);
			$(new_angka_faktor_konversi, lastRow).val('0');
			//*****end angka_faktor_konversi*************************************	

			//*****stok*************************************
			var stok = "#stok_" + n;
			var new_stok = "#stok_" + no;
			$(stok, lastRow).attr("id", "stok_" + no);
			$(new_stok, lastRow).attr("name", "stok_" + no);
			$(new_stok, lastRow).val('');
			//*****end stok*************************************	

			//*****qty_satawal*************************************
			var qty_satawal = "#qty_satawal_" + n;
			var new_qty_satawal = "#qty_satawal_" + no;
			$(qty_satawal, lastRow).attr("id", "qty_satawal_" + no);
			$(new_qty_satawal, lastRow).attr("name", "qty_satawal_" + no);
			//$(new_qty_satawal, lastRow).attr("onkeyup", "javascript: var x= $('#satuan_konv_"+no+"').val(); if (x == 0) { $('#qty_"+no+"').val(this.value); }");
			// onkeyup="javascript: var x= $('#satuan_konv_1').val(); if (x == 0) { $('#qty_1').val(this.value); }"
			$(new_qty, lastRow).attr("readonly", true);
			$(new_qty_satawal, lastRow).val('0');
			//*****end qty_satawal*************************************	

			//*****qty*************************************
			var qty = "#qty_" + n;
			var new_qty = "#qty_" + no;
			$(qty, lastRow).attr("id", "qty_" + no);
			$(new_qty, lastRow).attr("name", "qty_" + no);
			$(new_qty, lastRow).attr("onkeyup", "javascript: var rumus= $('#rumus_konv_" + no + "').val(); var angka= $('#angka_faktor_konversi_" + no + "').val(); " +
				"if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_" + no + "').val(qtynya); } " +
				"else { $('#qty_satawal_" + no + "').val(this.value); }");
			/*
		onkeyup="javascript: var rumus= $('#rumus_konv_1').val(); var angka= $('#angka_faktor_konversi_1').val(); 
          if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); $('#qty_1').val(qtynya); } 
          else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); $('#qty_1').val(qtynya); }
          else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); $('#qty_1').val(qtynya); }
          else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); $('#qty_1').val(qtynya); }
          else { $('#qty_1').val(this.value); }"
		*/
			$(new_qty, lastRow).val('0');

			//*****end qty*************************************	

			//*****ket_detail*************************************
			var ket_detail = "#ket_detail_" + n;
			var new_ket_detail = "#ket_detail_" + no;
			$(ket_detail, lastRow).attr("id", "ket_detail_" + no);
			$(new_ket_detail, lastRow).attr("name", "ket_detail_" + no);
			$(new_ket_detail, lastRow).val('');
			//*****end ket_detail*************************************	

			//*****id_detail*************************************
			var id_detail = "#id_detail_" + n;
			var new_id_detail = "#id_detail_" + no;
			$(id_detail, lastRow).attr("id", "id_detail_" + no);
			$(new_id_detail, lastRow).attr("name", "id_detail_" + no);
			$(new_id_detail, lastRow).val('n');
			//*****end id_detail*********************************

			//*****qty_lama*************************************
			var qty_lama = "#qty_lama_" + n;
			var new_qty_lama = "#qty_lama_" + no;
			$(qty_lama, lastRow).attr("id", "qty_lama_" + no);
			$(new_qty_lama, lastRow).attr("name", "qty_lama_" + no);
			$(new_qty_lama, lastRow).val('0');
			//*****end qty_lama*********************************

			//*****id_brg_lama*************************************
			var id_brg_lama = "#id_brg_lama_" + n;
			var new_id_brg_lama = "#id_brg_lama_" + no;
			$(id_brg_lama, lastRow).attr("id", "id_brg_lama_" + no);
			$(new_id_brg_lama, lastRow).attr("name", "id_brg_lama_" + no);
			$(new_id_brg_lama, lastRow).val('0');
			//*****end id_brg_lama*********************************

			//button pilih*****************************************
			var pilih = "#pilih_" + n;
			var new_pilih = "#pilih_" + no;
			$(pilih, lastRow).attr("id", "pilih_" + no);
			var nama_for_even = "document.f_purchase.iddata.value=" + no;

			var even_klik = "var x= $('#gudang').val(); var q='f'; openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg/'+ x+'/'+q+'/" + no + "');";

			$(new_pilih, lastRow).attr("name", "pilih_" + no);
			$(new_pilih, lastRow).attr("onmouseover", nama_for_even);
			$(new_pilih, lastRow).attr("onclick", even_klik);
			//end button pilih		

			// 04-11-2015
			//*****kode_brg_wip*************************************
			var kode_brg_wip = "#kode_brg_wip_" + n;
			var new_kode_brg_wip = "#kode_brg_wip_" + no;
			$(kode_brg_wip, lastRow).attr("id", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("name", "kode_brg_wip_" + no);
			$(new_kode_brg_wip, lastRow).attr("onkeyup", "cari('" + no + "', this.value);");
			$(new_kode_brg_wip, lastRow).val('');
			//*****end kode_brg_wip*********************************

			//******div infobrgwip*************************************
			var infobrgwip = "#infobrgwip_" + n;
			var new_infobrgwip = "#infobrgwip_" + no;
			$(infobrgwip, lastRow).attr("id", "infobrgwip_" + no);

			$(new_infobrgwip, lastRow).html("<input type='hidden' id='id_brg_wip_" + no + "' name='id_brg_wip_" + no + "' value=''>");

			//----------------END SETTING KONDISI ROW BARU*		
			//tambahin row nya sesuai settingan
			$("#tabelku").append(lastRow);
			//no++
			var x = parseInt(no);
			$("#no").val(x + 1);

		});

		$("#deleterow").click(function() {
			var x = $("#no").val();
			var jumawal = $("#jumawal").val();
			// kita rubah supaya boleh hapus semuaa
			//if (parseInt(x) > parseInt(jumawal)) {
			if (x > 2) {
				$("#tabelku tr:last").remove();
				var x = $("#no").val();
				var y = x - 1;
				$("#no").val(y);
			}
		});

	});
</script>
<script type="text/javascript">
	function openCenteredWindow(url) {

		var width = 680;
		var height = 480;
		var left = parseInt((screen.availWidth / 2) - (width / 2));
		var top = parseInt((screen.availHeight / 2) - (height / 2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

	function cek_bonm() {
		var no_bonm = $('#no_bonm_manual').val();
		var tgl_bonm = $('#tgl_bonm').val();
		var s = 0;
		kon = window.confirm("Yakin akan simpan data ini ??");

		if (kon) {
			if (no_bonm == '') {
				alert("Nomor Bon M manual harus diisi..!");
				s = 1;
				return false;
			}
			if (tgl_bonm == '') {
				alert("Tanggal Bon M harus dipilih..!");
				s = 1;
				return false;
			}

			var jum = $('#no').val() - 1;

			if (jum > 0) {
				for (var k = 1; k <= jum; k++) {

					if ($('#kode_' + k).val() == '') {
						alert("Data item bahan baku/pembantu tidak boleh ada yang kosong...!");
						s = 1;
						return false;
					}
					if ($('#qty_' + k).val() == '0' || $('#qty_' + k).val() == '') {
						alert("Data qty tidak boleh 0 / kosong...!");
						s = 1;
						return false;
					}
					if (isNaN($('#qty_' + k).val())) {
						alert("Qty harus berupa angka..!");
						s = 1;
						return false;
					}
					/*if ($('#id_brg_wip_'+k).val() == '') {
						alert("Kode barang WIP harus diisi..!");
						s = 1;
						return false;
					} */
					/*if ($('#stok_'+k).val() - $('#qty_'+k).val() < 0 ) {
						alert("Pengeluaran barang "+$('#nama_'+k).val()+" melebihi stok...!");
						s = 1;
						return false;
					} */

				}
			} else {
				alert("Data detail tidak ada");
				s = 1;
				return false;
			}

			if (s == 0)
				return true;
		} // end if kon
		else
			return false;
	}

	function cari(posisi, kodebrgwip) {
		$.ajax({
			type: 'post',
			url: '<?php echo base_url(); ?>index.php/bonmkeluar/cform/caribrgwip',
			data: 'kode_brg_wip=' + kodebrgwip + '&posisi=' + posisi,
			success: function(response) {
				$("#infobrgwip_" + posisi).html(response);
			}
		});
	}

	// 08-09-2015
	function cek_itemnya() {
		var xx = 0;
		var jumdetail = $('#no').val();
		if (jumdetail > 0) {
			for (var k = 1; k <= jumdetail; k++) {
				//	$('input[name=cek_'+k+']').attr('checked', true);
				if ($("#cek_" + k).is(":checked")) {
					xx = xx + 1;
				}
			}
		}

		var kon = window.confirm("yakin akan menghapus item barangnya ??");
		if (kon) {
			if (xx == 0) {
				alert('item2 barangnya hrs dipilih !!');
				return false;
			} else
				return true;
		} else {
			return false;
		}
	}
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/bonmkeluar/cform/updatedata" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id_bonm" value="<?php echo $query[0]['id'] ?>">
	<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
	<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
	<input type="hidden" name="tgl_awal" value="<?php echo $tgl_awal ?>">
	<input type="hidden" name="tgl_akhir" value="<?php echo $tgl_akhir ?>">
	<input type="hidden" name="cgudang" value="<?php echo $cgudang ?>">
	<input type="hidden" name="ctujuan" value="<?php echo $ctujuan ?>">
	<input type="hidden" name="carinya" value="<?php echo $carinya ?>">
	<input type="hidden" name="caribrg" value="<?php echo $caribrg ?>">
	<input type="hidden" name="filterbrg" value="<?php echo $filterbrg ?>">

	<?php $detail_bonm = $query[0]['detail_bonm'];
	$no = 1;
	$jumawal = count($detail_bonm) + 1;

	foreach ($detail_bonm as $hitung) {
		$no++;
	}
	?>
	<center>Edit Data</center>

	<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
	<input type="hidden" name="jumawal" id="jumawal" value="<?php echo $jumawal ?>">
	<input type="hidden" name="iddata" id="iddata" />
	<div align="center">
		<label id="status"></label>
		<br>

		<table class="proit-view" width="100%" cellspacing="2" cellpadding="1">
			<tr>
				<td width="15%">Lokasi Gudang</td>
				<td> <select name="gudang" id="gudang" disabled>
						<?php foreach ($list_gudang as $gud) { ?>
							<option value="<?php echo $gud->id ?>" <?php if ($query[0]['id_gudang'] == $gud->id) { ?> selected <?php } ?>><?php echo "[" . $gud->nama_lokasi . "] " . $gud->kode_gudang . "-" . $gud->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td width="15%">Nomor Bon M / SJ</td>
				<td>
					<input name="no_bonm" type="text" id="no_bonm" size="10" maxlength="20" value="<?php echo $query[0]['no_bonm'] ?>" readonly="true">
					&nbsp; Nomor Manual <input name="no_bonm_manual" type="text" id="no_bonm_manual" size="15" maxlength="15" value="<?php echo $query[0]['no_manual'] ?>">
				</td>

			</tr>
			<tr>
				<td>Tanggal Bon M / SJ</td>
				<td>
					<label>
						<input name="tgl_bonm" type="text" id="tgl_bonm" size="10" value="<?php echo $query[0]['tgl_bonm'] ?>" readonly="true">
					</label>
					<img alt="" id="tgl_bonm" align="middle" title="Pilih tgl.." src="<?php echo base_url(); ?>images/calendar.gif" onclick="displayCalendar(document.forms[0].tgl_bonm,'dd-mm-yyyy',this)">
				</td>
			</tr>

			<tr>
				<td>Dari</td>
				<td>
					<input name="esender" type="text" id="esender" value="<?php echo $query[0]['e_sender'] ?>" placeholder="Nama Penyerah" maxlength="125"> /
					<input style="width:200px" name="esendercompany" type="text" id="esendercompany" maxlength="125" value="<?php echo $query[0]['e_sender_company'] ?>">
				</td>
			</tr>

			<tr>
				<td>Untuk</td>
				<td>
					<input name="erecipient" type="text" id="erecipient" size="10" maxlength="125" value="<?php echo $query[0]['e_recipient'] ?>" placeholder="Nama Penerima"> /
					<input style="width:200px" name="erecipientcompany" type="text" id="erecipientcompany" placeholder="Nama Perusahaan/Departemen" maxlength="125" value="<?php echo $query[0]['e_recipient_company'] ?>">
				</td>
			</tr>

			<tr>
				<td>Tujuan</td>
				<td> <select name="tujuan" id="tujuan">
						<option value="1" <?php if ($query[0]['tujuan'] == '1') { ?> selected <?php } ?>>Cutting</option>
						<option value="2" <?php if ($query[0]['tujuan'] == '2') { ?> selected <?php } ?>>Unit</option>
						<option value="3" <?php if ($query[0]['tujuan'] == '3') { ?> selected <?php } ?>>Lain-Lain (Retur)</option>
						<option value="4" <?php if ($query[0]['tujuan'] == '4') { ?> selected <?php } ?>>Lain-Lain (Peminjaman)</option>
						<option value="5" <?php if ($query[0]['tujuan'] == '5') { ?> selected <?php } ?>>Lain-Lain (Lainnya)</option>
						<option value="6" <?php if ($query[0]['tujuan'] == '6') { ?> selected <?php } ?>>Pengadaan</option>
						<option value="7" <?php if ($query[0]['tujuan'] == '7') { ?> selected <?php } ?>>QC/WIP</option>
						<option value="8" <?php if ($query[0]['tujuan'] == '8') { ?> selected <?php } ?>>Makloon</option>
						<!--	<option value="6" <?php if ($query[0]['tujuan'] == '6') { ?> selected <?php } ?> >Pengadaan</option>
						<option value="7" <?php if ($query[0]['tujuan'] == '7') { ?> selected <?php } ?> >Quilting</option>
						<option value="8" <?php if ($query[0]['tujuan'] == '8') { ?> selected <?php } ?> >WIP</option>
						<option value="9" <?php if ($query[0]['tujuan'] == '9') { ?> selected <?php } ?> >RND</option>
						<option value="10" <?php if ($query[0]['tujuan'] == '10') { ?> selected <?php } ?> >BLN Kedungreja</option> -->
					</select>
				</td>
			</tr>

			<tr>
				<td>Unit Jahit</td>
				<td> <select name="id_unit_jahit" id="id_unit_jahit">
						<option value="0" <?php if ($query[0]['id_unit_jahit'] == '0') { ?> selected <?php } ?>>Tidak Ada</option>
						<?php foreach ($list_unit_jahit as $jht) { ?>
							<option <?php if ($query[0]['id_unit_jahit'] == $jht->id) { ?> selected <?php } ?> value="<?php echo $jht->id ?>"><?php echo $jht->kode_unit . "-" . $jht->nama ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>

			<tr>
				<td>Keterangan</td>
				<td>
					<input name="ket" type="text" id="ket" size="30" value="<?php if ($query[0]['keterangan'] != 'DIBEBANKAN') echo $query[0]['keterangan'];
																			else echo ''; ?>">&nbsp;<input type="checkbox" <?php if ($query[0]['keterangan'] == 'DIBEBANKAN') { ?>checked="true" <?php } ?> name="dibebankan" id="dibebankan" value="DIBEBANKAN">Dibebankan
				</td>
			</tr>

			<!--  <tr>
	<td colspan="2"><i>*) Untuk bahan yg satuannya Yard, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Meter (kecuali quilting, tetap Yard)<br>
	*) Untuk bahan yg satuannya Lusin, maka Qty yg dikeluarkan itu <b>otomatis</b> dlm bentuk Pieces<br>
	*) Untuk bahan lain yg memerlukan konversi satuan, silahkan pilih di kolom Satuan Konversi<br></i></td>
  </tr> -->

			<tr>
				<td colspan="2"><br>
					<form name="myform">
						<table id="tabelku" border="0" align="center" cellpadding="1" cellspacing="2" class="proit-view">
							<tr>
								<td colspan="8" align="left">
									<input id="addrow" type="button" name="addrow" value=" + " title="Tambah Item Barang">&nbsp;
									<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
								</td>
							</tr>
							<tr>
								<th width="20">No</th>
								<!--<th>Kelompok Brg</th> -->
								<!--  <th>Quilting</th> -->
								<th>Kode & Nama Bhn Baku/Pembantu</th>
								<th>Satuan Awal</th>
								<th>Satuan Konversi</th>
								<th>Qty<br>Sat Awal</th>
								<th>Qty<br>Sat Konv</th>
								<th>Acuan Barang<br>WIP</th>
								<th>Keterangan</th>
								<th>Pilih Utk Hapus</th>
							</tr>
							<?php $i = 1;
							if (count($query[0]['detail_bonm']) == 0) {
							?>
								<tr align="center">
									<td align="center" id="num_1">1</td>
									<td nowrap="nowrap">
										Data tidak ada</td>

								</tr>
								<?php
							} else {
								$detail_bonm = $query[0]['detail_bonm'];
								for ($k = 0; $k < count($detail_bonm); $k++) {
								?>

									<tr align="center">
										<td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>

										<!--  <td>
			<input type="checkbox" name="is_quilting_<?php echo $i ?>" id="is_quilting_<?php echo $i ?>" value="t" <?php //if ($detail_bonm[$k]['is_quilting'] == 't') { 
																													?> checked <?php //} 
																																?> >
		  </td> -->

										<td style="white-space:nowrap;">
											<input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="40" readonly="true" value="<?php echo $detail_bonm[$k]['kode_brg'] . " - " . str_replace("\"", "&quot;", $detail_bonm[$k]['nama']) ?>" />
											<input name="kode_<?php echo $i ?>" type="hidden" id="kode_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['kode_brg'] ?>" />
											<input name="id_brg_<?php echo $i ?>" type="hidden" id="id_brg_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['id_brg'] ?>" />
											<input name="stok_<?php echo $i ?>" type="hidden" id="stok_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['jum_stok'] ?>" />
											<input name="id_satuan_<?php echo $i ?>" type="hidden" id="id_satuan_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['id_satuan'] ?>" />
											<input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" onclick="javascript: var x= $('#gudang').val(); var q='f';
            openCenteredWindow('<?php echo base_url(); ?>index.php/bonmkeluar/cform/show_popup_brg/'+ x+'/'+q+'/<?php echo $i ?>');">
										</td>
										<td><input name="satuan_<?php echo $i ?>" type="text" id="satuan_<?php echo $i ?>" size="5" readonly="true" value="<?php echo $detail_bonm[$k]['satuan'] ?>" /></td>

										<td>
											<select name="tsatuan_konv_<?php echo $i ?>" id="tsatuan_konv_<?php echo $i ?>" disabled>
												<option value="0" <?php if ($detail_bonm[$k]['id_satuan_konversi'] == '0') { ?>selected<?php } ?>>Tidak Ada</option>
												<?php foreach ($list_satuan as $sat) { ?>
													<option value="<?php echo $sat->id ?>" <?php if ($detail_bonm[$k]['id_satuan_konversi'] == $sat->id) { ?>selected<?php } ?>><?php echo $sat->nama ?></option>
												<?php } ?>
											</select>

											<input type="hidden" name="satuan_konv_<?php echo $i ?>" id="satuan_konv_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['id_satuan_konversi'] ?>">
											<input type="hidden" name="rumus_konv_<?php echo $i ?>" id="rumus_konv_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['rumus_konv'] ?>">
											<input type="hidden" name="angka_faktor_konversi_<?php echo $i ?>" id="angka_faktor_konversi_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['angka_faktor_konversi'] ?>">
										</td>

										<td><input style="text-align:right;" name="qty_satawal_<?php echo $i ?>" type="text" id="qty_satawal_<?php echo $i ?>" size="3" value="<?php echo $detail_bonm[$k]['qty_satawal'] ?>" readonly="true" /></td>

										<td><input style="text-align:right;" name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="3" value="<?php echo $detail_bonm[$k]['qty'] ?>" onkeyup="javascript: var rumus= $('#rumus_konv_<?php echo $i ?>').val(); var angka= $('#angka_faktor_konversi_<?php echo $i ?>').val(); 
          if (rumus == '1') { var qtynya= parseFloat(this.value)/parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_<?php echo $i ?>').val(qtynya); } 
          else if (rumus == '2') { var qtynya= parseFloat(this.value)*parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_<?php echo $i ?>').val(qtynya); }
          else if (rumus == '3') { var qtynya= parseFloat(this.value)-parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_<?php echo $i ?>').val(qtynya); }
          else if (rumus == '4') { var qtynya= parseFloat(this.value)+parseFloat(angka); qtynya = qtynya.toFixed(2); $('#qty_satawal_<?php echo $i ?>').val(qtynya); }
          else { $('#qty_satawal_<?php echo $i ?>').val(this.value); }" /></td>

										<td style="white-space:nowrap;"><input name="kode_brg_wip_<?php echo $i ?>" type="text" id="kode_brg_wip_<?php echo $i ?>" size="10" value="<?php echo $detail_bonm[$k]['kode_brg_wip'] ?>" onkeyup="cari('<?php echo $i ?>',this.value);" /><br>
											<div id="infobrgwip_<?php echo $i ?>"><?php echo $detail_bonm[$k]['nama_brg_wip'] ?><input name="id_brg_wip_<?php echo $i ?>" type="hidden" id="id_brg_wip_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['id_brg_wip'] ?>" /></div>
										</td>

										<td style="white-space:nowrap;"><input name="ket_detail_<?php echo $i ?>" type="text" id="ket_detail_<?php echo $i ?>" size="20" value="<?php echo $detail_bonm[$k]['keterangan'] ?>" /> <input type="checkbox" name="untuk_bisbisan_<?php echo $i ?>" id="untuk_bisbisan_<?php echo $i ?>" value="t" <?php if ($detail_bonm[$k]['untuk_bisbisan'] == 't') { ?>checked="true" <?php } ?>>Untuk Makloon Bisbisan</td>

										<td><input name="cek_<?php echo $i; ?>" id="cek_<?php echo $i; ?>" type="checkbox" value="y" />
											<input type="hidden" name="id_detail_<?php echo $i ?>" id="id_detail_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['id'] ?>">
											<input type="hidden" name="qty_lama_<?php echo $i ?>" id="qty_lama_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['qty'] ?>">
											<input type="hidden" name="id_brg_lama_<?php echo $i ?>" id="id_brg_lama_<?php echo $i ?>" value="<?php echo $detail_bonm[$k]['id_brg'] ?>">
										</td>
									</tr>
							<?php $i++;
								} // end foreach 
							}
							?>
						</table>

					</form>
					<div align="center"><br>
						<?php
						if ($carinya == '') $carinya = "all";
						if ($is_cari == 0)
							$url_redirectnya = "bonmkeluar/cform/view/index/" . $cur_page;
						else
							$url_redirectnya = "bonmkeluar/cform/cari/" . $tgl_awal . "/" . $tgl_akhir . "/" . $cgudang . "/" . $ctujuan . "/" . $carinya . "/" . $caribrg . "/" . $filterbrg . "/" . $cur_page;
						?>
						<input type="submit" name="submit2" value="Simpan" onclick="return cek_bonm();">&nbsp;
						<input type="submit" name="submit3" value="Hapus Item Yg Dipilih" onclick="return cek_itemnya();">&nbsp;
						<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">

					</div>
				</td>
			</tr>

		</table>
	</div>
</form>