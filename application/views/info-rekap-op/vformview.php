<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 850;
		var height = 550;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function()
{
	$(".pilih").click(function()
	{
		var id_op_print=$("#id_op_print").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/op/cform/print_op/"+id_op_print;
		openCenteredWindow(urlnya);
	});
	
	$(".pilihgudang").click(function()
	{
		var id_op_print=$("#id_op_print").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/op/cform/print_op_nonharga/"+id_op_print;
		openCenteredWindow(urlnya);
	});
});


</script>

<h3>Data Rekap Order Pembelian (OP)</h3><br> 


<div>
Periode Tanggal = <?php echo $date_from ?> s/d <?php echo $date_to?> <br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_pembelian', 'id' => 'f_pembelian');
echo form_open('info-rekap-op/cform/export_excel', $attributes); ?>
<input type="hidden" name="jenis_beli" value="<?php echo $jenis_beli ?>" >
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="id_supplier" value="<?php echo $id_supplier ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br> </br>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_op_print" id="id_op_print">


<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No PP</th>
		 <th>No OP</th>
		 <th>Tgl OP</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Satuan</th>
		<!-- 25-06-2015 GA DIPAKE <th>Satuan Lain</th> -->
		 <th>Qty</th>
		 <th>Harga (Rp.)</th>
		 <th>Diskon (Rp.)</th>
		 <th>Subtotal (Rp.)</th>
		 <th>Grandtotal (Rp.)</th>
		 <th>Qty Pemenuhan SJ</th>
		 <th>Keterangan SJ</th>
		 <th>Qty Sisa</th>
		 <th>Status</th>
		 
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				$qstatusop	= $this->mmaster->statusfakturop($query[$j]['id']);
				$jstatusop	= $qstatusop->num_rows();
				if($jstatusop>0) {
					$jmlop	= $jstatusop;
				} else {
					$jmlop	= 0;
				}
				 
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				$pisah1 = explode("-", $query[$j]['tgl_op']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_op = $tgl1." ".$nama_bln." ".$thn1;
				
				if ($query[$j]['jenis_pembelian'] == '1')
					$jenis = "Cash";
				else if ($query[$j]['jenis_pembelian'] == '2')
					$jenis = "Kredit";
				else
					$jenis = "-";
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_pp']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['no_op']." (".$jenis.")</td>";
				 echo    "<td>".$tgl_op."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					// $total_qty =0;
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['qty'],0,',','.');
						  //	$total_qty += $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 // 22-09-2014
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo 
						  number_format( $var_detail[$k]['harga'],2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 // 02-07-2015
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						   number_format( $var_detail[$k]['diskon'],2,',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['subtotal'], 2, ',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
					echo number_format($query[$j]['grandtotal'], 2, ',','.');
				 echo "</td>";
				 
				  
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_beli'],2,'.','');
						 
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  if ($var_detail[$k]['count_beli'] > 1)
							 echo " (".$var_detail[$k]['count_beli']." SJ)";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['sisanya'],2,'.','');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td>";
				 if ($query[$j]['status_aktif'] == 't') echo "Aktif"; else echo "Non-Aktif";
				 echo "</td>";
				 
				// $total_pembantu += $query[$j]['jum_total_pembantu'];
				// $total_semua += $totalnya; 		 
				 echo  "</tr>";
		 	}
		   }
		 ?>
		 <tr>
			<?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
		if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					 for($k=0;$k<count($var_detail); $k++){
											$total_harga[]=$var_detail[$k]['harga'];
											$total_qty[]=$var_detail[$k]['qty'];
											$total_subtotal[]=$var_detail[$k]['subtotal'];
											$total_grandtotal[]= ($query[$j]['grandtotal']);
		  $totalharga = array_sum($total_harga);
		  
		  $totalqty = array_sum($total_qty);
		  $totalsubtotal = array_sum($total_subtotal);
		  $totalgrandtotal = array_sum($total_grandtotal);								
			}
		}
	}
}	
			 ?>
			<td>&nbsp;</td>
			<td colspan='5'><b>TOTAL</b></td>
			<?php 
			if ((isset($totalqty)) ||(isset($totalharga))){
		 echo    "<td align='right'>".number_format($totalqty,0,',','.')."</td>";
		// echo    "<td align='right'>".number_format($totalharga,4,',','.')."</td>";
	}
			?>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<?php
			 if ((isset($totalgrandtotal))){
			 echo    "<td align='right' colspan='2'>".number_format($totalsubtotal,2,',','.')."</td>";
		 }
			?>
			
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			
			
		 </tr>
		 </tr>
		 </tr>
		 </tr>
 	</tbody>
</table><br>
</form>
</div>
