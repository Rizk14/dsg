
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#tgl_kontrabon").datepicker();
});

function cknokontrabon(nomor) {

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('kontrabon/cform/carinokontrabon');?>",
	data:"nokontrabon="+nomor,
	success: function(data) {
		$("#confnokontrabon").html(data);
	},
	
	error:function(XMLHttpRequest) {
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function konfirm() { 
	
	kontrabon = document.getElementById('no_kontrabon').value;
	
	if(kontrabon.length<9) {
		alert('Nomor Kontra Bon hrs diisi (9 digit angka) !');
		document.getElementById('no_kontrabon').focus();
		return false;
	}
	
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_kontrabon; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_kontrabon; ?></td></tr>
   <tr>
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php
		
		 $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('kontrabon/cform/simpan', $attributes);?>
	
		<div id="masterlkontrabonform">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_kontrabon_no_faktur; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="no_faktur" type="text" id="no_faktur" maxlength="14" value="<?=$nofaktur?>" />
					</td>
					<td><?php echo $list_kontrabon_no_kontrabon; ?> : <input name="no_kontrabon" type="text" id="no_kontrabon" maxlength="9" value="<?=$no?>" onkeyup="cknokontrabon(this.value);" /></td>
				  </tr>
				  <tr>
					<td width="20%"><?php echo $list_kontrabon_tgl_kontrabon; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="tgl_kontrabon" type="text" id="tgl_kontrabon" maxlength="10" value="<?php echo date("d/m/Y"); ?>" onclick="displayCalendar(document.forms[0].tgl_kontrabon,'dd/mm/yyyy',this)"/></td>
					</td>
					<td><div id="confnokontrabon" style="color:#FF0000;"></div></td>
				  </tr>

				  <tr>
					<td width="20%"><?php echo $list_kontrabon_nota_sederhana; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="f_nota_sederhana" type="checkbox" id="f_nota_sederhana" value="<?=$tf_nota_sederhana?>" <?=$checked?> disabled />
					  <input name="tf_nota_sederhana" type="hidden" id="tf_nota_sederhana" value="<?=$tf_nota_sederhana?>" />
					</td>
					<td></td>
				  </tr>
				  				  
				</table></td>
			  </tr>
			  
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_kontrabon; ?></div></td>
			</tr>
			<tr>
			  <tr>
				<td><table width="85%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5%" class="tdatahead">NO</td>
					<td width="10%" class="tdatahead"><?php echo $list_kontrabon_no_faktur; ?></td>
					<td width="12%" class="tdatahead"><?php echo $list_kontrabon_tgl_faktur; ?> </td>
					<td width="12%" class="tdatahead"><?php echo $list_kontrabon_tgl_tempo; ?> </td>
					<td width="30%" class="tdatahead"><?php echo $list_kontrabon_pelanggan; ?></td>
					<td width="30%" class="tdatahead"><?php echo "BEA METERAI" ?></td>
					<td width="13%" class="tdatahead"><?php echo $list_kontrabon_jml_tagih; ?></td>
				  </tr>
				  <?php
				  echo $query;
				  ?>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="left"><table width="85%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="70%" align="right"><?php echo $list_kontrabon_total_tagih; ?></td>
					<td width="1%">:</td>
					<td width="*%" align="right">
					  <input name="totalgrand" type="text" id="totalgrand" maxlength="50" value="<?php echo number_format($totalgrand,'2','.',','); ?>" style="text-align:right;" />
					  <input name="totalhidden" type="hidden" id="totalhidden" value="<?=$total?>" />
					  <input name="discounthidden" type="hidden" id="discounthidden" value="<?=$discount?>" />
					  <input name="nilai_ppnhidden" type="hidden" id="nilai_ppnhidden" value="<?=$nilai_ppn?>" />
					  <input name="materai" type="hidden" id="materai" value="<?=$nilai_ppn?>" />
					  <input name="totalgrandhidden" type="hidden" id="totalgrandhidden" value="<?=$totalgrand?>" />
					</td>
					</tr>
				</table></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">
				  <input name="btnsimpan" id="btnsimpan" value="<?php echo $button_simpan; ?>" type="submit" onclick="return konfirm();">&nbsp;	
				  <input name="btnkeluar" type="button" id="btnkeluar" value="<?php echo $button_keluar; ?>"  onclick="window.location='<?php echo base_url(); ?>index.php/kontrabon/cform/'"/> 
				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close()?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
