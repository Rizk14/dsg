<script language="javascript">

	function ck_fnota() {
		if(document.getElementById('f_nota_sederhana').checked==true) {
			document.getElementById('tf_nota_sederhana').value = 't';
		}else{
			document.getElementById('tf_nota_sederhana').value = 'f';
		}
	}

	function konfirm() {
		if(document.getElementById('no_faktur').value=='') {
			alert('Kolom Nomor Faktur hrs diisi !!');
			document.getElementById('no_faktur').focus();
			return false;
		}else{
			return true;
		}
	}	
</script>

<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_kontrabon; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_kontrabon; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		 $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('kontrabon/cform/carifaktur', $attributes);?>
		<div id="masterlkontrabonform">
		
			<table width="60%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="20%"><?php echo $list_kontrabon_no_faktur; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="no_faktur" type="text" id="no_faktur" maxlength="14" readonly />
					  <input type="hidden" name="i_faktur" id="i_faktur" />
					  <img name="img_i_fpenjualan_" src="<?php echo base_url();?>asset/theme/images/table.gif" border="0" title="Tampilkan Nomor Faktur" class="imgLink" align="absmiddle" onclick="infofakturpenjualan(document.getElementById('tf_nota_sederhana').value);">
					</td>
				  </tr>
				  <tr>
					<td width="20%"><?php echo $list_kontrabon_nota_sederhana; ?></td>
					<td width="1%">:</td>
					<td>
					  <input name="f_nota_sederhana" type="checkbox" id="f_nota_sederhana" value="f" onclick="ck_fnota();"/>
					  <input name="tf_nota_sederhana" type="hidden" id="tf_nota_sederhana" value="f" />
					</td>
				  </tr>
			  <tr>			  
				<td colspan="3">&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="3">
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" onclick="return konfirm();" />
				<input name="btnbatal" id="btnbatal" value="Batal" type="reset"  value="<?php echo $button_batal; ?>" onclick="window.location='<?php echo base_url(); ?>index.php/kontrabon/cform/'">

				</td>
			  </tr>
			</table>
	
			</div>
			<?php echo form_close(); ?>
			</td>
		  </tr> 
		</table>

   </td>
  </tr>
</table>
