<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function settextfield(a,b,c,d,e,f,h) {
	
   	ada=false;
	var g	= "<?=$brs?>";

  	for(i=0;i<=g;i++){
		if(
			(b==opener.document.getElementById("i_product_tblItem_"+i).value) &&  
			(a==opener.document.getElementById("i_do_tblItem_"+i).value)
		  ){
			alert ("kode Barang "+b+" sdh ada!");
			ada=true;
			break;
		}else{
			ada=false;
		}
    }
    
	if(!ada){
		opener.document.getElementById('i_do_tblItem_'+'<?php echo $iterasi; ?>').value=a;
		opener.document.getElementById('i_product_tblItem_'+'<?php echo $iterasi; ?>').value=b;
		opener.document.getElementById('e_product_name_tblItem_'+'<?php echo $iterasi; ?>').value=c;
		opener.document.getElementById('v_hjp_tblItem_'+'<?php echo $iterasi; ?>').value=e;
		opener.document.getElementById('n_quantity_tblItem_'+'<?php echo $iterasi; ?>').value=d;
		opener.document.getElementById('v_unit_price_tblItem_'+'<?php echo $iterasi; ?>').value=f;
		
		if(g==0){
			var tsplit	= h.split('/'); // d/m/Y
			var t	= parseInt(tsplit['0'])+37;
			var tanggal = new Date(tsplit['2'],tsplit['1'],t,0,0,0); 
			var tgl	= tanggal.getDate();
			var bln = tanggal.getMonth();
			var thn = tanggal.getFullYear();
		
			nilai = tgl+'/'+bln+'/'+thn;
			
			opener.document.getElementById('d_due_date').value=nilai;
			opener.document.getElementById('d_faktur').value=h;
			opener.document.getElementById('d_pajak').value=h;
		}
		
		opener.document.getElementById('i_do_tblItem_'+'<?php echo $iterasi; ?>').focus();
		this.close();
	}
}

function getInfo(txt,cab){

$.ajax({

type: "POST",
url: "<?php echo site_url('fakpenjualanpopup/cform/flistbarangjadi');?>",
data:"key="+txt+"&cab="+cab,
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};

function carifocus() {
	document.getElementById('txtcari').focus();
}
</script>

</head>
<BODY id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="getInfo(this.value,'<?=$ibranch?>');"/>
  	</td>
  </tr>
  <tr>
    <td align="left">
	<?php echo form_open('fakpenjualanpopup/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php
		if(!empty($isi) || isset($isi)) {
			
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			
			if(count($isi)) {
				
				$tanggal	= array();
				$cc	= 1;
				
				foreach($isi as $row) {
				
					$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:""; // dd mm YYYY
					
					$idocode	= trim($row->ido_code);
					$iproduct	= trim($row->iproduct);
					
					$tglfaktur	= $tgl[2]."/".$tgl[1]."/".$tgl[0];
					
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->ido_code."</a></td>	 
					  <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$tanggal[$cc]."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->iproduct."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->motifname."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$iproduct','$row->motifname','$row->qtyakhir','$row->price','$row->nilai','$tglfaktur')\">".$row->qtyakhir."</a></td>					  
					 </tr>";
					 
					 $cc+=1;
				}
			}
		}
		?>
		
		<table class="listtable2" border="1">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Nomor DO"); ?></th>
		 <th width="130px;"><?php echo strtoupper("Tgl. DO"); ?></th>
		 <th><?php echo strtoupper("Kode BRG"); ?></th>
		 <th><?php echo strtoupper("Nama Barang"); ?></th>
		 <th><?php echo strtoupper("Qty DO"); ?></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>"; ?>
  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
