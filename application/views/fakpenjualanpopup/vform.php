<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">

function getJatuhTempo(source,destination,range) { // d/m/Y

	var nilai;

	/*var tsplit	= source.split('/'); // d/m/Y
	var t	= parseInt(tsplit['0'])+parseInt(range);
	var tanggal = new Date(tsplit['2'],tsplit['1'],t,0,0,0); 
	var tgl	= tanggal.getDate();
	var bln = tanggal.getMonth();
	var thn = tanggal.getFullYear();

	nilai = tgl+'/'+bln+'/'+thn;
	destination.value	= nilai; */
	
	/*
	var firstDay = new Date("2009/06/25");
	var nextWeek = new Date(firstDay.getTime() + 7 * 24 * 60 * 60 * 1000);
	*/
	var tsplit	= source.split('/');
	var baru = tsplit['2']+'/'+tsplit['1']+'/'+tsplit['0'];
	var firstDay = new Date(baru);
	var test = firstDay.getTime() + parseInt(38 * 24 * 60 * 60 * 1000);
	var tanggal = new Date(test);
	var tgl	= tanggal.getDate();
	var bln = tanggal.getMonth()+1; // +1 karena getMonth itu startnya dari 0 (Januari), 1 (Februari), dst
	var thn = tanggal.getFullYear();

	nilai = tgl+'/'+bln+'/'+thn;
	destination.value	= nilai;
}

function tgpajak(tfaktur) {
	document.getElementById('d_pajak').value = tfaktur;
}

function konfirm() {
	
	var kon=window.confirm("yakin akan simpan ??");
	
	if(document.getElementById('v_total_nilai').value=='' || document.getElementById('v_total_nilai').value==0){
		alert('Total Nilai penjualan tdk boleh sama dgn nol!');
		return false;
	}
	
	if (kon){
		return true;
	}else{
		return false;
	}
}

function ckfpenjualan(nomor) {

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualanpopup/cform/cari_fpenjualan');?>",
	data:"fpenj="+nomor,
	success: function(data){
		$("#confnomorfpenj").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function ckfpajak(nomor) {

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualanpopup/cform/cari_fpajak');?>",
	data:"fpajak="+nomor,
	success: function(data){
		$("#confnomorfpajak").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};

function validNum(column,column2,iterasi) {
	var angka	= document.getElementById(column+'_'+iterasi);
	
	if(!parseInt(angka.value)) {
		alert("Maaf, Kolom Pengisian hrs angka!");
		angka.value	= document.getElementById(column2+'_'+iterasi).value;
		angka.focus();
	}
}

function tnilaibarang() {

	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseInt(unitprice.value);
		}
	}
	totalnilai.value	= totaln;
}

function test() {
	
	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var nilai	= totalnilai;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;	
	var val		= document.getElementById('n_discount');
	
	/*var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow; */
	var jumdetail = document.getElementById('jumdetail');
	var iteration = jumdetail.value;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseFloat(unitprice.value);
		}
	}
		
	totalnilai.value	= totaln;
	
	if(parseFloat(nilai.value)) {
		
		hasil 	= ( (parseFloat(nilai.value)*parseFloat(val.value)) / 100 ); // dikurangi discount
		total_sblm_ppn	= parseFloat(nilai.value) - hasil; // DPP
		nilai_ppn	= ( (total_sblm_ppn*11) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;
		
		document.getElementById('v_discount').value = Math.round(hasil);
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
		
	}else{

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseFloat(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*11) / 100 );
		total_grand	= xtotaln + nilai_ppn;
		
		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

function test2() {
	
	var totaln=0;
	var unitprice;
	var totalnilai	= document.getElementById('v_total_nilai');
	
	var nilai	= totalnilai;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;	
	
	var aa;

	var val		= document.getElementById('v_discount');
	
	var tbl = document.getElementById('tblItem');
	var lastRow = tbl.rows.length;
	var iteration = lastRow;
	
	for(c=0; c<iteration;c++) {
		unitprice  = document.getElementById('v_unit_price_tblItem_'+c);
		if(unitprice.value=='') {
			totaln += 0;
		} else {
			totaln += parseFloat(unitprice.value);
		}
	}
		
	totalnilai.value	= totaln;
	
	if(parseInt(nilai.value)) {

		aa	= (val.value / parseInt(nilai.value))*100;
		total_sblm_ppn	= parseInt(nilai.value) - val.value;
		nilai_ppn	= ( (total_sblm_ppn*11) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;

		if(aa>0 && val.value>0){
			document.getElementById('n_discount').value = Math.round(aa*Math.pow(10,2))/Math.pow(10,2);
		}else{
			document.getElementById('n_discount').value = 0;
		}
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*11) / 100 );
		total_grand	= xtotaln + nilai_ppn;

		document.getElementById('n_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

function total(iterasi) {
	var	total;
	var	price;
	var	unit0;
	
	price	= document.getElementById('v_hjp_tblItem_'+iterasi);
	unit0	= document.getElementById('n_quantity_tblItem_'+iterasi);
	
	if(parseFloat(price.value) && parseFloat(unit0.value)) {
		total	= parseFloat(price.value) * parseFloat(unit0.value);
		document.getElementById('v_unit_price_tblItem_'+iterasi).value=total;
	}
}

function getdsk() {
	
	var nilai;
	var val;
	var hasil;
	var total_sblm_ppn;
	var nilai_ppn;
	var total_grand;

	nilai	= document.getElementById('v_total_nilai');
	val		= document.getElementById('n_discount');
	
	if(parseInt(nilai.value)) {
		
		hasil 	= ( (parseInt(nilai.value)*val.value) / 100 );
		total_sblm_ppn	= parseInt(nilai.value) - hasil;
		nilai_ppn	= ( (total_sblm_ppn*11) / 100 );
		total_grand	= total_sblm_ppn + nilai_ppn;

		document.getElementById('v_discount').value = Math.round(hasil);
		document.getElementById('v_total_faktur').value	= Math.round(total_sblm_ppn);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	} else {

		var xtotaln=0;
		var xunitprice;
		var xtotalnilai	= document.getElementById('v_total_nilai');
	
		var tbl = document.getElementById('tblItem');
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		
		for(c=0; c<iteration;c++) {
			xunitprice  = document.getElementById('v_unit_price_tblItem_'+c);
			if(xunitprice.value=='') {
				xtotaln += 0;
			} else {
				xtotaln += parseInt(xunitprice.value);
			}
		}
		xtotalnilai.value	= xtotaln;
		
		nilai_ppn	= ( (xtotaln*11) / 100 );
		total_grand	= xtotaln + nilai_ppn;

		document.getElementById('v_discount').value = 0;
		document.getElementById('v_total_faktur').value	= Math.round(xtotaln);
		document.getElementById('n_ppn').value	= Math.round(nilai_ppn);
		document.getElementById('v_total_fppn').value	= Math.round(total_grand);		
	}
}

function ipajak(faktur){
	var ifaktur	= faktur;
	var ccc		= (ifaktur.length)-4;
	var ipajak	= ifaktur.substr(4,ccc);
	document.getElementById('i_faktur_pajak').value	= ipajak;

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('fakpenjualanpopup/cform/cari_fpajak');?>",
	data:"fpajak="+ipajak,
	success: function(data){
		$("#confnomorfpajak").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})	
}

function ipajakck(faktur){
	var ifaktur	= faktur;
	var ccc		= (ifaktur.length)-4;
	var ipajak	= ifaktur.substr(4,ccc);
	var cbox	= document.getElementById('cbox').value;
	
	if(document.getElementById('ckbox').checked==true){
		var satu	= ipajak;
		document.getElementById('i_faktur_pajak').value	= ipajak;	
	}else{
		var satu	= cbox;
		document.getElementById('i_faktur_pajak').value = cbox;
	}

	$.ajax({
		
	type: "POST",
	url: "<?php echo site_url('fakpenjualanpopup/cform/cari_fpajak');?>",
	data:"fpajak="+satu,
	success: function(data){
		$("#confnomorfpajak").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})		
}

/*
function addRowToTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;
	var brs = tbl.rows.length;
	var iteration = lastRow;
	var row = tbl.insertRow(lastRow);
	var cab	= document.getElementById('i_branch');
	
	if(iteration<22){
		tbl.width='100%';
	
		if(iteration%2==0){
			var bgColorValue = '#FFFFFF';
		}else{
			var bgColorValue = '#FFFFFF';
		}
		row.bgColor = bgColorValue;

		var cell1 = row.insertCell(0);
		cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:18px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

		var cell1 = row.insertCell(1);
		cell1.innerHTML = "<DIV ID=\"ajax_i_do_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"i_do_"+nItem+"_"+iteration+"\"  name=\"i_do_"+nItem+"_"+iteration+"\" style=\"width:82px;\" onfocus=\"total("+iteration+");test();\" readonly ><img name=\"img_i_do_\" src=\"<?php echo base_url();?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor DO\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukfpenjualanpopup('"+iteration+"','"+document.getElementById('i_branch').value+"','"+brs+"');\"></DIV>";

		var cell1 = row.insertCell(2);
		cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:120px;\" readonly ></DIV>";

		var cell1 = row.insertCell(3);
		cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:200px;\" readonly ></DIV>";
	
		var cell1 = row.insertCell(4);
		cell1.innerHTML = "<DIV ID=\"ajax_v_hjp_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"v_hjp_"+nItem+"_"+iteration+"\"  name=\"v_hjp_"+nItem+"_"+iteration+"\" style=\"width:90px;text-align:right;\" value=\"0\" readonly ></DIV>";

		var cell1 = row.insertCell(5);
		cell1.innerHTML = "<DIV ID=\"ajax_n_quantity_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"n_quantity_"+nItem+"_"+iteration+"\"  name=\"n_quantity_"+nItem+"_"+iteration+"\" style=\"width:95px;text-align:right;\" onkeyup=\"total("+iteration+");test();validNum('n_quantity_tblItem','"+iteration+"');\" value=\"0\" ></DIV>";

		var cell1 = row.insertCell(6);
		cell1.innerHTML = "<DIV ID=\"ajax_v_unit_price_"+nItem+"_"+iteration+"\" text-align:right;\" >"+
		"<input type=\"text\" ID=\"v_unit_price_"+nItem+"_"+iteration+"\"  name=\"v_unit_price_"+nItem+"_"+iteration+"\" style=\"width:85px;text-align:right;\" onkeyup=\"total("+iteration+");test();validNum('v_unit_price_tblItem','"+iteration+"');\" value=\"0\" readonly >"+
		"<input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\" ></DIV>";
	}else{
		alert('item DO tdk dpt lebih dari 22 item !');
		return false;		
	}
}

function removeRowFromTable(nItem) {
	var tbl = document.getElementById(nItem);
	var lastRow = tbl.rows.length;	
	if (lastRow > 0) {
		tbl.width='100%';
		tbl.deleteRow(lastRow - 1);
	}
}
*/

function hitungnilai(j) {
	var total_nilai= $('#v_total_nilai').val();
	var total_faktur= $('#v_total_faktur').val();
	var ppn= $('#n_ppn').val();
	var total_fppn= $('#v_total_fppn').val();
	var n_discount= $('#n_discount').val();
	var v_discount= $('#v_discount').val();
		
	if (j == 1) { // n_discount, ini pake persen
		// v_total_nilai, v_total_faktur, n_ppn, v_total_fppn
		var hitungdiskon= (parseInt(total_nilai)*n_discount)/100;
		hitungdiskon = hitungdiskon.toFixed(0);
		$('#v_discount').val(hitungdiskon);
		var hitungtot= total_nilai-hitungdiskon;
		hitungtot = hitungtot.toFixed(2);
		$('#v_total_faktur').val(hitungtot);
		var hitungppn= hitungtot*0.11;
		hitungppn = hitungppn.toFixed(0);
		$('#n_ppn').val(hitungppn);
		var grandtotal= parseFloat(hitungtot)+parseFloat(hitungppn);
		grandtotal = grandtotal.toFixed(0);
		$('#v_total_fppn').val(grandtotal);
	}
	else {
		// v_discount, angka
		var hitungdiskon= parseInt(total_nilai)-v_discount;
		$('#v_total_faktur').val(hitungdiskon);
		var hitungppn= hitungdiskon*0.11;
		hitungppn = hitungppn.toFixed(0);
		$('#n_ppn').val(hitungppn);
		var grandtotal= parseFloat(hitungdiskon)+parseFloat(hitungppn);
		grandtotal = grandtotal.toFixed(0);
		$('#v_total_fppn').val(grandtotal);
	}
				
}

</script>

<!--
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	//~ $("#d_faktur").datepicker();
	//~ $("#d_due_date").datepicker();
	//~ $("#d_pajak").datepicker();
});
-->
</script>

<?php

include_once("funcs/1tanggal.php");
$tjthtempo =& dateAdd("d",38,$dateTime);


function JatuhTempo($tgl,$hari){ // d/m/Y
	list($tgl,$bln,$thn)	= explode("/",$tgl,strlen($tgl));
	$tempstamp	= mktime(0,0,0,$bln,$tgl+$hari,$thn);
	return date('d/m/Y',$tempstamp);
}

$tempo =& JatuhTempo(date('d/m/Y'),30);
?>

<BODY onload="test();">
<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_fpenjualando; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_fpenjualando; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">
	<table id="table-add-box">
	  <tr>
	    <td align="left">
		  <?php 
		  $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('fakpenjualanpopup/cform/simpan', $attributes);?>
		  
		    
		<div id="masterfpenjualandoform">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
					  <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td width="34%"><?php echo $form_nomor_f_fpenjualando; ?> : 
							  <input name="i_faktur" type="text" id="i_faktur" maxlength="13" value="<?php echo $no; ?>" onkeyup="ckfpenjualan(this.value); ipajak(this.value);"/>
							  <input type="hidden" name="cbox" id="cbox" value="<?=$nofakturpajak?>" />
							  
							  <input type="hidden" name="jumdetail" id="jumdetail" value="<?php echo $jumdetail ?>" />
							  
							  <input type="checkbox" name="ckbox" id="ckbox" onclick="ipajakck(document.getElementById('i_faktur').value);" />no.pajak							  
							  <div id="confnomorfpenj" style="color:#FF0000;"></div>
							  </td>
							<td width="33%"><?php echo $form_tgl_f_fpenjualando; ?> : 
							  <input name="d_faktur" type="text" id="d_faktur" maxlength="10" value="<?php echo $tgFaktur; ?>" onChange="getJatuhTempo(this.value,document.getElementById('d_due_date'),29); tgpajak(this.value);" onclick="displayCalendar(document.forms[0].d_faktur,'dd/mm/yyyy',this)"/>
							  </td>
							<td width="33%"><?php echo $form_cabang_fpenjualando; ?> : 
							  <select name="i_branch" id="i_branch" disabled >
								<option value="">[ <?php echo $form_pilih_cab_fpenjualando;?> ]</option>
								<?php
									foreach($opt_cabang as $row) {
										$Sel	= ($einisial==$row->einitial)?"selected":"";
										
										$lcabang	.="<option value=".$row->einitial." ".$Sel.">";
										$lcabang	.= $row->branch." ( ".$row->einitial." ) ";
										$lcabang	.= "</option>";
									}
									echo $lcabang;
								?>
							  </select>
							  </td>
						  </tr>
					  </table></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					</tr>
					<tr>
					  <td>
						  <div id="title-box2"><?php echo $form_detail_f_fpenjualando; ?>					  
						  <div style="float:right;">
						  <!-- 
						  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');"><span><img src="<?php //echo base_url(); ?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
						  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');"><span><img src="<?php //echo base_url(); ?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
						  -->
						  </div></div>					  
					  </td>
					</tr>
					<tr>
					  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="4%" class="tdatahead">NO</td>
						  <td width="15%" class="tdatahead"><?php echo $form_nomor_do_f_fpenjualando; ?></td>
						  <td width="15%" class="tdatahead"><?php echo $form_kd_brg_fpenjualando; ?></td>
						  <td width="26%" class="tdatahead"><?php echo $form_nm_brg_fpenjualando; ?></td>
						  <td width="5%" class="tdatahead">ADA BONEKA?</td>
						  <td width="14%" class="tdatahead"><?php echo $form_hjp_fpenjualando; ?></td>
						  <td width="5%" class="tdatahead">HRG GROSIR?</td>
						  <td width="14%" class="tdatahead"><?php echo $form_qty_fpenjualando; ?></td>
						  <td width="14%" class="tdatahead"><?php echo $form_nilai_fpenjualando; ?></td>
						</tr>
						<?php
								echo $List;
						  	?>
					<!--	<tr>
						  <td colspan="7">
						  	<table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
						  	
							</table>
						  </td>
						</tr> -->
                      </table></td>
					</tr>
					<tr>
					 <td>
					  <div id="title-box2">
					  <div style="float:right;">
					  &nbsp;
					  </div></div>
					 </td>	
					</tr>
					<tr>
					  <td><table width="98%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="16%">&nbsp;</td>
						  <td width="1%">&nbsp;</td>
						  <td width="23%">&nbsp;</td>
						  <td width="24%">&nbsp;</td>
						  <td width="14%"><?php echo $form_tnilai_fpenjualando; ?></td>
						  <td width="1%">:</td>
						  <td width="21%">
							<input name="v_total_nilai" type="text" id="v_total_nilai" size="15" maxlength="15" value="<?=$total?>" readonly />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>

						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_diskon_fpenjualando; ?> (%)</td>
						  <td>:</td>
						  <td>
						 <!-- <input name="n_discount" type="text" id="n_discount" size="10" maxlength="10" value="0" onkeyup="test();validNum('n_discount');"/> -->
						 <input name="n_discount" type="text" id="n_discount" size="10" maxlength="10" value="0" onkeyup='hitungnilai(1)' onblur='hitungnilai(1)'/>
						  <?php echo $form_dlm_fpenjualando; ?> 
						  <!-- <input name="v_discount" type="text" id="v_discount" size="12" maxlength="12" value="0" onkeyup="test2();" /> -->
						  <input name="v_discount" type="text" id="v_discount" size="12" maxlength="12" value="0" onkeyup='hitungnilai(2)' onblur='hitungnilai(2)' />
						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_tgl_jtempo_fpenjualando; ?> </td>
						  <td>:</td>
						  <td colspan="2">
							<input name="d_due_date" type="text" id="d_due_date" size="10" maxlength="10" value="<?php echo $tjthtempo; ?>" onclick="displayCalendar(document.forms[0].d_due_date,'dd/mm/yyyy',this)"/>  * 38 hari setelah tgl faktur
						  </td>
						  
						  <td><?php echo $form_total_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_faktur" type="text" id="v_total_faktur" size="15" maxlength="15" value="<?=$total_sblm_ppn?>" readonly />
						  </td>
						</tr>
						<tr>
						  <td><?php echo $form_no_fpajak_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="i_faktur_pajak" type="text" id="i_faktur_pajak" size="15" maxlength="18" value="<?php echo $nofakturpajak; ?>" onkeyup="ckfpajak(this.value);" />
							<div id="confnomorfpajak" style="color:#FF0000;"></div>
						  </td>
						  <td><?php echo $form_tgl_fpajak_fpenjualando; ?> :
							<input name="d_pajak" type="text" id="d_pajak" size="10" maxlength="10" value="<?php echo $tgPajak; ?>"onclick="displayCalendar(document.forms[0].d_pajak,'dd/mm/yyyy',this)"/>
							</td>
						  <td><!-- <#?php echo $form_ppn_fpenjualando; ?> -->PPN 11%</td>
						  <td>:</td>
						  <td>
							<input name="n_ppn" type="text" id="n_ppn" size="15" maxlength="15" value="<?=$nilai_ppn?>" readonly />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>
							<input type="checkbox" name="f_cetak" value="1" />
							&nbsp;<?php echo $form_ket_cetak_fpenjualando; ?></td>
						  <td>&nbsp;</td>
						  <td><?php echo $form_grand_t_fpenjualando; ?></td>
						  <td>:</td>
						  <td>
							<input name="v_total_fppn" type="text" id="v_total_fppn" size="15" maxlength="15" value="<?=$total_grand?>" readonly />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					</tr>
					<tr align="right">
					  <td>
						<input type="hidden" name="i_branch_hidden" id="i_branch_hidden" value="<?php echo $einisial ?>" />
						<!--<input name="btnhitung" type="button" id="btnhitung" value="<?php echo "Hitung Nilai Penjualan"; ?>" onclick="test();" />-->
						<input name="btnsimpan" type="submit" id="btnsimpan" value="<?php echo $button_simpan; ?>" onclick="return konfirm();" />
						
						<input name="btnbatal" type="button" id="btnbatal" value="<?php echo $button_batal; ?>" onclick="window.location='<?php echo base_url(); ?>index.php/fakpenjualanpopup/cform/'">
					  </td>
					</tr>		
				  </table>
				</td>
			  </tr>
			</table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
</BODY>
