<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>

<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function settextfield(a,b,c) {
	opener.document.getElementById('i_do_code').value=a;
	opener.document.getElementById('i_do').value=b;	
	opener.document.getElementById('i_product').value=c;	
	opener.document.getElementById('i_do_code').focus();
	this.close();
}

function checked1(){
	var x = document.getElementById('iteration');

	var banyak	= x.value;
	
	if(document.getElementById('ckall').checked==true){
		for (i = 1; i <= parseInt(banyak); i++){
			document.getElementById('f_ck_tblItem'+i).checked = true;
		}
	}else{
		for (i = 1; i <= parseInt(banyak); i++){
			document.getElementById('f_ck_tblItem'+i).checked = false;
		}
	}
}

function parsi(){
	
	var x = document.getElementById('iteration');

	var banyak	= x.value;
	var iter;
	var iter2;
	var iter3;

	var iter_external ;
	var iter_external_trim;
	var i_faktur_external;
	var i_faktur_trim;
	
	var i_prod_ext;
	var i_prod_trim;

	
	
		
	opener.document.getElementById('i_do_code').value= '';
	opener.document.getElementById('i_do').value	 = '';
	opener.document.getElementById('i_product').value = '';

	for(i = 1; i <= banyak; i++) {
	
		if(document.getElementById('f_ck_tblItem'+i).checked==true) {
			
			iter_external		= opener.document.getElementById('i_do_code').value;
			iter_external_trim 	= iter_external.trim();
			i_faktur_external	= opener.document.getElementById('i_do').value;
			i_faktur_trim 		= i_faktur_external.trim();
			i_prod_ext		= opener.document.getElementById('i_product').value;
			i_prod_trim		= i_prod_ext.trim();

			iter = document.getElementById('f_ck_tblItem'+i).value;
			iter2 = document.getElementById('i_do'+i).value;
			iter3 = document.getElementById('i_product'+i).value;
	
			if(iter_external_trim=='') {
				opener.document.getElementById('i_do_code').value = iter.trim();
				opener.document.getElementById('i_do').value = iter2.trim();
				opener.document.getElementById('i_product').value = iter3.trim();
			}else{
				opener.document.getElementById('i_do_code').value = iter_external_trim+'#'+iter.trim();
				opener.document.getElementById('i_do').value = i_faktur_trim+'#'+iter2.trim();
				opener.document.getElementById('i_product').value = i_prod_trim+'#'+iter3.trim(); 
			}

		}
	}
	
	if(i==banyak)
		this.close();

}

function klose(){
	this.close();
}

/***
function getInfo(txt,ibranch) {

$.ajax({

type: "POST",
url: "<?php echo site_url('fakpenjualanpopup/cform/flistdetaildo'); ?>",
data:"key="+txt+"&ibranch="+ibranch,
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};
***/

function carifocus() {
	//document.getElementById('txtcari').focus();
}

</script>

</head>
<BODY id="bodylist" onload="carifocus();">
<div id="main">
<br />
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
	<td align="center">
	<input type="button" name="btnkirim" id="btnkirim" value="AMBIL DO" onclick="parsi();">&nbsp;
  	<input type="button" name="btnclose" id="btnclose" value="KELUAR" onclick="klose();"></td>
  </tr>
  <!--
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="getInfo(this.value,'<?php echo $ibranch; ?>');"/>
  	</td>
  </tr>
  -->
  <tr>
    <td align="left">
	<?php echo form_open('fakpenjualanpopup/cform/caribrgjadi', array('id' => 'listformbrgjadi'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php
		if(!empty($isi) || isset($isi)) {
			
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			
			if(count($isi)) {
				
				$tanggal	= array();
				$cc	= 1;
				
				foreach($isi as $row) {
				
					$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:""; // dd mm YYYY
					
					$idocode	= trim($row->ido_code);
					$iproduct	= trim($row->iproduct);
					
					$tglfaktur	= $tgl[2]."/".$tgl[1]."/".$tgl[0];
					
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->ido_code."</a></td>	 
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$tanggal[$cc]."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->iproduct."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->motifname."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->qty."</a></td>
					  <td><a href=\"javascript:settextfield('$idocode','$row->ido','$iproduct')\">".$row->qtyakhir."</a></td>
					  <td>
					  <input type=\"hidden\" name=\"i_do".$cc."\" id=\"i_do".$cc."\" value=\"".$row->ido."\">
					  <input type=\"hidden\" name=\"i_product".$cc."\" id=\"i_product".$cc."\" value=\"".$iproduct."\">
					  <input type=\"checkbox\" ID=\"f_ck_tblItem".$cc."\" name=\"f_ck_tblItem".$cc."\" value=\"".$row->ido_code."\" /></td>
					 </tr>";
					 
					 $cc+=1;
				}
				echo " <input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\"".$cc."\"> ";
			}
		}
		?>
		
		<table class="listtable2" border="1">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Nomor DO"); ?></th>
		 <th width="100px;"><?php echo strtoupper("Tgl. DO"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Kode BRG"); ?></th>
		 <th width="200px;"><?php echo strtoupper("Nama Barang"); ?></th>
		 <th width="20px;"><?php echo strtoupper("Qty"); ?></th>
		 <th width="20px;"><?php echo strtoupper("Sisa"); ?></th>
		 <th width="4px;"><input type="checkbox" name="ckall" id="ckall" onclick="checked1();"/></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>

  	</div>
      </div>
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
