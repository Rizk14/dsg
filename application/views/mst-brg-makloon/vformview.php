<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Bahan Quilting</h3><br>
<?php if ($this->session->userdata('gid') != 3) { ?><a href="<? echo base_url(); ?>index.php/mst-brg-makloon/cform">Tambah Data</a>&nbsp;&nbsp;<?php } ?><a href="<? echo base_url(); ?>index.php/mst-brg-makloon/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-brg-makloon/cform/cari'); ?>
<!--Jenis Makloon
				<select name="jenis_makloon" id="jenis_makloon">
					<option value="0" <?php //if ($ejns_makloon == '') { ?> selected="true" <?php //} ?> >- All -</option>
				<?php //foreach ($jns_makloon as $jns) { ?>
					<option value="<?php //echo $jns->id ?>" <?php //if ($jns->id == $ejns_makloon) { ?> selected="true" <?php //} ?> ><?php //echo $jns->nama ?></option>
				<?php //} ?>
				</select> -->
<input type="text" name="cari" value="<?php echo $cari; ?>">&nbsp;
<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<!-- <th>Jenis Makloon</th> -->
		 <th>Kode Brg</th>
		 <th>Nama Barang</th>
		 <th>Satuan</th>
		 <th>Deskripsi</th>
		 <th>Lokasi Gudang</th>
		 <th>Status</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			 foreach ($query as $row){
				 $pisah1 = explode("-", $row->tgl_update);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				 
				 echo "<tr class=\"record\">";
				// echo    "<td>$row->nama_makloon</td>";
				 echo    "<td>$row->kode_brg</td>";
				 echo    "<td>$row->nama_brg</td>";
				 echo    "<td>$row->nama_satuan</td>";
				 echo    "<td>$row->deskripsi</td>";
				 
				 if ($row->id_gudang != '') {
					 $query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
					$hasilrow = $query3->row();
					$ekode_gudang	= $hasilrow->kode_gudang;
					$eid_gudang	= $hasilrow->id;
					$enama_gudang	= $hasilrow->nama;
					$enama_lokasi	= $hasilrow->nama_lokasi;
					
					 echo    "<td>[$enama_lokasi] $ekode_gudang - $enama_gudang</td>";
				 }
				 else {
					echo "<td>&nbsp;</td>";	 
				 }
				 if ($row->status_aktif == 't')
					echo    "<td>Aktif</td>";
				 else
					echo    "<td>Non-Aktif</td>";
				 echo    "<td>$tgl_update</td>";
				 
				 $ada = 0;
				 
				/* $query3	= $this->db->query(" SELECT id FROM tm_marker_gelaran WHERE kode_brg_quilting = '$row->kode_brg' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 } */
				 
				 // cek apakah data kode sudah dipake di tabel tm_sj_proses_quilting_detail
				/* $query3	= $this->db->query(" SELECT id FROM tm_sj_proses_quilting_detail WHERE kode_brg_makloon = '$row->kode_brg' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 } */
				 
				 $query3	= $this->db->query(" SELECT id FROM tm_sj_hasil_makloon_detail WHERE id_brg_makloon = '$row->id' ");
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				 
				 echo    "<td align=center>";
				 if ($this->session->userdata('gid') != 3) {
					echo "<a href=".base_url()."index.php/mst-brg-makloon/cform/index/$row->id \" >Edit</a>";
					 if ($ada == 0)
						echo    "&nbsp; <a href=".base_url()."index.php/mst-brg-makloon/cform/delete/$row->id \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				 }
				 echo "</td>";
				 echo  "</tr>";
		 	}
		 ?>
 	</tbody>
</table><br>
<? echo $this->pagination->create_links();?>
</div>
