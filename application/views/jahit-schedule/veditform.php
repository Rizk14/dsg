<h3>Data Schedule Jahit</h3><br>
<a href="<? echo base_url(); ?>index.php/jahit-schedule/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/jahit-schedule/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	//$("#no").val('2');
	
	//generate_nomor();
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****tgl*************************************
		var tgl="#tgl_"+n;
		var new_tgl="#tgl_"+no;
		$(tgl, lastRow).attr("id", "tgl_"+no);
		$(new_tgl, lastRow).attr("name", "tgl_"+no);		
		$(new_tgl, lastRow).val('');		
		//*****end tgl*********************************
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//***** jam *************************************
		var jam="#jam_"+n;
		var new_jam="#jam_"+no;
		$(jam, lastRow).attr("id", "jam_"+no);
		$(new_jam, lastRow).attr("name", "jam_"+no);		
		$(new_jam, lastRow).val('');				
		//*****end jam*************************************	
		
		//*****ket*************************************
		var ket="#keterangan_"+n;
		var new_ket="#keterangan_"+no;
		$(ket, lastRow).attr("id", "keterangan_"+no);
		$(new_ket, lastRow).attr("name", "keterangan_"+no);		
		$(new_ket, lastRow).val('');				
		//*****end ket*************************************	
				
				
		//*****img tgl*************************************
		var imgtgl="#imgtgl_"+n;
		var new_imgtgl="#imgtgl_"+no;
		$(imgtgl, lastRow).attr("id", "imgtgl_"+no);
		$(new_imgtgl, lastRow).attr("name", "imgtgl_"+no);
		
		var even_pick = "displayCalendar(document.forms[0].tgl_"+no+",'dd-mm-yyyy',this)"
		$(new_imgtgl, lastRow).attr("onclick", even_pick);
		//*****end img tgl*********************************
		
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		 
		 var  even_klik= "openCenteredWindow('<?php echo base_url(); ?>index.php/jahit-schedule/cform/show_popup_brg/"+no+"/');";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	

});
</script>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_input() {
	var no_schedule= $('#no_schedule').val();
	var operator_jahit= $('#operator_jahit').val();
	if (no_schedule == '') {
		alert("Nomor Schedule harus diisi..!");
		return false;
	}
	if (operator_jahit == '') {
		alert("Operator jahit harus diisi..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#tgl_'+k).val() == '') {
				alert("Data tanggal harus diisi...!");
				return false;
			}
			if ($('#kode_'+k).val() == '') {
				alert("Data item barang tidak boleh ada yang kosong...!");
				return false;
			}
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			
			if($('#jam_'+k).val() == '0' || $('#jam_'+k).val() == '' ) {				
				alert("Data jam kerja tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			if (isNaN($('#jam_'+k).val()) ) {
				alert("Jam kerja harus berupa angka..!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}
	
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/jahit-schedule/cform/updatedata" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_sc" value="<?php echo $query[0]['id'] ?>">

<?php $detail_sc = $query[0]['detail_sc']; 
	$no = 1;
	foreach ($detail_sc as $hitung) {
		$no++;
	}
?>
Edit Data

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">
<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
    <td width="15%">No Schedule</td>
    <td>
      <input name="no_schedule" type="text" id="no_schedule" size="20" maxlength="20" readonly="true" value="<?php echo $query[0]['no_schedule'] ?>">
    </td>
    
  </tr>
  <tr>
    <td>Unit Jahit</td>
    <td>
		<select name="unit_jahit" id="unit_jahit">
				<?php foreach ($unit_jahit as $unit) { ?>
					<option value="<?php echo $unit->kode_unit ?>" <?php if ($query[0]['kode_unit'] == $unit->kode_unit) { ?> selected <?php } ?> ><?php echo $unit->kode_unit." - ". $unit->nama ?></option>
				<?php } ?>
				</select>
	</td>
  </tr>
  <tr>
    <td>Operator Jahit</td>
    <td>
      <input name="operator_jahit" type="text" id="operator_jahit" size="40" maxlength="40" value="<?php echo $query[0]['operator_jahit'] ?>">
    </td>
    
  </tr>

  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="9" align="right">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Tanggal</th>
          <th>Kode Barang</th>
           <th>Nama Barang</th>
	      <th>Qty</th>
	      <th>Jam Kerja</th>
          <th>Keterangan</th>
        </tr>
		<?php $i=1;
        if (count($query[0]['detail_sc'])==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
        <?php
		} else {
			$detail_sc = $query[0]['detail_sc'];
			for($k=0;$k<count($detail_sc);$k++){
			?>
			
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>

          <td nowrap="nowrap">
           <label>
				<input name="tgl_<?php echo $i ?>" type="text" id="tgl_<?php echo $i ?>" size="10" value="<?php echo $detail_sc[$k]['tgl_jahit'] ?>" readonly="true">
		   </label>
		   <img alt="" id="imgtgl_<?php echo $i ?>" name="imgtgl_<?php echo $i ?>" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].tgl_<?php echo $i ?>,'dd-mm-yyyy',this)">
           </td>
           
           <td nowrap="nowrap">
           <input name="kode_<?php echo $i ?>" type="text" id="kode_<?php echo $i ?>" size="15" readonly="true" value="<?php echo $detail_sc[$k]['kode_brg_jadi'] ?>"/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=<?php echo $i ?>" name="pilih_<?php echo $i ?>" value="..." type="button" id="pilih_<?php echo $i ?>" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/jahit-schedule/cform/show_popup_brg/<?php echo $i ?>/');" type="button">
           </td>
          <td><input name="nama_<?php echo $i ?>" type="text" id="nama_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $detail_sc[$k]['nama_brg'] ?>" /></td>
          <td><input name="qty_<?php echo $i ?>" type="text" id="qty_<?php echo $i ?>" size="10" maxlength="5" value="<?php echo $detail_sc[$k]['qty'] ?>" /></td>
          <td><input name="jam_<?php echo $i ?>" type="text" id="jam_<?php echo $i ?>" size="5" maxlength="5" value="<?php echo $detail_sc[$k]['jam_kerja'] ?>" /></td>
          <td><input name="keterangan_<?php echo $i ?>" type="text" id="keterangan_<?php echo $i ?>" size="30" maxlength="30" value="<?php echo $detail_sc[$k]['ket'] ?>" /></td>
        </tr>
			
        <?php $i++; } // end foreach 
		}
		?>
	</table>	
	
	</form>
      <div align="center"><br> 
        
        <input type="submit" name="submit2" value="Edit" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/jahit-schedule/cform/view'">

      </div></td>
    </tr>

</table>
</div>
</form>
