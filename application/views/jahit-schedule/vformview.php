<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function cek_input() {
	var date_from= $('#date_from').val();
	var date_to= $('#date_to').val();

	if (date_from == '') {
		alert("Tanggal Awal harus dipilih..!");
		return false;
	}
	if (date_to == '') {
		alert("Tanggal Akhir harus dipilih..!");
		return false;
	}
	
	if (date_from > date_to) {
		alert("Tanggal Awal harus lebih kecil dari Tanggal Akhir..!");
		return false;
	}
	
}
</script>

<h3>Data Schedule Jahit</h3><br> 
<a href="<? echo base_url(); ?>index.php/jahit-schedule/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/jahit-schedule/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('jahit-schedule/cform/cari'); ?>
Unit Jahit: <select name="unit_jahit" id="unit_jahit">
				<option value="0" >-All-</option>
				<?php foreach ($list_unit_jahit as $unit) { ?>
					<option value="<?php echo $unit->kode_unit ?>" <?php if ($unit->kode_unit == $unit_jahit) { ?> selected <?php } ?> ><?php echo $unit->kode_unit." - ". $unit->nama ?></option>
				<?php } ?>
				</select>&nbsp;
		Dari tanggal: <label>
		  <input name="date_from" type="text" id="date_from" size="10" value="<?php echo $date_from ?>" readonly="true">
		</label>
		   <img alt="" id="date_from" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"> &nbsp;
		Sampai tanggal: <label>
		  <input name="date_to" type="text" id="date_to" size="10" value="<?php echo $date_to ?>" readonly="true">
		</label>
		   <img alt="" id="date_to" align="middle"
				title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
				onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)">
				
&nbsp;<input type="submit" name="submit" value="Cari" onclick="return cek_input();">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr>
		<th>No Schedule</th>
		 <th>Unit Jahit</th>
		 <th>Operator Jahit</th>
		 <th>Tgl</th>
		 <th>Nama Barang Jadi</th>
		 <th>Qty</th>
		 <th>Jml Jam Kerja</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
								 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_schedule']."</td>";
				 echo    "<td>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 echo    "<td>".$query[$j]['operator_jahit']."</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_jahit'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_jahit'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['tgl_jahit'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td>";
				 if (is_array($query[$j]['detail_jahit'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_jahit'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg_jadi']." - ".$var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_jahit'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_jahit'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td align='right'>";
				 if (is_array($query[$j]['detail_jahit'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_jahit'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['jam_kerja'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo    "<td>".$tgl_update."</td>";
				 
				 //if ($query[$j]['status_edit'] == 'f') {
					 echo    "<td align=center><a href=".base_url()."index.php/jahit-schedule/cform/edit/".$query[$j]['id']." \" id=\"".$query[$j]['id']."\">Edit</a>&nbsp;
					 <a href=".base_url()."index.php/jahit-schedule/cform/delete/".$query[$j]['id']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 //}

				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
