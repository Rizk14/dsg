

<?php
// Nomor urut data di tabel.
$per_page = 10;



if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Informasi Transaksi Makloon Baju di Unit Jahit</h2>
    <hr>

    <?php if (!empty($informasi_transaksi_makloon_baju) && is_array($informasi_transaksi_makloon_baju)): ?>
    <div class="row">
        <div class="col-md-12">
 <form action ="export_csv" name="myform" id='myform' class="form-inline" method="post">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>NO SJ</th>
                    <th>Tanggal SJ</th>
					<th>Masuk Bagus</th>
                    <th>Masuk Lain - lain</th>
                    <th>Keluar Bagus</th>
                    <th>Keluar Lain - lain</th>
                </tr>
                </thead>
                <tbody>
					
                <?php 
              
               $tot_masuk_bagus_jahit=0;$tot_masuk_lain_jahit=0;$tot_keluar_bagus_jahit=0;$tot_keluar_lain_jahit=0;
               for($i=0;$i<count($informasi_transaksi_makloon_baju);$i++){
				 ?>
                    <tr>
						
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $informasi_transaksi_makloon_baju[$i]['no_sj'] ;?></td>
						<td><?php echo $informasi_transaksi_makloon_baju[$i]['tanggal_sj'] ;?></td>
						<td align='center'><?php if($informasi_transaksi_makloon_baju[$i]['masuk_bagus_jahit']) echo $informasi_transaksi_makloon_baju[$i]['qty'] ;?></td>
                        <td align='center'><?php if($informasi_transaksi_makloon_baju[$i]['masuk_lain_jahit']) echo $informasi_transaksi_makloon_baju[$i]['qty'] ;?></td>
                        <td align='center'><?php if($informasi_transaksi_makloon_baju[$i]['keluar_bagus_jahit']) echo $informasi_transaksi_makloon_baju[$i]['qty'] ;?></td>
                        <td align='center'><?php if($informasi_transaksi_makloon_baju[$i]['keluar_lain_jahit']) echo $informasi_transaksi_makloon_baju[$i]['qty'] ;?></td>
                    </tr>
					
						<?php 
						if($informasi_transaksi_makloon_baju[$i]['masuk_bagus_jahit']==1) {
						$tot_masuk_bagus_jahit += $informasi_transaksi_makloon_baju[$i]['qty'] ;
						}
						if($informasi_transaksi_makloon_baju[$i]['masuk_lain_jahit']==1) {
						$tot_masuk_lain_jahit += $informasi_transaksi_makloon_baju[$i]['qty'] ;
						}
						if($informasi_transaksi_makloon_baju[$i]['keluar_bagus_jahit']==1) {
						$tot_keluar_bagus_jahit += $informasi_transaksi_makloon_baju[$i]['qty'] ;
						}
						if($informasi_transaksi_makloon_baju[$i]['keluar_lain_jahit']==1) {
						$tot_keluar_lain_jahit += $informasi_transaksi_makloon_baju[$i]['qty'] ;
						}
						?>
						
                <?php } ?>
					<tr>
						<td align='center' colspan='3'>TOTAL</td>
						<td align='center'><?php echo $tot_masuk_bagus_jahit ;?></td>	
						<td align='center'><?php echo $tot_masuk_lain_jahit ;?></td>	
						<td align='center'><?php echo $tot_keluar_bagus_jahit ;?></td>	
						<td align='center'><?php echo $tot_keluar_lain_jahit ;?></td>	
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="tanggal_sj_dari" id="tanggal_sj_dari" value="<?php echo $tanggal_sj_dari ?>">
             <input type="hidden" name="tanggal_sj_ke" id="tanggal_sj_ke" value="<?php echo $tanggal_sj_ke ?>">
               <input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">

</form>

        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $informasi_transaksi_makloon_baju ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



