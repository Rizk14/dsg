<h3>Laporan Kartu Stok Bahan Hasil Jahit</h3><br><br>

<div>
<b>Kode & Nama Barang Jadi: </b> <?php echo $brg_jadi; ?><br>
<b>Periode:</b> <?php echo $date_from ?> s.d. <?php echo $date_to ?> <br>
<b>Total Data = </b><?php echo $jum_total ?><br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr>
		 <th>Nomor Bukti</th>
		 <th>Masuk</th>
		 <th>Masuk Lain-lain</th>
		 <th>Keluar</th>
		 <th>Keluar Lain-lain</th>
		 <th>Saldo</th>
		 <th>Tgl Update Stok</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 
				 $pisah1 = explode("-", $query[$j]['tgl_input']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_input = $tgl1." ".$nama_bln." ".$thn1;
								 
				 echo "<tr class=\"record\">";

				 echo    "<td>".$query[$j]['no_bukti']."</td>";
				 
				 if ($query[$j]['masuk'] == '')
					echo "<td align='right'>0</td>";
				 else
					echo    "<td align='right'>".$query[$j]['masuk']."</td>";
				
				if ($query[$j]['masuk_lain'] == '')
					echo "<td align='right'>0</td>";
				else
					echo    "<td align='right'>".$query[$j]['masuk_lain']."</td>";
				 
				 if ($query[$j]['keluar'] == '')
					echo "<td align='right'>0</td>";
				 else
					echo    "<td align='right'>".$query[$j]['keluar']."</td>";
					
				if ($query[$j]['keluar_lain'] == '')
					echo "<td align='right'>0</td>";
				 else
					echo    "<td align='right'>".$query[$j]['keluar_lain']."</td>";
				
				 echo    "<td align='right'>".$query[$j]['saldo']."</td>";
				 echo    "<td align='center'>".$tgl_input."</td>";
				 echo  "</tr>";
					
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
