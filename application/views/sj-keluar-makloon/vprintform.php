<script language="javascript">

var howLong = 3600;

t = null;
function closeMe(){
	t = setTimeout("self.close()",howLong);
}

</script>

</head>

<?php

$get_attributes	= false;

if($get_attributes) {
	include_once("printipp_classes/PrintIPP.php"); 
}else{
	include_once("printipp_classes/BasicIPP.php");
}
?>

<body onload="closeMe();" />

<?php

$waktu	= date("H:i:s");
$line	= 80;

$ipp = new PrintIPP();

$ipp->setHost($host);
$ipp->setPrinterURI($uri);

$ipp->setLog($log_destination,$destination_type='file',$level=2);

$ipp->setRawText();
$ipp->unsetFormFeed();

$ipp->setData($List);
$ipp->printJob();

?>
