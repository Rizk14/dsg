<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var des=$("#des").val();
		var qty=$("#qty").val();
		var kel_brg=$("#kel_brg").val();
		var satuan=$("#satuan").val();
		var jumdata=$("#jumdata").val();
		//var qty_konversi = 0;
		
		for(var i=1; i <= jumdata; i++){
			var stringnya = opener.document.getElementById('kode_'+i).value;
			if (idx == stringnya) {
				alert("Kode barang sudah pernah dipilih...!");
				return false;
			}
		}
		
		if (qty == undefined || qty == '')
			qty = 0;
		
		//if (kel_brg == 'B') {
			if (satuan == "Yard") {
				qty_konversi = qty * 0.91;
				qty_konversi = qty_konversi.toFixed(2);
			}
			else
				qty_konversi = qty;
		//}
		//else
			//qty_konversi = qty;

		opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
		opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=des;
		opener.document.forms["f_purchase"].qty_<?php echo $posisi ?>.value = qty;
		opener.document.forms["f_purchase"].qty_stok_<?php echo $posisi ?>.value = qty;
		opener.document.forms["f_purchase"].qtym_<?php echo $posisi ?>.value = qty_konversi;
		opener.document.forms["f_purchase"].qtym_<?php echo $posisi ?>.focus();
		self.close();
	});
});
</script>
<center><h3>Daftar Barang Untuk Kelompok <?php echo $nama_kel ?></h3></center>
<div align="center"><br>
<?php echo form_open('sj-keluar-makloon/cform/show_popup_brg'); ?>

Jenis Bahan <select name="id_jenis_bhn" id="id_jenis_bhn">
				<option value="0" <?php if ($cjenis_bhn == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($jenis_bhn as $jns) { ?>
					<option value="<?php echo $jns->id ?>" <?php if ($cjenis_bhn == $jns->id) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nj_brg."] ". $jns->kode." - ".$jns->nama ?></option>
				<?php } ?>
				</select> &nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="kel_brg" id="kel_brg" value="<?php echo $kel_brg ?>">
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="des" id="des">
<input type="hidden" name="qty" id="qty">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="jumdata" id="jumdata" value="<?php echo $jumdata ?>">
</form>

  <table border="1" width="100%" align="center" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Stok Terkini</th>
      <th bgcolor="#999999">Fungsi</th>
    </tr>
	<?php 
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
				
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg']; ?></td>
      <td nowrap><?php echo $query[$j]['nama_brg']; ?></td>
      <td><?php echo $query[$j]['satuan']; ?></td>
      <td><?php if ($query[$j]['jum_stok']!= 0) echo $query[$j]['jum_stok']; else echo "0"; ?></td>
      <td>
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
	  window.document.f_master_brg.des.value='<?php echo str_replace("'", "\'", $query[$j]['nama_brg']) ?>'; 
	  window.document.f_master_brg.qty.value='<?php echo $query[$j]['jum_stok'] ?>';
	  window.document.f_master_brg.satuan.value='<?php echo $query[$j]['satuan'] ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
