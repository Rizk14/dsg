<style type="text/css">

  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px; }

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>

<script>
$(function()
{
	$("#pilih").click(function()
	{
		
		opener.document.forms["f_purchase"].iddetail.value = '';
		opener.document.forms["f_purchase"].bhn_baku.value = '';
		
		if (document.f_master_brg.iddetail.length > 0) {
			for(var i=0; i < document.f_master_brg.iddetail.length; i++){
				if(document.f_master_brg.iddetail[i].checked) {
					opener.document.forms["f_purchase"].iddetail.value+= document.f_master_brg.iddetail[i].value + ";";
					opener.document.forms["f_purchase"].bhn_baku.value += document.f_master_brg.bhn_baku[i].value+";";					
				}
			}
		}
		else {
			opener.document.forms["f_purchase"].iddetail.value = document.f_master_brg.iddetail.value+";";
			opener.document.forms["f_purchase"].bhn_baku.value = document.f_master_brg.bhn_baku.value+";";
		}
				
		self.close();
	});
});
</script>

<center><h3>Daftar Bahan Baku Yang Sudah Dipenuhi Dari Gudang</h3></center>
<div align="center"><br>
<form id="f_master_brg" name="f_master_brg">

  <table border="1" align="center" width="100%" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
    
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">No/Tgl Bon M</th>
      <th bgcolor="#999999">Utk Barang Jadi</th>
      <th bgcolor="#999999">Bahan Quilting</th>
      <th bgcolor="#999999">Bahan Baku</th>
      <th bgcolor="#999999">Pjg Kain (m)</th>
      <th bgcolor="#999999">Detail Gelar</th>
      <th bgcolor="#999999">Last Update</th>
      <th bgcolor="#999999">Action</th>
    </tr>
    
	<?php 
	
	$i=1;
	
	$query = $detailprosescutting;
	 
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {				
			$pisah1 = explode("-", $query[$j]['tgl_bonm']);
				
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
				
			if ($bln1 == '01')
				$nama_bln = "Januari";
			else if ($bln1 == '02')
				$nama_bln = "Februari";
			else if ($bln1 == '03')
				$nama_bln = "Maret";
			else if ($bln1 == '04')
				$nama_bln = "April";
			else if ($bln1 == '05')
				$nama_bln = "Mei";
			else if ($bln1 == '06')
				$nama_bln = "Juni";
			else if ($bln1 == '07')
				$nama_bln = "Juli";
			else if ($bln1 == '08')
				$nama_bln = "Agustus";
			else if ($bln1 == '09')
				$nama_bln = "September";
			else if ($bln1 == '10')
				$nama_bln = "Oktober";
			else if ($bln1 == '11')
				$nama_bln = "November";
			else if ($bln1 == '12')
				$nama_bln = "Desember";
				
			$tgl_bonm = $tgl1." ".$nama_bln." ".$thn1;
			
			$pisah1 = explode("-", $query[$j]['tgl_update']);
				
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
				
			if ($bln1 == '01')
				$nama_bln = "Januari";
			else if ($bln1 == '02')
				$nama_bln = "Februari";
			else if ($bln1 == '03')
				$nama_bln = "Maret";
			else if ($bln1 == '04')
				$nama_bln = "April";
			else if ($bln1 == '05')
				$nama_bln = "Mei";
			else if ($bln1 == '06')
				$nama_bln = "Juni";
			else if ($bln1 == '07')
				$nama_bln = "Juli";
			else if ($bln1 == '08')
				$nama_bln = "Agustus";
			else if ($bln1 == '09')
				$nama_bln = "September";
			else if ($bln1 == '10')
				$nama_bln = "Oktober";
			else if ($bln1 == '11')
				$nama_bln = "November";
			else if ($bln1 == '12')
				$nama_bln = "Desember";
				
			$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['no_bonm']." / ".$tgl_bonm; ?></td>
      <td><?php echo $query[$j]['kode_brg_jadi']." / ".$query[$j]['nama_brg_jadi'] ?></td>
      <td><?php echo $query[$j]['kode_brg_quilting']." / ".$query[$j]['nama_brg_quilting'] ?></td>
      <td><?php echo $query[$j]['kode_brg']." / ".$query[$j]['nama_brg'] ?></td>
      <td><?php echo $query[$j]['qty_pjg_kain'] ?></td>
      <td><?php echo $query[$j]['gelar_pjg_kain'] ?></td>
      <td><?php echo $tgl_update ?></td>
      <td><?php echo " <input type='checkbox' name='iddetail' id='iddetail' value='".$query[$j]['iddetail']."' >"; 
			echo "&nbsp; <input type='hidden' name='bhn_baku' id='bhn_baku' value='".$query[$j]['kode_brg']."' >";
      ?></td>
      
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  </form>

  <input type="button" name="pilih" id="pilih" value="Pilih" >
</div>
