<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 850;
		var height = 550;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function()
{
	$(".pilih").click(function()
	{
		var id_op_print=$("#id_op_print").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/op/cform/print_op/"+id_op_print;
		openCenteredWindow(urlnya);
	});
	
	$(".pilihgudang").click(function()
	{
		var id_op_print=$("#id_op_print").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/op/cform/print_op_nonharga/"+id_op_print;
		openCenteredWindow(urlnya);
	});
});


</script>

<h3>Data Order Pembelian (OP)</h3><br> 
<a href="<?php echo base_url(); ?>index.php/op/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/op/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('op/cform/cari'); ?>
Supplier <select name="supplier" id="supplier">
				<option value="xx" <?php if ($csupplier == 'xx') { ?> selected="true" <?php } ?> >- All -</option>
				<option value="0" <?php if ($csupplier == '0') { ?> selected="true" <?php } ?> >Lain-lain</option>
				<?php foreach ($list_supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" <?php if ($csupplier == $sup->id) { ?> selected="true" <?php } ?> ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select>&nbsp;
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_op_print" id="id_op_print">

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No PP</th>
		 <th>No OP</th>
		 <th>Tgl OP</th>
		 <th>Supplier</th>
		 <th>List Barang</th>
		 <th>Satuan</th>
		<!-- 25-06-2015 GA DIPAKE <th>Satuan Lain</th> -->
		 <th>Qty</th>
		 <th>Grandtotal Qty</th>
		 <th>Harga (Rp.)</th>
		 <th>Diskon (Rp.)</th>
		 <th>Subtotal (Rp.)</th>
		 <th>Grandtotal (Rp.)</th>
		 <th>Qty Pemenuhan SJ</th>
		 <th>Qty Sisa</th>
		 <th>Tgl Delivery</th>
		 <th>Tgl Expire</th>
		 <th>Status</th>
		 <th>Last Update</th>
		 <th>Per Item</th>
		 <th>Per OP</th>
		 <?php //if ($this->session->userdata('gid') == 1) { ?>
			<th>Hapus Per Item</th>
		 <?php //} ?>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				$qstatusop	= $this->mmaster->statusfakturop($query[$j]['id']);
				$jstatusop	= $qstatusop->num_rows();
				if($jstatusop>0) {
					$jmlop	= $jstatusop;
				} else {
					$jmlop	= 0;
				}
				 
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				$jam1nya= $exptgl1[1];
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				
				$pisah1 = explode("-", $query[$j]['tgl_op']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_op = $tgl1." ".$nama_bln." ".$thn1;
				
				if ($query[$j]['jenis_pembelian'] == '1')
					$jenis = "Cash";
				else if ($query[$j]['jenis_pembelian'] == '2')
					$jenis = "Kredit";
				else
					$jenis = "-";
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_pp']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['no_op']." (".$jenis.")</td>";
				 echo    "<td>".$tgl_op."</td>";
				 echo    "<td>".$query[$j]['kode_supplier']." - ".$query[$j]['nama_supplier']."</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				/* echo "<td nowrap>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['sat_lain']!= '')
						  echo $var_detail[$k]['sat_lain'];
						else
						  echo "&nbsp;";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>"; */
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";

				 echo "</td>";

				 echo "<td style='white-space:nowrap;' align='center'>";

						echo number_format($query[$j]['grandtotalqty']);
					
				 echo "</td>";
				 
				 // 22-09-2014
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['harga'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 // 02-07-2015
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['diskon'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['subtotal'], 2, ',','.');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
					echo number_format($query[$j]['grandtotal'], 2, ',','.');
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['jum_beli'],2,'.','');
						  if ($var_detail[$k]['count_beli'] > 1)
							 echo " (".$var_detail[$k]['count_beli']." SJ)";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo number_format($var_detail[$k]['sisanya'],2,'.','');
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";

   				 echo    "<td style='white-space:nowrap;'>".$query[$j]['tgl_delivery']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['tgl_expire']."</td>";
				 
				 echo    "<td>";
				 if ($query[$j]['status_aktif'] == 't') echo "Aktif"; else echo "Non-Aktif";
				 echo "</td>";
				 
				 echo    "<td>".$tgl_update."</td>";
				 
				 echo "<td align='center' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_fb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_fb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if($var_detail[$k]['status_op'] == 'f' && $var_detail[$k]['jum_beli'] == 0 ) {
							echo "<a href=".base_url()."index.php/op/cform/edititem/".$var_detail[$k]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \" id=\"".$var_detail[$k]['id']."\">Edit</a>";
							if ($k<$hitung-1)
								echo "<br> ";
						}
						else {
							echo "&nbsp;";
							if ($k<$hitung-1)
								echo "<br> ";
						}
					} // end for
				 } // end if
				 
				/* if ($query[$j]['status_edit'] == 'f')
				//if ($jmlop==0)
					echo    "<td align=center>
					<a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_op_print.value=".$query[$j]['id']."'; ><u>Print</u></a>
					<a href=".base_url()."index.php/op/cform/edit/".$query[$j]['id']." \">Edit</a>
					<a href=".base_url()."index.php/op/cform/delete/".$query[$j]['id']."/".$query[$j]['id_detailnya']." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 else
					echo "<td align=center><a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_op_print.value=".$query[$j]['id']."'; ><u>Print</u></a></td>";
				*/
				 echo "</td>";
				 
				 //if ($query[$j]['status_edit'] == 'f') {
				if ($query[$j]['jum_item_f'] > 0 || !is_array($query[$j]['detail_fb'])) {
				//if ($jmlop==0)
					echo    "<td align=center>";
					
					if ($query[$j]['jum_item_f'] > 0) {
						echo "<a href=".base_url()."index.php/op/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \">Edit</a>&nbsp;| &nbsp;";
						echo "<a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_op_print.value=".$query[$j]['id']."'; ><u>Print</u></a>&nbsp; | &nbsp;";
						echo "<a style='cursor:pointer' class='pilihgudang' onMouseOver='window.document.f_master_brg.id_op_print.value=".$query[$j]['id']."'; ><u>Print (Tanpa Harga)</u></a>&nbsp; | &nbsp;";
					}
					
					//if ($this->session->userdata('gid') == 1) {
						if ($query[$j]['jum_beli2'] == 0 || !is_array($query[$j]['detail_fb'])) {
							echo "<a href=".base_url()."index.php/op/cform/delete/".$query[$j]['id']."/".$query[$j]['id_detailnya']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>&nbsp; |";
						}
					//}
					if ($query[$j]['status_aktif'] == 't') {
						$statusnya = "Non-Aktifkan";
						$aksi = "off";
					}
					else {
						$statusnya = "Aktifkan";
						$aksi = "on";
					}
					echo "&nbsp;<a href=".base_url()."index.php/op/cform/updatestatus/".$query[$j]['id']."/".$aksi."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \" onclick=\"return confirm('Update status aktifnya?')\">".$statusnya."</a>";
					echo "</td>";
				 }
				 else {
					//echo "<td align=center><a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_op_print.value=".$query[$j]['id']."'; ><u>Print</u></a></td>";
					echo "<td><a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_op_print.value=".$query[$j]['id']."'; ><u>Print</u></a>&nbsp; | &nbsp;";
					echo "<a style='cursor:pointer' class='pilihgudang' onMouseOver='window.document.f_master_brg.id_op_print.value=".$query[$j]['id']."'; ><u>Print (Tanpa Harga)</u></a> |</td>";
					//echo "<td>&nbsp;</td>";
				 }
				 //if ($this->session->userdata('gid') == 1) {
					 echo "<td align='center'>";
					 
					 if (is_array($query[$j]['detail_fb'])) {
						 $var_detail = array();
						 $var_detail = $query[$j]['detail_fb'];
						 $hitung = count($var_detail);
						for($k=0;$k<count($var_detail); $k++){
							if($var_detail[$k]['status_op'] == 'f' && $var_detail[$k]['jum_beli'] == 0 ) {
								echo "<a href=".base_url()."index.php/op/cform/deleteitem/".$var_detail[$k]['id']."/".$cur_page."/".$is_cari."/".$csupplier."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
								if ($k<$hitung-1)
									echo "<br> ";
							}
							else {
								echo "&nbsp;";
								if ($k<$hitung-1)
									echo "<br> ";
							}
						} // end for
					 } // end if
					 
					 echo "</td>";
				// } // end if
				 
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
