<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat">Info OP Per Item Barang</td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0">Form Info OP Per Item Barang</td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'listopperitembrg/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		<div id="masterlopvsdoform">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td>Kode Barang </td>
							<td>:</td>
							<td>
							  <input name="i_product" type="text" id="i_product" maxlength="100" value="<?=$kproduksi?>" />
							</td>
						  </tr>	
						  <tr>
							<td width="19%">Dari Tanggal</td>
							<td>:</td>
							<td width="81%">
							  <input name="d_op_first" type="text" id="d_op_first" maxlength="10" value="<?=$tglopmulai?>"/>
							  <!--<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_first,'dd/mm/yyyy',this)">-->
							s.d 
							<input name="d_op_last" type="text" id="d_op_last" maxlength="10" value="<?=$tglopakhir?>" />
							<!--<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_op_last,'dd/mm/yyyy',this)">-->
							</td>
						  </tr>
						</table>
				</td>
			  </tr>
			  <tr>
			  	<td>&nbsp;</td>
			  </tr>
			  <tr>
			  	<td>
			  	<div id="title-box2">Detail Item Barang</div>
			  	</td>
			  </tr>
			  <tr>
				<td><table width="80%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="2%" class="tdatahead">NO</td>
					<td width="10%" align="center" class="tdatahead">KODE BARANG</td>
					<td width="40%" align="center" class="tdatahead">NAMA BARANG</td>
					<td width="10%" align="center" class="tdatahead">AREA</td>
					<td width="5%" align="center" class="tdatahead">QTY OP</td>
				  </tr>
				  
				  <?php
				  $sumqty = 0;	  
				  if(sizeof($query)>0) {
					
				  	$no	= 1;
					$cc	= 1;
					
					
				  	foreach($query as $row) {

						$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
						$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";
												
						// ambil jml qty sesuai cabangnya
						$qjmlqty	= $this->mclass->jumqtypercabang($row->i_product,$var_ddofirst,$var_ddolast, $row->i_branch);
						if($qjmlqty->num_rows()>0) {
							$rjmlqty	= $qjmlqty->row();
							$jmlqty	= $rjmlqty->jmlqty;
						}
						else
							$jmlqty = 0;
						
						// ambil nama branch/inisial
						$qbranch	= $this->db->query(" SELECT e_initial FROM tr_branch
											WHERE i_branch_code = '".$row->i_branch."' ");
						if($qbranch->num_rows() > 0 ) {
							$rbranch	= $qbranch->row();
							$inisial	= $rbranch->e_initial;
						}
						else
							$inisial = '';
						
						$lopvsdo	.= "
							  <tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
								<td height=\"22px\" bgcolor=\"$bgcolor\">".$no.".&nbsp;</td>
								<td>".$row->i_product."</td>
								<td>".$row->e_product_name."</td>
								<td>".$inisial."</td>
								<td align=\"right\">".$jmlqty."</td>
							  </tr>";
						
						$no+=1; $cc+=1;
						$sumqty+= $jmlqty;
					}
					echo $lopvsdo;
				  }
				  
				  ?>
			  <tr>
			  	<td colspan="4" align="right">
				TOTAL
				</td>
				<td><?php echo $sumqty ?></td>
			  </tr>
			  <tr>
			  	<td colspan="5" align="right">
				&nbsp;
				</td>
			  </tr>
			  <tr>
			  	<td colspan="5" align="right">
					<input name="btnkeluar" id="btnkeluar" value="Keluar" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/listopperitembrg/cform/'">

				</td>
			  </tr>
			</table>

		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
