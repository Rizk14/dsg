<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:350px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function() {
	
});

</script>

<h3>Data Kelompok Unit</h3><br>
<a href="<?php echo base_url(); ?>index.php/mst-kel-unit/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>index.php/mst-kel-unit/cform/view">View Data</a>&nbsp;<br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-kel-unit/cform/view'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>

	
	<tr>
		<td>Unit Jahit</td>
		<td>: <select name="id_unit_jahit" id="id_unit_jahit">
				<option value="0">- All -</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" <?php if ($jht->kode_unit == $id_unit_jahit) { ?> selected <?php } ?> ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	
	<tr>
		<td>Unit Packing</td>
		<td>: <select name="id_unit_packing" id="id_unit_packing">
				<option value="0">- All -</option>
				<?php foreach ($list_unit_packing as $upk) { ?>
					<option value="<?php echo $upk->id ?>" <?php if ($upk->kode_unit == $id_unit_packing) { ?> selected <?php } ?> ><?php echo $upk->kode_unit."-".$upk->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	
	<tr>
		<td>Nama Kelompok</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ><!--&nbsp;&nbsp;<input type="submit" name="submit2" value="Export ke Excel" >-->
		</td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>

<form id="f_master_unit" name="f_master_unit">

<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		
		<th>Nama Kelompok</th>
		 <th>Unit Jahit</th>
		<th>Unit Packing</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				//print_r($query);
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;

				$exptgl1 = explode(" ", $tgl1);
				$tgl1nya= $exptgl1[0];
				
				$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1;

				
				
				// 10-10-2015
				if ($cari == '')
					$xcari = "all";
				else
					$xcari = $cari;
					
				
				 
				 echo "<tr class=\"record\">";
				
				 echo    "<td align='center'>".$query[$j]['nama_kelompok']."</td>";		
				 echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_kelompok'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_kelompok'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if($var_detail[$k]['kode_unit_jahit'] !=''){
						  echo $var_detail[$k]['kode_unit_jahit']." - ".$var_detail[$k]['nama_unit_jahit'];
						  if ($k<$hitung-1)
						     echo "<br> ";
						 }    
					}
				 }
				 echo "</td>";	
				  echo "<td style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_kelompok'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_kelompok'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if($var_detail[$k]['kode_unit_packing'] !=''){
						  echo $var_detail[$k]['kode_unit_packing']." - ".$var_detail[$k]['nama_unit_packing'];
						  if ($k<$hitung-1)
						     echo "<br> ";
						 }
					}
				 }
				 echo "</td>";
				 		  
				 echo    "<td align='center'>".$tgl_update."</td>";
				 
					 echo    "<td align=center>";
					 echo " <a href=".base_url()."index.php/mst-kel-unit/cform/edit/".$query[$j]['id']."/".$cur_page."/".$id_unit_jahit."/".$id_unit_packing."/".$xcari." \" id=\"".$query[$j]['id']."\">Edit</a>&nbsp;
					 <a href=".base_url()."index.php/mst-kel-unit/cform/deletesjmasuk/".$query[$j]['id']."/".$id_unit_jahit."/".$id_unit_packing."/".$xcari."/".$cur_page." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
				 
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
