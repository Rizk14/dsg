<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<div class="container">
<h2>Export Makloon Baju Gudang Jadi Wip</h2>
<hr>

<?php 
$attributes = array('name' => 'myform', 'id' => 'myform');
echo form_open('export_makloon_baju_gudang_jadi_wip/export_makloon_baju_gudang_jadi_wip/view', $attributes); ?>
 
       
   <div class="row">
        <?php echo form_label('Tanggal SJ dari &nbsp;&nbsp;&nbsp;:&nbsp;', 'tanggal_sj_dari', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback ">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
				<input id='tanggal_sj_dari' name='tanggal_sj_dari' value='<?php echo date_to_id($values->tanggal_sj_dari) ?>' class='form-control'
				placeholder="Tanggal SJ Dari" maxlength="10" onclick="displayCalendar(document.forms[0].tanggal_sj_dari,'dd-mm-yyyy',this)"></input>
                
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>
    
    <div class="row">
        <?php echo form_label('Tanggal SJ ke &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;', 'tanggal_sj_ke', array('class' => 'control-label ')) ?>
         <div class="form-group form-group-sm has-feedback ">        
            <div class="input-group date" data-date-format="dd-mm-yyyy">
               <input id='tanggal_sj_ke' name='tanggal_sj_ke' value='<?php echo date_to_id($values->tanggal_sj_ke) ?>' class='form-control'
				placeholder="Tanggal SJ Ke" maxlength="10" onclick="displayCalendar(document.forms[0].tanggal_sj_ke,'dd-mm-yyyy',this)"></input>
                
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>    
        
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="jenis_masuk" class="control-label">Jenis Masuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>
            <?php
                   $jenis_masuk = array(
                '' => '- Pilih -',
                '1' => 'Masuk dari Unit Jahit',
                '2' => 'Masuk dari Unit Packing', 
                '3' => 'Masuk dari Gudang Jadi Makloon',
                '4' => 'Masuk Lain-lain', 
            );
                    $atribut_jenis_masuk = 'class="form-control"';
            echo form_dropdown('jenis_masuk', $jenis_masuk, $values->jenis_masuk, $atribut_jenis_masuk);
            ?>
        </div>
        </div>
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="unit_jahit" class="control-label">Unit Jahit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="unit_jahit" name="unit_jahit">
			<option value="0"> - Pilih -</option>
		<?php foreach ($unit_jahit as $unj) {?> 
    <option value=<?php echo $unj->id ?>><?php echo $unj->nama_unit_jahit?></option>
    <?php } ?>
  </select>
        </div>
        </div>
                <div class="row">
        <div class="form-group has-feedback ">
            <label for="unit_packing" class="control-label">Unit packing &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="unit_packing" name="unit_packing">
			<option value="0"> - Pilih -</option>
		<?php foreach ($unit_packing as $unp) {?> 
    <option value=<?php echo $unp->id ?>><?php echo $unp->nama_unit_packing?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        
        <div class="row">
        <div class="form-group has-feedback ">
            <label for="gudang" class="control-label">Gudang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</label>

        <select class="form-control" id="gudang" name="gudang">
			<option value="0"> - Pilih -</option>
		<?php foreach ($gudang as $ung) {?> 
    <option value=<?php echo $ung->id ?>><?php echo $ung->nama_gudang?></option>
    <?php } ?>
  </select>
        </div>
        </div>
        
        <?php echo form_button(array('content'=>'View', 'type'=>'submit', 'class'=>'btn btn-info')) ;
echo form_close();
?>
        </div>
