

<?php
// Nomor urut data di tabel.
$per_page = 10;



if (empty($page)) {
    $offset = 0;
} else {
    $offset = ($page * $per_page - $per_page);
}
?>

<div class="container">
    <h2>Export Data Makloon baju gudang Jadi WIP</h2>
    <hr>

    <?php if (!empty($export_makloon_baju_gudang_jadi_wip) && is_array($export_makloon_baju_gudang_jadi_wip)): ?>
    <div class="row">
        <div class="col-md-12">
 <form action ="export_csv" name="myform" id='myform' class="form-inline" method="post">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>No</th>
                    <th>NO SJ</th>
                    <th>Tanggal SJ</th>
                    <th>Jenis Masuk</th>
                    <th>Keterangan</th>
                  
                </tr>
                </thead>
                <tbody>
					
                <?php 
                foreach($export_makloon_baju_gudang_jadi_wip as $row):
				 ?>
                    <tr>
						
                        <td><?php echo ++$offset ?></td>
						<td><?php echo $row->no_sj ?></td>
						<td><?php echo $row->tanggal_sj ?></td>
						<td><?php if($row->jenis_masuk == 1)
							echo "Masuk dari Unit Jahit" ;
							elseif ($row->jenis_masuk == 2)
							echo "Masuk dari Unit Packing" ;
							elseif ($row->jenis_masuk == 3)
							echo "Masuk dari Gudang Jadi Makloon" ;
							elseif ($row->jenis_masuk == 4)
							echo "Masuk Lain-lain" ;
               
						 ?></td>
						 
						 <td><?php echo $row->keterangan_header ?></td>    
                        
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
            <input type="hidden" name="tanggal_sj_dari" id="tanggal_sj_dari" value="<?php echo $tanggal_sj_dari ?>">
             <input type="hidden" name="tanggal_sj_ke" id="tanggal_sj_ke" value="<?php echo $tanggal_sj_ke ?>">
              <input type="hidden" name="jenis_masuk" id="jenis_masuk" value="<?php echo $jenis_masuk ?>">
               <input type="hidden" name="unit_jahit" id="unit_jahit" value="<?php echo $unit_jahit ?>">
                <input type="hidden" name="unit_packing" id="unit_packing" value="<?php echo $unit_packing ?>">
                 <input type="hidden" name="gudang" id="gudang" value="<?php echo $gudang ?>">
	<?php echo form_button(array('content'=>'Export CSV', 'type'=>'submit', 'class'=>'btn btn-info')) ?>
</form>

        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $export_makloon_baju_gudang_jadi_wip ?>
            </div>
        </div>
    </div>
    <?php endif ?>

</div> <!-- /container -->



