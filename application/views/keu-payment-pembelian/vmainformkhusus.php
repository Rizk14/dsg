<h3>Data Pembayaran Hutang Pembelian</h3><br> 
<a href="<? echo base_url(); ?>index.php/keu-payment-pembelian/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{	
	$("#no").val('2');
	
	$("#addrow").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****id_supplier*************************************
		var id_supplier="#id_supplier_"+n;
		var new_id_supplier="#id_supplier_"+no;
		$(id_supplier, lastRow).attr("id", "id_supplier_"+no);
		$(new_id_supplier, lastRow).attr("name", "id_supplier_"+no);		
		//*****end id_supplier*********************************
		
		//*****jum_bayar*************************************
		var jum_bayar="#jum_bayar_"+n;
		var new_jum_bayar="#jum_bayar_"+no;
		$(jum_bayar, lastRow).attr("id", "jum_bayar_"+no);
		$(new_jum_bayar, lastRow).attr("name", "jum_bayar_"+no);		
		$(new_jum_bayar, lastRow).val('0');		
		//*****end jum_bayar*********************************
		
		//*****jum_bayarx*************************************
		var jum_bayarx="#jum_bayarx_"+n;
		var new_jum_bayarx="#jum_bayarx_"+no;
		$(jum_bayarx, lastRow).attr("id", "jum_bayarx_"+no);
		$(new_jum_bayarx, lastRow).attr("name", "jum_bayarx_"+no);	
		var  even_blur= "$('#jum_bayar_"+no+ "').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; ";
		$(new_jum_bayarx, lastRow).val('0');
		$(new_jum_bayarx, lastRow).attr("onblur",even_blur);		
		//*****end jum_bayarx*********************************
		
		//*****subtotal*************************************
		var subtotal="#subtotal_"+n;
		var new_subtotal="#subtotal_"+no;
		$(subtotal, lastRow).attr("id", "subtotal_"+no);
		$(new_subtotal, lastRow).attr("name", "subtotal_"+no);		
		$(new_subtotal, lastRow).val('0');		
		//*****end subtotal*********************************
		
		//*****subtotalx*************************************
		var subtotalx="#subtotalx_"+n;
		var new_subtotalx="#subtotalx_"+no;
		$(subtotalx, lastRow).attr("id", "subtotalx_"+no);
		$(new_subtotalx, lastRow).attr("name", "subtotalx_"+no);	
		var  even_blur= "$('#subtotal_"+no+ "').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; ";
		$(new_subtotalx, lastRow).val('0');
		$(new_subtotalx, lastRow).attr("onblur",even_blur);		
		//*****end subtotalx*********************************
		
		//*****pembulatan*************************************
	/*	var pembulatan="#pembulatan_"+n;
		var new_pembulatan="#pembulatan_"+no;
		$(pembulatan, lastRow).attr("id", "pembulatan_"+no);
		$(new_pembulatan, lastRow).attr("name", "pembulatan_"+no);		
		$(new_pembulatan, lastRow).val('0');		
		//*****end pembulatan*********************************
		
		//*****jenis_pembulatan*************************************
		var jenis_pembulatan="#jenis_pembulatan_"+n;
		var new_jenis_pembulatan="#jenis_pembulatan_"+no;
		$(jenis_pembulatan, lastRow).attr("id", "jenis_pembulatan_"+no);
		$(new_jenis_pembulatan, lastRow).attr("name", "jenis_pembulatan_"+no); */
		//*****end jenis_pembulatan*************************************	
		
		//*****deskripsi*************************************
		var deskripsi="#deskripsi_"+n;
		var new_deskripsi="#deskripsi_"+no;
		$(deskripsi, lastRow).attr("id", "deskripsi_"+no);
		$(new_deskripsi, lastRow).attr("name", "deskripsi_"+no);		
		$(new_deskripsi, lastRow).val('');				
		//*****end deskripsi*************************************	
		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 670;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_payment() {
	var no_voucher= $('#no_voucher').val();
	var tgl_voucher= $('#tgl_voucher').val();
	
	var kon=window.confirm("yakin akan simpan data ??");
	if (kon){
		if (no_voucher == '') {
			alert("Nomor Voucher Pembayaran harus diisi..!");
			return false;
		}
		if (tgl_voucher == '') {
			alert("Tanggal Voucher Pembayaran harus dipilih..!");
			return false;
		}
		
		var jum= $('#no').val()-1; 

			for (var k=1; k <= jum; k++) {
				if ($('#subtotal_'+k).val() == '' || $('#subtotal_'+k).val() == '0') {
					alert("Data subtotal tagihan tidak boleh 0 / kosong...!");
					return false;
				}
				if ($('#jum_bayar_'+k).val() == '' || $('#jum_bayar_'+k).val() == '0') {
					alert("Data pembayaran tidak boleh 0 / kosong...!");
					return false;
				}
							
				if ($('#deskripsi_'+k).val() == '') {
					alert("Deskripsi harus diisi...!");
					return false;
				}
			}
	}
	else
		return false;
}

function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/submit" method="post" enctype="multipart/form-data">
<input type="hidden" name="jenis_pembelian" value="<?php echo $jenis_pembelian ?>">
<input type="hidden" name="is_makloon" value="<?php echo $is_makloon ?>">
<input type="hidden" name="is_khusus" value="1">
<input type="hidden" name="no" id="no" value="">
<div align="center">

<label id="status"></label>
<br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
    <td width="10%">Jenis Pembelian </td>
    <td>
      <?php if ($is_makloon == 't') $makloon = "(Pembelian Makloon)"; else $makloon=""; if ($jenis_pembelian == '1') echo "Cash ".$makloon; else echo "Kredit ".$makloon; ?>
    </td>
	
	<tr>
    <td>No Voucher </td>
    <td>
      <input name="no_voucher" type="text" id="no_voucher" size="10" maxlength="20" value="">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tgl Voucher</td>
    <td>
	<label>
      <input name="tgl_voucher" type="text" id="tgl_voucher" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_voucher" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_voucher,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2">
		<tr>
			<td colspan="5" align="right"><input id="addrow" type="button" name="addrow" value=" + ">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - ">
			</td>
		</tr>
        <tr>
          <th width="3%">No</th>
          <th>Suppliersd</th>
          <th>Subtotal</th>
	      <th>Jml Bayar</th>
	      <!--<th>Pembulatan</th>
	      <th>Jns Pembulatan</th> -->
	      <th>Deskripsi</th>
        </tr>
        
        <tr align="center">
          <td align="center" id="num_1">1</td>
          <td><select name="id_supplier_1" id="id_supplier_1">
				<?php foreach ($supplier as $sup) { ?>
					<option value="<?php echo $sup->id ?>" ><?php echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php } ?>
				</select></td>
         <td style="white-space:nowrap;">
           <input name="subtotalx_1" type="text" id="subtotalx_1" size="10" style="text-align:right;" value="0" onblur="javascript: $('#subtotal_1').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; " />
           <input type="hidden" name="subtotal_1" id="subtotal_1" value="0">
         </td>
         <td style="white-space:nowrap;">
           <input name="jum_bayarx_1" type="text" id="jum_bayarx_1" size="10" style="text-align:right;" value="0" onblur="javascript: $('#jum_bayar_1').val(this.value); var angka = formatMoney(this.value, 2,',','.'); this.value= angka; " />
           <input type="hidden" name="jum_bayar_1" id="jum_bayar_1" value="0">
         </td>
         <!--<td><input name="pembulatan_1" type="text" id="pembulatan_1" size="5" style="text-align:right;" value="0"/></td>
         <td><select name="jenis_pembulatan_1" id="jenis_pembulatan_1">
				<option value="0">Tidak Ada</option>
				<option value="1">Keatas</option>
				<option value="2">Kebawah</option>
			</select>
         </td>-->
         <td><input name="deskripsi_1" type="text" id="deskripsi_1" size="25" value="" /></td>
        </tr>
        
	</table>	
      </form>
      </td>
    </tr>
</table><br>
<input type="submit" name="submit" value="Simpan" onclick="return cek_payment();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/'">
</div>
</form>
