<h3>Data Pembayaran Hutang Pembelian</h3><br> 
<a href="<? echo base_url(); ?>index.php/keu-payment-pembelian/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/keu-payment-pembelian/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	
	//$("#no").val('2');
	
	//get_data_pkp();
	$('#topnya').hide();
			
	$('#pilih_faktur').click(function(){
			var kode_supplier= $("#kode_supplier").val();	
			var jenis_pembelian= $("#jenis_pembelian").val();	
			//var urlnya = "<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/show_popup_faktur/"+kode_supplier;
			var urlnya = "<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/show_popup_faktur/"+jenis_pembelian;
			openCenteredWindow(urlnya);
	  });
	  
	$('#kode_supplier').change(function(){
	  	    $("#no_faktur").val('');	
	  });
	
});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 670;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_item_faktur() {
	var no_faktur= $('#no_faktur').val();
	var jenis_pembelian= $('#jenis_pembelian').val();
	if (jenis_pembelian == '0') {
		alert("Jenis pembelian harus dipilih..!");
		return false;
	}
	if (no_faktur == '') {
		alert("Nomor Faktur harus dipilih..!");
		return false;
	}
}

function cek_payment() {
	// blm diedit
	var tgl_voucher= $('#tgl_voucher').val();

	if (tgl_voucher == '') {
		alert("Tanggal Voucher Pembayaran harus dipilih..!");
		return false;
	}
	
	//var jum= $('#no').val()-1; 
	var jum_subtotal= $('#jum_subtotal').val();

	if (jum_subtotal > 0) {
		for (var k=1; k <= jum_subtotal; k++) {
			if ($('#jum_bayar_'+k).val() == '' || $('#jum_bayar_'+k).val() == '0') {
				alert("Data pembayaran tidak boleh 0 / kosong...!");
				return false;
			}
			
			if ($('#deskripsi_'+k).val() == '') {
				alert("Deskripsi harus diisi...!");
				return false;
			}
			
			//alert($('#sisa_bayar_'+k).val());
			//alert($('#jum_bayar_'+k).val());
		} //return false;
	}
	else {
		alert("Data faktur tidak ada");
		return false;
	}
	
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php 
if ($go_proses == '') {
$attributes = array('name' => 'f_payment', 'id' => 'f_payment');
echo form_open('keu-payment-pembelian/cform/', $attributes); ?>
<table>
<!--	<tr>
			<td>Supplier</td>
			<td><select name="kode_supplier" id="kode_supplier" onkeyup="this.blur();this.focus();" >
				<?php //foreach ($supplier as $sup) { ?>
					<option value="<?php //echo $sup->kode_supplier ?>" ><?php //echo $sup->kode_supplier." - ". $sup->nama ?></option>
				<?php //} ?>
				</select>&nbsp;</td> 
	</tr>
-->		
	
	<tr>
		<td>Jenis Pembelian</td>
		<td><select name="jenis_pembelian" id="jenis_pembelian">
				<option value="0">-Pilih-</option>
				<option value="1">Cash</option>
				<option value="2">Kredit</option>
			</select>&nbsp;</td> 
	</tr>
	<tr>
		<td>Nomor Faktur</td>
		<td><input type="text" name="no_faktur" id="no_faktur" value="" size="30" maxlength="30" readonly="true">
		<input type="hidden" name="id_faktur" id="id_faktur" value=""> &nbsp;
		<input type="hidden" name="is_quilting" id="is_quilting" value="">
		<input type="hidden" name="is_jahit" id="is_jahit" value="">
		<input name="pilih_faktur" id="pilih_faktur" value="..." type="button" title="browse data faktur">
		</td>
	</tr>
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_item_faktur();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/view'">
<?php echo form_close(); 
} else { ?>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/submit" method="post" enctype="multipart/form-data">
<!--<input type="hidden" name="kode_sup" value="<?php //echo $kode_supplier ?>"> -->
<input type="hidden" name="jenis_pembelian" value="<?php echo $jenis_pembelian ?>">

<?php 
		if (count($faktur_detail)>0) {
			$no=1;
			foreach ($faktur_detail as $hitung) {
				$no++;
			}
		}
		else $no=2;
?>

<input type="hidden" name="no" id="no" value="<?php echo $no ?>">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
	<tr>
    <td width="10%">Jenis Pembelian </td>
    <td>
      <?php if ($jenis_pembelian == '1') echo "Cash"; else echo "Kredit"; ?>
    </td>
	
	<tr>
    <td>No Voucher </td>
    <td>
      <input name="no_voucher" type="text" id="no_voucher" size="20" maxlength="20" value="">&nbsp;<span id="msgbox" style="display:none"></span>
    </td>
    
  </tr>
  <tr>
    <td>Tgl Voucher</td>
    <td>
	<label>
      <input name="tgl_voucher" type="text" id="tgl_voucher" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_voucher" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_voucher,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td align="right">
			&nbsp;
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Supplier</th>
          <th>Nomor Faktur</th>
           <th>Tgl Faktur</th>
	      <th>Nominal Faktur</th>
	      <th>Jml Retur</th>
	      <th>Jml Bayar</th>
	      <th>Deskripsi</th>
	      <!--<th>Jml Yg Sdh Dibayar</th> 
	      <th>Jml Pembulatan</th>
	      <th>Jns Pembulatan</th>
	      <th>Jml Bayar</th> -->
        </tr>

        <?php $i=1;
        if (count($faktur_detail)==0) {
		?>
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td nowrap="nowrap">
		   Data tidak ada</td>
          
        </tr>
		
		<?php
		} else {
			$zz=1; $total_persupplier = 0; $item_faktur = ""; $item_is_quilting = ""; $item_is_jahit = "";
			$item_tgl_akhir = "";
			for($j=0;$j<count($faktur_detail);$j++){
			?>
			<tr align="center">
          <td align="center" id="num_<?php echo $i ?>"><?php echo $i ?></td>
          <td nowrap="nowrap">
			<input type="hidden" name="id_faktur_<?php echo $i ?>" id="id_faktur_<?php echo $i ?>" value="<?php echo $faktur_detail[$j]['id'] ?>">
            <input name="supplier_<?php echo $i ?>" type="text" id="supplier_<?php echo $i ?>" size="30" readonly="true" value="<?php echo $faktur_detail[$j]['kode_supplier']." - ".$faktur_detail[$j]['nama_supplier'] ?>"/>
            <input type="hidden" name="kode_supplier_<?php echo $i ?>" id="kode_supplier_<?php echo $i ?>" value="<?php echo $faktur_detail[$j]['kode_supplier'] ?>">
		 </td>
          <td nowrap="nowrap">
            <input name="no_faktur_<?php echo $i ?>" type="text" id="no_faktur_<?php echo $i ?>" size="15" readonly="true" value="<?php echo $faktur_detail[$j]['no_faktur'] ?>"/>
		 </td>
		 <?php
			//if ($i == 1) {
				$tgl_akhir = $faktur_detail[$j]['tgl_faktur'];
				$pisah1 = explode("-", $tgl_akhir);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				$tgl_akhir = $thn1."-".$bln1."-".$tgl1;
				$item_tgl_akhir.= $tgl_akhir.";";
			//}
		 ?>
          <td><input name="tgl_faktur_<?php echo $i ?>" type="text" id="tgl_faktur_<?php echo $i ?>" size="7" readonly="true" value="<?php echo $faktur_detail[$j]['tgl_faktur'] ?>" /></td>
          <td><input name="jum_hutang_<?php echo $i ?>" type="text" id="jum_hutang_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $faktur_detail[$j]['jum_hutang'] ?>" readonly="true" /></td>
          
          <td><input name="jum_retur_<?php echo $i ?>" type="text" id="jum_retur_<?php echo $i ?>" size="10" maxlength="10" 
          value="<?php echo $faktur_detail[$j]['tot_retur'] ?>" readonly="true" /></td>
          
         <!-- <td><input name="jum_gabung_<?php echo $i ?>" type="text" id="jum_gabung_<?php echo $i ?>" size="15" maxlength="15" 
          value="<?php //echo $faktur_detail[$j]['jum_gabung'] ?>" readonly="true" /></td>
          <td><input name="jum_pembulatan_<?php echo $i ?>" type="text" id="jum_pembulatan_<?php echo $i ?>" 
          size="10" maxlength="10" value="<?php //echo $faktur_detail[$j]['pembulatan'] ?>" readonly="true" />
          <td><select name="jenis_pembulatan_<?php echo $i ?>" id="jenis_pembulatan_<?php echo $i ?>" >
			<option value="0" >Tidak Ada</option>
			<option value="1" >Keatas</option>
			<option value="2" >Kebawah</option>
		</select></td>
          <td><input name="jum_bayar_<?php echo $i ?>" type="text" id="jum_bayar_<?php echo $i ?>" size="15" maxlength="15" value="<?php //echo $faktur_detail[$j]['jum_bayar'] ?>" />
          <input name="sisa_bayar_<?php echo $i ?>" type="hidden" id="sisa_bayar_<?php echo $i ?>" value="<?php //echo $faktur_detail[$j]['jum_bayar'] ?>" />
          </td> -->
        </tr>
        <?php 
			$item_faktur.= $faktur_detail[$j]['id'].";";
			$item_is_quilting.= $faktur_detail[$j]['is_quilting'].";";
			$item_is_jahit.= $faktur_detail[$j]['is_jahit'].";";
			$total_persupplier = $total_persupplier + $faktur_detail[$j]['jum_hutang'] - $faktur_detail[$j]['tot_retur'] ;
			if (isset($faktur_detail[$j+1]['kode_supplier']) && ($faktur_detail[$j]['kode_supplier'] != $faktur_detail[$j+1]['kode_supplier'])) {
				//$total_persupplier = number_format($total_persupplier, 2, '.','');
				
				// 1 nov 2011, hitung jml retur utk kode brg yg diluar range tgl faktur
				/* alurnya: cek tanggal awal dan tanggal akhir dari faktur2 yg dipilih, 
				 * apakah ada yg sama dgn yg di data retur. Kalo ada, maka cek di tabel retur_beli_detail 
				 * apakah ada kode brg yg is_luar_range = 't'. Jika ada maka masukkan ke jumlah returnya
				 * 
				 * */
				 $tgl_awal = $faktur_detail[$j]['tgl_faktur'];
				 $pisah1 = explode("-", $tgl_awal);
				 $tgl1= $pisah1[0];
				 $bln1= $pisah1[1];
				 $thn1= $pisah1[2];
				 $tgl_awal = $thn1."-".$bln1."-".$tgl1;
				 
				 $list_item_tgl_akhir = explode(";", $item_tgl_akhir);
				 $ambil_tgl_akhir = $list_item_tgl_akhir[0]; // ambil tgl akhir (thn-bln-tgl) yg dari awal
				 
				 // ===========================================================================================
				// 11-04-2012 perlu ada pengecekan apakah returnya itu pake acuan faktur atau bukan.
				$tot_retur_lain = 0;
			/*	$sqlcek = "SELECT count(b.id) FROM tm_retur_beli a, tm_retur_beli_faktur b 
							WHERE a.id = b.id_retur_beli AND b.no_faktur = '".$faktur_detail[$j]['no_faktur']."' 
							AND a.kode_supplier = '".$faktur_detail[$j]['kode_supplier']."' "; 
				$querycek = $this->db->query($sqlcek);
				if ($querycek->num_rows() > 0){ // ini utk data lama yg nota debetnya pake acuan faktur
				*/
				// 05-06-2012
				$pisah1 = explode("-", $tgl_awal);
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tgl_awal_modif = $thn1."-".$bln1."-01";
				
				//07-02-2013
				$pisah1 = explode("-", $ambil_tgl_akhir);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$bln1,1,$thn1);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				$tgl_akhir_modif = $thn1."-".$bln1."-".$lastDay;
				
					// 11-04-2012. range query diubah dari faktur_date_from = '$tgl_awal' menjadi faktur_date_from >= '$tgl_awal' AND faktur_date_to <= '$tgl_akhir'
					// 07-02-2013, $ambil_tgl_akhir ganti jadi $tgl_akhir_modif
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli 
										WHERE kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
										AND faktur_date_from >= '$tgl_awal_modif' AND faktur_date_to <= '$tgl_akhir_modif' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$id_retur	= $hasilrow->id;
						
						/*
						 * SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
							tm_retur_beli_faktur b, tm_retur_beli_detail c WHERE a.id = b.id_retur_beli
							AND b.id = c.id_retur_beli_faktur 
							AND b.no_faktur = '$no_faktur' AND a.kode_supplier = '$kode_supplier'
						 * 
						 * */
						
						// 07-02-2013, dikoment
						/*$query3	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$tot_retur_lain	= $hasilrow->tot_retur;
						}
						else
							$tot_retur_lain = 0; */
							
						//31-01-2013, cek di retur_beli_detail apakah ada item brg yg is_luar_range='t'. 
						//jika ada, maka cek acuan fakturnya dan liat status lunasnya 't' atau 'f'. 
						
						$query3	= $this->db->query(" SELECT id FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
						/*echo "SELECT id FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur'"; */
						if ($query3->num_rows() > 0){
							//$hasilrow = $query3->row();
							//$tot_retur_lain	= $hasilrow->tot_retur;
							$query31	= $this->db->query(" SELECT no_faktur FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$id_retur' ");
							if ($query31->num_rows() > 0){
								$hasilrow31 = $query31->row();
								$no_fakturcek	= $hasilrow31->no_faktur;
							
								$query32	= $this->db->query(" SELECT status_lunas FROM tm_pembelian_nofaktur 
											WHERE no_faktur = '$no_fakturcek' AND kode_supplier='".$faktur_detail[$j]['kode_supplier']."' ");
								if ($query32->num_rows() > 0){ // ini utk faktur pembelian non-quilting
									$hasilrow32 = $query32->row();
									$status_lunas	= $hasilrow32->status_lunas;
									
									if ($status_lunas == 'f') {
										$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
														WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
										if ($query33->num_rows() > 0){
											$hasilrow33 = $query33->row();
											$tot_retur_lain	= $hasilrow33->tot_retur;
										}
										else
											$tot_retur_lain = 0;
									}
									else
										$tot_retur_lain = 0;
								}
								else { // ini faktur makloon quilting
									$query32	= $this->db->query(" SELECT status_lunas FROM tm_faktur_makloon 
												WHERE no_faktur = '$no_fakturcek' AND kode_unit='".$faktur_detail[$j]['kode_supplier']."' ");
									if ($query32->num_rows() > 0){
										$hasilrow32 = $query32->row();
										$status_lunas	= $hasilrow32->status_lunas;
										
										if ($status_lunas == 'f') {
											$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
															WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
											if ($query33->num_rows() > 0){
												$hasilrow33 = $query33->row();
												$tot_retur_lain	= $hasilrow33->tot_retur;
											}
											else
												$tot_retur_lain = 0;
										}
										else
											$tot_retur_lain = 0;
									} // ENDDDD
								}
							}
						}
						else
							$tot_retur_lain = 0;
							
						// ================================== end 07-02-2013 ====================================
					}
					else {
						$id_retur	= 0;
						$tot_retur_lain = 0;
					}
			//	} // end if querycek
			/*	else {
					// cek dari tgl awal
					$list_faktur = explode(";", $item_faktur);
					for($oi=0; $oi<count($list_faktur)-1; $oi++){
						$list_faktur[$oi] = trim($list_faktur[$oi]);
						
						$query2 = $this->db->query(" SELECT b.no_sj, b.tgl_sj FROM tm_pembelian_nofaktur_sj a, tm_pembelian b, 
								tm_pembelian_detail c WHERE b.id = c.id_pembelian AND a.no_sj = b.no_sj 
								AND b.kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
								AND a.id_pembelian_nofaktur = '".$list_faktur[$oi]."' ORDER BY b.tgl_sj ASC ");
						if ($query2->num_rows() > 0){
							$hasil2=$query2->result();
							foreach ($hasil2 as $row2) {
								$query4	= $this->db->query(" SELECT id FROM tm_retur_beli 
													WHERE kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
													AND faktur_date_from >= '$row2->tgl_sj' AND faktur_date_to <= '$ambil_tgl_akhir' ");
								if ($query4->num_rows() > 0){
									$hasilrow4 = $query4->row();
									$id_retur	= $hasilrow4->id;
									$query3	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
													WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
									if ($query3->num_rows() > 0){
										$hasilrow3 = $query3->row();
										$tot_retur_lain	= $hasilrow3->tot_retur;
									}
									else
										$tot_retur_lain = 0;
								}
							} // end foreach
						} // end if
					} //end foreach
				} */
				// ===========================================================================================
					$tot_retur_lain = number_format($tot_retur_lain, 2, '.','');
					
					$total_persupplier = $total_persupplier-$tot_retur_lain;
					$total_persupplier = number_format($total_persupplier, 2, '.','');
		?>
			<tr>
				<td colspan="4" align="right">Jml Retur Diluar Range Tgl <input readonly="true" name="retur_lain_<?php echo $zz ?>" type="text" id="retur_lain_<?php echo $zz ?>" size="15" value="<?php echo $tot_retur_lain ?>" /> <?php echo $faktur_detail[$j]['nama_supplier'] ?> TOTAL</td>
				<td colspan="2"><input readonly="true" name="subtotal_<?php echo $zz ?>" type="text" id="subtotal_<?php echo $zz ?>" size="15" value="<?php echo $total_persupplier ?>" /></td>
				<input type="hidden" name="kode_supplier2_<?php echo $zz ?>" id="kode_supplier2_<?php echo $zz ?>" value="<?php echo $faktur_detail[$j]['kode_supplier'] ?>">
				<input type="hidden" name="item_faktur_<?php echo $zz ?>" id="item_faktur_<?php echo $zz ?>" value="<?php echo $item_faktur ?>">
				<input type="hidden" name="item_is_quilting_<?php echo $zz ?>" id="item_is_quilting_<?php echo $zz ?>" value="<?php echo $item_is_quilting ?>">
				<input type="hidden" name="item_is_jahit_<?php echo $zz ?>" id="item_is_jahit_<?php echo $zz ?>" value="<?php echo $item_is_jahit ?>">
				
				<td><input name="jum_bayar_<?php echo $zz ?>" type="text" id="jum_bayar_<?php echo $zz ?>" size="15" maxlength="20" value="<?php echo $total_persupplier ?>" /></td>
				<td><input name="deskripsi_<?php echo $zz ?>" type="text" id="deskripsi_<?php echo $zz ?>" size="25" value="" /></td>
			</tr>
		<?php		
				$total_persupplier = 0;
				$item_faktur = "";
				$item_is_quilting = "";
				$item_is_jahit = "";
				$item_tgl_akhir = "";
				$zz++;
			}
			
			if (!isset($faktur_detail[$j+1]['kode_supplier'])) {
				//echo "woi 07-02-2013 ";
				//$total_persupplier = number_format($total_persupplier, 2, '.','');
				
				 $tgl_awal = $faktur_detail[$j]['tgl_faktur'];
				 $pisah1 = explode("-", $tgl_awal);
				 $tgl1= $pisah1[0];
				 $bln1= $pisah1[1];
				 $thn1= $pisah1[2];
				 $tgl_awal = $thn1."-".$bln1."-".$tgl1;
				 
				 $list_item_tgl_akhir = explode(";", $item_tgl_akhir);
				 $ambil_tgl_akhir = $list_item_tgl_akhir[0]; // ambil tgl akhir (thn-bln-tgl) yg dari awal
				 				 
				/* $query2	= $this->db->query(" SELECT id FROM tm_retur_beli 
										WHERE kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
										AND faktur_date_from = '$tgl_awal' AND faktur_date_to = '$tgl_akhir' "); */
					
				// ===========================================================================================
				// 11-04-2012 perlu ada pengecekan apakah returnya itu pake acuan faktur atau bukan.
				$tot_retur_lain = 0;
				/*$sqlcek = "SELECT count(b.id) FROM tm_retur_beli a, tm_retur_beli_faktur b 
							WHERE a.id = b.id_retur_beli AND b.no_faktur = '".$faktur_detail[$j]['no_faktur']."' 
							AND a.kode_supplier = '".$faktur_detail[$j]['kode_supplier']."' "; echo $sqlcek; 
				$querycek = $this->db->query($sqlcek);
				if ($querycek->num_rows() > 0){ // ini utk data lama yg nota debetnya pake acuan faktur
				*/
				
				// 05-06-2012
				$pisah1 = explode("-", $tgl_awal);
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tgl_awal_modif = $thn1."-".$bln1."-01";
				/*$sqlnya = " SELECT id FROM tm_retur_beli 
										WHERE kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
										AND faktur_date_from >= '$tgl_awal' AND faktur_date_to <= '$ambil_tgl_akhir' ";
				echo $sqlnya; */
				
				//07-02-2013
				$pisah1 = explode("-", $ambil_tgl_akhir);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$bln1,1,$thn1);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				$tgl_akhir_modif = $thn1."-".$bln1."-".$lastDay;
				
				//ini yg lama pake $ambil_tgl_akhir. skrg pake variabel baru: $tgl_akhir_modif
				/*echo "SELECT id FROM tm_retur_beli 
										WHERE kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
										AND faktur_date_from >= '$tgl_awal_modif' AND faktur_date_to <= '$ambil_tgl_akhir'"; */
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli 
										WHERE kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
										AND faktur_date_from >= '$tgl_awal_modif' AND faktur_date_to <= '$tgl_akhir_modif' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$id_retur	= $hasilrow->id;
						
						/*
						 * SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
							tm_retur_beli_faktur b, tm_retur_beli_detail c WHERE a.id = b.id_retur_beli
							AND b.id = c.id_retur_beli_faktur 
							AND b.no_faktur = '$no_faktur' AND a.kode_supplier = '$kode_supplier'
						 * 
						 * */
						 
						 //31-01-2013, cek di retur_beli_detail apakah ada item brg yg is_luar_range='t'. 
						//jika ada, maka cek acuan fakturnya dan liat status lunasnya 't' atau 'f'. 
					/*	echo "SELECT id FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur'"; */
						$query3	= $this->db->query(" SELECT id FROM tm_retur_beli_detail 
										WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
						if ($query3->num_rows() > 0){
							//$hasilrow = $query3->row();
							//$tot_retur_lain	= $hasilrow->tot_retur;
							$query31	= $this->db->query(" SELECT no_faktur FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$id_retur' ");
							if ($query31->num_rows() > 0){
								$hasilrow31 = $query31->row();
								$no_fakturcek	= $hasilrow31->no_faktur;
							
								$query32	= $this->db->query(" SELECT status_lunas FROM tm_pembelian_nofaktur 
											WHERE no_faktur = '$no_fakturcek' AND kode_supplier='".$faktur_detail[$j]['kode_supplier']."' ");
								if ($query32->num_rows() > 0){ // ini utk faktur pembelian non-quilting
									$hasilrow32 = $query32->row();
									$status_lunas	= $hasilrow32->status_lunas;
									
									if ($status_lunas == 'f') {
										$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
														WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
										if ($query33->num_rows() > 0){
											$hasilrow33 = $query33->row();
											$tot_retur_lain	= $hasilrow33->tot_retur;
										}
										else
											$tot_retur_lain = 0;
									}
									else
										$tot_retur_lain = 0;
								}
								else { // ini faktur makloon quilting
									$query32	= $this->db->query(" SELECT status_lunas FROM tm_faktur_makloon 
												WHERE no_faktur = '$no_fakturcek' AND kode_unit='".$faktur_detail[$j]['kode_supplier']."' ");
									if ($query32->num_rows() > 0){
										$hasilrow32 = $query32->row();
										$status_lunas	= $hasilrow32->status_lunas;
										
										if ($status_lunas == 'f') {
											$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
															WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
											if ($query33->num_rows() > 0){
												$hasilrow33 = $query33->row();
												$tot_retur_lain	= $hasilrow33->tot_retur;
											}
											else
												$tot_retur_lain = 0;
										}
										else
											$tot_retur_lain = 0;
									} // ENDDDD
								}
							} //
							else {
								// 09-02-2013, jika ga ada acuan faktur sama sekali, maka kita anggap status lunasnya f
								$query33	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
														WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
								if ($query33->num_rows() > 0){
									$hasilrow33 = $query33->row();
									$tot_retur_lain	= $hasilrow33->tot_retur;
								}
								else
									$tot_retur_lain = 0;
							}
						}
						else
							$tot_retur_lain = 0;
						
					}
					else {
						$id_retur	= 0;
						$tot_retur_lain = 0;
					}
				//} // end if querycek
			/*	else {
					// cek dari tgl awal
					$list_faktur = explode(";", $item_faktur);
					for($oi=0; $oi<count($list_faktur)-1; $oi++){
						$list_faktur[$oi] = trim($list_faktur[$oi]);
						
						$query2 = $this->db->query(" SELECT b.no_sj, b.tgl_sj FROM tm_pembelian_nofaktur_sj a, tm_pembelian b, 
								tm_pembelian_detail c WHERE b.id = c.id_pembelian AND a.no_sj = b.no_sj 
								AND b.kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
								AND a.id_pembelian_nofaktur = '".$list_faktur[$oi]."' ORDER BY b.tgl_sj ASC ");
						if ($query2->num_rows() > 0){
							$hasil2=$query2->result();
							foreach ($hasil2 as $row2) {
								$query4	= $this->db->query(" SELECT id FROM tm_retur_beli 
													WHERE kode_supplier = '".$faktur_detail[$j]['kode_supplier']."'
													AND faktur_date_from >= '$row2->tgl_sj' AND faktur_date_to <= '$ambil_tgl_akhir' ");
								if ($query4->num_rows() > 0){
									$hasilrow4 = $query4->row();
									$id_retur	= $hasilrow4->id;
									$query3	= $this->db->query(" SELECT sum(qty*harga) as tot_retur FROM tm_retur_beli_detail 
													WHERE is_luar_range = 't' AND id_retur_beli = '$id_retur' ");
									if ($query3->num_rows() > 0){
										$hasilrow3 = $query3->row();
										$tot_retur_lain	= $hasilrow3->tot_retur;
									}
									else
										$tot_retur_lain = 0;
								}
							} // end foreach
						} // end if
					} //end foreach
				} */
				// ===========================================================================================
				
				$tot_retur_lain = number_format($tot_retur_lain, 2, '.','');
					
					$total_persupplier = $total_persupplier-$tot_retur_lain;
					$total_persupplier = number_format($total_persupplier, 2, '.','');
        ?>
			<tr>
				<td colspan="4" align="right">Jml Retur Diluar Range Tgl <input readonly="true" name="retur_lain_<?php echo $zz ?>" type="text" id="retur_lain_<?php echo $zz ?>" size="15" value="<?php echo $tot_retur_lain ?>" /> <?php echo $faktur_detail[$j]['nama_supplier'] ?> TOTAL</td>
				<td colspan="2"><input readonly="true" name="subtotal_<?php echo $zz ?>" type="text" id="subtotal_<?php echo $zz ?>" size="15" value="<?php echo $total_persupplier ?>" /></td>
				<input type="hidden" name="kode_supplier2_<?php echo $zz ?>" id="kode_supplier2_<?php echo $zz ?>" value="<?php echo $faktur_detail[$j]['kode_supplier'] ?>">
				<input type="hidden" name="item_faktur_<?php echo $zz ?>" id="item_faktur_<?php echo $zz ?>" value="<?php echo $item_faktur ?>">
				<input type="hidden" name="item_is_quilting_<?php echo $zz ?>" id="item_is_quilting_<?php echo $zz ?>" value="<?php echo $item_is_quilting ?>">
				<input type="hidden" name="item_is_jahit_<?php echo $zz ?>" id="item_is_jahit_<?php echo $zz ?>" value="<?php echo $item_is_jahit ?>">
				
				<td><input name="jum_bayar_<?php echo $zz ?>" type="text" id="jum_bayar_<?php echo $zz ?>" size="15" maxlength="20" value="<?php echo $total_persupplier ?>" /></td>
				<td><input name="deskripsi_<?php echo $zz ?>" type="text" id="deskripsi_<?php echo $zz ?>" size="25" value="" /></td>
			</tr>
        <?php 
				$total_persupplier = 0;
				$item_faktur = "";
				$item_is_quilting = "";
				$item_is_jahit = "";
				$item_tgl_akhir = "";
				$zz++;
			}
        ?>
        
		<?php $i++; } // end foreach 
		}
		?>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<input type="hidden" name="jum_subtotal" id="jum_subtotal" value="<?php echo $zz-1 ?>">
			<td colspan="9" align="center"><input type="submit" name="submit" value="Simpan" onclick="return cek_payment();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/keu-payment-pembelian/cform/'"></td>
		</tr>
	</table>	
	<br>
      </form>
      </td>
    </tr>

</table>
</div>
</form>
<?php } ?>
