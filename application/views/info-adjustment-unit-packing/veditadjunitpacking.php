<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>


<h3>Informasi Adjustment Barang WIP (Hasil packing) Di Unit packing</h3><br><br>

<div>
Lokasi Unit packing: <?php echo $query[0]['kode_unit']." - ".$query[0]['nama_unit'] ?><br>
Periode: <?php echo $query[0]['nama_bln']." ".$query[0]['tahun'] ?><br>
Total Data = <?php echo $jum_total ?><br><br>
<?php 
$attributes = array('name' => 'f_opname', 'id' => 'f_opname');
echo form_open('info-adjustment-unit-packing/creport/updateadjunitpacking', $attributes);
$no = count($query);
 if ($ctahun == '') $ctahun = "0";
 $url_redirectnya = "info-adjustment-unit-packing/creport/viewadjunitpacking/".$cgudang."/".$cbulan."/".$ctahun."/".$cur_page;
 ?>
<input type="hidden" name="jum_data" id="jum_data" value="<?php echo $jum_total ?>">
<input type="hidden" name="id_adj" id="id_adj" value="<?php echo $id_adj ?>">

<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="cgudang" value="<?php echo $cgudang ?>">
<input type="hidden" name="cbulan" value="<?php echo $cbulan ?>">
<input type="hidden" name="ctahun" value="<?php echo $ctahun ?>">
<input type="hidden" name="id_unit" id="id_unit" value="<?php echo $query[0]['id_unit'] ?>">

<table border="0">
<tr>
	<td>Tanggal Pencatatan Adjustment</td>
	<td>: <input name="tgl_adj" type="text" id="tgl_adj" size="10" value="<?php echo $query[0]['tgl_adj'] ?>" readonly="true">
	   <img alt="" id="tgl_adj" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_adj,'dd-mm-yyyy',this)"></td>
</tr>

</table>
<br>

<input type="submit" name="submit" value="Update Adjustment" onclick="return confirm('Apakah anda yakin simpan data ini? Proses simpan ini otomatis mengupdate stok');">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'">
<br><br>
<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No </th>
		 <th>Kode</th>
		 <th>Nama Barang WIP</th>
		 <!--<th>Stok Akhir</th>-->
		 <th>Jumlah Fisik</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			$i = 1;
			if (is_array($query)) {
				for($j=0;$j<count($query);$j++){
					 echo "<tr class=\"record\">";
					 echo "<td align='center'>$i</td>";
					 echo    "<td>".$query[$j]['kode_brg_wip']."
					 <input type='hidden' name='id_brg_wip_$i' id='id_brg_wip_$i' value='".$query[$j]['id_brg_wip']."'>
					 <input type='hidden' name='id_$i' id='id_$i' value='".$query[$j]['id']."'>
					 </td>";
					 echo    "<td>".$query[$j]['nama_brg_wip']."</td>";
					 
					/* echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
							for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp; : ".$detailwarna[$zz]['saldo_akhir']."<br>";
						}
					}
					 echo "</td>"; */
					 
					 echo "<td style='white-space:nowrap;' align='right'>";
					 if (is_array($query[$j]['detail_warna'])) {
						$detailwarna = $query[$j]['detail_warna'];
						for($zz=0;$zz<count($detailwarna);$zz++){ ?>
							<span style="white-space:nowrap;">
							<?php echo $detailwarna[$zz]['nama_warna']."&nbsp;";
						?>
						<input type="hidden" name="id_warna_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['id_warna'] ?>">
						
						<input type="text" name="stok_fisik_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['adjustment'] ?>" size="3" style="text-align: right;" onblur="javascript: if(this.value == '') this.value='0'; if(isNaN(this.value)) this.value='0';">
						
						<input type="hidden" name="jum_stok_<?php echo $i ?>[]" value="<?php echo $detailwarna[$zz]['saldo_akhir'] ?>"><br>
						<?php
						}
					}
					 echo "</td>";

					 echo  "</tr>";
					 $i++;
				}
			}
			
		
		 ?>
		
 	</tbody>
</table>


<?php echo form_close();  ?>
</div>
