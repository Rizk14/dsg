<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo
{
	background-color:#DDD;
	max-width:350px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<h3>Informasi Adjustment Barang WIP (unit packing) Di Unit packing</h3><br><br>

<div>

Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('info-adjustment-unit-packing/creport/viewadjunitpacking'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	
	<tr>
		<td width="20%" style="white-space:nowrap;">unit packing</td>
		<td style="white-space:nowrap;">: <select name="id_unit" id="id_unit">
				<option value="0">-All-</option>
				<?php foreach ($list_unit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" <?php if ($jht->id == $cunit) { ?> selected <?php } ?> ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>

	<tr>
		<td style="white-space:nowrap;">Bulan</td>
		<td>: <select name="bulan" id="bulan">
				<option value="00" <?php if ($cbulan == "00") { ?> selected <?php } ?> >-All-</option>
				<option value="01" <?php if ($cbulan == "01") { ?> selected <?php } ?>>Januari</option>
				<option value="02" <?php if ($cbulan == "02") { ?> selected <?php } ?>>Februari</option>
				<option value="03" <?php if ($cbulan == "03") { ?> selected <?php } ?>>Maret</option>
				<option value="04" <?php if ($cbulan == "04") { ?> selected <?php } ?>>April</option>
				<option value="05" <?php if ($cbulan == "05") { ?> selected <?php } ?>>Mei</option>
				<option value="06" <?php if ($cbulan == "06") { ?> selected <?php } ?>>Juni</option>
				<option value="07" <?php if ($cbulan == "07") { ?> selected <?php } ?>>Juli</option>
				<option value="08" <?php if ($cbulan == "08") { ?> selected <?php } ?>>Agustus</option>
				<option value="09" <?php if ($cbulan == "09") { ?> selected <?php } ?>>September</option>
				<option value="10" <?php if ($cbulan == "10") { ?> selected <?php } ?>>Oktober</option>
				<option value="11" <?php if ($cbulan == "11") { ?> selected <?php } ?>>November</option>
				<option value="12" <?php if ($cbulan == "12") { ?> selected <?php } ?>>Desember</option>
			</select></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;">Tahun</td>
		<td>: <input name="tahun" type="text" id="tahun" size="4" value="<?php echo $ctahun ?>" maxlength="4"></td>
	</tr>
	
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>

<table border="1" cellpadding= "1" cellspacing = "1" width="80%">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		<th width="20%">Periode (bulan-tahun)</th>
		 <th>Gudang</th>
		
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				if ($startnya == '')
					$i=1;
				else
					$i = $startnya+1; 
			 for($j=0;$j<count($query);$j++){
				if ($query[$j]['tgl_update'] != '') {
					$pisah1 = explode("-", $query[$j]['tgl_update']);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					
					$exptgl1 = explode(" ", $tgl1);
					$tgl1nya= $exptgl1[0];
					$jam1nya= $exptgl1[1];
					$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				}
				else
					$tgl_update = "&nbsp;";
				 
				
				 
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['nama_bln']." ".$query[$j]['tahun']."</td>";
				 echo    "<td style='white-space:nowrap;'>".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
		
				 echo    "<td align='center'>".$tgl_update."</td>";
				 
				 echo    "<td align=center>";
				 echo "<a href=".base_url()."index.php/info-adjustment-unit-packing/creport/editadjunitpacking/".$query[$j]['id']."/".$cur_page."/".$cunit."/".$cbulan."/".$ctahun." \"  id=\"".$query[$j]['id']."\">Edit</a>&nbsp; ";
				 echo "</td>";
		
				 echo  "</tr>";
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
