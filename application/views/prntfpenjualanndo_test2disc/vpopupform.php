<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title></title>
<style type="text/css" media="screen, print">
body {
	background:#FFFFFF;
	color:black;
	margin:0;
}

#tbl_utama {
	width:1060px;
}

#tbl_utama2 {
	width:1050px;
	vertical-align:top;
}
</style>

<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 10000;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>

</head>

<body onload="window.print(); closeMe();">

<?php
include_once("funcs/terbilang.php");
?>

<table>
  <tr>
    <td valign="top"><table border="0" cellspacing="0" cellpadding="0" width="1080px;">
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border-bottom:#000000 2px solid; line-height:18px;"></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="47%" align="left" style="line-height:35px;"><span style="font-family:Times New Roman, Times, serif; font-size:28px;">Faktur Penjualan</span></td>
              <td width="53%" align="right"><span style="font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold; text-align:right;"><?php echo strtoupper($nminitial); ?></span></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border-bottom:#000000 2px solid; line-height:18px;"></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="47%" align="left" style="line-height:25px;"><span style="font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold;">Nomor Faktur</span><span style="font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold;"> <?php echo $nofaktur; ?></span></td>
              <td width="53%" align="right" style="line-height:25px;"><span style="font-family:Times New Roman, Times, serif; font-size:15px; font-weight:bold;">Tanggal Faktur</span> <span style="font-family:Times New Roman, Times, serif; font-size:13px;"><?php echo $tglfaktur; ?></span></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="47%"><table width="40%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><span style="font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold; line-height:25px;">Kepada :</span><br />
                        <span style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:25px;"><?php echo $nmcabang; ?><br /><?php echo $alamatcabang; ?></span></td>
                  </tr>
              </table></td>
              <td width="53%" align="right"><table width="80%" border="0" cellspacing="0" cellpadding="0" style="border-top:#000000 1px solid; border-bottom:#000000 1px solid; border-left:#000000 1px solid; border-right:#000000 1px solid; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                  <tr>
                    <td height="60px;" valign="top"><span style="font-family:Arial, Helvetica, sans-serif; font-size:11px; padding-left:4px;">Keterangan&nbsp;:</span></td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr style="font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold; text-align:center; vertical-align:middle; line-height:25px;">
              <td width="4%" align="center" style="border-bottom:#000000 1px solid; border-top:#000000 1px solid;">No.</td>
              <td width="20%" align="center" style="border-bottom:#000000 1px solid; border-top:#000000 1px solid;">Kode Barang</td>
              <td width="51%" align="center" style="border-bottom:#000000 1px solid; border-top:#000000 1px solid;">Nama Barang</td>
              <td width="10%" align="center" style="border-bottom:#000000 1px solid; border-top:#000000 1px solid;">Unit</td>
              <td width="10%" align="center" style="border-bottom:#000000 1px solid; border-top:#000000 1px solid;">Harga</td>
              <td width="11%" align="center" style="border-bottom:#000000 1px solid; border-top:#000000 1px solid;">Jumlah</td>
            </tr>
      <?php
	  $no	= 1;
	  foreach($isi as $row) {
	  	if($no<20) {
			/*
			  $lfaktur	.= "
			  <tr>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$no."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$row->imotif."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">".$row->motifname."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\" align=\"right\">".$row->qty."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\" align=\"right\">".number_format($row->unitprice)."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\" align=\"right\">".number_format($row->amount)."&nbsp;</td>
			  </tr>";
		  	*/
			  $lfaktur	.= "
			  <tr>
				<td style=\"font-family:Arial; font-size:10px; line-height:18px;\">".$no."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; line-height:18px;\">".$row->imotif."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; line-height:18px;\">".$row->motifname."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; line-height:18px;\" align=\"right\">".$row->qty."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; line-height:18px;\" align=\"right\">".number_format($row->unitprice)."&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; line-height:18px;\" align=\"right\">".number_format($row->amount)."&nbsp;</td>
        <td style=\"font-family:Arial; font-size:10px; line-height:18px;\" align=\"right\">".number_format($row->ndisc)."&nbsp;</td>
        <td style=\"font-family:Arial; font-size:10px; line-height:18px;\" align=\"right\">".number_format($row->vdisc)."&nbsp;</td>
			  </tr>";			
	  	}
	  $no+=1;
	  }
	  echo $lfaktur;
	  
	  /***
	  $akhr	= $no;
	  if($akhr<=20) {
	  	for($loop=$akhr;$loop<=20; $loop++) {
			if($loop==20) {		
			  echo "
			  <tr>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">&nbsp;</td>
				<td style=\"font-family:Arial; font-size:10px; border-bottom:#000000 1px solid; line-height:18px;\">&nbsp;</td>
			  </tr>";
		  	} else {
			  echo "
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>";			
			}
		}
	  }
	  ***/
	  ?>
	  		<tr>
				<td colspan="6" style="border-bottom:#000000 1px solid; line-height:18px;">&nbsp;</td>
			</tr>  
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="47%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><span style="font-family:Times New Roman, Times, serif; font-size:15px; font-weight:bold; line-height:25px;">Tgl Jatuh Tempo :</span> <span style="font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:25px;"><?php echo $tgljthtempo; ?></span></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:#000000 1px solid; border-bottom:#000000 1px solid; border-left:#000000 1px solid; border-right:#000000 1px solid; font-family:Arial, Helvetica, sans-serif; font-size:10px;">
                        <tr>
                          <td><span style="padding-left:5px;">Perhatian :</span>
                              <ol>
                                <li>Barang-barang yang sudah dibeli tidak dapat ditukar/ dikembalikan, kecuali ada perjanjian terlebih dahulu.</li>
                                <li>Faktur asli merupakan bukti pembayaran yang sah</li>
                                <li>Pembayaran dengan cek/giro baru dianggap sah setelah diuangkan</li>
                              </ol></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
              <td width="53%" align="right" valign="top"><table width="80%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="45%" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px;">Jumlah</td>
                    <td width="11%" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px;">Rp.</td>
                    <td width="44%" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:25px;"><?php echo number_format($jumlah); ?>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px; border-bottom:#000000 1px solid;">Diskon</td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px; border-bottom:#000000 1px solid;">Rp.</td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:25px; border-bottom:#000000 1px solid;"><?php echo number_format($diskon); ?>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px;">DPP</td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px;">Rp.</td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:25px;"><?php echo number_format($dpp); ?>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px; border-bottom:#000000 1px solid;">PPN</td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px; border-bottom:#000000 1px solid;">Rp.</td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:25px; border-bottom:#000000 1px solid;"><?php echo number_format($nilai_ppn); ?>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px;">Nilai Faktur </td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px;">Rp.</td>
                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; line-height:25px;"><?php echo number_format($nilai_faktur); ?>&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:25px;">(
            <?php Terbilang($nilai_faktur); ?> Rupiah
          )</td>
      </tr>
      <tr>
        <td><table width="35%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="31%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold; vertical-align:middle; line-height:35px;">SE<?php echo chr(199); ?>O</td>
                  </tr>
                  <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; vertical-align:middle; line-height:60px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold; vertical-align:middle;">Kasie. Accounting</td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>
      <?php
      $akhr=$no;
      if($akhr<=20){
      	for($loop=$akhr;$loop<=20;$loop++) {
      		if($loop==20){
            	echo "
            	<tr>
              		<td>&nbsp;</td>
            	</tr>";		
          	} else {
            	echo "
            	<tr>
             	 	<td>&nbsp;</td>
            	</tr>";
          }	
        }
      }
      ?>	  
	  <tr>
		<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border-bottom:#000000 2px solid; line-height:60px;">&nbsp;</td>
	  </tr>	  
    </table></td>
  </tr>
</table>
</body>
</html>