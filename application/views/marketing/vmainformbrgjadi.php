<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript">

function openCenteredWindow(url) {
		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

function cek_data() {
	var kode= $('#kode').val();
	var nama= $('#nama').val();

	if (kode == '') {
		alert("Kode Barang harus diisi..!");
		$('#kode').focus();
		return false;
	}
	if (nama == '') {
		alert("Nama Barang harus diisi..!");
		$('#nama').focus();
		return false;
	}
	
	if (kode.search(" ") > 0) {
		alert("Kode barang tidak boleh ada spasi kosong..!");
		$('#kode').focus();
		return false;
	}
	
}
	
</script>

<h3>Data Barang Jadi</h3><br>
<a href="<? echo base_url(); ?>index.php/marketing/cform/viewbrgjadi">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<?php 
	$attributes = array('name' => 'f_brg', 'id' => 'f_brg');
	echo form_open('marketing/cform/submitbrgjadi', $attributes); ?>
<?php if ($edit == '1') { ?> <input type="hidden" name="goedit" value="1"> <input type="hidden" name="kodeedit" value="<?php echo $ekode ?>"><?php } ?>
	<table>
		<tr>
			<td>Kode Barang</td>
			<td> : <input type="text" name="kode" id="kode" value="<?php echo $ekode ?>" maxlength="8" size="10" <?php if ($edit == '1') { ?> readonly="true" <?php } ?> > *<i>8 digit, contoh: DGT7226, DGTA7225</i>
			</td>
		</tr>
		<tr>
			<td>Nama Barang</td>
			<td> : <input type="text" name="nama" id="nama" value="<?php echo str_replace("\"", "&quot;", $enama_brg) ?>" maxlength="100" size="50" onkeyup="var nama=this.value; var ganti= nama.toUpperCase(); this.value=ganti;"></td>
		</tr>
				
		<tr>
			<td>Kelompok Brg Jadi</td>
			<td> : <select name="kel_brg_jadi" id="kel_brg_jadi"> 
						<option value="0" <?php if ($ekel_brg_jadi == '0') { ?> selected <?php } ?> > -Pilih- </option>
						<?php
						foreach($list_kelbrgjadi as $kel) {
							//echo "<option value=".$kel->id.">".$kel->nama."</option>";
						?>
							<option value="<?php echo $kel->id ?>" <?php if ($ekel_brg_jadi == $kel->id) { ?> selected <?php } ?>><?php echo $kel->nama ?></option>
						<?php
						}
						?>
					</select>
			</td>
		</tr>
		
		<tr>
			<td colspan="2"><?php if ($edit == '') { ?><input type="submit" name="submit" value="Simpan" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/marketing/cform/viewbrgjadi'">
			<?php } else { ?>
			<input type="submit" name="submit" value="Edit" onclick="return cek_data();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/marketing/cform/viewbrgjadi'"><?php } ?> </td>
		</tr>
	</table>
<?php echo form_close(); ?> <br>
