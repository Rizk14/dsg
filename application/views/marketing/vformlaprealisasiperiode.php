<h3>Laporan Data Realisasi FC vs OPvsDO Periode</h3><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	var bulan1 = parseInt($('#bulan1').val());
	var bulan2 = parseInt($('#bulan2').val());
	
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}
	
	if (bulan1 > bulan2) {
		alert("Bulan awal harus lebih kecil dari bulan akhir ..!");
		return false;
	}
	
}
</script>

<?php 
$attributes = array('name' => 'f_realisasi', 'id' => 'f_realisasi');
echo form_open('marketing/creport/viewlaprealisasiperiode', $attributes); ?>
<table width="60%">
	<tr>
		<td width="20%">Pelanggan</td>
		<td> <select name="i_customer" id="i_customer">
				<option value="0">- Semua -</option>
				<?php foreach ($list_pelanggan as $cust) { ?>
					<option value="<?php echo $cust->i_customer ?>" ><?php echo $cust->i_customer_code." - ".$cust->e_customer_name ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Dari Bulan</td>
		<td> <select name="bulan1" id="bulan1">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> &nbsp;&nbsp; s.d. Bulan <select name="bulan2" id="bulan2">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select>
		</td>
  </tr>	
	
	<tr>
		<td>Tahun</td>
		<td>
		<input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">

		</td>
  </tr>

</table><br>
<input type="submit" name="submit" value="View" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/marketing/creport/laprealisasiperiode'">
<?php echo form_close();  ?>
