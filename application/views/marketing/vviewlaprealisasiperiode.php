<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Data Realisasi FC vs OPvsDO Periode</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div>

Pelanggan: <?php if ($i_customer!= 0) { echo $e_customer_name; } else echo "Semua"; ?><br>
Periode: <?php echo $namabulan1 ?> s.d. <?php echo $namabulan2." ".$tahun ?><br><br>

<?php 
$attributes = array('name' => 'f_laprealisasi', 'id' => 'f_laprealisasi');
echo form_open('marketing/creport/export_excel_laprealisasiperiode', $attributes); ?>
<input type="hidden" name="bulan1" value="<?php echo $bulan1 ?>" >
<input type="hidden" name="bulan2" value="<?php echo $bulan2 ?>" >
<input type="hidden" name="namabulan1" value="<?php echo $namabulan1 ?>" >
<input type="hidden" name="namabulan2" value="<?php echo $namabulan2 ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="i_customer" value="<?php echo $i_customer ?>" >
<input type="hidden" name="e_customer_name" value="<?php echo $e_customer_name ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<b>Barang Reguler</b>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<tbody>
		<?php
			$i = 1;
			if (is_array($query)) {
				$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0;$sumdo=0;
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
				//print_r($query);
				//$tcdata=0; $tsumfc=0; $tsumop=0; $tsumdo=0;
				//$tsumpersendovsop=0; $tsumpersenopvsfc=0; $tsumpersendovsfc=0; $tcdatapersendovsop=0;
				
				// 29-04-2015
				for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
					if (strlen($xx) < 2)
						$bulannya = "0".$xx;
					else
						$bulannya = $xx;
					
					$tsumalldatabulan[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													//'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													);
				}
				
				for($j=0;$j<count($query);$j++){
					$databulanan = $query[$j]['databulanan'];
					$jmlcolspan = (count($databulanan)*6)+3;
					if ($temp_kodekel != $query[$j]['kode_kel']) {
						$temp_kodekel = $query[$j]['kode_kel'];
				?>
					<tr>
						<td colspan="<?php echo $jmlcolspan; ?>">&nbsp;<b><?php echo $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ?></b></td>
					</tr>
					
					<tr class="judulnya">
						<th rowspan="2">No </th>
						<th rowspan="2">Kode</th>
						<th rowspan="2">Nama Barang Jadi</th>
				<?php
					for($xx=0; $xx<count($databulanan); $xx++){
				?>
					
						<th colspan="6"><?php echo $databulanan[$xx]['namabulanxx'] ?></th>
				<?php
					}
				?>
					 </tr>
					 <tr class="judulnya">
				<?php
					for($xx=0; $xx<count($databulanan); $xx++){
				?>
						<th>Qty FC</th>
						 <th>Qty OP</th>
						 <th>% OP-FC</th>
						 <th>Qty DO</th>
						 <th>% DO-FC</th>
						 <th>% DO-OP</th>
				<?php
					}
				?>
					</tr>
				<?php
				 }
					 
					 echo "<tr class=\"record\">";
					 echo "<td align='center' id='num_$i'>$i</td>";
		?>
					<td>&nbsp;<?php echo $query[$j]['kode_brg_jadi'] ?></td>
					<td>&nbsp;<?php echo $query[$j]['nama_brg_jadi'] ?></td>
					
				<?php
				for($xx=0; $xx<count($databulanan); $xx++){
				?>
					
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($databulanan[$xx]['fc'], 0, ',','.') ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo $databulanan[$xx]['jum_op'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php if ($databulanan[$xx]['persenopvsfc'] != '') { echo number_format($databulanan[$xx]['persenopvsfc'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					 <td align="right" style="white-space:nowrap;"><?php echo $databulanan[$xx]['jum_do'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php if ($databulanan[$xx]['persendovsfc'] != '') { echo number_format($databulanan[$xx]['persendovsfc'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					 <td align="right" style="white-space:nowrap;"><?php if ($databulanan[$xx]['persendovsop'] != '') { echo number_format($databulanan[$xx]['persendovsop'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					
		<?php		
					// ini menampung semua data di semua bulan. difilternya nanti dibawah
					$alldatabulan[] = array('bulanxx'=> $databulanan[$xx]['bulanxx'],
										'fc'=> $databulanan[$xx]['fc'],
										'jum_op'=> $databulanan[$xx]['jum_op'],
										'persenopvsfc'=> $databulanan[$xx]['persenopvsfc'],
										'jum_do'=> $databulanan[$xx]['jum_do'],
										'persendovsfc'=> $databulanan[$xx]['persendovsfc'],
										'persendovsop'=> $databulanan[$xx]['persendovsop']
										);
					
					if ($databulanan[$xx]['bulanxx'] == $tsumalldatabulan[$xx]['bulanxx']) {
						$tsumalldatabulan[$xx]['tsumfc']+= $databulanan[$xx]['fc'];
						$tsumalldatabulan[$xx]['tsumop']+= $databulanan[$xx]['jum_op'];
						$tsumalldatabulan[$xx]['tsumdo']+= $databulanan[$xx]['jum_do'];
					}
				}
					echo "</tr>";

					$i++;
					
					if (isset($query[$j+1]['kode_kel']) && ($query[$j]['kode_kel'] != $query[$j+1]['kode_kel'])) {
						// 28-04-2015
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
									
									/*$tsumfc+=$alldatabulan[$xx1]['fc'];
									$tsumop+=$alldatabulan[$xx1]['jum_op'];
									$tsumdo+=$alldatabulan[$xx1]['jum_do'];
									$tcdata++; */
																		
								} // end if
							} // end for alldatabulan
							
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
														
							/*$tsumpersendovsop+= $ratapersendovsop;
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsop++; */
							
							if ($bulannya == $tsumalldatabulan[$thitung]['bulanxx']) {
								/*$tsumalldatabulan[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													); */
								$tsumalldatabulan[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						} // end for bulan

		?>
					<tr>
						<td colspan="3" align="center"><b>TOTAL</b></td>
						<?php
							$hitung=0;
							for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
						?>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumfc']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumop']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersenopvsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumdo']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsop'], 2, '.',',') ?> %&nbsp;</b></td>
					
		<?php					$hitung++;
					}
					echo "</tr>";
					
						/*$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0; */
						$sumalldatabulan = array();
						$alldatabulan = array();
						$i=1;
					}
					else if (!isset($query[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						$ratapersendovsfc= $sumpersendovsfc/$cdata;
						
						$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++;
						
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersendovsop;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsop; */
						
						// ============ 29-04-2015 ===========================================
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
																		
								} // end if
							} // end for alldatabulan
							
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
																					
							if ($bulannya == $tsumalldatabulan[$thitung]['bulanxx']) {
								$tsumalldatabulan[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						}
						// ===================================================================
		?>
					<tr>
						<td colspan="3" align="center"><b>TOTAL</b></td>
						<?php
							$hitung=0;
							for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
						?>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumfc']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumop']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersenopvsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumdo']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsop'], 2, '.',',') ?> %&nbsp;</b></td>
					
						<?php
								$hitung++;
							}
							echo "</tr>";
						?>
						
						<tr>
							<td colspan="3" align="center"><b>TOTAL BARANG REGULER</b></td>
							<?php
								$thitung=0;
								for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
									$ratapersen = $tsumalldatabulan[$thitung]['tsumpersendovsop']/$tsumalldatabulan[$thitung]['tcdatapersendovsop'];
									$ratapersenopfc = $tsumalldatabulan[$thitung]['tsumpersenopvsfc']/$tsumalldatabulan[$thitung]['tcdatapersendovsop'];
									$ratapersendofc = $tsumalldatabulan[$thitung]['tsumpersendovsfc']/$tsumalldatabulan[$thitung]['tcdatapersendovsop'];
									
							?>
							<td align="right"><b><?php echo $tsumalldatabulan[$thitung]['tsumfc']; ?>&nbsp;</b></td>
							<td align="right"><b><?php echo $tsumalldatabulan[$thitung]['tsumop']; ?>&nbsp;</b></td>
							<td align="right"><b><?php echo number_format($ratapersenopfc, 2, '.',',') ?> %&nbsp;</b></td>
							<td align="right"><b><?php echo $tsumalldatabulan[$thitung]['tsumdo']; ?>&nbsp;</b></td>
							<td align="right"><b><?php echo number_format($ratapersendofc, 2, '.',',') ?> %&nbsp;</b></td>
							<td align="right"><b><?php echo number_format($ratapersen, 2, '.',',') ?> %&nbsp;</b></td>
							<?php
									$thitung++;
								}
								echo "</tr>";
							?>	
			<?php
					}
					 
				} // end for
			}
	?>
	
 	</tbody>
</table><br><br>
<?php
	//$tsumalldatabulan = array();
	$alldatabulan = array();
	$sumalldatabulan = array();
?>
<b>Barang STP</b>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<tbody>
		<?php
			$i = 1;
			if (is_array($query2)) {
				$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0;$sumdo=0;
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
				
				// 29-04-2015
				for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
					if (strlen($xx) < 2)
						$bulannya = "0".$xx;
					else
						$bulannya = $xx;
					
					$tsumalldatabulan2[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													//'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													);
				}
				
				for($j=0;$j<count($query2);$j++){
					$databulanan = $query2[$j]['databulanan'];
					$jmlcolspan = (count($databulanan)*6)+3;
					if ($temp_kodekel != $query2[$j]['kode_kel']) {
						$temp_kodekel = $query2[$j]['kode_kel'];
				?>
					<tr>
						<td colspan="<?php echo $jmlcolspan; ?>">&nbsp;<b><?php echo $query2[$j]['kode_kel']." - ".$query2[$j]['nama_kel'] ?></b></td>
					</tr>
					
					<tr class="judulnya">
						<th rowspan="2">No </th>
						<th rowspan="2">Kode</th>
						<th rowspan="2">Nama Barang Jadi</th>
				<?php
					for($xx=0; $xx<count($databulanan); $xx++){
				?>
					
						<th colspan="6"><?php echo $databulanan[$xx]['namabulanxx'] ?></th>
				<?php
					}
				?>
					 </tr>
					 <tr class="judulnya">
				<?php
					for($xx=0; $xx<count($databulanan); $xx++){
				?>
						<th>Qty FC</th>
						 <th>Qty OP</th>
						 <th>% OP-FC</th>
						 <th>Qty DO</th>
						 <th>% DO-FC</th>
						 <th>% DO-OP</th>
				<?php
					}
				?>
					</tr>
				<?php
				 }
					 
					 echo "<tr class=\"record\">";
					 echo "<td align='center' id='num_$i'>$i</td>";
		?>
					<td>&nbsp;<?php echo $query2[$j]['kode_brg_jadi'] ?></td>
					<td>&nbsp;<?php echo $query2[$j]['nama_brg_jadi'] ?></td>
					
				<?php
				for($xx=0; $xx<count($databulanan); $xx++){
				?>
					
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($databulanan[$xx]['fc'], 0, ',','.') ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo $databulanan[$xx]['jum_op'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php if ($databulanan[$xx]['persenopvsfc'] != '') { echo number_format($databulanan[$xx]['persenopvsfc'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					 <td align="right" style="white-space:nowrap;"><?php echo $databulanan[$xx]['jum_do'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php if ($databulanan[$xx]['persendovsfc'] != '') { echo number_format($databulanan[$xx]['persendovsfc'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					 <td align="right" style="white-space:nowrap;"><?php if ($databulanan[$xx]['persendovsop'] != '') { echo number_format($databulanan[$xx]['persendovsop'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					
		<?php		
					// ini menampung semua data di semua bulan. difilternya nanti dibawah
					$alldatabulan[] = array('bulanxx'=> $databulanan[$xx]['bulanxx'],
										'fc'=> $databulanan[$xx]['fc'],
										'jum_op'=> $databulanan[$xx]['jum_op'],
										'persenopvsfc'=> $databulanan[$xx]['persenopvsfc'],
										'jum_do'=> $databulanan[$xx]['jum_do'],
										'persendovsfc'=> $databulanan[$xx]['persendovsfc'],
										'persendovsop'=> $databulanan[$xx]['persendovsop']
										);
					
					if ($databulanan[$xx]['bulanxx'] == $tsumalldatabulan2[$xx]['bulanxx']) {
						$tsumalldatabulan2[$xx]['tsumfc']+= $databulanan[$xx]['fc'];
						$tsumalldatabulan2[$xx]['tsumop']+= $databulanan[$xx]['jum_op'];
						$tsumalldatabulan2[$xx]['tsumdo']+= $databulanan[$xx]['jum_do'];
					}
				}
					echo "</tr>";

					$i++;
					
					if (isset($query2[$j+1]['kode_kel']) && ($query2[$j]['kode_kel'] != $query2[$j+1]['kode_kel'])) {
						// 28-04-2015
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
									
									/*$tsumfc+=$alldatabulan[$xx1]['fc'];
									$tsumop+=$alldatabulan[$xx1]['jum_op'];
									$tsumdo+=$alldatabulan[$xx1]['jum_do'];
									$tcdata++; */
																		
								} // end if
							} // end for alldatabulan
							
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
							/*$tsumpersendovsop+= $ratapersendovsop;
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsop++; */
							
							if ($bulannya == $tsumalldatabulan2[$thitung]['bulanxx']) {
								/*$tsumalldatabulan[] = array('bulanxx'=>$bulannya,
													'tsumfc'=> 0,
													'tsumop'=> 0,
													'tsumdo'=> 0,
													'tcdata'=> 0,
													'tsumpersendovsop'=> 0,
													'tsumpersenopvsfc'=> 0,
													'tsumpersendovsfc'=> 0,
													'tcdatapersendovsop'=> 0
													); */
								$tsumalldatabulan2[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan2[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan2[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan2[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						} // end for bulan

		?>
					<tr>
						<td colspan="3" align="center"><b>TOTAL</b></td>
						<?php
							$hitung=0;
							for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
						?>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumfc']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumop']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersenopvsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumdo']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsop'], 2, '.',',') ?> %&nbsp;</b></td>
					
		<?php					$hitung++;
					}
					echo "</tr>";
					
						$sumalldatabulan = array();
						$alldatabulan = array();
						$i=1;
					}
					else if (!isset($query2[$j+1]['kode_kel'])) {						
						// ============ 29-04-2015 ===========================================
						$thitung=0;
						for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
							if (strlen($xx) < 2)
								$bulannya = "0".$xx;
							else
								$bulannya = $xx;
						
							for($xx1=0; $xx1<count($alldatabulan); $xx1++){
								if($bulannya == $alldatabulan[$xx1]['bulanxx']) {
									$sumfc+=$alldatabulan[$xx1]['fc'];
									$sumop+=$alldatabulan[$xx1]['jum_op'];
									$sumdo+=$alldatabulan[$xx1]['jum_do'];
									$sumpersenopvsfc+=$alldatabulan[$xx1]['persenopvsfc'];
									$sumpersendovsfc+=$alldatabulan[$xx1]['persendovsfc'];
									$sumpersendovsop+=$alldatabulan[$xx1]['persendovsop'];
									
									if ($alldatabulan[$xx1]['jum_op'] != 0)
										$cdata++;
																		
								} // end if
							} // end for alldatabulan
							
							if ($cdata != 0) {
								$ratapersendovsop= $sumpersendovsop/$cdata;
								$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
								$ratapersendovsfc= $sumpersendovsfc/$cdata;
							}
							else {
								$ratapersendovsop= 0;
								$ratapersenopvsfc= 0;
								$ratapersendovsfc= 0;
							}
														
							if ($bulannya == $tsumalldatabulan2[$thitung]['bulanxx']) {
								$tsumalldatabulan2[$thitung]['tsumpersendovsop']+= $ratapersendovsop;
								$tsumalldatabulan2[$thitung]['tsumpersenopvsfc']+= $ratapersenopvsfc;
								$tsumalldatabulan2[$thitung]['tsumpersendovsfc']+= $ratapersendovsfc;
								$tsumalldatabulan2[$thitung]['tcdatapersendovsop']++;
							}
							
							$sumalldatabulan[] = array('bulanxx'=>$bulannya,
													'sumfc'=> $sumfc,
													'sumop'=> $sumop,
													'sumdo'=> $sumdo,
													'sumpersenopvsfc'=> $sumpersenopvsfc,
													'sumpersendovsfc'=> $sumpersendovsfc,
													'sumpersendovsop'=> $sumpersendovsop,
													'cdata'=> $cdata,
													'ratapersendovsop'=> $ratapersendovsop,
													'ratapersenopvsfc'=> $ratapersenopvsfc,
													'ratapersendovsfc'=> $ratapersendovsfc
													);
							
							$cdata=0; $sumfc=0; $sumop=0; $sumdo=0;
							$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0;
							
							$thitung++;
						}
						// ===================================================================
		?>
					<tr>
						<td colspan="3" align="center"><b>TOTAL</b></td>
						<?php
							$hitung=0;
							for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
						?>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumfc']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumop']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersenopvsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumalldatabulan[$hitung]['sumdo']; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsfc'], 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumalldatabulan[$hitung]['ratapersendovsop'], 2, '.',',') ?> %&nbsp;</b></td>
					
						<?php
								$hitung++;
							}
							echo "</tr>";
						?>
						
						<tr>
							<td colspan="3" align="center"><b>TOTAL BARANG REGULER</b></td>
							<?php
								$thitung=0;
								for ($yy=$bulan1; $yy<=$bulan2; $yy++) {
									$ratapersen = $tsumalldatabulan2[$thitung]['tsumpersendovsop']/$tsumalldatabulan2[$thitung]['tcdatapersendovsop'];
									$ratapersenopfc = $tsumalldatabulan2[$thitung]['tsumpersenopvsfc']/$tsumalldatabulan2[$thitung]['tcdatapersendovsop'];
									$ratapersendofc = $tsumalldatabulan2[$thitung]['tsumpersendovsfc']/$tsumalldatabulan2[$thitung]['tcdatapersendovsop'];
									
							?>
							<td align="right"><b><?php echo $tsumalldatabulan2[$thitung]['tsumfc']; ?>&nbsp;</b></td>
							<td align="right"><b><?php echo $tsumalldatabulan2[$thitung]['tsumop']; ?>&nbsp;</b></td>
							<td align="right"><b><?php echo number_format($ratapersenopfc, 2, '.',',') ?> %&nbsp;</b></td>
							<td align="right"><b><?php echo $tsumalldatabulan2[$thitung]['tsumdo']; ?>&nbsp;</b></td>
							<td align="right"><b><?php echo number_format($ratapersendofc, 2, '.',',') ?> %&nbsp;</b></td>
							<td align="right"><b><?php echo number_format($ratapersen, 2, '.',',') ?> %&nbsp;</b></td>
							<?php
									$thitung++;
								}
								echo "</tr>";
							?>	
			<?php
					}
					 
				} // end for
			}
	?>
	
 	</tbody>
</table><br>

</div>
