<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Laporan Data Realisasi FC vs OPvsDO</h3><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<div>

Pelanggan: <?php if ($i_customer!= 0) { echo $e_customer_name; } else echo "Semua"; ?><br>
Periode: <?php echo $date_from ?> s.d. <?php echo $date_to ?><br><br>

<?php 
$attributes = array('name' => 'f_laprealisasi', 'id' => 'f_laprealisasi');
echo form_open('marketing/creport/export_excel_laprealisasi', $attributes); ?>
<input type="hidden" name="date_from" value="<?php echo $date_from ?>" >
<input type="hidden" name="date_to" value="<?php echo $date_to ?>" >
<input type="hidden" name="i_customer" value="<?php echo $i_customer ?>" >
<input type="hidden" name="e_customer_name" value="<?php echo $e_customer_name ?>" >
<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br><br>

<b>Barang Non-STP</b>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
<!--	<thead>
	 <tr class="judulnya">
		<th rowspan="2">No </th>
		 <th rowspan="2">Kode</th>
		 <th rowspan="2">Nama Barang Jadi</th>
		 <th rowspan="2">HJP</th>
		 <th colspan="2">Order Pembelian (OP)</th>
		 <th colspan="2">Delivery Order (DO)</th>
		 <th rowspan="2">% DO-OP</th>
		 <th colspan="2">Pendingan</th>
	 </tr>
	 <tr class="judulnya">
		<th>Qty</th>
		<th>Rp.</th>
		<th>Qty</th>
		<th>Rp.</th>
		<th>Qty</th>
		<th>Rp.</th>
	 </tr>
	</thead> -->
	<tbody>
		<?php
			$i = 1;
			if (is_array($query)) {
				$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
				// 09-05-2015
				$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
				$tcdata=0; $tsumfc=0; $tsumop=0; $tsumoprp=0; $tsumdo=0; $tsumdorp=0; 
				$tsumpersendovsop=0; $tsumpersenopvsfc=0; $tsumpersendovsfc=0; $tcdatapersendovsop=0; $tsumpendingan=0; $tsumpendinganrp=0;
				// 11-05-2015
				$tcdatapersenopvsfc=0; $tcdatapersendovsfc=0;
				
				$sumdropping=0;$tsumdropping=0;$sumdroppingrp=0;$tsumdroppingrp=0;
				for($j=0;$j<count($query);$j++){
					if ($temp_kodekel != $query[$j]['kode_kel']) {
						$temp_kodekel = $query[$j]['kode_kel'];
				?>
					<tr>
						<td colspan="17">&nbsp;<b><?php echo $query[$j]['kode_kel']." - ".$query[$j]['nama_kel'] ?></b></td>
					</tr>
					<tr class="judulnya">
					<th rowspan="2">No </th>
					 <th rowspan="2">Kode</th>
					 <th rowspan="2">Nama Barang Jadi</th>
					 <th rowspan="2">HJP</th>
					 <th rowspan="2">FC</th>
					 <th colspan="2">Order Pembelian (OP)</th>
					 <th rowspan="2">% OP-FC</th>
					 <th colspan="2">Delivery Order (DO)</th>
					 <th rowspan="2">% DO-FC</th>
					 <th rowspan="2">% DO-OP</th>
					 <th colspan="2">Pendingan</th>
					  <th colspan="2">Dropping</th>
					  <th rowspan="2">Keterangan</th>
				 </tr>
				 <tr class="judulnya">
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
					<th>Qty</th>
					<th>Rp.</th>
				 </tr>
				<?php 
				 }
					 
					 echo "<tr class=\"record\">";
					 echo "<td align='center' id='num_$i'>$i</td>";
		?>
					<td>&nbsp;<?php echo $query[$j]['kode_brg_jadi'] ?></td>
					<td>&nbsp;<?php echo $query[$j]['nama_brg_jadi'] ?></td>
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($query[$j]['hjp'], 0, ',','.') ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($query[$j]['fc'], 0, ',','.') ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo $query[$j]['jum_op'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($query[$j]['jum_op_rp'], 2, ',','.') ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php if ($query[$j]['persenopvsfc'] != '') { echo number_format($query[$j]['persenopvsfc'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					 <td align="right" style="white-space:nowrap;"><?php echo $query[$j]['jum_do'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($query[$j]['jum_do_rp'], 2, ',','.') ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php if ($query[$j]['persendovsfc'] != '') { echo number_format($query[$j]['persendovsfc'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					 <td align="right" style="white-space:nowrap;"><?php if ($query[$j]['persendovsop'] != '') { echo number_format($query[$j]['persendovsop'], 2, '.',',') ?> %&nbsp; <?php } else echo ''; ?></td>
					 <td align="right" style="white-space:nowrap;"><?php echo $query[$j]['pendingan'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($query[$j]['pendingan_rp'], 2, ',','.') ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo $query[$j]['dropping'] ?>&nbsp;</td>
					 <td align="right" style="white-space:nowrap;"><?php echo number_format($query[$j]['dropping_rp'], 2, ',','.') ?>&nbsp;</td>
					  <td align="right" style="white-space:nowrap;">&nbsp;</td>
					</tr>
		<?php
					
					$sumfc+=$query[$j]['fc'];
					$sumop+=$query[$j]['jum_op'];
					$sumoprp+=$query[$j]['jum_op_rp'];
					$sumdo+=$query[$j]['jum_do'];
					$sumdorp+=$query[$j]['jum_do_rp'];
					
					$sumdropping+=$query[$j]['dropping'];
					$sumdroppingrp+=$query[$j]['dropping_rp'];
					//$cdatapersenopvsfc, $cdatapersendovsfc, $cdatapersendovsop
					if ($query[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query[$j]['pendingan'];
					$sumpendinganrp+=$query[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc+=$query[$j]['fc'];
					$tsumop+=$query[$j]['jum_op'];
					$tsumoprp+=$query[$j]['jum_op_rp'];
					$tsumdo+=$query[$j]['jum_do'];
					$tsumdorp+=$query[$j]['jum_do_rp'];
					//$tsumpersendovsop+=$query[$j]['persendovsop'];
					$tsumpendingan+=$query[$j]['pendingan'];
					$tsumpendinganrp+=$query[$j]['pendingan_rp'];
					
					$tsumdropping+=$query[$j]['dropping'];
					$tsumdroppingrp+=$query[$j]['dropping_rp'];
					$tcdata++;
					
					$i++;
					if (isset($query[$j+1]['kode_kel']) && ($query[$j]['kode_kel'] != $query[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						//$ratapersendovsfc= $sumpersendovsfc/$cdata;
						
						if ($cdata != 0) {
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						//echo $tsumpersendovsop."<br>";
		?>
					<tr>
						<td colspan="4" align="center"><b>TOTAL</b></td>
						<td align="right"><b><?php echo $sumfc; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumop; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumoprp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersenopvsfc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumdo; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdorp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersendovsfc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersendovsop, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumpendingan; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumpendinganrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumdropping ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdroppingrp, 2, ',','.') ?>&nbsp;</b></td>
						 <td align="right" style="white-space:nowrap;">&nbsp;</td>
					</tr>
		<?php			
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						 $sumdropping =0;$sumdroppingrp =0;
						$i=1;
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query[$j+1]['kode_kel'])) {
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						//$ratapersendovsfc= $sumpersendovsfc/$cdata;
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop+= $ratapersendovsop;
						$tsumpersenopvsfc+= $ratapersenopvsfc;
						$tsumpersendovsfc+= $ratapersendovsfc;
						$tcdatapersendovsop++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop+= $ratapersendovsop;
							$tcdatapersendovsop++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc+= $ratapersenopvsfc;
							$tcdatapersenopvsfc++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc+= $ratapersendovsfc;
							$tcdatapersendovsfc++;
						}
						
						//echo $tsumpersendovsop."<br>";
						
						/*$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersendovsop;
						$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsop; */
						// 11-05-2015, 20-05-2015
						if ($tcdatapersendovsop != 0)
							$ratapersen = $tsumpersendovsop/$tcdatapersendovsop;
						else
							$ratapersen = 0;
						
						if ($tcdatapersenopvsfc != 0)	
							$ratapersenopfc = $tsumpersenopvsfc/$tcdatapersenopvsfc;
						else
							$ratapersenopfc = 0;
						
						if ($tcdatapersendovsfc != 0)
							$ratapersendofc = $tsumpersendovsfc/$tcdatapersendovsfc;
						else
							$ratapersendofc = 0;
		?>
					<tr>
						<td colspan="4" align="center"><b>TOTAL</b></td>
						<td align="right"><b><?php echo $sumfc; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumop; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumoprp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersenopvsfc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumdo; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdorp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersendovsfc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersendovsop, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $sumpendingan; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumpendinganrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumdropping ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdroppingrp, 2, ',','.') ?>&nbsp;</b></td>
						 <td align="right" style="white-space:nowrap;">&nbsp;</td>
					</tr>
					
					<tr>
						<td colspan="4" align="center"><b>TOTAL BARANG NON-STP</b></td>
						<td align="right"><b><?php echo $tsumfc; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $tsumop; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumoprp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersenopfc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $tsumdo; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumdorp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersendofc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($ratapersen, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $tsumpendingan; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumpendinganrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $tsumdropping ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumdroppingrp, 2, ',','.') ?>&nbsp;</b></td>
						 <td align="right" style="white-space:nowrap;">&nbsp;</td>
					</tr>
			<?php		// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					 
				} // end for
			}
			// SAMPE YG NON-STP JAM 3.53 22-04-2015
	?>
	
 	</tbody>
</table><br><br>

<b>Barang STP</b>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<!--<thead>
	 <tr class="judulnya">
		<th rowspan="2">No </th>
		 <th rowspan="2">Kode</th>
		 <th rowspan="2">Nama Barang Jadi</th>
		 <th rowspan="2">HJP</th>
		 <th colspan="2">Order Pembelian (OP)</th>
		 <th colspan="2">Delivery Order (DO)</th>
		 <th rowspan="2">% DO-OP</th>
		 <th colspan="2">Pendingan</th>
	 </tr>
	 <tr class="judulnya">
		<th>Qty</th>
		<th>Rp.</th>
		<th>Qty</th>
		<th>Rp.</th>
		<th>Qty</th>
		<th>Rp.</th>
	 </tr>
	</thead>-->
	<tbody>
		<?php
			$i = 1;
			if (is_array($query2)) {
				$temp_kodekel = ""; $cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
				$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
				// 09-05-2015
				$cdatapersenopvsfc=0; $cdatapersendovsfc=0; $cdatapersendovsop=0;
				
				$tcdata2=0; $tsumfc2=0; $tsumop2=0; $tsumoprp2=0; $tsumdo2=0; $tsumdorp2=0; 
				$tsumpersendovsop2=0; $tsumpersenopvsfc2=0; $tsumpersendovsfc2=0; $tcdatapersendovsop2=0; $tsumpendingan2=0; $tsumpendinganrp2=0;
				$tsumdropping2=0;$tsumdroppingrp2=0;
				// 11-05-2015
				$tcdatapersenopvsfc2=0; $tcdatapersendovsfc2=0;
				
				$sumdropping=0;$sumdroppingrp=0;
				for($j=0;$j<count($query2);$j++){
					if ($temp_kodekel != $query2[$j]['kode_kel']) {
						$temp_kodekel = $query2[$j]['kode_kel'];
				?>
					<tr>
						<td colspan="17">&nbsp;<b><?php echo $query2[$j]['kode_kel']." - ".$query2[$j]['nama_kel'] ?></b></td>
					</tr>
					<tr class="judulnya">
						<th rowspan="2">No </th>
						 <th rowspan="2">Kode</th>
						 <th rowspan="2">Nama Barang Jadi</th>
						 <th rowspan="2">HJP</th>
						 <th rowspan="2">FC</th>
						 <th colspan="2">Order Pembelian (OP)</th>
						 <th rowspan="2">% OP-FC</th>
						 <th colspan="2">Delivery Order (DO)</th>
						 <th rowspan="2">% DO-FC</th>
						 <th rowspan="2">% DO-OP</th>
						 <th colspan="2">Pendingan</th>
						 <th colspan="2">Dropping</th>
						 <th rowspan="2">Keterangan</th>
					 </tr>
					 <tr class="judulnya">
						<th>Qty</th>
						<th>Rp.</th>
						<th>Qty</th>
						<th>Rp.</th>
						<th>Qty</th>
						<th>Rp.</th>
						<th>Qty</th>
						<th>Rp.</th>
					 </tr>
				<?php 
				 }
					 
					 echo "<tr class=\"record\">";
					 echo "<td align='center' id='num_$i'>$i</td>";
		?>
					<td>&nbsp;<?php echo $query2[$j]['kode_brg_jadi'] ?></td>
					<td>&nbsp;<?php echo $query2[$j]['nama_brg_jadi'] ?></td>
					 <td align="right"><?php echo number_format($query2[$j]['hjp'], 0, ',','.') ?>&nbsp;</td>
					 <td align="right"><?php echo number_format($query2[$j]['fc'], 0, ',','.') ?>&nbsp;</td>
					 <td align="right"><?php echo $query2[$j]['jum_op'] ?>&nbsp;</td>
					 <td align="right"><?php echo number_format($query2[$j]['jum_op_rp'], 2, ',','.') ?>&nbsp;</td>
					 <td align="right">&nbsp;</td>
					 <td align="right"><?php echo $query2[$j]['jum_do'] ?>&nbsp;</td>
					 <td align="right"><?php echo number_format($query2[$j]['jum_do_rp'], 2, ',','.') ?>&nbsp;</td>
					 <td align="right">&nbsp;</td>
					 <!--<td align="right"><?php //if ($query2[$j]['persendovsop'] != '') { echo number_format($query2[$j]['persendovsop'], 2, '.',',') ?> %&nbsp; <?php //} else echo ''; ?></td>-->
					 <td align="right">&nbsp;</td>
					 <td align="right"><?php echo $query2[$j]['pendingan'] ?>&nbsp;</td>
					 <td align="right"><?php echo number_format($query2[$j]['pendingan_rp'], 2, ',','.') ?>&nbsp;</td>
					  <td align="right"><?php echo $query2[$j]['dropping'] ?>&nbsp;</td>
					  <td align="right"><?php echo number_format($query2[$j]['dropping_rp'], 2, ',','.') ?>&nbsp;</td>
					   <td align="right">&nbsp;</td>
					</tr>
		<?php
					$sumfc+=$query2[$j]['fc'];
					$sumop+=$query2[$j]['jum_op'];
					$sumoprp+=$query2[$j]['jum_op_rp'];
					$sumdo+=$query2[$j]['jum_do'];
					$sumdorp+=$query2[$j]['jum_do_rp'];
					$sumdropping+=$query2[$j]['dropping'];
					$sumdroppingrp+=$query2[$j]['dropping_rp'];
					
					
					
					/*$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
					$sumpersendovsfc+=$query2[$j]['persendovsfc'];
					$sumpersendovsop+=$query2[$j]['persendovsop']; */
					//$cdatapersenopvsfc, $cdatapersendovsfc, $cdatapersendovsop
					if ($query2[$j]['persenopvsfc']!='') {
						$sumpersenopvsfc+=$query2[$j]['persenopvsfc'];
						$cdatapersenopvsfc++;
					}
					
					if ($query[$j]['persendovsfc']!='') {
						$sumpersendovsfc+=$query2[$j]['persendovsfc'];
						$cdatapersendovsfc++;
					}
					
					if ($query[$j]['persendovsop']!='') {
						$sumpersendovsop+=$query2[$j]['persendovsop'];
						$cdatapersendovsop++;
					}
					
					$sumpendingan+=$query2[$j]['pendingan'];
					$sumpendinganrp+=$query2[$j]['pendingan_rp'];
					$cdata++;
					
					$tsumfc2+=$query2[$j]['fc'];
					$tsumop2+=$query2[$j]['jum_op'];
					$tsumoprp2+=$query2[$j]['jum_op_rp'];
					$tsumdo2+=$query2[$j]['jum_do'];
					$tsumdorp2+=$query2[$j]['jum_do_rp'];
					$tsumpendingan2+=$query2[$j]['pendingan'];
					$tsumpendinganrp2+=$query2[$j]['pendingan_rp'];
					$tsumdropping2+=$query2[$j]['dropping'];
					$tsumdroppingrp2+=$query2[$j]['dropping_rp'];
					$tcdata2++;
					
					$i++;
					
					if (isset($query2[$j+1]['kode_kel']) && ($query2[$j]['kode_kel'] != $query2[$j+1]['kode_kel'])) {	
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$tsumpersendovsop+= $ratapersendovsop;
						//$tcdatapersendovsop++;
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
												
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
		?>
					<tr>
						<td colspan="4" align="center"><b>TOTAL</b></td>
						<td align="right"><b><?php echo $sumfc; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumop; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumoprp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right">&nbsp;</td>
						<td align="right"><b><?php echo $sumdo; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdorp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right"><b><?php echo $sumpendingan; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumpendinganrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumdropping ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdroppingrp, 2, ',','.') ?>&nbsp;</b></td>
						 <td align="right" style="white-space:nowrap;">&nbsp;</td>
					</tr>
		<?php			
						$cdata=0; $sumfc=0; $sumop=0; $sumoprp=0; $sumdo=0; $sumdorp=0; 
						$sumpersendovsop=0; $sumpersenopvsfc=0; $sumpersendovsfc=0; $sumpendingan=0; $sumpendinganrp=0;
						$sumdropping=0; $sumdroppingrp=0;
						$i=1;
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
					else if (!isset($query2[$j+1]['kode_kel'])) {
						/*$ratapersendovsop= $sumpersendovsop/$cdata;
						
						$tsumpersendovsop+= $ratapersendovsop;
						$tcdatapersendovsop++;
						
						$ratapersen = $tsumpersendovsop/$tcdatapersendovsop; */
						
						//$ratapersendovsop= $sumpersendovsop/$cdata;
						//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
						//$ratapersendovsfc= $sumpersendovsfc/$cdata;
						
						if ($cdata != 0) {
							/*$ratapersendovsop= $sumpersendovsop/$cdata;
							$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							$ratapersendovsfc= $sumpersendovsfc/$cdata; */
							//$ratapersendovsop= $sumpersendovsop/$cdata;
							if ($cdatapersendovsop != 0)
								$ratapersendovsop= $sumpersendovsop/$cdatapersendovsop;
							else
								$ratapersendovsop = 0;
								
							//$ratapersenopvsfc= $sumpersenopvsfc/$cdata;
							if ($cdatapersenopvsfc != 0)
								$ratapersenopvsfc= $sumpersenopvsfc/$cdatapersenopvsfc;
							else
								$ratapersenopvsfc = 0;
								
							//$ratapersendovsfc= $sumpersendovsfc/$cdata;
							if ($cdatapersendovsfc != 0)
								$ratapersendovsfc= $sumpersendovsfc/$cdatapersendovsfc;
							else
								$ratapersendovsfc = 0;
						}
						else {
							$ratapersendovsop= 0;
							$ratapersenopvsfc= 0;
							$ratapersendovsfc= 0;
						}
						
						/*$tsumpersendovsop2+= $ratapersendovsop;
						$tsumpersenopvsfc2+= $ratapersenopvsfc;
						$tsumpersendovsfc2+= $ratapersendovsfc;
						$tcdatapersendovsop2++; */
						// 11-05-2015, dihitung jika tidak 0
						if ($ratapersendovsop != 0) {
							$tsumpersendovsop2+= $ratapersendovsop;
							$tcdatapersendovsop2++;
						}
						
						if ($ratapersenopvsfc != 0) {
							$tsumpersenopvsfc2+= $ratapersenopvsfc;
							$tcdatapersenopvsfc2++;
						}
						
						if ($ratapersendovsfc != 0) {
							$tsumpersendovsfc2+= $ratapersendovsfc;
							$tcdatapersendovsfc2++;
						}
						
						$ratapersen2 = $tsumpersendovsop2/$tcdatapersendovsop2;
						$ratapersenopfc2 = $tsumpersenopvsfc2/$tcdatapersendovsop2;
						$ratapersendofc2 = $tsumpersendovsfc2/$tcdatapersendovsop2;
						
		?>
					<tr>
						<td colspan="4" align="center"><b>TOTAL</b></td>
						<td align="right"><b><?php echo $sumfc; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumop; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumoprp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right">&nbsp;</td>
						<td align="right"><b><?php echo $sumdo; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdorp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right"><b><?php echo $sumpendingan; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumpendinganrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $sumdropping ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($sumdroppingrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b>&nbsp;</b></td>
					</tr>
					
					<tr>
						<td colspan="4" align="center"><b>TOTAL BARANG STP</b></td>
						<td align="right"><b><?php echo $tsumfc2; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $tsumop2; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumoprp2, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right">&nbsp;</td>
						<td align="right"><b><?php echo $tsumdo2; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumdorp2, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right"><b><?php echo $tsumpendingan2; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumpendinganrp2, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $tsumdropping2 ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($tsumdroppingrp2, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b>&nbsp;</b></td>
					</tr>
			<?php
						// 20-05-2015
						$cdatapersendovsop = 0;
						$cdatapersenopvsfc = 0;
						$cdatapersendovsfc = 0;
					}
				} // end for
				
				$allsumfc = $tsumfc + $tsumfc2;
				$allsumop = $tsumop + $tsumop2;
				$allsumoprp = $tsumoprp + $tsumoprp2;
				$allsumdo = $tsumdo + $tsumdo2;
				$allsumdorp = $tsumdorp + $tsumdorp2;
				$allsumpendingan = $tsumpendingan + $tsumpendingan2;
				$allsumpendinganrp = $tsumpendinganrp + $tsumpendinganrp2;
				$allsumdropping = $tsumdropping + $tsumdropping2;
				$allsumdroppingrp = $tsumdroppingrp + $tsumdroppingrp2;
				// 11-05-2015, 25-05-2015 acuannya pake yg non-stp aja
				//if ($ratapersenopfc!=0 && $ratapersenopfc2!=0)
				//	$allratapersenopfc = ($ratapersenopfc+$ratapersenopfc2)/2;
				//else if ($ratapersenopfc2 == 0)
					$allratapersenopfc = $ratapersenopfc;
				//else if ($ratapersenopfc == 0)
				//	$allratapersenopfc = $ratapersenopfc2;
				
				//$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				//if ($ratapersendofc!=0 && $ratapersendofc2!=0)
				//	$allratapersendofc = ($ratapersendofc+$ratapersendofc2)/2;
				//else if ($ratapersendofc2 == 0)
					$allratapersendofc = $ratapersendofc;
				//else if ($ratapersendofc == 0)
				//	$allratapersendofc = $ratapersendofc2;
				
				//$allratapersen = ($ratapersen+$ratapersen2)/2;
				//if ($ratapersen!=0 && $ratapersen2!=0)
				//	$allratapersen = ($ratapersen+$ratapersen2)/2;
				//else if ($ratapersen2 == 0)
					$allratapersen = $ratapersen;
				//else if ($ratapersen == 0)
				//	$allratapersen = $ratapersen2;
			?>
				<tr>
						<td colspan="4" align="center"><b>GRAND TOTAL BARANG AKTIF + STP</b></td>
						<td align="right"><b><?php echo $allsumfc; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $allsumop; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($allsumoprp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($allratapersenopfc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $allsumdo; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($allsumdorp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($allratapersendofc, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($allratapersen, 2, '.',',') ?> %&nbsp;</b></td>
						<td align="right"><b><?php echo $allsumpendingan; ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($allsumpendinganrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b><?php echo $allsumdropping ?>&nbsp;</b></td>
						<td align="right"><b><?php echo number_format($allsumdroppingrp, 2, ',','.') ?>&nbsp;</b></td>
						<td align="right"><b>&nbsp;</b></td>
					</tr>
			<?php
			}
				// GRAND TOTAL DONE 05-05-2015
	?>
	
 	</tbody>
</table><br>
</div>
