<h3>Forecast Berdasarkan Pelanggan/Distributor</h3><br><br>

<link href="<? echo base_url(); ?>css/jquery.ui.datepicker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{

});
</script>
<script type="text/javascript">

function cek_input() {
	var tahun = $('#tahun').val();
	if (tahun == '') {
		alert("Tahun harus diisi..!");
		return false;
	}
	if (tahun.length < 4) {
		alert("Tahun harus 4 digit ..!");
		return false;
	}
	if (isNaN(tahun)) {
		alert("Tahun harus berupa angka ..!");
		return false;
	}	
}

/*function addElement() {
  var ni = document.getElementById('myDiv');
  var newdiv = document.createElement('div');
  
  var jumunit= $('#jumunit').val();
  var jumunitnew = parseInt(jumunit)+1;
  $('#jumunit').val(jumunitnew);
    
  newdiv.innerHTML = "<select name='kode_unit_"+jumunitnew+"' id='kode_unit_"+jumunitnew+"'>"+
					"<option value=''> -Pilih- </option>"+
					<?php
					foreach($list_unit as $uj) {
					?>
					"<option value='<?php echo $uj->kode_unit ?>'><?php echo $uj->nama ?></option>"+
					<?php }?>
					"</select>";
  
  ni.appendChild(newdiv);
}

function hapusElement() {
  var jumunit= $('#jumunit').val();
  if (jumunit > 1) {
	  $("#kode_unit_"+jumunit).remove(); 
	  var jumunitnew = parseInt(jumunit)-1;
	  $('#jumunit').val(jumunitnew);
  }
} */
</script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<?php 
$attributes = array('name' => 'f_forecast', 'id' => 'f_forecast');
echo form_open('marketing/creport/addforecastdistributor', $attributes); ?>
<table width="60%">
	<tr>
		<td width="20%">Periode (bulan-tahun)</td>
		<td> <select name="bulan" id="bulan">
				<option value="01">Januari</option>
				<option value="02">Februari</option>
				<option value="03">Maret</option>
				<option value="04">April</option>
				<option value="05">Mei</option>
				<option value="06">Juni</option>
				<option value="07">Juli</option>
				<option value="08">Agustus</option>
				<option value="09">September</option>
				<option value="10">Oktober</option>
				<option value="11">November</option>
				<option value="12">Desember</option>
			</select> -

		  <input name="tahun" type="text" id="tahun" size="4" value="" maxlength="4">
		</td>
  </tr>
			<tr>
			  <td>Pelanggan</td>
			  <td> <select name="i_customer" id="i_customer">
				<?php foreach ($list_pelanggan as $cust) { ?>
					<option value="<?php echo $cust->i_customer ?>" ><?php echo $cust->i_customer_code." - ".$cust->e_customer_name ?></option>
				<?php } ?>
				</select>
				  
			  </td>
			</tr>
		
</table><br>
<input type="submit" name="submit" value="Proses" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/marketing/creport/addforecastdistributor'">
<?php echo form_close();  ?>
