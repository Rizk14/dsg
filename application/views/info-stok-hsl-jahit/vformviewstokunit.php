<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.fieldsetdemo
{
	background-color:#DDD;
	max-width:350px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<h3>Laporan Stok Terkini Barang WIP (Hasil Jahit) Di Unit Jahit</h3><br><br>

<div>

Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('info-stok-hsl-jahit/cform/viewstokunit'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	
	<tr>
		<td>Unit Jahit</td>
		<td>: <select name="id_unit_jahit" id="id_unit_jahit">
				<option value="0">- All -</option>
				<?php foreach ($list_unit_jahit as $jht) { ?>
					<option value="<?php echo $jht->id ?>" <?php if ($jht->id == $cunit_jahit) { ?> selected <?php } ?> ><?php echo $jht->kode_unit."-".$jht->nama ?></option>
				<?php } ?>
				</select>
		</td>
	</tr>
	<tr>
		<td>Status</td>
		<td>: <select name="statusnya" id="statusnya">
		<option value="t" <?php if ($cstatus == 't') { ?> selected="true" <?php } ?> >Aktif</option>
		<option value="f" <?php if ($cstatus == 'f') { ?> selected="true" <?php } ?> >Non-Aktif</option>
		</select></td>
	</tr>
	<tr>
		<td style="white-space:nowrap;">Kode Brg/Nama Brg</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>

<table border="1" cellpadding= "1" cellspacing = "1" width="90%">
	<thead>
	 <tr class="judulnya">
		<th>No</th>
		<th>Kode</th>
		 <th>Nama Barang WIP</th>
		 <th>Unit Jahit</th>
		 <th>Stok</th>
		<!-- <th>Stok Per Warna</th> -->
		 <!--<th>Stok Total Per Warna</th>
		 <th>Stok Bagus</th>
		 <th>Stok Perbaikan</th> -->
		 <th>Last Update Stok</th>
	 </tr>
	</thead>
	<tbody>
		 <?php
			if (is_array($query)) {
				if ($startnya == '')
					$i=1;
				else
					$i = $startnya+1; 
			 for($j=0;$j<count($query);$j++){
				if ($query[$j]['tgl_update_stok'] != '') {
					$pisah1 = explode("-", $query[$j]['tgl_update_stok']);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
					//$tgl_update_stok = $tgl1." ".$nama_bln." ".$thn1;
					
					$exptgl1 = explode(" ", $tgl1);
					$tgl1nya= $exptgl1[0];
					$jam1nya= $exptgl1[1];
					$tgl_update_stok = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
				}
				else
					$tgl_update_stok = "&nbsp;";
				 
				 echo "<tr class=\"record\">";
				 echo "<td align='center'>$i</td>";
				 echo    "<td style='white-space:nowrap;'>&nbsp;".$query[$j]['kode_brg_wip']."</td>";
				 echo    "<td style='white-space:nowrap;'>&nbsp;".$query[$j]['nama_brg_wip']."</td>";
				 echo    "<td style='white-space:nowrap;'>&nbsp;".$query[$j]['kode_unit']." - ".$query[$j]['nama_unit']."</td>";
				 echo    "<td align='right'>".$query[$j]['stok']."&nbsp;</td>";
				 
			/*	 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailwarna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_warna']." : ".$var_detail[$k]['stok']."&nbsp;";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>"; */
				 
				/* echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailwarna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_warna']." : ".$var_detail[$k]['stok_bagus']."&nbsp;";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td style='white-space:nowrap;' align='right'>";
				 if (is_array($query[$j]['detailwarna'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detailwarna'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_warna']." : ".$var_detail[$k]['stok_perbaikan']."&nbsp;";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>"; */
				 
				 echo    "<td align='center'>".$tgl_update_stok."</td>";
				 echo  "</tr>";
				$i++;
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
