<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var des=$("#des").val();
		var satuan=$("#satuan").val();
		var q=$("#q").val();
		
		if (q == 'f') {
			opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=des;
			opener.document.forms["f_purchase"].satuan_<?php echo $posisi ?>.value=satuan;
			//opener.document.forms["f_purchase"].gelaran_<?php echo $posisi ?>.focus();
			self.close();
		}
		else {
			opener.document.forms["f_purchase"].kode_brg_quilting_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].brg_quilting_<?php echo $posisi ?>.value=des;
			self.close();
		}
	});
});
</script>
<center>
<?php
	if ($quilting == 'f') {
?>
<h3>Daftar Barang Untuk Kelompok <?php echo $nama_kel ?></h3>
<?php
	} else {
?>
<h3>Daftar Bahan Quilting</h3>
<?php
	}
?>

</center>
<div align="center"><br>
<?php echo form_open('mst-keb-bisbisan-perpcs/cform/show_popup_brg'); ?>
<?php
	if ($quilting == 'f') {
?>
Jenis Bahan <select name="id_jenis_bhn" id="id_jenis_bhn">
				<option value="0" <?php if ($cjenis_bhn == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($jenis_bhn as $jns) { ?>
					<option value="<?php echo $jns->id ?>" <?php if ($cjenis_bhn == $jns->id) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nj_brg."] ". $jns->kode." - ".$jns->nama ?></option>
				<?php } ?>
				</select> &nbsp;
<?php
	} else {
		echo "<input type='hidden' name='id_jenis_bhn' id='id_jenis_bhn' value='0' >";
	}
?>

<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="kel_brg" value="<?php echo $kel_brg ?>">
<input type="hidden" name="quilting" value="<?php echo $quilting ?>">
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="des" id="des">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="q" id="q" value="<?php echo $quilting ?>">
</form>

  <table border="1" align="center" width="100%" cellpadding="1" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Action</th>
    </tr>
	<?php 
		//$i=1; 
		
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
				
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg']; ?></td>
      <td nowrap><?php echo $query[$j]['nama_brg']; ?></td>
      <td><?php echo $query[$j]['nama_satuan']; ?></td>
      <td align="center">
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
	  window.document.f_master_brg.satuan.value='<?php echo $query[$j]['nama_satuan'] ?>';
	  window.document.f_master_brg.des.value='<?php if ($quilting == 'f') echo str_replace("'", "\'", $query[$j]['nama_brg']); else echo str_replace("\"", "&quot;", $query[$j]['nama_brg']) ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
