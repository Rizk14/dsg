<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
	
	.judulnya {
		background-color:#DDD;
	}
</style>

<h3>Data Kebutuhan Bis-Bisan Per Pcs</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">View Data</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('mst-keb-bisbisan-perpcs/cform/cari'); ?>
<input type="text" name="cari" value="<?php echo $cari ?>">&nbsp;<input type="submit" name="submit" value="Cari">
<?php echo form_close(); ?>
<br>
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th>Ukuran Bis-Bisan</th>
		 <th>Kode & Nama Brg Jadi </th>
		 <th>Kode & Nama Bahan Baku</th>
		 <th>Jml Kebutuhan</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 echo "<tr class=\"record\">";
				 echo    "<td nowrap>".$query[$j]['nama_ukuran']."</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."</td>";
				 echo    "<td nowrap>".$query[$j]['kode_brg']." - ".$query[$j]['nama_brg']."</td>";
			     
				 echo    "<td align='right'>".$query[$j]['jum_kebutuhan']."</td>";
				 echo    "<td nowrap align='center'>".$query[$j]['tgl_update']."</td>";
				
				 $ada = 0;
				 // cek apakah data kode sudah dipake di tabel tm_pb_cutting
				 $query3	= $this->db->query(" SELECT id FROM tm_pb_cutting_detail WHERE id_kebutuhan_perpcs = ".$query[$j]['id']);
				 if ($query3->num_rows() > 0){
					$ada = 1;
				 }
				
					 echo    "<td align=center><a href=".base_url()."index.php/mst-keb-bisbisan-perpcs/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>";
				if ($ada ==0)
					 echo    "&nbsp; <a href=".base_url()."index.php/mst-keb-bisbisan-perpcs/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a>";
				echo "</td>";
					
				echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
<?php echo $this->pagination->create_links();?>
</div>
