<h3>Data Kebutuhan Bis-Bisan Per Pcs</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">View Data</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
				
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************
		
		//*****satuan*************************************
		var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				
		//*****end satuan*************************************	
		
		//*****ukuran*************************************
		var ukuran="#ukuran_"+n;
		var new_ukuran="#ukuran_"+no;
		$(ukuran, lastRow).attr("id", "ukuran_"+no);
		$(new_ukuran, lastRow).attr("name", "ukuran_"+no);		
		//*****end ukuran*********************************
								
		//*****keb*************************************
		var keb="#keb_"+n;
		var new_keb="#keb_"+no;
		$(keb, lastRow).attr("id", "keb_"+no);
		$(new_keb, lastRow).attr("name", "keb_"+no);		
		$(new_keb, lastRow).val('');				
		//*****end keb*************************************	
						
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		 
		 /*
			onclick="javascript: var x= $('#kel_brg_1').val(); var q = ''; if ($('input[name=quilting_1]').is(':checked')) { q= 't'; x= '0'; } else { q='f'; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg/'+ x +'/1/'+q);"
		 */
		 
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); var z= $('#kode_brg_jadi').val(); var q = 'f'; if (z == '') { alert('pilih dulu barang jadinya..!'); return false; } openCenteredWindow('<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/show_popup_brg/'+x+'/"+no+"/'+q);";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	
function cek_input() {
	var brg_jadi= $('#brg_jadi').val();
	
	if (brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		return false;
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			if ($('#kode_'+k).val() == '') {
				alert("Data item barang tidak boleh ada yang kosong...!");
				return false;
			}
						
			if($('#keb_'+k).val() == '0' || $('#keb_'+k).val() == '' ) {				
				alert("Data jml kebutuhan tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#keb_'+k).val()) ) {
				alert("Data jml kebutuhan harus berupa angka..!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}	
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/submit" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
	<td width="10%">Barang Jadi</td>
	<td><input name="brg_jadi" type="text" id="brg_jadi" size="30" readonly="true" value=""/>
           <input name="kode_brg_jadi" type="hidden" id="kode_brg_jadi" value=""/>
           <input title="browse data brg jadi" onmouseover="document.f_purchase.iddata.value=1" name="pilih_brg_jadi" value="..." type="button" id="pilih_brg_jadi" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/show_popup_brg_jadi/');" type="button"></td>
  </tr>
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="7" align="right">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Kelompok Brg</th>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th>Satuan</th>
	      <th>Uk Bis-Bisan</th>
	      <th>Jml Kebutuhan</th>

        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td><select name="kel_brg_1" id="kel_brg_1">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
		  </td>
          <td nowrap="nowrap">
           <input name="kode_1" type="text" id="kode_1" size="15" readonly="true" value=""/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#kel_brg_1').val(); var z= $('#kode_brg_jadi').val(); var q = 'f'; 
            if (z == '') { alert('pilih dulu barang jadinya..!'); return false; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/show_popup_brg/'+ x +'/1/'+q);">
           </td>
          <td><input name="nama_1" type="text" id="nama_1" size="20" readonly="true" value="" /></td>
          <td><input name="satuan_1" type="text" id="satuan_1" size="5" readonly="true" value="" /></td>
          <td><select name="ukuran_1" id="ukuran_1">
				<?php foreach ($ukuran_bisbisan as $uk) { ?>
					<option value="<?php echo $uk->id ?>" ><?php echo $uk->nama ?></option>
				<?php } ?>
				</select>
		  </td>
          <td><input name="keb_1" type="text" id="keb_1" size="5" value=""/></td>
        </tr>
	</table>	
	
	</form>
      <div align="center"><br> 
        
        <input type="submit" name="submit2" value="Simpan" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view'">

      </div></td>
    </tr>

</table>
</div>
</form>
