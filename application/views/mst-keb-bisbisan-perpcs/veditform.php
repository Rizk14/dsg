<h3>Data Kebutuhan Bis-Bisan Per Pcs</h3><br>
<a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/view">View Data</a><br><br>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}

function cek_input() {
	var brg_jadi= $('#brg_jadi').val();
	var kode_brg_jadi= $('#kode_brg_jadi').val();
	var bhn_baku= $('#bhn_baku').val();
	var kode_bhn_baku= $('#kode_bhn_baku').val();
	var keb= $('#keb').val();
	
	if (brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		$('#brg_jadi').focus();
		return false;
	}
	
	if (bhn_baku == '') {
		alert("Bahan Baku harus dipilih..!");
		$('#bhn_baku').focus();
		return false;
	}
	if (keb == '' || keb == '0') {
		alert("Jml kebutuhan tidak boleh kosong/nol..!");
		$('#keb').focus();
		return false;
	}
	
	if (isNaN(keb)) {
		alert("Jml kebutuhan harus berupa angka..!");
		$('#keb').focus();
		return false;
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>
<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/updatedata" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_keb" value="<?php echo $id_keb ?>">
<input type="hidden" name="cur_page" value="<?php echo $cur_page ?>">
<input type="hidden" name="is_cari" value="<?php echo $is_cari ?>">
<input type="hidden" name="carinya" value="<?php echo $carinya ?>">

<br>
Edit Data<br><br>
<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
<tr>
	<td width="15%">Ukuran Bis-Bisan</td>
	<td><?php echo $query[0]['nama_ukuran'] ?></td>
  </tr>
  
<tr>
	<td>Barang Jadi</td>
	<td><input name="brg_jadi" type="text" id="brg_jadi" size="30" readonly="true" value="<?php echo $query[0]['nama_brg_jadi'] ?>"/>
           <input name="kode_brg_jadi" type="hidden" id="kode_brg_jadi" value="<?php echo $query[0]['kode_brg_jadi'] ?>"/>
           <input name="kode_brg_jadi_lama" type="hidden" id="kode_brg_jadi_lama" value="<?php echo $query[0]['kode_brg_jadi'] ?>"/>
           <input disabled title="browse data brg jadi" name="pilih_brg_jadi" value="..." type="button" id="pilih_brg_jadi" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/show_popup_brg_jadi/');"></td>
  </tr>

<tr>
	<td>Nama Bahan Baku</td>
	<td>
			<input name="bhn_baku" type="text" id="bhn_baku" size="30" readonly="true" value="<?php echo str_replace("\"", "&quot;", $query[0]['nama_brg']); ?>"/>
           <input name="kode_bhn_baku" type="hidden" id="kode_bhn_baku" value="<?php echo $query[0]['kode_brg'] ?>"/>
           <input name="kode_bhn_baku_lama" type="hidden" id="kode_bhn_baku_lama" value="<?php echo $query[0]['kode_brg'] ?>"/>
           <input disabled title="browse data bhn baku" name="pilih_bhn_baku" value="..." type="button" id="pilih_bhn_baku" 
           onclick="javascript: var q = 'f';  
           openCenteredWindow('<?php echo base_url(); ?>index.php/mst-keb-bisbisan-perpcs/cform/show_popup_brg/'+q);" type="button"></td>
  </tr>
  <tr>
    <td>Jml Kebutuhan</td>
    <td>
      <input name="keb" type="text" id="keb" size="5" maxlength="10" value="<?php echo $query[0]['jum_kebutuhan'] ?>">
    </td>
  </tr>
  <tr><td>&nbsp;</td>
	<td>
		<?php
			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "mst-keb-bisbisan-perpcs/cform/view/index/".$cur_page;
			else
				$url_redirectnya = "mst-keb-bisbisan-perpcs/cform/cari/".$carinya."/".$cur_page;
        ?>
	<input type="submit" name="submit" value="Edit" onclick="return cek_input();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/<?php echo $url_redirectnya ?>'"></td>
  </tr>

</table>
</form>
