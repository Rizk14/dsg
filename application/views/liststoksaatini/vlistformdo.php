<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title;?></title>
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/popup_styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>asset/styles/common.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-ui-1.7.3/jquery-1.3.2.js"></script>

<script language="javascript" type="text/javascript">

function getInfo(txt){

var dd	= document.getElementById('d');
var dd2	= document.getElementById('d2');
var ccode= document.getElementById('code');

$.ajax({

type: "POST",
url: "<?php echo site_url('liststoksaatini/cform/fldo');?>",
data:"key="+txt+"&d="+dd.value+"&d2="+dd2.value+"&code="+ccode.value,
success: function(data){
$("#listItem").html(data);
},

error:function(XMLHttpRequest){
	alert(XMLHttpRequest.responseText);
}

})
};

</script>

</head>
<body id="bodylist">
<div id="main">
<br />
<?php echo "<center><h3>$page_title</h3></center>"; ?>
<table class="maintable">
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td align="right">Pencarian :
  	  <input type="text" name="txtcari" id="txtcari" style="width:200px; text-align:right;" onkeyup="getInfo(this.value);"/>
  	</td>
  </tr>
  <tr>
    <td align="left">
	<?php echo form_open('liststoksaatini/cform/caribrgjadi', array('id' => 'listformdo'));?>
	<div class="effect">
	  <div class="accordion2">

		<?php
		if( !empty($isi) || isset($isi) ) {
			$bln	= array(
				'01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember' );
			
			if(count($isi)) {
				$tanggal	= array();
				$cc	= 1;
				
				foreach($isi as $row) {
				
					$tgl	= (!empty($row->ddo) || strlen($row->ddo)!=0)?@explode("-",$row->ddo,strlen($row->ddo)):""; // YYYY-mm-dd
					$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
					$tanggal[$cc]	= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
										
					$list .= "
					 <tr>
					  <td>".$cc."</td>
					  <td>".$row->idocode."</td>
					  <td>".$row->iproduct."</td>
					  <td>".$row->eproductname."</td>
					  <td align=\"right\" >".$row->jdo."</td> 
					  <td align=\"right\" >".$tanggal[$cc]."</td>
					 </tr>";
					 $cc+=1;
				}
			}
		}
		?>
		
		<table class="listtable2">
		 <th width="2px;"><?php echo strtoupper("No"); ?></th>
		 <th width="80px;"><?php echo strtoupper("Nomor DO"); ?></th>
		 <th width="90px;"><?php echo strtoupper("Kode BRG"); ?></th>
		 <th width="240px;"><?php echo strtoupper("Nama BRG"); ?></th>
		 <th width="40px;"><?php echo strtoupper("Qty"); ?></th>
		 <th width="100px;"><?php echo strtoupper("Tgl. DO"); ?></th>
		<tbody>
		<div id="listItem"></div>
		<?php echo $list; ?>
		</tbody>
		</table>
	  <?php echo "<center>".$create_links."</center>"; ?>
  	</div>
      </div>
	  <input type="hidden" name="d" id="d" value="<?=$d?>" />
	  <input type="hidden" name="d2" id="d2" value="<?=$d2?>" />
	  <input type="hidden" name="code" id="code" value="<?=$code?>" />
      <?php echo form_close(); ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>