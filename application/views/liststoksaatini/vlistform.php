<script type="text/javascript" src="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>asset/javascript/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo $page_title_stoksaatini; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_stoksaatini; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
		<?php 
		echo $this->pquery->form_remote_tag(array('url'=>'liststoksaatini/cform/aksi','update'=>'#content','type'=>'post'));
		?>
		
		<div id="masterlstoksaatiniform">

		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="98%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="23%"><?php echo $list_stoksaatini_tgl_do_mulai; ?></td>
					<td width="1%">:</td>
					<td width="76%">
					  <input name="d_do_first" type="text" id="d_do_first" maxlength="10" value="<?php echo $tgldomulai; ?>"/>
					  <input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_first,'dd/mm/yyyy',this)">
					s.d 
					<input name="d_do_last" type="text" id="d_do_last" maxlength="10" value="<?php echo $tgldoakhir; ?>"/>
					<input type="button" value="Cal" onclick="displayCalendar(document.forms[0].d_do_last,'dd/mm/yyyy',this)">
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>
					  <input name="f_stop_produksi" type="checkbox" <?php echo $sproduksi; ?> />
					<?php echo $list_stoksaatini_s_produk; ?></td>
					<td>&nbsp;</td>
					<td><?php echo $list_stoksaatini_j_produk; ?> : 
					<select name="i_class">
					<?php
					foreach($opt_jns_brg as $row) {
						$selected = $kproduksi==$row->i_class ? " selected ":" ";
						$ljnsbrg .= "
							<option value=\"$row->i_class\" $selected >$row->e_class_name</option>
						";
					} 
					echo $ljnsbrg;
					?>
					</select>					
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>
			  <div id="title-box2"><?php echo $form_title_detail_stoksaatini; ?></div>
			  </td>	
			</tr>
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="2%" class="tdatahead" rowspan="2">No</td>
				  <td width="8%" class="tdatahead" rowspan="2"><?php echo $list_stoksaatini_kd_brg; ?> </td>
				  <td width="30%" class="tdatahead" rowspan="2"><?php echo $list_stoksaatini_nm_brg; ?> </td>
				  <td width="8%" class="tdatahead" colspan="2"><?php echo $list_stoksaatini_s_awal; ?> </td>
				  <td width="8%" class="tdatahead" colspan="2"><?php echo $list_stoksaatini_s_akhir; ?> </td>
				  <td width="8%" class="tdatahead" colspan="2"><?php echo $list_stoksaatini_bmm; ?> </td>
				  <td width="8%" class="tdatahead" rowspan="2"><?php echo $list_stoksaatini_bbm; ?></td>
				  <td width="8%" class="tdatahead" colspan="2"><?php echo $list_stoksaatini_do; ?></td>
				  <td width="8%" class="tdatahead" colspan="2"><?php echo $list_stoksaatini_bmk; ?></td>
				  <td width="8%" class="tdatahead" rowspan="2"><?php echo $list_stoksaatini_bbk; ?></td>
				</tr>
				<tr>
					<td class="tdatahead">Qty</td>
					<td class="tdatahead">Per Warna</td>
					<td class="tdatahead">Qty</td>
					<td class="tdatahead">Per Warna</td>
					<td class="tdatahead">Qty</td>
					<td class="tdatahead">Per Warna</td>
					<td class="tdatahead">Qty</td>
					<td class="tdatahead">Per Warna</td>
					<td class="tdatahead">Qty</td>
					<td class="tdatahead">Per Warna</td>
				</tr>
				<?php
				echo $query;
				?>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table></td>
			</tr>
			<tr align="center">
			  <td><?=$create_link?></td>
			</tr>			
			<tr>
			<!--	<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:245px; padding:5px;">Saldo akhir setelah dikurangi SJ Penjualan</div></td> -->
				<td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:245px; padding:5px;">SJ Penjualan tidak diperhitungkan lagi sebagai pengurang stok</div></td>
			</tr>			
			<tr align="right">
			  <td>
				<!--
				<input name="btnbuka" type="button" id="btnbuka" value="<?php //echo $button_excel; ?>" />
				<input name="btnstok" type="button" id="btnstok" value="<?php //echo $button_cetak; ?>"/>
				-->
				
				<input type="button" name="btnkeluar" value="<?php echo $button_keluar; ?>"
				onclick="window.location='<?php echo base_url() ?>index.php/liststoksaatini/cform'"> 

			  </td>
			</tr>
		  </table>
		</div>
		<?php echo form_close(); ?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
