<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">
	function konfirm() {

		//var iter;
		var kon;
		var j = 0;
		var s = 0;

		kon = window.confirm("Yakin akan simpan data ini ??");

		if (kon) {
			if (document.getElementById('i_customer').value == '') {
				alert('Pilih Pelanggan !!');
				return false;
			}

			if (document.getElementById('i_branch').value == '') {
				alert('Pilih Cabang Pelanggan !!');
				return false;
			}

			// 14-03-2015
			var ada_op_beda = document.getElementById('ada_op_beda');
			if (ada_op_beda.value == '1') {
				alert('Ada nomor OP yg beda. Nomor OP harus seragam, silahkan dipilih ulang OPnya');
				return false;
			}

			var iter = document.getElementById('jumdata');

			if (iter.value != '') {
				while (j < iter.value) {
					var n_deliver = document.getElementById('n_deliver_tblItem_' + j).value;
					if (n_deliver == 0 || n_deliver == '') {
						s = 1;
						alert('Jml DO tdk boleh nol!');
						return false;
					}

					if ($('input[name=is_grosir_' + j + ']').is(':checked')) {
						//v_do_gross_tblItem_
						var harga_grosir = $('#grosir_tblItem_' + j).val();
						var qty = $('#n_deliver_tblItem_' + j).val();
						var subtotal = parseInt(harga_grosir) * parseInt(qty);
						$('#v_do_gross_tblItem_' + j).val(subtotal);
					} else {
						var hjp = $('#price_tblItem_' + j).val();
						var qty = $('#n_deliver_tblItem_' + j).val();
						var subtotal = parseInt(hjp) * parseInt(qty);
						$('#v_do_gross_tblItem_' + j).val(subtotal);
					}

					var kodebrg = $('#i_product_tblItem_' + j).val();
					var qtytotal = $('#n_deliver_tblItem_' + j).val();
					var qty_warna = document.forms[0].elements["qty_warna_" + j + "[]"];
					//alert(qty_warna.length);
					var totqty = 0;

					if (qty_warna.length > 1) {
						for (var i = 0, len = qty_warna.length; i < len; i++) {

							totqty += parseInt(qty_warna[i].value);
						}
					} else {
						totqty = parseInt(qty_warna.value);
					}


					//alert(totqty);
					if (parseInt(totqty) > parseInt(qtytotal)) {
						alert("qty warna utk barang " + kodebrg + " melebihi qty total, silahkan dikoreksi dulu ya");
						return false;
					}
					/*else if (parseInt(totqty) < parseInt(qtytotal)) {
						alert("qty warna utk barang "+kodebrg+" kurang dari qty total, silahkan dikoreksi dulu ya");
						return false;
					} */

					j += 1;
				} // end while
			} else {
				alert("Item barang di DO tidak ada");
				return false;
			}
			//alert(iter.value);
			//return false;
		} else
			return false;

		/*iter = document.getElementById('jumdata');

		if(document.getElementById('i_customer').value=='') {
			alert('Pilih Pelanggan !!');
			return false;
		}
		
		if(document.getElementById('i_branch').value=='') {
			alert('Pilih Cabang Pelanggan !!');
			return false;
		} */

		/*if(iter.value!='') {
			while(j<=iter.value) {
				var n_deliver = document.getElementById('n_deliver_tblItem_'+j).value;
				if(n_deliver==0 || n_deliver=='') {
					s = 1;
					alert('Jml DO tdk boleh nol!');
					break;
				}
				if ($('input[name=is_grosir_'+j+']').is(':checked')) {
					//v_do_gross_tblItem_
					var harga_grosir= $('#grosir_tblItem_'+j).val();
					var qty = $('#n_deliver_tblItem_'+j).val();
					var subtotal = parseInt(harga_grosir)*parseInt(qty);
					$('#v_do_gross_tblItem_'+j).val(subtotal);
				}
				else {
					var hjp= $('#price_tblItem_'+j).val();
					var qty = $('#n_deliver_tblItem_'+j).val();
					var subtotal = parseInt(hjp)*parseInt(qty);
					$('#v_do_gross_tblItem_'+j).val(subtotal);
				}
				
				j+=1;
			}
			
			if(s==0){
				kon = window.confirm("yakin akan simpan ??");
				
				if(kon){
					return true;
				}else{
					return false;
				}
			}else{
				document.getElementById('n_deliver_tblItem_'+j).focus();
				return false;
			}
		}else{
			alert('Tdk ada data DO yg akan disimpan!');
			return false;
		} */
	}

	// 23-06-2012
	function hitungnilai() {
		var iter = document.getElementById('jumdata');
		var j = 0;
		if (iter.value != '') {
			while (j < iter.value) {
				if ($('input[name=is_grosir_' + j + ']').is(':checked')) {
					//v_do_gross_tblItem_
					var harga_grosir = $('#grosir_tblItem_' + j).val();
					var qty = $('#n_deliver_tblItem_' + j).val();
					var subtotal = parseInt(harga_grosir) * parseInt(qty);
					$('#v_do_gross_tblItem_' + j).val(subtotal);
				} else {
					var hjp = $('#price_tblItem_' + j).val();
					var qty = $('#n_deliver_tblItem_' + j).val();
					var subtotal = parseInt(hjp) * parseInt(qty);
					//alert(qty);
					$('#v_do_gross_tblItem_' + j).val(subtotal);
				}
				j += 1;
			}
		}


	}

	function totalharga(jml, iterasi) {
		var harga;
		var totalharga;
		harga = document.getElementById('price_tblItem_' + iterasi);
		if (jml > 0) {
			totalharga = parseInt(harga.value) * jml;
			if (isNaN(totalharga)) {
				document.getElementById('v_do_gross_tblItem_' + iterasi).value = totalharga;
			} else {
				totalharga = parseInt(harga.value) * jml;
				document.getElementById('v_do_gross_tblItem_' + iterasi).value = totalharga;
			}
		} else {
			document.getElementById('v_do_gross_tblItem_' + iterasi).value = harga.value;
		}
	}

	function validNum(column, iterasi) {
		var angka = document.getElementById(column + '_' + iterasi);
		if (!parseInt(angka.value)) {
			alert("Maaf, Kolom Pengisian hrs angka.Terimakasih.");
			angka.value = 0;
			angka.focus();
		}
	}

	function validStok(iterasi) {
		/* dgn qty op */
		var qty = document.getElementById('qty_op_tblItem_' + iterasi);
		var masukan = document.getElementById('n_deliver_tblItem_' + iterasi);
		if (masukan.value > parseInt(qty.value)) {
			alert("Maaf, quantity melebihi quantity OP yg ada!");
			masukan.value = qty.value;
			masukan.focus();
		}
	}

	function validStok2(iterasi) {
		var qtyop = document.getElementById('qty_op_tblItem_' + iterasi);
		var qty = document.getElementById('qty_product_tblItem_' + iterasi);
		var masukan = document.getElementById('n_deliver_tblItem_' + iterasi);
		if (masukan.value > parseInt(qty.value)) {
			alert("Maaf, quantity melebihi stok yg ada!");
			masukan.focus();
		}
	}

	function getCabang(nilai) {

		$.ajax({

			type: "POST",
			url: "<?php echo site_url('dopopup/cform/cari_cabang'); ?>",
			data: "ibranch=" + nilai,
			success: function(data) {
				$("#cboPelanggan").html(data);

			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function ckdo(nomor) {

		$.ajax({

			type: "POST",
			url: "<?php echo site_url('dopopup/cform/cari_do'); ?>",
			data: "ndo=" + nomor,
			success: function(data) {
				$("#confnomordo").html(data);
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	/*
	function showData(nItem) {
		if (document.getElementById(nItem+'_select').style.display=='none') {			
			document.getElementById(nItem+'_select').style.display='block';		
			document.getElementById(nItem+'_select').style.position='absolute';	
		} else {
			document.getElementById(nItem).style.display='block';
			document.getElementById(nItem+'_select').style.display='none';
		} 
	}
	  
	function hideData(nItem){
	}

	function addRowToTable(nItem) {
		var tbl = document.getElementById(nItem);
		var lastRow = tbl.rows.length;
		var iteration = lastRow;
		var row = tbl.insertRow(lastRow);
		
		if(iteration<22){
			var cust= document.getElementById('i_customer');
			var cab	= document.getElementById('i_branch');
		
			tbl.width='100%';
		
			if(iteration%2==0){
				var bgColorValue = '#FFFFFF';
			}else{
				var bgColorValue = '#FFFFFF';
			}
			row.bgColor = bgColorValue;

			var cell1 = row.insertCell(0);
			cell1.innerHTML = "<div style=\"font:11px/24px;text-align:right;width:10px;margin-right:0px;\">"+(iteration*1+1)+".</div>";

			var cell1 = row.insertCell(1);
			cell1.innerHTML = "<DIV ID=\"ajax_i_op_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"i_op_"+nItem+"_"+iteration+"\"  name=\"i_op_"+nItem+"_"+iteration+"\" style=\"width:55px;\" onclick=\"shprodukdo('"+iteration+"','"+document.getElementById('i_branch').value+"','"+document.getElementById('i_customer').value+"');\" readonly ><img name=\"img_i_op_\" src=\"<?php echo base_url(); ?>asset/theme/images/table.gif\" border=\"0\" title=\"Tampilkan Nomor OP\" class=\"imgLink\" align=\"absmiddle\" onclick=\"shprodukdo('"+iteration+"','"+document.getElementById('i_branch').value+"','"+document.getElementById('i_customer').value+"');\"><input type=\"hidden\" name=\"qty_product_"+nItem+"_"+iteration+"\" id=\"qty_product_"+nItem+"_"+iteration+"\"><input type=\"hidden\" name=\"qty_op_"+nItem+"_"+iteration+"\" id=\"qty_op_"+nItem+"_"+iteration+"\"></DIV>";

			var cell1 = row.insertCell(2);
			cell1.innerHTML = "<DIV ID=\"ajax_i_product_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"i_product_"+nItem+"_"+iteration+"\"  name=\"i_product_"+nItem+"_"+iteration+"\" style=\"width:140px;\" readonly ></DIV>" +
			"<div id=\"ajax_i_product_"+nItem+"_"+iteration+"_select\" style=\"display:none;\">" +
			"</div>";

			var cell1 = row.insertCell(3);
			cell1.innerHTML = "<DIV ID=\"ajax_e_product_name_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"e_product_name_"+nItem+"_"+iteration+"\"  name=\"e_product_name_"+nItem+"_"+iteration+"\" style=\"width:160px;\" readonly ></DIV>";

			var cell1 = row.insertCell(4);
			cell1.innerHTML = "<DIV ID=\"ajax_n_deliver_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"n_deliver_"+nItem+"_"+iteration+"\"  name=\"n_deliver_"+nItem+"_"+iteration+"\" style=\"width:140px;text-align:right;\" onkeyup=\"validNum('n_deliver_tblItem','"+iteration+"');validStok("+iteration+");validStok2("+iteration+");totalharga(this.value,"+iteration+");\"></DIV>";

			var cell1 = row.insertCell(5);
			cell1.innerHTML = "<DIV ID=\"ajax_v_do_gross_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"v_do_gross_"+nItem+"_"+iteration+"\"  name=\"v_do_gross_"+nItem+"_"+iteration+"\" style=\"width:115px;text-align:right;\" onkeyup=\"validNum('v_do_gross_tblItem','"+iteration+"')\" readonly ></DIV>";

			var cell1 = row.insertCell(6);
			cell1.innerHTML = "<DIV ID=\"ajax_e_note_"+nItem+"_"+iteration+"\" ><input type=\"text\" ID=\"e_note_"+nItem+"_"+iteration+"\"  name=\"e_note_"+nItem+"_"+iteration+"\" style=\"width:120px;\"><input type=\"hidden\" ID=\"price_"+nItem+"_"+iteration+"\"  name=\"price_"+nItem+"_"+iteration+"\" ><input type=\"hidden\" ID=\"i_op_sebunyi_"+nItem+"_"+iteration+"\"  name=\"i_op_sebunyi_"+nItem+"_"+iteration+"\" ><input type=\"hidden\" ID=\"f_stp_"+nItem+"_"+iteration+"\"  name=\"f_stp_"+nItem+"_"+iteration+"\" ><input type=\"hidden\" name=\"iteration\" id=\"iteration\" value=\""+iteration+"\"></DIV>";
			
			document.getElementById('iter').value = iteration;
			
		}else{
			alert('item Order tdk dpt lebih dari 22 item barang!');
			return false;
		}
	}

	function removeRowFromTable(nItem) {
		var tbl = document.getElementById(nItem);
		var lastRow = tbl.rows.length;
		if (lastRow > 0) {
			tbl.width='100%';
			tbl.deleteRow(lastRow - 1);
		}
	}
	*/

	function cekqtywarna() {
		var iter = document.getElementById('jumdata');
		var j = 0;
		if (iter.value != '') {
			while (j < iter.value) {
				//var qty_warna= $('#qty_warna_'+j).val();
				//var qty_warna = document.getElementById('qty_warna_'+j);

				var qtytotal = $('#n_deliver_tblItem_' + j).val();
				var qty_warna = document.forms[0].elements["qty_warna_" + j + "[]"];
				//alert(qty_warna.length);
				var totqty = 0;
				for (var i = 0, len = qty_warna.length; i < len; i++) {
					//alert(qty_warna[i].value);
					totqty += parseInt(qty_warna[i].value);
				}

				if (totqty > qtytotal) {
					alert("qty warna ada yg melebihi qty total, silahkan dikoreksi dulu ya");
					return false;
				}

				j += 1;
			}
		}
		return false;
	}
</script>
<!--

<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$("#d_do").datepicker();
});
</script>
-->

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
		<td class="tcat"><?php echo $page_title_do; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
		<td class="thead"><img src="<?php echo base_url(); ?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_do; ?></td>
	</tr>
	<tr>
		<td class="alt2" style="padding:0px;">

			<table id="table-add-box">
				<tr>
					<td align="left">
						<?php
						$attributes = array('class' => 'f_master', 'id' => 'myform');
						echo form_open('dopopup/cform/simpan', $attributes); ?>

						<input type="hidden" name="jumdata" id="jumdata" value="<?php echo $jumdata ?>">
						<div id="masterdoform">

							<table>
								<tr>
									<td width="10%"><?php echo $form_nomor_do; ?></td>
									<td width="1%">:</td>
									<td width="18%">
										<?php
										$nodo = array(
											'name' => 'i_do',
											'id' => 'i_do',
											'value' => $no,
											'maxlength' => '14',
											'onkeyup' => 'ckdo(this.value)'
										);
										echo form_input($nodo);
										?>
										<div id="confnomordo" style="color:#FF0000;"></div>
									</td>
									<td width="6%"><?php echo $form_tgl_do; ?></td>
									<td width="1%">:</td>
									<td width="18%">
										<input name="d_do" type="text" id="d_do" size="10" value="<?php echo $tgDO ?>" readonly="true"> <!-- onclick="displayCalendar(document.forms[0].d_do,'dd/mm/yyyy',this)" -->

									</td>
									<td width="10%"><?php echo "Pelanggan"; ?></td>
									<td width="1%">:</td>
									<td width="18%">
										<?php
										$lpelanggan	.= "<select name=\"i_customer\" id=\"i_customer\" disabled >";
										foreach ($opt_pelanggan as $key => $row) {
											$Sel = $icustomer == $row->code ? "selected" : "";
											$lpelanggan .= "<option value=\"$row->code\" $Sel >" . $row->customer . "</option>";
										}
										$lpelanggan	.= "</select>";

										$lcabang	.= "<select name=\"i_branch\" id=\"i_branch\" disabled >";
										foreach ($opt_cabang as $key => $row) {
											$Sel = $ibranch == $row->codebranch ? "selected" : "";
											$lcabang .= "<option value=\"$row->codebranch\" $Sel >" . $row->branch . " ( " . $row->einitial . " ) " . "</option>";
										}
										$lcabang	.= "</select>";
										echo $lpelanggan . " " . $lcabang;
										?>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="10">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="10">
										<div id="title-box2"><?php echo $form_title_detail_do; ?>
											<div style="float:right;">
												<!-- 
			  <a class="button" href="#" title="Add Baris Data" onclick="this.blur();addRowToTable('tblItem');"><span><img src="<?php //echo base_url();
																																?>asset/images/icon/blank.gif" align="absmiddle">+</span></a>
			  <a class="button" href="#" title="Hapus Baris Data Terakhir" onclick="this.blur();removeRowFromTable('tblItem');"><span><img src="<?php //echo base_url();
																																				?>asset/images/icon/blank.gif" align="absmiddle">-</span></a>
			  -->
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="10">
										<table width="100%">
											<tr>
												<td width="2%" class="tdatahead">NO</td>
												<td width="10%" class="tdatahead"><?php echo $form_nomor_op_do; ?></td>
												<td width="13%" class="tdatahead"><?php echo $form_kode_produk_do; ?></td>
												<td width="23%" class="tdatahead"><?php echo $form_nm_produk_do; ?></td>
												<td width="5%" class="tdatahead"><?php echo $form_jml_product_do; ?></td>
												<!-- <td width="10%" class="tdatahead">HJP</td>
					 <td width="10%" class="tdatahead" nowrap>HARGA GROSIR</td> -->
												<!--<td width="10%" class="tdatahead"><?php //echo "SUBTOTAL"; //echo $form_harga_product_do; 
																						?></td> -->
												<td width="5%" class="tdatahead" nowrap>QTY<br>PERWARNA</td>
												<td width="10%" class="tdatahead" nowrap>ADA BONEKA?</td>
												<td width="20%" class="tdatahead"><?php echo $form_ket_do; ?></td>
											</tr>
											<?php
											echo $List;
											?>
											<!--	<tr>
					 <td colspan="7">
					  <table id="tblItem" cellspacing="0" cellpadding="0" width="100%" border="0">
					  <?php
						//echo $List;
						?>
					  </table>
					</tr> -->
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="10" align="right">
										<!-- <input type="hidden" name="iter" id="iter" value=""> -->
										<input type="hidden" name="ibranch" id="ibranch" value="<?= $ibranch ?>">
										<input type="hidden" name="icustomer" id="icustomer" value="<?= $icustomer ?>">
										<input type="hidden" name="ada_op_beda" id="ada_op_beda" value="<?php echo $ada_op_beda ?>">
										<input name="btnsimpan" id="btnsimpan" value="<?php echo $button_simpan; ?>" type="submit" onclick="return konfirm();">
										<input name="btnbatal" id="btnbatal" value="<?php echo $button_batal; ?>" type="button" onclick="window.location='<?php echo base_url(); ?>index.php/dopopup/cform'">
									</td>
								</tr>
							</table>
						</div>
						<?php echo form_close(); ?>
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>