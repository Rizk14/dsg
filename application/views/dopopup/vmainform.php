<script type="text/javascript" src="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen">
</link>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
<script language="javascript" type="text/javascript">
	function konfirm(ibranch, icustomer) {

		lebar = 700;
		tinggi = 400;

		if (ibranch == '' || icustomer == '') {
			alert('Pelanggan hrs dipilih !!');
			return false;
		} else {
			eval('window.open("<?php echo site_url(); ?>"+"/dopopup/cform/listdetailop/"+ibranch+"/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
		}
	}

	function konfirm2() {
		if (document.getElementById('i_customer').value == '') {
			alert('Pelanggan hrs dipilih !!');
			return false;
		}

		if (document.getElementById('i_branch').value == '') {
			alert('Pelanggan Cabang hrs dipilih !!');
			return false;
		}

		if (document.getElementById('i_op_code').value == '') {
			alert('Nomor OP hrs diisi !!');
			return false;
		}

	}

	function getCabang(nilai) {

		$.ajax({

			type: "POST",
			url: "<?php echo site_url('dopopup/cform/cari_cabang'); ?>",
			data: "ibranch=" + nilai,
			success: function(data) {
				$("#cboPelanggan").html(data);

			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};
</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
	<tr>
		<td class="tcat"><?php echo $page_title_do; ?></td>
	</tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
	<tr>
		<td class="thead"><img src="<?php echo base_url(); ?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo $page_title_do; ?></td>
	</tr>
	<tr>
		<td class="alt2" style="padding:0px;">

			<table id="table-add-box">
				<tr>
					<td align="left">
						<?php
						$attributes = array('class' => 'f_master', 'id' => 'myform');
						echo form_open('dopopup/cform/detailsimpan', $attributes); ?>


						<div id="masterfpenjualandoform">

							<table width="60%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<table width="98%" border="0" cellspacing="0" cellpadding="0">

											<tr>
												<td width="18%"><?php echo "Pelanggan"; ?></td>
												<td width="1%">:</td>
												<td>
													<?php
													$lpelanggan	.= "<select name=\"i_customer\" onchange=\"getCabang(this.value);\" id=\"i_customer\" >";
													$lpelanggan	.= "<option value=\"\">[" . $form_option_pel_do . "]</option>";
													foreach ($opt_pelanggan as $key => $row) {
														$lpelanggan .= "<option value=\"$row->code\" >" . $row->customer . "</option>";
													}
													$lpelanggan	.= "</select>";
													echo $lpelanggan;
													?>
													<div id="cboPelanggan"></div>
												</td>
											</tr>
											<tr>
												<td width="20%"><?php echo "Nomor OP"; ?></td>
												<td width="1%">:</td>
												<td>
													<input type="text" name="i_op_code" id="i_op_code" maxlength="14" readonly />
													<input type="hidden" name="i_op" id="i_op" />
													<input type="hidden" name="i_product" id="i_product" />
													<img name="img_i_op_" src="<?php echo base_url(); ?>asset/theme/images/table.gif" border="0" title="Tampilkan Nomor OP" class="imgLink" align="absmiddle" onclick="konfirm(document.getElementById('i_branch').value,document.getElementById('i_customer').value);">
												</td>
											</tr>

											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="3">
													<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" onclick="return konfirm2();" />
													<input name="btnbatal" type="reset" id="btnbatal" value="<?php echo $button_batal; ?>" />
												</td>
											</tr>
										</table>

						</div>
						<?php echo form_close(); ?>
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>