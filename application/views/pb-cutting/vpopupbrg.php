<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}

</style>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script>
$(function()
{
	$(".pilih").click(function()
	{
		var idx=$("#idx").val();
		var des=$("#des").val();
		var qty=$("#qty").val();
		var satuan=$("#satuan").val();
		var gelaran=$("#gelaran").val();
		var jum_set=$("#jum_set").val();
		var id_marker=$("#id_marker").val();
		var id_kebutuhan=$("#id_kebutuhan").val();
		var jum_kebutuhan=$("#jum_kebutuhan").val();
		var var1=$("#var1").val();
		var var2=$("#var2").val();
		var var3=$("#var3").val();
		var q=$("#q").val();
		
		if (qty == undefined || qty == '')
			qty = 0;
		
		if (q == 'f') {
			opener.document.forms["f_purchase"].kode_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].nama_<?php echo $posisi ?>.value=des;
			//opener.document.forms["f_purchase"].satuan_<?php echo $posisi ?>.value=satuan;
			opener.document.forms["f_purchase"].gelaran_<?php echo $posisi ?>.value=gelaran;
			opener.document.forms["f_purchase"].set_<?php echo $posisi ?>.value=jum_set;
			opener.document.forms["f_purchase"].id_marker_<?php echo $posisi ?>.value=id_marker;
			opener.document.forms["f_purchase"].id_kebutuhanperpcs_<?php echo $posisi ?>.value=id_kebutuhan;
			opener.document.forms["f_purchase"].keb_<?php echo $posisi ?>.value=jum_kebutuhan;
			opener.document.forms["f_purchase"].var1_<?php echo $posisi ?>.value=var1;
			opener.document.forms["f_purchase"].var2_<?php echo $posisi ?>.value=var2;
			opener.document.forms["f_purchase"].var3_<?php echo $posisi ?>.value=var3;
			
			opener.document.forms["f_purchase"].qty_<?php echo $posisi ?>.focus();
			self.close();
		}
		else {
			opener.document.forms["f_purchase"].kode_brg_quilting_<?php echo $posisi ?>.value=idx;
			opener.document.forms["f_purchase"].brg_quilting_<?php echo $posisi ?>.value=des;
			self.close();
		}
		
	});
});
</script>
<center>
<?php
	if ($quilting == 'f') {
?>
<h3>Daftar Barang Untuk Kelompok <?php echo $nama_kel ?></h3>
<?php
	} else {
?>
<h3>Daftar Barang Quilting</h3>
<?php
	}
?>

</center>
<div align="center"><br>
<?php echo form_open('pb-cutting/cform/show_popup_brg'); ?>
<?php
	if ($quilting == 'f') {
?>
Jenis Bahan <select name="id_jenis_bhn" id="id_jenis_bhn">
				<option value="0" <?php if ($cjenis_bhn == '') { ?> selected="true" <?php } ?> >- All -</option>
				<?php foreach ($jenis_bhn as $jns) { ?>
					<option value="<?php echo $jns->id ?>" <?php if ($cjenis_bhn == $jns->id) { ?> selected="true" <?php } ?> ><?php echo "[".$jns->nj_brg."] ". $jns->kode." - ".$jns->nama ?></option>
				<?php } ?>
				</select> &nbsp;
<?php
	} else {
		echo "<input type='hidden' name='id_jenis_bhn' id='id_jenis_bhn' value='0' >";
	}
?>

<input type="text" name="cari" value="<?php echo $cari ?>">
&nbsp;<input type="submit" name="submit" value="Cari">
<input type="hidden" name="posisi" value="<?php echo $posisi ?>">
<input type="hidden" name="kel_brg" value="<?php echo $kel_brg ?>">
<input type="hidden" name="quilting" value="<?php echo $quilting ?>">
<input type="hidden" name="kode_brg_jadi" value="<?php echo $kode_brg_jadi ?>">
<input type="hidden" name="kode_brg_quilting" value="<?php echo $kode_brg_quilting ?>">
<input type="hidden" name="jenis_proses" value="<?php echo $jenis_proses ?>">
<input type="hidden" name="uk_bisbisan" value="<?php echo $uk_bisbisan ?>">
<input type="hidden" name="motif" value="<?php echo $motif ?>">
<?php echo form_close(); ?>

<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="idx" id="idx">
<input type="hidden" name="des" id="des">
<input type="hidden" name="qty" id="qty">
<input type="hidden" name="satuan" id="satuan">
<input type="hidden" name="gelaran" id="gelaran">
<input type="hidden" name="jum_set" id="jum_set">
<input type="hidden" name="id_marker" id="id_marker">
<input type="hidden" name="id_kebutuhan" id="id_kebutuhan">
<input type="hidden" name="jum_kebutuhan" id="jum_kebutuhan">
<input type="hidden" name="var1" id="var1">
<input type="hidden" name="var2" id="var2">
<input type="hidden" name="var3" id="var3">
<input type="hidden" name="q" id="q" value="<?php echo $quilting ?>">
</form>

  <table border="1" align="center" cellpadding="1" width="100%" cellspacing="2" bordercolor="#666666" class="proit-view" >
    <tr>
      <th bgcolor="#999999">No</th>
      <th bgcolor="#999999">Kode</th>
      <th bgcolor="#999999">Nama Barang</th>
      <th bgcolor="#999999">Satuan</th>
      <th bgcolor="#999999">Stok Terkini</th>
      <th bgcolor="#999999">Action</th>
    </tr>
	<?php 
		//$i=1; 
		
		if ($startnya == '')
			$i=1;
		else
			$i = $startnya+1; 
				
	if (is_array($query)) {
		for($j=0;$j<count($query);$j++) {
	?>
	
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $query[$j]['kode_brg']; ?></td>
      <td nowrap><?php echo $query[$j]['nama_brg']; ?></td>
      <td><?php echo $query[$j]['satuan']; ?></td>
      <td><?php if ($query[$j]['jum_stok']!= 0) echo $query[$j]['jum_stok']; else echo "0"; ?></td>
      <td align="center">
	  <a class="pilih" style="cursor:pointer" id="pilih" onMouseOver="window.document.f_master_brg.idx.value='<?php echo $query[$j]['kode_brg'] ?>';
	  window.document.f_master_brg.des.value='<?php 
	  $pos = strpos($query[$j]['nama_brg'], "\"");
	  if ($pos > 0)
		echo str_replace("\"", "&quot;", $query[$j]['nama_brg']);
	  else
		echo str_replace("'", "\'", $query[$j]['nama_brg']);
	  
	  //if($quilting == 't') echo str_replace("\"", "&quot;", $query[$j]['nama_brg']); else echo str_replace("'", "\'", $query[$j]['nama_brg']); ?>'; 
	  window.document.f_master_brg.qty.value='<?php echo $query[$j]['jum_stok'] ?>'; 
	  window.document.f_master_brg.satuan.value='<?php echo $query[$j]['satuan'] ?>';
	  window.document.f_master_brg.gelaran.value='<?php echo $query[$j]['gelaran'] ?>';
	  window.document.f_master_brg.jum_set.value='<?php echo $query[$j]['jum_set'] ?>';
	  window.document.f_master_brg.id_marker.value='<?php echo $query[$j]['id_marker'] ?>';
	  window.document.f_master_brg.id_kebutuhan.value='<?php echo $query[$j]['id_kebutuhan'] ?>';
	  window.document.f_master_brg.jum_kebutuhan.value='<?php echo $query[$j]['jum_kebutuhan'] ?>';
	  window.document.f_master_brg.var1.value='<?php echo $query[$j]['var1'] ?>';
	  window.document.f_master_brg.var2.value='<?php echo $query[$j]['var2'] ?>';
	  window.document.f_master_brg.var3.value='<?php echo $query[$j]['var3'] ?>'; ">Pilih</a></td>
    </tr>
	<?php $i++; } 
	}
	?>
  </table>
  <? echo $this->pagination->create_links();?>
</div>
