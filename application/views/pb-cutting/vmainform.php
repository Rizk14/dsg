<h3>Data Permintaan Bahan Baku Ke Gudang</h3><br>
<a href="<? echo base_url(); ?>index.php/pb-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/pb-cutting/cform/view">View Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/pb-cutting/cform/print_pb">Print</a><br><br>

<?php if ($msg != '') echo "<i>".$msg."</i><br>"; ?>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script>
//tambah
$(function()
{
	$("#no").val('2');
	
	//generate_nomor();
	$("#ukuran_bisbisan_1").attr("disabled", true);
	$("#pilih_bisbisan_1").attr("disabled", true);
	$("#pilih_quilting_1").attr("disabled", true);
		
	$("#returpurchase").click(function()
	{
		//inisialisasi untuk id unik
		var no=$("#no").val();		
		var n=parseInt(no)-1;
		//copy last row
		var lastRow = $("#tabelku tr:last").clone();
		//----------------SETTING KONDISI ROW BARU*****
		//******no*************************************
		var num="#num_"+n;
		var new_num="#num_"+no;
		$(num, lastRow).attr("id", "num_"+no);		
		$(new_num, lastRow).html(no);		
		//******end no*********************************
		
		//*****jenis_proses*************************************
		var jenis_proses="#jenis_proses_"+n;
		var new_jenis_proses="#jenis_proses_"+no;
		$(jenis_proses, lastRow).attr("id", "jenis_proses_"+no);
		$(new_jenis_proses, lastRow).attr("name", "jenis_proses_"+no);	
		var even_change = "$('#brg_quilting_"+no+"').val(''); $('#kode_brg_quilting_"+no+"').val(''); $('#brg_bisbisan_"+no+"').val(''); $('#kode_brg_bisbisan_"+no+"').val(''); $('#kode_"+no+"').val(''); $('#nama_"+no+"').val(''); $('#qty_"+no+"').val(''); $('#gelaran_"+no+"').val(''); $('#set_"+no+"').val(''); $('#id_kebutuhanperpcs_"+no+"').val(''); $('#keb_"+no+"').val(''); $('#var1_"+no+"').val(''); $('#var2_"+no+"').val(''); $('#var3_"+no+"').val(''); if (this.value == '3' || this.value == '5') { $('#pilih_quilting_"+no+"').attr('disabled',false); } else { $('#pilih_quilting_"+no+"').attr('disabled',true); } if (this.value == '4') { $('#pilih_bisbisan_"+no+"').attr('disabled',false); $('#ukuran_bisbisan_"+no+"').attr('disabled',false); } else { $('#pilih_bisbisan_"+no+"').attr('disabled',true); $('#ukuran_bisbisan_"+no+"').attr('disabled',true); } ";	
		$(new_jenis_proses, lastRow).attr("onchange",even_change);
		//*****end jenis_proses*********************************
		
		//*****bordir*************************************
		var bordir="#bordir_"+n;
		var new_bordir="#bordir_"+no;
		$(bordir, lastRow).attr("id", "bordir_"+no);
		$(new_bordir, lastRow).attr("name", "bordir_"+no);		
		$(new_bordir, lastRow).val('t');
		//*****end bordir*********************************
		
		//*****kcutting*************************************
		var kcutting="#kcutting_"+n;
		var new_kcutting="#kcutting_"+no;
		$(kcutting, lastRow).attr("id", "kcutting_"+no);
		$(new_kcutting, lastRow).attr("name", "kcutting_"+no);		
		$(new_kcutting, lastRow).val('t');
		// onclick="javascript: if ($('input[name=kcutting_1]').is(':checked') ) { $('input[name=bscutting_1]').attr('checked', false); $('input[name=bagian_bs_1]').attr('readonly', true); } "
		var even_klikkcutting = "if ($('input[name=kcutting_"+no+"]').is(':checked') ) { $('input[name=bscutting_"+no+"]').attr('checked', false); $('input[name=bagian_bs_"+no+"]').attr('readonly', true); } ";
		$(new_kcutting, lastRow).attr("onclick",even_klikkcutting);
		$(new_kcutting, lastRow).attr("checked", false);
		//*****end kcutting*********************************
		
		//*****bscutting*************************************
		var bscutting="#bscutting_"+n;
		var new_bscutting="#bscutting_"+no;
		$(bscutting, lastRow).attr("id", "bscutting_"+no);
		$(new_bscutting, lastRow).attr("name", "bscutting_"+no);		
		$(new_bscutting, lastRow).val('t');
		// onclick="javascript: if ($('input[name=bscutting_1]').is(':checked') ) { $('input[name=kcutting_1]').attr('checked', false); $('input[name=bagian_bs_1]').attr('readonly', false); } else { $('input[name=bagian_bs_1]').attr('readonly', true); } "
		var even_klikbscutting = "if ($('input[name=bscutting_"+no+"]').is(':checked') ) { $('input[name=kcutting_"+no+"]').attr('checked', false); $('input[name=bagian_bs_"+no+"]').attr('readonly', false); } else { $('input[name=bagian_bs_"+no+"]').attr('readonly', true); } ";
		$(new_bscutting, lastRow).attr("onclick",even_klikbscutting);
		$(new_bscutting, lastRow).attr("checked", false);
		//*****end bscutting*********************************
		
		//*****bagian_bs*************************************
		var bagian_bs="#bagian_bs_"+n;
		var new_bagian_bs="#bagian_bs_"+no;
		$(bagian_bs, lastRow).attr("id", "bagian_bs_"+no);
		$(new_bagian_bs, lastRow).attr("name", "bagian_bs_"+no);		
		$(new_bagian_bs, lastRow).val('');		
		//*****end bagian_bs*********************************
		
		// kebbismanual_1
		//*****kebbismanual*************************************
		var kebbismanual="#kebbismanual_"+n;
		var new_kebbismanual="#kebbismanual_"+no;
		$(kebbismanual, lastRow).attr("id", "kebbismanual_"+no);
		$(new_kebbismanual, lastRow).attr("name", "kebbismanual_"+no);		
		$(new_kebbismanual, lastRow).val('t'); //
		// onclick="javascript: if ($('input[name=kebbismanual_1]').is(':checked') ) { $('#id_kebutuhanperpcs_1').val('0'); $('#keb_1').val('0'); $('input[name=vkebbismanual_1]').attr('readonly', false); } else { $('input[name=vkebbismanual_1]').attr('readonly', true); $('#vkebbismanual_1').val(''); } "
		var even_kebbismanual = "hitungulang("+no+"); if ($('input[name=kebbismanual_"+no+"]').is(':checked') ) { $('input[name=vkebbismanual_"+no+"]').attr('readonly', false); } else { $('input[name=vkebbismanual_"+no+"]').attr('readonly', true); $('#vkebbismanual_"+no+"').val(''); } ";
		$(new_kebbismanual, lastRow).attr("onclick",even_kebbismanual);
		$(new_kebbismanual, lastRow).attr("checked", false);
		//*****end new_kebbismanual*********************************
		
		//*****vkebbismanual*************************************
		var vkebbismanual="#vkebbismanual_"+n;
		var new_vkebbismanual="#vkebbismanual_"+no;
		$(vkebbismanual, lastRow).attr("id", "vkebbismanual_"+no);
		$(new_vkebbismanual, lastRow).attr("name", "vkebbismanual_"+no);		
		$(new_vkebbismanual, lastRow).val('');		
		// onkeyup="hitungulang(1)" onblur="hitungulang(1)"
		$(new_vkebbismanual, lastRow).attr("onkeyup","hitungulang("+no+")");
		$(new_vkebbismanual, lastRow).attr("onblur","hitungulang("+no+")");
		//*****end vkebbismanual*********************************
		
		//*****brg_quilting*************************************
		var brg_quilting="#brg_quilting_"+n;
		var new_brg_quilting="#brg_quilting_"+no;
		$(brg_quilting, lastRow).attr("id", "brg_quilting_"+no);
		$(new_brg_quilting, lastRow).attr("name", "brg_quilting_"+no);		
		$(new_brg_quilting, lastRow).val('');		
		//*****end brg_quilting*********************************
		
		//*****kode_brg_quilting*************************************
		var kode_brg_quilting="#kode_brg_quilting_"+n;
		var new_kode_brg_quilting="#kode_brg_quilting_"+no;
		$(kode_brg_quilting, lastRow).attr("id", "kode_brg_quilting_"+no);
		$(new_kode_brg_quilting, lastRow).attr("name", "kode_brg_quilting_"+no);		
		$(new_kode_brg_quilting, lastRow).val('');				
		//*****end kode_brg_quilting*************************************
		
		//*****brg_bisbisan*************************************
		var brg_bisbisan="#brg_bisbisan_"+n;
		var new_brg_bisbisan="#brg_bisbisan_"+no;
		$(brg_bisbisan, lastRow).attr("id", "brg_bisbisan_"+no);
		$(new_brg_bisbisan, lastRow).attr("name", "brg_bisbisan_"+no);		
		$(new_brg_bisbisan, lastRow).val('');		
		//*****end brg_bisbisan*********************************
		
		//*****kode_brg_bisbisan*************************************
		var kode_brg_bisbisan="#kode_brg_bisbisan_"+n;
		var new_kode_brg_bisbisan="#kode_brg_bisbisan_"+no;
		$(kode_brg_bisbisan, lastRow).attr("id", "kode_brg_bisbisan_"+no);
		$(new_kode_brg_bisbisan, lastRow).attr("name", "kode_brg_bisbisan_"+no);		
		$(new_kode_brg_bisbisan, lastRow).val('');				
		//*****end kode_brg_bisbisan*************************************
		
		/*
			onclick="javascript: var x= 'B'; var z= $('#kode_brg_jadi').val(); var q = 't'; 
           var y= $('#jenis_proses_1').val();
            if (z == '') { alert('pilih dulu barang jadinya..!'); return false; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg/'+ x +'/1/'+q+'/'+z+'/'+y);"
		*/
		//button pilih_quilting*****************************************
		 var pilih_quilting="#pilih_quilting_"+n;
		 var new_pilih_quilting="#pilih_quilting_"+no;
		 $(pilih_quilting, lastRow).attr("id","pilih_quilting_"+no);
		 var  even_klikq= "var x= 'B'; var z= $('#kode_brg_jadi').val(); var q = 't'; var mtf= $('#motif_brg_jadi').val(); var y= $('#jenis_proses_"+no+"').val(); var xx = 0; if (z == '') { alert('pilih dulu barang jadinya..!'); return false; } openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg/'+x+'/"+no+"/'+q+'/'+z+'/'+y+'/'+xx+'/0/'+mtf);";
		 $(new_pilih_quilting, lastRow).attr("name", "pilih_quilting_"+no);		
		 $(new_pilih_quilting, lastRow).attr("onclick",even_klikq);		 
		 $(new_pilih_quilting, lastRow).attr("disabled",true);
		//end button pilih_quilting
		
		//button pilih_bisbisan*****************************************
		 var pilih_bisbisan="#pilih_bisbisan_"+n;
		 var new_pilih_bisbisan="#pilih_bisbisan_"+no;
		 $(pilih_bisbisan, lastRow).attr("id","pilih_bisbisan_"+no);
		 var  even_klikb= "var z= $('#kode_brg_jadi').val(); var y= $('#jenis_proses_"+no+"').val(); if (z == '' && y != '4') { alert('pilih dulu barang jadinya..!'); return false; } openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg_bisbisan/"+no+"');";
		 $(new_pilih_bisbisan, lastRow).attr("name", "pilih_bisbisan_"+no);		
		 $(new_pilih_bisbisan, lastRow).attr("onclick",even_klikb);
		 $(new_pilih_bisbisan, lastRow).attr("disabled",true);
		//end button pilih_bisbisan
		
		//*****kel brg*************************************
		var kel_brg="#kel_brg_"+n;
		var new_kel="#kel_brg_"+no;
		$(kel_brg, lastRow).attr("id", "kel_brg_"+no);
		$(new_kel, lastRow).attr("name", "kel_brg_"+no);		
		//*****end kel*********************************
		
		//*****quilting*************************************
	/*	var quilting="#quilting_"+n;
		var new_quilting="#quilting_"+no;
		$(quilting, lastRow).attr("id", "quilting_"+no);
		$(new_quilting, lastRow).attr("name", "quilting_"+no);		
		$(new_quilting, lastRow).val('t');				
		
		//<input type="checkbox" name="quilting_1" id="quilting_1" value="t" onclick="$('#kode_1').val(''); $('#nama_1').val(''); ">
		var even_klik2 = "$('#kode_"+no+"').val(''); $('#nama_"+no+"').val(''); $('#satuan_"+no+"').val('');";
		
		 $(new_quilting, lastRow).attr("onclick",even_klik2);		*/
		//*****end quilting*************************************	
		
		//*****kode*************************************
		var kode="#kode_"+n;
		var new_kode="#kode_"+no;
		$(kode, lastRow).attr("id", "kode_"+no);
		$(new_kode, lastRow).attr("name", "kode_"+no);		
		$(new_kode, lastRow).val('');		
		//*****end kode*********************************
		
		//*****nama*************************************
		var nama="#nama_"+n;
		var new_nama="#nama_"+no;
		$(nama, lastRow).attr("id", "nama_"+no);
		$(new_nama, lastRow).attr("name", "nama_"+no);		
		$(new_nama, lastRow).val('');				
		//*****end nama*************************************
		
		//*****satuan*************************************
		/*var satuan="#satuan_"+n;
		var new_satuan="#satuan_"+no;
		$(satuan, lastRow).attr("id", "satuan_"+no);
		$(new_satuan, lastRow).attr("name", "satuan_"+no);		
		$(new_satuan, lastRow).val('');				*/
		//*****end satuan*************************************	
		
		//*****qty*************************************
		var qty="#qty_"+n;
		var new_qty="#qty_"+no;
		$(qty, lastRow).attr("id", "qty_"+no);
		$(new_qty, lastRow).attr("name", "qty_"+no);		
		$(new_qty, lastRow).val('');				
		//*****end qty*************************************	
		
		//*****ukuran_bisbisan*************************************
		var ukuran_bisbisan="#ukuran_bisbisan_"+n;
		var new_ukuran_bisbisan="#ukuran_bisbisan_"+no;
		$(ukuran_bisbisan, lastRow).attr("id", "ukuran_bisbisan_"+no);
		$(new_ukuran_bisbisan, lastRow).attr("name", "ukuran_bisbisan_"+no);		
		//$(new_ukuran_bisbisan, lastRow).val('');				
		
		// onchange="$('#kode_1').val(''); $('#nama_1').val(''); $('#satuan_1').val(''); $('#qty_1').val(''); $('#gelaran_1').val(''); $('#set_1').val(''); $('#id_kebutuhanperpcs_1').val(''); $('#keb_1').val(''); $('#var1_1').val(''); $('#var2_1').val(''); $('#var3_1').val(''); "
		var even_change2 = "$('#kode_"+no+"').val(''); $('#nama_"+no+"').val(''); $('#satuan_"+no+"').val(''); $('#qty_"+no+"').val(''); $('#gelaran_"+no+"').val(''); $('#set_"+no+"').val(''); $('#id_kebutuhanperpcs_"+no+"').val(''); $('#keb_"+no+"').val(''); $('#var1_"+no+"').val(''); $('#var2_"+no+"').val(''); $('#var3_"+no+"').val(''); ";	
		$(new_ukuran_bisbisan, lastRow).attr("onchange",even_change2);		
		$(new_ukuran_bisbisan, lastRow).attr("disabled",true);		
		//*****end ukuran_bisbisan*************************************	
		
		//*****gelaran*************************************
		var gelaran="#gelaran_"+n;
		var new_gelaran="#gelaran_"+no;
		$(gelaran, lastRow).attr("id", "gelaran_"+no);
		$(new_gelaran, lastRow).attr("name", "gelaran_"+no);		
		$(new_gelaran, lastRow).val('');				
		//*****end gelaran*************************************	
		
		//*****set*************************************
		var jum_set="#set_"+n;
		var new_jum_set="#set_"+no;
		$(jum_set, lastRow).attr("id", "set_"+no);
		$(new_jum_set, lastRow).attr("name", "set_"+no);		
		$(new_jum_set, lastRow).val('');				
		//*****end set*************************************	
		
		//*****jum_gelar*************************************
		var jum_gelar="#jum_gelar_"+n;
		var new_jum_gelar="#jum_gelar_"+no;
		$(jum_gelar, lastRow).attr("id", "jum_gelar_"+no);
		$(new_jum_gelar, lastRow).attr("name", "jum_gelar_"+no);		
		$(new_jum_gelar, lastRow).val('');				
		//*****end jum_gelar*************************************	
		
		//*****id_marker*************************************
		var id_marker="#id_marker_"+n;
		var new_id_marker="#id_marker_"+no;
		$(id_marker, lastRow).attr("id", "id_marker_"+no);
		$(new_id_marker, lastRow).attr("name", "id_marker_"+no);		
		$(new_id_marker, lastRow).val('');				
		//*****end id_marker*************************************	
		
		//*****id_kebutuhanperpcs*************************************
		var id_kebutuhanperpcs="#id_kebutuhanperpcs_"+n;
		var new_id_kebutuhanperpcs="#id_kebutuhanperpcs_"+no;
		$(id_kebutuhanperpcs, lastRow).attr("id", "id_kebutuhanperpcs_"+no);
		$(new_id_kebutuhanperpcs, lastRow).attr("name", "id_kebutuhanperpcs_"+no);		
		$(new_id_kebutuhanperpcs, lastRow).val('');				
		//*****end id_kebutuhanperpcs*************************************	
		
		//*****keb*************************************
		var keb="#keb_"+n;
		var new_keb="#keb_"+no;
		$(keb, lastRow).attr("id", "keb_"+no);
		$(new_keb, lastRow).attr("name", "keb_"+no);		
		$(new_keb, lastRow).val('');				
		//*****end keb*************************************	
		
		//*****var1*************************************
		var var1="#var1_"+n;
		var new_var1="#var1_"+no;
		$(var1, lastRow).attr("id", "var1_"+no);
		$(new_var1, lastRow).attr("name", "var1_"+no);		
		$(new_var1, lastRow).val('');				
		//*****end var1*************************************	
		
		//*****var2*************************************
		var var2="#var2_"+n;
		var new_var2="#var2_"+no;
		$(var2, lastRow).attr("id", "var2_"+no);
		$(new_var2, lastRow).attr("name", "var2_"+no);		
		$(new_var2, lastRow).val('');				
		//*****end var2*************************************	
		
		//*****var3*************************************
		var var3="#var3_"+n;
		var new_var3="#var3_"+no;
		$(var3, lastRow).attr("id", "var3_"+no);
		$(new_var3, lastRow).attr("name", "var3_"+no);		
		$(new_var3, lastRow).val('');				
		//*****end var3*************************************	
		
		//*****pjg_kain*************************************
		var pjg_kain="#pjg_kain_"+n;
		var new_pjg_kain="#pjg_kain_"+no;
		$(pjg_kain, lastRow).attr("id", "pjg_kain_"+no);
		$(new_pjg_kain, lastRow).attr("name", "pjg_kain_"+no);		
		$(new_pjg_kain, lastRow).val('');				
		//*****end pjg_kain*************************************	
				
		//button pilih*****************************************
		 var pilih="#pilih_"+n;
		 var new_pilih="#pilih_"+no;
		 $(pilih, lastRow).attr("id","pilih_"+no);
		 var nama_for_even="document.f_purchase.iddata.value="+no;	
		 
		 /*
			onclick="javascript: var x= $('#kel_brg_1').val(); var q = ''; if ($('input[name=quilting_1]').is(':checked')) { q= 't'; x= '0'; } else { q='f'; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg/'+ x +'/1/'+q);"
		 */
		 
		 var  even_klik= "var x= $('#kel_brg_"+no+ "').val(); var z= $('#kode_brg_jadi').val(); var q = ''; var mtf= $('#motif_brg_jadi').val(); if ($('input[name=quilting_"+no+"]').is(':checked')) { q= 't'; x= '0'; } else { q='f'; } var y= $('#jenis_proses_"+no+"').val(); var xx= $('#kode_brg_quilting_"+no+"').val(); if (xx == '') xx = 0; var uk= $('#ukuran_bisbisan_"+no+"').val(); if (z == '' && y != '4') { alert('pilih dulu barang jadinya..!'); return false; } if (z== '' && y == '4') z='xxx'; openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg/'+x+'/"+no+"/'+q+'/'+z+'/'+y+'/'+xx+'/'+uk+'/'+mtf);";

		$(new_pilih, lastRow).attr("name", "pilih_"+no);		
		 $(new_pilih, lastRow).attr("onmouseover",nama_for_even);
		 $(new_pilih, lastRow).attr("onclick",even_klik);		 
		//end button pilih		
		//----------------END SETTING KONDISI ROW BARU*		
		//tambahin row nya sesuai settingan
		$("#tabelku").append(lastRow);
		//no++
		var x=parseInt(no);
		$("#no").val(x+1);	
		
	});	
	
	$("#deleterow").click(function()
	{
		var x= $("#no").val();	
		if (x>2) {
			$("#tabelku tr:last").remove();	 
			var x= $("#no").val();	
			var y= x-1;
			$("#no").val(y);	
		}
	});	
	
	$('#is_kode_brg_jadi_manual').click(function(){ //
	  	    if ($('input[name=is_kode_brg_jadi_manual]').is(':checked')) {
				$("#pilih_brg_jadi").attr("disabled", true);
				$("#is_kode_brg_jadi_manual").attr("readonly", false);
				$("#brg_jadi_manual").attr("readonly", false);
				$('#brg_jadi').val('');
				$('#kode_brg_jadi').val('000000');
				$('#brg_jadi_manual').val('');
			}
			else {
				$("#pilih_brg_jadi").attr("disabled", false);
				$("#is_kode_brg_jadi_manual").attr("readonly", true);
				$("#brg_jadi_manual").attr("readonly", true);
				$('#brg_jadi').val('');
				$('#kode_brg_jadi').val('');
				$('#brg_jadi_manual').val('');
			}
	  });

});
</script>
<script type="text/javascript">

function openCenteredWindow(url) {

		var width = 640;
		var height = 480;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
	}
	

function generate_nomor() {
	var jum_detail = $('#no').val()-1;
    $.getJSON("<?php echo base_url(); ?>index.php/pb-cutting/cform/generate_nomor", function(data) {
		$('#no_request').val(data);
		
    });
    
}

function cek_pb() {
	var no_request= $('#no_request').val();
	var tgl= $('#tgl_request').val();
	var brg_jadi= $('#brg_jadi').val();
	var brg_jadi_manual= $('#brg_jadi_manual').val();
	
	if (no_request == '') {
		alert("Nomor Request harus diisi..!");
		return false;
	}
	if (tgl == '') {
		alert("Tanggal Request harus dipilih..!");
		return false;
	}
	/*if (brg_jadi == '') {
		alert("Barang Jadi harus dipilih..!");
		return false;
	} */
	if ($('input[name=is_kode_brg_jadi_manual]').is(':checked')) {
		if (brg_jadi_manual == '') {
			alert("Nama brg jadi harus diisi..!");
			return false;
		}
	}
	
	var jum= $('#no').val()-1; 

	if (jum > 0) {
		for (var k=1; k <= jum; k++) {
			var jenis_proses=$("#jenis_proses_"+k).val();
			
			if (jenis_proses == 3) {
				if ($('#kode_brg_quilting_'+k).val() == '') {
					alert("Nama item brg quilting tidak boleh kosong...!");
					return false;
				}
			}
			if (jenis_proses == 4) {
				if ($('#kode_brg_bisbisan_'+k).val() == '') {
					alert("Nama item brg bis-bisan tidak boleh kosong...!");
					return false;
				}
			}
			
			if ($('#kode_'+k).val() == '') {
				alert("Data item bahan baku/pembantu tidak boleh ada yang kosong...!");
				return false;
			}
			if($('#qty_'+k).val() == '0' || $('#qty_'+k).val() == '' ) {				
				alert("Data qty tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#qty_'+k).val()) ) {
				alert("Qty harus berupa angka..!");
				return false;
			}
			
			if($('#gelaran_'+k).val() == '0' || $('#gelaran_'+k).val() == '' ) {				
				alert("Data gelaran tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#gelaran_'+k).val()) ) {
				alert("Gelaran harus berupa angka..!");
				return false;
			}
			
			if($('#set_'+k).val() == '0' || $('#set_'+k).val() == '' ) {				
				alert("Data set tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#set_'+k).val()) ) {
				alert("Data set harus berupa angka..!");
				return false;
			}
			
			if($('#jum_gelar_'+k).val() == '0' || $('#jum_gelar_'+k).val() == '' ) {				
				alert("Data jumlah gelar tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#jum_gelar_'+k).val()) ) {
				alert("Data jumlah gelar harus berupa angka..!");
				return false;
			}
			
			if($('#pjg_kain_'+k).val() == '0' || $('#pjg_kain_'+k).val() == '' ) {				
				alert("Data panjang kain tidak boleh 0 / kosong...!");
				return false;
			}
			if (isNaN($('#pjg_kain_'+k).val()) ) {
				alert("Data panjang kain harus berupa angka..!");
				return false;
			}
		}
	}
	else {
		alert("Data detail tidak ada");
		return false;
	}	
}

function hitungnilai() {
	var jum_detail= $('#no').val()-1;

				var i=1;
				for (i=1;i<=jum_detail;i++) {
					var qty=$("#qty_"+i).val();
					var gelaran=$("#gelaran_"+i).val();
					var jum_set=$("#set_"+i).val();
					var jenis_proses=$("#jenis_proses_"+i).val();
					var keb=$("#keb_"+i).val();
					var var1=$("#var1_"+i).val();
					var var2=$("#var2_"+i).val();
					var var3=$("#var3_"+i).val();
					var pjg_kain = 0;
					if (jenis_proses != 4) {
						var jum_gelar = parseFloat(qty)/parseFloat(jum_set);
						jum_gelar = jum_gelar.toFixed(2);
						
						if (gelaran > 0) {
							pjg_kain = jum_gelar * gelaran;
							pjg_kain = pjg_kain.toFixed(2);
						}
						else
							pjg_kain = jum_gelar;
					}
					else {
						if ($("input[name=kebbismanual_"+i+"]").is(':checked') ) {
							keb=$("#vkebbismanual_"+i).val();
						}
						var pjg_kain = (parseFloat(qty)*parseFloat(keb)*parseFloat(var1))/(parseFloat(var2)*parseFloat(var3));
						//pjg_kain = pjg_kain.toFixed(0);
						//alert(pjg_kain);
						pjg_kain = Math.round(pjg_kain);
						jum_gelar = 1;
						$("#gelaran_"+i).val(pjg_kain);
						$("#set_"+i).val('1');
					}
					
					$("#pjg_kain_"+i).val(pjg_kain);
					$("#jum_gelar_"+i).val(jum_gelar);
				}

}

function hitungulang(pos) {
	var qty=$("#qty_"+pos).val();
	
	if ($("input[name=kebbismanual_"+pos+"]").is(':checked') ) {
		var keb=$("#vkebbismanual_"+pos).val();
	}
	else
		var keb=$("#keb_"+pos).val();

	var var1=$("#var1_"+pos).val();
	var var2=$("#var2_"+pos).val();
	var var3=$("#var3_"+pos).val();
	
	var pjg_kain = (parseFloat(qty)*parseFloat(keb)*parseFloat(var1))/(parseFloat(var2)*parseFloat(var3));
	pjg_kain = Math.round(pjg_kain);
	jum_gelar = 1;
	$("#gelaran_"+pos).val(pjg_kain);
	$("#set_"+pos).val('1');
					
	$("#pjg_kain_"+pos).val(pjg_kain);
	$("#jum_gelar_"+pos).val(jum_gelar);
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>js/pupop.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>

<form name="f_purchase" id="f_purchase" action="<?php echo base_url(); ?>index.php/pb-cutting/cform/submit" method="post" enctype="multipart/form-data">

<input type="hidden" name="no" id="no" value="">
<input type="hidden" name="iddata" id="iddata"/>
<div align="center">

<label id="status"></label>
<br>

<table  class="proit-view" width="100%" cellspacing="2" cellpadding="1" >
  <tr>
    <td width="15%">No Request</td>
    <td>
      <input name="no_request" type="text" id="no_request" size="20" maxlength="20" value="">
    </td>
    
  </tr>
  <tr>
    <td>Tanggal Request</td>
    <td>
	<label>
      <input name="tgl_request" type="text" id="tgl_request" size="10" value="" readonly="true">
    </label>
	   <img alt="" id="tgl_request" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].tgl_request,'dd-mm-yyyy',this)">
	</td>
  </tr>
  <tr>
	<td>Untuk Barang Jadi</td>
	<td><input name="brg_jadi" type="text" id="brg_jadi" size="45" readonly="true" value=""/>
           <input name="kode_brg_jadi" type="hidden" id="kode_brg_jadi" value=""/>
           <input title="browse data brg jadi" onmouseover="document.f_purchase.iddata.value=1" name="pilih_brg_jadi" value="..." type="button" id="pilih_brg_jadi" 
           onclick="javascript: openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg_jadi/');" type="button"><input type="checkbox" name="is_kode_brg_jadi_manual" id="is_kode_brg_jadi_manual" value="t"> Blm ada kode barang jadi
		&nbsp; <br> Nama barang jadi manual: <input type="text" name="brg_jadi_manual" id="brg_jadi_manual" value="" size="40" readonly="true">
    </td>
  </tr>
  
  <tr>
	<td>Motif Brg Jadi</td>
	<td><select name="motif_brg_jadi" id="motif_brg_jadi" >
			<option value="0">Tidak Ada</option>
					<?php foreach ($motif_brg_jadi as $mtf) { ?>
					<option value="<?php echo $mtf->id ?>" ><?php echo $mtf->nama_motif ?></option>
				<?php } ?>
				</select></td>
  </tr>
  
  <tr>
    <td>Keterangan</td>
    <td>
      <input name="ket" type="text" id="ket" size="30" value="">
    </td>  
  </tr>
  <tr>
	<td colspan="2"><i>*) Untuk bahan yg satuannya Yard, maka Qty yg dikeluarkan itu dlm bentuk Meter<br>
	*) Untuk bahan yg satuannya selain Yard, maka Qty yg dikeluarkan itu tidak ada konversi satuan</i></td>
  </tr>
  
  <tr>
    <td colspan="2"><br>
	<form name="myform">
	<table id="tabelku"  border="0" align="center"  cellpadding="1"  cellspacing="2"  class="proit-view" >
		<tr>
			<td colspan="12" align="left">
			<input id="returpurchase" type="button" name="returpurchase" value=" + " title="Tambah Item Barang">&nbsp;
			<input id="deleterow" type="button" name="deleterow" value=" - " title="Hapus Item Barang">
			</td>
		</tr>
        <tr>
          <th width="20">No</th>
          <th>Jenis Proses</th>
          <th>Bordir</th>
          <th>Utk Kekurangan Cutting</th>
          <th>Utk Ganti BS Cutting</th>
          <th>Bagian Yg BS</th>
          <th>Bhn Quilting</th>
          <th>Bhn Bisbisan</th>
          <th>Uk Bisbisan</th>
          <th>Kelompok Brg</th>
          <!-- <th>Quilting</th> -->
          <th>Nama Bhn Baku/Pembantu</th>
          <!--<th>Nama Barang</th> -->
          <!--<th>Satuan</th>-->
          <th>Keb bis2an manual ?</th>
	      <th>Plan Qty (Set)</th>
	      <th>Gelaran/Set</th>
	      <th>Jml Gelar</th>
	      <th>Qty Yg Diklrkan</th>
        </tr>
		
		<tr align="center">
          <td align="center" id="num_1">1</td>
          <td><select name="jenis_proses_1" id="jenis_proses_1" onchange="$('#brg_quilting_1').val(''); $('#kode_brg_quilting_1').val(''); $('#brg_bisbisan_1').val(''); $('#kode_brg_bisbisan_1').val(''); $('#kode_1').val(''); $('#nama_1').val(''); $('#qty_1').val(''); $('#gelaran_1').val(''); $('#set_1').val(''); $('#id_kebutuhanperpcs_1').val(''); $('#keb_1').val(''); $('#var1_1').val(''); $('#var2_1').val(''); $('#var3_1').val(''); if (this.value == '3' || this.value == '5') { $('#pilih_quilting_1').attr('disabled',false); } else { $('#pilih_quilting_1').attr('disabled',true); } if (this.value == '4') { $('#pilih_bisbisan_1').attr('disabled',false); $('#ukuran_bisbisan_1').attr('disabled',false); } else { $('#pilih_bisbisan_1').attr('disabled',true); $('#ukuran_bisbisan_1').attr('disabled',true); } " onkeyup="this.blur();this.focus();">
					<option value="1" >Dicutting</option>
					<option value="2" >Diprint/bordir</option>
					<option value="3" >Diquilting</option>
					<option value="4" >Dibisbisan</option>
					<option value="5" >Diquilting+print/bordir</option>
				</select>
		  </td>
		  
		  <td>
			<input type="checkbox" name="bordir_1" id="bordir_1" value="t">
		  </td>
		  
		  <td>
			<input type="checkbox" name="kcutting_1" id="kcutting_1" value="t" onclick="javascript: if ($('input[name=kcutting_1]').is(':checked') ) { $('input[name=bscutting_1]').attr('checked', false); $('input[name=bagian_bs_1]').attr('readonly', true); } ">
		  </td>
		  
		  <td>
			<input type="checkbox" name="bscutting_1" id="bscutting_1" value="t" onclick="javascript: if ($('input[name=bscutting_1]').is(':checked') ) { $('input[name=kcutting_1]').attr('checked', false); $('input[name=bagian_bs_1]').attr('readonly', false); } else { $('input[name=bagian_bs_1]').attr('readonly', true); } ">
		  </td>
		  
		  <td nowrap="nowrap">
          <input name="bagian_bs_1" type="text" id="bagian_bs_1" size="10" value="" readonly="true" />
          </td>
		  
		  <td nowrap="nowrap">
			<input name="brg_quilting_1" type="text" id="brg_quilting_1" size="25" readonly="true" value=""/>
            <input name="kode_brg_quilting_1" type="hidden" id="kode_brg_quilting_1" value=""/>
           <input disabled title="browse data bhn quilting" name="pilih_quilting_1" value="..." type="button" id="pilih_quilting_1" 
           onclick="javascript: var x= 'B'; var z= $('#kode_brg_jadi').val(); var q = 't'; 
           var mtf= $('#motif_brg_jadi').val();
           var y= $('#jenis_proses_1').val(); var xx = 0;
            if (z == '') { alert('pilih dulu barang jadinya..!'); return false; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg/'+ x +'/1/'+q+'/'+z+'/'+y+'/'+xx+'/0/'+mtf);" >
           </td>
           
           <td nowrap="nowrap">
			<input name="brg_bisbisan_1" type="text" id="brg_bisbisan_1" size="25" readonly="true" value=""/>
            <input name="kode_brg_bisbisan_1" type="hidden" id="kode_brg_bisbisan_1" value=""/>
           <input disabled title="browse data bhn bisbisan" name="pilih_bisbisan_1" value="..." type="button" id="pilih_bisbisan_1" 
           onclick="javascript: var z= $('#kode_brg_jadi').val(); var q = 't'; 
           var y= $('#jenis_proses_1').val(); var xx = 0;
            if (z == '' && y != '4') { alert('pilih dulu barang jadinya..!'); return false; }
            openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg_bisbisan/1');" >
           </td>
           
           <td><select name="ukuran_bisbisan_1" id="ukuran_bisbisan_1" onchange="$('#kode_1').val(''); $('#nama_1').val(''); $('#satuan_1').val(''); $('#qty_1').val(''); $('#gelaran_1').val(''); $('#set_1').val(''); $('#id_kebutuhanperpcs_1').val(''); $('#keb_1').val(''); $('#var1_1').val(''); $('#var2_1').val(''); $('#var3_1').val(''); " onkeyup="this.blur();this.focus();">
				<?php foreach ($ukuran_bisbisan as $uk) { ?>
					<option value="<?php echo $uk->id ?>" ><?php echo $uk->nama ?></option>
				<?php } ?>
				</select>
          </td>
		  
          <td><select name="kel_brg_1" id="kel_brg_1">
				<?php foreach ($kel_brg as $kel) { ?>
					<option value="<?php echo $kel->kode ?>" ><?php echo $kel->kode." - ". $kel->nama ?></option>
				<?php } ?>
				</select>
		  </td>
		  <!--<td> <input type="checkbox" name="quilting_1" id="quilting_1" value="t" onclick="$('#kode_1').val(''); $('#nama_1').val(''); $('#satuan_1').val(''); "> </td> -->
          <td nowrap="nowrap">
          <input name="nama_1" type="text" id="nama_1" size="20" readonly="true" value="" />
           <input name="kode_1" type="hidden" id="kode_1" size="15" readonly="true" value=""/>
           <input title="browse data barang" onmouseover="document.f_purchase.iddata.value=1" name="pilih_1" value="..." type="button" id="pilih_1" 
           onclick="javascript: var x= $('#kel_brg_1').val(); var z= $('#kode_brg_jadi').val(); var q = ''; 
           var mtf= $('#motif_brg_jadi').val();
           if ($('input[name=quilting_1]').is(':checked')) { q= 't'; x= '0'; } else { q='f'; }
           var y= $('#jenis_proses_1').val(); var xx= $('#kode_brg_quilting_1').val(); if (xx == '') xx = 0;
           var uk= $('#ukuran_bisbisan_1').val();
            if (z == '' && y != '4') { alert('pilih dulu barang jadinya..!'); return false; }
            if (z == '' && y == '4') z= 'xxx';
            openCenteredWindow('<?php echo base_url(); ?>index.php/pb-cutting/cform/show_popup_brg/'+ x +'/1/'+q+'/'+z+'/'+y+'/'+xx+'/'+uk+'/'+mtf);" >
           </td>
          <!--<td><input name="satuan_1" type="text" id="satuan_1" size="5" readonly="true" value="" /></td>           -->
          
          <td nowrap>
			<input type="checkbox" name="kebbismanual_1" id="kebbismanual_1" value="t" onclick="javascript: hitungulang(1); if ($('input[name=kebbismanual_1]').is(':checked') ) { $('input[name=vkebbismanual_1]').attr('readonly', false); } else { $('input[name=vkebbismanual_1]').attr('readonly', true); $('#vkebbismanual_1').val(''); } ">
			<input name="vkebbismanual_1" type="text" id="vkebbismanual_1" size="5" value="" readonly="true" onkeyup="hitungulang(1)" onblur="hitungulang(1)" />
		  </td>
          
          <td><input name="qty_1" type="text" id="qty_1" size="5" value="" onkeyup="hitungnilai()" onblur="hitungnilai()" /></td>
          
          <td nowrap><input name="gelaran_1" type="text" id="gelaran_1" size="5" value="" /> / <input name="set_1" type="text" id="set_1" size="5" value="" /> 
			<input type="hidden" name="id_marker_1" id="id_marker_1" value="" >
			<input type="hidden" name="id_kebutuhanperpcs_1" id="id_kebutuhanperpcs_1" value="" >
			<input type="hidden" name="keb_1" id="keb_1" value="" >
			<input type="hidden" name="var1_1" id="var1_1" value="" >
			<input type="hidden" name="var2_1" id="var2_1" value="" >
			<input type="hidden" name="var3_1" id="var3_1" value="" >
          </td>
          <td><input name="jum_gelar_1" type="text" id="jum_gelar_1" size="5" value="" /></td>
          <td><input name="pjg_kain_1" type="text" id="pjg_kain_1" size="5" value="" /></td>
        </tr>
	</table>	
	
	</form>
      <div align="center"><br> 
        
        <input type="submit" name="submit2" value="Simpan" onclick="return cek_pb();">&nbsp;<input type="button" name="batal" value="Batal" onClick="window.location='<?php echo base_url(); ?>index.php/pb-cutting/cform/view'">

      </div></td>
    </tr>

</table>
</div>
</form>
