<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 10px;}
  
  .fieldsetdemo
{
	background-color:#DDD;
	max-width:300px;
	padding:14px;
}
	.judulnya {
		background-color:#DDD;
	}

</style>

<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>

<script type="text/javascript"> 
function openCenteredWindow(url) {
		var width = 1200;
		var height = 400;
		var left = parseInt((screen.availWidth/2) - (width/2));
		var top = parseInt((screen.availHeight/2) - (height/2));
		var windowFeatures = "width=" + width + ",height=" + height +
			",status,resizable,toolbar,scrollbars,left=" + left + ",top=" + top +
			",screenX=" + left + ",screenY=" + top;
		myWindow = window.open(url, "subWind", windowFeatures);
}

$(function()
{
	$(".pilih").click(function()
	{
		var id_pb_cutting=$("#id_pb_cutting").val(); 
		
		var urlnya = "<?php echo base_url(); ?>index.php/pb-cutting/cform/print_pb/"+id_pb_cutting;
		openCenteredWindow(urlnya);
	});
});

</script>

<h3>Data Permintaan Bahan Baku Ke Gudang</h3><br> 
<a href="<? echo base_url(); ?>index.php/pb-cutting/cform">Tambah Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/pb-cutting/cform/view">View Data</a>&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/pb-cutting/cform/print_pb">Print</a><br><br>

<div>
Total Data = <?php echo $jum_total ?><br><br>
<?php echo form_open('pb-cutting/cform/cari'); ?>
<fieldset class="fieldsetdemo">
<legend>Filter Pencarian Data</legend>
<table>
	<tr>
		<td>Dari Tanggal</td>
		<td>: <input type="text" name="date_from" id="date_from" maxlength="10" size="10" readonly="true" value="<?php echo $date_from ?>" />
		  <img alt="" id="date1" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_from,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>: <input type="text" name="date_to" id="date_to" maxlength="10" size="10" readonly="true" value="<?php echo $date_to ?>" />
		  <img alt="" id="date2" align="middle"
			title="Pilih tgl.." src="<?php echo base_url();?>images/calendar.gif"
			onclick="displayCalendar(document.forms[0].date_to,'dd-mm-yyyy',this)"></td>
	</tr>
	<tr>
		<td>Nomor Request</td>
		<td>: <input type="text" name="cari" size="10" value="<?php echo $cari ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Cari" ></td>
	</tr>
</table>			
</fieldset>

<?php echo form_close(); ?>
<br>
<?php 
	if ($date_from == '')
		$tgl_awal = "00-00-0000";
	else
		$tgl_awal = $date_from;
	
	if ($date_to == '')
		$tgl_akhir = "00-00-0000";
	else
		$tgl_akhir = $date_to;
?>
<form id="f_master_brg" name="f_master_brg">
<input type="hidden" name="id_pb_cutting" id="id_pb_cutting">
<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		<th>No Request</th>
		 <th>Tgl Request</th>
		 <th>Utk Barang Jadi</th>
		 <th>Motif</th>
		 <th>Jenis Proses</th>
		 <th>Bagian Yg BS</th>
		 <th>Nama Bhn Quilting</th>
		 <th>Nama Bhn Bis-Bisan</th>
		 <th>Uk Bisbisan</th>
		 <th>List Bahan Baku & Satuan</th>
		 <th>Plan Qty (Set)</th>
		 <th>Gelaran/Set</th>
		 <th>Jml Gelar</th>
		 <th>Qty Yg Dikeluarkan</th>
		 <th>Last Update</th>
		 <th>Action</th>
	 </tr>
	</thead>
	<tbody>
		 <?
			if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				
				$pisah1 = explode("-", $query[$j]['tgl_update']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
				
				$pisah1 = explode("-", $query[$j]['tgl']);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
				 
				 echo "<tr class=\"record\">";
				 echo    "<td>".$query[$j]['no_pb_cutting']."</td>";
				 echo    "<td>".$tgl."</td>";
				 echo    "<td>".$query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']."</td>";
				 
				 echo    "<td nowrap>";
				 if ($query[$j]['id_motif'] == '0')
					echo " Tidak Ada ";
				 else
					echo $query[$j]['nama_motif'];
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  if ($var_detail[$k]['jenis_proses'] == 1) {
							echo "Dicutting";
							if ($var_detail[$k]['for_kekurangan_cutting'] == 't')
								echo " (utk kekurangan bhn cutting)";
							else if ($var_detail[$k]['for_bs_cutting'] == 't')
								echo " (utk ganti BS cutting)";
						  }
						  else if ($var_detail[$k]['jenis_proses'] == 2) {
							if ($var_detail[$k]['bordir'] == 't' ) {
								echo "Bordir";
								if ($var_detail[$k]['for_kekurangan_cutting'] == 't')
									echo " (utk kekurangan bhn cutting)";
								else if ($var_detail[$k]['for_bs_cutting'] == 't')
									echo " (utk ganti BS cutting)";
							}
							else {
								echo "Diprint";
								if ($var_detail[$k]['for_kekurangan_cutting'] == 't')
									echo " (utk kekurangan bhn cutting)";
								else if ($var_detail[$k]['for_bs_cutting'] == 't')
									echo " (utk ganti BS cutting)";
							}
						  }
						  else if ($var_detail[$k]['jenis_proses'] == 3)
						    echo "Diquilting";
						  else if ($var_detail[$k]['jenis_proses'] == 4)
						    echo "Dibisbisan";
						  else if ($var_detail[$k]['jenis_proses'] == 5) {
						    if ($var_detail[$k]['bordir'] == 't' )
								echo "Diquilting + Bordir";
							else
								echo "Diquilting + Print";
							
						  }
						    
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						echo $var_detail[$k]['bagian_bs'];

						if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['kode_brg_quilting'] != '')
							echo $var_detail[$k]['kode_brg_quilting']." - ".$var_detail[$k]['nama_brg_quilting'];
						else
							echo "&nbsp;";
						if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						if ($var_detail[$k]['kode_brg_bisbisan'] != '')
							echo $var_detail[$k]['kode_brg_bisbisan']." - ".$var_detail[$k]['nama_brg_bisbisan'];
						else
							echo "&nbsp;";
						if ($k<$hitung-1)
						    echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['nama_ukuran'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['kode_brg']." - ".$var_detail[$k]['nama']." (".$var_detail[$k]['satuan'].")";
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
					
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['gelaran']." / ".$var_detail[$k]['jum_set'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['jum_gelar'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				 
				 echo "<td nowrap align='right'>";
				 if (is_array($query[$j]['detail_pb'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_pb'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						  echo $var_detail[$k]['panjang_kain'];
						  if ($k<$hitung-1)
						     echo "<br> ";
					}
				 }
				 echo "</td>";
				  
				 echo    "<td align='center'>".$tgl_update."</td>";
				 
				 if ($query[$j]['status_edit'] == 'f') {
					 echo    "<td align=center>";
					 //<a style='cursor:pointer' class='pilih' onMouseOver='window.document.f_master_brg.id_pb_cutting.value=".$query[$j]['id']."'; ><u>Print</u></a> &nbsp;
					 echo "<a href=".base_url()."index.php/pb-cutting/cform/edit/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$tgl_awal."/".$tgl_akhir."/".$cari." \" id=\"".$query[$j]['id']."\">Edit</a>&nbsp;
					 <a href=".base_url()."index.php/pb-cutting/cform/delete/".$query[$j]['id']."/".$cur_page."/".$is_cari."/".$tgl_awal."/".$tgl_akhir."/".$cari." \" onclick=\"return confirm('Hapus data ini?')\">Delete</a></td>";
					 
				 }
				 else
					echo "<td>&nbsp;</td>";
				 echo  "</tr>";
		 	}
		   }
		 ?>
 	</tbody>
</table><br>
</form>
<?php echo $this->pagination->create_links();?>
</div>
