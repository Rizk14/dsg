<style type="text/css">
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 14px;}

</style>

<style type="text/css" media="print">
    .printable{ 
      page-break-after: always;
    }
    .no_print{
      display: none;
    }
</style>
<!-- media="screen" means these styles will only be used by screen 
  devices (e.g. monitors) -->
<style type="text/css" media="screen">
    .printable{
      display: none;
    }
</style>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>/js/jquery.js"></script>
<script type="text/javascript">
$(function()
{
	//printpr();
	window.print();
});

/*function printpr()
	{
		var OLECMDID = 7; */
		/* OLECMDID values:
		* 6 - print
		* 7 - print preview
		* 1 - open window
		* 4 - Save As
		*/
/*		var PROMPT = 1; // 2 DONTPROMPTUSER
		var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
		WebBrowser1.ExecWB(OLECMDID, PROMPT);
		WebBrowser1.outerHTML = "";
	} */
</script>

<?php
	//$tgl = date("d-m-Y");
	$pisah1 = explode("-", $tgl_request);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
?>


<table border="0" width="95%" class="tabelheader">
	<tr>
		<td><b>FORMULIR PERMINTAAN BAHAN BAKU</b></td>
		<td align="right">Tanggal: <?php echo $tgl ?></td>
	</tr>

</table>
<table border="1" cellpadding= "1" cellspacing = "1" width="95%" class="isinya">
	<thead>
	 <tr>
		<th colspan="10">Permintaan</th>
		<th colspan="13">Pemenuhan Barang</th>
	 </tr>
	 <tr>
		<th width="40px">Nomor</th>
		<th width="60px">Nama Brg Jadi</th>
		<th width="20px">Jml</th>
		<th width="60px">Nama Bahan</th>
		<th width="60px">Jns Proses</th>
		<th width="40px">Glrn/Set</th>
		<th width="40px">Jml Gelar</th>
		<th width="30px">Pjg Kain (m)</th>
		<th width="30px">Lbr</th>
		<th width="30px">TTD</th>
		<th width="30px">Tgl</th>
		<th width="30px">SJ</th>
		<th width="30px">1</th>
		<th width="30px">2</th>
		<th width="30px">3</th>
		<th width="30px">4</th>
		<th width="30px">5</th>
		<th width="30px">6</th>
		<th width="30px">7</th>
		<th width="30px">8</th>

		<th width="30px">Total</th>
		<th width="30px">TTD</th>
	 </tr>
	</thead>
	<tbody>
		<?php
			if (is_array($query)) {
				$kode_brg_jadi_temp = "";
				$nama_brg_jadi_temp = "";
				$no_pb_cutting_temp = "";
				$posisi_data = 0;
				for($j=0;$j<count($query);$j++){
					if ($no_pb_cutting_temp != $query[$j]['no_pb_cutting']) {
						$bariskertas = 1;
					}
					else
						$bariskertas++;
					
					$cek8 = $bariskertas%9;
					if ($cek8 == 0) {
						echo "<div class='printable'> </div>";
					}
		?>
			<tr>
				<td height="70"><?php 
					if ($no_pb_cutting_temp != $query[$j]['no_pb_cutting']) {
						$posisi_data++;
						$no_pb_cutting_temp = $query[$j]['no_pb_cutting'];
						$kode_brg_jadi_temp = $query[$j]['kode_brg_jadi'];
						$nama_brg_jadi_temp = $query[$j]['nama_brg_jadi'];
						//$bariskertas = 1;
						echo $query[$j]['no_pb_cutting']; 
					}
					else {
						echo "&nbsp;";
						$kode_brg_jadi_temp = "&nbsp;";
						$nama_brg_jadi_temp = "&nbsp;";
						//$bariskertas++;
					}
					
					?></td>
				<td><?php
					if ($kode_brg_jadi_temp != "&nbsp;")
						echo $kode_brg_jadi_temp." - ".$nama_brg_jadi_temp;
					else
						echo "&nbsp;";
					
				  /*if ($query[$j]['kode_brg_jadi'] != "000000" && $query[$j]['kode_brg_jadi'] != "") {
					if ($kode_brg_jadi_temp != $query[$j]['kode_brg_jadi']) {
						$kode_brg_jadi_temp = $query[$j]['kode_brg_jadi'];
						echo $query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']; 
					}
					else
						echo "&nbsp;";
				  }
				  else if ($query[$j]['kode_brg_jadi'] == "000000") {
					  if ($nama_brg_jadi_temp != $query[$j]['nama_brg_jadi']) {
						  $nama_brg_jadi_temp = $query[$j]['nama_brg_jadi'];
						  echo $query[$j]['kode_brg_jadi']." - ".$query[$j]['nama_brg_jadi']; 
					  }
					  else
						echo "&nbsp;";
				  }
				  else if ($query[$j]['kode_brg_jadi'] == '')
					echo "&nbsp; - &nbsp;"; */
					
				?></td>
				<td align="right"><?php echo $query[$j]['qty'] ?></td>
				<td><?php echo $query[$j]['nama_brg'] ?></td>
				
				<td>
				<?php
					if ($query[$j]['jenis_proses'] == 1) {
							echo "Dicutting";
							if ($query[$j]['for_kekurangan_cutting'] == 't')
								echo " (utk kekurangan bhn cutting)";
							else if ($query[$j]['for_bs_cutting'] == 't')
								echo " (utk ganti BS cutting) bagian ".$query[$j]['bagian_bs'];
						  }
						  else if ($query[$j]['jenis_proses'] == 2) {
							if ($query[$j]['bordir'] == 't' ) {
								echo "Bordir";
								if ($query[$j]['for_kekurangan_cutting'] == 't')
									echo " (utk kekurangan bhn cutting)";
								else if ($query[$j]['for_bs_cutting'] == 't') {
									echo " (utk ganti BS cutting) bagian ".$query[$j]['bagian_bs'];
								}
							}
							else {
								echo "Diprint";
								if ($query[$j]['for_kekurangan_cutting'] == 't')
									echo " (utk kekurangan bhn cutting)";
								else if ($query[$j]['for_bs_cutting'] == 't') {
									echo " (utk ganti BS cutting) bagian ".$query[$j]['bagian_bs'];
								}
							}
						  }
						  else if ($query[$j]['jenis_proses'] == 3) {
						    echo "Diquilting";
						    echo " (".$query[$j]['nama_brg_quilting'].")";
						  }
						  else if ($query[$j]['jenis_proses'] == 4) {
						    echo "Dibisbisan";
						    echo " (".$query[$j]['nama_brg_bisbisan'].")";
						  }
				?>
				</td>
				
				<td><?php echo $query[$j]['gelaran']."/".$query[$j]['jum_set'] ?></td>
				<td align="right"><?php echo $query[$j]['jum_gelar'] ?></td>
				<td align="right"><?php if ($query[$j]['satuan'] != "Lembar") echo $query[$j]['panjang_kain']; else echo "&nbsp;"; ?></td>
				<td align="right"><?php if ($query[$j]['satuan'] == "Lembar") echo $query[$j]['panjang_kain']; else echo "&nbsp;"; ?></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<?php 
				$jumlah_gelar = $query[$j]['jum_gelar']; 
				$hitung = ceil($jumlah_gelar/120);
				if ($hitung > 1) {
					for($nambah=1; $nambah<=$hitung-1; $nambah++) {
						$bariskertas++;
			 ?>
					<tr>
						<td height="70">&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
		<?php		
					} // end for nambah row
				  } // end if hitung > 1
				  if (isset($query[$j+1]['no_pb_cutting']) && ($query[$j]['no_pb_cutting'] != $query[$j+1]['no_pb_cutting'])) {
					  //echo $bariskertas."<br>";
					if ($posisi_data == 1) {
						  $cek_total = ceil($bariskertas/8);
						  $ulang = ($cek_total*8)-$bariskertas;
					}
					 else {
						  $cek_total = ceil($bariskertas/9);
						  $ulang = ($cek_total*9)-$bariskertas;
					  }
					  for($nambah=1; $nambah<=$ulang; $nambah++) {
						  echo "<tr>
								<td height='70'>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>";
						if ($nambah == $ulang)
							echo "<tr class='printable'>
								<td height='70'>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								</tr>";
					  } // end for nambah utk ulang
				  }
		?>						
		<?php	
				} // end for induk
			}
		?>
	</tbody>
</table>
