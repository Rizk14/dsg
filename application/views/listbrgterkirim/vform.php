<script type="text/javascript" src="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>js/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript" language="javascript">

/*
$(document).ready(function() {
	$("#d_op_first").datepicker();
	$("#d_op_last").datepicker();
});
*/

function toUpper(val) {
	var imotif	= val.value;
	var toUpper	= imotif.toUpperCase();
	val.value	= toUpper;
}

function ck_fstopproduksi() {
	if(document.getElementById('f_stop_produksi').checked==true) {
		document.getElementById('fstopproduksi').value = 't';
	}else{
		document.getElementById('fstopproduksi').value = 'f';
	}
}

</script>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center" style="border-bottom-width:0px">
   <tr><td class="tcat"><?php echo "Info Barang Terkirim"; ?></td></tr>
</table>

<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
   <tr>
     <td class="thead"><img src="<?php echo base_url();?>asset/images/layout/form.gif" align="absmiddle" border="0"> Form <?php echo "Barang Terkirim"; ?></td></tr>
   <tr> 
     <td class="alt2" style="padding:0px;">

	<table id="table-add-box">
	  <tr>
	    <td align="left">
			  <?php 
				   $attributes = array('class' => 'f_master', 'id' => 'myform');
				  	echo form_open('listbrgterkirim/cform/carilistpendding', $attributes);?>
	
		<div id="masterlbrgpenddform">

		  <table width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25%"><?php echo $list_pend_tgl_mulai_op; ?> </td>
					<td width="1%">:</td>
					<td width="74%">
					  <input name="d_op_first" type="text" id="d_op_first" maxlength="10" onclick="displayCalendar(document.forms[0].d_op_first,'dd/mm/yyyy',this)" />
					s.d 
					<input name="d_op_last" type="text" id="d_op_last" maxlength="10" onclick="displayCalendar(document.forms[0].d_op_last,'dd/mm/yyyy',this)"/>
					<span style="color:#FF0000">*</span></td>
				  </tr>	
				  <tr>
				  	<td></td>
					<td></td>
				  	<td><input name="f_stop_produksi" type="checkbox" id="f_stop_produksi" value="f" onclick="ck_fstopproduksi();" /><?php echo $list_opvsdo_stop_produk; ?></td>
				  </tr>
				  <tr>
				  	<td colspan="3">&nbsp;</td>
				  </tr>				  
				  <tr>
					<td><?php echo "Nomor OP "; ?> </td>
					<td>:</td>
					<td><input name="iop" type="text" id="iop" maxlength="10" style="width:80px;" /></td>
				  </tr>
				  <tr>
					<td><?php echo "Nomor DO "; ?> </td>
					<td>:</td>
					<td><input name="ido" type="text" id="ido" maxlength="10" style="width:80px;" /></td>
				  </tr>				  
				  <tr>
					<td><?php echo "Kode Barang "; ?> </td>
					<td>:</td>
					<td><input name="imotif" type="text" id="imotif" maxlength="10" style="width:80px;" onkeyup="toUpper(document.getElementById('imotif'));"/></td>
				  </tr>				  
				  <tr>
				  	<td colspan="3">&nbsp;</td>
				  </tr>
			  </table></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td><div style="border-top:#E4E4E4 1px solid; border-bottom:#E4E4E4 1px solid; border-left:#E4E4E4 1px solid; border-right:#E4E4E4 1px solid; color:#990000; width:210px; padding:5px;"><span style="color:#FF0000">*</span>) Format Tgl: dd/mm/YYYY</div></td>
			</tr>
			<tr>
			  <td align="right">
				<input type="hidden" name="fstopproduksi" id="fstopproduksi" value="f" />
				<input name="btndetail" type="submit" id="btndetail" value="<?php echo $button_detail; ?>" />
				<input name="btnbatal" type="reset" id="btnbatal" value="<?php echo $button_batal; ?>" />			  
			  </td>
			</tr>
		  </table>

		</div>
		<?php echo form_close()?>
	    </td>
	  </tr> 
	</table>

   </td>
  </tr>
</table>
