<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = '192.168.0.93';
$db['default']['username'] = 'dedy';
$db['default']['password'] = 'g#>m[J2P^^';
$db['default']['database'] = 'simdsg';
// yg new utk data2 baru yg udh fix
//$db['default']['database'] = 'cvdutaproduksinew';
$db['default']['dbdriver'] = 'postgre';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['db_external']['hostname'] = '192.168.0.93';
$db['db_external']['username'] = 'dedy';
$db['db_external']['password'] = 'g#>m[J2P^^';
$db['db_external']['database'] = 'sysdbduta';
$db['db_external']['dbdriver'] = 'postgre';
$db['db_external']['dbprefix'] = '';
$db['db_external']['pconnect'] = TRUE;
$db['db_external']['db_debug'] = TRUE;
$db['db_external']['cache_on'] = FALSE;
$db['db_external']['cachedir'] = '';
$db['db_external']['char_set'] = 'utf8';
$db['db_external']['dbcollat'] = 'utf8_general_ci';

$db['db_pelanggan']['hostname'] = '192.168.0.93';
$db['db_pelanggan']['username'] = 'dedy';
$db['db_pelanggan']['password'] = 'g#>m[J2P^^';
$db['db_pelanggan']['database'] = 'dialogue_new';
$db['db_pelanggan']['dbdriver'] = 'postgre';
$db['db_pelanggan']['dbprefix'] = '';
$db['db_pelanggan']['pconnect'] = TRUE;
$db['db_pelanggan']['db_debug'] = TRUE;
$db['db_pelanggan']['cache_on'] = FALSE;
$db['db_pelanggan']['cachedir'] = '';
$db['db_pelanggan']['char_set'] = 'utf8';
$db['db_pelanggan']['dbcollat'] = 'utf8_general_ci';

$db['db_additional']['hostname'] = '192.168.0.93';
$db['db_additional']['username'] = 'dedy';
$db['db_additional']['password'] = 'g#>m[J2P^^';
$db['db_additional']['database'] = 'cvdutaproduksi';
$db['db_additional']['dbdriver'] = 'postgre';
$db['db_additional']['dbprefix'] = '';
$db['db_additional']['pconnect'] = TRUE;
$db['db_additional']['db_debug'] = TRUE;
$db['db_additional']['cache_on'] = FALSE;
$db['db_additional']['cachedir'] = '';
$db['db_additional']['char_set'] = 'utf8';
$db['db_additional']['dbcollat'] = 'utf8_general_ci';

$db['db_sab']['hostname'] = '192.168.0.93';
$db['db_sab']['username'] = 'dedy';
$db['db_sab']['password'] = 'g#>m[J2P^^';
$db['db_sab']['database'] = 'sab';
$db['db_sab']['dbdriver'] = 'postgre';
$db['db_sab']['dbprefix'] = '';
$db['db_sab']['pconnect'] = TRUE;
$db['db_sab']['db_debug'] = TRUE;
$db['db_sab']['cache_on'] = FALSE;
$db['db_sab']['cachedir'] = '';
$db['db_sab']['char_set'] = 'utf8';
$db['db_sab']['dbcollat'] = 'utf8_general_ci';

/* End of file database.php */
/* Location: ./application/config/database.php */
