<?php
// Konversi waktu ke : Senin, 4 Januari 2014
function format_hari_tanggal($waktu)
{
    // Senin, Selasa dst.
    $hari_array = array(
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jumat',
        'Sabtu'
    );
    $hr = date('w', strtotime($waktu));
    $hari = $hari_array[$hr];

    // Tanggal: 1-31 dst, tanpa leading zero.
    $tanggal = date('j', strtotime($waktu));

    // Bulan: Januari, Maret dst.
    $bulan_array = array(
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    );
    $bl = date('n', strtotime($waktu));
    $bulan = $bulan_array[$bl];

    // Tahun, 4 digit.
    $tahun = date('Y', strtotime($waktu));

    // Hasil akhir: Senin, 1 Oktober 2014
    return "$hari, $tanggal $bulan $tahun";
}

// Format tangal ke 1 Januari 1990
function format_tanggal($waktu)
{
    // Tanggal, 1-31 dst, tanpa leading zero.
    $tanggal = date('j', strtotime($waktu));

    // Bulan, Januari, Maret dst
    $bulan_array = array(
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    );
    $bl = date('n', strtotime($waktu));
    $bulan = $bulan_array[$bl];

    // Tahun
    $tahun = date('Y', strtotime($waktu));

    // Senin, 12 Oktober 2014
    return "$tanggal $bulan $tahun";
}

// Format tangal ke yyyy-mm-dd
function date_to_en($tanggal)
{
    $tgl = date('Y-m-d', strtotime($tanggal));
    if ($tgl == '1970-01-01') {
        return '';
    } else {
        return $tgl;
    }
}

// Format tangal ke dd-mm-yyyy
function date_to_id($tanggal)
{
    $tgl = date('d-m-Y', strtotime($tanggal));
    if ($tgl == '01-01-1970') {
        return '';
    } else {
        return $tgl;
    }
}



// Buat setiap awal kata huruf besar
function format_title_case($string)
{
    return ucwords($string);
}


function format_status_pendaftaran($status)
{
    if ($status == '0') {
        return 'Mundur';
    } else {
        return 'Aktif';
    }
}



function format_status_verifikasi($status)
{
    if ($status == '0') {
        return 'Belum';
    } else {
        return 'Sudah';
    }
}

function format_status_approve($status)
{
    if ($status == 'f') {
        return 'Belum';
    } elseif ($status == 't') {
        return 'Sudah';
    }
}

function format_is_blokir($status)
{
    if ($status == '1') {
        return 'BLOKIR';
    } else {
        return 'AKTIF';
    }
}

function format_bagian($bagian)
{
    if ($bagian == '1') {
        return 'Gudang';
    } else {
        return 'Adminstok';
    }
}

function format_jenis_masuk($jenis_masuk)
{
    if ($jenis_masuk == '1') {
        return 'Masuk dari Unit Jahit';
    } else if ($jenis_masuk == '2') {
        return 'Masuk dari Unit Packing';
    } else if ($jenis_masuk == '3') {
        return 'Masuk Lain-lain';
    }
}

function format_jenis_keluar($jenis_keluar)
{
    if ($jenis_keluar == '1') {
        return 'Keluar Ke Gudang Jadi LG 1';
    } else if ($jenis_keluar == '2') {
        return 'Keluar Lain-lain';
    }
}

function encrypt_url($string)
{
    $output = false;
    $secret_key     = 'dukanaonteuapal';
    $secret_iv      = 'nanaonan';
    $encrypt_method = 'aes-256-cbc';
    $key    = hash("sha256", $secret_key, TRUE);
    $iv     = substr(hash("sha256", $secret_iv), 0, 16);
    $result = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($result);
    $output = str_replace('=', '', $output);
    return $output;
}
function decrypt_url($string)
{
    $output = false;
    $secret_key     = 'dukanaonteuapal';
    $secret_iv      = 'nanaonan';
    $encrypt_method = 'aes-256-cbc';
    $key    = hash("sha256", $secret_key, TRUE);
    $iv = substr(hash("sha256", $secret_iv), 0, 16);
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
}

if (!function_exists('get_tax')) {
    function get_tax($tanggal)
    {
        $ci  = get_instance();
        $cek = $ci->db->query("
        SELECT
            *, (n_tax / 100 + 1) excl_divider, n_tax / 100 n_tax_val
        FROM
            public.tr_tax_amount 
        where '$tanggal' between d_start and d_finish");
        if ($cek->num_rows() > 0) {
            return $cek->row();
        } else {
            return null;
        }
    }
}
