<?php
/*
t  = title
tt = textfile
ta = textarea
tb = textbox
lb = listbox
ts = button submit
tr = button reset

*/

/* Login */
$lang['t_username']	= 'Nama Pemakai';
$lang['t_passwd']	= 'Kata Sandi';
$lang['ts_login']	= 'Masuk';
$lang['tr_reset']	= 'Batal';
/* End 0f Login */

/* Tombol */
$lang['button_simpan']	= 'Simpan';
$lang['button_batal']	= 'Batal';
$lang['button_update']	= 'Update';
$lang['button_update_opname']	= 'Update Opname';
$lang['button_cari']	= 'Cari Data';
$lang['button_keluar']	= 'Keluar';
$lang['button_kembali']	= 'Kembali';
$lang['button_mutasi']	= 'Mutasi Stok Opname Ke Saldo Awal';
$lang['button_excel']	= 'Export Ke Excel';
$lang['button_cetak']	= 'Cetak Data';
$lang['button_detail']	= 'Lihat Detail Informasi';
$lang['button_transfer']	= 'Transfer';
$lang['button_transfer_ulang']	= 'Tranfer Ulang';
$lang['button_laporan']	= 'Laporan';
$lang['button_approve']	= 'Approve';
$lang['button_cekvoucher']	= 'Cek Voucher';
/* End 0f Tombol */

/* Text Pencarian */
$lang['text_cari']	= 'Pencarian Data';
/* End 0f Pencarian */

/* Link */
$lang['link_aksi']	= 'Aksi';
/* End 0f Link */

/* Inisial Perusahaan */
$lang['page_title_inisial']	= "Entitas Perusahaan";
$lang['form_inisial_nm_perusahaan']	= "Nama Perusahaan";
$lang['form_inisial_almt_perusahaan']	= "Alamat Perusahaan";
$lang['form_inisial_npwp_perusahaan']	= "NPWP";
$lang['form_inisial_telpon']	= "Telepon";
$lang['form_inisial_fax']	= "Fax";
$lang['form_inisial_kontak']	= "Kontak";
/* End 0f Inisial Perusahaan */

/* OP */
$lang['page_title_op']	= 'Order Pembelian';
$lang['form_nomor_op']	= 'Nomor OP';
$lang['form_tgl_op']	= 'Tanggal OP';
$lang['form_cabang_op']	= 'Cabang';
$lang['form_option_pel_op']	= 'Pilih Pelanggan';
$lang['form_option_cab_op']	= 'Pilih Cabang';
$lang['form_tgl_bts_kirim_op']	= 'Tgl. Batas Kirim';
$lang['form_nomor_sop_op']	= 'Nomor S.O.P';
$lang['form_title_detail_op']	= 'PERMINTAAN';
$lang['form_jml_op']	= 'JUMLAH';
$lang['form_kode_produk_op']	= 'KODE PRODUK';
$lang['form_nm_produk_op']	= 'NAMA PRODUK';
$lang['form_ket_op']	= 'KETERANGAN';
/* End 0f OP */

/* DO */
$lang['page_title_do']	= 'Delivery Order';
$lang['form_nomor_do']	= 'Nomor DO';
$lang['form_tgl_do']	= 'Tgl. DO';
$lang['form_cabang_do']	= 'Cabang';
$lang['form_option_pel_do']	= 'Pilih Pelanggan';
$lang['form_option_cab_do']	= 'Pilih Cabang';
$lang['form_title_detail_do']	= 'PERMINTAAN';
$lang['form_nomor_op_do']	= 'NOMOR OP';
$lang['form_kode_produk_do']	= 'KODE PRODUK';
$lang['form_nm_produk_do']	= 'NAMA PRODUK';
$lang['form_jml_product_do']	= 'JUMLAH';
$lang['form_harga_product_do']	= 'HARGA';
$lang['form_ket_do']	= 'KETERANGAN';
/* End 0f DO */

/* BMM */
$lang['page_title_bmm']	= 'Bon M Masuk';
$lang['form_title_detail_bmm']	= 'DETAIL';
$lang['form_nomor_bmm']	= 'Nomor';
$lang['form_tanggal_bmm']	= 'Tanggal';
$lang['form_sj_bmm']		= 'No. SJ';
$lang['form_title_detail_bmm']	= 'DETAIL';
$lang['form_kode_produk_bmm']	= 'KODE BARANG';
$lang['form_nm_produk_bmm']	= 'NAMA BARANG';
$lang['form_jml_produk_bmm']	= 'JUMLAH';
/* End 0f BBM */

/* Bon M Keluar */
$lang['page_title_bmk']	= 'Bon M Keluar';
$lang['form_kepada_bmk']	= 'Kepada';
$lang['form_option_kepada_bmk']	= 'Kepada';
$lang['form_nomor_bmk']	= 'Nomor';
$lang['form_tgl_bmk']	= 'Tgl.';
$lang['form_title_detail_bmk']	= 'DETAIL';
$lang['form_kode_produk_bmk']	= 'KODE BARANG';
$lang['form_nm_produk_bmk']	= 'NAMA BARANG';
$lang['form_jml_bmk']	= 'JUMLAH';
/* End 0f Bon M Keluar */

/* Bukti Barang Masuk */
$lang['page_title_bbm']	= 'Bukti Barang Masuk';
$lang['form_title_detail_bbm']	= 'DETAIL';
$lang['form_nomor_bbm']	= 'Nomor BBM';
$lang['form_tanggal_bbm']	= 'Tanggal BBM';
$lang['form_to_bbm']	= 'Dari';
$lang['form_nomor_sj_bbm']	= 'Nomor SJ Barang/ BBK';
$lang['form_alamat_bbm']	= 'Alamat';
$lang['form_kode_produk_bbm']	= 'KODE BARANG';
$lang['form_nm_produk_bbm']	= 'NAMA BARANG';
$lang['form_hjp_bbm']	= 'HJP';
$lang['form_unit_bbm']	= 'UNIT';
$lang['form_total_bbm']	= 'TOTAL';
$lang['form_total_nilai_brg_bbm']	= 'Total Nilai Barang';
$lang['form_ket_bbm']	= 'Keterangan';
/* End 0f Bukti Barang Masuk */

/* Bukti Barang Keluar */
$lang['page_title_bbk']	= 'Bukti Barang Keluar';
$lang['form_title_detail_bbk']	= 'DETAIL';
$lang['form_nomor_bbk']	= 'Nomor BBK';
$lang['form_tanggal_bbk']	= 'Tanggal BBK';
$lang['form_kepada_bbk']	= 'Kepada';
$lang['form_ket_bbk']	= 'Keterangan';
$lang['form_alamat_bbk']	= 'Alamat';
$lang['form_kode_produk_bbk']	= 'KODE BARANG';
$lang['form_nm_produk_bbk']	= 'NAMA BARANG';
$lang['form_hjp_bbk']	= 'HJP';
$lang['form_unit_bbk']	= 'UNIT';
$lang['form_total_bbk']	= 'TOTAL';
$lang['form_total_nilai_brg_bbk']	= 'Total Nilai Barang';
$lang['form_ket_bbk']	= 'Keterangan';
/* End 0f Bukti Barang Keluar*/

/* SJ */
$lang['page_title_sj']	= 'Surat Jalan (SJ)';
$lang['form_title_detail_sj']	= 'DETAIL';
$lang['form_nomor_sj']	= 'Nomor SJ';
$lang['form_tanggal_sj']	= 'Tanggal SJ';
$lang['form_kode_produk_sj']	= 'KODE BARANG';
$lang['form_nm_produk_sj']	= 'NAMA BARANG';
$lang['form_hjp_sj']	= 'HJP';
$lang['form_unit_sj']	= 'UNIT';
$lang['form_total_sj']	= 'TOTAL';
$lang['form_total_nilai_brg_sj']	= 'Total Nilai Barang';
$lang['form_ket_sj']	= 'Keterangan';
$lang['form_cabang_sj']	= 'Pelanggan';
$lang['form_option_pel_sj']	= ' Pilih Pelanggan ';
/* End 0f SJ */

/* SJ Bahan Baku*/
$lang['page_title_sjp_bhnbaku']		= 'SJ Penjualan Bahan Baku';
$lang['form_title_detail_sjp_bhnbaku']	= 'DETAIL';
$lang['form_nomor_sjp_bhnbaku']		= 'Nomor SJ';
$lang['form_tanggal_sjp_bhnbaku']	= 'Tanggal SJ';
$lang['form_nm_gdg_sjp_bhnbaku']	= 'NAMA GUDANG';
//$lang['form_kode_produk_sjp_bhnbaku']	= 'KODE BHN BAKU';
$lang['form_kode_produk_sjp_bhnbaku']	= 'KODE URUT';
$lang['form_nm_produk_sjp_bhnbaku']	= 'NAMA BHN BAKU';
$lang['form_hjp_sjp_bhnbaku']		= 'HARGA';
$lang['form_unit_sjp_bhnbaku']		= 'QTY';
$lang['form_satuan_sjp_bhnbaku']	= 'SATUAN';
$lang['form_total_sjp_bhnbaku']		= 'TOTAL';
$lang['form_total_nilai_brg_sjp_bhnbaku']	= 'Total Nilai';
$lang['form_ket_sjp_bhnbaku']		= 'Keterangan';
$lang['form_cabang_sjp_bhnbaku']	= 'Pelanggan';
$lang['form_option_pel_sjp_bhnbaku']= ' Pilih Pelanggan ';
/* End 0f SJ Bahan Baku*/

/* Stok Opname */
$lang['page_title_stokopname']	= 'Stok Opname Baru';
$lang['page_intitle_stokopname']	= 'STOK OPNAME GUDANG JADI';
$lang['page_title_stokopnamemutasi']	= 'Mutasi Stok Opname Ke Saldo Awal';
$lang['form_cari_tgl_stokopname']	= 'Cari Opname ';
$lang['form_tgl_stokopname']	= 'Tgl Opname ';
$lang['form_title_detail_stokopname']	= 'DETAIL';
$lang['form_kode_product_opname']	= 'KODE BARANG';
$lang['form_nm_product_opname']	= 'NAMA BARANG';
$lang['form_jml_product_opname']	= 'SALDO';
//$lang['form_jml_fisik_product_opname']	= 'S.O.P';
$lang['form_jml_fisik_product_opname']	= 'S.O';
$lang['form_note_opname']	= 'ALASAN KOREKSI';
/* End 0f Stok Opname */

/* Saldo Awal FC */
$lang['page_title_saldoawalfc']	= 'Saldo Awal Forecast';
$lang['page_intitle_saldoawalfc']	= 'SALDO AWAL FORECAST';
$lang['page_title_saldoawalfcmutasi']	= 'Mutasi Saldo Awal FC';
$lang['form_cari_tgl_saldoawalfc']	= 'Cari Saldo Awal FC ';
$lang['form_tgl_saldoawalfc']	= 'Tgl Saldo Awal ';
$lang['form_title_detail_saldoawalfc']	= 'DETAIL';
$lang['form_kode_product_opname']	= 'KODE BARANG';
$lang['form_nm_product_opname']	= 'NAMA BARANG';
$lang['form_jml_product_opname']	= 'SALDO';
//$lang['form_jml_fisik_product_opname']	= 'S.O.P';
$lang['form_jml_fisik_product_opname']	= 'Qty';
$lang['form_note_opname']	= 'ALASAN KOREKSI';
/* End 0f Saldo Awal FC */

/* Pelanggan */
$lang['page_title_pelanggan']	= 'PELANGGAN PUSAT';
$lang['form_panel_daftar_pelanggan']	= 'DAFTAR PELANGGAN';
$lang['form_form_pelanggan']	= 'FORM PELANGGAN';
$lang['form_cari_pelanggan']	= 'CARI PELANGGAN';
$lang['form_kode_pelanggan']	= 'Kode Pelanggan';
$lang['form_nm_pelanggan']	= 'Nama Pelanggan';
$lang['form_nm_group_pelanggan']	= 'Group Pelanggan';
$lang['form_option_group_pelanggan'] = '[ Pilih Group Pelanggan ]';
$lang['form_kontak_pelanggan']	= 'Kontak';
$lang['form_alamat_pelanggan']	= 'Alamat';
$lang['form_pkp_pelanggan']	= 'PKP';
$lang['form_npwp_pelanggan']	= 'NPWP';
$lang['form_top_pelanggan']	= 'TOP';
$lang['form_konsinyasi_pelanggan'] = 'Konsinyasi';
$lang['form_telpon_pelanggan']	= 'Telpon';
$lang['form_fax_pelanggan']	= 'Fax';
$lang['form_telp_fax_pelanggan']	= 'Telpon/ Fax';
/* End 0f Pelanggan */

/* Pelanggan Cabang */
$lang['page_title_pelcab']	= 'PELANGGAN CABANG';
$lang['form_panel_daftar_pelcab']	= 'DAFTAR CABANG';
$lang['form_panel_form_pelcab']	= 'FORM CABANG';
$lang['form_panel_cari_pelcab']	= 'CARI CABANG';
$lang['form_kode_cab']	= 'Kode Cabang';
$lang['form_kode_pelcab']	= 'Kode Pelanggan';
$lang['form_nm_cab']	= 'Nama Cabang';
$lang['form_kota_cab']	= 'Kota';
$lang['form_inisial_cab'] = 'Inisial';
/* End 0f Pelanggan Cabang */

/* Group Pelanggan */
$lang['page_title_group_pel']	= 'GROUP PELANGGAN';
$lang['form_panel_daftar_group_pel']	= 'DAFTAR GROUP PELANGGAN';
$lang['form_form_group_pel']	= 'FORM GROUP PELANGGAN';
$lang['form_cari_group_pel']	= 'CARI GROUP PELANGGAN';
$lang['form_kode_group_pel']	= 'Kode Group Pelanggan';
$lang['form_nm_group_pel']	= 'Nama Group Pelanggan';
$lang['form_note_group_pel']	= 'Keterangan';
/* End 0f Group Pelanggan */

/* Pelanggan Kode Transfer */
$lang['page_title_codetransfer']	= 'KODE TRANSFER DO PELANGGAAN';
$lang['form_panel_daftar_codetransfer']	= 'KODE TRANSFER';
$lang['form_panel_cari_codetransfer']	= 'PENCARIAN';
$lang['form_kode_codetransfer']	= 'Kode Pelanggan (Transfer)';
$lang['form_kode_pelcodetransfer']	= 'Pelanggan';
$lang['form_keyword_pencarian']	= '';
/* End 0f Pelanggan Kode Transfer */

/* Faktur Penjualan Non DO */
$lang['page_title_fpenjualanndo']	= 'Faktur Penjualan Non DO';
$lang['form_nomor_f_fpenjualanndo']	= 'Nomor Faktur';
$lang['form_tgl_f_fpenjualanndo']	= 'Tgl Faktur';
$lang['form_cabang_fpenjualanndo']	= 'Pelanggan';
$lang['form_pilih_cab_manual_fpenjualanndo']	= 'Input Manual dari BBK';
$lang['form_pilih_cab_fpenjualanndo']	= 'Pilih Pelanggan';
$lang['form_detail_f_fpenjualanndo']	= 'DETAIL FAKTUR';
$lang['form_kd_brg_fpenjualanndo']	= 'KODE BARANG';
$lang['form_nm_brg_fpenjualanndo']	= 'NAMA BARANG';
$lang['form_hjp_fpenjualanndo']	= 'HJP';
$lang['form_qty_fpenjualanndo']	= 'QTY';
$lang['form_nilai_fpenjualanndo']	= 'NILAI';
$lang['form_ket_f_fpenjualanndo']	= 'Keterangan';
$lang['form_tgl_jtempo_fpenjualanndo']	= 'Tgl. Jatuh Tempo';
$lang['form_no_fpajak_fpenjualanndo']	= 'No. Faktur Pajak';
$lang['form_tgl_fpajak_fpenjualanndo']	= 'Tgl.';
$lang['form_ket_cetak_fpenjualanndo']	= 'Faktur Pajak Sudah Diprint';
$lang['form_tnilai_fpenjualanndo']	= 'Total Nilai';
$lang['form_vdiscount_fpenjualanndo']	= 'Disc Rp.';
$lang['form_ndiscount_fpenjualanndo']	= 'Disc %';
$lang['form_dlm_fpenjualanndo']	= 'nilai';
$lang['form_total_fpenjualanndo']	= 'Total';
$lang['form_ppn_fpenjualanndo']		= 'PPN 11%';
$lang['form_grand_t_fpenjualanndo']	= 'Grand Total';
/* End 0f Faktur Penjualan non DO */

/* Faktur Penjualan Tertentu */
$lang['page_title_fpenjualantertentu']	= 'Faktur Penjualan Tertentu';
$lang['form_nomor_f_fpenjualantertentu']	= 'Nomor Faktur';
$lang['form_tgl_f_fpenjualantertentu']	= 'Tgl Faktur';
$lang['form_cabang_fpenjualantertentu']	= 'Pelanggan';
$lang['form_pilih_cab_manual_fpenjualantertentu']	= 'Manual Input';
$lang['form_pilih_cab_fpenjualantertentu']	= 'Pilih Cabang';
$lang['form_detail_f_fpenjualantertentu']	= 'DETAIL FAKTUR';
$lang['form_kd_brg_fpenjualantertentu']	= 'KODE BARANG';
$lang['form_nm_brg_fpenjualantertentu']	= 'NAMA BARANG';
$lang['form_hjp_fpenjualantertentu']	= 'HJP';
$lang['form_qty_fpenjualantertentu']	= 'QTY';
$lang['form_nilai_fpenjualantertentu']	= 'NILAI';
$lang['form_ket_f_fpenjualantertentu']	= 'Keterangan';
$lang['form_tgl_jtempo_fpenjualantertentu']	= 'Tgl. Jatuh Tempo';
$lang['form_no_fpajak_fpenjualantertentu']	= 'No. Faktur Pajak';
$lang['form_tgl_fpajak_fpenjualantertentu']	= 'Tgl.';
$lang['form_tnilai_fpenjualantertentu']	= 'Total Nilai';
$lang['form_diskon_fpenjualantertentu']	= 'Diskon';
$lang['form_dlm_fpenjualantertentu']	= 'nilai';
$lang['form_total_fpenjualantertentu']	= 'Total';
$lang['form_ppn_fpenjualantertentu']		= 'PPN 11%';
$lang['form_grand_t_fpenjualantertentu']	= 'Grand Total';
/* End 0f Faktur Penjualan Tertentu */

/* Faktur Penjualan Berdasarkan DO */
$lang['page_title_fpenjualando']	= 'Faktur Penjualan Berdasarkan DO';
$lang['form_nomor_f_fpenjualando']	= 'Nomor Faktur';
$lang['form_tgl_f_fpenjualando']	= 'Tgl Faktur';
$lang['form_cabang_fpenjualando']	= 'Cabang';
$lang['form_pilih_cab_fpenjualando']	= 'Pilih Cabang';
$lang['form_detail_f_fpenjualando']	= 'DETAIL FAKTUR';
$lang['form_nomor_do_f_fpenjualando']	= 'NOMOR DO';
$lang['form_tgl_do_f_fpenjualando']	= 'TANGGAL DO';
$lang['form_kd_brg_fpenjualando']	= 'KODE BARANG';
$lang['form_nm_brg_fpenjualando']	= 'NAMA BARANG';
$lang['form_hjp_fpenjualando']	= 'HJP';
$lang['form_qty_fpenjualando']	= 'QTY';
$lang['form_nilai_fpenjualando']	= 'NILAI';
$lang['form_tgl_jtempo_fpenjualando']	= 'Tgl. Jatuh Tempo';
$lang['form_no_fpajak_fpenjualando']	= 'No. Faktur Pajak';
$lang['form_tgl_fpajak_fpenjualando']	= 'Tgl.';
$lang['form_ket_cetak_fpenjualando']	= 'Faktur Pajak Sudah Diprint';
$lang['form_tnilai_fpenjualando']	= 'Total Nilai';
$lang['form_diskon_fpenjualando']	= 'Diskon';
$lang['form_dlm_fpenjualando']	= 'nilai';
$lang['form_total_fpenjualando']	= 'Total';
$lang['form_ppn_fpenjualando']		= 'PPN 11%';
$lang['form_grand_t_fpenjualando']	= 'Grand Total';
/* End 0f Faktur Penjualan Berdasarkan DO */

/* Faktur Penjualan Bahan Baku */
$lang['page_title_fpenjualanbhnbaku']	= 'Faktur Penjualan Bahan Baku';
$lang['form_nomor_f_fpenjualanbhnbaku']	= 'Nomor Faktur';
$lang['form_tgl_f_fpenjualanbhnbaku']	= 'Tgl Faktur';
$lang['form_cabang_fpenjualanbhnbaku']	= 'Pelanggan';
$lang['form_pilih_cab_manual_fpenjualanbhnbaku']	= 'Input Manual dari BBK';
$lang['form_pilih_cab_fpenjualanbhnbaku']	= 'Pilih Pelanggan';
$lang['form_detail_f_fpenjualanbhnbaku']	= 'DETAIL FAKTUR';
//$lang['form_kd_brg_fpenjualanbhnbaku']	= 'KODE BARANG';
$lang['form_kd_brg_fpenjualanbhnbaku']	= 'KODE URUT';
$lang['form_nm_brg_fpenjualanbhnbaku']	= 'NAMA BARANG';
$lang['form_hjp_fpenjualanbhnbaku']	= 'HARGA';
$lang['form_qty_fpenjualanbhnbaku']	= 'QTY';
$lang['form_nilai_fpenjualanbhnbaku']	= 'NILAI';
$lang['form_ket_f_fpenjualanbhnbaku']	= 'Keterangan';
$lang['form_tgl_jtempo_fpenjualanbhnbaku']	= 'Tgl. Jatuh Tempo';
$lang['form_no_fpajak_fpenjualanbhnbaku']	= 'No. Faktur Pajak';
$lang['form_tgl_fpajak_fpenjualanbhnbaku']	= 'Tgl.';
$lang['form_ket_cetak_fpenjualanbhnbaku']	= 'Faktur Pajak Sudah Diprint';
$lang['form_tnilai_fpenjualanbhnbaku']	= 'Total Nilai';
$lang['form_diskon_fpenjualanbhnbaku']	= 'Diskon';
$lang['form_dlm_fpenjualanbhnbaku']	= 'nilai';
$lang['form_total_fpenjualanbhnbaku']	= 'Total';
$lang['form_ppn_fpenjualanbhnbaku']		= 'PPN 11%';
$lang['form_grand_t_fpenjualanbhnbaku']	= 'Grand Total';
/* End 0f Faktur Penjualan Bahan Baku */

/* Tambah User Baru */
$lang['page_title_adduser']	= 'Pemakai Aplikasi';
$lang['form_tbl_user_id']	= 'User ID';
$lang['form_tbl_user_name']	= 'User Name';
$lang['form_tbl_level']	= 'Level';
$lang['form_tbl_status']	= 'Status';
$lang['form_user_name_usr']	= 'User Name';
$lang['form_passwd_usr']	= 'Password';
$lang['form_re_passwd_usr']	= 'Re-Password';
$lang['form_level_user_usr']	= 'Level User';
$lang['form_status_usr']	= 'Status';
$lang['form_pilihan_level_usr']	= 'Level User';
$lang['form_pilihan_status_usr']	= 'Status User';

$lang['form_passwd_old_usr']	= 'Password Lama';
$lang['form_passwd_new_usr']	= 'Password Baru';
/* End 0f Tambah User Baru */

/* Tambah Barang Mentah */
$lang['form_title_brgmentah']	= 'Barang Mentah';
$lang['form_kd_brg_brgmentah']	= 'Kode Barang';
$lang['form_nm_brg_brgmentah']	= 'Nama Barang';
$lang['form_color_brgmentah']	= 'Color';
$lang['form_qty_brgmentah']		= 'Qty';
$lang['form_unit_brgmentah']	= 'Satuan';
$lang['form_hjp_brgmentah']	= 'HJP';
$lang['form_pilih_satuan_brgmentah']	= 'Pilih Satuan';
/* End 0f Tambah Barang Mentah */

/* Tambah Barang Jadi */
$lang['form_title_brgjadi']		= 'Barang Jadi';
$lang['form_kd_brg_brgjadi']	= 'Kode Barang';
$lang['form_nm_brg_brgjadi']	= 'Nama Barang';
$lang['form_pemasok_brgjadi']	= 'Pemasok';
$lang['form_merek_brgjadi']		= 'Merek';
$lang['form_quality_brgjadi']	= 'Quality';
$lang['form_spenawaran_brgjadi']= 'Surat Penawaran';
$lang['form_sbrg_brgjadi']		= 'Status Barang';
$lang['form_tglregist_brgjadi']	= 'Tanggal Registrasi';
$lang['form_kat_brgjadi']	= 'Kategori';
$lang['form_layout_brgjadi']	= 'Layout';
$lang['form_sproduksi_brgjadi']	= 'Stop Produksi';
$lang['form_tpenawaran_brgjadi']	= 'Tgl Penawaran';
$lang['form_qty_brgjadi']	= 'Qty';
$lang['form_hjp_brgjadi']	= 'HJP';
$lang['form_pilih_status_brgjadi']	= 'Pilih Status Barang';
/* End 0f Tambah Barang Jadi */

/* Tambah Barang Jadi + Motif */
$lang['form_title_brgmotif']	= 'Barang Jadi + Motif';
$lang['form_kd_brg_brgmotif']	= 'Kode Barang';
$lang['form_kd_motif_brgmotif']	= 'Kode Barang + Motif';
/* disabled 04012011 
$lang['form_nm_motif_brgmotif']	= 'Nama Barang + Motif';
//$lang['form_pilih_color_brgmotif']	= 'Pilih Warna';
*/
$lang['form_nm_motif_brgmotif']	= 'Nama Barang';
$lang['form_qty_brgmotif']		= 'Qty';
$lang['form_color_brgmotif']	= 'Nama Motif/ Warna';
$lang['form_pilih_color_brgmotif']	= 'Pilih Motif/ Warna';
/* End 0f Tambah Barang Jadi + Motif */

$lang['page_title_tbl_parentmenu_priv']	= "Parent Menu";
$lang['form_id_tbl_parentmenu_priv']	= "MenuID";
$lang['form_label_tbl_parentmenu_priv']	= "Nama Label";
$lang['form_user_tbl_parentmenu_priv']	= "User";
$lang['form_sort_tbl_parentmenu_priv']	= "Sort";
$lang['form_active_tbl_parentmenu_priv']	= "Active Menu";
$lang['form_child_tbl_parentmenu_priv']	= "Child Menu";

/* Tambah Printer Baru */
$lang['page_title_addprinter']	= 'Daftar Printer';
$lang['form_tbl_printer_name']	= 'Nama Printer';
$lang['form_tbl_printer_ip']	= 'Destination IP';
$lang['form_tbl_printer_uri']	= 'Printer Uri (cups)';
$lang['form_tbl_level']	= 'Level';
$lang['form_tbl_status']	= 'Status';
$lang['form_name_printer']	= 'Nama Printer';
$lang['form_ip_printer']	= 'Destination IP';
$lang['form_uri_printer']	= 'Printer Uri (cups)';
$lang['form_level_user_printer']	= 'Level User';
$lang['form_pilihan_level_printer']	= 'Level User';
$lang['form_pilihan_user_printer']	= 'User';
/* End 0f Tambah Printer Baru */

/* Tambah Alamat IP */
$lang['page_title_ip']		= 'Alamat Internet';
$lang['form_tbl_ip_name']	= 'IP (Alamat Internet)';
$lang['form_tbl_ip_ket']	= 'Keterangan';
$lang['form_tbl_ip_status']	= 'Status';
/* End 0f Tambah Alamat IP */

/* Kode Akun Pajak */
$lang['page_title_akun_pajak']	= 'AKUN PAJAK';
$lang['form_panel_daftar_akun_pajak']	= 'DAFTAR AKUN PAJAK';
$lang['form_panel_form_akun_pajak']	= 'FORM AKUN PAJAK';
$lang['form_panel_cari_akun_pajak']	= 'CARI AKUN PAJAK';
$lang['form_kode_akun_pajak']	= 'Kode Akun Pajak';
/* End 0f Kode Akun Pajak */

/* Kode Jenis Setoran */
$lang['page_title_jsetor_pajak']	= 'KODE JENIS SETORAN';
$lang['form_panel_daftar_jsetor_pajak']	= 'DAFTAR KODE JENIS SETORAN';
$lang['form_panel_form_jsetor_pajak']	= 'FORM KODE JENIS SETORAN';
$lang['form_panel_cari_jsetor_pajak']	= 'CARI KODE JENIS SETORAN';
$lang['form_kode_jsetor_pajak']	= 'Kode Akun Pajak';
$lang['form_kode_kode_jsetor_pajak']	= 'Kode Jenis Setoran';
$lang['form_jns_setoran']		= 'Jns Setoran/ Uraian Pembayaran';
$lang['form_keterangan_jsetor_pajak']	= 'Keterangan';
/* End 0f Kode Jenis Setoran */


/* SSP */
$lang['page_title_ssp']			= 'SURAT SETORAN PAJAK';
$lang['form_nama_wp_ssp']		= 'Nama WP';
$lang['form_kd_akun_pajak_ssp']	= 'Kode Akun Pajak';
$lang['form_kd_jns_setor_ssp']	= 'Kode Jenis Setoran';
$lang['form_uraian_pembayaran_spp']	= 'Uraian Pembayaran';
$lang['form_masa_pajak_ssp']	= 'Masa Pajak';
$lang['form_masa_pajak_thn_ssp']= 'Tahun';
$lang['form_masa_pajak_bln_ssp']= 'Bulan';
$lang['form_jml_pembayaran_ssp']= 'Jumlah Pembayaran';
$lang['form_wjb_pajak_penyetor_ssp']		= 'Wajib Pajak/ Penyetor';
$lang['form_wjb_pajak_penyetor_tempat_ssp']	= 'Tempat WP';
$lang['form_wjb_pajak_penyetor_tgl_ssp']	= 'Tgl WP';
$lang['form_wjb_pajak_penyetor_penyetor_ssp']	= 'Penyetor WP';
/* End 0f SSP */

/* Transfer DO */
$lang['page_title_transferdo2']		= 'TRANSFER DO';
$lang['page_title_detail_transferdo2']	= 'DETAIL TRANSFER DO';
$lang['form_kode_do_transferdo2']	= 'Nomor DO';
$lang['form_tgl_do_transferdo2']	= 'Tgl. DO';
$lang['form_kode_op_transferdo2']	= 'Nomor OP';
$lang['form_tgl_op_transferdo2']	= 'Tgl. OP';
$lang['form_kodebrg_transferdo2']	= 'Kode Barang';
$lang['form_nmbrg_transferdo2']		= 'Nama Barang';
$lang['form_jmldo_transfer2']		= 'Jumlah';
$lang['form_note_transfer2']		= 'Keterangan';
$lang['form_area_transfer2']		= 'Area';
/* End 0f Transfer DO */

/* SJ Hasil Packing */
$lang['page_title_sjhsl_packing']			= 'Bon M Masuk dari SJ Hasil Packing';
$lang['page_title_detail_sjhsl_packing']	= 'DETAIL SJ HASIL PACKING';
$lang['list_tgl_mulai_sjhsl_packing']		= 'SJ Mulai Tgl.';
$lang['form_no_sjhsl_packing']				= 'No. SJ Hasil Packing';
$lang['form_sjhsl_packing']					= 'NOMOR SJ';
/* End 0f Transfer DO */

/* Forcast */
$lang['page_title_forcast']	= 'FORCAST';
$lang['form_title_detail_forcast']	= 'DETAIL';
$lang['form_nomor_forcast']	= 'Nomor';
$lang['form_tanggal_forcast']	= 'Tanggal';
$lang['form_no_dropforcast_forcast']	= 'No. Drop Forcast/ OP';
$lang['form_title_detail_forcast']	= 'DETAIL';
$lang['form_kode_produk_forcast']	= 'KODE BARANG';
$lang['form_nm_produk_forcast']	= 'NAMA BARANG';
$lang['form_jml_produk_forcast']	= 'JUMLAH';
/* End 0f Forcast */

/* SJ Drop Forcast */
$lang['page_title_sjforcast']	= 'SJ FORCAST';
$lang['form_title_detail_sjforcast']	= 'DETAIL';
$lang['form_nomor_sjforcast']	= 'Nomor';
$lang['form_tanggal_sjforcast']	= 'Tanggal';
$lang['form_title_detail_sjforcast']	= 'DETAIL';
$lang['form_kode_produk_sjforcast']	= 'KODE BARANG';
$lang['form_nm_produk_sjforcast']	= 'NAMA BARANG';
$lang['form_jml_produk_sjforcast']	= 'JUMLAH';
/* End 0f SJ Drop Forcast */

/* Jenis voucher pembayaran */
$lang['page_title_jnsvoucher']		= 'JENIS VOUCHER';
$lang['form_panel_daftar_jnsvoucher']	= 'DAFTAR JENIS VOUCHER';
$lang['form_panel_form_jnsvoucher']	= 'FORM JENIS VOUCHER';
$lang['form_panel_cari_jnsvoucher']	= 'CARI JENIS VOUCHER';
$lang['form_kode_jnsvoucher']		= 'Jenis Voucher';
/* End 0f Jenis voucher pembayaran */

/* Kode voucher pembayaran */
$lang['page_title_kodevoucher']		= 'KODE VOUCHER';
$lang['form_panel_daftar_kodevoucher']	= 'DAFTAR KODE VOUCHER';
$lang['form_panel_form_kodevoucher']	= 'FORM KODE VOUCHER';
$lang['form_panel_cari_kodevoucher']	= 'CARI KODE VOUCHER';
$lang['form_jns_kodevoucher']		= 'Jenis Voucher';
$lang['form_kode_kodevoucher']		= 'Kode Voucher';
$lang['form_nama_kodevoucher']		= 'Nama Voucher';
$lang['form_ket_kodevoucher']		= 'Ket. Voucher';
/* End 0f Kode voucher pembayaran */

/* Satuan Bahan Baku */
$lang['page_title_satuan']		= 'SATUAN SATUAN BHN BAKU';
$lang['form_panel_daftar_satuan']	= 'DAFTAR SATUAN BHN BAKU';
$lang['form_panel_form_satuan']	= 'FORM SATUAN BHN BAKU';
$lang['form_panel_cari_satuan']	= 'CARI SATUAN BHN BAKU';
$lang['form_kode_satuan']		= 'Satuan';
/* End 0f Satuan Bahan Baku */

?>
