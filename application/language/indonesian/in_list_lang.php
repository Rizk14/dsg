<?php
/* Stok Saat Ini */
$lang['form_title_detail_stoksaatini']	= 'DETAIL BARANG';
//$lang['page_title_stoksaatini']	= 'Stok Saat Ini';
$lang['page_title_stoksaatini']	= 'Laporan Data Stok';
//$lang['list_stoksaatini_tgl_do_mulai']	= 'DO Mulai Tgl. ';
$lang['list_stoksaatini_tgl_do_mulai']	= ' Mulai Tgl. ';
$lang['list_stoksaatini_s_produk']	= 'Stop Produksi';
$lang['list_stoksaatini_j_produk']	= 'Jenis Produksi';
$lang['list_stoksaatini_kd_brg']	= 'Kode Barang';
$lang['list_stoksaatini_nm_brg']	= 'Nama Barang';
$lang['list_stoksaatini_s_awal']	= 'Saldo Awal';
$lang['list_stoksaatini_s_akhir']	= 'Saldo Akhir';
$lang['list_stoksaatini_bmm']	= 'Bon M Masuk';
$lang['list_stoksaatini_bmk']	= 'Bon M Keluar';
$lang['list_stoksaatini_bbm']	= 'BBM';
$lang['list_stoksaatini_bbk']	= 'BBK';
$lang['list_stoksaatini_do']	= 'DO';
$lang['list_stoksaatini_sj']	= 'SJ';
/* End 0f Stok Saat Ini */

/* Laporan Data Stok */
/* 10082011
$lang['page_title_laporansok']	= 'Laporan Data Stok Barang';
$lang['form_title_detail_laporansok']	= 'DETAIL BARANG';
$lang['page_title_laporansok']	= 'Laporan Data Stok';
$lang['list_laporansok_kd_brg']	= 'KODE BRG';
$lang['list_laporansok_nm_brg']	= 'NAMA BARANG';
$lang['list_laporansok_awalstok_brg']	= 'STOK AWAL';
$lang['list_laporansok_akhirstok_brg']	= 'STOK BRG';
$lang['list_laporansok_ket_brg']	= 'KETERANGAN';
*/
$lang['page_title_laporansok']	= 'Stok saat ini';
$lang['form_title_detail_laporansok']	= 'DETAIL BARANG';
$lang['page_title_laporansok']	= 'Stok saat ini';
$lang['list_laporansok_kd_brg']	= 'KODE BRG';
$lang['list_laporansok_nm_brg']	= 'NAMA BARANG';
$lang['list_laporansok_awalstok_brg']	= 'SALDO ';
$lang['list_laporansok_akhirstok_brg']	= 'SALDO TERAKHIR';
$lang['list_laporansok_ket_brg']	= 'KET. KOREKSI';
//$lang['list_laporansok_sop_brg']	= 'S.O.P';
$lang['list_laporansok_sop_brg']	= 'S.O';
$lang['list_laporansok_saldoakhirbln_brg']	= 'SALDO AKHIR BLN';
/* End 0f Laporan Data Stok */

/* Daftar Brg belum dikirim */
$lang['form_title_detail_pendding']	= 'DETAIL BARANG';
$lang['page_title_pendding']	= 'Info Barang Belum Dikirim';
$lang['list_pend_tgl_mulai_op']	= 'OP Mulai Tgl.';
$lang['list_pend_sort_brg']	= 'Tampilkan Daftar Barang Berdasarkan';
$lang['list_pend_sort_dftr_brg']	= 'Daftar Per';
$lang['list_pend_tgl_op']	= 'TANGGAL OP';
$lang['list_pend_no_op']	= 'NOMOR OP';
$lang['list_pend_cabang']	= 'CABANG';
$lang['list_pend_kd_brg']	= 'KODE BARANG';
$lang['list_pend_nm_brg']	= 'NAMA BARANG';
$lang['list_pend_do']	= 'DO';
$lang['list_pend_sisa']	= 'SISA';
$lang['list_pend_amount']	= 'AMOUNT';
/* End 0f Daftar Brg belum dikirim */

/* Info OP vs DO */
$lang['form_title_detail_opvsdo']	= 'DETAIL BARANG';
$lang['page_title_opvsdo']	= 'Info OP vs DO';
$lang['list_opvsdo_kd_brg']	= 'Kode Barang';
$lang['list_opvsdo_tgl_mulai_op']	= 'OP Mulai Tgl.';
$lang['list_opvsdo_stop_produk']	= 'STP';
$lang['list_opvsdo_t_permintaan']	= 'Total Permintaan';
$lang['list_opvsdo_t_pengiriman']	= 'Total Pengiriman';
$lang['list_opvsdo_n_permintaan']	= 'Nilai Permintaan ';
$lang['list_opvsdo_n_penjualan']	= 'Nilai Penjualan';
$lang['list_opvsdo_selisih']	= 'Selisih';
$lang['list_opvsdo_t_n_selisih']	= 'Total Nilai Selisih';
$lang['list_opvsdo_nm_brg']	= 'NAMA BRG';
$lang['list_opvsdo_unit_price']	= 'HJP';
$lang['list_opvsdo_op']	= 'OP';
$lang['list_opvsdo_n_op']	= 'NILAI OP';
$lang['list_opvsdo_do']	= 'DO';
$lang['list_opvsdo_n_do']	= 'NILAI DO';
$lang['list_opvsdo_selisih_opdo']	= 'SELISIH OP DO';
$lang['list_opvsdo_n_selisih']	= 'NILAI SELISIH';
/* End 0f OP vs DO */

/* Detail Info DO - OP */
$lang['page_title_detailopdo']	= "Detail Info OP - DO ";
$lang['form_title_detail_detailopdo']	= 'DETAIL OP-DO';
/* End 0f Detail Indo DO - OP */

/* List Barang Jadi */
$lang['form_title_detail_brgjadi']	= 'DETAIL MOTIF BARANG';
$lang['page_title_brgjadi']	= 'Lihat Barang Jadi';
$lang['list_brgjadi_kd_brg'] = 'Kode Barang';
$lang['list_s_produksi'] = 'Stop Produksi';
$lang['list_brgjadi_nm_brg'] = 'Nama Barang';
$lang['list_brgjadi_tgl_penawaran']	= 'Tgl. Penawaran';
$lang['list_brgjadi_kategori'] = 'Kategori';
$lang['list_brgjadi_no_penawaran'] = 'No. Penawaran';
$lang['list_brgjadi_merek'] = 'Merek';
$lang['list_brgjadi_hjp'] = 'HJP';
$lang['list_brgjadi_layout'] = 'Layout';
$lang['list_brgjadi_status'] = 'Status';
$lang['list_brgjadi_qty'] = 'Quality';
$lang['list_brgjadi_kd_motif'] = 'KODE MOTIF';
$lang['list_brgjadi_nm_motif']	= 'NAMA BARANG + MOTIF';
$lang['list_brgjadi_stok']	= 'STOK';
/* End 0f Barang Jadi */

/* List Penjualan Berdasarkan DO */
$lang['page_title_penjualanperdo']	= 'Info Penjualan Berdasarkan DO ';
$lang['form_title_detail_penjualanperdo']	= 'DETAIL PENJUALAN';
$lang['list_penjperdo_kd_brg']	= 'Kode Barang';
$lang['list_penjperdo_no_faktur']	= 'Nomor Faktur';
$lang['list_penjperdo_tgl_mulai']	= 'DO Mulai Tgl.';
$lang['list_penjperdo_tgl_faktur_mulai']	= 'Faktur Mulai Tgl.';
$lang['list_penjperdo_no_do']	= 'NOMOR DO';
$lang['list_penjperdo_nm_brg']	= 'NAMA BARANG';
$lang['list_penjperdo_qty']	= 'JUMLAH';
$lang['list_penjperdo_hjp']	= 'HJP';
$lang['list_penjperdo_amount']	= 'AMOUNT';
$lang['list_penjperdo_total_pengiriman']	= 'Total Pengiriman';
$lang['list_penjperdo_total_penjualan']	= 'Total Penjualan';
/* End 0f List Penjualan Berdasarkan DO */

/* List Penjualan Non DO */
$lang['page_title_penjualanndo']	= 'Info Penjualan Barang Non DO';
$lang['form_title_detail_penjualanndo']	= 'DETAIL PENJUALAN';

$lang['list_penjualanndo_kd_brg']	= 'Kode Barang';
$lang['list_penjualanndo_no_faktur']	= 'Nomor Faktur';
$lang['list_penjualanndo_tgl_faktur_mulai']	= 'Faktur Mulai Tgl.';
$lang['list_penjualanndo_jenis_brg']	= 'Jenis Produksi';
$lang['list_penjualanndo_s_produksi'] = 'Stop Produksi';
$lang['list_penjualanndo_no_do']	= 'NOMOR SJ';
$lang['list_penjualanndo_nm_brg']	= 'NAMA BARANG';
$lang['list_penjualanndo_qty']	= 'JUMLAH';
$lang['list_penjualanndo_hjp']	= 'HJP';
$lang['list_penjualanndo_amount']	= 'AMOUNT';
$lang['list_penjualanndo_vdiscount']	= 'DISC %';
$lang['list_penjualanndo_ndiscount']	= 'DISC Rp.';
$lang['list_penjualanndo_total_pengiriman']	= 'Total Pengiriman';
$lang['list_penjualanndo_total_penjualan']	= 'Total Penjualan';
/* End 0f List Penjualan Non DO */

/* List Penjualan Tertentu */
$lang['page_title_penjualantertentu']	= 'Info Penjualan Barang Tertentu';
$lang['form_title_detail_penjualantertentu']	= 'DETAIL PENJUALAN';

$lang['list_penjualantertentu_kd_brg']	= 'Kode Barang';
$lang['list_penjualantertentu_no_faktur']	= 'Nomor Faktur';
$lang['list_penjualantertentu_jenis_brg']	= 'Jenis Produksi';
$lang['list_penjualantertentu_s_produksi'] = 'Stop Produksi';
$lang['list_penjualantertentu_no_do']	= 'NOMOR DO';
$lang['list_penjualantertentu_nm_brg']	= 'NAMA BARANG';
$lang['list_penjualantertentu_qty']	= 'JUMLAH';
$lang['list_penjualantertentu_hjp']	= 'HJP';
$lang['list_penjualantertentu_amount']	= 'AMOUNT';
$lang['list_penjualantertentu_total_pengiriman']	= 'Total Pengiriman';
$lang['list_penjualantertentu_total_penjualan']	= 'Total Penjualan';
/* End 0f List Penjualan Tertentu */

/* Export Faktur Pajak Penjualan berdasarkan DO */
$lang['list_fpenjperdo_tgl_mulai_faktur']	= 'Faktur Mulai Tgl.';
$lang['list_fpenjperdo_no_faktur']		= 'No. Faktur';
$lang['list_fpenjperdo_no_pajak']		= 'No. Pajak';
$lang['list_fpenjperdo_nm_pelanggan']	= 'Nama Pelanggan';
$lang['list_fpenjperdo_npwp']			= 'NPWP';
$lang['list_fpenjperdo_tgl_faktur']		= 'Tgl. Faktur';
$lang['list_fpenjperdo_tgl_bts_kirim']	= 'Tgl. Bts Kirim';
$lang['list_fpenjperdo_discount']		= 'Disc (%)';
$lang['list_fpenjperdo_total_faktur']	= 'Total Nilai';
$lang['list_fpenjperdo_grand_total']	= 'Total stlh PPN+Disc';
/* End 0f Export Faktur Pajak Penjualan berdasarkan DO */

/* Export Faktur Pajak Penjualan Non DO */
$lang['list_fpenjualanndo_tgl_mulai_faktur']= 'Faktur Mulai Tgl.';
$lang['list_fpenjualanndo_no_faktur']		= 'No. Faktur';
$lang['list_fpenjualanndo_no_pajak']		= 'No. Pajak';
$lang['list_fpenjualanndo_nm_pelanggan']	= 'Nama Pelanggan';
$lang['list_fpenjualanndo_npwp']			= 'NPWP';
$lang['list_fpenjualanndo_tgl_faktur']		= 'Tgl. Faktur';
$lang['list_fpenjualanndo_tgl_bts_kirim']	= 'Tgl. Bts Kirim';
$lang['list_fpenjualanndo_discount']		= 'Disc (%)';
$lang['list_fpenjualanndo_total_faktur']	= 'Total Nilai';
$lang['list_fpenjualanndo_grand_total']		= 'Total stlh PPN+Disc';
/* End 0f Export Faktur Pajak Penjualan Non DO */

/* Info OP */
$lang['page_title_op']	= 'Info Order Pembelian (OP) ';
$lang['form_title_detail_op']	= 'DETAIL ORDER PEMBELIAN (OP) ';
$lang['list_tgl_mulai_op']	= 'OP Mulai Tgl.';
$lang['list_tgl_akhir_op']	= 's.d';
$lang['list_no_op']	= 'Nomor OP';
$lang['list_nm_cab_op']	= 'Cabang';
$lang['list_kd_brg_op']	= 'Kode Barang';
$lang['list_nm_brg_op']	= 'Nama Barang';
$lang['list_qty_op']	= 'Qty';
$lang['list_sisa_order_op']	= 'Sisa Order';
$lang['list_limit_date_op']	= 'Batas Pengiriman';
$lang['list_no_sop_op']	= 'Nomor SOP';
/* End 0f OP */

/* Info DO */
$lang['page_title_do']	= 'Info Delivery Order (DO) ';
$lang['form_title_detail_do']	= 'DETAIL DELIVERY ORDER (DO) ';
$lang['list_tgl_mulai_do']	= 'DO Mulai Tgl.';
$lang['list_tgl_akhir_do']	= 's.d';
$lang['list_no_do']	= 'Nomor DO';
$lang['list_no_op_do']	= 'Nomor OP';
$lang['list_nm_cab_do']	= 'Cabang';
$lang['list_kd_brg_do']	= 'Kode Barang';
$lang['list_nm_brg_do']	= 'Nama Barang';
$lang['list_qty_do']	= 'DO';
$lang['list_sisa_delivery_do']	= 'Belum Faktur';
/* End 0f DO */

/* List Bon M Keluar */
$lang['page_title_outbonm']	= "Bon M Keluar";
$lang['list_tgl_mulai_outbonm']	= "Bon M Keluar Mulai Tgl";
$lang['form_title_detail_outbonm']	= "Detail";
$lang['list_no_outbonm']	= "No. Bon M Keluar";
$lang['list_tgl_outbonm']	= "Tgl Bon M Keluar";
$lang['list_kd_brg_outbonm']	= "Kode Barang";
$lang['list_nm_brg_outbonm']	= "Nama Barang";
$lang['list_qty_brg_outbonm']	= "Qty";
/* End 0f Bon M Keluar */

/* List Bon M Masuk */
$lang['page_title_inbonm']	= "Bon M Masuk";
$lang['list_tgl_mulai_inbonm']	= "Bon M Masuk Mulai Tgl";
$lang['form_title_detail_inbonm']	= "Detail";
$lang['list_no_inbonm']	= "No. Bon M Masuk";
$lang['list_no_insj']	= "No. SJ (Surat Jalan)";
$lang['list_tgl_inbonm']	= "Tgl Bon M Masuk";
$lang['list_kd_brg_inbonm']	= "Kode Barang";
$lang['list_nm_brg_inbonm']	= "Nama Barang";
$lang['list_qty_brg_inbonm']	= "Qty";
/* End 0f Bon M Masuk */

/* List BBM */
$lang['page_title_bbm']	= "Bukti Barang Masuk";
$lang['list_tgl_mulai_bbm']	= "BBM Mulai Tgl";
$lang['form_title_detail_bbm']	= "Detail";
$lang['list_no_bbm']	= "Nomor BBM";
$lang['list_from_bbm']	= "Dari";
$lang['list_tgl_bbm']	= "Tgl BBM";
$lang['list_kd_brg_bbm']	= "Kode Barang";
$lang['list_nm_brg_bbm']	= "Nama Barang";
$lang['list_unit_bbm']	= "Unit";
$lang['list_hjp_bbm']	= "HJP";
$lang['list_harga_bbm']	= "Total";
/* End 0f BBM */

/* List BBK */
$lang['page_title_bbk']	= "Bukti Barang Keluar";
$lang['list_tgl_mulai_bbk']	= "BBK Mulai Tgl";
$lang['form_title_detail_bbk']	= "Detail";
$lang['list_no_bbk']	= "Nomor BBK";
$lang['list_to_bbk']	= "Kepada";
$lang['list_tgl_bbk']	= "Tgl BBK";
$lang['list_kd_brg_bbk']	= "Kode Barang";
$lang['list_nm_brg_bbk']	= "Nama Barang";
$lang['list_unit_bbk']	= "Unit";
$lang['list_hjp_bbk']	= "HJP";
$lang['list_harga_bbk']	= "Total";
/* End 0f BBK */

/* List SJ */
$lang['page_title_sj']	= "Surat Jalan (SJ)";
$lang['list_tgl_mulai_sj']	= "SJ Mulai Tgl";
$lang['form_title_detail_sj']	= "Detail";
$lang['list_no_sj']	= "Nomor SJ";
$lang['list_tgl_sj']	= "Tgl SJ";
$lang['list_kd_brg_sj']	= "Kode Barang";
$lang['list_nm_brg_sj']	= "Nama Barang";
$lang['list_unit_sj']	= "Unit";
$lang['list_unit_belumfaktur_sj']	= "Belum Faktur";
$lang['list_hjp_sj']	= "HJP";
$lang['list_harga_sj']	= "Total";
/* End 0f SJ */

/* List Print Faktur Penjualan Berdasarkan DO */
$lang['page_title_fpenjualanperdo']	= 'Info Penjualan Berdasarkan DO ';
$lang['form_title_detail_fpenjualanperdo']	= 'DETAIL PENJUALAN';
$lang['list_fpenjperno_faktur']	= 'Nomor Faktur';
$lang['list_fpenjperdo_no_do']	= 'Nomor DO';
$lang['list_fpenjperdo_kd_brg']	= 'Kode Barang';
$lang['list_fpenjperdo_tgl_mulai']	= 'DO Mulai Tgl.';
$lang['list_fpenjperdo_nm_brg']	= 'NAMA BARANG';
$lang['list_fpenjperdo_qty']	= 'JUMLAH';
$lang['list_fpenjperdo_hjp']	= 'HJP';
$lang['list_fpenjperdo_amount']	= 'AMOUNT';
$lang['list_fpenjperdo_total_pengiriman']	= 'Total Pengiriman';
$lang['list_fpenjperdo_total_penjualan']	= 'Total Penjualan';
/* End 0f List Faktur Penjualan Berdasarkan DO */

/* List Print Faktur Penjualan Tertentu */
$lang['page_title_fpenjualanndo']	= 'Info Penjualan Barang Tertentu/ Non DO';
$lang['form_title_detail_fpenjualanndo']	= 'DETAIL PENJUALAN';
$lang['list_fpenjualanndo_faktur']	= 'Nomor Faktur';
$lang['list_fpenjualanndo_kd_brg']	= 'Kode Barang';
$lang['list_fpenjualanndo_jenis_brg']	= 'Jenis Produksi';
$lang['list_fpenjualanndo_s_produksi'] = 'Stop Produksi';
$lang['list_fpenjualanndo_no_do']	= 'NOMOR DO';
$lang['list_fpenjualanndo_nm_brg']	= 'NAMA BARANG';
$lang['list_fpenjualanndo_qty']	= 'JUMLAH';
$lang['list_fpenjualanndo_hjp']	= 'HJP';
$lang['list_fpenjualanndo_amount']	= 'AMOUNT';
$lang['list_fpenjualanndo_total_pengiriman']	= 'Total Pengiriman';
$lang['list_fpenjualanndo_total_penjualan']	= 'Total Penjualan';
/* End 0f List Faktur Penjualan Tententu */

/* List Print Faktur Penjualan Bahan Baku */
$lang['page_title_fpenjualanbhnbaku']	= 'Info Penjualan Barang Bahan Baku';
$lang['form_title_detail_fpenjualanbhnbaku']	= 'DETAIL PENJUALAN';
$lang['list_fpenjualanbhnbaku_faktur']	= 'Nomor Faktur';
//$lang['list_fpenjualanbhnbaku_kd_brg']	= 'Kode Barang';
$lang['list_fpenjualanbhnbaku_kd_brg']	= 'Kode Urut';
$lang['list_fpenjualanbhnbaku_jenis_brg']	= 'Jenis Produksi';
$lang['list_fpenjualanbhnbaku_s_produksi'] = 'Stop Produksi';
$lang['list_fpenjualanbhnbaku_nm_brg']	= 'NAMA BARANG';
$lang['list_fpenjualanbhnbaku_qty']	= 'JUMLAH';
$lang['list_fpenjualanbhnbaku_satuan']	= 'SATUAN';
$lang['list_fpenjualanbhnbaku_hjp']	= 'HARGA';
$lang['list_fpenjualanbhnbaku_amount']	= 'AMOUNT';
$lang['list_fpenjualanbhnbaku_total_pengiriman']	= 'Total Pengiriman';
$lang['list_fpenjualanbhnbaku_total_penjualan']	= 'Total Penjualan';
/* End 0f List Faktur Penjualan Bahan Baku */

/* List Print Faktur Pajak Penjualan Berdasarkan DO */
$lang['page_title_pjkpenjualanperdo']	= 'Info Penjualan Berdasarkan DO ';
$lang['form_title_detail_pjkpenjualanperdo']	= 'DETAIL PENJUALAN';
$lang['list_pjkpenjperno_faktur']	= 'Nomor Faktur';
$lang['list_pjkpenjperdo_no_do']	= 'Nomor DO';
$lang['list_pjkpenjperdo_kd_brg']	= 'Kode Barang';
$lang['list_pjkpenjperdo_tgl_mulai']	= 'DO Mulai Tgl.';
$lang['list_pjkpenjperdo_nm_brg']	= 'NAMA BARANG';
$lang['list_pjkpenjperdo_qty']	= 'JUMLAH';
$lang['list_pjkpenjperdo_hjp']	= 'HJP';
$lang['list_pjkpenjperdo_amount']	= 'AMOUNT';
$lang['list_pjkpenjperdo_total_pengiriman']	= 'Total Pengiriman';
$lang['list_pjkpenjperdo_total_penjualan']	= 'Total Penjualan';
/* End 0f List Faktur Pajak Penjualan Berdasarkan DO */

/* List Print Faktur Pjk Penjualan Tertentu */
$lang['page_title_fpajakndo']	= 'Info Penjualan Barang Tertentu/ Non DO';
$lang['form_title_detail_fpajakndo']	= 'DETAIL PENJUALAN';
$lang['list_fpajakndo_faktur']	= 'Nomor Faktur';
$lang['list_fpajakndo_kd_brg']	= 'Kode Barang';
$lang['list_fpajakndo_jenis_brg']	= 'Jenis Produksi';
$lang['list_fpajakndo_s_produksi'] = 'Stop Produksi';
$lang['list_fpajakndo_no_do']	= 'NOMOR DO';
$lang['list_fpajakndo_nm_brg']	= 'NAMA BARANG';
$lang['list_fpajakndo_qty']	= 'JUMLAH';
$lang['list_fpajakndo_hjp']	= 'HJP';
$lang['list_fpajakndo_amount']	= 'AMOUNT';
$lang['list_fpajakndo_total_pengiriman']	= 'Total Pengiriman';
$lang['list_fpajakndo_total_penjualan']	= 'Total Penjualan';
/* End 0f List Faktur Pjk Penjualan Tententu */

/* List Print Faktur Pjk Penjualan Bahan Baku */
$lang['page_title_fpajakbhnbaku']	= 'Info Penjualan Barang Bahan Baku';
$lang['form_title_detail_fpajakbhnbaku']	= 'DETAIL PENJUALAN';
$lang['list_fpajakbhnbaku_faktur']	= 'Nomor Faktur';
//$lang['list_fpajakbhnbaku_kd_brg']	= 'Kode Barang';
$lang['list_fpajakbhnbaku_kd_brg']	= 'Kode Urut';
$lang['list_fpajakbhnbaku_jenis_brg']	= 'Jenis Produksi';
$lang['list_fpajakbhnbaku_s_produksi'] = 'Stop Produksi';
$lang['list_fpajakbhnbaku_nm_brg']	= 'NAMA BARANG';
$lang['list_fpajakbhnbaku_qty']	= 'JUMLAH';
$lang['list_fpajakbhnbaku_satuan']	= 'SATUAN';
$lang['list_fpajakbhnbaku_hjp']	= 'HARGA';
$lang['list_fpajakbhnbaku_amount']	= 'AMOUNT';
$lang['list_fpajakbhnbaku_total_pengiriman']	= 'Total Pengiriman';
$lang['list_fpajakbhnbaku_total_penjualan']	= 'Total Penjualan';
/* End 0f List Faktur Pjk Penjualan Bahan Baku */

/* List Barang Mentah */
$lang['page_title_brgmentah']	= 'Barang Mentah';
$lang['form_title_detail_brgmentah']	= 'BARANG MENTAH';
$lang['page_title_brgmentah']	= 'Lihat Barang Mentah';
$lang['list_brgmentah_kd_brg'] = 'Kode Barang';
$lang['list_brgmentah_nm_brg'] = 'Nama Barang';
$lang['list_brgmentah_qty'] = 'Qty';
$lang['list_brgmentah_color'] = 'Warna';
$lang['list_brgmentah_unit'] = 'Satuan';
$lang['list_brgmentah_meter'] = 'dlm Meter';
$lang['list_brgmentah_price'] = 'Harga';
/* End 0f Barang Mentah */

/* List Bon M Masuk Brg Mentah */
$lang['page_title_inbonmbrgmentah']	= "Bon M Masuk";
$lang['list_tgl_mulai_inbonmbrgmentah']	= "Bon M Masuk Mulai Tgl";
$lang['form_title_detail_inbonmbrgmentah']	= "Detail";
$lang['list_no_inbonmbrgmentah']	= "No. Bon M Masuk";
$lang['list_tgl_inbonmbrgmentah']	= "Tgl Bon M Masuk";
$lang['list_kd_brg_inbonmbrgmentah']	= "Kode Barang";
$lang['list_nm_brg_inbonmbrgmentah']	= "Nama Barang";
$lang['list_qty_brg_inbonmbrgmentah']	= "Qty";
/* End 0f Bon M Masuk Brg Mentah  */

/* List File Laporan */
$lang['page_title_file_laporan']	= "Direktori Laporan";
$lang['list_no_file_laporan']		= "No";
$lang['list_nm_file_laporan']		= "File Laporan";
/* End 0f List File Laporan */

/* Transfer DO */
$lang['page_title_transfer_do']	= 'Transfer DO';
$lang['form_title_transfer_do']	= 'Transfer DO';
$lang['list_tgl_mulai_do_transfer_do']		= 'Do Mulai Tgl.';
$lang['list_tgl_akhir_do_transfer_do']		= 's.d';
$lang['list_transfer_do_stop_produk']	= 'STP';
/* End 0f Transfer DO */

/* Penyetor */
$lang['page_title_penyetor']			= 'Penyetor';
$lang['form_nama_penyetor']		= 'Nama Penyetor';
$lang['form_panel_daftar_penyetor']	= 'DAFTAR PENYETOR';
$lang['form_form_penyetor']	= 'FORM PENYETOR';
$lang['form_cari_penyetor']	= 'CARI PENYETOR';
/* End 0f Penyetor */

/* Laporan SSP */
$lang['page_title_spp']	= 'Surat Setoran Pajak';
$lang['form_title_detail_ssp']	= 'DETAIL SSP';
$lang['list_laporansok_nmwp_ssp']	= 'Nama WP';
$lang['list_laporansok_akunpajak_ssp']	= 'Akun Pajak';
$lang['list_laporansok_jns_setor_spp']	= 'Jenis Setoran';
$lang['list_laporansok_urai_spp']	= 'Uraian';
$lang['list_laporansok_jmlbayar_spp']	= 'Jml Bayar';
$lang['list_laporansok_penyetor_ssp']	= 'Penyetor';
/* End 0f Laporan SSP */

/* Laporan Transfer DO */
$lang['page_title_listtrasferdo']	= 'Informasi Transfer DO';
$lang['form_title_detail_listtrasferdo']	= 'DETAIL TRANSFER DO';
$lang['list_kodeop_listtrasferdo']	= 'Nomor OP';
$lang['list_tglop_listtrasferdo']	= 'Tgl. OP';
$lang['list_kodedo_listtrasferdo']	= 'Nomor DO';
$lang['list_tgldo_listtrasferdo']	= 'Tgl. DO';
$lang['list_area_listtrasferdo']	= 'Area';
$lang['list_kodebrg_listtrasferdo']	= 'Kode Barang';
$lang['list_nmbrg_listtrasferdo']	= 'Nama Barang';
$lang['list_jml_listtrasferdo']	= 'Jml DO';
/* End 0f Laporan Transfer DO */

/* List Forcast */
$lang['page_title_listforcast']		= "FORCAST";
$lang['list_tgl_mulai_listforcast']	= "Forcast Mulai Tgl";
$lang['form_title_detail_listforcast']	= "Detail";
$lang['list_no_listforcast']		= "No. Forcast";
$lang['list_tgl_listforcast']		= "Tgl Forcast";
$lang['list_op_listforcast']		= "No. OP";
$lang['list_kd_brg_listforcast']	= "Kode Barang";
$lang['list_nm_brg_listforcast']	= "Nama Barang";
$lang['list_qty_brg_listforcast']	= "Qty";
/* End 0f Forcast */

/* List SJ Drop Forcast */
$lang['page_title_listsjdropforcast']		= "SJ DROP FORCAST";
$lang['list_tgl_mulai_listsjdropforcast']	= "SJ Mulai Tgl";
$lang['form_title_detail_listsjdropforcast']	= "Detail";
$lang['list_no_listsjdropforcast']		= "No. SJ";
$lang['list_tgl_listsjdropforcast']		= "Tgl SJ";
$lang['list_op_listsjdropforcast']		= "Drop Forcast/ OP";
$lang['list_kd_brg_listsjdropforcast']	= "Kode Barang";
$lang['list_nm_brg_listsjdropforcast']	= "Nama Barang";
$lang['list_qty_brg_listsjdropforcast']	= "Qty SJ";
$lang['list_sisa_forcast_brg_listsjdropforcast']= "Sisa Forcast";
/* End 0f SJ Drop Forcast */

/* Expo Forcast */
$lang['form_title_detail_expo_forcast']	= 'DETAIL FORCAST';
$lang['page_title_expo_forcast']	= 'Info Forcast';
$lang['list_no_expo_forcast']= 'No';
$lang['list_no_forcast_expo_forcast']	= 'No Forcast';
$lang['list_drop_forcast_expo_forcast']	= 'No Drop Forcast';
$lang['list_tgl_forcast_expo_forcast']	= 'Tgl Forcast';
$lang['list_kd_brg_expo_forcast']	= 'Kode Barang';
$lang['list_nm_brg_expo_forcast']	= 'Nama Barang';
$lang['list_jml_forcast_expo_forcast']	= 'Forcast';
$lang['list_no_sj_expo_forcast']	= 'No SJ';
$lang['list_tgl_sj_expo_forcast']	= 'Tgl SJ';
$lang['list_jml_sj_expo_forcast']	= 'SJ';
$lang['list_sisa_forcast_expo_forcast']	= 'Sisa Forcast';
/* End 0f Expo Forcast */

/* Expo SJ Drop Forcast */
$lang['form_title_detail_expo_sjdforcast']	= 'DETAIL SJ DROP FORCAST';
$lang['page_title_expo_sjdforcast']		= 'Info SJ Drop Forcast';
$lang['list_no_expo_sjdforcast']	= 'No';
$lang['list_no_forcast_expo_sjdforcast']	= 'No Forcast';
$lang['list_drop_forcast_expo_sjdforcast']	= 'No Drop Forcast';
$lang['list_tgl_forcast_expo_sjdforcast']	= 'Tgl Forcast';
$lang['list_tgl_dforcast_expo_sjdforcast']	= 'Tgl Drop Forcast';
$lang['list_kd_brg_expo_sjdforcast']	= 'Kode Barang';
$lang['list_nm_brg_expo_sjdforcast']	= 'Nama Barang';
$lang['list_jml_forcast_expo_sjdforcast']	= 'Forcast';
$lang['list_no_sj_expo_sjdforcast']	= 'No SJ';
$lang['list_tgl_sj_expo_sjdforcast']	= 'Tgl SJ';
$lang['list_jml_sj_expo_sjdforcast']	= 'SJ';
/* End 0f Expo SJ Drop Forcast */

/* Kontrak Bon/ Daftar Tagihan */
/*** Desain Planning : 
 * NO | NOMOR FAKTUR | TANGGAL FAKTUR | TANGGAL JATUH TEMPO |PELANGGAN CABANG | JUMLAH TAGIHAN
 * ***/

$lang['page_title_kontrabon']	= 'Info Kontra Bon';
$lang['form_title_detail_kontrabon']= 'DETAIL KONTRA BON';
$lang['list_kontrabon_no_faktur']	= 'Nomor Faktur';
$lang['list_kontrabon_tgl_faktur']	= 'Tgl. Faktur';
$lang['list_kontrabon_tgl_kontrabon']	= 'Tgl. Kontra Bon';
$lang['list_kontrabon_tgl_tempo']	= 'Tgl. Jatuh Tempo';
$lang['list_kontrabon_kd_brg']	= 'KODE BARANG';
$lang['list_kontrabon_nm_brg']	= 'NAMA BARANG';
$lang['list_kontrabon_pelanggan']	= 'PELANGGAN';
$lang['list_kontrabon_jml_tagih']	= 'JUMLAH TAGIHAN';
$lang['list_kontrabon_total_tagih']	= 'TOTAL TAGIHAN';

$lang['list_kontrabon_tgl_kontrabon'] = 'Tgl. Kontra Bon';
$lang['list_kontrabon_nilai'] = 'Total Nilai';
$lang['list_kontrabon_discount'] = 'Discount';
$lang['list_kontrabon_ppn'] = 'PPN';
$lang['list_kontrabon_status_pelunasan'] = 'Pelunasan';
$lang['list_kontrabon_no_kontrabon'] = 'No Kontra Bon';

$lang['list_kontrabon_nota_sederhana'] = 'Faktur Non DO';
/* End 0f Kontrak Bon/ Daftar Tagihan */

/* Pelpen */
$lang['list_voucher_no_faktur']	= 'Nomor Faktur Penjualan';
$lang['list_voucher_tgl_faktur']	= 'Tgl Faktur Penjualan';
$lang['list_voucher_total_faktur']	= 'Total Faktur Penjualan';
$lang['list_voucher_nm_pelanggan']	= 'Nama Pelanggan';
/* End 0f Pelpen */

/* Pelunasan */
$lang['list_pelunasan_no_faktur']	= 'Nomor Faktur Penjualan';
$lang['list_pelunasan_sumber_faktur']	= 'List Pelunasan Sumber Faktur';
$lang['list_faktur_tgl_faktur']	= 'Tanggal Voucher';
/* End 0f Pelunasan */

/* Voucher */
$lang['page_title_voucher']			= 'Info Voucher';
$lang['form_title_detail_voucher']	= 'DETAIL VOUCHER';
$lang['list_voucher_no_kontrabon']	= 'Nomor Kontra Bon';
$lang['list_voucher_tgl_kontrabon']	= 'Tgl Kontra Bon';
$lang['list_voucher_total_kontrabon']	= 'Total Kontra Bon';
$lang['list_voucher_nilai_voucher_detail']	= 'Nilai Voucher';
$lang['list_voucher_piutang']		= 'Piutang';
$lang['list_voucher_tgl_faktur']	= 'Tgl. Faktur';
$lang['list_voucher_kd_sumber']		= 'Kode Sumber';
$lang['list_voucher_no']			= 'No. Voucher';
$lang['list_voucher_tgl']			= 'Tgl. Voucher';
$lang['list_voucher_kd_perusahaan']	= 'Kode Perusahaan';
$lang['list_voucher_deskripsi']		= 'Keterangan Voucher';
$lang['list_voucher_total']			= 'Total Voucher';
$lang['list_voucher_received']		= 'Penerima';
$lang['list_voucher_approved']		= 'Diapprove Oleh';
$lang['list_voucher_approved_dept']	= 'Departement';
$lang['list_voucher_approved_serv']	= 'Services';
$lang['list_voucher_manual_input']	= 'Input ';
$lang['list_voucher_status_approve']= 'Status Approv';
$lang['list_voucher_status_cek']	= 'Status Cek';
$lang['list_voucher_total_nilai']	= 'Total Nilai';
/* End 0f Voucher */

/* List Piutang */
$lang['page_title_piutang']			= 'Info Piutang';
$lang['form_title_detail_piutang']	= 'DETAIL PIUTANG';
$lang['list_piutang_no_kontrabon']	= 'Nomor Kontra Bon';
$lang['list_piutang_tgl_kontrabon']	= 'Tgl. Kontra Bon';
$lang['list_piutang_nilai_piutang']	= 'Piutang';
$lang['list_piutang_total_kontrabon']	= 'Total Kontra Bon';
$lang['list_piutang_piutang']		= 'Piutang';
$lang['list_piutang_total_piutang']	= 'Total Piutang';
$lang['list_piutang_no_faktur']		= 'Nomor Faktur';
$lang['list_piutang_tgl_faktur']	= 'Tgl. Faktur';
$lang['list_piutang_due_date']		= 'Tgl. Jatuh Tempo';
$lang['list_piutang_pelanggan']		= 'Pelanggan';
$lang['list_piutang_nilai_faktur']	= 'Nilai Faktur';
$lang['list_piutang_total_faktur']	= 'Total Nilai Faktur';
$lang['list_piutang_nilai_kontrabon'] = 'Nilai Kontra Bon';
$lang['list_piutang_nilai_piutang']	= 'Nilai Piutang';
/* End 0f piutang */

/* List Pelunasan */
$lang['page_title_pelunasan']		= 'Info Pelunasan';
$lang['form_title_detail_pelunasan']= 'DETAIL PELUNASAN';
$lang['list_pelunasan_no_voucher']	= 'Nomor Voucher';
$lang['list_pelunasan_tgl_voucher']	= 'Tgl. Voucher'; // ATAU
$lang['list_pelunasan_tgl_pelunasan']	= 'Tgl. Pelunasan';
$lang['list_pelunasan_keterangan']	= 'Ket. Voucher';
$lang['list_pelunasan_recieve']		= 'Recieve';
$lang['list_pelunasan_approve']		= 'Approve';
$lang['list_pelunasan_piutang']		= 'Piutang';
$lang['list_pelunasan_total_voucher']	= 'Total Voucher';
$lang['list_pelunasan_nilai_voucher']	= 'Nilai Voucher';  // ATAU
$lang['list_pelunasan_nilai_pelunasan']	= 'Nilai Pelunasan';
$lang['list_pelunasan_no_kontrabon']	= 'Nomor Kontra Bon';
$lang['list_pelunasan_tgl_kontrabon']	= 'Tgl. Kontra Bon';
$lang['list_pelunasan_total_pelunasan']	= 'Total Pelunasan';
$lang['list_pelunasan_nilai_faktur']	= 'Nilai Faktur';
$lang['list_pelunasan_tgl_faktur']		= 'Tgl. Faktur';
$lang['list_pelunasan_pelangga_faktur']	= 'Pelanggan';
$lang['list_pelunasan_total_faktur']	= 'Total Nilai Faktur';
$lang['list_pelunasan_total_kontrabon'] = 'Total Nilai Kontra Bon';
$lang['list_pelunasan_sumber_kontrabon'] = 'Sumber Kontra Bon';
/* End 0f Pelunasan */


/* Tambahan 13122011 */
/* List Penjualan Bahan Baku */
$lang['page_title_penjualanbhnbaku']	= 'Info Penjualan Barang Bahan Baku';
$lang['form_title_detail_penjualanbhnbaku']	= 'DETAIL PENJUALAN';

$lang['list_penjualanbhnbaku_kd_brg']	= 'Kode Barang';
$lang['list_penjualanbhnbaku_no_faktur']	= 'Nomor Faktur';
$lang['list_penjualanbhnbaku_tgl_faktur_mulai']	= 'Faktur Mulai Tgl.';
$lang['list_penjualanbhnbaku_jenis_brg']	= 'Jenis Produksi';
$lang['list_penjualanbhnbaku_s_produksi'] = 'Stop Produksi';
$lang['list_penjualanbhnbaku_no_do']	= 'NOMOR SJ';
$lang['list_penjualanbhnbaku_nm_brg']	= 'NAMA BARANG';
$lang['list_penjualanbhnbaku_qty']	= 'JUMLAH';
$lang['list_penjualanbhnbaku_hjp']	= 'HARGA';
$lang['list_penjualanbhnbaku_amount']	= 'AMOUNT';
$lang['list_penjualanbhnbaku_total_pengiriman']	= 'Total Pengiriman';
$lang['list_penjualanbhnbaku_total_penjualan']	= 'Total Penjualan';


$lang['list_fpenjualanbhnbaku_tgl_mulai_faktur']= 'Faktur Mulai Tgl.';
$lang['list_fpenjualanbhnbaku_no_faktur']		= 'No. Faktur';
$lang['list_fpenjualanbhnbaku_no_pajak']		= 'No. Pajak';
$lang['list_fpenjualanbhnbaku_nm_pelanggan']	= 'Nama Pelanggan';
$lang['list_fpenjualanbhnbaku_npwp']			= 'NPWP';
$lang['list_fpenjualanbhnbaku_tgl_faktur']		= 'Tgl. Faktur';
$lang['list_fpenjualanbhnbaku_tgl_bts_kirim']	= 'Tgl. Bts Kirim';
$lang['list_fpenjualanbhnbaku_discount']		= 'Disc (%)';
$lang['list_fpenjualanbhnbaku_total_faktur']	= 'Total Nilai';
$lang['list_fpenjualanbhnbaku_grand_total']		= 'Total stlh PPN+Disc';
/* End 0f List Penjualan Bahan Baku */


/* SJ Penjualan Bhn Baku */
$lang['page_title_sjbhnbaku']	= "Surat Jalan (SJ)";
$lang['list_tgl_mulai_sjbhnbaku']	= "SJ Mulai Tgl";
$lang['form_title_detail_sjbhnbaku']	= "Detail";
$lang['list_no_sjbhnbaku']	= "Nomor SJ";
$lang['list_tgl_sjbhnbaku']	= "Tgl SJ";
$lang['list_kd_brg_sjbhnbaku']	= "Kode Urut";
$lang['list_nm_brg_sjbhnbaku']	= "Nama Barang";
$lang['list_unit_sjbhnbaku']	= "Unit";
$lang['list_unit_belumfaktur_sjbhnbaku']	= "Belum Faktur";
$lang['list_hjp_sjbhnbaku']	= "Harga";
$lang['list_harga_sjbhnbaku']	= "Total";
/* End 0f Penjualan Bhn Baku */

/* End 0f Tambahan */
?>
