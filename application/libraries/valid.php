<?php

class Valid extends CI_Controller {
	public $logged_in = false;

	function __construct() {
		$this->obj =& get_instance();
	}
	
	function login() {

		$main['tp_title']= $this->obj->lang->line('tp_title');
		$main['ft_log']	= $this->obj->lang->line('ft_login');
		$main['t_user']	= $this->obj->lang->line('t_username');
		$main['t_pswd']	= $this->obj->lang->line('t_passwd');
		$main['ts_log']	= $this->obj->lang->line('ts_login');
		$main['tr_reset']	= $this->obj->lang->line('tr_reset');
		$main['n_head']	= $this->obj->lang->line('n_header');
		$main['n_foot']	= $this->obj->lang->line('n_footer');	
		$main['dir']	= "js";

		$usernamex	= $this->obj->input->post('username',false);
		$passwordx	= $this->obj->input->post('password',false);
		
		if((!empty($usernamex) && !empty($passwordx)) ) {

			$ip		= $_SERVER['REMOTE_ADDR'];
			$paswd_enkri	= md5($passwordx);
			
			$qlogin	= $this->obj->db->query(" SELECT * FROM tm_user 
					WHERE e_user_name='".$usernamex."' AND e_password='".$paswd_enkri."' AND i_user_active='1' ", false);			

			if($qlogin->num_rows()>0) {
				
				$qdate	= $this->obj->db->query( " SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS timestamp0 " );
				
				$rlogin	= $qlogin->row();
				$rdate	= $qdate->row();
				
				$last_login	= $rdate->timestamp0;
				$i_user_id	= $rlogin->i_user_id;
				$e_user_name= $rlogin->e_user_name;
				$i_level	= $rlogin->i_level;

				$i_user_id_enkri	= md5($i_user_id);
				$e_user_name_enkri	= md5($e_user_name);
				
				$session_item = array(
				  'ses_user_id'=>$i_user_id_enkri,
				  'ses_user_name'=>$e_user_name_enkri,
				  'ses_level'=>$i_level,
				  'user_idx'=>$i_user_id	
				);
				
				$this->obj->session->set_userdata($session_item);
				
				/***
				$this->obj->session->set_userdata('ses_user_id', $i_user_id_enkri);
				$this->obj->session->set_userdata('ses_user_name', $e_user_name_enkri);
				$this->obj->session->set_userdata('ses_level', $i_level);
				***/

				/***
				session_start();
				
				session_register("ses_user_id");
				session_register("ses_user_name");
				session_register("ses_level");
							
				$_SESSION['ses_user_id']	= $i_user_id_enkri;
				$_SESSION['ses_user_name']	= $e_user_name_enkri;
				$_SESSION['ses_level']		= $i_level;
				
				if(session_is_registered('ses_user_id') && 
					session_is_registered('ses_user_name') && 
					session_is_registered('ses_level') ) {

					$qlogs	= $this->mclass->counter();
					if($qlogs->num_rows()>0) {
						$rlogs	= $qlogs->row();
						$ilogs	= $rlogs->isession;
					} else {
						$ilogs	= 1;
					}
					
					session_register("isession");
					
					$_SESSION['isession']	= $ilogs;
					$isession		= $_SESSION['isession'];
					
					$this->mclass->LastLogin($last_login,$ip,$i_user_id);
					$this->mclass->Logs($isession,$i_user_id,$last_login,$last_login,$ip);

					$this->load->view('index',$main);
					$this->output->cache(0);
															
				} else {
					$this->load->view('login/vindex',$main);
				}
				***/
				
				if($this->obj->session->userdata('ses_user_id') && 
					$this->obj->session->userdata('ses_user_name') && 
					$this->obj->session->userdata('ses_level') ) 
				{
					$qlogs	= $this->obj->db->query(" SELECT cast(i_session AS integer)+1 AS isession FROM tm_logs ORDER BY cast(i_session AS integer) DESC, d_login_date DESC LIMIT 1 ");
					if($qlogs->num_rows()>0) {
						$rlogs	= $qlogs->row();
						$ilogs	= $rlogs->isession;
					} else {
						$ilogs	= 1;
					}
					$this->obj->session->set_userdata('isession', $ilogs);
					$isession	= $this->obj->session->userdata('isession');
					if($isession!="" || $isession!=0) {
						$user_item	= array(
							'd_user_login_date'=>$last_login,
							'd_user_login_host'=>$ip
						);
						$this->obj->db->update('tm_user',$user_item,array('i_user_id'=>$i_user_id));											

						$logs	= array(
							'i_session'=>$isession,
							'i_user_id'=>$i_user_id,
							'd_login_date'=>$last_login,
							'd_logout_date'=>$last_login,
							'd_user_login_host'=>$ip
						);
						$this->obj->db->insert('tm_logs',$logs);
					}
		
					$this->obj->load->view('index',$main);
					//$this->obj->output->cache(0);
					//$this->obj->load->view('xxxxxxxxxxxxxxxx');
				} else {
					//$this->obj->load->view('login/vindex',$main);
					$this->obj->load->view('zzzzzzzzzzzzzzzz');
				}
				
			} else {
				$this->obj->load->view('login/vindex',$main);
				//$this->obj->load->view('ssssssssssssssss');
			}
		} else {
			$this->obj->load->view('login/vindex',$main);
			//$this->obj->load->view('wwwwwwwwwwwwwww');
		}
	}

	function login2() {
		/*
		$main['tp_title']= $this->obj->lang->line('tp_title');
		$main['ft_log']	= $this->obj->lang->line('ft_login');
		$main['t_user']	= $this->obj->lang->line('t_username');
		$main['t_pswd']	= $this->obj->lang->line('t_passwd');
		$main['ts_log']	= $this->obj->lang->line('ts_login');
		$main['tr_reset']	= $this->obj->lang->line('tr_reset');
		$main['n_head']	= $this->obj->lang->line('n_header');
		$main['n_foot']	= $this->obj->lang->line('n_footer');	
		$main['dir']	= "js";
		*/
		$usernamex	= $this->obj->input->post('username',false);
		$passwordx	= $this->obj->input->post('password',false);
		
		if((!empty($usernamex) && !empty($passwordx)) ) {
			
			$ip		= $_SERVER['REMOTE_ADDR'];
			$paswd_enkri	= md5($passwordx);
			
			$qlogin	= $this->obj->db->query(" SELECT * FROM tm_user WHERE e_user_name='".$usernamex."' AND e_password='".$paswd_enkri."' AND i_user_active='1' ", false);			

			if($qlogin->num_rows()>0) {
				
				$qdate	= $this->obj->db->query( " SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS timestamp0 " );
				
				$rlogin	= $qlogin->row();
				$rdate	= $qdate->row();
				
				$last_login	= $rdate->timestamp0;
				$i_user_id	= $rlogin->i_user_id;
				$e_user_name= $rlogin->e_user_name;
				$i_level	= $rlogin->i_level;

				$i_user_id_enkri	= md5($i_user_id);
				$e_user_name_enkri	= md5($e_user_name);
				
				$session_item = array(
				  'ses_user_id'=>$i_user_id_enkri,
				  'ses_user_name'=>$e_user_name_enkri,
				  'ses_level'=>$i_level,
				  'user_idx'=>$i_user_id
				);
				
				$this->obj->session->set_userdata($session_item);
				
				if($this->obj->session->userdata('ses_user_id') && 
					$this->obj->session->userdata('ses_user_name') && 
					$this->obj->session->userdata('ses_level') ) 
				{
					$qlogs	= $this->obj->db->query(" SELECT cast(i_session AS integer)+1 AS isession FROM tm_logs ORDER BY cast(i_session AS integer) DESC, d_login_date DESC LIMIT 1 ");
					if($qlogs->num_rows()>0) {
						$rlogs	= $qlogs->row();
						$ilogs	= $rlogs->isession;
					} else {
						$ilogs	= 1;
					}
					$this->obj->session->set_userdata('isession', $ilogs);
					$isession	= $this->obj->session->userdata('isession');
					
					//if($isession!="" || $isession!=0) {
						$user_item	= array(
							'd_user_login_date'=>$last_login,
							'd_user_login_host'=>$ip
						);
						
						$this->obj->db->update('tm_user',$user_item,array('i_user_id'=>$i_user_id));											

						$logs	= array(
							'i_session'=>$isession,
							'i_user_id'=>$i_user_id,
							'd_login_date'=>$last_login,
							'd_logout_date'=>$last_login,
							'd_user_login_host'=>$ip
						);

						if($usernamex!=''){
							$this->obj->db->insert('tm_logs',$logs);
							$usernamex	= '';
						}
						
						return true;
					//}
				}else{
					return false;
				}
				
			} else {
				return false;
				//$this->obj->load->view('login/vindex',$main);
				//$this->obj->load->view('ssssssssssssssss');
				//redirect('cindex/');
			}
		}else{
			return false;
		}
	}
}
?>
