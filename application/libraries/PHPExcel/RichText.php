<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2010 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    RichText
 * @copyright  Copyright (c) 2006 - 2010 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.2, 2010-01-11
 */


/** PHPExcel root directory */
if (!defined('ROOT')) {
	/**
	 * @ignore
	 */
	define('ROOT', dirname(__FILE__) . '/../');
}

/** IComparable */
require_once ROOT . 'PHPExcel/IComparable.php';

/** Cell */
require_once ROOT . 'PHPExcel/Cell.php';

/** Cell_DataType */
require_once ROOT . 'PHPExcel/Cell/DataType.php';

/** RichText_ITextElement */
require_once ROOT . 'PHPExcel/RichText/ITextElement.php';

/** RichText_TextElement */
require_once ROOT . 'PHPExcel/RichText/TextElement.php';

/** RichText_Run */
require_once ROOT . 'PHPExcel/RichText/Run.php';

/** Style_Font */
require_once ROOT . 'PHPExcel/Style/Font.php';

/**
 * RichText
 *
 * @category   PHPExcel
 * @package    RichText
 * @copyright  Copyright (c) 2006 - 2010 PHPExcel (http://www.codeplex.com/PHPExcel)
 */
class RichText implements IComparable
{
	/**
	 * Rich text elements
	 *
	 * @var RichText_ITextElement[]
	 */
	private $_richTextElements;
	
	/**
	 * Parent cell
	 *
	 * @var Cell
	 */
	private $_parent;
	   
    /**
     * Create a new RichText instance
     *
     * @param 	Cell	$pParent
     * @throws	Exception
     */
    public function __construct(Cell $pCell = null)
    {
    	// Initialise variables
    	$this->_richTextElements = array();
    	
    	// Set parent?
    	if (!is_null($pCell)) {
	    	// Set parent cell
	    	$this->_parent = $pCell;
	    		
	    	// Add cell text and style
	    	if ($this->_parent->getValue() != "") {
	    		$objRun = new RichText_Run($this->_parent->getValue());
	    		$objRun->setFont(clone $this->_parent->getParent()->getStyle($this->_parent->getCoordinate())->getFont());
	    		$this->addText($objRun);
	    	}
	    		
	    	// Set parent value
	    	$this->_parent->setValueExplicit($this, Cell_DataType::TYPE_STRING);
    	}
    }
    
    /**
     * Add text
     *
     * @param 	RichText_ITextElement		$pText		Rich text element
     * @throws 	Exception
     * @return RichText
     */
    public function addText(RichText_ITextElement $pText = null)
    {
    	$this->_richTextElements[] = $pText;
    	return $this;
    }
    
    /**
     * Create text
     *
     * @param 	string	$pText	Text
     * @return	RichText_TextElement
     * @throws 	Exception
     */
    public function createText($pText = '')
    {
    	$objText = new RichText_TextElement($pText);
    	$this->addText($objText);
    	return $objText;
    }
    
    /**
     * Create text run
     *
     * @param 	string	$pText	Text
     * @return	RichText_Run
     * @throws 	Exception
     */
    public function createTextRun($pText = '')
    {
    	$objText = new RichText_Run($pText);
    	$this->addText($objText);
    	return $objText;
    }
    
    /**
     * Get plain text
     *
     * @return string
     */
    public function getPlainText()
    {
    	// Return value
    	$returnValue = '';
    	
    	// Loop through all RichText_ITextElement
    	foreach ($this->_richTextElements as $text) {
    		$returnValue .= $text->getText();
    	}
    	
    	// Return
    	return $returnValue;
    }
    
    /**
     * Convert to string
     *
     * @return string
     */
    public function __toString() {
    	return $this->getPlainText();
    }
    
    /**
     * Get Rich Text elements
     *
     * @return RichText_ITextElement[]
     */
    public function getRichTextElements()
    {
    	return $this->_richTextElements;
    }
    
    /**
     * Set Rich Text elements
     *
     * @param 	RichText_ITextElement[]	$pElements		Array of elements
     * @throws 	Exception
     * @return RichText
     */
    public function setRichTextElements($pElements = null)
    {
    	if (is_array($pElements)) {
    		$this->_richTextElements = $pElements;
    	} else {
    		throw new Exception("Invalid RichText_ITextElement[] array passed.");
    	}
    	return $this;
    }
 
    /**
     * Get parent
     *
     * @return Cell
     */
    public function getParent() {
    	return $this->_parent;
    }
    
    /**
     * Set parent
     *
     * @param Cell	$value
     * @return RichText
     */
    public function setParent(Cell $value) {
    	// Set parent
    	$this->_parent = $value;
    	
    	// Set parent value
    	$this->_parent->setValueExplicit($this, Cell_DataType::TYPE_STRING);
		
		// Verify style information

		$sheet = $this->_parent->getParent();
		$cellFont = $sheet->getStyle($this->_parent->getCoordinate())->getFont()->getSharedComponent();
		foreach ($this->getRichTextElements() as $element) {
			if (!($element instanceof RichText_Run)) continue;
			
			if ($element->getFont()->getHashCode() == $sheet->getDefaultStyle()->getFont()->getHashCode()) {
				if ($element->getFont()->getHashCode() != $cellFont->getHashCode()) {
					$element->setFont(clone $cellFont);
				}
			}
		}
		return $this;
    }
    
	/**
	 * Get hash code
	 *
	 * @return string	Hash code
	 */	
	public function getHashCode() {
		$hashElements = '';
		foreach ($this->_richTextElements as $element) {
			$hashElements .= $element->getHashCode();
		}
		
    	return md5(
    		  $hashElements
    		. __CLASS__
    	);
    }
    
	/**
	 * Implement PHP __clone to create a deep clone, not just a shallow copy.
	 */
	public function __clone() {
		$vars = get_object_vars($this);
		foreach ($vars as $key => $value) {
			if ($key == '_parent') continue;
			
			if (is_object($value)) {
				$this->$key = clone $value;
			} else {
				$this->$key = $value;
			}
		}
	}
}
