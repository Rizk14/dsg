<?php
class Mmaster extends CI_Model{
  function __construct() { 
  parent::__construct();

}
  function get_unit_packing(){
	  $query = $this->db->query("select * from tm_unit_packing order by kode_unit");
	  
	  return $query->result();
	  }
	  
	   function get_unit_jahit(){
	  $query = $this->db->query("select * from tm_unit_jahit order by kode_unit");
	  
	  return $query->result();
	  }
	  
	  function pop_get_unit_packing(){
	  $query = $this->db->query("select * from tm_unit_packing where status_kelompok='f'order by kode_unit");
	  
	  return $query->result();
	  }
	  
	   function pop_get_unit_jahit(){
	  $query = $this->db->query("select * from tm_unit_jahit where status_kelompok='f' order by kode_unit");
	  
	  return $query->result();
	  }
	  
	  function cekdataunit($list_jahit,$list_packing){
		  $this->db->select("id from tm_kelompok_unit where id_unit_jahit='$list_jahit' AND id_unit_packing='$list_packing' ",false); 
		 
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  return $query->result();
			  }
		  }
		  
		  function getAllkeltanpalimit($cari, $id_unit_jahit,$id_unit_packing){
			  
			  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(nama_kelompok) like UPPER('%".$this->db->escape_str($cari)."%')";
		
		if ($id_unit_jahit != "0")
			$pencarian.= " AND id_unit_jahit = '$id_unit_jahit' ";
		
		if ($id_unit_packing != "0")
			$pencarian.= " AND id_unit_packing = '$id_unit_packing' ";
			
		$query	= $this->db->query(" SELECT distinct * FROM tm_kelompok_unit 
					WHERE TRUE ".$pencarian);

    return $query->result();  
		  
		  }
		   function getAllkel($num,$offset,$cari, $id_unit_jahit,$id_unit_packing){
		  
		 $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(nama_kelompok) like UPPER('%".$this->db->escape_str($cari)."%')";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND id_unit_jahit = '$id_unit_jahit' ";
		if ($id_unit_packing != "0")
			$pencarian.= " AND id_unit_packing = '$id_unit_packing' ";
			
		$this->db->select(" distinct *  FROM tm_kelompok_unit 
					WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		  
		  $query = $this->db->get();
		  
		  

		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT * FROM tm_kelompok_unit_detail WHERE id_kelompok_unit = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();	
					
					foreach ($hasil2 as $row2) {
			$queryxx = $this->db->query(" SELECT * from tm_unit_jahit where id='$row2->id_unit_jahit'");
			if ($queryxx->num_rows() > 0){
				$hasilxx=$queryxx->row();
				
					$id_unit_jahit=$hasilxx->id;
					$kode_unit_jahit=$hasilxx->kode_unit;
					$nama_unit_jahit=$hasilxx->nama;
		
		}
		else{
			$kode_unit_jahit='';
					$nama_unit_jahit='';
			}
			$queryxx = $this->db->query(" SELECT * from tm_unit_packing where id='$row2->id_unit_packing'");
			if ($queryxx->num_rows() > 0){
				$hasilxx=$queryxx->row();
				
					$id_unit_packing=$hasilxx->id;
					$kode_unit_packing=$hasilxx->kode_unit;
					$nama_unit_packing=$hasilxx->nama;
		
		}
		else{
			$kode_unit_packing='';
					$nama_unit_packing='';
			}
			
			$detail_kelompok[] = array(			'id_unit_packing'=> $id_unit_packing,
												'kode_unit_packing'=> $kode_unit_packing,
												'nama_unit_packing'=> $nama_unit_packing,
												'id_unit_jahit'=> $id_unit_jahit,
												'kode_unit_jahit'=> $kode_unit_jahit,
												'nama_unit_jahit'=> $nama_unit_jahit
												
											);
									}		
				
			$data_kelompok[] = array(			
												'nama_kelompok'=> $row1->nama_kelompok,
												'tgl_update'=> $row1->tgl_update,
												'id'=> $row1->id,
												'detail_kelompok'=>$detail_kelompok
											);
				$detail_kelompok = array();							

}
}
}
else {
					$data_kelompok = '';
				}
				return $data_kelompok;

}

  function deletekel($id){
	$tgl = date("Y-m-d H:i:s");	
	
		
				$query2	= $this->db->query(" SELECT * FROM tm_kelompok_unit where id='$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						
						$query3	= $this->db->query(" SELECT id,id_unit_jahit,id_unit_packing FROM tm_kelompok_unit_detail WHERE id_kelompok_unit='$row2->id' ");
						if ($query3->num_rows() > 0){
						$hasilrow = $query3->result();
						foreach ($hasilrow as $row){
						$this->db->query(" UPDATE tm_unit_jahit SET status_kelompok = 'f' where id='$row->id_unit_jahit';
								 ");
						$this->db->query(" UPDATE tm_unit_packing SET status_kelompok = 'f' where id='$row->id_unit_packing';
								 ");
							 }
							
						}
						 $this->db->delete('tm_kelompok_unit_detail', array('id_kelompok_unit' => $id));
					}
					  $this->db->delete('tm_kelompok_unit', array('id' => $id));
					
							} 
													
					} 
		
  function getdataunit($id){
	$tgl = date("Y-m-d H:i:s");	
	
		
				$query2	= $this->db->query(" SELECT * FROM tm_kelompok_unit where id='$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT id,id_unit_jahit,id_unit_packing FROM tm_kelompok_unit_detail WHERE id_kelompok_unit='$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasilres3=$query3->result();
							foreach($hasilres3 as $row3){
								$query4	= $this->db->query(" SELECT * from tm_unit_packing where id ='$row3->id_unit_packing' ");
										if($query4->num_rows() >0){
										$hasilres4=$query4->row();
										$nama_unit_packing=$hasilres4->nama;
										$nama_unit_packing=$nama_unit_packing.";";
										$kode_unit_packing=$hasilres4->kode_unit;
										$kode_unit_packing=$kode_unit_packing.";";
										$id_unit_packing=$hasilres4->id;
										$id_unit_packing=$id_unit_packing.";";
										}
										else{
										$nama_unit_packing='';
										$kode_unit_packing='';
										$id_unit_packing='';
											}
								$query6	= $this->db->query(" SELECT * from tm_unit_jahit where id ='$row3->id_unit_jahit' ");
										if($query6->num_rows() >0){
										$hasilres6=$query6->row();
										$nama_unit_jahit=$hasilres6->nama;
										$nama_unit_jahit=$nama_unit_jahit.";";
										$kode_unit_jahit=$hasilres6->kode_unit;
										$kode_unit_jahit=$kode_unit_jahit.";";
										$id_unit_jahit=$hasilres6->id;	
										$id_unit_jahit=$id_unit_jahit.";";		
										}
										else{
										$nama_unit_jahit='';
										$kode_unit_jahit='';
										$id_unit_jahit='';
											}
								$data_detail [] =array(
								'nama_unit_packing'=>$nama_unit_packing,
								'id_unit_packing'=>$id_unit_packing,
								'kode_unit_packing'=>$kode_unit_packing,
								'nama_unit_jahit'=>$nama_unit_jahit,
								'kode_unit_jahit'=>$kode_unit_jahit,
								'id_unit_jahit'=>$id_unit_jahit
								);	
							}
										
										
							
					}
							$data_hd [] =array(
								'nama_kelompok'=>$row2->nama_kelompok,
								'id_kelompok'=>$row2->id,
								'data_detail'=>$data_detail
								);	
									
					}
				
				return $data_hd;	
						} 
													
					} 			
				} 
	


?>
