<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function lcustomertransfer() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_customer_transfer ORDER BY i_customer_transfer ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function lcustomer() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_customer ORDER BY e_customer_name ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function detailharga($iproductmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.v_unitprice AS unitprice FROM tr_product_base a 
			INNER JOIN tr_product_motif b ON b.i_product=a.i_product_base WHERE b.i_product_motif='$iproductmotif' ");
	}
	
	function detailhargaperpelanggan0($iproductmotif,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price AS unitprice FROM tr_product_price WHERE i_product_motif='$iproductmotif' AND i_customer='$icustomer' AND f_active='t' ORDER BY i_price DESC LIMIT 1 ");
	}

	function detailhargaperpelanggan1($iproductmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price AS unitprice FROM tr_product_price WHERE i_product_motif='$iproductmotif' AND i_customer='0' AND f_active='t' ORDER BY i_price DESC LIMIT 1 ");
	}
		
	function exptransferdo_new($ddofirst,$ddolast,$icustomertransfer) {
		$db2=$this->load->database('db_external', TRUE);
		
		if(($ddofirst!="" && $ddolast!="") && ($ddofirst!=0 && $ddolast!=0)) {
			$ddate	= " WHERE (a.d_do BETWEEN '$ddofirst' AND '$ddolast' ) ";
			$batal	= " AND a.f_do_cancel='f' ";
			$icusttransfer = " AND d.i_customer_from='$icustomertransfer' ";
		}else{
			$ddate	= " ";
			$batal	= " WHERE a.f_do_cancel='f' ";
			$icusttransfer = " AND d.i_customer_from='$icustomertransfer' ";
		}
		
		/* Disabled 20062011	
		return $db2->query("
			SELECT substring(a.i_do_code,3,12) AS idocode, a.d_do, d.i_customer_transfer, b.i_product, b.n_deliver, substring(c.i_op_code,3,12) AS iopcode, a.i_customer, substring(a.i_branch,3,2) AS ibranch
			
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tm_op c ON cast(c.i_op_code AS integer)=b.i_op
			INNER JOIN tr_customer_transfer d ON d.i_customer_from=a.i_customer
			
			".$ddate." ".$batal." ORDER BY a.d_do ");
		*/
		
		/*** 01072011
		return $db2->query("
			SELECT substring(a.i_do_code,5,12) AS idocode, 
			a.d_do, 
			d.i_customer_transfer, 
			b.i_product, 
			b.n_deliver, 
			substring(c.i_op_code,5,12) AS iopcode, 
			a.i_customer, 
			substring(a.i_branch,3,2) AS ibranch 
			FROM tm_do a 
			INNER JOIN tm_do_item b ON a.i_do=b.i_do 
			INNER JOIN tm_op c ON cast(c.i_op_code AS integer)=b.i_op 
			INNER JOIN tr_customer_transfer d ON d.i_customer_from=a.i_customer 
			".$ddate." ".$batal." ORDER BY a.d_do ");	
		***/
		
		return $db2->query(" SELECT substring(a.i_do_code,5,12) AS idocode, 
			a.d_do, 
			d.i_customer_transfer, 
			b.i_product, 
			b.n_deliver, 
			(b.v_do_gross/b.n_deliver) AS vunitprice,
			substring(c.i_op_code,5,12) AS iopcode, 
			a.i_customer, 
			e.i_code AS ibranch
			FROM tm_do a 
			INNER JOIN tm_do_item b ON a.i_do=b.i_do 
			INNER JOIN tm_op c ON c.i_op=b.i_op 
			INNER JOIN tr_customer_transfer d ON d.i_customer_from=a.i_customer 
			INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch
			".$ddate." ".$batal." ".$icusttransfer." ORDER BY a.d_do ");
	}
}
?>
