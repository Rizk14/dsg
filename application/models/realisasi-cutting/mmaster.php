<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function barangjdi($num, $offset, $cari) {
   	if($cari!='0' && $cari!=''){
		$pencarian	= " WHERE (i_product_motif LIKE '%$cari%' OR e_product_motifname LIKE '%$cari%') ";
	} else {
		$pencarian	= "";
	} 
  	$query	= $this->db->select(" * FROM tr_product_motif ".$pencarian." ORDER BY i_product_motif ASC ")->limit($num,$offset);
	$query = $this->db->get();
	
	return $query->result();
  }
  
  function barangjditanpalimit($cari) {
  	if($cari!='0' && $cari!=''){
		$pencarian	= " WHERE (i_product_motif LIKE '%$cari%' OR e_product_motifname LIKE '%$cari%')";
	}else{
		$pencarian	= "";
	}
	
  	return $this->db->query(" SELECT * FROM tr_product_motif ".$pencarian." ORDER BY i_product_motif ASC ");
  }
  
  function thnrealisasi() {
  	return $this->db->query(" SELECT substring(no_realisasi,4,4) AS tahun FROM tt_realisasi_cutting ORDER BY id DESC LIMIT 1 ");
  }

  function nomorrealisasi() {
  	return $this->db->query(" SELECT substring(no_realisasi,8,5) AS nomor FROM tt_realisasi_cutting ORDER BY id DESC LIMIT 1 ");
  }

  function getAll($num, $offset, $cari, $date_from, $date_to) {
	  $pencarian = "";
		if($cari!='0'){
			$pencarian.= " AND UPPER(no_realisasi) like UPPER('%$cari%') ";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_realisasi >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_realisasi <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_realisasi DESC, id DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_realisasi >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_realisasi <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_realisasi DESC, id DESC ";
		}
	  
		/*if($cari!='0'){
			$pencarian	= " WHERE UPPER(no_realisasi) like '%$cari%' ORDER BY tgl_realisasi DESC ";
		}else{
			$pencarian	= " ORDER BY tgl_realisasi DESC ";
		} */
		
		$this->db->select(" * FROM tt_realisasi_cutting WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_fb = array();
		$detail_fb = array();
		
		if ($query->num_rows() > 0){

			$j	= 0;
			
			foreach ($query->result() as $row1) {	
				
				$x	= 0;
				
				$query2	= $this->db->query(" SELECT * FROM tt_realisasi_cutting_detail WHERE id_realisasi_cutting='$row1->id' ");			
				foreach($query2->result() as $row2) {
					if ($row2->kode_brg_jadi != '') {
						$qbrgjadi	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' LIMIT 1 ");
						if ($qbrgjadi->num_rows() > 0){
							$rbrgjadi	= $qbrgjadi->row();
							$nama_brg_jadi = $rbrgjadi->e_product_motifname;
						}
						else {
							if ($row2->brg_jadi_manual == '')
								$nama_brg_jadi = '';
							else
								$nama_brg_jadi = $row2->brg_jadi_manual;
						}
					}
					else
						$nama_brg_jadi = '';

					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
					if ($query3->num_rows()>0){
						$hasilrow = $query3->row();
						$kode_brg	= $row2->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$satuan		= $hasilrow->nama_satuan;
					} else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $row2->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg = '';
							$satuan = '';
						}
					}
										
					$detail_fb[$x] = array('id'=> $row2->id,
											'id_realisasi_cutting'=> $row2->id_realisasi_cutting,
											'qty_realisasi'=> $row2->qty_realisasi,
											'jam_mulai'=> $row2->jam_mulai,
											'jam_selesai'=> $row2->jam_selesai,
											'operator_cutting'=> $row2->operator_cutting,
											'kode_brg_jadi'=> $row2->kode_brg_jadi,
											'nm_brg_jadi'=> $nama_brg_jadi,
											'kode_brg'=>$kode_brg,
											'nama_brg'=>$nama_brg,
											'jum_lot'=>$row2->jum_lot,
											//'panjang_kain_yard'=>$row2->panjang_kain_yard,
											'panjang_kain_mtr'=>$row2->detail_panjang_kain_mtr,
											'gelaran'=>$row2->gelaran,
											'set'=>$row2->set,
											'jum_gelar'=>$row2->detail_jum_gelar,
											'hasil_potong'=>$row2->detail_hasil_potong,
											'sisa_planning_mtr'=>$row2->sisa_planning_mtr,
											'sisa_aktual_mtr'=>$row2->sisa_aktual_mtr,
											'sisa_allow_mtr'=>$row2->sisa_allow_mtr,
											'allowance'=>$row2->allowance,
											'sisa_selisih_mtr'=>$row2->sisa_selisih_mtr,
											'keterangan'=>$row2->keterangan,
											'status_beres' => $row2->status_beres );
					$x+=1;
				}
				
				$pisah1 = explode("-", $row1->tgl_realisasi);
				$tgl1	= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tglnya = mktime(0,0,0,$bln1,$tgl1,$thn1);
				$hari = date("w", $tglnya);
				
				if ($hari == 0)
					$nama_hari = "Minggu";
				else if ($hari == 1)
					$nama_hari = "Senin";
				else if ($hari == 2)
					$nama_hari = "Selasa";
				else if ($hari == 3)
					$nama_hari = "Rabu";
				else if ($hari == 4)
					$nama_hari = "Kamis";
				else if ($hari == 5)
					$nama_hari = "Jumat";
				else
					$nama_hari = "Sabtu";
								
				$data_fb[$j] = array('id'=> $row1->id,	
									'no_realisasi'=> $row1->no_realisasi,
									'tgl_realisasi'=> $row1->tgl_realisasi,
									'is_dacron'=> $row1->is_dacron,
									'qty_dacron'=> $row1->qty_dacron,
									'detail_fb'=> $detail_fb,
									'status_apply_stok' => $row1->status_apply_stok,
									'status_beres' => $row1->status_beres,
									'tgl_update'=> $row1->tgl_update,
									'nama_hari'=> $nama_hari);
				$j+=1;		
				$detail_fb = '';
			}
		}
		else {
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($cari, $date_from, $date_to){
 	/*if($cari!='0'){
		$pencarian	= " WHERE UPPER(no_realisasi) like '%$cari%' ";
	}else{
		$pencarian	= "";
	} */
	$pencarian = "";
		if($cari!='0'){
			$pencarian.= " AND UPPER(no_realisasi) like UPPER('%$cari%') ";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_realisasi >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_realisasi <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_realisasi DESC, id DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_realisasi >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_realisasi <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_realisasi DESC, id DESC ";
		}
			 
	$query	= $this->db->query(" SELECT * FROM tt_realisasi_cutting WHERE TRUE ".$pencarian." ");
    return $query->result();  
  }
      
  function cek_data($norealisasi){
    return $this->db->query(" SELECT * from tt_realisasi_cutting WHERE no_realisasi='$norealisasi' ", false);
  }
  
  function save($eksternal, $norealisasi, $tglrealisasi, $diprint, $imotif, $qty, $kode, $jml_lot, $pjg_kain, $gelaran, $set, $jml_gelar, 
							$hsl_potong, $sisa_planning, $aktual, $sisa_allow, $allowance, $sisa_selisih, $id_bagian_brg_jadi, $ukuran_pola, $jammulai, $jamselesai, $keterangan, 
							$id_detail, $idx, $operator, $jumlah_input, $nobonm, $is_dacron, $qty_dacron, $ket_pekerjaan,
							$hide_jml_gelar, $hide_hsl_potong, $qty_klr, $is_sisa, $nama_brg_jadi_other){
	
    $tgl = date("Y-m-d");
	$tm_apply	= 0;
	if ($is_sisa == '')
		$is_sisa = 'f';
	
    $this->db->select(" * FROM tt_realisasi_cutting WHERE no_realisasi='$norealisasi' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
	
	if(count($hasil)==0) {
		$data_header = array(
			'no_realisasi'=>$norealisasi,
			'tgl_realisasi'=>$tglrealisasi,
			'no_bonm'=>$nobonm,
			'is_dacron'=>$is_dacron,
			'qty_dacron'=>$qty_dacron,
			'ket_pekerjaan_operator'=>$ket_pekerjaan,
			'tgl_input'=>$tgl,
			'tgl_update'=>$tgl,
			'for_eksternal'=>$eksternal);
		
		$this->db->insert('tt_realisasi_cutting',$data_header);
	}	
	if ($is_dacron == 't') {
		$kode_brg_dacron = $kode[0];
		//cek stok terakhir bahan dacron di tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg_dacron' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty_dacron;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg_dacron,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg_dacron' ");
					}
					
					// &&&&&&&&&&&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE 
												kode_brg = '$kode_brg_dacron' ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; 
							$harganya = $row2->harga;
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0)
									$selisih = $stoknya-$qty_dacron; 
								else
									$selisih = $stoknya+$temp_selisih; 
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; //
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode_brg_dacron' AND harga = '$harganya' "); //
									
									$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
											VALUES ('$kode_brg_dacron','$norealisasi', '$stoknya', '0', '$tglrealisasi', '$harganya') ");
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $qty_dacron; // 
										
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih; // 
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode_brg_dacron' AND harga = '$harganya' ");
						
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
												VALUES ('$kode_brg_dacron','$norealisasi', '".abs($temp_selisih)."', 
												'$new_stok', '$tglrealisasi', '$harganya') ");
					}
					// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end modifikasi 5 okt 2011
	} // end if is_dacron == t
	
	$qid_realisasi_cutting	= $this->db->query(" SELECT id FROM tt_realisasi_cutting ORDER BY id DESC LIMIT 1 ");
	if($qid_realisasi_cutting->num_rows()>0){
		$rid	= $qid_realisasi_cutting->row();
		$iid	= $rid->id;
	}else{
		$iid	= 1;
	}
	
	//$cek_bon_m	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting WHERE no_bonm='$nobonm' ");
	//if($cek_bon_m->num_rows()<=0){
		$this->db->query(" INSERT INTO tm_apply_stok_hasil_cutting(id_realisasi_cutting,no_bonm,tgl_bonm,tgl_input,tgl_update,status_stok) 
							VALUES('$iid','$nobonm','$tgl','$tgl','$tgl','f') ");	
		$tm_apply	= 1;
	//}
	
	$qtm_apply_stok_hsl_cutting	= $this->db->query(" SELECT id FROM tm_apply_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
	if($qtm_apply_stok_hsl_cutting->num_rows()>0){
		$rtm_apply_stok_hsl_cutting	= $qtm_apply_stok_hsl_cutting->row();
		$id_apply_stok	= $rtm_apply_stok_hsl_cutting->id;
	}
	
	$data_detail	= array();
	$diupdate	= '0';
	
	for($x=0;$x<=$jumlah_input;$x++) {
	
		//if($imotif[$x]!='' && $qty[$x]!='' && $nmbrg[$x]!='' && $kode[$x]!='' && $jammulai[$x]!='' && $jamselesai[$x]!='' && $gelaran[$x]!='' && $set[$x]!='' && $jml_gelar[$x]!='' && $aktual[$x]!=''){
		if($imotif[$x]!='' && $qty[$x]!='' && $kode[$x]!='' && $jammulai[$x]!='' && $jamselesai[$x]!=''){
			//&&&&&&&&& start utk update stok bahan baku yg bukan dacron
			if ($is_dacron != 't') {
				// 24-01-2012
				// cek apakah di schedule cutting itu id_apply_stok_detail dan id_realisasi_detail kosong atau ga
				// jika id_apply_stok_detail = 0 / kosong dan id_realisasi_detail = 0 / kosong, update stoknya
				
				$sql3	= $this->db->query(" SELECT id_apply_stok_detail, id_realisasi_detail FROM tt_schedule_cutting_detail 
										WHERE id = '$id_detail[$x]' ");
				if($sql3->num_rows()>0){
					$hasil3	= $sql3->row();
					$id_apply_stok_detail	= $hasil3->id_apply_stok_detail;
					$id_realisasi_detail	= $hasil3->id_realisasi_detail;
					
					if ($id_apply_stok_detail == 0 && $id_realisasi_detail == 0 && $is_sisa == 'f') {
						// 24-01-2012
						// cek apakah bhn quilting atau ga
						$sqlcek	= $this->db->query(" SELECT * FROM tm_brg_hasil_makloon WHERE kode_brg='$kode[$x]' ");
						if ($sqlcek->num_rows() > 0) {
							$tm_stok = "tm_stok_hasil_makloon";
							$tt_stok = "tt_stok_hasil_makloon";
							$quilting = 't';
						}
						else {
							$tm_stok = "tm_stok";
							$tt_stok = "tt_stok";
							$quilting = 'f';
						}
						
						//cek stok terakhir bahan di tm_stok, dan update stoknya 
							$query3	= $this->db->query(" SELECT stok FROM ".$tm_stok." WHERE kode_brg = '$kode[$x]' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qty_klr[$x];
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$kode[$x],
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert($tm_stok,$data_stok);
							}
							else {
								$this->db->query(" UPDATE ".$tm_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode[$x]' ");
							} //
							
							// &&&&&&&&&&&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&
							$selisih = 0;
							if ($quilting == 'f')
								$sql = "SELECT id, stok, harga FROM tm_stok_harga WHERE 
										kode_brg = '$kode[$x]' ORDER BY id ASC";
							else
								$sql = "SELECT id, stok, harga FROM tm_stok_harga WHERE 
										kode_brg_quilting = '$kode[$x]' AND quilting = 't' ORDER BY id ASC";
										
							$query2	= $this->db->query($sql);
							if ($query2->num_rows() > 0){
								$hasil2=$query2->result();
									
								$temp_selisih = 0;				
								foreach ($hasil2 as $row2) {
									$stoknya = $row2->stok; 
									$harganya = $row2->harga;
									
									if ($stoknya > 0) { 
										if ($temp_selisih == 0)
											$selisih = $stoknya-$qty_klr[$x]; 
										else
											$selisih = $stoknya+$temp_selisih; 
											
										if ($selisih < 0) {
											$temp_selisih = $selisih; //
											if ($quilting == 'f')
												$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
														where kode_brg= '$kode[$x]' AND harga = '$harganya' ");
											else
												$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
														where kode_brg_quilting= '$kode[$x]' AND harga = '$harganya' ");
											
											$this->db->query(" INSERT INTO ".$tt_stok." (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
													VALUES ('$kode[$x]','$norealisasi', '$stoknya', '0', '$tglrealisasi', '$harganya') ");
										}
											
										if ($selisih > 0) { // ke-2 masuk sini
											if ($temp_selisih == 0)
												$temp_selisih = $qty_klr[$x]; // 
											break;
										}
									}
								} // end for
							}
							
							if ($selisih != 0) {
								//echo $selisih." ".$quilting." ";
								$new_stok = $selisih; // 
								//echo $stoknya." ".$harganya." ".$quilting."<br>";
								if ($quilting == 'f') {
									//$sql = "UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									//	where kode_brg= '$kode[$x]' AND harga = '$harganya'"; echo $sql."<br>";
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg= '$kode[$x]' AND harga = '$harganya' ");
								}
								else {
									//$sql = "UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									//	where kode_brg_quilting= '$kode[$x]' AND harga = '$harganya'"; echo $sql."<br>";
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg_quilting= '$kode[$x]' AND harga = '$harganya' ");
								}
								/*echo "INSERT INTO ".$tt_stok." (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
														VALUES ('$kode[$x]','$norealisasi', '".abs($temp_selisih)."', 
														'$new_stok', '$tglrealisasi', '$harganya')"; */
								$this->db->query(" INSERT INTO ".$tt_stok." (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
														VALUES ('$kode[$x]','$norealisasi', '".abs($temp_selisih)."', 
														'$new_stok', '$tglrealisasi', '$harganya') "); //die();
							}
							// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end modifikasi 5 okt 2011
					} // end if
				}
			
			} // end if
			
			if ($jml_lot[$x] == '')
				$jml_lot[$x] = "0;";
			
			if ($pjg_kain[$x] == '')
				$pjg_kain[$x] = "0;";
			
			if ($gelaran[$x] == '')
				$gelaran[$x] = "0;";
			
			if ($set[$x] == '')
				$set[$x] = "0;";
			
			if ($sisa_planning[$x] == '')
				$sisa_planning[$x] = "0;";
			
			if ($aktual[$x] == '')
				$aktual[$x] = "0;";
			
			if ($sisa_allow[$x] == '')
				$sisa_allow[$x] = "0;";
			
			if ($sisa_selisih[$x] == '')
				$sisa_selisih[$x] = "0;";
			
			if ($qty_klr[$x] == '')
				$qty_klr[$x] = 0;
			
			//$pjg_kainyard	= $hide_pjg_kain[$x]/0.91;
			
			//$hasilpotong	= $jml_gelar[$x]*$set[$x];
			/*$sisaplanning	= $hide_pjg_kain[$x]-($jml_gelar[$x]*$gelaran[$x]);
			$sisaallow	= $jml_gelar[$x]*2/100;
			$sisaselisih	= $sisaplanning-$aktual[$x]-$sisaallow; */
			
			if ($id_bagian_brg_jadi[$x] == '')
				$id_bagian_brg_jadi[$x] = 0;
			
			/*$hasil_potongnya = "";
			$jml_gelarnya = "";
			$detail_hasil_potongnya = "";
			$detail_jml_gelarnya = ""; */
			if ($is_dacron == 't') {
				$hasil_potongnya = $hsl_potong[$x];
				$jml_gelarnya = $jml_gelar[$x];
				$detail_hasil_potongnya = $hsl_potong[$x];
				$detail_jml_gelarnya = $jml_gelar[$x];
			}
			else {
				$hasil_potongnya = $hide_hsl_potong[$x];
				$jml_gelarnya = $hide_jml_gelar[$x];
				$detail_hasil_potongnya = $hsl_potong[$x];
				$detail_jml_gelarnya = $jml_gelar[$x];
			}
			
			$data_detail[$x] = array(
				'id_realisasi_cutting'=>$iid,
				'qty_realisasi'=>$qty[$x],
				'qty_klr'=>$qty_klr[$x],
				'jam_mulai'=>$jammulai[$x],
				'jam_selesai'=>$jamselesai[$x],
				'operator_cutting'=>$operator[$x],
				'kode_brg_jadi'=>$imotif[$x],
				'kode_brg'=>$kode[$x],
				'jum_lot'=>$jml_lot[$x],
				//'panjang_kain_yard'=>$pjg_kainyard,
				'panjang_kain_yard'=>'0',
				//'panjang_kain_mtr'=>$hide_pjg_kain[$x],
				'gelaran'=>$gelaran[$x],
				'set'=>$set[$x],
				
				// 25-04-2012, ini pake filter variabel tambahan
				/*'jum_gelar'=>$hide_jml_gelar[$x],
				'hasil_potong'=>$hide_hsl_potong[$x], */
				'jum_gelar'=>$jml_gelarnya,
				'hasil_potong'=>$hasil_potongnya,
				
				'sisa_planning_mtr'=>$sisa_planning[$x],
				'sisa_aktual_mtr'=>$aktual[$x],
				'sisa_allow_mtr'=>$sisa_allow[$x],
				'allowance'=>$allowance[$x],
				'sisa_selisih_mtr'=>$sisa_selisih[$x],
				'keterangan'=>$keterangan[$x],
				'id_schedule_cutting_detail'=>$id_detail[$x],
				'id_bagian_brg_jadi'=>$id_bagian_brg_jadi[$x],
				 'ukuran_pola'=>$ukuran_pola[$x],
				 
				 'detail_panjang_kain_mtr'=>$pjg_kain[$x],
				 
				 // 25-04-2012, ini pake filter variabel tambahan
				 /*'detail_jum_gelar'=>$jml_gelar[$x],
				 'detail_hasil_potong'=>$hsl_potong[$x], */
				 'detail_jum_gelar'=>$detail_jml_gelarnya,
				 'detail_hasil_potong'=>$detail_hasil_potongnya,
				 
				 'diprint'=>$diprint[$x],
				 'is_sisa'=>$is_sisa[$x],
				 //nama_brg_jadi_other
				 'brg_jadi_manual'=>$nama_brg_jadi_other[$x]
				 );
							
			$this->db->insert('tt_realisasi_cutting_detail',$data_detail[$x]);
			
			$qrealissi_cutting_detail	= $this->db->query(" SELECT id FROM tt_realisasi_cutting_detail ORDER BY id DESC LIMIT 1 ");
			$rrealissi_cutting_detail	= $qrealissi_cutting_detail->row();
			$id_realisasi_cutting_detail= $rrealissi_cutting_detail->id;
			
			if($tm_apply==1){	
				$this->db->query(" INSERT INTO tm_apply_stok_hasil_cutting_detail(id_realisasi_cutting_detail,id_apply_stok,
							kode_brg_jadi,kode_brg,qty,status_stok, no_bonm, brg_jadi_manual) VALUES('$id_realisasi_cutting_detail','$id_apply_stok','$imotif[$x]','$kode[$x]',
							'$hasil_potongnya','f', 'bonm_rc', '$nama_brg_jadi_other[$x]') ");
			}
			
			/***
			$qstokhslcutting = $this->db->query(" SELECT * FROM tm_stok_hasil_cutting WHERE kode_brg_jadi='$imotif[$x]' AND kode_brg='$kode[$x]' ");
			if($qstokhslcutting->num_rows()>0){
				$rstokhslcutting = $qstokhslcutting->row();
				$jmlstokhslcutting = $rstokhslcutting->stok+$qty[$x];
				$jmlgelarhslcutting= $rstokhslcutting->total_gelar+$jml_gelar[$x];
				
				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='$jmlstokhslcutting', total_gelar='$jmlgelarhslcutting' WHERE kode_brg_jadi='$imotif[$x]' AND kode_brg='$kode[$x]' AND id='$rstokhslcutting->id' ");	
				
				$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi, kode_brg, no_bukti, masuk, saldo, tgl_input) VALUES('$imotif[$x]','$kode[$x]','$norealisasi','$hasilpotong','$jmlstokhslcutting','$tgl') ");
			}else{
				$this->db->query(" INSERT INTO tm_stok_hasil_cutting(kode_brg_jadi,kode_brg,stok,total_gelar,tgl_update_stok) VALUES('$imotif[$x]','$kode[$x]','$qty[$x]','$jml_gelar[$x]','$tgl') ");
				$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi, kode_brg, no_bukti, masuk, saldo, tgl_input) VALUES('$imotif[$x]','$kode[$x]','$norealisasi','$hasilpotong','$qty[$x]','$tgl') ");
			}
			***/
			
			
			if($diupdate=='0'){
				$this->db->query(" UPDATE tt_schedule_cutting SET status_realisasi='t' WHERE id='$idx' ");
				$diupdate	= '1';
			}
			$this->db->query(" UPDATE tt_schedule_cutting_detail SET status_realisasi='t' WHERE id='$id_detail[$x]' ");//
			
			$qschedule_cut_detail	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id='$id_detail[$x]' ");
				if($qschedule_cut_detail->num_rows()>0){
					$rscheudle_cut_detail	= $qschedule_cut_detail->row();
					$qty_plan_sche	= $rscheudle_cut_detail->qty_bhn;
				}else{
					$qty_plan_sche	= 0;
				}
			
			/*$qrealisasi_cut_detail	= $this->db->query(" SELECT sum(hasil_potong) AS total_qty_realisasi FROM tt_realisasi_cutting_detail WHERE id_schedule_cutting_detail='$id_detail[$x]' ");
			if($qrealisasi_cut_detail->num_rows()>0){
				$rrealisasi_cut_detail	= $qrealisasi_cut_detail->row();
				$total_qty_realisasi	= $rrealisasi_cut_detail->total_qty_realisasi;
				
				$belum_realisasi	= $qty_plan_sche - $total_qty_realisasi;
				
				if($belum_realisasi>0){
					$status_realisasi	= 'f';
				}else{
					$status_realisasi	= 't';
				}
				
				$this->db->query(" UPDATE tt_schedule_cutting_detail SET status_realisasi='$status_realisasi' WHERE id='$id_detail[$x]' ");
			} */
			
			// cek apakah realisasi cutting tsb dianggap beres, dilihat dari hasil potongnya
			if ($qty_plan_sche > $hide_hsl_potong[$x])
				$status_beres = 'f';
			else
				$status_beres = 't';
			
			$this->db->query(" UPDATE tt_realisasi_cutting_detail SET status_beres='$status_beres' 
						WHERE id='$id_realisasi_cutting_detail' ");
			
			//cek apakah udh t semua. kalo udh, di tt_realisasi_cutting ubah status_beres = 'f'
			$qrealisasi_cut_detail	= $this->db->query(" SELECT id FROM tt_realisasi_cutting_detail 
						WHERE id_realisasi_cutting ='$iid' AND status_beres = 'f' ");
			if($qrealisasi_cut_detail->num_rows()==0){
				$this->db->query(" UPDATE tt_realisasi_cutting SET status_beres='$status_beres' 
						WHERE id='$iid' ");
			}

		} // end if
	} // end for
  }
    
  
  function delete($kode){    
	$tgl = date("Y-m-d");
	$q	= $this->db->query(" SELECT * FROM tt_realisasi_cutting WHERE id='$kode' LIMIT 1 ");
	
	if($q->num_rows()>0){
		
		foreach($q->result() as $row){
			if ($row->is_dacron == 't') {
				$query3	= $this->db->query(" SELECT kode_brg FROM tt_realisasi_cutting_detail 
							WHERE id_realisasi_cutting ='$row->id' LIMIT 1 ");
				if($query3->num_rows()>0){
					$rquery3	= $query3->row();
					$kode_brg_dacron	= $rquery3->kode_brg;
				}
			}
			
			// 1. reset stoknya #############################################
			//cek stok terakhir tm_stok, dan update stoknya
			if ($row->is_dacron == 't') {
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg_dacron' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama+$row->qty_dacron; // menambah stok krn reset
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode_brg_dacron,
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode_brg_dacron' ");
				}
				
				// &&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
								$query4	= $this->db->query(" SELECT id, kode_brg, masuk, keluar, harga FROM tt_stok WHERE 
								kode_brg = '$kode_brg_dacron' AND no_bukti = '$row->no_realisasi' ORDER BY id DESC ");
									if ($query4->num_rows() > 0){
										$hasil4=$query4->result();
										
										foreach ($hasil4 as $row4) {
											$ttmasuk = $row4->masuk;
											$ttkeluar = $row4->keluar;
											$ttharga = $row4->harga;
											
											if ($ttmasuk != '')
												break;
											
											$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE 
																kode_brg = '$row4->kode_brg' AND harga = '$ttharga' ");
											if ($query3->num_rows() == 0){
												$stok_lama = 0;
											}
											else {
												$hasilrow = $query3->row();
												$stok_lama	= $hasilrow->stok;
											}
											$stokreset = $stok_lama+$ttkeluar;
											
											$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
												where kode_brg= '$row4->kode_brg' AND harga = '$ttharga' "); //
									
											$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
													VALUES ('$row4->kode_brg','$row->no_realisasi', '$ttkeluar', '$stokreset', '$tgl', '$ttharga') ");
										}
									}
			} // end if dacron
			// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			
			$id_detail	= $row->id;
			$q2	= $this->db->query(" SELECT * FROM tt_realisasi_cutting_detail WHERE id_realisasi_cutting='$id_detail' ");
			if($q2->num_rows()>0){
				
				$update=0;
				
				foreach($q2->result() as $row2){
					
					$idschedulecutting = $row2->id_schedule_cutting_detail;
					
					$jmlgelarnya		= $row2->jum_gelar;
					$hasil_potongnya	= $row2->hasil_potong;
					
					$q3	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id='$idschedulecutting' ");
					$row3	= $q3->row();
		
					$this->db->query(" UPDATE tt_schedule_cutting_detail SET status_realisasi='f' WHERE id='$idschedulecutting' ");
					$this->db->query(" UPDATE tt_schedule_cutting SET status_realisasi='f' WHERE id='$row3->id_schedule_cutting' ");
					
					/*if($update==0){
						//$this->db->query(" UPDATE tt_schedule_cutting SET status_realisasi='f' WHERE id='$row3->id_schedule_cutting' ");
						$update=1;
					}*/
					
					/***
					$qstokhslcutting = $this->db->query(" SELECT * FROM tm_stok_hasil_cutting WHERE kode_brg_jadi='$row2->kode_brg_jadi' AND kode_brg='$row2->kode_brg' ");
					
					if($qstokhslcutting->num_rows()>0){
						
						$rstokhslcutting = $qstokhslcutting->row();
						$jmlstokhslcutting = (($rstokhslcutting->stok)-($row2->qty_realisasi));
						$jmlgelarhslcutting= (($rstokhslcutting->total_gelar)-($jmlgelarnya));
						
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='$jmlstokhslcutting', total_gelar='$jmlgelarhslcutting' WHERE kode_brg_jadi='$row2->kode_brg_jadi' AND kode_brg='$row2->kode_brg' AND id='$rstokhslcutting->id' ");	
						
						//$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi, no_bukti, jum_gelar_keluar, jum_potong_keluar, saldo) VALUES('$row2->kode_brg_jadi','$row->no_realisasi','$jmlgelarnya','$hasil_potongnya','$jmlstokhslcutting') ");
						$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg_jadi, kode_brg, no_bukti, keluar, saldo) VALUES('$row2->kode_brg_jadi','$row2->kode_brg','$row->no_realisasi','$hasil_potongnya','$jmlstokhslcutting') ");
					}
					***/				
					
					// 25-01-2012, reset stok utk bahan baku yg qty_klr != 0 
					if ($row2->qty_klr > 0) {
						// 1. reset stoknya #############################################
						//cek stok terakhir tm_stok, dan update stoknya (ini blm ada pengecekan ke tabel brg quilting, blm perlu)
							$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$row->qty_klr; // menambah stok krn reset
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
								$data_stok = array(
									'kode_brg'=>$row2->kode_brg,
									'stok'=>$new_stok,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok',$data_stok);
							}
							else {
								$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$row2->kode_brg' ");
							}
							
							// &&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
								$query4	= $this->db->query(" SELECT id, kode_brg, masuk, keluar, harga FROM tt_stok WHERE 
								kode_brg = '$row2->kode_brg' AND no_bukti = '$row->no_realisasi' ORDER BY id DESC ");
									if ($query4->num_rows() > 0){
										$hasil4=$query4->result();
										
										foreach ($hasil4 as $row4) {
											$ttmasuk = $row4->masuk;
											$ttkeluar = $row4->keluar;
											$ttharga = $row4->harga;
											
											if ($ttmasuk != '')
												break;
											
											$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE 
																kode_brg = '$row4->kode_brg' AND harga = '$ttharga' ");
											if ($query3->num_rows() == 0){
												$stok_lama = 0;
											}
											else {
												$hasilrow = $query3->row();
												$stok_lama	= $hasilrow->stok;
											}
											$stokreset = $stok_lama+$ttkeluar;
											
											$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
												where kode_brg= '$row4->kode_brg' AND harga = '$ttharga' "); //
									
											$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
													VALUES ('$row4->kode_brg','$row->no_realisasi', '$ttkeluar', '$stokreset', '$tgl', '$ttharga') ");
										}
									}
					} // end if $row2->qty_klr
					
					// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
				} // end foreach row2
				
			} // end if q2
		} // end for
	}
	
	
	$tmapplystokhslcutting	= $this->db->query(" SELECT id FROM tm_apply_stok_hasil_cutting WHERE id_realisasi_cutting='$kode' ");
	
	if($tmapplystokhslcutting->num_rows()>0){
		$rtmapplystokhslcutting	= $tmapplystokhslcutting->row();
		
		$this->db->delete('tm_apply_stok_hasil_cutting_detail', array('id_apply_stok'=>$rtmapplystokhslcutting->id, 'status_stok'=>'f'));
		$this->db->delete('tm_apply_stok_hasil_cutting', array('id'=>$rtmapplystokhslcutting->id, 'status_stok'=>'f'));
	}
	
	$this->db->delete('tt_realisasi_cutting', array('id'=>$kode));
	$this->db->delete('tt_realisasi_cutting_detail', array('id_realisasi_cutting'=>$kode));
  }
  
  function get_schedule($num, $offset, $jenisnya, $cari) {
		/*$sql = " b.id, b.no_schedule, b.tgl_cutting FROM tt_schedule_cutting_detail a 
				INNER JOIN tt_schedule_cutting b ON b.id=a.id_schedule_cutting 
				WHERE a.status_realisasi='f' AND UPPER(b.no_schedule) LIKE '%$cari%' 
				GROUP BY b.id, b.no_schedule,b.tgl_cutting, b.status_realisasi "; */
		
		$sql = " distinct b.id, b.no_schedule, b.tgl_cutting, b.is_dacron, b.for_eksternal FROM tt_schedule_cutting_detail a 
				INNER JOIN tt_schedule_cutting b ON b.id=a.id_schedule_cutting 
				WHERE a.status_realisasi='f' ";
		if ($cari != "all")		
			$sql.= " AND UPPER(b.no_schedule) LIKE UPPER('%$cari%') ";
			$sql.= " AND b.is_dacron = '$jenisnya' ORDER BY b.tgl_cutting, b.no_schedule "; //echo $sql; die();

		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sch_cutting = array();
		$detail_sch_cutting = array();
		
		if ($query->num_rows() > 0){
		
			$hasil = $query->result();
			
			//$j=0;
			foreach ($hasil as $row1) {
				
				$sql2 = " SELECT * FROM tt_schedule_cutting_detail WHERE status_realisasi='f' 
						AND id_schedule_cutting='$row1->id' AND qty_bhn > 0 ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					//$jj=0;
					foreach ($hasil2 as $row2) {
						if ($row2->kode_brg_jadi != '') {
							$qbrgjdi = $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							if ($qbrgjdi->num_rows() > 0){
								$rbrgjdi = $qbrgjdi->row();
								$nama_brg_jadi = $rbrgjdi->e_product_motifname;
							}
							else
								$nama_brg_jadi = $row2->brg_jadi_manual;
						}
						else
							$nama_brg_jadi = '';

						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
												WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
						if ($query3->num_rows()>0){
							$hasilrow = $query3->row();
							$kode_brg	= $row2->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan		= $hasilrow->nama_satuan;
						} else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
													tm_satuan b WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg_quilting' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg	= $row2->kode_brg_quilting;
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$kode_brg	= '';
								$nama_brg = '';
								$satuan = '';
							}
						}
						
						$qschedulecutting	= $this->db->query(" SELECT sum(hasil_potong) AS qty_plan_realisasi FROM 
											tt_realisasi_cutting_detail WHERE id_schedule_cutting_detail='$row2->id' ");
						if($qschedulecutting->num_rows()>0){
							$rschedulecutting	= $qschedulecutting->row();
							$qty_plan_realisasi	= $rschedulecutting->qty_plan_realisasi;
						}else{
							$qty_plan_realisasi	= 0;
						}
						
						$sisa	= $row2->qty_bhn - $qty_plan_realisasi;
						
						if ($row1->is_dacron == 't') {
							$query3	= $this->db->query(" SELECT nama_bagian FROM 
												tm_bagian_brg_jadi WHERE id ='$row2->id_bagian_brg_jadi' ");
							if($query3->num_rows()>0){
								$rquery3	= $query3->row();
								$nama_bagian_brg_jadi	= $rquery3->nama_bagian;
							}else{
								$nama_bagian_brg_jadi	= '';
							}
						}
						else
							$nama_bagian_brg_jadi = '';
						
						$detail_sch_cutting[] = array('id'=> $row2->id,
								'id_schedule_cutting'	=> $row2->id_schedule_cutting,
								'kode_brg_jadi'	=> $row2->kode_brg_jadi,
								'nm_brg_jadi'	=> $nama_brg_jadi,
								'kode_brg'	=> $kode_brg,
								'nama_brg'	=> $nama_brg,
								'qty_bhn'	=> $row2->qty_bhn,
								'qty_plan_realisasi' => $sisa,
								'jam_mulai'	=> $row2->jam_mulai,
								'jam_selesai'	=>	$row2->jam_selesai,
								'operator_cutting'	=> $row2->operator_cutting,
								'keterangan'	=> $row2->keterangan,
								'id_apply_stok_detail'	=> $row2->id_apply_stok_detail,
								'nama_bagian_brg_jadi'	=> $nama_bagian_brg_jadi,
								'ukuran_pola'	=> $row2->ukuran_pola,
								'diprint'	=> $row2->diprint);
						//$jj+=1;
					}
				}
				else {
					$detail_sch_cutting = '';
				}
				
				$data_sch_cutting[] = array('id'	=> $row1->id,
						'eksternal'	=> $row1->for_eksternal,
						'no_schedule'	=> $row1->no_schedule,
						'tgl_cutting'	=> $row1->tgl_cutting,
						'is_dacron'	=> $row1->is_dacron,
						'detail_sch_cutting'=> $detail_sch_cutting);
				$detail_sch_cutting = array();
				//$j+=1;
			}
		}
		else {
			$data_sch_cutting = '';
		}
		return $data_sch_cutting;
  }
  
  function get_scheduletanpalimit($jenisnya, $cari){
		$sql = " SELECT distinct b.id, b.no_schedule, b.tgl_cutting, b.is_dacron FROM tt_schedule_cutting_detail a 
				INNER JOIN tt_schedule_cutting b ON b.id=a.id_schedule_cutting 
				WHERE a.status_realisasi='f' ";
		if ($cari != "all")		
			$sql.= " AND UPPER(b.no_schedule) LIKE UPPER('%$cari%') ";
			$sql.= " AND b.is_dacron = '$jenisnya' ORDER BY b.tgl_cutting, b.no_schedule ";
			
  		$query = $this->db->query($sql);
		return $query->result();  
  }
  
  function get_detail_schedule($id, $iitem,$ischhead){

    $detail_schedule = array();
    $i=0;    
    foreach($iitem as $key1 => $row1) {
		if ($row1!='') {
			//$sql = "SELECT * FROM tt_schedule_cutting_detail WHERE id = '$row1' 
			 //					AND id_schedule_cutting = '$id'"; echo $sql."<br>";
			$query3	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id = '$row1' 
			 					AND id_schedule_cutting = '$id' ");
			if ($query3->num_rows() > 0) {
			
				$query2	= $this->db->query(" SELECT b.*, a.for_eksternal FROM tt_schedule_cutting_detail b, tt_schedule_cutting a 
								WHERE a.id = b.id_schedule_cutting AND (b.status_realisasi='f' OR b.status_realisasi='t') 
								AND b.id='$row1' AND b.qty_bhn > 0 ");
				$hasilrow = $query2->row();
				
				$qnamabarang	= $this->db->query(" SELECT a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
								WHERE a.satuan = b.id AND a.kode_brg='$hasilrow->kode_brg_quilting' ");
				if ($qnamabarang->num_rows() > 0) {
					$nmbrg		= $qnamabarang->row();
					$kode_barangnya = $nmbrg->kode_brg; // quilting
					$satuannya = $nmbrg->nama_satuan;
				}
				else {
					$qnamabarang	= $this->db->query(" SELECT a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
									WHERE a.satuan = b.id AND a.kode_brg='$hasilrow->kode_brg' ");
					$nmbrg		= $qnamabarang->row();
					$kode_barangnya = $nmbrg->kode_brg; // bhn baku
					$satuannya = $nmbrg->nama_satuan;
				}
								
				if ($hasilrow->kode_brg_jadi != '') {
					$qmotif		= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$hasilrow->kode_brg_jadi' ");
					if ($qmotif->num_rows() > 0) {
						$nmmotif	= $qmotif->row();
						$nama_brg_jadi = $nmmotif->e_product_motifname;
					}
					else
						$nama_brg_jadi = $hasilrow->brg_jadi_manual;
				}
				else
					$nama_brg_jadi = '';
				
				// ambil nama bagian brg jadi
				if ($hasilrow->id_bagian_brg_jadi != '') {
					$qbagian		= $this->db->query(" SELECT nama_bagian FROM tm_bagian_brg_jadi WHERE id='$hasilrow->id_bagian_brg_jadi' ");
					$nmbagian	= $qbagian->row();
					$nama_bagian_brg_jadi = $nmbagian->nama_bagian;
				}
				else
					$nama_bagian_brg_jadi = '';

				$qschedulecutting	= $this->db->query(" SELECT sum(hasil_potong) AS qty_plan_realisasi FROM tt_realisasi_cutting_detail WHERE id_schedule_cutting_detail='$hasilrow->id' ");
				
				if($qschedulecutting->num_rows()>0){
					$rschedulecutting	= $qschedulecutting->row();
					$qty_plan_realisasi	= $rschedulecutting->qty_plan_realisasi;
				}else{
					$qty_plan_realisasi	= 0;
				}
							
				$sisa	= $hasilrow->qty_bhn - $qty_plan_realisasi;
				
				// ambil data gelaran / set utk bahan baku aja (disertai dgn apakah quilting atau bukan)
				if ($hasilrow->kode_brg_quilting != '')
					$diquilting = 't';
				else
					$diquilting = 'f';
					
				// ambil data marker gelaran/set
				$sql4 = " SELECT gelaran, jum_set FROM tm_marker_gelaran 
					WHERE kode_brg_jadi = '$hasilrow->kode_brg_jadi' AND kode_brg = '$hasilrow->kode_brg'
					AND diprint = '$hasilrow->diprint' AND diquilting = '$diquilting' ";
					
				$query4	= $this->db->query($sql4);
				if ($query4->num_rows() > 0){
					$hasil4 = $query4->row();
					$gelaran	= $hasil4->gelaran;
					$jum_set	= $hasil4->jum_set;
				}
				else {
					$gelaran	= '';
					$jum_set	= '';
				}
				// end data gelaran
													
				$detail_schedule[] = array('id'=> $row1,
					'id_schedule_cutting' => $hasilrow->id_schedule_cutting,
					'nmmotif'	=> $nama_brg_jadi,
					'kode_brg_jadi'	=> $hasilrow->kode_brg_jadi,
					'nama_brg_jadi_other'	=> $hasilrow->brg_jadi_manual,
					'nmbrg'	=> $nmbrg->nama_brg,
					'kode_brg'	=> $kode_barangnya,
					'satuan'	=> $satuannya,
					'qty_bhn'	=> $hasilrow->qty_bhn,
					'qty_plan_realisasi' => $sisa,
					'jam_mulai'	=> $hasilrow->jam_mulai,
					'jam_selesai'	=> $hasilrow->jam_selesai,
					'operator_cutting'	=> $hasilrow->operator_cutting,
					'keterangan'	=> $hasilrow->keterangan,
					'id_apply_stok_detail'	=> $hasilrow->id_apply_stok_detail,
					'ischhead'	=> $ischhead,
					'id_bagian_brg_jadi'	=> $hasilrow->id_bagian_brg_jadi,
					'nama_bagian_brg_jadi'	=> $nama_bagian_brg_jadi,
					'ukuran_pola'	=> $hasilrow->ukuran_pola,
					'gelaran'	=> $gelaran,
					'jum_set'	=> $jum_set,
					'diprint'	=> $hasilrow->diprint,
					'eksternal' => $hasilrow->for_eksternal
					 );
			}
		} // end if row1 = ''
		$i+=1;
	}
	return $detail_schedule;
  }
  
  function get_pkp_tipe_pajak_bykodesup($kode_sup){
	$query	= $this->db->query(" SELECT pkp, tipe_pajak, top FROM tm_supplier where kode_supplier = '$kode_sup' ");    
    return $query->result();  
  }
  
  function get_realisasi_cutting($id) {
		$query	= $this->db->query(" SELECT * FROM tt_realisasi_cutting where id='$id' ");
	
		$data_fb = array();
		$detail_fb = array();
		
		if ($query->num_rows()>0) {
			$j=0;
			foreach ($query->result() as $row1) {
				$query2	= $this->db->query(" SELECT * FROM tt_realisasi_cutting_detail WHERE id_realisasi_cutting='$row1->id' ");
				$x=0;
				if ($query2->num_rows() > 0){
					foreach ($query2->result() as $row2) {		

						$qnamabarang	= $this->db->query(" SELECT a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.kode_brg='$row2->kode_brg' AND a.satuan = b.id ");
						if ($qnamabarang->num_rows() > 0){
							$nmbrg		= $qnamabarang->row();
							$nama_barangnya = $nmbrg->nama_brg;
							$satuannya = $nmbrg->nama_satuan;
						}
						else {
							$qnamabarang2	= $this->db->query(" SELECT a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
											WHERE a.satuan = b.id AND a.kode_brg='$row2->kode_brg' ");
							$nmbrg		= $qnamabarang2->row();
							$nama_barangnya = $nmbrg->nama_brg;
							$satuannya = $nmbrg->nama_satuan;
						}
						
						if ($row2->kode_brg_jadi != '') {
							$qnmmotif	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							if ($qnmmotif->num_rows() > 0){
								$nmmotif	= $qnmmotif->row();
								$nama_brg_jadi = $nmmotif->e_product_motifname;
							}
							else
								$nama_brg_jadi = $row2->brg_jadi_manual;
						}
						else
							$nama_brg_jadi = '';
						
						$qschdetail	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id='$row2->id_schedule_cutting_detail' ");
						$schdetail	= $qschdetail->row();
						$sqlxx = " SELECT id FROM tm_apply_stok_hasil_cutting_detail 
										WHERE id_realisasi_cutting_detail='$row2->id' 
										AND kode_brg_jadi='$row2->kode_brg_jadi' ";
						if ($row2->brg_jadi_manual != '')
							$sqlxx.= " AND brg_jadi_manual = '$row2->brg_jadi_manual' ";
						else
							$sqlxx.= " AND (brg_jadi_manual is NULL OR brg_jadi_manual = '') ";
						$sqlxx.= " AND kode_brg='$row2->kode_brg' ";
						/*$qapply_stok_cutting	= $this->db->query(" SELECT id FROM tm_apply_stok_hasil_cutting_detail 
										WHERE id_realisasi_cutting_detail='$row2->id' 
										AND kode_brg_jadi='$row2->kode_brg_jadi' AND brg_jadi_manual = '$row2->brg_jadi_manual' 
										AND kode_brg='$row2->kode_brg' "); */
						$qapply_stok_cutting	= $this->db->query($sqlxx);
						$applystokcutting	= $qapply_stok_cutting->row();
						
						// ambil nama bagian brg jadi
						if ($row2->id_bagian_brg_jadi != 0 ) {
							//$sql = "SELECT nama_bagian FROM tm_bagian_brg_jadi WHERE id='$row2->id_bagian_brg_jadi'"; echo $sql;
							$qbagian = $this->db->query(" SELECT nama_bagian FROM tm_bagian_brg_jadi WHERE id='$row2->id_bagian_brg_jadi' ");
							$nmbagian	= $qbagian->row();
							$nama_bagian_brg_jadi = $nmbagian->nama_bagian;
						}
						else
							$nama_bagian_brg_jadi = '';
						
						$detail_fb[] = array('id'=> $row2->id,
								'id_realisasi_cutting'=> $row2->id_realisasi_cutting,
								'qty_realisasi'=> $row2->qty_realisasi,
								'qty_klr'=> $row2->qty_klr,
								'jam_mulai'=> $row2->jam_mulai,
								'jam_selesai'=> $row2->jam_selesai,
								'operator_cutting'=> $row2->operator_cutting,
								'nmmotif'	=> $nama_brg_jadi,
								'nama_brg_jadi_other'	=> $row2->brg_jadi_manual,
								'kode_brg_jadi'=> $row2->kode_brg_jadi,
								'nmbrg'	=> $nama_barangnya,
								'satuan'	=> $satuannya,
								'kode_brg'=> $row2->kode_brg,
								'jum_lot'=> $row2->jum_lot,
								'panjang_kain_yard'=> $row2->panjang_kain_yard,
								'panjang_kain_mtr'=> $row2->panjang_kain_mtr,
								'gelaran'=> $row2->gelaran,
								'set'=> $row2->set,
								'jum_gelar'=> $row2->jum_gelar,
								'hasil_potong'=> $row2->hasil_potong,
								'sisa_planning_mtr'=> $row2->sisa_planning_mtr,
								'sisa_aktual_mtr'=> $row2->sisa_aktual_mtr,
								'sisa_allow_mtr'=> $row2->sisa_allow_mtr,
								'allowance'=> $row2->allowance,
								'sisa_selisih_mtr'=> $row2->sisa_selisih_mtr,
								'keterangan'=> $row2->keterangan,
								'id_schedule_cutting'=> $row2->id_schedule_cutting_detail,
								'qty_bhn'=>$schdetail->qty_bhn,
								'id_apply_stok_hsl_cutting'=>$applystokcutting->id,
								'nama_bagian_brg_jadi'=> $nama_bagian_brg_jadi,
								'ukuran_pola'=> $row2->ukuran_pola,
								'detail_panjang_kain_mtr'=> $row2->detail_panjang_kain_mtr,
								'detail_jum_gelar'=> $row2->detail_jum_gelar,
								'detail_hasil_potong'=> $row2->detail_hasil_potong,
								'diprint'=> $row2->diprint
								);
						$x+=1;
					}
				}else{
					$detail_fb = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_realisasi);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tgl_realisasi = $tgl1."-".$bln1."-".$thn1;
								
				$data_fb[] = array('id' => $row1->id,
						'no_realisasi' => $row1->no_realisasi,
						'tgl_realisasi' => $tgl_realisasi,
						'is_dacron' => $row1->is_dacron,
						'qty_dacron' => $row1->qty_dacron,
						'ket_pekerjaan' => $row1->ket_pekerjaan_operator,
						'detail_fb' => $detail_fb);
				$j+=1;
				$detail_fb = array();
			} // end 0f for
		}else{
			$data_fb = '';
		}
		return $data_fb;
  }  
  
  // 25-04-2012
  function get_rc_for_print($id) {
		$query	= $this->db->query(" SELECT a.no_realisasi, a.tgl_realisasi, a.is_dacron, a.qty_dacron, a.ket_pekerjaan_operator, b.* 
								FROM tt_realisasi_cutting a, tt_realisasi_cutting_detail b 
								WHERE a.id = b.id_realisasi_cutting AND a.id='$id' 
								ORDER BY b.operator_cutting, b.kode_brg, b.kode_brg_quilting ");
	
		$data_fb = array();
		
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row2) {
				$qnamabarang	= $this->db->query(" SELECT a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
								WHERE a.kode_brg='$row2->kode_brg' AND a.satuan = b.id ");
				if ($qnamabarang->num_rows() > 0){
					$nmbrg		= $qnamabarang->row();
					$nama_barangnya = $nmbrg->nama_brg;
					$satuannya = $nmbrg->nama_satuan;
				}
				else {
					$qnamabarang2	= $this->db->query(" SELECT a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
									WHERE a.satuan = b.id AND a.kode_brg='$row2->kode_brg' ");
					$nmbrg		= $qnamabarang2->row();
					$nama_barangnya = $nmbrg->nama_brg;
					$satuannya = $nmbrg->nama_satuan;
				}
						
				$qnmmotif	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
				$nmmotif	= $qnmmotif->row();
						
				$qschdetail	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id='$row2->id_schedule_cutting_detail' ");
				$schdetail	= $qschdetail->row();
						
				$qapply_stok_cutting	= $this->db->query(" SELECT id FROM tm_apply_stok_hasil_cutting_detail WHERE id_realisasi_cutting_detail='$row2->id' AND kode_brg_jadi='$row2->kode_brg_jadi' AND kode_brg='$row2->kode_brg' ");
				$applystokcutting	= $qapply_stok_cutting->row();
						
				// ambil nama bagian brg jadi
				if ($row2->id_bagian_brg_jadi != 0 ) {
					//$sql = "SELECT nama_bagian FROM tm_bagian_brg_jadi WHERE id='$row2->id_bagian_brg_jadi'"; echo $sql;
					$qbagian = $this->db->query(" SELECT nama_bagian FROM tm_bagian_brg_jadi WHERE id='$row2->id_bagian_brg_jadi' ");
					$nmbagian	= $qbagian->row();
					$nama_bagian_brg_jadi = $nmbagian->nama_bagian;
				}
				else
					$nama_bagian_brg_jadi = '';
						
				$pisah1 = explode("-", $row2->tgl_realisasi);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						
				$tgl_realisasi = $tgl1."-".$bln1."-".$thn1;
						
				$data_fb[] = array('id'=> $row2->id,
								'id_realisasi_cutting'=> $row2->id_realisasi_cutting,
								'qty_realisasi'=> $row2->qty_realisasi,
								'qty_klr'=> $row2->qty_klr,
								'jam_mulai'=> $row2->jam_mulai,
								'jam_selesai'=> $row2->jam_selesai,
								'operator_cutting'=> $row2->operator_cutting,
								'nmmotif'	=> $nmmotif->e_product_motifname,
								'kode_brg_jadi'=> $row2->kode_brg_jadi,
								'nmbrg'	=> $nama_barangnya,
								'satuan'	=> $satuannya,
								'kode_brg'=> $row2->kode_brg,
								'jum_lot'=> $row2->jum_lot,
								'panjang_kain_yard'=> $row2->panjang_kain_yard,
								'panjang_kain_mtr'=> $row2->panjang_kain_mtr,
								'gelaran'=> $row2->gelaran,
								'set'=> $row2->set,
								'jum_gelar'=> $row2->jum_gelar,
								'hasil_potong'=> $row2->hasil_potong,
								'sisa_planning_mtr'=> $row2->sisa_planning_mtr,
								'sisa_aktual_mtr'=> $row2->sisa_aktual_mtr,
								'sisa_allow_mtr'=> $row2->sisa_allow_mtr,
								'allowance'=> $row2->allowance,
								'sisa_selisih_mtr'=> $row2->sisa_selisih_mtr,
								'keterangan'=> $row2->keterangan,
								'id_schedule_cutting'=> $row2->id_schedule_cutting_detail,
								'qty_bhn'=>$schdetail->qty_bhn,
								'id_apply_stok_hsl_cutting'=>$applystokcutting->id,
								'nama_bagian_brg_jadi'=> $nama_bagian_brg_jadi,
								'ukuran_pola'=> $row2->ukuran_pola,
								'detail_panjang_kain_mtr'=> $row2->detail_panjang_kain_mtr,
								'detail_jum_gelar'=> $row2->detail_jum_gelar,
								'detail_hasil_potong'=> $row2->detail_hasil_potong,
								'diprint'=> $row2->diprint,
								
								'no_realisasi' => $row2->no_realisasi,
								'tgl_realisasi' => $tgl_realisasi,
								'is_dacron' => $row2->is_dacron,
								'qty_dacron' => $row2->qty_dacron,
								'ket_pekerjaan' => $row2->ket_pekerjaan_operator
								);
						
			} // end 0f for
		}else{
			$data_fb = '';
		}
		return $data_fb;
  }
}

