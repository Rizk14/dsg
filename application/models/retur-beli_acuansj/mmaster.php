<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $supplier, $cari) {	  
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_retur_beli ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_retur_beli WHERE kode_supplier = '$supplier' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_retur_beli WHERE kode_supplier = '$supplier' 
					AND UPPER(no_retur) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_retur_beli WHERE UPPER(no_retur) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_detail WHERE id_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'id_pembelian_detail'=> $row2->id_pembelian_detail
											);
					}
				}
				else {
					$detail_fb = '';
				}
				
				// ambil data detail fakturnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_faktur WHERE id_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) { 
						//ambil tgl faktur
						$query3	= $this->db->query(" SELECT tgl_faktur FROM tm_pembelian_nofaktur
						WHERE no_faktur = '$row2->no_faktur' AND kode_supplier = '$row1->kode_supplier' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$tgl_faktur	= $hasilrow->tgl_faktur;
							
							$pisah1 = explode("-", $tgl_faktur);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							if ($bln1 == '01')
								$nama_bln = "Januari";
							else if ($bln1 == '02')
								$nama_bln = "Februari";
							else if ($bln1 == '03')
								$nama_bln = "Maret";
							else if ($bln1 == '04')
								$nama_bln = "April";
							else if ($bln1 == '05')
								$nama_bln = "Mei";
							else if ($bln1 == '06')
								$nama_bln = "Juni";
							else if ($bln1 == '07')
								$nama_bln = "Juli";
							else if ($bln1 == '08')
								$nama_bln = "Agustus";
							else if ($bln1 == '09')
								$nama_bln = "September";
							else if ($bln1 == '10')
								$nama_bln = "Oktober";
							else if ($bln1 == '11')
								$nama_bln = "November";
							else if ($bln1 == '12')
								$nama_bln = "Desember";
							$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						}
						else {
							$tgl_faktur	= '';
						}
				
						$detail_faktur[] = array('no_faktur'=> $row2->no_faktur,
												'tgl_faktur'=> $tgl_faktur
											);
					}
				}
				else {
					$detail_faktur = '';
				}
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$nama_supplier	= $hasilrow->nama;
				//ambil nomor faktur
			/*	$query3	= $this->db->query(" SELECT no_faktur FROM tm_pembelian_nofaktur WHERE id = '$row1->id_pembelian_nofaktur' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$no_faktur	= $hasilrow->no_faktur;
				 }
				 else
					$no_sj = ''; */
				
				$pisah1 = explode("-", $row1->faktur_date_from);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$faktur_date_from = $tgl1."-".$bln1."-".$thn1;
				
				$pisah2 = explode("-", $row1->faktur_date_to);
				$tgl2= $pisah2[2];
				$bln2= $pisah2[1];
				$thn2= $pisah2[0];
				$faktur_date_to = $tgl2."-".$bln2."-".$thn2;
				
				//11-04-2012, cek apakah data lama (Dari acuan faktur) atau bukan
				$query3	= $this->db->query(" SELECT count(id) as jumid FROM tm_retur_beli_faktur
										WHERE id_retur_beli = '$row1->id' ");
				//if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				if ($hasilrow->jumid > 0)
					$is_acuan_faktur = 't';
				else
					$is_acuan_faktur = 'f'; // blm ada pengecekan jika diluar range = t and tdk ada data di tabel retur faktur
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_dn_retur'=> $row1->no_dn_retur,	
											'tgl_retur'=> $row1->tgl_retur,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb,
											'detail_faktur'=> $detail_faktur,
											'faktur_date_from'=> $faktur_date_from,
											'faktur_date_to'=> $faktur_date_to,
											'status_nota'=> $row1->status_nota,
											'is_acuan_faktur'=> $is_acuan_faktur
											);
				$detail_fb = array();
				$detail_faktur = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			/*$query	= $this->db->query(" SELECT a.* FROM tm_retur_beli a, tm_pembelian_nofaktur b WHERE 
			a.id_pembelian_nofaktur = b.id AND b.id <> '0' "); */
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE kode_supplier = '$supplier' ");
	}
	else {
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE kode_supplier = '$supplier' 
					AND UPPER(no_retur) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE UPPER(no_retur) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_dn){
    $this->db->select("id from tm_retur_beli WHERE no_dn_retur = '$no_dn' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_notaretur($no_nota){
    $this->db->select("id from tm_nota_retur_beli WHERE no_nota = '$no_nota' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_dn_retur, $tgl_retur, $kode_supplier, $ket, $kode, $nama, $qty, $harga, $faktur, $no_sj, $id_pembelian_detail){  
    $tgl = date("Y-m-d");
			
			// ambil data terakhir di tabel tm_retur_beli
			$query2	= $this->db->query(" SELECT id FROM tm_retur_beli ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_retur	= $hasilrow->id; //echo $idnya; die();
			
			//save ke tabel retur_detail,
			if ($kode!='' && $qty!='') {
				//if ($no_sj != '') {
				// hitung jumlah qty dari tabel SJ/tm_pembelian (29 okt 2011, ini udh ga dipake)
				/*	$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
					tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.no_faktur = '$faktur' AND c.id=d.id_pembelian_nofaktur 
					AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier' 
					AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$qty_faktur = $hasilrow->jum;
							
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b,
									tm_retur_beli_faktur c WHERE c.id_retur_beli = b.id 
									AND a.id_retur_beli = b.id AND a.id_retur_beli_faktur = c.id AND c.no_faktur = '$faktur' 
									AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$jum_retur = $hasilrow->jum; // ini sum qty di retur beli berdasarkan kode brg tsb						
					//$qty = $qty-$jum_retur;
					if ($jum_retur == '')
						$jum_retur = 0;
					//echo "qty=".$qty." qty_faktur=".$qty_faktur." jum retur=".$jum_retur; die();
					$jumtot = $jum_retur+$qty;
					if ($jumtot > $qty_faktur)
						$qty = $qty_faktur-$jum_retur; */
				
				// ambil id di tabel tm_retur_beli_faktur (10-04-2012, ini ga dipake lagi)
			/*		$query2	= $this->db->query(" SELECT id FROM tm_retur_beli_faktur WHERE no_faktur = '$faktur' 
					AND id_retur_beli = '$id_retur' ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_retur_faktur = $hasilrow->id;
				}
				else {
					$id_retur_faktur = 0;
				}
				$is_luar_range = 'f';
				
				if ($id_retur_faktur == 0)
					$is_luar_range = 't'; */
				
				if ($id_pembelian_detail == '0')
					$is_luar_range = 't';

				// jika semua data tdk kosong, insert ke tm_retur_beli_detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'harga'=>$harga,
					'id_retur_beli'=>$id_retur,
					//'id_retur_beli_faktur'=>$id_retur_faktur,
					'is_luar_range'=>$is_luar_range,
					'id_pembelian_detail'=>$id_pembelian_detail
				);
				$this->db->insert('tm_retur_beli_detail',$data_detail);
			 
			//insert data brg keluar ke tabel history stok, dan kurangi stok di tabel tm_stok
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = abs($stok_lama-$qty);
				
				//NEW 140611, cek stok terakhir tm_stok_harga, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode' AND harga='$harga' ");
				if ($query3->num_rows() == 0){
					$stok_lama2 = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama2	= $hasilrow->stok;
				}
				$new_stok2 = abs($stok_lama2-$qty);
				// 29 okt 2011, tgl input isi datanya tgl_retur
				// 10-04-2012, tt_stok udh ga dipake lagi
				/*$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar_lain, saldo, tgl_input, harga) 
										VALUES ('$kode','$no_dn_retur', '$qty', '$new_stok2', '$tgl_retur', '$harga' ) "); */
				
				$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' where kode_brg= '$kode' ");
				
				$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok2', tgl_update_stok = '$tgl' 
									where kode_brg= '$kode' AND harga = '$harga' ");
			} // end if kode != '' dan $qty != ''
		  
		//}
		//else {
			// ambil data terakhir di tabel tm_retur_beli
		/*	$query2	= $this->db->query(" SELECT id FROM tm_retur_beli ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_retur	= $hasilrow->id; //echo $idnya; die();
			
			//save ke tabel retur_detail,
			if ($kode!='' && $qty!='') {
				// hitung jumlah qty dari tabel SJ/tm_pembelian
					$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
					tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.no_faktur = '$faktur' AND c.id=d.id_pembelian_nofaktur 
					AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier' 
					AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$qty_faktur = $hasilrow->jum;
							
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b,
									tm_retur_beli_faktur c WHERE c.id_retur_beli = b.id 
									AND a.id_retur_beli = b.id AND a.id_retur_beli_faktur = c.id AND c.no_faktur = '$faktur' 
									AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$jum_retur = $hasilrow->jum; // ini sum qty di retur beli berdasarkan kode brg tsb						
					//$qty = $qty-$jum_retur;
					if ($jum_retur == '')
						$jum_retur = 0;
					//echo "qty=".$qty." qty_faktur=".$qty_faktur." jum retur=".$jum_retur; die();
					$jumtot = $jum_retur+$qty;
					if ($jumtot > $qty_faktur)
						$qty = $qty_faktur-$jum_retur;
				
				// ambil id di tabel tm_retur_beli_faktur
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli_faktur WHERE no_faktur = '$faktur' 
					AND id_retur_beli = '$id_retur' ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_retur_faktur = $hasilrow->id;
				
				// jika semua data tdk kosong, insert ke tm_retur_beli_detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'id_retur_beli'=>$id_retur,
					'id_retur_beli_faktur'=>$id_retur_faktur
				);
				$this->db->insert('tm_retur_beli_detail',$data_detail);
				
			 
			//insert data brg keluar ke tabel history stok, dan kurangi stok di tabel tm_stok
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = abs($stok_lama-$qty);
				$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
										VALUES ('$kode','$no_sj_keluar', '$qty', '$new_stok', '$tgl', '$harga' ) ");
				
				$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' where kode_brg= '$kode' ");
			} */
		//} // end else
  }
    
  function delete($kode){    
	$tgl = date("Y-m-d");
	// reset stoknya
			$query3	= $this->db->query(" SELECT no_dn_retur, tgl_retur FROM tm_retur_beli WHERE id = '$kode' ");
			$hasilrow = $query3->row();
			$no_dn_retur = $hasilrow->no_dn_retur;
			$tgl_retur = $hasilrow->tgl_retur;
	
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_detail WHERE id_retur_beli = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$kode_brg = $row2->kode_brg;
						$qty = $row2->qty;
						$harga = $row2->harga;
						
						//1. ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok 
											WHERE kode_brg='$kode_brg' ");
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$stokreset = $stok_lama+$qty;
							
							// new 140611
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga 
											WHERE kode_brg='$kode_brg' AND harga = '$harga' ");
							$hasilrow = $query3->row();
							$stok_lama2	= $hasilrow->stok;
							$stokreset2 = $stok_lama2+$qty;
							
						//2. insert ke tabel tt_stok dgn tipe masuk, utk membatalkan data retur
							$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk_lain, saldo, tgl_input, harga)
							VALUES ('$kode_brg', '$no_dn_retur', '$qty', '$stokreset2', '$tgl_retur', '$harga')  ");
														
						//3. update stok di tm_stok
							$this->db->query(" UPDATE tm_stok SET stok = '$stokreset', tgl_update_stok = '$tgl'
												where kode_brg= '$kode_brg'  ");
							
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl'
												where kode_brg= '$kode_brg' AND harga = '$harga'  ");
					}
				}	
	
    $this->db->delete('tm_retur_beli_detail', array('id_retur_beli' => $kode));
    $this->db->delete('tm_retur_beli_faktur', array('id_retur_beli' => $kode));
    $this->db->delete('tm_retur_beli', array('id' => $kode));
  }
  
  //function get_faktur($num, $offset, $csupplier, $cari) {
	function get_faktur($csupplier, $cari, $pkp) {
	if ($cari == "all") {
		/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
		$query = $this->db->get(); */
		
		$sql = " SELECT * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$csupplier' "; 
		/*if ($pkp == 't')
			$sql.= " AND status_faktur_pajak = 't' "; */
		
		$sql.= " ORDER BY tgl_faktur DESC ";
							
		$query	= $this->db->query($sql);

	}
	else {
		/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
		$query = $this->db->get(); */
		
		$sql = " SELECT * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$csupplier' "; 
		/*if ($pkp == 't')
			$sql.= " AND status_faktur_pajak = 't' "; */
		$sql.= " AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ";
		
		$query	= $this->db->query($sql);
	}
		$data_faktur = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail faktur-nya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur_sj WHERE id_pembelian_nofaktur = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$no_sj	= $row2->no_sj;
						
						$sql = "SELECT tgl_sj from tm_pembelian where no_sj = '$no_sj' 
								AND kode_supplier= '$csupplier' AND status_aktif = 't'";
						$query3	= $this->db->query($sql);
						$hasilrow3 = $query3->row();
						$tgl_sj	= $hasilrow3->tgl_sj;
						
					/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b 
									WHERE a.id_retur_beli = b.id AND b.id_pembelian = '$row1->id' 
									AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$jum_op = $hasilrow->jum; // ini sum qty di returnya berdasarkan kode brg tsb
						
						$qty = $qty-$jum_op; */
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1; 
										
						$detail_faktur[] = array('id'=> $row2->id,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj
											);
					}
				}
				else {
					$detail_faktur = '';
				}
				
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'jumlah'=> $row1->jumlah,
											'detail_faktur'=> $detail_faktur
											);
				$detail_faktur = array();
			} // endforeach header
		}
		else {
			$data_faktur = '';
		}
		return $data_faktur;
  }
  
  function get_fakturtanpalimit($csupplier, $cari){
	if ($cari == "all") { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_faktur DESC ");
	}
	else {
		$query	= $this->db->query(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ");
	}
    
    return $query->result();  
  }
  
  function get_detail_brg($date_from, $date_to, $kode_supplier, $jenis_pembelian){
    $detail_brg = array();
    /*$sql = "SELECT a.kode_brg, a.harga FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
	tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.id = '$id_faktur' AND c.id=d.id_pembelian_nofaktur 
	AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier'";
	die($sql); */
    
    //=============================================
   // $list_faktur = explode(",", $no_faktur); 
  //  foreach($list_faktur as $rowutama) {
	//	$rowutama = trim($rowutama);
	//	if ($rowutama != '') { 
    //=============================================    
		/*	$query	= $this->db->query(" SELECT a.kode_brg, a.qty, a.harga, c.no_faktur, c.tgl_faktur, b.no_sj, b.tgl_sj 
			FROM tm_pembelian_detail a, tm_pembelian b, 
			tm_pembelian_nofaktur c, tm_pembelian_nofaktur_sj d 
			WHERE a.id_pembelian = b.id AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
			AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
			AND c.id=d.id_pembelian_nofaktur AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier 
			AND c.kode_supplier = '$kode_supplier' AND c.jenis_pembelian = '$jenis_pembelian'
			AND a.status_stok = 't' AND c.status_lunas = 'f' AND b.status_aktif = 't' ORDER BY c.no_faktur, b.no_sj, a.kode_brg "); */
		// 10-04-2012
		$query	= $this->db->query(" SELECT a.kode_brg, a.qty, a.harga, a.id, b.no_sj, b.tgl_sj 
			FROM tm_pembelian_detail a, tm_pembelian b
			WHERE a.id_pembelian = b.id AND b.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
			AND b.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
			AND b.kode_supplier = '$kode_supplier' AND b.jenis_pembelian = '$jenis_pembelian'
			AND a.status_stok = 't' AND b.status_lunas = 'f' AND b.status_aktif = 't' ORDER BY b.tgl_sj, a.kode_brg ");	
			
		/*	
			// ini utk test aja
		    $query	= $this->db->query(" SELECT distinct(a.kode_brg), a.harga FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
			tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.no_faktur = '$rowutama' AND c.id=d.id_pembelian_nofaktur 
			AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier' "); */
			
			$hasil = $query->result();
			foreach($hasil as $row1) {
				
					$query2	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg	= '';
						$satuan	= '';
					}
							
					// hitung jumlah qty dari tabel SJ/tm_pembelian
					// koreksi: 28 okt 2011, sementara ga usah dimunculin. jadi lieur
				/*	$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
					tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id 
					AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.id=d.id_pembelian_nofaktur 
					AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier' 
					AND a.kode_brg = '$row1->kode_brg' ");
					$hasilrow = $query3->row();
					$qty_faktur = $hasilrow->jum;
							
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b,
									tm_retur_beli_faktur c, tm_pembelian_nofaktur d 
									WHERE c.id_retur_beli = b.id 
									AND c.no_faktur = d.no_faktur
									AND b.kode_supplier = d.kode_supplier
									AND a.id_retur_beli = b.id AND a.id_retur_beli_faktur = c.id 
									AND d.tgl_faktur >= to_date('$date_from','dd-mm-yyyy')
									AND d.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
									AND a.kode_brg = '$row1->kode_brg' ");
					$hasilrow = $query3->row();
					$jum_retur = $hasilrow->jum; // ini sum qty di retur beli berdasarkan kode brg tsb						
					//$qty = $qty-$jum_retur;
					if ($jum_retur == '')
						$jum_retur = 0; */
					
					//10-04-2012	
					/*$pisah1 = explode("-", $row1->tgl_faktur);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					$tgl_faktur = $tgl1."-".$bln1."-".$thn1; */
					
					$pisah1 = explode("-", $row1->tgl_sj);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					$tgl_sj = $tgl1."-".$bln1."-".$thn1;
					
					$detail_brg[] = array(		'kode_brg'=> $row1->kode_brg,
												'harga'=> $row1->harga,
												'id_pembelian_detail'=> $row1->id,
												'no_faktur'=> '',
												'tgl_faktur'=> '',
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty_faktur'=> $row1->qty,
												//'qty_faktur'=> $qty_faktur,
												//'jum_retur'=> $jum_retur,
												'no_sj'=> $row1->no_sj,
												'tgl_sj'=> $tgl_sj
										);
				
			}
	//	}
//	} //end foreach
	return $detail_brg;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori= '1' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
  function get_retur($id_retur){
	$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE id = '$id_retur' ");    
    $hasil = $query->result();
    
    $data_retur = array();
	$detail_retur = array();
	
	foreach ($hasil as $row1) {
			// ambil data detail barangnya, order by no fakturnya (koreksi 4 nov 2011: order by kode_brg)
			//11-04-2012: id_retur_beli_faktur ga dipake
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_detail WHERE id_retur_beli = '$row1->id'
				ORDER BY kode_brg ");
				if ($query2->num_rows() > 0) {
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
					
					// 11-04-2012: ini dipake tapi hanya utk data lama
						$query3	= $this->db->query(" SELECT no_faktur FROM tm_retur_beli_faktur WHERE 
									id = '$row2->id_retur_beli_faktur' AND id_retur_beli = '$row1->id' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$no_faktur	= $hasilrow->no_faktur;
						}
						else
							$no_faktur = '';
						
						// ================================================
						if ($no_faktur != '') {
							// 24 okt 2011, ambil data tgl faktur
							$query3	= $this->db->query(" SELECT tgl_faktur FROM tm_pembelian_nofaktur WHERE 
										no_faktur = '$no_faktur' AND kode_supplier = '$row1->kode_supplier' ");
							$hasilrow = $query3->row();
							/*$tgl_faktur	= $hasilrow->tgl_faktur;
							
							$pisah1 = explode("-", $tgl_faktur);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$tgl_faktur = $tgl1."-".$bln1."-".$thn1; */
						
							// ================================================
							
							// ambil data harga sesuai kode brg dari tabel tm_pembelian_detail, dan juga no sj & tgl_sj
							$query3	= $this->db->query(" SELECT a.qty, a.harga, b.no_sj, b.tgl_sj FROM tm_pembelian_detail a, tm_pembelian b, 
							tm_pembelian_nofaktur c, tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id 
							AND c.id=d.id_pembelian_nofaktur AND c.no_faktur = '$no_faktur'
							AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier 
							AND c.kode_supplier = '$row1->kode_supplier' AND a.kode_brg = '$row2->kode_brg' ");
							$hasilrow = $query3->row();
							$harganya = $hasilrow->harga;
							$qty_faktur = $hasilrow->qty;
							$no_sj = $hasilrow->no_sj;
							$tgl_sj = $hasilrow->tgl_sj;
							
							$pisah1 = explode("-", $tgl_sj);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$tgl_sj = $tgl1."-".$bln1."-".$thn1;
							$tgl_faktur = '';
							//=================================================						
													
					} // end if no_faktur != ''
					else {
						$tgl_faktur = '';
						$no_sj = '';
						$tgl_sj = '';
						$qty_faktur = 0;
					}
						
						if ($row2->id_pembelian_detail != '0') {	
							// ambil data harga sesuai kode brg dari tabel tm_pembelian_detail, dan juga no sj & tgl_sj
							$query3	= $this->db->query(" SELECT a.harga, a.qty, b.no_sj, b.tgl_sj FROM tm_pembelian_detail a, 
							tm_pembelian b WHERE a.id_pembelian = b.id AND a.id = '$row2->id_pembelian_detail' ");
							$hasilrow = $query3->row();
							$harganya = $hasilrow->harga;
							$qty_faktur = $hasilrow->qty;
							$no_sj = $hasilrow->no_sj;
							$tgl_sj = $hasilrow->tgl_sj;
							
							$pisah1 = explode("-", $tgl_sj);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$tgl_sj = $tgl1."-".$bln1."-".$thn1;
							$tgl_faktur = '';
							//=================================================
																			
						} // end if id_pembelian_detail != '0'
						
				
						$detail_retur[] = array(		
										'id'=> $row2->id,
										'kode_brg'=> $row2->kode_brg,
									//	'harga'=> $harganya,
										'harga'=> $row2->harga,
										'is_luar_range'=>$row2->is_luar_range,
										'nama'=> $nama_brg,
										'satuan'=> $satuan,
										'qty_faktur'=> $qty_faktur,
										'qty'=> $row2->qty,
										'no_sj'=> $no_sj,
										'tgl_sj'=> $tgl_sj
										);
					}
				}
				else {
					$detail_retur = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$nama_supplier	= $hasilrow->nama;
				
				$pisah1 = explode("-", $row1->faktur_date_from);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$faktur_date_from = $tgl1."-".$bln1."-".$thn1;
				
				$pisah2 = explode("-", $row1->faktur_date_to);
				$tgl2= $pisah2[2];
				$bln2= $pisah2[1];
				$thn2= $pisah2[0];
				$faktur_date_to = $tgl2."-".$bln2."-".$thn2;
				 
				$data_retur[] = array(		'id'=> $row1->id,	
											//'list_no_faktur'=> $list_no_faktur,	
											'tgl_retur'=> $row1->tgl_retur,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'no_dn_retur'=> $row1->no_dn_retur,
											'faktur_date_from'=> $faktur_date_from,
											'faktur_date_to'=> $faktur_date_to,
											'detail_retur'=> $detail_retur
											);
				$detail_retur = array();
				//$list_no_faktur = '';
	}
	return $data_retur;
  }
  
  function get_sj_retur($jnsaction, $supplier, $no_notanya, $cari, $pkp) {
	  // ambil data pembelian 
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else { // utk proses edit
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' OR no_faktur <> '' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' OR no_nota = '$no_notanya' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' AND a.kode_supplier = '$supplier' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND (a.status_nota = 'f' OR a.no_nota = '$no_notanya') 
						AND a.kode_supplier = '$supplier' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql); //
			}
		}
	}
	else {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' 
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND (a.status_nota = 'f' OR a.no_nota = '$no_notanya' )
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' AND a.kode_supplier = '$supplier'
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND (a.status_nota = 'f' OR a.no_nota = '$no_notanya' ) 
						AND a.kode_supplier = '$supplier'
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
	}
		$data_fb = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$tot_sj = 0;
				// ambil sum total per faktur dari SJ yg dipilih (blm beres 280411)
				
				// 1. query perulangan dari tabel tm_retur_beli_faktur
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT SUM(qty*harga) as jumlahnya FROM tm_retur_beli_detail 
						WHERE id_retur_beli = '$row1->id' AND id_retur_beli_faktur = '$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jumlahnya	= $hasilrow->jumlahnya;
							$tot_sj+= $jumlahnya;
						}
						else {
							$jumlahnya	= '0';
						}
					}
				}
				
				$query3	= $this->db->query(" SELECT SUM(qty*harga) as jumlahnya FROM tm_retur_beli_detail 
						WHERE id_retur_beli = '$row1->id' AND id_retur_beli_faktur = '0' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jumlahnya	= $hasilrow->jumlahnya;
					$tot_sj+= $jumlahnya;
				}
				else {
					$jumlahnya	= '0';
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_dn_retur'=> $row1->no_dn_retur,
											'tgl_retur'=> $row1->tgl_retur,
											'tgl_update'=> $row1->tgl_update,
											'tot_sj'=> $tot_sj
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function savenota($no_fp, $tgl_fp, $supplier, $jum, $no_sj){  
    $tgl = date("Y-m-d");
	$list_sj = explode(",", $no_sj); 

	$data_header = array(
			  'no_nota'=>$no_fp,
			  'tgl_nota'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'kode_supplier'=>$supplier,
			  'jumlah'=>$jum
			);
	$this->db->insert('tm_nota_retur_beli',$data_header);
	
	// ambil data terakhir di tabel tm_nota_retur_beli
	$query2	= $this->db->query(" SELECT id FROM tm_nota_retur_beli ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_nota_retur_beli'=>$id_pf,
			  'no_dn_retur'=>$row1
			);
			$this->db->insert('tm_nota_retur_beli_sj',$data_detail);
			
			// update status_nota di tabel tm_retur_beli
			$this->db->query(" UPDATE tm_retur_beli SET no_nota = '$no_fp', status_nota = 't' 
								WHERE no_dn_retur = '$row1' AND kode_supplier = '$supplier' ");
		}
	}
   
  }
  
  function getAllnota($num, $offset, $supplier, $cari) {
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_nota_retur_beli ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_nota_retur_beli
							WHERE kode_supplier = '$supplier' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_nota_retur_beli where kode_supplier = '$supplier' AND 
							(UPPER(no_nota) like UPPER('%$cari%') ) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_nota_retur_beli where 
							(UPPER(no_nota) like UPPER('%$cari%')) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				//echo $totalnya; die();
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b, tm_nota_retur_beli a 
				WHERE a.kode_supplier = b.kode_supplier AND a.id = '$row1->id' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				// ambil detail data no & tgl SJ
				$query2	= $this->db->query(" SELECT a.no_dn_retur FROM tm_nota_retur_beli_sj a, tm_nota_retur_beli b 
							WHERE a.id_nota_retur_beli = b.id AND
							b.no_nota = '$row1->no_nota' AND b.kode_supplier = '$kode_supplier' ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_dn_retur	= $row2->no_dn_retur;
						
						$sql = "SELECT tgl_retur from tm_retur_beli where no_dn_retur = '$no_dn_retur' 
								AND kode_supplier= '$kode_supplier'";
						$query3	= $this->db->query($sql);
					//	if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_sj	= $hasilrow3->tgl_retur;
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_dn_retur'=> $no_dn_retur,
											'tgl_sj'=> $tgl_sj
											);		
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_nota);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_nota = $tgl1." ".$nama_bln." ".$thn1;
						
					$pisah1 = explode("-", $row1->tgl_input);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_input = $tgl1." ".$nama_bln." ".$thn1;
						
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,	
											'tgl_nota'=> $tgl_nota,	
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,	
											'tgl_input'=> $tgl_input,
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAllnotatanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli
							WHERE kode_supplier = '$supplier' ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where kode_supplier = '$supplier' AND 
							(UPPER(no_nota) like UPPER('%$cari%') )  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where (UPPER(no_nota) like UPPER('%$cari%'))  ");
	}
    
    return $query->result();  
  }
  
  function get_nota($id_nota){
	$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where id='$id_nota' ");    
	
	$data_nota = array();
	$detail_sj = array();
	$no_faktur_pajak = '';
	$tgl_faktur_pajak = '';
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT no_dn_retur FROM tm_nota_retur_beli_sj 
									WHERE id_nota_retur_beli = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sj = '';
				foreach ($hasil2 as $row2) {
					$no_sj .= $row2->no_dn_retur.", ";
					
					// 24 mei 2011
					// ambil salah satu no faktur (1 faktur pajak = banyak faktur beli, 
					// berarti faktur2 yg dipilih di retur itu faktur pajaknya sama)
					$query3	= $this->db->query(" SELECT b.no_faktur
							FROM tm_retur_beli a, tm_retur_beli_faktur b 
							WHERE a.id = b.id_retur_beli AND
							a.no_dn_retur = '$row2->no_dn_retur' LIMIT 1 ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$no_faktur	= $hasilrow->no_faktur;
					}
					else
						$no_faktur = '';
					
					// ambil no faktur pajak
					$query3	= $this->db->query(" SELECT a.no_faktur_pajak, a.tgl_faktur_pajak FROM 
								tm_pembelian_pajak a, tm_pembelian_pajak_detail b
								WHERE a.id = b.id_pembelian_pajak AND b.no_faktur = '$no_faktur' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$no_faktur_pajak	= $hasilrow->no_faktur_pajak;
						$tgl_faktur_pajak	= $hasilrow->tgl_faktur_pajak;
					}
					else {
						$no_faktur_pajak = '';
						$tgl_faktur_pajak= '';
					}
					
					$sql = " SELECT b.kode_brg, b.qty, b.harga
							FROM tm_retur_beli a, tm_retur_beli_detail b 
							WHERE a.id = b.id_retur_beli AND
							a.no_dn_retur = '$row2->no_dn_retur' ";
					//echo($sql."<br>");
					
					$query3	= $this->db->query(" SELECT b.kode_brg, b.qty, b.harga
							FROM tm_retur_beli a, tm_retur_beli_detail b 
							WHERE a.id = b.id_retur_beli AND
							a.no_dn_retur = '$row2->no_dn_retur' ");
					if ($query3->num_rows() > 0){
						$hasil3 = $query3->result();
						foreach ($hasil3 as $row3) {
								$query4	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row3->kode_brg' ");
								if ($query4->num_rows() > 0){
									$hasilrow = $query4->row();
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$nama_brg	= '';
									$satuan	= '';
								}
								
								$detail_brg[] = array(		
											'kode_brg'=> $row3->kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row3->qty,
											'harga'=> $row3->harga );
						}
					}
					else {
							$detail_brg = '';
					}
				}
				
				// ambil data supplier
				$query4	= $this->db->query(" SELECT nama_npwp, alamat, npwp FROM tm_supplier 
				WHERE kode_supplier = '$row1->kode_supplier' ");
					if ($query4->num_rows() > 0){
						$hasilrow = $query4->row();
						$nama_npwp	= $hasilrow->nama_npwp;
						$alamat	= $hasilrow->alamat;
						$npwp	= $hasilrow->npwp;
					}
					else {
						$nama_npwp	= '';
						$alamat	= '';
						$npwp	= '';
					}
				
				$pisah1 = explode("-", $row1->tgl_nota);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_nota = $tgl1."-".$bln1."-".$thn1;						
			
				$data_nota[] = array(		'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,
											'tgl_nota'=> $tgl_nota,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_npwp'=> $nama_npwp,
											'alamat'=> $alamat,
											'npwp'=> $npwp,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_faktur_pajak'=> $no_faktur_pajak,
											'tgl_faktur_pajak'=> $tgl_faktur_pajak,
											'no_sj'=> $no_sj,
											'detail_brg'=> $detail_brg
											);
				$detail_brg = array();
			} // endforeach header
	}
    return $data_nota; 
  }
  
  function deletenotaretur($kode){    	
	//semua no_sj di tabel tm_retur_beli yg bersesuaian dgn tm_nota_retur_beli dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.kode_supplier, b.no_dn_retur FROM tm_nota_retur_beli a, tm_nota_retur_beli_sj b
							WHERE a.id = b.id_nota_retur_beli AND a.id = '$kode' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$this->db->query(" UPDATE tm_retur_beli SET status_nota = 'f', no_nota = '' 
							WHERE kode_supplier = '$row1->kode_supplier' AND no_dn_retur = '$row1->no_dn_retur' ");
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di nota_retur_beli dan nota_retur_beli_sj
	$this->db->query(" DELETE FROM tm_nota_retur_beli_sj where id_nota_retur_beli = '$kode' ");
	$this->db->query(" DELETE FROM tm_nota_retur_beli where id = '$kode' ");

  }
  
  function konversi_angka2romawi($bln_now) {
	if ($bln_now == "01")
		$romawi_bln = "I";
	else if ($bln_now == "02")
		$romawi_bln = "II";
	else if ($bln_now == "03")
		$romawi_bln = "III";
	else if ($bln_now == "04")
		$romawi_bln = "IV";
	else if ($bln_now == "05")
		$romawi_bln = "V";
	else if ($bln_now == "06")
		$romawi_bln = "VI";
	else if ($bln_now == "07")
		$romawi_bln = "VII";
	else if ($bln_now == "08")
		$romawi_bln = "VIII";
	else if ($bln_now == "09")
		$romawi_bln = "IX";
	else if ($bln_now == "10")
		$romawi_bln = "X";
	else if ($bln_now == "11")
		$romawi_bln = "XI";
	else if ($bln_now == "12")
		$romawi_bln = "XII";
		
	return $romawi_bln;
  }
  
  function konversi_romawi2angka($blnrom) {
	if ($blnrom == "I")
		$bln_now = "01";
	else if ($blnrom == "II")
		$bln_now = "02";
	else if ($blnrom == "III")
		$bln_now = "03";
	else if ($blnrom == "IV")
		$bln_now = "04";
	else if ($blnrom == "V")
		$bln_now = "05";
	else if ($blnrom == "VI")
		$bln_now = "06";
	else if ($blnrom == "VII")
		$bln_now = "07";
	else if ($blnrom == "VIII")
		$bln_now = "08";
	else if ($blnrom == "IX")
		$bln_now = "09";
	else if ($blnrom == "X")
		$bln_now = "10";
	else if ($blnrom == "XI")
		$bln_now = "11";
	else if ($blnrom == "XII")
		$bln_now = "12";
		
	return $bln_now;
  }
  
  function konversi_bln2deskripsi($bln1) {
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";  
		return $nama_bln;
  }
  
  function get_bahan($num, $offset, $cari, $kode_supplier)
  {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b
				WHERE a.satuan = b.id
				AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b
				WHERE a.satuan = b.id
				AND a.status_aktif = 't' AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
				ORDER BY a.nama_brg ";
		$this->db->select($sql, false)->limit($num,$offset);	
    } 
   // echo $sql; die();

    $query = $this->db->get();
    return $query->result();
  }
  
  function get_bahantanpalimit($cari, $kode_supplier){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b
				WHERE a.satuan = b.id
				AND a.status_aktif = 't' ";
		$query	= $this->db->query($sql);
		return $query->result();
	}
	else {
		$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b
				WHERE a.satuan = b.id
				AND a.status_aktif = 't' AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";		
		$query	= $this->db->query($sql);
		return $query->result();
	}
  }

}
