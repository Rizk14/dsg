<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
   /* $this->db->select('*');
    $this->db->from('tm_jenis_barang');
    $this->db->order_by('tgl_update','DESC');
    $query = $this->db->get(); */
    
    $query	= $this->db->query(" SELECT a.*, b.nama as nama_kel FROM tm_jenis_barang a, tm_kelompok_barang b 
				WHERE a.kode_kel_brg = b.kode ORDER BY a.kode_kel_brg, a.kode ");
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_jenis_barang',array('id'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE id = '$id' ");
    return $query->result();		  
  }
  
  //
  function save($id_jenis, $kode, $kel_brg, $nama, $goedit, $kodeeditjenis, $kodeeditkel){  
    $tgl = date("Y-m-d H:i:s");
    $data = array(
      'kode'=>$kode,
      'kode_kel_brg'=>$kel_brg,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_jenis_barang',$data); 
	}
	else {
		
		$data = array(
		  'kode'=>$kode,
		  'kode_kel_brg'=>$kel_brg,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_jenis);
		$this->db->update('tm_jenis_barang',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_jenis_barang', array('id' => $id));
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data($kel_brg, $kode){
    $this->db->select(" id FROM tm_jenis_barang WHERE kode = '$kode' AND kode_kel_brg = '$kel_brg' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}

