<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_jenis_makloon');
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    $query = $this->db->getwhere('tm_jenis_makloon',array('id'=>$id));
    return $query->result();		  
  }
  
  //
  function save($id_satuan, $nama, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_jenis_makloon',$data); }
	else {
		
		$data = array(
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_satuan);
		$this->db->update('tm_jenis_makloon',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_jenis_makloon', array('id' => $id));
  }
  
  function cek_data($nama){
    $this->db->select("* FROM tm_jenis_makloon WHERE nama = '$nama' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
