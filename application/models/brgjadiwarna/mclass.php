<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	function delete($id){
		$db2=$this->load->database('db_external', TRUE);
		$db2->query(" DELETE FROM tr_product_color WHERE i_product_motif = '$id' ");
		
		redirect('brgjadiwarna/cform');
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		//return $db2->query(" SELECT * FROM tr_product_color ORDER BY i_product_motif ", false);
		$sqlnya	= $db2->query(" SELECT distinct a.i_product_motif, b.e_product_motifname 
							FROM tr_product_color a, tr_product_motif b
							WHERE a.i_product_motif = b.i_product_motif
							ORDER BY a.i_product_motif ");
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
										WHERE a.i_color = b.i_color AND a.i_product_motif = '".$rownya->i_product_motif."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'i_color'=> $rowxx->i_color,
												'e_color_name'=> $rowxx->e_color_name
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->i_product_motif,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	
	function viewperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		//return $db2->query(" SELECT * FROM tr_product_color ORDER BY i_product_motif ", false);
		$sqlnya	= $db2->query(" SELECT distinct a.i_product_motif, b.e_product_motifname 
							FROM tr_product_color a, tr_product_motif b
							WHERE a.i_product_motif = b.i_product_motif
							ORDER BY a.i_product_motif LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
										WHERE a.i_color = b.i_color AND a.i_product_motif = '".$rownya->i_product_motif."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'i_color'=> $rowxx->i_color,
												'e_color_name'=> $rowxx->e_color_name
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->i_product_motif,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}

	/*function viewperpages($limit,$offset) {
		$query	= $db2->query( " SELECT * FROM tr_product_color ORDER BY i_product_motif LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	} */
	
	function listpquery($tabel,$order,$filter) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		if ($query->num_rows() > 0) {
			return $result	= $query->result(); 
		}
	}
	
	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				WHERE b.n_active='1' ORDER BY b.i_product_motif ASC ");
	}
	
	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$strqry	= ("
			SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				WHERE b.n_active='1' ORDER BY b.i_product_motif ASC "." LIMIT ".$limit." OFFSET ".$offset);
		
		$query	= $db2->query($strqry);
			
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		return $db2->query("
			SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				WHERE b.n_active='1' AND b.i_product_motif='$ky_upper' ORDER BY b.i_product_motif ASC " );
	}
		
	function icolor() {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_color ORDER BY i_color ASC ");
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function ecolor($icolor){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT e_color_name FROM tr_color WHERE i_color='$icolor' ");
	}
	
	// 03-01-2014
	function msimpan($iproduct,$icolor) {
		$db2=$this->load->database('db_external', TRUE);
		$tgl = date("Y-m-d H:i:s");
		
		$listwarna = explode(";", $icolor);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $db2->query(" SELECT i_product_color FROM tr_product_color 
									 WHERE i_product_motif = '$iproduct' AND i_color = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $db2->query(" SELECT i_product_color FROM tr_product_color ORDER BY i_product_color DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$i_product_color	= $hasilrow->i_product_color;
					$new_i_product_color = $i_product_color+1;
					
					$datanya	= array(
							 'i_product_color'=>$new_i_product_color,
							 'i_product_motif'=>$iproduct,
							 'i_color'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$db2->insert('tr_product_color',$datanya);
				} // end if
			}
		}
			
		redirect('brgjadiwarna/cform/');
	}
	
	// 06-01-2014 --------------------------------------------------------------------
	function mupdate($iproduct,$icolor) {
		$db2=$this->load->database('db_external', TRUE);
		$tgl = date("Y-m-d H:i:s");
		
		// 08-09-2014, ini dikomen aja. menyesatkan.
		//$db2->query(" DELETE FROM tr_product_color WHERE i_product_motif = '$iproduct' ");
		
		$listwarna = explode(";", $icolor);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $db2->query(" SELECT i_product_color FROM tr_product_color 
									 WHERE i_product_motif = '$iproduct' AND i_color = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $db2->query(" SELECT i_product_color FROM tr_product_color ORDER BY i_product_color DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$i_product_color	= $hasilrow->i_product_color;
					$new_i_product_color = $i_product_color+1;
					
					$datanya	= array(
							 'i_product_color'=>$new_i_product_color,
							 'i_product_motif'=>$iproduct,
							 'i_color'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$db2->insert('tr_product_color',$datanya);
				} // end if
			}
		}
			
		redirect('brgjadiwarna/cform/');
		
	}
	
	//--------------------------------------------------------------------------------
		
	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$id' AND n_active='1' ");
	}
	
	
	
	/*function viewcari($cari) {
		return $db2->query( " SELECT * FROM tr_product_motif WHERE n_active='1' AND (UPPER(i_product_motif) LIKE UPPER('$cari%') OR UPPER(i_product) LIKE UPPER('$cari%') OR UPPER(e_product_motifname) LIKE UPPER('$cari%')) ORDER BY i_product_motif ASC, e_product_motifname ASC " );
	} */
	
	// 04-01-2014
	function viewcari($cari) {
		$db2=$this->load->database('db_external', TRUE);
		//return $db2->query(" SELECT * FROM tr_product_color ORDER BY i_product_motif ", false);
		$sqlnya	= $db2->query(" SELECT distinct a.i_product_motif, b.e_product_motifname 
							FROM tr_product_color a, tr_product_motif b
							WHERE a.i_product_motif = b.i_product_motif 
							AND (UPPER(a.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%'))
							ORDER BY a.i_product_motif ");
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
										WHERE a.i_color = b.i_color AND a.i_product_motif = '".$rownya->i_product_motif."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'i_color'=> $rowxx->i_color,
												'e_color_name'=> $rowxx->e_color_name
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->i_product_motif,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	//----------------------------
	
	/*function mcari($cari,$limit,$offset) {
		
		$query	= $db2->query(" SELECT * FROM tr_product_motif WHERE n_active='1' AND (UPPER(i_product_motif) LIKE UPPER('$cari%') OR UPPER(i_product) LIKE UPPER('$cari%') OR UPPER(e_product_motifname) LIKE UPPER('$cari%')) ORDER BY i_product_motif ASC, e_product_motifname ASC LIMIT ".$limit." OFFSET ".$offset,false);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	} */
	
	function mcari($cari,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		//return $db2->query(" SELECT * FROM tr_product_color ORDER BY i_product_motif ", false);
		$sqlnya	= $db2->query(" SELECT distinct a.i_product_motif, b.e_product_motifname 
							FROM tr_product_color a, tr_product_motif b
							WHERE a.i_product_motif = b.i_product_motif
							AND (UPPER(a.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%'))
							ORDER BY a.i_product_motif LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $db2->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
										WHERE a.i_color = b.i_color AND a.i_product_motif = '".$rownya->i_product_motif."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'i_color'=> $rowxx->i_color,
												'e_color_name'=> $rowxx->e_color_name
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->i_product_motif,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
}
?>
