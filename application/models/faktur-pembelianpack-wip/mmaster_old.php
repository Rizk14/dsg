<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $supplier, $cari, $date_from, $date_to) {
	/*if ($cari == '') {		
		$this->db->select(" * FROM tm_pp WHERE jenis_brg = '$jenis_brg' AND status_faktur = 'f' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	} */ 
	//09-05-2012
	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$this->db->select(" * FROM tm_pembelian_nofaktur WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	
	/*if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_pembelian_nofaktur ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_pembelian_nofaktur
							WHERE kode_supplier = '$supplier' ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_pembelian_nofaktur where kode_supplier = '$supplier' AND 
							(UPPER(no_faktur) like UPPER('%$cari%') ) ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_pembelian_nofaktur where 
							(UPPER(no_faktur) like UPPER('%$cari%')) ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	} */
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			/*	$query2	= $this->db->query(" SELECT SUM(a.total) as totnya FROM tm_pembelian a, tm_pembelian_nofaktur b, tm_pembelian_nofaktur_sj c 
									WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
									AND b.id = c.id_pembelian_nofaktur
									AND b.no_faktur = '$row1->no_faktur' ");
				if ($query2->num_rows() > 0) { //
					$hasilrow = $query2->row();
					$totalnya	= $hasilrow->totnya;				
				}
				else
					$totalnya = '0'; */
				
				//echo $totalnya; die();
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b, tm_pembelian_nofaktur a 
				WHERE a.kode_supplier = b.kode_supplier AND a.id = '$row1->id' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				// ambil detail data no & tgl SJ
				$query2	= $this->db->query(" SELECT a.no_sj FROM tm_pembelian_nofaktur_sj a, tm_pembelian_nofaktur b 
							WHERE a.id_pembelian_nofaktur = b.id AND
							b.no_faktur = '$row1->no_faktur' AND b.kode_supplier = '$kode_supplier' ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_sj	= $row2->no_sj;
						
						$sql = "SELECT tgl_sj from tm_pembelian where no_sj = '$no_sj' 
								AND kode_supplier= '$kode_supplier' AND status_aktif = 't'";
						$query3	= $this->db->query($sql);
					//	if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_sj	= $hasilrow3->tgl_sj;
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj
											);		
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						
					$pisah1 = explode("-", $row1->tgl_input);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						//$tgl_input = $tgl1." ".$nama_bln." ".$thn1;
						$exptgl1 = explode(" ", $tgl1);
						$tgl1nya= $exptgl1[0];
						$jam1nya= $exptgl1[1];
						$tgl_input = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
						
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
						$exptgl1 = explode(" ", $tgl1);
						$tgl1nya= $exptgl1[0];
						$jam1nya= $exptgl1[1];
						$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,	
											'no_faktur'=> $row1->no_faktur,	
											'tgl_faktur'=> $tgl_faktur,	
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,	
											'tgl_input'=> $tgl_input,
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb,
											'status_faktur_pajak'=> $row1->status_faktur_pajak,
											'status_lunas'=> $row1->status_lunas
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari, $date_from, $date_to){
	/*if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur
							WHERE kode_supplier = '$supplier' ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur where kode_supplier = '$supplier' AND 
							(UPPER(no_faktur) like UPPER('%$cari%') )  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur where (UPPER(no_faktur) like UPPER('%$cari%'))  ");
	} */
	//09-05-2012
	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$query = $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE TRUE ".$pencarian);
    
    return $query->result();  
  }
      
  function cek_data($no_fp, $supplier){
    $this->db->select("id from tm_pembelian_nofaktur WHERE no_faktur = '$no_fp' AND kode_supplier = '$supplier' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_fp, $tgl_fp, $supplier, $jenis_pembelian, $jum, $no_sj){  
    //$tgl = date("Y-m-d");
    $tgl = date("Y-m-d H:i:s");
	$list_sj = explode(",", $no_sj); 

	$data_header = array(
			  'no_faktur'=>$no_fp,
			  'tgl_faktur'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'kode_supplier'=>$supplier,
			  'jenis_pembelian'=>$jenis_pembelian,
			  'jumlah'=>$jum
			);
	$this->db->insert('tm_pembelian_nofaktur',$data_header);
	
	// ambil data terakhir di tabel tm_pembelian_nofaktur
	$query2	= $this->db->query(" SELECT id FROM tm_pembelian_nofaktur ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_pembelian_nofaktur'=>$id_pf,
			  'no_sj'=>$row1
			);
			$this->db->insert('tm_pembelian_nofaktur_sj',$data_detail);
			
			//10-04-2012, update jenis pembelian
			// update status_faktur di tabel tm_pembelian
			$this->db->query(" UPDATE tm_pembelian SET no_faktur = '$no_fp', status_faktur = 't', jenis_pembelian = '$jenis_pembelian' 
								WHERE no_sj = '$row1' AND kode_supplier = '$supplier' ");
		}
	}
   
  }
    
  function delete($kode){    	
	//semua no_sj di tabel tm_pembelian yg bersesuaian dgn tm_pembelian_nofaktur dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.kode_supplier, b.no_sj FROM tm_pembelian_nofaktur a, tm_pembelian_nofaktur_sj b
							WHERE a.id = b.id_pembelian_nofaktur AND a.id = '$kode' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$this->db->query(" UPDATE tm_pembelian SET status_faktur = 'f', no_faktur = '' 
							WHERE kode_supplier = '$row1->kode_supplier' AND no_sj = '$row1->no_sj' ");
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di pembelian_nofaktur dan pembelian_nofaktur_sj
	$this->db->query(" DELETE FROM tm_pembelian_nofaktur_sj where id_pembelian_nofaktur= '$kode' ");
	$this->db->query(" DELETE FROM tm_pembelian_nofaktur where id = '$kode' ");

  }
    
  //function get_pembelian($num, $offset, $jnsaction, $supplier, $cari) {
	function get_pembelian($jnsaction, $supplier, $no_fakturnya, $cari) {
	  // ambil data pembelian 
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' AND status_aktif = 't'
								order by kode_supplier, tgl_sj DESC ");
			}
			else { // utk proses edit
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' OR no_faktur <> '' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' AND status_aktif = 't'
				OR no_faktur = '$no_fakturnya' order by kode_supplier, tgl_sj DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' 
						AND kode_supplier = '$supplier' AND status_aktif = 't'
						order by kode_supplier, tgl_sj DESC ");
			}
			else {
				//$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE ( status_faktur = 'f' OR no_faktur = '$no_fakturnya' ) 
							AND kode_supplier = '$supplier' AND status_aktif = 't'
							order by kode_supplier, tgl_sj DESC "); //
			}
		}
	}
	else {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' AND 
				//(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' AND status_aktif = 't'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" select * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) AND status_aktif = 't'
				order by kode_supplier, tgl_sj DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" select * FROM tm_pembelian WHERE status_faktur = 'f' 
				AND kode_supplier = '$supplier' AND status_aktif = 't'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by kode_supplier, tgl_sj DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" select * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND kode_supplier = '$supplier' AND status_aktif = 't' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by kode_supplier, tgl_sj DESC ");
			}
		}
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query2	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query2->row();
				$nama_supplier	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'total'=> $row1->total,
											'total_pajak'=> $row1->total_pajak
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function get_pembeliantanpalimit($jnsaction, $supplier, $no_fakturnya, $cari){
	if ($cari == "all") {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$query = $this->db->getwhere('tm_pembelian',array('status_faktur'=>'f' ));
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f'
									OR no_faktur = '$no_fakturnya' ");
			}			
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$query = $this->db->getwhere('tm_pembelian',array('status_faktur'=>'f',
																'kode_supplier'=>$supplier)); */
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE (status_faktur = 'f' 
									OR no_faktur = '$no_fakturnya') AND kode_supplier = '$supplier' ");
			}
		}
	}
	else {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' 
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya')
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' 
								AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
									AND kode_supplier = '$supplier' 
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
	}
    
    return $query->result();  
  }
  
  function get_detail_op($id_op, $jenis_brg, $list_brg){
    //$query = $this->db->getwhere('tm_pp_detail',array('id_pp'=>$id_pp));
    //$hasil = $query->result();
    $detail_pp = array();
        
    foreach($list_brg as $row1) {
			//$row1 = intval($row1);
			if ($row1 != '') {
			$query2	= $this->db->query(" SELECT kode_brg, qty FROM tm_op_detail WHERE id = '$row1' ");
			$hasilrow = $query2->row();
			$kode_brg	= $hasilrow->kode_brg;
			$qty	= $hasilrow->qty;
    
    //foreach ($hasil as $row1) {
		/*	$query2	= $this->db->query(" SELECT nama_brg, harga FROM tm_bahan_baku WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query2->row();
			$nama_brg	= $hasilrow->nama_brg;
			$harga	= $hasilrow->harga; */
			
			if ($jenis_brg == '1')
				$nama_tabel = "tm_bahan_baku";
			else if ($jenis_brg == '2')
				$nama_tabel = "tm_asesoris";
			else if ($jenis_brg == '3')
				$nama_tabel = "tm_bahan_pendukung";
			else if ($jenis_brg == '4')
				$nama_tabel = "tm_perlengkapan";
			
			$query2	= $this->db->query(" SELECT nama_brg, harga FROM ".$nama_tabel." WHERE kode_brg = '$kode_brg' ");
			$hasilrow = $query2->row();
			$nama_brg	= $hasilrow->nama_brg;
			$harga	= $hasilrow->harga;
		
		/*	$detail_pp[] = array('kode_brg'=> $row1->kode_brg,
											'nama'=> $nama_brg,
											'harga'=> $harga,
											'qty'=> $row1->qty,
											'keterangan'=> $row1->keterangan
										); */
			$detail_op[] = array(		'id'=> $row1,
										'kode_brg'=> $kode_brg,
											'nama'=> $nama_brg,
											'harga'=> $harga,
											'qty'=> $qty
								);
		}
	}
	return $detail_op;
  }
      
  function get_faktur($id_faktur){
	$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur where id='$id_faktur' ");    
	
	$data_faktur = array();
	$detail_sj = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT no_sj FROM tm_pembelian_nofaktur_sj 
									WHERE id_pembelian_nofaktur = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sj = '';
				foreach ($hasil2 as $row2) {
					$no_sj .= $row2->no_sj.", ";
					
				}
				
				$pisah1 = explode("-", $row1->tgl_faktur);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_faktur = $tgl1."-".$bln1."-".$thn1;						
			
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_supplier'=> $row1->kode_supplier,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_sj'=> $no_sj
											);
			} // endforeach header
	}
    return $data_faktur; // sampe sini 10 maret 2011  
  }
  
  function get_supplier(){
	//$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori= '1' ORDER BY kode_supplier ");    
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  

}
