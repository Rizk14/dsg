<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id, status_approve FROM tt_stok_opname_hasil_quilting
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
				
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
		}
		else {
			$status_approve	= '';
			$idnya = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya
							);
							
		return $so_bahan;
  }
  
  function get_all_stok_opname($bulan, $tahun) {

		$query	= $this->db->query(" SELECT a.id as id_header, b.*, c.nama_brg, c.satuan FROM tt_stok_opname_hasil_quilting_detail b, 
					tt_stok_opname_hasil_quilting a, tm_brg_hasil_makloon c
					WHERE b.id_stok_opname_hasil_quilting = a.id 
					AND b.kode_brg = c.kode_brg
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY c.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil nama brg dan stok terkini
				/*$query3	= $this->db->query(" SELECT a.kode_brg, a.stok, b.nama_brg, c.nama as satuan 
					FROM tm_stok_hasil_makloon a, tm_brg_hasil_makloon b, tm_satuan c 
					WHERE b.satuan = c.id AND a.kode_brg = b.kode_brg AND b.kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->satuan;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg	= '';
					$satuan	= '';
					$stok = '';
				}*/
				
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $nama_satuan,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  } 

}

