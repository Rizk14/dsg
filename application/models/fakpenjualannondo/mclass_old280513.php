<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function cari_fpenjualan($fpenj) {
		return $this->db->query(" SELECT * FROM tm_faktur WHERE i_faktur_code=trim('$fpenj') AND f_faktur_cancel='f' ");
	}
	
	function cari_fpajak($fpajak,$tahun) {
		return $this->db->query(" SELECT * FROM tm_faktur WHERE substring(i_faktur_code,1,4)='$tahun' and i_faktur_pajak='$fpajak' AND f_faktur_cancel='f' ");
	}
	
	function getnomorfaktur() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' 
		
		ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function nofakturpajak() {
		$thn = date("Y");
		return $this->db->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	function cari_fpenjualan_do($fpenj) {
		return $this->db->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur_code=trim('$fpenj') AND f_faktur_cancel='f' ");
	}

	function cari_fpajak_do($fpajak,$tahun) {
		return $this->db->query(" SELECT * FROM tm_faktur_do_t WHERE substring(i_faktur_code,1,4)='$tahun' AND i_faktur_pajak='$fpajak' AND f_faktur_cancel='f' ");
	}
		
	function tahunnow_do() {
		$thn	= date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thnfaktur 
			
			FROM tm_faktur_do_t 
			
			WHERE SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' 
			
			ORDER BY i_faktur DESC LIMIT 1 ");	
	}
	
	function getnomorfaktur_do() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur_do() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_do(){
		$thn = date("Y");
		return $this->db->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}	
	
	function getnomorfaktur_bhnbaku() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur_bhnbaku() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_bhnbaku() {
		$thn = date("Y");
		return $this->db->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	function lcabang() {
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$this->db->select(" a.e_initial AS codebranch, 
				    a.i_customer AS codecustomer, 
				    a.e_branch_name AS branch,
					a.e_initial AS einitial FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
				    
		$query	= $this->db->get();
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}else{
			return false;
		}
	}
	
	function lbarangjadiperpages($ibranch,$limit,$offset) {
		$query	= $this->db->query(" SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode, 
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND a.f_faktur_created='f' AND c.e_initial='$ibranch' AND a.n_unit_akhir > 0
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 a.v_unit_price, b.i_sj_code
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	LIMIT ".$limit." OFFSET ".$offset);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}	
	}

	function lbarangjadi($ibranch) {
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND a.f_faktur_created='f' AND c.e_initial='$ibranch' AND a.n_unit_akhir > 0
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 a.v_unit_price, b.i_sj_code
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";
			
		return $this->db->query($qstr);
	}

	function flbarangjadi($ibranch,$key) {
		$ky_upper	= $key;
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND a.f_faktur_created='f' AND a.n_unit_akhir > 0 AND (b.i_sj_code='$ky_upper' OR a.i_product='$ky_upper') AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj,
					 b.i_customer,
					 b.i_branch,
					 a.i_product,
					 a.e_product_name,
					 a.v_product_price,
					 a.n_unit,
					 a.n_unit_akhir,
					 a.v_unit_price, b.i_sj_code
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";

		return $this->db->query($qstr);
	}
			
	/*
	function lbarangjadiperpages($ibbk,$limit,$offset){
		$query = $this->db->query("
			SELECT c.i_bbk,
				c.i_product AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				c.v_product_price AS hjp,
				c.n_unit AS qty,
				c.v_unit_price AS nilai
				
			FROM tm_bbk_item c 
			
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
			
			WHERE b.n_active='1' AND d.i_status_bbk='2' AND d.f_faktur_created=false AND c.i_bbk='$ibbk' AND d.f_return=false
			
			ORDER BY d.i_bbk DESC LIMIT ".$limit." OFFSET ".$offset);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi($ibbk){
		return $this->db->query("
			SELECT c.i_bbk,
				c.i_product AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				c.v_product_price AS hjp,
				c.n_unit AS qty,
				c.v_unit_price AS nilai
				
			FROM tm_bbk_item c 
			
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
			
			WHERE b.n_active='1' AND d.i_status_bbk='2' AND d.f_faktur_created=false AND c.i_bbk='$ibbk' AND d.f_return=false
			
			ORDER BY d.i_bbk DESC ");
	}
	
	function flbarangjadi($key,$ibbk) {
		$ky_upper	= strtoupper($key);
		return $this->db->query("
			SELECT c.i_bbk,
				c.i_product AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				c.v_product_price AS hjp,
				c.n_unit AS qty,
				c.v_unit_price AS nilai
				
			FROM tm_bbk_item c 
			
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
			
			WHERE b.n_active='1' AND d.i_status_bbk='2' AND d.f_faktur_created=false AND (b.i_product_motif='$ky_upper' OR a.e_product_basename LIKE '$key') AND c.i_bbk='$ibbk' AND d.f_return=false
			
			ORDER BY d.i_bbk DESC ");
	}
	*/
	/*
	function lpelanggan() {
		return $this->db->query("
			SELECT c.i_bbk AS nomorbbk,
				d.e_bbk_to AS kepada,
				d.e_bbk_address AS alamat,
				d.d_bbk AS tglbbk
							
			FROM tm_bbk_item c 
					
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
						
			WHERE d.i_status_bbk='2' AND d.f_faktur_created=false 
			
			GROUP BY c.i_bbk, d.e_bbk_to, d.e_bbk_address, d.d_bbk
			
			ORDER BY c.i_bbk DESC	
		");	
	}

	function lpelangganperpages($limit,$offset) {
		$query	= $this->db->query("
			SELECT c.i_bbk AS nomorbbk,
				d.e_bbk_to AS kepada,
				d.e_bbk_address AS alamat,
				d.d_bbk AS tglbbk
							
			FROM tm_bbk_item c 
					
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
						
			WHERE d.i_status_bbk='2' AND d.f_faktur_created=false 
			
			GROUP BY c.i_bbk, d.e_bbk_to, d.e_bbk_address, d.d_bbk
			
			ORDER BY c.i_bbk DESC LIMIT ".$limit." OFFSET ".$offset);

		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
					
	}

	function fpelanggan($key) {
		$ky_upper	= strtoupper($key);
		return $this->db->query("
			SELECT c.i_bbk AS nomorbbk,
				d.e_bbk_to AS kepada,
				d.e_bbk_address AS alamat,
				d.d_bbk AS tglbbk
							
			FROM tm_bbk_item c 
					
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
						
			WHERE d.i_status_bbk='2' AND d.f_faktur_created=false AND c.i_bbk='$ky_upper'
			
			GROUP BY c.i_bbk, d.e_bbk_to, d.e_bbk_address, d.d_bbk
			
			ORDER BY c.i_bbk DESC		
		");	
	}
	*/
				
	function msimpan($i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$isjcode) {
		
		$i_faktur_item	= array();
		$tm_faktur_item	= array();
		
		$qty_akhir	= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$this->db->trans_begin();
		
		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	

		$seq_tm_faktur	= $this->db->query(" SELECT cast(i_faktur AS integer) AS i_faktur FROM tm_faktur ORDER BY cast(i_faktur AS integer) DESC LIMIT 1 ");
		
		if($seq_tm_faktur->num_rows() >0 ) {
			$seqrow	= $seq_tm_faktur->row();
			$ifaktur	= $seqrow->i_faktur+1;
		} else {
			$ifaktur	= 1;
		}
		
		$this->db->set(
			array(
			 'i_faktur'=>$ifaktur,
			 'i_faktur_code'=>$i_faktur,
			 'd_faktur'=>$nw_d_faktur,
			 'e_branch_name'=>$i_branch,
			 'd_due_date'=>$nw_d_due_date,
			 'i_faktur_pajak'=>$i_faktur_pajak,
			 'd_pajak'=>$nw_d_pajak,
			 'n_discount'=>$n_discount,
			 'v_discount'=>$nw_v_discount,
			 'v_total_faktur'=>$nw_v_total_faktur,
			 'v_total_fppn'=>$nw_v_total_fppn,
			 'f_printed'=>'FALSE',
			 'f_do_or_nota'=>'FALSE',
			 'e_note_faktur'=>$e_note_faktur,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry));
		
		if($this->db->insert('tm_faktur')) {
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				
				$seq_tm_i_faktur_item	= $this->db->query(" SELECT cast(i_faktur_item AS integer) AS i_faktur_item FROM tm_faktur_item ORDER BY cast(i_faktur_item AS integer) DESC LIMIT 1 ");
				
				if($seq_tm_i_faktur_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_i_faktur_item->row();
					$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
				}else{
					$i_faktur_item[$jumlah]	= 1;
				}
				
				$tm_faktur_item[$jumlah]	= array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$ifaktur,
					 'i_sj'=>$isjcode[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_hjp[$jumlah],
					 'd_entry'=>$dentry );
				
				$q_qty_sj_item	= $this->db->query(" SELECT * FROM tm_sj_item WHERE i_sj='$isjcode[$jumlah]' AND i_product='$i_product[$jumlah]' ");
				
				if($q_qty_sj_item->num_rows()>0) {
					
					$row_item_br	= $q_qty_sj_item->row();
					
					if($n_quantity[$jumlah]==($row_item_br->n_unit_akhir)) {
						
						$qty_akhir[$jumlah]	= (($row_item_br->n_unit_akhir)-$n_quantity[$jumlah]);
						
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);
					} else if($n_quantity[$jumlah] < ($row_item_br->n_unit_akhir)) {
						$qty_akhir[$jumlah]	= (($row_item_br->n_unit_akhir) - $n_quantity[$jumlah]);
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'FALSE'
						);						
					}else{
						$qty_akhir[$jumlah]	= (($row_item_br->n_unit_akhir) - $n_quantity[$jumlah]);
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);						
					}
				}
				
				$this->db->update('tm_sj',$arrfakturupdate[$jumlah],array('i_sj'=>$isjcode[$jumlah]));
				$this->db->update('tm_sj_item',$arrfakturupdate2[$jumlah],array('i_sj'=>$isjcode[$jumlah],'i_product'=>$i_product[$jumlah]));
				$this->db->insert('tm_faktur_item',$tm_faktur_item[$jumlah]);

				if ($this->db->trans_status()===FALSE) {
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
								
			}
			print "<script>alert(\"Nomor Faktur : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");show(\"fakpenjualannondo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"fakpenjualannondo/cform\",\"#content\");</script>";
		}
	}

	function msimpan_old($i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$isjcode) {
		/* Disabled 19-02-2011

msimpan($i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$txt_pel_manual,$ibbk)		
		*/
		
		$i_faktur_item	= array();
		$tm_faktur_item	= array();
		
		$jml_item_br	= array();
		$qty_akhir	= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$this->db->trans_begin();
		
		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	

		$seq_tm_faktur	= $this->db->query(" SELECT cast(i_faktur AS integer) AS i_faktur FROM tm_faktur ORDER BY cast(i_faktur AS integer) DESC LIMIT 1 ");
		
		if($seq_tm_faktur->num_rows() >0 ) {
			$seqrow	= $seq_tm_faktur->row();
			$ifaktur	= $seqrow->i_faktur+1;
		} else {
			$ifaktur	= 1;
		}
		
		/*
		'e_branch_name'=>$i_branch,
		*/
		
		$this->db->set(
			array(
			 'i_faktur'=>$ifaktur,
			 'i_faktur_code'=>$i_faktur,
			 'd_faktur'=>$nw_d_faktur,
			 'e_branch_name'=>$i_branch,
			 'd_due_date'=>$nw_d_due_date,
			 'i_faktur_pajak'=>$i_faktur_pajak,
			 'd_pajak'=>$nw_d_pajak,
			 'n_discount'=>$n_discount,
			 'v_discount'=>$nw_v_discount,
			 'v_total_faktur'=>$nw_v_total_faktur,
			 'v_total_fppn'=>$nw_v_total_fppn,
			 'f_printed'=>'FALSE',
			 'f_do_or_nota'=>'FALSE',
			 'e_note_faktur'=>$e_note_faktur,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry));
		
		if($this->db->insert('tm_faktur')) {
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				$seq_tm_i_faktur_item	= $this->db->query(" SELECT cast(i_faktur_item AS integer) AS i_faktur_item FROM tm_faktur_item ORDER BY cast(i_faktur_item AS integer) DESC LIMIT 1 ");
				
				if($seq_tm_i_faktur_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_i_faktur_item->row();
					$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
				} else {
					$i_faktur_item[$jumlah]	= 1;
				}
				
				/*
				$this->db->set(
					array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$i_faktur,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_unit_price[$jumlah],
					 'd_entry'=>$dentry ));
				*/	 
				
				/*
				'i_faktur'=>$i_faktur,
				*/
				
				$tm_faktur_item[$jumlah]	= array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$ifaktur,
					 'i_sj'=>$isjcode[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_hjp[$jumlah],
					 'd_entry'=>$dentry );
				
				$q_qty_sj_item	= $this->db->query(" SELECT * FROM tm_sj_item WHERE i_sj='$isjcode[$jumlah]' AND i_product='$i_product[$jumlah]' ");
				if($q_qty_sj_item->num_rows()>0) {
					$row_item_br	= $q_qty_sj_item->row();
					$jml_item_br[$jumlah]	= $row_item_br->n_unit_akhir;
					if($n_quantity[$jumlah] == $jml_item_br[$jumlah]) {
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);
					} else if($n_quantity[$jumlah] < $jml_item_br[$jumlah]) { // jika jmlitwm kurang dari qty brg yg ada
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'FALSE'
						);						
					}else{
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);						
					}
				}
				
				$this->db->update('tm_sj',$arrfakturupdate[$jumlah],array('i_sj'=>$isjcode[$jumlah]));
				$this->db->update('tm_sj_item',$arrfakturupdate2[$jumlah],array('i_sj'=>$isjcode[$jumlah],'i_product'=>$i_product[$jumlah]));
				$this->db->insert('tm_faktur_item',$tm_faktur_item[$jumlah]);

				if ($this->db->trans_status()===FALSE) {
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
								
			}
			print "<script>alert(\"Nomor Faktur : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");show(\"fakpenjualannondo/cform\",\"#content\");</script>";
			
			//redirect('fakpenjualannondo/cform/');
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"fakpenjualannondo/cform\",\"#content\");</script>";
		}
	}	
}
?>
