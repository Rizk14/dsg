<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id, status_approve FROM tt_stok_opname_hasil_quilting
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
				
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
		}
		else {
			$status_approve	= '';
			$idnya = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya
							);
							
		return $so_bahan;
  }

  function get_all_stok() {
	/*	$query	= $this->db->query(" SELECT a.kode_brg, a.stok, b.nama_brg, c.nama as satuan 
					FROM tm_stok_hasil_makloon a, tm_brg_hasil_makloon b, tm_satuan c 
					WHERE b.satuan = c.id AND a.kode_brg = b.kode_brg
					ORDER BY b.nama_brg "); */
		
		$query	= $this->db->query(" SELECT b.kode_brg, b.nama_brg, c.id as id_satuan, c.nama as satuan 
					FROM tm_brg_hasil_makloon b, tm_satuan c 
					WHERE b.satuan = c.id AND b.status_aktif = 't' ORDER BY b.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				}
				
				$detail_bahan[] = array('kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $row->satuan,
										'stok'=> $stok,
										'stok_opname'=> '0'
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function get_all_stok_opname($bulan, $tahun) {

		$query	= $this->db->query(" SELECT b.*, c.nama_brg, c.satuan FROM tt_stok_opname_hasil_quilting_detail b, tt_stok_opname_hasil_quilting a, 
					tm_brg_hasil_makloon c
					WHERE b.id_stok_opname_hasil_quilting = a.id 
					AND b.kode_brg = c.kode_brg
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY c.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil nama brg dan stok terkini
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				/*$query3	= $this->db->query(" SELECT a.kode_brg, a.stok, b.nama_brg, c.nama as satuan 
					FROM tm_stok_hasil_makloon a, tm_brg_hasil_makloon b, tm_satuan c 
					WHERE b.satuan = c.id AND a.kode_brg = b.kode_brg AND b.kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->satuan;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg	= '';
					$satuan	= '';
					$stok = '';
				} */
				
				$detail_bahan[] = array('kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'id_satuan'=> $row->satuan,
										'satuan'=> $nama_satuan,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
			
			//  tambahkan pengecekan jika ada barang baru yg blm masuk ke tabel tt_stok_opname_hasil_quilting_detail
			$sql = "SELECT kode_brg, nama_brg, satuan FROM tm_brg_hasil_makloon where status_aktif = 't' AND kode_brg NOT IN 
		  			(select b.kode_brg FROM tt_stok_opname_hasil_quilting a, 
		  			tt_stok_opname_hasil_quilting_detail b where a.id = b.id_stok_opname_hasil_quilting
		  			AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f')";
					
			$query2	= $this->db->query($sql);
			
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->result();
				foreach ($hasil2 as $row2) {
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg= '$row2->kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$stok	= $hasilrow->stok;
					}
					else {
						$stok = 0;
					}
					
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row2->satuan' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan	= $hasilrow->nama;
					}
					else {
						$nama_satuan = "";
					}
					
					$detail_bahan[] = array('kode_brg'=> $row2->kode_brg,
											'nama_brg'=> $row2->nama_brg,
											'id_satuan'=> $row2->satuan,
											'satuan'=> $nama_satuan,
											'stok'=> $stok,
											'stok_opname'=> '0'
										);
				}
			} // end if
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function save($is_new, $id_stok, $kode_brg, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d"); 
	  
	  if ($is_new == '1') {
		   $data_detail = array(
						'id_stok_opname_hasil_quilting'=>$id_stok,
						'kode_brg'=>$kode_brg, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_hasil_quilting_detail',$data_detail);
	  }
	  else {
		 // cek di tt_stok_opname_hasil_quilting_detail. apakah kode brgnya ada yg blm ada
		 $query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_quilting_detail WHERE kode_brg = '$kode_brg' ");
		 if ($query3->num_rows() == 0){
			$data_detail = array(
						'id_stok_opname_hasil_quilting'=>$id_stok,
						'kode_brg'=>$kode_brg, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_hasil_quilting_detail',$data_detail);
		 }
		 else 
			$this->db->query(" UPDATE tt_stok_opname_hasil_quilting_detail SET jum_stok_opname = '$stok_fisik' , stok_awal = '$stok'
						where kode_brg= '$kode_brg' AND id_stok_opname_hasil_quilting = '$id_stok' ");
	  }
  } 

}

