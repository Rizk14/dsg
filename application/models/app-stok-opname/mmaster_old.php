<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so_bahanbaku($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, status_approve FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
		
	/*	$sql = " SELECT id, status_approve FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' "; die($sql); */
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
		}
		else {
			$status_approve	= '';
			$idnya = '';
		}
		
		$so_bahan_baku = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya
							);
							
		return $so_bahan_baku;
  }

  function get_all_stok_bahanbaku($gudang) {

		$query	= $this->db->query(" SELECT a.kode_brg, a.stok, b.nama_brg, c.nama as satuan 
					FROM tm_stok a, tm_barang b, tm_satuan c 
					WHERE b.satuan = c.id AND a.kode_brg = b.kode_brg AND b.id_gudang = '$gudang' 
					ORDER BY b.id_jenis_bahan, b.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				$detail_bahan[] = array('kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $row->satuan,
										'stok'=> $row->stok,
										'stok_opname'=> '0'
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function get_all_stok_opname_bahanbaku($bulan, $tahun, $gudang) {

		$query	= $this->db->query(" SELECT a.id as id_header, b.*, c.nama_brg, c.satuan FROM tt_stok_opname_bahan_baku_detail b, 
					tt_stok_opname_bahan_baku a, tm_barang c
					WHERE b.id_stok_opname_bahan_baku = a.id 
					AND b.kode_brg = c.kode_brg
					AND a.id_gudang = '$gudang' AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY c.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
					
					if ($row->satuan == '2') { // yard
						$stok = $stok*0.91; // ini ke meter
						$stok = round($stok, 2);
					}
					else if ($row->satuan == '7') { // lusin
						$stok = $stok*12; // ini ke pcs
						$stok = round($stok, 2);
					}
				}
				else {
					$stok = 0;
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $nama_satuan,
										'id_satuan'=> $row->satuan,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function save($is_new, $id_stok, $kode_brg, $stok_fisik){ 
	  $tgl = date("Y-m-d"); 
	  
	  if ($is_new == '1') {
		   $data_detail = array(
						'id_stok_opname_bahan_baku'=>$id_stok,
						'kode_brg'=>$kode_brg, 
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_bahan_baku_detail',$data_detail);
	  }
	  else {
		  $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET jum_stok_opname = '$stok_fisik', tgl_update = '$tgl' 
						where kode_brg= '$kode_brg' AND id_stok_opname_bahan_baku = '$id_stok' ");
	  }
  } 

}

