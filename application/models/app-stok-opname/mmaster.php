<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    	if($this->session->userdata('gid') == 16){
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
	    }else{
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
					                        }
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so_bahanbaku($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, tgl_so, status_approve FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
		
	/*	$sql = " SELECT id, status_approve FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' "; die($sql); */
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
		}
		
		$so_bahan_baku = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so
							);
							
		return $so_bahan_baku;
  }
  
  function get_all_stok_opname_bahanbaku($bulan, $tahun, $gudang) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		/*$query	= $this->db->query(" SELECT a.id as id_header, b.*, c.nama_brg, c.satuan FROM tt_stok_opname_bahan_baku_detail b, 
					tt_stok_opname_bahan_baku a, tm_barang c
					WHERE b.id_stok_opname_bahan_baku = a.id 
					AND b.kode_brg = c.kode_brg
					AND a.id_gudang = '$gudang' AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY c.nama_brg "); */
		$query	= $this->db->query(" SELECT a.id as id_header, b.*, c.kode_brg, c.nama_brg, c.satuan, 
					c.id_satuan_konversi, c.rumus_konversi, c.angka_faktor_konversi
					FROM tt_stok_opname_bahan_baku_detail b 
					INNER JOIN tt_stok_opname_bahan_baku a ON b.id_stok_opname_bahan_baku = a.id 
					INNER JOIN tm_barang c ON b.id_brg = c.id
					WHERE a.id_gudang = '$gudang' AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY c.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// 01-11-2014
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				// 22-06-2015 GA DIPAKE LAGII
				// ---------- 12-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a, 
									tt_stok_opname_bahan_baku_detail b
									WHERE a.id = b.id_stok_opname_bahan_baku
									AND b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$saldo_awal = $hasilx->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE b.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND b.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND b.status_stok = 't' AND e.id_gudang = '$gudang' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_masuk = $hasilx->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$jum_masuk = $jum_masuk * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$jum_masuk = $jum_masuk / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$jum_masuk = $jum_masuk + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$jum_masuk = $jum_masuk - $row->angka_faktor_konversi;
				} 
				$jum_masuk = round($jum_masuk, 2);
				
				// 3. masuk lain2
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$gudang' AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;
				
				// 3. hitung keluar bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty_satawal) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$gudang' AND b.is_quilting = 'f' ");
				
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_keluar = $hasilx->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
					
					// 27-02-2015
					if ($row->id_satuan_konversi != 0) {
						if ($row->rumus_konversi == '1') {
							$jum_keluar = $jum_keluar*$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '2') {
							$jum_keluar = $jum_keluar/$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '3') {
							$jum_keluar = $jum_keluar+$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '4') {
							$jum_keluar = $jum_keluar-$row->angka_faktor_konversi;
						}
					}
				}
				else
					$jum_keluar = 0;
								
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk+$jum_masuk_lain-$jum_keluar; */
				$auto_saldo_akhir = 0;
				//-------------------------------------------------------------------------------------
				
				// 31-10-2014
			/*	$sqlxx = " SELECT * FROM tt_stok_opname_bahan_baku_detail_harga
							WHERE id_stok_opname_bahan_baku_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_harga
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga
							WHERE kode_brg = '$row->kode_brg' AND
							harga = '$rowxx->harga' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							//$stok	= $hasilrow3->stok;
							
							if ($row->id_satuan_konversi != '0') {
								if ($row->rumus_konversi == '1')
									$stok = $hasilrow3->stok * $row->angka_faktor_konversi;
								else if ($row->rumus_konversi == '2')
									$stok = $hasilrow3->stok / $row->angka_faktor_konversi;
								else if ($row->rumus_konversi == '3')
									$stok = $hasilrow3->stok + $row->angka_faktor_konversi;
								else if ($row->rumus_konversi == '4')
									$stok = $hasilrow3->stok - $row->angka_faktor_konversi;
								else
									$stok = $hasilrow3->stok;
								
								$stok = round($stok, 2);
							} 
							else {
								$stok = $hasilrow3->stok;
							}
						}
						else {
							$stok = '0';
						}
						
						// ---------- 12-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a 
											INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											WHERE b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.harga = '".$rowxx->harga."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_harga = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_harga = 0;
						
						// 2. jum masuk bln ini
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
									INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND b.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND b.status_stok = 't' AND e.id_gudang = '$gudang'
									AND b.harga = '$rowxx->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_harga = $hasilrow->jum_masuk;
							if ($jum_masuk_harga == '')
								$jum_masuk_harga = 0;
						}
						else
							$jum_masuk_harga = 0;
						
						if ($row->id_satuan_konversi != '0') {
							if ($row->rumus_konversi == '1')
								$jum_masuk_harga = $jum_masuk_harga * $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '2')
								$jum_masuk_harga = $jum_masuk_harga / $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '3')
								$jum_masuk_harga = $jum_masuk_harga + $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '4')
								$jum_masuk_harga = $jum_masuk_harga - $row->angka_faktor_konversi;
						} 
						$jum_masuk_harga = round($jum_masuk_harga, 2);
						
						// 3. jum masuk lain2 bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
									INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
									INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$gudang' AND c.harga = '$rowxx->harga' AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_lain_harga = $hasilrow->jum_masuk_lain;
							if ($jum_masuk_lain_harga == '')
								$jum_masuk_lain_harga = 0;
						}
						else
							$jum_masuk_lain_harga = 0;
						
						// 4. jum keluar bln ini
						// ambilnya bukan qty, tapi qty_satawal
						$query3	= $this->db->query(" SELECT sum(c.qty_satawal) as jum_keluar FROM tm_bonmkeluar a
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$gudang' AND c.harga = '$rowxx->harga'
									AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_harga = $hasilrow->jum_keluar;
							if ($jum_keluar_harga == '')
								$jum_keluar_harga = 0;
							
							// 27-02-2015
							if ($row->id_satuan_konversi != 0) {
								if ($row->rumus_konversi == '1') {
									$jum_keluar_harga = $jum_keluar_harga*$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '2') {
									$jum_keluar_harga = $jum_keluar_harga/$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '3') {
									$jum_keluar_harga = $jum_keluar_harga+$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '4') {
									$jum_keluar_harga = $jum_keluar_harga-$row->angka_faktor_konversi;
								}
							}
							else
								$jum_keluar_harga = 0;
						}
						// 5. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_harga = $saldo_awal_harga+$jum_masuk_harga-$jum_keluar_harga;
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_harga[] = array(
									'harga'=> $rowxx->harga,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'auto_saldo_akhir_harga'=> $auto_saldo_akhir_harga
								);
					}
				}
				else
					$detail_harga = '';
				
				// end 13-05-2015 dikomen
				*/
				// --------------------------------
				
				/*$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
					
					if ($row->satuan == '2') { // yard
						$stok = $stok*0.91; // ini ke meter
						$stok = round($stok, 2);
					}
					else if ($row->satuan == '7') { // lusin
						$stok = $stok*12; // ini ke pcs
						$stok = round($stok, 2);
					}
				}
				else {
					$stok = 0;
				} */
				
				// 13-05-2015
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg= '$row->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				}
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $stok;
					
					$stokinduk = round($stokinduk, 2);
				} 
				else {
					$stokinduk = $stok;
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				/*$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $nama_satuan,
										'id_satuan'=> $row->satuan,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									); */
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										//13-05-2015
										//'detail_harga'=> $detail_harga,
										'stok'=> $stokinduk,
										'stok_opname'=> $row->jum_stok_opname,
										'auto_saldo_akhir'=> $auto_saldo_akhir
									);
				$detail_harga = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }

}

