<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}

	function lbarangjadi() {
	$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}
		
	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
						
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function flbarangjadi($key) {
	$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE a.i_faktur_code='$key' AND d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}
			
	function lklsbrg() {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC " );
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}

	function clistpenjualanndo($nofaktur,$dfirst,$dlast) {
		$db2=$this->load->database('db_external', TRUE);
		if($nofaktur!='0' && $dfirst!='0' && $dlast!='0') {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$dfaktur	= " AND (b.d_faktur BETWEEN '$dfirst' AND '$dlast') ";
			$kondisi	= " AND d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		} else if($nofaktur!='0' && ($dfirst=='0' || $dlast=='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$dfaktur	= "";		
			$kondisi	= " AND d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		} else if($nofaktur=='0' && ($dfirst!='0' && $dlast!='0')) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (b.d_faktur BETWEEN '$dfirst' AND '$dlast') ";
			$kondisi	= " AND d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		} else {
			$nfaktur	= "";
			$dfaktur	= "";
			$kondisi	= " WHERE d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		}
		
		$qstr	= " SELECT 	c.i_product AS imotif, c.i_code_references,
					d.i_sj_code AS sjcode,
					c.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
				
				FROM tm_faktur_bhnbaku_item a
				
				INNER JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur 
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(a.i_sj) 
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_sj)=trim(d.i_sj) 

				".$nfaktur." ".$dfaktur." ".$kondisi."
				
				GROUP BY c.i_product, d.i_sj_code, c.e_product_name, a.n_quantity, a.v_unit_price, c.i_code_references
				
				ORDER BY c.i_product DESC, c.e_product_name ASC ";
		// ini dihilangkan 11-07-2014
		//INNER JOIN tm_sj_bhnbaku_item c ON trim(c.e_product_name)=trim(a.e_product_name) 
		return $db2->query($qstr);
	}
		
	function clistpenjualanndoperpages($nofaktur,$dfirst,$dlast,$limit,$offset) {
$db2=$this->load->database('db_external', TRUE);
		if($nofaktur!='0' && ($dfirst!='0' && $dlast!='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$dfaktur	= " AND (b.d_faktur BETWEEN '$dfirst' AND '$dlast') ";
			$kondisi	= " AND d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		} else if($nofaktur!='0' && ($dfirst=='0' || $dlast=='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$dfaktur	= "";		
			$kondisi	= " AND d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		} else if($nofaktur=='0' && ($dfirst!='0' && $dlast!='0')) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (b.d_faktur BETWEEN '$dfirst' AND '$dlast') ";
			$kondisi	= " AND d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		} else {
			$nfaktur	= "";
			$dfaktur	= "";
			$kondisi	= " WHERE d.f_sj_cancel='f' AND b.f_faktur_cancel='f' AND d.f_faktur_created='t' ";
		}
		
		$qstr	= " SELECT 	c.i_product AS imotif, c.i_code_references,
					d.i_sj_code AS sjcode,
					c.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
				
				FROM tm_faktur_bhnbaku_item a

				INNER JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur 
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(a.i_sj) 
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_sj)=trim(d.i_sj) 
				
				".$nfaktur." ".$dfaktur." ".$kondisi."
				
				GROUP BY c.i_product, d.i_sj_code, c.e_product_name, a.n_quantity, a.v_unit_price, c.i_code_references
				
				ORDER BY c.i_product DESC, c.e_product_name ASC LIMIT ".$limit." OFFSET ".$offset;
		// ini dihilangkan
		//INNER JOIN tm_sj_bhnbaku_item c ON trim(c.e_product_name)=trim(a.e_product_name) 
		
		$query	= $db2->query($qstr);
						
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}	
	
	/* to Excell */
	function explistpenjualanndo($nofaktur,$tfirst,$tlast) {
		$db2=$this->load->database('db_external', TRUE);
		if( $nofaktur!='0' && ($tfirst!='0' && $tlast!='0')) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= " AND (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$is_cancel = " AND c.f_faktur_cancel = 'f' ";
		} else if($nofaktur!='0' && ($tfirst=='0' || $tlast=='0')) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= "";
			$is_cancel = " AND c.f_faktur_cancel = 'f' ";
		} else if($nofaktur=='0' && ($tfirst!='0' && $tlast!='0')) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$is_cancel = " AND c.f_faktur_cancel = 'f' ";
		} else {
			$nfaktur	= "";
			$dfaktur	= "";
			$is_cancel = " WHERE c.f_faktur_cancel = 'f' ";
			
		}
		
			$strq	= " SELECT  c.i_faktur_code AS ifakturcode,	
					a.e_customer_name AS customername,
					a.e_customer_npwp AS npwp,
					c.d_faktur AS dfaktur,
					c.d_pajak AS dpajak,
					c.d_due_date AS dduedate,
					c.n_discount AS discountpersen,
					c.v_discount AS nilaidiscon,
					c.v_total_faktur AS totalfaktur,
					(c.v_total_faktur-c.v_discount) AS dpp,
					c.v_total_fppn AS totalsetelahppndiscount,
					c.f_include_ppn
				
				FROM tr_customer a
					
					INNER JOIN tr_branch b ON b.i_customer=a.i_customer
					INNER JOIN tm_faktur_bhnbaku c ON c.e_branch_name=b.e_initial
					INNER JOIN tm_faktur_bhnbaku_item d ON d.i_faktur=cast(c.i_faktur AS integer)
					
					".$nfaktur." ".$dfaktur." ".$is_cancel."
					
					GROUP BY a.e_customer_name,
						a.e_customer_npwp,
						c.i_faktur_code,
						c.d_faktur,
						c.d_pajak,
						c.d_due_date,
						c.v_total_faktur,
						c.n_discount,
						c.v_discount,
						c.v_total_fppn,
						c.f_include_ppn ORDER BY c.i_faktur_code ASC, c.d_faktur DESC ";
		
		return $db2->query($strq);
	}
function explistpenjualanndoasnofakpajak($nofaktur,$tfirst,$tlast) {
		$db2=$this->load->database('db_external', TRUE);
		if( $nofaktur!='0' && ($tfirst!='0' && $tlast!='0')) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= " AND (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$is_cancel = " AND c.f_faktur_cancel = 'f' ";
		} else if($nofaktur!='0' && ($tfirst=='0' || $tlast=='0')) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= "";
			$is_cancel = " AND c.f_faktur_cancel = 'f' ";
		} else if($nofaktur=='0' && ($tfirst!='0' && $tlast!='0')) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$is_cancel = " AND c.f_faktur_cancel = 'f' ";
		} else {
			$nfaktur	= "";
			$dfaktur	= "";
			$is_cancel = " WHERE c.f_faktur_cancel = 'f' ";
			
		}
		
			$strq	= " SELECT  c.i_faktur_code AS ifakturcode,	
					a.e_customer_name AS customername,
					a.e_customer_npwp AS npwp,
					e.nomor_pajak AS nomorpajak,
					c.d_faktur AS dfaktur,
					c.d_pajak AS dpajak,
					c.d_due_date AS dduedate,
					c.n_discount AS discountpersen,
					c.v_discount AS nilaidiscon,
					c.v_total_faktur AS totalfaktur,
					(c.v_total_faktur-c.v_discount) AS dpp,
					c.v_total_fppn AS totalsetelahppndiscount,
					c.f_include_ppn
				
				FROM tr_customer a
					
					INNER JOIN tr_branch b ON b.i_customer=a.i_customer
					INNER JOIN tm_faktur_bhnbaku c ON c.e_branch_name=b.e_initial
					INNER JOIN tm_faktur_bhnbaku_item d ON d.i_faktur=cast(c.i_faktur AS integer)
					INNER JOIN tm_nomor_pajak_faktur e ON c.i_faktur_code = e.i_faktur_code		
					".$nfaktur." ".$dfaktur." ".$is_cancel."
					
					GROUP BY a.e_customer_name,
						a.e_customer_npwp,
						e.nomor_pajak,
						c.i_faktur_code,
						c.d_faktur,
						c.d_pajak,
						c.d_due_date,
						c.v_total_faktur,
						c.n_discount,
						c.v_discount,
						c.v_total_fppn,
						c.f_include_ppn ORDER BY c.i_faktur_code ASC, c.d_faktur DESC ";
		
		return $db2->query($strq);
	}
	function pajak($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT b.i_faktur_pajak AS ifakturpajak, b.d_pajak
			
			FROM tm_faktur_bhnbaku b
			
			INNER JOIN tm_faktur_bhnbaku_item a ON cast(b.i_faktur AS integer)=a.i_faktur
			INNER JOIN tm_sj_bhnbaku_item c ON c.i_sj=a.i_sj
			INNER JOIN tm_sj_bhnbaku d ON d.i_sj=c.i_sj
			
			WHERE b.i_faktur_code='$nofaktur'
			
			GROUP BY b.i_faktur_pajak, b.d_pajak ");
	}	
	
}
?>
