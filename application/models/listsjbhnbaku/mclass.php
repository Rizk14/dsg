<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function iterasionsjpbhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT CAST(i_iterasi_code AS integer)+1 AS iiterasicode FROM tm_iterasi_sjpbhnbaku ORDER BY CAST(i_iterasi_code AS integer) DESC LIMIT 1 ");
	}
	
	function clistsj($isj,$d_sj_first,$d_sj_last) {
	$db2=$this->load->database('db_external', TRUE);
		if(($d_sj_first!='' && $d_sj_last!='') && $isj!='') {
			$dsj	= " WHERE ( b.d_sj BETWEEN '$d_sj_first' AND '$d_sj_last' ) AND b.f_sj_cancel='f' ";
			$idsj	= " AND b.i_sj_code='$isj' ";
		} elseif(($d_sj_first!='' && $d_sj_last!='') && $isj=='') {
			$dsj	= " WHERE ( b.d_sj BETWEEN '$d_sj_first' AND '$d_sj_last' ) AND b.f_sj_cancel='f' ";
			$idsj	= " ";
		} elseif(($d_sj_first=='' || $d_sj_last=='') && $isj!='') {
			$dsj	= " WHERE b.f_sj_cancel='f' ";
			$idsj	= " AND b.i_sj_code='$isj' ";
		} elseif(($d_sj_first!='' || $d_sj_last!='') && $isj!='') {
			$dsj	= " WHERE b.f_sj_cancel='f' ";
			$idsj	= " AND b.i_sj_code='$isj' ";		
		} else {
			$dsj	= " WHERE b.f_sj_cancel='f' ";
			$idsj	= " ";
		}
		
		$strq	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode, 
				b.d_sj AS dsj, 
				a.i_product AS iproduct, 
				a.i_code_references AS ibhnbakucode,
				a.e_product_name AS productname, 
				a.n_unit AS unit, 
				a.n_unit_akhir AS belumfaktur,
				a.v_product_price AS hjp, 
				(a.n_unit * a.v_product_price) AS harga,
				a.f_faktur_created AS itemfaktur,
				b.f_faktur_created AS sjfaktur 
			
			FROM tm_sj_bhnbaku_item a 
			
			RIGHT JOIN tm_sj_bhnbaku b ON trim(b.i_sj)=trim(a.i_sj) 
			
			".$dsj." ".$idsj." ORDER BY b.d_sj DESC	";
			
		$query	= $db2->query($strq);
		
		if($query->num_rows() > 0 )	{
			return $result	= $query->result();
		}
	}

	function lsj() {
		$db2=$this->load->database('db_external', TRUE);		
		return $db2->query(" SELECT  b.i_sj_code AS isjcode,
					b.d_sj AS dsj
					
				FROM tm_sj_bhnbaku_item a
				
				INNER JOIN tm_sj_bhnbaku b ON b.i_sj=a.i_sj
				
				WHERE b.f_sj_cancel='f' GROUP BY b.i_sj_code, b.d_sj ORDER BY b.d_sj DESC ");
	}

	function lsjperpages($limit,$offset) {	
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT  b.i_sj_code AS isjcode,
					b.d_sj AS dsj
					
				FROM tm_sj_bhnbaku_item a
				
				INNER JOIN tm_sj_bhnbaku b ON b.i_sj=a.i_sj
				
				WHERE b.f_sj_cancel='f' GROUP BY b.i_sj_code, b.d_sj ORDER BY b.d_sj DESC LIMIT ".$limit." OFFSET ".$offset);
						
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}
	
	function flsj($key) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($key)) {
			
			return $db2->query(" SELECT  b.i_sj_code AS isjcode,
				b.d_sj AS dsj
				
				FROM tm_sj_bhnbaku_item a
				
				INNER JOIN tm_sj_bhnbaku b ON b.i_sj=a.i_sj
				
				WHERE b.f_sj_cancel='f' AND b.i_sj_code='$key' GROUP BY b.i_sj_code, b.d_sj ORDER BY b.d_sj DESC ");
		}
	}		
	
	function getsj($isj) {
		$db2=$this->load->database('db_external', TRUE);
		$strisj	= " SELECT * FROM tm_sj_bhnbaku WHERE i_sj='$isj' AND f_faktur_created='f' AND f_sj_cancel='f' ORDER BY i_sj DESC LIMIT 1 ";
		
		return $db2->query($strisj);
	}
	
	function getsjitem($isj, $tahun, $bulan) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT a.i_sj AS isj,
				b.d_sj AS dsj,
				a.i_gudang,
				a.e_satuan AS isatuan,
				c.e_satuan AS esatuan,
				a.i_product AS iproduct,
				a.i_code_references AS iprod,
				a.e_product_name AS productname,
				a.n_unit AS unit,
				a.n_unit_akhir AS qtyakhir,
				a.v_product_price AS hjp,
				(a.n_unit_akhir * a.v_product_price) AS harga

				FROM tm_sj_bhnbaku_item a
				
				INNER JOIN tm_sj_bhnbaku b ON trim(b.i_sj)=trim(a.i_sj)
				INNER JOIN tr_satuan c ON c.i_satuan=CAST(a.e_satuan AS integer)
				
				WHERE a.i_sj='$isj' AND b.f_sj_cancel='f' AND b.f_faktur_created='f' ORDER BY a.i_product ASC ";
				
		$query	= $db2->query($qstr);
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}	

	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lcabang($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.i_branch AS codebranch, 
					a.i_branch_code AS ibranchcode,
				    a.i_customer AS codecustomer, 
					a.e_branch_name AS ebranchname,
				    a.e_branch_name AS branch 
					
					FROM tr_branch a 
					
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$filter." ".$order;
					
		$query	= $db2->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function mupdate($i_sjcode,$isj,$d_sj,$i_customer,$i_branch,$v_sj_total,$e_note,$i_product,$e_product_name,$v_product_price,$n_unit,$v_unit_price,$iterasi,$e_satuan,$i_code_references,$i_gudang) {		
	$db2=$this->load->database('db_external', TRUE);
		/*
		$db2->trans_begin();
		*/

		$tmsj_item	= array();
				
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$db2->delete('tm_sj_bhnbaku',array('i_sj'=>$isj));
		$db2->delete('tm_sj_bhnbaku_item',array('i_sj'=>$isj));
		
		$qsjbhnbakuitem = $db2->query(" SELECT * FROM tm_sj_bhnbaku_item WHERE i_sj='$isj' ");
		foreach($qsjbhnbakuitem->result() as $row) {
			$db2->query(" DELETE FROM tm_iterasi_sjpbhnbaku WHERE i_iterasi_code='$row->i_product' ");
		}
		
		$db2->set(array(
			 'i_sj'=>$isj,
			 'i_sj_code'=>$i_sjcode,
			 'i_customer'=>$i_customer,
			 'i_branch'=>$i_branch,
			 'd_sj'=>$d_sj,
			 'e_note'=>$e_note,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry));
			 
		if($db2->insert('tm_sj_bhnbaku')) {
		
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++) {
				
				$seq_tm_sj_item	= $db2->query(" SELECT i_sj_item FROM tm_sj_bhnbaku_item ORDER BY i_sj_item DESC LIMIT 1 ");
				
				if($seq_tm_sj_item->num_rows() > 0) {
					$seqrow	= $seq_tm_sj_item->row();
					$i_sj_item[$jumlah]	= $seqrow->i_sj_item+1;
				} else {
					$i_sj_item[$jumlah]	= 1;
				}
				if ($i_gudang == '') {
			        $i_gudangx = $i_gudang;
			      }else{
			        $i_gudangx = '0';

			       }


				$tmsj_item[$jumlah] = array(
					 'i_sj_item'=>$i_sj_item[$jumlah],
					 'i_sj'=>$isj,
					 'i_gudang'=>$i_gudang[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'v_product_price'=>$v_product_price[$jumlah],
					 'n_unit'=>$n_unit[$jumlah],
					 'n_unit_akhir'=>$n_unit[$jumlah],
					 'e_satuan'=>$e_satuan[$jumlah],
					 'i_code_references'=>$i_code_references[$jumlah],
					 'd_entry'=>$dentry);
				
				$db2->insert('tm_sj_bhnbaku_item',$tmsj_item[$jumlah]);
				$db2->query(" INSERT INTO tm_iterasi_sjpbhnbaku(i_iterasi_code,d_entry) VALUES('$i_product[$jumlah]','$dentry') ");
			}
		} else {
			print "<script>alert(\"Maaf, Data SJ gagal diupdate!\");show(\"listsjbhnbaku/cform\",\"#content\");</script>";
		}

		$qsj	= $db2->query("SELECT * FROM tm_sj_bhnbaku WHERE i_sj='$isj' ");
		
		if($qsj->num_rows()==0) {
			print "<script>alert(\"Maaf, Data SJ gagal diupdate!\");show(\"listsjbhnbaku/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Nomor SJ : '\"+$i_sjcode+\"' telah diupdate, terimakasih.\");show(\"listsjbhnbaku/cform\",\"#content\");</script>";
		}
	}
	
	function cari_sj($nsj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_sj_bhnbaku WHERE i_sj_code=trim('$nsj') ");
	}
	
	function mbatal($isj) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($isj)) {
			
			$qcariisj	= $db2->query(" SELECT * FROM tm_sj_bhnbaku WHERE i_sj='$isj' AND f_sj_cancel='f' AND f_faktur_created='f' ORDER BY i_sj DESC LIMIT 1 ");
			
			if($qcariisj->num_rows()>0) {
				
				$row_sj	= $qcariisj->row();
				
				$isj	= $row_sj->i_sj;
				$isjcode= $row_sj->i_sj_code;
				
				$tbl_sj	= array(
					'f_sj_cancel'=>'TRUE'
				);
				
				$db2->update('tm_sj_bhnbaku',$tbl_sj,array('i_sj'=>$isj));
				
				print "<script>alert(\"Nomor SJ: '\"+$isjcode+\"' telah dibatalkan, terimakasih.\");
				window.open(\"../../index\", \"_self\");</script>";
				
				
			} else {
				print "<script>alert(\"Maaf, SJ tdk dpt dibatalkan.\");
				window.open(\"../../index\", \"_self\");</script>";
				
				
			}
		} else {
			print "<script>alert(\"Maaf, SJ tdk dpt dibatalkan.\");show(\"listsjbhnbaku/cform\",\"#content\");</script>";	
		}
	}

}
?>
