<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id, status_approve FROM tt_stok_opname_hasil_packing
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
		}
		else {
			$status_approve	= '';
			$idnya = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya
							);
							
		return $so_bahan;
  }

  function get_all_stok() {
		$query	= $this->db->query(" SELECT a.kode_brg_jadi, a.stok, c.e_product_motifname
					FROM tm_stok_hasil_packing a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi
					ORDER BY a.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				$detail_bahan[] = array('kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'stok'=> $row->stok,
										'stok_opname'=> '0'
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function get_all_stok_opname($bulan, $tahun) {

		$query	= $this->db->query(" SELECT b.* FROM tt_stok_opname_hasil_packing_detail b, tt_stok_opname_hasil_packing a, 
					tr_product_motif d
					WHERE b.id_stok_opname_hasil_packing = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil nama brg dan stok terkini
				$query3	= $this->db->query(" SELECT a.kode_brg_jadi, a.stok, c.e_product_motifname
					FROM tm_stok_hasil_packing a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi
					AND a.kode_brg_jadi = '$row->kode_brg_jadi' ");
					
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg_jadi	= '';
					$stok = '';
				}
				
				$detail_bahan[] = array( 'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function save($is_new, $id_stok, $kode_brg_jadi, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d"); 
	  
	  if ($is_new == '1') {
		   $data_detail = array(
						'id_stok_opname_hasil_packing'=>$id_stok,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_hasil_packing_detail',$data_detail);
	  }
	  else {
		  $this->db->query(" UPDATE tt_stok_opname_hasil_packing_detail SET jum_stok_opname = '$stok_fisik' 
						where kode_brg_jadi = '$kode_brg_jadi' 
						AND id_stok_opname_hasil_packing = '$id_stok' ");
	  }
  } 

}
