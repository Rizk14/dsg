<?php
class Stokopname_unit_model extends MY_Model
{
	protected $_per_page = 10;
    protected $_tabel = 'tb_stokopname_unit';
    protected $form_rules = array(
 
        
        
        array(
            'field' => 'unit_jahit',
            'label' => 'Unit Jahit',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'unit_packing',
            'label' => 'Unit Packing',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'gudang',
            'label' => 'Gudang',
            'rules' => 'trim|required|max_length[16]'
        ),
       
        
        
    );

     public $default_values = array(
		'id'	=>	'',
		'num'		=>1,
		'tanggal_so'		=>'',
    );
    
     public function get_unit_jahit()
    {
     $sql=$this->db->query("SELECT id,nama_unit_jahit FROM tb_master_unit_jahit order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }    
    public function get_unit_packing()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_unit_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }  
    public function get_gudang()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_gudang order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }
     public function getAll($id_unit_jahit,$id_unit_packing,$id_gudang,$periode_so,$id_jenis_so)
    {
	if($id_unit_jahit!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_unit_jahit where id_unit_jahit='$id_unit_jahit'");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query2 = $this->db->query("SELECT * from tb_master_barang_bb where id=$row->id_barang_bb");
						if($query2->num_rows > 0){
							$hasil2= $query2->row();
							
							}
							$data_detail[]=array(
							'id_barang_bb'=>$hasil2->id,
							'kode_barang_bb'=>$hasil2->kode_barang_bb,
							'nama_barang_bb'=>$hasil2->nama_barang_bb
							);				
					}
				
			$query3 = $this->db->query("SELECT * FROM tb_master_unit_jahit where id='$id_unit_jahit'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
				$nama_unit_jahit= $hasil3->nama_unit_jahit;
				
						}
						$data[]=array(
							'id_unit_packing'=>0,
							'id_gudang'=>0,	
							'id_unit_jahit'=>$hasil3->id,	
							'nama_unit_jahit'=>$nama_unit_jahit,			
							'data_detail'=>$data_detail
							);	
							
		}
		else $data= '';
			
	}		

	
	if($id_unit_packing!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_unit_packing_wip where id_unit_packing='$id_unit_packing'");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query2 = $this->db->query("SELECT * from tb_master_barang_wip where id=$row->id_barang_wip");
						if($query2->num_rows > 0){
							$hasil2= $query2->row();
							
							}
							$data_detail[]=array(
							'id_barang_wip'=>$hasil2->id,
							'kode_barang_wip'=>$hasil2->kode_barang_wip,
							'nama_barang_wip'=>$hasil2->nama_barang_wip
							);				
					}
				
			$query3 = $this->db->query("SELECT * FROM tb_master_unit_packing where id='$id_unit_packing'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
				$nama_unit_packing= $hasil3->nama_unit_packing;
				
						}
						$data[]=array(
							'id_unit_packing'=>$hasil3->id,	
							'id_unit_jahit'=>0,	
							'id_gudang'=>0,
							'nama_unit_packing'=>$nama_unit_packing,			
							'data_detail'=>$data_detail
							);	
							
		}
		else $data= '';
			
	}		
	
	if($id_gudang!= 0){
		if($id_jenis_so==1){
		$query = $this->db->query("SELECT * from tb_stok_gudang_jadi_1 where id_gudang='$id_gudang'");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query2 = $this->db->query("SELECT * from tb_master_barang_wip where id=$row->id_barang_wip");
						if($query2->num_rows > 0){
							$hasil2= $query2->row();
							
							}
							$data_detail[]=array(
							'id_barang_wip'=>$hasil2->id,
							'kode_barang_wip'=>$hasil2->kode_barang_wip,
							'nama_barang_wip'=>$hasil2->nama_barang_wip
							);				
					}
				
			$query3 = $this->db->query("SELECT * FROM tb_master_gudang where id='$id_gudang'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
				$nama_gudang= $hasil3->nama_gudang;
				
						}
						$data[]=array(
							'id_gudang'=>$hasil3->id,	
							'id_jenis_so'=>$hasil3->id_jenis_so,	
							'id_unit_jahit'=>0,	
							'id_unit_packing'=>0,	
							'nama_gudang'=>$nama_gudang,			
							'data_detail'=>$data_detail
							);	
							
		}
		else $data= '';
			
	}
	if($id_jenis_so==2){
		$query = $this->db->query("SELECT * from tb_stok_gudang_jadi_2 where id_gudang='$id_gudang'");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query2 = $this->db->query("SELECT * from tb_master_barang_bb where id=$row->id_barang_bb");
						if($query2->num_rows > 0){
							$hasil2= $query2->row();
							
							}
							$data_detail[]=array(
							'id_barang_bb'=>$hasil2->id,
							'kode_barang_bb'=>$hasil2->kode_barang_bb,
							'nama_barang_bb'=>$hasil2->nama_barang_bb
							);				
					}
				
			$query3 = $this->db->query("SELECT * FROM tb_master_gudang where id='$id_gudang'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
				$nama_gudang= $hasil3->nama_gudang;
				
						}
						$data[]=array(
							'id_gudang'=>$hasil3->id,	
							'id_jenis_so'=>$hasil3->id_jenis_so,	
							'id_unit_jahit'=>0,	
							'id_unit_packing'=>0,	
							'nama_gudang'=>$nama_gudang,			
							'data_detail'=>$data_detail
							);	
							
		}
		else $data= '';
			
	}
	
	return $data;	
}
}
 public function editAll($id_unit_jahit,$id_unit_packing,$id_gudang,$periode_so,$tahun,$bulan,$id_jenis_so)
    {
	if($id_unit_jahit!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_opname_unit_jahit where id_unit_jahit='$id_unit_jahit' AND tahun='$tahun' AND bulan ='$bulan' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_unit_jahit_detail where id_stok_opname_unit_jahit='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_bb where id=$row3->id_barang_bb");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_bb'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_bb'=>$hasil2->kode_barang_bb,
							'nama_barang_bb'=>$hasil2->nama_barang_bb
							);
						}
							
					}
				
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_unit_jahit where id='$id_unit_jahit'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
							'id_so'=>$row->id,
							'id_unit_jahit'=>$hasil3->id,
							'id_unit_packing'=>0,
							'id_gudang'=>0,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
							'nama_unit_jahit'=>$hasil3->nama_unit_jahit,			
							'data_detail'=>$data_detail
							);
			}
		}		
		return $data;
	}
	if($id_unit_packing!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_opname_unit_packing where id_unit_packing='$id_unit_packing' AND tahun='$tahun' AND bulan ='$bulan' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_unit_packing_detail where id_stok_opname_unit_packing='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_wip where id=$row3->id_barang_wip");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_wip'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_wip'=>$hasil2->kode_barang_wip,
							'nama_barang_wip'=>$hasil2->nama_barang_wip
							);
						}
							
					}
				
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_unit_packing where id='$id_unit_packing'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
							'id_so'=>$row->id,
							'id_unit_jahit'=>0,
							'id_unit_packing'=>$hasil3->id,
							'id_gudang'=>0,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
								
							'nama_unit_packing'=>$hasil3->nama_unit_packing,			
							'data_detail'=>$data_detail
							);
			}
		}		
		return $data;
	}
	if($id_gudang!= 0){
		if($id_jenis_so==1){
		$query = $this->db->query("SELECT * from tb_stok_opname_gudang_wip where id_gudang_wip='$id_gudang' AND tahun='$tahun' AND bulan ='$bulan' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_gudang_wip_detail where id_stok_opname_gudang_wip='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_wip where id=$row3->id_barang_wip");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_wip'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_wip'=>$hasil2->kode_barang_wip,
							'nama_barang_wip'=>$hasil2->nama_barang_wip
							);
						}
							
					}
				
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_gudang where id='$id_gudang'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
							'id_so'=>$row->id,
							'id_unit_jahit'=>0,
							'id_gudang'=>$hasil3->id,
							'id_unit_packing'=>0,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
								
							'nama_gudang'=>$hasil3->nama_gudang,			
							'data_detail'=>$data_detail
							);
			}
		}		
			return $data;
		}
		
		if($id_jenis_so==2){
		$query = $this->db->query("SELECT * from tb_stok_opname_gudang_bb where id_gudang_bb='$id_gudang' AND tahun='$tahun' AND bulan ='$bulan' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_gudang_bb_detail where id_stok_opname_gudang_bb='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_bb where id=$row3->id_barang_bb");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_bb'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_bb'=>$hasil2->kode_barang_bb,
							'nama_barang_bb'=>$hasil2->nama_barang_bb
							);
						}
							
					}
				
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_gudang where id='$id_gudang'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
							'id_so'=>$row->id,
							'id_unit_jahit'=>0,
							'id_gudang'=>$hasil3->id,
							'id_unit_packing'=>0,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
								
							'nama_gudang'=>$hasil3->nama_gudang,			
							'data_detail'=>$data_detail
							);
			}
		}		
			return $data;
		}
	}
}

public function submitAll($id_barang_bb,$qty,$jenis_hitung,$tanggal_so,$bulan,$tahun,$id_unit_jahit,
		$id_barang_bb_1,$qty_1,$id_unit_packing,$id_barang_wip,$id_barang_wip_1,$id_gudang,$kode_barang_bb,$kode_barang_bb_1,$id_jenis_so)
{
	
	if($id_unit_jahit != 0){
	
	$data_header=array(
	'bulan'=>$bulan,
	'tahun'=>$tahun,
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_unit_jahit'=>$id_unit_jahit
	);
	
	$this->db->insert('tb_stok_opname_unit_jahit',$data_header);
		
	$query=$this->db->query("SELECT id FROM tb_stok_opname_unit_jahit order by id desc limit 1");
	if($query->num_rows() > 0){
		$hasil=$query->row();
		$id_header =$hasil->id;
	
	for($i=0;$i<count($id_barang_bb);$i++){
		if($id_barang_bb[$i]!=''){
		$data_detail=array(
	'id_stok_opname_unit_jahit'=>$id_header,
	'id_barang_bb'=>$id_barang_bb[$i],
	'kode_barang_bb'=>$kode_barang_bb[$i],
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->insert('tb_stok_opname_unit_jahit_detail',$data_detail);
}

if($id_barang_bb_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_unit_jahit'=>$id_header,
	'id_barang_bb'=>$id_barang_bb_1[$i],
	'kode_barang_bb'=>$kode_barang_bb_1[$i],
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_unit_jahit_detail',$data_detail);
				}
			}
		}
	}
		elseif($id_unit_packing != 0){
	
	$data_header=array(
	'bulan'=>$bulan,
	'tahun'=>$tahun,
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_unit_packing'=>$id_unit_packing
	);
	
	$this->db->insert('tb_stok_opname_unit_packing',$data_header);
	$query=$this->db->query("SELECT id FROM tb_stok_opname_unit_packing order by id desc limit 1");
	if($query->num_rows() > 0){
		$hasil=$query->row();
		$id_header =$hasil->id;
	
	for($i=0;$i<count($id_barang_bb);$i++){
		$data_detail=array(
	'id_stok_opname_unit_packing'=>$id_header,
	'id_barang_wip'=>$id_barang_wip[$i],
	'kode_barang_wip'=>$kode_barang_wip,
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->insert('tb_stok_opname_unit_packing_detail',$data_detail);
	if($id_barang_wip_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_unit_packing'=>$id_header,
	'id_barang_wip'=>$id_barang_wip_1[$i],
	'kode_barang_wip'=>$kode_barang_wip,
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_unit_packing_detail',$data_detail);
			}
			}
		}
	}
	
		elseif($id_gudang != 0){
		if($id_jenis_so==1){
	$data_header=array(
	'bulan'=>$bulan,
	'tahun'=>$tahun,
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_gudang_wip'=>$id_gudang
	);
	
	$this->db->insert('tb_stok_opname_gudang_wip',$data_header);
	$query=$this->db->query("SELECT id FROM tb_stok_opname_gudang_wip order by id desc limit 1");
	if($query->num_rows() > 0){
		$hasil=$query->row();
		$id_header =$hasil->id;
	
	for($i=0;$i<count($id_barang_wip);$i++){
		$data_detail=array(
	'id_stok_opname_gudang_wip'=>$id_header,
	'id_barang_wip'=>$id_barang_wip[$i],
	'kode_barang_wip'=>$kode_barang_wip,
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->insert('tb_stok_opname_gudang_wip_detail',$data_detail);
	if($id_barang_wip_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_gudang_wip'=>$id_header,
	'id_barang_wip'=>$id_barang_wip_1[$i],
	'kode_barang_wip'=>$kode_barang_wip,
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_gudang_wip_detail',$data_detail);
					}
				}
			}
		}
	
		if($id_jenis_so==2){
	$data_header=array(
	'bulan'=>$bulan,
	'tahun'=>$tahun,
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_gudang_bb'=>$id_gudang
	);
	
	$this->db->insert('tb_stok_opname_gudang_bb',$data_header);
	$query=$this->db->query("SELECT id FROM tb_stok_opname_gudang_bb order by id desc limit 1");
	if($query->num_rows() > 0){
		$hasil=$query->row();
		$id_header =$hasil->id;
	
	for($i=0;$i<count($id_barang_bb);$i++){
		$data_detail=array(
	'id_stok_opname_gudang_bb'=>$id_header,
	'id_barang_bb'=>$id_barang_bb[$i],
	'kode_barang_bb'=>$kode_barang_bb,
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->insert('tb_stok_opname_gudang_bb_detail',$data_detail);
	if($id_barang_bb_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_gudang_bb'=>$id_header,
	'id_barang_bb'=>$id_barang_bb_1[$i],
	'kode_barang_bb'=>$kode_barang_bb,
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_gudang_bb_detail',$data_detail);
					}
				}
			}
		}	
		
	}
	
	return TRUE;
	}
	
	public function updateAll($id_so,$id_barang_bb,$kode_barang_bb,$qty,
	$jenis_hitung,$tanggal_so,$id_unit_jahit,
	$id_barang_bb_1,$qty_1,$id_unit_packing,$id_barang_wip,$id_barang_wip_1,
	$id_gudang,$id_detail_so,$kode_barang_wip,$kode_barang_wip_1,$kode_barang_bb_1)
{
	
	if($id_unit_jahit != 0){
	$this->db->where('id', $id_so);
	$data_header=array(
	
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_unit_jahit'=>$id_unit_jahit
	);
	
	$this->db->update('tb_stok_opname_unit_jahit',$data_header);
		
	for($i=0;$i<count($id_barang_bb);$i++){
		if($id_barang_bb[$i]!=''){
	$this->db->where('id', $id_detail_so[$i]);		
		$data_detail=array(
	'id_barang_bb'=>$id_barang_bb[$i],
	'kode_barang_bb'=>$kode_barang_bb[$i],
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->update('tb_stok_opname_unit_jahit_detail',$data_detail);
}
if($id_barang_bb_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_unit_jahit'=>$id_so,
	'id_barang_bb'=>$id_barang_bb_1[$i],
	'kode_barang_bb'=>$kode_barang_bb_1[$i],
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_unit_jahit_detail',$data_detail);
				}
			}
		
	}
		elseif($id_unit_packing != 0){
			
	$this->db->where('id', $id_so);
	$data_header=array(
	
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_unit_packing'=>$id_unit_packing
	);
	
	$this->db->update('tb_stok_opname_unit_packing',$data_header);
		

	for($i=0;$i<count($id_barang_wip);$i++){
		if($id_barang_wip[$i]!=''){
	$this->db->where('id', $id_detail_so[$i]);		
		$data_detail=array(
	'id_barang_wip'=>$id_barang_wip[$i],
	'kode_barang_wip'=>$kode_barang_wip[$i],
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->update('tb_stok_opname_unit_packing_detail',$data_detail);
}
if($id_barang_wip_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_unit_packing'=>$id_so,
	'id_barang_wip'=>$id_barang_wip_1[$i],
	'kode_barang_wip'=>$kode_barang_wip_1[$i],
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_unit_packing_detail',$data_detail);
				}
			}
		
	}
	
		elseif($id_gudang != 0){
			
			if($id_barang_bb != ''){
	$this->db->where('id', $id_so);
	$data_header=array(
	
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_gudang_bb'=>$id_gudang
	);
	
	$this->db->update('tb_stok_opname_gudang_bb',$data_header);
		

	for($i=0;$i<count($id_barang_bb);$i++){
		if($id_barang_bb[$i]!=''){
	$this->db->where('id', $id_detail_so[$i]);		
		$data_detail=array(
	'id_barang_bb'=>$id_barang_bb[$i],
	'kode_barang_bb'=>$kode_barang_bb[$i],
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->update('tb_stok_opname_gudang_bb_detail',$data_detail);
}
	if($id_barang_bb_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_gudang_bb'=>$id_so,
	'id_barang_bb'=>$id_barang_bb_1[$i],
	'kode_barang_bb'=>$kode_barang_bb_1[$i],
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_gudang_bb_detail',$data_detail);
				}
			}
		
	}
	
			if($id_barang_wip != ''){
	$this->db->where('id', $id_so);
	$data_header=array(
	
	'jenis_hitung'=>$jenis_hitung,
	'tanggal_so'=>$tanggal_so,
	'id_gudang_wip'=>$id_gudang
	);
	
	$this->db->update('tb_stok_opname_gudang_wip',$data_header);
		

	for($i=0;$i<count($id_barang_wip);$i++){
		if($id_barang_wip[$i]!=''){
	$this->db->where('id', $id_detail_so[$i]);		
		$data_detail=array(
	'id_barang_wip'=>$id_barang_wip[$i],
	'kode_barang_wip'=>$kode_barang_wip[$i],
	'v_stok_opname'=>$qty[$i]
	);
	$this->db->update('tb_stok_opname_gudang_wip_detail',$data_detail);
}
	if($id_barang_wip_1[$i]!=''){
	$data_detail=array(
	'id_stok_opname_gudang_wip'=>$id_so,
	'id_barang_wip'=>$id_barang_wip_1[$i],
	'kode_barang_wip'=>$kode_barang_wip_1[$i],
	'v_stok_opname'=>$qty_1[$i]
	);
	$this->db->insert('tb_stok_opname_gudang_wip_detail',$data_detail);
				}
			}
		
	}
	
}	
//	return TRUE;
	}
	
	public function cek_data($unit_jahit,$unit_packing,$id_gudang,$periode_so,$tahun,$bulan,$id_jenis_so){
		if($unit_jahit != 0){
		$sql=$this->db->get("tb_stok_opname_unit_jahit where id_unit_jahit='$unit_jahit' AND tahun='$tahun'  AND bulan='$bulan'");
	if($sql->num_rows() > 0){
	return TRUE;
    }else{
    return FALSE;}
	}
	elseif($unit_packing != 0){
		$sql=$this->db->get("tb_stok_opname_unit_packing where id_unit_packing='$unit_packing' AND tahun='$tahun'  AND bulan='$bulan'");
	if($sql->num_rows() > 0){
	return TRUE;
    }else{
    return FALSE;}
	}
     if($id_gudang != 0){
		 if($id_jenis_so==1){
			$sql=$this->db->get("tb_stok_opname_gudang_wip where id_gudang_wip='$id_gudang' AND tahun='$tahun'  AND bulan='$bulan'");
				if($sql->num_rows() > 0){
					return TRUE;
						}else{
						return FALSE;
					}
				}
		 elseif($id_jenis_so==2){
			$sql=$this->db->get("tb_stok_opname_gudang_bb where id_gudang_bb='$id_gudang' AND tahun='$tahun'  AND bulan='$bulan'");
				if($sql->num_rows() > 0){
					return TRUE;
						}else{
						return FALSE;}
					}		
				}
			}
	
		
		public function get_all_paged_custom(){
		
		
    $query = $this->db->query("
    SELECT distinct id, id_gudang_wip ,0 as  id_gudang_bb ,0 as  id_unit_jahit ,0 as  id_unit_packing  ,bulan,tahun,tanggal_so from tb_stok_opname_gudang_wip 
    UNION  ALL
    SELECT distinct id,0 as  id_gudang_wip  , id_gudang_bb ,0 as  id_unit_jahit ,0 as  id_unit_packing   ,bulan,tahun, tanggal_so from tb_stok_opname_gudang_bb
    UNION ALL
    SELECT distinct id,0 as  id_gudang_wip ,0 as  id_gudang_bb , id_unit_jahit ,0 as  id_unit_packing  ,bulan,tahun, tanggal_so from tb_stok_opname_unit_jahit
    UNION ALL
    SELECT distinct id,0 as  id_gudang_wip ,0 as  id_gudang_bb ,0 as  id_unit_jahit , id_unit_packing  ,bulan,tahun, tanggal_so from tb_stok_opname_unit_packing
     ");
    if($query->num_rows() >0){
		$hasil = $query->result();
			foreach($hasil as $row){
				$query2= $this->db->query(" SELECT nama_gudang from tb_master_gudang where id =$row->id_gudang_wip");
					if($query2->num_rows() > 0){
						$hasil2 = $query2->row();
						$nama_gudang_wip =$hasil2->nama_gudang;
						}
						else{
							$nama_gudang_wip ='';
							}
							
				$query3= $this->db->query(" SELECT nama_gudang from tb_master_gudang where id =$row->id_gudang_bb");
					if($query3->num_rows() > 0){
						$hasil3 = $query3->row();
						$nama_gudang_bb =$hasil3->nama_gudang;
						}
						else{
							$nama_gudang_bb ='';
							}
				$query4= $this->db->query(" SELECT nama_unit_jahit from tb_master_unit_jahit where id =$row->id_unit_jahit");
					if($query4->num_rows() > 0){
						$hasil4 = $query4->row();
						$nama_unit_jahit =$hasil4->nama_unit_jahit;
						}
						else{
							$nama_unit_jahit ='';
							}		
				$query5= $this->db->query(" SELECT nama_unit_packing from tb_master_unit_packing where id =$row->id_unit_packing");
					if($query5->num_rows() > 0){
						$hasil5 = $query5->row();
						$nama_unit_packing =$hasil5->nama_unit_packing;
						}
						else{
							$nama_unit_packing ='';
							}
							
		
					$data[]=array(
			'id'=>$row->id,
			'id_gudang_wip'=>$row->id_gudang_wip,
			'id_gudang_bb'=>$row->id_gudang_bb,
			'id_unit_jahit'=>$row->id_unit_jahit,
			'id_unit_packing'=>$row->id_unit_packing,
			'bulan'=>$row->bulan,
			'tahun'=>$row->tahun,
			'tanggal_so'=>$row->tanggal_so,
			'nama_gudang_wip'=>$nama_gudang_wip,
			'nama_gudang_bb'=>$nama_gudang_bb,
			'nama_unit_jahit'=>$nama_unit_jahit,
			'nama_unit_packing'=>$nama_unit_packing
			);		
				}
				
		
		
		}
		
		else{
			$data=null;
			}
		return $data;
	}
	
	public function get_all_paged_cari(){
		
		$kata_kunci=$this->input->get('kata_kunci');
		
    $query = $this->db->query("
    SELECT distinct id, id_gudang_wip ,0 as  id_gudang_bb ,0 as  id_unit_jahit ,0 as  id_unit_packing  ,bulan,tahun,tanggal_so 
    from tb_stok_opname_gudang_wip where bulan like '%$kata_kunci%' OR cast(tahun as varchar) like '%$kata_kunci%'
    UNION  ALL
    SELECT distinct id,0 as  id_gudang_wip  , id_gudang_bb ,0 as  id_unit_jahit ,0 as  id_unit_packing   ,bulan,tahun, tanggal_so 
    from tb_stok_opname_gudang_bb where bulan like '%$kata_kunci%' OR cast(tahun as varchar) like '%$kata_kunci%'
    UNION ALL
    SELECT distinct id,0 as  id_gudang_wip ,0 as  id_gudang_bb , id_unit_jahit ,0 as  id_unit_packing  ,bulan,tahun, tanggal_so 
    from tb_stok_opname_unit_jahit where bulan like '%$kata_kunci%' OR cast(tahun as varchar) like '%$kata_kunci%'
    UNION ALL
    SELECT distinct id,0 as  id_gudang_wip ,0 as  id_gudang_bb ,0 as  id_unit_jahit , id_unit_packing  ,bulan,tahun, tanggal_so from 
    tb_stok_opname_unit_packing where bulan like '%$kata_kunci%' OR cast(tahun as varchar) like '%$kata_kunci%'
     ");
    if($query->num_rows() >0){
		$hasil = $query->result();
			foreach($hasil as $row){
				$query2= $this->db->query(" SELECT nama_gudang from tb_master_gudang where id =$row->id_gudang_wip");
					if($query2->num_rows() > 0){
						$hasil2 = $query2->row();
						$nama_gudang_wip =$hasil2->nama_gudang;
						}
						else{
							$nama_gudang_wip ='';
							}
							
				$query3= $this->db->query(" SELECT nama_gudang from tb_master_gudang where id =$row->id_gudang_bb");
					if($query3->num_rows() > 0){
						$hasil3 = $query3->row();
						$nama_gudang_bb =$hasil3->nama_gudang;
						}
						else{
							$nama_gudang_bb ='';
							}
				$query4= $this->db->query(" SELECT nama_unit_jahit from tb_master_unit_jahit where id =$row->id_unit_jahit");
					if($query4->num_rows() > 0){
						$hasil4 = $query4->row();
						$nama_unit_jahit =$hasil4->nama_unit_jahit;
						}
						else{
							$nama_unit_jahit ='';
							}		
				$query5= $this->db->query(" SELECT nama_unit_packing from tb_master_unit_packing where id =$row->id_unit_packing");
					if($query5->num_rows() > 0){
						$hasil5 = $query5->row();
						$nama_unit_packing =$hasil5->nama_unit_packing;
						}
						else{
							$nama_unit_packing ='';
							}
							
		
					$data[]=array(
			'id'=>$row->id,
			'id_gudang_wip'=>$row->id_gudang_wip,
			'id_gudang_bb'=>$row->id_gudang_bb,
			'id_unit_jahit'=>$row->id_unit_jahit,
			'id_unit_packing'=>$row->id_unit_packing,
			'bulan'=>$row->bulan,
			'tahun'=>$row->tahun,
			'tanggal_so'=>$row->tanggal_so,
			'nama_gudang_wip'=>$nama_gudang_wip,
			'nama_gudang_bb'=>$nama_gudang_bb,
			'nama_unit_jahit'=>$nama_unit_jahit,
			'nama_unit_packing'=>$nama_unit_packing
			);		
				}
				
		
		
		}
		return $data;
	}
		function paging_custom($tipe, $base_url, $uri_segment)
    {
        // Memanggil library pagination.
        $this->load->library('pagination');

        // Konfigurasi.
        $config = array(
            'base_url' => $base_url,
            'uri_segment' => $uri_segment,
            'per_page' => $this->_per_page,
            'use_page_numbers' => true,
            'num_links' => 4,
            'first_link' => '&#124;&lt; First',
            'last_link' => 'Last &gt;&#124;',
            'next_link' => 'Next &gt;',
            'prev_link' => '&lt; Prev',

            // Menyesuaikan untuk Twitter Bootstrap 3.2.0.
            'full_tag_open' => '<ul class="pagination pagination-sm">',
            'full_tag_close' => '</ul>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li class="disabled"><li class="active"><a href="#">',
            'cur_tag_close' => '<span class="sr-only"></span></a></li>',
            'next_tag_open' => '<li>',
            'next_tagl_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tagl_close' => '</li>',
            'first_tag_open' => '<li>',
            'first_tagl_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tagl_close' => '</li>',
        );

        // Jika paging digunakan untuk "pencarian", tambahkan / tampilkan $_GET di URL.
        // Caranya dengan memanipulasi $config['suffix'].
        if ($tipe == 'pencarian') {
            if (count($_GET) > 0) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['total_rows'] = $this->cari_num_rows_custom();
        } else {
            $config['first_url'] = '1';
            $config['total_rows'] = $this->get_all_num_rows_custom();
        }

        // Set konfigurasi.
        $this->pagination->initialize($config);

        // Buat link dan kembalikan link paging yang sudah jadi.
        return $this->pagination->create_links();
    }
	public function get_all_num_rows_custom()
    {
         $query = $this->db->query("
    SELECT id, id_gudang_wip ,0 as  id_gudang_bb ,0 as  id_unit_jahit ,0 as  id_unit_packing  ,bulan,tahun from tb_stok_opname_gudang_wip 
    UNION ALL
    SELECT id,0 as  id_gudang_wip  , id_gudang_bb ,0 as  id_unit_jahit ,0 as  id_unit_packing   ,bulan,tahun from tb_stok_opname_gudang_bb
    UNION ALL
    SELECT id,0 as  id_gudang_wip ,0 as  id_gudang_bb , id_unit_jahit ,0 as  id_unit_packing  ,bulan,tahun from tb_stok_opname_unit_jahit
    UNION ALL
    SELECT id,0 as  id_gudang_wip ,0 as  id_gudang_bb ,0 as  id_unit_jahit , id_unit_packing  ,bulan,tahun from tb_stok_opname_unit_packing
     ");
   
		return $query->num_rows();
		
    }	
    public function hapus($id,$id_gudang_wip,$id_gudang_bb,$id_unit_jahit,$id_unit_packing)
    {
		if($id_unit_jahit != 0){
		$this->db->where('id', $id);
		$this->db->delete('tb_stok_opname_unit_jahit');
		
		$this->db->where('id_stok_opname_unit_jahit', $id);
		$this->db->delete('tb_stok_opname_unit_jahit_detail');
	}
		elseif($id_unit_packing != 0){
		$this->db->where('id', $id);
		$this->db->delete('tb_stok_opname_unit_packing');
		
		$this->db->where('id_stok_opname_unit_packing', $id);
		$this->db->delete('tb_stok_opname_unit_packing_detail');
	}
		elseif($id_gudang_wip != 0){
		$this->db->where('id', $id);
		$this->db->delete('tb_stok_opname_gudang_wip');
		
		$this->db->where('id_stok_opname_gudang_wip', $id);
		$this->db->delete('tb_stok_opname_gudang_wip_detail');
	}
	elseif($id_gudang_bb != 0){
		$this->db->where('id', $id);
		$this->db->delete('tb_stok_opname_gudang_bb');
		
		$this->db->where('id_stok_opname_gudang_bb', $id);
		$this->db->delete('tb_stok_opname_gudang_bb_detail');
	}
}

 public function edit_so($id,$id_gudang_bb,$id_gudang_wip,$id_unit_jahit,$id_unit_packing)
    {
	if($id_unit_jahit!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_opname_unit_jahit where id='$id' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_unit_jahit_detail where id_stok_opname_unit_jahit='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_bb where id=$row3->id_barang_bb");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_bb'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_bb'=>$hasil2->kode_barang_bb,
							'nama_barang_bb'=>$hasil2->nama_barang_bb
							);
						}		
					}
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_unit_jahit where id='$id_unit_jahit'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
							'id_so'=>$row->id,
							'id_unit_jahit'=>$hasil3->id,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
							'nama_unit_jahit'=>$hasil3->nama_unit_jahit,			
							'data_detail'=>$data_detail
							);
			}
		}		
		return $data;
	}
	if($id_unit_packing!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_opname_unit_packing where id='$id' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_unit_packing_detail where id_stok_opname_unit_packing='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_wip where id=$row3->id_barang_wip");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_wip'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_wip'=>$hasil2->kode_barang_wip,
							'nama_barang_wip'=>$hasil2->nama_barang_wip
							);
						}
							
					}
				
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_unit_packing where id='$id_unit_packing'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
						'id_so'=>$row->id,
							'id_unit_packing'=>$hasil3->id,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
							'nama_unit_packing'=>$hasil3->nama_unit_packing,			
							'data_detail'=>$data_detail
							);
			}
		}		
		return $data;
	}
	if($id_gudang_wip!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_opname_gudang_wip where id='$id' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_gudang_wip_detail where id_stok_opname_gudang_wip='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_wip where id=$row3->id_barang_wip");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_wip'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_wip'=>$hasil2->kode_barang_wip,
							'nama_barang_wip'=>$hasil2->nama_barang_wip
							);
						}
							
					}
				
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_gudang where id='$id_gudang_wip'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
						'id_so'=>$row->id,
							'id_gudang'=>$hasil3->id,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
							'nama_gudang'=>$hasil3->nama_gudang,			
							'data_detail'=>$data_detail
							);
			}
		}		
		return $data;
	}
		if($id_gudang_bb!= 0){
		
		$query = $this->db->query("SELECT * from tb_stok_opname_gudang_bb where id='$id' ");
			if($query->num_rows() > 0){
				$hasil=$query->result();
				foreach($hasil as $row){
					$query3 = $this->db->query("SELECT * from tb_stok_opname_gudang_bb_detail where id_stok_opname_gudang_bb='$row->id' ");
						if($query3->num_rows() > 0){
							$hasil3=$query3->result();
							foreach($hasil3 as $row3){
								
							$query2 = $this->db->query("SELECT * from tb_master_barang_bb where id=$row3->id_barang_bb");
								if($query2->num_rows > 0){
								$hasil2= $query2->row();
							}	
							$data_detail[]=array(
							'id_barang_bb'=>$hasil2->id,
							'v_stok_opname'=>$row3->v_stok_opname,
							'id_detail_so'=>$row3->id,
							'kode_barang_bb'=>$hasil2->kode_barang_bb,
							'nama_barang_bb'=>$hasil2->nama_barang_bb
							);
						}
							
					}
				
				}
			$query3 = $this->db->query("SELECT * FROM tb_master_gudang where id='$id_gudang_bb'");
			if($query3->num_rows() > 0){
				$hasil3= $query3->row();
					
				$data[]=array(
						'id_so'=>$row->id,
							'id_gudang'=>$hasil3->id,
							'jenis_hitung'=>$row->jenis_hitung,
							'tanggal_so'=>$row->tanggal_so,	
							'nama_gudang'=>$hasil3->nama_gudang,			
							'data_detail'=>$data_detail
							);
			}
		}		
		return $data;
	}
}
}
