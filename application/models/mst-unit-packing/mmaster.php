<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_unit_packing');
    $this->db->order_by('kode_unit','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_unit_packing',array('kode_unit'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_unit_packing WHERE id = '$id' ");
    return $query->result();
  }
  
  //
  function save($kode,$id_unit_packing,$lokasi, $nama, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_unit_packing ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
		
    $data = array(
      'id'=>$idbaru,
      'kode_unit'=>$kode,
      'lokasi'=>$lokasi,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_unit_packing',$data); }
	else {
		
		$data = array(
		  'kode_unit'=>$kode,
		  'lokasi'=>$lokasi,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_unit_packing);
		$this->db->update('tm_unit_packing',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_unit_packing', array('id' => $id));
  }

}

