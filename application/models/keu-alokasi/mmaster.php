<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();
  }
  
  function getArea(){
    $this->db->select('*');
    $this->db->from('tm_area');
    $this->db->order_by('kode_area','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
   function getBank(){
    $this->db->select('*');
    $this->db->from('tm_bank');
    $this->db->order_by('kode','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function getAll($i_area,$date_from,$date_to){
	  $query=$this->db->query("SELECT a.*,b.id as i_kbank,b.nama as nama_bank,
	  c.nama as nama_area, c.id as i_area,c.lokasi  FROM tt_kas_bank a inner join tm_bank b ON a.id_coa_bank=b.id_coa 
	  inner join tm_area c ON a.id_area=c.id
	  where a.id_area='1' and a.tgl >= to_date('$date_from','DD-MM-YYYY') 
	  and a.kode_coa='110-41000'and a.f_alokasi ='f'
	  and a.tgl <= to_date('$date_to','DD-MM-YYYY')");
	 
	  if($query->num_rows() > 0){
	  return $query->result();
	  
	  }
	  
}
 function getAllAlo($i_area,$date_from,$date_to){
	 
	  $query=$this->db->query("SELECT * FROM tm_alokasi Where TRUE");
	 
	  if($query->num_rows() > 0){
	  return $query->result();
	  
	  }
}

function getAllAlolimit($i_area,$date_from,$date_to){
	 
	  $query=$this->db->query("SELECT * FROM tm_alokasi Where TRUE");
	 
	  if($query->num_rows() > 0){
		  $hasil=$query->result();
	  foreach($hasil as $row){

		  $query2=$this->db->query("SELECT * FROM tm_alokasi_item  
		  where i_alokasi='$row->i_alokasi'");
			if($query2->num_rows() > 0){
				$hasil2 =$query2->result();
				
					foreach ($hasil2 as $row2){
			
						$nota=$row2->kd_nota;
						$i_nota=$row2->i_nota;
						$d_nota=$row2->d_nota;
						$v_jumlah=$row2->v_jumlah;
						$v_sisa=$row2->v_sisa;
						
						$data_detail[]=array(
						'nota'=>$nota,
						'i_nota'=>$i_nota,
						'd_nota'=>$d_nota,
						'v_jumlah'=>$v_jumlah,
						'v_sisa'=>$v_sisa,
						);	
						
						}		
				}
				
					$query3 = $this->db->query("SELECT * FROM tm_area where id='$row->i_area'");
						if($query3->num_rows() > 0){
							$hasil3=$query3->row();
							$kode_area=$hasil3->kode_area;
							$nama_area=$hasil3->nama;
							$lokasi=$hasil3->lokasi;
					}
				
		   $data[]=array(
				'i_alokasi'=>$row->i_alokasi,
				'no_transaksi'=>$row->no_transaksi,
				'kode_area'=>$kode_area,
				'nama_area'=>$nama_area,
				'lokasi'=>$lokasi,
				'i_kbank'=>$row->i_kbank,
				'i_area'=>$row->i_area,
				'e_customer_name'=>$row->e_customer_name,
				'e_bank_name'=>$row->e_bank_name,
				'd_entry'=>$row->d_entry,
				'data_detail'=>$data_detail
				);
		$data_detail=array(); 	
		  }
		return $data;
				
	  }
	 
}

function getAllitem($id){
	  $query=$this->db->query("SELECT a.*,b.id as i_bank,b.nama as nama_bank,b.id_coa as id_coa_bank,
	  c.nama as nama_area, c.id as i_area,c.lokasi  FROM tt_kas_bank a inner join tm_bank b ON a.id_coa_bank=b.id_coa 
	  inner join tm_area c ON a.id_area=c.id
	  where a.id='$id'  ");
	 
	  if($query->num_rows() > 0){
	  $hasil=  $query->result();
	  foreach ($hasil as $row){
		    $query2=$this->db->query("SELECT * FROM tm_alokasi a inner join tm_alokasi_item b ON a.i_alokasi=b.i_alokasi where a.i_kbank='$id'");
				if($query2->num_rows() > 0){
					$hasil2 =$query2->result();
					foreach ($hasil2 as $row2){
						
						$nota=$row2->kd_nota;
						$i_nota=$row2->i_nota;
						$d_nota=$row2->d_nota;
						$v_jumlah=$row2->v_jumlah;
						$v_bayar=$row2->v_bayar;
						$v_sisa=$row2->v_sisa;
						$v_lebih=$row2->v_lebih;
						$d_alokasi=$row2->d_alokasi;
						$e_customer_name=$row2->e_customer_name;
						$e_remark=$row2->e_remark;
						$e_alamat=$row2->e_alamat;
						$e_bank_name=$row2->e_customer_name;
						$i_kategory=$row2->i_kategory;
						
						$data_detail[]=array(
						'nota'=>$nota,
						'i_nota'=>$i_nota,
						'd_nota'=>$d_nota,
						'v_jumlah'=>$v_jumlah,
						'v_bayar'=>$v_bayar,
						'v_sisa'=>$v_sisa,
						'v_lebih'=>$v_lebih,
						'd_alokasi'=>$d_alokasi,
						'e_remark'=>$e_remark,
						'i_kategory'=>$i_kategory,
					);	
					
						}
						
					
					
				}
				
			//	 $query3=$this->db->query("SELECT * FROM tm_alokasi where i_alokasi=$");
				
		  }
		  $data[]=array(
		  'i_kbank'=>$row->id,
				'i_bank'=>$row->i_bank,
				'nama_bank'=>$row->nama_bank,
				'id_coa_bank'=>$row->id_coa_bank,
				'nama_area'=>$row->nama_area,
				'i_area'=>$row->i_area,
				'lokasi'=>$row->lokasi,
				'bulan'=>$row->bulan,
				'tahun'=>$row->tahun,
				'tgl'=>$row->tgl,
				'deskripsi'=>$row->deskripsi,
				'no_transaksi'=>$row->no_transaksi,
				'd_alokasi'=>$row2->d_alokasi,
				'e_bank_name'=>$e_bank_name,
				'e_alamat'=>$e_alamat,
				'jumlah'=>$row->jumlah,
				'e_customer_name'=>$e_customer_name,
				'data_detail'=>$data_detail
				);
		  return $data;
	  }
}
function getAllitem_old($i_area,$date_from,$date_to,$id){
	  $query=$this->db->query("SELECT a.*,b.id as i_kbank,b.nama as nama_bank,b.id_coa as id_coa_bank,
	  c.nama as nama_area, c.id as i_area,c.lokasi  FROM tt_kas_bank a inner join tm_bank b ON a.id_coa_bank=b.id_coa 
	  inner join tm_area c ON a.id_area=c.id
	  where a.id_area='1' and a.tgl >= to_date('$date_from','DD-MM-YYYY') 
	  and a.kode_coa='110-4100' and a.id='$id'
	  and a.tgl <= to_date('$date_to','DD-MM-YYYY')");
	 
	  if($query->num_rows() > 0){
	  return $query->result_array();
	  
	  }
}

function getdebittur(){
	 $db2 = $this->load->database('db_external', TRUE);
	 $query3=$db2->query("SELECT * from tr_customer order by i_customer_code");

	 if($query3->num_rows() > 0){
		 
		 return $query3->result();
		 
		 } 
}
function getnota($date_from){

	 $db2 = $this->load->database('db_external', TRUE);
	 $query3=$db2->query("SELECT * FROM (
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,v_sisa_alo,kategory FROM tm_faktur WHERE f_faktur_cancel='f' AND v_sisa_alo > 0
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,v_sisa_alo,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'  AND v_sisa_alo > 0
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,v_sisa_alo,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f' AND v_sisa_alo > 0
			)x where d_faktur < to_date('$date_from','DD-MM-YYYY') ");

	 if($query3->num_rows() > 0){
		 
		 return $query3->result();
		 
		 } 
}
function getdebitturlimit($limit,$offset){
	 $db2 = $this->load->database('db_external', TRUE);
	 $query3=$db2->query("SELECT * from tr_customer order by i_customer_code ",false);

	 if($query3->num_rows() > 0){
		 
		 return $query3->result();
		 
		 } 
}
function getnotalimit($limit,$offset,$date_from){
	 $db2 = $this->load->database('db_external', TRUE);
	 $db2->select(" * FROM (
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,v_sisa_alo,kategory FROM tm_faktur WHERE f_faktur_cancel='f' AND v_sisa_alo > 0
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,v_sisa_alo,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'  AND v_sisa_alo > 0
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,v_sisa_alo,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f' AND v_sisa_alo > 0
			)x where d_faktur < to_date('$date_from','DD-MM-YYYY') order by d_faktur DESC ",false);
	$db2->limit($limit,$offset);
	$query3=	$db2->get();
	 if($query3->num_rows() > 0){
		 
		 return $query3->result();
		 
		 } 
}

  function insertheader(  $ikbank,$iarea,$icustomer,$dbank,$dalokasi,$ebankname, $vjumlah,$vlebih,$icoabank,$debitur,$alamat,$no_bank)
  {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_alokasi (i_kbank,i_area,i_customer,d_alokasi,e_bank_name,v_jumlah,v_lebih,d_entry,i_coa_bank,e_customer_name,e_alamat,no_transaksi)
                        values
                      ('$ikbank','$iarea','$icustomer','$dalokasi','$ebankname',$vjumlah,$vlebih,'$dentry','$icoabank','$debitur','$alamat','$no_bank')");
                      
	$query2 =$this->db->query("Select i_alokasi from tm_alokasi order by i_alokasi DESC LIMIT 1");
	if($query2->num_rows() > 0){
		$hasil2= $query2->row();
		return $hasil2->i_alokasi;
		}
  }
  
  function updatebank($ikbank,$icoabank,$iarea,$pengurang)
    {
      $this->db->select(" v_sisa from tt_kas_bank
                          where id='$ikbank' and id_coa_bank='$icoabank'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $xx){

            $this->db->query("update tt_kas_bank set v_sisa='$pengurang',f_alokasi='true' where id='$ikbank' and id_coa_bank='$icoabank'");
            return true;
          
        }
      }else{
        return false;
      }
    }
    
     function cekheader($ikbank,$i_area)
  {
                
	$query2 =$this->db->query("Select i_alokasi from tm_alokasi where i_kbank='$ikbank'");
	if($query2->num_rows() > 0){
		$hasil2= $query2->row();
		return $hasil2->i_alokasi;
		}
  }
  function delheadalo($i_alok,$i_kbank)
  {
                
	$query2 =$this->db->query("Select * from tm_alokasi where i_alokasi='$i_alok'");
	if($query2->num_rows() > 0){
		$hasil2= $query2->result();
		foreach ($hasil2 as $row2){
			$query3 =$this->db->query("Select * from tm_alokasi_item where i_alokasi='$row2->i_alokasi'");
					if($query3->num_rows() >0){
						$hasil3 =$query3->result();
							foreach($hasil3 as $row3){
								$query7 =$this->db->query("Select * from tt_kas_bank where id='$i_kbank'");
								if($query7->num_rows() > 0){
									$hasil7 = $query7->result();
										foreach ($hasil7 as $row7){
								$query4 =$this->db->query("update tt_kas_bank SET v_sisa=$row7->v_sisa + $row3->v_bayar,f_alokasi='f'  where id='$i_kbank'");
							
							}
						}
					}
				}
			}
			$query5 =$this->db->query("Delete from tm_alokasi where i_alokasi='$i_alok'");
			$query6 =$this->db->query("Delete from tm_alokasi_item where i_alokasi='$i_alok'");
		}
	}
    
   function insertdetail($ialokasi,$ibank,$iarea,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$icoabank,$kdnota,$kategory,$vbayar,$ikbank)
  {
    $tmp=$this->db->query(" select i_alokasi from tm_alokasi_item
                            where i_alokasi='$ialokasi' and i_area='$iarea' and i_nota='$inota' and i_kbank='$ikbank' 
                            and i_coa_bank='$icoabank'", false);
    if($tmp->num_rows()>0){
      $this->db->query("update tm_alokasi_item set d_nota='$dnota',v_jumlah=$vjumlah,v_sisa=$vsisa,n_item_no=$i,
                        e_remark='$eremark'
                        where i_alokasi='$ialokasi' and i_area='$iarea' and i_nota='$inota' and i_kbank='$ikbank' 
                        and i_coa_bank='$icoabank'");
    }else{
        $this->db->query("insert into tm_alokasi_item
                      ( i_alokasi,i_kbank,i_area,i_nota,d_nota,v_jumlah,v_sisa,n_item_no,e_remark,i_coa_bank,kd_nota,i_kategory,v_bayar,i_bank)
                      values
                      ('$ialokasi','$ikbank','$iarea','$inota','$dnota',$vjumlah,$vsisa,$i,'$eremark','$icoabank','$kdnota','$kategory','$vbayar','$ibank')");
    }
  }
  
   function updatedetail($ialokasi,$ibank,$iarea,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$icoabank,$kdnota,$kategory,$vbayar,$ikbank)
  {
    $tmp=$this->db->query(" select i_alokasi from tm_alokasi_item
                            where i_alokasi='$ialokasi' and i_area='$iarea' and i_nota='$inota' and i_kbank='$ikbank' 
                            and i_coa_bank='$icoabank'", false);
    if($tmp->num_rows()>0){
      $this->db->query("update tm_alokasi_item set d_nota='$dnota',v_jumlah=$vjumlah,v_sisa=$vsisa,n_item_no=$i,
                        e_remark='$eremark'
                        where i_alokasi='$ialokasi' and i_area='$iarea' and i_nota='$inota' and i_kbank='$ikbank' 
                        and i_coa_bank='$icoabank'");
    }else{
        $this->db->query("insert into tm_alokasi_item
                      ( i_alokasi,i_kbank,i_area,i_nota,d_nota,v_jumlah,v_sisa,n_item_no,e_remark,i_coa_bank,kd_nota,i_kategory,v_bayar,i_bank)
                      values
                      ('$ialokasi','$ikbank','$iarea','$inota','$dnota',$vjumlah,$vsisa,$i,'$eremark','$icoabank','$kdnota','$kategory','$vbayar','$ibank')");
    }
  }
  function updatenota($inota, $kdnota,$kategory,$dnota,$vsisa)
    {
		$db2=$this->load->database('db_external',TRUE);
	$query2 =     $db2->query("SELECT *  FROM (
				SELECT i_faktur, i_faktur_code,d_faktur,v_sisa_alo,kategory FROM tm_faktur WHERE f_faktur_cancel='f'
				UNION
				SELECT i_faktur, i_faktur_code,d_faktur,v_sisa_alo,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
				UNION
				SELECT i_faktur, i_faktur_code,d_faktur,v_sisa_alo,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
				)x
				WHERE i_faktur='$inota' AND d_faktur='$dnota' AND i_faktur_code='$kdnota'  AND kategory='$kategory' ", false);
     
      if ($query2->num_rows() > 0){
        foreach($query2->result() as $xx){
          $sisa=$xx->v_sisa_alo-$vsisa;
          if($sisa<0){
            return false;
            break;
          }if($vsisa==0){
			  if($xx->kategory == 1){
						$db2->query("UPDATE tm_faktur  SET v_sisa_alo='$vsisa',f_alokasi='t' 
							WHERE CAST(i_faktur_code as integer)= $xx->i_faktur_code ")	;
							return true;
						}
			
			elseif($xx->kategory == 2){	
						$db2->query("UPDATE tm_faktur_do_t  SET v_sisa_alo='$vsisa',f_alokasi='t' 
							WHERE CAST(i_faktur_code as integer)= $xx->i_faktur_code ")	;	
							return true;
						}
			elseif($xx->kategory == 3){
						$db2->query("UPDATE tm_faktur_bhnbaku  SET v_sisa_alo='$vsisa',f_alokasi='t' 
							WHERE CAST(i_faktur_code as integer)= '$xx->i_faktur_code' ")	;
							return true;
						}			
           
          }
          else{
			  if($xx->kategory == 1){
						$db2->query("UPDATE tm_faktur  SET v_sisa_alo='$vsisa'
							WHERE CAST(i_faktur_code as integer)= $xx->i_faktur_code ")	;
							return true;
						}
			elseif($xx->kategory == 3){
						$db2->query("UPDATE tm_faktur_bhnbaku  SET v_sisa_alo='$vsisa'
							WHERE CAST(i_faktur_code as integer)= $xx->i_faktur_code ")	;
							return true;
						}
			elseif($xx->kategory == 2){	
						$db2->query("UPDATE tm_faktur_do_t  SET v_sisa_alo='$vsisa'
							WHERE CAST(i_faktur_code as integer)= $xx->i_faktur_code ")	;	
							return true;
						}
			  
			  }
        }
      }else{
        return false;
      }
    }
}
