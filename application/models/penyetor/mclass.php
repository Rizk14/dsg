<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function cekpenyetor($epenyetor){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_penyetor WHERE e_penyetor='$epenyetor' ");	
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_penyetor ORDER BY i_penyetor DESC ", false);
	}

	function viewperpages($limit,$offset) {	
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_penyetor ORDER BY i_penyetor DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($epenyetor) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$str = array(
			'e_penyetor'=>$epenyetor,
			'd_entry'=>$dentry
		);
		
		$db2->insert('tr_penyetor',$str);
		redirect('penyetor/cform/');
    }
	
	function mupdate($ipenyetor,$epenyetor) {
		$db2=$this->load->database('db_external', TRUE);
		$penyetor_item	= array(
			'e_penyetor'=>$epenyetor
		);
		$db2->update('tr_penyetor',$penyetor_item,array('i_penyetor'=>$ipenyetor));
		redirect('penyetor/cform');
	}

	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_penyetor WHERE i_penyetor='$id' ");
	}
	
	function viewcari($ep) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_penyetor WHERE e_penyetor LIKE '%$ep%' ");
	}
	
	function mcari($ep,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_penyetor WHERE e_penyetor LIKE '%$ep%' LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function delete($id) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_penyetor',array('i_penyetor'=>$id));
		redirect('penyetor/cform/');	 
	}
}
?>
