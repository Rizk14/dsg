<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function cari_fpenjualan($fpenj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_nonrupiah WHERE i_faktur_code=trim('$fpenj') AND f_faktur_cancel='f' ");
	}
	
	function cari_fpajak($fpajak,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur WHERE substring(i_faktur_code,1,4)='$tahun' and i_faktur_pajak='$fpajak' AND f_faktur_cancel='f' ");
	}
	
	function getnomorfaktur() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function nofakturpajak() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	function cari_fpenjualan_do($fpenj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur_code=trim('$fpenj')  AND f_faktur_cancel='f' ");
	}

	function cari_fpajak_do($fpajak,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_do_t WHERE substring(i_faktur_code,1,4)='$tahun' AND i_faktur_pajak='$fpajak'  AND f_faktur_cancel='f' ");
	}
		
	function tahunnow_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn	= date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thnfaktur 
			
			FROM tm_faktur_do_t 
			
			WHERE SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn'
			
			ORDER BY i_faktur DESC LIMIT 1 ");	
	}
	
	function getnomorfaktur_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}	
	
	function getnomorfaktur_bhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur_bhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_bhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	// ================= 22-07-2013 ========================================
	function getnomorfaktur_jahit() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_jahit WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur_jahit() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_jahit WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}
	// ======================================================================
	
	function lcabang() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$db2->select(" a.e_initial AS codebranch, 
				    a.i_customer AS codecustomer, 
				    a.e_branch_name AS branch,
					a.e_initial AS einitial FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		} else {
			return false;
		}
	}
	
	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT  i_product_motif as imotif, e_product_motifname AS productname
										FROM tr_product_motif
										ORDER BY e_product_motifname ASC LIMIT ".$limit." OFFSET ".$offset);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}	
	}

	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		$qstr = "SELECT  i_product_motif as imotif, e_product_motifname AS productname
										FROM tr_product_motif
										ORDER BY e_product_motifname ASC";
		return $db2->query($qstr);
	}

	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= $key;
			$qstr = "SELECT  i_product_motif as imotif, e_product_motifname AS productname
					FROM tr_product_motif WHERE i_product_motif = '$ky_upper'
					ORDER BY e_product_motifname ASC";

		return $db2->query($qstr);
	}
			
	/*
	function lbarangjadiperpages($ibbk,$limit,$offset){
		$query = $db2->query("
			SELECT c.i_bbk,
				c.i_product AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				c.v_product_price AS hjp,
				c.n_unit AS qty,
				c.v_unit_price AS nilai
				
			FROM tm_bbk_item c 
			
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
			
			WHERE b.n_active='1' AND d.i_status_bbk='2' AND d.f_faktur_created=false AND c.i_bbk='$ibbk' AND d.f_return=false
			
			ORDER BY d.i_bbk DESC LIMIT ".$limit." OFFSET ".$offset);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi($ibbk){
		return $db2->query("
			SELECT c.i_bbk,
				c.i_product AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				c.v_product_price AS hjp,
				c.n_unit AS qty,
				c.v_unit_price AS nilai
				
			FROM tm_bbk_item c 
			
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
			
			WHERE b.n_active='1' AND d.i_status_bbk='2' AND d.f_faktur_created=false AND c.i_bbk='$ibbk' AND d.f_return=false
			
			ORDER BY d.i_bbk DESC ");
	}
	
	function flbarangjadi($key,$ibbk) {
		$ky_upper	= strtoupper($key);
		return $db2->query("
			SELECT c.i_bbk,
				c.i_product AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				c.v_product_price AS hjp,
				c.n_unit AS qty,
				c.v_unit_price AS nilai
				
			FROM tm_bbk_item c 
			
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
			
			WHERE b.n_active='1' AND d.i_status_bbk='2' AND d.f_faktur_created=false AND (b.i_product_motif='$ky_upper' OR a.e_product_basename LIKE '$key') AND c.i_bbk='$ibbk' AND d.f_return=false
			
			ORDER BY d.i_bbk DESC ");
	}
	*/
	/*
	function lpelanggan() {
		return $db2->query("
			SELECT c.i_bbk AS nomorbbk,
				d.e_bbk_to AS kepada,
				d.e_bbk_address AS alamat,
				d.d_bbk AS tglbbk
							
			FROM tm_bbk_item c 
					
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
						
			WHERE d.i_status_bbk='2' AND d.f_faktur_created=false 
			
			GROUP BY c.i_bbk, d.e_bbk_to, d.e_bbk_address, d.d_bbk
			
			ORDER BY c.i_bbk DESC	
		");	
	}

	function lpelangganperpages($limit,$offset) {
		$query	= $db2->query("
			SELECT c.i_bbk AS nomorbbk,
				d.e_bbk_to AS kepada,
				d.e_bbk_address AS alamat,
				d.d_bbk AS tglbbk
							
			FROM tm_bbk_item c 
					
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
						
			WHERE d.i_status_bbk='2' AND d.f_faktur_created=false 
			
			GROUP BY c.i_bbk, d.e_bbk_to, d.e_bbk_address, d.d_bbk
			
			ORDER BY c.i_bbk DESC LIMIT ".$limit." OFFSET ".$offset);

		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
					
	}

	function fpelanggan($key) {
		$ky_upper	= strtoupper($key);
		return $db2->query("
			SELECT c.i_bbk AS nomorbbk,
				d.e_bbk_to AS kepada,
				d.e_bbk_address AS alamat,
				d.d_bbk AS tglbbk
							
			FROM tm_bbk_item c 
					
			INNER JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
						
			WHERE d.i_status_bbk='2' AND d.f_faktur_created=false AND c.i_bbk='$ky_upper'
			
			GROUP BY c.i_bbk, d.e_bbk_to, d.e_bbk_address, d.d_bbk
			
			ORDER BY c.i_bbk DESC		
		");	
	}
	*/

	// $i_faktur,$nw_d_faktur,$nama_pelanggan,$e_note_faktur,$nw_v_total_faktur,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration
	function msimpan($i_faktur,$nw_d_faktur,$nama_pelanggan,$e_note_faktur,$nw_v_total_faktur,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration) {
		$db2=$this->load->database('db_external', TRUE);
		$i_faktur_item	= array();
		$tm_faktur_item	= array();
		
		$jml_item_br	= array();
		$qty_akhir	= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	

		$seq_tm_faktur	= $db2->query(" SELECT cast(i_faktur AS integer) AS i_faktur FROM tm_faktur_nonrupiah ORDER BY cast(i_faktur AS integer) DESC LIMIT 1 ");
		
		if($seq_tm_faktur->num_rows() >0 ) {
			$seqrow	= $seq_tm_faktur->row();
			$ifaktur	= $seqrow->i_faktur+1;
		}else{
			$ifaktur	= 1;
		}
		
		$db2->set(
			array(
			 'i_faktur'=>$ifaktur,
			 'i_faktur_code'=>$i_faktur,
			 'd_faktur'=>$nw_d_faktur,
			 'e_branch_name'=>$nama_pelanggan,
			 'v_total_faktur'=>$nw_v_total_faktur,
			 'f_printed'=>'f',
			 'e_note_faktur'=>$e_note_faktur,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry));
		
		if($db2->insert('tm_faktur_nonrupiah')) {
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				$seq_tm_i_faktur_item	= $db2->query(" SELECT cast(i_faktur_item AS integer) AS i_faktur_item FROM tm_faktur_nonrupiah_item ORDER BY cast(i_faktur_item AS integer) DESC LIMIT 1 ");
				
				if($seq_tm_i_faktur_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_i_faktur_item->row();
					$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
				}else{
					$i_faktur_item[$jumlah]	= 1;
				}
				
				$tm_faktur_item[$jumlah]	= array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$ifaktur,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_hjp[$jumlah],
					 'd_entry'=>$dentry );
				
				$db2->insert('tm_faktur_nonrupiah_item',$tm_faktur_item[$jumlah]);
			}

			if ($db2->trans_status()===FALSE) {
				$db2->trans_rollback();
			}else{
				$db2->trans_commit();
			}
							
			print "<script>alert(\"Nomor Faktur (Dlm Dolar) : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");show(\"fakpenjualandolar/cform\",\"#content\");</script>";
		}else{
			print "<script>alert(\"Maaf, Faktur (Dolar) gagal disimpan. Terimakasih.\");show(\"fakpenjualandolar/cform\",\"#content\");</script>";
		}
	}	
}
?>
