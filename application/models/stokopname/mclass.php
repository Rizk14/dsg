<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function ckstokopname($stopproduksi) {
		$db2=$this->load->database('db_external', TRUE);
		$sproduksi	= ($stopproduksi==1 || $stopproduksi=='1')?'TRUE':'FALSE';
		return $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$sproduksi' ORDER BY i_so DESC LIMIT 1 ");
	}
	
	function mutasistockopname($d_so_baru,$d_so_lama,$i_product,$e_product_name,$n_quantity_akhir,$n_quantity_fisik,$iteration,$iterasi,$stopproduksi, $stok_fisik, $i_color) {		
		$db2=$this->load->database('db_external', TRUE);
		$iso_item	= array();
		$i_so_item	= array();
		
		$sproduksi	= ($stopproduksi==1 || $stopproduksi=='1')?'TRUE':'FALSE';
		$d_input	= date("Y-m-d");
		
		$qcariopname	= $db2->query(" SELECT * FROM tm_stokopname WHERE f_stop_produksi='$sproduksi' AND d_so='$d_so_baru' ");
		// 17-03-2015
		$hasilcariopname	= $qcariopname->row();
		$ncariopname	= $qcariopname->num_rows();
		
		if($ncariopname==0){
			$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
			$drow	= $qdate->row();
			$dentry	= $drow->date;
			
			$seq_tm_stokopname	= $db2->query(" SELECT cast(i_so AS integer) AS iso FROM tm_stokopname ORDER BY cast(i_so AS integer) DESC LIMIT 1 ");
			if($seq_tm_stokopname->num_rows() > 0 ) {
				$seqrow	= $seq_tm_stokopname->row();
				$i_so	= $seqrow->iso+1;
			} else {
				$i_so	= 1;
			}
			
			/*
			status	= 1 (telah dilakukan stokopname)
			status	= 0 (stok yg digunakan saat ini/ belum di stokopname)
			*/
			
			$qiso_lama	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE d_so='$d_so_lama' AND f_stop_produksi='$sproduksi' AND i_status_so='0' "); 
			$riso_lama	= $qiso_lama->row();
			
			$db2->set(array(
					'i_so'=>$i_so,
					'd_so'=>$d_so_baru,
					'i_status_so'=>'0',
					'f_stop_produksi'=>$sproduksi,
					'd_entry'=>$d_input,
					'd_update'=>$d_input,
					'i_so_lama'=>$riso_lama->i_so));
			
			if($db2->insert('tm_stokopname')) {
				
				$update_tm_stokopname	= array(
						'i_status_so'=>'1'
				);
				$db2->update('tm_stokopname', $update_tm_stokopname, array('d_so'=>$d_so_lama,'i_status_so'=>'0','f_stop_produksi'=>$sproduksi));
				
				$j=0;
				
				$imutasinya	= array();
				$insert_stokmutasi	= array();
				$update_stokmutasi	= array();
				$ii_mutasi_old	= array();
				$insert_stokmutasi_new	= array();
				
				for($jumlah=0; $jumlah<=$iterasi; $jumlah++) {
					
					
					$seq_tm_stokopname_item	= $db2->query(" SELECT cast(i_so_item AS integer) AS iso_item FROM tm_stokopname_item ORDER BY cast(i_so_item AS integer) DESC LIMIT 1 ");
					if($seq_tm_stokopname_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_stokopname_item->row();
						$i_so_item[$j]	= $seqrow->iso_item+1;
					}else{
						$i_so_item[$j]	= 1;
					}
					
					$db2->set(array(
						'i_so_item'=>$i_so_item[$j],
						'i_so'=>$i_so,
						'i_product'=>$i_product[$j],
						'n_quantity_awal'=>$n_quantity_fisik[$j],
						'n_quantity_akhir'=>$n_quantity_fisik[$j]
					));
					
					$db2->insert('tm_stokopname_item');
					
					$imutasi	= $db2->query(" SELECT cast(i_mutasi AS integer)+1 AS imutasi FROM tm_stokmutasi ORDER BY cast(i_mutasi AS integer) DESC LIMIT 1 ");
					if($imutasi->num_rows()>0) {
						$row_imutasi	= $imutasi->row();
						$imutasinya[$j]	= $row_imutasi->imutasi;
					}else{
						$imutasinya[$j]	= 1;
					}
					
					$qstokmutasi	= $db2->query(" SELECT * FROM tm_stokmutasi WHERE i_product='$i_product[$j]' AND f_active_month='t' ORDER BY cast(i_mutasi AS integer) DESC LIMIT 1 ");
					if($qstokmutasi->num_rows()>0) { // Update kemudian insert
						$row_stokmutasi	= $qstokmutasi->row();
						$ii_mutasi_old[$j]	= $row_stokmutasi->i_mutasi;

						$insert_stokmutasi[$j]	= array(
							'i_mutasi'=>$imutasinya[$j],
							'i_product'=>$i_product[$j],
							'f_stop_produksi'=>$sproduksi,
							'n_quantity_trans'=>$n_quantity_akhir[$j],
							'n_quantity_fisik'=>$n_quantity_fisik[$j],
							'f_active_month'=>'TRUE',
							'd_entry'=>$dentry,
							'i_so'=>$i_so
						);
						
						$db2->insert('tm_stokmutasi',$insert_stokmutasi[$j]);

						$update_stokmutasi[$j]	= array(
								'f_active_month'=>'FALSE'
						);
						$db2->update('tm_stokmutasi', $update_stokmutasi[$j], array('i_mutasi'=>$ii_mutasi_old[$j],'i_product'=>$i_product[$j],'f_stop_produksi'=>$sproduksi));
						
					}else{ // Insert Baru
						$insert_stokmutasi_new[$j]	= array(
							'i_mutasi'=>$imutasinya[$j],
							'i_product'=>$i_product[$j],
							'f_stop_produksi'=>$sproduksi,
							'n_quantity_trans'=>$n_quantity_akhir[$j],
							'n_quantity_fisik'=>$n_quantity_fisik[$j],
							'f_active_month'=>'TRUE',
							'd_entry'=>$dentry,
							'i_so'=>$i_so
						);
						$db2->insert('tm_stokmutasi',$insert_stokmutasi_new[$j]);				
					}
					
					// 10-10-2014, SO per warna =================
						for ($xx=0; $xx<count($i_color[$j]); $xx++) {
							
							//* PERBAIKAN 7 OKT 2021 i_so_item_color nya ditentuin disini dari no terakhir + 1
							$seq_tm_stokopname_item_color = $db2->query(" SELECT cast(i_so_item_color AS integer) AS iso_item_color FROM tm_stokopname_item_color ORDER BY cast(i_so_item_color AS integer) DESC LIMIT 1 ");
							if($seq_tm_stokopname_item_color->num_rows() > 0 ) {
								$seqrow1	= $seq_tm_stokopname_item_color->row();
								$i_so_item_color[$xx]	= $seqrow1->iso_item_color+1;
							}else{
								$i_so_item_color[$xx]	= 1;
							}

							if (is_array($i_color[$j])) {
								$stokwarna	= array(
									'i_so_item_color'=>$i_so_item_color[$xx],
									'i_so_item'=>$i_so_item[$j],
									'i_color'=>$i_color[$j][$xx],
									'n_quantity_awal'=>$stok_fisik[$j][$xx],
									'n_quantity_akhir'=>$stok_fisik[$j][$xx]
								);
								$db2->insert('tm_stokopname_item_color',$stokwarna);	
							}
						}
					
					//===========================================
					
					// $j+=1;
					$j++;
				}
				print "<script>alert(\"Stokopname bln ini telah disimpan, terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
			}else{
				print "<script>alert(\"Stokopname bln ini gagal disimpan, terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
			}
		}else{
		//	print "<script>alert(\"Stokopname gagal disimpan, Tgl. Opname sdh ada!\");show(\"stokopname/cformmutasi\",\"#content\");</script>";
			// modif 17-03-2015. disini fungsi utk edit SO. HANYA TEMP AJA NANTINYA HRS DI-OFF-KAN LAGI SKRIP INI
			// ambil id so, trus ambil i_so_item ke tabel stokopname_item, trus query ke tabel stokopname_item_color, kemudian update
			
			// 18-03-2015, ditutup lagi
			// 19-12-2015 DIBUKA UTK UPDATE QTY PER WARNA YG BLM ADA
// 21-12-2015 tutup lagi, soalnya mau ditambah manual aja di database
// 15-01-2016 dibuka utk edit data SO
			$i_so	= $hasilcariopname->i_so;
			for($jj=0; $jj<=$iterasi; $jj++) {
				$queryxx	= $db2->query(" SELECT i_so_item FROM tm_stokopname_item 
										 WHERE i_so = '$i_so' AND i_product='".$i_product[$jj]."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$i_so_item	= $hasilxx->i_so_item;

					$db2->query(" update tm_stokopname_item set n_quantity_awal='".$n_quantity_fisik[$jj]."',
						n_quantity_akhir = '".$n_quantity_fisik[$jj]."' where i_so_item = '$i_so_item' ");
					
					for ($xx=0; $xx<count($i_color[$jj]); $xx++) {
						if (is_array($i_color[$jj])) {
							
							// 18-03-2015
							// ambil n_quantity_akhir dari query, nantinya quantity_akhir ditambahkan dgn quantity_awal
							$queryxx2	= $db2->query(" SELECT n_quantity_akhir FROM tm_stokopname_item_color 
										 WHERE i_so_item = '$i_so_item' AND i_color='".$i_color[$jj][$xx]."' ");
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$qtyakhir	= $hasilxx2->n_quantity_akhir;
								
								$qtyakhirnew = $qtyakhir+$stok_fisik[$jj][$xx];
								// n_quantity_akhir diganti dari $qtyakhirnew jadi $stok_fisik
								$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_awal='".$stok_fisik[$jj][$xx]."',
									n_quantity_akhir='".$stok_fisik[$jj][$xx]."' WHERE i_so_item='$i_so_item' 
									AND i_color='".$i_color[$jj][$xx]."' "); 
							}
							/*else {
								$stokwarna	= array(
									'i_so_item'=>$i_so_item,
									'i_color'=>$i_color[$jj][$xx],
									'n_quantity_awal'=>$stok_fisik[$jj][$xx],
									'n_quantity_akhir'=>$stok_fisik[$jj][$xx]
								);
								$db2->insert('tm_stokopname_item_color',$stokwarna);	
							} */
							
						} // end if
					} // end for
				} // end if
			} // end for utama
			
			print "<script>alert(\"Update Stokopname per warna berhasil disimpan, terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
                       
			// ------------------------------------------------------------
		}	
		//redirect('stokopname/cform/');
	}
	
	function tso($stopproduksi) {
		$db2=$this->load->database('db_external', TRUE);
	return 	$db2->query(" SELECT  a.i_so,
				a.d_so,
				b.i_so_item
				
		FROM tm_stokopname a 
		
		RIGHT JOIN tm_stokopname_item b ON a.i_so=b.i_so
		INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product 
		INNER JOIN tr_product_base d ON d.i_product_base=c.i_product

	 	WHERE a.i_status_so='0' AND d.f_stop_produksi=$stopproduksi
 
        ORDER BY b.i_product ASC ");	
	}
	
	function lso($d_date_n,$stopproduksi) {
		$db2=$this->load->database('db_external', TRUE);
	
	/* WHERE a.d_so >= '$d_date_n' AND a.d_so <= '$d_date_n' */
	return 	$db2->query(" SELECT  a.i_so,
				a.d_so,
				b.i_so_item,
				b.i_product AS iproduct,
				b.n_quantity_awal AS awal,
				b.n_quantity_akhir AS akhir,
				c.e_product_motifname AS motifname
				
		FROM tm_stokopname a 
		
		RIGHT JOIN tm_stokopname_item b ON a.i_so=b.i_so
		INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product 
		INNER JOIN tr_product_base d ON d.i_product_base=c.i_product

	 	WHERE a.d_so = '$d_date_n' AND a.i_status_so='0' AND d.f_stop_produksi='$stopproduksi' ORDER BY b.i_product ASC ");
	}
	
	function listbrgjadi() {
	$db2=$this->load->database('db_external', TRUE);
	/* Disabled 09-02-2011
	return $db2->query(" SELECT  a.i_product_base,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty
			
		FROM tr_product_base a 
		
		INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product 
		
		WHERE b.i_product_motif NOT IN (SELECT c.i_product FROM tm_stokopname d LEFT JOIN tm_stokopname_item c ON c.i_so=d.i_so) " );
	*/
	return $db2->query(" SELECT  a.i_product_base AS ibaseproduct,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty,
			a.f_stop_produksi
					
		FROM tr_product_motif b 
				
		INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
			
		WHERE trim(b.i_product_motif) NOT IN (SELECT trim(c.i_product) AS iproduct FROM tm_stokopname d INNER JOIN tm_stokopname_item c ON c.i_so=d.i_so GROUP BY c.i_product) AND b.n_active=1 
		
		GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update
		
		ORDER BY a.d_update DESC ");
	}
	
	function listbrgjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
	/* Disabled 09-02-2011
	$query	= $db2->query(" SELECT  a.i_product_base,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty
			
		FROM tr_product_base a 
		
		INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product 
		
		WHERE b.i_product_motif NOT IN (SELECT c.i_product FROM tm_stokopname d LEFT JOIN tm_stokopname_item c ON c.i_so=d.i_so) LIMIT ".$limit." OFFSET ".$offset );
	*/
	/* 06-05-2011
	 AND b.n_quantity!=0
	*/	
	
	$query	= $db2->query(" SELECT  a.i_product_base AS ibaseproduct,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty,
			a.f_stop_produksi
				
		FROM tr_product_motif b 
				
		INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
			
		WHERE trim(b.i_product_motif) NOT IN (SELECT trim(c.i_product) AS iproduct FROM tm_stokopname d INNER JOIN tm_stokopname_item c ON c.i_so=d.i_so GROUP BY c.i_product) AND b.n_active=1
		
		GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update ORDER BY a.d_update DESC LIMIT ".$limit." OFFSET ".$offset );
				
		if ($query->num_rows() > 0 ) {
			return $query->result();
		}
	}
	
	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		//$ky_upper	= strtoupper($key);
		/* Disabled 09-02-2011
		return $db2->query(" SELECT  a.i_product_base,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty
			
		FROM tr_product_base a 
		
		INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product 
		
		WHERE b.i_product_motif NOT IN (SELECT c.i_product FROM tm_stokopname d LEFT JOIN tm_stokopname_item c ON c.i_so=d.i_so) AND b.i_product_motif='$ky_upper' " );
		*/
	return $db2->query(" SELECT  a.i_product_base AS ibaseproduct,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty,
			a.f_stop_produksi
			
		FROM tr_product_motif b 
				
		INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
			
		WHERE trim(b.i_product_motif) NOT IN (SELECT trim(c.i_product) AS iproduct FROM tm_stokopname d INNER JOIN tm_stokopname_item c ON c.i_so=d.i_so GROUP BY c.i_product) AND b.n_active=1 AND b.i_product_motif=trim('$key')
		
		GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update ORDER BY a.d_update DESC ");		
	}
	
	
	function msimpanbaru($d_so,$i_product,$e_product_name,$qty_warna, $i_product_color, $i_color, $f_stop_produksi,$iteration) {
		$db2=$this->load->database('db_external', TRUE);
		$i_so_item	= array();
		$ar_isoopname = array();
		
		/*
		$seq_tm_stokopname	= $db2->query( " SELECT cast(i_so AS integer)+1 AS iso FROM tm_stokopname ORDER BY cast(i_so AS integer) DESC LIMIT 1 " );
		if($seq_tm_stokopname->num_rows() > 0 ) {
			$seqrow	= $seq_tm_stokopname->row();
			$i_so	= $seqrow->iso;
		}else{
			$i_so	= 1;
		}
		*/
		
		/*
		$db2->set(array(
			'i_so'=>$i_so,
			'd_so'=>$d_so,
			'i_status_so'=>0
		));
		*/
		
		//if($db2->insert('tm_stokopname')) {
			for($x=0;$x<=$iteration;$x++) {
				
				$q_tm_opname = $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so=0 AND f_stop_produksi='$f_stop_produksi[$x]' ORDER BY i_so DESC LIMIT 1 ");
				
				if($q_tm_opname->num_rows()>0) {
					
					$isoopname	= $q_tm_opname->row();
					
					$seq_tm_stokopname_item	= $db2->query(" SELECT i_so_item+1 AS isoitem FROM tm_stokopname_item ORDER BY i_so_item DESC LIMIT 1 ");
					if($seq_tm_stokopname_item->num_rows()>0) {
						$seqrow	= $seq_tm_stokopname_item->row();
						$i_so_item[$x]	= $seqrow->isoitem;
					}else{
						$i_so_item[$x]	= 1;
					}
					
					//if($n_quantity_akhir[$x]=='')
					//	$n_quantity_akhir[$x]	= 0;
					
					// 25-03-2015
					//-------------- hitung total qty dari detail tiap2 warna -------------------
					$qtytotal = 0;
					for ($xx=0; $xx<count($i_color[$x]); $xx++) {

						$i_product_color[$x][$xx] = trim($i_product_color[$x][$xx]);
						$i_color[$x][$xx] = trim($i_color[$x][$xx]);
						$qty_warna[$x][$xx] = trim($qty_warna[$x][$xx]);
														
						$qtytotal+= $qty_warna[$x][$xx];
					} // end for
					// ---------------------------------- END -----------------------------------
						
					$ar_isoopname[$x] = array(
						'i_so_item'=>$i_so_item[$x],
						'i_so'=>$isoopname->i_so,
						'i_product'=>$i_product[$x],
						//'n_quantity_awal'=>$n_quantity_akhir[$x],
						//'n_quantity_akhir'=>$n_quantity_akhir[$x]
						'n_quantity_awal'=>$qtytotal,
						'n_quantity_akhir'=>$qtytotal
					);
					
					//$db2->set($ar_isoopname[$jumlah]);

					if($db2->insert('tm_stokopname_item',$ar_isoopname[$x])) {
						
						// 25-03-2015
						for ($xx=0; $xx<count($i_color[$x]); $xx++) {
							$i_product_color[$x][$xx] = trim($i_product_color[$x][$xx]);
							$i_color[$x][$xx] = trim($i_color[$x][$xx]);
							$qty_warna[$x][$xx] = trim($qty_warna[$x][$xx]);
							
							/*$seq_tm_inbonm_item_color	= $db2->query(" SELECT i_inbonm_item_color FROM tm_inbonm_item_color 
														ORDER BY i_inbonm_item_color DESC LIMIT 1 ");
						
							if($seq_tm_inbonm_item_color->num_rows() > 0) {
								$seqrow	= $seq_tm_inbonm_item_color->row();
								$i_inbonm_item_color[$jumlah]	= $seqrow->i_inbonm_item_color+1;
							}else{
								$i_inbonm_item_color[$jumlah]	= 1;
							}

							$tm_bonmmasuk_item_color[$jumlah]	= array(
								 'i_inbonm_item_color'=>$i_inbonm_item_color[$jumlah],
								 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
								 'i_product_color'=>$i_product_color[$jumlah][$xx],
								 'i_color'=>$i_color[$jumlah][$xx],
								 'qty'=>$qty_warna[$jumlah][$xx]
							);
							$db2->insert('tm_inbonm_item_color',$tm_bonmmasuk_item_color[$jumlah]); */
							
							$seq_tm_stokopname_item_color	= $db2->query(" SELECT i_so_item_color+1 AS isoitemcolor FROM tm_stokopname_item_color ORDER BY i_so_item_color DESC LIMIT 1 ");
							if($seq_tm_stokopname_item_color->num_rows()>0) {
								$seqriw	= $seq_tm_stokopname_item_color->row();
								$i_so_item_color	= $seqriw->isoitemcolor;
							}else{
								$i_so_item_color	= 1;
							}

							// update stok per warna
								$sqlwarna = " SELECT * FROM tm_stokopname_item_color WHERE i_so_item = '".$i_so_item[$x]."'
											AND i_color = '".$i_color[$x][$xx]."' ";
								$querywarna = $db2->query($sqlwarna);
								
								if($querywarna->num_rows() == 0) {
									
									$sowarna[$x]	= array(
										 'i_so_item_color'=>$i_so_item_color,
										 'i_so_item'=>$i_so_item[$x],
										 'i_color'=>$i_color[$x][$xx],
										 'n_quantity_awal'=>$qty_warna[$x][$xx],
										 'n_quantity_akhir'=>$qty_warna[$x][$xx]
									);
									$db2->insert('tm_stokopname_item_color',$sowarna[$x]);
							
									/*$hasilwarna = $querywarna->row();
									
									$qtyawalwarna	= $hasilwarna->n_quantity_awal;
									$qtyakhirwarna	= $hasilwarna->n_quantity_akhir;

									$back_qty_awalwarna	= $qtyawalwarna;
									$back_qty_akhirwarna	= $qtyakhirwarna+$qty_warna[$jumlah][$xx];
									
									if($back_qty_akhirwarna=="")
										$back_qty_akhirwarna	= 0;
																			
									$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir = '$back_qty_akhirwarna'
														WHERE i_so_item = '$i_so_item' AND i_color = '".$i_color[$jumlah][$xx]."' "); */
									
									
								} // end if
							
						//} // end if
					} // end for
					// ------------------------------------------

						$qdate	= $db2->query( " SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date " );
						$drow	= $qdate->row();
						$dentry	= $drow->date;
			
						$isoxxx	=	$db2->query(" SELECT * FROM tm_so WHERE i_product_motif='$i_product[$x]' ORDER BY cast(i_so AS integer) DESC LIMIT 1 ");
						if($isoxxx->num_rows() < 0 || $isoxxx->num_rows()==0) {
							$iso	=	$db2->query(" SELECT * FROM tm_so ORDER BY cast(i_so AS integer) DESC LIMIT 1 ");
							if($iso->num_rows() > 0 ) {
								$iso_row	= $iso->row_array();
								$i_so_new	= $iso_row['i_so']+1;
							} else {
								$i_so_new	= 1;
							}
							
							$iproductmotif	= $db2->query(" SELECT a.i_product_motif, 
																		a.n_quantity AS qty,
																		b.i_product_base, a.e_product_motifname 
											FROM tr_product_motif a 
											
											INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
											
											WHERE i_product_motif='$i_product[$x]' LIMIT 1 ");
											
							if($iproductmotif->num_rows() > 0) {
								$motif_row	= $iproductmotif->row();
								$iproductmotif	= $motif_row->i_product_motif;
								$iproduct	= $motif_row->i_product_base;
								$motifname	= $motif_row->e_product_motifname;
								$qty		= $motif_row->qty;
			
								$iso_item	= array(
									'i_so'=>$i_so_new,
									'i_product'=>$iproduct,
									'i_product_motif'=>$iproductmotif,
									'e_product_motifname'=>$motifname,
									'i_status_do'=>'0',
									'n_saldo_awal'=>$qty,
									'n_saldo_akhir'=>$qty,
									'd_entry'=>$dentry
								);
								$db2->insert('tm_so', $iso_item);
							}						
						}
						
						$qmutasistokopname	= $db2->query(" SELECT * FROM tm_stokmutasi WHERE i_product='$i_product[$x]' AND f_stop_produksi='$f_stop_produksi[$x]' AND f_active_month='t' ");	
						
						if($qmutasistokopname->num_rows()==0 || $qmutasistokopname->num_rows()<0) {
							
							$qnummutasi	= $db2->query(" SELECT cast(i_mutasi AS integer)+1 AS i_mutasi FROM tm_stokmutasi ORDER BY cast(i_mutasi AS integer) DESC LIMIT 1 ");
							
							if($qnummutasi->num_rows()>0) {
								$rnummutasi	= $qnummutasi->row();
								$imutas	= $rnummutasi->i_mutasi;
							}else{
								$imutas	= 1;
							}
							
							$db2->query(" INSERT INTO tm_stokmutasi(i_mutasi,i_product,f_stop_produksi,n_quantity_trans,n_quantity_fisik,f_active_month,d_entry,i_so,d_update) 
									VALUES('$imutas','$i_product[$x]','$f_stop_produksi[$x]','$qtytotal','$qtytotal','t','$dentry','$isoopname->i_so','$dentry') ");
							// '$n_quantity_akhir[$x]'
						}
					}
				}
				
			}
			
			redirect('stokopname/cformbaru/');
		//}
		
	}
}
?>
