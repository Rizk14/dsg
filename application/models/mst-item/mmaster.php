<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_item');
    $this->db->order_by('kode','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_item',array('kode'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_item WHERE kode = '$id' ");
    return $query->result();		  
  }
  
  //
  function save($kode,$kodeedit, $nama, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'kode'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_item',$data); }
	else {
		
		$data = array(
		  'kode'=>$kode,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('kode',$kodeedit);
		$this->db->update('tm_item',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_item', array('kode' => $kode));
  }
  
  function cek_data($kode){
    $this->db->select("* FROM tm_item WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
