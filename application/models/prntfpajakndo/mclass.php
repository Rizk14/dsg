<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
		
	function clistpenjualanndo($nofaktur) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( "
				SELECT 	c.i_product AS imotif,
					c.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj_code)=trim(c.i_sj)

				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true " );
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function clistfpenjndo_header($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		/*
		Ini dipakai utk menmpikan info : Nomor Faktur, Tanggal Faktur, Tgl Jatuh Tempo
		*/
		return $db2->query( "
		
				SELECT	count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname
				
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj_code)=trim(c.i_sj)
				
				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name " );
	}

	function clistfpenjndo_jml($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT 	b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					sum((a.n_quantity * a.v_unit_price)) AS total
			
			FROM tm_faktur_item a
					
			RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
			INNER JOIN tm_sj d ON trim(d.i_sj_code)=trim(c.i_sj)
			
			WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true

			GROUP BY b.n_discount, b.v_discount " );
	}

	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
										
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj_code)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel=false AND d.f_faktur_created=true
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj_code)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel=false AND d.f_faktur_created=true
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}
	
	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj_code)=trim(c.i_sj)
				
				WHERE a.i_faktur_code='$key' AND d.f_sj_cancel=false AND d.f_faktur_created=true
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );	
	}
	
	function lklsbrg() {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC " );
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	
	function ititas() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT a.* FROM tr_initial_company a ORDER BY i_initial, i_initial_code DESC LIMIT 1 " );
	}
	
	function pelanggan($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT a.e_customer_name  AS customername,
				   a.e_customer_address AS customeraddress,
				   a.e_customer_npwp AS npwp
			
			FROM tr_customer a
			
			INNER JOIN tr_branch b ON b.i_customer=a.i_customer
			INNER JOIN tm_faktur c ON c.e_branch_name=b.e_initial
			INNER JOIN tm_faktur_item d ON d.i_faktur=c.i_faktur
			
			WHERE c.i_faktur_code='$nofaktur'
			
			GROUP BY a.e_customer_name, a.e_customer_address, a.e_customer_npwp
		");
	}
	
	function pajak($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT	b.i_faktur_pajak AS ifakturpajak,
					b.d_pajak
			
			FROM tm_faktur b
			
			INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
			
			INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
			INNER JOIN tm_sj d ON trim(d.i_sj_code)=trim(c.i_sj)
			
			WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true
			
			GROUP BY b.i_faktur_pajak, b.d_pajak ");
	}	
}
?>
