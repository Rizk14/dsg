<?php
class Export_makloon_baju_gudang_jadi_wip_model extends MY_Model
{
	protected $_per_page = 10;
    protected $_tabel = 'tb_stokopname_unit';
    protected $form_rules = array(
 
        
        
        array(
            'field' => 'unit_jahit',
            'label' => 'Unit Jahit',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'unit_packing',
            'label' => 'Unit Packing',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'gudang',
            'label' => 'Gudang',
            'rules' => 'trim|required|max_length[16]'
        ),
       
        
        
    );

     public $default_values = array(
		'id'	=>	'',
		'num'		=>1,
		'no_sj' => '',
        'tgl_sj' => '',
		'jenis_masuk' => '',
		'tanggal_sj_ke'=>'',
		'tanggal_sj_dari'=>'',
		'keterangan_header'=>'',
    );
    
     public function get_unit_jahit()
    {
     $sql=$this->db->query("SELECT id,nama_unit_jahit FROM tb_master_unit_jahit order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }    
    public function get_unit_packing()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_unit_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }  
    public function get_gudang()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_gudang order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }
     public function get_all_inner_paged($offset,$tanggal_sj_dari,$tanggal_sj_ke,$jenis_masuk,$unit_jahit,$unit_packing,$gudang)
    {
		$pencarian1='';
		if($tanggal_sj_dari != ''){
		$pencarian1.="AND a.tanggal_sj >='$tanggal_sj_dari'";
		}
		$pencarian2='';
		if($tanggal_sj_ke != ''){
		$pencarian2.="AND a.tanggal_sj <='$tanggal_sj_ke'";
		}
		$pencarian3='';
		if($jenis_masuk != ''){
		$pencarian3.="AND a.jenis_masuk='$jenis_masuk'";
		}
		$pencarian4='';
		if($unit_jahit != 0){
		$pencarian4.="AND a.id_unit_jahit='$unit_jahit'";
		}
		$pencarian5='';
		if($unit_packing !=0){
		$pencarian5.="AND a.id_unit_packing='$unit_packing'";	
		}
		$pencarian6='';
		if($gudang != 0){
		$pencarian6.="AND a.id_gudang='$gudang'";	
		}
		
		
     $select=$this->db->query("SELECT a.id as id_header,a.no_sj,a.jenis_masuk,a.id_unit_jahit,a.id_unit_packing,a.jenis_pembelian,a.jenis_pembelian,
     a.status_pembelian,a.status_aktif as status_aktif_header,a.status_edit as status_edit_header,a.created_at as created_at_header,a.created_by as created_by_header,
     a.updated_at as updated_at_header,a.updated_by as updated_by_header,a.keterangan as keterangan_header,a.id_gudang,a.tanggal_sj,
     b.id as id_detail,b.id_makloon_baju_gudang_jadi_wip,b.id_barang_wip,b.qty,b.harga,b.diskon,b.status_pembelian_detail,b.keterangan as keterangan_detail,
     b.created_at as created_at_detail, b.created_by as created_by_detail, b.updated_at as updated_at_detail, b.updated_by as updated_by_detail,b.status_aktif as status_aktif_detail
     FROM tb_makloon_baju_gudang_jadi_wip a inner join tb_makloon_baju_gudang_jadi_wip_detail b
     ON a.id=b.id_makloon_baju_gudang_jadi_wip where a.status_aktif='t' AND b.status_aktif='t'"
     .$pencarian1." ".$pencarian2." ".$pencarian3." ".$pencarian4."  ".$pencarian5." ".$pencarian6." order by a.updated_at");   
            
     if($select->num_rows() > 0){
		 return $select->result();
		 }
    }
}
