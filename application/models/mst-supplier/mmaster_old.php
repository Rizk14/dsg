<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari){
	if ($cari == "all")
		$this->db->select("* from tm_supplier ORDER BY kode_supplier", false)->limit($num,$offset);
	else {
		$this->db->select("* from tm_supplier WHERE UPPER(nama) like UPPER('%$cari%') OR UPPER(kode_supplier) like UPPER('%$cari%') ORDER BY kode_supplier", false)->limit($num,$offset);
	
	}

    $query = $this->db->get();
    
    return $query->result();
  }
  
  function getAlltanpalimit($cari){
	//$this->db->select('*');
    //$this->db->from('tm_supplier');
    //$query = $this->db->get();
    
    if ($cari == "all")
		$query = $this->db->query("SELECT * FROM tm_supplier");
	else
		$query = $this->db->query("SELECT * FROM tm_supplier WHERE UPPER(nama) like UPPER('%$cari%') OR UPPER(kode_supplier) like UPPER('%$cari%') ");
    
    return $query->result();  
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_supplier',array('kode_supplier'=>$id));
    $query = $this->db->query(" SELECT * FROM tm_supplier WHERE kode_supplier = '$id' ");
    return $query->result();
  }
  
  function cek_data($kode){
    $this->db->select("kode_supplier from tm_supplier WHERE kode_supplier = '$kode'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode,$nama, $alamat,$kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $kodeedit, $goedit){  
    $tgl = date("Y-m-d");
    if ($pkp == '')
		$pkp = 'f';
    $data = array(
      'kode_supplier'=>$kode,
      'nama'=>$nama,
      'alamat'=>$alamat,
      'kota'=>$kota,
      'kontak_person'=>$kontak_person,
      'telp'=>$telp,
      'fax'=>$fax,
      'top'=>$top,
      'pkp'=>$pkp,
      'npwp'=>$npwp,
      'nama_npwp'=>$nama_npwp,
      'tipe_pajak'=>$tipe_pajak,
      'kategori'=>$kategori,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_supplier',$data); }
	else {
		$data = array(
		  'kode_supplier'=>$kode,
		  'nama'=>$nama,
		  'alamat'=>$alamat,
		  'kota'=>$kota,
		  'kontak_person'=>$kontak_person,
		  'telp'=>$telp,
		  'fax'=>$fax,
		  'top'=>$top,
		  'pkp'=>$pkp,
		  'npwp'=>$npwp,
		  'nama_npwp'=>$nama_npwp,
		  'tipe_pajak'=>$tipe_pajak,
		  'kategori'=>$kategori,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('kode_supplier',$kodeedit);
		$this->db->update('tm_supplier',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_supplier', array('kode_supplier' => $kode));
  }

}

