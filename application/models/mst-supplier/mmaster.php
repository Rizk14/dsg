<?php
class Mmaster extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  function getAll($num, $offset, $cari)
  {
    if ($cari == "all")
      $this->db->select("a.* ,b.nama as nama_kategory FROM tm_supplier a inner join tm_kategory_sup b ON a.kategori=b.id ORDER BY kode_supplier", false)->limit($num, $offset);
    else {
      $this->db->select("a.* ,b.nama as nama_kategory FROM tm_supplier a inner join tm_kategory_sup b ON a.kategori=b.id WHERE UPPER(a.nama) like UPPER('%$cari%') OR UPPER(kode_supplier) like UPPER('%$cari%') ORDER BY kode_supplier", false)->limit($num, $offset);
    }

    $query = $this->db->get();

    return $query->result();
  }

  function getAlltanpalimit($cari)
  {
    //$this->db->select('*');
    //$this->db->from('tm_supplier');
    //$query = $this->db->get();

    if ($cari == "all")
      $query = $this->db->query("SELECT a.* ,b.nama as nama_kategory FROM tm_supplier a inner join tm_kategory_sup b ON a.kategori=b.id ORDER BY kode_supplier");
    else
      $query = $this->db->query("SELECT a.* ,b.nama as nama_kategory FROM tm_supplier a inner join tm_kategory_sup b ON a.kategori=b.id WHERE UPPER(a.nama) like UPPER('%$cari%') OR UPPER(kode_supplier) like UPPER('%$cari%') ORDER BY kode_supplier ");

    return $query->result();
  }

  function get($id)
  {
    //$query = $this->db->getwhere('tm_supplier',array('kode_supplier'=>$id));
    // 09-02-2015
    $query = $this->db->query("SELECT * FROM tm_supplier WHERE id = '$id'");
    return $query->result();
  }

  function getekategori()
  {
    //$query = $this->db->getwhere('tm_supplier',array('kode_supplier'=>$id));
    // 09-02-2015
    $query = $this->db->query("SELECT * FROM tm_kategory_sup ");
    return $query->result();
  }

  function getekategorijenis()
  {
    //$query = $this->db->getwhere('tm_supplier',array('kode_supplier'=>$id));
    // 09-02-2015
    $query = $this->db->query("SELECT * FROM tm_jns_kat_sup ");
    return $query->result();
  }
  //~ function getekategori($kategori){
  //~ $penc="";
  //~ if($kategori != 0)
  //~ $penc="AND id=$kategori";
  //~ //$query = $this->db->getwhere('tm_supplier',array('kode_supplier'=>$id));
  //~ // 09-02-2015
  //~ $query = $this->db->query("SELECT * FROM tm_kategory_sup where TRUE ".$penc);
  //~ return $query->result();
  //~ }
  //~ 
  //~ function getekategorijenis($jenis){
  //~ //$query = $this->db->getwhere('tm_supplier',array('kode_supplier'=>$id));
  //~ // 09-02-2015
  //~ $penc="";
  //~ if($jenis != 0)
  //~ $penc="AND id=$jenis";
  //~ $query = $this->db->query("SELECT * FROM tm_jns_kat_sup where TRUE ".$penc);
  //~ return $query->result();
  //~ }
  function cek_data($kode)
  {
    $this->db->select("kode_supplier from tm_supplier WHERE kode_supplier = '$kode'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }

  //
  function save($kode, $nama, $alamat, $kota, $kontak_person, $telp, $fax, $top, $pkp, $npwp, $nama_npwp, $tipe_pajak, $kategori, $id_supplier, $goedit, $jenis, $statusaktif, $paymenttype, $bankname, $accountnumber, $accountname, $email)
  {
    $tgl = date("Y-m-d H:i:s");

    // ambil id tertinggi
    $sql2 = " SELECT id FROM tm_supplier ORDER BY id DESC LIMIT 1 ";
    $query2  = $this->db->query($sql2);
    if ($query2->num_rows() > 0) {
      $hasil2 = $query2->row();
      $idlama  = $hasil2->id;
      $idbaru = $idlama + 1;
    } else
      $idbaru = 1;

    if ($pkp == '')
      $pkp = 'f';
    $data = array(
      'id' => $idbaru,
      'kode_supplier' => $kode,
      'nama' => $nama,
      'alamat' => $alamat,
      'kota' => $kota,
      'kontak_person' => $kontak_person,
      'telp' => $telp,
      'fax' => $fax,
      'top' => $top,
      'pkp' => $pkp,
      'npwp' => $npwp,
      'nama_npwp' => $nama_npwp,
      'tipe_pajak' => $tipe_pajak,
      'kategori' => $kategori,
      'jenis' => $jenis,
      'tgl_input' => $tgl,
      'tgl_update' => $tgl,
      /* PENAMBAHAN 11 AGS 2022 */
      'payment_type' => $paymenttype,
      'bank_name' => $bankname,
      'account_number' => $accountnumber,
      'account_name' => $accountname,
      'email' => $email
      /* ***************** */
    );

    if ($goedit == '') {
      $this->db->insert('tm_supplier', $data);
    } else {
      $data = array(
        // 29-06-2015 kode_supplier ga perlu diedit
        //'kode_supplier'=>$kode,
        'nama' => $nama,
        'alamat' => $alamat,
        'kota' => $kota,
        'kontak_person' => $kontak_person,
        'telp' => $telp,
        'fax' => $fax,
        'top' => $top,
        'pkp' => $pkp,
        'npwp' => $npwp,
        'nama_npwp' => $nama_npwp,
        'tipe_pajak' => $tipe_pajak,
        'kategori' => $kategori,
        'jenis' => $jenis,
        'tgl_update' => $tgl,
        'f_aktif' => $statusaktif,
        /* PENAMBAHAN 11 AGS 2022 */
        'payment_type' => $paymenttype,
        'bank_name' => $bankname,
        'account_number' => $accountnumber,
        'account_name' => $accountname,
        'email' => $email
        /* ********************* */
      );

      $this->db->where('id', $id_supplier);
      $this->db->update('tm_supplier', $data);
    }
  }

  function delete($id)
  {
    $this->db->delete('tm_supplier', array('id' => $id));
  }
}
