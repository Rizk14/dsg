<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  function getlistsuplier(){
	$sql = " * FROM tm_supplier ORDER BY kode_supplier ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
  
   function get_all_fakturwip($date_from, $date_to, $id_unit, $jenis_masuk) {
		$sql ="select b.id,b.no_faktur,b.tgl_faktur ,b.jumlah,d.kode_supplier,d.nama FROM tm_pembelian_aplikasi_nofaktur b inner join tm_supplier d on b.id_supplier=d.id
				where tgl_faktur >= to_date('$date_from','dd-mm-yyyy') and tgl_faktur <= to_date('$date_to','dd-mm-yyyy')";
		if ($id_unit != '0')
				$sql.=" and id_supplier='$id_unit'" ;
				$sql.= " ORDER BY id_supplier ASC,tgl_faktur ASC, no_faktur ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM 
				 tm_pembelian_aplikasi_nofaktur_sj WHERE id_pembelian_aplikasi_nofaktur = '$row1->id' ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tm_pembelian_aplikasi b  inner join 	tm_pembelian_aplikasi_detail a
						ON a.id_pembelian_aplikasi=b.id  WHERE a.id='$row2->id_sj_pembelian_aplikasi' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$id_brg	= $hasilrow->id_brg;
							$satuan = "Pieces";
							$qty	= $hasilrow->qty;
							$no_sj	= $hasilrow->no_sjmasukpembelian_aplikasi;
							$tgl_sj	= $hasilrow->tgl_sj;
							$harga	= $hasilrow->harga;
							$diskon	= $hasilrow->diskon;
							$total	= $hasilrow->total;
						}
						else {
							
							$satuan		= '';
							$qty		= '';
							$no_sj		= '';
							$tgl_sj		= '';
							$total		= '';
						}
						$query35	= $this->db->query(" SELECT * FROM tm_barang where id='$id_brg' ");
						if ($query35->num_rows() > 0) {
							$hasilrow35 = $query35->row();
							$kode_brg = $hasilrow35->kode_brg;
							$nama_brg = $hasilrow35->nama_brg;
							
						}
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'no_sj'	=> $no_sj,
												'tgl_sj'	=> $tgl_sj,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'harga'=>$harga,
												'diskon'=>$diskon,
												'subtotal'=>$total
											);		
				}	 // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				
				//$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_supplier,
											'nama_unit'=> $row1->nama,
											'grandtotal'=> $row1->jumlah,
											'detail_beli'=> $detail_beli
											
											);
				$detail_beli = array();
			} // endforeach header
			//print_r($data_beli);
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
   function get_all_fakturwip_for_print($date_from, $date_to, $id_unit, $jenis_masuk) {
		$sql ="select b.id,b.no_faktur,b.tgl_faktur ,b.jumlah,d.kode_supplier,d.nama FROM tm_pembelian_aplikasi_nofaktur b inner join tm_supplier d on b.id_supplier=d.id
				where tgl_faktur >= to_date('$date_from','dd-mm-yyyy') and tgl_faktur <= to_date('$date_to','dd-mm-yyyy')";
		if ($id_unit != '0')
				$sql.=" and id_supplier='$id_unit'" ;
				$sql.= " ORDER BY id_supplier ASC,tgl_faktur ASC, no_faktur ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM 
				 tm_pembelian_aplikasi_nofaktur_sj WHERE id_pembelian_aplikasi_nofaktur = '$row1->id' ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tm_pembelian_aplikasi b  inner join 	tm_pembelian_aplikasi_detail a
						ON a.id_pembelian_aplikasi=b.id  WHERE a.id='$row2->id_sj_pembelian_aplikasi' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$id_brg	= $hasilrow->id_brg;
							$satuan = "Pieces";
							$qty	= $hasilrow->qty;
							$no_sj	= $hasilrow->no_sjmasukpembelian_aplikasi;
							$tgl_sj	= $hasilrow->tgl_sj;
							$harga	= $hasilrow->harga;
							$diskon	= $hasilrow->diskon;
							$total	= $hasilrow->total;
						}
						else {
							
							$satuan		= '';
							$qty		= '';
							$no_sj		= '';
							$tgl_sj		= '';
							$total		= '';
						}
						$query35	= $this->db->query(" SELECT * FROM tm_barang where id='$id_brg' ");
						if ($query35->num_rows() > 0) {
							$hasilrow35 = $query35->row();
							$kode_brg = $hasilrow35->kode_brg;
							$nama_brg = $hasilrow35->nama_brg;
							
						}
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'no_sj'	=> $no_sj,
												'tgl_sj'	=> $tgl_sj,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'harga'=>$harga,
												'diskon'=>$diskon,
												'subtotal'=>$total
											);		
				}	 // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				
				//$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_supplier,
											'nama_unit'=> $row1->nama,
											'grandtotal'=> $row1->jumlah,
											'detail_beli'=> $detail_beli
											
											);
				$detail_beli = array();
			} // endforeach header
			//print_r($data_beli);
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
}
?>
