<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$iarea,$ikb,$icoabank)
    {
        $this->db->query("update tm_kbank set f_kbank_cancel='t' WHERE i_kbank='$ikb' and i_periode='$iperiode' 
                          and i_area='$iarea' and f_posting='f' and f_close='f' and i_coa_bank='$icoabank'");
        #####UnPosting      
        $this->db->query("delete from th_jurnal_transharian
                          where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
        $this->db->query("delete from th_jurnal_transharianitem
                          where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
        $this->db->query("delete from th_general_ledger
                          where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");

        $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                          where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
        $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                          where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
        $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                          where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");

        $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
        from tm_general_ledger
        where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=(v_mutasi_debet)-($xx->v_mutasi_debet), 
                            v_mutasi_kredit=(v_mutasi_kredit)-($xx->v_mutasi_kredit),
                            v_saldo_akhir=(v_saldo_akhir)-($xx->v_mutasi_debet)+($xx->v_mutasi_kredit)
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }

      $this->db->query("delete from tm_jurnal_transharian where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
      $this->db->query("delete from tm_general_ledger where i_refference='$ikb' and i_area='$iarea' and i_coa_bank='$icoabank'");
      $quer 	= $this->db->query("SELECT i_pv, i_area, v_pv, i_pv_type, i_coa_bank from tm_pv_item
                                  where i_kk='$ikb' and i_coa_bank='$icoabank' and i_pv_type='02'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_pv set v_pv=v_pv-$xx->v_pv
                            where i_pv='$xx->i_pv' and i_coa='$xx->i_coa_bank' and i_pv_type='$xx->i_pv_type'");
#          $this->db->query("delete from tm_pv_item where i_kk='$ikb' and i_pv='$xx->i_pv' and i_coa_bank='$xx->i_coa_bank' and i_pv_type='$xx->i_pv_type'");
          $this->db->query("update tm_pv_item set f_pv_cancel='t' where i_kk='$ikb' and i_pv='$xx->i_pv' and i_coa_bank='$xx->i_coa_bank' and i_pv_type='$xx->i_pv_type'");
        }
      }
      $quer 	= $this->db->query("SELECT i_rv, i_area, v_rv, i_rv_type, i_coa_bank from tm_rv_item
                                  where i_kk='$ikb' and i_coa_bank='$icoabank' and i_rv_type='02'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_rv set v_rv=v_rv-$xx->v_rv
                            where i_rv='$xx->i_rv' and i_coa='$xx->i_coa_bank' and i_rv_type='$xx->i_rv_type'");
#          $this->db->query("delete from tm_rv_item where i_kk='$ikb' and i_rv='$xx->i_rv' and i_coa_bank='$xx->i_coa_bank' and i_rv_type='$xx->i_rv_type'");
          $this->db->query("update tm_rv_item set f_rv_cancel='t' where i_kk='$ikb' and i_rv='$xx->i_rv' and i_coa_bank='$xx->i_coa_bank' and i_rv_type='$xx->i_rv_type'");
        }
      }

#####
    }
 /*   function bacasemua($area1, $area2, $area3, $area4, $area5, $cari, $num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area
                and a.f_kb_cancel='f'
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } 
    function cari($area1, $area2, $area3, $area4, $area5, $cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_area) like '%$cari%' or upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								order by a.i_area, a.i_jurnal desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
	function bacabank($num,$offset)
    {
		  $this->db->select("* from tr_bank order by i_bank", false)->limit($num,$offset);
  		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function caribank($cari,$num,$offset)
    {
	  $this->db->select("i_bank, e_nama_bank, i_coa from tr_bank where (upper(e_nama_bank) like '%$cari%') 
	                     order by i_bank ", FALSE)->limit($num,$offset);

		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($icoabank,$dfrom,$dto,$ibank,$num,$offset,$cari)
    {
		$this->db->select("	distinct on (i_kbank) vc, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_bank, v_sisa, i_periode, f_debet, f_posting, 
		                    i_cek, e_area_name, i_bank, i_coa_bank, e_nama_bank
	                    	from(
                  			select x.i_rvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                  			a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                  			a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                  			from tr_area e, tm_kbank a
                  			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' 
                  			and b.i_coa_bank='$icoabank')
                  			left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                  			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv 
                  			and x.i_coa_bank='$icoabank')
                  			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                  			where (upper(a.i_kbank) like '%$cari%' or upper(c.i_rv) like '%$cari%') and a.i_area=e.i_area
                  			and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                  			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 'f'
                  			union
                    		select x.i_pvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, 
                    		a.v_sisa, a.i_periode, a.f_debet, a.f_posting, 
                    		a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_nama_bank
                    		from tr_area e, tm_kbank a
                    		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' 
                    		and b.i_coa_bank='$icoabank')
                    		left join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                    		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv 
                    		and x.i_coa_bank='$icoabank')
                    		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                    		where (upper(a.i_kbank) like '%$cari%' or upper(c.i_pv) like '%$cari%') and a.i_area=e.i_area 
                    		and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
                    		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_debet = 't'
                  			) as a
                        ORDER BY i_kbank",false)->limit($num,$offset);
#where not vc isnull
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							where (upper(a.i_kb) like '%$cari%')
							and a.i_area=b.i_area and a.f_kb_cancel='f'
							and a.i_area='$iarea' and
							a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kb <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
