<?php
class Informasi_mutasi_makloon_baju_model extends MY_Model
{
	protected $_per_page = 10;

    protected $form_rules = array(
        
        
         array(
            'field' => 'jenis_masuk',
            'label' => 'jenis_masuk',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'tanggal_sj_dari',
            'label' => 'Tanggal SJ Dari',
            'rules' => 'trim|required|max_length[16]'
        ),
          array(
            'field' => 'tanggal_sj_ke',
            'label' => 'Tanggal SJ Ke',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'unit_jahit',
            'label' => 'Unit Jahit',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'unit_packing',
            'label' => 'Unit Packing',
            'rules' => 'trim|required|max_length[16]'
        ),
          
         array(
            'field' => 'keterangan_header',
            'label' => 'Keterangan Header',
            'rules' => 'trim|required|max_length[16]'
        )
        
        
    );

     public $default_values = array(
		'id'	=>	'',
		'num'		=>1,

        'tanggal_sj_dari' => '',
        'tanggal_sj_ke' => '',
		'jenis_masuk' => '',
	
		'keterangan_header'=>'',
    );
    
     public function get_unit_jahit()
    {
     $sql=$this->db->query("SELECT id,nama_unit_jahit FROM tb_master_unit_jahit order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }    
    public function get_unit_packing()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_unit_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }  
    public function get_gudang()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_gudang order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }
    
    
     public function get_all_inner_paged($tanggal_sj_dari,$tanggal_sj_ke,$id_unit_jahit,$id_unit_packing,$id_gudang)
    {	
		if($id_unit_jahit != 0){
     $sql=$this->db->query("Select id,id_barang_bb from tb_stok_unit_jahit where id_unit_jahit='$id_unit_jahit'");
          
     if($sql->num_rows() > 0){
		 $query= $sql->result();
			 foreach($query as $row){
				
				$sql2=$this->db->query("Select * from tb_master_barang_bb where id='$row->id_barang_bb'");
				 if($sql2->num_rows() > 0){
					 $hasil2 =$sql2->row();
					 $kode_barang_bb = $hasil2->kode_barang_bb;
					 $nama_barang_bb = $hasil2->nama_barang_bb;
					 }
					 $total_masuk_bagus_unit_jahit=0;
					 $total_masuk_lain_unit_jahit=0;
					 $total_keluar_bagus_unit_jahit=0;
					 $total_keluar_lain_unit_jahit=0;
					 $total_stok_opname_jahit=0;
					 $total_saldo_akhir=0;
					 $total_selisih=0;
				 for($x=0; $x <count($query);$x++){
				$sql3=$this->db->query("Select sum(qty) as jum_masuk_unit_jahit from tb_makloon_pengadaan_baju_wip a 
				INNER JOIN  tb_makloon_pengadaan_baju_wip_detail b ON a.id=b.id_makloon_pengadaan_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_jahit='$id_unit_jahit'");
					if($sql3->num_rows() > 0){
						 $hasil3 =$sql3->row();
							$jum_masuk_bagus_unit_jahit = $hasil3->jum_masuk_unit_jahit;
						}
				
				$sql6=$this->db->query("Select sum(qty) as jum_masuk_unit_jahit from tb_makloon_pengadaan_baju_wip a 
				INNER JOIN  tb_makloon_pengadaan_baju_wip_detail b ON a.id=b.id_makloon_pengadaan_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_jahit='$id_unit_jahit'");
					if($sql6->num_rows() > 0){
						 $hasil6 =$sql6->row();
							$jum_masuk_lain_unit_jahit = $hasil3->jum_masuk_unit_jahit;
						}
				
				$sql4=$this->db->query("Select sum(qty) as jum_keluar_unit_jahit from tb_makloon_unit_baju_wip a 
				INNER JOIN  tb_makloon_unit_baju_wip_detail b ON a.id=b.id_makloon_unit_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_jahit='$id_unit_jahit'");
					if($sql4->num_rows() > 0){
						 $hasil4 =$sql4->row();
							$jum_keluar_bagus_unit_jahit = $hasil4->jum_keluar_unit_jahit;
						}
				
				$sql7=$this->db->query("Select sum(qty) as jum_keluar_unit_jahit from tb_makloon_unit_baju_wip a 
				INNER JOIN  tb_makloon_unit_baju_wip_detail b ON a.id=b.id_makloon_unit_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_jahit='$id_unit_jahit'");
					if($sql7->num_rows() > 0){
						 $hasil7 =$sql7->row();
							$jum_keluar_lain_unit_jahit = $hasil7->jum_keluar_unit_jahit;
						}
						
				$sql5=$this->db->query("Select v_stok_opname from tb_stok_opname_unit_jahit a 
				INNER JOIN  tb_stok_opname_unit_jahit_detail b ON a.id=b.id_stok_opname_unit_jahit where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_jahit='$id_unit_jahit'");
					if($sql5->num_rows() > 0){
						 $hasil5 =$sql5->row();
							$jum_stok_opname_jahit = $hasil5->v_stok_opname;
						
					}
					else{
						$jum_stok_opname_jahit = 0;
						
					}
					
					$saldo_akhir = $jum_masuk_bagus_unit_jahit + $jum_masuk_lain_unit_jahit - $jum_keluar_bagus_unit_jahit - $jum_keluar_lain_unit_jahit ;
					$selisih =	$jum_stok_opname_jahit - $saldo_akhir;
						$data_detail[]=array('id_barang_bb'=>$row->id_barang_bb,
						'kode_barang_bb'=>$kode_barang_bb,
						'nama_barang_bb'=>$nama_barang_bb,
						'jum_masuk_bagus_unit_jahit'=>$jum_masuk_bagus_unit_jahit,
						'jum_keluar_bagus_unit_jahit'=>$jum_keluar_bagus_unit_jahit,
						'jum_masuk_lain_unit_jahit'=>$jum_masuk_lain_unit_jahit,
						'jum_keluar_lain_unit_jahit'=>$jum_keluar_lain_unit_jahit,
						'jum_stok_opname_jahit'=>$jum_stok_opname_jahit,
						'saldo_akhir'=>	$saldo_akhir,
						'selisih'=>$selisih
						);
						
						$total_masuk_bagus_unit_jahit+=$jum_masuk_bagus_unit_jahit;
						$total_masuk_lain_unit_jahit+=$jum_masuk_lain_unit_jahit;
						$total_keluar_bagus_unit_jahit+=$jum_keluar_bagus_unit_jahit;
						$total_keluar_lain_unit_jahit+=$jum_keluar_lain_unit_jahit;
						$total_stok_opname_jahit+=$jum_stok_opname_jahit;
						$total_saldo_akhir+=$saldo_akhir;
						$total_selisih+=$selisih;
				}
				
				}	
			}	 
			$sql6=$this->db->query("Select nama_unit_jahit from tb_master_unit_jahit where id='$id_unit_jahit'");
				if($sql6->num_rows() > 0){
						 $hasil6 =$sql6->row();
							$nama_unit_jahit = $hasil6->nama_unit_jahit;
						}
						
				$data[]=array('nama_unit_jahit'=>$nama_unit_jahit,
						'id_unit_jahit'=>$id_unit_jahit,
						'tanggal_sj_dari'=>$tanggal_sj_dari,
						'tanggal_sj_ke'=>$tanggal_sj_ke,
						'total_masuk_bagus_unit_jahit'=>$total_masuk_bagus_unit_jahit,
						'total_masuk_lain_unit_jahit'=>$total_masuk_lain_unit_jahit,
						'total_keluar_bagus_unit_jahit'=>$total_keluar_bagus_unit_jahit,
						'total_keluar_lain_unit_jahit'=>$total_keluar_lain_unit_jahit,
						'total_stok_opname_jahit'=>$total_stok_opname_jahit,
						'total_saldo_akhir'=>$total_saldo_akhir,
						'total_selisih'=>$total_selisih,
						'data_detail'=>$data_detail
						);
				
		}
		if($id_unit_packing != 0){
     $sql=$this->db->query("Select id,id_barang_bb from tb_stok_unit_packing where id_unit_packing='$id_unit_packing'");
     
     if($sql->num_rows() > 0){
		 $query= $sql->result();
			 foreach($query as $row){
				  
				$sql2=$this->db->query("Select * from tb_master_barang_bb where id='$row->id_barang_bb'");
				 if($sql2->num_rows() > 0){
					 $hasil2 =$sql2->row();
					 $kode_barang_bb = $hasil2->kode_barang_bb;
					 $nama_barang_bb = $hasil2->nama_barang_bb;
					 }
					 $total_masuk_bagus_unit_packing=0;
					 $total_masuk_lain_unit_packing=0;
					 $total_keluar_bagus_unit_packing=0;
					 $total_keluar_lain_unit_packing=0;
					 $total_stok_opname_packing=0;
					 $total_saldo_akhir=0;
					 $total_selisih=0;
				 for($x=0; $x <count($query);$x++){
				$sql3=$this->db->query("Select sum(qty) as jum_masuk_unit_packing from tb_makloon_pengadaan_baju_wip a 
				INNER JOIN  tb_makloon_pengadaan_baju_wip_detail b ON a.id=b.id_makloon_pengadaan_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_packing='$id_unit_packing'");
					if($sql3->num_rows() > 0){
						 $hasil3 =$sql3->row();
							$jum_masuk_bagus_unit_packing = $hasil3->jum_masuk_unit_packing;
						}
						else{
							$jum_masuk_bagus_unit_packing = 0;
							}
				
				$sql6=$this->db->query("Select sum(qty) as jum_masuk_unit_packing from tb_makloon_pengadaan_baju_wip a 
				INNER JOIN  tb_makloon_pengadaan_baju_wip_detail b ON a.id=b.id_makloon_pengadaan_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_packing='$id_unit_packing'");
					if($sql6->num_rows() > 0){
						 $hasil6 =$sql6->row();
							$jum_masuk_lain_unit_packing = $hasil3->jum_masuk_unit_packing;
						}
						else{
							$jum_masuk_lain_unit_packing=0;
							}
				
				$sql4=$this->db->query("Select sum(qty) as jum_keluar_unit_packing from tb_makloon_unit_baju_wip a 
				INNER JOIN  tb_makloon_unit_baju_wip_detail b ON a.id=b.id_makloon_unit_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_packing='$id_unit_packing'");
					if($sql4->num_rows() > 0){
						 $hasil4 =$sql4->row();
							$jum_keluar_bagus_unit_packing = $hasil4->jum_keluar_unit_packing;
						}
				
				$sql7=$this->db->query("Select sum(qty) as jum_keluar_unit_packing from tb_makloon_unit_baju_wip a 
				INNER JOIN  tb_makloon_unit_baju_wip_detail b ON a.id=b.id_makloon_unit_baju_wip where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_packing='$id_unit_packing'");
					if($sql7->num_rows() > 0){
						 $hasil7 =$sql7->row();
							$jum_keluar_lain_unit_packing = $hasil7->jum_keluar_unit_packing;
						}
						else{
							$jum_keluar_lain_unit_packing =0;
							}
						
				$sql5=$this->db->query("Select v_stok_opname from tb_stok_opname_unit_packing a 
				INNER JOIN  tb_stok_opname_unit_packing_detail b ON a.id=b.id_stok_opname_unit_packing where id_barang_bb='$row->id_barang_bb'
				AND a.id_unit_packing='$id_unit_packing'");
					if($sql5->num_rows() > 0){
						 $hasil5 =$sql5->row();
							$jum_stok_opname_packing = $hasil5->v_stok_opname;
						
					}
					else{
						$jum_stok_opname_packing = 0;
						
					}
					
					$saldo_akhir = $jum_masuk_bagus_unit_packing + $jum_masuk_lain_unit_packing - $jum_keluar_bagus_unit_packing - $jum_keluar_lain_unit_packing ;
					$selisih =	$jum_stok_opname_packing - $saldo_akhir;
						$data_detail[]=array('id_barang_bb'=>$row->id_barang_bb,
						'kode_barang_bb'=>$kode_barang_bb,
						'nama_barang_bb'=>$nama_barang_bb,
						'jum_masuk_bagus_unit_packing'=>$jum_masuk_bagus_unit_packing,
						'jum_keluar_bagus_unit_packing'=>$jum_keluar_bagus_unit_packing,
						'jum_masuk_lain_unit_packing'=>$jum_masuk_lain_unit_packing,
						'jum_keluar_lain_unit_packing'=>$jum_keluar_lain_unit_packing,
						'jum_stok_opname_packing'=>$jum_stok_opname_packing,
						'saldo_akhir'=>	$saldo_akhir,
						'selisih'=>$selisih
						);
						
						$total_masuk_bagus_unit_packing+=$jum_masuk_bagus_unit_packing;
						$total_masuk_lain_unit_packing+=$jum_masuk_lain_unit_packing;
						$total_keluar_bagus_unit_packing+=$jum_keluar_bagus_unit_packing;
						$total_keluar_lain_unit_packing+=$jum_keluar_lain_unit_packing;
						$total_stok_opname_packing+=$jum_stok_opname_packing;
						$total_saldo_akhir+=$saldo_akhir;
						$total_selisih+=$selisih;
					}
				}	
			 
			
			$sql6=$this->db->query("Select nama_unit_packing from tb_master_unit_packing where id='$id_unit_packing'");
				if($sql6->num_rows() > 0){
						 $hasil6 =$sql6->row();
							$nama_unit_packing = $hasil6->nama_unit_packing;
						}
						
				$data[]=array('nama_unit_packing'=>$nama_unit_packing,
						'id_unit_packing'=>$id_unit_packing,
						'tanggal_sj_dari'=>$tanggal_sj_dari,
						'tanggal_sj_ke'=>$tanggal_sj_ke,
						'total_masuk_bagus_unit_packing'=>$total_masuk_bagus_unit_packing,
						'total_masuk_lain_unit_packing'=>$total_masuk_lain_unit_packing,
						'total_keluar_bagus_unit_packing'=>$total_keluar_bagus_unit_packing,
						'total_keluar_lain_unit_packing'=>$total_keluar_lain_unit_packing,
						'total_stok_opname_packing'=>$total_stok_opname_packing,
						'total_saldo_akhir'=>$total_saldo_akhir,
						'total_selisih'=>$total_selisih,
						'data_detail'=>$data_detail
						);
			}	
			else
			{
				$data[]=null;
				}	
		}
		
		
		            return $data; 
	}
}
