<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_jenis_voucher ORDER BY e_jenis_voucher ASC  ", false);
	}

	function viewperpages($limit,$offset) {	
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_jenis_voucher ORDER BY e_jenis_voucher DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($ejenisvoucher) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qijenisvoucher = $db2->query(" SELECT i_jenis+1 AS ijenis FROM tr_jenis_voucher ORDER BY i_jenis DESC LIMIT 1 ");
		$row_ijenisvoucher = $qijenisvoucher->row();
		
		$str = array(
			'i_jenis'=>$row_ijenisvoucher->ijenis,
			'e_jenis_voucher'=>$ejenisvoucher,
			'd_entry'=>$dentry
		);
		
		$db2->insert('tr_jenis_voucher',$str);
		redirect('jenisvoucher/cform/');
    }
	
	function mupdate($ijenis,$ejenisvoucher) {
		$db2=$this->load->database('db_external', TRUE);
		$class_item	= array(
			'e_jenis_voucher'=>$ejenisvoucher
		);
		$db2->update('tr_jenis_voucher',$class_item,array('i_jenis'=>$ijenis));
		redirect('jenisvoucher/cform');
	}
	
	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_jenis_voucher WHERE i_jenis='$id' ");
	}
	
	function viewcari($ejenisvoucher) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_jenis_voucher WHERE e_jenis_voucher LIKE '$ejenisvoucher%' ");
	}
	
	function mcari($ejenisvoucher,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_jenis_voucher WHERE e_jenis_voucher LIKE '$ejenisvoucher%' LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function delete($id) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_jenis_voucher',array('i_jenis'=>$id));
		redirect('jenisvoucher/cform/');	 
	}
}

?>
