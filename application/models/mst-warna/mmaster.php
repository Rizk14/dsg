<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_warna');
    $this->db->order_by('nama','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_warna',array('kode'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_warna WHERE id = '$id' ");
    return $query->result();		  
  }
  
  //
  function save($id,$nama, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_warna ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
		
    $data = array(
      'id'=>$idbaru,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_warna',$data); }
	else {
		$data = array(
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id);
		$this->db->update('tm_warna',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_warna', array('id' => $id));
  }
  
  function cek_data($nama){
    $this->db->select("* FROM tm_warna WHERE UPPER(nama) = UPPER('$nama') ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
