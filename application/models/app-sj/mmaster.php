<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
   function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE a.jenis = '2' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
 function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  } 
  
   function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
   
  function getAllsjmasuk($num, $offset, $cari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_masuk) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";

		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit_jahit = '$id_unit_jahit' ";
		if ($id_unit_packing != "0")
			$pencarian.= " AND a.id_unit_packing = '$id_unit_packing' ";
		if ($jenis_masuk != "all")
			$pencarian.= " AND a.jenis_masuk = '$jenis_masuk' ";

		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$this->db->select(" distinct a.*, d.periode FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							LEFT JOIN tm_closeperiode_unitjahit d ON to_char(a.tgl_sj,'yyyymm') = d.periode 
							WHERE TRUE AND a.status_app_qc='t'".$pencarian, false)->limit($num,$offset);

		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%$caribrg%') OR UPPER(a.nama_brg_wip) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_sjmasukwip_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id 
								WHERE a.id_sjmasukwip = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
												
						// 30-01-2014, detail qty warna -------------
						$strwarna = "";
						$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjmasukwip_detail_warna a
									INNER JOIN tm_warna c ON a.id_warna = c.id
									WHERE a.id_sjmasukwip_detail = '$row2->id' ORDER BY c.nama ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						}
						//if ($strwarna == "")
						//	$strwarna = $row2->ket_qty_warna;
						//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->id_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$kode_unit_jahit	= $hasilrow->kode_unit;
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else {
						$kode_unit_jahit = "";
						$nama_unit_jahit = "";
					}
				}
				else {
					$kode_unit_jahit = "";
					$nama_unit_jahit = "";
				}
				
				// unit packing
				if ($row1->id_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$kode_unit_packing	= $hasilrow->kode_unit;
						$nama_unit_packing	= $hasilrow->nama;
					}
					else {
						$kode_unit_packing = "";
						$nama_unit_packing = "";
					}
				}
				else {
					$kode_unit_packing = "";
					$nama_unit_packing = "";
				}
				
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Masuk bagus hasil jahit";
				else if ($row1->jenis_masuk == '2')
					$nama_jenis = "Lain-lain (Perbaikan dari unit jahit)";
				else if ($row1->jenis_masuk == '3')
					$nama_jenis = "Lain-lain (Retur dari unit packing)";
				else if ($row1->jenis_masuk == '4')
					$nama_jenis = "Lain-lain (Retur dari gudang jadi)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
					
					
					$query41 = $this->db->query("SELECT * FROM tm_periode_wip order by id desc limit 1");
					if($query41->num_rows() > 0){
					$hasilrow41 = $query41->row();
					$tahun_periode = $hasilrow41->i_year;
					$bulan_periode = $hasilrow41->i_month;
					$tanggal_periode = $hasilrow41->i_periode;
					}
					else{
						$tahun_periode = '';
										$bulan_periode = '';
										$tanggal_periode = '';
						}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'tahun_periode'=> $tahun_periode,
											'bulan_periode'=> $bulan_periode,
											'tanggal_periode'=>$tanggal_periode,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_masuk'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'id_unit_packing'=> $row1->id_unit_packing,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'keterangan'=> $row1->keterangan,
											'status_btb'=> $row1->status_sjmasukwip,
											'detail_sj'=> $detail_sj,
											'periode'=> $row1->periode,
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function getAllsjmasuktanpalimit($cari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_masuk){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit_jahit = '$id_unit_jahit' ";
		if ($id_unit_packing != "0")
			$pencarian.= " AND a.id_unit_packing = '$id_unit_packing' ";
		if ($jenis_masuk != "all")
			$pencarian.= " AND a.jenis_masuk = '$jenis_masuk' ";

		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
						INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
						WHERE TRUE AND a.status_app_qc='t'".$pencarian);

    return $query->result();  
  }
  
  function getAllsjkeluartanpalimit($cari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_keluar){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit_jahit = '$id_unit_jahit' ";
		if ($id_unit_packing != "0")
			$pencarian.= " AND a.id_unit_packing = '$id_unit_packing' ";
		if ($jenis_keluar != "all")
			$pencarian.= " AND a.jenis_keluar = '$jenis_keluar' ";

		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
						INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
						WHERE TRUE AND a.status_app_qc='t'".$pencarian);

    return $query->result();  
  }
  
  
  
    function get_sjmasukwiptanpalimit($id_ujh, $keywordcari, $tgldari, $tglke ,$js_msk,$id_upk){
		
		$pencarian1='';
		$pencarian2='';
		$pencarian3='';
		$pencarian11='';
		$pencarian14='';
		if ($tgldari != '0000-00-00'){
		
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		
		if ($tglke != '0000-00-00'){	
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}
		if($js_msk !=0){
		$pencarian3.=	"AND jenis_masuk='$js_msk'";
			}
	
	
	if($id_ujh != 0){
		$pencarian11.= "AND id_unit_jahit = '$id_ujh'";
		}
	if($id_upk != 0){
		$pencarian14.= "AND id_unit_packing = '$id_upk'";
		}	
		
		
	if ($keywordcari == "all") {
				$query	= $this->db->query(" SELECT * FROM tm_sjmasukwip WHERE status_app_qc = 'f' AND status_aktif = 't' AND tgl_sj >= '2016-11-01'
											 ".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3."".$pencarian14);
	}
	else {
				$query	= $this->db->query(" SELECT * FROM tm_sjmasukwip WHERE status_app_qc = 'f' AND status_aktif = 't'  AND tgl_sj  >= '2016-11-01'
											".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3."".$pencarian14.
							  "AND UPPER(no_sj) like UPPER('%$keywordcari%') ");
	}
    
    return $query->result();  
  }
  
  function get_sjkeluarwiptanpalimit($id_ujh, $keywordcari, $tgldari, $tglke ,$js_kel,$id_upk){
		
		$pencarian1='';
		$pencarian2='';
		$pencarian3='';
		$pencarian11='';
		$pencarian14='';
		if ($tgldari != '0000-00-00'){
		
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		
		if ($tglke != '0000-00-00'){	
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}
		if($js_kel !=0){
		$pencarian3.=	"AND jenis_keluar='$js_kel'";
			}
	
	
	if($id_ujh != 0){
		$pencarian11.= "AND id_unit_jahit = '$id_ujh'";
		}
	if($id_upk != 0){
		$pencarian14.= "AND id_unit_packing = '$id_upk'";
		}	
		
		
	if ($keywordcari == "all") {
				$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip WHERE status_app_qc = 'f' AND status_aktif = 't'  AND tgl_sj >= '2016-11-01'
											 ".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3."".$pencarian14);
	}
	else {
				$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip WHERE status_app_qc = 'f' AND status_aktif = 't'  AND tgl_sj >= '2016-11-01'
											".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3."".$pencarian14.
							  "AND UPPER(no_sj) like UPPER('%$keywordcari%') ");
	}
    
    return $query->result();  
  }
  
  function get_sjmasukwip($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk,$id_upk){
		$pencarian1='';
		$pencarian2='';
		$pencarian3='';
		$pencarian11='';
		if ($tgldari!= '0000-00-00'){
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		if ($tglke!= '0000-00-00'){
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}	
		
		if($js_msk !=0){
		$pencarian3.=	"AND jenis_masuk='$js_msk'";
			}
	
	if($id_ujh != 0){
		$pencarian11.= "AND a.id_unit_jahit = '$id_ujh'";
		}
		
	if ($keywordcari == "all") {
				$sql = " distinct a.*, d.periode 
						FROM tm_sjmasukwip a 
						INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
						LEFT JOIN tm_closeperiode_unitjahit d ON to_char(a.tgl_sj,'yyyymm') = d.periode  
						WHERE a.status_app_qc = 'f' 
						AND a.status_aktif = 't' AND b.status_app_qc = 'f' 
						AND tgl_sj >= '2016-11-01' 
						".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3." 
						AND b.status_app_qc='f' 
						and d.periode is null 
						order by a.tgl_sj DESC, a.no_sj DESC ";

						//".$pencarian3." 
			$this->db->select($sql, false);
			$query = $this->db->get();
		
	}
	else {
				$sql = " distinct a.* FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_barang_wip c ON b.id_brg_wip = c.id
							LEFT JOIN tm_closeperiode_unitjahit d ON to_char(a.tgl_sj,'yyyymm') = d.periode
							WHERE a.status_app_qc = 'f' AND a.status_aktif = 't'  AND tgl_sj >= '2016-11-01'
							  AND b.status_app_qc='f'
							   and d.periode is null
								".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3."
							AND UPPER(no_sj) like UPPER('%$keywordcari%') 
							order by a.tgl_sj DESC, a.no_sj DESC ";

			$this->db->select($sql, false);
			$query = $this->db->get();
	
	}
	
		$data_sjmasukwip = array();
		$detail_sjmasukwip = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
					$filterbrg = "";
					if ($keywordcari != "all")
						$filterbrg = " AND UPPER(no_sj) like UPPER('%$keywordcari%') ";
					
					$sql2 = "SELECT b.* FROM tm_sjmasukwip_detail b INNER JOIN tm_sjmasukwip a ON b.id_sjmasukwip = a.id 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							WHERE b.id_sjmasukwip = '$row1->id' AND b.status_app_qc = 'f' ".$filterbrg." ORDER BY b.id ASC ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip 
										WHERE id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
											
							$query4	= $this->db->query(" SELECT id FROM tm_pembelian_wip_detail WHERE id_sjmasukwip_detail = '".$row2->id."' ");
							if ($query4->num_rows() > 0){
						
									$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_wip_detail a 
											INNER JOIN tm_pembelian_wip b ON a.id_pembelian_wip = b.id WHERE b.status_aktif = 't' ";
									$sql3.= " AND a.id_sjmasukwip_detail = '$row2->id' AND a.id_brg_wip = '$row2->id_brg_wip'
												 ";
									
									$query3	= $this->db->query($sql3);
									$hasilrow = $query3->row();
									$jum_sjmasukwip = $hasilrow->jum; 
			
							}
							else {
								$jum_sjmasukwip = 0;
							}
						
						$qty = $row2->qty-$jum_sjmasukwip;
						
							$nama_satuan_lain = '';
							
						$query5	= $this->db->query(" SELECT id,id_warna,qty FROM tm_sjmasukwip_detail_warna 
										WHERE id_sjmasukwip_detail = '$row2->id' ");
						$hasil5 = $query5->result();
						foreach($hasil5 as $row5){
							$query6	= $this->db->query(" SELECT nama FROM tm_warna 
										WHERE id = '$row5->id_warna' ");
						$hasilrow6 = $query6->row();
						$nama_warna	= $hasilrow6->nama;
						
						$detail_warna[] = array(	'iddetail_warna '=> $row5->id,
												'id_warna'=> $row5->id_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $row5->qty,
											);
							}
						
						if ($qty > 0) {
							$detail_sjmasukwip[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan,
												'detail_warna'=>$detail_warna
											);
											$detail_warna = array();
						}
					}
				}
				else {
					$detail_sjmasukwip = '';
				}
				
					if ($row1->id_unit_jahit != '0') {
						$query3	= $this->db->query(" SELECT kode_unit, nama from tm_unit_jahit where id = '$row1->id_unit_jahit' ");
						$hasilrow = $query3->row();
						$kode_unit_jahit	= $hasilrow->kode_unit;
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else{
						$kode_unit_jahit	= '';
						$nama_unit_jahit	= '';
						}
					
					if ($row1->id_unit_packing != '0') {
						$query3	= $this->db->query(" SELECT kode_unit, nama from tm_unit_packing where id = '$row1->id_unit_packing' ");
						$hasilrow = $query3->row();
						$kode_unit_packing	= $hasilrow->kode_unit;
						$nama_unit_packing	= $hasilrow->nama;
					}
					else{
						$kode_unit_packing	= '';
						$nama_unit_packing	= '';
						}
				
					//$no_sjmasukwip = $row1->no_sjmasukwip;
					$no_sj = $row1->no_sj;
					//$tgl_sjmasukwip = $row1->tgl_sjmasukwip;
					$tgl_sj = $row1->tgl_sj;
				
				$data_sjmasukwip[] = array(	'id'=> $row1->id,	
											'no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj,
											'jenismasuk'=> $row1->jenis_masuk,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'tgl_update'=> $row1->tgl_update,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'detail_sjmasukwip'=> $detail_sjmasukwip
											);
				$detail_sjmasukwip = array();
			} // endforeach header
		}
		else {
			$data_sjmasukwip = '';
		}
		return $data_sjmasukwip;
  }
  
  
   function get_sjkeluarwip($id_ujh, $keywordcari, $tgldari, $tglke,$js_kel,$id_upk){
		$pencarian1='';
		$pencarian2='';
		$pencarian3='';
		$pencarian11='';
		$pencarian14='';
		if ($tgldari!= '0000-00-00'){
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		if ($tglke!= '0000-00-00'){
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}	
		
		if($js_kel !=0){
		$pencarian3.=	"AND jenis_keluar='$js_kel'";
			}
	
	if($id_ujh != 0){
		$pencarian11.= "AND a.id_unit_jahit = '$id_ujh'";
		}
		if($id_upk != 0){
		$pencarian14.= "AND id_unit_packing = '$id_upk'";
		}	
		
	if ($keywordcari == "all") {
				$sql = " distinct a.*, d.periode FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				LEFT JOIN tm_closeperiode_unitjahit d ON to_char(a.tgl_sj,'yyyymm') = d.periode
						WHERE a.status_app_qc = 'f' AND a.status_aktif = 't' AND b.status_app_qc = 'f' AND tgl_sj >= '2016-11-01'
						 ".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3." ".$pencarian14.
					"AND b.status_app_qc='f'  and d.periode is null order by a.tgl_sj DESC, a.no_sj DESC ";

			$this->db->select($sql, false);
			$query = $this->db->get();
		
	}
	else {
				$sql = " distinct a.*, d.periode FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				LEFT JOIN tm_closeperiode_unitjahit d ON to_char(a.tgl_sj,'yyyymm') = d.periode
						WHERE a.status_app_qc = 'f' AND a.status_aktif = 't' AND b.status_app_qc = 'f' AND tgl_sj >= '2016-11-01'
						 ".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3." ".$pencarian14.
					"AND b.status_app_qc='f'  and d.periode is null
							AND UPPER(no_sj) like UPPER('%$keywordcari%') 
							order by a.tgl_sj DESC, a.no_sj DESC ";
			$this->db->select($sql, false);
			$query = $this->db->get();
	
	}
	
		$data_sjkeluarwip = array();
		$detail_sjkeluarwip = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
					$filterbrg = "";
					if ($keywordcari != "all")
						$filterbrg = " AND UPPER(no_sj) like UPPER('%$keywordcari%') ";
					
					$sql2 = "SELECT b.* FROM tm_sjkeluarwip_detail b INNER JOIN tm_sjkeluarwip a ON b.id_sjkeluarwip = a.id 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							WHERE b.id_sjkeluarwip = '$row1->id' AND b.status_app_qc = 'f' ".$filterbrg." ORDER BY b.id ASC ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip 
										WHERE id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						
							
							
						$query5	= $this->db->query(" SELECT id,id_warna,qty FROM tm_sjkeluarwip_detail_warna 
										WHERE id_sjkeluarwip_detail = '$row2->id' ");
						$hasil5 = $query5->result();
						foreach($hasil5 as $row5){
							$query6	= $this->db->query(" SELECT nama FROM tm_warna 
										WHERE id = '$row5->id_warna' ");
						$hasilrow6 = $query6->row();
						$nama_warna	= $hasilrow6->nama;
						
						$detail_warna[] = array(	'iddetail_warna '=> $row5->id,
												'id_warna'=> $row5->id_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $row5->qty,
											);
							}
							$nama_satuan_lain = '';
						
							$detail_sjkeluarwip[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'detail_warna'=>$detail_warna
											);
											$detail_warna = array();
						
					}
				}
				else {
					$detail_sjkeluarwip = '';
				}
				
				
					if ($row1->id_unit_jahit != '0') {
						$query3	= $this->db->query(" SELECT kode_unit, nama from tm_unit_jahit where id = '$row1->id_unit_jahit' ");
						$hasilrow = $query3->row();
						$kode_unit_jahit	= $hasilrow->kode_unit;
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else{
						$kode_unit_jahit	= '';
						$nama_unit_jahit	= '';
						}
					
					if ($row1->id_unit_packing != '0') {
						$query3	= $this->db->query(" SELECT kode_unit, nama from tm_unit_packing where id = '$row1->id_unit_packing' ");
						$hasilrow = $query3->row();
						$kode_unit_packing	= $hasilrow->kode_unit;
						$nama_unit_packing	= $hasilrow->nama;
					}
					else{
						$kode_unit_packing	= '';
						$nama_unit_packing	= '';
						}
				
					//$no_sjkeluarwip = $row1->no_sjkeluarwip;
					$no_sj = $row1->no_sj;
					//$tgl_sjkeluarwip = $row1->tgl_sjkeluarwip;
					$tgl_sj = $row1->tgl_sj;
				
				$data_sjkeluarwip[] = array(	'id'=> $row1->id,	
											'no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj,
											'jeniskeluar'=> $row1->jenis_keluar,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'tgl_update'=> $row1->tgl_update,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'detail_sjkeluarwip'=> $detail_sjkeluarwip
											);
				$detail_sjkeluarwip = array();
			} // endforeach header
		}
		else {
			$data_sjkeluarwip = '';
		}
		return $data_sjkeluarwip;
  }
  
  function getAllsjkeluar($num,$offset,$cari, $date_from, $date_to, $gudang, $id_unit_jahit, $id_unit_packing, $caribrg, $filterbrg, $jenis_keluar){
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";

		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit_jahit = '$id_unit_jahit' ";
		if ($id_unit_packing != "0")
			$pencarian.= " AND a.id_unit_packing = '$id_unit_packing' ";
		if ($jenis_keluar != "all")
			$pencarian.= " AND a.jenis_keluar = '$jenis_keluar' ";

		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$this->db->select(" distinct a.*, d.periode FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							LEFT JOIN tm_closeperiode_unitjahit d ON to_char(a.tgl_sj,'yyyymm') = d.periode  
							WHERE TRUE AND a.status_app_qc='t'".$pencarian, false)->limit($num,$offset);

		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%$caribrg%') OR UPPER(a.nama_brg_wip) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_sjkeluarwip_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id 
								WHERE a.id_sjkeluarwip = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
												
						// 30-01-2014, detail qty warna -------------
						$strwarna = "";
						$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjkeluarwip_detail_warna a
									INNER JOIN tm_warna c ON a.id_warna = c.id
									WHERE a.id_sjkeluarwip_detail = '$row2->id' ORDER BY c.nama ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						}
						//if ($strwarna == "")
						//	$strwarna = $row2->ket_qty_warna;
						//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->id_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$kode_unit_jahit	= $hasilrow->kode_unit;
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else {
						$kode_unit_jahit = "";
						$nama_unit_jahit = "";
					}
				}
				else {
					$kode_unit_jahit = "";
					$nama_unit_jahit = "";
				}
				
				// unit packing
				if ($row1->id_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$kode_unit_packing	= $hasilrow->kode_unit;
						$nama_unit_packing	= $hasilrow->nama;
					}
					else {
						$kode_unit_packing = "";
						$nama_unit_packing = "";
					}
				}
				else {
					$kode_unit_packing = "";
					$nama_unit_packing = "";
				}
				
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "keluar bagus hasil jahit";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Lain-lain (Perbaikan dari unit jahit)";
				else if ($row1->jenis_keluar == '3')
					$nama_jenis = "Lain-lain (Retur dari unit packing)";
				else if ($row1->jenis_keluar == '4')
					$nama_jenis = "Lain-lain (Retur dari gudang jadi)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
					
					
					$query41 = $this->db->query("SELECT * FROM tm_periode_wip order by id desc limit 1");
					if($query41->num_rows() > 0){
					$hasilrow41 = $query41->row();
					$tahun_periode = $hasilrow41->i_year;
					$bulan_periode = $hasilrow41->i_month;
					$tanggal_periode = $hasilrow41->i_periode;
					}
					else{
						$tahun_periode = '';
										$bulan_periode = '';
										$tanggal_periode = '';
						}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'tahun_periode'=> $tahun_periode,
											'bulan_periode'=> $bulan_periode,
											'tanggal_periode'=>$tanggal_periode,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'id_unit_packing'=> $row1->id_unit_packing,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'keterangan'=> $row1->keterangan,
											'status_btb'=> $row1->status_sjkeluarwip,
											'detail_sj'=> $detail_sj,
											'periode'=> $row1->periode,
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
 
  function save($id_sjmasukwip,$id_sjmasukwip_detail){  
    
    $tgl = date("Y-m-d H:i:s");
    if($id_sjmasukwip_detail !=''){
		$this->db->query("UPDATE tm_sjmasukwip_detail SET status_app_qc = 't' where id= $id_sjmasukwip_detail ");
	
	$this->db->select("id from tm_sjmasukwip_detail WHERE status_app_qc = 'f' AND id_sjmasukwip = '$id_sjmasukwip' ", false);
	$query = $this->db->get();
					//~ 
	if ($query->num_rows() == 0){
	$this->db->query(" UPDATE tm_sjmasukwip SET status_app_qc = 't' where id= '$id_sjmasukwip' ");
	}
	$th_now	= date("Y");

}
  }
  
  function savesjkeluar($id_sjkeluarwip,$id_sjkeluarwip_detail){  
    
    $tgl = date("Y-m-d H:i:s");
    if($id_sjkeluarwip_detail !=''){
		$this->db->query("UPDATE tm_sjkeluarwip_detail SET status_app_qc = 't' where id= $id_sjkeluarwip_detail ");
	
	$this->db->select("id from tm_sjkeluarwip_detail WHERE status_app_qc = 'f' AND id_sjkeluarwip = '$id_sjkeluarwip' ", false);
	$query = $this->db->get();
					//~ 
	if ($query->num_rows() == 0){
	$this->db->query(" UPDATE tm_sjkeluarwip SET status_app_qc = 't' where id= '$id_sjkeluarwip' ");
	}
	$th_now	= date("Y");

}
  }

  function getAlltanpalimit($cunit_jahit, $cari, $date_from, $date_to, $caribrg, $filterbrg){

    $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelian) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_jahit != '0')
		$pencarian.= " AND a.id_unit_jahit = '$cunit_jahit' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelian DESC, a.id DESC ";
	
	$query = $this->db->query(" SELECT distinct a.* FROM tm_pembelian_wip a LEFT JOIN tm_pembelian_wip_detail b ON a.id=b.id_pembelian_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian);
	      
    return $query->result();  
  }
  function getAll($num, $offset, $cunit_jahit, $cari, $date_from, $date_to, $caribrg, $filterbrg) {	
	
	
	 $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelian) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_jahit != '0')
		$pencarian.= " AND a.id_unit_jahit = '$cunit_jahit' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelian DESC, a.id DESC ";
	
	$this->db->select(" distinct a.* FROM tm_pembelian_wip a LEFT JOIN tm_pembelian_wip_detail b ON a.id=b.id_pembelian_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	  
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				//print_r($row1);
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
								OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_wip_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id
							WHERE a.id_pembelian_wip = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						//----------------

					  
				
					  $id_detailnya = "";
					  
						//----------------
						
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							
						}
						else {
							$kode_brg = '';
						
						}
						

						$detail_fb[] = array(	'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg,
												'nama_brg_wip'=> $row2->nama_brg,
											
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$id_detailnya = "";
					$detail_fb = '';
				}
		
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$kode_unit_jahit	= $hasilrow->kode_unit;
				$nama_unit_jahit	= $hasilrow->nama;
		
				$no_sjmasukwipnya = "";
				
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj FROM tm_pembelian_wip_detail a INNER JOIN tm_sjmasukwip_detail b ON a.id_sjmasukwip_detail = b.id 
						INNER JOIN tm_sjmasukwip c ON c.id = b.id_sjmasukwip
						WHERE a.id_pembelian_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$no_sjmasukwipnya.= $rowxx->no_sj."<br>";
					}// end for
				}
				else {
					$no_sjmasukwipnya = '';
				
				}
				/*
				$sqlxx = " SELECT status_stok FROM tm_apply_stok_pembelian WHERE no_sj = '$row1->no_sj' 
							AND id_supplier = '$row1->id_supplier' AND status_aktif = 't' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$status_stok= $hasilxx->status_stok;
					if ($status_stok == 't')
						$cetakbtb = '1';
					else
						$cetakbtb = '0';
				}
				else
					$cetakbtb = '0';
					*/
						
				$data_fb[] = array(			'id'=> $row1->id,	
											
											'no_sjmasukwip'=> $no_sjmasukwipnya,	
											'jenismasuk' =>$row1->jenismasuk,
											'no_sjmasukpembelian'=> $row1->no_sjmasukpembelian,
											'tgl_sjpembelian'=> $row1->tgl_sjpembelian,
											'total'=> $row1->total,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'status_stok'=> $row1->status_stok,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											//'cetakbtb'=> $cetakbtb
											//'ambil_pp'=> $ambil_pp
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function delete($id){    
	  $tgl = date("Y-m-d H:i:s");
	  
					$sqlxx2= " SELECT distinct a.id as id_header ,b.id as id_detail FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
							 WHERE b.id = '$id' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_sjmasukwip	= $hasilxx2->id_header;
						$id_sjmasukwip_detail	= $hasilxx2->id_detail;
						$this->db->query(" UPDATE tm_sjmasukwip SET status_app_qc = 'f' where id= '$id_sjmasukwip' ");
					}
					// reset status di detailnya dari sjmasukwip pake perulangan
					$this->db->query(" UPDATE tm_sjmasukwip_detail SET status_app_qc = 'f' where id= '$id_sjmasukwip_detail' ");
				
  }
  function deletesjkeluar($id){    
	  $tgl = date("Y-m-d H:i:s");
	  
					$sqlxx2= " SELECT distinct a.id as id_header ,b.id as id_detail FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
							 WHERE b.id = '$id' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_sjkeluarwip	= $hasilxx2->id_header;
						$id_sjkeluarwip_detail	= $hasilxx2->id_detail;
						$this->db->query(" UPDATE tm_sjkeluarwip SET status_app_qc = 'f' where id= '$id_sjkeluarwip' ");
					}
					// reset status di detailnya dari sjkeluarwip pake perulangan
					$this->db->query(" UPDATE tm_sjkeluarwip_detail SET status_app_qc = 'f' where id= '$id_sjkeluarwip_detail' ");
				
  }
   function get_pembelian($id_pembelian) {
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_wip where id = '$id_pembelian' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			
				
				
				// 10-07-2015 DIMODIF
				$no_sjmasukwipnya = ""; 
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj,c.tgl_sj FROM tm_pembelian_wip_detail a INNER JOIN tm_sjmasukwip_detail b ON a.id_sjmasukwip_detail = b.id 
						INNER JOIN tm_sjmasukwip c ON c.id = b.id_sjmasukwip
						WHERE a.id_pembelian_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$pisah1 = explode("-", $rowxx->tgl_sj);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						$no_sjmasukwipnya.= $rowxx->no_sj." (".$tgl_sj.")"."<br>";
					}// end for
				}
				else {
					$no_sjmasukwipnya = '-';
				
				}
				
				
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_wip_detail WHERE id_pembelian_wip = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						if($query3->num_rows > 0 ){
						$hasilrow = $query3->row();
						$nama_satuan = $hasilrow->nama;
					}
					else{
						$nama_satuan = 'pieces';
						}
						
						
						
						$sum_qty = 0;
						$sum_beli = 0;
					  
					  if ($row2->satuan_lain != 0) {
						  $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
						  $hasilrow = $query3->row();
						  $nama_satuan_lain = $hasilrow->nama;
					  }
					  else
						$nama_satuan_lain = '';
					  
					    $query3	= $this->db->query(" SELECT d.kode as kode_kel_brg FROM tm_barang_wip a 
					    INNER JOIN tm_jenis_brg_wip c ON a.id_jenis_brg_wip = c.id 
						INNER JOIN tm_kel_brg_wip d ON a.id_kel_brg_wip=d.id
									WHERE a.id = '$row2->id_brg_wip' ");
						if($query3->num_rows > 0){			
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
					}
					else
						$kode_kel_brg = 0;
						
						$sqlop = " SELECT b.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE b.id = '$row2->id_sjmasukwip_detail' ";
						$queryop	= $this->db->query($sqlop);
						if($queryop->num_rows()>0) {
							$hasilop = $queryop->row();
							$qty_op_detail = $hasilop->qty;
						}
						else
							$qty_op_detail = 0;
						
						if ($row2->id_sjmasukwip_detail != '0') {
							$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelian_wip_detail a 
										INNER JOIN tm_pembelian_wip b ON b.id=a.id_pembelian_wip
										WHERE a.id_sjmasukwip_detail='$row2->id_sjmasukwip_detail' AND a.id_brg_wip='$row2->id_brg_wip' 
										AND b.status_aktif='t'
										AND b.tgl_sjpembelian <= '$row1->tgl_sjpembelian' ");
							
							if($query3->num_rows()>0) {
								$hasilrow4 = $query3->row();
								$pemenuhan = $hasilrow4->pemenuhan;
								$sisa = $qty_op_detail-$pemenuhan;
							}else{
								$pemenuhan = 0;
								$sisa = $qty_op_detail-$pemenuhan;
								
								if($pemenuhan=='')
									$pemenuhan = 0;
								
								if($sisa=='')
									$sisa = 0;
							}
						}
						else {
							$sisa = '-';
							$pemenuhan = 0;
						}
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_kel_brg'=> $kode_kel_brg,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $this->db->escape_str($nama_brg),
												'nama_satuan'=> $nama_satuan,
												'harga'=> $row2->harga,
												'qty'=> $row2->qty,
												'id_satuan'=> $row2->id_satuan,
												'id_satuan_konversi'=> $row2->id_satuan_konversi,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total,
												'qty_sjmasukwip'=> $sum_qty,
												'jum_beli'=> $sum_beli,
												'satuan_lain'=> $row2->satuan_lain,
												'nama_satuan_lain'=> $nama_satuan_lain,
												'qty_sat_lain'=> $row2->qty_satuan_lain,
												'id_sjmasukwip_detail'=> $row2->id_sjmasukwip_detail,
												
												
												'pemenuhan'=> $pemenuhan,
												'sisa'=> $sisa
											);
					}
				}
				else {
					$detail_fb = '';
				}


				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$kode_unit_jahit	= $hasilrow->kode_unit;
				$nama_unit_jahit	= $hasilrow->nama;
				$top	= '';
				
				$pisah1 = explode("-", $row1->tgl_sjpembelian);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
				
			
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_sjmasukwip'=> $no_sjmasukwipnya,
											//'no_bonm'=> $no_bonmnya,
											//'no_pp'=> $no_ppnya,	
											'no_sjmasukpembelian'=> $row1->no_sjmasukpembelian,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'total'=> $row1->total,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'pkp'=> $row1->pkp,
											'tipe_pajak'=> $row1->tipe_pajak,
											'dpp'=> $row1->dpp,
											'stok_masuk_lain_cash'=> $row1->stok_masuk_lain_cash,
											'stok_masuk_lain_kredit'=> $row1->stok_masuk_lain_kredit,
											'detail_fb'=> $detail_fb,
											//'list_id_gudang'=> $list_id_gudang,
										//	'admgudang_uid_update_by'=> $admgudang_uid_update_by
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }  
  

  }  
  
  

