<?php
  class Mmaster extends CI_Model{
    function __construct() { 

    parent::__construct();

  }
  
  function getAll(){
    $this->db->select('tm_bagian.* ,tm_gudang.nama as nama_gudang');
    $this->db->from('tm_bagian');
    $this->db->order_by('kode','ASC');
    $this->db->join('tm_gudang', 'tm_gudang.id = tm_bagian.id_gudang', 'left');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_bagian',array('kode'=>$id));
    $query	= $this->db->query(" SELECT a.*, b.id as id_gudang FROM tm_bagian a left join tm_gudang b ON a.id_gudang=b.id WHERE a.id = '$id' ");
    return $query->result();
  }
  
  function get_gudang(){
   
   	$query3	= $this->db->query(" SELECT * FROM tm_gudang ");
   	if($query3->num_rows() > 0){
      return $query3->result();
    }
  }
  
  function cek_data($kode){
    $this->db->select("kode from tm_bagian WHERE kode = '$kode'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  
  //
  function save($kode,$id_bagian,$nama, $goedit,$gudang){  
    $tgl = date("Y-m-d H:i:s");
    
    // ambil id tertinggi
	  $sql2 = " SELECT id FROM tm_bagian ORDER BY id DESC LIMIT 1 ";
	  $query2	= $this->db->query($sql2);
	  if ($query2->num_rows() > 0){
		  $hasil2 = $query2->row();
		  $idlama	= $hasil2->id;
		  $idbaru = $idlama+1;
	  }
	  else
		$idbaru = 1;
		
    $data = array(
      'id'=>$idbaru,
      'id_gudang'=>$gudang,
      'kode'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_bagian',$data); }
	else {
		$data = array(
		  'kode'=>$kode,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_bagian);
		$this->db->update('tm_bagian',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_bagian', array('id' => $id));
  }

}

