<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function nmcabang($idocode) {
		return $this->db->query(" SELECT b.e_branch_name AS e_branch_name FROM tr_branch b, tm_do a WHERE a.i_branch=b.i_branch_code AND  a.i_do_code='$idocode' AND a.f_do_cancel='f' ");
	}
	
	function jmlFaktur($ifakturcode) {
		return	$this->db->query(" SELECT * FROM tm_faktur_do_item_t a INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur WHERE b.i_faktur_code='$ifakturcode' AND b.f_faktur_cancel='f' ");
	}

	function clistpenjualanperdo($limit,$offset,$nofaktur,$ddo_first,$ddo_last,$iproduct) {
		
		/*
		$query	= $this->db->query( "
			SELECT 	a.i_do AS ido,
				c.i_product_motif AS imotif,
				c.e_product_motifname AS motifname,
				a.n_quantity AS qty,
				a.v_unit_price AS unitprice,
				(a.n_quantity * a.v_unit_price) AS amount
				
			FROM tm_fakturdo_item a
			
			LEFT JOIN tm_fakturdo b ON b.i_faktur=a.i_faktur
			LEFT JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			LEFT JOIN tr_product_base d ON d.i_product_base=c.i_product
			LEFT JOIN tm_do e ON e.i_do=a.i_do
			
			WHERE (c.i_product='$i_product' OR d.i_product_base='$i_product') AND (e.d_do BETWEEN '$d_do_first' AND '$d_do_last')		
		" );
		*/
		
		if(!empty($nofaktur) && ($ddo_first!='0' && $ddo_last!='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";	
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && ($ddo_first=='0' || $ddo_last=='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if($ddo_first!='0' && $ddo_last!='0') {
			$nfaktur	= "";
			$ddo		= " WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
		
		/*** if( strlen($nfaktur)!=0 || strlen($ddo)!=0 ) { ***/
		
		// if(strlen($ddo)!=0) { 
		
			/* Disabled 26042011
			$query	= $this->db->query( "
			
				SELECT 	b.i_faktur_code,
						e.d_do AS ddo,
						e.i_do_code AS idocode,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname,
						a.n_quantity AS qty,
						a.v_unit_price AS unitprice,
						sum((a.n_quantity * a.v_unit_price)) AS amount
					
				FROM tm_faktur_do_item a
					
				RIGHT JOIN tm_faktur_do b ON cast(b.i_faktur_code AS integer)=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo."
				
				GROUP BY b.i_faktur_code, e.i_do_code, c.i_product_motif, c.e_product_motifname, a.n_quantity, a.v_unit_price, e.d_do
				
				");
			*/	
			
			/*
					(a.v_unit_price / a.n_quantity) AS unitprice, 
					sum(a.v_unit_price) AS amount 
			*/
			
			$query	= $this->db->query(" SELECT  b.i_faktur_code, b.i_faktur, b.f_kontrabon, c.i_do_code AS idocode, c.d_do AS ddo, a.i_product AS imotif, 
					a.e_product_name AS motifname,
					sum(a.n_quantity) AS qty,
					a.v_unit_price AS unitprice,
					sum((a.n_quantity * a.v_unit_price)) AS amount
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				 
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur_code, b.i_faktur, b.f_kontrabon, c.i_do_code, c.d_do, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price 
				
				ORDER BY c.i_do_code ASC, a.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");
				
			/*
			$qstr	= " SELECT 	e.d_do AS ddo,
						e.i_do_code AS idocode,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname,
						a.n_quantity AS qty,
						d.v_unitprice AS unitprice,
						(a.n_quantity * d.v_unitprice) AS amount
				
				FROM tm_faktur_do_item a
				
				INNER JOIN tm_faktur_do b ON cast(b.i_faktur_code AS integer)=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo."
				
				GROUP BY e.i_do_code, c.i_product_motif, c.e_product_motifname, a.n_quantity, d.v_unitprice, e.d_do
				
				";
			*/
			
			// $query	= $this->db->query($qstr);
			
			if($query->num_rows()>0) {
				return $result	= $query->result();
			}			

	}
			
	function clistpenjualanperdoallpage($nofaktur,$ddo_first,$ddo_last,$iproduct) {
		
		if(!empty($nofaktur) && ($ddo_first!='0' && $ddo_last!='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";	
			$fbatal		= " AND b.f_faktur_cancel='f' ";		
		}else if(!empty($nofaktur) && ($ddo_first=='0' || $ddo_last=='0') ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else if($ddo_first!='0' && $ddo_last!='0'){
			$nfaktur	= "";
			$ddo		= " WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else{
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}

		return $this->db->query("
				SELECT  b.i_faktur_code, b.i_faktur, c.i_do_code AS idocode, c.d_do AS ddo, a.i_product AS imotif, 
					a.e_product_name AS motifname, 
					sum(a.n_quantity) AS qty, 
					a.v_unit_price AS unitprice,
					sum((a.n_quantity * a.v_unit_price)) AS amount
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				 
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur_code, b.i_faktur, c.i_do_code, c.d_do, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ORDER BY c.i_do_code ASC, a.i_product ASC ");			
	}

	function clistpenjualanperdoallpage_itembarang($ddo_first,$ddo_last,$iproduct) {

		return $this->db->query("
				SELECT  c.i_do_code AS idocode, c.d_do AS ddo, sum(a.n_quantity) AS jmldo, b.i_faktur_code AS ifakturcode, b.d_faktur AS dfaktur, sum(a.n_quantity) AS jmlfaktur
				
				FROM tm_faktur_do_item_t a 
							
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				
				WHERE a.i_product='$iproduct' AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') AND b.f_faktur_cancel='f' AND c.f_do_cancel='f'
							
				GROUP BY c.i_do_code, c.d_do, a.n_quantity, b.i_faktur_code, b.d_faktur ORDER BY b.i_faktur_code ASC, c.i_do_code ASC ");
	}

	function clistpenjualanperdo_itembarang($limit,$offset,$ddo_first,$ddo_last,$iproduct) {

		$query	= $this->db->query(" SELECT c.i_do_code AS idocode, c.d_do AS ddo, b.f_kontrabon, sum(a.n_quantity) AS jmldo, b.i_faktur_code AS ifakturcode, b.d_faktur AS dfaktur, sum(a.n_quantity) AS jmlfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				
				WHERE a.i_product='$iproduct' AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') AND b.f_faktur_cancel='f' AND c.f_do_cancel='f'
							
				GROUP BY c.i_do_code, c.d_do, b.f_kontrabon, a.n_quantity, b.i_faktur_code, b.d_faktur ORDER BY b.i_faktur_code ASC, c.i_do_code ASC 
				
				LIMIT ".$limit." OFFSET ".$offset." ");
							
			if($query->num_rows()>0) {
				return $result	= $query->result();
			}

	}
			
	function clistpenjualanperdototal($nofaktur,$ddo_first,$ddo_last,$iproduct) {
		
		if(!empty($nofaktur) && ($ddo_first!='0' && $ddo_last!='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";	
			$fbatal		= " AND b.f_faktur_cancel='f' ";		
		}else if(!empty($nofaktur) && ($ddo_first=='0' || $ddo_last=='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";	
			$fbatal		= " AND b.f_faktur_cancel='f' ";	
		}else if($ddo_first!='0' && $ddo_last!='0'){
			$nfaktur	= "";
			$ddo		= " WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else{
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
		/*echo " SELECT  sum(a.n_quantity) AS qty, sum((a.n_quantity * a.v_unit_price)) AS amount
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do ".$nfaktur." ".$ddo." ".$fbatal." "; */
				
		return $this->db->query(" SELECT  sum(a.n_quantity) AS qty, sum((a.n_quantity * a.v_unit_price)) AS amount
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do ".$nfaktur." ".$ddo." ".$fbatal." ");	

	}
	
	function lbarangjadi() {
	
		return $this->db->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				INNER JOIN tm_do_item d ON d.i_do=c.i_do
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product
				
				WHERE b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}	

	function lbarangjadiperpages($limit,$offset) {
			
		$query	= $this->db->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
									
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flbarangjadi($key) {
	
		return $this->db->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.i_faktur_code='$key' AND b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}
	
	/*** Disabled 28062011
	function ldo($ibranch) {
		return $this->db->query( "
		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_do_gross / b.n_deliver) AS price, 
				(b.n_residual * (b.v_do_gross/b.n_deliver)) AS nilai, 
				f.e_product_motifname AS motifname
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tr_product_motif f ON f.i_product_motif=b.i_product
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch
			
			WHERE g.e_initial='$ibranch' AND a.f_do_cancel=false AND b.f_faktur_created='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND b.n_residual > 0
			
			ORDER BY a.d_do DESC, a.i_do_code DESC ");
	}
	***/
	function ldo($ibranch) {
		return $this->db->query( "
		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_do_gross / b.n_deliver) AS price, 
				(b.n_residual * (b.v_do_gross/b.n_deliver)) AS nilai, 
				f.e_product_motifname AS motifname
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tr_product_motif f ON f.i_product_motif=b.i_product
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch
			
			WHERE g.e_initial='$ibranch' AND a.f_do_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND (a.f_faktur_created='f' OR a.f_faktur_created='t')
			
			ORDER BY a.d_do DESC, a.i_do_code DESC ");
	}
		
	function ldoperpages($ibranch,$limit,$offset) {
		$qstr	= "		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,

				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_do_gross / b.n_deliver) AS price, 
				(b.n_residual * (b.v_do_gross/b.n_deliver)) AS nilai, 
				f.e_product_motifname AS motifname
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tr_product_motif f ON f.i_product_motif=b.i_product

			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			

			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch
			

			WHERE g.e_initial='$ibranch' AND a.f_do_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND (a.f_faktur_created='f' OR a.f_faktur_created='t')
			
			ORDER BY a.d_do DESC, a.i_do_code DESC LIMIT ".$limit." OFFSET ".$offset." ";

		$query = $this->db->query($qstr);
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function fldo($key,$ibranch) {
		$ky_upper	= strtoupper($key);
		return $this->db->query( "
		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_do_gross / b.n_deliver) AS price, 
				(b.n_residual * (b.v_do_gross/b.n_deliver)) AS nilai, 
				f.e_product_motifname AS motifname
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tr_product_motif f ON f.i_product_motif=b.i_product
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch
			
			WHERE g.e_initial='$ibranch' AND (a.i_do_code='$ky_upper' OR b.i_product='$ky_upper' OR b.e_product_name LIKE '$key' OR f.e_product_motifname LIKE '$key') AND a.f_do_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND (a.f_faktur_created='f' OR a.f_faktur_created='t') 
			
			ORDER BY b.i_product ASC LIMIT 1000 ");
	}
	
	function getfakheader($ifaktur){
		return $this->db->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur='$ifaktur' AND f_faktur_cancel='f' ");
	}
	
	function geticustomer($ibranch){
		return $this->db->query(" SELECT * FROM tr_branch WHERE e_initial='$ibranch' ORDER BY i_branch DESC ");
	}	
	
	function lfakturitem($ifakturcode){
		$query	= $this->db->query(" SELECT * FROM tm_faktur_do_item_t WHERE i_faktur='$ifakturcode' ");
		if($query->num_rows()>0){
			return $result	= $query->result();
		}
	}
	
	function lfakturitem2($ifakturcode){
		$query	= $this->db->query("
				SELECT  b.i_faktur_code, c.i_do_code AS idocode, c.d_do AS ddo, a.i_product AS imotif, 
					a.e_product_name AS motifname, 
					a.n_quantity AS qty, 
					a.v_unit_price AS unitprice
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				 
				WHERE b.i_faktur_code='$ifakturcode' AND b.f_faktur_cancel='f' GROUP BY b.i_faktur_code, c.i_do_code, c.d_do, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ");	
		if($query->num_rows()>0){
			return $result	= $query->result();
		}			
	}
	
	function lcabang($icustomer) {
		if(!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.e_initial AS codebranch, a.i_branch_code AS ibranchcode, a.i_customer AS codecustomer, a.e_branch_name AS ebranchname, a.e_branch_name AS branch, a.e_initial AS einitial
				
				FROM tr_branch a 	
				INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$filter." ".$order;
										
		$query	= $this->db->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}	

	function mupdate($i_faktur,$nw_d_faktur,$i_branch,$v_total_nilai,$n_discount,$v_discount,$nw_d_due_date,$v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$v_total_fppn,$i_do,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$ifakturcode) {
		
		$i_faktur_item	= array();
		$tm_fakturdo_item	= array();
		$qget_tm_do	= array();
		
		$qty_akhir		= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$this->db->trans_begin();
		
		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qfakturitem	= $this->db->query(" SELECT * FROM tm_faktur_do_item_t WHERE i_faktur='$i_faktur' ");
		
		if($qfakturitem->num_rows()>0) {
			
			foreach($qfakturitem->result() as $row) {
				
				$qdoitem	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
				
				if($qdoitem->num_rows()>0) {
					
					$rdoitem	= $qdoitem->row();
					
					$ndeliver	= $rdoitem->n_deliver;
					$nresidual	= (($rdoitem->n_residual)+($row->n_quantity));

					$this->db->query(" UPDATE tm_do_item SET n_residual='$nresidual' WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
				}
				
			}
		}
		
		$query	= $this->db->query(" UPDATE tm_faktur_do_t SET i_faktur_code='$ifakturcode', d_faktur='$nw_d_faktur', d_due_date='$nw_d_due_date', d_pajak='$nw_d_pajak', n_discount='$n_discount', v_discount='$v_discount', v_total_faktur='$v_total_faktur', v_total_fppn='$v_total_fppn', f_printed='$f_cetak', d_update='$dentry' WHERE i_faktur='$i_faktur' ");
		if($query) {
			$this->db->query("DELETE FROM tm_faktur_do_item_t WHERE i_faktur='$i_faktur' ");
		}
		
		if(isset($iteration)) {
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				
				$seq_tm_fakturdo_item	= $this->db->query(" SELECT i_faktur_item FROM tm_faktur_do_item_t ORDER BY i_faktur_item DESC LIMIT 1 ");
				
				if($seq_tm_fakturdo_item->num_rows() > 0) {
					$seqrow	= $seq_tm_fakturdo_item->row();
					$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
				} else {
					$i_faktur_item[$jumlah]	= 1;
				}

				$qget_tm_do[$jumlah]	= $this->db->query(" SELECT * FROM tm_do WHERE i_do_code='$i_do[$jumlah]' AND f_do_cancel='f' ORDER BY i_do DESC LIMIT 1 ");
				
				if($qget_tm_do[$jumlah]->num_rows()>0) {
					$row_tm_do[$jumlah]	= $qget_tm_do[$jumlah]->row();
					$ido[$jumlah]	= $row_tm_do[$jumlah]->i_do;
				}
				
				$q_qty_do_item	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' ");
				
				if($q_qty_do_item->num_rows()>0) {
					
					$row_item_br	= $q_qty_do_item->row();
					
					if($n_quantity[$jumlah]==($row_item_br->n_residual)) {
						
						$qty_akhir[$jumlah]	= (($row_item_br->n_residual) - $n_quantity[$jumlah]);
						
						if($qty_akhir[$jumlah]=='') {
							$qty_akhir[$jumlah]	= 0;
						}

						$this->db->query(" UPDATE tm_do SET f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND f_faktur_created='f' ");
						
						$this->db->query(" UPDATE tm_do_item SET n_residual='$qty_akhir[$jumlah]', f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND i_do_item='$row_item_br->i_do_item' ");
						
					}elseif($n_quantity[$jumlah] < ($row_item_br->n_residual)) {
					
						$qty_akhir[$jumlah]	= (($row_item_br->n_residual) - $n_quantity[$jumlah]);
						
						$this->db->query(" UPDATE tm_do SET f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND f_faktur_created='f' ");
						
						$this->db->query(" UPDATE tm_do_item SET n_residual='$qty_akhir[$jumlah]', f_faktur_created='FALSE' WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND i_do_item='$row_item_br->i_do_item' ");
						
					}else{
						$qty_akhir[$jumlah]	= (($row_item_br->n_residual) - $n_quantity[$jumlah]);
						
						if($qty_akhir[$jumlah]=='') {
							$qty_akhir[$jumlah]	= 0;
						}

						$this->db->query(" UPDATE tm_do SET f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND f_faktur_created='f' ");
						
						$this->db->query(" UPDATE tm_do_item SET n_residual='$qty_akhir[$jumlah]', f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND i_do_item='$row_item_br->i_do_item' ");						
					}
					
					// 24-11-2012, ambil status is_grosir
					$sqlxx	= $this->db->query(" SELECT is_grosir, harga_grosir FROM tm_do_item WHERE i_do = '$ido[$jumlah]' 
									AND i_product = '$i_product[$jumlah]' ");
					if($sqlxx->num_rows() > 0) {
						$sqlxxrow	= $sqlxx->row();
						$is_grosir[$jumlah]	= $sqlxxrow->is_grosir;
						//$harga_grosir[$jumlah]	= $sqlxxrow->harga_grosir;
					}
					else {
						$is_grosir[$jumlah]	= 'f';
						//$harga_grosir[$jumlah]	= 0;
					}
					
					$this->db->query(" INSERT INTO tm_faktur_do_item_t(i_faktur_item,i_faktur,i_do,i_product,e_product_name,n_quantity,v_unit_price,d_entry, is_grosir) 
						VALUES('$i_faktur_item[$jumlah]','$i_faktur','$ido[$jumlah]','$i_product[$jumlah]','$e_product_name[$jumlah]','$n_quantity[$jumlah]','$v_hjp[$jumlah]','$dentry', '$is_grosir[$jumlah]') ");
				}

				if($this->db->trans_status()===FALSE || $this->db->trans_status()==false) {
					$ok	= 0;
					$this->db->trans_rollback();
				}else{
					$ok	= 1;
					$this->db->trans_commit();
				}
			}				
		}else{
			$ok	= 0;
		}
		
		if($ok==1) {
			print "<script>alert(\"Nomor Faktur : '\"+$ifakturcode+\"' telah disimpan, terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		}
	}
	
		
	function mupdate_old($i_faktur,$nw_d_faktur,$i_branch,$v_total_nilai,$n_discount,$v_discount,$nw_d_due_date,$v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$v_total_fppn,$i_do,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$ifakturcode) {
		
		$i_faktur_item	= array();
		$tm_fakturdo_item	= array();
		$qget_tm_do	= array();
		
		$jml_item_br	= array();
		$qty_akhir		= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$this->db->trans_begin();
		
		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qfakturitem	= $this->db->query(" SELECT * FROM tm_faktur_do_item_t WHERE i_faktur='$i_faktur' ");
		if($qfakturitem->num_rows()>0){
			foreach($qfakturitem->result() as $row) {
				$qdoitem	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
				if($qdoitem->num_rows()>0){
					$rdoitem	= $qdoitem->row();
					$ndeliver	= $rdoitem->n_deliver;
					$nresidual	= (($rdoitem->n_residual)+($row->n_quantity));

					$this->db->query(" UPDATE tm_do_item SET n_residual='$nresidual' WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
				}
			}
		}
		
		//$this->db->delete('tm_faktur_do_t', array('i_faktur_code' => $ifakturcode)); 
		$query	= $this->db->query(" UPDATE tm_faktur_do_t SET i_faktur_code='$ifakturcode', d_faktur='$nw_d_faktur', d_due_date='$nw_d_due_date', d_pajak='$nw_d_pajak', n_discount='$n_discount', v_discount='$v_discount', v_total_faktur='$v_total_faktur', v_total_fppn='$v_total_fppn', f_printed='$f_cetak', d_update='$dentry' WHERE i_faktur='$i_faktur' ");
		if($query) {
			//$this->db->delete('tm_faktur_do_item_t', array('i_faktur' => $ifakturcode)); 
			$this->db->query("DELETE FROM tm_faktur_do_item_t WHERE i_faktur='$i_faktur' ");
		}
		
		/***
		$seq_tm_faktur_do	= $this->db->query(" SELECT cast(i_faktur AS integer) AS i_faktur FROM tm_faktur_do_t ORDER BY cast(i_faktur AS integer) DESC LIMIT 1 ");
		if($seq_tm_faktur_do->num_rows() >0 ) {
			$seqrow	= $seq_tm_faktur_do->row();
			$ifaktur	= $seqrow->i_faktur+1;
		} else {
			$ifaktur	= 1;
		}
	
		$this->db->set(array(
			 'i_faktur'=>$ifaktur,
			 'i_faktur_code'=>$i_faktur,
			 'd_faktur'=>$nw_d_faktur,
			 'e_branch_name'=>$i_branch,
			 'd_due_date'=>$nw_d_due_date,
			 'i_faktur_pajak'=>$i_faktur_pajak,
			 'd_pajak'=>$nw_d_pajak,
			 'n_discount'=>$n_discount,
			 'v_discount'=>$v_discount,
			 'v_total_faktur'=>$v_total_faktur,
			 'v_total_fppn'=>$v_total_fppn,
			 'f_printed'=>'FALSE',
			 'f_do_or_nota'=>'FALSE',
			 'd_entry'=>$dentry ));
		
		if($this->db->insert('tm_faktur_do_t')) {
		***/
		
		//$berhasil=0;
		
		if(isset($iteration)){
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				
				$seq_tm_fakturdo_item	= $this->db->query(" SELECT i_faktur_item FROM tm_faktur_do_item_t ORDER BY i_faktur_item DESC LIMIT 1 ");
				
				if($seq_tm_fakturdo_item->num_rows() > 0) {
					$seqrow	= $seq_tm_fakturdo_item->row();
					$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
				} else {
					$i_faktur_item[$jumlah]	= 1;
				}

				$qget_tm_do[$jumlah]	= $this->db->query(" SELECT * FROM tm_do WHERE i_do_code='$i_do[$jumlah]' AND f_do_cancel='f' ORDER BY i_do DESC LIMIT 1 ");
				if($qget_tm_do[$jumlah]->num_rows()>0) {
					$row_tm_do[$jumlah]	= $qget_tm_do[$jumlah]->row();
					$ido[$jumlah]	= $row_tm_do[$jumlah]->i_do;
				}
				
				/***
				$tm_fakturdo_item[$jumlah]	= 
					array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$ifakturcode,
					 'i_do'=>$ido[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_hjp[$jumlah],
					 'd_entry'=>$dentry );				
				***/
				
				$q_qty_do_item	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' ");
				
				if($q_qty_do_item->num_rows()>0) {
					
					$row_item_br	= $q_qty_do_item->row();
					
					$jml_item_br[$jumlah]	= $row_item_br->n_residual;
					
					if($n_quantity[$jumlah]==$jml_item_br[$jumlah]) {
						
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						
						if($qty_akhir[$jumlah]==0) {
							$qty_akhir[$jumlah]	= 0;
						}
						
						/***
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
						
						$arrfakturupdate2[$jumlah]	= array(
							'n_residual'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);
						***/

						//if($berhasil==0 || $berhasil=='0'){						
							$this->db->query(" UPDATE tm_do SET f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND f_faktur_created='f' ");
							//$berhasil=1;
						//}
						
						$this->db->query(" UPDATE tm_do_item SET n_residual='$qty_akhir[$jumlah]', f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND i_do_item='$row_item_br->i_do_item' ");
											 
					} else if($n_quantity[$jumlah] < $jml_item_br[$jumlah]) { // jika jmlitem kurang dari qty brg yg ada
					
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						
						/***
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);

						$arrfakturupdate2[$jumlah]	= array(
							'n_residual'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'FALSE'
						);
						***/
						
						//if($berhasil==0 || $berhasil=='0'){						
							$this->db->query(" UPDATE tm_do SET f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND f_faktur_created='f' ");
							//$berhasil=1;
						//}
						
						$this->db->query(" UPDATE tm_do_item SET n_residual='$qty_akhir[$jumlah]', f_faktur_created='FALSE' WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND i_do_item='$row_item_br->i_do_item' ");
						
					}else{
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						
						if($qty_akhir[$jumlah]==0) {
							$qty_akhir[$jumlah]	= 0;
						}
						/***
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
						
						$arrfakturupdate2[$jumlah]	= array(
							'n_residual'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);					
						***/
						//if($berhasil==0 || $berhasil=='0') {	
							$this->db->query(" UPDATE tm_do SET f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND f_faktur_created='f' ");
							//$berhasil=1;
						//}
						
						$this->db->query(" UPDATE tm_do_item SET n_residual='$qty_akhir[$jumlah]', f_faktur_created='t' WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND i_do_item='$row_item_br->i_do_item' ");
											
					}
					
					/*** Disabled 02072011
					if($berhasil==0){	
						$this->db->update('tm_do',$arrfakturupdate[$jumlah],array('i_do'=>$ido[$jumlah]));					
						$berhasil=1;
					}
										
					$this->db->update('tm_do_item',$arrfakturupdate2[$jumlah],array('i_do'=>$ido[$jumlah],'i_product'=>$i_product[$jumlah],'i_do_item'=>$row_item_br->i_do_item));
					$this->db->insert('tm_faktur_do_item_t',$tm_fakturdo_item[$jumlah]);						
					***/
					
					$this->db->query(" INSERT INTO tm_faktur_do_item_t(i_faktur_item,i_faktur,i_do,i_product,e_product_name,n_quantity,v_unit_price,d_entry) 
						VALUES('$i_faktur_item[$jumlah]','$i_faktur','$ido[$jumlah]','$i_product[$jumlah]','$e_product_name[$jumlah]','$n_quantity[$jumlah]','$v_hjp[$jumlah]','$dentry') ");
				}

				if ($this->db->trans_status()===FALSE || $this->db->trans_status()==false) {
					$ok	= 0;
					$this->db->trans_rollback();
				}else{
					$ok	= 1;
					$this->db->trans_commit();
				}
			}				
		}else{
			$ok	= 0;
		}
		
		if($ok==1) {
			print "<script>alert(\"Nomor Faktur : '\"+$ifakturcode+\"' telah disimpan, terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		}
	}	

	function cari_fpenjualan($ifakturcode,$ifakturcodehiden) {
		return $this->db->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur_code='$ifakturcode' AND i_faktur_code!='$ifakturcodehiden' ");
	}
	
	function clistpenjualanperdo2($nofaktur,$d_do_first,$d_do_last) {
	
	}	


	function mbatal($i_faktur,$i_faktur_code) {
		
		$this->db->trans_begin();
		
		$qfakturitem	= $this->db->query(" SELECT * FROM tm_faktur_do_item_t WHERE i_faktur='$i_faktur' ");
		$numfakturitem	= $qfakturitem->num_rows();
		
		if($numfakturitem>0) {
			
			foreach($qfakturitem->result() as $row) {
				
				$qdoitem	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
				
				if($qdoitem->num_rows()>0) {
					
					$rdoitem	= $qdoitem->row();
					
					$ndeliver	= $rdoitem->n_deliver;
					$nresidual	= (($rdoitem->n_residual)+($row->n_quantity));

					$this->db->query(" UPDATE tm_do_item SET n_residual='$nresidual' WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
					
					$this->db->query(" UPDATE tm_do SET f_faktur_created='f', f_printed='f', f_transfer='f' WHERE i_do='$row->i_do' AND f_do_cancel='f' AND f_faktur_created='t' ");
				}
			}
			
			$this->db->query(" UPDATE tm_faktur_do_t SET f_faktur_cancel='TRUE' WHERE i_faktur='$i_faktur' AND f_faktur_cancel='f' ");
		}else{
		}				

		if ($this->db->trans_status()===FALSE || $this->db->trans_status()==false) {
			$ok	= 0;
			$this->db->trans_rollback();
		}else{
			$ok	= 1;
			$this->db->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Nomor Faktur Penjualan : '\"+$i_faktur_code+\"' telah dibatalkan, terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Data Faktur Penjuaan tdk ditemukan.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		}
						
	}
	
		
	function mbatal_old($i_faktur,$i_faktur_code) {
		
		/*** $update = 0; ***/
		
		$this->db->trans_begin();
		
		$qfakturitem	= $this->db->query(" SELECT * FROM tm_faktur_do_item_t WHERE i_faktur='$i_faktur' ");
		$numfakturitem	= $qfakturitem->num_rows();
		
		if($numfakturitem>0) {
			
			foreach($qfakturitem->result() as $row) {
				
				$qdoitem	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
				
				if($qdoitem->num_rows()>0) {
					
					$rdoitem	= $qdoitem->row();
					
					$ndeliver	= $rdoitem->n_deliver;
					$nresidual	= (($rdoitem->n_residual)+($row->n_quantity));

					$this->db->query(" UPDATE tm_do_item SET n_residual='$nresidual' WHERE i_do='$row->i_do' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
					
					/*** if($update==0) { ***/
						$this->db->query(" UPDATE tm_do SET f_faktur_created='f', f_printed='f', f_transfer='f' WHERE i_do='$row->i_do' AND f_do_cancel='f' AND f_faktur_created='t' ");
						/*** $update = 1; ***/
					/*** } ***/
				}
			}
			
			$this->db->query(" UPDATE tm_faktur_do_t SET f_faktur_cancel='TRUE' WHERE i_faktur='$i_faktur' AND f_faktur_cancel='f' ");
			/***
			print "<script>alert(\"Nomor Faktur Penjualan : '\"+$i_faktur_code+\"' telah dibatalkan, terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
			***/
		}else{
			/***
			print "<script>alert(\"Maaf, Data Faktur Penjuaan tdk ditemukan.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
			***/
		}

		if ($this->db->trans_status()===FALSE || $this->db->trans_status()==false) {
			$ok	= 0;
			$this->db->trans_rollback();
		}else{
			$ok	= 1;
			$this->db->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Nomor Faktur Penjualan : '\"+$i_faktur_code+\"' telah dibatalkan, terimakasih.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Data Faktur Penjuaan tdk ditemukan.\");show(\"listpenjualanperdo/cform\",\"#content\");</script>";
		}
						
	}
}
?>
