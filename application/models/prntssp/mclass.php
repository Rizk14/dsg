<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view($cari) {
			$db2=$this->load->database('db_external', TRUE);
		if($cari!='kosong' && $cari!=''){
			$paramcari	= " WHERE (e_initial_npwp='$cari' OR i_akun_pajak LIKE '%$cari%' OR i_jsetor_pajak LIKE '%$cari%' OR e_jsetor_pajak LIKE '%$cari%') AND f_ssp_cancel='f' ";
		}else{
			$paramcari	= " WHERE f_ssp_cancel='f' ";
		}
		return $db2->query(" SELECT * FROM tm_ssp ".$paramcari." ORDER BY i_ssp ASC ");
	}
	
	function viewperpages($limit,$offset,$cari) {
			$db2=$this->load->database('db_external', TRUE);
		if($cari!='kosong' && $cari!='') {
			$paramcari	= " WHERE (e_initial_npwp='$cari' OR i_akun_pajak LIKE '%$cari%' OR i_jsetor_pajak LIKE '%$cari%' OR e_jsetor_pajak LIKE '%$cari%') AND f_ssp_cancel='f' ";
		}else{
			$paramcari	= " WHERE f_ssp_cancel='f' ";
		}
		$query	= $db2->query(" SELECT * FROM tm_ssp ".$paramcari." ORDER BY i_ssp ASC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}	
	}
	
	function cetakssp($issp) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tm_ssp WHERE i_ssp='$issp' AND f_ssp_cancel='f' ");
		if($query->num_rows()>0){
			return $result	= $query->result();
		}
	}
	
	function penyetor($ipenyetor) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_penyetor WHERE i_penyetor='$ipenyetor' ");
	}
	
	function inisal($npwp) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE e_initial_npwp='$npwp' ");
	}

	function remote($id) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' ORDER BY i_printer DESC LIMIT 1 ");
	}	
}
?>
