<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function totalkontrabon($dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(a.v_total_grand) AS totalkontrabon, sum(a.v_grand_sisa) AS pelunasan 
		FROM tm_dt a 

		WHERE (a.d_dt BETWEEN '$dkontrabon_first' AND '$dkontrabon_last') AND a.f_dt_cancel='f' AND a.f_nota_sederhana='$f_nota_sederhana' ");
	}
	
	function clistkontrabonperpages($limit,$offset,$ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		if(($dkontrabon_first!='' || $dkontrabon_first!='0') && ($dkontrabon_last!='' || $dkontrabon_last!='0')){
			$ddt	= " WHERE (a.d_dt BETWEEN '$dkontrabon_first' AND '$dkontrabon_last') ";	
			$fbatal	= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}else{
			$ddt	= "";
			$fbatal	= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}

		if($f_nota_sederhana=='f') {
			$query	= $db2->query(" SELECT c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota	
					INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
			$query	= $db2->query(" SELECT c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
					INNER JOIN tm_faktur_item e ON e.i_faktur=c.i_faktur
					INNER JOIN tm_sj f ON f.i_sj=e.i_sj
					INNER JOIN tr_branch d ON d.i_branch_code=f.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");		
		}
										
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}			
	}
			
	function clistkontrabon($ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
	$db2=$this->load->database('db_external', TRUE);
		if(($dkontrabon_first!='' || $dkontrabon_first!='0') && ($dkontrabon_last!='' || $dkontrabon_last!='0')) {
			$ddt	= " WHERE (a.d_dt BETWEEN '$dkontrabon_first' AND '$dkontrabon_last') ";
			$fbatal	= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}else{
			$ddt	= "";
			$fbatal	= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}
		
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT  c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota	
					INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC ");
		}else{
			return $db2->query(" SELECT c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota	
					INNER JOIN tm_faktur_item e ON e.i_faktur=c.i_faktur
					INNER JOIN tm_sj f ON f.i_sj=e.i_sj
					INNER JOIN tr_branch d ON d.i_branch_code=f.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC ");
		}
	}

	function clistkontrabonperpages2($limit,$offset,$ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		if($ikontrabon!=''){
			$idtcode	= " WHERE a.i_dt='$ikontrabon' ";	
			$fbatal		= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		} else {
			$idtcode	= "";
			$fbatal		= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}

		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				
				FROM tm_dt a 
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				 
				".$idtcode." ".$fbatal." ".$fnotasederhana."
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				ORDER BY a.i_dt_code ASC, a.d_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
							
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}			
	}
			
	function clistkontrabon2($ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
	$db2=$this->load->database('db_external', TRUE);
		if($ikontrabon!=''){
			$idtcode	= " WHERE a.i_dt='$ikontrabon' ";	
			$fbatal		= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		} else {
			$idtcode	= "";
			$fbatal		= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}

		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				
				FROM tm_dt a 
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				 
				".$idtcode." ".$fbatal." ".$fnotasederhana."
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				ORDER BY a.i_dt_code ASC, a.d_dt ASC ");						
	}

	function lbarangjadi($fnotasederhana) {
			$db2=$this->load->database('db_external', TRUE);
		/* 26-10-2011
		return $db2->query("
				SELECT a.i_dt_code AS idtcode, a.d_dt AS ddt
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND a.f_pelunasan='f' GROUP BY a.i_dt_code, a.d_dt
				
				ORDER BY a.i_dt_code ASC, a.d_dt ASC ");
		*/
		
		return $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt AND a.f_nota_sederhana='$fnotasederhana'
				
				WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana'
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				
				ORDER BY a.i_dt_code DESC, a.d_dt DESC ");		
	}	

	function lbarangjadiperpages($limit,$offset,$fnotasederhana) {
			$db2=$this->load->database('db_external', TRUE);
		/*	
		$query	= $db2->query("
				SELECT a.i_dt_code AS idtcode, a.d_dt AS ddt
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND a.f_pelunasan='f' GROUP BY a.i_dt_code, a.d_dt
				
				ORDER BY a.i_dt_code ASC, a.d_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
		*/

		$query	= $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana' 
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				
				ORDER BY a.i_dt_code DESC, a.d_dt DESC LIMIT ".$limit." OFFSET ".$offset." ");
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flbarangjadi($key,$fnotasederhana) {
			$db2=$this->load->database('db_external', TRUE);
		/*
		a.d_dt='$key'
		 */ 
		/*
		return $db2->query("
				SELECT a.i_dt_code AS idtcode, a.d_dt AS ddt
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND a.f_pelunasan='f' AND (a.i_dt_code LIKE '$key%') GROUP BY a.i_dt_code, a.d_dt 
				
				ORDER BY a.i_dt_code ASC, a.d_dt ASC ");
		*/

		return $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND (a.i_dt_code LIKE '$key%') AND a.f_nota_sederhana='$fnotasederhana'
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				
				ORDER BY a.i_dt_code DESC, a.d_dt DESC ");
	}
	
	function lfaktur($f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
		return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn
						
			FROM tm_faktur_do_t b
			
			INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
			INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
			
			WHERE b.f_faktur_cancel='f'
				
			GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn ORDER BY b.i_faktur_code ASC ");						
		}else{
			return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
							
				FROM tm_faktur b
				
				INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj d ON d.i_sj=a.i_sj
				INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
				
				WHERE b.f_faktur_cancel='f'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC ");		
		}	
	}
	
	function lfakturperpages($limit,$offset,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			$query	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn
							
				FROM tm_faktur_do_t b
				
				INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
				INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
				
				WHERE b.f_faktur_cancel='f'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn ORDER BY b.i_faktur_code ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
			$query	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
							
				FROM tm_faktur b
				
				INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj d ON d.i_sj=a.i_sj
				INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
				
				WHERE b.f_faktur_cancel='f'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC LIMIT ".$limit." OFFSET ".$offset." ");		
		}
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}		
	}
	
	function flfaktur($key,$f_nota_sederhana){	
			$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {	
			return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn
							
				FROM tm_faktur_do_t b
				
				INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
				INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
				
				WHERE b.f_faktur_cancel='f' AND b.i_faktur_code LIKE '$key%'
				
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn ORDER BY b.i_faktur_code ASC ");
		}else{
			return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
							
				FROM tm_faktur b
				
				INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj d ON d.i_sj=a.i_sj
				INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
				
				WHERE b.f_faktur_cancel='f' AND b.i_faktur_code LIKE '$key%'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC ");			
		}		
	}
	
	function getdtheader($idt,$fnotasederhana){
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_dt WHERE i_dt='$idt' AND f_pelunasan='f' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana' ");
	}

	function getdtheader2($idt,$fnotasederhana){
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_dt WHERE i_dt='$idt' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana' ");
	}
		
	function ldtitem($idt,$fnotasederhana) {
			$db2=$this->load->database('db_external', TRUE);
		if($fnotasederhana=='f') {
			$query	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
					
					FROM tm_faktur_do_t b
					
					INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
					INNER JOIN tm_dt_item d ON d.i_nota=b.i_faktur
					INNER JOIN tm_dt e ON e.i_dt=d.i_dt
					INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
								
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='t' AND e.f_pelunasan='f' AND e.i_dt='$idt'
							
					GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
	
					ORDER BY b.i_faktur_code ASC ");
		}else{
		$query	= $db2->query(" SELECT c.i_faktur, c.i_faktur_code, d.e_branch_name, c.d_faktur, c.d_due_date, d.i_customer, d.i_branch_code, d.e_branch_name, c.n_discount, c.v_discount, c.v_total_faktur, c.v_total_fppn 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota	
					INNER JOIN tm_faktur_item e ON e.i_faktur=c.i_faktur
					INNER JOIN tm_sj f ON f.i_sj=e.i_sj
					INNER JOIN tr_branch d ON d.i_branch_code=f.i_branch
					
					WHERE c.f_faktur_cancel='f' AND c.f_kontrabon='t' AND a.f_pelunasan='f' AND a.i_dt='$idt'
					
					GROUP BY c.i_faktur, c.i_faktur_code, d.e_branch_name, c.d_faktur, c.d_due_date, d.i_customer, d.i_branch_code, d.e_branch_name, c.n_discount, c.v_discount, c.v_total_faktur, c.v_total_fppn 
					ORDER BY c.i_faktur_code ASC ");		
		}
		
		if($query->num_rows()>0){
			return $result	= $query->result();
		}			
	}

	function totalfaktur($ifaktur) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_do_item_t WHERE i_faktur='$ifaktur' ");
	}

	function totalfakturNONDO($ifaktur) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_item WHERE i_faktur='$ifaktur' ");
	}
	
	function ldtitem2($idt,$fnotasederhana){
			$db2=$this->load->database('db_external', TRUE);
		if($fnotasederhana=='f') {
			$query	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
					
					FROM tm_faktur_do_t b
					
					INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
					INNER JOIN tm_dt_item d ON d.i_nota=b.i_faktur
					INNER JOIN tm_dt e ON e.i_dt=d.i_dt
					INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
								
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='t' AND e.i_dt='$idt'
							
					GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
	
					ORDER BY b.i_faktur_code ASC ");
		}else{
		$query	= $db2->query(" SELECT c.i_faktur, c.i_faktur_code, d.e_branch_name, c.d_faktur, c.d_due_date, d.i_customer, d.i_branch_code, d.e_branch_name, c.n_discount, c.v_discount, c.v_total_faktur, c.v_total_fppn 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota	
					INNER JOIN tm_faktur_item e ON e.i_faktur=c.i_faktur
					INNER JOIN tm_sj f ON f.i_sj=e.i_sj
					INNER JOIN tr_branch d ON d.i_branch_code=f.i_branch
					
					WHERE c.f_faktur_cancel='f' AND c.f_kontrabon='t' AND a.i_dt='$idt'
					
					GROUP BY c.i_faktur, c.i_faktur_code, d.e_branch_name, c.d_faktur, c.d_due_date, d.i_customer, d.i_branch_code, d.e_branch_name, c.n_discount, c.v_discount, c.v_total_faktur, c.v_total_fppn 
					
					ORDER BY c.i_faktur_code ASC ");		
		}
		
		if($query->num_rows()>0){
			return $result	= $query->result();
		}			
	}
		
	function mupdate($i_kontrabon_code,$ikontrabon,$tgl_kontrabon,$iteration,$i_faktur_code,$i_faktur,$tgl_faktur,$tgl_fakturhidden,$due_date,$due_datehidden,$e_branch_name,$v_total_plusppn,$i_customer,$i_branch_code,$fnotasederhana) {
			$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$indek = 0;
		$ifaktur = 0;
		
		$qdtitem_awal = $db2->query(" SELECT * FROM tm_dt_item WHERE i_dt='$ikontrabon' ORDER BY i_dt_item ASC ");
		$result 	  = $qdtitem_awal->result();
		
		foreach($result as $row) {
			
			if($fnotasederhana=='f') {
				
				$q_tm_faktur = $db2->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' ");
				
				if($q_tm_faktur->num_rows()>0) {
					
					if($ifaktur!=$row->i_nota) {
						
						$db2->query(" UPDATE tm_faktur_do_t SET f_kontrabon='f' WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' AND f_kontrabon='t' ");
						$ifaktur = $row->i_nota;
					}
				}
				
			}else{
				$q_tm_faktur = $db2->query(" SELECT * FROM tm_faktur WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' ");
				
				if($q_tm_faktur->num_rows()>0) {
					
					if($ifaktur!=$row->i_nota) {
						
						$db2->query(" UPDATE tm_faktur SET f_kontrabon='f' WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' AND f_kontrabon='t' ");
						$ifaktur = $row->i_nota;
					}
				}
			}
			
			$indek+=1;
		}
		
		$indek2		= 0;
		$discount	= 0;
		$totalnya	= 0;
		$nilai_ppn	= 0;
		$totalgrand	= 0;
		
		$db2->query(" DELETE FROM tm_dt_item WHERE i_dt='$ikontrabon' ");
		
		for($jml=0; $jml<=$iteration; $jml++) {
			
			if($fnotasederhana=='f') {
				
				$qfaktur	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
							
				FROM tm_faktur_do_t b
				
				INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
				INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
				
				WHERE b.i_faktur='$i_faktur[$jml]' AND b.f_faktur_cancel='f'
				
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
				
				ORDER BY b.i_faktur_code ASC ");
				
			}else{
				
				$qfaktur	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
					
					FROM tm_faktur b
					
					INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
					INNER JOIN tm_sj d ON d.i_sj=a.i_sj
					INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
					
					WHERE b.i_faktur='$i_faktur[$jml]' AND b.f_faktur_cancel='f'
						
					GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
					
					ORDER BY b.i_faktur_code ASC ");	
					
			}
						
			$rfaktur	= $qfaktur->row();
			
			/*** 17112011
			$discount	= $discount+$rfaktur->v_discount;
			$v_total	= $rfaktur->v_discount+$rfaktur->v_total_faktur; // di field tsb sudah dikurangi discount maka hrs ditambah dulu
			$totalnya 	= $totalnya+$v_total;
			$ppn		= (($rfaktur->v_total_faktur*10) / 100); // total fakturnya itu adalah total faktur setelah dikurangi discount
			$nilai_ppn	= $nilai_ppn+$ppn;
			$totalgrand = $totalgrand+$rfaktur->v_total_fppn; // setelah dikurangi discount & ditambah ppn				
			***/
			
			/*** Memakai variabel dari $v_total_plusppn dapat ditemukan nilai totalfaktur nya ***/
			if($fnotasederhana=='f') {
				$query3 = $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_do_item_t WHERE i_faktur='$rfaktur->i_faktur' ");
			}else{
				$query3 = $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_item WHERE i_faktur='$rfaktur->i_faktur' ");
			}
			
			$row3 = $query3->row();
			
			$nilai_ppn_perfaktur = (($row3->totalfaktur*11) / 100);
				
			if($rfaktur->v_discount>0) {
				$total_sblm_ppn_perfaktur = round($row3->totalfaktur - $rfaktur->v_discount); // DPP
				$nilai_ppn2_perfaktur	= round((($total_sblm_ppn_perfaktur*11) / 100));
				$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
			}else{
				$total_sblm_ppn_perfaktur = round($row3->totalfaktur); // DPP
				$nilai_ppn2_perfaktur	= round($nilai_ppn_perfaktur);
				$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
			}
			
			if($fnotasederhana=='f') {
				$db2->query(" UPDATE tm_faktur_do_t SET f_kontrabon='t' WHERE i_faktur='$rfaktur->i_faktur' AND f_faktur_cancel='f' ");
				
				$qfakturitem_detail_dt	= $db2->query(" SELECT i_product, e_product_name FROM tm_faktur_do_item_t WHERE i_faktur='$rfaktur->i_faktur' ORDER BY i_faktur_item ASC ");
			}else{
				$db2->query(" UPDATE tm_faktur SET f_kontrabon='t' WHERE i_faktur='$rfaktur->i_faktur' AND f_faktur_cancel='f' ");
								
				$qfakturitem_detail_dt	= $db2->query(" SELECT i_product, e_product_name FROM tm_faktur_item WHERE i_faktur='$rfaktur->i_faktur' ORDER BY i_faktur_item ASC ");
			}
			
			if($qfakturitem_detail_dt->num_rows()>0) {
				$result_fakturitem_detail_dt = $qfakturitem_detail_dt->result();
				
				foreach($result_fakturitem_detail_dt as $row2) {
					$qidtitem	= $db2->query(" SELECT CAST(i_dt_item AS integer)+1 AS i_dt_item FROM tm_dt_item ORDER BY CAST(i_dt_item AS integer) DESC LIMIT 1 ");
					if($qidtitem->num_rows()>0) {
						$ridtitem = $qidtitem->row();
						$idtitem = $ridtitem->i_dt_item;
					}else{
						$idtitem = 1;
					}
					
					$db2->query(" INSERT INTO tm_dt_item (i_dt_item,i_dt,i_nota,d_nota,i_customer,i_branch,i_product,e_product_name,d_entry,d_update) VALUES('$idtitem','$ikontrabon','$rfaktur->i_faktur','$rfaktur->d_faktur','$rfaktur->i_customer','$rfaktur->i_branch_code','$row2->i_product','$row2->e_product_name','$dentry','$dentry') ");	
				}
			}
			
			$discount	= $discount+$rfaktur->v_discount;
			
			$v_total	= $row3->totalfaktur;
			$totalnya 	= $totalnya+$v_total;
					
			$ppn		= $nilai_ppn2_perfaktur;
			$nilai_ppn	= $nilai_ppn+$ppn;
				
			$totalgrand = $totalgrand+$total_grand_perfaktur;
						
			$indek2+=1;
		}
		
		/***
		* 18112011
		
		$db2->query(" UPDATE tm_dt SET d_dt='$tgl_kontrabon', v_total='$v_total', v_discount='$discount', v_ppn='$nilai_ppn', v_total_grand='$totalgrand', v_grand_sisa='$totalgrand', d_update='$dentry' WHERE i_dt='$ikontrabon' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana' ");
		***/
		
		$db2->query(" UPDATE tm_dt SET d_dt='$tgl_kontrabon', v_total='$totalnya', v_discount='$discount', v_ppn='$nilai_ppn', v_total_grand='$totalgrand', v_grand_sisa='$totalgrand', d_update='$dentry' WHERE i_dt='$ikontrabon' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana' ");
		
		if ($db2->trans_status()===FALSE) {
			$ok	= 0;
			$db2->trans_rollback();
		} else {
			$ok	= 1;
			$db2->trans_commit();
		}
		
		if($ok==1) {
			print "<script>alert(\"Nomor Kontra Bon : '\"+$i_kontrabon_code+\"' telah disimpan, terimakasih.\");show(\"listkontrabon/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Kontra Bon gagal disimpan. Terimakasih.\");show(\"listkontrabon/cform\",\"#content\");</script>";
		}
	}

	function cari_dt($idtcode,$idtcodehidden) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_dt WHERE i_dt_code='$idtcode' AND i_dt_code!='$idtcodehidden' AND f_dt_cancel='f' ");
	}
	
	function mbatal($i_dt,$i_dt_code,$fnotasederhana) {
			$db2=$this->load->database('db_external', TRUE);
		$qcaridt	= $db2->query(" SELECT i_dt FROM tm_dt WHERE i_dt='$i_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana' ");
		$ncaridt	= $qcaridt->num_rows();
		
		if($ncaridt>0){	
		
			$indek = 0;
			$ifaktur = 0;
			
			$qdtitem_awal = $db2->query(" SELECT * FROM tm_dt_item WHERE i_dt='$i_dt' ORDER BY i_dt_item ASC ");
			$result 	  = $qdtitem_awal->result();
			foreach($result as $row) {
				
				if($fnotasederhana=='f') {
					$q_tm_faktur = $db2->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' ");
					if($q_tm_faktur->num_rows()>0) {
						if($ifaktur!=$row->i_nota) {
							$db2->query(" UPDATE tm_faktur_do_t SET f_kontrabon='f' WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' AND f_kontrabon='t' ");
							$ifaktur = $row->i_nota;
						}
					}
				}else{
					$q_tm_faktur = $db2->query(" SELECT * FROM tm_faktur WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' ");
					if($q_tm_faktur->num_rows()>0) {
						if($ifaktur!=$row->i_nota) {
							$db2->query(" UPDATE tm_faktur SET f_kontrabon='f' WHERE i_faktur='$row->i_nota' AND f_faktur_cancel='f' AND f_kontrabon='t' ");
							$ifaktur = $row->i_nota;
						}
					}
				}
				$indek+=1;
			}
		
			$db2->query(" UPDATE tm_dt SET f_dt_cancel='t' WHERE i_dt='$i_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana' AND f_dt_cancel='f' ");
			
			print "<script>alert(\"Nomor Kontra Bon : '\"+$i_dt_code+\"' telah dibatalkan, terimakasih.\");
			window.open(\"../../../index\", \"_self\");</script>";
			
			
		}else{
			
			print "<script>alert(\"Maaf, Data Kontra Bon tdk ditemukan.\");
			window.open(\"../../index\", \"_self\");</script>";
			
			
		}
	}
}
?>
