<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ikk,$iperiode,$iarea)
    {
		$this->db->select("	a.i_kendaraan from tm_kk a 
							inner join tr_area b on(a.i_area=b.i_area)
							where a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->i_kendaraan;
			}
		}else{
			$xxx='';
		}

		if(trim($xxx)==''){
			$this->db->select("	a.*, b.e_area_name, '' as e_pengguna from tm_kk a, tr_area b 
								where a.i_area=b.i_area and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		}else{
			$this->db->select("	a.*, b.e_area_name
													from tm_kk a, tr_area b, tr_kendaraan c
													where a.i_area=b.i_area and a.i_kendaraan=c.i_kendaraan and a.i_periode=c.i_periode
													and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacoa($num,$offset,$area)
    {
		/*
		if($area!='00'){
  		$this->db->select(" * from tr_coa where not (i_coa like '111-4%') and not i_coa_group like '%000' or i_coa in('120-3000','120-1000','120-2000','120-3000','120-4000','120-5000') order by i_coa",false)->limit($num,$offset);
  		$this->db->select(" * from tr_coa where (i_coa like '6%' or upper(e_coa_name) like 'HUTANG%' or upper(e_coa_name) like 'AKTIVA%' 
 		                    or upper(e_coa_name) like 'PAJAK%') order by i_coa",false)->limit($num,$offset);
    }else{
  		$this->db->select(" * from tr_coa where not (i_coa like '111-4%') and not i_coa_group like '%000' or i_coa in('120-3000','120-1000','120-2000','120-3000','120-4000','120-5000') order by i_coa",false)->limit($num,$offset);
		$this->db->select(" * from tr_coa where (i_coa like '6%' or upper(e_coa_name) like 'HUTANG%' or upper(e_coa_name) like 'AKTIVA%' 
  		                    or upper(e_coa_name) like 'PAJAK%') order by i_coa",false)->limit($num,$offset);
    }
    */
   if($area!='00'){	  
			 $this->db->select(" * from tr_coa where (i_coa like '60%' or i_coa like '61%' or i_coa like '8%' or i_coa like '9%' 
			 or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
			  or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%' )order by i_coa",false)->limit($num,$offset);
      }else{
			$this->db->select(" * from tr_coa where (i_coa like '60%' or i_coa like '61%' or i_coa like '8%' or i_coa like '9%' 
			or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
			  or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%' )order by i_coa",false)->limit($num,$offset);
		  } 
    
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricoa($cari,$num,$offset,$area)
    {
		/*
		if($area!='00'){
		  $this->db->select(" * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) and 
                    		  not (i_coa like '111-4%') and not i_coa_group like '%000' or i_coa in('120-3000','120-1000','120-2000','120-3000','120-4000','120-5000') order by i_coa",false)->limit($num,$offset);
		  $this->db->select(" * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) and 
                   		  (i_coa like '6%' or upper(e_coa_name) like 'HUTANG%' or upper(e_coa_name) like 'AKTIVA%' 
 		                    or upper(e_coa_name) like 'PAJAK%') order by i_coa",false)->limit($num,$offset);
    }else{
		  $this->db->select(" * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) and 
                    		  not (i_coa like '111-4%') and not i_coa_group like '%000' or i_coa in('120-3000','120-1000','120-2000','120-3000','120-4000','120-5000') order by i_coa",false)->limit($num,$offset);
		  $this->db->select(" * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) and 
		                      (i_coa like '6%' or upper(e_coa_name) like 'HUTANG%' or upper(e_coa_name) like 'AKTIVA%' 
 		                    or upper(e_coa_name) like 'PAJAK%') order by i_coa",false)->limit($num,$offset);
    }
    */
    if($area!='00'){
		  $this->db->select("  * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) and i_coa in (select i_coa from tr_coa where i_coa like '60%' or i_coa like '61%' 
											oor i_coa like '8%' or i_coa like '9%' or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
											or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%'  )order by i_coa",false)->limit($num,$offset);
      }else{
		 $this->db->select("  * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) and i_coa in (select i_coa from tr_coa where i_coa like '60%' or i_coa like '61%' 
											or i_coa like '8%' or i_coa like '9%' or i_coa like '620-2200' or i_coa like '1%' or i_coa like '2%'
											or i_coa like '3%' or i_coa like '4%'  or i_coa like '5%' or i_coa like '9%' )order by i_coa",false)->limit($num,$offset);
    }
    
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacakendaraan($cari,$area,$periode,$num,$offset)
    {
    if($cari=='sikasep'){
		  $this->db->select(" * from tr_kendaraan a
							            inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							            inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							            where a.i_area='$area' and a.i_periode='$periode'
							            order by a.i_kendaraan",false)->limit($num,$offset);
    }else{
		  $this->db->select(" * from tr_kendaraan a
							            inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							            inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							            where a.i_area='$area' and a.i_periode='$periode'
                          and (upper(i_kendaraan) like '%$cari%' or upper(e_pengguna) like '%$cari%')
							            order by a.i_kendaraan",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carikendaraan($area,$periode,$cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan a
					inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
					inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
					where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
					and a.i_area='$area' and a.i_periode='$periode'
					order by a.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
/*
    function insert($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,$ecoaname,$edescription,$ejamin,$ejamout,$nkm,$etempat,$fdebet,$dbukti,$enamatoko,$epengguna,$ibukti)
*/
    function insert($iareax,$ikbank,$iperiode,$icoa,$vkb,$dkb,$ecoaname,$edescription,$fdebet,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	            => $iareax,
				'i_kbank'	            => $ikbank,				
				'i_periode'         	=> $iperiode,
				'i_coa'	            	=> $icoa,
				'v_bank'	            => $vkb,
				'd_bank'	            => $dkb,
				'e_coa_name'	        => $ecoaname,
				'e_description'	        => $edescription,
				'd_entry'           	=> $dentry,
				'f_debet'	            => $fdebet,
				'i_coa_bank'            => $icoabank,
				'v_sisa'				=> $vkb
    		)
    	);
    	$this->db->insert('tm_kbank');
    }
        function insertx($iareax,$ikbank,$iperiode,$icoa,$vkb,$dkb,$ecoaname,$edescription,$fdebet,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	            => $iareax,
				'i_kbank'	            => $ikbank,				
				'i_periode'         	=> $iperiode,
				'i_coa'	            	=> $icoa,
				'v_bank'	            => $vkb,
				'v_sisa'	            => $vkb,
				'd_bank'	            => $dkb,
				'e_coa_name'	        => $ecoaname,
				'e_description'	      => $edescription,
				'd_entry'           	=> $dentry,
				'f_debet'	            => $fdebet,
				'i_coa_bank'          => $icoabank
    		)
    	);
    	$this->db->insert('tm_kbank');
    }




 function insertkb($iareax,$ikb,$iperiode,$icoa,$vkb,$dbukti,$ecoaname,$edescription,$fdebet)
     {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->set(
    		array(
				'i_area'	            => $iareax,
				'i_kb'	            	=> $ikb,				
				'i_periode'         	=> $iperiode,
				'i_coa'	            	=> $icoa,
				'd_bukti'                => $dbukti,
				'v_kb'		            => $vkb,
				'd_kb'		            => $dbukti,
				'e_coa_name'	        => $ecoaname,
				'e_description'	     	 => $edescription,
				'd_entry'           	=> $dentry,
				'f_debet'	            => $fdebet
    		)
    	);
    	$this->db->insert('tm_kb');
      }   	


         function insertkk($iareax,$ikk,$iperiode,$icoa,$vkk,$dkk,$ecoaname,$edescription,$fdebet)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	            => $iareax,
				'i_kk'	            	=> $ikk,				
				'i_periode'         	=> $iperiode,
				'_coa'	            	=> $icoa,
				'v_kk'		            => $vkk,
				'd_kk'		            => $dkk,
				'e_coa_name'	        => $ecoaname,
				'e_description'	        => $edescription,
				'd_entry'           	=> $dentry,
				'f_debet'	            => $fdebet,
    		)
    	);
    	$this->db->insert('tm_kk');
    }


    function update($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,$ecoaname,$edescription,$ejamin,$ejamout,$nkm,$etempat,$fdebet,$dbukti,$enamatoko,$epengguna,$ibukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
    	$this->db->set(
    		array(
				'i_kendaraan'	=> $ikendaraan,
				'i_coa'		=> $icoa,
				'v_kk'		=> $vkk,
				'i_bukti_pengeluaran'	=> $ibukti,
				'd_kk'		=> $dkk,
				'e_coa_name'	=> $ecoaname,
				'e_description'	=> $edescription,
				'e_jam_in'	=> $ejamin,
				'e_jam_out'	=> $ejamout,
				'n_km'		=> $nkm,
				'e_tempat'	=> $etempat,
				'd_update'	=> $dupdate,
				'd_bukti'	=> $dbukti,
				'f_debet'	=> $fdebet,
				'e_nama_toko'	=> $enamatoko,
				'e_pengguna'	=> $epengguna
    		)
    	);
		$this->db->where('i_area',$iarea);
		$this->db->where('i_kk',$ikk);
		$this->db->where('i_periode',$iperiode);
   	$this->db->update('tm_kk');
    }
#####
    function insertpvb($ipvb,$icoabank,$ipv,$iarea,$ipvtype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_pvb'               => $ipvb,
				'i_coa_bank' 	        => $icoabank,
				'i_pv'	            	=> $ipv,
				'i_area'	            => $iarea,
				'i_pv_type'           => $ipvtype,
				'd_entry'           	=> $dentry,
    		)
    	);
    	$this->db->insert('tm_pvb');
    }
    function insertpv($ipv,$iarea,$iperiode,$icoa,$dpv,$tot,$eremark,$ipvtype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_pv'	            	=> $ipv,
				'i_area'	            => $iarea,
				'i_periode'         	=> $iperiode,
				'i_coa' 	            => $icoa,
				'd_pv'		            => $dpv,
				'v_pv'		            => $tot,
				'd_entry'           	=> $dentry,
				'i_pv_type'             => $ipvtype
    		)
    	);
    	$this->db->insert('tm_pv');
    }
    function insertpvitem($ipv,$iarea,$icoa,$ecoaname,$vpv,$edescription,$ikk,$ipvtype,$iareax,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	          => $iarea,
				'i_pv'	              => $ipv,
				'i_coa'               => $icoa,
				'e_coa_name'	      => $ecoaname,
				'v_pv'		          => $vpv,
				'e_remark'    	      => $edescription,
				'i_kk'                => $ikk,
				'i_pv_type'           => $ipvtype,
				'i_area_kb'           => $iareax,
				'i_coa_bank'          => $icoabank
    		)
    	);
    	$this->db->insert('tm_pv_item');
    }
     function insertrvitem( $irv,$iarea,$icoabank,$ecoaname,$vrv,$ireff,$ikodebm,$irvtype,$iareax,$icoa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	          => $iarea,
				'i_rv'	              => $irv,
				'i_coa'               => $icoabank,
				'e_coa_name'	      => $ecoaname,
				'v_rv'		          => $vrv,
				'e_remark'    	      => $ireff,
				'i_kk'                => $ikodebm,
				'i_rv_type'           => $irvtype,
				'i_area_kb'           => $iareax,
				'i_coa_bank'          => $icoa
    		)
    	);
    	$this->db->insert('tm_rv_item');
    }
       function insertrv($irv,$iarea,$iperiode,$icoa,$drv,$tot,$eremark,$irvtype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_rv'	            	=> $irv,
				'i_area'	            => $iarea,
				'i_periode'         	=> $iperiode,
				'i_coa' 	            => $icoa,
				'd_rv'		            => $drv,
				'v_rv'		            => $tot,
				'd_entry'           	=> $dentry,
				'i_rv_type'           => $irvtype
    		)
    	);
    	$this->db->insert('tm_rv');
    }
#####
    function insertrvb($irvb,$icoabank,$irv,$iarea,$irvtype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_rvb'                 => $irvb,
				'i_coa_bank' 	        => $icoabank,
				'i_rv'	            	=> $irv,
				'i_area'	            => $iarea,
				'i_rv_type'             => $irvtype,
				'd_entry'           	=> $dentry,
    		)
    	);
    	$this->db->insert('tm_rvb');
    }
  function runningnumberpvb($th,$bl,$icoabank,$iarea)
	{
		$this->db->select(" max(substr(i_pvb,11,6)) as max 
		                    from tm_pvb 
		                    where substr(i_pvb,4,2)='$th' and substr(i_pvb,6,2)='$bl' and i_coa_bank='$icoabank'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nopvb  =$terakhir+1;
			settype($nopvb,"string");
			$a=strlen($nopvb);
			while($a<6){
			  $nopvb="0".$nopvb;
			  $a=strlen($nopvb);
			}
			$nopvb  ="PV-".$th.$bl."-".$iarea.$nopvb;
			return $nopvb;
		}else{
			$nopvb  ="000001";
			$nopvb  ="PV-".$th.$bl."-".$iarea.$nopvb;
			return $nopvb;
		}
  }
    function runningnumberrvb($th,$bl,$icoa,$iarea)
	{
		$this->db->select(" max(substr(i_rvb,11,6)) as max 
		                    from tm_rvb 
		                    where substr(i_rvb,4,2)='$th' and substr(i_rvb,6,2)='$bl' and i_coa_bank='$icoa'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$norvb  =$terakhir+1;
			settype($norvb,"string");
			$a=strlen($norvb);
			while($a<6){
			  $norvb="0".$norvb;
			  $a=strlen($norvb);
			}
			$norvb  ="RV-".$th.$bl."-".$iarea.$norvb;
			return $norvb;
		}else{
			$norvb  ="000001";
			$norvb  ="RV-".$th.$bl."-".$iarea.$norvb;
			return $norvb;
		}
  }
  function runningnumberrv($th,$bl,$iarea,$irvtype)
	{
		$this->db->select(" max(substr(i_rv,11,6)) as max 
		                    from tm_rv 
		                    where substr(i_rv,4,2)='$th' and substr(i_rv,6,2)='$bl' and i_area='$iarea'
		                    and i_rv_type='$irvtype'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$norv  =$terakhir+1;
			settype($norv,"string");
			$a=strlen($norv);
			while($a<6){
			  $norv="0".$norv;
			  $a=strlen($norv);
			}
			$norv  ="RV-".$th.$bl."-".$iarea.$norv;
			return $norv;
		}else{
			$norv  ="000001";
			$norv  ="RV-".$th.$bl."-".$iarea.$norv;
			return $norv;
		}
  }
	function runningnumberpv($th,$bl,$iarea,$ipvtype)
	{
		$this->db->select(" max(substr(i_pv,11,6)) as max 
		                    from tm_pv 
		                    where substr(i_pv,4,2)='$th' and substr(i_pv,6,2)='$bl' and i_area='$iarea'
		                    and i_pv_type='$ipvtype'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nopv  =$terakhir+1;
			settype($nopv,"string");
			$a=strlen($nopv);
			while($a<6){
			  $nopv="0".$nopv;
			  $a=strlen($nopv);
			}
			$nopv  ="PV-".$th.$bl."-".$iarea.$nopv;
			return $nopv;
		}else{
			$nopv  ="000001";
			$nopv  ="PV-".$th.$bl."-".$iarea.$nopv;
			return $nopv;
		}
  }
	function runningnumberbank($th,$bl,$iarea,$icoabank)
	{
		$this->db->select(" max(substr(i_kbank,9,5)) as max from tm_kbank 
		                    where substr(i_kbank,4,2)='$th' and substr(i_kbank,6,2)='$bl' and i_coa_bank='$icoabank'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="BK-".$th.$bl."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="BK-".$th.$bl."-".$nogj;
			return $nogj;
		}
    }

    function runningnumberbankmasuk($th,$bl,$iarea,$icoabank)
	{
		$this->db->select(" max(substr(i_kbank,9,5)) as max from tm_kbank 
		                    where substr(i_kbank,4,2)='$th' and substr(i_kbank,6,2)='$bl' and i_coa_bank='$icoabank'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="BM-".$th.$bl."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="BM-".$th.$bl."-".$nogj;
			return $nogj;
		}
    }

        function runningnumberkk($th,$bl,$iarea,$icoabank)
{
		$this->db->select(" max(substr(i_kk,9,5)) as max from tm_kk where substr(i_kk,4,2)='$th' and substr(i_kk,6,2)='$bl' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="KK-".$th.$bl."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="KK-".$th.$bl."-".$nogj;
			return $nogj;
		}
    }

        function runningnumberkasbesar($th,$bl,$iarea,$icoabank)
	{
		$this->db->select(" max(substr(i_kb,9,5)) as max from tm_kb where substr(i_kb,4,2)='$th' and substr(i_kb,6,2)='$bl'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="KB-".$th.$bl."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="KB-".$th.$bl."-".$nogj;
			return $nogj;
		}
    }

	function bacasaldo($area,$tanggal,$icoabank)
  {	    
		$tmp = explode("-", $tanggal);
		$thn	= $tmp[0];
		$bln	= $tmp[1];
		$tgl 	= $tmp[2];
		$dsaldo	= $thn."/".$bln."/".$tgl;
		$dtos	= $this->mmaster->dateAdd("d",1,$dsaldo);
		$tmp1 	= explode("-", $dtos,strlen($dtos));
		$th	= $tmp1[0];
		$bl	= $tmp1[1];
		$dt	= $tmp1[2];
		$dtos	= $th.$bl;
		$this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and i_coa='$icoabank' ",false);
#and substr(i_coa,6,2)='$area' and substr(i_coa,1,5)='111.1'
		$query = $this->db->get();
		$saldo=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$saldo=$row->v_saldo_awal;
			}
		}
		$this->db->select(" sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and x.i_area='$area' and b.d_bank<='$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and x.i_area='$area' and b.d_bank<='$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x ",false);
		$query = $this->db->get();
		$kredit=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$kredit=$row->v_bank;
			}
		}
		$this->db->select(" sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and x.i_area='$area' and b.d_bank<='$tanggal' and b.f_debet='f' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and x.i_area='$area' and b.d_bank<='$tanggal' and b.f_debet='f' and x.i_coa='$icoabank' 
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x",false);
		$query = $this->db->get();
		$debet=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$debet=$row->v_bank;
			}
		}
		$saldo=$saldo+$debet-$kredit;
		return $saldo;
  }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		     }      
		    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
		    $dateTimeArr=getdate($dateTime);
		    $nosecmin = 0;
		    $min=$dateTimeArr['minutes'];
		    $sec=$dateTimeArr['seconds'];
		    if ($hr==0){$nosecmin += 1;}
		    if ($min==0){$nosecmin += 1;}
		    if ($sec==0){$nosecmin += 1;}
		    if ($nosecmin>2){     
				return(date("Y-m-d",$dateTime));
			} else {     
				return(date("Y-m-d G:i:s",$dateTime));
			}
	}
	function area($iarea)
    {
		$this->db->select(" e_area_name from tr_area where i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
				$nama=$row->e_area_name;
				return $nama;
			}
		}
    }
	function bacakgroup($cari,$num,$offset)
    {
		$this->db->select(" * from tr_kk_group where upper(i_kk_group) like '%$cari%' or upper(e_kk_groupname) like '%$cari%' order by i_kk_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacapv($ipv,$iarea,$icoabank)
    {
		$this->db->select(" d.i_pvb as i_pv, a.d_pv, b.i_coa, b.e_remark, b.v_pv, c.e_bank_name
		                    from tm_pv a, tm_pv_item b, tr_bank c, tm_pvb d
		                    where a.i_pv=b.i_pv and a.i_area=b.i_area and a.i_area='$iarea' and d.i_pvb='$ipv' 
		                    and a.i_pv_type= b.i_pv_type and a.i_pv_type='02'
		                    and a.i_coa=c.i_coa and a.i_area=d.i_area and a.i_pv_type=d.i_pv_type and a.i_pv=d.i_pv 
  	                    and d.i_coa_bank='$icoabank'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacapvprint($icoabank,$cari,$area,$periode,$num,$offset)
  {
    if($cari=='sikasep'){
	  	$this->db->select(" distinct on (a.i_pv) *, a.v_pv as v_pv, d.i_pvb as i_pv from tm_pv a, tm_pv_item b, tr_bank c, tm_pvb d
	  	                    where a.i_pv=b.i_pv and a.i_area=b.i_area 
	  	                    and a.i_area='$area' and a.i_periode='$periode' and a.i_pv_type='02'
	  	                    and a.i_coa=c.i_coa and a.i_area=d.i_area and a.i_pv_type=d.i_pv_type and a.i_pv=d.i_pv 
	  	                    and d.i_coa_bank='$icoabank'",false)->limit($num,$offset);
    }else{
	  	$this->db->select(" distinct on (a.i_pv) *, a.v_pv as v_pv, d.i_pvb as i_pv from tm_pv a, tm_pv_item b, tr_bank c, tm_pvb d
	  	                    where a.i_pv=b.i_pv and a.i_area=b.i_area 
	  	                    and a.i_area='$area' and a.i_periode='$periode' and a.i_pv_type='02'
	  	                    and (upper(a.i_pv) like '%$cari%')
	  	                    and a.i_coa=c.i_coa and a.i_area=d.i_area and a.i_pv_type=d.i_pv_type and a.i_pv=d.i_pv 
	  	                    and d.i_coa_bank='$icoabank'",false)->limit($num,$offset);
    }
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
###########posting##########
	function inserttransheader(	$inota,$iarea,$eremark,$fclose,$dkn,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi,i_coa_bank)
						  	  values
					  	 ('$inota','$iarea','$dentry','$eremark','$fclose','$dkn','$dkn','$icoabank')");
	}
	function inserttransitemdebet($accdebet,$ikn,$namadebet,$fdebet,$fposting,$iarea,$eremark,$vjumlah,$dkn,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry, i_area,i_coa_bank)
						  	  values
					  	 ('$accdebet','$ikn','$namadebet','$fdebet','$fposting','$vjumlah','$dkn','$dkn','$dentry','$iarea','$icoabank')");
	}
	function inserttransitemkredit($acckredit,$ikn,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dkn,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry, i_area, i_coa_bank)
						  	  values
					  	 ('$acckredit','$ikn','$namakredit','$fdebet','$fposting','$vjumlah','$dkn','$dkn','$dentry','$iarea','$icoabank')");
	}
	function insertgldebet($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry,i_coa_bank)
						  	  values
					  	 ('$ikn','$accdebet','$dkn','$namadebet','$fdebet',$vjumlah,'$iarea','$dkn','$eremark','$dentry','$icoabank')");
	}

	function insertgldebetx($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry,i_coa_bank)
						  	  values
					  	 ('$ikn','$accdebet','$dkn','$namadebet','$fdebet',$vjumlah,'$iarea','$dkn','$eremark','$dentry','$icoabank')");
	}
	function insertglkredit($acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry,i_coa_bank)
						  	  values
					  	 ('$ikn','$acckredit','$dkn','$namakredit','$fdebet','$vjumlah','$iarea','$dkn','$eremark','$dentry','$icoabank')");
	}
	function updatekk($ikn,$iarea,$iperiode)
    {
		$this->db->query("update tm_kk set f_posting='t' where i_kk='$ikn' and i_area='$iarea' and i_periode='$iperiode'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
  }
###########end of posting##########
	function bacabank($num,$offset)
  {
	  $this->db->select("* from tr_bank order by i_bank", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
