<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAll($num, $offset, $cari, $date_from, $date_to) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_bonm) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$this->db->select(" * FROM tm_bonmmasuklain WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasuklain_detail WHERE id_bonmmasuklain = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$id_satuan	= $hasilrow->id_satuan;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg = '';
								$id_satuan = 0;
								$satuan = '';
							}
						}
						else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$id_satuan	= $hasilrow->id_satuan;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg = '';
							$id_satuan = 0;
							$satuan = '';
						}
										
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'id_satuan'=> $id_satuan,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												//'konversi_satuan'=> $row2->konversi_satuan,
												'is_quilting'=> $row2->is_quilting
											);
					}
				}
				else {
					$detail_bonm = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Pengembalian Pinjaman";
				else if ($row1->jenis_masuk == '2')
					$nama_jenis = "Penggantian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'jenis_masuk'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function getAlltanpalimit($cari, $date_from, $date_to){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_bonm) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_bonmmasuklain WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  //
  function save($no_bonm,$tgl_bonm, $no_bonm_manual, $id_gudang, $jenis, $ket, 
			$is_quilting, $kode, $nama, $qty, $stok, $satuan, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");

	if ($is_quilting == '')
		$is_quilting = 'f';
	
    // cek apa udah ada datanya blm dgn no bon M tadi
    $this->db->select("id from tm_bonmmasuklain WHERE no_bonm = '$no_bonm' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_bonm
			$data_header = array(
			  'no_bonm'=>$no_bonm,
			  'no_manual'=>$no_bonm_manual,
			  'tgl_bonm'=>$tgl_bonm,
			  'jenis_masuk'=>$jenis,
			  'id_gudang'=>$id_gudang,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_bonmmasuklain',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_bonmmasuklain
			$query2	= $this->db->query(" SELECT id FROM tm_bonmmasuklain ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_bonm	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				if ($is_quilting == '')
					$is_quilting = 'f';
				
				// cek satuan barangnya, jika yard (qty = meter) maka konversi balik ke yard
			/*	$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$kode' ");
				$hasilrow = $queryxx->row();
				$id_satuan	= $hasilrow->satuan; 
				
				if ($id_satuan == '2') { // jika satuan awalnya yard
					$qty_sat_awal = $qty / 0.90;
					$konv = 't';
				}
				else if ($id_satuan == '7') { // jika satuan awalnya lusin
					$qty_sat_awal = $qty / 12;
					$konv = 't';
				}
				else {
					$qty_sat_awal = $qty;
					$konv = 'f';
				} */
				
				$qty_sat_awal = $qty;
				
				// ======== update stoknya! =============
				if ($is_quilting == 't')
					$nama_tabel_stok = "tm_stok_hasil_makloon";
				else
					$nama_tabel_stok = "tm_stok";
				
				//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty_sat_awal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' ");
					}
										
							//cek stok terakhir tm_stok_harga, ambil harga terbaru aja dan update stoknya
							$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
							if ($is_quilting == 't')
								$sqlxx.= " kode_brg_quilting = '$kode' ";
							else
								$sqlxx.= " kode_brg = '$kode' ";
							$sqlxx.= " AND quilting = '$is_quilting' ";
							$sqlxx.= " ORDER BY id DESC LIMIT 1";
							
						/*	if ($jenis == '1') // pengembalian pinjaman
								$sqlxx.= " ORDER BY id ASC LIMIT 1 ";
							else
								$sqlxx.= " ORDER BY id DESC LIMIT 1 "; // 
						*/
								
							$queryxx	= $this->db->query($sqlxx);
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$id_stok_harga	= $hasilxx->id;
								$stokharganya	= $hasilxx->stok;
								$harga	= $hasilxx->harga;
							
								/*else {
									$id_stok_harga = 0;
									$stokharganya = 0;
									$harga = 0;
								} */
								
								$new_stok = $stokharganya+$qty_sat_awal;
								if ($is_quilting == 'f')
									$fieldkode = 'kode_brg';
								else
									$fieldkode = 'kode_brg_quilting';
									
								if ($queryxx->num_rows() == 0){ // jika blm ada data di tm_stok_harga, insert
									$data_stok = array(
										$fieldkode=>$kode,
										'stok'=>$new_stok,
										'harga'=>$harga,
										'tgl_update_stok'=>$tgl,
										'quilting'=>$is_quilting
										);
									$this->db->insert('tm_stok_harga',$data_stok);
								}
								else {
									if ($is_quilting == 'f')
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
											where kode_brg= '$kode' AND harga = '$harga' ");
									else
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
											where kode_brg_quilting= '$kode' AND harga = '$harga' ");
								}
							}
					// #########################################################################################
				
				// ============================= end 22-12-2012 ============================
				
				// jika semua data tdk kosong, insert ke tm_bonmmasuklain_detail
				$data_detail = array(
					'id_bonmmasuklain'=>$id_bonm,
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					//'konversi_satuan'=>$konv,
					'is_quilting'=>$is_quilting
				);
				$this->db->insert('tm_bonmmasuklain_detail',$data_detail);
			} 
		
  }
    
  function delete($kode){
	$tgl = date("Y-m-d");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasuklain_detail WHERE id_bonmmasuklain = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// cek dulu satuan brgnya. jika satuannya yard, maka qty di detail ini konversikan lagi ke yard
							$query3	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->satuan;
								}
								else {
									$satuan	= '';
								}
								
							/*	if ($satuan == '2') {
									$qty_sat_awal = $row2->qty / 0.90; //meter ke yard 07-03-2012, rubah dari 0.91 ke 0.90
								}
								else if ($satuan == '7') {
									$qty_sat_awal = $row2->qty / 12; // pcs ke lusin
								}
								else */
									$qty_sat_awal = $row2->qty;
								
						
						// ============ update stok =====================
						
						//$nama_tabel_stok = "tm_stok";
						if ($row2->is_quilting == 't')
							$nama_tabel_stok = "tm_stok_hasil_makloon";
						else
							$nama_tabel_stok = "tm_stok";
				
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama-$qty_sat_awal; // berkurang stok karena reset dari bon M masuk
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' ");
								}
								
								// ========== 25-01-2013 ==============
								$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
								if ($row2->is_quilting == 't')
									$sqlxx.= " kode_brg_quilting = '$row2->kode_brg' ";
								else
									$sqlxx.= " kode_brg = '$row2->kode_brg' ";
								$sqlxx.= " AND quilting = '$row2->is_quilting' ORDER BY id DESC LIMIT 1";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilrow = $queryxx->row();
									$stok_lama	= $hasilrow->stok;
									$harganya	= $hasilrow->harga;
								}
								
								$stokreset = $stok_lama-$qty_sat_awal;
								if ($row2->is_quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg= '$row2->kode_brg' AND harga = '$harganya' "); //			
								else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row2->kode_brg' AND harga = '$harganya' "); //
								// ========== end 25-01-2013 ===========					
								
						// ==============================================
					} // end foreach
				} // end if
	
	// 2. hapus data bonmmasuklain
    $this->db->delete('tm_bonmmasuklain_detail', array('id_bonmmasuklain' => $kode));
    $this->db->delete('tm_bonmmasuklain', array('id' => $kode));
  }
          
  function generate_nomor(){
    // generate no Permintaan Bhn ke gudang
			$th_now	= date("Y");
			$query3	= $this->db->query(" SELECT no_pb_cutting FROM tm_pb_cutting ORDER BY no_pb_cutting DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_pb	= $hasilrow->no_pb_cutting;
			else
				$no_pb = '';
			if(strlen($no_pb)==12) {
				$nopb = substr($no_pb, 3, 9);
				$n_pb	= (substr($nopb,4,5))+1;
				$th_pb	= substr($nopb,0,4);
				if($th_now==$th_pb) {
						$jml_n_pb	= $n_pb;
						switch(strlen($jml_n_pb)) {
							case "1": $kodepb	= "0000".$jml_n_pb;
							break;
							case "2": $kodepb	= "000".$jml_n_pb;
							break;	
							case "3": $kodepb	= "00".$jml_n_pb;
							break;
							case "4": $kodepb	= "0".$jml_n_pb;
							break;
							case "5": $kodepb	= $jml_n_pb;
							break;	
						}
						$nomorpb = $th_now.$kodepb;
				}
				else {
					$nomorpb = $th_now."00001";
				}
			}
			else {
				$nomorpb	= $th_now."00001";
			}
			$nomorpb = "PB-".$nomorpb;

			return $nomorpb;  
  }
  
  function get_bahan($num, $offset, $cari, $id_gudang, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			if ($is_quilting == 'f')
				$tabelstok = "tm_stok";
			else
				$tabelstok = "tm_stok_hasil_makloon";
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $id_gudang, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
    
  function get_bonm($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmmasuklain where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasuklain_detail WHERE id_bonmmasuklain = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$id_satuan	= $hasilrow->id_satuan;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->is_quilting == 'f')
							$tabelstok = "tm_stok";
						else
							$tabelstok = "tm_stok_hasil_makloon";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'id_satuan'=> $id_satuan,
												'qty'=> $row2->qty,
												'is_quilting'=> $row2->is_quilting,
												'keterangan'=> $row2->keterangan,
												//'konversi_satuan'=> $row2->konversi_satuan,
												'jum_stok'=> $jum_stok
											);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->jenis_masuk=='1')
					$nama_jenis = "Pengembalian Pinjaman";
				else if ($row1->jenis_masuk=='2')
					$nama_jenis = "Penggantian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_ukuran_bisbisan(){
    $this->db->select("* from tm_ukuran_bisbisan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 09-04-2012
  function get_pb_cutting_for_print($tgl_request) { // to_date('$date_from','dd-mm-yyyy') 
		$query	= $this->db->query(" SELECT a.no_pb_cutting, a.tgl, a.kode_brg_jadi, a.brg_jadi_manual, a.id_motif,
					b.* FROM tm_pb_cutting a, tm_pb_cutting_detail b
					WHERE a.id = b.id_pb_cutting AND a.tgl = to_date('$tgl_request','dd-mm-yyyy')  
					ORDER BY a.id ASC, b.id ASC ");
	
		$data_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row2) {
				
				// 21-07-2012, nama motif brg jadi jika id_motif != 0
				if ($row2->id_motif != '0') {
					$query3	= $this->db->query(" SELECT nama_motif FROM tm_motif_brg_jadi
										WHERE id = '$row2->id_motif' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_motif	= $hasilrow->nama_motif;
					}
					else
						$nama_motif = '';
				}
				else
					$nama_motif = '';

						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->jenis_proses == '3') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_quilting' ");
								
							$hasilrow = $query3->row();
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$satuan_quilting	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
						// jika jenis prosesnya bisbisan
						if ($row2->jenis_proses == 4) {
							// 13 des 2011
							if ($row2->kode_brg_bisbisan != '') {
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
											FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id 
											AND a.kode_brg = '$row2->kode_brg_bisbisan' ");
									
								$hasilrow = $query3->row();
								$nama_brg_bisbisan	= $hasilrow->nama_brg;
								$satuan_bisbisan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg_bisbisan = '';
								$satuan_bisbisan = '';
							}
							
							// ambil data kebutuhan perpcs bisbisan
							$sql = " SELECT id, jum_kebutuhan FROM tm_keb_bisbisan_perpcs 
								WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_brg = '$row2->kode_brg' 
								AND id_ukuran_bisbisan = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_kebutuhan	= $hasilrow->id;
								$jum_kebutuhan	= $hasilrow->jum_kebutuhan;
							}
							else {
								$id_kebutuhan = 0;
								$jum_kebutuhan	= 0;
							}
							
							// ambil variabel2 rumus
							$sql = " SELECT var1, var2, var3 FROM tm_ukuran_bisbisan 
								WHERE id = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$var1	= $hasilrow->var1;
								$var2	= $hasilrow->var2;
								$var3	= $hasilrow->var3;
							}
							else {
								$var1 = 0;
								$var2 = 0;
								$var3 = 0;
							}
						}
						else {
							$id_kebutuhan = 0;
							$jum_kebutuhan	= 0;
							$var1 = 0;
							$var2 = 0;
							$var3 = 0;
							$nama_brg_bisbisan = '';
							$satuan_bisbisan = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
														
				$pisah1 = explode("-", $row2->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row2->kode_brg_jadi != '') {
					$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$row2->kode_brg_jadi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
					}
					else {
						if ($row2->brg_jadi_manual == '')
							$nama_brg_jadi = '';
						else
							$nama_brg_jadi = $row2->brg_jadi_manual;
					}
				}
				else
					$nama_brg_jadi = '';
												
				$data_pb[] = array(			'no_pb_cutting'=> $row2->no_pb_cutting,
											'tgl'=> $tgl,
											'kode_brg_jadi'=> $row2->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											
											'id_detail'=> $row2->id,
											'id_pb_cutting'=> $row2->id_pb_cutting,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'gelaran'=> $row2->gelaran,
												'jum_set'=> $row2->jum_set,
												'jum_gelar'=> $row2->jum_gelar,
												'panjang_kain'=> $row2->panjang_kain,
												'jenis_proses'=> $row2->jenis_proses,
												'id_marker'=> $row2->id_marker_gelaran,
												'kode_brg_quilting'=> $row2->kode_brg_quilting,
												'nama_brg_quilting'=> $nama_brg_quilting,
												'kode_brg_bisbisan'=> $row2->kode_brg_bisbisan,
												'nama_brg_bisbisan'=> $nama_brg_bisbisan,
												'ukuran_bisbisan'=> $row2->id_ukuran_bisbisan,
												//'id_kebutuhanperpcs'=> $row2->id_kebutuhan_perpcs,
												'id_kebutuhan'=> $id_kebutuhan,
												'jum_kebutuhan'=> $jum_kebutuhan,
												'var1'=> $var1,
												'var2'=> $var2,
												'var3'=> $var3,
												'bordir'=> $row2->bordir,
												'for_kekurangan_cutting'=> $row2->for_kekurangan_cutting,
												'for_bs_cutting'=> $row2->for_bs_cutting,
												'bagian_bs'=> $row2->bagian_bs,
												'id_motif'=> $row2->id_motif,
												'nama_motif'=> $nama_motif 
											);
											
			} // endforeach header
		}
		else {
			$data_pb = '';
		}
		return $data_pb;
  }
  
  function cek_data($no_bonm_manual, $thn1){
    $this->db->select("id from tm_bonmmasuklain WHERE no_manual = '$no_bonm_manual' 
				AND extract(year from tgl_bonm) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 16-07-2012
  function get_motif_brg_jadi(){
    $this->db->select("* from tm_motif_brg_jadi order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
