<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $supplier, $cari, $date_from, $date_to, $caribrg, $filterbrg) {	
	/*$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_sj) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_sj DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_sj DESC, id DESC ";
	}
	
	$this->db->select(" * FROM tm_pembelian_aplikasi WHERE status_aktif = 't' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get(); */
	
	$pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelian_aplikasi) like UPPER('%".$this->db->escape_str($cari)."%')  ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
	if ($supplier != '0')
		$pencarian.= " AND a.id_supplier = '$supplier' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')  
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
	$pencarian.= " ORDER BY a.tgl_sj DESC, a.id DESC ";
	
	$this->db->select(" distinct a.* FROM tm_pembelian_aplikasi a LEFT JOIN tm_pembelian_aplikasi_detail b ON a.id=b.id_pembelian_aplikasi 
						LEFT JOIN tm_barang c ON c.id = b.id_brg WHERE  
						a.status_aktif = 't' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	  
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
								OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_barang b ON a.id_brg = b.id
							WHERE a.id_pembelian_aplikasi = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						//----------------
						// 17-06-2015 DIKOMEN
					  /*if ($row1->id_op != '' || $row1->id_pp != '') {
						if ($row1->id_op != '') {
							$list_op = explode(";", $row1->id_op);
							foreach($list_op as $rowop) {
								if ($rowop != '') {
									$rowop = trim($rowop);
																					
									$query3	= $this->db->query(" SELECT id, id_brg FROM tm_op_detail WHERE id_op = '$rowop' ");
									
									$hasil3= $query3->result();
									foreach ($hasil3 as $row3) {
										if ($row2->id_brg == $row3->id_brg)
											$id_detailnya.= $row3->id."-";
									}
								}
							} // end foreach
						}
						else if ($row1->id_pp != '') {
							$list_pp = explode(";", $row1->id_pp);
							foreach($list_pp as $rowpp) {
								if ($rowpp != '') {
									$rowpp = trim($rowpp);

									$query3	= $this->db->query(" SELECT id, kode_brg FROM tm_pp_detail 
												WHERE id_pp = '$rowpp' ");
									
									$hasil3= $query3->result();
									foreach ($hasil3 as $row3) {
										if ($row2->kode_brg == $row3->kode_brg)
											$id_detailnya.= $row3->id."-";
									}
								}
							} // end foreach
						}
						
					  } // end if id op !='' or id_pp != ''
					  */
					  //else
					  
					  // ambil id_op_detail ? ini skip aja 17-06-2015
					  // 18-06-2015, id_detail ini adalah id detail dari OP atau PPnya. ini diset 0 aja dah. diquery ulang di function edit/delete
					  $id_detailnya = "";
					  
						//----------------
						
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg = '';
							//$nama_brg = '';
							//$satuan = '';
						}
						
						// 23-09-2015 nama satuan
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}
						else {
							$satuan = '';
						}
						
						// 23-10-2015
						/*$pos = strpos($row2->nama_brg, "\"");
						  if ($pos > 0)
							$nama_brg= str_replace("\"", "&quot;", $row2->nama_brg);
						  else
							$nama_brg= str_replace("'", "\'", $row2->nama_brg); */
				
						$detail_fb[] = array(	'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $row2->nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$id_detailnya = "";
					$detail_fb = '';
				}
				//print_r($detail_fb); die();
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				
				// 17-06-2015 DIKOMEN
				/*if ($row1->id_op != '' && $row1->id_pp == '') {
					$ambil_pp = 0;
				}
				else if ($row1->id_op == '' && $row1->id_pp != '')
					$ambil_pp = 1;
				
				if ($row1->id_op != '' || $row1->id_pp != '') {
					if ($ambil_pp == 0) {
						$id_opnya = ""; $no_opnya = "";
						$list_op = explode(";", $row1->id_op);
							foreach($list_op as $rowop) {
								if ($rowop != '') {
									$rowop = trim($rowop);
									
									$query3	= $this->db->query(" SELECT no_op FROM tm_op WHERE id = '$rowop' ");
									$hasil3= $query3->result();
									foreach ($hasil3 as $row3) {										
										$no_opnya.= $row3->no_op."; ";
									}
								}
							} // end foreach
						$no_ppnya = "";
					}
					else {
						$id_ppnya = ""; $no_ppnya = "";
						$list_pp = explode(";", $row1->id_pp);
							foreach($list_pp as $rowpp) {
								if ($rowpp != '') {
									$rowpp = trim($rowpp);
									
									$query3	= $this->db->query(" SELECT no_pp FROM tm_pp WHERE id = '$rowpp' ");
									$hasil3= $query3->result();
									foreach ($hasil3 as $row3) {										
										$no_ppnya.= $row3->no_pp."; ";
									}
								}
							} // end foreach
							$no_opnya = "";			
					}
				} // end if
				else { */
					//$no_opnya = '';
					//$no_ppnya = '';
					//$ambil_pp = 0;
				//}
				
				// 18-06-2015, query utk ambil no PP / OP
				$no_opnya = ""; $no_ppnya = "";
				
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_op FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_op_detail b ON a.id_op_detail = b.id 
						INNER JOIN tm_op c ON c.id = b.id_op
						WHERE a.id_pembelian_aplikasi='$row1->id' ORDER BY c.no_op ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$no_opnya.= $rowxx->no_op."<br>";
					}// end for
				}
				else {
					$no_opnya = '';
					$no_ppnya = '';
				}
				
			/*	$sqlxx = " SELECT DISTINCT id_op_detail, id_pp_detail FROM tm_pembelian_aplikasi_detail WHERE id_pembelian_aplikasi='$row1->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$id_op_detail	= $rowxx->id_op_detail;
						$id_pp_detail	= $rowxx->id_pp_detail;
						
						if ($id_op_detail != '0') {
							$sqlxx2= " SELECT distinct a.no_op FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op 
									 WHERE b.id = '$id_op_detail' ";
							$queryxx2	= $this->db->query($sqlxx2);
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$no_opnya.= $hasilxx2->no_op."<br>";
							}
						}
						
						if ($id_pp_detail != '0') {
							$sqlxx2= " SELECT distinct a.no_pp FROM tm_pp a INNER JOIN tm_pp_detail b ON a.id = b.id_pp 
									 WHERE b.id = '$id_pp_detail' ";
							$queryxx2	= $this->db->query($sqlxx2);
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$no_ppnya.= $hasilxx2->no_pp."<br>";
							}
						}
					}// end for
				}
				else {
					$no_opnya = '';
					$no_ppnya = '';
				} */
				
				if ($row1->status_stok == 't')
					$cetakbtb = '1';
				else
					$cetakbtb = '0';
						
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_pp'=> $no_ppnya,	
											'no_op'=> $no_opnya,	
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'no_sj'=> $row1->no_sjmasukpembelian_aplikasi,
											'tgl_sj'=> $row1->tgl_sj,
											'total'=> $row1->total,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'status_stok'=> $row1->status_stok,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											'cetakbtb'=> $cetakbtb
											//'ambil_pp'=> $ambil_pp
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari, $date_from, $date_to, $caribrg, $filterbrg){
	 /* $pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_sj) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_sj DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($supplier != '0')
			$pencarian.= " AND kode_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_sj DESC, id DESC ";
	}
	
	$query = $this->db->query(" SELECT * FROM tm_pembelian_aplikasi WHERE status_aktif = 't' ".$pencarian);
	      
    return $query->result();  */
    $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelian_aplikasi) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
	if ($supplier != '0')
		$pencarian.= " AND a.id_supplier = '$supplier' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sj DESC, a.id DESC ";
	
	$query = $this->db->query(" SELECT distinct a.* FROM tm_pembelian_aplikasi a LEFT JOIN tm_pembelian_aplikasi_detail b ON a.id=b.id_pembelian_aplikasi 
						LEFT JOIN tm_barang c ON c.id = b.id_brg WHERE  
						a.status_aktif = 't' ".$pencarian);
	      
    return $query->result();  
  }
      
  function cek_data($no_sj, $id_supplier){
    $this->db->select("id from tm_pembelian_aplikasi WHERE no_sjmasukpembelian_aplikasi = '$no_sj' AND id_supplier = '$id_supplier' AND status_aktif = 't' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_sj,$tgl_sj,$id_supplier,$id_supplierbaru, $gtotal,$asligtotal,$total_pajak, $dpp, $uang_muka, $sisa_hutang,$ket,
				$hide_pkp, $hide_tipe_pajak, $ambil_pp, $lain_cash, $lain_kredit, $is_no_ppop, $jenis_pembelian, 
				$id_op_detail, $id_op2,
				$id_brg, $nama, $id_satuan, $id_satuan_konversi, $qty, $harga, $harga_lama, $pajak, $diskon, $total, $aslitotal){  
    // parameter $id_op dihilangkan
    // 25-06-2015 $satuan_lain, $qty_sat_lain dihilangkan
    
    $tgl = date("Y-m-d H:i:s");
    
    if ($id_supplier == '0')
		$id_sup_forsave = $id_supplierbaru;
	else
		$id_sup_forsave = $id_supplier;
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pembelian_aplikasi WHERE no_sjmasukpembelian_aplikasi = '$no_sj' AND id_supplier = '$id_sup_forsave' 
					AND status_aktif = 't' ", false);
    $query = $this->db->get();
    //$hasil = $query->result();
    
	// jika data header blm ada 
	//if(count($hasil)== 0) {
	if ($query->num_rows() == 0){
			// insert di tm_pembelian_aplikasi			
			/*if ($faktur_sj == 't') {
				$no_faktur = $no_sj;
				$tgl_faktur = $tgl_sj;
				$status_faktur = 't';
			}
			else { */
				$no_faktur = NULL;
				$tgl_faktur = NULL;
				$status_faktur = 'f';
			//}
			
			//if ($faktur_sj == '')
				$faktur_sj = 'f';
						
			// 18-06-2015 dijadikan 1 aja data_header
			//if ($is_no_ppop == 't' && $ambil_pp == '') {
			
			// 27-08-2015
			$uid_update_by = $this->session->userdata('uid');
			
				$data_header = array(
				  //'id_pp'=>NULL,
				  //'id_op'=>NULL,
				  'no_sjmasukpembelian_aplikasi'=>$no_sj,
				  'tgl_sj'=>$tgl_sj,
				  'no_faktur'=>$no_faktur,
				  'id_supplier'=>$id_sup_forsave,
				  //'total'=>$gtotal,
				  // 06-06-2015, ganti pake asligtotal
				  'total'=>$asligtotal,
				  'uang_muka'=>$uang_muka,
				  'sisa_hutang'=>$sisa_hutang,
				  'keterangan'=>$ket,
				  'pkp'=>$hide_pkp,
				  'tipe_pajak'=>$hide_tipe_pajak,
				  'total_pajak'=>$total_pajak,
				  'dpp'=>$dpp,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'faktur_sj'=>$faktur_sj,
				  'status_faktur'=>$status_faktur,
				  'stok_masuk_lain_cash'=>$lain_cash,
				  'stok_masuk_lain_kredit'=>$lain_kredit,
				  'jenis_pembelian'=>$jenis_pembelian,
				  'uid_update_by'=>$uid_update_by );
			//}
			
			/*if ($ambil_pp == '' && $is_no_ppop == '' ) {
				$data_header = array(
				  'no_sj'=>$no_sj,
				  'tgl_sj'=>$tgl_sj,
				  'no_faktur'=>$no_faktur,
				  'id_op'=>$id_op,
				  'id_supplier'=>$id_supplier,
				  //'total'=>$gtotal,
				  'total'=>$asligtotal,
				  'uang_muka'=>$uang_muka,
				  'sisa_hutang'=>$sisa_hutang,
				  'keterangan'=>$ket,
				  'pkp'=>$hide_pkp,
				  'tipe_pajak'=>$hide_tipe_pajak,
				  'total_pajak'=>$total_pajak,
				  'dpp'=>$dpp,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'faktur_sj'=>$faktur_sj,
				  'status_faktur'=>$status_faktur,
				  'stok_masuk_lain_cash'=>$lain_cash,
				  'stok_masuk_lain_kredit'=>$lain_kredit,
				  'jenis_pembelian'=>$jenis_pembelian
				);
			}
			else if ($ambil_pp == 't' && $is_no_ppop == '') {
				$data_header = array(
				  'no_sj'=>$no_sj,
				  'tgl_sj'=>$tgl_sj,
				  'no_faktur'=>$no_faktur,
				  'id_pp'=>$id_op, // bedanya disini kalo dia ambil dari PP
				  'id_supplier'=>$id_supplier,
				 // 'total'=>$gtotal,
				 'total'=>$asligtotal,
				  'uang_muka'=>$uang_muka,
				  'sisa_hutang'=>$sisa_hutang,
				  'keterangan'=>$ket,
				  'pkp'=>$hide_pkp,
				  'tipe_pajak'=>$hide_tipe_pajak,
				  'total_pajak'=>$total_pajak,
				  'dpp'=>$dpp,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'faktur_sj'=>$faktur_sj,
				  'status_faktur'=>$status_faktur,
				  'stok_masuk_lain_cash'=>$lain_cash,
				  'stok_masuk_lain_kredit'=>$lain_kredit,
				  'jenis_pembelian'=>$jenis_pembelian
				);
			} */
			
		$this->db->insert('tm_pembelian_aplikasi',$data_header);
	}
			
	// 18-06-2015 DIKOMEN, NTAR DIGANTI PAKE QUERY LAIN
	// untuk cek apakah ambil dari OP atau PP atau tidak. kalo ya, maka ambil id_op2 utk update ke tabel op
			if ($is_no_ppop == '') {
				if ($ambil_pp == '') {
					$this->db->query(" UPDATE tm_op SET status_edit = 't' where id= '$id_op2' ");
				}
				else {
					$this->db->query(" UPDATE tm_pp SET status_edit = 't' where id= '$id_op2' ");
				}
			}
			
			/*if ($is_no_ppop == '') {
				if ($ambil_pp == '') {
					$list_op = explode(";", $id_op);
					foreach($list_op as $row1) {
						if ($row1 != '') {
							$row1 = trim($row1);
							$this->db->query(" UPDATE tm_op SET status_edit = 't' where id= '$row1' ");
						}
					}
				}
				else {
					$list_pp = explode(";", $id_op);
					foreach($list_pp as $row1) {
						if ($row1 != '') {
							$row1 = trim($row1);
							$this->db->query(" UPDATE tm_pp SET status_edit = 't' where id= '$row1' ");
						}
					}
				} // end if ambil_pp
			} */
			
			
			// insert ke tabel pembelian_nofaktur jika no_sj = no_faktur
			// 06-06-2015, GA DIPAKE LAGI
		/*	if ($faktur_sj == 't') {
				$this->db->query(" INSERT INTO tm_pembelian_aplikasi_nofaktur(no_faktur, tgl_faktur, tgl_input, 
							tgl_update, kode_supplier, jumlah) VALUES ('$no_faktur', '$tgl_faktur', '$tgl', '$tgl', 
							'$kode_supplier', '$gtotal' ) ");
							
				// ambil id terakhir dari tabel tm_pembelian_aplikasi_nofaktur
				$query2	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_nofaktur ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_faktur	= $hasilrow->id;
				
				// insert ke tabel detail
				$this->db->query(" INSERT INTO tm_pembelian_aplikasi_nofaktur_sj(no_sj, id_pembelian_aplikasi_nofaktur) 
				VALUES ('$no_sj', '$id_faktur') ");
			} */
			
			// ambil data terakhir di tabel tm_pembelian_aplikasi
			$query2	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pembelian_aplikasi	= $hasilrow->id; //echo $idnya; die();
			
			if ($id_brg!='' && $qty!='0' && $harga!='') {
				// jika semua data tdk kosong, insert ke tm_pembelian_aplikasi_detail
				if ($is_no_ppop == 't' && $ambil_pp == '') {
					$data_detail = array(
						'id_brg'=>$id_brg,
						// 29-10-2015 diganti jadi ga pake escape
						//'nama_brg'=>$this->db->escape_str($nama),
						'nama_brg'=>$nama,
						'qty'=>$qty,
						'id_satuan'=>$id_satuan,
						'id_satuan_konversi'=>$id_satuan_konversi,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						//'total'=>$total,
						// 06-06-2015 ganti jadi aslitotal
						'total'=>$aslitotal,
					  'id_pembelian_aplikasi'=>$id_pembelian_aplikasi
					);
				}
				
				if ($ambil_pp == '' && $is_no_ppop == '' ) {
					$data_detail = array(
						'id_brg'=>$id_brg,
						// 29-10-2015 diganti jadi ga pake escape
						//'nama_brg'=>$this->db->escape_str($nama),
						'nama_brg'=>$nama,
						'qty'=>$qty,
						'id_satuan'=>$id_satuan,
						'id_satuan_konversi'=>$id_satuan_konversi,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						//'total'=>$total,
						'total'=>$aslitotal,
						'id_pembelian_aplikasi'=>$id_pembelian_aplikasi,
						'id_op_detail'=>$id_op_detail
					);
					// 'satuan_lain'=>$satuan_lain,'qty_satuan_lain'=>$qty_sat_lain,
				}
				else if ($ambil_pp == 't' && $is_no_ppop == '') {
					$data_detail = array(
						'id_brg'=>$id_brg,
						// 29-10-2015 diganti jadi ga pake escape
						//'nama_brg'=>$this->db->escape_str($nama),
						'nama_brg'=>$nama,
						'qty'=>$qty,
						'id_satuan'=>$id_satuan,
						'id_satuan_konversi'=>$id_satuan_konversi,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						//'total'=>$total,
						'total'=>$aslitotal,
					  'id_pembelian_aplikasi'=>$id_pembelian_aplikasi,
					  'id_pp_detail'=>$id_op_detail
					);
				}
				
				$this->db->insert('tm_pembelian_aplikasi_detail',$data_detail);
				
				// 18-06-2015 AMBIL id_pembelian_aplikasi_DETAIL UNTUK DIPAKE DI APPLY_STOK_PEMBELIAN_DETAIL
				// ambil data terakhir di tabel tm_pembelian_aplikasi
				$queryxx	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail ORDER BY id DESC LIMIT 1 ");
				$hasilxx = $queryxx->row();
				$id_pembelian_aplikasi_detail	= $hasilxx->id;
								
				// 30-09-2015 dikomen aja
				/*if (($harga != $harga_lama) && $id_supplier != '0') {
					$data_harga = array(
						'id_brg'=>$id_brg,
						'id_supplier'=>$id_supplier,
						'harga'=>$harga,
						'tgl_input'=>$tgl
					);
					$this->db->insert('tt_harga', $data_harga);
				}
				
				if ($id_supplier == '0') {
					$data_harga = array(
						'id_brg'=>$id_brg,
						'id_supplier'=>$id_supplierbaru,
						'harga'=>$harga,
						'tgl_input'=>$tgl
					);
					$this->db->insert('tt_harga', $data_harga);
				} */
				
				// sementara id gudang blm diisi dulu, harus nanyain ke org Duta
					$id_gudang = 0;
					$lokasi = "01"; // duta
			
			//==============================================================================================
			// WARNING..!! UPDATE STOK NANTI BUKAN DISINI, TAPI DI APPLY STOK DI BON M MASUK
					
				//2. cek stok terakhir tm_stok, dan update stoknya
				// ini yg lama
				/* $query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' AND jenis_brg = '$jenis_brg' 
										AND id_gudang = '$id_gudang'"); */
				
				// ini yg baru
				/*						
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama+$qty;
			
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode,
						'stok'=>$new_stok,
					//	'id_gudang'=>$id_gudang, //
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode' ");
				}
				
				//3. insert di tt_stok 
				$data_trx_stok = array(
						'kode_brg'=>$kode,
						'no_bukti'=>$no_sj,
						'masuk'=>$qty,
						'saldo'=>$new_stok,
					//	'id_gudang'=>$id_gudang, //
						'tgl_input'=>$tgl,
						'harga'=>$harga
					);
				$this->db->insert('tt_stok',$data_trx_stok);
		// sampe sini yg stok	*/	
		
				//4. update harga di tabel harga_brg_supplier
				if (($harga != $harga_lama) && $id_supplier != '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '$id_brg'
									AND id_supplier = '$id_supplier' AND id_satuan = '$id_satuan' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_supplier (id_brg, id_supplier, harga, id_satuan,
						tgl_input, tgl_update) VALUES ('$id_brg', '$id_supplier', '$harga', '$id_satuan', '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg' AND id_supplier = '$id_supplier' AND id_satuan='$id_satuan' ");
					}
				}
				
				if ($id_supplier == '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '$id_brg'
									AND id_supplier = '$id_supplierbaru' AND id_satuan='$id_satuan' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_supplier (id_brg, id_supplier, harga, id_satuan,
						tgl_input, tgl_update) VALUES ('$id_brg', '$id_supplierbaru', '$harga', '$id_satuan', '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg' AND id_supplier = '$id_supplierbaru' AND id_satuan='$id_satuan' ");
					}
				}
			}
			
			// ---------------------------
			if ($is_no_ppop == '') {
				if ($ambil_pp == '') {
					// cek sum qty di pembelian. jika sum di pembelian = qty OP, maka update status OPnya menjadi true
					/*$query3	= $this->db->query(" SELECT a.qty FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
								WHERE b.id = '$id_op2' AND a.id_brg = '$id_brg' "); */
					$query3	= $this->db->query(" SELECT a.qty FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
								WHERE a.id = '$id_op_detail' ");
								
					$hasilrow = $query3->row();
					$qty_op = $hasilrow->qty; // ini qty di OP detail
					
					// 19-12-2011, cek
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_op_detail = '".$id_op_detail."' ");
					if ($query4->num_rows() > 0){
						// 25-06-2015 SATUAN LAIN GA DIPAKE LG
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_pembelian_aplikasi b ON a.id_pembelian_aplikasi = b.id
									WHERE a.id_op_detail = '$id_op_detail' AND b.status_aktif = 't' ");
							// 04-07-2015 dibuang dari query: AND b.id_supplier = '$id_supplier'
						
						$hasilrow = $query3->row();
						$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
					}
					else {
						$jum_beli = 0;
					}
					
					//if ($qty_op == $jum_beli) {
					if ($jum_beli >= $qty_op) {
						$this->db->query(" UPDATE tm_op_detail SET status_op = 't' where id= '$id_op_detail' ");
						//cek di tabel tm_op_detail, apakah status_op sudah 't' semua?
						$this->db->select("id from tm_op_detail WHERE status_op = 'f' AND id_op = '$id_op2' ", false);
						$query = $this->db->get();
						//jika sudah t semua, maka update tabel tm_op di field status_op menjadi t
						if ($query->num_rows() == 0){
							$this->db->query(" UPDATE tm_op SET status_op = 't' where id= '$id_op2' ");
						}
					}
				} // end jika dari OP
				// 04-07-2015 GA DIPAKE LAGI UTK ACUAN PP, TAPI BIARKAN AJA SKRIPNYA, GA AKAN MASUK KESINI
				// 20-08-2015 DIKOMENN
				/*else {
					// cek sum qty di pembelian. jika sum di pembelian = qty PP, maka update status PPnya menjadi true
					$query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a INNER JOIN tm_pp b ON a.id_pp = b.id
								WHERE a.id = '$id_op_detail' ");
					$hasilrow = $query3->row();
					$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
					
					// ini sum OP berdasarkan kode brg tsb (140511)
						$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
							WHERE b.id_pp = '$id_op2' AND a.id_brg = '$id_brg' ");
						$hasilrow = $query3->row();
						$jum_op = $hasilrow->jum;

					// ##################################################
					// 19-12-2011, cek
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_pp_detail = '".$id_op_detail."' ");
					if ($query4->num_rows() > 0){
						// 25-06-2015 SATUAN LAIN GA DIPAKE LG						
						$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_pembelian_aplikasi b ON a.id_pembelian_aplikasi = b.id
								WHERE a.id_pp_detail = '$id_op_detail' AND b.status_aktif = 't' ");
						
						$hasilrow = $query3->row();
						$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
					}
					else {
						$jum_beli = 0;
					}
					
					if ($jum_beli >= $qty_pp-$jum_op) {
						$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 't' where id= '$id_op_detail' ");
						//cek di tabel tm_pp_detail, apakah status_faktur sudah 't' semua?
						$this->db->select("id from tm_pp_detail WHERE status_faktur = 'f' AND id_pp = '$id_op2' ", false);
						$query = $this->db->get();
						//jika sudah t semua, maka update tabel tm_pp di field status_faktur menjadi t
						if ($query->num_rows() == 0){
							$this->db->query(" UPDATE tm_pp SET status_faktur = 't' where id= '$id_op2' ");
						}
					}
				} */
			} // end is_no_ppop
			//----------------------------
			
			//update pkp dan tipe_pajak di tabel supplier
			$this->db->query(" UPDATE tm_supplier SET pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak' where id= '$id_sup_forsave' ");
			
			// 18-06-2015, save ke apply_stok digabung kesini
			$th_now	= date("Y");
	
		// ====================================================
		

			// ====================================================
  }
    
  function delete($id){    
	  $tgl = date("Y-m-d H:i:s");
	  
	  // ================================== NEW 18-06-2015 =================================
	  // reset status OP jika dia dari acuan OP/PP
	  // $this->db->query(" UPDATE tm_op SET status_edit = 'f', status_op = 'f' where id= '$rowop' ");
	  // $this->db->query(" UPDATE tm_pp SET status_edit = 'f', status_faktur = 'f' where id= '$rowpp' ");
	  
	    $sqlxx = " SELECT DISTINCT id_op_detail, id_pp_detail FROM tm_pembelian_aplikasi_detail WHERE id_pembelian_aplikasi='$id' ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			//$hasilxx = $queryxx->row();
			$hasilxx=$queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$id_op_detail	= $rowxx->id_op_detail;
				$id_pp_detail	= $rowxx->id_pp_detail;
				
				if ($id_op_detail != '0') {
					$sqlxx2= " SELECT distinct a.id FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op 
							 WHERE b.id = '$id_op_detail' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_op	= $hasilxx2->id;
						$this->db->query(" UPDATE tm_op SET status_edit = 'f', status_op = 'f' where id= '$id_op' ");
					}
					// reset status di detailnya dari OP pake perulangan
					$this->db->query(" UPDATE tm_op_detail SET status_op = 'f' where id= '$id_op_detail' ");
				}
						
				if ($id_pp_detail != '0') {
					$sqlxx2= " SELECT distinct a.id FROM tm_pp a INNER JOIN tm_pp_detail b ON a.id = b.id_pp 
							 WHERE b.id = '$id_pp_detail' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_pp	= $hasilxx2->id;
						$this->db->query(" UPDATE tm_pp SET status_edit = 'f', status_faktur = 'f' where id= '$id_pp' ");
					}
					// reset status di detailnya dari PP pake perulangan
					$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 'f' where id= '$id_pp_detail' ");
				}
				
			} // end for
		}
	  
	  // ====================================================================================
		
		// batalkan di tabel tm_apply_stok_pembelian
		//1. ambil no_sj dan kode_supplier
		$query4	= $this->db->query(" SELECT no_sjmasukpembelian_aplikasi, id_supplier FROM tm_pembelian_aplikasi where id = '$id' ");
		$hasilrow = $query4->row();
		$no_sj	= $hasilrow->no_sjmasukpembelian_aplikasi;
		$id_supplier	= $hasilrow->id_supplier;
				
		//2. ambil id tm_apply_stok_pembelian
		$query4	= $this->db->query(" SELECT id, status_stok, no_bonm from tm_apply_stok_pembelian 
							where no_sj = '$no_sj' AND id_supplier = '$id_supplier' AND status_aktif = 't' ");
		if ($query4->num_rows() > 0){
			$hasil4=$query4->result();
			foreach ($hasil4 as $row4) {
				// ============================================
				
				// ambil id detail.......
				$query5	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail 
									where id_apply_stok = '$row4->id' AND status_stok = 't' ");
				if ($query5->num_rows() > 0){
					$hasil5=$query5->result();
					foreach ($hasil5 as $row5) {
					// edit transaksi stoknya
									
					$status_stok_header = $row4->status_stok;
					$status_stok_detail = $row5->status_stok;
											
					if ($status_stok_header == 't' || $status_stok_detail == 't' ) {
												
						//2. ambil stok terkini di tm_stok
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg='$row5->id_brg' ");
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
						$stokreset = $stok_lama-$row5->qty;
												
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE id_brg='$row5->id_brg' 
												AND harga='$row5->harga' ");
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
						$stokreset2 = $stok_lama-$row5->qty;
												
						//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
						// 18-06-2015, MASIH BINGUNG APAKAH TT_STOK INI DIPAKE ATAU GA						
						// 24-07-2015 GA DIPAKE
						/*$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_supplier, ".$stok_keluar.", saldo, tgl_input, harga)
											VALUES ('$row5->kode_brg', '$row4->no_bonm', '$kode_supplier', '$row5->qty', '$stokreset2', '$tgl', '$row5->harga' ) ");
						*/
													
						//4. update stok di tm_stok
						$this->db->query(" UPDATE tm_stok SET stok = '$stokreset', tgl_update_stok = '$tgl'
											where id_brg= '$row5->id_brg'  ");
													
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl'
											where id_brg= '$row5->id_brg' AND harga='$row5->harga'  ");
											
						$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET status_stok = 'f' 
										WHERE id = '$row5->id' ");
					} // end if status_stoknya = 't'
				} // end foreach
										
			} // end if query5
									
				// ============================================
			}
		}
		
		//4. batalkan tm_apply_stok_pembelian
		$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_aktif = 'f', status_stok = 'f' 
						WHERE no_sj = '$no_sj' AND id_supplier = '$id_supplier' ");
			
		$this->db->query(" UPDATE tm_pembelian_aplikasi SET status_aktif = 'f', status_stok = 'f' WHERE id = '$id' ");
		$this->db->query(" UPDATE tm_pembelian_aplikasi_detail SET status_stok = 'f' WHERE id_pembelian_aplikasi = '$id' ");
	//}
  }
    
  //function get_op($num, $offset, $supplier, $cek_pp, $cari) {
  function get_op($supplier, $cek_pp, $jenis_pembelian, $cari) {
	  // ambil data OP yg status_op = FALSE
	
	if ($cari == "all") {
		/*if ($supplier == '0') {
			if ($cek_pp == 0) {
				$sql = " * FROM tm_op WHERE status_op = 'f' AND status_aktif = 't' AND jenis_pembelian = '$jenis_pembelian' 
						order by tgl_op DESC, no_op DESC ";
			}
			else
				$sql = " * FROM tm_pp WHERE status_faktur = 'f' order by tgl_pp DESC, no_pp DESC ";
			
			//$this->db->select($sql, false)->limit($num,$offset);
			$this->db->select($sql, false);
			$query = $this->db->get();
		} 
		else { */
			if ($cek_pp == 0) {
				$sql = " distinct a.* FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op 
						WHERE a.status_op = 'f' AND a.status_aktif = 't' AND a.id_supplier = '$supplier' 
						AND b.status_op='f'
						AND a.jenis_pembelian = '$jenis_pembelian' order by a.tgl_op DESC, a.no_op DESC ";
			}
			else
				$sql = " * FROM tm_pp WHERE status_faktur = 'f' order by tgl_pp DESC, no_pp DESC ";
				
			//$this->db->select($sql, false)->limit($num,$offset);
			$this->db->select($sql, false);
			$query = $this->db->get();
		//}
	}
	else {
		/*if ($supplier == '0') {
			if ($cek_pp == 0)
				$sql = " * FROM tm_op WHERE status_op = 'f' AND status_aktif = 't' AND jenis_pembelian = '$jenis_pembelian'
						AND UPPER(no_op) like UPPER('%$cari%') order by tgl_op DESC, no_op DESC ";
			else
				$sql = " * FROM tm_pp WHERE status_faktur = 'f' AND UPPER(no_pp) like UPPER('%$cari%') order by tgl_pp DESC, no_pp DESC ";
			
			//$this->db->select($sql, false)->limit($num,$offset);
			$this->db->select($sql, false);
			$query = $this->db->get();
		}
		else { */
			if ($cek_pp == 0) {
				// modif 17-12-2015
				/*$sql = " * FROM tm_op WHERE status_op = 'f' AND status_aktif = 't' AND jenis_pembelian = '$jenis_pembelian' 
							AND id_supplier = '$supplier' AND UPPER(no_op) like UPPER('%$cari%') order by tgl_op DESC, no_op DESC "; */
				$sql = " distinct a.* FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op 
							INNER JOIN tm_barang c ON b.id_brg = c.id
							WHERE a.status_op = 'f' AND a.status_aktif = 't' 
							AND a.jenis_pembelian = '$jenis_pembelian' AND a.id_supplier = '$supplier' 
							AND b.status_op='f'
							AND (UPPER(c.kode_brg) like UPPER('%$cari%') OR UPPER(c.nama_brg) like UPPER('%$cari%')) 
							order by a.tgl_op DESC, a.no_op DESC ";
			}
			else
				$sql = " * FROM tm_pp WHERE status_faktur = 'f' AND UPPER(no_pp) like UPPER('%$cari%') order by tgl_pp DESC, no_pp DESC ";
				
			//$this->db->select($sql, false)->limit($num,$offset);
			$this->db->select($sql, false);
			$query = $this->db->get();
		//}
	}
	//echo $sql."<br>";
		$data_op = array();
		$detail_op = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				if ($cek_pp == 0) {
					// modif 17-12-2015
					//$sql2 = "SELECT * FROM tm_op_detail WHERE id_op = '$row1->id' AND status_op = 'f' ORDER BY id ASC ";
					$filterbrg = "";
					if ($cari != "all")
						$filterbrg = " AND (UPPER(c.kode_brg) like UPPER('%$cari%') OR UPPER(c.nama_brg) like UPPER('%$cari%')) ";
					
					$sql2 = "SELECT b.* FROM tm_op_detail b INNER JOIN tm_op a ON b.id_op = a.id 
							INNER JOIN tm_barang c ON c.id = b.id_brg
							WHERE b.id_op = '$row1->id' AND b.status_op = 'f' ".$filterbrg." ORDER BY b.id ASC ";
				}
				else
					$sql2 = "SELECT * FROM tm_pp_detail WHERE id_pp = '$row1->id' AND status_faktur = 'f' ORDER BY id ASC ";
				
				//echo $sql2; echo "<br>";
				
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
										INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
				
						// -------------------
						
						/* $query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a, tm_pp b 
									WHERE b.id = '$id_pp' AND a.id_pp = b.id AND a.kode_brg = '$kode' ");
						$hasilrow = $query3->row();
						$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
						*/
						
						// ini sum OP berdasarkan kode brg tsb (140511)
						// 04-07-2015 GA DIPAKE LAGI
						//if ($cek_pp != 0) {
							/*$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
								WHERE a.id_op = b.id AND b.id_pp = '$row1->id' AND a.kode_brg = '$row2->kode_brg' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; */
							
							//17-06-2015 DIGANTI
						/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
								on a.id_op = b.id WHERE a.id_pp_detail = '$row2->id' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum;
						}
						else
							$jum_op = 0; */
						
						// ##################################################
						
						// 19-12-2011, cek. 17-06-2015 dimodif
						if ($cek_pp == 0) {
							$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_op_detail = '".$row2->id."' ");
							if ($query4->num_rows() > 0){
								// 02-07-2015 SATUAN LAIN GA DIPAKE
								//if ($supplier != '0') {
									$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a 
											INNER JOIN tm_pembelian_aplikasi b ON a.id_pembelian_aplikasi = b.id WHERE b.status_aktif = 't' ";
									// 17 des 2011
									$sql3.= " AND a.id_op_detail = '$row2->id' AND a.id_brg = '$row2->id_brg'
												 ";
									// 04-07-2015 DIBUANG dari query diatas: AND b.id_supplier = '$supplier'
									
									$query3	= $this->db->query($sql3);
									$hasilrow = $query3->row();
									$jum_ppop = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan OP detail tsb
								//}
								//else
								//	$jum_ppop = 0;
							}
							else {
								$jum_ppop = 0;
							}
						}
						else {
							// 15-08-2015 DIKOMEN, GA DIPAKE LG
							
							/*$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_pp_detail = '".$row2->id."' ");
							if ($query4->num_rows() > 0){
								// 02-07-2015 SATUAN LAIN GA DIPAKE
								$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_pembelian_aplikasi b ON a.id_pembelian_aplikasi = b.id 
										WHERE b.status_aktif = 't' AND a.id_pp_detail = '$row2->id' AND a.id_brg = '$row2->id_brg' ";
								$query3	= $this->db->query($sql3);
								$hasilrow = $query3->row();
								$jum_ppop = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan OP detail tsb
							}
							else
								$jum_ppop = 0;
							*/
							$jum_ppop = 0;
						}
						
						//$qty = $row2->qty-$jum_ppop-$jum_op;
						// 04-07-2015
						//echo $row2->qty." ".$jum_ppop."<br>";
						$qty = $row2->qty-$jum_ppop;
						
						// 26-09-2015
						//if ($kode_brg == 'KAI0027')
						//	echo $row2->qty." ".$jum_ppop." ".$qty."<br>";
						
						if ($qty <= 0) {
							$this->db->query(" UPDATE tm_op_detail SET status_op = 't' WHERE id='".$row2->id."' ");
							
							// 13-08-2015, cek apakah sudah t semua. jika sudah, maka update headernya juga
							/*$queryxx	= $this->db->query(" SELECT id_op FROM tm_op_detail WHERE id = '".$row2->id."' ");
							$hasilxx = $queryxx->row();
							$id_op	= $hasilxx->id_op; */
							
							$queryxx	= $this->db->query(" SELECT id FROM tm_op_detail WHERE id_op = '".$row1->id."' AND status_op = 'f' ");
							if ($queryxx->num_rows() == 0){
								$this->db->query(" UPDATE tm_op SET status_op = 't' WHERE id='".$row1->id."' ");
							}
						}
						//--------------------
						
						// 02-07-2015 SATUAN LAIN GA DIPAKE
						/*if ($row2->is_satuan_lain == 't') {
							$query2	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '".$row2->satuan_lain."' ");
							$hasilrow = $query2->row();
							$nama_satuan_lain	= $hasilrow->nama;
						}
						else */
							$nama_satuan_lain = '';
						
						// 29-07-2015
						if ($qty > 0) {
							$detail_op[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan
												// 02-07-2015 SATUAN LAIN GA DIPAKE
												//'is_satuan_lain'=> $row2->is_satuan_lain,
												//'satuan_lain'=> $row2->satuan_lain,
												//'nama_satuan_lain'=> $nama_satuan_lain
											);
						}
					}
				}
				else {
					$detail_op = '';
				}
				
				if ($cek_pp == 0) {
					if ($row1->id_supplier != '0') {
						$query3	= $this->db->query(" SELECT kode_supplier, nama from tm_supplier where id = '$row1->id_supplier' ");
						$hasilrow = $query3->row();
						$kode_supplier	= $hasilrow->kode_supplier;
						$nama_supplier	= $hasilrow->nama;
					}
					else {
						$kode_supplier	= '';
						$nama_supplier	= 'Lain-lain';
					}
				}
				else {
					$kode_supplier = "";
					$nama_supplier = "";
				}
				
				if ($cek_pp == 0) {
					$no_ppop = $row1->no_op;
					$tgl_ppop = $row1->tgl_op;
				}
				else {
					$no_ppop = $row1->no_pp;
					$tgl_ppop = $row1->tgl_pp;
				}
				
				$data_op[] = array(			'id'=> $row1->id,	
											'no_op'=> $no_ppop,
											'tgl_op'=> $tgl_ppop,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'detail_op'=> $detail_op
											);
				$detail_op = array();
			} // endforeach header
		}
		else {
			$data_op = '';
		}
		return $data_op;
  }
  
  function get_optanpalimit($supplier, $cek_pp, $jenis_pembelian, $cari){
	if ($cari == "all") {
		/*if ($supplier == '0') {
			if ($cek_pp == 0) {
				//$query = $this->db->getwhere('tm_op',array('status_op'=>"f"));
				$query = $this->db->query(" SELECT * FROM tm_op WHERE status_op = 'f' AND status_aktif = 't' AND jenis_pembelian = '$jenis_pembelian'  ");
			}
			else {
				$query = $this->db->query(" SELECT * FROM tm_pp WHERE status_faktur = 'f' ");
			}
		}
		else { */
			if ($cek_pp == 0) {
				$query	= $this->db->query(" SELECT * FROM tm_op WHERE status_op = 'f' AND status_aktif = 't' 
							AND jenis_pembelian = '$jenis_pembelian'  AND id_supplier = '$supplier' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pp WHERE status_faktur = 'f' ");
			}
		//}
	}
	else {
		/*if ($supplier == '0') {
			if ($cek_pp == 0) {
				$query	= $this->db->query(" SELECT * FROM tm_op WHERE status_op = 'f' AND status_aktif = 't' 
							AND jenis_pembelian = '$jenis_pembelian'  AND UPPER(no_op) like UPPER('%$cari%') ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pp WHERE status_faktur = 'f' 
								AND UPPER(no_pp) like UPPER('%$cari%') ");
			}
		}
		else { */
			if ($cek_pp == 0) {
				$query	= $this->db->query(" SELECT * FROM tm_op WHERE status_op = 'f' AND status_aktif = 't' AND id_supplier = '$supplier' 
								AND jenis_pembelian = '$jenis_pembelian'  AND UPPER(no_op) like UPPER('%$cari%') ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pp WHERE status_faktur = 'f' 
								AND UPPER(no_pp) like UPPER('%$cari%') ");
			}
		//}
	}
    
    return $query->result();  
  }
  
  function get_detail_op($list_brg, $id_supplier, $ambil_pp){
    $detail_pp = array();
   // $no_ppop = ""; $id_ppop = ""; $temp_no_ppop = ""; $temp_id_ppop = "";
    
    foreach($list_brg as $row1) {
			//$row1 = intval($row1);
			if ($row1 != '') {
				if ($ambil_pp == '') {
					$query2	= $this->db->query(" SELECT a.no_op, b.id_op, b.id_brg, b.qty, b.harga, b.diskon, 
											b.is_satuan_lain, b.satuan_lain  
											FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op
											WHERE b.id = '$row1' ");
					$hasilrow = $query2->row();
					$id_op	= $hasilrow->id_op;
					$id_brg	= $hasilrow->id_brg;
					$qty	= $hasilrow->qty;
					$harga	= $hasilrow->harga;
					$diskon	= $hasilrow->diskon;
					$no_op	= $hasilrow->no_op;
					$is_satuan_lain	= $hasilrow->is_satuan_lain;
					$satuan_lain	= $hasilrow->satuan_lain;
					
					/*if ($no_op != $temp_no_ppop)
						$no_ppop.= $no_op.";";
					
					if ($id_op != $temp_id_ppop)
						$id_ppop.= $id_op.";";
						
					$temp_no_ppop = $no_op;
					$temp_id_ppop = $id_op; */
				}
				else {
					$query2	= $this->db->query(" SELECT a.no_pp, b.id_pp, b.id_brg, b.qty, b.is_satuan_lain, b.satuan_lain 
											FROM tm_pp a INNER JOIN tm_pp_detail b ON a.id = b.id_pp
											WHERE b.id = '$row1' ");
					$hasilrow = $query2->row();
					$id_op	= $hasilrow->id_pp;
					$id_brg	= $hasilrow->id_brg;
					$qty	= $hasilrow->qty;
					$diskon = 0;
					$no_op	= $hasilrow->no_pp;
					$is_satuan_lain	= $hasilrow->is_satuan_lain;
					$satuan_lain	= $hasilrow->satuan_lain;
					
					/*if ($no_pp != $temp_no_ppop)
						$no_ppop.= $no_pp.";";
					
					if ($id_op != $temp_id_ppop)
						$id_ppop.= $id_op.";";
					
					$temp_no_ppop = $no_pp;
					$temp_id_ppop = $id_op; */
				}
								
				$query2	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.satuan, b.nama as nama_satuan, e.kode as kode_kel, 
							a.id_satuan_konversi
							FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
							INNER JOIN tm_jenis_barang d ON a.id_jenis_barang = d.id
							INNER JOIN tm_kelompok_barang e ON d.kode_kel_brg = e.kode
							WHERE a.id = '$id_brg' ");
				$hasilrow = $query2->row();
				$kode_brg	= $hasilrow->kode_brg;
				$nama_brg	= $hasilrow->nama_brg;
				// 21-09-2015
				$id_satuan	= $hasilrow->satuan;
				$nama_satuan	= $hasilrow->nama_satuan;
				$kode_kel	= $hasilrow->kode_kel;
				$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
				
				if ($id_supplier != '0') {
					$query2	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '$id_brg'
										AND id_supplier = '$id_supplier' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$hargamaster	= $hasilrow->harga;
					}
					else
						$hargamaster = 0;
				}
				else {
					$hargamaster	= $harga;
				}
				
				/*if ($ambil_pp == '') {
					// ambil qty op_detail
					$query3	= $this->db->query(" SELECT qty FROM tm_op_detail WHERE id_op = '".$id_op."'
					AND kode_brg = '$kode_brg' ");
				}
				else {
					// ambil qty pp_detail
					$query3	= $this->db->query(" SELECT qty FROM tm_pp_detail WHERE id_pp = '".$id_op."'
					AND kode_brg = '$kode_brg' ");
				}
				
				if($query3->num_rows()>0) {
					$hasilrow = $query3->row();
					$qty_op = $hasilrow->qty;
				}
				else
					$qty_op = 0; */
				$qty_op = $qty; //
				
				if ($ambil_pp == '') {
					// 19-12-2011
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_op_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
						// ambil sum qty pembelian detail
						// 25-06-2015 SATUAN LAIN GA DIPAKE
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_pembelian_aplikasi b ON a.id_pembelian_aplikasi = b.id
										WHERE a.id_op_detail = '$row1' AND b.status_aktif = 't' 
										AND a.id_brg = '".$id_brg."' ");
							// 04-07-2015 DIBUANG DARI QUERY: AND b.id_supplier = '$id_supplier'
						
						if($query3->num_rows()>0) {
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum;
						}
						else
							$jum_beli = 0;
					}
					else {
						$jum_beli = 0;
					}
				}
				// 04-07-2015 AMBIL DARI PP UDH GA DIPAKE, TAPI BIARIN AJA, GA AKAN MASUK KE SKRIP INI
				else { // jika dari PP
					// ambil sum qty pembelian detail utk yg dari PP
					// 19-12-2011, cek apakah ada id_pp_detail
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_pp_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
						// 25-06-2015 SATUAN LAIN GA DIPAKE
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_pembelian_aplikasi b ON a.id_pembelian_aplikasi = b.id
										WHERE a.id_pp_detail = '$row1' AND b.status_aktif = 't'
										AND a.id_brg = '".$id_brg."' ");						
						if($query3->num_rows()>0) {
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum;
						}
						else
							$jum_beli = 0;
					}
					else {
						$jum_beli = 0;
					}
				}
																
				/*if ($ambil_pp == '') {
					// 19-12-2011, cek apakah ada id_pp_detail
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_op_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
						if ($is_satuan_lain == 'f')							
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b
								WHERE a.id_pembelian_aplikasi = b.id AND a.id_op_detail = '$row1' 
								AND b.status_aktif = 't'
								AND a.kode_brg = '$kode_brg' AND b.kode_supplier = '$kode_supplier' ");
						else
							$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b
								WHERE a.id_pembelian_aplikasi = b.id AND a.id_op_detail = '$row1' 
								AND b.status_aktif = 't' 
								AND a.kode_brg = '$kode_brg' AND b.kode_supplier = '$kode_supplier' ");
					}
					else {
						if ($is_satuan_lain == 'f')
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b,
								tm_op c, tm_op_detail d
								WHERE a.id_pembelian_aplikasi = b.id AND b.id_op like '%$id_op%' 
								AND b.status_aktif = 't' AND c.id = d.id_op
								AND c.id = '$id_op'
								AND d.kode_brg = '$kode_brg' AND a.kode_brg = d.kode_brg AND b.kode_supplier = '$kode_supplier' ");
						else
							$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b,
								tm_op c, tm_op_detail d
								WHERE a.id_pembelian_aplikasi = b.id AND b.id_op like '%$id_op%' 
								AND b.status_aktif = 't' AND c.id = d.id_op
								AND c.id = '$id_op'
								AND d.kode_brg = '$kode_brg' AND a.kode_brg = d.kode_brg AND b.kode_supplier = '$kode_supplier' ");
					}
				}
				else { // ambil dari PP
					// 19-12-2011, cek apakah ada id_pp_detail
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_aplikasi_detail WHERE id_pp_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
						if ($is_satuan_lain == 'f')
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b
								WHERE a.id_pembelian_aplikasi = b.id AND a.id_pp_detail = '$row1' 
								AND b.status_aktif = 't'
								AND a.kode_brg = '$kode_brg' ");
						else
							$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b
								WHERE a.id_pembelian_aplikasi = b.id AND a.id_pp_detail = '$row1' 
								AND b.status_aktif = 't' 
								AND a.kode_brg = '$kode_brg' ");
					}
					else {
						if ($is_satuan_lain == 'f')
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b,
								tm_pp c, tm_pp_detail d
								WHERE a.id_pembelian_aplikasi = b.id AND b.id_pp like '%$id_op%' 
								AND b.status_aktif = 't' AND c.id = d.id_pp AND c.id = '$id_op'
								AND a.kode_brg = '$kode_brg' AND a.kode_brg = d.kode_brg ");
						else
							$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_aplikasi_detail a, tm_pembelian_aplikasi b,
								tm_pp c, tm_pp_detail d
								WHERE a.id_pembelian_aplikasi = b.id AND b.id_pp like '%$id_op%' 
								AND b.status_aktif = 't' AND c.id = d.id_pp AND c.id = '$id_op'
								AND a.kode_brg = '$kode_brg' AND a.kode_brg = d.kode_brg ");
					}
				}
							
				$hasilrow = $query3->row();
				$jum_op = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
				*/
				
				// 15-06-2015, PROSES DIATAS KOK 2 KALI ? ANEH. PADAHAL SAMA
				$jum_op = $jum_beli;
				
					// ini sum OP berdasarkan kode brg tsb jika PP (140511)
					if ($ambil_pp != '') {
						/*$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
							ON a.id_op = b.id WHERE b.id_pp = '$id_op' AND a.kode_brg = '$kode_brg' ");
						$hasilrow = $query3->row();
						$jum_opbeneran = $hasilrow->jum; */
						
						//17-06-2015 DIGANTI
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
								on a.id_op = b.id WHERE a.id_pp_detail = '$row1' ");
							$hasilrow = $query3->row();
							$jum_opbeneran = $hasilrow->jum;
					}
					else
						$jum_opbeneran = 0;
						// ##################################################
							
				//$qty = $qty-$jum_op-$jum_opbeneran;
				// 04-07-2015
				$qty = $qty-$jum_op;
				// 26-09-2015
				$qty = number_format($qty, 4, '.','');

					if ($is_satuan_lain == 't') {
						$query2	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$satuan_lain' ");
						$hasilrow = $query2->row();
						$nama_satuan_lain	= $hasilrow->nama;
					}
					else
						$nama_satuan_lain = '';
					
					$detail_op[] = array(	'id'=> $row1,
											'id_brg'=> $id_brg,
											'kode_brg'=> $kode_brg,
											// 29-10-2015 diganti ga pake escape
											//'nama'=> $this->db->escape_str($nama_brg),
											'nama'=> $nama_brg,
											'nama_satuan'=> $nama_satuan,
											'id_satuan'=> $id_satuan,
											'kode_kel'=> $kode_kel,
											'id_satuan_konversi'=> $id_satuan_konversi,
											'qty'=> $qty,
											'harga'=> $hargamaster,
											'diskon'=> $diskon,
											'qty_op'=> $qty_op,
											'jum_beli'=> $jum_beli,
											'no_op'=> $no_op,
											'id_op'=> $id_op,
											'is_satuan_lain'=> $is_satuan_lain,
											'satuan_lain'=> $satuan_lain,
											'nama_satuan_lain'=> $nama_satuan_lain
									);
				
				
		}
	}
	return $detail_op;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_pkp_tipe_pajak_bykodesup($id_sup){
	$query	= $this->db->query(" SELECT pkp, tipe_pajak, top FROM tm_supplier where id = '$id_sup' ");    
    return $query->result();  
  }
  
  function get_pembelian($id_pembelian_aplikasi) {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian_aplikasi where id = '$id_pembelian_aplikasi' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				
				// 18-06-2015, query utk ambil no PP / OP
				
				// 10-07-2015 DIMODIF
				$no_opnya = ""; $no_ppnya = "";
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_op, c.tgl_op FROM tm_pembelian_aplikasi_detail a INNER JOIN tm_op_detail b ON a.id_op_detail = b.id 
						INNER JOIN tm_op c ON c.id = b.id_op
						WHERE a.id_pembelian_aplikasi='$row1->id' ORDER BY c.no_op ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$pisah1 = explode("-", $rowxx->tgl_op);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_op = $tgl1."-".$bln1."-".$thn1;
						$no_opnya.= $rowxx->no_op." (".$tgl_op.")"."<br>";
					}// end for
				}
				else {
					$no_opnya = '-';
					$no_ppnya = '';
				}
				
				/*$sqlxx = " SELECT DISTINCT id_op_detail, id_pp_detail FROM tm_pembelian_aplikasi_detail WHERE id_pembelian_aplikasi='$row1->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$id_op_detail	= $rowxx->id_op_detail;
						$id_pp_detail	= $rowxx->id_pp_detail;
						
						if ($id_op_detail != '0') {
							$sqlxx2= " SELECT distinct a.no_op FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op 
									 WHERE b.id = '$id_op_detail' ";
							$queryxx2	= $this->db->query($sqlxx2);
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$no_opnya.= $hasilxx2->no_op.";";
							}
						}
						
						if ($id_pp_detail != '0') {
							$sqlxx2= " SELECT distinct a.no_pp FROM tm_pp a INNER JOIN tm_pp_detail b ON a.id = b.id_pp 
									 WHERE b.id = '$id_pp_detail' ";
							$queryxx2	= $this->db->query($sqlxx2);
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$no_ppnya.= $hasilxx2->no_pp.";";
							}
						}
					}// end for
				}
				else {
					$no_opnya = '';
					$no_ppnya = '';
				} */
				
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_aplikasi_detail WHERE id_pembelian_aplikasi = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						$hasilrow = $query3->row();
						$nama_satuan = $hasilrow->nama;
						
						
						// 18-06-2015
						// 1. ambil total qty op / pp
						// 2. ambil sum beli di tabel pembelian
						//'qty_op'=> $sum_qty,
						//'jum_beli'=> $sum_beli,
						$sum_qty = 0;
						$sum_beli = 0;
					  
					  if ($row2->satuan_lain != 0) {
						  $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
						  $hasilrow = $query3->row();
						  $nama_satuan_lain = $hasilrow->nama;
					  }
					  else
						$nama_satuan_lain = '';
					  
					    $query3	= $this->db->query(" SELECT c.kode_kel_brg FROM tm_barang a INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
									WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
						
						// 31-07-2015 hitung sisa OP
						//echo $row2->id_op_detail."<br>";
						// 1. ambil qty di OP sesuai id_op_detail
						$sqlop = " SELECT b.qty FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op
									WHERE b.id = '$row2->id_op_detail' ";
						$queryop	= $this->db->query($sqlop);
						if($queryop->num_rows()>0) {
							$hasilop = $queryop->row();
							$qty_op_detail = $hasilop->qty;
						}
						else
							$qty_op_detail = 0;
						
						if ($row2->id_op_detail != '0') {
							$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelian_aplikasi_detail a 
										INNER JOIN tm_pembelian_aplikasi b ON b.id=a.id_pembelian_aplikasi
										WHERE a.id_op_detail='$row2->id_op_detail' AND a.id_brg='$row2->id_brg' 
										AND b.status_aktif='t'
										AND b.tgl_sj <= '$row1->tgl_sj' ");
							
							if($query3->num_rows()>0) {
								$hasilrow4 = $query3->row();
								$pemenuhan = $hasilrow4->pemenuhan;
								$sisa = $qty_op_detail-$pemenuhan;
							}else{
								$pemenuhan = 0;
								$sisa = $qty_op_detail-$pemenuhan;
								
								if($pemenuhan=='')
									$pemenuhan = 0;
								
								if($sisa=='')
									$sisa = 0;
							}
						}
						else {
							$sisa = '-';
							$pemenuhan = 0;
						}
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_kel_brg'=> $kode_kel_brg,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $this->db->escape_str($nama_brg),
												'nama_satuan'=> $nama_satuan,
												'harga'=> $row2->harga,
												'qty'=> $row2->qty,
												'id_satuan'=> $row2->id_satuan,
												'id_satuan_konversi'=> $row2->id_satuan_konversi,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total,
												'qty_op'=> $sum_qty,
												'jum_beli'=> $sum_beli,
												'satuan_lain'=> $row2->satuan_lain,
												'nama_satuan_lain'=> $nama_satuan_lain,
												'qty_sat_lain'=> $row2->qty_satuan_lain,
												'id_op_detail'=> $row2->id_op_detail,
												'id_pp_detail'=> $row2->id_pp_detail,
											
												//31-07-2015
												'pemenuhan'=> $pemenuhan,
												'sisa'=> $sisa
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama, top FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				$top	= $hasilrow->top;
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
				
				// 31-07-2015, ambil id gudang pake distinct utk keperluan nama staf adm stok di cetak BTB
				$sqlxx = " SELECT DISTINCT d.id_gudang FROM 
							 tm_pembelian_aplikasi_detail b 
							INNER JOIN tm_pembelian_aplikasi c ON b.id_pembelian_aplikasi = c.id
							INNER JOIN tm_barang d ON d.id = b.id_brg
							WHERE c.id='$row1->id'  ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$list_id_gudang='';
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$list_id_gudang.= $rowxx->id_gudang.";";
					}// end for
				}
				else {
					$list_id_gudang='';
				}
				
				
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_op'=> $no_opnya,
											
											'no_pp'=> $no_ppnya,
											'no_bonm'=> $row2->no_bonm,
											'no_sj'=> $row1->no_sjmasukpembelian_aplikasi,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'total'=> $row1->total,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'pkp'=> $row1->pkp,
											'tipe_pajak'=> $row1->tipe_pajak,
											'dpp'=> $row1->dpp,
											'stok_masuk_lain_cash'=> $row1->stok_masuk_lain_cash,
											'stok_masuk_lain_kredit'=> $row1->stok_masuk_lain_kredit,
											'detail_fb'=> $detail_fb,
											'list_id_gudang'=> $list_id_gudang,
											'admgudang_uid_update_by'=> $row1->uid_updatestok_by
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }  
  
  // 18-06-2015 DIKOMEN, GA DIPAKE LAGI
  /*function create_bonm($no_sj, $id_supplier, $id_brg, $nama, $qty, $harga, $stok_lain){  
    $tgl = date("Y-m-d");
    $th_now	= date("Y");
	
		// ====================================================
			// insert Bon M masuk secara otomatis di tm_apply_stok_pembelian
			
				// ambil data gudang sesuai barangnya
				$query3	= $this->db->query(" SELECT id_gudang FROM tm_barang WHERE id = '$id_brg' ");
				$hasilrow = $query3->row();
				$id_gudang = $hasilrow->id_gudang;
				
				// cek apakah header bonm udh ada
				$query3	= $this->db->query(" SELECT id, no_bonm FROM tm_apply_stok_pembelian WHERE no_sj = '$no_sj' 
				AND id_supplier = '$id_supplier' AND id_gudang = '$id_gudang' AND status_aktif = 't' ");
				if ($query3->num_rows() > 0) { // jika udh ada
					$hasilrow = $query3->row();
					$no_bonm = $hasilrow->no_bonm;
					$id_apply_stok = $hasilrow->id;
					
					//save ke tabel tm_apply_stok_pembelian_detail
					if ($id_brg!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
						$data_detail = array(
							'id_brg'=>$id_brg,
							'qty'=>$qty,
							'harga'=>$harga,
							'id_apply_stok'=>$id_apply_stok,
							'no_bonm'=>$no_bonm
						);
						$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
						
					} // end if kode != '' dan $qty != ''
				}
				else {
					// generate no Bon M
					$query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_pembelian WHERE id_gudang = '$id_gudang' 
					ORDER BY no_bonm DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() != 0) 
						$no_bonm	= $hasilrow->no_bonm;
					else
						$no_bonm = '';
					if(strlen($no_bonm)==12) {
						$nobonm = substr($no_bonm, 3, 9);
						$n_bonm	= (substr($nobonm,4,5))+1;
						$th_bonm	= substr($nobonm,0,4);
						if($th_now==$th_bonm) {
								$jml_n_bonm	= $n_bonm; //
								switch(strlen($jml_n_bonm)) {
									case "1": $kodebonm	= "0000".$jml_n_bonm;
									break;
									case "2": $kodebonm	= "000".$jml_n_bonm;
									break;	
									case "3": $kodebonm	= "00".$jml_n_bonm;
									break;
									case "4": $kodebonm	= "0".$jml_n_bonm;
									break;
									case "5": $kodebonm	= $jml_n_bonm;
									break;	
								}
								$nomorbonm = $th_now.$kodebonm;
						}
						else {
							$nomorbonm = $th_now."00001";
						}
					}
					else {
						$nomorbonm	= $th_now."00001";
					}
					$no_bonm = "BM-".$nomorbonm;
					
					// insert di tm_apply_stok_pembelian
					$data_header2 = array(
					  'no_bonm'=>$no_bonm,
					  'tgl_bonm'=>$tgl,
					  'no_sj'=>$no_sj,
					  'id_supplier'=>$id_supplier,
					  'id_gudang'=>$id_gudang,
					  'tgl_input'=>$tgl,
					  'tgl_update'=>$tgl,
					  'stok_lain'=>$stok_lain
					);
					$this->db->insert('tm_apply_stok_pembelian',$data_header2);
					
					// ambil data terakhir di tabel tm_apply_stok_pembelian
					$query2	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_apply_stok	= $hasilrow->id;
					
					//save ke tabel tm_apply_stok_pembelian_detail
					if ($id_brg!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
						$data_detail = array(
							'id_brg'=>$id_brg,
							'qty'=>$qty,
							'harga'=>$harga,
							'id_apply_stok'=>$id_apply_stok,
							'no_bonm'=>$no_bonm
						);
						$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
						
					} // end if kode != '' dan $qty != ''
				} // end else

			// ====================================================
  } */
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_brg)
  {
	  $sql= " a.*, b.nama as nj_brg, d.nama as nama_satuan 
			FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id 
			INNER JOIN tm_satuan d ON a.satuan = d.id 
			INNER JOIN tm_kelompok_barang e ON b.kode_kel_brg = e.kode
			WHERE a.status_aktif = 't'  "; // sampe sini 16:00
	  if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
	  if ($id_jenis_brg != '0')
			$sql.= " AND b.id = '$id_jenis_brg' ";
	  if ($cari != "all")
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($cari)."%')) ";
	  $sql.=" order by a.kode_brg";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  	  
	/*if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		//$sql.=" order by b.kode, c.kode, a.tgl_update DESC"; echo $sql;
		$sql.=" order by a.kode_brg";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
		order by a.kode_brg";
		$this->db->select($sql, false)->limit($num,$offset);	
    }  */

    $query = $this->db->get();
    return $query->result();
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_brg){
	$sql= " SELECT a.*, b.nama as nj_brg, d.nama as nama_satuan 
			FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id 
			INNER JOIN tm_satuan d ON a.satuan = d.id 
			INNER JOIN tm_kelompok_barang e ON b.kode_kel_brg = e.kode
			WHERE a.status_aktif = 't'  "; // sampe sini 16:00
	  if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
	  if ($id_jenis_brg != '0')
			$sql.= " AND b.id = '$id_jenis_brg' ";
	  if ($cari != "all")
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%".$this->db->escape_str($cari)."%')  
					OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') ) ";
		
		$query	= $this->db->query($sql);
		return $query->result();
	
	/*if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		$query	= $this->db->query($sql);
		return $query->result();
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();
		
		//return $this->db->query($sql);
	} */
  }
  
  function get_jenis_brg($kel_brg) {
	$sql = " SELECT b.nama as nama_kel_brg, c.* FROM tm_kelompok_barang b INNER JOIN tm_jenis_barang c ON b.kode = c.kode_kel_brg
				WHERE b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode ASC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  // 13 sept 2011
  function get_ppop($jenisnya, $kode_sup, $cari) {
	  // ambil data OP yg status_op = FALSE
	if ($cari == "all") {
		if ($jenisnya == '1') {
			$sql = " * FROM tm_pp WHERE status_faktur = 'f' order by tgl_pp DESC ";
			
			$this->db->select($sql, false);
			$query = $this->db->get();
		}
		else {
			$sql = " * FROM tm_op WHERE status_op = 'f' AND kode_supplier = '$kode_sup' order by tgl_op DESC ";
				
			$this->db->select($sql, false);
			$query = $this->db->get();
		}
	}
	else {
		if ($jenisnya == '1') {
			$sql = " * FROM tm_pp WHERE status_faktur = 'f' AND UPPER(no_pp) like UPPER('%$cari%') order by tgl_pp DESC ";
			
			$this->db->select($sql, false);
			$query = $this->db->get();
		}
		else {
			$sql = " * FROM tm_op WHERE status_op = 'f' AND kode_supplier = '$kode_sup' 
					AND UPPER(no_op) like UPPER('%$cari%') order by tgl_op DESC ";
				
			$this->db->select($sql, false);
			$query = $this->db->get();
		}
	}
		$data_op = array();
		$detail_op = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				if ($jenisnya == 2) {
					$sql2 = "SELECT * FROM tm_op_detail WHERE id_op = '$row1->id' AND status_op = 'f'";
					$no_op = $row1->no_op;
					$id_op = $row1->id;
				}
				else {
					$sql2 = "SELECT * FROM tm_pp_detail WHERE id_pp = '$row1->id' AND status_faktur = 'f'";
					$no_op = $row1->no_pp;
					$id_op = $row1->id;
				}
				
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
																		
						// ##################################################
						//--------------------
				
						$detail_op[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'no_op'=> $no_op,
												'id_op'=> $id_op
											);
					}
				}
				else {
					$detail_op = '';
				}
								
				if ($jenisnya == 2) {
					$no_ppop = $row1->no_op;
					$tgl_ppop = $row1->tgl_op;
				}
				else {
					$no_ppop = $row1->no_pp;
					$tgl_ppop = $row1->tgl_pp;
				}
				
				if ($jenisnya == 2) {
					$query3	= $this->db->query(" SELECT kode_supplier, nama from tm_supplier where kode_supplier = '$row1->kode_supplier' ");
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else {
					$kode_supplier = "";
					$nama_supplier = "";
				}
				
				$data_op[] = array(			'id'=> $row1->id,	
											'no_op'=> $no_ppop,
											'tgl_op'=> $tgl_ppop,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'detail_op'=> $detail_op
											);
				$detail_op = array();
			} // endforeach header
		}
		else {
			$data_op = '';
		}
		return $data_op;
  }
  
  // 09-07-2015
  function get_perusahaan() {
	$query3	= $this->db->query(" SELECT id, nama, alamat, kota, bag_pembelian, bag_pembelian2, bag_keuangan, 
						kepala_bagian, bag_admstok, bag_admstok2,
						id_gudang_admstok, id_gudang_admstok2, spv_bag_admstok FROM tm_perusahaan ");
				
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
		$bag_pembelian = $hasilrow->bag_pembelian;
		$bag_pembelian2 = $hasilrow->bag_pembelian2;
		$bag_keuangan = $hasilrow->bag_keuangan;
		$kepala_bagian = $hasilrow->kepala_bagian;
		$bag_admstok = $hasilrow->bag_admstok;
		$bag_admstok2 = $hasilrow->bag_admstok2;
		$id_gudang_admstok = $hasilrow->id_gudang_admstok;
		$id_gudang_admstok2 = $hasilrow->id_gudang_admstok2;
		$spv_bag_admstok = $hasilrow->spv_bag_admstok;
	}
	else {
		$id	= '';
		$nama = '';
		$kota = '';
		$alamat = '';
		$bag_pembelian = '';
		$bag_pembelian2 = '';
		$bag_keuangan = '';
		$kepala_bagian = '';
		$bag_admstok = '';
		$bag_admstok2 = '';
		$id_gudang_admstok = '';
		$id_gudang_admstok2 = '';
		$spv_bag_admstok = '';
	}
		
	$datasetting = array('id'=> $id,
						  'nama'=> $nama,
						  'alamat'=> $alamat,
						  'kota'=> $kota,
						  'bag_pembelian'=> $bag_pembelian,
						  'bag_pembelian2'=> $bag_pembelian2,
						  'bag_keuangan'=> $bag_keuangan,
						  'kepala_bagian'=> $kepala_bagian,
						  'bag_admstok'=> $bag_admstok,
						  'bag_admstok2'=> $bag_admstok2,
						  'id_gudang_admstok'=> $id_gudang_admstok,
						  'id_gudang_admstok2'=> $id_gudang_admstok2,
						  'spv_bag_admstok'=> $spv_bag_admstok
					);
							
	return $datasetting;
  }

}

