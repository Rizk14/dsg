<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  function get_gudang(){
	  $query77=$this->db->query("Select * from tm_gudang order by kode_gudang");
	  if($query77->num_rows() >0 ){
		  
		  return $query77->result();
		  }
	  
	  }
  
  //function get_all_pembelian($num, $offset, $jenis_beli, $date_from, $date_to) {
	function get_all_pembelian_old($jenis_beli, $kategori, $date_from, $date_to, $supplier) {
		$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		// 26-06-2015 SJ HASIL MAKLOON GA DIPAKE LAGI
		
		// hasil modifikasi 25 nov 2011, query gabungan antara pembelian bhn baku/pembantu biasa dgn bhn quilting
		// 14-03-2012, gabung juga dgn data sj hasil jahit
		//12-04-2012, modifikasi lagi supaya langsung dari SJ
		
		// modif 27-08-2015
		if ($kategori == 0) {
			$sql = " SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '1' as bhnbaku
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			
			$sql.= " UNION SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '0' as bhnbaku
						FROM tm_pembelian_makloon a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
		}
		else if ($kategori == 1) {
			$sql = " SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '1' as bhnbaku
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
		}
		else {
			$sql = " SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '0' as bhnbaku
						FROM tm_pembelian_makloon a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
		}
			
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ==================================================================================================
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_detail
				if ($row1->bhnbaku == 1)
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian c INNER JOIN tm_pembelian_detail d ON c.id = d.id_pembelian 
								WHERE c.id = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
				else
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian_makloon c INNER JOIN tm_pembelian_makloon_detail d ON c.id = d.id_pembelian_makloon 
								WHERE c.id = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
				
				if ($query2->num_rows() > 0) {
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
					//	$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a 
						INNER JOIN tm_gudang b ON a.id_gudang=b.id WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
							
							$nama_brg = htmlspecialchars($nama_brg);
						}
						else {
							$kode_brg = '';
							//$nama_brg = '';
							//$satuan = '';
						}
						
						// 22-09-2015
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						else {
							$query3	= $this->db->query(" SELECT b.nama FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
											WHERE a.id = '$row2->id_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT b.kode_perkiraan FROM tm_barang a 
											INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
											INNER JOIN tm_kelompok_barang b ON b.kode = c.kode_kel_brg 
											WHERE a.id = '$row2->id_brg' ");
									
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_perk	= $hasilrow->kode_perkiraan; 
							}
							else {
								$kode_perk = '';
							}
							$qty = $row2->qty;
						}
						else {
							$kode_perk = '523.100';
							$qty = $row2->qty_satawal;
						}
						
						$harga	= $row2->harga;
						$total	= $row2->total;
						
						// 09-09-2015
						if ($kategori == 0 || $kategori == 2) {
							if ($row1->bhnbaku != 1) {
								if ($row2->jenis_potong == 1)
									$nama_jenis_potong = "Potong Serong";
								else
									$nama_jenis_potong = "Potong Lurus";
								
								$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row2->id_ukuran_bisbisan' ");
								$hasilxx = $queryxx->row();
								$nama_ukuran = $hasilxx->nama;
							}
							else {
								$nama_jenis_potong = '';
								$nama_ukuran = '';
							}
						}
						else {
							$nama_jenis_potong = '';
							$nama_ukuran = '';
						}
						
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total,
												'nama_jenis_potong'=> $nama_jenis_potong,
												'nama_ukuran_bisbisan'=> $nama_ukuran
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
				
				if ($row1->bhnbaku == 1) {
					// ambil jumlah pajaknya
					if ($row1->pkp == 't') {
						$dpp = $row1->total/1.1;
						$pajaknya = $row1->total / 11;
					}
					else {
						$dpp = 0;
						$pajaknya = 0;
					}
				}
				else {
					$dpp = $row1->total*0.98;
					$pajaknya = $row1->total-$dpp;
				}
								
				$pisah1 = explode("-", $row1->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$data_beli[] = array(		'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'jumlah'=> $row1->total,
											'dpp'=> $dpp,
											'pajaknya'=> $pajaknya,
											'is_bhnbaku'=> $row1->bhnbaku,
											'detail_beli'=> $detail_beli
											);
				
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
    function get_all_pembelian($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg) {
		$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		// 26-06-2015 SJ HASIL MAKLOON GA DIPAKE LAGI
		
		// hasil modifikasi 25 nov 2011, query gabungan antara pembelian bhn baku/pembantu biasa dgn bhn quilting
		// 14-03-2012, gabung juga dgn data sj hasil jahit
		//12-04-2012, modifikasi lagi supaya langsung dari SJ
		
		// modif 27-08-2015
		if ($kategori == 0) {
			$sql = " SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '1' as bhnbaku,
						l.no_faktur, l.tgl_faktur, l.no_faktur_pajak
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						left join tm_pembelian_nofaktur_sj k on(a.id = k.id_sj_pembelian)
						left join tm_pembelian_nofaktur l on (k.id_pembelian_nofaktur = l.id)
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
			if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
			
			$sql.= " UNION SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, 
						a.total, '0' as bhnbaku, l.no_faktur, l.tgl_faktur, l.no_faktur_pajak
						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						left join tm_faktur_makloon_sj k on a.id = k.no_sj_masuk::int
						left join tm_pembelian_makloon_faktur l on k.id_faktur_makloon = l.id
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";		
		}
		else if ($kategori == 1) {
			$sql = "  SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, 
						a.total, '1' as bhnbaku, l.no_faktur, l.tgl_faktur, l.no_faktur_pajak
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						left join tm_pembelian_nofaktur_sj k on(a.id = k.id_sj_pembelian)
						left join tm_pembelian_nofaktur l on (k.id_pembelian_nofaktur = l.id)
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
		else {
			$sql = " SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, 
						a.total, '0' as bhnbaku, l.no_faktur, l.tgl_faktur, l.no_faktur_pajak
						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						left join tm_pembelian_nofaktur_sj k on(a.id = k.id_sj_pembelian)
						left join tm_pembelian_nofaktur l on (k.id_pembelian_nofaktur = l.id)
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
			
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ==================================================================================================
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_detail
				if ($row1->bhnbaku == 1)
					$query2	= $this->db->query(" SELECT d.*, f.no_faktur, f.tgl_faktur, f.no_faktur_pajak
								FROM tm_pembelian c 
								INNER JOIN tm_pembelian_detail d ON c.id = d.id_pembelian 
								left join tm_pembelian_nofaktur_sj e on(c.id = e.id_sj_pembelian)
								left join tm_pembelian_nofaktur f on (e.id_pembelian_nofaktur = f.id)
								WHERE c.id = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
				else
					$query2	= $this->db->query(" SELECT d.* , f.no_faktur, f.tgl_faktur, f.no_faktur_pajak
								FROM tm_pembelian_makloon c 
								INNER JOIN tm_pembelian_makloon_detail d ON c.id = d.id_pembelian_makloon 
								left join tm_faktur_makloon_sj e on c.id = e.no_sj_masuk::int 
								left join tm_pembelian_makloon_faktur f on e.id_faktur_makloon = f.id 
								WHERE c.id = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
				
				if ($query2->num_rows() > 0) {
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
					//	$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						$query3	= $this->db->query(" SELECT a.kode_brg,a.nama_brg FROM tm_barang a 
						INNER JOIN tm_gudang b ON a.id_gudang=b.id WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
							
							//$nama_brg = htmlspecialchars($nama_brg);
						}
						else {
							$kode_brg = '';
							//$nama_brg = '';
							//$satuan = '';
						}
						
						// 22-09-2015
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						else {
							$query3	= $this->db->query(" SELECT b.nama FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
											WHERE a.id = '$row2->id_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT b.kode_perkiraan FROM tm_barang a 
											INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
											INNER JOIN tm_kelompok_barang b ON b.kode = c.kode_kel_brg 
											WHERE a.id = '$row2->id_brg' ");
									
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_perk	= $hasilrow->kode_perkiraan; 
							}
							else {
								$kode_perk = '';
							}
							$qty = $row2->qty;
						}
						else {
							$kode_perk = '523.100';
							$qty = $row2->qty_satawal;
						}
						
						$harga	= $row2->harga;
						$total	= $row2->total;
						
				$query7	= $this->db->query(" SELECT count(id) as jum FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' ");
				if ($query7->num_rows() > 0){
					$hasilrow7 = $query7->row();
					$jum_item	= $hasilrow7->jum;
				}
				else
					$jum_item = 0;
				
						
						// 09-09-2015
						if ($kategori == 0 || $kategori == 2) {
							if ($row1->bhnbaku != 1) {
								if ($row2->jenis_potong == 1)
									$nama_jenis_potong = "Potong Serong";
								else
									$nama_jenis_potong = "Potong Lurus";
								
								$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row2->id_ukuran_bisbisan' ");
								$hasilxx = $queryxx->row();
								$nama_ukuran = $hasilxx->nama;
							}
							else {
								$nama_jenis_potong = '';
								$nama_ukuran = '';
							}
						}
						else {
							$nama_jenis_potong = '';
							$nama_ukuran = '';
						}
						
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total,
												'nama_jenis_potong'=> $nama_jenis_potong,
												'nama_ukuran_bisbisan'=> $nama_ukuran
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
				
				if ($row1->bhnbaku == 1) {
					// ambil jumlah pajaknya
					if ($row1->pkp == 't') {
						$dpp = $row1->total/1.11;
						//$pajaknya = $row1->total / 11;
						//last 20 Mei 2022
						//$pajaknya = $row1->total * 0.11;
						//next 20 Mei 2022
						$pajaknya = round(($row1->total / 1.11)*0.11);
					}
					else {
						$dpp = 0;
						$pajaknya = 0;
					}
				}
				else {
					$dpp = $row1->total*0.98;
					//$dpp = $row1->total*0.11;
					$pajaknya = $row1->total-$dpp;
				}
								
				$pisah1 = explode("-", $row1->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$data_beli[] = array(		
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'jumlah'=> $row1->total,
											'dpp'=> $dpp,
											'pajaknya'=> $pajaknya,
											'is_bhnbaku'=> $row1->bhnbaku,
											'jum_item'=>$jum_item,
											'detail_beli'=> $detail_beli,
											'tgl_faktur'=> $row1->tgl_faktur, 
											'no_faktur_pajak'=> $row1->no_faktur_pajak,
											'no_faktur'=> $row1->no_faktur
											);
				
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  function get_all_pembelian_khusus($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg) {
		$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		// 26-06-2015 SJ HASIL MAKLOON GA DIPAKE LAGI
		
		// hasil modifikasi 25 nov 2011, query gabungan antara pembelian bhn baku/pembantu biasa dgn bhn quilting
		// 14-03-2012, gabung juga dgn data sj hasil jahit
		//12-04-2012, modifikasi lagi supaya langsung dari SJ
		
		// modif 27-08-2015
		if ($kategori == 0) {
			$sql = " SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, 
						a.tgl_sj, a.total, '1' as bhnbaku, l.no_faktur, l.tgl_faktur, l.no_faktur_pajak
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						left join tm_pembelian_nofaktur_sj k on(a.id = k.id_sj_pembelian)
						left join tm_pembelian_nofaktur l on (k.id_pembelian_nofaktur = l.id)
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
			if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
			
			$sql.= " UNION SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '0' as bhnbaku
						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";		
		}
		else if ($kategori == 1) {
			$sql = "  SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '1' as bhnbaku
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
		else {
			$sql = " SELECT distinct a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '0' as bhnbaku
						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
			
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ==================================================================================================
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_detail
				if ($row1->bhnbaku == 1)
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian c INNER JOIN tm_pembelian_detail d ON c.id = d.id_pembelian 
								WHERE c.id = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
				else
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian_makloon c INNER JOIN tm_pembelian_makloon_detail d ON c.id = d.id_pembelian_makloon 
								WHERE c.id = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
				
				if ($query2->num_rows() > 0) {
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
					//	$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						$query3	= $this->db->query(" SELECT a.kode_brg,a.nama_brg FROM tm_barang a 
						INNER JOIN tm_gudang b ON a.id_gudang=b.id WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
							
							//$nama_brg = htmlspecialchars($nama_brg);
						}
						else {
							$kode_brg = '';
							//$nama_brg = '';
							//$satuan = '';
						}
						
						// 22-09-2015
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						else {
							$query3	= $this->db->query(" SELECT b.nama FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
											WHERE a.id = '$row2->id_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT b.kode_perkiraan FROM tm_barang a 
											INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
											INNER JOIN tm_kelompok_barang b ON b.kode = c.kode_kel_brg 
											WHERE a.id = '$row2->id_brg' ");
									
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_perk	= $hasilrow->kode_perkiraan; 
							}
							else {
								$kode_perk = '';
							}
							$qty = $row2->qty;
						}
						else {
							$kode_perk = '523.100';
							$qty = $row2->qty_satawal;
						}
						
						$harga	= $row2->harga;
						$total	= $row2->total;
						
				$query7	= $this->db->query(" SELECT count(id) as jum FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' ");
				if ($query7->num_rows() > 0){
					$hasilrow7 = $query7->row();
					$jum_item	= $hasilrow7->jum;
				}
				else
					$jum_item = 0;
				
						
						// 09-09-2015
						if ($kategori == 0 || $kategori == 2) {
							if ($row1->bhnbaku != 1) {
								if ($row2->jenis_potong == 1)
									$nama_jenis_potong = "Potong Serong";
								else
									$nama_jenis_potong = "Potong Lurus";
								
								$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row2->id_ukuran_bisbisan' ");
								$hasilxx = $queryxx->row();
								$nama_ukuran = $hasilxx->nama;
							}
							else {
								$nama_jenis_potong = '';
								$nama_ukuran = '';
							}
						}
						else {
							$nama_jenis_potong = '';
							$nama_ukuran = '';
						}
						
						$detail_beli[] = array( 
						'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
						'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total,
												'nama_jenis_potong'=> $nama_jenis_potong,
												'nama_ukuran_bisbisan'=> $nama_ukuran
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
				
				if ($row1->bhnbaku == 1) {
					// ambil jumlah pajaknya
					if ($row1->pkp == 't') {
						//$dpp = $row1->total/1.1;
						$dpp = $row1->total/1.11;
						//$pajaknya = $row1->total / 11;
						//$pajaknya = $row1->total * 0.11;
						$pajaknya = round(($row1->total / 1.11)*0.11);
					}
					else {
						$dpp = 0;
						$pajaknya = 0;
					}
				}
				else {
					$dpp = $row1->total*0.98;
					//$dpp = $row1->total*0.11;
					$pajaknya = $row1->total-$dpp;
				}
								
				$pisah1 = explode("-", $row1->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$data_beli[] = array(		
				//~ 'no_sj'=> $row1->no_sj,
											//~ 'tgl_sj'=> $tgl_sj,
											//~ 'kode_supplier'=> $row1->kode_supplier,
											//~ 'nama_supplier'=> $row1->nama,
											'jumlah'=> $row1->total,
											'dpp'=> $dpp,
											'pajaknya'=> $pajaknya,
											'is_bhnbaku'=> $row1->bhnbaku,
											'jum_item'=>$jum_item,
											'detail_beli'=> $detail_beli
											);
				
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  function get_all_pembelian_old_old($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg) {
		$duta = 1; 
		
		if ($kategori == 0) {
			$sql = " SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj,
			d.id as id_brg,d.nama_brg,c.id_satuan,c.qty,0 as qty_satawal, c.harga, a.total, '1' as bhnbaku ,0 as jenis_potong ,0 as id_ukuran_bisbisan

						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
			if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
			
			$sql.= " UNION SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj,
			d.id as id_brg,d.nama_brg,d.satuan,0 as qty,c.qty_satawal,c.harga, a.total, '0' as bhnbaku,c.jenis_potong,c.id_ukuran_bisbisan

						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";		
		}
		else if ($kategori == 1) {
			$sql = "  SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj,
			d.id as id_brg,d.nama_brg,c.id_satuan,c.qty,0 as qty_satawal,c.harga, a.total, '1' as bhnbaku,0 as jenis_potong, 0 as id_ukuran_bisbisan

						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
		else {
			$sql = " a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj,
			d.id as id_brg,d.nama_brg,d.satuan,0 as qty,c.qty_satawal,c.harga, a.total, '0' as bhnbaku,c.jenis_potong,c.id_ukuran_bisbisan
						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
			
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ==================================================================================================
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		
				if ($query->num_rows() > 0) {
					$hasil = $query->result();
					foreach ($hasil as $row1) {
					//	$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a 
						INNER JOIN tm_gudang b ON a.id_gudang=b.id WHERE a.id = '$row1->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
							
							//$nama_brg = htmlspecialchars($nama_brg);
						}
						else {
							$kode_brg = '';
							//$nama_brg = '';
							//$satuan = '';
						}
						
						// 22-09-2015
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->id_satuan' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						else {
							$query3	= $this->db->query(" SELECT b.nama FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
											WHERE a.id = '$row1->id_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT b.kode_perkiraan FROM tm_barang a 
											INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
											INNER JOIN tm_kelompok_barang b ON b.kode = c.kode_kel_brg 
											WHERE a.id = '$row1->id_brg' ");
									
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_perk	= $hasilrow->kode_perkiraan; 
							}
							else {
								$kode_perk = '';
							}
							$qty = $row1->qty;
						}
						else {
							$kode_perk = '523.100';
							$qty = $row1->qty_satawal;
						}
						
						$harga	= $row1->harga;
						$total	= $row1->total;
						
						// 09-09-2015
						if ($kategori == 0 || $kategori == 2) {
							if ($row1->bhnbaku != 1) {
								if ($row1->jenis_potong == 1)
									$nama_jenis_potong = "Potong Serong";
								else
									$nama_jenis_potong = "Potong Lurus";
								
								$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row1->id_ukuran_bisbisan' ");
								$hasilxx = $queryxx->row();
								$nama_ukuran = $hasilxx->nama;
							}
							else {
								$nama_jenis_potong = '';
								$nama_ukuran = '';
							}
						}
						else {
							$nama_jenis_potong = '';
							$nama_ukuran = '';
						}
						
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $row1->nama_brg,
												'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total,
												'nama_jenis_potong'=> $nama_jenis_potong,
												'nama_ukuran_bisbisan'=> $nama_ukuran
											);		
					
			
				// ===========================================================================
				
				if ($row1->bhnbaku == 1) {
					// ambil jumlah pajaknya
					if ($row1->pkp == 't') {
						$dpp = $row1->total/1.1;
						$pajaknya = $row1->total / 11;
					}
					else {
						$dpp = 0;
						$pajaknya = 0;
					}
				}
				else {
					$dpp = $row1->total*0.98;
					$pajaknya = $row1->total-$dpp;
				}
								
				$pisah1 = explode("-", $row1->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$data_beli[] = array(		'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'jumlah'=> $row1->total,
											'dpp'=> $dpp,
											'pajaknya'=> $pajaknya,
											'is_bhnbaku'=> $row1->bhnbaku,
											'detail_beli'=> $detail_beli
											);
				
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
		
	
  
  // 26-06-2015 GA DIPAKE LAGI
  /*function get_all_pembeliantanpalimit($jenis_beli, $date_from, $date_to, $supplier){
	  // 14-03-2012, gabung juga dgn data sj hasil jahit
	//12-04-2012, modifikasi lagi supaya langsung dari SJ
	$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		$sql = " SELECT a.kode_supplier, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit 
					FROM tm_pembelian a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_aktif = 't'
					AND a.jenis_pembelian = '$jenis_beli'";
		if ($supplier != '0')
			$sql.= " AND a.kode_supplier = '$supplier' ";
			
		$sql.= " UNION select c.kode_unit, c.no_sj, c.tgl_sj, c.total, c.is_makloon, c.is_jahit
					FROM tm_sj_hasil_makloon c, tm_supplier d WHERE c.kode_unit = d.kode_supplier
					AND c.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' ";
		if ($supplier != '0')
			$sql.= " AND c.kode_unit = '$supplier' ";
		
		if ($jenis_beli == '1') {
			$sql.= " UNION select a.kode_unit, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit
					FROM tm_sj_hasil_jahit a, tm_unit_jahit b WHERE a.kode_unit = b.kode_unit
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' ";
			if ($duta == '1')
				$sql.= " AND a.kode_unit = '06' "; // 06 = ci lalan 
		}
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  } */
  
  // =========================== 30-05-2015 khusus cash ==============================
  function get_all_pembeliancash($jenis_beli, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg) {
	  $sql = " select distinct a.id, a.no_faktur, a.tgl_faktur, a.jumlah, b.kode_supplier, b.nama, a.no_faktur, 
	  a.tgl_faktur, a.no_faktur_pajak
	  from tm_pembelian_nofaktur a 
			inner join tm_pembelian_nofaktur_sj f ON a.id=f.id_pembelian_nofaktur
			inner join tm_supplier b ON a.id_supplier=b.id
			inner join tm_pembelian c ON f.id_sj_pembelian=c.id
			inner join tm_pembelian_detail d ON d.id_pembelian=c.id
			inner join tm_barang e ON d.id_brg=e.id
			inner join tm_gudang g ON e.id_gudang=g.id 
			INNER JOIN tm_jenis_barang i ON e.id_jenis_barang=i.id
			INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
			WHERE 
					a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' AND a.status_lunas='t' ";
	  if ($supplier != '0')
			$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
			$sql.= " AND e.id_gudang = '$gudang' ";
			if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
	  $sql.= " ORDER BY a.tgl_faktur ASC, b.nama ASC, a.no_faktur ASC ";
			
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil no voucher pelunasan dan tglnya
				$sqlxx = " SELECT a.no_voucher, a.tgl FROM tm_payment_pembelian a 
						INNER JOIN tm_payment_pembelian_detail b ON a.id=b.id_payment
						INNER JOIN tm_payment_pembelian_nofaktur c ON b.id = c.id_payment_pembelian_detail
						WHERE c.id_pembelian_nofaktur = '$row1->id'
						 ";
				$queryxx = $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$no_voucher	= $hasilxx->no_voucher;
					$tgl_voucher	= $hasilxx->tgl;
					
					$pisah1 = explode("-", $tgl_voucher);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					
					$tgl_voucher = $tgl1."-".$bln1."-".$thn1;
				}
				else {
					$no_voucher = '';
					$tgl_voucher = '';
				}
				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_nofaktur_sj
				$query2	= $this->db->query(" SELECT b.no_sj, b.tgl_sj, c.id_brg, c.qty, c.harga, c.total, 
							d.kode_brg, c.nama_brg, e.nama as nama_satuan ,l.no_faktur, l.tgl_faktur, l.no_faktur_pajak
							FROM tm_pembelian_nofaktur_sj a
							INNER JOIN tm_pembelian b ON a.id_sj_pembelian = b.id
							INNER JOIN tm_pembelian_detail c ON b.id = c.id_pembelian
							INNER JOIN tm_barang d ON c.id_brg = d.id
							INNER JOIN tm_satuan e ON e.id = c.id_satuan
							LEFT JOIN tm_pembelian_nofaktur l on (a.id_pembelian_nofaktur = l.id)
							WHERE a.id_pembelian_nofaktur = '$row1->id'
							ORDER BY b.tgl_sj, b.no_sj, c.nama_brg 
							 ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$pisah1 = explode("-", $row2->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];						
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						$detail_beli[] = array( 'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $row2->nama_brg,
												'no_sj'=> $row2->no_sj,
												'tgl_sj'=> $tgl_sj,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'total'=> $row2->total,
												'nama_satuan'=> $row2->nama_satuan,
												'tgl_faktur'=> $row2->tgl_faktur, 
												'no_faktur_pajak'=> $row2->no_faktur_pajak,
												'no_faktur'=> $row2->no_faktur
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
				if($row1->tgl_faktur != ''){								
				$pisah1 = explode("-", $row1->tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
					}else{
						$tgl_faktur = '';
					}
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'jumlah'=> $row1->jumlah,
											'no_voucher'=> $no_voucher,
											'tgl_voucher'=> $tgl_voucher,
											'detail_beli'=> $detail_beli,
											'tgl_faktur'=> $row1->tgl_faktur, 
											'no_faktur_pajak'=> $row1->no_faktur_pajak,
											'no_faktur'=> $row1->no_faktur
											);
				
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  // =====================================================================
  
  //21-03-2012
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  function get_jenis_barang(){
	$query	= $this->db->query("select * from tm_jenis_barang ORDER BY kode ");    
    return $query->result();  
  }
   function get_kelompok_barang(){
	$query	= $this->db->query("select * from tm_kelompok_barang ORDER BY kode ");    
    return $query->result();  
  }
  
  //26-04-2012
  function get_all_pembelian_for_print($jenis_beli, $kategori, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg) {
		$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		
		// hasil modifikasi 25 nov 2011, query gabungan antara pembelian bhn baku/pembantu biasa dgn bhn quilting
		// 14-03-2012, gabung juga dgn data sj hasil jahit
		//12-04-2012, modifikasi lagi supaya langsung dari SJ
		// MODIF 09-07-2015
		
		
		if ($kategori == 0) {
			$sql = " SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '1' as bhnbaku
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
			if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
			
			$sql.= " UNION SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '0' as bhnbaku
						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";		
		}
		else if ($kategori == 1) {
			$sql = "  SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '1' as bhnbaku
						FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_detail c ON a.id=c.id_pembelian
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
		else {
			$sql = " SELECT a.id, a.id_supplier, b.kode_supplier, b.nama, b.pkp, a.no_sj, a.tgl_sj, a.total, '0' as bhnbaku
						FROM tm_pembelian_makloon a 
						INNER JOIN tm_supplier b ON a.id_supplier = b.id 
						INNER JOIN tm_pembelian_makloon_detail c ON a.id=c.id_pembelian_makloon
						INNER JOIN tm_barang d ON c.id_brg=d.id
						INNER JOIN tm_gudang e ON e.id=d.id_gudang
						INNER JOIN tm_jenis_barang i ON d.id_jenis_barang=i.id
						INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
						WHERE a.status_aktif = 't'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND a.id_supplier = '$supplier' ";
				if ($gudang != '0')
				$sql.= " AND d.id_gudang = '$gudang' ";	
				if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
		}
			
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ======================================================================================================
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				$pisah1 = explode("-", $row1->tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					
				// ambil jumlah pajaknya
				/*if ($pkp == 't')
					$pajaknya = $row1->total / 11;
				else
					$pajaknya = 0; */
				
				if ($row1->bhnbaku == 1) {
					// ambil jumlah pajaknya
					if ($row1->pkp == 't') {
						$dpp = $row1->total / 1.1;
						$pajaknya = $row1->total / 11;
					}
					else {
						$dpp = 0;
						$pajaknya = 0;
					}
				}
				else {
					$dpp = $row1->total*0.98;
					$pajaknya = $row1->total-$dpp;
				}
				
				// 18-12-2015 ambil jumlah detail
				$query3	= $this->db->query(" SELECT count(id) as jum FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_item	= $hasilrow->jum;
				}
				else
					$jum_item = 0;
				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_detail
				// MODIF 09-07-2015
				if ($row1->bhnbaku == 1)
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian c INNER JOIN tm_pembelian_detail d
								ON c.id = d.id_pembelian WHERE d.id_pembelian = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
					// 09-07-2015 DIKELUARIN
					// c.no_sj = '$row1->no_sj' AND c.kode_supplier = '$row1->kode_supplier' 
				else
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian_makloon c INNER JOIN tm_pembelian_makloon_detail d
								ON c.id = d.id_pembelian_makloon WHERE d.id_pembelian_makloon = '$row1->id' AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id ASC ");
				
				if ($query2->num_rows() > 0) {
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
							
							// 08-07-2015
							/*$pos = strpos($nama_brg, "\"");
							if ($pos > 0) {
							  $nama_brg = str_replace("\"", "&quot;", $nama_brg);
							}
							else {
							  $nama_brg = str_replace("'", "\'", $nama_brg);
							} */
							
							//$nama_brg = htmlspecialchars($nama_brg);
						}
						else {
							$kode_brg = '';
							//$nama_brg = '';
							//$satuan = '';
						}
						
						// 22-09-2015. 06-11-2015
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan	= $hasilrow->nama;
							}
							else
								$satuan = '';
						}
						else
							$satuan = "Yard";
						
						if ($row1->bhnbaku == 1) {
							$query3	= $this->db->query(" SELECT b.kode_perkiraan FROM tm_barang a INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
											INNER JOIN tm_kelompok_barang b ON b.kode = c.kode_kel_brg
											WHERE a.id = '$row2->id_brg' ");
									
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_perk	= $hasilrow->kode_perkiraan; 
							}
							else {
								$kode_perk = '';
							}
							$qty	= $row2->qty;
						}
						else {
							$kode_perk = '523.100';
							$qty = $row2->qty_satawal;
						}
						
						$harga	= $row2->harga;
						$total	= $row2->total;
						
						// 09-09-2015
						if ($kategori == 0 || $kategori == 2) {
							if ($row1->bhnbaku != 1) {
								if ($row2->jenis_potong == 1)
									$nama_jenis_potong = "Potong Serong";
								else
									$nama_jenis_potong = "Potong Lurus";
								
								$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row2->id_ukuran_bisbisan' ");
								$hasilxx = $queryxx->row();
								$nama_ukuran = $hasilxx->nama;
							}
							else {
								$nama_jenis_potong = '';
								$nama_ukuran = '';
							}
						}
						else {
							$nama_jenis_potong = '';
							$nama_ukuran = '';
						}
											
						$detail_beli[] = array( 'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'jumlah'=> $row1->total,
											'dpp'=> $dpp,
											'pajaknya'=> $pajaknya,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $row2->nama_brg,
											'kode_perk'=> $kode_perk,
											'harga'=> $harga,
											'qty'=> $qty,
											'satuan'=> $satuan,
											'total'=> $total,
											'nama_jenis_potong'=> $nama_jenis_potong,
											'nama_ukuran_bisbisan'=> $nama_ukuran,
											'jum_item'=> $jum_item
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
			} // endforeach header
		}
		else {
			$detail_beli = '';
		}
		return $detail_beli;
  }
  
  // 30-05-2015
  function get_all_pembeliancash_for_print($jenis_beli, $date_from, $date_to, $supplier,$gudang,$kel_brg,$jns_brg) {
		$sql = " select a.id, a.no_faktur, a.tgl_faktur, a.jumlah, b.kode_supplier, b.nama
	  from tm_pembelian_nofaktur a 
			inner join tm_pembelian_nofaktur_sj f ON a.id=f.id_pembelian_nofaktur
			inner join tm_supplier b ON a.id_supplier=b.id
			inner join tm_pembelian c ON f.id_sj_pembelian=c.id
			inner join tm_pembelian_detail d ON d.id_pembelian=c.id
			inner join tm_barang e ON d.id_brg=e.id
			inner join tm_gudang g ON e.id_gudang=g.id 
			INNER JOIN tm_jenis_barang i ON e.id_jenis_barang=i.id
			INNER JOIN tm_kelompok_barang j ON j.kode=i.kode_kel_brg
			WHERE 
					a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' AND a.status_lunas='t' ";
	  if ($supplier != '0')
			$sql.= " AND a.id_supplier = '$supplier' ";
			if ($gudang != '0')
			$sql.= " AND e.id_gudang = '$gudang' ";
			if ($kel_brg != '0')
				$sql.= " AND i.kode_kel_brg = '$kel_brg' ";
			if ($jns_brg != '0')
				$sql.= " AND d.id_jenis_barang = '$jns_brg' ";	
			
		$query	= $this->db->query($sql);
				
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$pisah1 = explode("-", $row1->tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				
				// ambil no voucher pelunasan dan tglnya
				$sqlxx = " SELECT a.no_voucher, a.tgl FROM tm_payment_pembelian a 
						INNER JOIN tm_payment_pembelian_detail b ON a.id=b.id_payment
						INNER JOIN tm_payment_pembelian_nofaktur c ON b.id = c.id_payment_pembelian_detail
						WHERE c.id_pembelian_nofaktur = '$row1->id'
						 ";
				$queryxx = $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$no_voucher	= $hasilxx->no_voucher;
					$tgl_voucher	= $hasilxx->tgl;
					
					$pisah1 = explode("-", $tgl_voucher);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					
					$tgl_voucher = $tgl1."-".$bln1."-".$thn1;
				}
				else {
					$no_voucher = '';
					$tgl_voucher = '';
				}
				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_nofaktur_sj
				$query2	= $this->db->query(" SELECT b.no_sj, b.tgl_sj, c.kode_brg, c.qty, c.harga, c.total, 
							c.nama_brg, e.nama as nama_satuan 
							FROM tm_pembelian_nofaktur_sj a
							INNER JOIN tm_pembelian b ON a.id_sj_pembelian = b.id
							INNER JOIN tm_pembelian_detail c ON b.id = c.id_pembelian
							INNER JOIN tm_barang d ON c.id_brg = d.id
							INNER JOIN tm_satuan e ON e.id = c.id_satuan
							WHERE a.id_pembelian_nofaktur = '$row1->id'
							ORDER BY b.tgl_sj, b.no_sj, c.nama_brg 
							 ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$pisah1 = explode("-", $row2->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];						
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						$detail_beli[] = array( 'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $row2->nama_brg,
												'no_sj'=> $row2->no_sj,
												'tgl_sj'=> $tgl_sj,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'total'=> $row2->total,
												'nama_satuan'=> $row2->nama_satuan,
												
												'no_faktur'=> $row1->no_faktur,
												'tgl_faktur'=> $tgl_faktur,
												'kode_supplier'=> $row1->kode_supplier,
												'nama_supplier'=> $row1->nama,
												'jumlah'=> $row1->jumlah,
												'no_voucher'=> $no_voucher,
												'tgl_voucher'=> $tgl_voucher
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
				
				/*$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'jumlah'=> $row1->jumlah,
											'no_voucher'=> $no_voucher,
											'tgl_voucher'=> $tgl_voucher,
											'detail_beli'=> $detail_beli
											);
				
				$detail_beli = array(); */
			} // endforeach header
		}
		else {
			$detail_beli = '';
		}
		return $detail_beli;
  }
  
  // 29-05-2015
  function getlistunitjahit(){
	$sql = " * FROM tm_unit_jahit ORDER BY kode_unit ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get_all_fakturwip($date_from, $date_to, $kode_unit, $jenis_masuk) {
		$sql = " SELECT a.id, b.kode_unit,  a.tgl_sj,a.no_sj,  b.nama as nama_unit 
					FROM tm_sjmasukwip a, tm_unit_jahit b WHERE a.id_unit_jahit = b.id
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')";
		$sql.= " AND jenis_masuk = '$jenis_masuk' ";
		if ($kode_unit != '0')
			$sql.= " AND b.kode_unit = '$kode_unit' ";
		
		$sql.= " ORDER BY b.nama ASC, a.tgl_sj ASC, a.no_sj ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail a
								WHERE id_sjmasukwip = '$row1->id' ORDER BY a.id ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tm_barang_wip WHERE id='$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan = "Pieces";
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan = '';
						}
						
						//else {
						//	if ($row1->is_jahit == 'f')
						//		$kode_perk = "511.100"; 
						//	else
						//		$kode_perk = "512.100";
						//}
						
						$qty	= $row2->qty;
						//$harga	= $row2->harga;
						//$diskon	= $row2->diskon;
						//$subtotal	= $row2->subtotal;
						$no_sj	= $row1->no_sj;
						$tgl_sj	= $row1->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												//'kode_perk'=> $kode_perk,
												//'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												//'subtotal'=> $subtotal,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												//'diskon'=> $diskon
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
			//	$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		//'no_faktur'=> $row1->no_faktur,
											//'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $row1->nama_unit,
											//'grandtotal'=> $grandtotal,
											'detail_beli'=> $detail_beli
											);
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  // utk export
  function get_all_fakturwip_for_print($date_from, $date_to, $kode_unit,$jenis_masuk) {
		$sql = " SELECT a.id, b.kode_unit,  a.tgl_sj,a.no_sj,  b.nama as nama_unit 
					FROM tm_sjmasukwip a, tm_unit_jahit b WHERE a.id_unit_jahit = b.id
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')";
		if ($jenis_masuk != '0'){
		$sql.= " AND jenis_masuk = '$jenis_masuk' ";
		}
		if ($kode_unit != '0')
			$sql.= " AND b.kode_unit = '$kode_unit' ";
		$sql.= " ORDER BY b.nama ASC, a.tgl_sj ASC, a.no_sj ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail a
								WHERE id_sjmasukwip = '$row1->id' ORDER BY a.id ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tm_barang_wip WHERE id='$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan = "Pieces";
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan = '';
						}
						
						//else {
						//	if ($row1->is_jahit == 'f')
						//		$kode_perk = "511.100"; 
						//	else
						//		$kode_perk = "512.100";
						//}
						
						$qty	= $row2->qty;
						//$harga	= $row2->harga;
						//$diskon	= $row2->diskon;
						//$subtotal	= $row2->subtotal;
						$no_sj	= $row1->no_sj;
						$tgl_sj	= $row1->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												//'kode_perk'=> $kode_perk,
												//'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												//'subtotal'=> $subtotal,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												//'diskon'=> $diskon
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
			//	$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		//'no_faktur'=> $row1->no_faktur,
											//'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $row1->nama_unit,
											//'grandtotal'=> $grandtotal,
											'detail_beli'=> $detail_beli
											);
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  
  // 13-06-2015
  function get_all_pelunasan($jenis_beli, $date_from, $date_to, $supplier) {
	  $pencarian='';
	  if ($jenis_beli != 0)
		  $pencarian.="AND a.jenis_pembelian = '$jenis_beli'";
	  	  $sql = " SELECT a.id, a.no_voucher, a.tgl, b.id as idpaymentdetail, d.kode_supplier, d.nama, b.subtotal, b.jumlah_bayar,
				a.is_makloon
				FROM tm_payment_pembelian a 
				INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
				INNER JOIN tm_supplier d ON d.id = b.id_supplier
				WHERE a.tgl >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl <= to_date('$date_to','dd-mm-yyyy')"
				.$pencarian ;
	  if ($supplier != '0'){
		  $sql.= " AND d.kode_supplier = '$supplier' ";
	  $sql.= " ORDER BY a.tgl ASC, d.nama, a.no_voucher ASC ";
	}
	/*  SELECT a.id, a.no_faktur, a.tgl_faktur, a.jumlah, b.kode_supplier, b.nama FROM tm_pembelian_nofaktur a 
					INNER JOIN tm_supplier b ON a.kode_supplier = b.kode_supplier WHERE 
					a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' AND a.status_lunas='t' ";
	  if ($supplier != '0')
			$sql.= " AND a.kode_supplier = '$supplier' ";
	  $sql.= " ORDER BY a.tgl_faktur ASC, b.nama ASC, a.no_faktur ASC "; */
			
		$query	= $this->db->query($sql);
				
		$data_voucher = array();
		$detail_item = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				/*$pisah1 = explode("-", $row1->tgl_voucher);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tgl_voucher = $tgl1."-".$bln1."-".$thn1; */
									
				// =============================== detailnya ================================
				// ambil detail data list barang dari tabel tm_payment_pembelian_nofaktur, tm_pembelian_nofaktur, 
				//tm_pembelian_detail
				/*$query2	= $this->db->query(" SELECT b.no_faktur, b.tgl_faktur, f.kode_brg, e.qty, e.harga, e.total,
							e.diskon, f.nama_brg, g.nama as nama_satuan
							FROM tm_payment_pembelian_nofaktur a 
							INNER JOIN tm_pembelian_nofaktur b ON a.id_pembelian_nofaktur = b.id
							INNER JOIN tm_pembelian_nofaktur_sj c ON b.id = c.id_pembelian_nofaktur
							
							INNER JOIN tm_pembelian d ON c.id_sj_pembelian = d.id
							INNER JOIN tm_pembelian_detail e ON d.id = e.id_pembelian
							INNER JOIN tm_barang f ON e.id_brg = f.id
							INNER JOIN tm_satuan g ON f.satuan = g.id
							WHERE a.id_payment_pembelian_detail = '$row1->idpaymentdetail'
							ORDER BY b.tgl_faktur, b.no_faktur, f.nama_brg 							
							 "); */
				
				// 02-12-2015
				if ($row1->is_makloon == 'f')
					$query2	= $this->db->query(" SELECT distinct b.no_faktur, b.tgl_faktur, b.id_supplier, 
								d.id as id_sj_pembelian, e.id_brg, f.kode_brg, e.nama_brg, e.harga, g.nama as nama_satuan
								FROM tm_payment_pembelian_nofaktur a 
								INNER JOIN tm_pembelian_nofaktur b ON a.id_pembelian_nofaktur = b.id
								INNER JOIN tm_pembelian_nofaktur_sj c ON b.id = c.id_pembelian_nofaktur
								
								INNER JOIN tm_pembelian d ON c.id_sj_pembelian = d.id
								INNER JOIN tm_pembelian_detail e ON d.id = e.id_pembelian
								INNER JOIN tm_barang f ON e.id_brg = f.id
								INNER JOIN tm_satuan g ON e.id_satuan = g.id
								WHERE a.id_payment_pembelian_detail = '$row1->idpaymentdetail'
								ORDER BY b.tgl_faktur, b.no_faktur, e.nama_brg 							
								 ");
				else
					$query2	= $this->db->query(" SELECT distinct b.no_faktur, b.tgl_faktur, b.id_supplier, 
								d.id as id_sj_pembelian, e.id_brg, f.kode_brg, e.nama_brg, e.harga, 'Yard' as nama_satuan
								FROM tm_payment_pembelian_nofaktur a 
								INNER JOIN tm_pembelian_makloon_faktur b ON a.id_pembelian_nofaktur = b.id
								INNER JOIN tm_pembelian_makloon_faktur_detail c ON b.id = c.id_pembelian_makloon_faktur
								
								INNER JOIN tm_pembelian_makloon d ON c.id_pembelian_makloon = d.id
								INNER JOIN tm_pembelian_makloon_detail e ON d.id = e.id_pembelian_makloon
								INNER JOIN tm_barang f ON e.id_brg = f.id
								WHERE a.id_payment_pembelian_detail = '$row1->idpaymentdetail'
								ORDER BY b.tgl_faktur, b.no_faktur, e.nama_brg 							
								 ");
				
				/*b.no_sj, b.tgl_sj, c.kode_brg, c.qty, c.harga, c.total, 
							d.nama_brg, e.nama as nama_satuan 
							FROM tm_pembelian_nofaktur_sj a
							INNER JOIN tm_pembelian b ON a.id_sj_pembelian = b.id
							INNER JOIN tm_pembelian_detail c ON b.id = c.id_pembelian
							INNER JOIN tm_barang d ON c.kode_brg = d.kode_brg
							INNER JOIN tm_satuan e ON e.id = d.satuan
							WHERE a.id_pembelian_nofaktur = '$row1->id'
							ORDER BY b.tgl_sj, b.no_sj, d.nama_brg */
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$pisah1 = explode("-", $row2->tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];						
						$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
						
						//$total= ($row2->qty*$row2->harga)-$row2->diskon;
						
						// 04-09-2015, ambil qty, total
						// 02-12-2015
						if ($row1->is_makloon == 'f')
							$query3	= $this->db->query(" SELECT sum(e.qty) as jumqty, sum(e.total) as jumtotal
								FROM tm_payment_pembelian_nofaktur a 
								INNER JOIN tm_pembelian_nofaktur b ON a.id_pembelian_nofaktur = b.id
								INNER JOIN tm_pembelian_nofaktur_sj c ON b.id = c.id_pembelian_nofaktur
								
								INNER JOIN tm_pembelian d ON c.id_sj_pembelian = d.id
								INNER JOIN tm_pembelian_detail e ON d.id = e.id_pembelian
								INNER JOIN tm_barang f ON e.id_brg = f.id
								INNER JOIN tm_satuan g ON f.satuan = g.id
								WHERE a.id_payment_pembelian_detail = '$row1->idpaymentdetail'
								AND b.no_faktur = '$row2->no_faktur' AND b.id_supplier='$row2->id_supplier'
								AND e.id_brg = '$row2->id_brg' AND d.id = '$row2->id_sj_pembelian' ");
						else
							$query3	= $this->db->query(" SELECT sum(e.qty_satawal) as jumqty, sum(e.total) as jumtotal
								FROM tm_payment_pembelian_nofaktur a 
								INNER JOIN tm_pembelian_makloon_faktur b ON a.id_pembelian_nofaktur = b.id
								INNER JOIN tm_pembelian_makloon_faktur_detail c ON b.id = c.id_pembelian_makloon_faktur
								
								INNER JOIN tm_pembelian_makloon d ON c.id_pembelian_makloon = d.id
								INNER JOIN tm_pembelian_makloon_detail e ON d.id = e.id_pembelian_makloon
								INNER JOIN tm_barang f ON e.id_brg = f.id
								INNER JOIN tm_satuan g ON f.satuan = g.id
								WHERE a.id_payment_pembelian_detail = '$row1->idpaymentdetail'
								AND b.no_faktur = '$row2->no_faktur' AND b.id_supplier='$row2->id_supplier'
								AND e.id_brg = '$row2->id_brg' AND d.id = '$row2->id_sj_pembelian' ");
							// sum(e.diskon) as jumdiskon
							
						if ($query3->num_rows() > 0) {
							$hasil3 = $query3->row();
							$qty	= $hasil3->jumqty;
							//$diskon	= $hasil3->jumdiskon;
							$total	= $hasil3->jumtotal;
						}
						else {
							$qty=0;
							$diskon = 0;
							$total = 0;
						}
						
						//$total = ($qty*$row2->harga)-$diskon;
						
						$detail_item[] = array( 'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $row2->nama_brg,
												'no_faktur'=> $row2->no_faktur,
												'tgl_faktur'=> $tgl_faktur,
												'qty'=> $qty,
												'harga'=> $row2->harga,
												'total'=> $total,
												'nama_satuan'=> $row2->nama_satuan
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_voucher = $tgl1." ".$nama_bln." ".$thn1;
				
				$data_voucher[] = array(	'no_voucher'=> $row1->no_voucher,
											'tgl_voucher'=> $tgl_voucher,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'subtotal'=> $row1->subtotal,
											'jumlah_bayar'=> $row1->jumlah_bayar,
											'detail_item'=> $detail_item
											);
				
				$detail_item = array();
			} // endforeach header
		}
		else {
			$data_voucher = '';
		}
		return $data_voucher;
  }
  // =====================================================================
    
}

