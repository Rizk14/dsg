<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  //function get_all_pembelian($num, $offset, $jenis_beli, $date_from, $date_to) {
	function get_all_pembelian($jenis_beli, $date_from, $date_to, $supplier) {
		$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		
		// hasil modifikasi 25 nov 2011, query gabungan antara pembelian bhn baku/pembantu biasa dgn bhn quilting
		// 14-03-2012, gabung juga dgn data sj hasil jahit
		//12-04-2012, modifikasi lagi supaya langsung dari SJ
		$sql = " SELECT a.kode_supplier, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit 
					FROM tm_pembelian a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND a.status_aktif = 't'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli'";
		if ($supplier != '0')
			$sql.= " AND a.kode_supplier = '$supplier' ";
			
		$sql.= " UNION select c.kode_unit, c.no_sj, c.tgl_sj, c.total, c.is_makloon, c.is_jahit
					FROM tm_sj_hasil_makloon c, tm_supplier d WHERE c.kode_unit = d.kode_supplier
					AND c.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' ";
		if ($supplier != '0')
			$sql.= " AND c.kode_unit = '$supplier' ";
		
		if ($jenis_beli == '1') {
			$sql.= " UNION select a.kode_unit, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit
					FROM tm_sj_hasil_jahit a, tm_unit_jahit b WHERE a.kode_unit = b.kode_unit
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' ";
			//if ($duta == '1')
			//	$sql.= " AND a.kode_unit = '06' "; // 06 = ci lalan 
			if ($supplier != '0')
				$sql.= " AND a.kode_unit = '$supplier' ";
		}
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ====================================================================================================== end 14-03-2012
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_supplier	= $hasilrow->nama;
					$pkp	= $hasilrow->pkp;
				}
				else {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_supplier' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_supplier	= $hasilrow->nama;
						$pkp	= 'f';
					}
				} 
				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_detail
				if ($row1->is_makloon == 'f')
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian c, tm_pembelian_detail d
								WHERE c.id = d.id_pembelian
								AND c.no_sj = '$row1->no_sj' AND c.kode_supplier = '$row1->kode_supplier' 
								AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id DESC ");
				else {
					//14-03-2012, query ke tabel detail sj hasil jahit
					if ($row1->is_jahit == 'f')
						$query2	= $this->db->query(" SELECT d.* FROM tm_sj_hasil_makloon c, tm_sj_hasil_makloon_detail d
								WHERE c.id = d.id_sj_hasil_makloon
								AND c.no_sj = '$row1->no_sj' AND c.kode_unit = '$row1->kode_supplier' 
								ORDER BY c.tgl_sj ASC, c.no_sj, d.kode_brg ");
					else
						$query2	= $this->db->query(" SELECT b.*, a.no_sj, a.tgl_sj FROM tm_sj_hasil_jahit a, tm_sj_hasil_jahit_detail b
								WHERE a.id = b.id_sj_hasil_jahit
								AND a.kode_unit = '06'
								AND a.no_sj = '$row1->no_sj'  
								ORDER BY a.tgl_sj ASC, a.no_sj, b.kode_brg_jadi "); 
					
				} 
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						if ($row1->is_jahit == 'f') {
							if ($row1->is_makloon == 'f') {
								$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
											WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							}
							else {
								$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
											WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
							}
						
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg	= $hasilrow->kode_brg;
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$kode_brg = '';
								$nama_brg = '';
								$satuan = '';
							}
						}
						else {
							$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$kode_brg	= $row2->kode_brg_jadi;
								$nama_brg	= $hasilrow->e_product_motifname;
								$satuan = "Pieces";
							}
							else {
								$kode_brg	= '';
								$nama_brg	= '';
								$satuan = '';
							}
						}
						
						if ($row1->is_makloon == 'f') {
							$query3	= $this->db->query(" SELECT b.kode_perkiraan FROM tm_barang a, tm_kelompok_barang b, 
										tm_jenis_barang c, tm_jenis_bahan d WHERE a.id_jenis_bahan = d.id
										AND d.id_jenis_barang = c.id
										AND c.kode_kel_brg = b.kode
										AND a.kode_brg = '$row2->kode_brg' ");
								
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_perk	= $hasilrow->kode_perkiraan; 
							}
							else {
								$kode_perk = '';
							}
						}
						else {
							if ($row1->is_jahit == 'f')
								$kode_perk = "511.100"; 
							else
								$kode_perk = "512.100";
						}
						
						if ($row1->is_makloon == 'f') {
							$qty	= $row2->qty;
							$harga	= $row2->harga;
							$total	= $row2->total;
						}
						else {
							if ($row1->is_jahit == 'f') {
								$qty	= $row2->qty_makloon;
								$harga	= $row2->harga;
								$total	= $row2->biaya;
							}
							else {
								$qty	= $row2->qty_brg_jadi;
								$harga	= $row2->harga;
								$total	= $row2->biaya;
							}
						}
					
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
				
				// ambil jumlah pajaknya
				if ($pkp == 't')
					$pajaknya = $row1->total / 11;
				else
					$pajaknya = 0;
								
				$pisah1 = explode("-", $row1->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				$data_beli[] = array(		'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'jumlah'=> $row1->total,
											'pajaknya'=> $pajaknya,
											'detail_beli'=> $detail_beli
											);
				
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  function get_all_pembeliantanpalimit($jenis_beli, $date_from, $date_to, $supplier){
	  // 14-03-2012, gabung juga dgn data sj hasil jahit
	//12-04-2012, modifikasi lagi supaya langsung dari SJ
	$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		$sql = " SELECT a.kode_supplier, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit 
					FROM tm_pembelian a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_aktif = 't'
					AND a.jenis_pembelian = '$jenis_beli'";
		if ($supplier != '0')
			$sql.= " AND a.kode_supplier = '$supplier' ";
			
		$sql.= " UNION select c.kode_unit, c.no_sj, c.tgl_sj, c.total, c.is_makloon, c.is_jahit
					FROM tm_sj_hasil_makloon c, tm_supplier d WHERE c.kode_unit = d.kode_supplier
					AND c.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' ";
		if ($supplier != '0')
			$sql.= " AND c.kode_unit = '$supplier' ";
		
		if ($jenis_beli == '1') {
			$sql.= " UNION select a.kode_unit, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit
					FROM tm_sj_hasil_jahit a, tm_unit_jahit b WHERE a.kode_unit = b.kode_unit
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' ";
			if ($duta == '1')
				$sql.= " AND a.kode_unit = '06' "; // 06 = ci lalan 
		}
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  //21-03-2012
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  // 03-04-2012
  function get_sj_nonsinkron() {
	  // 1. dari tm_pembelian (jgn lupa yg dari tm_sj_hasil_makloon, bikin terpisah aja)
		$sql = " SELECT id, kode_supplier, no_sj, tgl_sj, total, status_faktur, status_lunas, tgl_input, tgl_update 
				FROM tm_pembelian WHERE status_aktif = 't' ORDER BY kode_supplier ";
		$query	= $this->db->query($sql);		
		
		//$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query4	= $this->db->query(" SELECT sum(total) as jumdetail FROM tm_pembelian_detail WHERE id_pembelian='$row1->id' ");
				if ($query4->num_rows() > 0){
					$hasilrow = $query4->row();
					$jumdetail	= $hasilrow->jumdetail;
					
					if ($jumdetail != $row1->total) {
						//echo $row1->id." ".$jumdetail." ".$row1->total."<br>";
						// ambil data nama supplier
						$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_supplier	= $hasilrow->nama;
						}
						else {
							$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_supplier' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_supplier	= $hasilrow->nama;
							}
						} 
						
						$query2	= $this->db->query(" SELECT * FROM tm_pembelian_detail WHERE id_pembelian='$row1->id' ");
						if ($query2->num_rows() > 0) { //
							$hasil2 = $query2->result();
							
							foreach ($hasil2 as $row2) {
								$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$kode_brg	= $hasilrow->kode_brg;
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$kode_brg = '';
									$nama_brg = '';
									$satuan = '';
								}
								$qty	= $row2->qty;
								$harga	= $row2->harga;
								$total	= $row2->total;
							
								$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total,
												'no_sj'=> $row1->no_sj,
												'tgl_sj'=> $row1->tgl_sj,
												'kode_supplier'=> $row1->kode_supplier,
												'nama_supplier'=> $nama_supplier,
												'total_header'=> $row1->total,
												'status_faktur'=> $row1->status_faktur,
												'status_lunas'=> $row1->status_lunas,
												'tgl_input'=> $row1->tgl_input,
												'tgl_update'=> $row1->tgl_update
											);		
							}
							
						}
					} // end if $jumdetail != $row1->total
				}
				
			} // endforeach header
		}
		/*else {
			$detail_beli = '';
		} */
		
		// 2. SJ hasil makloon
		$sql = " SELECT id, kode_unit, no_sj, tgl_sj, total, status_faktur, status_lunas, tgl_input, tgl_update 
				FROM tm_sj_hasil_makloon ORDER BY kode_unit ";
		$query	= $this->db->query($sql);		
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query4	= $this->db->query(" SELECT sum(biaya) as jumdetail FROM tm_sj_hasil_makloon_detail WHERE id_sj_hasil_makloon='$row1->id' ");
				if ($query4->num_rows() > 0){
					$hasilrow = $query4->row();
					$jumdetail	= $hasilrow->jumdetail;
					if ($jumdetail != $row1->total) {
						
						// ambil data nama supplier
						$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_supplier	= $hasilrow->nama;
						}
						
						$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon_detail WHERE id_sj_hasil_makloon ='$row1->id' ");
						if ($query2->num_rows() > 0) { //
							$hasil2 = $query2->result();
							
							foreach ($hasil2 as $row2) {
								$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
							
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$kode_brg	= $hasilrow->kode_brg;
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$kode_brg = '';
									$nama_brg = '';
									$satuan = '';
								}
								$qty	= $row2->qty_makloon;
								$harga	= $row2->harga;
								$total	= $row2->biaya;
							
								$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total,
												'no_sj'=> $row1->no_sj,
												'tgl_sj'=> $row1->tgl_sj,
												'kode_supplier'=> $row1->kode_unit,
												'nama_supplier'=> $nama_supplier,
												'total_header'=> $row1->total,
												'status_faktur'=> $row1->status_faktur,
												'status_lunas'=> $row1->status_lunas,
												'tgl_input'=> $row1->tgl_input,
												'tgl_update'=> $row1->tgl_update
											);		
							}
							
						}
					} // end if $jumdetail != $row1->total
				}
				
			} // endforeach header
		}
		else {
			$detail_beli = '';
		}
		
		return $detail_beli;
  }
  
  //26-04-2012
  function get_all_pembelian_for_print($jenis_beli, $date_from, $date_to, $supplier) {
		$duta = 1; // khusus duta ini valuenya 1. kalo client lain, setting aja 0
		
		// hasil modifikasi 25 nov 2011, query gabungan antara pembelian bhn baku/pembantu biasa dgn bhn quilting
		// 14-03-2012, gabung juga dgn data sj hasil jahit
		//12-04-2012, modifikasi lagi supaya langsung dari SJ
		$sql = " SELECT a.kode_supplier, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit 
					FROM tm_pembelian a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND a.status_aktif = 't'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli'";
		if ($supplier != '0')
			$sql.= " AND a.kode_supplier = '$supplier' ";
			
		$sql.= " UNION select c.kode_unit, c.no_sj, c.tgl_sj, c.total, c.is_makloon, c.is_jahit
					FROM tm_sj_hasil_makloon c, tm_supplier d WHERE c.kode_unit = d.kode_supplier
					AND c.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' ";
		if ($supplier != '0')
			$sql.= " AND c.kode_unit = '$supplier' ";
		
		if ($jenis_beli == '1') {
			$sql.= " UNION select a.kode_unit, a.no_sj, a.tgl_sj, a.total, a.is_makloon, a.is_jahit
					FROM tm_sj_hasil_jahit a, tm_unit_jahit b WHERE a.kode_unit = b.kode_unit
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' ";
			if ($duta == '1')
				$sql.= " AND a.kode_unit = '06' "; // 06 = ci lalan 
		}
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ====================================================================================================== end 14-03-2012
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_supplier	= $hasilrow->nama;
					$pkp	= $hasilrow->pkp;
				}
				else {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_supplier' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_supplier	= $hasilrow->nama;
						$pkp	= 'f';
					}
				} 
				
						$pisah1 = explode("-", $row1->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					
				// ambil jumlah pajaknya
				if ($pkp == 't')
					$pajaknya = $row1->total / 11;
				else
					$pajaknya = 0;
				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_pembelian_detail
				if ($row1->is_makloon == 'f')
					$query2	= $this->db->query(" SELECT d.* FROM tm_pembelian c, tm_pembelian_detail d
								WHERE c.id = d.id_pembelian
								AND c.no_sj = '$row1->no_sj' AND c.kode_supplier = '$row1->kode_supplier' 
								AND c.status_aktif = 't'
								ORDER BY c.tgl_sj ASC, c.no_sj, d.id DESC ");
				else {
					//14-03-2012, query ke tabel detail sj hasil jahit
					if ($row1->is_jahit == 'f')
						$query2	= $this->db->query(" SELECT d.* FROM tm_sj_hasil_makloon c, tm_sj_hasil_makloon_detail d
								WHERE c.id = d.id_sj_hasil_makloon
								AND c.no_sj = '$row1->no_sj' AND c.kode_unit = '$row1->kode_supplier' 
								ORDER BY c.tgl_sj ASC, c.no_sj, d.kode_brg ");
					else
						$query2	= $this->db->query(" SELECT b.*, a.no_sj, a.tgl_sj FROM tm_sj_hasil_jahit a, tm_sj_hasil_jahit_detail b
								WHERE a.id = b.id_sj_hasil_jahit
								AND a.kode_unit = '06'
								AND a.no_sj = '$row1->no_sj'  
								ORDER BY a.tgl_sj ASC, a.no_sj, b.kode_brg_jadi "); 
					
				} 
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						if ($row1->is_jahit == 'f') {
							if ($row1->is_makloon == 'f') {
								$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
											WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							}
							else {
								$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
											WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
							}
						
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg	= $hasilrow->kode_brg;
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$kode_brg = '';
								$nama_brg = '';
								$satuan = '';
							}
						}
						else {
							$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$kode_brg	= $row2->kode_brg_jadi;
								$nama_brg	= $hasilrow->e_product_motifname;
								$satuan = "Pieces";
							}
							else {
								$kode_brg	= '';
								$nama_brg	= '';
								$satuan = '';
							}
						}
						
						if ($row1->is_makloon == 'f') {
							$query3	= $this->db->query(" SELECT b.kode_perkiraan FROM tm_barang a, tm_kelompok_barang b, 
										tm_jenis_barang c, tm_jenis_bahan d WHERE a.id_jenis_bahan = d.id
										AND d.id_jenis_barang = c.id
										AND c.kode_kel_brg = b.kode
										AND a.kode_brg = '$row2->kode_brg' ");
								
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_perk	= $hasilrow->kode_perkiraan; 
							}
							else {
								$kode_perk = '';
							}
						}
						else {
							if ($row1->is_jahit == 'f')
								$kode_perk = "511.100"; 
							else
								$kode_perk = "512.100";
						}
						
						if ($row1->is_makloon == 'f') {
							$qty	= $row2->qty;
							$harga	= $row2->harga;
							$total	= $row2->total;
						}
						else {
							if ($row1->is_jahit == 'f') {
								$qty	= $row2->qty_makloon;
								$harga	= $row2->harga;
								$total	= $row2->biaya;
							}
							else {
								$qty	= $row2->qty_brg_jadi;
								$harga	= $row2->harga;
								$total	= $row2->biaya;
							}
						}
					
						$detail_beli[] = array( 'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'jumlah'=> $row1->total,
											'pajaknya'=> $pajaknya,
						
												'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'total'=> $total
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
				
			/*	$data_beli[] = array(		'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'jumlah'=> $row1->total,
											'pajaknya'=> $pajaknya,
											'detail_beli'=> $detail_beli
											); */
				
				//$detail_beli = array();
			} // endforeach header
		}
		else {
			$detail_beli = '';
		}
		return $detail_beli;
  }
  
  // 29-05-2015
  function getlistunitjahit(){
	$sql = " * FROM tm_unit_jahit ORDER BY kode_unit ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get_all_fakturwip($date_from, $date_to, $kode_unit) {
		$sql = " SELECT a.id, a.kode_unit_jahit, a.no_faktur, a.tgl, a.grandtotal, b.nama as nama_unit 
					FROM tm_fakturmasukwip a, tm_unit_jahit b WHERE a.kode_unit_jahit = b.kode_unit 
					AND a.tgl >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl <= to_date('$date_to','dd-mm-yyyy')";
		if ($kode_unit != '0')
			$sql.= " AND a.kode_unit_jahit = '$kode_unit' ";
		$sql.= " ORDER BY b.nama ASC, a.tgl ASC, a.no_faktur ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_fakturmasukwip_detail
					$query2	= $this->db->query(" SELECT * FROM tm_fakturmasukwip_detail
								WHERE id_fakturmasukwip = '$row1->id' ORDER BY id ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $row2->kode_brg_jadi;
							$nama_brg	= $hasilrow->e_product_motifname;
							$satuan = "Pieces";
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan = '';
						}
						
						//else {
						//	if ($row1->is_jahit == 'f')
						//		$kode_perk = "511.100"; 
						//	else
						//		$kode_perk = "512.100";
						//}
						
						$qty	= $row2->qty;
						$harga	= $row2->harga;
						$diskon	= $row2->diskon;
						$subtotal	= $row2->subtotal;
						$no_sj	= $row2->no_sj;
						$tgl_sj	= $row2->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												//'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'subtotal'=> $subtotal,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'diskon'=> $diskon
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_unit_jahit,
											'nama_unit'=> $row1->nama_unit,
											'grandtotal'=> $grandtotal,
											'detail_beli'=> $detail_beli
											);
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  // utk export
  function get_all_fakturwip_for_print($date_from, $date_to, $kode_unit) {
		$sql = " SELECT a.id, a.kode_unit_jahit, a.no_faktur, a.tgl, a.grandtotal, b.nama as nama_unit 
					FROM tm_fakturmasukwip a, tm_unit_jahit b WHERE a.kode_unit_jahit = b.kode_unit 
					AND a.tgl >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl <= to_date('$date_to','dd-mm-yyyy')";
		if ($kode_unit != '0')
			$sql.= " AND a.kode_unit_jahit = '$kode_unit' ";
		$sql.= " ORDER BY b.nama ASC, a.tgl ASC, a.no_faktur ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_fakturmasukwip_detail
					$query2	= $this->db->query(" SELECT * FROM tm_fakturmasukwip_detail
								WHERE id_fakturmasukwip = '$row1->id' ORDER BY id ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $row2->kode_brg_jadi;
							$nama_brg	= $hasilrow->e_product_motifname;
							$satuan = "Pieces";
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan = '';
						}
						
						//else {
						//	if ($row1->is_jahit == 'f')
						//		$kode_perk = "511.100"; 
						//	else
						//		$kode_perk = "512.100";
						//}
						
						$qty	= $row2->qty;
						$harga	= $row2->harga;
						$diskon	= $row2->diskon;
						$subtotal	= $row2->subtotal;
						$no_sj	= $row2->no_sj;
						$tgl_sj	= $row2->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						//$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						// 29-03-2014
						$tgl_sj = $thn1."/".$bln1."/".$tgl1;
						
						$pisah1 = explode("-", $row1->tgl);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
								if ($bln1 == '01')
									$nama_bln = "Januari";
								else if ($bln1 == '02')
									$nama_bln = "Februari";
								else if ($bln1 == '03')
									$nama_bln = "Maret";
								else if ($bln1 == '04')
									$nama_bln = "April";
								else if ($bln1 == '05')
									$nama_bln = "Mei";
								else if ($bln1 == '06')
									$nama_bln = "Juni";
								else if ($bln1 == '07')
									$nama_bln = "Juli";
								else if ($bln1 == '08')
									$nama_bln = "Agustus";
								else if ($bln1 == '09')
									$nama_bln = "September";
								else if ($bln1 == '10')
									$nama_bln = "Oktober";
								else if ($bln1 == '11')
									$nama_bln = "November";
								else if ($bln1 == '12')
									$nama_bln = "Desember";
						//$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						
						// 29-03-2014
						$tgl_faktur = $thn1."/".$bln1."/".$tgl1;
						
						$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												//'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'subtotal'=> $subtotal,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'diskon'=> $diskon,
												
												'no_faktur'=> $row1->no_faktur,
												'tgl_faktur'=> $tgl_faktur,
												'kode_unit'=> $row1->kode_unit_jahit,
												'nama_unit'=> $row1->nama_unit,
												'grandtotal'=> $grandtotal
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				
				
				/*$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_unit_jahit,
											'nama_unit'=> $row1->nama_unit,
											'grandtotal'=> $grandtotal,
											'detail_beli'=> $detail_beli
											);
				$detail_beli = array(); */
			} // endforeach header
		}
		else {
			$detail_beli = '';
		}
		return $detail_beli;
  }
    
}

