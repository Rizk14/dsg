<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
function get_supplier (){
	 $this->db->select(" * from tm_supplier order by kode_supplier");
	$query=$this->db->get();
	if($query->num_rows() > 0){
		return $query->result();
		}
	}
	
	function get_nama_supplier ($id_supplier){
	 $this->db->select(" nama,kode_supplier from tm_supplier where id='$id_supplier' order by kode_supplier");
	$query=$this->db->get();
	if($query->num_rows() > 0){
		return $query->row();
		}
	}

  function cek_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_supplier){
     $this->db->select("* from tt_harga WHERE SUBSTRING(cast(tgl_input as  varchar), 1,7 ) >= '".$tahun_dari."-".$bulan_dari."' AND SUBSTRING(cast(tgl_input as  varchar), 1,7 ) <= '".$tahun_ke."-".$bulan_ke."' AND id_supplier='$id_supplier'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
	  function get_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_supplier){
   
    $this->db->select("* from tt_harga WHERE SUBSTRING(cast(tgl_input as  varchar), 1,7 ) >= '".$tahun_dari."-".$bulan_dari."' AND SUBSTRING(cast(tgl_input as  varchar), 1,7 ) <= '".$tahun_ke."-".$bulan_ke."' AND id_supplier='$id_supplier'
    order by id_brg asc,tgl_input asc", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		$hasil= $query->result();
		foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan,a.kode_brg FROM tm_barang a, tm_satuan b 
						WHERE a.satuan = b.id AND a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$kode_brg	= $hasilrow->kode_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
					$kode_brg='';
				}
				
				$query3	= $this->db->query(" SELECT nama,kode_supplier FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else
					$nama_supplier = '';
					
					
				
								
				$data_harga[] = array(		'id'=> $row1->id,	
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,						
											'harga'=> $row1->harga,
											'tgl_input'=>$row1->tgl_input,
																	
											);
		} // endforeach header
	}
	else {
		$data_harga = '';
	}
	return $data_harga;
  }
  
  function get_rekap_hargatanpalimit($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_supplier){
	$query	=  $this->db->query("SELECT * from tt_harga WHERE SUBSTRING(cast(tgl_input as  varchar), 1,7 ) >= '".$tahun_dari."-".$bulan_dari."' AND SUBSTRING(cast(tgl_input as  varchar), 1,7 ) <= '".$tahun_ke."-".$bulan_ke."' AND id_supplier='$id_supplier'", false);
    
    return $query->result();  
  }
  

}
