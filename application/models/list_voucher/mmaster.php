<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mmaster extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	function bacarvbprint($icoabank,$cari,$area,$date_from,$date_to,$num,$offset)
  {
    if($cari==''){
	  	$this->db->select(" distinct on (a.i_rv) *, a.v_rv as v_rv, b.i_rvb as i_rv from tm_rv a, tm_rvb b
							                      where a.i_area='$area' and d_rv >='$date_from' AND d_rv <='$date_to' and a.i_rv_type='02'
							                      and a.i_area=b.i_area and a.i_rv=b.i_rv and a.i_rv_type=b.i_rv_type and b.i_coa_bank='$icoabank'",false)->limit($num,$offset);
    }else{
	  	$this->db->select(" distinct on (a.i_rv) *, a.v_rv as v_rv, b.i_rvb as i_rv from tm_rv a, tm_rvb b
							                      where a.i_area='$area'and d_rv >='$date_from' AND d_rv <='$date_to' and a.i_rv_type='02'
							                      and a.i_area=b.i_area and a.i_rv=b.i_rv and a.i_rv_type=b.i_rv_type and b.i_coa_bank='$icoabank'
                                    and (upper(b.i_rvb) like '%$cari%')",false)->limit($num,$offset);
    }
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
	function bacapvbprint($icoabank,$cari,$area,$date_from,$date_to,$num,$offset)
  {
    if($cari==''){
	  	$this->db->select(" a.v_pv as v_pv, b.i_pvb as i_pv from tm_pv a, tm_pvb b
							                      where a.i_area='$area' and d_pv >='$date_from' AND d_pv <='$date_to' and a.i_pv_type='02'
							                      and a.i_area=b.i_area and a.i_pv=b.i_pv and a.i_pv_type=b.i_pv_type and b.i_coa_bank='$icoabank'",false)->limit($num,$offset);
    }else{
	  	$this->db->select(" a.v_pv as v_pv, b.i_pvb as i_pv from tm_pv a, tm_pvb b
							                      where a.i_area='$area' and d_pv >='$date_from' AND d_pv <='$date_to' and a.i_pv_type='02'
							                      and a.i_area=b.i_area and a.i_pv=b.i_pv and a.i_pv_type=b.i_pv_type and b.i_coa_bank='$icoabank'
                                    and (upper(b.i_pvb) like '%$cari%')",false)->limit($num,$offset);
    }
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
	function bacarvprint1($cari,$area,$date_from,$date_to,$num,$offset)
  {
    if($cari==''){
	  	$this->db->select(" distinct on (a.i_rv) *, a.v_rv from tm_rv a, tm_rv_item b 
	  	                    where a.i_rv=b.i_rv and a.i_area=b.i_area and a.i_rv_type='01'  and b.i_rv_type='01'
	  	                    and a.i_area='$area' AND d_rv >='$date_from' AND d_rv <='$date_to'",false)->limit($num,$offset);
    }else{
	  	$this->db->select(" distinct on (a.i_rv) *, a.v_rv from tm_rv a, tm_rv_item b 
	  	                    where a.i_rv=b.i_rv and a.i_area=b.i_area and a.i_rv_type='01' and b.i_rv_type='01'
	  	                    and a.i_area='$area' AND d_rv >='$date_from' AND d_rv <='$date_to'
	  	                    and upper(a.i_rv) like '%$cari%'",false)->limit($num,$offset);
    }
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
function bacapvprint1($cari,$area,$date_from,$date_to,$num,$offset)
  {
    if($cari==''){
	  	$this->db->select(" distinct on (a.i_pv) a.* from tm_pv a, tm_pv_item b 
	  	                    where a.i_pv=b.i_pv and a.i_area=b.i_area and a.i_pv_type='00' 
	  	                    and a.i_area='$area' and a.d_pv >='$date_from' AND a.d_pv <='$date_to' order by a.i_pv, b.i_kk
	  	                     ",false)->limit($num,$offset);
    }else{
	  	$this->db->select(" distinct on (a.i_pv) a.* from tm_pv a, tm_pv_item b 
	  	                    where a.i_pv=b.i_pv and a.i_area=b.i_area and a.i_pv_type='00'
	  	                    and a.i_area='$area' and a.d_pv >='$date_from' AND a.d_pv <='$date_to'
	  	                    and upper(a.i_pv) like '%$cari%' order by a.i_pv, b.i_kk",false)->limit($num,$offset);
    }
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
  function bacapvprint2($cari,$area,$date_from,$date_to,$num,$offset)
  {
    if($cari=='sikasep'){
	  	$this->db->select(" distinct on (a.i_pv) *, a.v_pv as v_pv from tm_pv a, tm_pv_item b 
	  	                    where a.i_pv=b.i_pv and a.i_area=b.i_area 
	  	                    and a.i_area='$area'and a.d_pv >='$date_from' AND a.d_pv <='$date_to' and a.i_pv_type='01'",false)->limit($num,$offset);
    }else{
	  	$this->db->select(" distinct on (a.i_pv) *, a.v_pv as v_pv from tm_pv a, tm_pv_item b 
	  	                    where a.i_pv=b.i_pv and a.i_area=b.i_area 
	  	                    and a.i_area='$area' and a.d_pv >='$date_from' AND a.d_pv <='$date_to' and a.i_pv_type='01'
	  	                    and upper(a.i_pv) like '%$cari%'",false)->limit($num,$offset);
    }
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
  function get_area()
  { 
	 $query = 	$this->db->query(" SELECT * FROM tm_area ORDER by id");
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
   function get_bank()
  { 
	 $query = 	$this->db->query(" SELECT * FROM tm_bank ORDER by id");
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
  }
  function bacapv1($ipv,$iarea)
    {
		$this->db->select(" * from tm_pv a, tm_pv_item b
		                    where a.i_pv=b.i_pv and a.i_area=b.i_area and a.i_area='$iarea' and a.i_pv='$ipv' 
		                    and a.i_pv_type= b.i_pv_type and a.i_pv_type='00'
		                    order by b.i_kk",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacapv2($ipv,$iarea)
     {
		$this->db->select(" * from tm_pv a, tm_pv_item b
		                    where a.i_pv=b.i_pv and a.i_area=b.i_area and a.i_area='$iarea' and a.i_pv='$ipv' 
		                    and a.i_pv_type= b.i_pv_type and a.i_pv_type='01'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacarv1($irv,$iarea)
    {
		$this->db->select(" * from tm_rv a, tm_rv_item b 
		                    where a.i_rv=b.i_rv and a.i_area=b.i_area and a.i_area='$iarea' and a.i_rv='$irv' and a.i_rv_type='01' and b.i_rv_type='01'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  	function bacapvb1($ipv,$iarea,$icoabank)
    {
		
	  	$this->db->select(" d.i_pvb as i_pv, a.d_pv, b.i_coa, b.e_remark, b.v_pv
		                    from tm_pv a, tm_pv_item b,  tm_pvb d
		                    where a.i_pv=b.i_pv and a.i_area=b.i_area and a.i_area='$iarea' and d.i_pvb='$ipv' 
		                    and a.i_pv_type= b.i_pv_type and a.i_pv_type='02'
		                    and a.i_area=d.i_area and a.i_pv_type=d.i_pv_type and a.i_pv=d.i_pv 
  	                    and d.i_coa_bank='$icoabank'",false);
  	                    
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  		return $query->result();
  	}
    }
    function bacarvb1($irv,$iarea,$icoabank)
    {
		$this->db->select(" d.i_rvb as i_rv, a.d_rv, b.i_coa, b.e_remark, b.v_rv
		                    from  tm_rvb d, tm_rv a
		                    left join tm_rv_item b on(a.i_rv=b.i_rv and a.i_area=b.i_area and a.i_rv_type= b.i_rv_type)
		                    where a.i_area='$iarea' and d.i_rvb='$irv' 
		                    and a.i_rv_type='02'
		                     and a.i_area=d.i_area and a.i_rv_type=d.i_rv_type and a.i_rv=d.i_rv 
  	                    and d.i_coa_bank='$icoabank'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
     function baca_persh()
    {
		$this->db->select(" * from tm_perusahaan",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result_array();
		}
    }
}
