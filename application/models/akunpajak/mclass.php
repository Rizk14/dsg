<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function cekakunpajak($ekodeakunpajak){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_akun_pajak WHERE i_akun_pajak='$ekodeakunpajak' ");	
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_akun_pajak ORDER BY i_akun_pajak DESC ", false);
	}

	function viewperpages($limit,$offset) {	
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_akun_pajak ORDER BY i_akun_pajak DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($iakunpajak) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$str = array(
			'i_akun_pajak'=>$iakunpajak,
			'd_entry'=>$dentry
		);
		
		$db2->insert('tr_akun_pajak',$str);
		redirect('akunpajak/cform/');
    }
	
	function mupdate($iakun,$iakunpajak) {
		$db2=$this->load->database('db_external', TRUE);
		$class_item	= array(
			'i_akun_pajak'=>$iakunpajak
		);
		$db2->update('tr_akun_pajak',$class_item,array('i_akun'=>$iakun));
		redirect('akunpajak/cform');
	}

	function classcode() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT cast(i_akun AS integer)+1 AS classcode FROM tr_akun_pajak ORDER BY i_akun DESC LIMIT 1");
	}

	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_akun_pajak WHERE i_akun='$id' ");
	}
	
	function viewcari($iakunpajak) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_akun_pajak WHERE i_akun_pajak LIKE '%$iakunpajak%' ");
	}
	
	function mcari($iakunpajak,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_akun_pajak WHERE i_akun_pajak LIKE '%$iakunpajak%' LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function delete($id) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_akun_pajak',array('i_akun'=>$id));
		redirect('akunpajak/cform/');	 
	}
}
?>
