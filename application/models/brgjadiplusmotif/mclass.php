<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function delete($id){
		$db2 = $this->load->database('db_external', TRUE);
		$db2->delete('tr_product_motif',array('i_product_motif'=>$id));
		$db2->delete('tm_stokmutasi',array('i_product'=>$id,'f_active_month'=>'TRUE'));
		$db2->query("UPDATE tr_product_price SET f_active='f' WHERE i_product_motif='$id' ");	
			
		$qso	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' ");
		
		if($qso->num_rows()>0){
			foreach($qso->result() as $row){
				$i_so	= $row->i_so;
				$db2->delete('tm_stokopname_item',array('i_product'=>$id,'i_so'=>$i_so));
			}
		}
		
		redirect('brgjadiplusmotif/cform');
	}
	
	function view() {
		$db2 = $this->load->database('db_external', TRUE);
		$filter	= " WHERE n_active='1' ";
		$order	= " ORDER BY i_product_motif ASC, e_product_motifname ASC ";		
		return $db2->query("SELECT * FROM tr_product_motif ".$filter." ".$order." ", false);
	}

	function viewperpages($limit,$offset) {
		$db2 = $this->load->database('db_external', TRUE);
		$filter	= " WHERE n_active='1' ";
		$order	= " ORDER BY i_product_motif ASC, e_product_motifname ASC ";
		$query	= $db2->query( " SELECT * FROM tr_product_motif ".$filter." ".$order. " LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function listpquery($tabel,$order,$filter) {
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		if ($query->num_rows() > 0) {
			return $result	= $query->result(); 
		}
	}
	
	/* 05082011
	function lbarangjadi() {
		return $db2->query("
			SELECT 	a.i_product_base AS iproductname, 
					b.i_product_motif AS iproductmotif,
					a.e_product_basename AS productname,
					b.n_quantity AS qty
			
			FROM tr_product_base a 
			INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product			
			WHERE b.n_active='1'
			GROUP BY a.i_product_base, b.i_product_motif, a.e_product_basename, b.n_quantity
			ORDER BY a.e_product_basename ASC, b.i_product_motif DESC ");
	}
	
	function lbarangjadiperpages($limit,$offset) {
		
		$strqry	= ("
			SELECT 	a.i_product_base AS iproductname, 
					b.i_product_motif AS iproductmotif,
					a.e_product_basename AS productname,
					b.n_quantity AS qty
			FROM tr_product_base a 
			INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product
			WHERE b.n_active='1'
			GROUP BY a.i_product_base, b.i_product_motif, a.e_product_basename, b.n_quantity
			ORDER BY a.e_product_basename ASC, b.i_product_motif DESC "." LIMIT ".$limit." OFFSET ".$offset);
		
		$query	= $db2->query($strqry);
			
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function flbarangjadi($key) {
		$ky_upper	= strtoupper($key);
		return $db2->query("
			SELECT 	a.i_product_base AS iproductname, 
					b.i_product_motif AS iproductmotif,
					a.e_product_basename AS productname,
					b.n_quantity AS qty
			FROM tr_product_base a 
			INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product
			WHERE b.n_active='1' AND a.i_product_base='$ky_upper'
			GROUP BY a.i_product_base, b.i_product_motif, a.e_product_basename, b.n_quantity
			ORDER BY a.e_product_basename ASC, b.i_product_motif DESC " );
	}	
	*/
	
	function lbarangjadi() {
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT 	a.i_product_base AS iproductname, a.e_product_basename AS productname
			
			FROM tr_product_base a 

			GROUP BY a.i_product_base, a.e_product_basename
			ORDER BY a.i_product_base ASC ");
	}
	
	function lbarangjadiperpages($limit,$offset) {
		$db2 = $this->load->database('db_external', TRUE);
		$strqry	= ("
			SELECT 	a.i_product_base AS iproductname, a.e_product_basename AS productname
			FROM tr_product_base a 
			GROUP BY a.i_product_base, a.e_product_basename
			ORDER BY a.i_product_base ASC "." LIMIT ".$limit." OFFSET ".$offset);
		
		$query	= $db2->query($strqry);
			
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function flbarangjadi($key) {
		$db2 = $this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		return $db2->query("
			SELECT 	a.i_product_base AS iproductname, a.e_product_basename AS productname
			FROM tr_product_base a 
			WHERE a.i_product_base='$ky_upper'
			GROUP BY a.i_product_base, a.e_product_basename
			ORDER BY a.i_product_base ASC " );
	}
		
	function icolor() {
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_color ORDER BY i_color ASC ");
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function ecolor($icolor){
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT e_color_name FROM tr_color WHERE i_color='$icolor' ");
	}
	
	
	function msimpan($i_product,$i_product_motif,$n_quantity,$color,$ecolor) {
		
		$db2 = $this->load->database('db_external', TRUE);
		
		$qdate	= $db2->query( " SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date " );
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qproduct	= $db2->query(" SELECT * FROM tr_product_base WHERE i_product_base='$i_product' ORDER BY i_product_base DESC LIMIT 1 ");
		if($qproduct->num_rows()>0) {
			$row_product	= $qproduct->row();
			if($color!=0) {
				$productname	= $row_product->e_product_basename." ".TRIM($ecolor);
			}else{
				$productname	= $row_product->e_product_basename;	
			}
		}
			
		$productmotif_item	= array(
			 'i_product_motif'=>$i_product_motif,
			 'i_product'=>$i_product,
			 'e_product_motifname'=>$productname,
			 'n_quantity'=>$n_quantity,
			 'n_active'=>'1',
			 'i_color'=>$color,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry);
		
		if($productname!="") {
		
			$db2->insert('tr_product_motif',$productmotif_item);
			
			$qproductmotif2	= $db2->query(" INSERT INTO tr_product_motif (i_product_motif,i_product,e_product_motifname,n_quantity,n_active,i_color,d_entry,d_update) 
					VALUES('$i_product_motif','$i_product','$productname','$n_quantity','1','$color','$dentry','$dentry') ");
			
			$qryprice	= $db2->query(" SELECT * FROM tr_product_price ORDER BY i_price DESC LIMIT 1 ");	
			
			if($qryprice->num_rows()>0) {					
				$row_price	= $qryprice->row();
				
				$iproductprice	= $row_price->i_price+1;
				$imotifkode	= $row_price->i_product_motif;
				$icustomer	= $row_price->i_customer;
				
				$icodeprice0	= $row_price->i_product_price; // H00001
				$icodeprice_angka	= substr($icodeprice0,1,strlen($icodeprice0)-1); // 00001
				$icodeprice_jml	= $icodeprice_angka + 1;

				switch(strlen($icodeprice_jml)) {
					case 1:
						$icode	= 'H'.'0000'.$icodeprice_jml;
					break;
					case 2:
						$icode	= 'H'.'000'.$icodeprice_jml;
					break;
					case 3:
						$icode	= 'H'.'00'.$icodeprice_jml;
					break;
					case 4:
						$icode	= 'H'.'0'.$icodeprice_jml;
					break;
					default:
						$icode	= 'H'.$icodeprice_jml;
				}			
				$icodeprice		= $icode;
			}else{
				$icodeprice		= "H00001";
				$iproductprice	= 1;
			}
			
			$qimotif	= $db2->query(" SELECT * FROM tr_product_base WHERE i_product_base='$i_product' LIMIT 1 ");
			
			if($qimotif->num_rows()>0) {
				$rimotif	= $qimotif->row();
				$db2->query(" INSERT INTO tr_product_price (i_price, i_product_price,i_customer,i_product,i_product_motif,v_price,d_entry,d_update) 
				VALUES('$iproductprice','$icodeprice','0','$i_product','$i_product_motif','$rimotif->v_unitprice','$dentry','$dentry')  ");
			}
			
			redirect('brgjadiplusmotif/cform/');
		}else{
			redirect('brgjadiplusmotif/cform/');
		}
		
	}


	function msimpan_old($i_product,$i_product_motif,$n_quantity,$color,$ecolor) {
		$db2 = $this->load->database('db_external', TRUE);
		
		
		$qdate	= $db2->query( " SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date " );
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qproduct	= $db2->query(" SELECT * FROM tr_product_base WHERE i_product_base='$i_product' ORDER BY i_product_base DESC LIMIT 1 ");
		if($qproduct->num_rows()>0) {
			$row_product	= $qproduct->row();
			if($color!=0){
				$productname	= $row_product->e_product_basename." ".TRIM($ecolor);
			}else{
				$productname	= $row_product->e_product_basename;	
			}
		}
		
		$productmotif_item	= array(
			 'i_product_motif'=>$i_product_motif,
			 'i_product'=>$i_product,
			 'e_product_motifname'=>$productname,
			 'n_quantity'=>$n_quantity,
			 'n_active'=>'1',
			 'i_color'=>$color,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry);
		
		/* 05082011
		if($db2->insert('tr_product_motif',$productmotif_item)) {
		*/	
		if($productname!=""){		
		
			$db2->insert('tr_product_motif',$productmotif_item);
			
			$qproductmotif2	= $db2->query(" INSERT INTO tr_product_motif(i_product_motif,i_product,e_product_motifname,n_quantity,n_active,i_color,d_entry,d_update) 
					VALUES('$i_product_motif','$i_product','$productname','$n_quantity','1','$color','$dentry','$dentry') ");
			
			//$qryprice	= $db2->query(" SELECT * FROM tr_product_price WHERE f_active='t' ORDER BY i_price DESC LIMIT 1 ");
			$qryprice	= $db2->query(" SELECT * FROM tr_product_price ORDER BY i_price DESC LIMIT 1 ");	
			
			if($qryprice->num_rows()>0) {					
				$row_price	= $qryprice->row();
				
				$iproductprice	= $row_price->i_price+1;
				$imotifkode	= $row_price->i_product_motif;
				$icustomer	= $row_price->i_customer;
				
				$icodeprice0	= $row_price->i_product_price; // H00001
				$icodeprice_angka	= substr($icodeprice0,1,strlen($icodeprice0)-1); // 00001
				$icodeprice_jml	= $icodeprice_angka + 1;

				switch(strlen($icodeprice_jml)) {
					case "1":
						$icode	= 'H'.'0000'.$icodeprice_jml;
					break;
					case "2":
						$icode	= 'H'.'000'.$icodeprice_jml;
					break;
					case "3":
						$icode	= 'H'.'00'.$icodeprice_jml;
					break;
					case "4":
						$icode	= 'H'.'0'.$icodeprice_jml;
					break;
					default:
						$icode	= 'H'.$icodeprice_jml;
				}
				
				$icodeprice		= $icode;
			}else{
				$icodeprice		= "H00001";
				$iproductprice	= 1;
			}
			
			$qimotif	= $db2->query(" SELECT * FROM tr_product_base WHERE i_product_base='$i_product' LIMIT 1 ");
			
			if($qimotif->num_rows()>0) {
				$rimotif	= $qimotif->row();
				//if(($imotifkode!=$i_product_motif) && ($icustomer!=0)){
					$db2->query(" INSERT INTO tr_product_price(i_price, i_product_price,i_customer,i_product,i_product_motif,v_price,d_entry,d_update) VALUES('$iproductprice','$icodeprice','0','$i_product','$i_product_motif','$rimotif->v_unitprice','$dentry','$dentry')  ");
				//}
			}
			
			/****** Disabled 30-03-2011, di pindahkan ke stokopname baru
			$iso	=	$db2->query(" SELECT * FROM tm_so WHERE i_product_motif='$i_product_motif' ORDER BY cast(i_so AS integer) DESC LIMIT 1 ");
			if($iso->num_rows() > 0 ) {
				$iso_row				= $iso->row();
				
				$i_so					= $iso_row->i_so;
				$i_so_new				= $i_so+1;
				$i_product				= $iso_row->i_product;
				$i_product_motif		= $iso_row->i_product_motif;
				$e_product_motifname	= $iso_row->e_product_motifname;
				$i_status_do			= $iso_row->i_status_do;
				$d_do					= $iso_row->d_do;
				$n_saldo_awal			= $iso_row->n_saldo_awal;
				$n_saldo_akhir			= $iso_row->n_saldo_akhir;
				$n_inbonm				= $iso_row->n_inbonm;
				$n_outbonm				= $iso_row->n_outbonm;
				$n_bbm					= $iso_row->n_bbm;
				$n_bbk					= $iso_row->n_bbk;
				$d_entry				= $iso_row->d_entry;
				
				$iso_item	= array(
					'i_so'=>$i_so_new,
					'i_product'=>$i_product,
					'i_product_motif'=>$i_product_motif,
					'e_product_motifname'=>$e_product_motifname,
					'i_status_do'=>'0',
					'n_saldo_awal'=>$qty,
					'n_saldo_akhir'=>$qty,
					'd_entry'=>$dentry
				);
				
				$db2->insert('tm_so', $iso_item);
				
			} else {
			
				$iso	=	$db2->query(" SELECT * FROM tm_so ORDER BY cast(i_so AS integer) DESC LIMIT 1 ");
				if($iso->num_rows() > 0 ) {
					$iso_row	= $iso->row_array();
					$i_so_new	= $iso_row['i_so']+1;
				} else {
					$i_so_new	= 1;
				}
				
				$iproductmotif	= $db2->query(" SELECT a.i_product_motif, 
															a.n_quantity AS qty,
															b.i_product_base 
								FROM tr_product_motif a 
								
								INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
								
								WHERE i_product_motif='$i_product_motif' LIMIT 1 ");
								
				if($iproductmotif->num_rows() > 0 ) {
					$motif_row		= $iproductmotif->row();
					$iproductmotif	= $motif_row->i_product_motif;
					$iproduct		= $motif_row->i_product_base;
					$motifname		= $motif_row->e_product_motifname;
					$qty			= $motif_row->qty;

					$iso_item	= array(
						'i_so'=>$i_so_new,
						'i_product'=>$iproduct,
						'i_product_motif'=>$iproductmotif,
						'e_product_motifname'=>$motifname,
						'i_status_do'=>'0',
						'n_saldo_awal'=>$qty,
						'n_saldo_akhir'=>$qty,
						'd_entry'=>$dentry
					);
					$db2->insert('tm_so', $iso_item);
				}		
			}
			***/
			redirect('brgjadiplusmotif/cform/');
		} else {
			redirect('brgjadiplusmotif/cform/');
		}
	}
		
	function medit($id) {
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$id' AND n_active='1' ");
	}
	
	function mupdate($iproduct,$iproductmotif,$nquantity,$color,$ecolor) {
		
		$db2 = $this->load->database('db_external', TRUE);
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dupdate= $drow->date;
		
		$qproduct	= $db2->query(" SELECT * FROM tr_product_base WHERE i_product_base='$iproduct' ORDER BY i_product_base DESC LIMIT 1 ");
		if($qproduct->num_rows()>0) {
			$row_product	= $qproduct->row();
			
			if($color!=0) {
				$productname	= $row_product->e_product_basename." ".TRIM($ecolor);
			}else{
				$productname	= $row_product->e_product_basename;
			}
			
			/*
			$productmotifitem	= $db2->set(array(
				'e_product_motifname'=>$productname,
				'n_quantity'=>$nquantity,
				'i_color'=>$color,
				'd_product_motifupdate'=>$dupdate
			));
			$db2->update('tr_product_motif',array('i_product_motif'=>$iproductmotif,'i_product'=>$iproduct));			
			*/
			
			$db2->query(" UPDATE tr_product_motif SET e_product_motifname='$productname', n_quantity='$nquantity', i_color='$color', d_update='$dupdate' WHERE i_product_motif='$iproductmotif' AND i_product='$iproduct' ");
			$db2->query(" UPDATE tr_product_motif SET e_product_motifname='$productname', n_quantity='$nquantity', i_color='$color', d_update='$dupdate' WHERE i_product_motif='$iproductmotif' AND i_product='$iproduct' ");
		}

		redirect('brgjadiplusmotif/cform/');		
	}
	
	function viewcari($cari) {
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_product_motif WHERE n_active='1' AND (UPPER(i_product_motif) LIKE UPPER('$cari%') OR UPPER(i_product) LIKE UPPER('$cari%') OR UPPER(e_product_motifname) LIKE UPPER('$cari%')) ORDER BY i_product_motif ASC, e_product_motifname ASC " );
	}
	
	function mcari($cari,$limit,$offset) {
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_product_motif WHERE n_active='1' AND (UPPER(i_product_motif) LIKE UPPER('$cari%') OR UPPER(i_product) LIKE UPPER('$cari%') OR UPPER(e_product_motifname) LIKE UPPER('$cari%')) ORDER BY i_product_motif ASC, e_product_motifname ASC LIMIT ".$limit." OFFSET ".$offset,false);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
}
?>
