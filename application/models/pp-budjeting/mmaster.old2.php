<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
    function get_jenis_barang(){
	$query	= $this->db->query("select * from tm_jenis_barang ORDER BY kode ");    
    return $query->result();  
  }
   function get_kelompok_barang(){
	$query	= $this->db->query("select * from tm_kelompok_barang ORDER BY kode ");    
    return $query->result();  
  }
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
						WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  

			
	function get_mutasi_pp($date_from, $date_to, $gudang,$kel_brg,$jns_brg) {
	$pencarian ="";
	$dari =	explode('-',$date_from);
	$tanggal1 =$dari[0];
	$bulan1 =$dari[1];
	$tahun1 =$dari[2];
	$exdari =$tahun1."-".$bulan1."-".$tanggal1;
	
	$sampai =	explode('-',$date_to);
	$tanggal2 =$sampai[0];
	$bulan2 =$sampai[1];
	$tahun2 =$sampai[2];
	$exsampai =$tahun2."-".$bulan2."-".$tanggal2;
		
		if($gudang != 0){
			$pencarian .= "AND b.id_gudang = '$gudang'";
			}
			  $query	= $this->db->query(" SELECT f.kode_supplier ,f.nama as nama_supplier FROM tm_pp a 
			INNER JOIN tm_bagian b ON a.id_bagian=b.id 
			INNER JOIN tm_pp_detail c ON c.id_pp=a.id 
			LEFT JOIN tm_op_detail d ON c.id=d.id_pp_detail 
			INNER JOIN tm_op e ON e.id=d.id_op 
			INNER JOIN tm_supplier f ON f.id=e.id_supplier  
			where a.tgl_pp >= '$exdari' AND a.tgl_pp <= '$exsampai' ".$pencarian."
			GROUP BY f.kode_supplier,f.nama
			ORDER BY kode_supplier DESC  ");
		
		$data_pp = array();
		$detail_pp = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				// ambil data detail barangnya
				$query2	= $this->db->query( " SELECT * FROM tm_pp_detail WHERE id_pp = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' " );
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						//$nama_brg	= $hasilrow->nama_brg;
						//$satuan	= $hasilrow->nama_satuan;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' " );
						$hasilrow = $query3->row();
						$satuan	= $hasilrow->nama;
						
						// 11-07-2012
						$sqlxx = " SELECT * FROM tm_op a inner join tm_op_detail b ON a.id=b.id_op
						 where id_pp_detail = '$row2->id' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$op_detail = array();
							foreach ($queryxx->result() as $row13) {
				
				$query33	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelian_detail a 
					INNER JOIN tm_pembelian b ON b.id=a.id_pembelian
					WHERE a.id_op_detail='$row13->id' AND a.id_brg='$row13->id_brg' AND  b.status_aktif='t' ");
					
					if($query33->num_rows()>0) {
						$hasilrow33 = $query33->row();
						$pemenuhan = $hasilrow33->pemenuhan;
						$sisa = ($row13->qty)-$pemenuhan;
					}else{
						$pemenuhan = 0;
						$sisa = ($row13->qty)-$pemenuhan;
						
						if($pemenuhan=='')
							$pemenuhan = 0;
						
						if($sisa=='')
							$sisa = 0;
					}
				$hargapemenuhan=($row13->harga * $pemenuhan);
				$hargasisa=($row13->harga * $sisa);
				
				$op_detail[] = array('no_op'=> $row13->no_op,
					//~ 'tgl_op'=> $row13->tgl_op,
					//~ 'kode_supplier'=> $row13->kode_supplier,
					//~ 'nama_supplier'=> $row13->nama,
					//~ 'status_op'=> $row13->status_op,
					'keterangan'=> $row13->keterangan,
					//~ 'kode_brg' => $row13->kode_brg, 
					//~ 'qty' => $row13->qty, 
					//~ 'nama_brg' => $row13->nama_brg,]
					'harga' => $row13->harga, 
					'pemenuhan' => $pemenuhan,
					'sisa' => $sisa,
					'hargapemenuhan' => $hargapemenuhan,
					'hargasisa' => $hargasisa,
				);
			}
									
							$sql = " SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
									WHERE a.id_pp_detail = '$row2->id' ";
							$query3	= $this->db->query($sql);
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum;
							
							
						}
						else {
							
							$jum_op = 0;
							
						}
						
				
						// ======= end 11-07-2012 =====================
						
						$sisanya = $row2->qty-$jum_op;
						if ($sisanya < 0)
							$sisanya = 0;
						
						$hargajum_op=($row13->harga * $jum_op);
						$hargasisanya=($row13->harga * $sisanya);
						$hargaqty=($row13->harga * $row2->qty);
						
						$count_op = 0;
						$query3	= $this->db->query(" SELECT distinct a.id_op FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE a.id_pp_detail = '$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
							foreach ($hasil3 as $row3) {
								$count_op++;
							}
						}
						else
							$count_op = 0;
						
						if ($row2->is_satuan_lain == 't') {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
							$hasilrow = $query3->row();
							$sat_lain = $hasilrow->nama;
						}
						else
							$sat_lain = '';
				
						$detail_pp[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $row2->nama_brg,
												'satuan'=> $satuan,
												'id_satuan'=> $row2->id_satuan,
												'qty'=> $row2->qty,
												'jum_op'=> $jum_op,
												'sisanya'=> $sisanya,
												'count_op'=> $count_op,
												'keterangan'=> $row2->keterangan,
												'status_faktur'=> $row2->status_faktur,
												'is_satuan_lain'=> $row2->is_satuan_lain,
												'sat_lain'=> $sat_lain,
												'hargajum_op'=> $hargajum_op,
												'hargasisanya'=> $hargasisanya,
												'hargaqty'=> $hargaqty,
												'op_detail'=>$op_detail
											);
										
						//~ $op_detail[]= array();					
					}
				}
				else {
					$detail_pp = '';
				}
				
				$data_pp[] = array('id'=> $row1->id,	
							'no_pp'=> $row1->no_pp,
							'tgl_pp'=> $row1->tgl_pp,
							'kode_bagian'=> $row1->kode_bagian,
							'nama_bagian'=> $row1->nama_bagian,
							'tgl_update'=> $row1->tgl_update,
							'status_faktur'=> $row1->status_faktur,
							'status_edit'=> $row1->status_edit,
							'status_aktif'=> $row1->status_aktif,
							'kode_supplier'=> $row1->kode_supplier,
							'nama_supplier'=> $row1->nama_supplier,
							'detail_pp'=> $detail_pp
						);
				
				$detail_pp = array();
			} // endforeach header
		}
		else {
			$data_pp = '';
		}
		return $data_pp;
  }			
				
  function get_mutasi_pp_old($date_from, $date_to, $gudang,$kel_brg,$jns_brg) {
	
	$dari =	explode('-',$date_from);
	$tanggal1 =$dari[0];
	$bulan1 =$dari[1];
	$tahun1 =$dari[2];
	$exdari =$tahun1."-".$bulan1."-".$tanggal1;
	
	$sampai =	explode('-',$date_to);
	$tanggal2 =$sampai[0];
	$bulan2 =$sampai[1];
	$tahun2 =$sampai[2];
	$exsampai =$tahun2."-".$bulan2."-".$tanggal2;
		
			$query	= $this->db->query(" SELECT * FROM tm_pp where tgl_pp >= '$exdari' 
			AND tgl_pp <= '$exsampai' ORDER BY tgl_pp DESC, no_pp DESC ");
		
		$data_pp = array();
		$detail_pp = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT kode, nama FROM tm_bagian WHERE id = '$row1->id_bagian' " );
				$hasilrow = $query3->row();
				$kode_bagian	= $hasilrow->kode;
				$nama_bagian	= $hasilrow->nama;
				
				// ambil data detail barangnya
				$query2	= $this->db->query( " SELECT * FROM tm_pp_detail WHERE id_pp = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' " );
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						//$nama_brg	= $hasilrow->nama_brg;
						//$satuan	= $hasilrow->nama_satuan;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' " );
						$hasilrow = $query3->row();
						$satuan	= $hasilrow->nama;
						
						// 11-07-2012
						$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$row2->id' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$sql = " SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
									WHERE a.id_pp_detail = '$row2->id' ";
							$query3	= $this->db->query($sql);
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum;
						}
						else {
							
							$jum_op = 0;
						}
						// ======= end 11-07-2012 =====================
						
						$sisanya = $row2->qty-$jum_op;
						if ($sisanya < 0)
							$sisanya = 0;
						
						
						$count_op = 0;
						$query3	= $this->db->query(" SELECT distinct a.id_op FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE a.id_pp_detail = '$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
							foreach ($hasil3 as $row3) {
								$count_op++;
							}
						}
						else
							$count_op = 0;
						
						if ($row2->is_satuan_lain == 't') {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
							$hasilrow = $query3->row();
							$sat_lain = $hasilrow->nama;
						}
						else
							$sat_lain = '';
				
						$detail_pp[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $row2->nama_brg,
												'satuan'=> $satuan,
												'id_satuan'=> $row2->id_satuan,
												'qty'=> $row2->qty,
												'jum_op'=> $jum_op,
												'sisanya'=> $sisanya,
												'count_op'=> $count_op,
												'keterangan'=> $row2->keterangan,
												'status_faktur'=> $row2->status_faktur,
												'is_satuan_lain'=> $row2->is_satuan_lain,
												'sat_lain'=> $sat_lain
											);
					}
				}
				else {
					$detail_pp = '';
				}
				$detail_pp = array();
				$data_pp[] = array('id'=> $row1->id,	
							'no_pp'=> $row1->no_pp,
							'tgl_pp'=> $row1->tgl_pp,
							'kode_bagian'=> $kode_bagian,
							'nama_bagian'=> $nama_bagian,
							'tgl_update'=> $row1->tgl_update,
							'status_faktur'=> $row1->status_faktur,
							'status_edit'=> $row1->status_edit,
							'status_aktif'=> $row1->status_aktif,
							'detail_pp'=> $detail_pp
						);
				
				
			} // endforeach header
		}
		else {
			$data_pp = '';
		}
		return $data_pp;
  }
}
