<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function fprinted($nofaktur) {
		return $this->db->query(" UPDATE tm_faktur SET f_printed='t' WHERE i_faktur_code='$nofaktur' ");
	}
		
	function clistpenjualanndo($nofaktur) {
			
		$query	= $this->db->query(" SELECT a.i_product AS imotif,
					a.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)

				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ");
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function clistpenjualanndo2($nofaktur) {
			
		$query	= $this->db->query(" SELECT a.i_faktur, 
					b.i_faktur_code, 
					a.i_product AS imotif,
					a.e_product_name AS motifname
					
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)

				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel='f' AND d.f_faktur_created='t' 
				
				GROUP BY a.i_faktur, b.i_faktur_code, a.i_product, a.e_product_name " );
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	
	function clistfpenjndo_header($nofaktur) {
		/*
		Ini dipakai utk menmpikan info : Nomor Faktur, Tanggal Faktur, Tgl Jatuh Tempo
		*/
		return $this->db->query(" SELECT count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname,
						b.d_pajak AS dpajak
				
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name, b.d_pajak " );
	}

	function clistfpenjndo_jml($nofaktur) {
		/* 24082011
		return $this->db->query( "
			SELECT 	b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					sum((a.n_quantity * a.v_unit_price)) AS total
			
			FROM tm_faktur_item a
					
			RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
			INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
			
			WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true

			GROUP BY b.n_discount, b.v_discount " );
		*/
		return $this->db->query(" SELECT 	b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					sum((a.n_quantity * a.v_unit_price)) AS total
			
			FROM tm_faktur_item a
					
			RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
			
			WHERE b.i_faktur_code='$nofaktur'

			GROUP BY b.n_discount, b.v_discount ");
	}

	function lbarangjadiperpages($limit,$offset) {
		
		$query	= $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
										
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		return $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}
	
	function flbarangjadi($key) {
		return $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE a.i_faktur_code='$key' AND d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );	
	}
	
	function lklsbrg() {
		$query	= $this->db->query(" SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC " );
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	
	function ititas() {
		return $this->db->query(" SELECT a.* FROM tr_initial_company a ORDER BY i_initial, i_initial_code DESC LIMIT 1 " );
	}
	
	function pelanggan($nofaktur) {
		
		return $this->db->query(" SELECT a.e_customer_name  AS customername,
				   a.e_customer_address AS customeraddress,
				   a.e_customer_npwp AS npwp
			
			FROM tr_customer a
			
			INNER JOIN tr_branch b ON b.i_customer=a.i_customer
			INNER JOIN tm_faktur c ON c.e_branch_name=b.e_initial
			INNER JOIN tm_faktur_item d ON d.i_faktur=c.i_faktur
			
			WHERE c.i_faktur_code='$nofaktur'
			
			GROUP BY a.e_customer_name, a.e_customer_address, a.e_customer_npwp
		");
	}
	
	function pajak($nofaktur) {
		return $this->db->query(" SELECT b.i_faktur_pajak AS ifakturpajak, b.d_pajak
			
			FROM tm_faktur b
			
			INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
			
			INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
			INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
			
			WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true
			
			GROUP BY b.i_faktur_pajak, b.d_pajak ");
	}
	
	/*
	function remote($destination_ip) {
		return $this->db->query(" SELECT * FROM tr_printer WHERE ip='$destination_ip' ORDER BY i_printer DESC LIMIT 1 ");
	}
	*/
	function remote($id) {
		return $this->db->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' ORDER BY i_printer DESC LIMIT 1 ");
	}

	function jmlitemharga($ifaktur,$iproduct) {
		return $this->db->query(" SELECT sum(n_quantity) AS qty, 
					v_unit_price AS unitprice,
					sum(v_unit_price) AS jmlunitprice,
					sum(n_quantity * v_unit_price) AS amount
				
				FROM tm_faktur_item

				WHERE i_faktur='$ifaktur' AND i_product='$iproduct'
				
				GROUP BY i_product, i_faktur, v_unit_price
		");
	}	
}
?>
