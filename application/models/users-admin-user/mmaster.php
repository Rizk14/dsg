<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $groupnya)
  {
	if ($cari == "all") {
		if ($groupnya == '0') {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = b.gid 
							order by a.gid, a.username ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = '$groupnya' AND a.gid = b.gid
							ORDER BY a.gid, a.username ", false)->limit($num,$offset);
		}
	}
	else {
		if ($groupnya == '0') {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = b.gid AND
							(UPPER(a.username) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%') ) ORDER BY a.gid, a.username ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = b.gid AND 
							a.gid = '$groupnya' AND (UPPER(a.username) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%') ) ORDER BY a.gid, a.username ", false)->limit($num,$offset);
		}
    }
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari, $groupnya){
	if ($cari == "all") {
		if ($groupnya == '0') {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = b.gid  ", false);
		}
		else {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = '$groupnya' AND a.gid = b.gid ", false);
		}
	}
	else {
		if ($groupnya == '0') {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = b.gid AND
							(UPPER(a.username) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%') ) ", false);
		}
		else {
			$this->db->select(" a.*, b.nama_group FROM tm_user a, tm_groups b WHERE a.gid = b.gid AND 
							a.gid = '$groupnya' AND (UPPER(a.username) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%') ) ", false);
		}
	}
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get($uid){
	$this->db->select("* from tm_user WHERE uid='$uid'", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  function get_group_user(){
    $this->db->select("* from tm_groups order by gid ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
        
  function cek_data($username){
    $this->db->select("uid from tm_user WHERE username = '$username' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($username,$passwd, $nama, $group_user, $uid_user, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'username'=>$username,
      'passwd'=>$passwd,
      'nama'=>$nama,
      'gid'=>$group_user,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_user',$data); 
	}
	else {
		$data = array(
				  'passwd'=>$passwd,
				  'nama'=>$nama,
				  'tgl_update'=>$tgl
				);
		
		$this->db->where('uid',$uid_user);
		$this->db->update('tm_user',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_user', array('uid' => $kode));
  }

}

