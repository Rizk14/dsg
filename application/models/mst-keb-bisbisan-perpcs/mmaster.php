<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari) {
	if ($cari == "all") {
			$this->db->select(" * FROM tm_keb_bisbisan_perpcs ORDER BY id_ukuran_bisbisan, kode_brg_jadi, kode_brg ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
	else {			
			$this->db->select(" a.* FROM tm_keb_bisbisan_perpcs a
							LEFT JOIN tm_barang b ON b.kode_brg=a.kode_brg
							LEFT JOIN tr_product_motif d ON d.i_product_motif = a.kode_brg_jadi
							WHERE UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') 
							OR UPPER(b.nama_brg) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%') 
							ORDER BY d.e_product_motifname, b.nama_brg ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				// brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$row1->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
				}
				else {
					$nama_brg_jadi	= '';
				}
				
				// ambil nama ukuran bisbisan
				$query3	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan 
								WHERE id = '$row1->id_ukuran_bisbisan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_ukuran	= $hasilrow->nama;
				}
				else {
					$nama_ukuran	= '';
				}
				
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'id_ukuran_bisbisan'=> $row1->id_ukuran_bisbisan,
											'nama_ukuran'=> $nama_ukuran,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,	
											'nama_brg_jadi'=> $nama_brg_jadi,	
											'kode_brg'=> $row1->kode_brg,	
											'nama_brg'=> $nama_brg,	
											'tgl_update'=> $tgl_update,
											'jum_kebutuhan'=> $row1->jum_kebutuhan
											); 
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($cari){
	if ($cari == "all") {
			$query	= $this->db->query(" SELECT * FROM tm_keb_bisbisan_perpcs ");
	}
	else { 
			$query	= $this->db->query(" SELECT a.* FROM tm_keb_bisbisan_perpcs a
							LEFT JOIN tm_barang b ON b.kode_brg=a.kode_brg
							LEFT JOIN tr_product_motif d ON d.i_product_motif = a.kode_brg_jadi
							WHERE UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') 
							OR UPPER(b.nama_brg) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
        
  //
  function save($kode_brg_jadi, $kode, $nama, $ukuran, $keb){  
    $tgl = date("Y-m-d");
			
	$query3	= $this->db->query(" SELECT id from tm_keb_bisbisan_perpcs WHERE id_ukuran_bisbisan = '$ukuran' 
						AND kode_brg_jadi = '$kode_brg_jadi' 
						AND kode_brg = '$kode' ");
	if ($query3->num_rows() == 0){
		$data_header = array(
			  'kode_brg_jadi'=>$kode_brg_jadi,
			  'kode_brg'=>$kode,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'id_ukuran_bisbisan'=>$ukuran,
			  'jum_kebutuhan'=>$keb
			);
		$this->db->insert('tm_keb_bisbisan_perpcs',$data_header);
	}
  }
    
  function delete($kode){    	
	//---------------------------------------------	
	// dan hapus datanya 
	$this->db->query(" DELETE FROM tm_keb_bisbisan_perpcs where id = '$kode' ");
  }
        
  function get_kebutuhan($id_keb){
	$query	= $this->db->query(" SELECT * FROM tm_keb_bisbisan_perpcs where id='$id_keb' ");    
	
	$data_keb = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				// ambil nama ukuran bisbisan
				$query3	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan 
								WHERE id = '$row1->id_ukuran_bisbisan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_ukuran	= $hasilrow->nama;
				}
				else {
					$nama_ukuran	= '';
				}
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
												
				// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row1->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
			
				$data_keb[] = array(		'id'=> $row1->id,	
											'nama_ukuran'=> $nama_ukuran,	
											'kode_brg_jadi'=> $row1->kode_brg_jadi,	
											'nama_brg_jadi'=> $nama_brg_jadi,	
											'kode_brg'=> $row1->kode_brg,	
											'nama_brg'=> $nama_brg,	
											'jum_kebutuhan'=> $row1->jum_kebutuhan
											);
			} // endforeach header
	}
    return $data_keb; // 
  }
    
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_bahan($num, $offset, $cari, $quilting, $kel_brg, $id_jenis_bhn)
  {
      if ($cari == "all") {
		if ($quilting == 'f') {
			$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
			
			//$sql.=" order by b.kode, c.kode, a.tgl_update DESC";
			$sql.=" order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id ";
			
			$sql.=" order by a.kode_brg ";
		}
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($quilting == 'f') {		
			$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')				
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
			order BY a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.kode_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'nama_satuan'=> $row1->nama_satuan,
										'tgl_update'=> $row1->tgl_update
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $quilting, $kel_brg, $id_jenis_bhn){
	  if ($cari == "all") {
		if ($quilting == 'f') {
			$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')				
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($quilting == 'f') {		
			$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')				
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  function cek_data($kode_brg_jadi, $kode_bhn_baku){
    $this->db->select("id from tm_marker_gelaran WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '$kode_bhn_baku' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_ukuran_bisbisan(){
    $this->db->select("* from tm_ukuran_bisbisan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
