<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	// 18-03-2014
	function get_do($num, $offset, $cari) {	
		 $db2 = $this->load->database('db_external', TRUE);	
		$sql = " * FROM tm_do WHERE f_do_cancel='f' AND f_printed = 'f' ";
		if ($cari != "all")
			$sql.=" AND i_do_code like '%$cari%' ";
		$sql.= " ORDER BY i_do DESC ";
		$db2->select($sql, false)->limit($num,$offset);	
		$query = $db2->get();
		$datado = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query3	= $db2->query(" SELECT count(i_do_item) as jumitem FROM tm_do_item WHERE i_do = '$row1->i_do' ");
				$hasilrow = $query3->row();
				$jumitem	= $hasilrow->jumitem;
				
				// nama customer dan branch
				$query3	= $db2->query(" SELECT e_branch_name FROM tr_branch WHERE i_customer = '$row1->i_customer'
							AND i_branch_code='$row1->i_branch' ");
				$hasilrow = $query3->row();
				$e_branch_name	= $hasilrow->e_branch_name;
				
				$datado[] = array(			'i_do'=> trim($row1->i_do),	
											'i_do_code'=> trim($row1->i_do_code),	
											'd_do'=> $row1->d_do,	
											'e_branch_name'=> $e_branch_name,
											'jumitem'=> $jumitem
											);
			} // endforeach header
	}
	else {
			$datado = '';
	}
		return $datado;	
	}
	
  function get_dotanpalimit($cari){
	   $db2 = $this->load->database('db_external', TRUE);
		$sql = " SELECT * FROM tm_do WHERE f_do_cancel='f' AND f_printed = 'f' ";
		if ($cari != "all")
			$sql.=" AND i_do_code like '%$cari%' ";
		$sql.= " ORDER BY i_do DESC ";
		$query = $db2->query($sql);
		
		return $query->result();  
  }

	// 19-03-2014
	function cnmrop($kddo) {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT b.i_op AS iop,  e.i_op_code AS iopcode FROM tm_do_item b 
				
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
			
			WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' ");
					
	}
	
	function ctgldobrg($kddo) {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query( " SELECT a.d_do AS tgl
		
			FROM tm_do_item b
				
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' " );
	}
	
	function getcabang($kddo){
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT  a.i_do_code AS iopcode, e.e_branch_name AS cabangname
			
			FROM tm_do_item b
			
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch
			INNER JOIN tr_customer f ON f.i_customer=e.i_customer
			
			WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' GROUP BY a.i_do_code, e.e_branch_name ");
	}
	
	function remote($id, $ip_address) {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' AND ip='$ip_address' ORDER BY i_printer DESC LIMIT 1 ");
	}

	function getinitial() {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial_code DESC LIMIT 1");
	}	

	function fprinted($kddo){
		 $db2 = $this->load->database('db_external', TRUE);
			$db2->query(" UPDATE tm_do SET f_printed='t' WHERE i_do_code='$kddo' ");
	}
	
	function clistdobrg($kddo) {
		 $db2 = $this->load->database('db_external', TRUE);
		// 08-09-2014, tambahin field i_do_item
		$qrystr	= " SELECT  a.d_do AS dop,
					a.i_do_code AS iopcode,
					a.i_branch AS ibranch,
					e.i_op_code AS iop,
					b.i_do_item,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					sum(b.n_deliver) AS qty,
					sum(b.n_residual) AS belumfaktur,
					b.e_note AS keterangan,
					e.i_op_code AS iopcode
				
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
				INNER JOIN tm_op e ON e.i_op=b.i_op
				
				WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f'

				GROUP BY a.d_do, a.i_do_code, a.i_branch, b.i_op, b.i_do_item, b.i_product, c.e_product_motifname, b.e_note, e.i_op_code 
				
				ORDER BY b.i_product ASC ";
						
		$query	= $db2->query($qrystr);

		if($query->num_rows() > 0 )	{
			return $result	= $query->result();
		}	
	}
}
?>
