<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}

  
  function get_unit_jahit(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit ");    
    return $query->result();  
  }  
  
  function get_unit_packing(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit ");    
    return $query->result();  
  }  
  
  	function get_all_sjpembelian($date_from, $date_to, $unit_jahit, $unit_packing){
		
			if ($unit_jahit != 0 && $unit_packing == 0){
			
				$sql = " SELECT id, no_sjmasukpembelian as no_bukti, tgl_sjpembelian as tgl_bukti, total, status_lunas,
						0 as pembulatan, 1 as pembelian, 'xx' as deskripsi
						FROM tm_pembelian_wip WHERE status_aktif = 't'
						AND tgl_sjpembelian >= to_date('$date_from','dd-mm-yyyy') 
						AND tgl_sjpembelian <= to_date('$date_to','dd-mm-yyyy')
						AND id_unit_jahit = '$unit_jahit' ";
					}
					
				elseif ($unit_packing != 0 && $unit_jahit == 0){
			
				$sql = " SELECT id, no_sjmasukpembelianpack as no_bukti, tgl_sjpembelianpack as tgl_bukti, total, status_lunas,
						0 as pembulatan, 1 as pembelian, 'xx' as deskripsi
						FROM tm_pembelianpack_wip WHERE status_aktif = 't'
						AND tgl_sjpembelianpack >= to_date('$date_from','dd-mm-yyyy') 
						AND tgl_sjpembelianpack <= to_date('$date_to','dd-mm-yyyy')
						AND id_unit_packing = '$unit_packing' ";
					}
					
			$sql.= " UNION SELECT a.id, a.no_voucher as no_bukti, a.tgl as tgl_bukti, 
					b.jumlah_bayar as total, '0' as status_lunas,
					b.pembulatan as pembulatan, 0 as pembelian, b.deskripsi as deskripsi
						FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
						WHERE a.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl <= to_date('$date_to','dd-mm-yyyy')
						AND b.id_unit_jahit = '$unit_jahit' AND b.id_unit_packing = '$unit_packing'
					ORDER BY tgl_bukti ASC, no_bukti ASC ";
			
			$query	= $this->db->query($sql);
				
			$data_beli = array();
			if ($query->num_rows() > 0){
				$hasil = $query->result();
				foreach ($hasil as $row1) {
					$list_brg = "";
					if ($unit_jahit != 0 && $unit_packing == 0){
						$sql2 = " SELECT nama_brg FROM tm_pembelian_wip_detail WHERE id_pembelian_wip = '$row1->id' ";
				}
				elseif ($unit_packing != 0 && $unit_jahit == 0){
						$sql2 = " SELECT nama_brg FROM tm_pembelianpack_wip_detail WHERE id_pembelianpack_wip = '$row1->id' ";
					}
					$query2	= $this->db->query($sql2);
					if ($query2->num_rows() > 0) {
						$hasil2 = $query2->result();
						$hitung = count($hasil2);
						$k = 0;
						
						foreach ($hasil2 as $row2) {
							$list_brg.= $row2->nama_brg;
							if ($k<$hitung-1)
								$list_brg.= ", ";
						    $k++;
						} // end for2
					} // end if2
					
					$pisah1 = explode("-", $row1->tgl_bukti);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_bukti = $tgl1."-".$bln1."-".$thn1;
					
					if ($row1->pembelian == 1) {
						$pembelian = $row1->total;
						$pelunasan = '';
					}
					else {
						$pelunasan = $row1->total;
						$pembelian = '';
					}
					$data_beli[] = array(	'no_bukti'=> $row1->no_bukti,
											'tgl_bukti'=> $tgl_bukti,
											'pembelian'=> $pembelian,
											'pelunasan'=> $pelunasan,
											'pembulatan'=> $row1->pembulatan,
											'is_pembelian'=> $row1->pembelian,
											'deskripsi_pelunasan'=> $row1->deskripsi,
											'list_brg'=> $list_brg,
											'status_lunas'=> $row1->status_lunas
											);
				
					$list_brg = "";
				}
			}
			else
				$data_beli = '';
		
		
		// --------------------- END 04-12-2015 ----------------------------------------------------
		return $data_beli;
  }
    
}

