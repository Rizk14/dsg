<?php
class Mmaster extends CI_Model {
	
  function __construct() { 

  parent::__construct();

}  
  
  function getAll($num, $offset, $unit_packing, $cari) {	

		$data_masuk = array();
		$data_masuk_detail = array();
		
		if ($cari=="all") {
			if ($unit_packing=='0') {
				$this->db->select(" * FROM tm_sj_hasil_packing ORDER BY id DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			}
			else {
				$this->db->select(" * FROM tm_sj_hasil_packing WHERE kode_unit='$unit_packing' ORDER BY id DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			}
		}
		else {
			if ($unit_packing!='0') {
				$this->db->select(" * FROM tm_sj_hasil_packing WHERE kode_unit='$unit_packing' AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			}
			else {
				$this->db->select(" * FROM tm_sj_hasil_packing WHERE UPPER(no_sj) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			}
		}

		if ($query->num_rows() > 0) {
			
			$hasil = $query->result();
			
			foreach ($hasil as $row1) {

				$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit='$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing_detail WHERE id_sj_hasil_packing='$row1->id' ");
				
				if ($query2->num_rows() > 0) {
					
					$hasil2=$query2->result();
										
					$id_detailnya = "";
					
					foreach ($hasil2 as $row2) {			
									
						$id_detailnya.= $row2->id_sj_proses_packing_detail."-";
						
						$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg	= '';
						}
						
						$query4	= $this->db->query(" SELECT a.no_sj, a.tgl_sj FROM tm_sj_proses_packing a 
						INNER JOIN tm_sj_proses_packing_detail b ON a.id=b.id_sj_proses_packing WHERE b.id='$row2->id_sj_proses_packing_detail' ");
						
						if ($query4->num_rows() > 0) {
							$hasilrow = $query4->row();
							$no_sj_keluar	= $hasilrow->no_sj;
							$tgl_sj_keluar = $hasilrow->tgl_sj;
						}
						else {
							$no_sj_keluar	= '';
							$tgl_sj_keluar = '';
						}
						
						$data_masuk_detail[] = array(
								'no_sj_keluar'=> $no_sj_keluar,
								'tgl_sj_keluar'=> $tgl_sj_keluar,
								'kode_brg'=> $row2->kode_brg_jadi,
								'nama'=> $nama_brg,
								'qty'=> $row2->qty,
								'sisa_qty'=> $row2->sisa_qty,
								'id_detailnya'=> $id_detailnya);
					}
				}
				else {
					$data_masuk_detail = '';
				}
				
				/***
				$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sj_proses_packing WHERE id='$row1->id_sj_proses_packing' ");
				if ($query3->num_rows() > 0) {
					$hasilrow = $query3->row();
					$no_sj_keluar	= $hasilrow->no_sj;
					$tgl_sj_keluar = $hasilrow->tgl_sj;
				}
				else {
					$no_sj_keluar	= '';
					$tgl_sj_keluar = '';
				}
				***/
				
				$data_masuk[] = array('id'=> $row1->id,	
							'id_sj_proses_packing'=> $row1->id_sj_proses_packing,	
							'no_sj'=> $row1->no_sj,
							'tgl_sj'=> $row1->tgl_sj,
							'kode_unit'=> $row1->kode_unit,
							'nama_unit'=> $nama_unit,
							'keterangan'=> $row1->keterangan,
							'tgl_update'=> $row1->tgl_update,
							'data_masuk_detail'=> $data_masuk_detail,
							'id_detailnya'=> $id_detailnya );
								
				$id_detailnya = "";
				
				$data_masuk_detail = array();
			}
		}
		else {
			$data_masuk = '';
		}
		
		return $data_masuk;
  }
  
  function getAlltanpalimit($unit_packing, $cari) {
	
	if ($cari=="all") {
		if ($unit_packing=='0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing WHERE kode_unit='$unit_packing' ");
	}
	else {
		if ($unit_packing!='0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing WHERE kode_unit='$unit_packing' 
			AND UPPER(no_sj) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing WHERE UPPER(no_sj) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
 
  function get_packing(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit ASC ");    
    return $query->result();  
  } 
   
  function generate_nomor(){
			$th_now	= date("Y");
			
			$query3	= $this->db->query(" SELECT no_sj FROM tm_sj_hasil_packing ORDER BY id DESC, no_sj DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
			if(strlen($no_sj)==13) {
				$nosj = substr($no_sj, 4, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}
			$nomorsj = "SJM-".$nomorsj;

			return $nomorsj;  
  }
     
  function cek_data($no_sj){
    $this->db->select("id from tm_sj_hasil_packing WHERE no_sj='$no_sj' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // function save($id_sj_keluar, $no_sj_masuk, $no_sj_keluar, $tgl_sj, $unit_packing, $ket, $totalnya, $kode, $id_sj_keluar_detail, $qty, $biaya, $gud){
  // function save($id_sj_keluar, $no_sj_masuk, $no_sj_keluar, $tgl_sj, $unit_packing, $ket, $kode, $id_sj_keluar_detail, $qty, $gud) {
  function save($no_sj_masuk, $tgl_sj, $unit_packing, $ket, $kode, $id_sj_keluar_detail, $qty, $gud) {
		  
    $tgl = date("Y-m-d");
    
    $this->db->select("id from tm_sj_hasil_packing WHERE no_sj='$no_sj_masuk' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
	
		if(count($hasil)==0){
			/*
			$data_header = array(
			  'id_sj_proses_packing'=>$id_sj_keluar,
			  'no_sj'=>$no_sj_masuk,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_packing,
			  'keterangan'=>$ket,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'total'=>$totalnya
			);
			*/
			
			/***
			$data_header = array(
			  'id_sj_proses_packing'=>$id_sj_keluar,
			  'no_sj'=>$no_sj_masuk,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_packing,
			  'keterangan'=>$ket,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			**/

			$data_header = array(
			  'no_sj'=>$no_sj_masuk,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_packing,
			  'keterangan'=>$ket,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
									
			$this->db->insert('tm_sj_hasil_packing',$data_header);
			
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_packing ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			
			$id_sj_hsl_pck	= $hasilrow->id;
			
			if ($kode!='' && $qty!='') {
				if($gud=='')
					$gud = 0;
  				/*
				$data_detail = array(
					'id_sj_hasil_packing'=>$id_sj_hsl_pck,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qty,
					'id_sj_proses_packing_detail'=>$id_sj_keluar_detail,
					'biaya'=>$biaya,
					'from_gudang'=>$gud
				);
				*/
				$data_detail = array(
					'id_sj_hasil_packing'=>$id_sj_hsl_pck,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qty,
					'sisa_qty'=>$qty,
					'id_sj_proses_packing_detail'=>$id_sj_keluar_detail,
					'from_gudang'=>$gud
				);
								
				$this->db->insert('tm_sj_hasil_packing_detail',$data_detail);
				
				$qstokhsilpacking	= $this->db->query(" SELECT * FROM tm_stok_hasil_packing WHERE kode_brg_jadi='$kode' ORDER BY id DESC LIMIT 1 ");
				if($qstokhsilpacking->num_rows()>0){
					$rstokhsilpacking	= $qstokhsilpacking->row();
					$qtystokhslpacking	= $rstokhsilpacking->stok;
					$idstokhslpacking	= $rstokhsilpacking->id;
					
					$stokhsilpacking_upt	= $qtystokhslpacking+$qty;
				
					$this->db->query(" UPDATE tm_stok_hasil_packing SET stok='$stokhsilpacking_upt', 
								tgl_update_stok='$tgl' WHERE kode_brg_jadi='$kode' AND id='$idstokhslpacking' ");
				}
				else {
					$data_detail = array(
						'kode_brg_jadi'=>$kode,
						'stok'=>$qty,
						'tgl_update_stok'=>$tgl
					);
					$this->db->insert('tm_stok_hasil_packing',$data_detail);
				}
				
			}
		} // end if count = 0
		else {
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_packing ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj_hsl_pck	= $hasilrow->id;
			
			if ($kode!='' && $qty!='') {
				$data_detail = array(
					'id_sj_hasil_packing'=>$id_sj_hsl_pck,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qty,
					'sisa_qty'=>$qty,
					'id_sj_proses_packing_detail'=>$id_sj_keluar_detail,
					'from_gudang'=>$gud
				);
								
				$this->db->insert('tm_sj_hasil_packing_detail',$data_detail);
			}

			$qstokhsilpacking	= $this->db->query(" SELECT * FROM tm_stok_hasil_packing WHERE kode_brg_jadi='$kode' ORDER BY id DESC LIMIT 1 ");
			if($qstokhsilpacking->num_rows()>0){
				$rstokhsilpacking	= $qstokhsilpacking->row();
				$qtystokhslpacking	= $rstokhsilpacking->stok;
				$idstokhslpacking	= $rstokhsilpacking->id;
				
				$stokhsilpacking_upt	= $qtystokhslpacking+$qty;
				$this->db->query(" UPDATE tm_stok_hasil_packing SET stok='$stokhsilpacking_upt', tgl_update_stok='$tgl' WHERE kode_brg_jadi='$kode' AND id='$idstokhslpacking' ");
			
			}else{
				$data_detail = array(
						'kode_brg_jadi'=>$kode,
						'stok'=>$qty,
						'tgl_update_stok'=>$tgl
				);
				$this->db->insert('tm_stok_hasil_packing',$data_detail);
			}

		}
  }
   
  function delete($kode, $id_sj_proses_packing_detail) {    
	  
	  $tgl = date("Y-m-d");
	  $id_brg = explode("-", $id_sj_proses_packing_detail);
	  
	  $query3	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing WHERE id='$kode' ");
	  $hasilrow = $query3->row();
	  
	  foreach($id_brg as $row1) {
		
		if($row1!='') {
			$qsjprosespackingdetail = $this->db->query(" SELECT * FROM tm_sj_proses_packing_detail WHERE status_packing='f' AND  id='$row1' ");
			
			if($qsjprosespackingdetail->num_rows()>0) {
				$rsjprosespackingdetail = $qsjprosespackingdetail->row();
				
				$qsjprosespacking = $this->db->query(" SELECT * FROM tm_sj_proses_packing 
				
				WHERE id='$rsjprosespackingdetail->id_sj_proses_packing' AND status_packing='t' ");
				
				if($qsjprosespacking->num_rows()>0) {
					
					$rsjprosespacking = $qsjprosespacking->row();
					
					$this->db->query(" UPDATE tm_sj_proses_packing SET status_packing='f', status_edit='f' WHERE id='$rsjprosespacking->id' AND status_packing='t' "); 
				}
				
				if ($row1!='') {
					$this->db->query(" UPDATE tm_sj_proses_packing_detail SET status_packing='f' WHERE id='$row1' ");
				}
				
			}
		}
	  }
	  
	  $query3	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing WHERE id='$kode' ");
	  $hasilrow = $query3->row();
	  
	  $id_sj	= $hasilrow->id; 
	  $nosj		= $hasilrow->no_sj; 
	  
	  $query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing_detail WHERE id_sj_hasil_packing='$id_sj' ");
	  if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
										
			foreach ($hasil2 as $row2) {

				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_packing WHERE kode_brg_jadi='$row2->kode_brg_jadi' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama-($row2->qty);
				
				if ($query3->num_rows()==0){
					$data_stok = array(
						'kode_brg_jadi'=>$row2->kode_brg_jadi,
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
						
					$this->db->insert('tm_stok_hasil_packing',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok_hasil_packing SET stok='$new_stok', tgl_update_stok='$tgl' WHERE kode_brg_jadi='$row2->kode_brg_jadi' ");
				}

				//$ttstok	= $this->db->query(" SELECT * FROM tt_stok_hasil_packing WHERE kode_brg_jadi='$row2->kode_brg_jadi' AND no_bukti='$nosj' AND id_gudang='$row2->from_gudang' ");
				
				//if($ttstok->num_rows()>0){
					//$rttstok	= $ttstok->row();
					//$masuk_awal	= $rttstok->masuk;
					//$saldo_awal	= $rttstok->saldo;
					//$saldonya	= $saldo_awal-$masuk_awal;
					
					//$this->db->query(" INSERT INTO tt_stok_hasil_packing(kode_brg_jadi,tgl_input,no_bukti,keluar,saldo,id_gudang,biaya) VALUES('$row2->kode_brg_jadi','$tgl','$nosj','$masuk_awal','$saldonya','$row2->from_gudang','$row2->biaya') ");
					//$this->db->query(" INSERT INTO tt_stok_hasil_packing(kode_brg_jadi,tgl_input,no_bukti,keluar,saldo,id_gudang,biaya) VALUES('$row2->kode_brg_jadi','$tgl','$nosj','$row2->qty','$saldonya','$row2->from_gudang','$row2->biaya') ");						
				//}else{
					
					//$this->db->query(" INSERT INTO tt_stok_hasil_packing(kode_brg_jadi,tgl_input,no_bukti,keluar,saldo,id_gudang) VALUES('$row2->kode_brg_jadi','$tgl','$nosj','$row2->qty','$new_stok','$row2->from_gudang') ");		
				//}
			}
	 }
	 
	 $this->db->delete('tm_sj_hasil_packing_detail', array('id_sj_hasil_packing' => $kode));
	 $this->db->delete('tm_sj_hasil_packing', array('id' => $kode));
  } 
  
  
  function get_sj($id_sj) {

    $data_sj = array();
    $detail_sj = array();
    
	$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing WHERE id='$id_sj' ");    
    $hasil = $query->result();
	
	foreach ($hasil as $row1) {
	
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_packing_detail WHERE id_sj_hasil_packing='$row1->id' ");
				
				if ($query2->num_rows() > 0) {
					
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						
						$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg	= '';
						}
						
						/***
						$query4	= $this->db->query(" SELECT a.no_sj, a.tgl_sj FROM tm_sj_proses_packing a 
						INNER JOIN tm_sj_proses_packing_detail b ON a.id=b.id_sj_proses_packing WHERE b.id='$row2->id_sj_proses_packing_detail' ");
						***/
						
						/***
						$query4	= $this->db->query(" SELECT b.id FROM tm_sj_proses_packing a 
						INNER JOIN tm_sj_proses_packing_detail b ON a.id=b.id_sj_proses_packing WHERE b.id='$row2->id_sj_proses_packing_detail' ");
						***/

						$query4	= $this->db->query(" SELECT a.no_sj FROM tm_sj_proses_packing a 
						INNER JOIN tm_sj_proses_packing_detail b ON a.id=b.id_sj_proses_packing WHERE b.id='$row2->id_sj_proses_packing_detail' ");
																		
						if ($query4->num_rows() > 0) {
							$hasilrow = $query4->row();
							$no_sj_keluar	= $hasilrow->no_sj;
						}
						else {
							$no_sj_keluar	= '';
						}
												
						/***
						$detail_sj[] = array('id' => $row2->id,
								'id_sj_hasil_packing' => $row2->id_sj_hasil_packing,
								'kode_brg_jadi' => $row2->kode_brg_jadi,
								'nama' => $nama_brg,
								'qty' => $row2->qty,
								'id_sj_proses_packing_detail' => $row2->id_sj_proses_packing_detail,
								'biaya' => $row2->biaya,
								'gudang' => $row2->from_gudang );
						***/
						
						$detail_sj[] = array('id' => $row2->id,
								'id_sj_hasil_packing' => $row2->id_sj_hasil_packing,
								'no_sj_keluar'=> $no_sj_keluar,				
								'kode_brg_jadi' => $row2->kode_brg_jadi,
								'nama' => $nama_brg,
								'qty' => $row2->qty,
								'id_sj_proses_packing_detail' => $row2->id_sj_proses_packing_detail,
								'gudang' => $row2->from_gudang );
														
					}
				}
				else {
					$detail_sj = '';
				}
		

				$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit='$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				/***
				$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sj_proses_packing WHERE id='$row1->id_sj_proses_packing' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$no_sj_keluar	= $hasilrow->no_sj;
					$tgl_sj_keluar	= $hasilrow->tgl_sj;
				}
				else {
					$no_sj_keluar	= '';
					$tgl_sj_keluar	= '';
				}
				***/
				
				/***
				$data_sj[]	= array('id'=> $row1->id,	
						'id_sj_proses_packing'=> $row1->id_sj_proses_packing,	
						'no_sj'=> $row1->no_sj,
						'tgl_sj'=> $row1->tgl_sj,
						'no_sj_keluar'=> $no_sj_keluar,
						'tgl_sj_keluar'=> $tgl_sj_keluar,
						'kode_unit'=> $row1->kode_unit,
						'nama_unit'=> $nama_unit,
						'keterangan'=> $row1->keterangan,
						'tgl_update'=> $row1->tgl_update,
						'total'=> $row1->total,
						'detail_sj'=> $detail_sj );
				***/	
							
				$data_sj[]	= array('id'=> $row1->id,	
					'id_sj_proses_packing'=> $row1->id_sj_proses_packing,	
					'no_sj'=> $row1->no_sj,
					'tgl_sj'=> $row1->tgl_sj,
					'kode_unit'=> $row1->kode_unit,
					'nama_unit'=> $nama_unit,
					'keterangan'=> $row1->keterangan,
					'tgl_update'=> $row1->tgl_update,
					'detail_sj'=> $detail_sj );
				
				$detail_sj = array();
				
			}
	
	return $data_sj;
	
  }
  
  function get_unit_quilting() {
	
    $this->db->select(" * from tm_unit_quilting order by kode_unit ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
	
  }
  
  function get_kel_brg() {
	
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
	
  }
  
  function get_jenis_makloon() {
	  
    $this->db->select("* from tm_jenis_makloon order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0) {
		return $query->result();
	}
	
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_bhn)
  {
	if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_bhn) {
	
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  function get_sj_keluar($num, $offset, $cari, $unit_packing)
  {
	if ($cari=="all") {
		$sql = " * FROM tm_sj_proses_packing WHERE kode_unit='$unit_packing' AND status_packing='f' ORDER BY no_sj DESC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tm_sj_proses_packing WHERE kode_unit='$unit_packing' AND status_packing='f' ";
		$sql.=" AND UPPER(no_sj) like UPPER('%$cari%')) order by no_sj DESC ";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$gudang	= '';
		
		foreach($hasil as $row1) {		
				
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_packing_detail WHERE id_sj_proses_packing='$row1->id' AND status_packing='f' ");
				
				$jmlqry	= $query2->num_rows();
				$jmlnya	= 0;
				
				if ($jmlqry > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						
						$terpenuhi = 0;
						
						$gudang	.= $row2->from_gudang.'-';
						$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->e_product_motifname;
						}
						
						$query3	= $this->db->query(" SELECT sum(qty) AS jml FROM tm_sj_hasil_packing_detail WHERE id_sj_proses_packing_detail='$row2->id' AND kode_brg_jadi='$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$jum_hasil = $hasilrow->jml;
						
						if ($jum_hasil==0)
							$qty_belum_pack = $row2->qty;
						else
							$qty_belum_pack = $row2->qty-$jum_hasil;
						
						if($qty_belum_pack==0){
							$terpenuhi	= 1;
							$jmlnya+=1;
						}
							
						$detail_bhn[] = array('id' => $row2->id,
								'id_sj_proses_packing' => $row2->id_sj_proses_packing,
								'kode_brg_jadi' => $row2->kode_brg_jadi,
								'nama_brg' => $nama_brg,
								'qty' => $qty_belum_pack,
								'terpenuhi' => $terpenuhi );
					}
				} 
				else {
					$detail_bhn = '';
				}
			
			if($jmlnya==$jmlqry){
				$terpenuhi=1;
			}else{
				$terpenuhi=0;
			}
			
			$data_bhn[] = array('id' => $row1->id,	
							'no_sj' => $row1->no_sj,	
							'tgl_sj' => $row1->tgl_sj,
							'kode_unit' => $row1->kode_unit,
							'tgl_update' => $row1->tgl_update,
							'keterangan' => $row1->keterangan,
							'detail_bhn' => $detail_bhn,
							'gudang' => $gudang,
							'terpenuhi' => $terpenuhi );
			
			$detail_bhn = array();
								
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_sj_keluartanpalimit($cari, $unit_packing){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_sj_proses_packing WHERE kode_unit='$unit_packing' AND status_packing='f' ORDER BY no_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_sj_proses_packing WHERE kode_unit='$unit_packing' AND status_packing='f' ";
		$sql.=" AND UPPER(no_sj) like UPPER('%$cari%')) ORDER BY no_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  } 
  
  function get_detail_sj_keluar($isjproseshader, $id_proses_packing_detail, $unit_packing, $gudang) {
	
    $detail_sj = array();
    
    $j = 0;
    
    foreach($id_proses_packing_detail as $row1) {
		
			if ($row1!='') {
				
				$query1		= $this->db->query(" SELECT no_sj FROM tm_sj_proses_packing WHERE id='$isjproseshader[$j]' ");
				$hasilrow1 	= $query1->row();
				
				$query2		= $this->db->query(" SELECT id, kode_brg_jadi, qty, from_gudang FROM tm_sj_proses_packing_detail WHERE id='$row1' AND status_packing='f' ");
				$hasilrow 	= $query2->row();
				
				$kode_brg	= $hasilrow->kode_brg_jadi;
				$qty		= $hasilrow->qty;
				$gud		= $hasilrow->from_gudang;
						
				$query3	= $this->db->query(" SELECT sum(qty) AS jml FROM tm_sj_hasil_packing_detail WHERE id_sj_proses_packing_detail='$hasilrow->id' AND kode_brg_jadi='$kode_brg' ");
				$hasilrow2 = $query3->row();
				
				$jum_hasil = $hasilrow2->jml;
						
				$qty_belum_pack = $qty-$jum_hasil;
						
				$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$kode_brg' ");
				
				if ($query3->num_rows() > 0){
					$hasilrow3 = $query3->row();
					$nama_brg	= $hasilrow3->e_product_motifname;
				}
				else {
					$nama_brg	= '';
				}
		
				$detail_sj[] = array('id'=> $hasilrow->id,
						'no_sj'=>$hasilrow1->no_sj,
						'kode_brg'=> $kode_brg,
						'nama'=> $nama_brg,
						'qty'=> $qty_belum_pack,
						'gudang' => $gud);
			}
			
			$j+=1;
			
	}
	
	return $detail_sj;
  }

}
