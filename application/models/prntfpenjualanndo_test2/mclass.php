<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function clistpenjualanndo($nofaktur)
	{
		$db2 = $this->load->database('db_external', TRUE);
		// OLD 29-05-2013
		/*$query	= $db2->query( "
				SELECT 	c.i_product AS imotif,
					a.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)

				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel='f' AND d.f_faktur_created='t' 

				GROUP BY c.i_product, a.e_product_name, a.n_quantity, a.v_unit_price " ); */
		$query	= $db2->query("
				SELECT 	a.i_product AS imotif,
					a.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				WHERE b.i_faktur_code='$nofaktur' 
				GROUP BY a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ");

		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function clistpenjualanndo2($nofaktur)
	{
		$db2 = $this->load->database('db_external', TRUE);
		// OLD 29-05-2013
		/*$query	= $db2->query(" SELECT a.i_faktur, b.i_faktur_code, 
				a.i_product AS imotif,
				a.e_product_name AS motifname
						
				FROM tm_faktur_item a
								
				INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)	
				
				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel='f' AND d.f_faktur_created='t'

				GROUP BY a.i_faktur, b.i_faktur_code, a.i_product, a.e_product_name "); */
		$query	= $db2->query(" SELECT a.i_faktur, b.i_faktur_code, 
				a.i_product AS imotif,
				a.e_product_name AS motifname
						
				FROM tm_faktur_item a
								
				INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				WHERE b.i_faktur_code='$nofaktur' 
				GROUP BY a.i_faktur, b.i_faktur_code, a.i_product, a.e_product_name ");

		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function clistfpenjndo_header($nofaktur)
	{
		$db2 = $this->load->database('db_external', TRUE);
		// OLD 29-05-2013
		/*return $db2->query( "
		
				SELECT	count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname,
						b.e_note_faktur as enote
						
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name, b.e_note_faktur " ); */
		return $db2->query("
		
				SELECT	count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname,
						b.e_note_faktur as enote
						
				FROM tm_faktur_item a
								
				RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				
				WHERE b.i_faktur_code='$nofaktur'
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name, b.e_note_faktur ");

		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	/*** 18072011
	function clistfpenjndo_jml($nofaktur) {
		return $db2->query( "
			SELECT 	b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					sum((a.n_quantity * a.v_unit_price)) AS total
			
			FROM tm_faktur_item a
					
			RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
			INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
			
			WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel='f' AND d.f_faktur_created='t'

			GROUP BY b.n_discount, b.v_discount " );
	}
	 ***/
	function clistfpenjndo_jml($nofaktur)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT 	b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					sum((a.n_quantity * a.v_unit_price)) AS total
			
			FROM tm_faktur_item a
					
			RIGHT JOIN tm_faktur b ON b.i_faktur=a.i_faktur
			
			WHERE b.i_faktur_code='$nofaktur'

			GROUP BY b.n_discount, b.v_discount ");
	}

	function lbarangjadiperpages($limit, $offset)
	{
		$db2 = $this->load->database('db_external', TRUE);
		// OLD 29-05-2013
		/*$query	= $db2->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
						
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." "); */

		$query	= $db2->query("
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				FROM tm_faktur_item b
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				WHERE a.f_printed='f' AND a.f_faktur_cancel = 'f'
				GROUP BY a.d_faktur, a.i_faktur_code
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT " . $limit . " OFFSET " . $offset . " ");

		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi()
	{
		$db2 = $this->load->database('db_external', TRUE);
		// old 29-05-2013
		/*return $db2->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " ); */
		return $db2->query("
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur 
				WHERE a.f_printed='f'
				GROUP BY a.d_faktur, a.i_faktur_code
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}

	function flbarangjadi($key)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE a.i_faktur_code='$key' AND d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}

	function getcabang($nofaktur)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT c.e_branch_name AS cabangname, c.e_branch_address AS address
							
			FROM tm_faktur a
				
			INNER JOIN tm_faktur_item b ON b.i_faktur=a.i_faktur
			INNER JOIN tr_branch c ON c.e_initial=a.e_branch_name
			INNER JOIN tr_customer d ON d.i_customer=c.i_customer
			
			WHERE a.i_faktur_code='$nofaktur'
			
			GROUP BY c.e_branch_name, c.e_branch_address
		");
	}

	function getinitial()
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial_code DESC LIMIT 1 ");
	}

	function lklsbrg()
	{
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC ");
		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function jmlitemharga($ifaktur, $iproduct)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT sum(n_quantity) AS qty, 
					v_unit_price AS unitprice,
					sum(v_unit_price) AS jmlunitprice,
					sum(n_quantity * v_unit_price) AS amount
				
				FROM tm_faktur_item

				WHERE i_faktur='$ifaktur' AND i_product='$iproduct'
				
				GROUP BY i_product, i_faktur, v_unit_price
		");
	}

	/*
	function remote($destination_ip) {
		return $db2->query(" SELECT * FROM tr_printer WHERE ip='$destination_ip' ORDER BY i_printer DESC LIMIT 1 ");
	}
	*/

	function remote($id)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' ORDER BY i_printer DESC LIMIT 1 ");
	}
}
