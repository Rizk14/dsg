<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  //function get_opname_hutang($num, $offset, $date_from, $date_to) {
	  // sementara ga pake paging
	function get_opname_hutang($date_from, $date_to) {
		/*$this->db->select(" a.* FROM tm_pembelian_nofaktur a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_lunas = 'f'
					ORDER BY b.nama ASC, a.tgl_faktur ASC ", false)->limit($num,$offset);
		$query = $this->db->get(); */
		/*$sql = "SELECT a.* FROM tm_pembelian_nofaktur a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_lunas = 'f'
					ORDER BY b.nama ASC, a.tgl_faktur ASC"; */
		//echo $sql; die();

		// yg diambil adalah faktur yg jenis pembeliannya kredit (2) 
		$query	= $this->db->query(" SELECT distinct a.id, a.no_sj, a.tgl_sj, a.kode_supplier, a.tgl_update, a.total, a.no_faktur
					FROM tm_pembelian a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND ((a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy'))
					OR a.tgl_sj < to_date('$date_from','dd-mm-yyyy'))
					AND a.status_aktif = 't'
					AND a.status_lunas = 'f' AND a.jenis_pembelian = '2'
					
					UNION select distinct a.id, a.no_sj, a.tgl_sj, a.kode_unit, a.tgl_update, a.total, a.no_faktur
					FROM tm_sj_hasil_makloon a, tm_supplier b WHERE a.kode_unit = b.kode_supplier 
					AND ((a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy'))
					OR a.tgl_sj < to_date('$date_from','dd-mm-yyyy'))
					AND a.status_lunas = 'f' AND a.jenis_pembelian = '2' 
					
					ORDER BY kode_supplier ASC, tgl_sj ASC ");
		
		$data_opname = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				$tot_retur = 0;
				// ##########################################################
			
				//$jum_gabung = $uang_muka+$tot_bayar+ $tot_retur;
				$jum_gabung = $tot_retur;
				$total = $row1->total-$jum_gabung;
								
				$data_opname[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'total'=> $total,											
											'tgl_update'=> $row1->tgl_update
											);
			} // endforeach header
		}
		else {
			$data_opname = '';
		}
		return $data_opname;
  }
  
  function get_opname_hutangtanpalimit($date_from, $date_to){
	/*$query	= $this->db->query(" SELECT distinct a.* FROM tm_pembelian_nofaktur a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND ((a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy'))
					OR a.tgl_faktur < to_date('$date_from','dd-mm-yyyy'))
					AND a.status_lunas = 'f' AND a.jenis_pembelian = '2' "); */
	
	$query	= $this->db->query(" SELECT distinct a.id, a.no_sj, a.tgl_sj, a.kode_supplier, a.tgl_update, a.total, a.uang_muka 
					FROM tm_pembelian a, tm_supplier b WHERE a.kode_supplier = b.kode_supplier 
					AND ((a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy'))
					OR a.tgl_sj < to_date('$date_from','dd-mm-yyyy'))
					AND a.status_aktif = 't'
					AND a.status_lunas = 'f' AND a.jenis_pembelian = '2'
					
					UNION select distinct a.id, a.no_sj, a.tgl_sj, a.kode_unit, a.tgl_update, a.total, a.uang_muka 
					FROM tm_sj_hasil_makloon a, tm_supplier b WHERE a.kode_unit = b.kode_supplier 
					AND ((a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy'))
					OR a.tgl_sj < to_date('$date_from','dd-mm-yyyy'))
					AND a.status_lunas = 'f' AND a.jenis_pembelian = '2' 
					
					ORDER BY kode_supplier ASC, tgl_sj ASC ");
    
    return $query->result();  
  }
      
  function cek_data($no_voucher, $kode_supplier){
    $this->db->select("id from tm_payment_pembelian WHERE no_voucher = '$no_voucher' 
				AND kode_supplier = '$kode_supplier' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_faktur($num, $offset, $supplier, $cari) {
		  // ambil data faktur pembelian
	if ($cari == '') {
		if ($supplier == '0') {		
			//if ($jnsaction == 'A') { // utk add
				$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND status_lunas = 'f' order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			//}

		}
		else {
			//if ($jnsaction == 'A') { // utk add
				$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND status_lunas = 'f' AND kode_supplier = '$supplier' 
									order by kode_supplier,tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			//}
		}
	}
	else {
		if ($supplier == '0') {
			//if ($jnsaction == 'A') { // utk add		
				$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND status_lunas = 'f' AND
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			//}
		}
		else {
			//if ($jnsaction == 'A') { // utk add		
				$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND status_lunas = 'f' AND
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get();
			//}
		}
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query2->row();
				$nama_supplier	= $hasilrow->nama;
																
				$data_fb[] = array(			'id'=> $row1->id,	
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'tgl_update'=> $row1->tgl_update,
											'totalnya'=> $row1->jumlah
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function get_fakturtanpalimit($supplier, $cari){
	if ($cari == '') { // belum beres
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' 
			AND status_lunas = 'f' ");
	
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' 
			AND status_lunas = 'f' AND kode_supplier = '$supplier' ");
	}
	else {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' 
			AND status_lunas = 'f' AND UPPER(no_faktur) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' 
				AND kode_supplier = '$supplier' AND status_lunas = 'f' AND UPPER(no_faktur) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_detail_faktur($list_faktur, $kode_supplier){
    $detail_faktur = array();
        
    foreach($list_faktur as $row1) {
		if ($row1 != '') {
			$query2	= $this->db->query(" SELECT no_faktur, tgl_faktur, kode_supplier, tgl_update, jumlah 
			FROM tm_pembelian_nofaktur WHERE no_faktur = '$row1' AND kode_supplier = '$kode_supplier' ");
			$hasilrow = $query2->row();
			$no_faktur	= $hasilrow->no_faktur;
			$tgl_faktur	= $hasilrow->tgl_faktur;
			$tgl_update	= $hasilrow->tgl_update;
			$jum_hutang	= $hasilrow->jumlah; // 1. ini jumlah hutang dagang faktur tsb
			
			// 2. hitung jumlah uang muka di tabel SJ/pembelian
			$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_pembelian a, tm_pembelian_nofaktur b, 
			tm_pembelian_nofaktur_sj c WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
			AND b.id = c.id_pembelian_nofaktur AND b.no_faktur = '$row1' AND a.kode_supplier = '$kode_supplier' ");
			$hasilrow = $query2->row();
			$uang_muka	= $hasilrow->tot_uang_muka;
			
			// 3. hitung jumlah yg sudah dibayar di tabel payment_pembelian
			$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar FROM tm_payment_pembelian a, 
			tm_payment_pembelian_detail b WHERE a.id = b.id_payment
			AND b.no_faktur = '$row1' AND a.kode_supplier = '$kode_supplier' ");
			$hasilrow = $query2->row();
			$tot_bayar	= $hasilrow->tot_bayar;
		
			$jum_gabung = $uang_muka+$tot_bayar;
			
			$pisah1 = explode("-", $tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
			$detail_faktur[] = array(		'no_faktur'=> $row1,
											'tgl_faktur'=> $tgl_faktur,
											'tgl_update'=> $tgl_update,
											'jum_hutang'=> $jum_hutang,
											'jum_gabung'=> $jum_gabung
								);
		}
	}
	return $detail_faktur;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_pkp_tipe_pajak_bykodesup($kode_sup){
	$query	= $this->db->query(" SELECT pkp, tipe_pajak, top FROM tm_supplier where kode_supplier = '$kode_sup' ");    
    return $query->result();  
  }
  
  function get_payment($id_payment) {
		$query	= $this->db->query(" SELECT * FROM tm_payment_pembelian where id = '$id_payment' ");
	
		$data_pay = array();
		$detail_pay = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_payment_pembelian_detail WHERE id_payment = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT tgl_faktur, jumlah 
						FROM tm_pembelian_nofaktur WHERE no_faktur = '$row2->no_faktur' AND kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$tgl_faktur	= $hasilrow->tgl_faktur;
						$jum_hutang	= $hasilrow->jumlah;
						
						// 2. hitung jumlah uang muka di tabel SJ/pembelian
						$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_pembelian a, tm_pembelian_nofaktur b, 
						tm_pembelian_nofaktur_sj c WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
						AND b.id = c.id_pembelian_nofaktur AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query2->row();
						$uang_muka	= $hasilrow->tot_uang_muka;
						
						// 3. hitung jumlah yg sudah dibayar di tabel payment_pembelian
						$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar FROM tm_payment_pembelian a, 
						tm_payment_pembelian_detail b WHERE a.id = b.id_payment
						AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query2->row();
						$tot_bayar	= $hasilrow->tot_bayar;
					
						$jum_gabung = $uang_muka+$tot_bayar;
										
						$pisah1 = explode("-", $tgl_faktur);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
						
						$detail_pay[] = array(	'id'=> $row2->id,
												'no_faktur'=> $row2->no_faktur,
												'tgl_faktur'=> $tgl_faktur,
												'jum_hutang'=> $jum_hutang,
												'jum_gabung'=> $jum_gabung,
												'jum_bayar'=> $row2->jumlah_bayar
											);
					}
				}
				else {
					$detail_pay = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama, top, pkp, tipe_pajak FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$nama_supplier	= $hasilrow->nama;
						$top	= $hasilrow->top;
						$pkp	= $hasilrow->pkp;
						$tipe_pajak	= $hasilrow->tipe_pajak;
				
				$pisah1 = explode("-", $row1->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_voucher = $tgl1."-".$bln1."-".$thn1;
								
				$data_pay[] = array(		'id'=> $row1->id,	
											'no_voucher'=> $row1->no_voucher,	
											'tgl_voucher'=> $tgl_voucher,
											'total'=> $row1->total,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'top'=> $top,
											'pkp'=> $pkp,
											'tipe_pajak'=> $tipe_pajak,
											'detail_pay'=> $detail_pay
											);
				$detail_pay = array();
			} // endforeach header
		}
		else {
			$data_pay = '';
		}
		return $data_pay;
  }  
  
  function create_bonm($no_sj, $kode_supplier, $kode, $nama, $qty, $harga){  
    $tgl = date("Y-m-d");
    $th_now	= date("Y");
	
		// ====================================================
			// insert Bon M masuk secara otomatis di tm_apply_stok_pembelian
			
				// ambil data gudang sesuai barangnya
				$query3	= $this->db->query(" SELECT id_gudang FROM tm_barang WHERE kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$id_gudang = $hasilrow->id_gudang;
				
				// cek apakah header bonm udh ada
				$query3	= $this->db->query(" SELECT id, no_bonm FROM tm_apply_stok_pembelian WHERE no_sj = '$no_sj' 
				AND kode_supplier = '$kode_supplier' AND id_gudang = '$id_gudang' ");
				if ($query3->num_rows() > 0) { // jika udh ada
					$hasilrow = $query3->row();
					$no_bonm = $hasilrow->no_bonm;
					$id_apply_stok = $hasilrow->id;
					
					//save ke tabel tm_apply_stok_pembelian_detail
					if ($kode!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'harga'=>$harga,
							'id_apply_stok'=>$id_apply_stok
						);
						$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
						
					} // end if kode != '' dan $qty != ''
				}
				else {
					// generate no Bon M
					$query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_pembelian WHERE id_gudang = '$id_gudang' 
					ORDER BY no_bonm DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() != 0) 
						$no_bonm	= $hasilrow->no_bonm;
					else
						$no_bonm = '';
					if(strlen($no_bonm)==12) {
						$nobonm = substr($no_bonm, 3, 9);
						$n_bonm	= (substr($nobonm,4,5))+1;
						$th_bonm	= substr($nobonm,0,4);
						if($th_now==$th_bonm) {
								$jml_n_bonm	= $n_bonm; //
								switch(strlen($jml_n_bonm)) {
									case "1": $kodebonm	= "0000".$jml_n_bonm;
									break;
									case "2": $kodebonm	= "000".$jml_n_bonm;
									break;	
									case "3": $kodebonm	= "00".$jml_n_bonm;
									break;
									case "4": $kodebonm	= "0".$jml_n_bonm;
									break;
									case "5": $kodebonm	= $jml_n_bonm;
									break;	
								}
								$nomorbonm = $th_now.$kodebonm;
						}
						else {
							$nomorbonm = $th_now."00001";
						}
					}
					else {
						$nomorbonm	= $th_now."00001";
					}
					$no_bonm = "BM-".$nomorbonm;
					
					// insert di tm_apply_stok_pembelian
					$data_header2 = array(
					  'no_bonm'=>$no_bonm,
					  'tgl_bonm'=>$tgl,
					  'no_sj'=>$no_sj,
					  'kode_supplier'=>$kode_supplier,
					  'id_gudang'=>$id_gudang,
					  'tgl_input'=>$tgl,
					  'tgl_update'=>$tgl
					);
					$this->db->insert('tm_apply_stok_pembelian',$data_header2);
					
					// ambil data terakhir di tabel tm_apply_stok_pembelian
					$query2	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_apply_stok	= $hasilrow->id;
					
					//save ke tabel tm_apply_stok_pembelian_detail
					if ($kode!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'harga'=>$harga,
							'id_apply_stok'=>$id_apply_stok
						);
						$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
						
					} // end if kode != '' dan $qty != ''
				} // end else

			// ====================================================
  }

}

