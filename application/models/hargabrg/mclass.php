<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		 $db2=$this->load->database('db_external', TRUE);
		//$order	= " WHERE f_active='t' ORDER BY i_price ASC, i_product_motif ASC ";
		$order	= " ORDER BY i_price ASC, i_product_motif ASC ";
		return $db2->query(" SELECT * FROM tr_product_price ".$order, false);
	}

	function viewperpages($limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		//$order	= " WHERE f_active='t' ORDER BY i_price ASC, i_product_motif ASC ";
		$order	= " ORDER BY i_price ASC, i_product_motif ASC ";
		$query	= $db2->query(" SELECT * FROM tr_product_price ".$order." LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function cek_harga($icustomer,$iproductmotif) {
		 $db2=$this->load->database('db_external', TRUE);
		//return $db2->query(" SELECT * FROM tr_product_price WHERE i_customer='$icustomer' AND i_product_motif='$iproductmotif' AND f_active='t' ");
		return $db2->query(" SELECT * FROM tr_product_price WHERE i_customer='$icustomer' AND i_product_motif='$iproductmotif' ");
	}

	function namapelanggan($icustomer) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_customer WHERE i_customer='$icustomer' ");
	}
	
	function msimpan($iproductprice,$iproductprice2,$i_customer,$iproduct,$iproductmotif,$vprice) {
		 $db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$db2->set(
			array(
			 'i_price'=>$iproductprice,
		 	 'i_product_price'=>$iproductprice2,
			 'i_customer'=>$i_customer,
			 'i_product'=>$iproduct,
			 'i_product_motif'=>$iproductmotif,
			 'v_price'=>$vprice,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry));
			 
		$db2->insert('tr_product_price');
		
		redirect('hargabrg/cform/');
	}
	
	/*
	function listpquery($tabel,$order,$filter) {
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		if ($query->num_rows() > 0) {
			return $result	= $query->result(); 
		}
	}
	*/
	
	function lbarangjadi() {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_product AS iproduct, a.i_product_motif AS iproductmotif, a.e_product_motifname AS productmotif, b.v_unitprice AS v_price FROM tr_product_motif a INNER JOIN tr_product_base b ON b.i_product_base=a.i_product WHERE a.n_active='1' ORDER BY a.i_product_motif ASC  ");
	}

	function flbarangjadi($key) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_product AS iproduct, a.i_product_motif AS iproductmotif, a.e_product_motifname AS productmotif, b.v_unitprice AS v_price FROM tr_product_motif a INNER JOIN tr_product_base b ON b.i_product_base=a.i_product WHERE a.n_active='1' AND (UPPER(a.i_product_motif)=UPPER('$key') OR UPPER(a.i_product)=UPPER('$key') OR UPPER(a.e_product_motifname) LIKE UPPER('$key%') ) ORDER BY a.i_product_motif ASC  ");
	}
	
	function lbarangjadiperpages($limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		$strqry	= (" SELECT a.i_product AS iproduct, a.i_product_motif AS iproductmotif, a.e_product_motifname AS productmotif, b.v_unitprice AS v_price FROM tr_product_motif a INNER JOIN tr_product_base b ON b.i_product_base=a.i_product WHERE a.n_active='1' ORDER BY a.i_product_motif ASC "." LIMIT ".$limit." OFFSET ".$offset);
		
		$query	= $db2->query($strqry);
			
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}	
	
	function pricecode() {
		 $db2=$this->load->database('db_external', TRUE);
		//return $db2->query("SELECT cast(i_price AS integer)+1 AS pricecode, i_product_price FROM tr_product_price WHERE f_active='t' ORDER BY i_price DESC LIMIT 1");
		return $db2->query("SELECT cast(i_price AS integer)+1 AS pricecode, i_product_price FROM tr_product_price ORDER BY i_price DESC LIMIT 1");
	}
	
	function viewcari($txt_codeharga) {
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_codeharga!="") {
			//$filter	= " WHERE f_active='t' AND (i_product_price LIKE '$txt_codeharga%' OR i_product='$txt_codeharga' OR i_product_motif='$txt_codeharga') ";
			$filter	= " WHERE (UPPER(i_product_price) LIKE UPPER('$txt_codeharga%') OR UPPER(i_product)=UPPER('$txt_codeharga') OR UPPER(i_product_motif)=UPPER('$txt_codeharga') ) ";
		}else{
			//$filter	= " WHERE f_active='t' ";
			$filter	= "";
		}
		
		return $db2->query(" SELECT * FROM tr_product_price ".$filter." ORDER BY i_product_price DESC ");
	}
	
	function mcari($txt_codeharga,$limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_codeharga!="") {
			//$filter	= " WHERE f_active='t' AND (i_product_price LIKE '$txt_codeharga%' OR i_product='$txt_codeharga' OR i_product_motif='$txt_codeharga') ";
			$filter	= " WHERE (UPPER(i_product_price) LIKE UPPER('$txt_codeharga%') OR UPPER(i_product)=UPPER('$txt_codeharga') OR UPPER(i_product_motif)=UPPER('$txt_codeharga')) ";
		}else{
			$filter	= "";
		}
		
		$query	= $db2->query( " SELECT * FROM tr_product_price ".$filter." ORDER BY i_product_price DESC LIMIT ".$limit." OFFSET ".$offset, false);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function medit($id) {
		 $db2=$this->load->database('db_external', TRUE);
		//return $db2->query(" SELECT * FROM tr_product_price WHERE i_price='$id' AND f_active='t' ");
		return $db2->query(" SELECT * FROM tr_product_price WHERE i_price='$id' ");
	}
		
	function mupdate($iproductprice,$iproductprice2,$i_customer,$i_customer_hidden,$iproduct,$iproductmotif,$vprice,$iproductprice_hidden,$iprice_hidden,$f_active_hidden,$vpricehidden,$iproductmotifhidden) {
		 $db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		/*** 02012012
		
		$product_item	= array(
			'i_product_price'=>$iproductprice_hidden,
			'i_customer'=>$i_customer,
			'i_product'=>$iproduct,
			'i_product_motif'=>$iproductmotif,
			'v_price'=>$vprice,
			'd_update'=>$dentry);
		
		$db2->update('tr_product_price',$product_item,array('i_price'=>$iprice_hidden));
		redirect('hargabrg/cform');	
		
		***/ 	
		
		$qcaricustomer = $db2->query(" SELECT * FROM tr_product_price WHERE i_customer='$i_customer' AND i_customer!='$i_customer_hidden' AND i_product_motif='$iproductmotif' AND f_active='t' ");
		
		if($qcaricustomer->num_rows()==0) {
			
			if($i_customer_hidden==0) {		
				$product_item	= array(
					'v_price'=>$vprice,
					'd_update'=>$dentry);			
			}else{
				$product_item	= array(
					'i_customer'=>$i_customer,
					'i_product'=>$iproduct,
					'i_product_motif'=>$iproductmotif,
					'v_price'=>$vprice,
					'f_active'=>$f_active_hidden,
					'd_update'=>$dentry);			
			}
			
			$tmp_price = array(
					'i_price'=>$iprice_hidden,
					'i_customer'=>$i_customer_hidden,
					'i_product_motif'=>$iproductmotifhidden,
					'v_price'=>$vpricehidden,
					'd_entry'=>$dentry);
			
			$db2->update('tr_product_price',$product_item,array('i_price'=>$iprice_hidden));
			$db2->insert('tr_tmp_price',$tmp_price);
			
			// 21-03-2014, update juga ke tr_product_base
			$db2->query(" UPDATE tr_product_base SET v_unitprice = '$vprice', d_update='$dentry' WHERE i_product_base='$iproduct' ");
			
			redirect('hargabrg/cform');
			
		}else{
			print "<script>alert(\"Harga gagal diupdate, Data dgn Kode Brg & Pelanggan tsb sdh ada.\");
			window.open(\"index\", \"_self\");</script>";
			
		}
	}
	
	function cari_kode($nkode) {
		 $db2=$this->load->database('db_external', TRUE);
		//return $db2->query(" SELECT * FROM tr_product_price WHERE i_product_price=trim('$nkode') AND f_active='t' ");
		return $db2->query(" SELECT * FROM tr_product_price WHERE i_product_price=trim('$nkode') ");
	}

	function lpelanggan() {
		 $db2=$this->load->database('db_external', TRUE);
		
		$qstr	= " SELECT a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ORDER BY a.e_customer_name ASC ";
		$query	= $db2->query($qstr);
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}	
}

?>
