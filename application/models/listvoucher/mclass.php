<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function clistvoucher($i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast) {
			$db2=$this->load->database('db_external', TRUE);
		if($ndvoucherfirst!='0' && $ndvoucherlast!='0') {
			$ivocuher = "";
			$dvoucher = " WHERE (a.d_voucher BETWEEN '$ndvoucherfirst' AND '$ndvoucherlast' ) ";	
			$fbatal = " AND a.f_voucher_cancel='f' ";
		}elseif(($ndvoucherfirst=='0' || $ndvoucherlast=='0') && ($no_voucher!='0' || $no_voucher!='')) {
			$ivocuher = " WHERE a.i_voucher='$i_voucher' AND a.i_voucher_no='$no_voucher' ";
			$dvoucher = "";
			$fbatal	= " AND a.f_voucher_cancel='f' ";
		}else{
			$fbatal	= " WHERE a.f_voucher_cancel='f' ";
		}

		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher, a.f_approved, a.f_checked
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				
				".$ivocuher." ".$dvoucher." ".$fbatal."
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher, a.f_approved, a.f_checked
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC ");
	}
		
	function clistvoucherperpages($limit,$offset,$i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast) {
			$db2=$this->load->database('db_external', TRUE);
		if($ndvoucherfirst!='0' && $ndvoucherlast!='0') {
			$ivocuher = "";
			$dvoucher = " WHERE (a.d_voucher BETWEEN '$ndvoucherfirst' AND '$ndvoucherlast' ) ";	
			$fbatal = " AND a.f_voucher_cancel='f' ";
		}elseif(($ndvoucherfirst=='0' || $ndvoucherlast=='0') && ($no_voucher!='0' || $no_voucher!='')) {
			$ivocuher = " WHERE a.i_voucher='$i_voucher' AND a.i_voucher_no='$no_voucher' ";
			$dvoucher = "";
			$fbatal	= " AND a.f_voucher_cancel='f' ";
		}else{
			$fbatal	= " WHERE a.f_voucher_cancel='f' ";
		}

		$query	= $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher, a.f_approved, a.f_checked
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				
				".$ivocuher." ".$dvoucher." ".$fbatal."
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher, a.f_approved, a.f_checked
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC LIMIT ".$limit." OFFSET ".$offset." ");
							
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}			
	}
	
	function lvoucher() {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				WHERE a.f_voucher_cancel='f' 
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC ");		
	}	

	function lvoucherperpages($limit,$offset) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				WHERE a.f_voucher_cancel='f' 
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC LIMIT ".$limit." OFFSET ".$offset." ");
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flvoucher($key) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				WHERE a.f_voucher_cancel='f' AND (c.i_voucher_code LIKE '$key%' OR a.e_description LIKE '$key%') 
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC ");
	}	
	
	function getvoucherheader($ivoucher,$novoucher){
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, 
										a.i_voucher_code, 
										a.i_voucher_no, 
										a.d_voucher, 
										a.i_company_code, 
										a.e_recieved, 
										a.e_approved,
										a.v_total_voucher,
										a.f_voucher_cancel,
										a.f_nilai_manual,
										a.f_approve_dept,
										a.e_description,
										b.i_voucher_code AS kodesumber FROM tm_voucher a 
										
										INNER JOIN tr_kode_voucher b ON b.i_voucher=a.i_voucher_code 
										
										WHERE a.i_voucher='$ivoucher' AND a.i_voucher_no='$novoucher' AND a.f_voucher_cancel='f' ");
	}
	
	function lvoucheritem($ivoucher) {
			$db2=$this->load->database('db_external', TRUE);
		/* c.v_grand_sisa = hrs nya ditambahkan dulu dgn voucher yg sudah di penuhi */
		return $db2->query(" SELECT a.i_voucher, 
										 a.v_total_voucher,
										 a.f_nilai_manual,
										 b.i_voucher_item,
										 b.i_dt,
										 c.i_dt_code,
										 b.d_dt,
										 c.v_total_grand,
										 c.v_grand_sisa,
										 b.v_voucher,
										 (c.v_grand_sisa+b.v_voucher) AS v_grand_sisa2,
										 b.f_nota_sederhana FROM tm_voucher a 
										 
										 INNER JOIN tm_voucher_item b ON a.i_voucher=b.i_voucher 
										 INNER JOIN tm_dt c ON c.i_dt=b.i_dt
										 
										 WHERE  a.i_voucher='$ivoucher' AND a.f_voucher_cancel='f' 
										 GROUP BY a.i_voucher, a.v_total_voucher, a.f_nilai_manual, b.i_voucher_item, b.i_dt, c.i_dt_code, b.d_dt, c.v_total_grand, c.v_grand_sisa, b.v_voucher, b.f_nota_sederhana
										 ORDER BY a.i_voucher ASC, b.i_voucher_item ASC ");
	}
	
	
	function mupdate($kode_sumber,$i_kode_sumber,$no_voucher,$i_voucher,$tgl_voucher,$kode_perusahaan,$deskripsi_voucher,$total_nilai_voucher,$total_nilai_voucherhidden,$recieved_voucher,$approve_voucher,$app_dept,$f_nilai_manual,$iteration,$i_kontrabon_code,$i_kontrabon,$tgl_kontrabonhidden,$total_kontrabonhidden,$piutang,$piutanghidden,$nilai_voucher,$nilai_voucherhidden,$fnotasederhana) {
			$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$indek	= 0;
		$indek2	= 0;
		
		$sisanilaivoucher = '';
		
		$qvoucheritem_awal = $db2->query(" SELECT * FROM tm_voucher_item WHERE i_voucher='$i_voucher' ORDER BY i_voucher_item ASC ");
		
		$result	= $qvoucheritem_awal->result();
		
		foreach($result as $row) {
			
			$q_tm_dt = $db2->query(" SELECT * FROM tm_dt WHERE i_dt='$row->i_dt' AND d_dt='$row->d_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$row->f_nota_sederhana' ");
			
			if($q_tm_dt->num_rows()>0) {
				
				$r_tm_dt = $q_tm_dt->row();
				$v_grand_sisa = $r_tm_dt->v_grand_sisa;
				
				if($v_grand_sisa=='')
					$v_grand_sisa = 0;
					
				$v_total_dt = $row->v_voucher+$v_grand_sisa;
				
				if(($r_tm_dt->v_total_grand)==$v_total_dt) {
					$f_pelunasan = 'f';
				}else{
					$f_pelunasan = 't';
				}
				
				$db2->query(" UPDATE tm_dt SET v_grand_sisa='$v_total_dt', f_pelunasan='$f_pelunasan' WHERE i_dt='$row->i_dt' AND d_dt='$row->d_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$row->f_nota_sederhana' ");
			}
			
			$indek+=1;
		}
		
		$db2->query(" DELETE FROM tm_voucher_item WHERE i_voucher='$i_voucher' ");
		
		$db2->query(" UPDATE tm_voucher SET i_voucher_code='$i_kode_sumber', 
												i_voucher_no='$no_voucher', 
												d_voucher='$tgl_voucher', 
												i_company_code='$kode_perusahaan', 
												e_recieved='$recieved_voucher', 
												e_approved='$approve_voucher', 
												v_total_voucher='$total_nilai_voucher', 
												f_nilai_manual='$f_nilai_manual', 
												f_approve_dept='$app_dept', 
												e_description='$deskripsi_voucher', d_update='$dentry' WHERE i_voucher='$i_voucher' AND f_voucher_cancel='f' ");

		if($sisanilaivoucher=='') {
			$sisanilaivoucher = $total_nilai_voucher;
		}
						
		for($jml=0; $jml<=$iteration; $jml++) {
			
			$qvouchertitem2	= $db2->query(" SELECT CAST(i_voucher_item AS integer) AS i_voucher_item FROM tm_voucher_item ORDER BY CAST(i_voucher_item AS integer) DESC LIMIT 1 ");
			
			if($qvouchertitem2->num_rows()>0) {
				$rvouchertitem2 = $qvouchertitem2->row();
				$ivoucheritem = $rvouchertitem2->i_voucher_item+1;
			}else{
				$ivoucheritem = 1;
			}
			
			if($f_nilai_manual=='t') {
				$db2->query(" INSERT INTO tm_voucher_item (i_voucher_item, i_voucher, i_dt, d_dt, v_voucher, d_entry, d_update, f_nota_sederhana) 
							VALUES('$ivoucheritem','$i_voucher','$i_kontrabon[$jml]','$tgl_kontrabonhidden[$jml]','$nilai_voucher[$jml]','$dentry','$dentry','$fnotasederhana[$jml]') ");
			}
			
			if($f_nilai_manual=='f') {
				
				if(($sisanilaivoucher >= $piutanghidden[$jml])) {
					$sisanilaivoucher = ($sisanilaivoucher - $piutanghidden[$jml]); /* $piutanghidden[$jml] nilainya hrs didapat dari sisa dt+nilai voucher sebelum dilakukan perubahan  */
					$nilaivoucher[$jml] = $piutanghidden[$jml];
				}elseif(($sisanilaivoucher <= $piutanghidden[$jml]) && ($sisanilaivoucher!=0 || $sisanilaivoucher!='0')) {
					$nilaivoucher[$jml] = $sisanilaivoucher;
					$sisanilaivoucher = 0;
				}elseif($sisanilaivoucher==0 || $sisanilaivoucher<0) {
					$nilaivoucher[$jml] = 0;
				}else{
					$nilaivoucher[$jml] = 0;
				}
				
				$db2->query(" INSERT INTO tm_voucher_item (i_voucher_item, i_voucher, i_dt, d_dt, v_voucher, f_nota_sederhana, d_entry, d_update)
						VALUES('$ivoucheritem','$i_voucher','$i_kontrabon[$jml]','$tgl_kontrabonhidden[$jml]','$nilaivoucher[$jml]','$fnotasederhana[$jml]','$dentry','$dentry') ");
			}


			//		
			$qDT	= $db2->query(" SELECT * FROM tm_dt WHERE i_dt='$i_kontrabon[$jml]' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana[$jml]' ");
				
			if($qDT->num_rows()>0) {
					
				$rDT	= $qDT->row();
					
				if($f_nilai_manual=='t') {
					$s		= (($rDT->v_grand_sisa) - ($nilai_voucher[$jml]));
				}
					
				if($f_nilai_manual=='f') {
					$s		= (($rDT->v_grand_sisa) - ($nilaivoucher[$jml]));
				}
			
				$db2->query(" UPDATE tm_dt SET v_grand_sisa='$s', f_pelunasan='t' WHERE i_dt='$i_kontrabon[$jml]' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana[$jml]' ");
			}
			//	
			
			$indek2+=1;
		}
		
		if ($db2->trans_status()===FALSE) {
			$ok	= 0;
			$db2->trans_rollback();
		} else {
			$ok	= 1;
			$db2->trans_commit();
		}
		
		if($ok==1) {
			print "<script>alert(\"Nomor voucher : '\"+$no_voucher+\"' telah disimpan, terimakasih.\");show(\"listvoucher/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, voucher gagal disimpan. Terimakasih.\");show(\"listvoucher/cform\",\"#content\");</script>";
		}
	}
	
	
	function mbatal($i_voucher,$i_voucher_no) {
			$db2=$this->load->database('db_external', TRUE);
		$qcarivoucher	= $db2->query(" SELECT * FROM tm_voucher WHERE i_voucher='$i_voucher' AND f_voucher_cancel='f' ");
		$ncarivoucher	= $qcarivoucher->num_rows();
		
		
		if($ncarivoucher>0) {
			
			$indek = 0;
			
			$qvoucheritem_awal = $db2->query(" SELECT * FROM tm_voucher_item WHERE i_voucher='$i_voucher' ORDER BY i_voucher_item ASC ");
			
			$result	= $qvoucheritem_awal->result();
			
			foreach($result as $row) {
				
				$q_tm_dt = $db2->query(" SELECT * FROM tm_dt WHERE i_dt='$row->i_dt' AND d_dt='$row->d_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$row->f_nota_sederhana' ");
				
				if($q_tm_dt->num_rows()>0) {
					
					$r_tm_dt = $q_tm_dt->row();
					
					$v_grand_sisa = $r_tm_dt->v_grand_sisa;
					
					if($v_grand_sisa=='')
						$v_grand_sisa = 0;
						
					$v_total_dt = ($row->v_voucher)+$v_grand_sisa;
					
					if(($r_tm_dt->v_total_grand)==$v_total_dt) {
						$f_pelunasan = 'f';
					}else{
						$f_pelunasan = 't';
					}
					
					$db2->query(" UPDATE tm_dt SET v_grand_sisa='$v_total_dt', f_pelunasan='$f_pelunasan' WHERE i_dt='$row->i_dt' AND d_dt='$row->d_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$row->f_nota_sederhana' ");
				}
				
				$indek+=1;
			}
			
			$db2->query(" UPDATE tm_voucher SET f_voucher_cancel='t' WHERE i_voucher='$i_voucher' AND f_voucher_cancel='f' ");
			
			print "<script>alert(\"Nomor voucher : '\"+$i_voucher_no+\"' telah dibatalkan, terimakasih.\");show(\"listvoucher/cform\",\"#content\");</script>";
			
		}else{
			
			print "<script>alert(\"Maaf, Data voucher tdk ditemukan.\");show(\"listvoucher/cform\",\"#content\");</script>";
			
		}
	}
}

?>
