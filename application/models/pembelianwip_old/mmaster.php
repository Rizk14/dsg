<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
    {
        parent::__construct();
    }
    
  // ++++++++  22-08-2013 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  function getlistunitjahit(){
	$sql = " * FROM tm_unit_jahit ORDER BY kode_unit ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function getbrgjadi($num, $offset, $cari, $kode_unit)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			
			$query3	= $this->db->query(" SELECT harga FROM tm_harga_hasil_jahit WHERE kode_brg_jadi='$row1->i_product_motif'
								AND kode_unit = '$kode_unit' ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				$harganya = $hasilrow3->harga;
			}
			else
				$harganya = 0;
			
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname,
										'harga_jahit'=> $harganya
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function getbrgjaditanpalimit($cari, $kode_unit){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function savefakturwip($no_faktur,$tgl_faktur, $kode_unit, $gtotal, $ket, 
					$kode, $nama, $qty, $harga, $harga_lama, $diskon, $subtotal, $no_sj, $tgl_sj){  
    $tgl = date("Y-m-d H:i:s");
	$uid = $this->session->userdata('uid');
	
    // cek apa udah ada datanya blm dgn no faktur tadi
    $this->db->select("id from tm_fakturmasukwip WHERE no_faktur = '".$this->db->escape_str($no_faktur)."' 
						AND tgl = '$tgl_faktur' AND kode_unit_jahit = '$kode_unit' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
	
			// insert di tm_fakturmasukwip
			$data_header = array(
			  'no_faktur'=>$no_faktur,
			  'tgl'=>$tgl_faktur,
			  'kode_unit_jahit'=>$kode_unit,
			  'keterangan'=>$ket,
			  'grandtotal'=>$gtotal,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			$this->db->insert('tm_fakturmasukwip',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_fakturmasukwip
			$query2	= $this->db->query(" SELECT id FROM tm_fakturmasukwip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_faktur	= $hasilrow->id; 
			
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			
			// insert ke tm_fakturmasukwip_detail
			$data_detail = array(
				'id_fakturmasukwip'=>$id_faktur,
				'kode_brg_jadi'=>$kode,
				'qty'=>$qty,
				'harga'=>$harga,
				'diskon'=>$diskon,
				'subtotal'=>$subtotal,
				'no_sj'=>$no_sj,
				'tgl_sj'=>$tgl_sj
			);
			$this->db->insert('tm_fakturmasukwip_detail',$data_detail);
			
		if ($harga_lama != $harga) {
			// ========= 13-03-2012, insert ke tabel master tm_harga_hasil_jahit =================
			$sql3	= $this->db->query(" SELECT id FROM tm_harga_hasil_jahit WHERE kode_brg_jadi='$kode' 
									AND kode_unit = '$unit_jahit' AND harga = '$harga_lama' ");
			if($sql3->num_rows() > 0){
				$hasil3	= $sql3->row();
				$id_harga = $hasil3->id;
					
				$this->db->query(" UPDATE tm_harga_hasil_jahit SET harga='$harga', tgl_update ='$tgl' 
							WHERE id='$id_harga' ");
			}
			else {
				$data_detail2 = array(
					'kode_brg_jadi'=>$kode,
					'kode_unit'=>$kode_unit,
					'harga'=>$harga,
					'tgl_input'=>$tgl,
					'tgl_update'=>$tgl
				);
				$this->db->insert('tm_harga_hasil_jahit',$data_detail2);
			}
		}
				// ================================================================
  }
  
  function getAllfakturwip($num, $offset, $cunit, $date_from, $date_to, $cari) {	  
		$sql= " * FROM tm_fakturmasukwip WHERE TRUE ";
		if($cari!="all") {
			$sql.= " AND UPPER(no_faktur) like UPPER('%".$this->db->escape_str($cari).")%'";
		}
		
		if ($cunit != "xx")
			$sql.= " AND kode_unit_jahit = '$cunit' ";
		if ($date_from != "00-00-0000")
			$sql.= " AND tgl_faktur >= to_date('$date_from','%d-%m-%Y') ";
		if ($date_to != "00-00-0000")
			$sql.= " AND tgl_faktur <= to_date('$date_to','%d-%m-%Y') ";
		$sql.= " ORDER BY tgl DESC, id DESC ";
		
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();

		$data_faktur = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_fakturmasukwip_detail WHERE id_fakturmasukwip = '$row1->id' ORDER BY id ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						
						 $query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							
							if ($query3->num_rows() > 0) {
								
								$hasilrow = $query3->row();
								$kode_brg_jadi	= $row2->kode_brg_jadi;
								$nama_brg_jadi	= $hasilrow->e_product_motifname;
							}
							else {
								$kode_brg_jadi	= '';
								$nama_brg_jadi	= '';
							}
						if ($row2->tgl_sj != '') {
							$pisah1 = explode("-", $row2->tgl_sj);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						}
						else
							$tgl_sj = '';
						
						$detail_faktur[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'diskon'=> $row2->diskon,
												'subtotal'=> $row2->subtotal,
												'no_sj'=> $row2->no_sj,
												'tgl_sj'=> $tgl_sj
											);
					}
				}
				else {
					$detail_faktur = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$namaunit	= $hasilrow->nama;
				}
				else {
					$namaunit = '';
				}
												
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl,
											'tgl_update'=> $row1->tgl_update,
											'kode_unit'=> $row1->kode_unit_jahit,
											'nama_unit'=> $namaunit,
											'ket'=> $row1->keterangan,
											'grandtotal'=> $row1->grandtotal,
											'detail_faktur'=> $detail_faktur
											);
				$detail_faktur = array();
			} // endforeach header
		}
		else {
			$data_faktur = '';
		}
		return $data_faktur;
  }

  function getAllfakturwiptanpalimit($cunit, $date_from, $date_to, $cari){
	  $sql= " * FROM tm_fakturmasukwip WHERE TRUE ";
		if($cari!="all") {
			$sql.= " AND UPPER(no_faktur) like UPPER('%".$this->db->escape_str($cari)."%') ";
		}
		
		if ($cunit != "xx")
			$sql.= " AND kode_unit_jahit = '$cunit' ";
		if ($date_from != "00-00-0000")
			$sql.= " AND tgl_faktur >= to_date('$date_from','%d-%m-%Y') ";
		if ($date_to != "00-00-0000")
			$sql.= " AND tgl_faktur <= to_date('$date_to','%d-%m-%Y') ";
		
		$this->db->select($sql, false);
		$query = $this->db->get();

    return $query->result();  
  }
  
  function cek_data_fakturwip($no_faktur, $kode_unit, $thn1){
    $this->db->select("id from tm_fakturmasukwip WHERE no_faktur = '$no_faktur' 
				AND kode_unit_jahit='$kode_unit' AND extract(year from tgl) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function deletefakturwip($id_faktur){
	// 2. hapus data tm_fakturmasukwip_detail dan tm_fakturmasukwip
    
    // ======================================================================
    $this->db->delete('tm_fakturmasukwip_detail', array('id_fakturmasukwip' => $id_faktur));
    $this->db->delete('tm_fakturmasukwip', array('id' => $id_faktur));
  }
  
  function get_fakturwip($id_faktur) {
		$query	= $this->db->query(" SELECT * FROM tm_fakturmasukwip where id = '$id_faktur' ");
	
		$data_faktur = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_fakturmasukwip_detail WHERE id_fakturmasukwip = '$row1->id' ORDER BY id ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi		= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
						
						if ($row2->tgl_sj != '') {
							$pisah1 = explode("-", $row2->tgl_sj);
							$thn1= $pisah1[0];
							$bln1= $pisah1[1];
							$tgl1= $pisah1[2];
							$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						}
						else
							$tgl_sj = '';
																			
						$detail_faktur[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'diskon'=> $row2->diskon,
												'subtotal'=> $row2->subtotal,
												'no_sj'=> $row2->no_sj,
												'tgl_sj'=> $tgl_sj
											);
					}
				}
				else {
					$detail_faktur = '';
				}
				
				$pisah1 = explode("-", $row1->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$namaunit	= $hasilrow->nama;
				}
				else {
					$namaunit = '';
				}
								
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl'=> $tgl,
											'grandtotal'=> $row1->grandtotal,
											'keterangan'=> $row1->keterangan,
											'kodeunit'=> $row1->kode_unit_jahit,
											'namaunit'=> $namaunit,
											'detail_faktur'=> $detail_faktur
											);
				$detail_faktur = array();
			} // endforeach header
		}
		else {
			$data_faktur = '';
		}
		return $data_faktur;
  }
  
  // ===================================================================================
  
  // ++++++++++++++++++++++++++++++++++++++++++++++++    
}
