<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  function get_kel_brg_wip($id){
	$query = $this->db->query(" SELECT * FROM tm_kel_brg_wip WHERE id='$id' ");
    return $query->result();
  }
   function getAllkelbrgwip(){
	$query = $this->db->query(" SELECT * FROM tm_kel_brg_wip ORDER BY kode ");    
    return $query->result();
  }
  function get_bahantanpalimit($cari, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't'  ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
   function get_bahan($num, $offset, $cari, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
   function savematerialbrgjadi($kode_brg_jadi,$kode_bhn_baku, $kode_warna) {
		$tgl = date("Y-m-d H:i:s");
		
		$listbhn = explode(";", $kode_bhn_baku);
		$listwarna = explode(";", $kode_warna);
		//foreach ($listbhn as $row1) {
		for ($i=0; $i<count($listbhn); $i++) {
			if (trim($listbhn[$i]!= '')) {
				//echo $listbhn[$i]." ".$listwarna[$i]."<br>";
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_material_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '".$listbhn[$i]."' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_material_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_material	= $hasilrow->id;
						$new_id_material = $id_material+1;
					}
					else
						$new_id_material = 1;
					
					$datanya	= array(
							 'id'=>$new_id_material,
							 'kode_brg_jadi'=>$kode_brg_jadi,
							 'kode_brg'=>$listbhn[$i],
							 'kode_warna'=>$listwarna[$i],
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_material_brg_jadi',$datanya);
				} // end if
			}
		} // end foreach
	}
  function get_jenis_barang_wip($num, $offset, $kel_brg_wip, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg_wip' order by kode ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg_wip' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) 
		order by kode ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
   function get_jenis_barang_wiptanpalimit($kel_brg_wip, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg_wip' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg_wip' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}    
    return $query->result();  
  }
  function cek_data_kel_brg_wip($kode){
    $this->db->select("* FROM tm_kel_brg_wip WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }function save($kode,$nama, $kel_brg_wip, $id_jenis, $id_brg, $status_aktif, $goedit){  
	$tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_barang_wip ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'kode_brg'=>$kode,
      'nama_brg'=>$nama,
      'id_kel_brg_wip'=>$kel_brg_wip,
      'id_jenis_brg_wip'=>$id_jenis,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'status_aktif'=>'t',
      'uid_update_by'=>$uid_update_by
    );


	$data_tb = array(
	  'id'=>$idbaru,
      'kode_barang_wip'=>$kode,
      'nama_barang_wip'=>$nama,
      'created_at'=>$tgl,
      'updated_at'=>$tgl,
   
    );

    if ($goedit == '') {
		$this->db->insert('tm_barang_wip',$data); 
		$this->db->insert('tb_master_barang_wip',$data_tb); 
	}
	else {
		if ($status_aktif == '')
			$status_aktif = 't';
		$data = array(
			  'kode_brg'=>$kode,
			  'nama_brg'=>$nama,
			  'id_kel_brg_wip'=>$kel_brg_wip,
			  'id_jenis_brg_wip'=>$id_jenis,
			  'tgl_update'=>$tgl,
			  'status_aktif'=>$status_aktif,
			  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_brg);
		$this->db->update('tm_barang_wip',$data);  
		
		
     $data_tb = array(
        'nama_barang_wip' => $nama,
       'kode_barang_wip' => $kode,
       'updated_at' => $tgl,
);
	$this->db->where('id', $id);
    $this->db->update('tb_master_barang_wip', $data_tb);
	}		
		// 11-12-14 edit kode brg di tabel master, sekaligus update kode brg di tabel2 lain
		// updatenya ke tabel: tr_product_base OK, tr_product_motif OK, tm_bonmkeluarcutting_detail, tm_bonmmasukcutting_detail,
		// tm_hpp_gudang_jadi, tm_hpp_packing, tm_hpp_pengadaan, tm_hpp_wip, tm_material_brg_jadi,
		// tm_sjkeluarwip_detail, tm_sjmasukbhnbakupic_detail, tm_sjmasukwip_detail, tm_stok_hasil_cutting, tm_stok_hasil_jahit,
		// tm_stok_unit_jahit, tt_stok_opname_hasil_jahit_detail, tt_stok_opname_unit_jahit_detail
		
		// 11-09-2015 GA PERLU LAGI
		/*$this->db->query(" UPDATE tr_product_base SET i_product_base = '$kode' WHERE i_product_base='$kodeedit' ");
		$kode_motif_lama = $kodeedit."00";
		$this->db->query(" UPDATE tr_product_motif SET i_product_motif = '$kode_brg_jadi' WHERE i_product_motif='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_bonmkeluarcutting_detail SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_bonmmasukcutting_detail SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_hpp_gudang_jadi SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_hpp_packing SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_hpp_pengadaan SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		
		$this->db->query(" UPDATE tm_hpp_wip SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_material_brg_jadi SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_sjkeluarwip_detail SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_sjmasukbhnbakupic_detail SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_sjmasukwip_detail SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_stok_hasil_cutting SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_stok_hasil_jahit SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		
		$this->db->query(" UPDATE tm_stok_unit_jahit SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET kode_brg_jadi = '$kode_brg_jadi' WHERE kode_brg_jadi='$kode_motif_lama' ");
		*/
	
		
  }
   function getAllwarnabrgwiptanpalimit($cari) {
		$sql = " SELECT distinct a.id_brg_wip, b.kode_brg, b.nama_brg 
				FROM tm_warna_brg_wip a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id ";
		if ($cari != "all")
			$sql.= " AND (UPPER(b.kode_brg) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%')) ";
		$sql.= " ORDER BY b.kode_brg ";
		  
		$query = $this->db->query($sql);
		return $query->result();  
	}
  
  function getAllwarnabrgwip($limit,$offset, $cari) {
	  $sql = " SELECT distinct a.id_brg_wip, b.kode_brg, b.nama_brg 
				FROM tm_warna_brg_wip a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id ";
		if ($cari != "all")
			$sql.= " AND (UPPER(b.kode_brg) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%')) ";
		$sql.= " ORDER BY b.kode_brg LIMIT ".$limit;
		
	  			
	  $query	= $this->db->query($sql);
	  $this->db->select($sql, false)->limit($limit,$offset);
		
		if ($query->num_rows() > 0){
			$hasilnya=$query->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $this->db->query(" SELECT a.id_warna, b.nama as nama_warna FROM tm_warna_brg_wip a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE a.id_brg_wip = '".$rownya->id_brg_wip."' ORDER BY b.nama ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
												'nama_warna'=> $rowxx->nama_warna
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'id_brg_wip'=> $rownya->id_brg_wip,
										'kode_brg'=> $rownya->kode_brg,
										'nama_brg'=> $rownya->nama_brg,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	function getbarangwiptanpalimit($cari){
		if ($cari == "all") {
			$sql = " select * FROM tm_barang_wip ";
			$query = $this->db->query($sql);
			return $query->result();  
		}
		else {
			$sql = " SELECT * FROM tm_barang_wip WHERE UPPER(kode_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
						OR UPPER(nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') ";
			$query = $this->db->query($sql);
			return $query->result();  
		}
  }
  function savewarnabrgwip($id_brg_wip,$idwarna) {
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		$listwarna = explode(";", $idwarna);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_warna_brg_wip 
									 WHERE id_brg_wip = '$id_brg_wip' AND id_warna = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_warna_brg_wip ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_warna	= $hasilrow->id;
						$new_id_warna = $id_warna+1;
					}
					else
						$new_id_warna = 1;
					
					$datanya	= array(
							 'id'=>$new_id_warna,
							 'id_brg_wip'=>$id_brg_wip,
							 'id_warna'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl,
							 'uid_update_by'=>$uid_update_by);
					$this->db->insert('tm_warna_brg_wip',$datanya);
				} // end if
			}
		} // end foreach
	}
	
	function deletewarnabrgwip($id_brg_wip){
		$this->db->query(" DELETE FROM tm_warna_brg_wip WHERE id_brg_wip = '$id_brg_wip' ");
	}
  function getbarangwip($num,$offset, $cari){    
    if ($cari == "all") {
		$sql = " * FROM tm_barang_wip ORDER BY kode_brg ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tm_barang_wip ";		
		$sql.=" WHERE UPPER(kode_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by kode_brg ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {									
			$data_bhn[] = array(		'id'=> $row1->id,	
										'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
	function get_warna(){
		$query = $this->db->query(" SELECT * FROM tm_warna ORDER BY nama");
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	 function get_brgjadi($num, $offset, $cari)
  {
	  $db2=$this->load->database('db_external',TRUE);
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$db2->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$db2->select($sql, false)->limit($num,$offset);	
    }
    $query = $db2->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
	function get_brgjaditanpalimit($cari){
		$db2=$this->load->database('db_external',TRUE);
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $db2->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $db2->query($sql);
		return $query->result();  
	}
  }
	
   function save_kel_brg_wip($kode, $kodeedit, $nama, $ket, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $data = array(
      'kode'=>$kode,
      'nama'=>$nama,
      'keterangan'=>$ket,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_kel_brg_wip',$data); }
	else {
		
		$data = array(
		  'kode'=>$kode,
		  'nama'=>$nama,
		  'keterangan'=>$ket,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$kodeedit);
		$this->db->update('tm_kel_brg_wip',$data);  
	}
		
  }
   function getAllmaterialbrgjaditanpalimit($cari) {
	   $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')) ";
			
		$sqlnya = $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_material_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif ".$filter."
							ORDER BY a.kode_brg_jadi ");
		return $sqlnya->result();  
	}
  
  function getAllmaterialbrgjadi($limit,$offset, $cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')) ";
			
	  $sqlnya	= $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_material_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif "
							.$filter." ORDER BY a.kode_brg_jadi LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailbrg = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $this->db->query(" SELECT kode_brg, kode_warna FROM tm_material_brg_jadi WHERE kode_brg_jadi = '".$rownya->kode_brg_jadi."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						// bhn baku/quilting
						$query3	= $this->db->query(" SELECT a.id,a.nama_brg, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
						if ($query3->num_rows() > 0){
							$quilting = 'f';
							$hasilrow = $query3->row();
							$id_brg	= $hasilrow->id;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$quilting = 't';
							$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
							if ($query31->num_rows() > 0){
								$quilting = 't';
								$hasilrow31 = $query31->row();
								$nama_brg	= $hasilrow31->nama_brg;
								$satuan	= $hasilrow31->nama_satuan;
							}
							else {
								$nama_brg = '';
								$satuan = '';
								$quilting = 'f';
							}
						}
						$kode_brg_jadin=substr($rownya->kode_brg_jadi,0,7);
						$query3	= $this->db->query(" SELECT id 
									FROM tm_barang_wip where kode_brg = '$kode_brg_jadin' ");
						if ($query3->num_rows() > 0){
							$quilting = 'f';
							$hasilrow = $query3->row();
							$id_brg_wip	= $hasilrow->id;
							
						}
						 $db2=$this->load->database('db_external',TRUE);		
						$queryxx = $db2->query(" SELECT a.i_product_color, a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
												WHERE a.i_color = b.i_color AND a.i_product_color = '".$rowxx->kode_warna."' ");
									if ($queryxx->num_rows() > 0){
							$hasilrow = $queryxx->row();
							$nama_warna	= $hasilrow->e_color_name;
						}			
						// warna
						//~ $query3	= $this->db->query(" SELECT nama FROM tm_warna WHERE kode = '$rowxx->kode_warna' ");
						//~ if ($query3->num_rows() > 0){
							//~ $hasilrow = $query3->row();
							//~ $nama_warna	= $hasilrow->nama;
						//~ }
						
						$detailbrg[] = array(	'kode_brg'=> $rowxx->kode_brg,
												
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'quilting'=> $quilting,
												'kode_warna'=> $rowxx->kode_warna,
												'nama_warna'=> $nama_warna,
												);
					}
				}
				else
					$detailbrg = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->kode_brg_jadi,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailbrg'=> $detailbrg,
										'id_brg_wip'=> $id_brg_wip,
								);
				
				$detailbrg = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
  
  function delete_kel_brg_wip($id){    
    $this->db->delete('tm_kel_brg_wip', array('id' => $id));
  }
   function get_jenis_brg_wip($id){
    $query	= $this->db->query(" SELECT * FROM tm_jenis_brg_wip WHERE id = '$id' ");
    return $query->result();		  
  }
  
  function getAlljenisbrgwip(){
    $query	= $this->db->query(" SELECT b.*, a.kode as kode_kel, a.nama as nama_kel FROM tm_jenis_brg_wip b INNER JOIN tm_kel_brg_wip a ON b.id_kel_brg_wip = a.id ORDER BY b.kode ");
    return $query->result();
  }
  function cek_data_jenis_brg_wip($kode){
    $this->db->select(" id FROM tm_jenis_brg_wip WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function save_jenis_brg_wip($id_jenis, $kode, $nama, $goedit, $kodeeditjenis, $id_kel_brg_wip){  
    $tgl = date("Y-m-d H:i:s");
    $data = array(
      'id_kel_brg_wip'=>$id_kel_brg_wip,
      'kode'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_jenis_brg_wip',$data); 
	}
	else {
		
		$data = array(
		  'id_kel_brg_wip'=>$id_kel_brg_wip,
		  'kode'=>$kode,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_jenis);
		$this->db->update('tm_jenis_brg_wip',$data);  
	}	
  }
   function cek_data($kode){
    $this->db->select("id from tm_barang_wip WHERE kode_brg = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function get($id_brg){
    $query = $this->db->query(" SELECT * FROM tm_barang_wip WHERE id = '$id_brg' ");
    return $query->result();
  }
  function getAll($num, $offset, $cari, $jenis_brg_wip, $cstatus)
  {
	$sql = " b.id, b.kode_brg, b.nama_brg, b.id_kel_brg_wip, b.id_jenis_brg_wip FROM tm_barang_wip b
				LEFT JOIN tm_kel_brg_wip xx ON b.id_kel_brg_wip = xx.id
				WHERE b.status_aktif = 't' ";
		
	if ($cari != "all") {
		$sql.= " AND (UPPER(b.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')) ";
	}
	if ($jenis_brg_wip != '0')
		$sql.= " AND b.id_jenis_brg_wip = '$jenis_brg_wip' ";
	if ($cstatus != '0')
		$sql.= " AND b.status_aktif = '$cstatus' ";
	$sql.= " ORDER BY xx.kode, b.kode_brg ";
				
	$this->db->select($sql, false)->limit($num,$offset);

    $query = $this->db->get();
    return $query->result();
  }
  function delete_jenis_brg_wip($id){    
    $this->db->delete('tm_jenis_brg_wip', array('id' => $id));
  }
  function getAlltanpalimit($cari, $jenis_brg_wip, $cstatus){
	  $sql = " b.id, b.kode_brg, b.nama_brg, b.id_kel_brg_wip, b.id_jenis_brg_wip FROM tm_barang_wip b
				LEFT JOIN tm_kel_brg_wip xx ON b.id_kel_brg_wip = xx.id
				WHERE b.status_aktif = 't' ";
		
	if ($cari != "all") {
		$sql.= " AND (UPPER(b.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')) ";
	}
	if ($jenis_brg_wip != '0')
		$sql.= " AND b.id_jenis_brg_wip = '$jenis_brg_wip' ";
	if ($cstatus != '0')
		$sql.= " AND b.status_aktif = '$cstatus' ";
	$sql.= " ORDER BY xx.kode, b.kode_brg ";
	
	$this->db->select($sql, false);
	
    $query = $this->db->get();
    return $query->result();  
  }

#######ddy
	function updatewarnabrgwip($id_brg_wip,$ada, $idwarna) {
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		//if ($ada == 0)
			$this->db->query(" DELETE FROM tm_warna_brg_wip WHERE id_brg_wip = '$id_brg_wip' ");
		
		$listwarna = explode(";", $idwarna);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_warna_brg_wip
									 WHERE id_brg_wip = '$id_brg_wip' AND id_warna = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$hasilsqlcek = $sqlcek->row();
					$query2	= $this->db->query(" SELECT id FROM tm_warna_brg_wip ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_warna	= $hasilrow->id;
						$new_id_warna = $id_warna+1;
					}
					else
						$new_id_warna = 1;
					
					$datanya	= array(
							 'id'=>$new_id_warna,
							 'id_brg_wip'=>$id_brg_wip,
							 'id_warna'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl,
							 'uid_update_by'=>$uid_update_by);
					$this->db->insert('tm_warna_brg_wip',$datanya);
				} // end if
				// 04-12-2015 DIKOMEN
				//else { // jika udh ada, maka update aja (sepertinya ga akan masuk kesini, soalnya jika datanya udh pernah dipake di SJ masuk/keluar maka ga bisa diedit sama sekali
				//	$this->db->query(" UPDATE tm_warna_brg_wip SET id_warna = '$row1' WHERE id='$hasilsqlcek->id' ");
				//}
			}
		} // end foreach
	}
#######end of ddy
}
