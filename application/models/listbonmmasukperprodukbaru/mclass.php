<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function getqtyperproduk($d_first,$d_last) {
		$db2=$this->load->database('db_external', TRUE);
		//=============================
		$sql=" select UPPER(b.i_product) AS imotif, b.e_product_name AS productmotif, sum(b.n_count_product) as qty
			FROM tm_inbonm a, tm_inbonm_item b  
			WHERE a.i_inbonm = b.i_inbonm AND a.f_inbonm_cancel = 'f' ";
		if ($d_first != '' && $d_last!='')
			$sql.= " AND (a.d_inbonm >='$d_first' AND a.d_inbonm <='$d_last') ";
		
		$sql.= " GROUP BY b.i_product, b.e_product_name ORDER BY b.i_product ASC ";
		//=============================
	
		$query	= $db2->query($sql);
		$data_brg = array();
		
		if($query->num_rows()>0) {
			$hasil = $query->result();
			foreach ($hasil as $row) {
				
				//--------------11-01-2014 ----------------------------
				// ambil data qty warna dari tr_product_color dan tm_inbonm_item_color
				$sqlxx	= $db2->query(" SELECT b.e_color_name, sum(c.qty) as jum FROM tr_product_color a, tr_color b, 
										tm_inbonm_item_color c, tm_inbonm d, tm_inbonm_item e
										WHERE d.i_inbonm = e.i_inbonm AND c.i_inbonm_item = e.i_inbonm_item AND
										a.i_color=b.i_color AND c.i_product_color = a.i_product_color
										AND (d.d_inbonm >='$d_first' AND d.d_inbonm <='$d_last') AND e.i_product='$row->imotif'
										GROUP BY b.e_color_name ORDER BY b.e_color_name ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
							
					foreach ($hasilxx as $rownya) {
						$listwarna.= $rownya->e_color_name." : ".$rownya->jum."<br>";
						$warna=$rownya->e_color_name;
						$jum=$rownya->jum;
					}
				}
				//-----------------------------------------------------
				
				$data_brg[] = array(		'imotif'=> $row->imotif,	
											'productmotif'=> $row->productmotif,	
											'qty'=> $row->qty,
											'listwarna'=> $listwarna,
											'warna'=>$warna,
											'jum'=>$jum
											);
			} // end foreach
		}
		else 
			$data_brg = '';
		return $data_brg;		
	}
	
	
	function explistopvsdo_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi) {
		$db2=$this->load->database('db_external', TRUE);
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		}
		
		/* 26072011	
		return $db2->query("
				SELECT c.f_stop_produksi AS stopproduct, 
				d.i_product_motif AS imotif, 
				d.e_product_motifname AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
				INNER JOIN tm_do f ON f.i_do=e.i_do 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." 
				
				GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice		
		");
		*/ 
		return $db2->query(" SELECT c.f_stop_produksi AS stopproduct, 
				d.i_product_motif AS imotif, 
				d.e_product_motifname AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tm_do_item e ON e.i_op=b.i_op 
				INNER JOIN tm_do f ON f.i_do=e.i_do 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." 
				
				GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice		
		");
		
		
	}
	
	function explistopvsdorinci_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif) {
		$db2=$this->load->database('db_external', TRUE);
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		}
		
		/* 26072011	
		return $db2->query("
			SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			*/ 

		return $db2->query(" SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON e.i_op=b.i_op 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
						
	}		
	/* End 0f 18052011 */
	
}
?>
