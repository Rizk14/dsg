<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
              
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }
  
  function get_barang_stok_zero() {
	$sql = " SELECT a.kode_brg, a.nama_brg, b.nama FROM tm_barang a, tm_satuan b where a.satuan = b.id
			AND a.status_aktif = 't' AND a.kode_brg NOT IN (select kode_brg FROM tm_stok) 
			UNION SELECT a.kode_brg, a.nama_brg, b.nama FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id
			AND a.status_aktif = 't' AND a.kode_brg NOT IN (select kode_brg FROM tm_stok_hasil_makloon)
			ORDER BY kode_brg ";
	$query	= $this->db->query($sql);
	
	$data_brg = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$pos = strpos($row1->nama_brg, "\"");
			  if ($pos > 0)
				$nama_brg_konv = str_replace("\"", "&quot;", $row1->nama_brg);
			  else
				$nama_brg_konv = str_replace("'", "\'", $row1->nama_brg); 
			
			$data_brg[] = array(			
								'kode_brg'=> $row1->kode_brg,
								'nama_brg'=> $nama_brg_konv,
								'satuan'=> $row1->nama
							);
		} // end foreach
	}
	else
		$data_brg = '';
    return $data_brg;  
  }
  
  function get_harga($idharga){
	$query = $this->db->query(" SELECT * FROM tm_harga_brg_supplier WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
			$hasilrow = $query3->row();
			$nama_supplier	= $hasilrow->nama;
			
			// ambil data nama barang
			$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			$nama_brg	= $hasilrow->nama_brg;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }
  
  // 18-11-2014
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bhnbaku_stokawal($num, $offset, $gudang, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_barang WHERE id_gudang = '$gudang' AND kode_brg NOT IN (SELECT kode_brg FROM tm_stok) ORDER BY kode_brg ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tm_barang WHERE id_gudang = '$gudang' AND kode_brg NOT IN (SELECT kode_brg FROM tm_stok)  ";		
		$sql.=" AND (UPPER(kode_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(nama_brg) like UPPER('%".$this->db->escape_str($cari)."%')) 
				order by kode_brg ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$row1->i_product_motif'
										AND id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
			}
			else
				$stok = 0; */
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg
										//'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bhnbaku_stokawaltanpalimit($gudang, $cari){
	if ($cari == "all") {
		$sql = " select * FROM tm_barang WHERE id_gudang = '$gudang' AND kode_brg NOT IN (SELECT kode_brg FROM tm_stok)";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_barang WHERE id_gudang = '$gudang' AND kode_brg NOT IN (SELECT kode_brg FROM tm_stok)
					AND (UPPER(kode_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(nama_brg) like UPPER('%".$this->db->escape_str($cari)."%')) ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function savestokawal($id_gudang, $kode, $nama, $qty, $harga){  
    $tgl = date("Y-m-d H:i:s");
	
	$sqlxx	= $this->db->query(" SELECT id FROM tm_stok WHERE kode_brg='$kode' ");
	if($sqlxx->num_rows() == 0) {
		$datanya = array(
			  'kode_brg'=>$kode,
			  'stok'=>$qty,
			  'tgl_update_stok'=>$tgl
			);
		$this->db->insert('tm_stok',$datanya);
	}
	// ambil id_stok utk dipake di stok harga
	/*$sqlxx	= $this->db->query(" SELECT id FROM tm_stok ORDER BY id DESC LIMIT 1 ");
	if($sqlxx->num_rows() > 0) {
		$hasilxx	= $sqlxx->row();
		$id_stok	= $hasilxx->id;
	} */
	
	$datanya2 = array(
			  'kode_brg'=>$kode,
			  'stok'=>$qty,
			  'harga'=>$harga,
			  'tgl_update_stok'=>$tgl
			);
	$this->db->insert('tm_stok_harga',$datanya2);
	
			/*	for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
					
					// ========================= stok per warna ===============================================
					//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_jahit='$id_stok' ");
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_jahit'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							'stok'=>$qty_warna[$xx],
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
					}
				} // end for 
				*/
				// ----------------------------------------------
  }
  
  function getAllstokawal($num, $offset, $cari) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY b.id_gudang, b.kode_brg ";
		}else{
			$pencarian.= " ORDER BY b.id_gudang, b.kode_brg ";
		}
		
		$this->db->select(" a.id, a.kode_brg, a.stok, b.nama_brg, b.id_gudang, a.tgl_update_stok 
						FROM tm_stok a, tm_barang b 
						WHERE a.kode_brg = b.kode_brg ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				// 15-02-2014, stok harga
					$queryxx	= $this->db->query(" SELECT stok, harga FROM tm_stok_harga
									WHERE kode_brg = '$row1->kode_brg' ORDER BY kode_brg ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailharga[] = array(	'harga'=> $rowxx->harga,
													'stok'=> $rowxx->stok
												);
						}
					}
					else
						$detailharga = '';
					// --------------------------
				
				$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
								
				$data_stok[] = array(		'id'=> $row1->id,	
											'kode_brg'=> $row1->kode_brg,
											'stok'=> $row1->stok,
											'nama_brg'=> $row1->nama_brg,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'detailharga'=> $detailharga
											);
				$detailharga = array();
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  function getAllstokawaltanpalimit($cari){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY b.id_gudang, b.kode_brg ";
		}else{
			$pencarian.= " ORDER BY b.id_gudang, b.kode_brg ";
		}
		
		$query	= $this->db->query(" SELECT a.id, a.kode_brg, a.stok, b.nama_brg, b.id_gudang, a.tgl_update_stok 
						FROM tm_stok a, tm_barang b 
						WHERE a.kode_brg = b.kode_brg ".$pencarian." ");

    return $query->result();  
  }

}
