<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
              
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }
  
  function get_barang_stok_zero() {
	$sql = " SELECT a.kode_brg, a.nama_brg, b.nama FROM tm_barang a, tm_satuan b where a.satuan = b.id
			AND a.status_aktif = 't' AND a.kode_brg NOT IN (select kode_brg FROM tm_stok) 
			UNION SELECT a.kode_brg, a.nama_brg, b.nama FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id
			AND a.status_aktif = 't' AND a.kode_brg NOT IN (select kode_brg FROM tm_stok_hasil_makloon)
			ORDER BY kode_brg ";
	$query	= $this->db->query($sql);
	
	$data_brg = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$pos = strpos($row1->nama_brg, "\"");
			  if ($pos > 0)
				$nama_brg_konv = str_replace("\"", "&quot;", $row1->nama_brg);
			  else
				$nama_brg_konv = str_replace("'", "\'", $row1->nama_brg); 
			
			$data_brg[] = array(			
								'kode_brg'=> $row1->kode_brg,
								'nama_brg'=> $nama_brg_konv,
								'satuan'=> $row1->nama
							);
		} // end foreach
	}
	else
		$data_brg = '';
    return $data_brg;  
  }
  
  function get_harga($idharga){
	$query = $this->db->query(" SELECT * FROM tm_harga_brg_supplier WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
			$hasilrow = $query3->row();
			$nama_supplier	= $hasilrow->nama;
			
			// ambil data nama barang
			$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			$nama_brg	= $hasilrow->nama_brg;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }

}
