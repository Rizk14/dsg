<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function clistvoucher($i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast) {
			$db2=$this->load->database('db_external', TRUE);
		if($ndvoucherfirst!='0' && $ndvoucherlast!='0') {
			$ivocuher = "";
			$dvoucher = " WHERE (a.d_voucher BETWEEN '$ndvoucherfirst' AND '$ndvoucherlast' ) ";	
			$fbatal = " AND a.f_voucher_cancel='f' ";
		}elseif(($ndvoucherfirst=='0' || $ndvoucherlast=='0') && ($no_voucher!='0' || $no_voucher!='')) {
			$ivocuher = " WHERE a.i_voucher='$i_voucher' AND a.i_voucher_no='$no_voucher' ";
			$dvoucher = "";
			$fbatal	= " AND a.f_voucher_cancel='f' ";
		}else{
			$fbatal	= " WHERE a.f_voucher_cancel='f' ";
		}

		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				
				".$ivocuher." ".$dvoucher." ".$fbatal."
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC ");
	}
		
	function clistvoucherperpages($limit,$offset,$i_voucher,$no_voucher,$ndvoucherfirst,$ndvoucherlast) {
			$db2=$this->load->database('db_external', TRUE);
		if($ndvoucherfirst!='0' && $ndvoucherlast!='0') {
			$ivocuher = "";
			$dvoucher = " WHERE (a.d_voucher BETWEEN '$ndvoucherfirst' AND '$ndvoucherlast' ) ";	
			$fbatal = " AND a.f_voucher_cancel='f' ";
		}elseif(($ndvoucherfirst=='0' || $ndvoucherlast=='0') && ($no_voucher!='0' || $no_voucher!='')) {
			$ivocuher = " WHERE a.i_voucher='$i_voucher' AND a.i_voucher_no='$no_voucher' ";
			$dvoucher = "";
			$fbatal	= " AND a.f_voucher_cancel='f' ";
		}else{
			$fbatal	= " WHERE a.f_voucher_cancel='f' ";
		}

		$query	= $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				
				".$ivocuher." ".$dvoucher." ".$fbatal."
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved, a.v_total_voucher
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC LIMIT ".$limit." OFFSET ".$offset." ");
							
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}			
	}
	
	function lvoucher() {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				WHERE a.f_voucher_cancel='f' 
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC ");		
	}	

	function lvoucherperpages($limit,$offset) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				WHERE a.f_voucher_cancel='f' 
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC LIMIT ".$limit." OFFSET ".$offset." ");
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}


	function flvoucher($key) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code AS kodesumber, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				FROM tm_voucher a
				
				INNER JOIN tm_voucher_item b ON b.i_voucher=a.i_voucher
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				WHERE a.f_voucher_cancel='f' AND (c.i_voucher_code LIKE '$key%' OR a.e_description LIKE '$key%') 
				
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_description, a.e_recieved, a.e_approved
				
				ORDER BY a.i_voucher ASC, a.d_voucher ASC ");
	}	
	
	function getvoucherheader($ivoucher,$novoucher) {
			$db2=$this->load->database('db_external', TRUE);
				return $db2->query(" SELECT a.i_voucher, 
										a.i_voucher_code, 
										a.i_voucher_no, 
										a.d_voucher, 
										a.i_company_code, 
										a.e_recieved, 
										a.e_approved,
										a.v_total_voucher,
										a.f_voucher_cancel,
										a.f_nilai_manual,
										a.f_approve_dept,
										a.e_description,
										b.i_voucher_code AS kodesumber, a.f_approved FROM tm_voucher a 
										
										INNER JOIN tr_kode_voucher b ON b.i_voucher=a.i_voucher_code 
										
										WHERE a.i_voucher='$ivoucher' AND a.i_voucher_no='$novoucher' AND a.f_voucher_cancel='f' ");
	}
	
	function lvoucheritem($ivoucher){
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, 
										 a.v_total_voucher,
										 a.f_nilai_manual,
										 b.i_voucher_item,
										 b.i_dt,
										 c.i_dt_code,
										 b.d_dt,
										 c.v_total_grand,
										 c.v_grand_sisa,
										 b.v_voucher,
										 b.f_nota_sederhana FROM tm_voucher a 
										 
										 INNER JOIN tm_voucher_item b ON a.i_voucher=b.i_voucher 
										 INNER JOIN tm_dt c ON c.i_dt=b.i_dt
										 
										 WHERE  a.i_voucher='$ivoucher' AND a.f_voucher_cancel='f' 
										 GROUP BY a.i_voucher, a.v_total_voucher, a.f_nilai_manual, b.i_voucher_item, b.i_dt, c.i_dt_code, b.d_dt, c.v_total_grand, c.v_grand_sisa, b.v_voucher, b.f_nota_sederhana
										 ORDER BY a.i_voucher ASC, b.i_voucher_item ASC ");
	}
	
	
	function mupdate($i_voucher,$ckapprove,$kode_sumber,$no_voucher) {
			$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$db2->query(" UPDATE tm_voucher SET f_approved='$ckapprove', d_approved='$dentry' WHERE i_voucher='$i_voucher' AND f_voucher_cancel='f' AND f_approved='f' ");
		
		if ($db2->trans_status()===FALSE) {
			$ok	= 0;
			$db2->trans_rollback();
		} else {
			$ok	= 1;
			$db2->trans_commit();
		}
		
		if($ok==1) {
			print "<script>alert(\"Nomor voucher : '\"+$kode_sumber+\"' - '\"+$no_voucher+\"' telah diapprove, terimakasih.\");show(\"approvevoucher/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, voucher gagal diapprove. Terimakasih.\");show(\"approvevoucher/cform\",\"#content\");</script>";
		}
	}
}

?>
