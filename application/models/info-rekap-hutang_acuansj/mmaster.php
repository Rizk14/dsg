<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}

	function get_all_rekap_pembelian($jenis_beli, $date_from, $date_to) {
	//if ($jenis_beli == 1) {			
		/*$query	= $this->db->query(" SELECT distinct a.kode_supplier, b.nama FROM tm_pembelian_nofaktur a, tm_supplier b 
					WHERE a.kode_supplier = b.kode_supplier 
					AND a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '1' ORDER BY a.kode_supplier "); */
		$sql = " SELECT kode_supplier, nama FROM tm_supplier ORDER BY kode_supplier ";
		$query	= $this->db->query($sql);
	
		$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// 1. hitung jumlah saldo awal yg tanggalnya < date_from ===================
				// 25 mei 2011
				// hitung total hutang dagang, jum retur
				
				/*$sql = "SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a, tm_supplier b 
				WHERE a.kode_supplier = b.kode_supplier AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND a.status_aktif = 't'"; 
				if ($row1->kode_supplier == 'SA003') echo $sql."<br>"; */
				
				// 14-04-2012
				$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a, tm_supplier b 
				WHERE a.kode_supplier = b.kode_supplier AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND a.status_aktif = 't' ");
				$hasilrow = $query2->row();
				$tot_hutang	= $hasilrow->tot_hutang;
				if ($tot_hutang == '')
					$tot_hutang = 0;
				
				$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_sj_hasil_makloon a, tm_supplier b 
				WHERE a.kode_unit = b.kode_supplier AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
				AND a.kode_unit = '$row1->kode_supplier' ");
				$hasilrow = $query2->row();
				$tot_hutang_quilting	= $hasilrow->tot_hutang;
				if ($tot_hutang_quilting == '')
					$tot_hutang_quilting = 0;
				
				$tot_hutang += $tot_hutang_quilting;
				
				// hitung jum uang muka,
				$query2	= $this->db->query(" SELECT SUM(uang_muka) as tot_uang_muka FROM tm_pembelian
				WHERE tgl_sj < to_date('$date_from','dd-mm-yyyy') 
				AND kode_supplier = '$row1->kode_supplier' AND jenis_pembelian = '$jenis_beli' AND status_aktif = 't' ");
				$hasilrow = $query2->row();
				$uang_muka	= $hasilrow->tot_uang_muka;
				if ($uang_muka == '')
					$uang_muka = 0;
				// 29 nov 2011
				$query2	= $this->db->query(" SELECT SUM(uang_muka) as tot_uang_muka FROM tm_sj_hasil_makloon
				WHERE tgl_sj < to_date('$date_from','dd-mm-yyyy') 
				AND kode_unit = '$row1->kode_supplier' AND jenis_pembelian = '$jenis_beli' ");
				$hasilrow = $query2->row();
				$uang_muka_quilting	= $hasilrow->tot_uang_muka;
				if ($uang_muka_quilting == '')
					$uang_muka_quilting = 0;
				
				$uang_muka+= $uang_muka_quilting;
				
			/*	$sql = "SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat 
				FROM tm_payment_pembelian a, 
				tm_payment_pembelian_detail b
				WHERE a.id = b.id_payment AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl < to_date('$date_from','dd-mm-yyyy')
				AND b.kode_supplier = '$row1->kode_supplier'";
				if ($row1->kode_supplier == 'SA003') echo $sql."<br>"; */
				
				$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat 
				FROM tm_payment_pembelian a, 
				tm_payment_pembelian_detail b
				WHERE a.id = b.id_payment AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl < to_date('$date_from','dd-mm-yyyy')
				AND b.kode_supplier = '$row1->kode_supplier' ");
				
				$hasilrow = $query2->row();
				$tot_bayar	= $hasilrow->tot_bayar;
				$tot_bulat	= $hasilrow->tot_bulat;
				if ($tot_bayar == '')
					$tot_bayar = 0;
				if ($tot_bulat == '')
					$tot_bulat = 0;
								
			/*	$sql = "SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
				tm_retur_beli_detail c WHERE a.id = c.id_retur_beli
				AND a.jenis_pembelian = '$jenis_beli'
				AND a.kode_supplier = '$row1->kode_supplier'
				AND a.tgl_retur < to_date('$date_from','dd-mm-yyyy')"; if ($row1->kode_supplier == 'SA003') echo $sql."<br>"; */
				
				$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
				tm_retur_beli_detail c WHERE a.id = c.id_retur_beli
				AND a.jenis_pembelian = '$jenis_beli'
				AND a.kode_supplier = '$row1->kode_supplier'
				AND a.tgl_retur < to_date('$date_from','dd-mm-yyyy') ");
					
				$hasilrow = $query2->row();
				$tot_retur	= $hasilrow->tot_retur;	
				
				if ($tot_retur == '')
					$tot_retur = '0';
								//if ($row1->kode_supplier == 'SR002') echo $tot_hutang."<br>";
				$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar-$tot_retur+$tot_bulat;
				$tot_hutang = abs($tot_hutang);
				//if ($row1->kode_supplier == 'ST001') echo $tot_hutang;
				// tot_hutang ini sebagai saldo awal
					
				// ########################################################### end untuk saldo awal @@@@@@@@@@@@
								
				// 2. hitung jumlah uang muka di tabel SJ/pembelian
				$query2	= $this->db->query(" SELECT SUM(uang_muka) as tot_uang_muka FROM tm_pembelian
				WHERE tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
				AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
				AND kode_supplier = '$row1->kode_supplier' AND jenis_pembelian = '$jenis_beli' AND status_aktif = 't' ");
				$hasilrow = $query2->row();
				$uang_muka	= $hasilrow->tot_uang_muka;
				if ($uang_muka == '')
					$uang_muka = 0;
				
				// 29 nov 2011
				$query2	= $this->db->query(" SELECT SUM(uang_muka) as tot_uang_muka FROM tm_sj_hasil_makloon
				WHERE tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
				AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
				AND kode_unit = '$row1->kode_supplier' AND jenis_pembelian = '$jenis_beli' ");
				$hasilrow = $query2->row();
				$uang_muka_quilting	= $hasilrow->tot_uang_muka;
				if ($uang_muka_quilting == '')
					$uang_muka_quilting = 0;
				
				$uang_muka+= $uang_muka_quilting;
				
				// 3. hitung jumlah yg sudah dibayar di tabel payment_pembelian		
				$sql = "SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat 
				FROM tm_payment_pembelian a, 
				tm_payment_pembelian_detail b
				WHERE a.id = b.id_payment AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl >= to_date('$date_from','dd-mm-yyyy') AND a.tgl <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier'"; if ($row1->kode_supplier == 'SA003') echo $sql."<br>";
						
				$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat 
				FROM tm_payment_pembelian a, 
				tm_payment_pembelian_detail b
				WHERE a.id = b.id_payment AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl >= to_date('$date_from','dd-mm-yyyy') AND a.tgl <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' ");
				
				$hasilrow = $query2->row();
				$tot_bayar	= $hasilrow->tot_bayar;
				$tot_bulat	= $hasilrow->tot_bulat;
				if ($tot_bayar == '')
					$tot_bayar = 0;
				if ($tot_bulat == '')
					$tot_bulat = 0;
								
				// ##########################################################
								 
				// 4. jumlah retur utk faktur tsb jika ada
				// koreksi 25 okt 2011: ga perlu ada acuan ke status_nota di nota retur
				
				$sql = "SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
				tm_retur_beli_detail c
				WHERE a.id = c.id_retur_beli
				AND a.jenis_pembelian = '$jenis_beli'
				AND a.kode_supplier = '$row1->kode_supplier'
				AND a.tgl_retur >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_retur <= to_date('$date_to','dd-mm-yyyy')";
				if ($row1->kode_supplier == 'SA003') echo $sql."<br>";
				
				$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
				tm_retur_beli_detail c
				WHERE a.id = c.id_retur_beli
				AND a.jenis_pembelian = '$jenis_beli'
				AND a.kode_supplier = '$row1->kode_supplier'
				AND a.tgl_retur >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_retur <= to_date('$date_to','dd-mm-yyyy') ");
					
				$hasilrow = $query2->row();
				$tot_retur	= $hasilrow->tot_retur;	
				
				if ($tot_retur == '')
					$tot_retur = '0';
				// ##########################################################
								
				// 5. hitung jumlah pembelian pada range tanggal tsb
				$sql = " SELECT SUM(a.total) as tot_pembelian FROM tm_pembelian a, tm_supplier b 
				WHERE a.kode_supplier = b.kode_supplier
				AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' ";
				if ($row1->kode_supplier == 'SA003') echo $sql."<br>";
				
				$query2	= $this->db->query(" SELECT SUM(a.total) as tot_pembelian FROM tm_pembelian a, tm_supplier b 
				WHERE a.kode_supplier = b.kode_supplier
				AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' ");
				$hasilrow = $query2->row();
				$tot_pembelian	= $hasilrow->tot_pembelian;
				if ($tot_pembelian == '')
					$tot_pembelian = 0;
				// 29 nov 2011
				$query2	= $this->db->query(" SELECT SUM(a.total) as tot_pembelian FROM tm_sj_hasil_makloon a, tm_supplier b 
				WHERE a.kode_unit = b.kode_supplier
				AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND a.jenis_pembelian = '$jenis_beli' ");
				$hasilrow = $query2->row();
				$tot_pembelian_quilting	= $hasilrow->tot_pembelian;
				if ($tot_pembelian_quilting == '')
					$tot_pembelian_quilting = 0;
				$tot_pembelian += $tot_pembelian_quilting;
				
				// 6. hitung saldo akhir
				//$saldo_akhir = $tot_hutang+$uang_muka+$tot_bayar-$tot_pembelian;
				$saldo_akhir = $tot_hutang-$uang_muka-$tot_bayar+$tot_bulat-$tot_retur+$tot_pembelian;
				//$saldo_akhir = intval($saldo_akhir);
									
				if ($saldo_akhir == '')
					$saldo_akhir = 0;
				
				// ==========================================================================
												
				$data_beli[] = array(		'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'tot_hutang'=> $tot_hutang,
											'tot_debet'=> $uang_muka+$tot_bayar,
											'tot_pembelian'=> $tot_pembelian,
											'tot_bulat'=> $tot_bulat,
											'tot_retur'=> $tot_retur,
											'saldo_akhir'=> $saldo_akhir
											);
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  function get_all_rekap_pembeliantanpalimit($jenis_beli, $date_from, $date_to){
		$query	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier ");
	    
    return $query->result();  
  }
          
}

