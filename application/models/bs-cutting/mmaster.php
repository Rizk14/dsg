<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari) {	  
	if ($cari == "all") {
			$this->db->select(" * FROM tm_hasil_cutting_bs ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
	else {
			$this->db->select(" * FROM tm_hasil_cutting_bs WHERE UPPER(kode_brg_jadi) like UPPER('%$cari%') 
						OR UPPER(kode_brg) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
		$data_bs = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// 26-01-2012
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
					}
					else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg = '';
							$satuan = '';
						}
					}
					if ($row1->kode_brg_jadi != '') {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
									WHERE i_product_motif = '$row1->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							if ($row1->brg_jadi_manual == '')
								$nama_brg_jadi = '';
							else
								$nama_brg_jadi = $row1->brg_jadi_manual;
						}
					}
					else
						$nama_brg_jadi = '';
				// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
												
				$data_bs[] = array(			'id'=> $row1->id,	
											'tgl_input'=> $row1->tgl_input,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'jumlah'=> $row1->jumlah,
											'diprint'=> $row1->diprint,
											'is_dacron'=> $row1->is_dacron
											);
			} // endforeach header
		}
		else {
			$data_bs = '';
		}
		return $data_bs;
  }
  
  function getAlltanpalimit($cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_hasil_cutting_bs ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_hasil_cutting_bs WHERE UPPER(kode_brg_jadi) like UPPER('%$cari%') 
						OR UPPER(kode_brg) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  //
  function save($tgl_input, $ket, $kode_brg, $qty, $kode_brg_jadi, $brg_jadi_manual, $diprint, $is_dacron){  
		$tgl = date("Y-m-d");
			// insert di tm_hasil_cutting_bs
			$data_header = array(
			  'tgl_input'=>$tgl_input,
			  'keterangan'=>$ket,
			  'kode_brg_jadi'=>$kode_brg_jadi,
			  'brg_jadi_manual'=>$brg_jadi_manual,
			  'kode_brg'=>$kode_brg,
			  'diprint'=>$diprint,
			  'is_dacron'=>$is_dacron,
			  'jumlah'=>$qty
			);
			$this->db->insert('tm_hasil_cutting_bs',$data_header);
			
		// update stoknya (mengurangi stok)
		// 23-05-2012
		$sqlxx = " SELECT id, stok FROM tm_stok_hasil_cutting 
					WHERE kode_brg_jadi='$kode_brg_jadi' AND kode_brg='$kode_brg' AND diprint = '$diprint'
					AND is_dacron = '$is_dacron'";
		if ($brg_jadi_manual != '')
			$sqlxx.= " AND brg_jadi_manual = '$brg_jadi_manual' ";
		else
			$sqlxx.= " AND (brg_jadi_manual is NULL OR brg_jadi_manual = '') ";
		$query3 = $this->db->query($sqlxx);
							
			/*		$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg = '$kode_brg'
										 AND kode_brg_jadi = '$kode_brg_jadi' "); */
					if ($query3->num_rows() == 0){
						$id=0;
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
						$id = $hasilrow->id;
					}
					$new_stok = $stok_lama-$qty;
					//
					if ($query3->num_rows() == 0){ // jika blm ada data, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg,
							'kode_brg_jadi'=>$kode_brg_jadi,
							'diprint'=>$diprint,
							'is_dacron'=>$is_dacron,
							'brg_jadi_manual'=>$brg_jadi_manual,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id= '$id' ");
					}
					
  }
    
  function delete($kode){
	$tgl = date("Y-m-d");	
	
	$query4	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, brg_jadi_manual, diprint, is_dacron, jumlah 
							FROM tm_hasil_cutting_bs WHERE id = '$kode' ");
	if ($query4->num_rows() > 0){
		$hasil4 = $query4->row();
		$kode_brg	= $hasil4->kode_brg;
		$kode_brg_jadi	= $hasil4->kode_brg_jadi;
		$jumlah	= $hasil4->jumlah;
		$diprint	= $hasil4->diprint;
		$is_dacron	= $hasil4->is_dacron;
		$brg_jadi_manual	= $hasil4->brg_jadi_manual;
		
		// update stoknya (menambah stok)
		// 23-05-2012
		$sqlxx = " SELECT id, stok FROM tm_stok_hasil_cutting 
					WHERE kode_brg_jadi='$kode_brg_jadi' AND kode_brg='$kode_brg' AND diprint = '$diprint'
					AND is_dacron = '$is_dacron'";
		if ($brg_jadi_manual != '')
			$sqlxx.= " AND brg_jadi_manual = '$brg_jadi_manual' ";
		else
			$sqlxx.= " AND (brg_jadi_manual is NULL OR brg_jadi_manual = '') ";
		$query3 = $this->db->query($sqlxx);
		
					if ($query3->num_rows() == 0){
						$id=0;
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
						$id = $hasilrow->id;
					}
					$new_stok = $stok_lama+$jumlah;
					
					if ($query3->num_rows() == 0){ // jika blm ada data, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg,
							'kode_brg_jadi'=>$kode_brg_jadi,
							'diprint'=>$diprint,
							'is_dacron'=>$is_dacron,
							'brg_jadi_manual'=>$brg_jadi_manual,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where id= '$id' ");
					}
					
	} // end if

    $this->db->delete('tm_hasil_cutting_bs', array('id' => $kode));
  }
            
  function get_bahan($num, $offset, $cari)
  {
		$sql = " a.* FROM tm_stok_hasil_cutting a 
				LEFT JOIN tm_barang b ON a.kode_brg = b.kode_brg
				LEFT JOIN tm_brg_hasil_makloon c ON a.kode_brg = c.kode_brg 
				LEFT JOIN tr_product_motif d ON a.kode_brg_jadi = d.i_product_motif 
				WHERE TRUE ";
		if ($cari != "all")
			$sql.= " AND UPPER(b.kode_brg) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%') 
					OR UPPER(c.kode_brg) LIKE UPPER('%$cari%') OR UPPER(c.nama_brg) LIKE UPPER('%$cari%')
					OR UPPER(a.brg_jadi_manual) LIKE UPPER('%$cari%') OR UPPER(a.brg_jadi_manual) LIKE UPPER('%$cari%')
					OR UPPER(d.e_product_motifname) LIKE UPPER('%$cari%') OR UPPER(d.i_product_motif) LIKE UPPER('%$cari%') ";
		$sql.= " ORDER BY a.kode_brg_jadi ";
		$this->db->select($sql, false)->limit($num,$offset);

    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$kode_brg = $hasilrow->kode_brg;
				$nama_brg = $hasilrow->nama_brg;
				$satuan = $hasilrow->nama_satuan;
			}
			else {
				$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan 
							FROM tm_brg_hasil_makloon a, tm_satuan b
							WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg = $hasilrow->kode_brg;
					$nama_brg = $hasilrow->nama_brg;
					$satuan = $hasilrow->nama_satuan;
				}
				else {
					$kode_brg = '';
					$nama_brg = '';
					$satuan = '';
				}
			}
			
			// brg jadi
			if ($row1->kode_brg_jadi != '') {
				$qbrgjadi	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row1->kode_brg_jadi' ");
				if ($qbrgjadi->num_rows() > 0){
					$rbrgjadi	= $qbrgjadi->row();
					$nama_brg_jadi = $rbrgjadi->e_product_motifname;
				}
				else {
					if ($row1->brg_jadi_manual == '')
						$nama_brg_jadi = '';
					else
						$nama_brg_jadi = $row1->brg_jadi_manual;
				}
			}
			else
				$nama_brg_jadi = '';
					
			$data_bhn[] = array(		'kode_brg'=> $kode_brg,	
										'nama_brg'=> $nama_brg,
										'satuan'=> $satuan,
										'kode_brg_jadi'=> $row1->kode_brg_jadi,	
										'nama_brg_jadi'=> $nama_brg_jadi,
										'brg_jadi_manual'=> $row1->brg_jadi_manual,	
										'tgl_update_stok'=> $row1->tgl_update_stok,
										'stok'=> $row1->stok,
										'diprint'=> $row1->diprint,	
										'is_dacron'=> $row1->is_dacron
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari){
	  /*$sql = " SELECT * FROM tm_stok_hasil_cutting a 
				LEFT JOIN tm_barang b ON a.kode_brg = b.kode_brg
				LEFT JOIN tm_brg_hasil_makloon c ON a.kode_brg = c.kode_brg 
				WHERE a.kode_brg_jadi = '$kode_brg_jadi' ";
		if ($cari != "all")
			$sql.= " AND UPPER(b.kode_brg) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%') 
					OR UPPER(c.kode_brg) LIKE UPPER('%$cari%') UPPER(c.nama_brg) LIKE UPPER('%$cari%')  "; */
	  	$sql = " select a.* FROM tm_stok_hasil_cutting a 
				LEFT JOIN tm_barang b ON a.kode_brg = b.kode_brg
				LEFT JOIN tm_brg_hasil_makloon c ON a.kode_brg = c.kode_brg 
				LEFT JOIN tr_product_motif d ON a.kode_brg_jadi = d.i_product_motif 
				WHERE TRUE ";
		if ($cari != "all")
			$sql.= " AND UPPER(b.kode_brg) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%') 
					OR UPPER(c.kode_brg) LIKE UPPER('%$cari%') OR UPPER(c.nama_brg) LIKE UPPER('%$cari%')
					OR UPPER(a.brg_jadi_manual) LIKE UPPER('%$cari%') OR UPPER(a.brg_jadi_manual) LIKE UPPER('%$cari%')
					OR UPPER(d.e_product_motifname) LIKE UPPER('%$cari%') OR UPPER(d.i_product_motif) LIKE UPPER('%$cari%') ";
		$sql.= " ORDER BY a.kode_brg_jadi ";
	  	
		$query	= $this->db->query($sql);
		return $query->result();  
	
  }
  
  // 12 des 2011
  function get_bahanbisbisan($num, $offset, $cari) {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" order by a.kode_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.kode_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok_hasil_bisbisan 
								WHERE kode_brg_bisbisan = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahanbisbisantanpalimit($cari) {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" order by a.kode_brg ";
		
		$this->db->select($sql, false);
	}
	else {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.kode_brg ";
		
		$this->db->select($sql, false);	
    }
    $query = $this->db->get();
	return $query->result();  
  }
  
  function get_pb_cutting($id_pb) {
		$query	= $this->db->query(" SELECT * FROM tm_pb_cutting where id = '$id_pb' ");
	
		$data_pb = array();
		$detail_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detail pbnya
				$query2	= $this->db->query(" SELECT * FROM tm_pb_cutting_detail WHERE id_pb_cutting = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->jenis_proses == '3') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_quilting' ");
								
							$hasilrow = $query3->row();
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$satuan_quilting	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
						// jika jenis prosesnya bisbisan
						if ($row2->jenis_proses == 4) {
							// 13 des 2011
							if ($row2->kode_brg_bisbisan != '') {
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
											FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id 
											AND a.kode_brg = '$row2->kode_brg_bisbisan' ");
									
								$hasilrow = $query3->row();
								$nama_brg_bisbisan	= $hasilrow->nama_brg;
								$satuan_bisbisan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg_bisbisan = '';
								$satuan_bisbisan = '';
							}
							
							// ambil data kebutuhan perpcs bisbisan
							$sql = " SELECT id, jum_kebutuhan FROM tm_keb_bisbisan_perpcs 
								WHERE kode_brg_jadi = '$row1->kode_brg_jadi' AND kode_brg = '$row2->kode_brg' 
								AND id_ukuran_bisbisan = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_kebutuhan	= $hasilrow->id;
								$jum_kebutuhan	= $hasilrow->jum_kebutuhan;
							}
							else {
								$id_kebutuhan = 0;
								$jum_kebutuhan	= 0;
							}
							
							// ambil variabel2 rumus
							$sql = " SELECT var1, var2, var3 FROM tm_ukuran_bisbisan 
								WHERE id = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$var1	= $hasilrow->var1;
								$var2	= $hasilrow->var2;
								$var3	= $hasilrow->var3;
							}
							else {
								$var1 = 0;
								$var2 = 0;
								$var3 = 0;
							}
						}
						else {
							$id_kebutuhan = 0;
							$jum_kebutuhan	= 0;
							$var1 = 0;
							$var2 = 0;
							$var3 = 0;
							$nama_brg_bisbisan = '';
							$satuan_bisbisan = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
										
						$detail_pb[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'gelaran'=> $row2->gelaran,
												'jum_set'=> $row2->jum_set,
												'jum_gelar'=> $row2->jum_gelar,
												'panjang_kain'=> $row2->panjang_kain,
												'jenis_proses'=> $row2->jenis_proses,
												'id_marker'=> $row2->id_marker_gelaran,
												'kode_brg_quilting'=> $row2->kode_brg_quilting,
												'nama_brg_quilting'=> $nama_brg_quilting,
												'kode_brg_bisbisan'=> $row2->kode_brg_bisbisan,
												'nama_brg_bisbisan'=> $nama_brg_bisbisan,
												'ukuran_bisbisan'=> $row2->id_ukuran_bisbisan,
												//'id_kebutuhanperpcs'=> $row2->id_kebutuhan_perpcs,
												'id_kebutuhan'=> $id_kebutuhan,
												'jum_kebutuhan'=> $jum_kebutuhan,
												'var1'=> $var1,
												'var2'=> $var2,
												'var3'=> $var3
											);
					}
				}
				else {
					$detail_pb = '';
				}
				
				$pisah1 = explode("-", $row1->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$row1->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi = '';
						}
												
				$data_pb[] = array(			'id'=> $row1->id,	
											'no_pb_cutting'=> $row1->no_pb_cutting,
											'tgl'=> $tgl,
											'ket'=> $row1->keterangan,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'detail_pb'=> $detail_pb
											);
				$detail_pb = array();
			} // endforeach header
		}
		else {
			$data_pb = '';
		}
		return $data_pb;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  // new 4 april 2011
  function create_bonm($no_request, $kode, $nama, $qty){
    $tgl = date("Y-m-d");
    $th_now	= date("Y");
	
		// ====================================================
			// insert Bon M masuk secara otomatis di tm_apply_stok_proses_quilting
			
				// ambil data gudang sesuai barangnya
				$query3	= $this->db->query(" SELECT id_gudang FROM tm_barang WHERE kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$id_gudang = $hasilrow->id_gudang;
				
				// cek apakah header bonm udh ada
				$query3	= $this->db->query(" SELECT id, no_bonm FROM tm_apply_stok_proses_quilting 
				WHERE no_pb_quilting = '$no_request' AND id_gudang = '$id_gudang' ");
				if ($query3->num_rows() > 0) { // jika udh ada
					$hasilrow = $query3->row();
					$no_bonm = $hasilrow->no_bonm;
					$id_apply_stok = $hasilrow->id;
					
					//save ke tabel tm_apply_stok_proses_quilting_detail
					if ($kode!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_proses_quilting_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'id_apply_stok'=>$id_apply_stok
						);
						$this->db->insert('tm_apply_stok_proses_quilting_detail',$data_detail);
						
					} // end if kode != '' dan $qty != ''
				}
				else {
					// generate no Bon M
					$query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_proses_quilting WHERE id_gudang = '$id_gudang' 
					ORDER BY no_bonm DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() != 0) 
						$no_bonm	= $hasilrow->no_bonm;
					else
						$no_bonm = '';
					if(strlen($no_bonm)==13) {
						$nobonm = substr($no_bonm, 4, 9);
						$n_bonm	= (substr($nobonm,4,5))+1;
						$th_bonm	= substr($nobonm,0,4);
						if($th_now==$th_bonm) {
								$jml_n_bonm	= $n_bonm; //
								switch(strlen($jml_n_bonm)) {
									case "1": $kodebonm	= "0000".$jml_n_bonm;
									break;
									case "2": $kodebonm	= "000".$jml_n_bonm;
									break;	
									case "3": $kodebonm	= "00".$jml_n_bonm;
									break;
									case "4": $kodebonm	= "0".$jml_n_bonm;
									break;
									case "5": $kodebonm	= $jml_n_bonm;
									break;	
								}
								$nomorbonm = $th_now.$kodebonm;
						}
						else {
							$nomorbonm = $th_now."00001";
						}
					}
					else {
						$nomorbonm	= $th_now."00001";
					}
					$no_bonm = "BKM-".$nomorbonm;
					
					// insert di tm_apply_stok_proses_quilting
					$data_header2 = array(
					  'no_bonm'=>$no_bonm,
					  'tgl_bonm'=>$tgl,
					  'no_pb_quilting'=>$no_request,
					  'id_gudang'=>$id_gudang,
					  'tgl_input'=>$tgl,
					  'tgl_update'=>$tgl
					);
					$this->db->insert('tm_apply_stok_proses_quilting',$data_header2);
					
					// ambil data terakhir di tabel tm_apply_stok_proses_quilting
					$query2	= $this->db->query(" SELECT id FROM tm_apply_stok_proses_quilting ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_apply_stok	= $hasilrow->id;
					
					//save ke tabel tm_apply_stok_proses_quilting_detail
					if ($kode!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_proses_quilting_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'id_apply_stok'=>$id_apply_stok
						);
						$this->db->insert('tm_apply_stok_proses_quilting_detail',$data_detail);
												
					} // end if kode != '' dan $qty != ''
				} // end else

			// ====================================================
  }
  
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
}
