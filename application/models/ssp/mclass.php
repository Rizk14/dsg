<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function lpenyetor(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_penyetor ORDER BY d_entry DESC ");
	}

	function lpenyetorperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_penyetor ORDER BY d_entry DESC LIMIT ".$limit." OFFSET ".$offset." ");
		if($query->num_rows()>0){
			return $result	= $query->result();
		}	
	}
		
	function ljnssetorpajak() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_jns_setor_pajak ORDER BY d_entry DESC ");
	}
	
	function ljnssetorpajakperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_jns_setor_pajak ORDER BY d_entry DESC LIMIT ".$limit." OFFSET ".$offset." ");
		if($query->num_rows()>0){
			return $result	= $query->result();
		}	
	}

	function linisial(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ");
	}
	
	function linisialperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' LIMIT ".$limit." OFFSET ".$offset." ");
		if($query->num_rows()>0){
			return $result	= $query->result();
		}		
	}
			
	function msimpan($wajibpajak1,$i_initial1,$kdakunpajak1,$kdjnssetor1,$uraipembayaran1,$bln1,$thn1,$jmlpebayaran1,$tmptwp1,$tglwp1,$penyetor1,$ipenyetor1) {
			$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
					
		$tm_ssp	=
			array(
			 'e_initial_npwp'=>$wajibpajak1,
			 'i_akun_pajak'=>$kdakunpajak1,
			 'i_jsetor_pajak'=>$kdjnssetor1,
			 'e_jsetor_pajak'=>$uraipembayaran1,
			 'd_bln'=>$bln1,
			 'd_thn'=>$thn1,
			 'v_jml_bayar'=>$jmlpebayaran1,
			 'e_tempat_wp'=>$tmptwp1,
			 'd_wp'=>$tglwp1,
			 'i_penyetor'=>$ipenyetor1,
			 'd_entry'=>$dentry);

		$db2->insert('tm_ssp',$tm_ssp);
				
		if ($db2->trans_status()===FALSE) {
			print "<script>alert(\"Maaf, Data SSP gagal disimpan!\");window.open(\"index\", \"_self\");</script>";
			$db2->trans_rollback();
		} else {
			print "<script>alert(\"Data SSP telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
			$db2->trans_commit();
		}
		
	}
}
?>
