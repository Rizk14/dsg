<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function fprinted($nofaktur) {
		return $this->db->query(" UPDATE tm_faktur_bhnbaku SET f_printed='t' WHERE i_faktur_code='$nofaktur' ");
	}
		
	function clistpenjualanndo($nofaktur) {

		/* 07122011	
		$query	= $this->db->query(" S SELECT c.i_product AS imotif,
					c.e_product_name AS motifname,
					a.n_quantity AS qty,
					e.e_satuan,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_bhnbaku_item a
								
				INNER JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				INNER JOIN tr_satuan e ON e.i_satuan=CAST(a.e_satuan AS integer)
				
				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY c.i_product, c.e_product_name, a.n_quantity, a.v_unit_price, e.e_satuan ");
		*/
		
		$query = $this->db->query(" SELECT a.i_product AS imotif, 
			a.e_product_name AS motifname, 
			a.n_quantity AS qty, 
			e.e_satuan, 
			a.v_unit_price AS unitprice, 
			(a.n_quantity * a.v_unit_price) AS amount 

			FROM tm_faktur_bhnbaku_item a 

			INNER JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur 
			INNER JOIN tr_satuan e ON e.i_satuan=CAST(a.e_satuan AS integer) 

			WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f' 

			GROUP BY a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price, e.e_satuan 
		");
	
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function clistpenjualanndo2($nofaktur) {
		
		/* Disabled 07122011	
		$query	= $this->db->query(" SELECT a.i_faktur, 
					b.i_faktur_code, 
					c.i_product AS imotif,
					c.e_product_name AS motifname
					
				FROM tm_faktur_bhnbaku_item a
								
				RIGHT JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)

				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel='f' AND d.f_faktur_created='t' 
				
				GROUP BY a.i_faktur, b.i_faktur_code, c.i_product, c.e_product_name " );
		*/
		
		$query = $this->db->query(" SELECT a.i_faktur, 
					b.i_faktur_code, 
					a.i_product AS imotif,
					a.e_product_name AS motifname
					
				FROM tm_faktur_bhnbaku_item a
								
				RIGHT JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur

				WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f'
				
				GROUP BY a.i_faktur, b.i_faktur_code, a.i_product, a.e_product_name ");
	
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	
	function clistfpenjndo_header($nofaktur) {
		/*
		Ini dipakai utk menmpikan info : Nomor Faktur, Tanggal Faktur, Tgl Jatuh Tempo
		*/
		
		/* Disabled 07122011
		return $this->db->query(" SELECT count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname,
						b.d_pajak AS dpajak
				
				FROM tm_faktur_bhnbaku_item a
								
				RIGHT JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(a.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name, b.d_pajak " );
		*/

		return $this->db->query(" SELECT count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname,
						b.d_pajak AS dpajak
				
				FROM tm_faktur_bhnbaku_item a
								
				INNER JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur
				
				WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f'
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name, b.d_pajak ");
	}

	function clistfpenjndo_jml($nofaktur) {

		return $this->db->query(" SELECT b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					sum((a.n_quantity * a.v_unit_price)) AS total
			
			FROM tm_faktur_bhnbaku_item a
					
			RIGHT JOIN tm_faktur_bhnbaku b ON b.i_faktur=a.i_faktur
			
			WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f'

			GROUP BY b.n_discount, b.v_discount ");
	}

	function lbarangjadiperpages($limit,$offset) {
		
		/* Disabled 07122011
		$query	= $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
										
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset );
		*/

		$query	= $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
										
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				
				WHERE a.f_faktur_cancel='f' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset );

		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		
		/* Disabled 07122011
		return $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
		*/

		return $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				
				WHERE a.f_faktur_cancel='f' AND a.f_printed='f'

				GROUP BY a.d_faktur, a.i_faktur_code

				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}
	
	function flbarangjadi($key) {
		/* Disabled 07122011
		return $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE a.i_faktur_code='$key' AND d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );	
		*/
		return $this->db->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
							
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				
				WHERE a.i_faktur_code='$key' AND a.f_faktur_cancel='f' AND a.f_printed='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}
	
	function lklsbrg() {
		$query	= $this->db->query(" SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC " );
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	
	function ititas() {
		return $this->db->query(" SELECT a.* FROM tr_initial_company a ORDER BY i_initial, i_initial_code DESC LIMIT 1 " );
	}
	
	function pelanggan($nofaktur) {
		
		return $this->db->query(" SELECT a.e_customer_name  AS customername,
				   a.e_customer_address AS customeraddress,
				   a.e_customer_npwp AS npwp
			
			FROM tr_customer a
			
			INNER JOIN tr_branch b ON b.i_customer=a.i_customer
			INNER JOIN tm_faktur_bhnbaku c ON c.e_branch_name=b.e_initial
			INNER JOIN tm_faktur_bhnbaku_item d ON d.i_faktur=c.i_faktur
			
			WHERE c.i_faktur_code='$nofaktur' AND c.f_faktur_cancel='f'
			
			GROUP BY a.e_customer_name, a.e_customer_address, a.e_customer_npwp
		");
	}
	
	function pajak($nofaktur) {
		/* Disabled 07122011	
		return $this->db->query(" SELECT b.i_faktur_pajak AS ifakturpajak, b.d_pajak
			
			FROM tm_faktur_bhnbaku b
			
			INNER JOIN tm_faktur_bhnbaku_item a ON b.i_faktur=a.i_faktur
			
			INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(a.i_product)
			INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
			
			WHERE b.i_faktur_code='$nofaktur' AND d.f_sj_cancel=false AND d.f_faktur_created=true
			
			GROUP BY b.i_faktur_pajak, b.d_pajak ");
		*/

		return $this->db->query(" SELECT b.i_faktur_pajak AS ifakturpajak, b.d_pajak
			
			FROM tm_faktur_bhnbaku b
			
			INNER JOIN tm_faktur_bhnbaku_item a ON b.i_faktur=a.i_faktur
			
			WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f'
			
			GROUP BY b.i_faktur_pajak, b.d_pajak ");
	}
	
	function remote($id) {
		return $this->db->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' ORDER BY i_printer DESC LIMIT 1 ");
	}

	function jmlitemharga($ifaktur,$iproduct) {
		return $this->db->query(" SELECT sum(n_quantity) AS qty, 
					v_unit_price AS unitprice,
					sum(v_unit_price) AS jmlunitprice,
					sum(n_quantity * v_unit_price) AS amount
				
				FROM tm_faktur_bhnbaku_item

				WHERE i_faktur='$ifaktur' AND i_product='$iproduct'
				
				GROUP BY i_product, i_faktur, v_unit_price
		");
	}	

	function fincludeppn($nofaktur) {
		return $this->db->query(" SELECT f_include_ppn AS fincludeppn FROM tm_faktur_bhnbaku WHERE i_faktur_code='$nofaktur' AND f_faktur_cancel='f' ");
	}	
}
?>
