<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function cari_fpenjualan($fpenj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_bhnbaku WHERE i_faktur_code=trim('$fpenj') AND f_faktur_cancel='f' ");
	}
	
	function cari_fpajak($fpajak,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_bhnbaku WHERE substring(i_faktur_code,1,4)='$tahun' and i_faktur_pajak='$fpajak' AND f_faktur_cancel='f' ");
	}
	
	function getnomorfaktur() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function nofakturpajak() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	function cari_fpenjualan_do($fpenj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur_code=trim('$fpenj') ");
	}

	function cari_fpajak_do($fpajak,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_do_t WHERE substring(i_faktur_code,1,4)='$tahun' AND i_faktur_pajak='$fpajak' ");
	}
	
	function tahunnow_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn	= date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thnfaktur 
			
			FROM tm_faktur_do_t 
			
			WHERE SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn'
			
			ORDER BY i_faktur DESC LIMIT 1 ");	
	}
	
	function getnomorfaktur_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_do() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}	

	function getnomorfaktur_bhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur_bhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_bhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		$thn = date("Y");
		return $db2->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	function lcabang() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		
		$db2->select(" a.e_initial AS codebranch,
				    a.i_customer AS codecustomer,
				    a.e_branch_name AS branch,
					a.e_initial AS einitial FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
		
		$query	= $db2->get();
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}else{
			return false;
		}
	}
	
	function lbarangjadiperpages($ibranch,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode, 
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.i_code_references,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai,
				a.e_satuan
			
			FROM tm_sj_bhnbaku_item a
			
			INNER JOIN tm_sj_bhnbaku b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND (b.f_faktur_created='t' OR b.f_faktur_created='f') AND c.e_initial='$ibranch' AND a.n_unit_akhir > 0
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.i_code_references,
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 b.i_sj_code,
					 a.e_satuan
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	LIMIT ".$limit." OFFSET ".$offset);
			
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function lbarangjadi($ibranch) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.i_code_references,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai,
				a.e_satuan
			
			FROM tm_sj_bhnbaku_item a
			
			INNER JOIN tm_sj_bhnbaku b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND (b.f_faktur_created='t' OR b.f_faktur_created='f') AND c.e_initial='$ibranch' AND a.n_unit_akhir > 0
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.i_code_references,
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 b.i_sj_code,
					 a.e_satuan
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";
			
		return $db2->query($qstr);
	}

	function flbarangjadi($ibranch,$key) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= $key;
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.i_code_references,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai,
				a.e_satuan
			
			FROM tm_sj_bhnbaku_item a
			
			INNER JOIN tm_sj_bhnbaku b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND (b.f_faktur_created='t' OR b.f_faktur_created='f') AND a.n_unit_akhir > 0 AND (b.i_sj_code='$ky_upper' OR a.i_product='$ky_upper') AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj,
					 b.i_customer,
					 b.i_branch,
					 a.i_product,
					 a.i_code_references,
					 a.e_product_name,
					 a.v_product_price,
					 a.n_unit,
					 a.n_unit_akhir,
					 b.i_sj_code,
					 a.e_satuan
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";

		return $db2->query($qstr);
	}
				
	function msimpan($i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$isjcode,$e_satuan,$f_include_ppn) {
		$db2=$this->load->database('db_external', TRUE);
		$i_faktur_item	= array();
		$tm_faktur_item	= array();
		
		$jml_item_br	= array();
		$qty_akhir	= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	

		$seq_tm_faktur	= $db2->query(" SELECT cast(i_faktur AS integer) AS i_faktur FROM tm_faktur_bhnbaku ORDER BY cast(i_faktur AS integer) DESC LIMIT 1 ");
		
		if($seq_tm_faktur->num_rows() >0 ) {
			$seqrow	= $seq_tm_faktur->row();
			$ifaktur	= $seqrow->i_faktur+1;
		} else {
			$ifaktur	= 1;
		}
		
		/*if($nw_v_total_fppn > 4999999){
		$db2->set(
			array(
			 'i_faktur'=>$ifaktur,
			 'i_faktur_code'=>$i_faktur,
			 'd_faktur'=>$nw_d_faktur,
			 'e_branch_name'=>$i_branch,
			 'd_due_date'=>$nw_d_due_date,
			 'i_faktur_pajak'=>$i_faktur_pajak,
			 'd_pajak'=>$nw_d_pajak,
			 'n_discount'=>$n_discount,
			 'v_discount'=>$nw_v_discount,
			 'v_total_faktur'=>$nw_v_total_faktur,
			 'v_total_fppn'=>$nw_v_total_fppn,
			 'f_include_ppn'=>$f_include_ppn,
			 'e_note_faktur'=>$e_note_faktur,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry,
			  'v_grand_sisa'=>$nw_v_total_fppn,
			  'v_materai'=>'10000',
			  'v_materai_sisa'=>'10000'
			 ));
			}else{*/
			 $db2->set(
				array(
				 'i_faktur'=>$ifaktur,
				 'i_faktur_code'=>$i_faktur,
				 'd_faktur'=>$nw_d_faktur,
				 'e_branch_name'=>$i_branch,
				 'd_due_date'=>$nw_d_due_date,
				 'i_faktur_pajak'=>$i_faktur_pajak,
				 'd_pajak'=>$nw_d_pajak,
				 'n_discount'=>$n_discount,
				 'v_discount'=>$nw_v_discount,
				 'v_total_faktur'=>$nw_v_total_faktur,
				 'v_total_fppn'=>$nw_v_total_fppn,
				 'f_include_ppn'=>$f_include_ppn,
				 'e_note_faktur'=>$e_note_faktur,
				 'f_pelunasan'=>'FALSE',
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry,
				  'v_grand_sisa'=>$nw_v_total_fppn,
				  'v_materai'=>'0',
				  'v_materai_sisa'=>'0'
				 ));
				/*}*/





		
		if($db2->insert('tm_faktur_bhnbaku')) {
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				$seq_tm_i_faktur_item	= $db2->query(" SELECT cast(i_faktur_item AS integer) AS i_faktur_item FROM tm_faktur_bhnbaku_item ORDER BY cast(i_faktur_item AS integer) DESC LIMIT 1 ");
				
				if($seq_tm_i_faktur_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_i_faktur_item->row();
					$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
				} else {
					$i_faktur_item[$jumlah]	= 1;
				}
				
				$tm_faktur_item[$jumlah]	= array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$ifaktur,
					 'i_sj'=>$isjcode[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_hjp[$jumlah],
					 'e_satuan'=>$e_satuan[$jumlah],
					 'd_entry'=>$dentry );
				
				$q_qty_sj_item	= $db2->query(" SELECT * FROM tm_sj_bhnbaku_item WHERE i_sj='$isjcode[$jumlah]' AND i_product='$i_product[$jumlah]' ");
				if($q_qty_sj_item->num_rows()>0) {
					$row_item_br	= $q_qty_sj_item->row();
					$jml_item_br[$jumlah]	= $row_item_br->n_unit_akhir;
					if($n_quantity[$jumlah] == $jml_item_br[$jumlah]) {
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);
					} else if($n_quantity[$jumlah] < $jml_item_br[$jumlah]) { // jika jmlitwm kurang dari qty brg yg ada
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'FALSE'
						);
					}else{
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
						$arrfakturupdate[$jumlah]	= array(
							'f_faktur_created'=>'TRUE'
						);
		
						$arrfakturupdate2[$jumlah]	= array(
							'n_unit_akhir'=>$qty_akhir[$jumlah],
							'f_faktur_created'=>'TRUE'
						);						
					}
				}
				
				$db2->update('tm_sj_bhnbaku',$arrfakturupdate[$jumlah],array('i_sj'=>$isjcode[$jumlah]));
				$db2->update('tm_sj_bhnbaku_item',$arrfakturupdate2[$jumlah],array('i_sj'=>$isjcode[$jumlah],'i_product'=>$i_product[$jumlah]));
				$db2->insert('tm_faktur_bhnbaku_item',$tm_faktur_item[$jumlah]);

				if ($db2->trans_status()===FALSE) {
					$db2->trans_rollback();
				} else {
					$db2->trans_commit();
				}
								
			}
			print "<script>alert(\"Nomor Faktur : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
	}	
}
?>
