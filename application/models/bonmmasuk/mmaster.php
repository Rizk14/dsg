<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $gudang, $cari) {	  
	  $sql = " distinct b.no_bonm, b.tgl_bonm, c.id_gudang FROM tm_apply_stok_pembelian a 
			INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok 
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND a.no_bonm <> b.no_bonm
			AND b.status_stok = 't' AND a.stok_lain = 'f' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  ";
	  if ($gudang != '0')
	    $sql.= " AND c.id_gudang = '$gudang' ";
	  $sql.=" ORDER BY b.tgl_bonm DESC, c.id_gudang ASC ";
	  //$sql.=" ORDER BY b.id ";
	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
			
	/*if ($cari == "all") {
		if ($gudang == '0') {			
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($gudang != '0') {
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' AND UPPER(b.no_bonm) like UPPER('%$cari%') 
			ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' AND UPPER(b.no_bonm) like UPPER('%$cari%') 
			ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	} */
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT b.* FROM tm_apply_stok_pembelian_detail b 
				INNER JOIN tm_apply_stok_pembelian a ON a.id = b.id_apply_stok 
				INNER JOIN tm_barang c ON c.id = b.id_brg
				WHERE b.no_bonm = '$row1->no_bonm' AND b.tgl_bonm='$row1->tgl_bonm'
				AND b.status_stok = 't' AND c.id_gudang = '$row1->id_gudang' ORDER BY b.id ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// 11-07-2015
						$query3	= $this->db->query(" SELECT a.no_sj, a.tgl_sj, c.kode_supplier, c.nama
										FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
										INNER JOIN tm_supplier c ON c.id = a.id_supplier
										WHERE b.id = '$row2->id_pembelian_detail' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$no_sj	= $hasilrow->no_sj;
							$tgl_sj	= $hasilrow->tgl_sj;
							$kode_supplier	= $hasilrow->kode_supplier;
							$nama_supplier	= $hasilrow->nama;
							
							$pisah1 = explode("-", $tgl_sj);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						}
						else {
							$no_sj	= '';
							$tgl_sj	= '';
							$kode_supplier = '';
							$nama_supplier = '';
						}
				
						$detail_fb[] = array('id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'tgl_update'=> $row2->tgl_update,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'kode_supplier'=> $kode_supplier,
												'nama_supplier'=> $nama_supplier
											);
					}//end foreach row2
				}
				else {
					$detail_fb = '';
				}
				
				 // ambil data nama gudang
					$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row1->id_gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
					
					$query41 = $this->db->query("SELECT * FROM tm_periode_bb order by id desc limit 1");
					if($query41->num_rows() > 0){
					$hasilrow41 = $query41->row();
					$tahun_periode = $hasilrow41->i_year;
					$bulan_periode = $hasilrow41->i_month;
					$tanggal_periode = $hasilrow41->i_periode;
					}
					else{
						$tahun_periode = '';
										$bulan_periode = '';
										$tanggal_periode = '';
						}
					
					
				$data_fb[] = array(			'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											//'tgl_update'=> $row1->tgl_update,
											'id_gudang'=> $row1->id_gudang,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'tahun_periode'=> $tahun_periode,
											'bulan_periode'=> $bulan_periode,
											'tanggal_periode'=>$tanggal_periode,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($gudang, $cari){
	  $sql = " SELECT distinct b.no_bonm, b.tgl_bonm, c.id_gudang FROM tm_apply_stok_pembelian a 
			INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok 
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND a.no_bonm <> b.no_bonm
			AND b.status_stok = 't' AND a.stok_lain = 'f' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  ";
	  if ($gudang != '0')
	    $sql.= " AND c.id_gudang = '$gudang' ";
	  $query	= $this->db->query($sql);
	  
	/*if ($cari == "all") {
		if ($gudang == '0')
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' ");
		else
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' ");
	}
	else {
		if ($gudang != '0')
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' AND UPPER(b.no_bonm) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' AND UPPER(b.no_bonm) like UPPER('%$cari%') ");
	} */
    
    return $query->result();  
  }
  
  function get_gudang(){
    if($this->session->userdata('gid') == 16){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
				                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
    }
    else
    {
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
				                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
	}
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  function cek_data($no_bonm, $tgl_bonm, $id_gudang){
    $this->db->select("b.id from tm_apply_stok_pembelian_detail b, tm_apply_stok_pembelian a 
				WHERE a.id = b.id_apply_stok AND b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
				AND a.id_gudang = '$id_gudang' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_sj($num, $offset, $csupplier, $id_gudang, $cari) {
	if ($cari == '') {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail brg-nya
			/*	$sql = "SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id'";
				echo($sql); */
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id' 
				AND a.status_stok = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama FROM tm_barang a, tm_satuan b
								WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama;
							
						$detail_sj[] = array('id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'qty'=> $row2->qty,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_sjtanpalimit($csupplier, $cari){
	if ($cari == '') { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ");
	}
    
    return $query->result();  
  }
  
  function get_detail_bonm($id_gudang, $date_from, $date_to){
    $headernya = array();    
    $detail_brg = array();    
    // =====================================================================================
    /*echo "SELECT a.tgl_bonm, a.no_sj, a.kode_supplier, c.tgl_sj, b.* FROM tm_apply_stok_pembelian a, 
					tm_apply_stok_pembelian_detail b, tm_pembelian c, tm_barang d
					WHERE a.id = b.id_apply_stok AND a.no_sj = c.no_sj AND a.kode_supplier=c.kode_supplier 
					AND d.kode_brg = b.kode_brg 
					AND a.status_stok = 'f' AND a.status_aktif = 't' AND c.status_aktif='t'
					AND b.status_stok = 'f' AND a.stok_lain = 'f'
					AND d.id_gudang = '$id_gudang' 
					AND c.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
					ORDER BY a.kode_supplier, a.id DESC, b.id ASC"; die(); */
	
		// INNER JOIN tm_pembelian c ON (a.no_sj = c.no_sj AND a.id_supplier=c.id_supplier )
		//INNER JOIN tm_pembelian_detail e ON (c.id = e.id_pembelian AND e.id=b.id_pembelian_detail )
    $query	= $this->db->query(" SELECT a.no_sj, a.id_supplier, c.tgl_sj, b.* FROM tm_apply_stok_pembelian a 
					INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
					INNER JOIN tm_pembelian_detail e ON b.id_pembelian_detail = e.id
					INNER JOIN tm_pembelian c ON e.id_pembelian = c.id
					INNER JOIN tm_barang d ON d.id = b.id_brg 
					WHERE a.status_stok = 'f' AND a.status_aktif = 't' AND c.status_aktif='t'
					AND b.status_stok = 'f' AND a.stok_lain = 'f'
					AND d.id_gudang = '$id_gudang' 
					AND c.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
					ORDER BY a.id_supplier, c.tgl_sj, b.id ASC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		$temp_id_pembelian_detail = 0;
		
		foreach($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			
			// 21-09-2015
			$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$row1->id_satuan' ");
			$hasilrow = $query3->row();
			$satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama supplier
			//echo $row1->id_supplier."<br>";
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
								
				//ambil tgl SJ dan id pembelian detail. 19-06-2015: ID_PEMBELIAN_DETAIL UDAH DIPAKE DI TM_APPLY_STOK_PEMBELIAN_DETAIL
				$query3	= $this->db->query(" SELECT DISTINCT a.id, a.tgl_sj FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
						WHERE b.id = '$row1->id_pembelian_detail' AND b.status_stok = 'f' AND a.status_aktif = 't' ");
				 if ($query3->num_rows() > 0) {
					 $hasilrow = $query3->row();
					 $id_pembelian	= $hasilrow->id;
					 $tgl_sj	= $hasilrow->tgl_sj;
				 }
				 else {
					 $id_pembelian = '0';
					 $tgl_sj = '';				
				 }
				
			/*	$query3	= $this->db->query(" SELECT a.id AS id_pembelian, a.tgl_sj, b.id AS id_detail FROM tm_pembelian a, tm_pembelian_detail b 
							WHERE a.id = b.id_pembelian AND a.no_sj = '$row1->no_sj' 
							AND a.kode_supplier = '$row1->kode_supplier' AND b.kode_brg = '$row1->kode_brg' 
							AND b.qty='$row1->qty' AND b.status_stok = 'f' AND a.status_aktif = 't' ORDER BY b.id DESC ");
				 if ($query3->num_rows() > 0) {
					 if ($query3->num_rows() == 1) {
						$hasilrow = $query3->row();
						$tgl_sj	= $hasilrow->tgl_sj;
						$id_pembelian	= $hasilrow->id_pembelian;
					 	$id_pembelian_detail	= $hasilrow->id_detail;
					 }
					 else if ($query3->num_rows() > 1) {
						 /* misalnya ada 3 data kode brg yg sama dan qty yg sama*/
				/*		 $hasil3 = $query3->result();
						 foreach ($hasil3 as $row3) {
							 if ($row3->id_detail != $temp_id_pembelian_detail) {
								$temp_id_pembelian_detail = $row3->id_detail;
								$id_pembelian = $temp_id_pembelian_detail;
								$tgl_sj	= $row3->tgl_sj;
								$id_pembelian_detail = $row3->id_detail;
								break;
						     }
						 } // sampe sini 04:01 selasa
					 }
				 }
				 else {
					$tgl_sj = '';
					$id_pembelian = '0';
					$id_pembelian_detail = '0';
				} */
				
				// 08-07-2015
				$pos = strpos($row1->nama_brg, "\"");
				  if ($pos > 0)
					$row1->nama_brg = str_replace("\"", "&quot;", $row1->nama_brg);
				  else
					$row1->nama_brg = str_replace("'", "\'", $row1->nama_brg);
				
				$headernya[] = array(		
											'id'=> $row1->id,
											'id_apply_stok'=> $row1->id_apply_stok,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'id_pembelian'=> $id_pembelian,
											'id_pembelian_detail'=> $row1->id_pembelian_detail,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $row1->nama_brg,
											'satuan'=> $satuan,
											'id_satuan'=> $row1->id_satuan,
											'id_satuan_konversi'=> $row1->id_satuan_konversi,
											'qty'=> $row1->qty,
											'harga'=> $row1->harga,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm
									);
		}
	}
	else {
			$headernya = '';
	}
    // =====================================================================================
    
	return $headernya;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori='1' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
   // NEW 27-05-2015
   function get_bonm($no_bonmback, $id_gudang, $tgl_bonm){		
	$query	= $this->db->query(" SELECT b.no_bonm, b.tgl_bonm, a.id_gudang, a.no_sj, a.id_supplier,
			b.id_brg, b.qty, b.harga, b.id_apply_stok, b.id, c.kode_brg, c.nama_brg, d.nama as nama_satuan
			FROM tm_apply_stok_pembelian a 
			INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
			INNER JOIN tm_barang c ON b.id_brg = c.id
			INNER JOIN tm_satuan d ON c.satuan = d.id
			WHERE a.status_aktif = 't' AND b.no_bonm ='$no_bonmback' AND c.id_gudang='$id_gudang'
			AND b.tgl_bonm='$tgl_bonm'
			AND b.status_stok = 't' AND a.stok_lain = 'f' ORDER BY b.kode_brg ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	
	foreach ($hasil as $row1) {
		$no_sj = $row1->no_sj;
		$id_supplier = $row1->id_supplier;
		$tgl_bonm = $row1->tgl_bonm;
		$id_brg = $row1->id_brg;
		$qty = $row1->qty;
		$harga = $row1->harga;
		$id_apply_stok = $row1->id_apply_stok;
		$id_apply_stok_detail = $row1->id;
		$kode_brg = $row1->kode_brg;
		$nama_brg = $row1->nama_brg;
		$nama_satuan = $row1->nama_satuan;
				
		// nama gudang
		$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE a.id = '$id_gudang' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) {
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
			$nama_lokasi	= $hasilrow->nama_lokasi;
		}
		
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
				 
		$data_bonm[] = array(		'tgl_bonm'=> $tgl_bonm,	
									'no_bonm'=> $no_bonmback,	
									'id_brg'=> $id_brg,
									'qty'=> $qty,
									'harga'=> $harga,
									'id_apply_stok'=> $id_apply_stok,
									'id_apply_stok_detail'=> $id_apply_stok_detail,
									'kode_brg'=> $kode_brg,
									'nama_brg'=> $nama_brg,
									'nama_satuan'=> $nama_satuan,
									'kode_gudang'=> $kode_gudang,
									'nama_gudang'=> $nama_gudang,
									'nama_lokasi'=> $nama_lokasi,
									'no_sj'=> $no_sj,
									'id_supplier'=> $id_supplier
									);
	}
	return $data_bonm;
  }
  
  function delete($no_bonm, $id_gudang, $tgl_bonm){   
	  $tgl = date("Y-m-d H:i:s");
	   
	  $sql = "SELECT a.no_sj, a.id_supplier, b.* FROM tm_apply_stok_pembelian a INNER JOIN 
					tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok 
					INNER JOIN tm_barang c ON b.id_brg = c.id
					WHERE 
					c.id_gudang = '$id_gudang' AND b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
					AND b.status_stok = 't' AND a.status_aktif = 't' "; //echo $sql; die();
							
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sj;
				$id_supplier = $row4->id_supplier;
				$id_apply_stok = $row4->id_apply_stok;
				$id_detail = $row4->id;
				$id_brg = $row4->id_brg;
				$id_satuan = $row4->id_satuan;
				$qty = $row4->qty;
				$harga = $row4->harga;
				$id_pembelian_detail = $row4->id_pembelian_detail;
				
				//2. ambil tgl sj di tm_pembelian
				//$query3	= $this->db->query(" SELECT id, tgl_sj FROM tm_pembelian WHERE no_sj= '$no_sj' AND id_supplier = '$id_supplier' ");
				$query3	= $this->db->query(" SELECT distinct a.id, a.tgl_sj FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
										WHERE b.id = '$id_pembelian_detail' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_pembelian	= $hasilrow->id;
					$tgl_sj	= $hasilrow->tgl_sj;
				}
				else {
					$id_pembelian = '';
					$tgl_sj = '';
				}
				
				//2. ambil stok terkini di tm_stok
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg='$id_brg' AND id_satuan = '$id_satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty;
				
				// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
				//cek stok terakhir tm_stok_harga, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE id_brg = '$id_brg' 
											AND harga = '$harga' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$stokreset2 = $stok_lama-$qty;
				// 16-02-2012: warning, tt_stok udh ga akan dipake lagi utk rekap mutasi stok
				//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
				/*$this->db->query(" INSERT INTO tt_stok (kode_brg, kode_supplier, no_bukti, keluar, 
									saldo, tgl_input, harga)
									VALUES ('$kode_brg', '$kode_supplier', '$no_bonm', '$qty', '$stokreset2', 
									'$tgl_sj', '$harga' ) "); */
																					
				//4. update stok di tm_stok
				$this->db->query(" UPDATE tm_stok SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where id_brg= '$id_brg' AND id_satuan = '$id_satuan' ");
										
				$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
									where id_brg= '$id_brg' AND harga = '$harga' ");
									
				$this->db->query(" UPDATE tm_pembelian SET status_stok = 'f' where id = '$id_pembelian' ");
				$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 'f' where id = '$id_pembelian_detail' ");
				
				$this->db->query(" UPDATE tm_apply_stok_pembelian SET 
														status_stok = 'f', tgl_update = '$tgl'
														where id= '$id_apply_stok'  ");
														
				$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET 
														no_bonm = '', tgl_bonm = NULL, status_stok = 'f'
														where id= '$id_detail'  ");
		} // end foreach
	  }
  }
  
  // 27-07-2015
  function getAllmanual($num, $offset, $cari, $date_from, $date_to, $cgudang) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_bonm) like UPPER('%$cari%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			// 27-07-2015
			if ($cgudang != "0")
				$pencarian.= " AND id_gudang = '$cgudang' ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			// 27-07-2015
			if ($cgudang != "0")
				$pencarian.= " AND id_gudang = '$cgudang' ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$this->db->select(" * FROM tm_bonmmasukmanual WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasukmanual_detail WHERE id_bonmmasukmanual = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						
							$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
									FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row2->id_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg	= $hasilrow->kode_brg;
								$nama_brg	= $hasilrow->nama_brg;
								$id_satuan	= $hasilrow->id_satuan;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$kode_brg = '';
								$nama_brg = '';
								$id_satuan = 0;
								$satuan = '';
							}
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$id_satuan	= $hasilrow->id_satuan;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg = '';
							$kode_brg = '';
							$id_satuan = 0;
							$satuan = '';
						}
																
						$detail_bonm[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'id_satuan'=> $id_satuan,
												'keterangan'=> $row2->keterangan,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_bonm = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
									$query41 = $this->db->query("SELECT * FROM tm_periode_bb order by id desc limit 1");
					if($query41->num_rows() > 0){
					$hasilrow41 = $query41->row();
					$tahun_periode = $hasilrow41->i_year;
					$bulan_periode = $hasilrow41->i_month;
					$tanggal_periode = $hasilrow41->i_periode;
}
else{
	$tahun_periode = '';
					$bulan_periode = '';
					$tanggal_periode = '';
	}

				$data_bonm[] = array(		'id'=> $row1->id,	
										'keterangan'=> $row1->keterangan,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'tahun_periode'=> $tahun_periode,
											'bulan_periode'=> $bulan_periode,
											'tanggal_periode'=>$tanggal_periode,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function getAllmanualtanpalimit($cari, $date_from, $date_to, $cgudang){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_bonm) like UPPER('%$cari%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			// 27-07-2015
			if ($cgudang != "0")
				$pencarian.= " AND id_gudang = '$cgudang' ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			// 27-07-2015
			if ($cgudang != "0")
				$pencarian.= " AND id_gudang = '$cgudang' ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_bonmmasukmanual WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  function get_bahan($num, $offset, $cari, $id_gudang)
  {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id 
					WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nama_satuan 
			FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
			WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";			
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
				order by a.nama_brg";
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
				$tabelstok = "tm_stok";
				
				// 29-10-2014
				$id_satuan_konversi = $row1->id_satuan_konversi;
				$rumus_konversi = $row1->rumus_konversi;
				$angka_faktor_konversi = $row1->angka_faktor_konversi;

			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE id_brg = '$row1->id' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}
						
			$data_bhn[] = array(		'id_brg'=> $row1->id,	
										'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										// 29-10-2014
										'id_satuan_konversi'=> $id_satuan_konversi,
										'rumus_konversi'=> $rumus_konversi,
										'angka_faktor_konversi'=> $angka_faktor_konversi,
										//------------
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $id_gudang){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
					WHERE a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
							WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";		
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function cek_data_manual($no_bonm, $tgl_bonm, $id_gudang){
    $this->db->select("id from tm_bonmmasukmanual 
				WHERE no_bonm = '$no_bonm' AND tgl_bonm='$tgl_bonm'
				AND id_gudang = '$id_gudang' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savemanual($id_bonm, $id_gudang, $id_brg, $nama, $qty, $satuan,$ket_det){  
    $tgl = date("Y-m-d H:i:s");
			
			if ($id_brg!='' && ($qty!='' || $qty!=0)) {

				// 28-10-2014 pindah kesini. jika semua data tdk kosong, insert ke tm_bonmmasukmanual_detail
				$data_detail = array(
					'id_bonmmasukmanual'=>$id_bonm,
					'id_brg'=>$id_brg,
					'keterangan'=>$ket_det,
					'qty'=>$qty,
				);
				$this->db->insert('tm_bonmmasukmanual_detail',$data_detail);
				
				// GA SIMPAN KE TABEL DETAIL HARGA
				// ======== update stoknya! =============
				$nama_tabel_stok = "tm_stok";
				
				//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE id_brg = '$id_brg' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'id_brg'=>$id_brg,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id_brg= '$id_brg' ");
					}
										
					//cek stok terakhir tm_stok_harga, ambil harga terbaru dan update stoknya
					// 27-07-2015 GA USAH KATA ERIK
						/*	$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE id_brg = '$id_brg' 
										AND quilting = 'f' ORDER BY id DESC LIMIT 1";
															
							$queryxx	= $this->db->query($sqlxx);
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$id_stok_harga	= $hasilxx->id;
								$stokharganya	= $hasilxx->stok;
								$harga	= $hasilxx->harga;
															
								$new_stok = $stokharganya+$qty;
								
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
											where id_brg= '$id_brg' AND harga = '$harga' ");
							}
							else {
								$fieldkode = 'id_brg';
								
								$new_stok = $qty;
								$harga = 0;
								// jika blm ada data di tm_stok_harga, insert
								$data_stok = array(
										$fieldkode=>$id_brg,
										'stok'=>$new_stok,
										'harga'=>$harga,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_harga',$data_stok);
							}	*/
					
					// #########################################################################################
			} 
		
  }
  
  function deletebonmmanual($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasukmanual_detail WHERE id_bonmmasukmanual = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {									
						$qty_sat_awal = $row2->qty;
						
						// ============ update stok =====================
						$nama_tabel_stok = "tm_stok";
				
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE 
										id_brg = '$row2->id_brg' AND id_satuan = '$row2->id_satuan' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama-$qty_sat_awal; // berkurang stok karena reset dari bon M masuk
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'id_brg'=>$row2->id_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where id_brg= '$row2->id_brg' AND id_satuan = '$row2->id_satuan' ");
								}
								// ==============================================================================
						// ==============================================
					} // end foreach
				} // end if
	
	// 2. hapus data bonmmasukmanual
    $this->db->delete('tm_bonmmasukmanual_detail', array('id_bonmmasukmanual' => $id));
    $this->db->delete('tm_bonmmasukmanual', array('id' => $id));
  }
  
  // 09-09-2015
  function getAllbonmmasukbisbisan($num, $offset, $cari) {	  
	  $sql = " distinct b.no_bonm, b.tgl_bonm FROM tm_pembelian_makloon a 
			INNER JOIN tm_pembelian_makloon_detail b ON a.id = b.id_pembelian_makloon
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND b.status_stok = 't' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  ";
	  $sql.=" ORDER BY b.tgl_bonm DESC ";
	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
			
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.no_sj, a.tgl_sj, a.id_supplier, b.* FROM tm_pembelian_makloon_detail b 
				INNER JOIN tm_pembelian_makloon a ON a.id = b.id_pembelian_makloon 
				INNER JOIN tm_barang c ON c.id = b.id_brg
				WHERE b.no_bonm = '$row1->no_bonm' AND b.tgl_bonm='$row1->tgl_bonm'
				AND b.status_stok = 't' ORDER BY b.id ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$pisah1 = explode("-", $row2->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// 11-07-2015
						$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier 
										WHERE id = '$row2->id_supplier' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_supplier	= $hasilrow->kode_supplier;
							$nama_supplier	= $hasilrow->nama;
						}
						else {
							$kode_supplier = '';
							$nama_supplier = '';
						}
						
						if ($row2->jenis_potong == 1)
							$nama_jenis_potong = "Potong Serong";
						elseif ($row2->jenis_potong == 2)
							$nama_jenis_potong = "Potong Lurus";
						elseif ($row2->jenis_potong == 3)
							$nama_jenis_potong = "Potong Spiral";
						
						$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row2->id_ukuran_bisbisan' ");
						$hasilxx = $queryxx->row();
						$nama_ukuran = $hasilxx->nama;
				
						$detail_fb[] = array('id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty_satawal'=> $row2->qty_satawal,
												'tgl_update'=> $row2->tgl_update_bonm,
												'no_sj'=> $row2->no_sj,
												'tgl_sj'=> $row2->tgl_sj,
												'kode_supplier'=> $kode_supplier,
												'nama_supplier'=> $nama_supplier,
												'nama_jenis_potong'=> $nama_jenis_potong,
												'nama_ukuran_bisbisan'=> $nama_ukuran
											);
					}
				}
				else {
					$detail_fb = '';
				}
				 
				$data_fb[] = array(			'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											//'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
		//
  }
  
   function getAllbonmmasukbordir($num, $offset, $cari) {	  
	  $sql = "  distinct b.no_bonm, b.tgl_bonm FROM tm_pembelian_bordir a 
			INNER JOIN tm_pembelian_bordir_detail b ON a.id = b.id_pembelian_bordir
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND b.status_stok = 't' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  order by b.tgl_bonm desc  ";
	 
	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
			
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.no_sjmasukpembelian_bordir, a.tgl_sj, a.id_supplier, b.* FROM tm_pembelian_bordir_detail b 
				INNER JOIN tm_pembelian_bordir a ON a.id = b.id_pembelian_bordir 
				INNER JOIN tm_barang c ON c.id = b.id_brg
				WHERE b.no_bonm = '$row1->no_bonm' AND b.tgl_bonm='$row1->tgl_bonm'
				AND b.status_stok = 't' ORDER BY b.id ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$pisah1 = explode("-", $row2->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// 11-07-2015
						$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier 
										WHERE id = '$row2->id_supplier' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_supplier	= $hasilrow->kode_supplier;
							$nama_supplier	= $hasilrow->nama;
						}
						else {
							$kode_supplier = '';
							$nama_supplier = '';
						}
						
						
				
						$detail_fb[] = array('id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'tgl_update'=> $row2->tgl_update_bonm,
												'no_sj'=> $row2->no_sjmasukpembelian_bordir,
												'tgl_sj'=> $row2->tgl_sj,
												'kode_supplier'=> $kode_supplier,
												'nama_supplier'=> $nama_supplier
												
												
											);
					}
				}
				else {
					$detail_fb = '';
				}
				 
				$data_fb[] = array(			'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											//'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
		//
  }
   function getAllbonmmasukaplikasi($num, $offset, $cari) {	  
	  $sql = "  distinct b.no_bonm, b.tgl_bonm FROM tm_pembelian_aplikasi a 
			INNER JOIN tm_pembelian_aplikasi_detail b ON a.id = b.id_pembelian_aplikasi
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND b.status_stok = 't' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  ";
	 
	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
			
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.no_sjmasukpembelian_aplikasi, a.tgl_sj, a.id_supplier, b.* FROM tm_pembelian_aplikasi_detail b 
				INNER JOIN tm_pembelian_aplikasi a ON a.id = b.id_pembelian_aplikasi 
				INNER JOIN tm_barang c ON c.id = b.id_brg
				WHERE b.no_bonm = '$row1->no_bonm' AND b.tgl_bonm='$row1->tgl_bonm'
				AND b.status_stok = 't' ORDER BY b.id ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$pisah1 = explode("-", $row2->tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// 11-07-2015
						$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier 
										WHERE id = '$row2->id_supplier' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_supplier	= $hasilrow->kode_supplier;
							$nama_supplier	= $hasilrow->nama;
						}
						else {
							$kode_supplier = '';
							$nama_supplier = '';
						}
						
						
				
						$detail_fb[] = array('id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'tgl_update'=> $row2->tgl_update_bonm,
												'no_sj'=> $row2->no_sjmasukpembelian_aplikasi,
												'tgl_sj'=> $row2->tgl_sj,
												'kode_supplier'=> $kode_supplier,
												'nama_supplier'=> $nama_supplier
												
												
											);
					}
				}
				else {
					$detail_fb = '';
				}
				 
				$data_fb[] = array(			'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											//'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
		//
  }
  function getAllbonmmasukbisbisantanpalimit($cari){
	  $sql = " SELECT distinct b.no_bonm, b.tgl_bonm FROM tm_pembelian_makloon a 
			INNER JOIN tm_pembelian_makloon_detail b ON a.id = b.id_pembelian_makloon
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND b.status_stok = 't' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  ";
	  $query	= $this->db->query($sql);
    
    return $query->result();  
  }
  
    function getAllbonmmasukbordirtanpalimit($cari){
	  $sql = " SELECT distinct b.no_bonm, b.tgl_bonm FROM tm_pembelian_bordir a 
			INNER JOIN tm_pembelian_bordir_detail b ON a.id = b.id_pembelian_bordir
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND b.status_stok = 't' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  ";
	  $query	= $this->db->query($sql);
    
    return $query->result();  
  }
   function getAllbonmmasukaplikasitanpalimit($cari){
	  $sql = " SELECT distinct b.no_bonm, b.tgl_bonm FROM tm_pembelian_aplikasi a 
			INNER JOIN tm_pembelian_aplikasi_detail b ON a.id = b.id_pembelian_aplikasi
			INNER JOIN tm_barang c ON b.id_brg = c.id
			WHERE a.status_aktif = 't' AND b.status_stok = 't' ";
	  if ($cari!="all")
		$sql.= " AND UPPER(b.no_bonm) like UPPER('%$cari%')  ";
	  $query	= $this->db->query($sql);
    
    return $query->result();  
  }
  function get_detail_sjbisbisan($date_from, $date_to){
    $headernya = array();    
    $detail_brg = array();    
    // =====================================================================================	
    $query	= $this->db->query(" SELECT a.no_sj, a.id_supplier, a.tgl_sj, b.* FROM tm_pembelian_makloon a 
					INNER JOIN tm_pembelian_makloon_detail b ON a.id = b.id_pembelian_makloon
					WHERE a.status_stok = 'f' AND a.status_aktif = 't'
					AND b.status_stok = 'f'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
					ORDER BY a.id_supplier, a.tgl_sj, b.id ASC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		$temp_id_pembelian_detail = 0;
		
		foreach($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
								WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			$satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
																
				// 08-07-2015
				$pos = strpos($nama_brg, "\"");
				  if ($pos > 0)
					$nama_brg = str_replace("\"", "&quot;", $nama_brg);
				  else
					$nama_brg = str_replace("'", "\'", $nama_brg);
				
				if ($row1->jenis_potong == 1)
					$nama_jenis_potong = "Potong Serong";
				elseif ($row1->jenis_potong == 2)
					$nama_jenis_potong = "Potong Lurus";
				elseif ($row1->jenis_potong == 3)
					$nama_jenis_potong = "Potong Spiral";
				
				$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row1->id_ukuran_bisbisan' ");
				$hasilxx = $queryxx->row();
				$nama_ukuran = $hasilxx->nama;
				
				$headernya[] = array(		
											'id_pembelian_makloon_detail'=> $row1->id,
											'id_pembelian_makloon'=> $row1->id_pembelian_makloon,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,											
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty_satawal'=> $row1->qty_satawal,
											'harga'=> $row1->harga,
											'jenis_potong'=> $row1->jenis_potong,
											'nama_jenis_potong'=> $nama_jenis_potong,
											'id_ukuran_bisbisan'=> $row1->id_ukuran_bisbisan,
											'nama_ukuran_bisbisan'=> $nama_ukuran,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm
									);
		}
	}
	else {
			$headernya = '';
	}
    // =====================================================================================
    
	return $headernya;
  }
  
  function get_detail_sjbordir($date_from, $date_to){
    $headernya = array();    
    $detail_brg = array();    
    // =====================================================================================	
    $query	= $this->db->query(" SELECT a.no_sjmasukpembelian_bordir, a.id_supplier, a.tgl_sj, b.* FROM tm_pembelian_bordir a 
					INNER JOIN tm_pembelian_bordir_detail b ON a.id = b.id_pembelian_bordir
					WHERE a.status_stok = 'f' AND a.status_aktif = 't'
					AND b.status_stok = 'f'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
					ORDER BY a.id_supplier, a.tgl_sj, b.id ASC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		$temp_id_pembelian_detail = 0;
		
		foreach($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
								WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			$satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
																
				
				
				$headernya[] = array(		
											'id_pembelian_bordir_detail'=> $row1->id,
											'id_pembelian_bordir'=> $row1->id_pembelian_bordir,
											'no_sj'=> $row1->no_sjmasukpembelian_bordir,
											'tgl_sj'=> $row1->tgl_sj,											
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=>$row1->qty,
											'harga'=> $row1->harga,
											
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm
									);
		}
	}
	else {
			$headernya = '';
	}
    // =====================================================================================
    
	return $headernya;
  }
   function get_detail_sjaplikasi($date_from, $date_to){
    $headernya = array();    
    $detail_brg = array();    
    // =====================================================================================	
    $query	= $this->db->query(" SELECT a.no_sjmasukpembelian_aplikasi, a.id_supplier, a.tgl_sj, b.* FROM tm_pembelian_aplikasi a 
					INNER JOIN tm_pembelian_aplikasi_detail b ON a.id = b.id_pembelian_aplikasi
					WHERE a.status_stok = 'f' AND a.status_aktif = 't'
					AND b.status_stok = 'f'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
					ORDER BY a.id_supplier, a.tgl_sj, b.id ASC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		$temp_id_pembelian_detail = 0;
		
		foreach($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
								WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			$satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
																
				
				
				$headernya[] = array(		
											'id_pembelian_aplikasi_detail'=> $row1->id,
											'id_pembelian_aplikasi'=> $row1->id_pembelian_aplikasi,
											'no_sj'=> $row1->no_sjmasukpembelian_aplikasi,
											'tgl_sj'=> $row1->tgl_sj,											
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=>$row1->qty,
											'harga'=> $row1->harga,
											
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm
									);
		}
	}
	else {
			$headernya = '';
	}
    // =====================================================================================
    
	return $headernya;
  }
  function cek_data_bisbisan($no_bonm, $tgl_bonm){
    $this->db->select("b.id from tm_pembelian_makloon_detail b, tm_pembelian_makloon a 
				WHERE a.id = b.id_pembelian_makloon AND b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
				AND a.status_aktif='t' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
    function cek_data_bordir($no_bonm, $tgl_bonm){
    $this->db->select("b.id from tm_pembelian_bordir_detail b, tm_pembelian_bordir a 
				WHERE a.id = b.id_pembelian_bordir AND b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
				AND a.status_aktif='t' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
   function cek_data_aplikasi($no_bonm, $tgl_bonm){
    $this->db->select("b.id from tm_pembelian_aplikasi_detail b, tm_pembelian_aplikasi a 
				WHERE a.id = b.id_pembelian_aplikasi AND b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
				AND a.status_aktif='t' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  // 10-09-2015
  function deletebisbisan($no_bonm, $tgl_bonm){   
	  $tgl = date("Y-m-d H:i:s");
	   
	  $sql = "SELECT a.no_sj, a.id_supplier, b.* FROM tm_pembelian_makloon a INNER JOIN 
					tm_pembelian_makloon_detail b ON a.id = b.id_pembelian_makloon 
					INNER JOIN tm_barang c ON b.id_brg = c.id
					WHERE b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
					AND b.status_stok = 't' AND a.status_aktif = 't' "; //echo $sql; die();
							
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sj;
				$id_supplier = $row4->id_supplier;
				$id_pembelian_makloon = $row4->id_pembelian_makloon;
				$id_detail = $row4->id;
				$id_brg = $row4->id_brg;
				$qty_satawal = $row4->qty_satawal;
				$harga = $row4->harga;
				$jenis_potong = $row4->jenis_potong;
				$id_ukuran_bisbisan = $row4->id_ukuran_bisbisan;
								
				//ambil stok terkini di tm_stok_bisbisan
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_bisbisan WHERE id_brg='$id_brg' 
								AND jenis_potong='$jenis_potong' AND id_ukuran_bisbisan='$id_ukuran_bisbisan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty_satawal;
																									
				// update stok di tm_stok
				$this->db->query(" UPDATE tm_stok_bisbisan SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where id_brg= '$id_brg' AND jenis_potong='$jenis_potong' AND id_ukuran_bisbisan='$id_ukuran_bisbisan' ");
																			
				$this->db->query(" UPDATE tm_pembelian_makloon SET status_stok = 'f' where id = '$id_pembelian_makloon' ");
				$this->db->query(" UPDATE tm_pembelian_makloon_detail SET status_stok = 'f', no_bonm='', tgl_bonm=NULL, 
									tgl_update_bonm = NULL where id = '$id_detail' ");
				
		} // end foreach
	  }
  }
  
  function deletebordir($no_bonm, $tgl_bonm){   
	  $tgl = date("Y-m-d H:i:s");
	   
	  $sql = "SELECT a.no_sjmasukpembelian_bordir, a.id_supplier, b.* FROM tm_pembelian_bordir a INNER JOIN 
					tm_pembelian_bordir_detail b ON a.id = b.id_pembelian_bordir
					INNER JOIN tm_barang c ON b.id_brg = c.id
					WHERE b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
					AND b.status_stok = 't' AND a.status_aktif = 't' "; //echo $sql; die();
							
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sjmasukpembelian_bordir;
				$id_supplier = $row4->id_supplier;
				$id_pembelian_bordir = $row4->id_pembelian_bordir;
				$id_detail = $row4->id;
				$id_brg = $row4->id_brg;
				$qty = $row4->qty;
				$harga = $row4->harga;
			
				//ambil stok terkini di tm_stok_bisbisan
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_bordir WHERE id_brg='$id_brg' 
						 ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty;
																									
				// update stok di tm_stok
				$this->db->query(" UPDATE tm_stok_bordir SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where id_brg= '$id_brg' ");
																			
				$this->db->query(" UPDATE tm_pembelian_bordir SET status_stok = 'f' where id = '$id_pembelian_bordir' ");
				$this->db->query(" UPDATE tm_pembelian_bordir_detail SET status_stok = 'f', no_bonm='', tgl_bonm=NULL, 
									tgl_update_bonm = NULL where id = '$id_detail' ");
				
		} // end foreach
	  }
  }
  
   function deleteaplikasi($no_bonm, $tgl_bonm){   
	  $tgl = date("Y-m-d H:i:s");
	   
	  $sql = "SELECT a.no_sjmasukpembelian_aplikasi, a.id_supplier, b.* FROM tm_pembelian_aplikasi a INNER JOIN 
					tm_pembelian_aplikasi_detail b ON a.id = b.id_pembelian_aplikasi
					INNER JOIN tm_barang c ON b.id_brg = c.id
					WHERE b.no_bonm = '$no_bonm' AND b.tgl_bonm='$tgl_bonm'
					AND b.status_stok = 't' AND a.status_aktif = 't' "; //echo $sql; die();
							
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sjmasukpembelian_aplikasi;
				$id_supplier = $row4->id_supplier;
				$id_pembelian_aplikasi = $row4->id_pembelian_aplikasi;
				$id_detail = $row4->id;
				$id_brg = $row4->id_brg;
				$qty = $row4->qty;
				$harga = $row4->harga;
			
				//ambil stok terkini di tm_stok_bisbisan
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_aplikasi WHERE id_brg='$id_brg' 
						 ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty;
																									
				// update stok di tm_stok
				$this->db->query(" UPDATE tm_stok_aplikasi SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where id_brg= '$id_brg' ");
																			
				$this->db->query(" UPDATE tm_pembelian_aplikasi SET status_stok = 'f' where id = '$id_pembelian_aplikasi' ");
				$this->db->query(" UPDATE tm_pembelian_aplikasi_detail SET status_stok = 'f', no_bonm='', tgl_bonm=NULL, 
									tgl_update_bonm = NULL where id = '$id_detail' ");
				
		} // end foreach
	  }
  }
  
  function get_bonm_bisbisan($no_bonmback, $tgl_bonm){		
	$query	= $this->db->query(" SELECT b.no_bonm, b.tgl_bonm, a.no_sj, a.id_supplier,
			b.id_brg, b.qty_satawal, b.harga, b.id_pembelian_makloon, b.id, c.kode_brg, c.nama_brg, d.nama as nama_satuan,
			b.jenis_potong, b.id_ukuran_bisbisan
			FROM tm_pembelian_makloon a 
			INNER JOIN tm_pembelian_makloon_detail b ON a.id = b.id_pembelian_makloon
			INNER JOIN tm_barang c ON b.id_brg = c.id
			INNER JOIN tm_satuan d ON c.satuan = d.id
			WHERE a.status_aktif = 't' AND b.no_bonm ='$no_bonmback'
			AND b.tgl_bonm='$tgl_bonm'
			AND b.status_stok = 't' ORDER BY b.id ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	
	foreach ($hasil as $row1) {
		$no_sj = $row1->no_sj;
		$id_supplier = $row1->id_supplier;
		$tgl_bonm = $row1->tgl_bonm;
		$id_brg = $row1->id_brg;
		$qty_satawal = $row1->qty_satawal;
		$harga = $row1->harga;
		$id_pembelian_makloon = $row1->id_pembelian_makloon;
		$id_pembelian_makloon_detail = $row1->id;
		$kode_brg = $row1->kode_brg;
		$nama_brg = $row1->nama_brg;
		$nama_satuan = $row1->nama_satuan;
		$jenis_potong = $row1->jenis_potong;
		$id_ukuran_bisbisan = $row1->id_ukuran_bisbisan;
						
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
		
		if ($row1->jenis_potong == 1)
					$nama_jenis_potong = "Potong Serong";
		elseif ($row1->jenis_potong == 2)
			$nama_jenis_potong = "Potong Lurus";
		elseif ($row1->jenis_potong == 3)
			$nama_jenis_potong = "Potong Spiral";
				
		$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row1->id_ukuran_bisbisan' ");
		$hasilxx = $queryxx->row();
		$nama_ukuran = $hasilxx->nama;
				 
		$data_bonm[] = array(		'tgl_bonm'=> $tgl_bonm,	
									'no_bonm'=> $no_bonmback,	
									'id_brg'=> $id_brg,
									'qty_satawal'=> $qty_satawal,
									'harga'=> $harga,
									'id_pembelian_makloon'=> $id_pembelian_makloon,
									'id_pembelian_makloon_detail'=> $id_pembelian_makloon_detail,
									'kode_brg'=> $kode_brg,
									'nama_brg'=> $nama_brg,
									'nama_satuan'=> $nama_satuan,
									'jenis_potong'=> $jenis_potong,
									'nama_jenis_potong'=> $nama_jenis_potong,
									'id_ukuran_bisbisan'=> $id_ukuran_bisbisan,
									'nama_ukuran_bisbisan'=> $nama_ukuran,
									'no_sj'=> $no_sj,
									'id_supplier'=> $id_supplier
									);
	}
	return $data_bonm;
  }
function get_bonm_bordir($no_bonmback, $tgl_bonm){		
	$query	= $this->db->query(" SELECT b.no_bonm, b.tgl_bonm, a.no_sjmasukpembelian_bordir, a.id_supplier,
			b.id_brg, b.qty, b.harga, b.id_pembelian_bordir, b.id, c.kode_brg, c.nama_brg, d.nama as nama_satuan
			FROM tm_pembelian_bordir a 
			INNER JOIN tm_pembelian_bordir_detail b ON a.id = b.id_pembelian_bordir
			INNER JOIN tm_barang c ON b.id_brg = c.id
			INNER JOIN tm_satuan d ON c.satuan = d.id
			WHERE a.status_aktif = 't' AND b.no_bonm ='$no_bonmback'
			AND b.tgl_bonm='$tgl_bonm'
			AND b.status_stok = 't' ORDER BY b.id ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	
	foreach ($hasil as $row1) {
		$no_sj = $row1->no_sjmasukpembelian_bordir;
		$id_supplier = $row1->id_supplier;
		$tgl_bonm = $row1->tgl_bonm;
		$id_brg = $row1->id_brg;
		$qty = $row1->qty;
		$harga = $row1->harga;
		$id_pembelian_bordir = $row1->id_pembelian_bordir;
		$id_pembelian_bordir_detail = $row1->id;
		$kode_brg = $row1->kode_brg;
		$nama_brg = $row1->nama_brg;
		$nama_satuan = $row1->nama_satuan;
		
						
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
		
		
				
	
				 
		$data_bonm[] = array(		'tgl_bonm'=> $tgl_bonm,	
									'no_bonm'=> $no_bonmback,	
									'id_brg'=> $id_brg,
									'qty'=> $qty,
									'harga'=> $harga,
									'id_pembelian_bordir'=> $id_pembelian_bordir,
									'id_pembelian_bordir_detail'=> $id_pembelian_bordir_detail,
									'kode_brg'=> $kode_brg,
									'nama_brg'=> $nama_brg,
									'nama_satuan'=> $nama_satuan,
									
									'no_sj'=> $no_sj,
									'id_supplier'=> $id_supplier
									);
	}
	return $data_bonm;
  }
  
  function get_bonm_aplikasi($no_bonmback, $tgl_bonm){		
	$query	= $this->db->query(" SELECT b.no_bonm, b.tgl_bonm, a.no_sjmasukpembelian_aplikasi, a.id_supplier,
			b.id_brg, b.qty, b.harga, b.id_pembelian_aplikasi, b.id, c.kode_brg, c.nama_brg, d.nama as nama_satuan
			FROM tm_pembelian_aplikasi a 
			INNER JOIN tm_pembelian_aplikasi_detail b ON a.id = b.id_pembelian_aplikasi
			INNER JOIN tm_barang c ON b.id_brg = c.id
			INNER JOIN tm_satuan d ON c.satuan = d.id
			WHERE a.status_aktif = 't' AND b.no_bonm ='$no_bonmback'
			AND b.tgl_bonm='$tgl_bonm'
			AND b.status_stok = 't' ORDER BY b.id ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	
	foreach ($hasil as $row1) {
		$no_sj = $row1->no_sjmasukpembelian_aplikasi;
		$id_supplier = $row1->id_supplier;
		$tgl_bonm = $row1->tgl_bonm;
		$id_brg = $row1->id_brg;
		$qty = $row1->qty;
		$harga = $row1->harga;
		$id_pembelian_aplikasi = $row1->id_pembelian_aplikasi;
		$id_pembelian_aplikasi_detail = $row1->id;
		$kode_brg = $row1->kode_brg;
		$nama_brg = $row1->nama_brg;
		$nama_satuan = $row1->nama_satuan;
		
						
		$pisah1 = explode("-", $tgl_bonm);
		$tgl1= $pisah1[2];
		$bln1= $pisah1[1];
		$thn1= $pisah1[0];
		$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
		
		
				
	
				 
		$data_bonm[] = array(		'tgl_bonm'=> $tgl_bonm,	
									'no_bonm'=> $no_bonmback,	
									'id_brg'=> $id_brg,
									'qty'=> $qty,
									'harga'=> $harga,
									'id_pembelian_aplikasi'=> $id_pembelian_aplikasi,
									'id_pembelian_aplikasi_detail'=> $id_pembelian_aplikasi_detail,
									'kode_brg'=> $kode_brg,
									'nama_brg'=> $nama_brg,
									'nama_satuan'=> $nama_satuan,
									
									'no_sj'=> $no_sj,
									'id_supplier'=> $id_supplier
									);
	}
	return $data_bonm;
  }
    function get_bonm_manual($id_bonm){
	$query	= $this->db->query(" SELECT * FROM tm_bonmmasukmanual where id = '$id_bonm' ");
	
	$data_bonm = array();
	$detail_bonm = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
			   //ambil data detailnya
			   $query2 = $this->db->query(" SELECT * FROM tm_bonmmasukmanual_detail WHERE id_bonmmasukmanual = '$row1->id' ");
			   if ($query2->num_rows() > 0){
				   $hasil2=$query2->result();
				   foreach ($hasil2 as $row2) {
					   //ambil detail barangnya
					   $query3 = $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() > 0){
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$id_satuan	= $hasilrow->id_satuan;
						$nama_satuan	= $hasilrow->nama_satuan;
						}
					
					$detail_bonm[] = array( 'id'=> $row2->id,
											'id_brg'=> $row2->id_brg,
											'id_bonmmasukmanual'=> $row2->id_bonmmasukmanual,
											'qty'=>$row2->qty,
											'keterangan1'=>$row2->keterangan,
											'nama_brg'=>$nama_brg,
											'kode_brg'=>$kode_brg,
											'id_satuan'=>$id_satuan,
											'nama_satuan'=>$nama_satuan
										   );
					}
				}else{
					$detail_bonm = '';
				}
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$data_bonm[] = array(	'id'=>$row1->id,
										'no_bonm'=>$row1->no_bonm,
										'tgl_bonm'=>$row1->tgl_bonm,
										'id_gudang'=>$row1->id_gudang,
										'detail_bonm'=>$detail_bonm,
										'keterangan'=>$row1->keterangan,
										'tgl_bonm'=> $tgl,
									);
									$detail_bonm = array();
			}
	}
	return $data_bonm;
}
}
