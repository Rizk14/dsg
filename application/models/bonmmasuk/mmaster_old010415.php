<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $gudang, $cari) {	  
	if ($cari == "all") {
		if ($gudang == '0') {			
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($gudang != '0') {
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' AND UPPER(b.no_bonm) like UPPER('%$cari%') 
			ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31'
			AND b.status_stok = 't' AND a.stok_lain = 'f' AND UPPER(b.no_bonm) like UPPER('%$cari%') 
			ORDER BY b.tgl_bonm DESC, a.id_gudang ASC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT b.* FROM tm_apply_stok_pembelian_detail b, tm_apply_stok_pembelian a 
				WHERE a.id = b.id_apply_stok AND b.no_bonm = '$row1->no_bonm' 
				AND b.status_stok = 't' AND a.id_gudang = '$row1->id_gudang' ORDER BY b.kode_brg ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_fb = '';
				}
				
				 // ambil data nama gudang
					$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				 
				$data_fb[] = array(			'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'id_gudang'=> $row1->id_gudang,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($gudang, $cari){
	if ($cari == "all") {
		if ($gudang == '0')
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' ");
		else
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' ");
	}
	else {
		if ($gudang != '0')
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' AND a.id_gudang = '$gudang' AND UPPER(b.no_bonm) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND b.tgl_bonm >'2012-12-31' AND
			b.status_stok = 't' AND a.stok_lain = 'f' AND UPPER(b.no_bonm) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  function cek_data($no_bonm, $id_gudang){
    $this->db->select("b.id from tm_apply_stok_pembelian_detail b, tm_apply_stok_pembelian a 
				WHERE a.id = b.id_apply_stok AND b.no_bonm = '$no_bonm' AND a.id_gudang = '$id_gudang' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_sj($num, $offset, $csupplier, $id_gudang, $cari) {
	if ($cari == '') {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail brg-nya
			/*	$sql = "SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id'";
				echo($sql); */
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id' 
				AND a.status_stok = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama FROM tm_barang a, tm_satuan b
								WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama;
							
						$detail_sj[] = array('id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'qty'=> $row2->qty,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_sjtanpalimit($csupplier, $cari){
	if ($cari == '') { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ");
	}
    
    return $query->result();  
  }
  
  function get_detail_bonm($id_gudang){
    $headernya = array();    
    $detail_brg = array();    
    // =====================================================================================
    // NEW 15 AGUSTUS 2011
    /*$sql = "SELECT a.no_sj, a.kode_supplier, b.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
					WHERE a.id = b.id_apply_stok AND a.status_stok = 'f' AND a.status_aktif = 't'
					AND b.status_stok = 'f' AND a.stok_lain = 'f'
					AND a.id_gudang = '$id_gudang' ORDER BY a.kode_supplier, b.id DESC "; echo $sql; die(); */
    $query	= $this->db->query(" SELECT a.no_sj, a.kode_supplier, b.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
					WHERE a.id = b.id_apply_stok AND a.status_stok = 'f' AND a.status_aktif = 't'
					AND b.status_stok = 'f' AND a.stok_lain = 'f'
					AND a.id_gudang = '$id_gudang' ORDER BY a.kode_supplier, a.id DESC, b.id ASC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		$temp_id_pembelian_detail = 0;
		
		foreach($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
								WHERE a.satuan = b.id AND
								a.kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			$nama_brg	= $hasilrow->nama_brg;
			$satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
								
				//ambil tgl SJ dan id pembelian detail
				/*$sql = " SELECT a.id AS id_pembelian, a.tgl_sj, b.id AS id_detail FROM tm_pembelian a, tm_pembelian_detail b 
							WHERE a.id = b.id_pembelian AND a.no_sj = '$row1->no_sj' 
							AND a.kode_supplier = '$row1->kode_supplier' AND b.kode_brg = '$row1->kode_brg' 
							AND b.qty='$row1->qty' AND b.status_stok = 'f' AND a.status_aktif = 't' ORDER BY b.id DESC "; echo $sql."<br>"; */
							
				$query3	= $this->db->query(" SELECT a.id AS id_pembelian, a.tgl_sj, b.id AS id_detail FROM tm_pembelian a, tm_pembelian_detail b 
							WHERE a.id = b.id_pembelian AND a.no_sj = '$row1->no_sj' 
							AND a.kode_supplier = '$row1->kode_supplier' AND b.kode_brg = '$row1->kode_brg' 
							AND b.qty='$row1->qty' AND b.status_stok = 'f' AND a.status_aktif = 't' ORDER BY b.id DESC ");
				 if ($query3->num_rows() > 0) {
					 if ($query3->num_rows() == 1) {
						$hasilrow = $query3->row();
						$tgl_sj	= $hasilrow->tgl_sj;
						$id_pembelian	= $hasilrow->id_pembelian;
					 	$id_pembelian_detail	= $hasilrow->id_detail;
					 }
					 else if ($query3->num_rows() > 1) {
						 /* misalnya ada 3 data kode brg yg sama dan qty yg sama*/
						 $hasil3 = $query3->result();
						 foreach ($hasil3 as $row3) {
							 if ($row3->id_detail != $temp_id_pembelian_detail) {
								$temp_id_pembelian_detail = $row3->id_detail;
								$id_pembelian = $temp_id_pembelian_detail;
								$tgl_sj	= $row3->tgl_sj;
								$id_pembelian_detail = $row3->id_detail;
								break;
						     }
						 } // sampe sini 04:01 selasa
					 }
				 }
				 else {
					$tgl_sj = '';
					$id_pembelian = '0';
					$id_pembelian_detail = '0';
				}
				
				$headernya[] = array(		
											'id'=> $row1->id,
											'id_apply_stok'=> $row1->id_apply_stok,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'id_pembelian'=> $id_pembelian,
											'id_pembelian_detail'=> $id_pembelian_detail,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row1->qty,
											'harga'=> $row1->harga,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm
									);
		}
	}
	else {
			$headernya = '';
	}
    // =====================================================================================
    
	return $headernya;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori='1' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
  function get_bonm($id_apply_stok){
	$query	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian WHERE id = '$id_apply_stok' ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	$detail_bonm = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b, tm_pembelian c, 
				tm_apply_stok_pembelian d 
				WHERE a.kode_brg = b.kode_brg AND a.id_pembelian = c.id AND d.no_sj = c.no_sj 
				AND d.kode_supplier = c.kode_supplier AND b.id_gudang = '$row1->id_gudang' 
				AND c.no_sj = '$row1->no_sj' 
				AND d.kode_supplier = '$row1->kode_supplier' ");  
		
				//$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id'
								AND kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$cek = 'y';
						}
						else
							$cek = 't';
						
						$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE 
									kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE 
									kode_brg = '$row2->kode_brg' AND kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$harga	= $hasilrow->harga;
				
						$detail_bonm[] = array(		
										'id'=> $row2->id,
										'kode_brg'=> $row2->kode_brg,
										'nama'=> $nama_brg,
										'harga'=> $harga,
										'qty'=> $row2->qty,
										'cek'=> $cek
										);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$query2	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE 
									kode_supplier = '$row1->kode_supplier' AND no_sj = '$row1->no_sj' ");
						$hasilrow = $query2->row();
						$tgl_sj	= $hasilrow->tgl_sj;
				
				$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				}
				 
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,	
											'tgl_sj'=> $tgl_sj,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
	}
	return $data_bonm;
  }
  
  function delete($no_bonm, $id_gudang){   
	  $tgl = date("Y-m-d");
	   
	  $sql = "SELECT a.no_sj, a.kode_supplier, b.* FROM tm_apply_stok_pembelian a, 
					tm_apply_stok_pembelian_detail b WHERE a.id = b.id_apply_stok
					AND a.id_gudang = '$id_gudang' AND b.no_bonm = '$no_bonm'
					AND b.status_stok = 't' AND a.status_aktif = 't' "; //echo $sql; die();
							
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sj;
				$kode_supplier = $row4->kode_supplier;
				$id_apply_stok = $row4->id_apply_stok;
				$id_detail = $row4->id;
				$kode_brg = $row4->kode_brg;
				$qty = $row4->qty;
				$harga = $row4->harga;
				
				//2. ambil tgl sj di tm_pembelian
				$query3	= $this->db->query(" SELECT id, tgl_sj FROM tm_pembelian WHERE no_sj= '$no_sj' AND kode_supplier = '$kode_supplier' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_pembelian	= $hasilrow->id;
					$tgl_sj	= $hasilrow->tgl_sj;
				}
				else {
					$id_pembelian = '';
					$tgl_sj = '';
				}
				
				//2. ambil stok terkini di tm_stok
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg='$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty;
				
				// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
				//cek stok terakhir tm_stok_harga, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode_brg' 
											AND harga = '$harga' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$stokreset2 = $stok_lama-$qty;
				// 16-02-2012: warning, tt_stok udh ga akan dipake lagi utk rekap mutasi stok
				//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
				/*$this->db->query(" INSERT INTO tt_stok (kode_brg, kode_supplier, no_bukti, keluar, 
									saldo, tgl_input, harga)
									VALUES ('$kode_brg', '$kode_supplier', '$no_bonm', '$qty', '$stokreset2', 
									'$tgl_sj', '$harga' ) "); */
																					
				//4. update stok di tm_stok
				$this->db->query(" UPDATE tm_stok SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where kode_brg= '$kode_brg' ");
										
				$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
									where kode_brg= '$kode_brg' AND harga = '$harga' ");
									
				$this->db->query(" UPDATE tm_pembelian SET status_stok = 'f' where id = '$id_pembelian' ");
				
				$query3	= $this->db->query(" SELECT b.id FROM tm_pembelian_detail b, tm_pembelian a 
								WHERE a.id = b.id_pembelian AND a.no_sj= '$no_sj' AND a.kode_supplier = '$kode_supplier' 
								AND b.kode_brg = '$kode_brg' ORDER BY b.id DESC ");
				if ($query3->num_rows() > 0){
					$hasil3 = $query3->result();
					foreach ($hasil3 as $row3) {
						//$hasilrow = $query3->row();
						$id_pembelian_detail	= $row3->id;
						$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 'f' where id = '$id_pembelian_detail' ");
					}
				}
				else {
					$id_pembelian_detail = '';
				}
									
				$this->db->query(" UPDATE tm_apply_stok_pembelian SET 
														status_stok = 'f', tgl_update = '$tgl'
														where id= '$id_apply_stok'  ");
														
				$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET 
														no_bonm = '', tgl_bonm = NULL, status_stok = 'f'
														where id= '$id_detail'  ");
		}
	  }
										
		
  }

}
