<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function getqtyperproduk($d_first,$d_last) {
		$db2=$this->load->database('db_external', TRUE);
		//=============================
/*
		$sql=" select UPPER(b.i_product) AS imotif, b.e_product_name AS productmotif, sum(b.n_deliver) as qty
			FROM tm_dofc a, tm_dofc_item b  
			WHERE a.i_do = b.i_do AND a.f_do_cancel='f' ";
		if ($d_first != '' && $d_last!='')
			$sql.= " AND (a.d_do >='$d_first' AND a.d_do <='$d_last') ";
		
		$sql.= " GROUP BY b.i_product, b.e_product_name ORDER BY b.i_product ASC ";
*/
		$periode=substr($d_first,0,4).substr($d_first,5,2);
/*
		$sql="select UPPER(c.i_product) AS imotif, c.e_product_name AS productmotif, qty as saldo_awal, sum(b.n_deliver) as qty, 
	        (qty-sum(b.n_deliver)) as sisa
          FROM tm_saldo_awalfc c
          left join tm_dofc_item b on(c.i_product=b.i_product)
          left join tm_dofc a on (a.i_do = b.i_do AND a.f_do_cancel='f' AND (a.d_do >='$d_first' AND a.d_do <='$d_last'))";
#		if ($d_first != '' && $d_last!='')
#			$sql.= " AND (a.d_do >='$d_first' AND a.d_do <='$d_last') ";
		
		$sql.= "WHERE c.i_periode='$periode'
            GROUP BY c.i_product, c.e_product_name, c.qty
            ORDER BY c.i_product ASC ";
*/

######Sikasep 2017-08-04
$sql=" SELECT UPPER(c.i_product) AS imotif, c.e_product_name AS productmotif, c.qty as saldo_awal, sum(e.qty) as qty, (c.qty-sum(e.qty)) as sisa, d.e_color_name, d.i_color
FROM tm_saldo_awalfc c
left join tm_dofc a on (a.f_do_cancel='f' AND (a.d_do >='$d_first' AND a.d_do <='$d_last'))
left join tm_dofc_item b on(c.i_product=b.i_product and a.i_do = b.i_do)
left join tm_dofc_item_color e on(e.i_do_item=b.i_do_item and c.i_color=e.i_color)
left join tr_color d on (d.i_color=c.i_color) 
WHERE c.i_periode='$periode'
GROUP BY c.i_product, c.e_product_name, c.qty, d.e_color_name, d.i_color
ORDER BY c.i_product, d.i_color ASC";
######

		//=============================
				
		$query	= $db2->query($sql);
		$data_brg = array();
		
		if($query->num_rows()>0) {
			$hasil = $query->result();
			foreach ($hasil as $row) {
				
				//--------------06-09-2014 ----------------------------
				// ambil data qty warna dari tr_product_color dan tm_dofc_item_color
/*
				$sqlxx	= $db2->query(" SELECT b.e_color_name, sum(c.qty) as jum FROM tr_color b, 
										tm_dofc_item_color c, tm_dofc d, tm_dofc_item e
										WHERE d.i_do = e.i_do AND c.i_do_item = e.i_do_item AND
										b.i_color=c.i_color 
										AND (d.d_do >='$d_first' AND d.d_do <='$d_last') AND
										e.i_product='$row->imotif'
										GROUP BY b.e_color_name ORDER BY b.e_color_name ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
							
					foreach ($hasilxx as $rownya) {
						$listwarna.= $rownya->e_color_name." : ".$rownya->jum."<br>";
					}
				}

				//-----------------------------------------------------
				
				$data_brg[] = array(		'imotif'=> $row->imotif,	
											'productmotif'=> $row->productmotif,	
											'qty'=> $row->qty,
											'saldo_awal'=> $row->saldo_awal,
											'sisa'=>$row->saldo_awal-$row->qty,
											'listwarna'=> $listwarna
											);
*/
				$data_brg[] = array('imotif'=> $row->imotif,	
											'productmotif'=> $row->productmotif,	
											'qty'=> $row->qty,
											'saldo_awal'=> $row->saldo_awal,
											'sisa'=>$row->sisa,
											'listwarna'=> $row->e_color_name,
											'icolor'=> $row->i_color
											);
			} // end foreach
		}
		else 
			$data_brg = '';
		return $data_brg;		
	}



	/*
	function update_saldo_awalfc($periode, $i_product, $e_product_name, $i_color, $qty_warna, $iter)
	{
		$db2=$this->load->database('db_external', TRUE);
		
var_dump($e_product_name);
die;
		{

	    for($x=0;$x<=$iter;$x++) {
		    for ($xx=0; $xx<count($i_color[$x]); $xx++) {
			    $iproduct = trim($i_product[$x][$xx]);
			    $icolor = trim($i_color[$x][$xx]);
			    $qty = trim($qty_warna[$x][$xx]);
          $db2->query("insert into tm_saldo_awalfc
          						 (i_periode, i_product, i_color, e_product_name, qty )
						          	  values
					          	 ('$periode','$i_product[$x]',$icolor,'$e_product_name[$x]',$qty)");
		    }
	    }



    }
	}
*/
/*
 function deletesfc($periode, $i_product, $e_product_name, $n_quantity_warna, $i_product_color, $i_color,$iterasi) 
    {

		$db2=$this->load->database('db_external', TRUE);
		//var_dump($e_product_name);
  		$query 	= $db2->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;

	    for($x=0;$x<=$iteration;$x++) {
		    for ($xx=0; $xx<count($i_color[$x]); $xx++) {
			    $iproductcolor = ($i_product_color[$x][$xx]);
			    $iproduct = ($i_product[$x][$xx]);
			    $icolor = ($i_color[$x][$xx]);
			    $qty = ($qty_warna[$x][$xx]);

		  $db2->query("DELETE FROM tm_saldo_awalfc WHERE i_periode='$periode' and i_product='$iproduct[$x]' and i_color='$icolor'");
    }
}
}
*/
function deletesfc($periode,$i_product,$e_product_name,$qty_warna, $i_product_color, $i_color,$iteration) 
{
		$db2=$this->load->database('db_external', TRUE);
		//var_dump($e_product_name);
  		$query 	= $db2->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		//$q_tm_opname = $db2->query(" SELECT * FROM tm_saldo_awalfc WHERE i_periode='$periode'");
//		if($q_tm_opname->num_rows()==0) {
	    for($x=0;$x<=$iteration;$x++) {
		    for ($xx=0; $xx<count($i_color[$x]); $xx++) {
			    $iproductcolor = ($i_product_color[$x][$xx]);
			    $iproduct = ($i_product[$x][$xx]);
			    $icolor = ($i_color[$x][$xx]);
			    $qty = ($qty_warna[$x][$xx]);
          $db2->query("delete from tm_saldo_awalfc where i_periode='$periode' and i_product='$i_product[$x]' and i_color='$icolor'");
		    }
	    }
#	    $db2->insert('tm_saldo_awalfc',$item[$x]);
//    }
//	  redirect('saldoawalfc/cformbaru/');
	}

function msimpanbaru($periode,$i_product,$e_product_name,$qty_warna, $i_product_color, $i_color,$iteration) 
{
		$db2=$this->load->database('db_external', TRUE);
		//var_dump($e_product_name);
  		$query 	= $db2->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		//$q_tm_opname = $db2->query(" SELECT * FROM tm_saldo_awalfc WHERE i_periode='$periode'");
//		if($q_tm_opname->num_rows()==0) {
	    for($x=0;$x<=$iteration;$x++) {
		    for ($xx=0; $xx<count($i_color[$x]); $xx++) {
			    $iproductcolor = ($i_product_color[$x][$xx]);
			    $iproduct = ($i_product[$x][$xx]);
			    $icolor = ($i_color[$x][$xx]);
			    $qty = ($qty_warna[$x][$xx]);
          $db2->query("insert into tm_saldo_awalfc
          						 (i_periode, i_product, i_color, e_product_name, qty, d_entry )
						          	  values
					          	 ('$periode','$i_product[$x]',$icolor,'$e_product_name[$x]',$qty,'$dentry')");
		    }
	    }
#	    $db2->insert('tm_saldo_awalfc',$item[$x]);
//    }
//	  redirect('saldoawalfc/cformbaru/');
	}
	




	function getbonmmasukitem($n_d_do_first,$n_d_do_last, $periode) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT UPPER(c.i_product) AS imotif, 
				c.e_product_name AS productmotif
				FROM tm_saldo_awalfc c 
				left join tm_dofc a on (a.f_do_cancel='f' 
				AND (a.d_do >='$n_d_do_first' 
				AND a.d_do <='$n_d_do_last')) 
				left join tm_dofc_item b on(c.i_product=b.i_product 
				and a.i_do = b.i_do) 
				left join tr_color d on (d.i_color=c.i_color)
				WHERE c.i_periode='$periode ' 
				GROUP BY c.i_product, c.e_product_name order by c.i_product ";
										
				$query	= $db2->query($qstr);
				if($query->num_rows()>0) {
					return $result = $query->result();
		}
	}



	function listbrgjadi() {
	$db2=$this->load->database('db_external', TRUE);
	
	return $db2->query("SELECT  a.i_product_base AS ibaseproduct, b.i_product_motif AS imotif, b.e_product_motifname AS motifname, 
	                    b.n_quantity AS qty
                      FROM tr_product_motif b 
                      INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
                      WHERE trim(b.i_product_motif) NOT IN (SELECT trim(d.i_product) AS iproduct FROM tm_saldo_awalfc d ) 
                      AND b.n_active=1
                      GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update
                      ORDER BY b.i_product_motif, a.d_update DESC  ");
}


	function listbrgjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
	
	$query	= $db2->query(" SELECT  a.i_product_base AS ibaseproduct, b.i_product_motif AS imotif, b.e_product_motifname AS motifname, 
                          b.n_quantity AS qty
                          FROM tr_product_motif b 
                          INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
                          WHERE trim(b.i_product_motif) NOT IN (SELECT trim(d.i_product) AS iproduct FROM tm_saldo_awalfc d ) 
                          AND b.n_active=1
                          GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update
                          ORDER BY b.i_product_motif, a.d_update DESC  LIMIT ".$limit." OFFSET ".$offset);
				
		if ($query->num_rows() > 0 ) {
			return $query->result();
		}
	}
	
	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		
	return $db2->query(" SELECT  a.i_product_base AS ibaseproduct,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty,
			a.f_stop_produksi
			
		FROM tr_product_motif b 
		INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
		WHERE b.n_active=1 AND b.i_product_motif=trim('$key')
		GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update ORDER BY a.d_update DESC ");		
	}
	
}
?>
