<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_all_pembelian($jenis_beli, $date_from, $date_to) {		
		$query	= $this->db->query(" SELECT distinct a.kode_supplier, b.nama FROM tm_pembelian_nofaktur a, tm_supplier b 
					WHERE a.kode_supplier = b.kode_supplier 
					AND a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli'
					
					UNION SELECT distinct a.kode_unit, b.nama FROM tm_faktur_makloon a, tm_supplier b 
					WHERE a.kode_unit = b.kode_supplier 
					AND a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli'
					AND a.jenis_makloon = '1'
					ORDER BY nama ASC ");
		
		$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
								
				$query3	= $this->db->query(" SELECT sum(b.total) as jum_total FROM tm_pembelian a, tm_pembelian_detail b, 
					tm_pembelian_nofaktur c, tm_pembelian_nofaktur_sj d, tm_jenis_bahan e, tm_jenis_barang f, tm_barang g
					WHERE g.id_jenis_bahan = e.id AND e.id_jenis_barang = f.id AND f.kode_kel_brg = 'B' AND
					a.id = b.id_pembelian AND c.id = d.id_pembelian_nofaktur AND a.kode_supplier = c.kode_supplier
					AND b.kode_brg = g.kode_brg
					AND a.no_sj = d.no_sj AND a.kode_supplier = '$row1->kode_supplier'
					AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' ");
										
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_total_baku	= $hasilrow->jum_total;
				}
				else {
					$jum_total_baku = '';
				}
				// 29 nov 2011
				$query3	= $this->db->query(" SELECT sum(b.biaya) as jum_total FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b, 
					tm_faktur_makloon c, tm_faktur_makloon_sj d WHERE
					a.id = b.id_sj_hasil_makloon AND c.id = d.id_faktur_makloon AND a.kode_unit = c.kode_unit
					AND a.no_sj = d.no_sj_masuk AND a.kode_unit = '$row1->kode_supplier'
					AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' AND c.jenis_makloon = '1' ");
										
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_total_baku_quilting	= $hasilrow->jum_total;
				}
				else {
					$jum_total_baku_quilting = '';
				}
				$jum_total_baku+= $jum_total_baku_quilting;
				
				$query3	= $this->db->query(" SELECT sum(b.total) as jum_total FROM tm_pembelian a, tm_pembelian_detail b, 
					tm_pembelian_nofaktur c, tm_pembelian_nofaktur_sj d, tm_jenis_bahan e, tm_jenis_barang f, tm_barang g
					WHERE g.id_jenis_bahan = e.id AND e.id_jenis_barang = f.id AND f.kode_kel_brg = 'P' AND
					a.id = b.id_pembelian AND c.id = d.id_pembelian_nofaktur AND a.kode_supplier = c.kode_supplier
					AND b.kode_brg = g.kode_brg
					AND a.no_sj = d.no_sj AND a.kode_supplier = '$row1->kode_supplier'
					AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_total_pembantu	= $hasilrow->jum_total;
				}
				else {
					$jum_total_pembantu = '';
				}
																
				$data_beli[] = array(		'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'jum_total_baku'=> $jum_total_baku,
											'jum_total_pembantu'=> $jum_total_pembantu
											);
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
      
}

