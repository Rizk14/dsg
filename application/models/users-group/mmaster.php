<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_groups');
    $this->db->order_by('gid','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_groups',array('gid'=>$id));
    $query = $this->db->query(" SELECT * FROM tm_groups WHERE gid='$id' ");
    return $query->result();		  
  }
  
  //
  function save($id_group, $nama, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'nama_group'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_groups',$data); }
	else {
		
		$data = array(
		  'nama_group'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('gid',$id_group);
		$this->db->update('tm_groups',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_groups', array('gid' => $id));
  }
  
  function cek_data($nama){
    $this->db->select("* FROM tm_groups WHERE nama_group = '$nama' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}

