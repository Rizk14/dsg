<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function cari_op($nop,$icust) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_op WHERE i_op_code=trim('$nop') AND i_customer='$icust' AND f_op_cancel='f' ");
	}

	function cari_sop($nsop) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_op WHERE i_sop=trim('$nsop') ");
	}
	
	function getnomorop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),5,(LENGTH(cast(i_op_code AS character varying))-4)) AS iopcode FROM tm_op WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 " );
	}

	function getthnop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),1,4) AS thn FROM tm_op WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
	}

	function getnomorsop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),5,(LENGTH(cast(i_sop AS character varying))-4)) AS isop FROM tm_op ORDER BY i_op DESC LIMIT 1 ");
	}

	function getnomorop_inc_cabang($i_customer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),5,(LENGTH(cast(i_op_code AS character varying))-4)) AS iopcode FROM tm_op WHERE i_customer='$i_customer' AND f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 " );
	}
	
	function getthnop_inc_cabang($i_customer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),1,4) AS thn FROM tm_op WHERE i_customer='$i_customer' AND f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
	}
		
	function getthnsop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),1,4) AS thisop FROM tm_op ORDER BY i_op DESC LIMIT 1 " );
	}
				
	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		
		$query	= $db2->get();
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lcabang() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		
		$db2->select(" a.i_branch AS codebranch, 
				    a.i_customer AS codecustomer, 
				    a.e_branch_name AS branch FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
#				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer_code ".$order." ",false);
#Dedy 2016-11-11
#		
		$query	= $db2->get();
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lcabangxx($i_customer) {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_branch_name ASC ";
		$db2->select(" * FROM tr_branch WHERE i_customer='$i_customer' ".$order." ",false);
		
		$query	= $db2->get();
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function getcabang($icusto) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_customer',$icusto);
		$db2->order_by('e_branch_name');
		
		return $db2->get('tr_branch');
	}
	
	function getproduct() {
		$db2=$this->load->database('db_external', TRUE);
		$db2->order_by('e_product_basename');
		
		return $db2->get('tr_product_base');
	}

	function listpquery($tabel,$order,$filter) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		
		if ($query->num_rows()>0) {
			return $result	= $query->result(); 
		}
	}

	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		/*
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND d.d_so = '2010-12%' ORDER BY b.i_product_motif DESC		
		*/
		
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false ORDER BY b.i_product_motif DESC ");
		*/
		
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' ORDER BY b.i_product_motif ASC ");		
	}

	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		//$tgl	= date("Y-m-d");
		/*
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0' AND d.d_so = '$tgl%' ORDER BY b.i_product_motif DESC LIMIT		
		*/
		
		/* Disabled 07-02-2011
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false 
				
				ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset, false);
		*/
		
		/*
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       b.n_quantity AS qty
					   
				FROM tr_product_motif b INNER JOIN tr_product_base a
				
					ON a.i_product_base=b.i_product WHERE b.i_product_motif='XX00100' ", false);
		*/			
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset, false);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$ky_upper' ) 
				
				ORDER BY b.i_product_motif DESC ");
		*/
		
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$ky_upper' ) 
				
				ORDER BY b.i_product_motif ASC ");		
	}

	function flbarangjadiperpages($txt,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		/* Disabled 07-02-2011
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false 
				
				ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset, false);
		*/
		
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset, false);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}


	function msimpan($i_op,$i_customer,$i_branch,$d_op,$d_delivery_limit,$i_sop,$f_op_dropforcast,$v_count,$i_product,
					$e_product_name,$e_note,$iteration, $jenis_op) {	
		$db2=$this->load->database('db_external', TRUE);
		$i_op_item	= array();
		$tm_op_item	= array();
		$tm_sop_item= array();
		
		$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;	
				
		$q_cek_oder	= $db2->query(" SELECT * FROM tm_op WHERE i_op_code=trim('$i_op') AND i_customer='$i_customer' AND f_op_cancel='f' ");
		
		if($q_cek_oder->num_rows()>0) {
			$ada=1;
		}else{
			$ada=0;
		}

		$seq_tm_op	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_op ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
		
		if($seq_tm_op->num_rows() > 0 ) {
			$seqrow	= $seq_tm_op->row();
			$iop	= $seqrow->i_op+1;
		}else{
			$iop	= 1;
		}

		$seq_tm_sop	= $db2->query(" SELECT i_sop FROM tm_sop ORDER BY i_sop DESC LIMIT 1 ");
		
		if($seq_tm_sop->num_rows() > 0 ) {
			$seqrow2= $seq_tm_sop->row();
			$isop	= $seqrow2->i_sop+1;
		}else{
			$isop	= 1;
		}
				
		if($ada==0) {
			$tm_op	= array(
				 'i_op'=>$iop,
				 'i_op_code'=>$i_op,
				 'i_customer'=>$i_customer,
				 'i_branch'=>$i_branch,
				 'd_op'=>$d_op,
				 'd_delivery_limit'=>$d_delivery_limit,
				 'i_sop'=>$i_sop,
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry,
				 'f_op_dropforcast'=>$f_op_dropforcast,
				 'jenis_op'=>$jenis_op);

			$tm_sop	= array(
				 'i_sop'=>$isop,
				 'i_sop_code'=>$i_sop,
				 'i_customer'=>$i_customer,
				 'i_branch'=>$i_branch,
				 'd_sop'=>$d_op,
				 'i_op'=>$iop,
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry
			);
				 	
			if($db2->insert('tm_op',$tm_op)) {
				
				$db2->insert('tm_sop',$tm_sop);
				
				for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
					
					$seq_tm_op_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_op_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
					if($seq_tm_op_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_op_item->row();
						$i_op_item[$jumlah]	= $seqrow->i_op_item+1;
					}else{
						$i_op_item[$jumlah]	= 1;
					}

					$seq_tm_sop_item	= $db2->query(" SELECT i_sop_item FROM tm_sop_item ORDER BY i_sop_item DESC LIMIT 1 ");
					if($seq_tm_sop_item->num_rows() > 0 ) {
						$seqrow2	= $seq_tm_sop_item->row();
						$i_sop_item	= $seqrow2->i_sop_item+1;
					}else{
						$i_sop_item	= 1;
					}
						
					$tm_op_item[$jumlah]	= array(
									 'i_op_item'=>$i_op_item[$jumlah],
									 'i_op'=>$iop[$xx],
									 'i_product'=>$i_product[$jumlah],
									 'n_count'=>$v_count[$jumlah],
									 'e_product_name'=>$e_product_name[$jumlah],
									 'f_delivered'=>'FALSE',
									 'n_residual'=>$v_count[$jumlah],
									 'e_note'=>$e_note[$jumlah],
									 'd_entry'=>$dentry);

					$tm_sop_item[$jumlah]	= array(
									 'i_sop_item'=>$i_sop_item,
									 'i_sop'=>$isop,
									 'i_product'=>$i_product[$jumlah],
									 'e_product_name'=>$e_product_name[$jumlah],
									 'd_entry'=>$dentry);
					
					$db2->insert('tm_op_item',$tm_op_item[$jumlah]);
					$db2->insert('tm_sop_item',$tm_sop_item[$jumlah]);
				}
			}else{
			}

			if($db2->trans_status()===FALSE || $db2->trans_status()==false) {
				$db2->trans_rollback();
				$ok = 0;
			}else{
				$db2->trans_commit();
				$ok = 1;
			}
						
			if($ok==1) {
				/*** redirect('op/cform/'); ***/
				print "<script>alert(\"Nomor OP : '\"+$i_op+\"' telah disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}else{
				/*** redirect('op/cform/'); ***/
				print "<script>alert(\"Maaf, Order Pembelian (OP) gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}
			
		}else{
			print "<script>alert(\"Maaf, Nomor OP tsb sebelumnya telah diinput. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
		
	}

	function get_op_sab(){
		
		//$konek = "host=192.168.0.218 user=postgres dbname=sab port=5432 password=dialogue";
		//$konek = "host=192.168.0.107 user=dedy dbname=dialogue port=5432 password=dedyalamsyah";
		//$db = pg_connect($konek);
		/*
		 * skrip asli dari view//
		 * CREATE OR REPLACE VIEW v_op_itemduta AS 
			 SELECT b.i_op, b.i_product, b.i_product_motif, b.i_product_grade, b.n_order, b.n_delivery, b.v_product_mill, 
			   b.e_product_name, b.n_item_no
			   FROM tm_op a, tm_op_item b
			  WHERE a.i_op = b.i_op AND a.i_supplier = 'SP030'::bpchar AND a.f_op_close = false;
		 * 
		 * */
		 $dbpel=$this->load->database('db_sab', TRUE);
		 $db2=$this->load->database('db_external', TRUE);
		 $db2->trans_begin();
		 $data_op = array(); 
		 $i=1;
		 $d_opyes=date("Y-m-d",strtotime(""));

		 
		$query	=$dbpel->query("SELECT * FROM tm_po a,tm_po_item b, tr_product c WHERE a.i_po = b.i_po and b.i_product = c.i_product and a.i_supplier = '1' and a.f_po_cancel = 'f' and a.f_po_transfer = 't' and a.f_po_spb = 'f' order by a.i_po_no desc");

			if ($query->num_rows() > 0 ){
				$result = $query->result();
				
				foreach ($result as $row) {
					$qbranch = $db2->query("SELECT i_branch_code, e_initial FROM tr_branch WHERE i_customer = '325' AND i_code = '6413' ");
					
					if ($qbranch->num_rows() > 0 ) {
						$rbranch = $qbranch->row();
						$i_branch_code	= $rbranch->i_branch_code;
						$inisial	= $rbranch->e_initial;
					}
				
					$data_op[] = array(
							'no' => $i,
							'i_po_no' => $row->i_po_no,
							'd_po' => $row->d_po,
							'i_branch_code' => $i_branch_code,
							'e_initial' => $inisial,
							'i_product' => $row->i_product,
							'i_product_code' => $row->i_product_code,
							'e_product_name' => $row->e_product_name,
							'n_quantity' => $row->n_quantity

						);
					$i++;
					}	
			
					
			}

			return $data_op;


		}


		function simpanopsab($no_op,$tgl_op,$i_product, $i_product_motif,$e_product_name, $qty, $fck,$iterasi, 
						$i_customer, $i_branch_code, $d_delivery_limit){
			$tahun	= date("Y");
			$db2	= $this->load->database('db_external', TRUE);
			
			

			$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date");
			$drow	= $qdate->row();
			$dentry = $drow->date;


		for($xx=1; $xx<=$iterasi-1; $xx++) {
			if ($fck[$xx] == 'y') {

			$noop2 = substr($no_op[$xx],-5);
			$noop2 = "SAB-".$tahun.$noop2;

				$qopsab = $db2->query("SELECT i_op FROM tm_op WHERE i_op_code = '".$noop2."' AND f_op_cancel = 'f' ");
				
				if($qopsab->num_rows()==0){
			
					$seq_tm_op	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_op 
									ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
						if($seq_tm_op->num_rows() > 0 ) {
							$seqrow	= $seq_tm_op->row();
							$iop	= $seqrow->i_op+1;
						}else{
							$iop	= 1;
						}
						
						// *) sequence i_sop
					$seq_tm_sop	= $db2->query(" SELECT i_sop FROM tm_sop ORDER BY i_sop DESC LIMIT 1 ");
						if($seq_tm_sop->num_rows() > 0 ) {
							$seqrow2= $seq_tm_sop->row();
							$isop	= $seqrow2->i_sop+1;
						}else{
							$isop	= 1;
						}

					$qrytsop = $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),1,4) AS thisop FROM tm_op ORDER BY i_op DESC LIMIT 1 " );
					$qrysop	 = $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),5,(LENGTH(cast(i_sop AS character varying))-4)) AS isop FROM tm_op ORDER BY i_op DESC LIMIT 1 ");
					if($qrytsop->num_rows() > 0) {
						$th		= $qrytsop->row_array();
						$thn2	= $th['thisop'];
					} else {
						$thn2	= $tahun;
					}
						
						if($thn2==$tahun) {
							if($qrysop->num_rows() > 0)  {
								$row2	= $qrysop->row_array();
								$sop		= $row2['isop']+1;		
								switch(strlen($sop)) {
									case "1": $nomorsop	= "0000".$sop;
									break;
									case "2": $nomorsop	= "000".$sop;
									break;	
									case "3": $nomorsop	= "00".$sop;
									break;
									case "4": $nomorsop	= "0".$sop;
									break;
									case "5": $nomorsop	= $sop;
									break;	
								}
							} else {
								$nomorsop		= "00001";
							}
							$nomor2	= $tahun.$nomorsop."B";
						} else {
							$nomor2	= $tahun."00001";
						}


					//insert ke tm_op dan tm_sop
					$tm_op = array(
							'i_op' 				=> $iop,
							'i_op_code' 		=> $noop2,
							'i_customer'		=> $i_customer,
							'i_branch'			=> $i_branch_code,
							'd_op'				=> $tgl_op[$xx],
							'd_delivery_limit'	=> $d_delivery_limit,
							'i_sop'				=> $nomor2,
							'd_entry'			=> $dentry,
							'd_update'			=> $dentry,
							'f_transfer_op'		=> 't',
							'no_op_sab'			=> $no_op[$xx]
							);
					$db2->insert('tm_op',$tm_op);

					$tm_sop = array(
							'i_sop'				=> $isop,
							'i_sop_code'		=> $nomor2,
							'i_customer'		=> $i_customer,
							'i_branch'			=> $i_branch_code,
							'd_sop'				=> $tgl_op[$xx],
							'i_op'				=> $iop,
							'd_entry'			=> $dentry,
							'd_update'			=> $dentry,
							'f_transfer_op'		=> 't'
							);
					$db2->insert('tm_sop',$tm_sop);



					//tabel 2 item barang
					$qbrgjadi = $db2->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '".$i_product_motif[$xx]."' ");

						if ($qbrgjadi->num_rows() > 0) {
							
							$rbrgjadi		= $qbrgjadi->row();
							$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
							$kode_brg_jadi	= $i_product_motif[$xx];
						}else {
							$qbrgjadi	= $db2->query(" SELECT e_product_motifname FROM tr_product_motif
												WHERE i_product_motif = '".$i_product[$xx]."00' ");
							if($qbrgjadi->num_rows() > 0 ) {
								$rbrgjadi	= $qbrgjadi->row();
								$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
								$kode_brg_jadi = $i_product[$xx]."00";
							}
							else {
								$nama_brg_jadi = "";
								$kode_brg_jadi = "";
							}
						}


					$seq_tm_op_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_op_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
						if($seq_tm_op_item->num_rows() > 0 ) {
							$seqrow	= $seq_tm_op_item->row();
							$i_op_item = $seqrow->i_op_item+1;
						}else{
							$i_op_item	= 1;
						}

						$seq_tm_sop_item	= $db2->query(" SELECT i_sop_item FROM tm_sop_item ORDER BY i_sop_item DESC LIMIT 1 ");
							if($seq_tm_sop_item->num_rows() > 0 ) {
								$seqrow2	= $seq_tm_sop_item->row();
								$i_sop_item	= $seqrow2->i_sop_item+1;
							}else{
								$i_sop_item	= 1;
							}

					$tm_op_item = array(
								'i_op_item'				=> $i_op_item,
								'i_op'					=> $iop,
								'i_product'				=> $kode_brg_jadi,
								'n_count'				=> $qty[$xx],
								'e_product_name'		=> $nama_brg_jadi,
								'f_delivered'			=> 'f',
								'n_residual'			=> $qty[$xx],
								'e_note'				=> '',
								'd_entry'				=> $dentry
								);

					$tm_sop_item = array(
								'i_sop_item'			=> $i_sop_item,
								'i_sop'					=> $isop,
								'i_product'				=> $kode_brg_jadi,
								'e_product_name'		=> $nama_brg_jadi,
								'd_entry'				=> $dentry
								);

					$db2->insert('tm_op_item',$tm_op_item);
					$db2->insert('tm_sop_item',$tm_sop_item);
								
				}else{
					//save tbl item saja
					// ambil id dari tabel tm_op dna tm_sop

					// tm_op
					$seq_tm_op	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_op 
									ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
					if($seq_tm_op->num_rows() > 0 ) {
							$seqrow	= $seq_tm_op->row();
							$iop	= $seqrow->i_op;
						}else{
							$iop	= 1;
						}


					// tm_sop
					$seq_tm_sop	= $db2->query(" SELECT i_sop FROM tm_sop ORDER BY i_sop DESC LIMIT 1 ");
						if($seq_tm_sop->num_rows() > 0 ) {
							$seqrow2= $seq_tm_sop->row();
							$isop	= $seqrow2->i_sop;
						}else{
							$isop	= 1;
						}

						$qbrgjadi = $db2->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '".$i_product_motif[$xx]."' ");

					if ($qbrgjadi->num_rows() > 0) {
							
							$rbrgjadi		= $qbrgjadi->row();
							$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
							$kode_brg_jadi	= $i_product_motif[$xx];
					}else {
							$qbrgjadi	= $db2->query(" SELECT e_product_motifname FROM tr_product_motif
												WHERE i_product_motif = '".$i_product[$xx]."00' ");
						if($qbrgjadi->num_rows() > 0 ) {
								$rbrgjadi	= $qbrgjadi->row();
								$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
								$kode_brg_jadi = $i_product[$xx]."00";
							}
						else {
								$nama_brg_jadi = "";
								$kode_brg_jadi = "";
						}
					}
					
					//tm_op_item
					$seq_tm_op_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_op_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
						if($seq_tm_op_item->num_rows() > 0 ) {
							$seqrow	= $seq_tm_op_item->row();
							$i_op_item = $seqrow->i_op_item+1;
						}else{
							$i_op_item	= 1;
						}

					//tm_sop_item
					$seq_tm_sop_item	= $db2->query(" SELECT i_sop_item FROM tm_sop_item ORDER BY i_sop_item DESC LIMIT 1 ");
						if($seq_tm_sop_item->num_rows() > 0 ) {
							$seqrow2	= $seq_tm_sop_item->row();
							$i_sop_item	= $seqrow2->i_sop_item+1;
						}else{
							$i_sop_item	= 1;
						}
					
					$tm_op_item	= array(
								'i_op_item'				=> $i_op_item,
								'i_op'					=> $iop,
								'i_product'				=> $kode_brg_jadi,
								'n_count'				=> $qty[$xx],
								'e_product_name'		=> $nama_brg_jadi,
								'f_delivered'			=> 'f',
								'n_residual'			=> $qty[$xx],
								'e_note'				=> '',
								'd_entry'				=> $dentry
										);

					$tm_sop_item	= array(
								'i_sop_item'			=> $i_sop_item,
								'i_sop'					=> $isop,
								'i_product'				=> $kode_brg_jadi,
								'e_product_name'		=> $nama_brg_jadi,
								'd_entry'				=> $dentry
								);
						
					$db2->insert('tm_op_item',$tm_op_item);
					$db2->insert('tm_sop_item',$tm_sop_item);	
				}


			}//IF FCK
					
		}// TUtup FOR

			if($db2->trans_status()===FALSE || $db2->trans_status()==false) {
				$db2->trans_rollback();
				$ok = 0;
			}else{
				$db2->trans_commit();
				$ok = 1;
					
				for($xx=1; $xx<=$iterasi-1; $xx++) {

					if ($fck[$xx] == 'y') {

						$db3	= $this->load->database('db_sab',TRUE);
								
						$data = array(

								'f_po_spb'		=> 't'

									);
						$db3->where	('i_po_no',$no_op[$xx]);			
						$db3->update('tm_po',$data);
						
					}

				}
				
				echo $ok;
			}

			if($ok==1) {
				/*** redirect('op/cform/'); ***/

				print "<script>alert(\"Transfer OP DGU berhasil disimpan. Terimakasih.\");window.open(\"opsab\", \"_self\");</script>";
			 }
			 else{
			 	/*** redirect('op/cform/'); ***/
				print "<script>alert(\"Maaf, transfer Order Pembelian (OP) DGU gagal disimpan. Terimakasih.\");window.open(\"opsab\", \"_self\");</script>";
			 }

		} //Function
			
}
?>
