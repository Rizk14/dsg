<?php
class Mreport extends CI_Model{
  function Mreport() {

  parent::__construct();
  }
  
 function get_gudang(){
    $query = $this->db->query(" SELECT * FROM tm_gudang where kel_gudang='4' ");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
    function get_transaksi_gudang($gudang, $date_from, $date_to, $list_id_brg_wip) {		
	 $pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgldari = $thn1."-".$bln1."-".$tgl1;
		
		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$pisah1 = explode("-", $date_to);
		$tgl2= $pisah1[0];
		$bln2= $pisah1[1];
		$thn2= $pisah1[2];
		$tglke = $thn2."-".$bln2."-".$tgl2;
				
		$data_brg_wip = array();
		$data_warna = array();
		$data_so_warna = array();
		$jum_stok_opname = 0;

//=========================================================================================================================

		$id_brg_wip_exp = explode(";", $list_id_brg_wip);
		foreach($id_brg_wip_exp as $row1) {
			if ($row1 != '') {
		
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$row1' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk_bgs1'=> 0,
												'tot_keluar_bgs_packing1'=> 0,
											);
					}
				}
// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}



				
				// ambil SO dari tt_stok_opname_hasil_jahit_detail
				$sqlwarna = " SELECT c.id_warna, d.nama, c.jum_stok_opname ,c.jum_stok_opname
							FROM tt_stok_opname_hasil_jahit a INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
							INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail 
							INNER JOIN tm_warna d ON d.id = c.id_warna
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$row1' AND a.id_gudang='$gudang' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['id_warna'] == $rowwarna->id_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_stok_opname;
							}
						} // end for
						
						$data_so_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_stok_opname'=> $rowwarna->jum_stok_opname
											);
					}
				}
				else
					$data_so_warna = '';
				
				// 19-01-2016, tambahan. SO-nya ambil dari global juga
				// ambil SO dari tt_stok_opname_unit_jahit_detail
				$sqlso = " SELECT b.jum_stok_opname,b.tot_jum_stok_opname
							FROM tt_stok_opname_hasil_jahit a INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$row1' AND a.id_gudang='$gudang' ";
				$queryso	= $this->db->query($sqlso);
				if ($queryso->num_rows() > 0){
					$hasilso = $queryso->row();
					$jum_stok_opname = $hasilso->jum_stok_opname;
				}
				
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
											
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 1 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain , 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$gudang' AND a.jenis_masuk = '6' AND b.id_brg_wip='$row1' ";
				

				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 1 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$gudang' AND a.jenis_keluar in ('4','7') AND b.id_brg_wip='$row1'
						AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						ORDER BY tgl_sj ASC ";
		
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				$tot_saldo=$jum_stok_opname;
				
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$id_gudang = $row2->id_gudang;
						$masuk_bgs = $row2->masuk_bgs;
						$keluar_bgs_packing = $row2->keluar_bgs_packing;

						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($masuk_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty,a.id_unit_jahit,a.id_unit_packing FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '6'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
																		
						// keluar
						if ($keluar_bgs_packing == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty,a.id_unit_jahit,a.id_unit_packing FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar in ('4','7')
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}

						// edit 15-02-2016

						// --------------------
						
						// --------------------
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;

								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											// 11-12-2015
											$tot_saldo+= $row3->qty;
											
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											// 11-12-2015
											$tot_saldo-= $row3->qty;
											
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_bgs == '1') {
											$data_warna[$xx]['tot_masuk_bgs1']+= $row3->qty;
										}
										
										else if ($keluar_bgs_packing == '1') {
											$data_warna[$xx]['tot_keluar_bgs_packing1']+= $row3->qty;
										}
										
									}
								}
							
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															
															);
							} // end foreach
						}
						
						if ($id_gudang != '0') {
							$query3	= $this->db->query(" SELECT a.nama as nama_lokasi, b.kode_gudang, b.nama as nama_gudang FROM tm_lokasi_gudang a 
									INNER JOIN tm_gudang b ON a.id = b.id_lokasi 
									WHERE b.id = '$id_gudang' ");
							$hasilrow = $query3->row();
							$nama_lokasi	= $hasilrow->nama_lokasi;
							$nama_gudang	= $hasilrow->nama_gudang;
							$kode_gudang	= $hasilrow->kode_gudang;
							$gudangnya = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
						}
						else {
							//$gudangnya = "Semua";
							$kode_gudang = "";
							$nama_gudang = "";
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'id_gudang'=> $id_gudang,
												'nama_lokasi'=> $nama_lokasi,
												'kode_gudang'=> $kode_gudang,
												'nama_gudang'=> $nama_gudang,
												'masuk_bgs'=> $masuk_bgs,
												'keluar_bgs_packing'=> $keluar_bgs_packing,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												'tot_saldo'=> $tot_saldo
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();


					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
		// =================================== END NEW =======================================================
				
		// array brg wip
		$data_brg_wip[] = array(
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna,
												'jum_stok_opname'=> $jum_stok_opname,
												'gtot_saldo'=> $tot_saldo
									);
						$data_tabel1 = array();
				$data_warna = array();
				$data_so_warna = array();
	}
}

		return $data_brg_wip;

}


  
function get_mutasi_gudang($date_from, $date_to, $gudang) {		
	  
	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	// explode date_from utk keperluan saldo awal 
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
			
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}

	$periode = $thn1.$bln1;
	  
	$sql = "SELECT a.id_brg_wip, a.id_kel_brg_wip FROM (
				select b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukwip a 
				INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarwip a 
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id
				
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a 
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit 
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.id_gudang = '$gudang' /*AND b.jum_stok_opname <> 0*/
					 GROUP BY b.id_brg_wip, d.id
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a 
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit 
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1' 
					AND a.id_gudang = '$gudang' /*AND b.jum_stok_opname <> 0*/
					 GROUP BY b.id_brg_wip, d.id
				)
				a GROUP BY a.id_brg_wip, a.id_kel_brg_wip ORDER BY a.id_kel_brg_wip, a.id_brg_wip"; //echo $sql; die();
		//AND a.status_approve='t'
		
		$query	= $this->db->query($sql);
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}

				// 13-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
				// 04-03-2015, saldo awal bukan dari jum_stok_opname, tapi ambil dari auto_saldo_akhir
				// 21-04-2015, skrg saldo awal ambil dari SO LAGI.
				// 12-11-2015, khusus gudang QC, ambil dari SO
				
				 if($periode <= '202008'){
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname,b.id FROM tt_stok_opname_hasil_jahit a 
									INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't' 
									AND a.status_approve = 't'");
					if ($queryx->num_rows() > 0){
						$hasilrow = $queryx->row();
						$saldo_awal = $hasilrow->jum_stok_opname;
						$id_so_detail = $hasilrow->id;							
					}else{						
						$saldo_awal=0;
					}
				 }if($periode >= '202008'){
					$queryx	= $this->db->query(" SELECT b.saldo_akhir,b.id FROM tt_stok_opname_hasil_jahit a 
									INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't' 
									AND a.status_approve = 't'");
					if ($queryx->num_rows() > 0){
						$hasilrow = $queryx->row();
						$saldo_awal = $hasilrow->saldo_akhir;
						$id_so_detail = $hasilrow->id;							
					}else{						
						$saldo_awal=0;
					}
				}						

				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip				
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '6' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;	
					
				}
				else
					$jum_masuk = 0;
				
				//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip				
					$jum_masuk_total = $jum_masuk;					
			//--------------------------------------------------------------------------------------------------	
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '7' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_pack = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_pack = 0;
				
				
				
				$jum_keluar_total = $jum_keluar_pack;
				// 18-01-2016, saldo akhir dihitung dari selisih masuk dan keluar
				$jum_saldo_akhir = $saldo_awal + $jum_masuk_total - $jum_keluar_total; 
				
	

				// 13-09-2013 ================================
				$sql = "SELECT a.tgl_so,a.jenis_perhitungan_stok, b.id, b.jum_stok_opname FROM tt_stok_opname_hasil_jahit a 
							INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
							WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1'  ";
				//if($periode >= )
				//AND b.status_approve = 't'
												
				$query3	= $this->db->query($sql);
				
				//$jum_so='';
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_so_detail = $hasilrow->id;
					$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
					$tgl_so = $hasilrow->tgl_so;
					
					// 15-12-2015, update saldo akhir di tabel stok opname nya
					$sqlupdate = " UPDATE tt_stok_opname_hasil_jahit_detail SET saldo_akhir = '$jum_saldo_akhir'
									WHERE id = '$id_so_detail' ";
					$this->db->query($sqlupdate);
					
						//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '6' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masukx = $hasilrow77->jum_masuk;
							
							if ($jum_masukx == '')
								$jum_masukx = 0;
						}
						else
							$jum_masukx = 0;
							
					$jum_masuk_totalx = $jum_masukx;
					
							// 1. hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '7' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_packx = $hasilrow77->jum_keluar;
							
							if ($jum_keluar_packx == '')
								$jum_keluar_packx = 0;
						}
						else
							$jum_keluar_packx = 0;
							
								$jum_keluar_totalx = $jum_keluar_packx;
								
								$tambahkurang = $jum_masuk_totalx - $jum_keluar_totalx;
								
						$jum_stok_opname = $jum_stok_opname+ $tambahkurang;	
						
					$sqlupdate="Update tt_stok_opname_hasil_jahit_detail set tot_jum_stok_opname='$jum_stok_opname'		
					WHERE id='$id_so_detail'";	
					$this->db->query($sqlupdate);
						
					$sqlupdatetm = " UPDATE tm_stok_hasil_jahit SET stok = '$jum_stok_opname'
									WHERE id_brg_wip = '$row1->id_brg_wip'
									AND id_gudang='$gudang' ";
					$this->db->query($sqlupdatetm);
					
						
				}
				else {
					$id_so_detail = 0;
					$jum_stok_opname = 0;
				}
				//=========================================
				
			
				//17-09-2014, ambil nama kel brg jadi
				if ($row1->id_kel_brg_wip != '') {
					$sqlxx = " SELECT kode, nama FROM tm_kel_brg_wip WHERE id='$row1->id_kel_brg_wip' ";
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$kode_kel = $hasilxx->kode;
						$nama_kel = $hasilxx->nama;
					}
					else {
						$kode_kel = '';
						$nama_kel = '';
					}
				}
				else {
					$kode_kel = '';
					$nama_kel = '';
				}
																
				$data_stok[] = array(		'kode_brg'=> $kode_brg_wip,
											'kode_kel'=> $kode_kel,
											'nama_kel'=> $nama_kel,
											'nama_brg'=> $nama_brg_wip,
											'saldo_awal'=> $saldo_awal,
											'jum_keluar_pack'=>$jum_keluar_pack,
											'jum_masuk'=>$jum_masuk,
											'jum_saldo_akhir'=>$jum_saldo_akhir,
											'jum_stok_opname'=> $jum_stok_opname,
											
											);

											
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }

}