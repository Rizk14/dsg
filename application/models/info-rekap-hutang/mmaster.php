<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}

	function get_all_rekap_pembelian($jenis_beli, $date_from, $date_to, $jenisdata) {
	//if ($jenis_beli == 1) {			
		/*$query	= $this->db->query(" SELECT distinct a.kode_supplier, b.nama FROM tm_pembelian_nofaktur a, tm_supplier b 
					WHERE a.kode_supplier = b.kode_supplier 
					AND a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '1' ORDER BY a.kode_supplier "); */
		//$sql = " SELECT id, kode_supplier, nama FROM tm_supplier ORDER BY kode_supplier ";
		
		// 13-07-2015
		$pisah1 = explode("-", $date_from);
		$thn1= $pisah1[2];
		$bln1= $pisah1[1];
		$tgl1= $pisah1[0];
		
		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// 26-08-2015 ALGORITMANYA DIGANTI:
		/* 1. QUERY KE SEMUA SUPPLIER
		 * 2. DIDALAM PERULANGAN HASIL QUERY, CEK APAKAH SUPPLIER TSB ADA TRANSAKSI PEMBELIAN DI RANGE TGL YG DIPILIH di tabel tm_pembelian
		 * 3. JIKA TIDAK ADA TRANSAKSI, CEK APAKAH SALDO HUTANG MASIH ADA: cek di tabel tt_stok_opname_hutang_dagang, lalu hitung juga selisih hutang dikurangi pelunasannya di bulan sebelum range tanggal yg dipilih
		 * 4. JIKA MASIH ADA HUTANG, MAKA MUNCULKAN SUPPLIER TSB
		 */
		
		/*$sql = " SELECT DISTINCT a.id, a.kode_supplier, a.nama FROM tm_supplier a INNER JOIN tm_pembelian b ON b.id_supplier = a.id
				WHERE b.tgl_sj >= to_date('$date_from','dd-mm-yyyy') AND b.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
				AND b.jenis_pembelian='$jenis_beli' ORDER BY a.kode_supplier "; */
		$sql = " SELECT id, kode_supplier, nama, kategori,jenis FROM tm_supplier ORDER BY kode_supplier ";
		$query	= $this->db->query($sql);
		
		$pencarian='';
		if($jenis_beli!=0)
			$pencarian.= " AND a.jenis_pembelian = '$jenis_beli' ";
		$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$kategori_sup = $row1->kategori;
				$jenis_sup = $row1->jenis;
				// 1. hitung jumlah saldo awal yg tanggalnya < date_from ===================
				// 25 mei 2011
				// hitung total hutang dagang, jum retur
				// 13-07-2015, kalo tgl awal = juli 2015 maka ambil data saldo awal dari tabel tt_stok_opname_hutang_dagang
				if ($bln1 == '07' && $thn1 == '2015') {
					if ($kategori_sup == '1') {
						$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
											INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
											WHERE a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.id_supplier = '$row1->id' ");
						if ($query2->num_rows() > 0){
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->jum_stok_opname;
							if ($tot_hutang == '')
								$tot_hutang = 0;
						}
						else
							$tot_hutang = 0;
					}
					else
						$tot_hutang = 0;
				}
				else if(($bln1 >= '07' && $thn1 == '2015')||($bln1 <= '12' && $thn1 == '2016') ) {
					// 23-07-2015
					if ($jenisdata == '1') {
						if ($kategori_sup == '1') {
							$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.status_aktif = 't'
												AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_sj >= '2015-07-01'
												AND b.id = '$row1->id' "
												.$pencarian);
							/*echo "SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't'
												AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
												AND b.id = '$row1->id' <br>"; */
												
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
								
							// 09-09-2015, tambahkan hutang dari saldo awal hutang yg diinput pertama kali (= saldo akhir juni 2015)
							$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
												INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
												WHERE a.bulan = '06' AND a.tahun = '2015' AND b.id_supplier = '$row1->id' ");
							if ($query2->num_rows() > 0){
								$hasilrow = $query2->row();
								$tot_hutang	+= $hasilrow->jum_stok_opname;
							}
							// ---------------------------------------------
						}
						else {
							$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian_makloon a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.status_aktif = 't'
												AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_sj >= '2015-07-01'
												AND b.id = '$row1->id' "
												.$pencarian);							
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
						}
					}
					else {
						if ($kategori_sup == '1') {
							$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_hutang FROM tm_pembelian_nofaktur a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.tgl_faktur < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_faktur >= '2015-07-01'
												AND b.id = '$row1->id' "
												.$pencarian);
												
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
							
							// 09-09-2015, tambahkan hutang dari saldo awal hutang yg diinput pertama kali (= saldo akhir juni 2015)
							$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
												INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
												WHERE a.bulan = '06' AND a.tahun = '2015' AND b.id_supplier = '$row1->id' ");
							if ($query2->num_rows() > 0){
								$hasilrow = $query2->row();
								$tot_hutang	+= $hasilrow->jum_stok_opname;
							}
						}
						else {
							$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_hutang FROM tm_pembelian_makloon_faktur a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.tgl_faktur < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_faktur >= '2015-07-01'
												AND b.id = '$row1->id' "
												.$pencarian);
												
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
						}
					}
				}
					else if(($bln1 == '01' && $thn1 == '2017') ) {
		
					if ($jenisdata == '1') {
						$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
											INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
											WHERE a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.id_supplier = '$row1->id' ");
						if ($query2->num_rows() > 0){
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->jum_stok_opname;
							if ($tot_hutang == '')
								$tot_hutang = 0;
						}
						else
							$tot_hutang = 0;
					}
					else {
						$tot_hutang = 0;
					}
				}
			else if(($bln1 >= '01' && $thn1 >= '2017') ) {

					
						if ($jenisdata == '1') {
						if ($kategori_sup == '1') {
							$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.status_aktif = 't'
												AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_sj >= '2017-01-01'
												AND b.id = '$row1->id' "
												.$pencarian);
							/*echo "SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't'
												AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
												AND b.id = '$row1->id' <br>"; */
												
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
								
							// 09-09-2015, tambahkan hutang dari saldo awal hutang yg diinput pertama kali 
							$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
												INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
												WHERE a.bulan = '01' AND a.tahun = '2017' AND b.id_supplier = '$row1->id' ");
							if ($query2->num_rows() > 0){
								$hasilrow = $query2->row();
								$tot_hutang	+= $hasilrow->jum_stok_opname;
							}
							// ---------------------------------------------
						}
						else {
							$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian_makloon a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.status_aktif = 't'
												AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_sj >= '2017-01-01'
												AND b.id = '$row1->id' "
												.$pencarian);							
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
						}
					}
					else {
						if ($kategori_sup == '1') {
							$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_hutang FROM tm_pembelian_nofaktur a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.tgl_faktur < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_faktur >= 2017-01-01'
												AND b.id = '$row1->id' "
												.$pencarian);
												
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
							
							// 09-09-2015, tambahkan hutang dari saldo awal hutang yg diinput pertama kali (= saldo akhir juni 2015)
							$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
												INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
												WHERE a.bulan = '01' AND a.tahun = '2017' AND b.id_supplier = '$row1->id' ");
							if ($query2->num_rows() > 0){
								$hasilrow = $query2->row();
								$tot_hutang	+= $hasilrow->jum_stok_opname;
							}
						}
						else {
							$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_hutang FROM tm_pembelian_makloon_faktur a 
												INNER JOIN tm_supplier b ON a.id_supplier = b.id
												WHERE a.tgl_faktur < to_date('$date_from','dd-mm-yyyy') 
												AND a.tgl_faktur >= '2017-01-01'
												AND b.id = '$row1->id' "
												.$pencarian);
												
							$hasilrow = $query2->row();
							$tot_hutang	= $hasilrow->tot_hutang;
							if ($tot_hutang == '')
								$tot_hutang = 0;
						}
					}
				}
			
				// 28 nov 2011. 30-06-2015 GA DIPAKE
				/*$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_hutang FROM tm_faktur_makloon a, tm_supplier b 
				WHERE a.kode_unit = b.kode_supplier AND a.jenis_pembelian = '$jenis_beli'
				AND a.tgl_faktur < to_date('$date_from','dd-mm-yyyy') 
				AND a.kode_unit = '$row1->kode_supplier' AND a.jenis_makloon = '1' ");
				$hasilrow = $query2->row();
				$tot_hutang_quilting	= $hasilrow->tot_hutang;
				if ($tot_hutang_quilting == '')
					$tot_hutang_quilting = 0;
				
				$tot_hutang += $tot_hutang_quilting;
				
				// hitung jum uang muka,
				$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_pembelian a, tm_pembelian_nofaktur b, 
				tm_pembelian_nofaktur_sj c WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
				AND b.id = c.id_pembelian_nofaktur AND b.tgl_faktur < to_date('$date_from','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND b.jenis_pembelian = '$jenis_beli' ");
				$hasilrow = $query2->row();
				$uang_muka	= $hasilrow->tot_uang_muka;
				if ($uang_muka == '')
					$uang_muka = 0;
				
				// 29 nov 2011
				$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_sj_hasil_makloon a, tm_faktur_makloon b, 
				tm_faktur_makloon_sj c WHERE a.kode_unit = b.kode_unit AND a.no_sj = c.no_sj_masuk 
				AND b.id = c.id_faktur_makloon AND b.tgl_faktur < to_date('$date_from','dd-mm-yyyy') 
				AND b.kode_unit = '$row1->kode_supplier' AND b.jenis_pembelian = '$jenis_beli' AND b.jenis_makloon = '1' ");
				$hasilrow = $query2->row();
				$uang_muka_quilting	= $hasilrow->tot_uang_muka;
				if ($uang_muka_quilting == '')
					$uang_muka_quilting = 0;
				
				$uang_muka+= $uang_muka_quilting; */
				
				// hitung jum bayar
				if (($bln1 >= '07' && $thn1 == '2015')||($bln1 <= '12' && $thn1 == '2016')) {
				// 31 okt 2011								
				$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat , SUM(b.cndn) as tot_cndn 
				FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
				WHERE a.tgl < to_date('$date_from','dd-mm-yyyy')
				AND a.tgl >= '2015-07-01'
				AND b.id_supplier = '$row1->id' "
				.$pencarian);
				
				$hasilrow = $query2->row();
				$tot_bayar	= $hasilrow->tot_bayar;
				$tot_bulat	= $hasilrow->tot_bulat;
				$tot_cndn	= $hasilrow->tot_cndn;
				if ($tot_bayar == '')
					$tot_bayar = 0;
				if ($tot_bulat == '')
					$tot_bulat = 0;
				if ($tot_cndn == '')
					$tot_cndn = 0;
								
				// jumlah retur jika ada
				// koreksi 25 okt 2011: ga perlu ada acuan ke status_nota di nota retur
				// 27-08-2015 UNTUK SUPPLIER MAKLOON GA PAKE RETUR
				if ($kategori_sup == '1') {
					$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
					INNER JOIN tm_retur_beli_detail c ON a.id = c.id_retur_beli WHERE a.id_supplier = '$row1->id'
					AND a.tgl_retur < to_date('$date_from','dd-mm-yyyy')
					AND a.tgl_retur >= '2015-07-01' "
					.$pencarian
					);
						
					$hasilrow = $query2->row();
					$tot_retur	= $hasilrow->tot_retur;	
					if ($tot_retur == '')
						$tot_retur = 0;
				}
				else
					$tot_retur = 0;
								
				//ini asli. 30-06-2015 DIMODIF TANPA UANG MUKA
				//$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar-$tot_retur+$tot_bulat;
				$tot_hutang = $tot_hutang-$tot_bayar-$tot_retur+$tot_bulat;
				$tot_hutang = round($tot_hutang, 2);
			}
			elseif ($bln1 >= '01' && $thn1 == '2017') {
				// 31 okt 2011								
				$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat , SUM(b.cndn) as tot_cndn 
				FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
				WHERE a.tgl < to_date('$date_from','dd-mm-yyyy')
				AND a.tgl >= '2017-01-01'
				AND b.id_supplier = '$row1->id' "
				.$pencarian);
				
				$hasilrow = $query2->row();
				$tot_bayar	= $hasilrow->tot_bayar;
				$tot_bulat	= $hasilrow->tot_bulat;
				$tot_cndn	= $hasilrow->tot_cndn;
				if ($tot_bayar == '')
					$tot_bayar = 0;
				if ($tot_bulat == '')
					$tot_bulat = 0;
				if ($tot_cndn == '')
					$tot_cndn = 0;
								
				// jumlah retur jika ada
				// koreksi 25 okt 2011: ga perlu ada acuan ke status_nota di nota retur
				// 27-08-2015 UNTUK SUPPLIER MAKLOON GA PAKE RETUR
				if ($kategori_sup == '1') {
					$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
					INNER JOIN tm_retur_beli_detail c ON a.id = c.id_retur_beli WHERE a.id_supplier = '$row1->id'
					AND a.tgl_retur < to_date('$date_from','dd-mm-yyyy')
					AND a.tgl_retur >= '2017-01-01' "
					.$pencarian
					);
						
					$hasilrow = $query2->row();
					$tot_retur	= $hasilrow->tot_retur;	
					if ($tot_retur == '')
						$tot_retur = 0;
				}
				else
					$tot_retur = 0;
								
				//ini asli. 30-06-2015 DIMODIF TANPA UANG MUKA
				//$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar-$tot_retur+$tot_bulat;
				$tot_hutang = $tot_hutang-$tot_bayar-$tot_retur+$tot_bulat;
				$tot_hutang = round($tot_hutang, 2);
			}
				//if ($jenis_pembulatan == 2)
				//	$tot_hutang = $tot_hutang-$tot_bulat;
				
				/*if ($jenis_pembulatan == 1) // jika keatas
					$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar +$tot_bulat -$tot_retur; 
				else if ($jenis_pembulatan == 2)
					$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar -$tot_bulat -$tot_retur; 
				else
					$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar-$tot_retur;  */
				// tot_hutang ini sebagai saldo awal
					
				// ########################################################### end untuk saldo awal @@@@@@@@@@@@
								
				// 2. hitung jumlah uang muka di tabel SJ/pembelian. 30-06-2015 GA DIPAKE
				/*$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_pembelian a, tm_pembelian_nofaktur b, 
				tm_pembelian_nofaktur_sj c WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
				AND b.id = c.id_pembelian_nofaktur AND b.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
				AND b.tgl_faktur <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND b.jenis_pembelian = '$jenis_beli' ");
				$hasilrow = $query2->row();
				$uang_muka	= $hasilrow->tot_uang_muka;
				if ($uang_muka == '')
					$uang_muka = 0;
				
				// 29 nov 2011
				$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_sj_hasil_makloon a, tm_faktur_makloon b, 
				tm_faktur_makloon_sj c WHERE a.kode_unit = b.kode_unit AND a.no_sj = c.no_sj_masuk 
				AND b.id = c.id_faktur_makloon AND b.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
				AND b.tgl_faktur <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_unit = '$row1->kode_supplier' AND b.jenis_pembelian = '$jenis_beli' AND b.jenis_makloon = '1' ");
				$hasilrow = $query2->row();
				$uang_muka_quilting	= $hasilrow->tot_uang_muka;
				if ($uang_muka_quilting == '')
					$uang_muka_quilting = 0;
				
				$uang_muka+= $uang_muka_quilting; */
				
				// 3. hitung jumlah yg sudah dibayar di tabel payment_pembelian
				
				$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat , SUM(b.cndn) as tot_cndn 
				FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
				WHERE a.tgl >= to_date('$date_from','dd-mm-yyyy') AND a.tgl <= to_date('$date_to','dd-mm-yyyy') 
				AND b.id_supplier = '$row1->id' "
				.$pencarian
				);
				
				$hasilrow = $query2->row();
				$tot_cndn	= $hasilrow->tot_cndn;
				$tot_bayar	= $hasilrow->tot_bayar;
				$tot_bulat	= $hasilrow->tot_bulat;
				if ($tot_cndn == '')
					$tot_cndn = 0;
				if ($tot_bayar == '')
					$tot_bayar = 0;
				if ($tot_bulat == '')
					$tot_bulat = 0;
								
				// ##########################################################
				// 27-08-2015 UNTUK SUPPLIER MAKLOON GA PAKE RETUR
				if ($kategori_sup == '1') {
					$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
					INNER JOIN tm_retur_beli_detail c ON a.id = c.id_retur_beli
					WHERE a.jenis_pembelian = '$jenis_beli'
					AND a.id_supplier = '$row1->id'
					AND a.tgl_retur >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_retur <= to_date('$date_to','dd-mm-yyyy') ");
						
					$hasilrow = $query2->row();
					$tot_retur	= $hasilrow->tot_retur;	
					if ($tot_retur == '')
						$tot_retur = '0';
				}
				else
					$tot_retur = 0;
				// ##########################################################
								
				// 5. hitung jumlah pembelian pada range tanggal tsb
				// 23-07-2015
				if ($jenisdata == '1') {
					if ($kategori_sup == '1') {
						$query2	= $this->db->query(" SELECT SUM(a.total) as tot_pembelian FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id
						WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
						AND b.id = '$row1->id' AND a.status_aktif = 't' "
						.$pencarian
						);
						$hasilrow = $query2->row();
						$tot_pembelian	= $hasilrow->tot_pembelian;
						if ($tot_pembelian == '')
							$tot_pembelian = 0;
					}
					else {
						if($jenis_sup == '2'){
						$query2	= $this->db->query(" SELECT SUM(a.total) as tot_pembelian FROM tm_pembelian_makloon a INNER JOIN tm_supplier b ON a.id_supplier = b.id
						WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
						AND b.id = '$row1->id'  AND a.status_aktif = 't' "
						.$pencarian);
						$hasilrow = $query2->row();
						$tot_pembelian	= $hasilrow->tot_pembelian;
						if ($tot_pembelian == '')
							$tot_pembelian = 0;
						}
						elseif($jenis_sup == '3'){
						$query2	= $this->db->query(" SELECT SUM(a.total) as tot_pembelian FROM tm_pembelian_aplikasi a INNER JOIN tm_supplier b ON a.id_supplier = b.id
						WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
						AND b.id = '$row1->id'  AND a.status_aktif = 't' "
						.$pencarian);
						$hasilrow = $query2->row();
						$tot_pembelian	= $hasilrow->tot_pembelian;
						if ($tot_pembelian == '')
							$tot_pembelian = 0;
						}
						elseif($jenis_sup == '4'){
						$query2	= $this->db->query(" SELECT SUM(a.total) as tot_pembelian FROM tm_pembelian_bordir a INNER JOIN tm_supplier b ON a.id_supplier = b.id
						WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
						AND b.id = '$row1->id'  AND a.status_aktif = 't' "
						.$pencarian);
						$hasilrow = $query2->row();
						$tot_pembelian	= $hasilrow->tot_pembelian;
						if ($tot_pembelian == '')
							$tot_pembelian = 0;
						}
					}	
				}
				else {
					if ($kategori_sup == '1') {
						$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_pembelian FROM tm_pembelian_nofaktur a INNER JOIN tm_supplier b ON a.id_supplier = b.id
						WHERE a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy') 
						AND b.id = '$row1->id'"
						.$pencarian
						);
						$hasilrow = $query2->row();
						$tot_pembelian	= $hasilrow->tot_pembelian;
						if ($tot_pembelian == '')
							$tot_pembelian = 0;
					}
					else {
						$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_pembelian FROM tm_pembelian_makloon_faktur a INNER JOIN tm_supplier b ON a.id_supplier = b.id
						WHERE a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy') 
						AND b.id = '$row1->id' "
						.$pencarian
						);
						$hasilrow = $query2->row();
						$tot_pembelian	= $hasilrow->tot_pembelian;
						if ($tot_pembelian == '')
							$tot_pembelian = 0;
					}
				}
				
				// 29 nov 2011. 30-06-2015 GA DIPAKE
				/*$query2	= $this->db->query(" SELECT SUM(a.jumlah) as tot_pembelian FROM tm_faktur_makloon a, tm_supplier b 
				WHERE a.kode_unit = b.kode_supplier
				AND a.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_faktur <= to_date('$date_to','dd-mm-yyyy') 
				AND b.kode_supplier = '$row1->kode_supplier' AND a.jenis_pembelian = '$jenis_beli' AND a.jenis_makloon = '1' ");
				$hasilrow = $query2->row();
				$tot_pembelian_quilting	= $hasilrow->tot_pembelian;
				if ($tot_pembelian_quilting == '')
					$tot_pembelian_quilting = 0;
				$tot_pembelian += $tot_pembelian_quilting; */
				
				// 6. hitung saldo akhir
				//$saldo_akhir = $tot_hutang+$uang_muka+$tot_bayar-$tot_pembelian;
				// 30-06-2015 tanpa uang muka
				//$saldo_akhir = $tot_hutang-$uang_muka-$tot_bayar+$tot_bulat-$tot_retur+$tot_pembelian;
				//$tot_bayar+=$tot_cndn;
				$saldo_akhir = $tot_hutang+$tot_pembelian-$tot_retur-$tot_bayar-$tot_cndn+$tot_bulat;
					//$saldo_akhir = $tot_hutang+$tot_pembelian-$tot_retur-$tot_bayar+$tot_bulat;				
				if ($saldo_akhir == '')
					$saldo_akhir = 0;
				
				// ==========================================================================
				$tampilkan = 0;
				// 27-08-2015 cek suppliernya sesuai algoritma diatas
				if ($kategori_sup == 1){
					$tabel_pembelian = "tm_pembelian";
				}
				else if ($kategori_sup == 2){
					if($jenis_sup == 2){
					$tabel_pembelian = "tm_pembelian_makloon";
				}
				elseif($jenis_sup == 3){
					$tabel_pembelian = "tm_pembelian_aplikasi";
				}
				elseif($jenis_sup == 4){
					$tabel_pembelian = "tm_pembelian_bordir";
				}
			}
					$sqlxx = " SELECT a.id FROM tm_supplier b INNER JOIN ".$tabel_pembelian." a ON a.id_supplier = b.id
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') 
							 AND b.id = '$row1->id' AND a.status_aktif = 't' "
							 .$pencarian;
							 
							
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$tampilkan = 1;
				}
				else {
					if ($tot_hutang > 0)
						$tampilkan = 1;
				}
				
				if ($tampilkan == 1) {
					$data_beli[] = array(		'id_supplier'=> $row1->id,
												'kode_supplier'=> $row1->kode_supplier,
												'nama_supplier'=> $row1->nama,
												'kategori_sup'=> $kategori_sup,
												'tot_hutang'=> $tot_hutang,
												//'tot_debet'=> $uang_muka+$tot_bayar,
												'tot_cndn'=> $tot_cndn,
												'tot_debet'=> $tot_bayar,
												'tot_pembelian'=> $tot_pembelian,
												'tot_bulat'=> $tot_bulat,
												'tot_retur'=> $tot_retur,
												'saldo_akhir'=> $saldo_akhir
												);
				}
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  /*function get_all_rekap_pembeliantanpalimit($jenis_beli, $date_from, $date_to, $jenisdata){
		$query	= $this->db->query(" SELECT id, kode_supplier, nama FROM tm_supplier ");
	    
    return $query->result();  
  } */
          
}

