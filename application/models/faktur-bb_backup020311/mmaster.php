<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $supplier, $cari) {
	/*if ($cari == '') {		
		$this->db->select(" * FROM tm_pp WHERE jenis_brg = '$jenis_brg' AND status_faktur = 'f' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	} */ 
	  
	if ($cari == '') {
		if ($supplier == '0') {
			$this->db->select(" a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' AND a.kode_supplier = '$supplier' ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier == '0') {
			$this->db->select(" a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' 
						AND (UPPER(a.no_sj) like UPPER('%$cari%') OR UPPER(b.no_op) LIKE UPPER('%$cari%') ) ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' 
						AND a.kode_supplier = '$supplier' AND (UPPER(a.no_sj) like UPPER('%$cari%') OR UPPER(b.no_op) LIKE UPPER('%$cari%') ) ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.satuan FROM tm_barang a, tm_satuan b 
						WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->satuan;
						}
						else {
							$nama_brg = '';
							$satuan = '';
						}
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$nama_supplier	= $hasilrow->nama;
				//ambil id_pp dari tabel tm_op
				$query3	= $this->db->query(" SELECT id_pp, no_op FROM tm_op WHERE id = '$row1->id_op' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_pp	= $hasilrow->id_pp;
					$no_op	= $hasilrow->no_op;
				}
				else {
					$id_pp	= '0';
					$no_op	= '';
				}
				
				$query3	= $this->db->query(" SELECT no_pp FROM tm_pp WHERE id = '$id_pp' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$no_pp	= $hasilrow->no_pp;
				}
				else 
					$no_pp = '';
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_pp'=> $no_pp,	
											'no_op'=> $no_op,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'total'=> $row1->total,
											'no_po'=> $row1->no_po,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'jenis_brg'=> $row1->jenis_brg,
											'kode_bagian'=> $row1->kode_bagian,
											'tgl_update'=> $row1->tgl_update,
											'status_lunas'=> $row1->status_lunas,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == '') {
		if ($supplier == '0') {
			$query	= $this->db->query(" SELECT a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' ");
		}
		else
			$query	= $this->db->query(" SELECT a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' 
							AND a.kode_supplier = '$supplier' ");
	}
	else {
		if ($supplier == '0') {
			$query	= $this->db->query(" a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' 
						AND (UPPER(a.no_sj) like UPPER('%$cari%') OR UPPER(b.no_op) LIKE UPPER('%$cari%') ) ");
		}
		else
			$query	= $this->db->query(" a.* FROM tm_pembelian a, tm_op b where a.id_op = b.id AND b.id <> '0' 
						AND a.kode_supplier = '$supplier' AND (UPPER(a.no_sj) like UPPER('%$cari%') OR UPPER(b.no_op) LIKE UPPER('%$cari%') ) ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_sj){
    $this->db->select("id from tm_pembelian WHERE no_sj = '$no_sj' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_sj,$tgl_sj,$id_op,$jenis_brg, $kode_supplier,$gtotal,$total_pajak,$uang_muka, $sisa_hutang,$ket,
				$hide_pkp, $hide_tipe_pajak, $id_op_detail, $kode, $nama, $qty, $harga, $pajak, $diskon, $total){  
    $tgl = date("Y-m-d");
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pembelian WHERE no_sj = '$no_sj' AND jenis_brg = '$jenis_brg'", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_pembelian
			if ($sisa_hutang == 0) {
				$status_lunas = 1;
			}
			else {
				$status_lunas = 0;
			}
			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'id_op'=>$id_op,
			  'jenis_brg'=>$jenis_brg,
			  'kode_supplier'=>$kode_supplier,
			  'total'=>$gtotal,
			  'uang_muka'=>$uang_muka,
			  'sisa_hutang'=>$sisa_hutang,
			  'keterangan'=>$ket,
			  'pkp'=>$hide_pkp,
			  'tipe_pajak'=>$hide_tipe_pajak,
			  'total_pajak'=>$total_pajak,
			  'status_lunas'=>$status_lunas,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			$this->db->insert('tm_pembelian',$data_header);
			
			// ambil data terakhir di tabel tm_pembelian
			$query2	= $this->db->query(" SELECT id FROM tm_pembelian ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pembelian	= $hasilrow->id; //echo $idnya; die();
			
			// simpan di tabel tt_pajak jika no_fp ada datanya
		/*	if ($no_fp != '' && $tgl_fp!='') {
				$data_pajak = array(
					'id_pembelian'=>$id_pembelian,
					'no_faktur_pajak'=>$no_fp,
					'tgl_faktur_pajak'=>$tgl_fp,
					'jumlah'=>$total_pajak,
					'tgl_input'=>$tgl
					);
				$this->db->insert('tt_pajak',$data_pajak);
			} */
			
			if ($kode!='' && $qty!='' && $harga!='' && $total!='') {
				// jika semua data tdk kosong, insert ke tm_pembelian_detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'harga'=>$harga,
					'pajak'=>$pajak,
					'diskon'=>$diskon,
					'total'=>$total,
				  'id_pembelian'=>$id_pembelian
				);
				$this->db->insert('tm_pembelian_detail',$data_detail);
				
				$data_harga = array(
					'kode_brg'=>$kode,
					'kode_supplier'=>$kode_supplier,
					'jenis_brg'=>$jenis_brg,
					'harga'=>$harga,
					'tgl_input'=>$tgl
				);
				$this->db->insert('tt_harga', $data_harga);
				
				// insert/update ke tabel stok (tt_stok dan tm_stok)
				
				//1. ambil data gudang
				if ($jenis_brg == '1') {
					$nama_gudang = "Gudang Bahan Baku";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_baku";
				}
				else if ($jenis_brg == '2') {
					$nama_gudang = "Gudang Accessories";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_asesoris";
				}
				else if ($jenis_brg == '3') {
					$nama_gudang = "Gudang Bahan Pendukung";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_pendukung";
				}
				else
					$nama_tabel = "tm_perlengkapan";
				
				if ($jenis_brg != '4') {
					$query3	= $this->db->query(" SELECT id FROM tm_gudang WHERE nama = '$nama_gudang' AND kode_lokasi = '$lokasi'");
					$hasilrow = $query3->row();
					$id_gudang	= $hasilrow->id;
				}
				else
					$id_gudang = 0;
					
				//2. cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' AND jenis_brg = '$jenis_brg' 
										AND id_gudang = '$id_gudang'");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama+$qty;
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode,
						'jenis_brg'=>$jenis_brg,
						'stok'=>$new_stok,
						'id_gudang'=>$id_gudang, //
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl_update_stok' 
					where kode_brg= '$kode' AND jenis_brg='$jenis_brg' AND id_gudang = '$id_gudang'  ");
				}
				
				//3. insert di tt_stok 
				$data_trx_stok = array(
						'kode_brg'=>$kode,
						'jenis_brg'=>$jenis_brg,
						'no_bukti'=>$no_sj,
						'masuk'=>$qty,
						'saldo'=>$new_stok,
						'id_gudang'=>$id_gudang, //
						'tgl_input'=>$tgl,
						'harga'=>$harga
					);
				$this->db->insert('tt_stok',$data_trx_stok);
				
				//4. update harga di tabel master brg
				$this->db->query(" UPDATE ".$nama_tabel." SET harga = '$harga' where kode_brg= '$kode' ");
			}
			
			$this->db->query(" UPDATE tm_op_detail SET status_op = 't' where id= '$id_op_detail' ");
			//cek di tabel tm_op_detail, apakah status_op sudah 't' semua?
			$this->db->select("id from tm_op_detail WHERE status_op = 'f' AND id_op = '$id_op' ", false);
			$query = $this->db->get();
			//jika sudah t semua, maka update tabel tm_op di field status_op menjadi t
			if ($query->num_rows() == 0){
				$this->db->query(" UPDATE tm_op SET status_op = 't' where id= '$id_op' ");
			}
			//update pkp dan tipe_pajak di tabel supplier
			$this->db->query(" UPDATE tm_supplier SET pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak' where kode_supplier= '$kode_supplier' ");

		}
		else {
			// ambil data terakhir di tabel tm_pembelian
			$query2	= $this->db->query(" SELECT id FROM tm_pembelian ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pembelian	= $hasilrow->id;
			
			if ($kode!='' && $qty!='' && $harga!='' && $total!='') {
				// jika semua data tdk kosong, insert ke tm_pembelian_detail
				$data_detail = array(
					'kode_brg'=>$kode,
				'qty'=>$qty,
				'harga'=>$harga,
				'pajak'=>$pajak,
				'diskon'=>$diskon,
				'total'=>$total,
				  'id_pembelian'=>$id_pembelian
				);
				$this->db->insert('tm_pembelian_detail',$data_detail);
				
				// insert ke tabel tt_harga_
				$data_harga = array(
					'kode_brg'=>$kode,
					'kode_supplier'=>$kode_supplier,
					'jenis_brg'=>$jenis_brg,
					'harga'=>$harga,
					'tgl_input'=>$tgl
				);
				$this->db->insert('tt_harga', $data_harga);
				
				// insert/update ke tabel stok (tt_stok dan tm_stok)
				
				//1. ambil data gudang
				if ($jenis_brg == '1') {
					$nama_gudang = "Gudang Bahan Baku";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_baku";
				}
				else if ($jenis_brg == '2') {
					$nama_gudang = "Gudang Accessories";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_asesoris";
				}
				else if ($jenis_brg == '3') {
					$nama_gudang = "Gudang Bahan Pendukung";
					$lokasi = "01"; // duta
					$nama_tabel = "tm_bahan_pendukung";
				}
				else
					$nama_tabel = "tm_perlengkapan";
				
				if ($jenis_brg != '4') {
					$query3	= $this->db->query(" SELECT id FROM tm_gudang WHERE nama = '$nama_gudang' AND kode_lokasi = '$lokasi'");
					$hasilrow = $query3->row();
					$id_gudang	= $hasilrow->id;
				}
				else
					$id_gudang = 0;
					
				//2. cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' AND jenis_brg = '$jenis_brg' 
										AND id_gudang = '$id_gudang'");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = $stok_lama+$qty;
				
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
					$data_stok = array(
						'kode_brg'=>$kode,
						'jenis_brg'=>$jenis_brg,
						'stok'=>$new_stok,
						'id_gudang'=>$id_gudang, //
						'tgl_update_stok'=>$tgl
						);
					$this->db->insert('tm_stok',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok SET stok = '$new_stok' where kode_brg= '$kode' AND jenis_brg='$jenis_brg' 
									AND id_gudang = '$id_gudang'");
				}
				
				//3. insert di tt_stok 
				$data_trx_stok = array(
						'kode_brg'=>$kode,
						'jenis_brg'=>$jenis_brg,
						'no_bukti'=>$no_sj,
						'masuk'=>$qty,
						'saldo'=>$new_stok,
						'id_gudang'=>$id_gudang, //
						'tgl_input'=>$tgl,
						'harga'=>$harga
					);
				$this->db->insert('tt_stok',$data_trx_stok);
				
				//4. update harga di tabel master brg
				$this->db->query(" UPDATE ".$nama_tabel." SET harga = '$harga' where kode_brg= '$kode' ");
			}
			$this->db->query(" UPDATE tm_op_detail SET status_op = 't' where id= '$id_op_detail' ");
			//cek di tabel tm_op_detail, apakah status_op sudah 't' semua?
			$this->db->select("id from tm_op_detail WHERE status_op = 'f' AND id_op = '$id_op' ", false);
			$query = $this->db->get();
			//jika sudah t semua, maka update tabel tm_op di field status_op menjadi t
			if ($query->num_rows() == 0){
				$this->db->query(" UPDATE tm_op SET status_op = 't' where id= '$id_op' ");
			}
		}
  }
    
  function delete($kode){    
	$query2	= $this->db->query(" SELECT id_op FROM tm_pembelian WHERE id = '$kode' ");
	$hasilrow = $query2->row();
	$id_op	= $hasilrow->id_op;  
	  
	//update status_op di tabel tm_op_detail dan tm_op menjadi FALSE semua
	$this->db->query(" UPDATE tm_op_detail SET status_op = 'f' where id_op= '$id_op' ");
	$this->db->query(" UPDATE tm_op SET status_op = 'f' where id= '$id_op' ");
	
	$this->db->delete('tt_pajak', array('id_pembelian' => $kode));
    $this->db->delete('tm_pembelian_detail', array('id_pembelian' => $kode));
    $this->db->delete('tm_pembelian', array('id' => $kode));
  }
  
 /* function get_bahan_bakutanpalimit($cari){
	if ($cari == '') {
		$this->db->select('*');
		$this->db->from('tm_bahan_baku');
	}
	else {
		$this->db->select("* from tm_bahan_baku where kode_brg like '%$cari%' OR nama_brg like '%$cari%'", false);
	}
    $query = $this->db->get();
    
    return $query->result();  
  } */
  
  function get_op($num, $offset, $jenis_brg, $cari) {
	  // ambil data OP yg status_op = FALSE
	if ($cari == '') {		
		$this->db->select(" * FROM tm_op WHERE jenis_brg = '$jenis_brg' AND status_op = 'f' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_op WHERE jenis_brg = '$jenis_brg' AND status_op = 'f' AND no_op like '%$cari%' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_op = array();
		$detail_op = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
						if ($row1->jenis_brg == '1')
							$nama_tabel = "tm_bahan_baku";
						else if ($row1->jenis_brg == '2')
							$nama_tabel = "tm_asesoris";
						else if ($row1->jenis_brg == '3')
							$nama_tabel = "tm_bahan_pendukung";
						else if ($row1->jenis_brg == '4')
							$nama_tabel = "tm_perlengkapan";
				
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_op_detail WHERE id_op = '$row1->id' AND status_op = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT nama_brg, satuan FROM ".$nama_tabel." WHERE kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->satuan;
				
						$detail_op[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan
											);
					}
				}
				else {
					$detail_op = '';
				}
				
				$data_op[] = array(			'id'=> $row1->id,	
											'no_op'=> $row1->no_op,
											'tgl_op'=> $row1->tgl_op,
											'jenis_brg'=> $row1->jenis_brg,
											'kode_bagian'=> $row1->kode_bagian,
											'tgl_update'=> $row1->tgl_update,
											'detail_op'=> $detail_op
											);
				$detail_op = array();
			} // endforeach header
		}
		else {
			$data_op = '';
		}
		return $data_op;
  }
  
  function get_optanpalimit($jenis_brg, $cari){
	if ($cari == '') {
		$query = $this->db->getwhere('tm_op',array('jenis_brg'=>$jenis_brg,
													'status_op'=>"f"));
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_op WHERE jenis_brg = '$jenis_brg' AND status_op = 'f' AND no_op like '%$cari%' ");
	}
    
    return $query->result();  
  }
  
  function get_detail_op($id_op, $jenis_brg, $list_brg){
    //$query = $this->db->getwhere('tm_pp_detail',array('id_pp'=>$id_pp));
    //$hasil = $query->result();
    $detail_pp = array();
        
    foreach($list_brg as $row1) {
			//$row1 = intval($row1);
			if ($row1 != '') {
			$query2	= $this->db->query(" SELECT kode_brg, qty FROM tm_op_detail WHERE id = '$row1' ");
			$hasilrow = $query2->row();
			$kode_brg	= $hasilrow->kode_brg;
			$qty	= $hasilrow->qty;
    
    //foreach ($hasil as $row1) {
		/*	$query2	= $this->db->query(" SELECT nama_brg, harga FROM tm_bahan_baku WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query2->row();
			$nama_brg	= $hasilrow->nama_brg;
			$harga	= $hasilrow->harga; */
			
			if ($jenis_brg == '1')
				$nama_tabel = "tm_bahan_baku";
			else if ($jenis_brg == '2')
				$nama_tabel = "tm_asesoris";
			else if ($jenis_brg == '3')
				$nama_tabel = "tm_bahan_pendukung";
			else if ($jenis_brg == '4')
				$nama_tabel = "tm_perlengkapan";
			
			$query2	= $this->db->query(" SELECT nama_brg, harga FROM ".$nama_tabel." WHERE kode_brg = '$kode_brg' ");
			$hasilrow = $query2->row();
			$nama_brg	= $hasilrow->nama_brg;
			$harga	= $hasilrow->harga;
		
		/*	$detail_pp[] = array('kode_brg'=> $row1->kode_brg,
											'nama'=> $nama_brg,
											'harga'=> $harga,
											'qty'=> $row1->qty,
											'keterangan'=> $row1->keterangan
										); */
			$detail_op[] = array(		'id'=> $row1,
										'kode_brg'=> $kode_brg,
											'nama'=> $nama_brg,
											'harga'=> $harga,
											'qty'=> $qty
								);
		}
	}
	return $detail_op;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_pkp_tipe_pajak_bykodesup($kode_sup){
	$query	= $this->db->query(" SELECT pkp, tipe_pajak, top FROM tm_supplier where kode_supplier = '$kode_sup' ");    
    return $query->result();  
  }
  
  function get_pembelian($id_pembelian) {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian where id = '$id_pembelian' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				if ($row1->jenis_brg == 1) {
					$nama_jenis = "Bahan Baku";
					$nama_tabel = "tm_bahan_baku";
				}
				else if ($row1->jenis_brg == 2) {
					$nama_jenis = "Accessories";
					$nama_tabel = "tm_asesoris";
				}
				else if ($row1->jenis_brg == 3) {
					$nama_jenis = "Bahan Pendukung";
					$nama_tabel = "tm_bahan_pendukung";
				}
				else if ($row1->jenis_brg == 4) {
					$nama_jenis = "Alat Perlengkapan";
					$nama_tabel = "tm_perlengkapan";
				}
				
				//ambil id_pp dari tabel tm_op
				$query3	= $this->db->query(" SELECT id_pp, no_op FROM tm_op WHERE id = '$row1->id_op' ");
				$hasilrow = $query3->row();
				$id_pp	= $hasilrow->id_pp;
				$no_op	= $hasilrow->no_op;
				
				$query3	= $this->db->query(" SELECT no_pp FROM tm_pp WHERE id = '$id_pp' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$no_pp	= $hasilrow->no_pp;
				}
				else {
					$no_pp = '';
				}
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT nama_brg FROM ".$nama_tabel." WHERE kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'harga'=> $row2->harga,
												'qty'=> $row2->qty,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama, top FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$nama_supplier	= $hasilrow->nama;
						$top	= $hasilrow->top;
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->tgl_faktur != '') {
					$pisah2 = explode("-", $row1->tgl_faktur);
					$thn1= $pisah2[0];
					$bln1= $pisah2[1];
					$tgl1= $pisah2[2];
					$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
				} else $tgl_faktur = '';
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'jenis_brg'=> $row1->jenis_brg,
											'no_op'=> $no_op,	
											'no_pp'=> $no_pp,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'total'=> $row1->total,
											'no_po'=> $row1->no_po,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'nama_jenis'=> $nama_jenis,
											'kode_bagian'=> $row1->kode_bagian,
											'tgl_update'=> $row1->tgl_update,
											'status_lunas'=> $row1->status_lunas,
											'pkp'=> $row1->pkp,
											'tipe_pajak'=> $row1->tipe_pajak,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }  

}

