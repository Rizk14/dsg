<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE jenis = '2' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, tgl_so, jenis_perhitungan_stok, status_approve FROM tt_stok_opname_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
				
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
			$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
			$jenis_perhitungan_stok = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so,
							   'jenis_perhitungan_stok'=> $jenis_perhitungan_stok
							);
							
		return $so_bahan;
  }
  
  function get_all_stok_opname($bulan, $tahun, $gudang) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.kode_brg, d.nama_brg 
					FROM tt_stok_opname_hasil_jahit_detail b INNER JOIN tt_stok_opname_hasil_jahit a ON b.id_stok_opname_hasil_jahit = a.id
					INNER JOIN tm_barang_wip d ON b.id_brg_wip = d.id
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang='$gudang'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {
				// ---------- 04-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 21-10-2015 GA DIPAKE!
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a, 
									tt_stok_opname_hasil_jahit_detail b
									WHERE a.id = b.id_stok_opname_hasil_jahit
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				// 3. hitung keluar bln ini
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar; */
				//-------------------------------------------------------------------------------------
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_hasil_jahit_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_stok_opname_hasil_jahit_detail = '$row->id' 
							ORDER BY c.nama";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ---------- 04-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 21-10-2015 GA DIPAKE!
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
					/*	$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a 
											INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
											INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						// 3. hitung keluar bln ini
						// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna; */
						//-------------------------------------------------------------------------------------
						
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a INNER JOIN tm_stok_hasil_jahit_warna b ON a.id = b.id_stok_hasil_jahit
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_gudang = '$gudang'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'stok'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  } 
}
