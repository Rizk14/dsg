<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, tgl_so, status_approve FROM tt_stok_opname_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
				
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			// 04-12-2014
			$tgl_so = $hasilrow->tgl_so;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so
							);
							
		return $so_bahan;
  }
  
  function get_all_stok_opname($bulan, $tahun, $gudang) {

		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.e_product_motifname as nama_brg_jadi 
					FROM tt_stok_opname_hasil_jahit_detail b, tt_stok_opname_hasil_jahit a, tr_product_motif d
					WHERE b.id_stok_opname_hasil_jahit = a.id AND b.kode_brg_jadi = d.i_product_motif
					AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang='$gudang'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_hasil_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_hasil_jahit_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a, tm_stok_hasil_jahit_warna b
							WHERE a.id = b.id_stok_hasil_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
							AND b.kode_warna = '$rowxx->kode_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									'stok'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				// ambil nama brg jadi dan stok terkini
				/*$query3	= $this->db->query(" SELECT a.kode_brg_jadi, a.stok, c.e_product_motifname
					FROM tm_stok_hasil_jahit a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi
					AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang='$gudang' ");
					
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg_jadi	= '';
					$stok = '';
				} */
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->nama_brg_jadi,
										/*'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname */
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  } 
}
