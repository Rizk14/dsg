<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function getqtyperproduk($d_first,$d_last) {
		$db2=$this->load->database('db_external', TRUE);
		//=============================
		$sql=" select UPPER(b.i_product) AS imotif, b.e_product_name AS productmotif, sum(b.n_deliver) as qty
			FROM tm_do a, tm_do_item b  
			WHERE a.i_do = b.i_do AND a.f_do_cancel='f' ";
		if ($d_first != '' && $d_last!='')
			$sql.= " AND (a.d_do >='$d_first' AND a.d_do <='$d_last') ";
		
		$sql.= " GROUP BY b.i_product, b.e_product_name ORDER BY b.i_product ASC ";
		//=============================
				
		$query	= $db2->query($sql);
		$data_brg = array();
		
		if($query->num_rows()>0) {
			$hasil = $query->result();
			foreach ($hasil as $row) {
				
				//--------------06-09-2014 ----------------------------
				// ambil data qty warna dari tr_product_color dan tm_do_item_color
				$sqlxx	= $db2->query(" SELECT b.e_color_name, sum(c.qty) as jum FROM tr_color b, 
										tm_do_item_color c, tm_do d, tm_do_item e
										WHERE d.i_do = e.i_do AND c.i_do_item = e.i_do_item AND
										b.i_color=c.i_color 
										AND (d.d_do >='$d_first' AND d.d_do <='$d_last') AND 											e.i_product='$row->imotif'
										GROUP BY b.e_color_name ORDER BY b.e_color_name ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
							
					foreach ($hasilxx as $rownya) {
						$listwarna.= $rownya->e_color_name." : ".$rownya->jum."<br>";
					}
				}
				//-----------------------------------------------------
				
				$data_brg[] = array(		'imotif'=> $row->imotif,	
											'productmotif'=> $row->productmotif,	
											'qty'=> $row->qty,
											'listwarna'=> $listwarna
											);
			} // end foreach
		}
		else 
			$data_brg = '';
		return $data_brg;		
	}
	
}
?>
