<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_all_pembelian($jenis_beli, $date_from, $date_to) {
	  //modifikasi 18-02-2016 pencarian all
	  $pencarian='';
	  if ($jenis_beli !=0)
			$pencarian.="AND a.jenis_pembelian=$jenis_beli";
		//$pencarian.= "ORDER BY nama ASC";

	  // 30-06-2015 DIMODIF
	  /*UNION SELECT distinct a.kode_unit, b.nama FROM tm_sj_hasil_makloon a, tm_supplier b 
					WHERE a.kode_unit = b.kode_supplier 
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' */
					
	//18-02-2016 dimodif pencarian
	  $sql = " SELECT distinct a.id_supplier, b.kode_supplier, b.nama, b.pkp FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id
					WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					 AND a.status_aktif = 't' ".$pencarian." ORDER BY nama ASC ";
	  
		$query	= $this->db->query($sql);
		
		$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
								
				$query3	= $this->db->query(" SELECT sum(b.total) as jum_total FROM tm_pembelian a 
					INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
					INNER JOIN tm_barang g ON b.id_brg = g.id
					INNER JOIN tm_jenis_barang f ON f.id = g.id_jenis_barang 
					WHERE f.kode_kel_brg = 'B'
					AND a.id_supplier = '$row1->id_supplier'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_aktif = 't' ".$pencarian);
										
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_total_baku	= $hasilrow->jum_total;
				}
				else {
					$jum_total_baku = '';
				}
				// 29 nov 2011. 30-06-2015 GA DIPAKE
				/*$query3	= $this->db->query(" SELECT sum(b.biaya) as jum_total FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b 
					WHERE
					a.id = b.id_sj_hasil_makloon 
					AND a.kode_unit = '$row1->kode_supplier'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' ");
										
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_total_baku_quilting	= $hasilrow->jum_total;
				}
				else {
					$jum_total_baku_quilting = '';
				}
				$jum_total_baku+= $jum_total_baku_quilting; */
				
				/*$query3	= $this->db->query(" SELECT sum(b.total) as jum_total FROM tm_pembelian a, tm_pembelian_detail b, 
					tm_jenis_bahan e, tm_jenis_barang f, tm_barang g
					WHERE g.id_jenis_bahan = e.id AND e.id_jenis_barang = f.id AND f.kode_kel_brg = 'P' AND
					a.id = b.id_pembelian 
					AND b.kode_brg = g.kode_brg
					AND a.kode_supplier = '$row1->kode_supplier'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' "); */
					
				$query3	= $this->db->query(" SELECT sum(b.total) as jum_total FROM tm_pembelian a 
					INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
					INNER JOIN tm_barang g ON b.id_brg = g.id
					INNER JOIN tm_jenis_barang f ON f.id = g.id_jenis_barang 
					WHERE f.kode_kel_brg = 'P'
					AND a.id_supplier = '$row1->id_supplier'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_aktif = 't' ".$pencarian);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_total_pembantu	= $hasilrow->jum_total;
				}
				else {
					$jum_total_pembantu = '';
				}
				
				if ($row1->pkp == 't') {
					$dpp_baku = $jum_total_baku/1.1;
					$ppn_baku = $jum_total_baku/11;
					$dpp_pembantu = $jum_total_pembantu/1.1;
					$ppn_pembantu = $jum_total_pembantu/11;
				}
				else {
					$dpp_baku = 0;
					$ppn_baku = 0;
					$dpp_pembantu = 0;
					$ppn_pembantu = 0;
				}	
															
				$data_beli[] = array(		'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'pkp'=> $row1->pkp,
											'jum_total_baku'=> $jum_total_baku,
											'jum_total_pembantu'=> $jum_total_pembantu,
											'dpp_baku'=> $dpp_baku,
											'dpp_pembantu'=> $dpp_pembantu,
											'ppn_baku'=> $ppn_baku,
											'ppn_pembantu'=> $ppn_pembantu
											);
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
      
}

