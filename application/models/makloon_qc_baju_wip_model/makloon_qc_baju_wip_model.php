<?php
class Makloon_qc_baju_wip_model extends MY_Model
{
	protected $_per_page = 10;
    protected $_tabel = 'tb_makloon_qc_baju_wip';
    protected $form_rules = array(
        array(
            'field' => 'no_sj',
            'label' => 'Nomor SJ',
            'rules' => 'trim|required|max_length[64]'
        ),
        
         array(
            'field' => 'jenis_masuk',
            'label' => 'jenis_masuk',
            'rules' => 'trim|required|max_length[16]'
        ),
          array(
            'field' => 'tanggal_sj',
            'label' => 'Tanggal SJ',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'unit_packing',
            'label' => 'Unit Jahit',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'unit_packing',
            'label' => 'Unit Packing',
            'rules' => 'trim|required|max_length[16]'
        ),
          
         array(
            'field' => 'keterangan_header',
            'label' => 'Keterangan Header',
            'rules' => 'trim|required|max_length[16]'
        )
        
        
    );

     public $default_values = array(
		'id'	=>	'',
		'num'		=>1,
		'no_sj' => '',
        'tgl_sj' => '',
		'jenis_masuk' => '',
		'tanggal_sj'=>'',
		'keterangan_header'=>'',
    );
    
     public function input_header($no_sj,$jenis_masuk,  $id_unit_packing,$id_gudang,$keterangan,$tanggal_sj)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 $created_by = $updated_by = $this->session->userdata('pengguna_id');
		 
		$data= array(
		'no_sj' => $no_sj,
		'jenis_masuk' => $jenis_masuk,
		'id_unit_packing' => $id_unit_packing,
		'id_gudang' => $id_gudang,
		'created_at'=> $created_at,
		'updated_at'=> $updated_at,
		'created_by'=> $created_by,
		'updated_by'=> $updated_by,
		'keterangan'=> $keterangan,
		'tanggal_sj'=> $tanggal_sj,
		);
		
        $header= $this->db->insert('tb_makloon_qc_baju_wip',$data);
        
        $query = $this->db->query(" SELECT id from tb_makloon_qc_baju_wip order by id desc limit 1");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
    

    }
    
  public function input_detail($id_header,$id_barang_wip,$id_barang_bb,$qty,$keterangan,$id_unit_packing,$id_gudang)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 $created_by = $updated_by = $this->session->userdata('pengguna_id');
		$data= array(
		'id_makloon_qc_baju_wip' => $id_header,
		'id_barang_wip' => $id_barang_wip,
		'qty' => $qty,
		'created_at'=> $created_at,
		'updated_at'=> $updated_at,
		'created_by'=> $created_by,
		'updated_by'=> $updated_by,
		'keterangan'=> $keterangan
		);
        $detail= $this->db->insert('tb_makloon_qc_baju_wip_detail',$data);
        		$exid_barang_bb=explode(";",$id_barang_bb);
       $query = $this->db->query("SELECT id FROM tb_makloon_qc_baju_wip_detail order by id desc limit 1");
		if($query->num_rows() >0){
			$hasilrow=$query->result();
			foreach ($hasilrow as $row){
			$id_detail =$row->id;
					
			for($xx=0;$xx<count($exid_barang_bb)-1;$xx++){
				$tb_makloon_motif= array(
				'id_makloon_qc_baju_wip_detail'=>$id_detail,
				'id_makloon_qc_baju_wip'=>$id_header,
				'id_barang_bb'=>$exid_barang_bb[$xx],
				'qty' => $qty,	
				'updated_at'=>$updated_at,
				'updated_by'=>$updated_by
				);
			 $detail_motif= $this->db->insert('tb_makloon_qc_baju_wip_detail_motif',$tb_makloon_motif); 
			 
				
			$this->db->select("id,id_barang_bb,id_unit_packing,stok FROM tb_stok_unit_packing where id_unit_packing='$id_unit_packing' 
        AND id_barang_bb='$exid_barang_bb[$xx]'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$idxy = $hasilrow->id;
			$id_barang_bbxy=$hasilrow->id_barang_bb;
			$id_unit_packingxy=$hasilrow->id_unit_packing;
			$stokxy=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stokxy+$qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_unit_packing',$data_stok_update);	
        
	}
	else{
		$data_stok= array(
		'id_barang_bb' => $exid_barang_bb[$xx],
		'stok' => $qty,
		'id_unit_packing'=> $id_unit_packing,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at

		);
        $detail= $this->db->insert('tb_stok_unit_packing',$data_stok);		
		}
		
		$this->db->select("id,id_barang_bb,id_gudang,stok FROM tb_stok_gudang_jadi_2 where id_gudang='$id_gudang' 
        AND id_barang_bb='$exid_barang_bb[$xx]'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$idxy = $hasilrow->id;
			$id_barang_bbxy=$hasilrow->id_barang_bb;
			$id_gudangxy=$hasilrow->id_gudang;
			$stokxy=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stokxy-$qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_2',$data_stok_update);	
        
	}
	else{
		$data_stok= array(
		'id_barang_bb' => $exid_barang_bb[$xx],
		'stok' => -$qty,
		'id_gudang'=> $id_gudang,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at

		);
        $detail= $this->db->insert('tb_stok_gudang_jadi_2',$data_stok);		
		}
			
			}
		}
		 $this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok-$qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
        
	}
	else{
		$data_stok= array(
		'id_barang_wip' => $id_barang_wip,
		'stok' => -$qty,
		'id_gudang'=> $id_gudang,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at

		);
        $detail= $this->db->insert('tb_stok_gudang_jadi_1',$data_stok);		
		}
		
		 $this->db->select("id,id_barang_wip,id_unit_packing,stok FROM tb_stok_unit_packing_wip where id_unit_packing='$id_unit_packing' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_unit_packingx=$hasilrow->id_unit_packing;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok+$qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_unit_packing_wip',$data_stok_update);	
        
	}
	else{
		$data_stok= array(
		'id_barang_wip' => $id_barang_wip,
		'stok' => +$qty,
		'id_unit_packing'=> $id_unit_packing,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at

		);
        $detail= $this->db->insert('tb_stok_unit_packing_wip',$data_stok);		
		}
	}	
		 if($detail){
			return true;
			}
			else{
				return false;
			}
      
    }
 
     public function cari($offset)
    {
        $this->get_real_offset($offset);
        $kata_kunci = $this->input->get('kata_kunci', true);
       
        return $this->db->where("( no_sj LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->result();
    }
    
     
    
     public function cari_num_rows()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
          return $this->db->where("( no_sj LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->num_rows();
    }
    
    
    public function update_header($id,$no_sj,$tanggal_sj,$jenis_masuk,
		$id_gudang,$id_unit_packing,$keterangan_header)
    {
     $updated_at = date('Y-m-d H:i:s');
     $this->db->where('id', $id);
     $data= array(
		'no_sj' => $no_sj,
		'tanggal_sj' => $tanggal_sj,
		'jenis_masuk' => $jenis_masuk,
		'id_unit_packing' => $id_unit_packing,
		'id_gudang' => $id_gudang,
		
		'keterangan' => $keterangan_header,
		'status_edit' => "t",
		'updated_at'=> $updated_at,
		);
    $this->db->update('tb_makloon_qc_baju_wip', $data);
    
    $query = $this->db->query(" SELECT id from tb_makloon_qc_baju_wip_detail where id_makloon_qc_baju_wip='$id' order by id desc limit 1");
    
    return $query->result();
   
    }
    
    public function update_detail_motif($id_detail_motif,$qty,$id_unit_packing,$id_gudang,$qty_lama,$id_barang_bb,$id_detail,$jum_data_detail)
    {
	$created_by = $updated_by = $this->session->userdata('pengguna_id');
     $updated_at = date('Y-m-d H:i:s');		

     for($l=0;$l<=count($id_barang_bb);$l++){
			$this->db->where('id_makloon_qc_baju_wip_detail', $id_detail);
				$tb_makloon_motif= array(
				'qty'=> $qty,
				'updated_at'=>$updated_at,
				'updated_by'=>$updated_by
				);
			 $detail_motif= $this->db->update('tb_makloon_qc_baju_wip_detail_motif',$tb_makloon_motif); 
	 
	 $update_stok=$qty-$qty_lama;
	
	if($id_barang_bb[$l] !=''){
			 $this->db->select("id,id_barang_bb,id_gudang,stok FROM tb_stok_gudang_jadi_2 where id_gudang='$id_gudang' 
        AND id_barang_bb='$id_barang_bb[$l]'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id_ujh = $hasilrow->id;
			$id_barang_bbx=$hasilrow->id_barang_bb;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update_gudang= array(
		'stok' => $stok-$update_stok,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_ujh);
        $detail= $this->db->update('tb_stok_gudang_jadi_2',$data_stok_update_gudang);	       
        
 }
	 $this->db->select("id,id_barang_bb,id_unit_packing,stok FROM tb_stok_unit_packing where id_unit_packing='$id_unit_packing' 
        AND id_barang_bb='$id_barang_bb[$l]'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id_ujh = $hasilrow->id;
			$id_barang_bbx=$hasilrow->id_barang_bb;
			$id_unit_packingx=$hasilrow->id_unit_packing;
			$stok=$hasilrow->stok;
			
		$data_stok_update_packing= array(
		'stok' => $stok+$update_stok,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_ujh);
        $detail= $this->db->update('tb_stok_unit_packing',$data_stok_update_packing);
        
        }
			}
		} 
    }
    
    public function update_detail($id_unit_packing,$id_header,$id_barang_wip,$qty,$qty_lama,$keterangan_detail,$id_detail,$id_gudang)
    {
   $updated_at = date('Y-m-d H:i:s');
    $created_by = $updated_by = $this->session->userdata('pengguna_id');
     $this->db->where('id', $id_detail);
     $data= array(
		'id_barang_wip' => $id_barang_wip,
		'qty'=> $qty,
		'keterangan' => $keterangan_detail,
		'updated_at'=> $updated_at,
		);
		$this->db->update('tb_makloon_qc_baju_wip_detail', $data);
			
		$update_stok=$qty-$qty_lama;
		
		 $this->db->select("id,id_barang_wip,id_unit_packing,stok FROM tb_stok_unit_packing_wip where id_unit_packing='$id_unit_packing' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id_h = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_unit_packingx=$hasilrow->id_unit_packing;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok+$update_stok,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_h);
        $detail= $this->db->update('tb_stok_unit_packing_wip',$data_stok_update);	
        
	}
	
	$this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id_h = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok-$update_stok,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_h);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
        
	}
    return true;
    }

 public function get_unit_jahit()
    {
     $sql=$this->db->query("SELECT id,nama_unit_jahit FROM tb_master_unit_jahit order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }    
    public function get_unit_packing()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_unit_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }  
    public function get_gudang()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_gudang order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }
    
     
    
     public function get_all_inner_paged($offset)
    {
     $select=$this->db->select("* from tb_makloon_qc_baju_wip where status_aktif='t' order by updated_at")->limit($this->_per_page,$offset);  
            $select=$this->db->get();
     if($select->num_rows() > 0){
		 return $select->result();
		 }
    }
    
  function paging_barang($tipe, $base_url, $uri_segment)
    {
        // Memanggil library pagination.
        $this->load->library('pagination');

        // Konfigurasi.
        $config = array(
            'base_url' => $base_url,
            'uri_segment' => $uri_segment,
            'per_page' => $this->_per_page,
            'use_page_numbers' => true,
            'num_links' => 4,
            'first_link' => '&#124;&lt; First',
            'last_link' => 'Last &gt;&#124;',
            'next_link' => 'Next &gt;',
            'prev_link' => '&lt; Prev',

            // Menyesuaikan untuk Twitter Bootstrap 3.2.0.
            'full_tag_open' => '<ul class="pagination pagination-sm">',
            'full_tag_close' => '</ul>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li class="disabled"><li class="active"><a href="#">',
            'cur_tag_close' => '<span class="sr-only"></span></a></li>',
            'next_tag_open' => '<li>',
            'next_tagl_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tagl_close' => '</li>',
            'first_tag_open' => '<li>',
            'first_tagl_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tagl_close' => '</li>',
        );

        // Jika paging digunakan untuk "pencarian", tambahkan / tampilkan $_GET di URL.
        // Caranya dengan memanipulasi $config['suffix'].
        if ($tipe == 'pencarian') {
            if (count($_GET) > 0) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['total_rows'] = $this->cari_num_rows_barang();
        } else {
            $config['first_url'] = '1';
            $config['total_rows'] = $this->get_all_num_rows_barang();
        }

        // Set konfigurasi.
        $this->pagination->initialize($config);

        // Buat link dan kembalikan link paging yang sudah jadi.
        return $this->pagination->create_links();
    }
   
    
    public function get_all_num_rows_barang()
    {
		$this->db->where('status_aktif','t');
        return $this->db->get('tb_makloon_qc_baju_wip')->num_rows();
    }
    
    
     public function delete_aktif($id,$id_unit_packing,$id_gudang)
    {
		$updated_at =$deleted_at= date('Y-m-d H:i:s');
		$updated_by=$deleted_by=$this->session->userdata('pengguna_id');
	$data = array(
               'status_aktif' => "f",
               'deleted_by'=>$deleted_by,
               'deleted_at'=>$deleted_at
            );
            
    $this->db->where('id', $id);
	$this->db->update('tb_makloon_qc_baju_wip', $data); 
	
	$query=$this->db->query("SELECT * FROM tb_makloon_qc_baju_wip_detail_motif where id_makloon_qc_baju_wip='$id'");
	if($query->num_rows() > 0){
		$hasil4=$query->result();
		foreach ($hasil4 as $row4){
		$this->db->select("id,id_barang_bb,id_gudang,stok FROM tb_stok_gudang_jadi_2 where id_gudang='$id_gudang' 
        AND id_barang_bb='$row4->id_barang_bb'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id_h = $hasilrow->id;
			$id_barang_bbx=$hasilrow->id_barang_bb;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok+$row4->qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_h);
        $detail= $this->db->update('tb_stok_gudang_jadi_2',$data_stok_update);	
			}
		$this->db->select("id,id_barang_bb,id_unit_packing,stok FROM tb_stok_unit_packing where id_unit_packing='$id_unit_packing' 
        AND id_barang_bb='$row4->id_barang_bb'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id_j = $hasilrow->id;
			$id_barang_bbx=$hasilrow->id_barang_bb;
			$id_unit_packingx=$hasilrow->id_unit_packing;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok-$row4->qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_j);
        $detail= $this->db->update('tb_stok_unit_packing',$data_stok_update);	
			}
		
		}
				  
		
	}
	
	$query2=$this->db->query("SELECT * FROM tb_makloon_qc_baju_wip_detail where id_makloon_qc_baju_wip='$id'");
	if($query2->num_rows() > 0){
		$hasil5=$query2->result();
		foreach ($hasil5 as $row5){
		
		$this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang' 
        AND id_barang_wip='$row5->id_barang_wip'");
        $stok= $this->db->get();
        if($stok->num_rows() > 0){
			$hasilrow=$stok->row();
			$id_k = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok+$row5->qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_k);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
			}
		$this->db->select("id,id_barang_wip,id_unit_packing,stok FROM tb_stok_unit_packing_wip where id_unit_packing='$id_unit_packing' 
        AND id_barang_wip='$row5->id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id_l = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_unit_packingx=$hasilrow->id_unit_packing;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok-$row5->qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id_l);
        $detail= $this->db->update('tb_stok_unit_packing_wip',$data_stok_update);	
			}
		
		}
				  
		$this->db->where('id_makloon_qc_baju_wip', $id);
		$this->db->update('tb_makloon_qc_baju_wip_detail', $data); 
	}

	return true;	    
    }
    
         
         public function getAllDetail($id)
    {
      $query =$this->db->query("SELECT * FROM tb_makloon_qc_baju_wip where id ='$id'");
	
	if($query->num_rows > 0){
		$hasil = $query->result();
		foreach($hasil as $row){
			
			$query2 =$this->db->query("SELECT id_barang_wip,qty,keterangan ,id
			from tb_makloon_qc_baju_wip_detail where id_makloon_qc_baju_wip ='$row->id'");
			if($query2->num_rows > 0){
				$hasil2 =$query2->result();
					foreach($hasil2 as $row2){
						
						$query5 =$this->db->query("SELECT  nama_barang_wip,kode_barang_wip from tb_master_barang_wip where id ='$row2->id_barang_wip'");	
											if($query5->num_rows() > 0){
											$hasil5=$query5->row();
					
					}
						$query3 =$this->db->query("SELECT id,id_barang_bb,qty
						from tb_makloon_qc_baju_wip_detail_motif where id_makloon_qc_baju_wip_detail ='$row2->id'");
							if($query3->num_rows > 0){
							$hasil3 =$query3->result();
									foreach ($hasil3 as $row3){
										
											$query4 =$this->db->query("SELECT  nama_barang_bb from tb_master_barang_bb where id ='$row3->id_barang_bb'");	
											if($query4->num_rows() > 0){
											$hasil4=$query4->row();
												
					}
			
												$detail_motif_data[]=array(
											'nama_barang_bb'  =>$hasil4->nama_barang_bb,
											'id_barang_bb'  =>$row3->id_barang_bb,
											'id_detail_motif' =>$row3->id,
											'qty_detail_motif'=>$row3->qty
											);
						
				}
		
			}	
			$detail_data[]=array(
											'nama_barang_wip'  =>$hasil5->nama_barang_wip,
											'kode_barang_wip'  =>$hasil5->kode_barang_wip,
											'id_barang_wip' =>$row2->id_barang_wip,
											'keterangan_detail' =>$row2->keterangan,
											'qty' =>$row2->qty,
											'id_detail' =>$row2->id,
											'detail_motif_data'=>$detail_motif_data
											);				
												$detail_motif_data=array();															
		}			
	}
	
			$header_data[]=array(
			'id'=>$row->id,
			'no_sj'=>$row->no_sj,
			'tanggal_sj'=>$row->tanggal_sj,
			'jenis_masuk'=>$row->jenis_masuk,
			
			'id_gudang'=>$row->id_gudang,
			'id_unit_packing'=>$row->id_unit_packing,
			'keterangan_header'=>$row->keterangan,
			'detail_data'=>$detail_data
		);    
    }
  }
		else{
			$header_data[]=array();
			}
		return $header_data;    
    
}
    
     public function getAllDetail_old($id)
    {
      $query =$this->db->query("SELECT * FROM tb_makloon_qc_baju_wip where id='$id'");
	
	if($query->num_rows > 0){
		$hasil = $query->result();
		foreach($hasil as $row){
			
			$query2 =$this->db->query("SELECT id,id_barang_bb,qty,keterangan 
			from tb_makloon_qc_baju_wip_detail where id_makloon_qc_baju_wip ='$row->id'");
			
			if($query2->num_rows > 0){
				$hasil2 =$query2->row();
					$id_detail =$hasil2->id;
				$id_barang_bb =$hasil2->id_barang_bb;
				$qty =$hasil2->qty;
				$keterangan_detail=$hasil2->keterangan;
				}
				
				$query3=$this->db->query("SELECT nama_barang_bb,kode_barang_bb FROM tb_master_barang_bb 
				where id='$id_barang_bb'");
				if($query3->num_rows > 0){
					$hasil3 =$query3->row();
					$nama_barang_bb=$hasil3->nama_barang_bb;
					$kode_barang_bb=$hasil3->kode_barang_bb;
					}

		
			}
			$detail_data[]=array(
					
					'nama_barang_bb'=>$nama_barang_bb,
					'kode_barang_bb'=>$kode_barang_bb,
					'id_barang_bb'=>$id_barang_bb,
					'id_detail'=>$id_detail,
					'qty'=>$qty,
					'keterangan_detail'=>$keterangan_detail
				);
			
			$header_data[]=array(
			'id'=>$row->id,
			'no_sj'=>$row->no_sj,
			'tanggal_sj'=>$row->tanggal_sj,
			'jenis_masuk'=>$row->jenis_masuk,
			'id_unit_packing'=>$row->id_unit_packing,
			'id_gudang'=>$row->id_gudang,
			'keterangan_header'=>$row->keterangan,
			'detail_data'=>$detail_data
		);

		
	    
    }
    else{
			$header_data[]=array();
			}
		return $header_data;    
    
}
}
