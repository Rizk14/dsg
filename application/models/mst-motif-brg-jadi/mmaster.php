<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_motif_brg_jadi');
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_motif_brg_jadi',array('id'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_motif_brg_jadi WHERE id = '$id' ");
    return $query->result();		  
  }
  
  //
  function save($id_bagian, $nama, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'nama_motif'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_motif_brg_jadi',$data); }
	else {
		
		$data = array(
		  'nama_motif'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_bagian);
		$this->db->update('tm_motif_brg_jadi',$data);  
	}
		
  }
  
  function delete($id){
    $this->db->delete('tm_motif_brg_jadi', array('id' => $id));
  }
  
  function cek_data($nama){
    $this->db->select("* FROM tm_motif_brg_jadi WHERE UPPER(nama_motif) = UPPER('$nama') ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}

