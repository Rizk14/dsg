<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_area');
    $this->db->order_by('kode_area','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_area',array('kode_area'=>$id));
    $query = $this->db->query(" SELECT * FROM tm_area WHERE id='$id' ");
    return $query->result();
  }
  
  
  function save($kode, $id_area, $lokasi,$nama,  $goedit)
  {  
    $tgl = date("Y-m-d H:i:s");
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_area ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
		
    $data = array(
	  'id'=>$idbaru,
      'kode_area'=>$kode,
      'lokasi'=>$lokasi,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
     
    );

    if ($goedit == '') {
		$this->db->insert('tm_area',$data); }
	else {
		
		$data = array(
		  'kode_area'=>$kode,
		  'lokasi'=>$lokasi,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
 
		);
		
		$this->db->where('id',$id_area);
		$this->db->update('tm_area',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_area', array('id' => $id));
  }
  /*
  
	
	function get_unit_jahit(){
		$query = $this->db->query(" SELECT * FROM tm_area ORDER BY kode_area");
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	
	
*/
}

