<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $date_from, $date_to, $unit_jahit) {	  
	  $sql = " distinct a.id, a.no_schedule, a.kode_unit, a.operator_jahit, a.tgl_update FROM tt_schedule_jahit a, tt_schedule_jahit_detail b WHERE a.id = b.id_schedule_jahit ";
				if ($date_from != "all")
					$sql.= " AND b.tgl_jahit >= to_date('$date_from','dd-mm-yyyy') ";
				if ($date_to != "all")
					$sql.= " AND b.tgl_jahit <= to_date('$date_to','dd-mm-yyyy') ";
				if ($unit_jahit != '0')
					$sql.= " AND a.kode_unit = '$unit_jahit' ";
			$sql.= " ORDER BY kode_unit";
	  
/*	  $sql = " * FROM tt_schedule_jahit ";
	  if ($unit_jahit != '0')
			$sql.= " WHERE kode_unit = '$unit_jahit' ";
	  $sql .= " ORDER BY kode_unit";	*/
	  
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();
			
		$data_jahit = array();
		$detail_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail schedulenya
				$sql2 = " SELECT * FROM tt_schedule_jahit_detail WHERE id_schedule_jahit = '$row1->id' ";
				if ($date_from != "all")
					$sql2.= " AND tgl_jahit >= to_date('$date_from','dd-mm-yyyy') ";
				if ($date_to != "all")
					$sql2.= " AND tgl_jahit <= to_date('$date_to','dd-mm-yyyy') ";
				
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
																	
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg = '';
						}
						
						$pisah1 = explode("-", $row2->tgl_jahit);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_jahit = $tgl1." ".$nama_bln." ".$thn1;
				
						$detail_jahit[] = array('kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg'=> $nama_brg,
												'tgl_jahit'=> $tgl_jahit,
												'qty'=> $row2->qty,
												'jam_kerja'=> $row2->jam_kerja,
												'keterangan'=> $row2->keterangan
											);
					}
				}
				else {
					$detail_jahit = '';
				}
				
				// ambil nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit	= $hasilrow->nama;
				}
				else {
					$nama_unit = '';
				}
				//if ($detail_jahit == '')
					$data_jahit[] = array(			'id'=> $row1->id,	
											'no_schedule'=> $row1->no_schedule,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'operator_jahit'=> $row1->operator_jahit,
											'tgl_update'=> $row1->tgl_update,
											'detail_jahit'=> $detail_jahit
											);
					$detail_jahit = array();
				
			} // endforeach header
		}
		else {
			$data_jahit = '';
		}
		return $data_jahit;
  }
  
  function getAlltanpalimit($date_from, $date_to, $unit_jahit){
	$sql = " SELECT distinct a.no_schedule FROM tt_schedule_jahit a, tt_schedule_jahit_detail b WHERE a.id = b.id_schedule_jahit ";
				if ($date_from != "all")
					$sql.= " AND b.tgl_jahit >= to_date('$date_from','dd-mm-yyyy') ";
				if ($date_to != "all")
					$sql.= " AND b.tgl_jahit <= to_date('$date_to','dd-mm-yyyy') ";
				if ($unit_jahit != '0')
					$sql.= " AND a.kode_unit = '$unit_jahit' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
      
  function cek_data($no_request, $jenis_bhn){
    $this->db->select("id from tm_pb_quilting WHERE no_pb_quilting = '$no_request' AND jenis_bahan = '$jenis_bhn' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_schedule,$unit_jahit, $operator_jahit, $tgl_jahit, $kode, $nama, $qty, $jam, $keterangan){  
    $tgl = date("Y-m-d");
    
    // cek apa udah ada datanya blm dgn no schedule tadi
    $this->db->select("id from tt_schedule_jahit WHERE no_schedule = '$no_schedule' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_pb_quilting
			$data_header = array(
			  'no_schedule'=>$no_schedule,
			  'kode_unit'=>$unit_jahit,
			  'operator_jahit'=>$operator_jahit,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			$this->db->insert('tt_schedule_jahit',$data_header);
			
			// ambil data terakhir di tabel tt_schedule_jahit
			$query2	= $this->db->query(" SELECT id FROM tt_schedule_jahit ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sc	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0) && ($jam!='' || $jam!=0) ) {
				// jika semua data tdk kosong, insert ke tt_schedule_jahit_detail
				$pisah1 = explode("-", $tgl_jahit);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				$tgl_jahit = $thn1."-".$bln1."-".$tgl1;
				
				$data_detail = array(
					'kode_brg_jadi'=>$kode,
					'tgl_jahit'=>$tgl_jahit,
					'qty'=>$qty,
					'jam_kerja'=>$jam,
					'keterangan'=>$keterangan,
					'id_schedule_jahit'=>$id_sc
				);
				$this->db->insert('tt_schedule_jahit_detail',$data_detail);
				
			}

		}
		else {
			// ambil data terakhir di tabel tt_schedule_jahit
			$query2	= $this->db->query(" SELECT id FROM tt_schedule_jahit ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sc	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0) && ($jam!='' || $jam!=0) ) {
				// jika semua data tdk kosong, insert ke tt_schedule_jahit_detail
				$pisah1 = explode("-", $tgl_jahit);
				$tgl1= $pisah1[0];
				$bln1= $pisah1[1];
				$thn1= $pisah1[2];
				$tgl_jahit = $thn1."-".$bln1."-".$tgl1;
				
				$data_detail = array(
					'kode_brg_jadi'=>$kode,
					'tgl_jahit'=>$tgl_jahit,
					'qty'=>$qty,
					'jam_kerja'=>$jam,
					'keterangan'=>$keterangan,
					'id_schedule_jahit'=>$id_sc
				);
				$this->db->insert('tt_schedule_jahit_detail',$data_detail);
				
			}
		}
  }
    
  function delete($kode){    	
    $this->db->delete('tt_schedule_jahit_detail', array('id_schedule_jahit' => $kode));
    $this->db->delete('tt_schedule_jahit', array('id' => $kode));
  }
          
  function generate_nomor(){
    // generate no schedule
			$th_now	= date("Y");
			$query3	= $this->db->query(" SELECT no_schedule FROM tt_schedule_jahit ORDER BY no_schedule DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_pb	= $hasilrow->no_schedule;
			else
				$no_pb = '';
			if(strlen($no_pb)==12) {
				$nopb = substr($no_pb, 3, 9);
				$n_pb	= (substr($nopb,4,5))+1;
				$th_pb	= substr($nopb,0,4);
				if($th_now==$th_pb) {
						$jml_n_pb	= $n_pb;
						switch(strlen($jml_n_pb)) {
							case "1": $kodepb	= "0000".$jml_n_pb;
							break;
							case "2": $kodepb	= "000".$jml_n_pb;
							break;	
							case "3": $kodepb	= "00".$jml_n_pb;
							break;
							case "4": $kodepb	= "0".$jml_n_pb;
							break;
							case "5": $kodepb	= $jml_n_pb;
							break;	
						}
						$nomorpb = $th_now.$kodepb;
				}
				else {
					$nomorpb = $th_now."00001";
				}
			}
			else {
				$nomorpb	= $th_now."00001";
			}
			$nomorpb = "JS-".$nomorpb;

			return $nomorpb;  
  }
  
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_schedule_jahit($id_sc) {
		$query	= $this->db->query(" SELECT * FROM tt_schedule_jahit where id = '$id_sc' ");
	
		$data_pb = array();
		$detail_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tt_schedule_jahit_detail WHERE id_schedule_jahit = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->e_product_motifname;
										
						$pisah1 = explode("-", $row2->tgl_jahit);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_jahit = $tgl1."-".$bln1."-".$thn1;
										
						$detail_sc[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg'=> $nama_brg,
												'tgl_jahit'=> $tgl_jahit,
												'qty'=> $row2->qty,
												'jam_kerja'=> $row2->jam_kerja,
												'ket'=> $row2->keterangan
											);
					}
				}
				else {
					$detail_sc = '';
				}
																
				$data_sc[] = array(			'id'=> $row1->id,	
											'no_schedule'=> $row1->no_schedule,
											'kode_unit'=> $row1->kode_unit,
											'operator_jahit'=> $row1->operator_jahit,
											'detail_sc'=> $detail_sc
											);
				$detail_sc = array();
			} // endforeach header
		}
		else {
			$data_sc = '';
		}
		return $data_sc;
  }
  
  function get_unit_jahit(){
    $this->db->select(" * from tm_unit_jahit order by kode_unit ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }

}

