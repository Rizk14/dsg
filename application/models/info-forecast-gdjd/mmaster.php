<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang_jd(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
						WHERE a.jenis = '3' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_mutasi_stok_wip($date_from, $date_to, $gudang) {		
	  
	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	// explode date_from utk keperluan saldo awal 
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
			
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}
	  
	$sql =  "
			SELECT DISTINCT a.id_brg from tm_sj_drop_forecast_detail a 
			inner join tm_sj_drop_forecast b on a.id_sj_drop_forecast=b.id WHERE
			 b.tgl_sj >='".$tgldari."'AND b.tgl_sj <='".$tglke."' ORDER BY id_brg
				"; 
	//echo $sql; die();
	
		//AND a.status_approve='t'
		
		$query	= $this->db->query($sql);
		$jum_masuk = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
				
				$query3	= $this->db->query(" SELECT  sum(b.jum_forecast) as sa from tt_forecast_gudang_jd a inner join 
							tt_forecast_gudang_jd_detail b ON a.id=b.id_forecast_gudang_jd
							WHERE a.bulan ='$bln1' AND a.tahun ='$thn1' AND b.id_brg_wip = '$row1->id_brg' AND id_gudang='$gudang'
							  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$sa = $hasilrow->sa;
				}
				else
					$sa = 0;
				
				
				
				$query3	= $this->db->query(" select sum(b.qty) as jum_keluar from tm_sj_drop_forecast a 
							inner join tm_sj_drop_forecast_detail b on a.id=b.id_sj_drop_forecast
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				
				
				$sisa=$sa-$jum_keluar;
				
				//=========================================
				
				$query3	= $this->db->query("SELECT  c.id,c.id_warna from tt_forecast_gudang_jd a 
							inner join 
							tt_forecast_gudang_jd_detail b ON a.id=b.id_forecast_gudang_jd
							inner join 
							tt_forecast_gudang_jd_detail_warna c ON b.id=c.id_forecast_gudang_jd_detail
							WHERE a.bulan ='$bln1' AND a.tahun ='$thn1' AND b.id_brg_wip = '$row1->id_brg' AND id_gudang='$gudang'
							  ");
				if ($query3->num_rows() > 0){
					$hasilrow3 = $query3->result();
				//~ 
				foreach ($hasilrow3 as $row3){
					
					$query4	= $this->db->query("SELECT b.nama,a.jum_forecast FROM tt_forecast_gudang_jd_detail_warna a
												INNER JOIN tm_warna b ON b.id=a.id_warna 
												WHERE a.id=	'$row3->id'	AND b.id='$row3->id_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow4 = $query4->row();
							$tm_warna=$hasilrow4->nama;
							$qty_fore_warna=$hasilrow4->jum_forecast;
						}
						
					
					$query4	= $this->db->query("SELECT sum(c.qty) as qty_sj_warna  FROM tm_sj_drop_forecast a
												INNER JOIN tm_sj_drop_forecast_detail b ON a.id=b.id_sj_drop_forecast
												INNER JOIN tm_sj_drop_forecast_detail_warna c ON b.id=c.id_sj_drop_forecast_detail
												INNER JOIN tm_warna d ON d.id=c.id_warna 
												WHERE extract(year from a.tgl_sj)= '$thn1' AND extract(month from a.tgl_sj)= '$bln1'
												AND d.id='$row3->id_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow4 = $query4->row();
							$qty_sj_warna=$hasilrow4->qty_sj_warna;
							
						}
						
						$qty_sisa_warna = $qty_fore_warna - $qty_sj_warna;
						
						$data_stok_warna[] = array(		
											'tm_warna'=> $tm_warna,
											'qty_fore_warna'=> $qty_fore_warna,
											'qty_sj_warna'=>$qty_sj_warna,
											'qty_sisa_warna'=>$qty_sisa_warna
											);
					}
				}
				
					
				
												
				$data_stok[] = array(		'kode_brg'=> $kode_brg_wip,
											'nama_brg'=> $nama_brg_wip,
											'sa'=>$sa,
											'jum_keluar'=>$jum_keluar,
											'sisa'=>$sisa,
											'data_stok_warna'=>$data_stok_warna
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  }

