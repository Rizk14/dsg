<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $unit_makloon, $cari) {	  
	if ($cari == "all") {
		if ($unit_makloon == '0') {
			$this->db->select(" * FROM tm_sj_hasil_print ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_hasil_print WHERE kode_unit = '$unit_makloon' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($unit_makloon != '0') {
			$this->db->select(" * FROM tm_sj_hasil_print WHERE kode_unit = '$unit_makloon' 
			AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_hasil_print WHERE UPPER(no_sj) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_masuk = array();
		$data_masuk_detail = array();

		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
								
				// ===============================
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_print_detail WHERE id_sj_hasil_print = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					$id_detailnya = "";
					
					foreach ($hasil2 as $row2) {						
						$id_detailnya.= $row2->id_sj_proses_print_detail."-";
												
						// bhn baku
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$query3	= $this->db->query(" SELECT nama_brg FROM tm_brg_hasil_makloon
										WHERE kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$nama_brg	= $hasilrow->nama_brg;
							}
							else
								$nama_brg = '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
				
						$data_masuk_detail[] = array( 'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi
												//'id_detailnya'=> $id_detailnya
											);
					}
				}
				else {
					$data_masuk_detail = '';
				}
				// ===========
				
			/*	$query3	= $this->db->query(" SELECT tgl_sj FROM tm_sj_proses_quilting WHERE no_sj = '$row1->no_sj_keluar'
											AND kode_unit= '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$tgl_sj_keluar	= $hasilrow->tgl_sj;
				}
				else {
					$tgl_sj_keluar = '';
				} */
																
				$data_masuk[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'no_sj_keluar'=> $row1->no_sj_keluar,
										//	'tgl_sj_keluar'=> $tgl_sj_keluar,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'data_masuk_detail'=> $data_masuk_detail,
											'id_detailnya'=> $id_detailnya
											);
				$id_detailnya = "";
				$data_masuk_detail = array();
			} // endforeach header
		}
		else {
			$data_masuk = '';
		}
		
		return $data_masuk;
  }
  
  function getAlltanpalimit($unit_makloon, $cari){
	if ($cari == "all") {
		if ($unit_makloon == '0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print WHERE kode_unit = '$unit_makloon' ");
	}
	else {
		if ($unit_makloon != '0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print WHERE kode_unit = '$unit_makloon' 
			AND UPPER(no_sj) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print WHERE UPPER(no_sj) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_sj, $unit_makloon){
    $this->db->select("id from tm_sj_hasil_print WHERE no_sj = '$no_sj' AND kode_unit = '$unit_makloon' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function save($no_sj_keluar, $no_sj, $tgl_sj, $unit_makloon, $ket, $kode, $id_sj_detail, $id_sj_proses_print, $qty, $kode_brg_jadi){  
    $tgl = date("Y-m-d");
    
    // cek apa udah ada datanya blm dgn no sj tadi
    $this->db->select("id from tm_sj_hasil_print WHERE no_sj = '$no_sj' AND kode_unit = '$unit_makloon' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di header (tm_sj_hasil_makloon)
			$data_header = array(
			  'no_sj_keluar'=>$no_sj_keluar,
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_makloon,
			  'keterangan'=>$ket,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			$this->db->insert('tm_sj_hasil_print',$data_header);
			
			$this->db->query(" UPDATE tm_sj_proses_print SET status_edit = 't' WHERE id = '$id_sj_proses_print' ");
			
			// ambil data terakhir di tabel tm_sj_hasil_print
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_print ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj_hasil_print	= $hasilrow->id;
			
			if ($kode!='' && $qty!='') {
				//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di SJ keluar
				// koreksi 19-08-2011: skrip ini ga usah dipake lagi soalnya qty yg masuk boleh melebihi qty planning
				/*$query3	= $this->db->query(" SELECT qty FROM tm_sj_proses_print_detail 
							WHERE id = '$id_sj_detail' ");
				$hasilrow = $query3->row();
				$qty_sj_keluar = $hasilrow->qty; // ini qty di sj proses print detail */
							
				/*$query3	= $this->db->query(" SELECT sum(b.qty) as jum FROM tm_sj_hasil_print a, 
										tm_sj_hasil_print_detail b WHERE a.id = b.id_sj_hasil_print 
										AND b.id_sj_proses_print_detail = '$id_sj_detail' 
										AND b.kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_print berdasarkan id sj_proses_print
									
				$jumtot = $jum_hasil+$qty;
				if ($jumtot > $qty_sj_keluar)
					$qty = $qty_sj_keluar-$jum_hasil;
				*/
					
				$data_detail = array(
					'kode_brg'=>$kode,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'qty'=>$qty,
					'id_sj_hasil_print'=>$id_sj_hasil_print,
					'id_sj_proses_print_detail'=>$id_sj_detail
				);
				$this->db->insert('tm_sj_hasil_print_detail',$data_detail);
			}
			// =======================================
			
			$query3	= $this->db->query(" SELECT qty as jum FROM tm_sj_proses_print_detail WHERE id = '$id_sj_detail' ");
			$hasilrow = $query3->row();
			$qty_sj_keluar = $hasilrow->jum; // ini qty di sj_proses_print_detail
			
			$query3	= $this->db->query(" SELECT sum(qty) as jum FROM 
								tm_sj_hasil_print_detail WHERE 
								id_sj_proses_print_detail = '$id_sj_detail' ");
			$hasilrow = $query3->row();
			$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_print_detail berdasarkan id_detail di sj_proses_print tsb
			
			// cek status_makloon di tabel tm_sj_proses_print
			if ($qty_sj_keluar <= $jum_hasil) {
					$this->db->query(" UPDATE tm_sj_proses_print_detail SET status_makloon = 't' 
									where id= '$id_sj_detail' ");
					
					//cek di tabel tm_sj_proses_print_detail, apakah status_makloon sudah 't' semua?
					$this->db->select(" id from tm_sj_proses_print_detail WHERE status_makloon = 'f' 
										AND id_sj_proses_print = '$id_sj_proses_print' ", false);
					$query = $this->db->get();
					//jika sudah t semua, maka update tabel tm_sj_proses_print di field status_makloon menjadi t
					if ($query->num_rows() == 0){
						$this->db->query(" UPDATE tm_sj_proses_print SET status_makloon = 't' where id= '$id_sj_proses_print' ");
					}
			}
			
			// ============ update stok =====================
			//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_print WHERE kode_brg = '$kode' 
											AND kode_brg_jadi = '$kode_brg_jadi' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty; // bertambah stok karena SJ masuk hasil print
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_print, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'kode_brg_jadi'=>$kode_brg_jadi,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_print',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_print SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' AND kode_brg_jadi = '$kode_brg_jadi' ");
					}
					
					$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_print (kode_brg, kode_brg_jadi, no_bukti, kode_unit, masuk, saldo, tgl_input) 
											VALUES ('$kode', '$kode_brg_jadi', '$no_sj', '$unit_makloon', '$qty', '$new_stok', '$tgl') ");
					 
			// ==============================================
		}
		else {
			// ambil data terakhir di tabel tm_sj_hasil_print
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_print ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj_hasil_print	= $hasilrow->id;
			
			if ($kode!='' && $qty!='') {
				//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di SJ keluar
				// koreksi 19-08-2011: skrip ini ga usah dipake lagi soalnya qty yg masuk boleh melebihi qty planning
				/*$query3	= $this->db->query(" SELECT qty FROM tm_sj_proses_print_detail 
							WHERE id = '$id_sj_detail' ");
				$hasilrow = $query3->row();
				$qty_sj_keluar = $hasilrow->qty; // ini qty di sj proses print detail */
							
				/*$query3	= $this->db->query(" SELECT sum(b.qty) as jum FROM tm_sj_hasil_print a, 
										tm_sj_hasil_print_detail b WHERE a.id = b.id_sj_hasil_print 
										AND b.id_sj_proses_print_detail = '$id_sj_detail' 
										AND b.kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_print berdasarkan id sj_proses_print
									
				$jumtot = $jum_hasil+$qty;
				if ($jumtot > $qty_sj_keluar)
					$qty = $qty_sj_keluar-$jum_hasil;
				*/
					
				$data_detail = array(
					'kode_brg'=>$kode,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'qty'=>$qty,
					'id_sj_hasil_print'=>$id_sj_hasil_print,
					'id_sj_proses_print_detail'=>$id_sj_detail
				);
				$this->db->insert('tm_sj_hasil_print_detail',$data_detail);
			}
			// =======================================
			
			$query3	= $this->db->query(" SELECT qty as jum FROM tm_sj_proses_print_detail WHERE id = '$id_sj_detail' ");
			$hasilrow = $query3->row();
			$qty_sj_keluar = $hasilrow->jum; // ini qty di sj_proses_print_detail
			
			$query3	= $this->db->query(" SELECT sum(qty) as jum FROM 
								tm_sj_hasil_print_detail WHERE 
								id_sj_proses_print_detail = '$id_sj_detail' ");
			$hasilrow = $query3->row();
			$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_print_detail berdasarkan id_detail di sj_proses_print tsb
			
			// cek status_makloon di tabel tm_sj_proses_print
			if ($qty_sj_keluar <= $jum_hasil) {
					$this->db->query(" UPDATE tm_sj_proses_print_detail SET status_makloon = 't' 
									where id= '$id_sj_detail' ");
					
					//cek di tabel tm_sj_proses_print_detail, apakah status_makloon sudah 't' semua?
					$this->db->select(" id from tm_sj_proses_print_detail WHERE status_makloon = 'f' 
										AND id_sj_proses_print = '$id_sj_proses_print' ", false);
					$query = $this->db->get();
					//jika sudah t semua, maka update tabel tm_sj_proses_print di field status_makloon menjadi t
					if ($query->num_rows() == 0){
						$this->db->query(" UPDATE tm_sj_proses_print SET status_makloon = 't' where id= '$id_sj_proses_print' ");
					}
			}
			
			// ============ update stok =====================
			//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_print WHERE kode_brg = '$kode' 
											AND kode_brg_jadi = '$kode_brg_jadi' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty; // bertambah stok karena SJ masuk hasil print
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_print, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'kode_brg_jadi'=>$kode_brg_jadi,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_print',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_print SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' AND kode_brg_jadi = '$kode_brg_jadi' ");
					}
					
					$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_print (kode_brg, kode_brg_jadi, no_bukti, kode_unit, masuk, saldo, tgl_input) 
											VALUES ('$kode', '$kode_brg_jadi', '$no_sj', '$unit_makloon', '$qty', '$new_stok', '$tgl') ");
					 
			// ==============================================
		}

  }
    
  function delete($kode, $list_brg){    
	  $tgl = date("Y-m-d");
	  $id_brg = explode("-", $list_brg);
	  
	 /* $query3	= $this->db->query(" SELECT id_sj_proses_print FROM tm_sj_hasil_print WHERE id = '$kode' ");
	  $hasilrow = $query3->row();
	  $id_sj_keluar	= $hasilrow->id_sj_proses_print; */
	  
	  //
	 // $this->db->query(" UPDATE tm_sj_proses_print SET status_makloon = 'f', status_edit = 'f' where id= '$id_sj_keluar' "); 
	  
	  foreach($id_brg as $row1) {
			if ($row1 != '') {
				$this->db->query(" UPDATE tm_sj_proses_print_detail SET status_makloon = 'f' where id= '$row1' ");
				
				  $query3	= $this->db->query(" SELECT id_sj_proses_print FROM tm_sj_proses_print_detail 
												WHERE id = '$row1' ");
				  $hasilrow = $query3->row();
				  $id_sj_keluar	= $hasilrow->id_sj_proses_print;
				  
				 $this->db->query(" UPDATE tm_sj_proses_print SET status_makloon = 'f', status_edit = 'f' 
									where id= '$id_sj_keluar' ");
			}
	  }
	  
	  $query3	= $this->db->query(" SELECT no_sj, kode_unit FROM tm_sj_hasil_print WHERE id = '$kode' ");
	  $hasilrow = $query3->row();
	  $no_sj	= $hasilrow->no_sj; 
	  $unit_makloon	= $hasilrow->kode_unit; 
	  
	  //
	  $query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_print_detail WHERE id_sj_hasil_print = '$kode' ");
	  if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
										
			foreach ($hasil2 as $row2) {
				
				// ============ update stok =====================
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_print 
						WHERE kode_brg = '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama-$row2->qty; // berkurang stok karena reset dari SJ msk hsl print
										
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_print, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'kode_brg_jadi'=>$row2->kode_brg_jadi,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_print',$data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_print SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								}
										
								$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_print 
										(kode_brg, kode_brg_jadi, no_bukti, kode_unit, keluar, saldo, tgl_input) VALUES 
										('$row2->kode_brg', '$row2->kode_brg_jadi', '$no_sj', '$unit_makloon', '$row2->qty', '$new_stok', '$tgl') ");
								
								// ==============================================
			}
	 }
		$this->db->delete('tm_sj_hasil_print', array('id' => $kode));
		$this->db->delete('tm_sj_hasil_print_detail', array('id_sj_hasil_print' => $kode));

  } 
  
  function get_sj($id_sj){
	$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print WHERE id = '$id_sj' ");    
    $hasil = $query->result();
    
    $data_sj = array();
    $detail_sj = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_print_detail WHERE id_sj_hasil_print = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {						
						// bhn baku
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							//$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$query3	= $this->db->query(" SELECT nama_brg FROM tm_brg_hasil_makloon
										WHERE kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$nama_brg_makloon	= $hasilrow->nama_brg;
							}
							else
								$nama_brg_makloon = '';
						}
						
						$query3	= $this->db->query(" SELECT keterangan 
								FROM tm_sj_proses_print_detail WHERE id = '$row2->id_sj_proses_print_detail' ");
						$hasilrow = $query3->row();
						$ket_bagian	= $hasilrow->keterangan;
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
																
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg_makloon,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'id_sj_proses_print_detail'=> $row2->id_sj_proses_print_detail,
												'ket_bagian'=> $ket_bagian
											);
					}
				}
				else {
					$detail_sj = '';
				}
		
		// ================== end detail
		
				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
												
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj_keluar'=> $row1->no_sj_keluar,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
											
			} // endforeach header
	
	return $data_sj;
  }
  
  function get_jenis_makloon(){
    $this->db->select("* from tm_jenis_makloon order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_bhn)
  {
	if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_bhn){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  //function get_sj_keluar($num, $offset, $cari, $unit_makloon)
  function get_sj_keluar($cari, $unit_makloon)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_sj_proses_print where kode_unit = '$unit_makloon' AND status_makloon = 'f' order by no_sj DESC ";
		//$this->db->select($sql, false)->limit($num,$offset);
		$this->db->select($sql, false);
	}
	else {
		$sql = " * FROM tm_sj_proses_print where kode_unit = '$unit_makloon' AND status_makloon = 'f' ";
		$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by no_sj DESC ";
		$this->db->select($sql, false);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {		
			
			// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_print_detail 
											WHERE id_sj_proses_print = '$row1->id' AND status_makloon = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
	//				$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						// bhn baku
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$query3	= $this->db->query(" SELECT nama_brg FROM tm_brg_hasil_makloon
										WHERE kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$nama_brg	= $hasilrow->nama_brg;
							}
							else
								$nama_brg = '';

						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
						
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum FROM tm_sj_hasil_print a, 
									tm_sj_hasil_print_detail b WHERE a.id = b.id_sj_hasil_print 
									AND b.id_sj_proses_print_detail = '$row2->id' 
									AND b.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_print berdasarkan id sj_proses_print_detail
						
						if ($jum_hasil == 0)
							$qty = $row2->qty;
						else
							$qty = $row2->qty-$jum_hasil;
						
						$detail_bhn[] = array('id'=> $row2->id,
												'id_sj_proses_print'=> $row2->id_sj_proses_print,
												'no_sj'=> $row1->no_sj,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $qty
									);
						
					} 
				} 
				else {
					$detail_bhn = '';
				} // end detail
			
				// ambil nama jenis makloon
				$query3	= $this->db->query(" SELECT nama FROM tm_jenis_makloon 
										WHERE id = '$row1->id_jenis_makloon' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_jenis_makloon	= $hasilrow->nama;
				}

			$data_bhn[] = array(		'id'=> $row1->id,	
										'no_sj'=> $row1->no_sj,	
										'tgl_sj'=> $row1->tgl_sj,
										'tgl_update'=> $row1->tgl_update,
										'keterangan'=> $row1->keterangan,
										'nama_jenis_makloon'=> $nama_jenis_makloon,
										'detail_bhn'=> $detail_bhn
								);
			
			$detail_bhn = array();
								
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_sj_keluartanpalimit($cari, $unit_makloon){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_sj_proses_print where kode_unit = '$unit_makloon' AND status_makloon = 'f' order by no_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_sj_proses_print where kode_unit = '$unit_makloon' AND status_makloon = 'f' ";
		$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by no_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  //function get_detail_sj_keluar($id_sj_keluar, $list_brg, $unit_makloon){
	function get_detail_sj_keluar($list_brg, $unit_makloon){
    $detail_sj = array();
        
    foreach($list_brg as $row1) {
			if ($row1 != '') {
			$query2	= $this->db->query(" SELECT id_sj_proses_print, kode_brg, qty, kode_brg_jadi, keterangan 
								FROM tm_sj_proses_print_detail WHERE id = '$row1' ");
			$hasilrow = $query2->row();
			$id_sj_proses_print	= $hasilrow->id_sj_proses_print;
			$kode_brg	= $hasilrow->kode_brg;
			$qty	= $hasilrow->qty;
			$kode_brg_jadi	= $hasilrow->kode_brg_jadi;
			$keterangan	= $hasilrow->keterangan;
    		
						/* $query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum FROM tm_sj_hasil_makloon a, 
									tm_sj_hasil_makloon_detail b WHERE a.id = b.id_sj_hasil_makloon 
									AND a.id_sj_proses_quilting = '$row1->id'
									AND b.id_sj_proses_makloon_detail = '$row2->id' 
									AND b.kode_brg_makloon = '$row2->kode_brg_makloon' ");
						$hasilrow = $query3->row();
						$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_makloon berdasarkan id sj_proses_makloon_detail
						*/
						
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum FROM tm_sj_hasil_print a,
									tm_sj_hasil_print_detail b WHERE a.id = b.id_sj_hasil_print 
									AND b.id_sj_proses_print_detail = '$row1' 
									AND b.kode_brg = '$kode_brg' ");
						$hasilrow = $query3->row();
						$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_print berdasarkan kode brg tsb
						
						$qty = $qty-$jum_hasil;
						
						// bhn baku
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						//	$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$query3	= $this->db->query(" SELECT nama_brg FROM tm_brg_hasil_makloon
										WHERE kode_brg = '$kode_brg' ");
							if ($query3->num_rows() > 0){
								$nama_brg	= $hasilrow->nama_brg;
							}
							else
								$nama_brg = '';

						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
		
			$detail_sj[] = array(		'id'=> $row1,
										'id_sj_proses_print'=> $id_sj_proses_print,
										'kode_brg'=> $kode_brg,
										'nama'=> $nama_brg,
										'kode_brg_jadi'=> $kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'qty'=> $qty,
										'keterangan'=> $keterangan
								);
		}
	}
	return $detail_sj;
  }

}
