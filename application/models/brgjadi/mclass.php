<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {
	
	
	function __construct() { 
		parent::__construct();

	}
	
	function view() {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_product_base ORDER BY i_product_base ASC, e_product_basename ASC ", false);
	}
	
	function cek_imotif($imotif) {
		 $db2=$this->load->database('db_external', TRUE);
		$imotifnya	= $imotif.'00';
		return $db2->query(" SELECT * FROM tr_product_motif WHERE n_active='1' AND i_product_motif='$imotifnya' ");
	}
	
	function viewperpages($limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_product_base ORDER BY i_product_base ASC, e_product_basename ASC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function listpquery($tabel,$order,$filter) {
		 $db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		if ($query->num_rows() > 0) {
			return $result	= $query->result(); 
		}
	}
	
	function pquery($tabel,$order,$filter) {	
		$db2=$this->load->database('db_external', TRUE);	
		$db2->select(" * FROM ".$tabel." ".$order." ".$filter);		
		$query = $db2->get();
		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function msimpan($i_product_base,$e_product_basename,$i_category,$i_brand,$i_layout,$v_price,$e_quality,
	$f_stop_produksi,$e_surat_penawaran,$d_penawaran,$n_status,$d_launching,$n_quantity, $is_grosir, $harga_grosir) {	
		$db2 = $this->load->database('db_external', TRUE);
		$db2->trans_begin();

		
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		if ($is_grosir == "")
			$harga_grosir = 0;

		$db2->set(array(
			 'i_product_base'=>$i_product_base,
			 'e_product_basename'=>$e_product_basename,
			 'i_supplier'=>0,
			 'i_category'=>$i_category,
			 'i_brand'=>$i_brand,
			 'i_layout'=>$i_layout,
			 'v_unitprice'=>$v_price,
			 'e_quality'=>$e_quality,
			 'f_stop_produksi'=>$f_stop_produksi,
			 'e_surat_penawaran'=>$e_surat_penawaran,
			 'd_surat_penawaran'=>$d_penawaran,
			 'n_status'=>$n_status,
			 'd_entry'=>$d_launching,
			 'd_update'=>$d_launching,
			 'harga_grosir'=>$harga_grosir));
		
		if($db2->insert('tr_product_base')) {
			
			// Database External
		/*	$qproductbase2	= $db2->query(" INSERT INTO tr_product_base(i_product_base,e_product_basename,i_supplier,i_category,i_brand,i_layout,v_unitprice, harga_grosir, e_quality,f_stop_produksi,e_surat_penawaran,d_surat_penawaran,n_status,d_entry) 
			VALUES('$i_product_base','$e_product_basename','0','$i_category','$i_brand','$i_layout','$v_price', '$harga_grosir', '$e_quality','$f_stop_produksi','$e_surat_penawaran','$d_penawaran','$n_status','$d_launching') "); */
			// End 0f 
			
			$iproductmotif	= $i_product_base.'00';
			//$qryprice		= $db2->query(" SELECT * FROM tr_product_price WHERE f_active='t' ORDER BY i_price DESC LIMIT 1 ");
			$qryprice		= $db2->query(" SELECT * FROM tr_product_price ORDER BY i_price DESC LIMIT 1 ");
			
			if($qryprice->num_rows()>0) {
				
				$row_price	= $qryprice->row();
				
				$iproductprice	= $row_price->i_price+1;
				
				$icodeprice0		= $row_price->i_product_price; // H00001
				$icodeprice_angka	= substr($icodeprice0,1,strlen($icodeprice0)-1); // 00001
				$icodeprice_jml		= $icodeprice_angka+1;

				switch(strlen($icodeprice_jml)) {
					case 1:
						$icode	= 'H'.'0000'.$icodeprice_jml;
					break;
					case 2:
						$icode	= 'H'.'000'.$icodeprice_jml;
					break;
					case 3:
						$icode	= 'H'.'00'.$icodeprice_jml;
					break;
					case 4:
						$icode	= 'H'.'0'.$icodeprice_jml;
					break;
					default:
						$icode	= 'H'.$icodeprice_jml;
				}
				
				$icodeprice		= $icode;
				
			}else{
				$icodeprice		= "H00001";
				$iproductprice	= 1;
			}
			
			$insprice	= array(
				'i_price'=>$iproductprice,
				'i_product_price'=>$icodeprice,
				'i_customer'=>0,
				'i_product'=>$i_product_base,
				'i_product_motif'=>$iproductmotif,
				'v_price'=>$v_price,
				'd_entry'=>$dentry,
				'd_update'=>$dentry
			);
			
			if($db2->insert('tr_product_price',$insprice)) {
				
				if ($harga_grosir != 0) {
					$insprice	= array(
						'i_price'=>$iproductprice,
						'i_product_price'=>$icodeprice,
						'i_customer'=>0,
						'i_product'=>$i_product_base,
						'i_product_motif'=>$iproductmotif,
						'v_price'=>$harga_grosir,
						'd_entry'=>$dentry,
						'd_update'=>$dentry,
						'is_grosir'=>'t'
					);
					$db2->insert('tr_product_price',$insprice);
				}
				
				$db2->set(array(
					'i_product_motif'=>$iproductmotif,
					'i_product'=>$i_product_base,
					'e_product_motifname'=>$e_product_basename,
					'n_quantity'=>$n_quantity,
					'n_active'=>'1',
					'i_color'=>'0',
					'd_entry'=>$dentry,
					'd_update'=>$dentry
				));
				
				$db2->insert('tr_product_motif');
				
				// Database External
				/*
				$qproductprice2	= $db2->query(" INSERT INTO tr_product_price(i_price,i_product_price,i_customer,i_product,i_product_motif,v_price) 
					VALUES('$iproductprice','$icodeprice','0','$i_product_base','$iproductmotif','$v_price') ");
				*/
				
			/*	$qproductmotif2 = $db2->query(" INSERT INTO tr_product_motif (i_product_motif,i_product,e_product_motifname,n_quantity,n_active,i_color,d_entry,d_update)
					VALUES('$iproductmotif','$i_product_base','$e_product_basename','$n_quantity','1','0','$dentry','$dentry') "); */
				
			}

		}
		
		if ($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
		
		redirect('brgjadi/cform/');
	}
	
	function medit($id) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_product_base WHERE i_product_base='$id' ");
	}
	
	function ilayout($ilayout) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_layout WHERE i_layout='$ilayout' ORDER BY i_layout DESC LIMIT 1 ");
	}

	function icategory($icategory) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_categories WHERE i_category='$icategory' ORDER BY i_category DESC LIMIT 1 ");
	}	
	
	function ibrand($ibrand) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_brand WHERE i_brand='$ibrand' ORDER BY i_brand DESC LIMIT 1 ");
	}	

	function qty($iproduct) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_product_motif WHERE i_product='$iproduct' AND n_active='1' ORDER BY i_product DESC LIMIT 1 ");
	}
	
	function getmotifname($iproduct) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_product_motif WHERE i_product='$iproduct' ORDER BY i_product_motif DESC, d_entry DESC LIMIT 1 " );
	}

	function mupdate($iproductbase,$eproductbasename,$icategory,$ibrand,$ilayout,$vprice, $is_grosir, $harga_grosir, 
				$equality,$f_stop_produksi,$esuratpenawaran,$dpenawaran,$nstatus,$dlaunching,$nquantity) {	
		
		$db2 = $this->load->database('db_external', TRUE);
		
		$dupdate = date("Y-m-d");		
		
		$imotif = $iproductbase."00";
		
		if ($is_grosir == "")
			$harga_grosir = 0;
		
		$productbase_item	= array(
			'e_product_basename'=>$eproductbasename,
			'i_category'=>$icategory,
			'i_brand'=>$ibrand,
			'i_layout'=>$ilayout,
			'v_unitprice'=>$vprice,
			'harga_grosir'=>$harga_grosir,
			'e_quality'=>$equality,
			'f_stop_produksi'=>$f_stop_produksi,
			'e_surat_penawaran'=>$esuratpenawaran,
			'd_surat_penawaran'=>$dpenawaran,
			'n_status'=>$nstatus,
			'd_update'=>$dupdate);
		
		$productprice	= array(
			'v_price'=>$vprice
		);
		
		$productpricegrosir	= array(
			'v_price'=>$harga_grosir
		);
		
		$db2->update('tr_product_base',$productbase_item,array('i_product_base'=>$iproductbase));
		$db2->update('tr_product_price',$productprice,array('i_product'=>$iproductbase,'i_customer'=>'0'));
		// harga grosir
		$db2->update('tr_product_price',$productpricegrosir,array('i_product'=>$iproductbase,'i_customer'=>'0', 'is_grosir'=>'t' ));
		
		$db2->query(" UPDATE tr_product_motif SET e_product_motifname='$eproductbasename' WHERE i_product='$iproductbase' AND i_product_motif='$imotif' ");
		
	/*	$qproductbase2 = $db2->query(" UPDATE tr_product_base SET e_product_basename = '$eproductbasename',
				i_category = '$icategory', 
				i_brand = '$ibrand', 
				i_layout = '$ilayout', 
				v_unitprice = '$vprice', 
				e_quality = '$equality', 
				f_stop_produksi = '$f_stop_produksi', 
				e_surat_penawaran = '$esuratpenawaran', 
				d_surat_penawaran = '$dpenawaran', 
				n_status = '$nstatus',
				d_update = '$dupdate' WHERE i_product_base='$iproductbase' ");
		
		$db2->query(" UPDATE tr_product_motif SET e_product_motifname='$eproductbasename' WHERE i_product='$iproductbase' AND i_product_motif='$imotif' ");
		*/
		$digit	= 0;
		
		for($awal=0;$awal<10;$awal++) {
			
			$insert_digit	= '0'.$digit;
			
			$kode_digit_baru	= $iproductbase.$insert_digit;
			
			$qcari_mutasi	= $db2->query(" SELECT i_mutasi, i_product FROM tm_stokmutasi WHERE i_product='$kode_digit_baru' AND f_active_month='t' ");
			
			if($qcari_mutasi->num_rows()>0) {
				$rcari_mutasi	= $qcari_mutasi->row();
				if($insert_digit=='00') {
					$db2->query(" UPDATE tm_stokmutasi SET f_stop_produksi='$f_stop_produksi' WHERE i_mutasi='$rcari_mutasi->i_mutasi' AND f_active_month='t' ");
				}else{
					$db2->query(" UPDATE tm_stokmutasi SET f_stop_produksi='$f_stop_produksi' WHERE i_mutasi='$rcari_mutasi->i_mutasi' AND f_active_month='t' ");
				}
			}
			
			$qfstp	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stop_produksi' ");
			$rfstp	= $qfstp->row();
			
			$qcari_opname	= $db2->query(" SELECT a.*,b.i_so_item FROM tm_stokopname a 
							
							INNER JOIN tm_stokopname_item b ON a.i_so=b.i_so 
							
							WHERE b.i_product='$kode_digit_baru' AND a.i_status_so='0' ORDER BY b.i_so_item DESC LIMIT 1 ");	
			
			if($qcari_opname->num_rows()>0) {
				$rcari_opname	= $qcari_opname->row();
				if($insert_digit=='00') {
					$db2->query(" UPDATE tm_stokopname_item SET i_so='$rfstp->i_so' WHERE i_so_item='$rcari_opname->i_so_item' ");
				}else{
					$db2->query(" UPDATE tm_stokopname_item SET i_so='$rfstp->i_so' WHERE i_so_item='$rcari_opname->i_so_item' ");
				}
			}
			
			$digit+=1;
			
		}
		
		redirect('brgjadi/cform/');
	}
	
	function viewcari($cari) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_product_base WHERE UPPER(i_product_base) LIKE UPPER('$cari%') OR UPPER(e_product_basename) LIKE UPPER('$cari%') OR UPPER(e_surat_penawaran) LIKE UPPER('$cari%') ORDER BY i_product_base ASC, e_product_basename ASC " );
	}
	
	function mcari($cari,$limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_product_base WHERE UPPER(i_product_base) LIKE UPPER('$cari%') OR UPPER(e_product_basename) LIKE UPPER('$cari%') OR UPPER(e_surat_penawaran) LIKE UPPER('$cari%') ORDER BY i_product_base ASC, e_product_basename ASC LIMIT ".$limit." OFFSET ".$offset." ",false);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
	
	function carikodebrg($kodebrg) {
		 $db2=$this->load->database('db_external', TRUE);
		$kodebarang	= trim($kodebrg);
		
		return $db2->query(" SELECT * FROM tr_product_base WHERE UPPER(i_product_base)=UPPER('$kodebarang') ");
	}
	
}
?>
