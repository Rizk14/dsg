<?php
class Master_jenis_bb_model extends MY_Model
{
	protected $_per_page = 10;
    protected $_tabel = 'tb_master_jenis_bb';
    protected $form_rules = array(
        array(
            'field' => 'nama_jenis_bb',
            'label' => 'Nama jenis BB',
            'rules' => 'trim|required|max_length[64]'
        ),
        array(
            'field' => 'kode_jenis_bb',
            'label' => 'Kode jenis BB',
            'rules' => 'trim|required|max_length[10]'
        ),
        
    );
    

     public $default_values = array(
		'nama_jenis_bb' => '',
        'kode_jenis_bb' => '',
        'id'	=>	'',
		'num'		=>1,
    );
    public function __construct()
	{
			parent::__construct();
			 $this->_db4 = $this->load->database('db_additional',TRUE);
	}
        public function input($kode_jenis_bb,$nama_jenis_bb)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 $created_by=$updated_by=$this->session->userdata('pengguna_id');
		$data= array(
		'kode_jenis_bb' => $kode_jenis_bb,
		'nama_jenis_bb' => $nama_jenis_bb,
		'created_at'=> $created_at,
		'updated_at'=> $updated_at,
		'created_by'=> $created_by,
		'updated_by'=> $updated_by
		);
		
        $id= $this->_db4->insert('tb_master_jenis_bb',$data);
	
        if ($id) {
		 return true;
		}
		 return false;

    }
     public function cari($offset)
    {
        $this->get_real_offset($offset);
        $kata_kunci = $this->input->get('kata_kunci', true);
       
        return $this->_db4->where("( kode_jenis_bb LIKE '%$kata_kunci%' OR nama_jenis_bb LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->result();
    }
    
     public function cari_num_rows()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
          return $this->_db4->where("( kode_jenis_bb LIKE '%$kata_kunci%' OR nama_jenis_bb LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->num_rows();
    }
    
    public function edit($id, $nama_jenis_bb,$kode_jenis_bb)
    {
     $updated_at = date('Y-m-d H:i:s');
     $this->_db4->where('id', $id);
     $data = array(
        'nama_jenis_bb' => $nama_jenis_bb,
       'kode_jenis_bb' => $kode_jenis_bb,
       'updated_at' => $updated_at
);
    $this->_db4->update('tb_master_jenis_bb', $data);
    return true;
    }
    
     public function master_jenis_bb($offset)
    {
     $this->_db4->select("* FROM tb_master_jenis_bb where TRUE" )->limit($this->_per_page,$offset);
     $query = $this->_db4->get();
     if ($query->num_rows > 0){
		$hasilrow=$query->result();
		
		return $hasilrow;
		 }
    }
    
   
    
}
