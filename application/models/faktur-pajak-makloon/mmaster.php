<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $supplier, $cari) {
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_faktur_pajak_makloon
								ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_faktur_pajak_makloon WHERE kode_unit = '$supplier'
								ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_faktur_pajak_makloon WHERE kode_unit = '$supplier'
								AND UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_faktur_pajak_makloon WHERE UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) { 
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b, tm_faktur_pajak_makloon a 
				WHERE a.kode_unit = b.kode_supplier AND a.id = '$row1->id' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				// ambil data detail no & tgl faktur
				$query2	= $this->db->query(" SELECT a.no_faktur FROM tm_faktur_pajak_makloon_detail a, tm_faktur_pajak_makloon b 
							WHERE a.id_faktur_pajak_makloon = b.id AND
							b.no_faktur_pajak = '$row1->no_faktur_pajak' AND b.kode_unit = '$kode_supplier'
							AND b.jenis_makloon = '$row1->jenis_makloon' "); 
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_faktur	= $row2->no_faktur;
						
						$sql = "SELECT tgl_faktur from tm_faktur_makloon where no_faktur = '$no_faktur' 
								AND kode_unit = '$kode_supplier' AND jenis_makloon = '$row1->jenis_makloon' "; //echo $sql."<br>";
						$query3	= $this->db->query($sql);
						if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_faktur	= $hasilrow3->tgl_faktur;
						}
						else
							$tgl_faktur	= '';
							
						$pisah1 = explode("-", $tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_faktur'=> $no_faktur,
											'tgl_faktur'=> $tgl_faktur
											);
						
						}		
					}
					else {
						$detail_fb = '';
					}
					
				$sql = "SELECT nama from tm_jenis_makloon where id = '$row1->jenis_makloon'  ";
						$query3	= $this->db->query($sql);
						if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$nama_jenis	= $hasilrow3->nama;
						}
						else
							$nama_jenis	= '';
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_faktur_pajak'=> $row1->no_faktur_pajak,	
											'tgl_faktur_pajak'=> $row1->tgl_faktur_pajak,	
											'jumlah'=> $row1->jumlah,	
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'jenis_makloon'=> $row1->jenis_makloon,
											'nama_jenis'=> $nama_jenis,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" select * FROM tm_faktur_pajak_makloon ORDER BY tgl_faktur_pajak DESC  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_faktur_pajak_makloon WHERE kode_unit = '$supplier'
								ORDER BY tgl_faktur_pajak DESC ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_faktur_pajak_makloon WHERE kode_unit = '$supplier'
								AND UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY tgl_faktur_pajak DESC ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_faktur_pajak_makloon WHERE 
								UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY tgl_faktur_pajak DESC ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_fp, $jenis_makloon, $supplier){
    $this->db->select("id from tm_faktur_pajak_makloon WHERE kode_unit = '$supplier' AND no_faktur_pajak = '$no_fp'
						AND jenis_makloon = '$jenis_makloon' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_fp, $tgl_fp, $jenis_makloon, $supplier, $jum, $dpp, $no_faktur){  
    $tgl = date("Y-m-d");
	$list_faktur = explode(",", $no_faktur); 
	
	$data_header = array(
			  'no_faktur_pajak'=>$no_fp,
			  'tgl_faktur_pajak'=>$tgl_fp,
			  'jumlah'=>$jum,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'kode_unit'=>$supplier,
			  'dpp'=>$dpp,
			  'jenis_makloon'=>$jenis_makloon
			);
			$this->db->insert('tm_faktur_pajak_makloon',$data_header);
	
	//
	// ambil data terakhir di tabel tm_faktur_pajak_makloon
	$query2	= $this->db->query(" SELECT id FROM tm_faktur_pajak_makloon ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail faktur-nya
	foreach($list_faktur as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_faktur_pajak_makloon'=>$id_pf,
			  'no_faktur'=>$row1
			);
			$this->db->insert('tm_faktur_pajak_makloon_detail',$data_detail);
			
			// update status_faktur di tabel tm_faktur_makloon
			$this->db->query(" UPDATE tm_faktur_makloon SET no_faktur_pajak = '$no_fp', status_faktur_pajak = 't' 
								WHERE no_faktur = '$row1' AND kode_unit = '$supplier' AND jenis_makloon = '$jenis_makloon' ");
		}
	}
   
  }
    
  function delete($kode){
	//semua no_faktur di tabel tm_faktur_makloon yg bersesuaian dgn tm_faktur_pajak_makloon 
	//dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.kode_unit, a.jenis_makloon, b.no_faktur FROM tm_faktur_pajak_makloon a, tm_faktur_pajak_makloon_detail b
							WHERE a.id = b.id_faktur_pajak_makloon AND a.id = '$kode' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' WHERE 
			kode_unit = '$row1->kode_unit' AND no_faktur = '$row1->no_faktur' AND jenis_makloon = '$row1->jenis_makloon' ");
		}
	}
	//---------------------------------------------  
	  
	$this->db->delete('tm_faktur_pajak_makloon_detail', array('id_faktur_pajak_makloon' => $kode));    	
	$this->db->delete('tm_faktur_pajak_makloon', array('id' => $kode));

  }
  
  //function get_faktur_pembelian($num, $offset, $jnsaction, $supplier, $cari) {
	function get_faktur_jasa($jnsaction, $supplier, $jenis_makloon, $cari) {
	  // ambil data faktur jasa
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk add
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f'
									AND jenis_makloon = '$jenis_makloon'
									order by kode_unit, tgl_faktur DESC ");
			}
			else { // utk edit
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> '' 
								order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> '' 
								AND jenis_makloon = '$jenis_makloon' order by kode_unit, tgl_faktur DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk add
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND kode_supplier = '$supplier' 
									order by kode_supplier,tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' AND kode_unit = '$supplier' 
									AND jenis_makloon = '$jenis_makloon' order by kode_unit,tgl_faktur DESC ");
			}
			else { // edit
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
				AND kode_supplier = '$supplier' order by kode_supplier,tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
				AND kode_unit = '$supplier' AND jenis_makloon = '$jenis_makloon' order by kode_unit,tgl_faktur DESC ");
			}
		}
	}
	else {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk add		
			/*	$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' 
							AND jenis_makloon = '$jenis_makloon' AND UPPER(no_faktur) like UPPER('%$cari%') 
							order by kode_unit, tgl_faktur DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') AND jenis_makloon = '$jenis_makloon' order by kode_unit, tgl_faktur DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk add		
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' 
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' 
				AND kode_unit = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') 
				AND jenis_makloon = '$jenis_makloon' order by kode_unit, tgl_faktur DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
							AND kode_unit = '$supplier' AND jenis_makloon = '$jenis_makloon' 
							AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_unit, tgl_faktur DESC ");
			}
		}
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query2	= $this->db->query(" SELECT nama, pkp, tipe_pajak FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query2->row();
				$nama_supplier	= $hasilrow->nama;
				$pkp	= $hasilrow->pkp;
				$tipe_pajak	= $hasilrow->tipe_pajak;
				
			/*	$query2	= $this->db->query(" SELECT SUM(a.total) as totnya FROM tm_pembelian a, tm_pembelian_nofaktur b, tm_pembelian_nofaktur_sj c 
									WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
									AND b.id = c.id_pembelian_nofaktur
									AND b.no_faktur = '$row1->no_faktur' ");
				if ($query2->num_rows() > 0) { //
					$hasilrow = $query2->row();
					$totalnya	= $hasilrow->totnya;				
				}
				else
					$totalnya = '0'; */
				
			/*	$query2	= $this->db->query(" SELECT SUM(a.total_pajak) as totnya FROM tm_pembelian a, tm_pembelian_nofaktur b, tm_pembelian_nofaktur_sj c 
									WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
									AND b.id = c.id_pembelian_nofaktur
									AND b.no_faktur = '$row1->no_faktur' "); */
				
				$jum_tot = $row1->jumlah;
				
				if ($pkp == 't' && $tipe_pajak == 'I') {
					$pajaknya = $jum_tot / 11;
					$dpp = $row1->jumlah/1.1;
				}
				else if ($pkp == 't' && $tipe_pajak == 'E') {
					$pajaknya = $jum_tot * 0.1;
					$dpp = $row1->jumlah/1.1;
				}
				else if ($pkp == 'f') {
					$pajaknya = 0;
					$dpp = 0;
				}
				
				//$pajaknya = number_format($pajaknya,2,',','.');
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'kode_supplier'=> $row1->kode_unit,
											'nama_supplier'=> $nama_supplier,
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'tgl_update'=> $row1->tgl_update,
											'totalnya'=> $row1->jumlah,
											'pajaknya'=> $pajaknya,
											'dpp'=> $dpp
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function get_faktur_jasatanpalimit($jnsaction, $supplier, $jenis_makloon, $cari){ // sampe sini
	if ($cari == "all") {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk add
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f'
									AND jenis_makloon = '$jenis_makloon' ");
			}
			else { // utk edit
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> '' 
								order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> '' 
								AND jenis_makloon = '$jenis_makloon' ");
			}
			
			//$query= $this->db->query(" select * FROM tm_pembelian_nofaktur ");
		}
		else {
			if ($jnsaction == 'A') { // utk add
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND kode_supplier = '$supplier' 
									order by kode_supplier,tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' AND kode_unit = '$supplier' 
									AND jenis_makloon = '$jenis_makloon' ");
			}
			else { // edit
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
				AND kode_supplier = '$supplier' order by kode_supplier,tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
				AND kode_unit = '$supplier' AND jenis_makloon = '$jenis_makloon' ");
			}
			
			//$query = $this->db->query(" select * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$supplier' order by tgl_faktur DESC ");
		}
	}
	else {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk add		
			/*	$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' 
							AND jenis_makloon = '$jenis_makloon' AND UPPER(no_faktur) like UPPER('%$cari%')  ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') AND jenis_makloon = '$jenis_makloon' ");
			}
			
			/*$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE  
								UPPER(no_faktur) like UPPER('%$cari%') "); */
		}
		else {
			if ($jnsaction == 'A') { // utk add		
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' 
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE status_faktur_pajak = 'f' 
				AND kode_unit = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') AND jenis_makloon = '$jenis_makloon' ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
				AND kode_unit = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') AND jenis_makloon = '$jenis_makloon' ");
			}
			
			/*$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$supplier' AND 
								UPPER(no_faktur) like UPPER('%$cari%') "); */
								
		}
	}
    
    return $query->result();  
  }
        
  function get_pajak($id_pajak){
	$query	= $this->db->query(" SELECT * FROM tm_faktur_pajak_makloon where id='$id_pajak' ");    
	
	$data_pajak = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT no_faktur FROM tm_faktur_pajak_makloon_detail 
									WHERE id_faktur_pajak_makloon = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_faktur = '';
				foreach ($hasil2 as $row2) {
					$no_faktur .= $row2->no_faktur.", ";
					
				}
				
				$pisah1 = explode("-", $row1->tgl_faktur_pajak);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_faktur_pajak = $tgl1."-".$bln1."-".$thn1;						
			
				$data_pajak[] = array(		'id'=> $row1->id,	
											'no_faktur_pajak'=> $row1->no_faktur_pajak,
											'tgl_faktur_pajak'=> $tgl_faktur_pajak,
											'kode_supplier'=> $row1->kode_unit,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'dpp'=> $row1->dpp,
											'jenis_makloon'=> $row1->jenis_makloon,
											'no_faktur'=> $no_faktur
											);
			} // endforeach header
	}
    return $data_pajak; 
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }
  
  function get_detail_supplier($kode_sup){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kode_supplier = '$kode_sup' ");    
    return $query->result();  
  }  
  
  function get_jenis_makloon(){
	$query	= $this->db->query(" SELECT * FROM tm_jenis_makloon ORDER BY id ");    
    return $query->result();  
  }  

}
