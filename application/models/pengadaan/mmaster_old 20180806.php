<?php
class Mmaster extends CI_Model{
  function __construct() { 
  parent::__construct();

}
  
  function getbarangjaditanpalimit($cari){
	
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function getbarangjadi($num,$offset, $cari){    
    // new 27-01-2014
    if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {									
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function getAllsjmasuk($num, $offset, $cari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit = '$id_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";

		$this->db->select(" distinct a.* FROM tm_sjmasukbhnbakupic a 
							INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id=b.id_sjmasukbhnbakupic
							INNER JOIN tm_barang_wip c ON b.id_brg_wip = c.id
							WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%$caribrg%') OR UPPER(a.nama_brg_wip) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_sjmasukbhnbakupic_detail a 
									INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id  
									WHERE a.id_sjmasukbhnbakupic = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						
						// 30-01-2014, detail qty warna -------------
							$strwarna = "";
							$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjmasukbhnbakupic_detail_warna a
										INNER JOIN tm_warna c ON a.id_warna = c.id
										WHERE a.id_sjmasukbhnbakupic_detail = '$row2->id' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx=$queryxx->result();
								$strwarna.= $row2->kode_brg.": <br>";
								foreach ($hasilxx as $rowxx) {
									$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
								}
							}
							//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
																
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Retur Bahan Baku";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_unit_jahit	= $hasilrow->kode_unit;
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else {
					$kode_unit_jahit = "";
					$nama_unit_jahit = "";
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_masuk'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'kode_unit'=> $kode_unit_jahit,
											'nama_unit'=> $nama_unit_jahit,
											'keterangan'=> $row1->keterangan,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  // 24-02-2015. 15-10-2015 GA DIPAKE !
  /*function getAllsjmasuk_export($cari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.id_unit = '$id_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(c.nama_brg) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";

		$this->db->select(" distinct a.* FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b
					ON a.id = b.id_sjmasukbhnbakupic
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE TRUE ".$pencarian." ", false);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(b.e_product_motifname) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_sjmasukbhnbakupic_detail a, tr_product_motif b  
									WHERE a.kode_brg_jadi=b.i_product_motif 
									AND a.id_sjmasukbhnbakupic = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname 
									FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg_jadi	= $hasilrow->e_product_motifname;
							}
							else {
								$nama_brg_jadi = '';
							}
						
						// 30-01-2014, detail qty warna -------------
							$strwarna = "";
							$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjmasukbhnbakupic_detail_warna a, tm_warna c
										WHERE a.kode_warna = c.kode
										AND a.id_sjmasukbhnbakupic_detail = '$row2->id' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx=$queryxx->result();
								$strwarna.= $row2->kode_brg_jadi.": <br>";
								foreach ($hasilxx as $rowxx) {
									$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
								}
							}
							//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
																
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Retur Bahan Baku";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else
					$nama_unit_jahit = "";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_masuk'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit_jahit,
											'keterangan'=> $row1->keterangan,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  } */
  //-----------------------
  
  function getAllsjmasuktanpalimit($cari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit = '$id_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
	  		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_sjmasukbhnbakupic a 
					INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic 
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE TRUE ".$pencarian);

    return $query->result();  
  }
  
  function cek_data_sjmasuk($no_sj, $thn1, $id_unit_jahit){
    $this->db->select("id from tm_sjmasukbhnbakupic WHERE id_unit='$id_unit_jahit' AND no_sj = '$no_sj' 
				AND extract(year from tgl_sj) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savesjmasuk($id_sj, $jenis, $id_unit_jahit,
			$id_brg_wip, $nama_brg_wip, $temp_qty, $id_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
			
			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal+= $qty_warna[$xx];
			} // end for
			
			// ======== update stoknya! =============
			//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
						$data_stok = array(
							'id_brg_wip'=>$id_brg_wip,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
						
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' ");
					}
					
					//----------------------- 04-02-2014 ---------------------------------------------------
					//update stok unit jahit
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit 
								WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$id_unit_jahit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_lama = 0;
							$stok_unit_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unit_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit = $stok_unit_lama-$qtytotal; // berkurang stok karena keluar dari unit
						$new_stok_unit_bagus = $stok_unit_bagus_lama-$qtytotal;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'id_brg_wip'=>$id_brg_wip,
								'id_unit'=>$id_unit_jahit,
								'stok'=>$new_stok_unit,
								'stok_bagus'=>$new_stok_unit_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
							stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
							where id_brg_wip= '$id_brg_wip' AND id_unit='$id_unit_jahit' ");
						}
					//-------------------------------------------------------------------------------------	
							
				// jika semua data tdk kosong, insert ke tm_sjmasukbhnbakupic_detail
				$data_detail = array(
					'id_sjmasukbhnbakupic'=>$id_sj,
					'id_brg_wip'=>$id_brg_wip,
					'nama_brg_wip'=>$nama_brg_wip,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				);
				$this->db->insert('tm_sjmasukbhnbakupic_detail',$data_detail);
				
				// ambil id detail tm_sjmasukbhnbakupic_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_sjmasukbhnbakupic_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_sjmasukbhnbakupic_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_sjmasukbhnbakupic_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_sjmasukbhnbakupic_detail_warna',$tm_sjmasukbhnbakupic_detail_warna);
					
					// ========================= 04-02-2014, stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama+$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_cutting'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
					}
					
					// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit
					//if ($kode_unit_jahit != '0') {
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna 
								WHERE id_warna = '".$id_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama-$qty_warna[$xx]; // berkurang stok karena keluar unit
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama-$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							// 24-03-2014, save juga ke field stok_bagus karena termasuk keluar bagus dari unit
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna'=>$id_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_bagus'=>$new_stok_unit_warna_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
				//	} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
				} // end for
  }
  
  function deletesjmasuk($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_unit, b.* FROM tm_sjmasukbhnbakupic_detail b
									 INNER JOIN tm_sjmasukbhnbakupic a ON a.id = b.id_sjmasukbhnbakupic
									 WHERE b.id_sjmasukbhnbakupic = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok pabrik =====================
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$row2->id_brg_wip' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama-$row2->qty; // berkurang stok karena reset
						
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' WHERE id_brg_wip= '$row2->id_brg_wip' ");
									
						// ================= update stok unit jahit ==========================
					//	if ($row2->kode_unit_jahit != '0') {
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
										 WHERE id_brg_wip = '$row2->id_brg_wip' AND id_unit='$row2->id_unit' ");
							if ($query3->num_rows() == 0){
								$stok_unit_lama = 0;
								$stok_unit_bagus_lama = 0;
								$id_stok_unit = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unit_bagus_lama = $hasilrow->stok_bagus;
							}
							$new_stok_unit = $stok_unit_lama+$row2->qty; // bertambah stok karena reset dari SJ masuk
							$new_stok_unit_bagus = $stok_unit_bagus_lama+$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus',
										tgl_update_stok = '$tgl' WHERE id_brg_wip= '$row2->id_brg_wip'
										AND id_unit = '$row2->id_unit' ");
					//	} // end if update stok unit jahit
						
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_sjmasukbhnbakupic_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_sjmasukbhnbakupic_detail_warna 
												WHERE id_sjmasukbhnbakupic_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama-$rowwarna->qty; // berkurang stok karena reset dari SJ masuk
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");
																
								// ============= update stok unit jahit
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND id_warna='$rowwarna->id_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warna_bagus_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama+$rowwarna->qty; // bertambah stok di unit karena reset dari SJ masuk
									$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama+$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												stok_bagus = '$new_stok_unit_warna_bagus', 
												tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND id_warna = '$rowwarna->id_warna' ");
							}
						} // end if detail warna
												
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_sjmasukbhnbakupic_detail_warna 04-02-2014
						$this->db->query(" DELETE FROM tm_sjmasukbhnbakupic_detail_warna WHERE id_sjmasukbhnbakupic_detail='".$row2->id."' ");
						
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_sjmasukbhnbakupic
    $this->db->delete('tm_sjmasukbhnbakupic_detail', array('id_sjmasukbhnbakupic' => $id));
    $this->db->delete('tm_sjmasukbhnbakupic', array('id' => $id));
  }
  // ----------------------------------------------------------------------------------------------
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
      
  function get_brgjadi($num, $offset, $gudang, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$row1->i_product_motif'
										AND id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
			}
			else
				$stok = 0;
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname,
										'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($gudang, $cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  
  
  // 14-03-2013, SJ keluar WIP
  
  function cek_data_sjkeluarwip($no_sj){
    $this->db->select("id from tm_sjkeluarwip WHERE no_sj = '$no_sj' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAllsjkeluar($num, $offset, $cari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			if ($gudang != '0')
				$pencarian.= " AND id_gudang = '$gudang' ";
			if ($kode_unit_jahit != "0")
				$pencarian.= " AND kode_unit_jahit = '$kode_unit_jahit' ";
			if ($kode_unit_packing != "0")
				$pencarian.= " AND kode_unit_packing = '$kode_unit_packing' ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			if ($gudang != '0')
				$pencarian.= " AND id_gudang = '$gudang' ";
			if ($kode_unit_jahit != "0")
				$pencarian.= " AND kode_unit_jahit = '$kode_unit_jahit' ";
			if ($kode_unit_packing != "0")
				$pencarian.= " AND kode_unit_packing = '$kode_unit_packing' ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		$this->db->select(" * FROM tm_sjkeluarwip WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname as nama_brg FROM tr_product_motif 
										 WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$nama_brg = '';
						}
						
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $row2->ket_qty_warna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar bagus ke unit packing";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Keluar bagus ke gudang jadi";
				else if ($row1->jenis_keluar == '3')
					$nama_jenis = "Lain-lain (Retur ke unit jahit)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'keterangan'=> $row1->keterangan,
											'status_nota_retur'=> $row1->status_nota_retur,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function getAllsjkeluartanpalimit($cari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			if ($gudang != '0')
				$pencarian.= " AND id_gudang = '$gudang' ";
			if ($kode_unit_jahit != "0")
				$pencarian.= " AND kode_unit_jahit = '$kode_unit_jahit' ";
			if ($kode_unit_packing != "0")
				$pencarian.= " AND kode_unit_packing = '$kode_unit_packing' ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			if ($gudang != '0')
				$pencarian.= " AND id_gudang = '$gudang' ";
			if ($kode_unit_jahit != "0")
				$pencarian.= " AND kode_unit_jahit = '$kode_unit_jahit' ";
			if ($kode_unit_packing != "0")
				$pencarian.= " AND kode_unit_packing = '$kode_unit_packing' ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  //
 /* function savesjkeluar($no_sj,$tgl_sj, $id_gudang, $jenis, $kode_unit_jahit, $kode_unit_packing, $ket, 
			$kode, $nama, $qty, $ket_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
    // cek apa udah ada datanya blm dgn no SJ tadi
    $this->db->select("id from tm_sjkeluarwip WHERE no_sj = '".$this->db->escape_str($no_sj)."' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_sjkeluarwip
			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'jenis_keluar'=>$jenis,
			  'id_gudang'=>$id_gudang,
			  'kode_unit_jahit'=>$kode_unit_jahit,
			  'kode_unit_packing'=>$kode_unit_packing,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_sjkeluarwip',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_sjkeluarwip
			$query2	= $this->db->query(" SELECT id FROM tm_sjkeluarwip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			
				// ======== update stoknya! =============
				$nama_tabel_stok = "tm_stok_hasil_jahit";
				
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg_jadi = '$kode'
							AND id_gudang='$id_gudang' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode,
							'id_gudang'=>$id_gudang,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode' AND id_gudang='$id_gudang' ");
					}
										
				
				// jika semua data tdk kosong, insert ke tm_sjkeluarwip_detail
				$data_detail = array(
					'id_sjkeluarwip'=>$id_sj,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					'ket_qty_warna'=>$ket_warna
				);
				$this->db->insert('tm_sjkeluarwip_detail',$data_detail);
  }
    
  function deletesjkeluar($kode){
	$tgl = date("Y-m-d");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_gudang, b.* FROM tm_sjkeluarwip_detail b, tm_sjkeluarwip a 
								 WHERE a.id = b.id_sjkeluarwip AND b.id_sjkeluarwip = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ============ update stok =====================
						$nama_tabel_stok = "tm_stok_hasil_jahit";
										
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." 
											 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row2->id_gudang' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
								
								$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', 
											tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
											AND id_gudang = '$row2->id_gudang' ");
								
					} // end foreach
				} // end if
	
	// 2. hapus data tm_sjkeluarwip_detail dan tm_sjkeluarwip
    $this->db->delete('tm_sjkeluarwip_detail', array('id_sjkeluarwip' => $kode));
    $this->db->delete('tm_sjkeluarwip', array('id' => $kode));
  }
    
  function get_sjkeluar($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						
						$tabelstok = "tm_stok_hasil_jahit";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." 
									 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row1->id_gudang' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'ket_qty_warna'=> $row2->ket_qty_warna,
												'keterangan'=> $row2->keterangan,
												'jum_stok'=> $jum_stok
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar bagus ke unit packing";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Keluar bagus ke gudang jadi";
				else if ($row1->jenis_keluar == '3')
					$nama_jenis = "Lain-lain (Retur ke unit jahit)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  } */
  
  // 11-07-2013
 /* function getAllstokawal($num, $offset, $cari) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}
		
		$this->db->select(" a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.id_gudang, a.tgl_update_stok 
						FROM tm_stok_hasil_jahit a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
								
				$data_stok[] = array(		'id'=> $row1->id,	
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'stok'=> $row1->stok,
											'nama_brg_jadi'=> $row1->e_product_motifname,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $kode_gudang." - ".$nama_gudang,
											'tgl_update_stok'=> $row1->tgl_update_stok
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  function getAllstokawaltanpalimit($cari){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}
		
		$query	= $this->db->query(" SELECT a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.id_gudang, a.tgl_update_stok 
						FROM tm_stok_hasil_jahit a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ");

    return $query->result();  
  }
  
  function savestokawal($id_gudang, $kode, $nama, $qty){  
    $tgl = date("Y-m-d");
	
	$datanya = array(
			  'kode_brg_jadi'=>$kode,
			  'stok'=>$qty,
			  'id_gudang'=>$id_gudang,
			  'tgl_update_stok'=>$tgl
			);
	$this->db->insert('tm_stok_hasil_jahit',$datanya);
  }
  
  function get_brgjadi_stokawal($num, $offset, $gudang, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang') ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang')  ";		
		$sql.=" AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
										//'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjadi_stokawaltanpalimit($gudang, $cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang')";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang')
					AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  // 25-07-2013
  function get_sj_keluar2($cari, $unit_jahit)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' 
				AND status_nota_retur = 'f' order by tgl_sj DESC ";
		$this->db->select($sql, false);
	}
	else {
		$sql = " * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' 
				AND status_nota_retur = 'f' AND UPPER(no_sj) like UPPER('%$cari%') 
				order by tgl_sj DESC ";
		$this->db->select($sql, false);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {		
			
			// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1->id' 
											ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ambil nama brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
												WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else
							$nama_brg_jadi = '';
																		
						$detail_bhn[] = array('id'=> $row2->id,
												'id_sjkeluarwip'=> $row2->id_sjkeluarwip,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty
									);
						
					} 
				} 
				else {
					$detail_bhn = '';
				} // end detail
			
			$data_bhn[] = array(		'id'=> $row1->id,	
										'no_sj'=> $row1->no_sj,	
										'tgl_sj'=> $row1->tgl_sj,
										'tgl_update'=> $row1->tgl_update,
										'detail_bhn'=> $detail_bhn
								);
			
			$detail_bhn = array();
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_sj_keluar2tanpalimit($cari, $unit_jahit){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' order by tgl_sj DESC ";
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' AND UPPER(no_sj) like UPPER('%$cari%') 
				order by tgl_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_detail_sj_keluar($no_sj_keluar, $id_sj_keluar, $unit_jahit){
    $detail_sj = array();
    
    $list_id_sj = explode(",", $id_sj_keluar); 
    foreach($list_id_sj as $row1) {
		if ($row1 != '') {
			// ====================================
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1' ORDER BY id ASC ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$id_sj_keluarwip_detail	= $row2->id;
						$kode_brg_jadi	= $row2->kode_brg_jadi;
						$qty	= $row2->qty;
						
						$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sjkeluarwip WHERE id = '$row1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$no_sj	= $hasilrow->no_sj;
							$tgl_sj	= $hasilrow->tgl_sj;
							
							$pisah1 = explode("-", $tgl_sj);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							if ($bln1 == '01')
								$nama_bln = "Januari";
							else if ($bln1 == '02')
								$nama_bln = "Februari";
							else if ($bln1 == '03')
								$nama_bln = "Maret";
							else if ($bln1 == '04')
								$nama_bln = "April";
							else if ($bln1 == '05')
								$nama_bln = "Mei";
							else if ($bln1 == '06')
								$nama_bln = "Juni";
							else if ($bln1 == '07')
								$nama_bln = "Juli";
							else if ($bln1 == '08')
								$nama_bln = "Agustus";
							else if ($bln1 == '09')
								$nama_bln = "September";
							else if ($bln1 == '10')
								$nama_bln = "Oktober";
							else if ($bln1 == '11')
								$nama_bln = "November";
							else if ($bln1 == '12')
								$nama_bln = "Desember";
							$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
						}
						else {
							$no_sj	= '';
							$tgl_sj	= '';
						}
																		
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
						
						// 13-01-2014
						// ambil harga barang jahit
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_hasil_jahit
										WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_unit = '$unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$harganya	= $hasilrow->harga;
						}
						else {
							$harganya	= 0;
						}
						
						// ambil data nama unit
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
						
						$detail_sj[] = array(		'id'=> $id_sj_keluarwip_detail,
										'id_sj_keluarwip'=> $row1,
										'kode_brg_jadi'=> $kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'qty'=> $qty,
										'no_sj'=> $no_sj,
										'tgl_sj'=> $tgl_sj,
										'kode_unit'=> $unit_jahit,
										'nama_unit'=> $nama_unit,
										'harganya'=> $harganya
								);
					}
				}
			// ====================================
		
		}
	} // end foreach
	return $detail_sj;
  } */
  
  // 04-02-2014
  function get_sjmasuk($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sjmasukbhnbakupic where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjmasukbhnbakupic_detail WHERE id_sjmasukbhnbakupic = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$stok	= $hasilrow->stok;
						else
							$stok = 0;
						
						$query4	= $this->db->query(" SELECT kode_brg , nama_brg FROM tm_barang_wip WHERE id='$row2->id_brg_wip' ");
						if($query4->num_rows()>0){
							$hasilrow2	= $query4->row();
							$kode_brg_wip	= $hasilrow2->kode_brg;
							$nama_brg_wip	= $hasilrow2->nama_brg;
						}else{
							$nama_brg_wip	= '';
							$kode_brg_wip = '';
						}
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_warna dan tm_sjmasukbhnbakupic_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.id_warna, b.nama, c.qty FROM tm_warna b
												 INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_warna
												 WHERE c.id_sjmasukbhnbakupic_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$id_warna = $row3->id_warna;
								
								$detail_warna[] = array(
												'id_warna'=> $id_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else
							$detail_warna = '';
						//-------------------------------------------------------------------------------------
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'stok'=> $stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->jenis_masuk=='1')
					$nama_jenis = "Retur Bahan Baku";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_unit_jahit	= $hasilrow->kode_unit;
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else {
					$kode_unit_jahit = '';
					$nama_unit_jahit = '';
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_unit'=> $row1->id_unit,
											'kode_unit'=> $kode_unit_jahit,
											'nama_unit'=> $nama_unit_jahit,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  // -------------------06-03-2014 ---------------------------------------------------------------------------------------
  function get_warna(){
		$query = $this->db->query(" SELECT * FROM tm_warna ORDER BY kode");
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  
  function savetempwarnabrgjadi($kode_brg_jadi,$kodewarna) {
	//	$tgl = date("Y-m-d H:i:s");
		$this->db->query(" DELETE FROM tm_temp_warna_brg_jadi WHERE kode_brg_jadi = '$kode_brg_jadi' ");
		$listwarna = explode(";", $kodewarna);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_temp_warna_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_warna = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_temp_warna_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_warna	= $hasilrow->id;
						$new_id_warna = $id_warna+1;
					}
					else
						$new_id_warna = 1;
					
					$datanya	= array(
							 'id'=>$new_id_warna,
							 'kode_brg_jadi'=>$kode_brg_jadi,
							 'kode_warna'=>$row1);
					$this->db->insert('tm_temp_warna_brg_jadi',$datanya);
				} // end if
			}
		} // end foreach
	}
	
	function get_tempwarnabrgjadi_bygudang($kode_brg_jadi) {
		/*$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
							FROM tm_temp_warna_brg_jadi a, tm_warna b
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$kode_brg_jadi' ");
	
		$data_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$data_warna[] = array(		'kode_warna'=> $row1->kode_warna,	
											'nama_warna'=> $row1->nama_warna,
											'kode_brg_jadi'=> $kode_brg_jadi
											);
			} // endforeach header
		}
		else {
			$data_warna = '';
		}
		return $data_warna; */
		
		$sqlheader = $this->db->query(" SELECT distinct id_gudang FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kode_brg_jadi' ");
		if ($sqlheader->num_rows() > 0){
			$hasilheader = $sqlheader->result();
			foreach ($hasilheader as $row) {
				$id_gudang = $row->id_gudang;
				$queryxx	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id='$id_gudang' ");
				if($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->row();
					$nama_gudang	= $hasilxx->nama;
				}
				else
					$nama_gudang = '';
				
				// ambil detail warna berdasarkan brg jadi
				$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
							FROM tm_temp_warna_brg_jadi a, tm_warna b
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$kode_brg_jadi' ");
	
				$data_warna = array();
				if ($query->num_rows() > 0){
					$hasil = $query->result();
					foreach ($hasil as $row1) {
						$data_warna[] = array(		'kode_warna'=> $row1->kode_warna,	
													'nama_warna'=> $row1->nama_warna
													//'kode_brg_jadi'=> $kode_brg_jadi
													);
					} // endforeach header
				}
				else {
					$data_warna = '';
				}
				
				$data_header[] = array(		'id_gudang'=> $id_gudang,	
											'nama_gudang'=> $nama_gudang,	
											'kode_brg_jadi'=> $kode_brg_jadi,
											'data_warna'=> $data_warna
											);
				$data_warna = array();
			}
		}
		else
			$data_header = '';
		
		
		return $data_header;
  }
  
  // 07-03-2014
  function get_tempwarnabrgjadi_byunit($kode_brg_jadi) {		
		$sqlheader = $this->db->query(" SELECT distinct kode_unit FROM tm_stok_unit_jahit WHERE kode_brg_jadi='$kode_brg_jadi' ");
		if ($sqlheader->num_rows() > 0){
			$hasilheader = $sqlheader->result();
			foreach ($hasilheader as $row) {
				$kode_unit = $row->kode_unit;
				$queryxx	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$kode_unit' ");
				if($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->row();
					$nama_unit	= $hasilxx->nama;
				}
				else
					$nama_unit = '';
				
				// ambil detail warna berdasarkan brg jadi
				$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
							FROM tm_temp_warna_brg_jadi a, tm_warna b
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$kode_brg_jadi' ");
	
				$data_warna = array();
				if ($query->num_rows() > 0){
					$hasil = $query->result();
					foreach ($hasil as $row1) {
						$data_warna[] = array(		'kode_warna'=> $row1->kode_warna,	
													'nama_warna'=> $row1->nama_warna
													//'kode_brg_jadi'=> $kode_brg_jadi
													);
					} // endforeach header
				}
				else {
					$data_warna = '';
				}
				
				$data_header[] = array(		'kode_unit'=> $kode_unit,	
											'nama_unit'=> $nama_unit,	
											'kode_brg_jadi'=> $kode_brg_jadi,
											'data_warna'=> $data_warna
											);
				$data_warna = array();
			}
		}
		else
			$data_header = '';
		
		
		return $data_header;
  }
  
  function get_tempwarnabrgjadi($kode_brg_jadi) {		
		// ambil detail warna berdasarkan brg jadi
		$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
							FROM tm_temp_warna_brg_jadi a, tm_warna b
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$kode_brg_jadi' ");
	
		$data_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$data_warna[] = array(		'kode_warna'=> $row1->kode_warna,	
											'nama_warna'=> $row1->nama_warna
											//'kode_brg_jadi'=> $kode_brg_jadi
									);
			} // endforeach header
		}
		else {
			$data_warna = '';
		}
		return $data_warna;
  }
  
  function get_material_bhnbaku($kode_brg_jadi) {		
		// ambil detail bhn baku berdasarkan brg jadi
		$query	= $this->db->query(" SELECT * FROM tm_material_brg_jadi
							WHERE kode_brg_jadi = '$kode_brg_jadi' ORDER BY id ASC ");
	
		$data_material = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$kode_warna = $row1->kode_warna;
				$kode_brg = $row1->kode_brg;
				$queryxx	= $this->db->query(" SELECT nama FROM tm_warna WHERE kode='".$row1->kode_warna."' ");
				if($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->row();
					$nama_warna	= $hasilxx->nama;
				}
				else
					$nama_warna = '';
				
				// bhn baku/quilting
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					//$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
							FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
					if ($query31->num_rows() > 0){
						$hasilrow31 = $query31->row();
						$nama_brg	= $hasilrow31->nama_brg;
						//$satuan	= $hasilrow31->nama_satuan;
					}
					else {
						$nama_brg = '';
						//$satuan = '';
					}
				}
					
				$data_material[] = array(	'kode_warna'=> $kode_warna,	
											'nama_warna'=> $nama_warna,
											'kode_brg'=> $kode_brg,	
											'nama_brg'=> $nama_brg
											//'kode_brg_jadi'=> $kode_brg_jadi
									);
			} // endforeach header
		}
		else {
			$data_material = '';
		}
		return $data_material;
  }
	
	function get_stok_hasil_jahit_warna($kode_brg_jadi) {
		$sqlheader = $this->db->query(" SELECT distinct id_gudang FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kode_brg_jadi' ");
		if ($sqlheader->num_rows() > 0){
			$hasilheader = $sqlheader->result();
			foreach ($hasilheader as $row) {
				$id_gudang = $row->id_gudang;
				$queryxx	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id='$id_gudang' ");
				if($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->row();
					$nama_gudang	= $hasilxx->nama;
				}
				else
					$nama_gudang = '';
				
				// ambil detail stok tiap2 gudang berdasarkan brg jadi
				$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
							FROM tm_stok_hasil_jahit_warna a, tm_warna b, tm_stok_hasil_jahit c 
							WHERE a.kode_warna = b.kode AND a.id_stok_hasil_jahit=c.id 
							AND c.kode_brg_jadi = '$kode_brg_jadi' AND c.id_gudang='$id_gudang' ");
	
				if ($query->num_rows() > 0){
					$hasil = $query->result();
					foreach ($hasil as $row1) {
						$data_stok[] = array(		'kode_warna'=> $row1->kode_warna,	
													'nama_warna'=> $row1->nama_warna,
													'stok'=> $row1->stok,
													'kode_brg_jadi'=> $kode_brg_jadi
													);
					} // endforeach detail
				}
				else {
					$data_stok = '';
				}
				
				$data_header[] = array(		'id_gudang'=> $id_gudang,	
											'nama_gudang'=> $nama_gudang,	
											'kode_brg_jadi'=> $kode_brg_jadi,
											'data_stok'=> $data_stok
											);
				$data_stok = array();
			}
		}
		else
			$data_header = '';
		
		
		return $data_header;
  }
  
  // 07-03-2014
  function get_stok_unit_jahit_warna($kode_brg_jadi) {
		$sqlheader = $this->db->query(" SELECT distinct kode_unit FROM tm_stok_unit_jahit WHERE kode_brg_jadi='$kode_brg_jadi' ");
		if ($sqlheader->num_rows() > 0){
			$hasilheader = $sqlheader->result();
			foreach ($hasilheader as $row) {
				$kode_unit = $row->kode_unit;
				$queryxx	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$kode_unit' ");
				if($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->row();
					$nama_unit	= $hasilxx->nama;
				}
				else
					$nama_unit = '';
				
				// ambil detail stok tiap2 unit jahit berdasarkan brg jadi
				$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
							FROM tm_stok_unit_jahit_warna a, tm_warna b, tm_stok_unit_jahit c 
							WHERE a.kode_warna = b.kode AND a.id_stok_unit_jahit=c.id 
							AND c.kode_brg_jadi = '$kode_brg_jadi' AND c.kode_unit='$kode_unit' ");
	
				if ($query->num_rows() > 0){
					$hasil = $query->result();
					foreach ($hasil as $row1) {
						$data_stok[] = array(		'kode_warna'=> $row1->kode_warna,	
													'nama_warna'=> $row1->nama_warna,
													'stok'=> $row1->stok,
													'stok_bagus'=> $row1->stok_bagus,
													'stok_perbaikan'=> $row1->stok_perbaikan,
													'kode_brg_jadi'=> $kode_brg_jadi
													);
					} // endforeach detail
				}
				else {
					$data_stok = '';
				}
				
				$data_header[] = array(		'kode_unit'=> $kode_unit,	
											'nama_unit'=> $nama_unit,	
											'kode_brg_jadi'=> $kode_brg_jadi,
											'data_stok'=> $data_stok
											);
				$data_stok = array();
			}
		}
		else
			$data_header = '';
		
		
		return $data_header;
  }
  
  // 08-03-2014
  function get_stok_hasil_cutting_warna($kode_brg_jadi) {
	  // ambil stok hasil cutting warna berdasarkan brg jadi
		$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
					FROM tm_stok_hasil_cutting_warna a, tm_warna b, tm_stok_hasil_cutting c 
					WHERE a.kode_warna = b.kode AND a.id_stok_hasil_cutting=c.id 
					AND c.kode_brg_jadi = '$kode_brg_jadi' ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$data_stok[] = array(		'kode_warna'=> $row1->kode_warna,	
											'nama_warna'=> $row1->nama_warna,
											'stok'=> $row1->stok,
											'kode_brg_jadi'=> $kode_brg_jadi
											);
			} // endforeach detail
		}
		else {
			$data_stok = '';
		}
		
		return $data_stok;
  }
  
  function get_stok_hasil_cutting_bhnbaku($kode_brg_jadi) {
	  // ambil stok hasil cutting bhn baku berdasarkan brg jadi
		$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
					FROM tm_stok_hasil_cutting_bhnbaku a, tm_warna b, tm_stok_hasil_cutting c 
					WHERE a.kode_warna = b.kode AND a.id_stok_hasil_cutting=c.id 
					AND c.kode_brg_jadi = '$kode_brg_jadi' ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$kode_brg = $row1->kode_brg;
				// bhn baku/quilting
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					//$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
							FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
					if ($query31->num_rows() > 0){
						$hasilrow31 = $query31->row();
						$nama_brg	= $hasilrow31->nama_brg;
						//$satuan	= $hasilrow31->nama_satuan;
					}
					else {
						$nama_brg = '';
						//$satuan = '';
					}
				}
				
				$data_stok[] = array(		'kode_warna'=> $row1->kode_warna,	
											'nama_warna'=> $row1->nama_warna,
											'kode_brg'=> $kode_brg,	
											'nama_brg'=> $nama_brg,
											'stok'=> $row1->stok,
											'kode_brg_jadi'=> $kode_brg_jadi
											);
			} // endforeach detail
		}
		else {
			$data_stok = '';
		}
		
		return $data_stok;
  }
  
  function get_tempmaterialbrgjadi($kode_brg_jadi) {		
		// ambil temp material brg jadi
		$query	= $this->db->query(" SELECT a.* FROM tm_temp_material_brg_jadi a, tm_stok_hasil_cutting b
							WHERE a.kode_brg_jadi=b.kode_brg_jadi AND a.kode_brg_jadi = '$kode_brg_jadi' ORDER BY id ASC ");
	
		$data_material = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$kode_warna = $row1->kode_warna;
				$kode_brg = $row1->kode_brg;
				$queryxx	= $this->db->query(" SELECT nama FROM tm_warna WHERE kode='".$row1->kode_warna."' ");
				if($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->row();
					$nama_warna	= $hasilxx->nama;
				}
				else
					$nama_warna = '';
				
				// bhn baku/quilting
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					//$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
							FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
					if ($query31->num_rows() > 0){
						$hasilrow31 = $query31->row();
						$nama_brg	= $hasilrow31->nama_brg;
						//$satuan	= $hasilrow31->nama_satuan;
					}
					else {
						$nama_brg = '';
						//$satuan = '';
					}
				}
					
				$data_material[] = array(	'kode_warna'=> $kode_warna,	
											'nama_warna'=> $nama_warna,
											'kode_brg'=> $kode_brg,	
											'nama_brg'=> $nama_brg
											//'kode_brg_jadi'=> $kode_brg_jadi
									);
			} // endforeach header
		}
		else {
			$data_material = '';
		}
		return $data_material;
  }
  
  function get_tempwarnabrgjadi_hasilcutting($kode_brg_jadi) {		
		// ambil detail warna berdasarkan brg jadi
		$query	= $this->db->query(" SELECT a.*, b.nama as nama_warna
							FROM tm_temp_warna_brg_jadi a, tm_warna b, tm_stok_hasil_cutting c
							WHERE a.kode_warna = b.kode AND c.kode_brg_jadi = a.kode_brg_jadi
							AND a.kode_brg_jadi = '$kode_brg_jadi' ");
	
		$data_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$data_warna[] = array(		'kode_warna'=> $row1->kode_warna,	
											'nama_warna'=> $row1->nama_warna
											//'kode_brg_jadi'=> $kode_brg_jadi
									);
			} // endforeach header
		}
		else {
			$data_warna = '';
		}
		return $data_warna;
  }
  
  // 10-03-2014
  function saveallpengubahanwarna($kode_brg_jadi){  
    $tgl = date("Y-m-d H:i:s");
    
    // 1. query ke tm_temp_warna_brg_jadi
    $query	= $this->db->query(" SELECT * FROM tm_temp_warna_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' ORDER BY kode_warna ASC ");
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		$this->db->query(" DELETE FROM tm_warna_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' ");
		
		foreach ($hasil as $row1) {
			$kode_warna = $row1->kode_warna;
			$seqxx	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi ORDER BY id DESC LIMIT 1 ");
			if($seqxx->num_rows() > 0) {
				$seqrowxx	= $seqxx->row();
				$id_warna	= $seqrowxx->id+1;
			}else{
				$id_warna	= 1;
			}
			
			$dataxx = array(
								'id'=>$id_warna,
								'kode_brg_jadi'=>$kode_brg_jadi,
								'kode_warna'=>$kode_warna,
								'tgl_input'=>$tgl,
								'tgl_update'=>$tgl
								);
			$this->db->insert('tm_warna_brg_jadi', $dataxx);
		}
	} // end if 1
	
	// 2. query ke tm_temp_stok_hasil_jahit_warna
	$query	= $this->db->query(" SELECT distinct id_gudang FROM tm_temp_stok_hasil_jahit_warna WHERE kode_brg_jadi='$kode_brg_jadi' ");
	/*$query	= $this->db->query(" SELECT * FROM tm_temp_stok_hasil_jahit_warna WHERE kode_brg_jadi='$kode_brg_jadi' 
						ORDER BY id_gudang ASC, kode_warna ASC "); */
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		
		foreach ($hasil as $row1) {
			$id_gudang = $row1->id_gudang;
			
			// ambil id_stok_hasil_jahit
			$queryxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$kode_brg_jadi'
							AND id_gudang='$id_gudang' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_stok_hasil_jahit	= $hasilxx->id;
				
				$this->db->query(" DELETE FROM tm_stok_hasil_jahit_warna WHERE id_stok_hasil_jahit='$id_stok_hasil_jahit' ");
			}			
			
			$query2 = $this->db->query(" SELECT * FROM tm_temp_stok_hasil_jahit_warna WHERE id_gudang='$id_gudang'
									AND kode_brg_jadi='$kode_brg_jadi' ");
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->result();
				$stoktotal=0;
				
				foreach ($hasil2 as $row2) {
					$kode_warna = $row2->kode_warna;
					$stok = $row2->stok;
					$stoktotal+=$stok;
					
					$seqxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
					if($seqxx->num_rows() > 0) {
						$seqrowxx	= $seqxx->row();
						$id_stok_warna	= $seqrowxx->id+1;
					}else{
						$id_stok_warna	= 1;
					}
					
					$dataxx = array(
										'id'=>$id_stok_warna,
										'id_stok_hasil_jahit'=>$id_stok_hasil_jahit,
										'id_warna_brg_jadi'=>'0',
										'kode_warna'=>$kode_warna,
										'stok'=>$stok,
										'tgl_update_stok'=>$tgl
										);
					$this->db->insert('tm_stok_hasil_jahit_warna', $dataxx);
				} // end for
			} // end if
			
			// update stoktotal di tm_stok_hasil_jahit
			$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$stoktotal', tgl_update_stok='$tgl' 
							WHERE kode_brg_jadi='$kode_brg_jadi' AND id_gudang='$id_gudang' ");
			
		} // end for kepala
	} // end if 2
	
	// 3. query ke tm_temp_stok_unit_jahit_warna
	$query	= $this->db->query(" SELECT distinct kode_unit FROM tm_temp_stok_unit_jahit_warna WHERE kode_brg_jadi='$kode_brg_jadi' ");
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		
		foreach ($hasil as $row1) {
			$kode_unit = $row1->kode_unit;
			
			// ambil id_stok_unit_jahit
			$queryxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode_brg_jadi'
							AND kode_unit='$kode_unit' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$id_stok_unit_jahit	= $hasilxx->id;
				
				$this->db->query(" DELETE FROM tm_stok_unit_jahit_warna WHERE id_stok_unit_jahit='$id_stok_unit_jahit' ");
			}			
			
			$query2 = $this->db->query(" SELECT * FROM tm_temp_stok_unit_jahit_warna WHERE kode_unit='$kode_unit'
									AND kode_brg_jadi='$kode_brg_jadi' ");
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->result();
				$stoktotal=0;
				$stokbagustotal=0; $stokperbaikantotal=0;
				
				foreach ($hasil2 as $row2) {
					$kode_warna = $row2->kode_warna;
					//$stok = $row2->stok;
					$stok_bagus = $row2->stok_bagus;
					//$stoktotal+=$stok;
					$stokbagustotal+=$stok_bagus;
					
					$stok_perbaikan = $row2->stok_perbaikan;
					$stokperbaikantotal+=$stok_perbaikan;
					
					$totalxx = $stok_bagus+$stok_perbaikan;
					
					$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
					if($seqxx->num_rows() > 0) {
						$seqrowxx	= $seqxx->row();
						$id_stok_warna	= $seqrowxx->id+1;
					}else{
						$id_stok_warna	= 1;
					}
					
					$dataxx = array(
										'id'=>$id_stok_warna,
										'id_stok_unit_jahit'=>$id_stok_unit_jahit,
										'id_warna_brg_jadi'=>'0',
										'kode_warna'=>$kode_warna,
										//'stok'=>$stok,
										'stok'=>$totalxx,
										'stok_bagus'=>$stok_bagus,
										'stok_perbaikan'=>$stok_perbaikan,
										'tgl_update_stok'=>$tgl
										);
					$this->db->insert('tm_stok_unit_jahit_warna', $dataxx);
				} // end for
			} // end if
			
			$totalxx = $stokbagustotal+$stokperbaikantotal;
			// update stoktotal di tm_stok_unit_jahit
			$this->db->query(" UPDATE tm_stok_unit_jahit SET stok='$totalxx', stok_bagus='$stokbagustotal',
							stok_perbaikan='$stokperbaikantotal', tgl_update_stok='$tgl' 
							WHERE kode_brg_jadi='$kode_brg_jadi' AND kode_unit='$kode_unit' ");
			
		} // end for kepala
	} // end if 3
	
	// 4. query ke tm_temp_material_brg_jadi
	$query	= $this->db->query(" SELECT * FROM tm_temp_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' ");
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		
		foreach ($hasil as $row1) {
			$kode_brg = $row1->kode_brg;
			$kode_warna = $row1->kode_warna;
			
			$this->db->query(" UPDATE tm_material_brg_jadi SET kode_warna='$kode_warna' 
							WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jadi' ");
		}
	} // end if 4
	
	// 5. query ke tm_temp_stok_hasil_cutting_warna
	// ambil id_stok_hasil_cutting
	$queryxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' ");
	if ($queryxx->num_rows() > 0){
		$hasilxx = $queryxx->row();
		$id_stok_hasil_cutting	= $hasilxx->id;
				
		$this->db->query(" DELETE FROM tm_stok_hasil_cutting_warna WHERE id_stok_hasil_cutting='$id_stok_hasil_cutting' ");
	}			
			
	$query2 = $this->db->query(" SELECT * FROM tm_temp_stok_hasil_cutting_warna WHERE kode_brg_jadi='$kode_brg_jadi' ");
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
		$stoktotal=0;
				
		foreach ($hasil2 as $row2) {
			$kode_warna = $row2->kode_warna;
			$stok = $row2->stok;
			$stoktotal+=$stok;
				
			$seqxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
			if($seqxx->num_rows() > 0) {
				$seqrowxx	= $seqxx->row();
				$id_stok_warna	= $seqrowxx->id+1;
			}else{
				$id_stok_warna	= 1;
			}
					
			$dataxx = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_cutting'=>$id_stok_hasil_cutting,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna,
								'stok'=>$stok,
								'tgl_update_stok'=>$tgl
								);
			$this->db->insert('tm_stok_hasil_cutting_warna', $dataxx);
		} // end for
	} // end if
			
	// update stoktotal di tm_stok_hasil_cutting
	$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok='$stoktotal', tgl_update_stok='$tgl' 
					WHERE kode_brg_jadi='$kode_brg_jadi' ");
	// end query 5
		
	// 6. query ke tm_temp_stok_hasil_cutting_bhnbaku
	// ambil id_stok_hasil_cutting
	$queryxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' ");
	if ($queryxx->num_rows() > 0){
		$hasilxx = $queryxx->row();
		$id_stok_hasil_cutting	= $hasilxx->id;
				
		$this->db->query(" DELETE FROM tm_stok_hasil_cutting_bhnbaku WHERE id_stok_hasil_cutting='$id_stok_hasil_cutting' ");
	}			
			
	$query2 = $this->db->query(" SELECT * FROM tm_temp_stok_hasil_cutting_bhnbaku WHERE kode_brg_jadi='$kode_brg_jadi' ");
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
		$stoktotal=0;
				
		foreach ($hasil2 as $row2) {
			$kode_warna = $row2->kode_warna;
			$kode_brg = $row2->kode_brg;
			$stok = $row2->stok;
			$stoktotal+=$stok;
				
			$seqxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
			if($seqxx->num_rows() > 0) {
				$seqrowxx	= $seqxx->row();
				$id_stok_warna	= $seqrowxx->id+1;
			}else{
				$id_stok_warna	= 1;
			}
					
			$dataxx = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_cutting'=>$id_stok_hasil_cutting,
								'kode_warna'=>$kode_warna,
								'kode_brg'=>$kode_brg,
								'stok'=>$stok,
								'tgl_update_stok'=>$tgl
								);
			$this->db->insert('tm_stok_hasil_cutting_bhnbaku', $dataxx);
		} // end for
	} // end if
	// end query 6
    
    // finally, hapus semua data di tabel temp berdasarkan kode brg jadi tsb
    $this->db->query(" DELETE FROM tm_temp_warna_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' ");
    $this->db->query(" DELETE FROM tm_temp_stok_hasil_jahit_warna WHERE kode_brg_jadi='$kode_brg_jadi' ");
    $this->db->query(" DELETE FROM tm_temp_stok_unit_jahit_warna WHERE kode_brg_jadi='$kode_brg_jadi' ");
    $this->db->query(" DELETE FROM tm_temp_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' ");
    $this->db->query(" DELETE FROM tm_temp_stok_hasil_cutting_warna WHERE kode_brg_jadi='$kode_brg_jadi' ");
    $this->db->query(" DELETE FROM tm_temp_stok_hasil_cutting_bhnbaku WHERE kode_brg_jadi='$kode_brg_jadi' ");
    // ============================================= END ==========================================================
	

  }
	// --------------------------------------------------------------------------------------------------------------
	
	// 12-10-2015 dari BLN
  function getAllbonmkeluarcutting($num, $offset, $cari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND (UPPER(a.no_bonm) like UPPER('%$cari%') OR UPPER(a.no_manual) like UPPER('%$cari%'))";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit = '$id_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_bonm DESC, a.no_bonm DESC ";
		
		$this->db->select(" distinct a.* FROM tm_bonmkeluarcutting a 
					INNER JOIN tm_bonmkeluarcutting_detail b ON a.id=b.id_bonmkeluarcutting
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%$caribrg%') OR UPPER(a.nama_brg_wip) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_bonmkeluarcutting_detail a 
									INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id
									WHERE a.id_bonmkeluarcutting = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {													
							// 30-01-2014, detail qty warna -------------
							$strwarna = "";
							$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_bonmkeluarcutting_detail_warna a
										INNER JOIN tm_warna c ON a.id_warna = c.id
										WHERE a.id_bonmkeluarcutting_detail = '$row2->id' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx=$queryxx->result();
								$strwarna.= $row2->kode_brg.": <br>";
								foreach ($hasilxx as $rowxx) {
									$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
								}
							}
							//-------------------------------------------
										
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_bonm = '';
				}
																
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar Bagus";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Pengembalian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_unit_jahit	= $hasilrow->kode_unit;
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else {
					$kode_unit_jahit = '';
					$nama_unit_jahit = '';
				}
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_unit'=> $row1->id_unit,
											'kode_unit'=> $kode_unit_jahit,
											'nama_unit'=> $nama_unit_jahit,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function getAllbonmkeluarcuttingtanpalimit($cari, $date_from, $date_to, $id_unit_jahit, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND (UPPER(a.no_bonm) like UPPER('%$cari%') OR UPPER(a.no_manual) like UPPER('%$cari%'))";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
		if ($id_unit_jahit != "0")
			$pencarian.= " AND a.id_unit = '$id_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
	  
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_bonmkeluarcutting a 
					INNER JOIN tm_bonmkeluarcutting_detail b ON a.id=b.id_bonmkeluarcutting
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE TRUE ".$pencarian);

    return $query->result();  
  }
  
  // 15-10-2015
  function cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $id_unit_jahit, $jenis){
    $this->db->select("id from tm_bonmkeluarcutting WHERE no_manual = '$no_bonm_manual' 
				AND extract(year from tgl_bonm) = '$thn1' AND id_unit='$id_unit_jahit'
				AND jenis_keluar = '$jenis' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savebonmkeluarcutting($id_bonm, $jenis, $id_unit_jahit,
			$id_brg_wip, $nama_brg_wip, $temp_qty, $id_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
			
			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal+= $qty_warna[$xx];
			} // end for
			
			// ======== update stoknya! =============
			//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
						$data_stok = array(
							'id_brg_wip'=>$id_brg_wip,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
						
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' ");
					}
					
					//----------------------- 03-02-2014 ---------------------------------------------------
					//update stok unit jahit
					//if ($kode_unit_jahit != '0') {
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit WHERE id_brg_wip = '$id_brg_wip'
								AND id_unit='$id_unit_jahit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_lama = 0;
							$stok_unit_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unit_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit = $stok_unit_lama+$qtytotal; // bertambah stok karena masuk ke unit
						$new_stok_unit_bagus = $stok_unit_bagus_lama+$qtytotal;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'id_brg_wip'=>$id_brg_wip,
								'id_unit'=>$id_unit_jahit,
								'stok'=>$new_stok_unit,
								'stok_bagus'=>$new_stok_unit_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
							stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
							where id_brg_wip= '$id_brg_wip' AND id_unit='$id_unit_jahit' ");
						}
					//} // end if
					//-------------------------------------------------------------------------------------	
							
				// jika semua data tdk kosong, insert ke tm_bonmkeluarcutting_detail
				$data_detail = array(
					'id_bonmkeluarcutting'=>$id_bonm,
					'id_brg_wip'=>$id_brg_wip,
					'nama_brg_wip'=>$nama_brg_wip,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				);
				$this->db->insert('tm_bonmkeluarcutting_detail',$data_detail);
				
				// ambil id detail tm_bonmkeluarcutting_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_bonmkeluarcutting_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_bonmkeluarcutting_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_bonmkeluarcutting_detail_warna',$tm_bonmkeluarcutting_detail_warna);
					
					// ========================= 04-02-2014, stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_cutting'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
					}
					
					// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit
					//if ($kode_unit_jahit != '0') {
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna WHERE id_warna = '".$id_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama+$qty_warna[$xx]; // bertambah stok karena masuk ke unit
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna'=>$id_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_bagus'=>$new_stok_unit_warna_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
				//	} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
					
				} // end for
  }
  
  function deletebonmkeluarcutting($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
			/* SELECT a.id_gudang, a.kode_unit_jahit, b.* 
								 FROM tm_sjkeluarwip_detail b, tm_sjkeluarwip a 
								 WHERE a.id = b.id_sjkeluarwip AND b.id_sjkeluarwip = '$kode' */
				$query2	= $this->db->query(" SELECT a.id_unit, b.*  FROM tm_bonmkeluarcutting_detail b 
							INNER JOIN tm_bonmkeluarcutting a ON a.id = b.id_bonmkeluarcutting WHERE b.id_bonmkeluarcutting = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok pabrik =====================
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$row2->id_brg_wip' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
						
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' WHERE id_brg_wip= '$row2->id_brg_wip' ");
									
						// ================= update stok unit jahit ==========================
					//	if ($row2->kode_unit_jahit != '0') {
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
										 WHERE id_brg_wip = '$row2->id_brg_wip' AND id_unit='$row2->id_unit' ");
							if ($query3->num_rows() == 0){
								$stok_unit_lama = 0;
								$id_stok_unit = 0;
								$stok_unit_bagus_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unit_bagus_lama = $hasilrow->stok_bagus;
							}
							$new_stok_unit = $stok_unit_lama-$row2->qty; // berkurang stok karena reset dari bon M keluar
							$new_stok_unit_bagus = $stok_unit_bagus_lama-$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
										WHERE id_brg_wip= '$row2->id_brg_wip'
										AND id_unit = '$row2->id_unit' ");
					//	} // end if update stok unit jahit
						
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_bonmkeluarcutting_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna 
												WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari bon M keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");
								
								// ============= update stok unit jahit
							//	if ($row2->kode_unit_jahit != '0') {
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND id_warna='$rowwarna->id_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warna_bagus_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama-$rowwarna->qty; // berkurang stok di unit karena reset dari bon M keluar
									$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama-$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
												WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND id_warna = '$rowwarna->id_warna' ");
							//	} // end if kode_unit_jahit != 0
							}
						} // end if detail warna
						
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_bonmkeluarcutting_detail_warna 04-02-2014
						$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail='".$row2->id."' ");
										
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_bonmkeluarcutting
    $this->db->delete('tm_bonmkeluarcutting_detail', array('id_bonmkeluarcutting' => $id));
    $this->db->delete('tm_bonmkeluarcutting', array('id' => $id));
  }
  
  function get_bonmkeluarcutting($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_bonmkeluarcutting_detail a 
								INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id WHERE id_bonmkeluarcutting = '$row1->id' 
								ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$stok	= $hasilrow->stok;
						else
							$stok = 0;
												
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_warna dan tm_bonmkeluarcutting_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.id_warna, b.nama, c.qty FROM tm_warna b
												INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_warna
												WHERE c.id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$id_warna = $row3->id_warna;
								
								$detail_warna[] = array(	'id_warna'=> $id_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else
							$detail_warna = '';
						//-------------------------------------------------------------------------------------
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'stok'=> $stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->jenis_keluar=='1')
					$nama_jenis = "Keluar Bagus";
				else if ($row1->jenis_keluar=='2')
					$nama_jenis = "Pengembalian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_unit_jahit	= $hasilrow->kode_unit;
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else {
					$kode_unit_jahit = '';
					$nama_unit_jahit = '';
				}
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											//'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_unit'=> $row1->id_unit,
											'kode_unit'=> $kode_unit_jahit,
											'nama_unit'=> $nama_unit_jahit,
											'bln_forecast'=>  $row1->bln_forecast,
											'detail_bonm'=> $detail_bonm,
											'thn_forecast'=> $row1->thn_forecast
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
}
