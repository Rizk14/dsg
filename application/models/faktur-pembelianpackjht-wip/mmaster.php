<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  

  
  function get_unit_packing(){
	
	$query	= $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit ");    
    return $query->result();  
  }  
  
  function get_unit_jahit(){
	
	$query	= $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit ");    
    return $query->result();  
  }  
   function get_pembeliantanpalimit($jnsaction, $unit_packing,$unit_jahit, $no_fakturnya, $cari){
	if ($unit_packing != 0 || $unit_jahit != 0){
	if ($cari == "all") {
		
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE status_faktur = 'f' AND status_aktif = 't'
							 AND id_unit_packing='$unit_packing' AND id_unit_jahit='$unit_jahit'");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE (status_faktur = 'f'  AND status_aktif = 't'
									OR no_faktur = '$no_fakturnya')  AND id_unit_packing = '$unit_packing'  AND id_unit_jahit='$unit_jahit' ");
			}
		}
	else{
	
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE status_faktur = 'f'  AND status_aktif = 't' AND id_unit_packing = '$unit_packing' 
								AND id_unit_jahit = '$unit_jahit' 
								AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE (status_faktur = 'f'  AND status_aktif = 't' OR no_faktur = '$no_fakturnya') 
									AND id_unit_packing = '$unit_packing' AND id_unit_jahit='$unit_jahit'
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
	
}
else{
	if ($cari == "all") {
		
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE status_faktur = 'f' AND status_aktif = 't'
							");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE (status_faktur = 'f'  AND status_aktif = 't'
									OR no_faktur = '$no_fakturnya')   ");
			}
		}
	else{
	
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE status_faktur = 'f'  AND status_aktif = 't'
								
								AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE (status_faktur = 'f'  AND status_aktif = 't' OR no_faktur = '$no_fakturnya') 
								
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
	
	
	}

    return $query->result();  
  }
  
  function get_pembelian($jnsaction, $unit_packing,$cunit_jahit,  $no_fakturnya, $cari) {
	  // ambil data pembelian 
	if ($cari == "all") {
		
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE status_faktur = 'f'  AND status_aktif = 't'
							 AND id_unit_packing='$unit_packing' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE (status_faktur = 'f'   AND status_aktif = 't'
									OR no_faktur = '$no_fakturnya')  AND id_unit_packing = '$unit_packing' ");
			}
		}
	else{
	
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE status_faktur = 'f'  AND status_aktif = 't' AND id_unit_packing = '$unit_packing' 
								
								AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip WHERE (status_faktur = 'f'  AND status_aktif = 't' OR no_faktur = '$no_fakturnya') 
									AND id_unit_packing = '$unit_packing'
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
//print_r($row1);
				
				$query2	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit_packing' ");
				$hasilrow = $query2->row();
				$kode_unit_packing	= $hasilrow->kode_unit;
				$nama_unit_packing	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'id_unit_packing'=> $row1->id_unit_packing,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'no_sjmasukpembelianpackjht'=> $row1->no_sjmasukpembelianpackjht,
											'tgl_sjpembelianpackjht'=> $row1->tgl_sjpembelianpackjht,
											'tgl_update'=> $row1->tgl_update,
											'total'=> $row1->total,
											'total_pajak'=> $row1->total_pajak
											
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
    function cek_data($no_fp, $unit_packing,$unit_jahit){
    $this->db->select("id from tm_pembelianpackjht_wip_nofaktur WHERE no_faktur = '$no_fp' 
    AND id_unit_packing = '$unit_packing' AND id_unit_jahit = '$unit_jahit'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function save($no_fp, $tgl_fp, $unit_packing,$kode_unit_packing,$nama_unit_packing,
				$unit_jahit,$kode_unit_jahit,$nama_unit_jahit, $jum, $id_sj){  
    //$tgl = date("Y-m-d");
    $tgl = date("Y-m-d H:i:s");
	$list_sj = explode(";", $id_sj); 
	$uid_update_by = $this->session->userdata('uid');

	$data_header = array(
			  'no_faktur'=>$no_fp,
			  'tgl_faktur'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'id_unit_packing'=>$unit_packing,
			'kode_unit_packing'=>$kode_unit_packing,
			'nama_unit_packing'=>$nama_unit_packing,
			'kode_unit_jahit'=>$kode_unit_jahit,
			'nama_unit_jahit'=>$nama_unit_jahit,
			  'id_unit_jahit'=>$unit_jahit,
			  'jumlah'=>$jum,
			  'uid_update_by'=>$uid_update_by
			);
	$this->db->insert('tm_pembelianpackjht_wip_nofaktur',$data_header);
	
	// ambil data terakhir di tabel tm_pembelian_nofaktur
	$query2	= $this->db->query(" SELECT id FROM tm_pembelianpackjht_wip_nofaktur ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_pembelianwippackjht_nofaktur'=>$id_pf,
			  'id_sj_pembelianwippackjht'=>$row1,
			  'no_sj'=>''
			);
			$this->db->insert('tm_pembelianpackjht_wip_nofaktur_sj',$data_detail);
			
			
			$this->db->query(" UPDATE tm_pembelianpackjht_wip SET no_faktur = '$no_fp', status_faktur = 't'
								WHERE id = '$row1' ");
		}
	}
   
  }
function getAlltanpalimit( $status_lunas, $unit_packing,$unit_jahit, $cari, $date_from, $date_to){

	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($unit_packing != '0')
			$pencarian.= " AND id_unit_packing = '$unit_packing' ";
		if ($unit_jahit != '0')
			$pencarian.= " AND id_unit_jahit = '$unit_jahit' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($unit_packing != '0')
			$pencarian.= " AND id_unit_packing = '$unit_packing' ";
		if ($unit_jahit != '0')
			$pencarian.= " AND id_unit_jahit = '$unit_jahit' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$query = $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip_nofaktur WHERE TRUE ".$pencarian);
    
    return $query->result();  
  }
  
  function getAll($num, $offset, $status_lunas, $unit_packing,$unit_jahit, $cari, $date_from, $date_to) {
	//09-05-2012
	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($unit_packing != '0')
			$pencarian.= " AND id_unit_packing = '$unit_packing' ";
			if ($unit_jahit != '0')
			$pencarian.= " AND id_unit_jahit = '$unit_jahit' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($unit_packing != '0')
			$pencarian.= " AND id_unit_packing = '$unit_packing' ";
			if ($unit_jahit != '0')
			$pencarian.= " AND id_unit_jahit = '$unit_jahit' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$this->db->select(" * FROM tm_pembelianpackjht_wip_nofaktur WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			
				$query3	= $this->db->query(" SELECT * from tm_pembelianpackjht_wip_nofaktur 
								WHERE id = '$row1->id' ");
				if ($query3->num_rows() > 0) {
					$hasilrow3 = $query3->row();
					$kode_unit_packing	= $hasilrow3->kode_unit_packing;
					$nama_unit_packing	= $hasilrow3->nama_unit_packing;
					$kode_unit_jahit	= $hasilrow3->kode_unit_jahit;
					$nama_unit_jahit	= $hasilrow3->nama_unit_jahit;
				}
				else {
					$kode_unit_packing	= '';
					$nama_unit_packing	= '';
					$kode_unit_jahit	= '';
					$nama_unit_jahit	= '';
				}
				
				// ambil detail data no & tgl SJ
				// 25-06-2015, a.no_sj ga dipake
				$query2	= $this->db->query(" SELECT a.id_sj_pembelianwippackjht FROM tm_pembelianpackjht_wip_nofaktur_sj a
							INNER JOIN tm_pembelianpackjht_wip_nofaktur b ON a.id_pembelianwippackjht_nofaktur = b.id
							WHERE a.id_pembelianwippackjht_nofaktur = '$row1->id' ");
				//AND b.no_faktur = '$row1->no_faktur' AND b.kode_unit_packing = '$kode_unit_packing'
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$id_sj_pembelianwippackjht = $row2->id_sj_pembelianwippackjht; 
					
							$sql = "SELECT no_sjmasukpembelianpackjht, tgl_sjpembelianpackjht from tm_pembelianpackjht_wip where id = '$id_sj_pembelianwippackjht' ";
						
						$query3	= $this->db->query($sql);

					//	if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_sjpembelianpackjht	= $hasilrow3->tgl_sjpembelianpackjht;
							$no_sjmasukpembelianpackjht	= $hasilrow3->no_sjmasukpembelianpackjht;
							
						$pisah1 = explode("-", $tgl_sjpembelianpackjht);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sjpembelian = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_sjmasukpembelianpackjht'=> $no_sjmasukpembelianpackjht,
											'tgl_sjpembelianpackjht'=> $tgl_sjpembelianpackjht,
											'id_sj_pembelianwippackjht'=> $id_sj_pembelianwippackjht
											);
											
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
						$exptgl1 = explode(" ", $tgl1);
						$tgl1nya= $exptgl1[0];
						$jam1nya= $exptgl1[1];
						$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,	
											'no_faktur'=> $row1->no_faktur,	
											'tgl_faktur'=> $tgl_faktur,	
											'id_unit_packing'=> $row1->id_unit_packing,
											'kode_unit_packing'=> $kode_unit_packing,	
											'nama_unit_packing'=> $nama_unit_packing,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,	
											'nama_unit_jahit'=> $nama_unit_jahit,	
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb,
											'status_faktur_pajak'=> $row1->status_faktur_pajak,
											'status_lunas'=> $row1->status_lunas
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  function delete($id){    	
	//semua no_sj di tabel tm_pembelian yg bersesuaian dgn tm_pembelian_nofaktur dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.id_unit_packing,a.id_unit_jahit, b.no_sj, b.id_sj_pembelianwippackjht FROM tm_pembelianpackjht_wip_nofaktur a 
							INNER JOIN tm_pembelianpackjht_wip_nofaktur_sj b ON a.id = b.id_pembelianwippackjht_nofaktur
							WHERE a.id = '$id' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			
				$this->db->query(" UPDATE tm_pembelianpackjht_wip SET status_faktur = 'f', no_faktur = '' 
							WHERE id = '$row1->id_sj_pembelianwippackjht' ");
		
			
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di pembelian_nofaktur dan pembelian_nofaktur_sj
	$this->db->query(" DELETE FROM tm_pembelianpackjht_wip_nofaktur_sj where id_pembelianwippackjht_nofaktur= '$id' ");
	$this->db->query(" DELETE FROM tm_pembelianpackjht_wip_nofaktur where id = '$id' ");

  }
  
    function get_faktur($id_faktur){
	$query	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip_nofaktur where id='$id_faktur' ");    
	
	$data_faktur = array();
	$detail_sj = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT id_sj_pembelianwippackjht FROM tm_pembelianpackjht_wip_nofaktur_sj 
									WHERE id_pembelianwippackjht_nofaktur = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sjmasukpembelianpackjht = ''; $id_sj = '';
				foreach ($hasil2 as $row2) {
					// 10-06-2014
					// 23-06-2015 dikomen ifnya
					//if ($row2->id_sj_pembelianwippackjht != 0) {
						$sqlxx = " SELECT id, no_sjmasukpembelianpackjht FROM tm_pembelianpackjht_wip WHERE id='$row2->id_sj_pembelianwippackjht' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$no_sjmasukpembelianpackjht .= $hasilxx->no_sjmasukpembelianpackjht.",";
							$id_sj .= $hasilxx->id.";";
						}

				}
				
				$pisah1 = explode("-", $row1->tgl_faktur);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_faktur = $tgl1."-".$bln1."-".$thn1;						
			
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'id_unit_packing'=> $row1->id_unit_packing,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_sjmasukpembelianpackjht'=> $no_sjmasukpembelianpackjht,
											'id_sj'=> $id_sj
											);
			} // endforeach header
	}
    return $data_faktur;
  }
    
	
}
