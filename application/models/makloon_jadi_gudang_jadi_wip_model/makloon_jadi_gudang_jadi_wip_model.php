<?php
class Makloon_jadi_gudang_jadi_wip_model extends MY_Model
{
	protected $_per_page = 10;
    protected $_tabel = 'tb_makloon_baju_gudang_jadi_2_wip';
    protected $form_rules = array(
        array(
            'field' => 'no_sj',
            'label' => 'Nomor SJ',
            'rules' => 'trim|required|max_length[64]'
        ),
        
         array(
            'field' => 'jenis_masuk',
            'label' => 'jenis_masuk',
            'rules' => 'trim|required|max_length[16]'
        ),
          array(
            'field' => 'tanggal_sj',
            'label' => 'Tanggal SJ',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'unit_jahit',
            'label' => 'Unit Jahit',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'unit_packing',
            'label' => 'Unit Packing',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'gudang_masuk',
            'label' => 'Gudang Masuk',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'gudang_keluar',
            'label' => 'Gudang Keluar',
            'rules' => 'trim|required|max_length[16]'
        ),
         array(
            'field' => 'keterangan_header',
            'label' => 'Keterangan Header',
            'rules' => 'trim|required|max_length[16]'
        ),
        
        
    );

     public $default_values = array(
		'id'	=>	'',
		'num'		=>1,
		'no_sj' => '',
        'tgl_sj' => '',
		'jenis_masuk' => '',
		'tanggal_sj'=>'',
		'keterangan_header'=>'',
    );
    
     public function input_header($no_sj,$jenis_masuk,$unit_jahit,$unit_packing, $id_gudang_masuk,$id_gudang_keluar,$keterangan,$tanggal_sj)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 
		$data= array(
		'no_sj' => $no_sj,
		'jenis_masuk' => $jenis_masuk,
		'id_gudang_masuk' => $id_gudang_masuk,
		'id_gudang_keluar' => $id_gudang_keluar,
		'created_at'=> $created_at,
		'updated_at'=> $updated_at,
		'keterangan'=> $keterangan,
		'tanggal_sj'=> $tanggal_sj,
		);
		
        $header= $this->db->insert('tb_makloon_baju_gudang_jadi_2_wip',$data);
        
        $query = $this->db->query(" SELECT id from tb_makloon_baju_gudang_jadi_2_wip order by id desc limit 1");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
    

    }
    
    public function input_detail($id,$id_barang_wip,$qty,$keterangan,$id_gudang_masuk,$id_gudang_keluar,$tanggal_sj)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 $created_by = $updated_by = $this->session->userdata('pengguna_id');
		$data= array(
		'id_makloon_baju_gudang_jadi_2_wip' => $id,
		'id_barang_wip' => $id_barang_wip,
		'qty' => $qty,
		'created_at'=> $created_at,
		'updated_at'=> $updated_at,
		'keterangan'=> $keterangan
		);
        $detail= $this->db->insert('tb_makloon_baju_gudang_jadi_2_wip_detail',$data);
        
        $this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang_keluar' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok-$qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
        
	}
	else{
		$data_stok= array(
		'id_barang_wip' => $id_barang_wip,
		'stok' => -$qty,
		'id_gudang'=> $id_gudang_keluar,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		);
        $detail= $this->db->insert('tb_stok_gudang_jadi_1',$data_stok);		
		}
		
	 $this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang_masuk' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipxw=$hasilrow->id_barang_wip;
			$id_gudangxw=$hasilrow->id_gudang;
			$stokw=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stokw+$qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
        
	}
	else{
		$data_stok= array(
		'id_barang_wip' => $id_barang_wip,
		'stok' => +$qty,
		'id_gudang'=> $id_gudang_masuk,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at	
	);
	$this->db->where('id', $id);
        $detail= $this->db->insert('tb_stok_gudang_jadi_1',$data_stok);	
	}
		 if ($detail) return true;
		else return false;
		}
        
 
     public function cari($offset)
    {
        $this->get_real_offset($offset);
        $kata_kunci = $this->input->get('kata_kunci', true);
       
        return $this->db->where("( no_sj LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->result();
    }
    
     
    
     public function cari_num_rows()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
          return $this->db->where("( no_sj LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->num_rows();
    }
    
    
    public function update_header($id,$no_sj,$tanggal_sj,$jenis_masuk,$id_gudang_masuk,$id_gudang_keluar,$keterangan_header)
    {
     $updated_at = date('Y-m-d H:i:s');
     $this->db->where('id', $id);
     $data= array(
		'no_sj' => $no_sj,
		'tanggal_sj' => $tanggal_sj,
		'jenis_masuk' => $jenis_masuk,
		'id_gudang_masuk' => $id_gudang_masuk,
		'id_gudang_keluar' => $id_gudang_keluar,
		
		'keterangan' => $keterangan_header,
		'status_edit' => "t",
		'updated_at'=> $updated_at,
		);
    $this->db->update('tb_makloon_baju_gudang_jadi_2_wip', $data);
    
    $query = $this->db->query(" SELECT id from tb_makloon_baju_gudang_jadi_2_wip_detail where id_makloon_baju_gudang_jadi_2_wip='$id' order by id desc limit 1");
    
    return $query->result();
   
    }
    
    public function update_detail($id_header,$id_detail,$id_barang_wip,$qty,$keterangan_detail,$id_gudang_masuk,$id_gudang_keluar,$qty_lama)
    {
    $updated_at = date('Y-m-d H:i:s');
     $created_by = $updated_by = $this->session->userdata('pengguna_id');
     $this->db->where('id', $id_detail);
     $data= array(
		'id_barang_wip' => $id_barang_wip,
		'qty'=> $qty,
		'keterangan' => $keterangan_detail,
		'status_edit' => 't',
		'updated_at'=> $updated_at,
		);
    $this->db->update('tb_makloon_baju_gudang_jadi_2_wip_detail', $data);
    
    
		$update_stok= $qty-$qty_lama;
		
     $this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang_masuk' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok+$update_stok,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
	}
         $this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang_keluar' 
        AND id_barang_wip='$id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok-$update_stok,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);
        
	}
	
    
    return true;
    }

 public function get_unit_jahit()
    {
     $sql=$this->db->query("SELECT id,nama_unit_jahit FROM tb_master_unit_jahit order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }    
    public function get_unit_packing()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_unit_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }  
    public function get_gudang()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_gudang order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }
    
     
    
     public function get_all_inner_paged($offset)
    {
     $select=$this->db->select("* from tb_makloon_baju_gudang_jadi_2_wip where status_aktif='t' order by updated_at")->limit($this->_per_page,$offset);  
            $select=$this->db->get();
     if($select->num_rows() > 0){
		 return $select->result();
		 }
    }
    
  function paging_barang($tipe, $base_url, $uri_segment)
    {
        // Memanggil library pagination.
        $this->load->library('pagination');

        // Konfigurasi.
        $config = array(
            'base_url' => $base_url,
            'uri_segment' => $uri_segment,
            'per_page' => $this->_per_page,
            'use_page_numbers' => true,
            'num_links' => 4,
            'first_link' => '&#124;&lt; First',
            'last_link' => 'Last &gt;&#124;',
            'next_link' => 'Next &gt;',
            'prev_link' => '&lt; Prev',

            // Menyesuaikan untuk Twitter Bootstrap 3.2.0.
            'full_tag_open' => '<ul class="pagination pagination-sm">',
            'full_tag_close' => '</ul>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li class="disabled"><li class="active"><a href="#">',
            'cur_tag_close' => '<span class="sr-only"></span></a></li>',
            'next_tag_open' => '<li>',
            'next_tagl_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tagl_close' => '</li>',
            'first_tag_open' => '<li>',
            'first_tagl_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tagl_close' => '</li>',
        );

        // Jika paging digunakan untuk "pencarian", tambahkan / tampilkan $_GET di URL.
        // Caranya dengan memanipulasi $config['suffix'].
        if ($tipe == 'pencarian') {
            if (count($_GET) > 0) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['total_rows'] = $this->cari_num_rows_barang();
        } else {
            $config['first_url'] = '1';
            $config['total_rows'] = $this->get_all_num_rows_barang();
        }

        // Set konfigurasi.
        $this->pagination->initialize($config);

        // Buat link dan kembalikan link paging yang sudah jadi.
        return $this->pagination->create_links();
    }
   
    
    public function get_all_num_rows_barang()
    {
		$this->db->where('status_aktif','t');
        return $this->db->get('tb_makloon_baju_gudang_jadi_2_wip')->num_rows();
    }
    
    
     public function delete_aktif($id,$id_gudang_masuk,$id_gudang_keluar)
    {
	$updated_by=$deleted_by=$this->session->userdata('pengguna_id');
	$data = array(
               'status_aktif' => "f",
               'deleted_by'=>$deleted_by,
               'deleted_at'=>$deleted_at
            );
            
    $this->db->where('id', $id);
	$this->db->update('tb_makloon_baju_gudang_jadi_2_wip', $data); 
	
	$query=$this->db->query("SELECT * FROM tb_makloon_baju_gudang_jadi_2_wip_detail where id_makloon_baju_gudang_jadi_2_wip='$id'");
	if($query->num_rows() > 0){
		$hasil4=$query->result();
		foreach ($hasil4 as $row4){
		$this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang_masuk' 
        AND id_barang_wip='$row4->id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok-$row4->qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
			}
		$this->db->select("id,id_barang_wip,id_gudang,stok FROM tb_stok_gudang_jadi_1 where id_gudang='$id_gudang_keluar' 
        AND id_barang_wip='$row4->id_barang_wip'");
        $stok= $this->db->get();
        if($stok -> num_rows() > 0){
			$hasilrow=$stok->row();
			$id = $hasilrow->id;
			$id_barang_wipx=$hasilrow->id_barang_wip;
			$id_gudangx=$hasilrow->id_gudang;
			$stok=$hasilrow->stok;
			
		$data_stok_update= array(
		'stok' => $stok+$row4->qty,
		'updated_by'=> $updated_by,
		'updated_at'=> $updated_at
		
		);
		$this->db->where('id', $id);
        $detail= $this->db->update('tb_stok_gudang_jadi_1',$data_stok_update);	
			}

		}  
		$this->db->where('id_makloon_baju_gudang_jadi_2_wip', $id);
		$this->db->update('tb_makloon_baju_gudang_jadi_2_wip_detail', $data); 
	}
	
	

	return true;	    
    }
    
     public function getAllDetail($id)
    {
      $query =$this->db->query("SELECT * FROM tb_makloon_baju_gudang_jadi_2_wip where id='$id'");
	
	if($query->num_rows > 0){
		$hasil = $query->result();
		foreach($hasil as $row){
			
			$query2 =$this->db->query("SELECT id_barang_wip,qty,keterangan 
			from tb_makloon_baju_gudang_jadi_2_wip_detail where id_makloon_baju_gudang_jadi_2_wip ='$row->id'");
			
			if($query2->num_rows > 0){
				$hasil2 =$query2->result();
				foreach ($hasil2 as $row2){
				
				$query3=$this->db->query("SELECT nama_barang_wip,kode_barang_wip FROM tb_master_barang_wip 
				where id='$row2->id_barang_wip'");
				if($query3->num_rows > 0){
					$hasil3 =$query3->row();
					$nama_barang_wip=$hasil3->nama_barang_wip;
					$kode_barang_wip=$hasil3->kode_barang_wip;
					}
					
					$id_barang_wip =$row2->id_barang_wip;
					$qty =$row2->qty;
					$keterangan_detail=$row2->keterangan;
					
					$detail_data[]=array(
					
					'nama_barang_wip'=>$nama_barang_wip,
					'kode_barang_wip'=>$kode_barang_wip,
					'id_barang_wip'=>$id_barang_wip,
					'qty'=>$qty,
					'keterangan_detail'=>$keterangan_detail
				);
			}
		}	
	}
			
			
			$header_data[]=array(
			'id'=>$row->id,
			'no_sj'=>$row->no_sj,
			'tanggal_sj'=>$row->tanggal_sj,
			'jenis_masuk'=>$row->jenis_masuk,
			'id_gudang_masuk'=>$row->id_gudang_masuk,
			'id_gudang_keluar'=>$row->id_gudang_keluar,
			'keterangan_header'=>$row->keterangan,
			'detail_data'=>$detail_data
		);

		
	    
    }
    else{
			$header_data[]=array();
			}
		return $header_data;    
    
}
}
