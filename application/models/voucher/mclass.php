<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}	
	
	function lkodesumber() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_kode_voucher ORDER BY i_voucher_code ASC ");
	}
	
	function lkodesumberperpages() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_kode_voucher ORDER BY i_voucher_code ASC ");
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	
	function flkodesumber($key) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_kode_voucher WHERE (i_voucher_code='$key' OR e_voucher_name='$key'  OR e_description LIKE '$key%') ORDER BY i_voucher_code ASC ");
	}
	
	function lkontrabon() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa, a.f_nota_sederhana
			
			FROM tm_dt a INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
			
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_grand_sisa > 0) 
			
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa, a.f_nota_sederhana ORDER BY a.i_dt_code ASC ");
	}
	
	function lkontrabonperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa, a.f_nota_sederhana
				
			FROM tm_dt a INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_grand_sisa > 0)
				
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa, a.f_nota_sederhana ORDER BY a.i_dt_code ASC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function flkontrabon($key) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa, a.f_nota_sederhana
				
			FROM tm_dt a INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_grand_sisa > 0) AND a.d_dt='$key' 
				
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa, a.f_nota_sederhana ORDER BY a.i_dt_code ASC ");
	}

	function clistkontrabon($ikontrabon) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa
				
			FROM tm_dt a INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_grand_sisa > 0) AND a.i_dt='$ikontrabon' 
				
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa ORDER BY a.i_dt_code ASC ");
	}
	

	function msimpan($kode_sumber,$i_kode_sumber,$no_voucher,$tgl_voucher,$kode_perusahaan,$deskripsi_voucher,$total_nilai_voucher,$recieved_voucher,$approve_voucher,$app_dept,$f_nilai_manual,$iteration,$i_kontrabon_code,$i_kontrabon,$tglkontrabon,$total_kontrabonhidden,$piutanghidden,$nilai_voucher,$fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$nilaivoucher = array();
		$totalvoucher = 0;
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$date_dt = date("Y-m-d");
		
		$qtm_voucher	= $db2->query(" SELECT i_voucher FROM tm_voucher ORDER BY i_voucher DESC LIMIT 1 ");
		if($qtm_voucher->num_rows()>0) {
			$rtm_voucher = $qtm_voucher->row();
			$ivoucher = $rtm_voucher->i_voucher+1;
		}else{
			$ivoucher = 1;
		}
		
		if(isset($iteration)) {
			
			$qinsert_voucher = $db2->query(" INSERT INTO tm_voucher (i_voucher,i_voucher_code,i_voucher_no,d_voucher,i_company_code,e_recieved,e_approved,e_description,v_total_voucher,f_nilai_manual,f_approve_dept,d_entry,d_update) 
					VALUES('$ivoucher','$i_kode_sumber','$no_voucher','$tgl_voucher','$kode_perusahaan','$recieved_voucher','$approve_voucher','$deskripsi_voucher','$total_nilai_voucher','$f_nilai_manual','$app_dept','$dentry','$dentry') ");
			
			$sisanilaivoucher = '';

			if($sisanilaivoucher=='') {
				$sisanilaivoucher = $total_nilai_voucher;
			}
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

				if(($sisanilaivoucher >= $piutanghidden[$jumlah])) {
					$sisanilaivoucher = ($sisanilaivoucher - $piutanghidden[$jumlah]);
					$nilaivoucher[$jumlah] = $piutanghidden[$jumlah];
				}elseif(($sisanilaivoucher <= $piutanghidden[$jumlah]) && ($sisanilaivoucher!=0 || $sisanilaivoucher!='0')) {
					$nilaivoucher[$jumlah] = $sisanilaivoucher;
					$sisanilaivoucher = 0;
				}elseif($sisanilaivoucher==0 || $sisanilaivoucher<0) {
					$nilaivoucher[$jumlah] = 0;
				}else{
					$nilaivoucher[$jumlah] = 0;
				}
									
				$qtm_voucher_item = $db2->query(" SELECT i_voucher_item FROM tm_voucher_item ORDER BY i_voucher_item DESC LIMIT 1 ");
				if($qtm_voucher_item->num_rows()>0){
					$rtm_voucher_item = $qtm_voucher_item->row();
					$ivoucheritem = $rtm_voucher_item->i_voucher_item+1;
				}else{
					$ivoucheritem = 1;
				}
				
				if($f_nilai_manual=='t') {
							
					$nilaivoucher[$jumlah] = $nilai_voucher[$jumlah];
					
					if($nilaivoucher[$jumlah]>0) {
						$qinsert_voucher_item = $db2->query(" INSERT INTO tm_voucher_item (i_voucher_item,i_voucher,i_dt,d_dt,v_voucher,d_entry,d_update,f_nota_sederhana) 
							VALUES('$ivoucheritem','$ivoucher','$i_kontrabon[$jumlah]','$tglkontrabon[$jumlah]','$nilaivoucher[$jumlah]','$dentry','$dentry','$fnotasederhana[$jumlah]') ");
					}		
							
				}else{
					
					if($nilaivoucher[$jumlah]>0) {
						$qinsert_voucher_item = $db2->query(" INSERT INTO tm_voucher_item (i_voucher_item,i_voucher,i_dt,d_dt,v_voucher,d_entry,d_update,f_nota_sederhana) 
							VALUES('$ivoucheritem','$ivoucher','$i_kontrabon[$jumlah]','$tglkontrabon[$jumlah]','$nilaivoucher[$jumlah]','$dentry','$dentry','$fnotasederhana[$jumlah]') ");
					}		
							
				}
				
				$qdt = $db2->query(" SELECT i_dt, v_grand_sisa FROM tm_dt WHERE i_dt='$i_kontrabon[$jumlah]' AND d_dt='$tglkontrabon[$jumlah]' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana[$jumlah]' ");
				
				if($qdt->num_rows()>0) {
					
					$rdt = $qdt->row();
					
					if($nilaivoucher[$jumlah]>0) {
												
						$sisadt = ($rdt->v_grand_sisa) - $nilaivoucher[$jumlah];
						
						if($sisadt=='')
							$sisadt = 0;
						
						$db2->query(" UPDATE tm_dt SET v_grand_sisa='$sisadt', f_pelunasan='t' WHERE i_dt='$rdt->i_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana[$jumlah]' ");
					}
				}
			}
		}
	
		if($db2->trans_status()===FALSE) {
			$ok	= 0;
			$db2->trans_rollback();
		}else{
			$ok	= 1;
			$db2->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Voucher : '\"+$no_voucher+\"' telah disimpan, terimakasih.\");show(\"voucher/cform\",\"#content\");</script>";
		}else{
			print "<script>alert(\"Maaf, Voucher gagal disimpan. Terimakasih.\");show(\"voucher/cform\",\"#content\");</script>";
		}
	}
	
}

?>
