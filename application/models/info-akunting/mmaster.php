<?php
class Mmaster extends CI_model{
	function __construct() { 
		parent::__construct();
	}

	function getcoa(){
		$sql = " * from tm_coa order by kode ";
		$this->db->select($sql,false);
		$query=$this->db->get();
		return $query->result();	
	}	

	function get_coa_item($id_coa){
		$sql = " * from tm_coa where id='$id_coa' order by kode ";
		$this->db->select($sql,false);
		$query=$this->db->get();
		return $query->result_array();	
	}	

	function getarea(){
		$sql = " * from tm_area order by kode_area ";
		$this->db->select($sql,false);
		$query=$this->db->get();
		return $query->result();	
	}

	function getbank(){
		$sql = " * from tm_bank order by kode ";
		$this->db->select($sql,false);
		$query=$this->db->get();
		return $query->result();	
	}

	function get_kodecoa($id_area){
		$query = $this->db->query(" SELECT 
										kode 
									FROM 
										tm_coa 
									WHERE id_area = '$id_area'
								",false);
				    
		if ($query->num_rows() > 0){
			return $query->row()->kode;
		}else{
	    	return 0;
		}
	}

	function saldoawal_k($date_from,$date_to,$id_area,$kode_coa){
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT 
	    								saldo_awal 
	    							FROM 
	    								tt_saldo_akun 
	    							WHERE i_periode = '$periode' 
	    							AND kode_coa = '$kode_coa'
	    						",false);
	    
		if ($query->num_rows() > 0){
			return $query->row()->saldo_awal;
		}else{
	    	return 0;
		}
	}

	function kredit_k($date_from,$date_to,$id_area){
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT sum(jumlah) as jumlah
									FROM tt_kas_kecil 
									WHERE d_periode = '$periode' 
									AND is_kredit = 't' 
									AND f_cancel_kk = 'f'
									AND id_area  = '$id_area'
								",false);
			    
		if ($query->num_rows() > 0){
			return $query->row()->jumlah;
		}else{
	    	return 0;
		}
	}

	function debet_k($date_from,$date_to,$id_area){
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT sum(jumlah) as jumlah
									FROM tt_kas_kecil
									WHERE d_periode = '$periode' 
									AND is_debet = 't' 
									AND f_cancel_kk = 'f'
									AND id_area  = '$id_area'
								",false);
			    
		if ($query->num_rows() > 0){
			return $query->row()->jumlah;
		}else{
	    	return 0;
		}
	}

	function viewkaskecil($date_from,$date_to,$id_area){
		
		$sqlkk="
				SELECT
				   a.*,
				   CASE 
				   		WHEN a.is_debet ='f' THEN c.i_rv
  				   		WHEN a.is_kredit ='f' THEN e.i_pv 
  				   END AS no_vocher,

				   CASE WHEN a.is_debet = 'f' THEN a.jumlah 
				      ELSE 0 
				   END AS debet,

				   CASE WHEN a.is_kredit = 'f' THEN a.jumlah 
				      ELSE 0 
				   END AS kredit
				FROM
				   tt_kas_kecil a
				LEFT JOIN tm_rv_item b ON
		  			b.i_kk = a.no_transaksi
		  		LEFT JOIN tm_rv c ON
		  			b.i_rv = c.i_rv
		  		LEFT JOIN tm_pv_item d ON
		  			d.i_kk = a.no_transaksi
		  		LEFT JOIN tm_pv e ON
		  			e.i_pv = d.i_pv
				WHERE
				   tgl >= to_date('$date_from', 'dd-mm-yyyy') 
				   AND tgl <= to_date('$date_to', 'dd-mm-yyyy') 
				   AND id_area = '$id_area' 
				ORDER BY
				   no_transaksi, tgl asc
		";
		$sqlquerykk= $this->db->query($sqlkk);
		if ($sqlquerykk->num_rows > 0){
			$hasilkk =$sqlquerykk->result();
			foreach ($hasilkk as $row1){
				$tanggal= $row1->tgl;
				$kode_coa= $row1->kode_coa;
				$nama_coa= $row1->nama_coa;
				$keterangan= $row1->deskripsi;
				$value = $row1->jumlah;
				$no_transaksi= $row1->no_transaksi;	
				$debet= $row1->debet;	
				$kredit= $row1->kredit;	
				$irv = $row1->no_vocher;
				
				$datakk[]=array(
				'tanggal'=> $tanggal,
				'nama_coa'=> $nama_coa,
				'keterangan'=> $keterangan,
				'value' => $value,
				'no_transaksi'=> $no_transaksi,
				'kode_coa' =>$kode_coa,
				'debet'=>$debet,
				'kredit'=>$kredit,
				'irv'=>$irv,
				);
			}
		}else{
			$datakk=array();
		}				
		return $datakk;
	}
	
	function viewju($date_from,$date_to,$id_area){
	
		$sqlju="SELECT * from tt_jurnal_umum where tgl_jurnal >= to_date('$date_from','dd-mm-yyyy') and tgl_jurnal <= to_date('$date_to','dd-mm-yyyy')
		and id_area='$id_area' order by no_jurnal,tgl_jurnal asc";
		$sqlqueryju= $this->db->query($sqlju);
		if ($sqlqueryju->num_rows > 0){
			$hasilju =$sqlqueryju->result();
			foreach ($hasilju as $row1){

				$sqljudetail="SELECT * from tt_jurnal_umum_detail where id_jurnal_umum=$row1->id";
				if ($sqljudetail->num_rows > 0){
					$hasiljudetail =$sqljudetail->row();
					foreach ($hasiljudetail as $row2){
						$tanggal= $row2->tgl_jurnal;
						$kode_coa= $row1->kode_coa;
						$nama_coa= $row2->nama_coa;
						$keterangan= $row2->deskripsi;
						$kredit = $row1->kredit;
						$debet = $row1->debet;
						
						$datajudetail[]=array(
						'tanggal'=> $tanggal,
						'nama_coa'=> $nama_coa,
						'keterangan'=> $keterangan,
						'kredit'=> $kredit,
						'debet'=> $debet
						);
					}

					$total_kredit = $row1->total_kredit;
					$total_debet = $row1->total_debet;
					$no_jurnal= $row1->no_jurnal;	
					
					$dataju[]=array(
					'tanggal'=> $tanggal,
					'nama_coa'=> $nama_coa,
					'keterangan'=> $keterangan,
					'no_jurnal'=> $no_jurnal,
					'datajudetail'=>$datajudetail
					);
				}
			}	
			return $dataju;
		}else{
			$dataju=array();
		}
	}

	function saldoawal($date_from,$date_to){
		$kasbesar=KasBesar;
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT 
	    								saldo_awal 
	    							FROM 
	    								tt_saldo_akun 
	    							WHERE i_periode = '$periode' 
	    							AND kode_coa  = '$kasbesar'
	    						",false);
	    
		if ($query->num_rows() > 0){
			return $query->row()->saldo_awal;
		}else{
	    	return 0;
		}
	}

	function kredit($date_from,$date_to){
		$kasbesar=KasBesar;
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT sum(jumlah) as jumlah
									FROM tt_kas_besar 
									WHERE d_periode = '$periode' 
									AND is_kredit = 't' 
									AND f_cancel_kb = 'f'
								",false);
			    
		if ($query->num_rows() > 0){
			return $query->row()->jumlah;
		}else{
	    	return 0;
		}
	}

	function debet($date_from,$date_to){
		$kasbesar=KasBesar;
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT sum(jumlah) as jumlah
									FROM tt_kas_besar 
									WHERE d_periode = '$periode' 
									AND is_debet = 't' 
									AND f_cancel_kb = 'f'
								",false);
			    
		if ($query->num_rows() > 0){
			return $query->row()->jumlah;
		}else{
	    	return 0;
		}
	}

	function viewkasbesar($date_from,$date_to,$id_area){
		$kasbesar=KasBesar;
		$periode=substr($date_from,6,4).substr($date_to,3,2);

		$sqlkb="
				SELECT
				   a.*, c.i_rv,
				   CASE 
				   		WHEN a.is_debet ='f' THEN c.i_rv
  				   		WHEN a.is_kredit ='f' THEN e.i_pv 
  				   END AS no_vocher,
				   CASE WHEN a.is_debet = 'f' THEN a.jumlah 
				      ELSE 0 
				   END AS debet, 
				   CASE WHEN a.is_kredit = 'f' THEN a.jumlah 
				      ELSE 0 
				   END AS kredit 
				FROM
				   tt_kas_besar a
			    LEFT JOIN tm_rv_item b ON
		  			b.i_kk = a.no_transaksi
		  		LEFT JOIN tm_rv c ON
		  			b.i_rv = c.i_rv
		  		LEFT JOIN tm_pv_item d ON
		  			d.i_kk = a.no_transaksi
		  		LEFT JOIN tm_pv e ON
		  			e.i_pv = d.i_pv
				WHERE
				   a.tgl >= to_date('$date_from', 'dd-mm-yyyy') 
				   AND a.tgl <= to_date('$date_to', 'dd-mm-yyyy') 
				   AND a.id_area = '$id_area' 
				   AND a.f_cancel_kb = 'f'	
				ORDER BY
				   a.no_transaksi, a.tgl asc
				";
		$sqlquerykb= $this->db->query($sqlkb);
		if ($sqlquerykb->num_rows > 0){
			$hasilkb =$sqlquerykb->result();
			foreach ($hasilkb as $row1){
				$tanggal= $row1->tgl;
				$nama_coa= $row1->nama_coa;
				$kode_coa= $row1->kode_coa;
				$keterangan= $row1->deskripsi;
				$value = $row1->jumlah;
				$no_transaksi= $row1->no_transaksi;	
				$debet= $row1->debet;	
				$kredit= $row1->kredit;	
				$irv = $row1->no_vocher;
				
				$querybank=$this->db->query("Select * from tm_coa where id ='$row1->id_coa'")	;
					if($querybank ->num_rows > 0){
						$hasilbank = $querybank->result();
						foreach($hasilbank as $row3){
							$kode_bank=$row3->kode;
							$nama_bank=$row3->nama;
					
							$datakb[]=array(
											'tanggal'=> $tanggal,
											'nama_coa'=> $nama_coa,
											'keterangan'=> $keterangan,
											'value' => $value,
											'no_transaksi'=> $no_transaksi,
											'kode_coa' =>$kode_coa,
											'kode_bank'=>$kode_bank,
											'nama_bank'=>$nama_bank,
											'debet'=>$debet,
											'kredit'=>$kredit,
											'irv'=>$irv,
							);			
						}
				
					}	
			}
		}else{
			$datakb=array();
		}			
		return $datakb;
	}

	function saldoawal_b($date_from,$date_to,$id_bank){
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT 
	    								saldo_awal 
	    							FROM 
	    								tt_saldo_akun 
	    							WHERE i_periode = '$periode' 
	    							AND id_coa  = '$id_bank'
	    						",false);
	    
		if ($query->num_rows() > 0){
			return $query->row()->saldo_awal;
		}else{
	    	return 0;
		}
	}

	function kredit_b($date_from,$date_to,$id_bank){
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT sum(jumlah) as jumlah
									FROM tt_kas_bank 
									WHERE d_periode = '$periode' 
									AND is_kredit = 't' 
									AND f_cancel_bk = 'f'
									AND id_coa_bank  = '$id_bank'
								",false);
			    
		if ($query->num_rows() > 0){
			return $query->row()->jumlah;
		}else{
	    	return 0;
		}
	}

	function debet_b($date_from,$date_to,$id_bank){
		$periode=substr($date_from,6,4).substr($date_to,3,2);
	    $query = $this->db->query(" SELECT sum(jumlah) as jumlah
									FROM tt_kas_bank
									WHERE d_periode = '$periode' 
									AND is_debet = 't' 
									AND f_cancel_bk = 'f'
									AND id_coa_bank  = '$id_bank'
								",false);
			    
		if ($query->num_rows() > 0){
			return $query->row()->jumlah;
		}else{
	    	return 0;
		}
	}
	
	function viewbank($date_from,$date_to,$id_area,$id_bank){
		//$databank=array();
		// $pencarian='';
		// if($id_bank!=0){
		// 	$pencarian.=" AND id_coa_bank= '$id_bank' ";
		// 	}
		// if($id_area !=0){
		// 	$pencarian.=" AND id_area='$id_area' ";
		// 	}

		$sqlbank="
					SELECT a.*,
						CASE 
					   		WHEN a.is_debet ='f' THEN c.i_rv
	  				   		WHEN a.is_kredit ='f' THEN e.i_pv 
	  				   END AS no_vocher,
						CASE WHEN a.is_debet = 'f' THEN a.jumlah 
					      ELSE 0 
					   	END AS debet, 
					   	CASE WHEN a.is_kredit = 'f' THEN a.jumlah 
					      ELSE 0 
					   	END AS kredit  
					FROM 
						tt_kas_bank a 
						LEFT JOIN tm_rv_item b ON
			  			b.i_kk = a.no_transaksi and a.kode_coa_bank = b.i_coa_bank
				  		LEFT JOIN tm_rv c ON
				  			b.i_rv = c.i_rv
				  		LEFT JOIN tm_pv_item d ON
				  			d.i_kk = a.no_transaksi and a.kode_coa_bank = b.i_coa_bank
				  		LEFT JOIN tm_pv e ON
				  			e.i_pv = d.i_pv
					WHERE a.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl <= to_date('$date_to','dd-mm-yyyy') 
						AND a.f_cancel_bk = 'f' 
						AND a.id_coa_bank= '$id_bank' ";
		
		$sqlquerybank= $this->db->query($sqlbank);
		if ($sqlquerybank->num_rows() > 0){
			$hasilbank =$sqlquerybank->result();
			foreach ($hasilbank as $row1){
				$tanggal= $row1->tgl;
				$nama_coa= $row1->nama_coa;
				$keterangan= $row1->deskripsi;
				$value = $row1->jumlah;
				$no_transaksi= $row1->no_transaksi;
				$debet= $row1->debet;	
				$kredit= $row1->kredit;	
				$irv = $row1->no_vocher;
				
				$querycoa=$this->db->query("Select * from tm_coa where id ='$row1->id_coa'")	;
				if($querycoa->num_rows() > 0){
					$hasilcoa = $querycoa->row();
						$kode_coa=$hasilcoa->kode;
				}
				else{
					$kode_coa='';
				}
				
				$querybank=$this->db->query("Select * from tm_bank where id_coa ='$row1->id_coa_bank'")	;
				if($querybank->num_rows() > 0){
					$hasilbank = $querybank->row();
					$kode_bank=$hasilbank->kode;
					$nama_bank=$hasilbank->nama;
				}
			
				$databank[]=array(
				'tanggal'=> $tanggal,
				'nama_coa'=> $nama_coa,
				'keterangan'=> $keterangan,
				'value' => $value,
				'no_transaksi'=> $no_transaksi,
				'kode_coa' =>$kode_coa,
				'nama_bank'=>$nama_bank,
				'kode_bank'=>$kode_bank,
				'debet'=>$debet,
				'kredit'=>$kredit,
				'irv'=>$irv,
				);				
			}
				
		}else{
			$databank=array();
		}				
		return $databank;
	}
	
	function get_saldoawal($dfrom,$icoa){
		$itahun=substr($dfrom,6,4);
		$ibulan=substr($dfrom,3,2);
		$this->db->select("	saldo_awal from tt_saldo_akun
							where bulan = '$ibulan' AND tahun = '$itahun'
							and id_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->saldo_awal;
		}
		if($dfrom!=''){
			$tmp=explode("-",$dfrom);
			$th=$tmp[2];
			$bl=$tmp[1];
			$hr=$tmp[0];
			$dfrom =$th."-".$bl."-".$hr;
			$dstart=$th."-".$bl."-01";
		}
			
		$this->db->select("	sum(v_mutasi_debet) as v_mutasi_debet, sum(v_mutasi_kredit) as v_mutasi_kredit from tm_general_ledger
          							where id_coa='$icoa' and d_mutasi>='$dstart' and d_mutasi<'$dfrom' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$vmutasidebet = $tmp->v_mutasi_debet;
			$vmutasikredit= $tmp->v_mutasi_kredit;
		}
		if(!isset($sawal))$sawal=0;
		$sawal=$sawal+$vmutasidebet-$vmutasikredit;
		return $sawal;		
    }
    
    /*
    function get_allbukubesar($id_coa,$date_from,$date_to){
		
		$query= $this->db->query("SELECT * FROM (
		SELECT a.tgl_jurnal as tanggal, b.deskripsi ,b.debet,b.kredit,b.id_coa,cast(0 as varchar) as no_transaksi  FROM  tt_jurnal_umum a inner join tt_jurnal_umum_detail b ON a.id=b.id_jurnal_umum
		UNION 
		SELECT tgl as tanggal , deskripsi ,jumlah as debet,0 as kredit,id_coa,no_transaksi from tt_kas_bank where is_debet='TRUE'
		UNION 
		SELECT tgl as tanggal , deskripsi ,0 as debet,jumlah as kredit,id_coa,no_transaksi from tt_kas_bank where is_kredit='TRUE'
		UNION 
		SELECT tgl as tanggal , deskripsi ,jumlah as debet,0 as kredit,id_coa,no_transaksi from tt_kas_besar where is_debet='TRUE'
		UNION 
		SELECT tgl as tanggal , deskripsi ,0 as debet,jumlah as kredit,id_coa,no_transaksi from tt_kas_besar where is_kredit='TRUE'
		UNION 
		SELECT tgl as tanggal , deskripsi ,jumlah as debet,0 as kredit,id_coa,no_transaksi from tt_kas_kecil where is_debet='TRUE'
		UNION 
		SELECT tgl as tanggal , deskripsi ,0 as debet,jumlah as kredit,id_coa,no_transaksi from tt_kas_kecil where is_debet='TRUE'
		)z where z.tanggal >=to_date('$date_from', 'DD-MM-YYYY') AND z.tanggal <=to_date('$date_to', 'DD-MM-YYYY') AND  z.id_coa='$id_coa' ORDER by no_transaksi ASC
		  ");
		
		if($query->num_rows() > 0){
			return $query->result();
			
			}
		
		}
		*/
	function get_allbukubesar($id_coa,$dfrom,$dto,$icoa){
		
		$query= $this->db->query("SELECT * FROM tm_general_ledger where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
						  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
						  and id_coa='$id_coa'  ORDER by  d_mutasi, i_refference");
		
		if($query->num_rows() > 0){
			$hasil= $query->result();
			foreach($hasil as $row){
				$reff=$row->i_refference;
				$dreff=$row->d_refference;
				$fdebet=$row->f_debet;
				$iarea=$row->id_area;
				$icoabank=$row->i_coa_bank;
				$flagvmutasidebet=$row->v_mutasi_debet;
				$flagvmutasikredit=$row->v_mutasi_kredit;
				$ireff=trim($reff);
		
				if($icoabank!=null){
				  	$sql ="	* from(
				          select * from(
								  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, kode_coa, i_refference, deskripsi, f_debet, v_mutasi_debet, 
								  v_mutasi_kredit, i_general_ledger, id_area, d_refference, e_coa_name, i_coa_bank
								  from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa<>'$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) as a
								  where 
								  a.i_refference in (
								  SELECT i_refference from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) and
								  a.i_coa_bank in (
								  SELECT i_coa_bank from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  )
								  union all						  
								  select * from(
								  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, kode_coa, i_refference, deskripsi, f_debet, v_mutasi_debet, 
								  v_mutasi_kredit, i_general_ledger, id_area, d_refference, e_coa_name, i_coa_bank
								  from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa<>'$icoa' and f_debet<>'$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) as b
								  where 
								  b.i_refference in (
								  SELECT i_refference from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) and
								  b.i_coa_bank in (
								  SELECT i_coa_bank from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  )
								  )as c
								  order by c.d_mutasi, c.i_refference ";
								  $this->db->select($sql,false);
				}else{
			 		$sql = $this->db->select("	* from(
				          select * from(
								  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, kode_coa, i_refference, deskripsi, f_debet, v_mutasi_debet, 
								  v_mutasi_kredit, i_general_ledger, id_area, d_refference, e_coa_name, i_coa_bank
								  from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa<>'$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) as a
								  where 
								  a.i_refference in (
								  SELECT i_refference from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) and
								  a.deskripsi in (
								  SELECT deskripsi from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  )
								  union all						  
								  select * from(
								  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, kode_coa, i_refference, deskripsi, f_debet, v_mutasi_debet, 
								  v_mutasi_kredit, i_general_ledger, id_area, d_refference, e_coa_name, i_coa_bank
								  from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa<>'$icoa' and f_debet<>'$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) as b
								  where 
								  b.i_refference in (
								  SELECT i_refference from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  ) and
								  b.deskripsi in (
								  SELECT deskripsi from tm_general_ledger 
								  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
								  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
								  and kode_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and id_area='$iarea'
								  )
								  )as c
								  order by c.d_mutasi, c.i_refference ",false);						  
	    		}    
				$query2 = $this->db->get();
				if ($query2->num_rows() > 0){					
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2){	
		
						$data[]=array(
						'd_mutasi'=>$row2->d_mutasi,
						'kode_coa'=>$row2->kode_coa,
						'i_refference'=>$row2->i_refference,
						'deskripsi'=>$row2->deskripsi,
						'f_debet'=>$row2->f_debet,
						'v_mutasi_debet'=>$row2->v_mutasi_debet,
						'v_mutasi_kredit'=>$row2->v_mutasi_kredit,
						'id_area'=>$row2->id_area,
						'd_refference'=>$row2->d_refference,
						'e_coa_name'=>$row2->e_coa_name,
						'i_coa_bank'=>$row2->i_coa_bank,
						'flagvmutasidebet'=>$flagvmutasidebet,
						'flagvmutasikredit'=>$flagvmutasikredit,

						);
					}		
				}
			}
			return $data;
		}
		
	}
}
?>