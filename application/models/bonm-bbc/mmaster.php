<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari) {
	if ($cari == "all") {
			$this->db->select(" * FROM tm_apply_stok_proses_cutting ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
	else {
			$this->db->select(" * FROM tm_apply_stok_proses_cutting WHERE UPPER(no_bonm) like UPPER('%$cari%') 
							ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();

	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
			/*	$query3	= $this->db->query(" SELECT id FROM tm_pb_cutting 
								WHERE no_pb_cutting = '$row1->no_pb_cutting' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_pb_cutting	= $hasilrow->id;
				}
				else
					$id_pb_cutting	= ''; */
				
				$id_pb_cutting = $row1->id_pb_cutting;
				if ($id_pb_cutting == '0') {
					$query3	= $this->db->query(" SELECT id FROM tm_pb_cutting 
								WHERE no_pb_cutting = '$row1->no_pb_cutting' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_pb_cutting	= $hasilrow->id;
					}
					else
						$id_pb_cutting	= '0';
				}
				
				$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting_detail WHERE id_apply_stok = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					$id_detailnya = "";
									
					foreach ($hasil2 as $row2) {
						//----------------
						$query3	= $this->db->query(" SELECT id, kode_brg FROM tm_pb_cutting_detail 
										WHERE id_pb_cutting = '$id_pb_cutting' ");
						$hasil3= $query3->result();
						foreach ($hasil3 as $row3) {
							if ($row2->kode_brg == $row3->kode_brg)
								$id_detailnya.= $row3->id."-";
						}
						//----------------
						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						if ($row2->kode_brg_jadi != '') {
							$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
							WHERE i_product_motif = '$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg_jadi	= $hasilrow->e_product_motifname;
							}
							else {
								if ($row2->brg_jadi_manual == '')
									$nama_brg_jadi = '';
								else
									$nama_brg_jadi = $row2->brg_jadi_manual;
							}
						}
						else
							$nama_brg_jadi = '';
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty_pjg_kain'=> $row2->qty_pjg_kain,
												'gelar_pjg_kain'=> $row2->gelar_pjg_kain,
												'plan_qty_cutting'=> $row2->plan_qty_cutting,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'jenis_proses'=> $row2->jenis_proses
											);
					}
				}
				else {
					$detail_fb = '';
				}
				 
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_pb_cutting'=> $row1->no_pb_cutting,
											'id_pb_cutting'=> $id_pb_cutting,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'status_stok'=> $row1->status_stok,
											'status_edit'=> $row1->status_edit,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya
											);
				$detail_fb = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting WHERE UPPER(no_bonm) like UPPER('%$cari%') ");
		
	}
    
    return $query->result();  
  }
      
  function cek_data($no_faktur){
    $this->db->select("id from tm_op WHERE no_op = '$no_faktur' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_bonm,$tgl_bonm,$no_pb_cutting, $id_pb, $id_pb_detail, $kode, $plan_qty, $pjg_kain, $gelar_pjg_kain,
		$jenis_proses, $kode_brg_quilting, $ukuran_bisbisan, $kode_brg_jadi, $nama_brg_jadi_other, $kode_brg_bisbisan, 
		$no_sc, $id_sc_detail, $bordir, $bscutting, $kcutting){  
    $tgl = date("Y-m-d");
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_apply_stok_proses_cutting WHERE no_bonm = '$no_bonm' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_bonm
			$data_header = array(
			  'no_bonm'=>$no_bonm,
			  'tgl_bonm'=>$tgl_bonm,
			  'no_pb_cutting'=>$no_pb_cutting,
			  'id_pb_cutting'=>$id_pb,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'status_stok'=>'t'
			);
			$this->db->insert('tm_apply_stok_proses_cutting',$data_header);
			
			$this->db->query(" UPDATE tm_pb_cutting SET status_pb = 't', status_edit = 't' where no_pb_cutting= '$no_pb_cutting'
								 AND id='$id_pb' ");
			
			/*$query2	= $this->db->query(" SELECT id FROM tm_pb_cutting WHERE no_pb_cutting = '$no_pb_cutting' ");
			$hasilrow = $query2->row();
			$id_pb	= $hasilrow->id; */
		} // end if header
			
			// ambil data terakhir di tabel tm_apply_stok_proses_cutting
			$query2	= $this->db->query(" SELECT id FROM tm_apply_stok_proses_cutting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_bonm	= $hasilrow->id;
			
			if ($kode!='' && $pjg_kain!='') {
				// cek quilting (update 26 sept: ini udh ga dipake lagi)
				/*$query2	= $this->db->query(" SELECT quilting FROM tm_pb_cutting_detail WHERE id_pb_cutting = '$id_pb'
										AND kode_brg = '$kode' ");
				//if ($query2->num_rows() > 0){
					$hasilrow = $query2->row();
					$quilting	= $hasilrow->quilting; */
				//}
				//else
				//	$quilting = 'f'; 
				
				$this->db->query(" UPDATE tm_pb_cutting_detail SET status_pb = 't' where id_pb_cutting= '$id_pb' ");
				
				// update stoknya #############################################
				// 08-01-2013, warning! update pengurangan stok skrg bukan disini, tapi di bon M keluar. skrip ini dinonaktifkan
		
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% begin 08-01-2013  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				/*
				// cek dulu satuan brgnya. jika satuannya yard, maka qty (m) yg tadi diambil itu konversikan lagi ke yard
				// 28 des 2011: utk yg satuannya roll, ada konversi. 1 roll = 45.5 m. 1 roll = 50 yard
					$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$kode' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$satuan	= '';
						}
						
						if ($satuan == "Yard") {
							$qty_sat_awal = $pjg_kain / 0.90; // 07-03-2012, rubah dari 0.91 ke 0.90
						}
						else if ($satuan == "Roll") {
							$qty_sat_awal = $pjg_kain/45.5;
						}
						else
							$qty_sat_awal = $pjg_kain;
				
				// ##############################
				
				//if ($quilting == 'f') {
					$nama_tabel_stok = "tm_stok";
					$nama_tabel_tt = "tt_stok";
				//}
				//else {
					//$nama_tabel_stok = "tm_stok_hasil_makloon";
					//$nama_tabel_tt = "tt_stok_hasil_makloon";
				//}
				
					//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty_sat_awal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' ");
					}
					
					// &&&&&&&&&&&&&&&&&&&&&&& modifikasi 5 okt 2011 &&&&&&&&&&&&&&&&&&&&&
					// warning 16-02-2012: tabel tt_stok udh ga akan dipake lagi utk rekap stok
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg = '$kode' ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; // data ke-1: 500		data ke-2: 10
							$harganya = $row2->harga; // data ke-1: 20000	data ke-2: 370000
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) // temp_selisih = -6
									$selisih = $stoknya-$qty_sat_awal; // selisih1 = 500-506 = -6
								else
									$selisih = $stoknya+$temp_selisih; // selisih2 = 10-6 = 4
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; // temp_selisih = -6
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode' AND harga = '$harganya' "); //
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $qty_sat_awal; // temp_selisih = -6
										
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih; // new_stok = 4
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode' AND harga = '$harganya' ");

					} */
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END 08-01-2013  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
					if ($id_sc_detail == '')
						$id_sc_detail = 0;
					// jika semua data tdk kosong, insert ke tm_apply_stok_proses_cutting_detail
					$data_detail = array(
						'kode_brg'=>$kode,
						'plan_qty_cutting'=>$plan_qty,
						'qty_pjg_kain'=>$pjg_kain,
						'gelar_pjg_kain'=>$gelar_pjg_kain,
						'status_stok'=>'t',
						'id_apply_stok'=>$id_bonm,
						'jenis_proses'=>$jenis_proses,
						'kode_brg_quilting'=>$kode_brg_quilting,
						'kode_brg_bisbisan'=>$kode_brg_bisbisan,
						'id_ukuran_bisbisan'=>$ukuran_bisbisan,
						'kode_brg_jadi'=>$kode_brg_jadi,
						'harga'=>$harganya,
						'id_schedule_cutting_detail'=>$id_sc_detail,
						'id_pb_cutting_detail'=>$id_pb_detail,
						'bordir'=>$bordir,
						'for_bs_cutting'=>$bscutting,
						'for_kekurangan_cutting'=>$kcutting,
						'brg_jadi_manual'=>$nama_brg_jadi_other
					);
					$this->db->insert('tm_apply_stok_proses_cutting_detail',$data_detail); 
					// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end modifikasi 5 okt 2011
					
				/*	$query3	= $this->db->query(" INSERT INTO ".$nama_tabel_tt." (kode_brg, no_bukti, keluar, saldo, tgl_input) 
											VALUES ('$kode','$no_bonm', '$qty_sat_awal', '$new_stok', '$tgl') "); */

				// ###########################		
			}
		
		
  }
    
  function delete($kode, $list_brg){    
	  $tgl = date("Y-m-d");
	  
	  $query3	= $this->db->query(" SELECT id_pb_cutting FROM tm_apply_stok_proses_cutting WHERE id = '$kode' ");
	  $hasilrow = $query3->row();
	  $id_pb_cutting	= $hasilrow->id_pb_cutting;  
	  
	  /*$query3	= $this->db->query(" SELECT id FROM tm_pb_cutting WHERE no_pb_cutting = '$no_pb_cutting' ");
	  $hasilrow = $query3->row();
	  $id_pb_cutting	= $hasilrow->id;  */
	  
	  $id_brg = explode("-", $list_brg);
	  
	  foreach($id_brg as $row1) {
		if ($row1 != '') {
			$this->db->query(" UPDATE tm_pb_cutting_detail SET status_pb = 'f' where id= '$row1' ");
		}
	  }
	  
	  $this->db->query(" UPDATE tm_pb_cutting SET status_pb = 'f', status_edit = 'f' where id= '$id_pb_cutting' ");
	  
	  $query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_proses_cutting WHERE id = '$kode' ");
		$hasilrow = $query3->row();
		$no_bonm	= $hasilrow->no_bonm; 
		// reset stoknya
		// 08-01-2013, WARNING! reset stok bukan disini, tapi di bon M keluar.
		// ambil data detail barangnya
			/*	$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting_detail 
											WHERE id_apply_stok = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// cek dulu satuan brgnya. jika satuannya yard, maka qty di detail ini konversikan lagi ke yard
							$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$satuan	= '';
								}
								
								if ($satuan == "Yard") {
									$qty_sat_awal = $row2->qty_pjg_kain / 0.90; // 07-03-2012, rubah dari 0.91 ke 0.90
								}
								else {
									$qty_sat_awal = $row2->qty_pjg_kain;
								}
						
						// ============ update stok =====================
						
						$nama_tabel_stok = "tm_stok";
						$nama_tabel_tt = "tt_stok";
				
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty_sat_awal; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' ");
								}
								
								// 16-02-2012: update stok harga for reset
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$row2->kode_brg' 
															AND harga = '$row2->harga' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$stokreset = $stok_lama+$qty_sat_awal;
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg= '$row2->kode_brg' AND harga = '$row2->harga' "); //
								//&&&&&&& end 16-02-2012 &&&&&&&&&&&
						// ==============================================
					}
				} */
		
		$this->db->delete('tm_apply_stok_proses_cutting_detail', array('id_apply_stok' => $kode));
		$this->db->delete('tm_apply_stok_proses_cutting', array('id' => $kode));

  }
  
  function get_pb_cutting($num, $offset, $cari) {
	  // ambil data PB yg status_pb = FALSE
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_pb_cutting WHERE status_pb = 'f' order by tgl DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pb_cutting WHERE status_pb = 'f' AND UPPER(no_pb_cutting) like UPPER('%$cari%') order by tgl DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_pb = array();
		$detail_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
							
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pb_cutting_detail WHERE id_pb_cutting = '$row1->id' AND status_pb = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						// -------------------
						
						/* $query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a, tm_pp b 
									WHERE b.id = '$id_pp' AND a.id_pp = b.id AND a.kode_brg = '$kode' ");
						$hasilrow = $query3->row();
						$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
						*/
						
					/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$row1->id' AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
						
						// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
						$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b 
									WHERE a.id_pembelian = b.id ";
						//if ($cek_pp == 0)			
						//	$sql3.= " AND b.id_op = '$row1->id' ";
						//else
							$sql3.= " AND b.id_pp = '$row1->id' ";
							
						$sql3.= " AND a.kode_brg = '$row2->kode_brg' ";
						
						$query3	= $this->db->query($sql3);
						$hasilrow = $query3->row();
						$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
						// #######################################################
						
						$qty = $row2->qty-$jum_op-$jum_ppsj; */
						
						//--------------------
				
						$detail_pb[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												//'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'gelaran'=> $row2->gelaran,
												'jum_set'=> $row2->jum_set,
												'jum_gelar'=> $row2->jum_gelar,
												'panjang_kain'=> $row2->panjang_kain,
												'jenis_proses'=> $row2->jenis_proses,
												'bordir'=> $row2->bordir,
												'for_kekurangan_cutting'=> $row2->for_kekurangan_cutting,
												'for_bs_cutting'=> $row2->for_bs_cutting
											);
					}
				}
				else {
					$detail_pb = '';
				}
				
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$row1->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							if ($row1->is_brg_jadi_manual == 'f')
								$nama_brg_jadi = '';
							else
								$nama_brg_jadi = $row1->brg_jadi_manual;
						}
				
				$data_pb[] = array(			'id'=> $row1->id,	
											'no_pb_cutting'=> $row1->no_pb_cutting,
											'tgl'=> $row1->tgl,
											'tgl_update'=> $row1->tgl_update,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'detail_pb'=> $detail_pb
											);
				$detail_pb = array();
			} // endforeach header
		}
		else {
			$data_pb = '';
		}
		return $data_pb;
  }
  
  function get_pb_cuttingtanpalimit($cari){
	if ($cari == "all") {
		$query = $this->db->getwhere('tm_pb_cutting',array('status_pb'=>"f"));
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pb_cutting WHERE status_pb = 'f' AND UPPER(no_pb_cutting) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_detail_pb($id_pb, $list_brg){
    $detail_pb = array();
        
    foreach($list_brg as $row1) {
			if ($row1 != '') {
				$query2	= $this->db->query(" SELECT a.kode_brg_jadi, a.is_brg_jadi_manual, a.brg_jadi_manual, 
								b.kode_brg, b.qty, b.gelaran, b.jum_set, b.jum_gelar, b.panjang_kain, 
								b.jenis_proses, b.kode_brg_quilting, b.id_ukuran_bisbisan, b.kode_brg_bisbisan,
								b.bordir, b.for_kekurangan_cutting, b.for_bs_cutting
								FROM tm_pb_cutting a, tm_pb_cutting_detail b WHERE a.id = b.id_pb_cutting AND b.id = '$row1'
								AND a.id = '$id_pb' ");
				/*$sql= "SELECT a.kode_brg_jadi, a.is_brg_jadi_manual, a.brg_jadi_manual, 
								b.kode_brg, b.qty, b.gelaran, b.jum_set, b.jum_gelar, b.panjang_kain, 
								b.jenis_proses, b.kode_brg_quilting, b.id_ukuran_bisbisan, b.kode_brg_bisbisan,
								b.bordir, b.for_kekurangan_cutting, b.for_bs_cutting
								FROM tm_pb_cutting a, tm_pb_cutting_detail b WHERE a.id = b.id_pb_cutting AND b.id = '$row1'
								AND a.no_pb_cutting = '$no_pb'"; echo $sql; */
				if ($query2->num_rows() > 0){
					$hasilrow = $query2->row();
					$kode_brg_jadi	= $hasilrow->kode_brg_jadi;
					$kode_brg	= $hasilrow->kode_brg;
					$qty		= $hasilrow->qty;
					$gelaran	= $hasilrow->gelaran;
					$jum_set	= $hasilrow->jum_set;
					$jum_gelar	= $hasilrow->jum_gelar;
					$panjang_kain	= $hasilrow->panjang_kain;
					$jenis_proses	= $hasilrow->jenis_proses;
					$kode_brg_quilting	= $hasilrow->kode_brg_quilting;
					$ukuran_bisbisan	= $hasilrow->id_ukuran_bisbisan;
					$kode_brg_bisbisan	= $hasilrow->kode_brg_bisbisan;
					$bordir	= $hasilrow->bordir;
					$kcutting	= $hasilrow->for_kekurangan_cutting;
					$bscutting	= $hasilrow->for_bs_cutting;
					
					$is_brg_jadi_manual	= $hasilrow->is_brg_jadi_manual;
					$brg_jadi_manual	= $hasilrow->brg_jadi_manual;
    		
						/* $query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a, tm_pp b 
									WHERE b.id = '$id_pp' AND a.id_pp = b.id AND a.kode_brg = '$kode' ");
						$hasilrow = $query3->row();
						$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
						*/
						
					/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$id_pp' AND a.kode_brg = '$kode_brg' ");
						$hasilrow = $query3->row();
						$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
						
						// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
						$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b 
									WHERE a.id_pembelian = b.id ";
						$sql3.= " AND b.id_pp = '$id_pp' ";
						$sql3.= " AND a.kode_brg = '$kode_brg' ";
						
						$query3	= $this->db->query($sql3);
						$hasilrow = $query3->row();
						$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
						// #######################################################
						
						$qty = $qty-$jum_op-$jum_ppsj; */
			
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE b.id = a.satuan AND a.kode_brg = '$kode_brg' ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() > 0){
						$nama_brg	= $hasilrow->nama_brg;
						$nama_satuan	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg	= '';
						$nama_satuan	= '';
					}
					
					// bhn quilting jika ada
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE b.id = a.satuan AND a.kode_brg = '$kode_brg_quilting' ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() > 0){
						$nama_brg_quilting	= $hasilrow->nama_brg;
						$nama_satuan_quilting	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg_quilting	= '';
						$nama_satuan_quilting	= '';
					}
					
					// bhn bisbisan jika ada
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b 
										WHERE b.id = a.satuan AND a.kode_brg = '$kode_brg_bisbisan' ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() > 0){
						$nama_brg_bisbisan	= $hasilrow->nama_brg;
						$nama_satuan_bisbisan	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg_bisbisan	= '';
						$nama_satuan_bisbisan	= '';
					}
				
				// cek dulu satuan brgnya. jika satuannya yard, maka konversikan ke (m)						
				/*			if ($nama_satuan == "Yard") {
								$qty_sat_awal = $panjang_kain / 0.91;
							}
							else
								$qty_sat_awal = $panjang_kain; */
					
					//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode_brg' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok / 0.90; // 07-03-2012, rubah dari 0.91 ke 0.90
					}
					
					$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$kode_brg_jadi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						$nama_brg_jadi_other = '';
					}
					else {
						if ($is_brg_jadi_manual == 'f') {
							$nama_brg_jadi = '';
							$nama_brg_jadi_other = '';
						}
						else {
							$nama_brg_jadi = $brg_jadi_manual;
							$nama_brg_jadi_other = $brg_jadi_manual;
						}
					}
			
				$detail_pb[] = array(		'id'=> $row1,
											'kode_brg'=> $kode_brg,
											'nama'=> $nama_brg,
											'nama_satuan'=> $nama_satuan,
											'qty'=> $qty,
											'gelaran'=> $gelaran,
											'jum_set'=> $jum_set,
											'jum_gelar'=> $jum_gelar,
											'panjang_kain'=> $panjang_kain,
											'stok_lama'=> $stok_lama,
											'jenis_proses'=> $jenis_proses,
											'kode_brg_quilting'=> $kode_brg_quilting,
											'nama_brg_quilting'=> $nama_brg_quilting,
											'ukuran_bisbisan'=> $ukuran_bisbisan,
											'kode_brg_bisbisan'=> $kode_brg_bisbisan,
											'nama_brg_bisbisan'=> $nama_brg_bisbisan,
											'kode_brg_jadi'=> $kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'nama_brg_jadi_other'=> $nama_brg_jadi_other,
											'bordir'=> $bordir,
											'kcutting'=> $kcutting,
											'bscutting'=> $bscutting
									);
			} // end if query2
		} // end if row1
	}
	return $detail_pb;
  }
  
  function get_bonm($id_bonm){
	$query	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting WHERE id = '$id_bonm' ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	$detail_bonm = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting_detail WHERE id_apply_stok = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// ambil jml gelar/set
						$query3	= $this->db->query(" SELECT b.gelaran, b.jum_set, b.jum_gelar FROM tm_pb_cutting a, tm_pb_cutting_detail b
									WHERE a.id = b.id_pb_cutting AND a.no_pb_cutting = '$row1->no_pb_cutting'
									AND b.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$gelaran	= $hasilrow->gelaran;
							$jum_set	= $hasilrow->jum_set;
							$jum_gelar	= $hasilrow->jum_gelar;
						}
						else {
							$gelaran	= '';
							$jum_set	= '';
						}
						
						// cek dulu satuan brgnya. jika satuannya yard, maka konversikan ke (m)						
					/*	if ($satuan == "Yard") {
							$qty_sat_awal = $qty_pjg_kain / 0.91;
						}
						else
							$qty_sat_awal = $qty_pjg_kain; */
				
						//cek stok terakhir tm_stok
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$stok_lama = $stok_lama / 0.90; // 07-03-2012, rubah dari 0.91 ke 0.90
						}
						
						// bhn quilting jika ada
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
											WHERE b.id = a.satuan AND a.kode_brg = '$row2->kode_brg_quilting' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() > 0){
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$nama_satuan_quilting	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_quilting	= '';
							$nama_satuan_quilting	= '';
						}
						
						// bhn bisbisan jika ada
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b 
											WHERE b.id = a.satuan AND a.kode_brg = '$row2->kode_brg_bisbisan' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() > 0){
							$nama_brg_bisbisan	= $hasilrow->nama_brg;
							$nama_satuan_bisbisan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_bisbisan	= '';
							$nama_satuan_bisbisan	= '';
						}
						
						// ambil nama ukuran bisbisan jika ada
						if ($row2->id_ukuran_bisbisan != '') {
							$query3	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan
												WHERE id = '$row2->id_ukuran_bisbisan' ");
							$hasilrow = $query3->row();
							if ($query3->num_rows() > 0){
								$nama_ukuran_bisbisan	= $hasilrow->nama;
							}
							else
								$nama_ukuran_bisbisan = '';
						}
						else {
							$nama_ukuran_bisbisan	= '';
						}
						
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							if ($row2->brg_jadi_manual == '')
								$nama_brg_jadi = '';
							else
								$nama_brg_jadi = $row2->brg_jadi_manual;
						}
						
						$query3	= $this->db->query(" SELECT a.no_schedule FROM tt_schedule_cutting a, tt_schedule_cutting_detail b 
								WHERE a.id=b.id_schedule_cutting AND b.id = '$row2->id_schedule_cutting_detail' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$no_schedule	= $hasilrow->no_schedule;
						}
						else
							$no_schedule = '';
						
						$detail_bonm[] = array('id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'nama_satuan'=> $satuan,
												'qty_pjg_kain'=> $row2->qty_pjg_kain,
												'gelar_pjg_kain'=> $row2->gelar_pjg_kain,
												'plan_qty_cutting'=> $row2->plan_qty_cutting,
												'gelaran'=> $gelaran,
												'jum_set'=> $jum_set,
												'jum_gelar'=> $jum_gelar,
												'stok_lama'=> $stok_lama,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'nama_brg_jadi_other'=> $row2->brg_jadi_manual,
												'jenis_proses'=> $row2->jenis_proses,
												'ukuran_bisbisan'=> $nama_ukuran_bisbisan,
												'kode_brg_quilting'=> $row2->kode_brg_quilting,
												'nama_brg_quilting'=> $nama_brg_quilting,
												'kode_brg_bisbisan'=> $row2->kode_brg_bisbisan,
												'nama_brg_bisbisan'=> $nama_brg_bisbisan,
												'harga'=> $row2->harga,
												'id_schedule_cutting_detail'=> $row2->id_schedule_cutting_detail,
												'kcutting'=> $row2->for_kekurangan_cutting,
												'no_schedule'=> $no_schedule
											);
										
					}
				}
				else {
					$detail_bonm = '';
				}
				 
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_pb_cutting'=> $row1->no_pb_cutting,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'status_stok'=> $row1->status_stok,
											'status_edit'=> $row1->status_edit,
											'detail_bonm'=> $detail_bonm
											);
				
				$detail_bonm = array();
	}
	return $data_bonm;
  }
  
  function konversi_angka2romawi($bln_now) {
	if ($bln_now == "01")
		$romawi_bln = "I";
	else if ($bln_now == "02")
		$romawi_bln = "II";
	else if ($bln_now == "03")
		$romawi_bln = "III";
	else if ($bln_now == "04")
		$romawi_bln = "IV";
	else if ($bln_now == "05")
		$romawi_bln = "V";
	else if ($bln_now == "06")
		$romawi_bln = "VI";
	else if ($bln_now == "07")
		$romawi_bln = "VII";
	else if ($bln_now == "08")
		$romawi_bln = "VIII";
	else if ($bln_now == "09")
		$romawi_bln = "IX";
	else if ($bln_now == "10")
		$romawi_bln = "X";
	else if ($bln_now == "11")
		$romawi_bln = "XI";
	else if ($bln_now == "12")
		$romawi_bln = "XII";
		
	return $romawi_bln;
  }
  
  function konversi_romawi2angka($blnrom) {
	if ($blnrom == "I")
		$bln_now = "01";
	else if ($blnrom == "II")
		$bln_now = "02";
	else if ($blnrom == "III")
		$bln_now = "03";
	else if ($blnrom == "IV")
		$bln_now = "04";
	else if ($blnrom == "V")
		$bln_now = "05";
	else if ($blnrom == "VI")
		$bln_now = "06";
	else if ($blnrom == "VII")
		$bln_now = "07";
	else if ($blnrom == "VIII")
		$bln_now = "08";
	else if ($blnrom == "IX")
		$bln_now = "09";
	else if ($blnrom == "X")
		$bln_now = "10";
	else if ($blnrom == "XI")
		$bln_now = "11";
	else if ($blnrom == "XII")
		$bln_now = "12";
		
	return $bln_now;
  }
  
  function konversi_bln2deskripsi($bln1) {
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";  
		return $nama_bln;
  }
  
  function get_ukuran_bisbisan(){
    $this->db->select("* from tm_ukuran_bisbisan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //16-03-2012
  function get_sched_cutting($num, $offset, $cari) {
	  // ambil data2 sched cutting yg status_realisasi = FALSE
	  $sql= " a.no_schedule, a.tgl_cutting, a.tgl_update, b.* FROM tt_schedule_cutting a, tt_schedule_cutting_detail b 
			WHERE a.id = b.id_schedule_cutting AND a.status_realisasi = 'f' AND b.status_realisasi = 'f'
			AND a.is_dacron = 'f' ";
	  if ($cari != "all")
		$sql.= " AND UPPER(a.no_schedule) like UPPER('%$cari%') ";
	  $sql.=" order by a.tgl_cutting DESC ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  $query = $this->db->get();

      $data_sc = array();
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$nama_brg	= $hasilrow->nama_brg;
				$satuan	= $hasilrow->nama_satuan;
			}
			else {
				$nama_brg	= '';
				$satuan	= '';
			}
			
			// bhn quilting jika ada
			$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE b.id = a.satuan AND a.kode_brg = '$row1->kode_brg_quilting' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() > 0){
				$nama_brg_quilting	= $hasilrow->nama_brg;
				$nama_satuan_quilting	= $hasilrow->nama_satuan;
			}
			else {
				$nama_brg_quilting	= '';
				$nama_satuan_quilting	= '';
			}
			
			$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$row1->kode_brg_jadi' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$nama_brg_jadi	= $hasilrow->e_product_motifname;
			}
			else {
				$nama_brg_jadi = '';
			}
				
			$data_sc[] = array(			'id'=> $row1->id,	
										'id_schedule_cutting'=> $row1->id_schedule_cutting,	
										'no_schedule'=> $row1->no_schedule,
										'tgl_cutting'=> $row1->tgl_cutting,
										'tgl_update'=> $row1->tgl_update,
										'kode_brg_jadi'=> $row1->kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'kode_brg_quilting'=> $row1->kode_brg_quilting,
										'nama_brg_quilting'=> $nama_brg_quilting,
										'satuan_quilting'=> $nama_satuan_quilting,
										'kode_brg'=> $row1->kode_brg,
										'nama_brg'=> $nama_brg,
										'satuan'=> $satuan,
										'qty_bhn'=> $row1->qty_bhn,
										'qty_bhn'=> $row1->qty_bhn,
										'jam_mulai'=> $row1->jam_mulai,
										'jam_selesai'=> $row1->jam_selesai,
										'operator_cutting'=> $row1->operator_cutting,
										'keterangan'=> $row1->keterangan
										);
		} // endforeach header
	  }
	  else {
		$data_sc = '';
	  }
	  return $data_sc;
  }
  
  function get_sched_cuttingtanpalimit($cari){
	// ambil data2 sched cutting yg status_realisasi = FALSE
	  $sql= " SELECT a.no_schedule, a.tgl_cutting, a.tgl_update, b.* FROM tt_schedule_cutting a, tt_schedule_cutting_detail b 
			WHERE a.id = b.id_schedule_cutting AND a.status_realisasi = 'f' AND b.status_realisasi = 'f'
			AND a.is_dacron = 'f' ";
	  if ($cari != "all")
		$sql.= " AND UPPER(a.no_schedule) like UPPER('%$cari%') ";
	  $sql.=" order by a.tgl_cutting DESC ";
      
      $query = $this->db->query($sql);
    
    return $query->result();  
  }

}

