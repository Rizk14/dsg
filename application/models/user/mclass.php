<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_user_name ";
		$query	= $db2->query(" SELECT * FROM tm_user ".$order." ", false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function LevelUser() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY i_level ";
		$query	= $db2->query(" SELECT * FROM tm_level_user ".$order." ", false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	
	}
	
	function CekUser($username) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_user WHERE e_user_name='$username' AND i_user_active='1' ");
	}
	
	function msimpan($e_user_name,$passwd,$ilevel,$iuseractive) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	

		$seq_tm_user	= $db2->query(" SELECT cast(i_user_id AS integer) AS iuserid FROM tm_user ORDER BY cast(i_user_id AS integer) DESC LIMIT 1 ");
		if($seq_tm_user->num_rows() > 0 ) {
			$seqrow	= $seq_tm_user->row();
			$i_user_id	= $seqrow->iuserid+1;		
		} else {
			$i_user_id	= 1;		
		}
							
		$tm_user	= array(
			'i_user_id'=>$i_user_id,
			'e_user_name'=>$e_user_name,
			'e_password'=>$passwd,
			'd_user_join'=>$dentry,
			'i_user_active'=>$iuseractive,
			'i_level'=>$ilevel,
			'd_user_login_date'=>$dentry
			);
		
		$db2->insert('tm_user',$tm_user);
		
		redirect('user/cform/');
    }

  function get($id){
	  $db2=$this->load->database('db_external', TRUE);
    $query = $db2->get_where('tm_user',array('i_user_id'=>$id));
    return $query->row_array();		  
  }
  
  function getid($i_user_id,$e_user_name,$e_password_old_enkri) {
	  $db2=$this->load->database('db_external', TRUE);
  	return $db2->query(" SELECT * FROM tm_user WHERE i_user_id='$i_user_id' AND e_user_name='$e_user_name' AND e_password='$e_password_old_enkri' AND i_user_active='1' " );
  }
  
  function update($i_user_id,$e_user_name,$e_password_new_enkri) {
	$db2=$this->load->database('db_external', TRUE);
  	$user_item	= array(
		'e_password'=>$e_password_new_enkri
	);
	
	$db2->update('tm_user',$user_item,array('i_user_id'=>$i_user_id,'e_user_name'=>$e_user_name));
	
	redirect('user/cform');
  }
  
  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
	 $db2->delete('tm_user',array('i_user_id'=>$id)); 
	 redirect('user/cform');
  }
}
?>
