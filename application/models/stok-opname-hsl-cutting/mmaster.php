<?php
class Mmaster extends CI_Model{
  function __construct() { 
  parent::__construct();

}
  
  function cek_so_hasil_cutting($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id, tgl_so, status_approve FROM tt_stok_opname_hasil_cutting
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
				
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so
							);
							
		return $so_bahan;
  }

  function get_all_stok_hasil_cutting($bulan, $tahun) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
	  
	  // 25-11-2014. pembaharuan 20-10-2015
		$query	= $this->db->query(" SELECT a.id, a.id_brg_wip, c.kode_brg, c.nama_brg, a.stok
					FROM tm_stok_hasil_cutting a INNER JOIN tm_barang_wip c ON c.id = a.id_brg_wip
					ORDER BY c.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_warna = array();
			foreach ($hasil as $row) {
				
				// 06-04-201. 20-10-2015 GA DIPAKE!
				// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field jum_stok_opname
				/*$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_cutting a 
									INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
									WHERE b.id_brg_wip = '$row->id_brg_wip' 
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				//	2.1. masuk bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a 
							INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
							WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.id_brg_wip = '$row->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//	2.2. masuk retur bhn baku
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
							INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
							WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.id_brg_wip = '$row->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk+= $hasilrow->jum_masuk;
				}
				
				// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
							INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
							WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.id_brg_wip = '$row->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar; */
				//-------------------------------------------------------------------------------------
				
				// 1. ambil stok berdasarkan warna			
				$sqlxx = " SELECT c.stok, c.id_warna, b.nama as nama_warna 
						FROM tm_stok_hasil_cutting_warna c INNER JOIN tm_warna b ON c.id_warna = b.id
						WHERE c.id_stok_hasil_cutting = '".$row->id."'
						ORDER BY b.nama ";
				
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					
					foreach ($hasilxx as $rowxx) {
						
						// 06-04-2015. 20-10-2015 GA DIPAKE
						// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
					/*	$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a 
											INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
											INNER JOIN tt_stok_opname_hasil_cutting_detail_warna c ON b.id = c.id_stok_opname_hasil_cutting_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' 
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						//	2.1. masuk bonmmasukcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmmasukcutting a 
									INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
									INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						//	2.2. masuk retur bhn baku
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna+= $hasilrow->jum_masuk;
						}
						
						// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna; */
						//-------------------------------------------------------------------------------------
						
						$detail_warna[] = array(
										'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> $rowxx->stok,
										'stok_opname'=> $rowxx->stok
										);
					}
				}
				
				$detail_bahan[] = array('id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  /*function get_all_stok_opname($bulan, $tahun) {
		$query	= $this->db->query(" SELECT b.* FROM tt_stok_opname_hasil_cutting_detail b, tt_stok_opname_hasil_cutting a
					WHERE b.id_stok_opname_hasil_cutting = a.id 
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY b.kode_brg_jadi, b.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
						// bhn baku
						$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang 
										WHERE kode_brg = '$row->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$query3	= $this->db->query(" SELECT nama_brg FROM tm_brg_hasil_makloon
										WHERE kode_brg = '$row->kode_brg' ");
							if ($query3->num_rows() > 0){
								$nama_brg	= $hasilrow->nama_brg;
							}
							else
								$nama_brg = '';
						}
				
				// ambil nama brg jadi dan stok terkini
				$query3	= $this->db->query(" SELECT a.kode_brg, a.kode_brg_jadi, a.stok, c.e_product_motifname
					FROM tm_stok_hasil_cutting a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi
					AND a.kode_brg= '$row->kode_brg' AND a.kode_brg_jadi = '$row->kode_brg_jadi' ");
					
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg_jadi	= '';
					$stok = '';
				}
				
				$detail_bahan[] = array('kode_brg'=> $row->kode_brg,
										'nama_brg'=> $nama_brg,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  } */
  
  //function save($is_new, $id_stok, $kode_brg, $kode_brg_jadi, $stok, $stok_fisik){ 
  //$kode_bhn, $kode_warna_bhn, $stok_bhn, $stok_fisik_bhn. 13-12-2014 ga dipake
  // modif 25-09-2015
  function save($is_new, $id_stok, $is_pertamakali, $id_brg_wip, $id_warna, $stok, $stok_fisik_warna) { 
	  $tgl = date("Y-m-d H:i:s"); 
	  // 01-12-2014
	  //-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokawal = 0;
		$qtytotalstokfisikwarna = 0;
		//$qtytotalstokfisikbgs = 0;
		//$qtytotalstokfisikperbaikan = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$qtytotalstokawal+= $stok[$xx];				
			$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
			$qtytotalstokfisikwarna+= $stok_fisik_warna[$xx];
		} // end for
	// ---------------------------------------------------------------------
	  
	  if ($is_new == '1') {
		  $data_detail = array(
						'id_stok_opname_hasil_cutting'=>$id_stok,
						'id_brg_wip'=>$id_brg_wip, 
						'stok_awal'=>$qtytotalstokawal,
						'jum_stok_opname'=>$qtytotalstokfisikwarna
					);
		   $this->db->insert('tt_stok_opname_hasil_cutting_detail',$data_detail);
		   
		   // 01-12-2014
		   $seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting_detail ORDER BY id DESC LIMIT 1 ");
		   if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
		   }
		   else
				$iddetail = 0;
				
		   // ------------------stok berdasarkan warna brg wip----------------------------
		   if (isset($id_warna) && is_array($id_warna)) {
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
				
				$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting_detail_warna ORDER BY id DESC LIMIT 1 ");
				if($seq_warna->num_rows() > 0) {
					$seqrow	= $seq_warna->row();
					$idbaru	= $seqrow->id+1;
				}else{
					$idbaru	= 1;
				}
				
				$tt_stok_opname_hasil_cutting_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_stok_opname_hasil_cutting_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'jum_stok_opname'=>$stok_fisik_warna[$xx]
					);
				$this->db->insert('tt_stok_opname_hasil_cutting_detail_warna',$tt_stok_opname_hasil_cutting_detail_warna);
			} // end for
		  } // end if
	  }
	  else {
		 /* $this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail SET jum_stok_opname = '$stok_fisik' 
						where kode_brg= '$kode_brg' AND kode_brg_jadi = '$kode_brg_jadi' 
						AND id_stok_opname_hasil_cutting = '$id_stok' "); */
		
		// ambil id detail id_stok_opname_hasil_cutting_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting_detail 
						where id_brg_wip = '$id_brg_wip' 
						AND id_stok_opname_hasil_cutting = '$id_stok' ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
		  
		  $totalxx = $qtytotalstokfisikwarna;
		  
		  $this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail SET jum_stok_opname = '".$totalxx."' where id = '$iddetail' ");
		  
		  if (isset($id_warna) && is_array($id_warna)) {
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok_fisik_warna[$xx] = trim($stok_fisik_warna[$xx]);
				
				$totalxx = $stok_fisik_warna[$xx];
				
				$this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail_warna SET jum_stok_opname = '".$totalxx."'
							WHERE id_stok_opname_hasil_cutting_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
			}
		  } // end if
	  } // end if
  }
  
  // 01-12-2014
  function get_all_stok_opname_hasil_cutting($bulan, $tahun) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.kode_brg, d.nama_brg 
					FROM tt_stok_opname_hasil_cutting_detail b 
					INNER JOIN tt_stok_opname_hasil_cutting a ON b.id_stok_opname_hasil_cutting = a.id 
					INNER JOIN tm_barang_wip d ON b.id_brg_wip = d.id
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' 
					AND a.status_approve = 'f' AND b.status_approve = 'f' ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				// 06-04-2015. 25-09-2015 GA DIPAKE
				// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a, 
									tt_stok_opname_hasil_cutting_detail b
									WHERE a.id = b.id_stok_opname_hasil_cutting
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' 
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				//	2.1. masuk bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//	2.2. masuk retur bhn baku
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk+= $hasilrow->jum_masuk;
				}
				
				// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar; */
				//-------------------------------------------------------------------------------------
				
				// 1. berdasarkan warna brg wip
				$sqlxx = " SELECT a.*, c.id, c.nama FROM tt_stok_opname_hasil_cutting_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_stok_opname_hasil_cutting_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_cutting_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_cutting a INNER JOIN tm_stok_hasil_cutting_warna b ON a.id = b.id_stok_hasil_cutting
							WHERE a.id_brg_wip = '$row->id_brg_wip' 
							AND b.id_warna = '$rowxx->id' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// 06-04-2015. 25-09-2015 GA DIPAKE LG
						// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
					/*	$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a 
											INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
											INNER JOIN tt_stok_opname_hasil_cutting_detail_warna c ON b.id = c.id_stok_opname_hasil_cutting_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' 
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						//	2.1. masuk bonmmasukcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmmasukcutting a 
									INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
									INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						//	2.2. masuk retur bhn baku
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna+= $hasilrow->jum_masuk;
						}
						
						// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna; */
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  } 
  
  // 25-09-2015
  function cek_sokosong_hasil_cutting() {
	$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting ");
	if ($query3->num_rows() > 0){
		return 'f';
	}
	else {
		return 't';
	}
  }
  
  // 25-09-2015
  function get_barang($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " a.* FROM tm_barang_wip a WHERE a.status_aktif = 't' ORDER BY a.nama_brg ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.* FROM tm_barang_wip a 
				WHERE a.status_aktif = 't' AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
				order by a.kode_brg";
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// query keterangan warnanya
			$data_bhn[] = array(	'id_brg'=> $row1->id,	
									'kode_brg'=> $row1->kode_brg,	
									'nama_brg'=> $row1->nama_brg
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_barangtanpalimit($cari){
	if ($cari == "all") {
		$sql = " select a.* FROM tm_barang_wip a
					WHERE a.status_aktif = 't' ORDER BY a.nama_brg ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " select a.* FROM tm_barang_wip a WHERE a.status_aktif = 't' 
				AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
				
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }

}
