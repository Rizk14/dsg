<?php
class Mmaster extends CI_Model {
  function __construct() { 

  parent::__construct();

}
  
  function detail_realisasi($tgl_new){
	  /*$sql = "SELECT a.operator_cutting FROM tt_realisasi_cutting_detail a
INNER JOIN tt_realisasi_cutting b ON b.id=a.id_realisasi_cutting WHERE b.tgl_realisasi='$tgl_new' GROUP BY a.operator_cutting";
	echo $sql; */

  	$qrealisasi	= $this->db->query(" SELECT a.operator_cutting FROM tt_realisasi_cutting_detail a
INNER JOIN tt_realisasi_cutting b ON b.id=a.id_realisasi_cutting WHERE b.tgl_realisasi='$tgl_new' GROUP BY a.operator_cutting  ");

	$data_realisasi	= array();
	$data_realisasi_detail = array();
	
	//$indek1 = 0;
	//$indek2 = 0;
	
	if($qrealisasi->num_rows()>0){
		$result	= $qrealisasi->result();
		foreach($result as $row){
			
			$qrealisasi_detail	= $this->db->query(" SELECT b.no_realisasi, a.kode_brg_jadi, a.kode_brg FROM tt_realisasi_cutting_detail a 
					INNER JOIN tt_realisasi_cutting b ON b.id=a.id_realisasi_cutting WHERE a.operator_cutting='$row->operator_cutting' 
					AND b.tgl_realisasi = '$tgl_new'
					GROUP BY b.no_realisasi, a.kode_brg_jadi, a.kode_brg 
					ORDER BY a.kode_brg_jadi DESC ");
			if($qrealisasi_detail->num_rows()>0){
				
				$result2 = $qrealisasi_detail->result();
				
				foreach($result2 as $row2){
				
					$kodebrgjadi = $row2->kode_brg_jadi;
					$kodebrg	 = $row2->kode_brg;
					$norealisasi = $row2->no_realisasi;
					
					$qjk	= $this->db->query(" SELECT jam_mulai, jam_selesai FROM tt_realisasi_cutting_detail WHERE kode_brg_jadi='$kodebrgjadi' AND kode_brg='$kodebrg' ");
					if($qjk->num_rows()>0){
						
						$total = 0;
						$result3 = $qjk->result();
						
						foreach($result3 as $row3){
							
						  list($h,$m,$s) = explode(":",$row3->jam_mulai);
						  $dtawal = mktime($h,$m,$s,"1","1","1");
						  list($h,$m,$s) = explode(":",$row3->jam_selesai);
						  $dtakhir = mktime($h,$m,$s,"1","1","1");
						  $dtselisih = $dtakhir-$dtawal;
						  $total = $total+$dtselisih;
						}

						$totalmenit = $total/60;
						$jam = explode(".",$totalmenit/60);
						$sisamenit  = ($totalmenit/60)-$jam[0];
						$sisamenit2 = $sisamenit*60;	
						if($sisamenit2==0)
							$sisamenit2 = '00';
						$jk	= $jam[0].".".$sisamenit2;
					}else{
						$jk	= 0;
					}
					
					$qhslrealisasi	= $this->db->query(" SELECT SUM(hasil_potong) AS hsl FROM tt_realisasi_cutting_detail WHERE kode_brg_jadi='$kodebrgjadi' AND kode_brg='$kodebrg' ");
					$rhslrealisasi	= $qhslrealisasi->row();
					$hsl	= str_replace(".",",",$rhslrealisasi->hsl);
					//$data_realisasi_detail[$indek2] = array(
					$data_realisasi_detail[] = array(
						'no_realisasi' => $norealisasi,
						'kode_brg_jadi' => $kodebrgjadi,
						'kode_brg' => $kodebrg,
						'hasil' => $hsl,
						'jk' => $jk);
				//	$indek2+=1;
				}
			}
			
			//$data_realisasi[$indek1]	= array(
			$data_realisasi[]	= array(
				'operator_cutting' => $row->operator_cutting,
				'detail' => $data_realisasi_detail);
			$data_realisasi_detail = array();
			//$indek1++;
		}
	}
	
	return $data_realisasi;
  }
  
}

