<?php
class Mreport extends CI_Model{
  function Mreport() {

  parent::__construct();
  }
  function get_adjwipunittanpalimit($id_unit, $bulan, $tahun, $s_approve){
	$sql = " select * FROM tt_adjustment_unit_packing WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_unit != '0')
		$sql.= " AND id_unit = '$id_unit' ";
	if ($s_approve != '0')
		$sql.= " AND status_approve = '$s_approve' ";	
		
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function get_adjwipunit($num, $offset, $id_unit, $bulan, $tahun, $s_approve) {	
	$sql = " * FROM tt_adjustment_unit_packing WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_unit != '0')
		$sql.= " AND id_unit = '$id_unit' ";
	if ($s_approve != '0')
		$sql.= " AND status_approve = '$s_approve' ";	
		
	$sql.= " ORDER BY tahun DESC, bulan DESC, id_unit ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$unit = $query->result();
			foreach ($unit as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit' ");
				$unitrow = $query3->row();
				$kode_unit	= $unitrow->kode_unit;
				$nama_unit	= $unitrow->nama;
				
				
					
				$query41 = $this->db->query("SELECT * FROM tm_periode_wip order by id desc limit 1");
					if($query41->num_rows() > 0){
					$unitrow41 = $query41->row();
					$tahun_periode = $unitrow41->i_year;
					$bulan_periode = $unitrow41->i_month;
					$tanggal_periode = $unitrow41->i_periode;
					}
					else{
						$tahun_periode = '';
										$bulan_periode = '';
										$tanggal_periode = '';
						}
				$data_so[] = array(			'id'=> $row1->id,	
											'tahun_periode'=> $tahun_periode,
											'bulan_periode'=> $bulan_periode,
											'tanggal_periode'=>$tanggal_periode,
											'bulan'=> $row1->bulan,	
											'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											
											
											'tgl_update'=> $row1->tgl_update,
											'id_unit'=> $row1->id_unit,	
											'kode_unit'=> $kode_unit,	
											'nama_unit'=> $nama_unit
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
   function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_adjunitpacking($id_adj) {
		$query	= $this->db->query(" SELECT a.id as id_header, a.id_unit, a.bulan, a.tahun, a.tgl_adj, 
					b.*, d.kode_brg, d.nama_brg
					FROM tt_adjustment_unit_packing_detail b INNER JOIN tt_adjustment_unit_packing a ON b.id_adjustment_unit_packing = a.id 
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.id = '$id_adj' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$unit = $query->result();
			
			$detail_bahan = array();			
			foreach ($unit as $row) {
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
		
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_packing 
								WHERE id = '$row->id_unit' ");
				$unitrow = $query3->row();
				$kode_unit	= $unitrow->kode_unit;
				$nama_unit	= $unitrow->nama;
			
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_adjustment_unit_packing_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_adjustment_unit_packing_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$unitxx = $queryxx->result();
						
					foreach ($unitxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_packing_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_unit_packing a INNER JOIN tm_stok_unit_packing_warna b ON a.id = b.id_stok_unit_packing
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_unit = '$row->id_unit'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$unitrow3 = $query3->row();
							$stok	= $unitrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'adjustment'=> $rowxx->jum_adjustment
								);
					}
				}
				else
					$detail_warna = '';
				
				if ($row->tgl_adj != '') {
					$pisah1 = explode("-", $row->tgl_adj);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_adj = $tgl1."-".$bln1."-".$thn1;
				}
				else
					$tgl_adj = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id_unit'=> $row->id_unit,
										'kode_unit'=> $kode_unit,
										'nama_unit'=> $nama_unit,
										
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'tgl_adj'=> $tgl_adj,
										'nama_bln'=> $nama_bln,
									
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
    function saveadjunitpacking($id_adj, $tgl_adj, $jenis_perhitungan_stok, $id_unit, $id_itemnya, $id_brg_wip, 
			$id_warna, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			// 11-03-2015
			//$auto_saldo_akhir_warna[$xx] = trim($auto_saldo_akhir_warna[$xx]);
			
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
	// ---------------------------------------------------------------------
		$totalxx = $qtytotalstokfisik;
		
		// ambil id detail id_adjustment_unit_packing_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_adjustment_unit_packing_detail 
					where id_brg_wip = '$id_brg_wip' 
					AND id_adjustment_unit_packing = '$id_adj' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;

			// 03-11-2015 sementara dikeluarin auto_saldo_akhir='$auto_saldo_akhir', 
		
		// =============================== 03-11-2015 SEKALIGUS UPDATE STOKNYA =======================================================
		// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// 01-02-2016 GA DIPAKE LG!
		
			
			// ======== update stoknya! =============
				//cek stok terakhir tm_stok_unit_packing, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_packing WHERE id_brg_wip = '$id_brg_wip'
							AND id_unit='$id_unit' ");
					if ($query3->num_rows() == 0){
						$data_stok = array(
							'id_brg_wip'=>$id_brg_wip,
							'id_unit'=>$id_unit,
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_packing', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_unit_packing ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$unitxx	= $sqlxx->row();
							$id_stok	= $unitxx->id;
						}else{
							$id_stok	= 1;
						}
					}
					else {
						$unitrow = $query3->row();
						$id_stok	= $unitrow->id;
						
						$this->db->query(" UPDATE tm_stok_unit_packing SET stok = '$totalxx', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_unit='$id_unit' ");
					}
					
				// stok_unit_packing_warna
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					
					$totalxx = $stok_fisik[$xx];
				
			
			// ========================= stok per warna ===============================================
					//cek stok terakhir tm_stok_unit_packing_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_packing_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_unit_packing='$id_stok' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_unit_packing_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_packing_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						//26-06-2014, pake $totalxx dari unit perhitungan transaksi keluar/masuk
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_unit_packing'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_packing_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_unit_packing_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_packing='$id_stok' ");
					}
					
					if ($id_itemnya != '0') {
						$this->db->query(" UPDATE tt_adjustment_unit_packing_detail_warna SET jum_adjustment = '".$stok_fisik[$xx]."'
							WHERE id_adjustment_unit_packing_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
					}
				} // end for
				// ----------------------------------------------
				
			// 04-02-2016
			if ($id_itemnya != '0')
				$this->db->query(" UPDATE tt_adjustment_unit_packing_detail SET jum_adjustment = '$qtytotalstokfisik'
						 where id = '$iddetail' ");
			
			if ($id_itemnya == '0') {				
				$databaru = array(
							'id_adjustment_unit_packing'=>$id_so,
							'id_brg_wip'=>$id_brg_wip, 
							'stok_awal'=>0,
							'jum_adjustment'=>$qtytotalstokfisik,
							'status_approve'=> 't'
						);
			   $this->db->insert('tt_adjustment_unit_packing_detail',$databaru);
			   
			   	$seq	= $this->db->query(" SELECT id FROM tt_adjustment_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
				if($seq->num_rows() > 0) {
					$seqrow	= $seq->row();
					$iddetailnew	= $seqrow->id;
				}else{
					$iddetailnew	= 0;
				}

			   // insert ke tabel SO warna
			   for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);				
					
					$seq_warna	= $this->db->query(" SELECT id FROM tt_adjustment_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_warna->num_rows() > 0) {
								$seqrow	= $seq_warna->row();
								$idbaru	= $seqrow->id+1;
							}else{
								$idbaru	= 1;
							}
							
							$tt_adjustment_unit_packing_detail_warna	= array(
									 'id'=>$idbaru,
									 'id_adjustment_unit_packing_detail'=>$iddetailnew,
									 'id_warna'=>$id_warna[$xx],
									 'jum_adjustment'=>$stok_fisik[$xx],
									 'saldo_akhir'=>0
								);
								$this->db->insert('tt_adjustment_unit_packing_detail_warna',$tt_adjustment_unit_packing_detail_warna);
				} // end for
			} // end if item baru
			 // ====================================================================================
		// ========================================================================================================================
  }
}
