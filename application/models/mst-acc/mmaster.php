<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari){
	if ($cari == '')
		$this->db->select("a.*, b.nama as nama_kel FROM tm_asesoris a, tm_kelompok_barang b
					WHERE a.kode_kel_barang = b.kode
					order by a.tgl_update DESC", false);
	else
		$this->db->select("a.*, b.nama as nama_kel FROM tm_asesoris a, tm_kelompok_barang b
					WHERE a.kode_kel_barang = b.kode AND (a.kode_brg like '%$cari%' OR a.nama_brg like '%$cari%')
					order by a.tgl_update DESC", false);
	
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function getAlltanpalimit($cari){
	if ($cari == '') {
		$this->db->select('*');
		$this->db->from('tm_asesoris');
	}
	else {
		$this->db->select("* from tm_asesoris where kode_brg like '%$cari%' OR nama_brg like '%$cari%'", false);
	}
    $query = $this->db->get();
    
    return $query->result();  
  }
  
  function get($id){
    $query = $this->db->getwhere('tm_asesoris',array('kode_brg'=>$id));
    return $query->result();
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data($kode){
    $this->db->select("kode_brg from tm_asesoris WHERE kode_brg = '$kode'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode,$nama, $kode_kel_brg,$satuan, $deskripsi, $harga, $kodeedit, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'kode_brg'=>$kode,
      'nama_brg'=>$nama,
      'satuan'=>$satuan,
      'kode_kel_barang'=>$kode_kel_brg,
      'deskripsi'=>$deskripsi,
      'harga'=>$harga,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_asesoris',$data); }
	else {
		$data = array(
				  'kode_brg'=>$kode,
				  'nama_brg'=>$nama,
				  'satuan'=>$satuan,
				  'kode_kel_barang'=>$kode_kel_brg,
				  'deskripsi'=>$deskripsi,
				  'harga'=>$harga,
				  'tgl_update'=>$tgl
				);
		
		$this->db->where('kode_brg',$kodeedit);
		$this->db->update('tm_asesoris',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_asesoris', array('kode_brg' => $kode));
  }

}

