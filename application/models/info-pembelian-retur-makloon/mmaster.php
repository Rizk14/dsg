<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
   function getlistunitjahit(){
	$sql = " * FROM tm_unit_jahit ORDER BY kode_unit ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
  
  
  function get_all_fakturwip($date_from, $date_to, $kode_unit, $jenis_keluar) {
		$sql = "  select distinct a.id, c.nama,a.tgl_sjpembelianretur, a.no_sjkeluarpembelian , c.kode_unit, c.nama ,a.total
				FROM tm_pembelianretur_wip a INNER JOIN tm_unit_jahit c ON c.id = a.id_unit_jahit WHERE a.status_aktif = 't' 
				AND a.tgl_sjpembelianretur >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sjpembelianretur <= to_date('$date_to','dd-mm-yyyy')  ";
		if ($kode_unit != '0')
			$sql.= " AND c.kode_unit = '$kode_unit' ";	
		$sql.= " ORDER BY c.nama ASC, a.tgl_sjpembelianretur ASC, a.no_sjkeluarpembelian  ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {		
				//print_r($row1);		
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail a
					inner join tm_sjkeluarwip b on a.id_sjkeluarwip=b.id
					LEFT JOIN tm_pembelianretur_wip_detail c ON a.id=c.id_sjkeluarwip_detail 
						WHERE c.id_pembelianretur_wip = '$row1->id' ORDER BY a.id ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tm_barang_wip WHERE id='$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan = "Pcs";
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan = '';
						}
						
						
						
						$qty	= $row2->qty;
						$harga	= $row2->harga;
						$diskon	= $row2->diskon;
						$subtotal	= $row2->total;
						$no_sj	= $row2->no_sj;
						$tgl_sjkeluar	= $row2->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sjkeluar);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sjkeluar = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												//'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'subtotal'=> $subtotal,
												'no_sj'=> $no_sj,
												'tgl_sjkeluar'=> $tgl_sjkeluar,
												'diskon'=> $diskon
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_sjpembelianretur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_sjkeluarpembelian = $tgl1." ".$nama_bln." ".$thn1;
			//	$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_sjkeluarpembelian,
											'tgl_faktur'=> $tgl_sjkeluarpembelian,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $row1->nama,
											'grandtotal'=> $row1->total,
											'detail_beli'=> $detail_beli
											);
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
     function get_all_fakturwip_for_print($date_from, $date_to, $kode_unit, $jenis_keluar) {
		$sql = "  select distinct a.id, c.nama,a.tgl_sjpembelianretur, a.no_sjkeluarpembelian , c.kode_unit, c.nama ,a.total
				FROM tm_pembelianretur_wip a INNER JOIN tm_unit_jahit c ON c.id = a.id_unit_jahit WHERE a.status_aktif = 't' 
				AND a.tgl_sjpembelianretur >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sjpembelianretur <= to_date('$date_to','dd-mm-yyyy')  ";
		if ($kode_unit != '0')
			$sql.= " AND c.kode_unit = '$kode_unit' ";	
		$sql.= " ORDER BY c.nama ASC, a.tgl_sjpembelianretur ASC, a.no_sjkeluarpembelian  ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {		
				//print_r($row1);		
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail a
					inner join tm_sjkeluarwip b on a.id_sjkeluarwip=b.id
					LEFT JOIN tm_pembelianretur_wip_detail c ON a.id=c.id_sjkeluarwip_detail 
						WHERE c.id_pembelianretur_wip = '$row1->id' ORDER BY a.id ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tm_barang_wip WHERE id='$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan = "Pieces";
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan = '';
						}
						
						
						
						$qty	= $row2->qty;
						$harga	= $row2->harga;
						$diskon	= $row2->diskon;
						$subtotal	= $row2->total;
						$no_sj	= $row2->no_sj;
						$tgl_sjkeluar	= $row2->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sjkeluar);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sjkeluar = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												//'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'subtotal'=> $subtotal,
												'no_sj'=> $no_sj,
												'tgl_sjkeluar'=> $tgl_sjkeluar,
												'diskon'=> $diskon
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_sjpembelianretur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_sjkeluarpembelian = $tgl1." ".$nama_bln." ".$thn1;
			//	$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_sjkeluarpembelian,
											'tgl_faktur'=> $tgl_sjkeluarpembelian,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $row1->nama,
											'grandtotal'=> $row1->total,
											'detail_beli'=> $detail_beli
											);
				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  function get_perusahaan() {
	$query3	= $this->db->query(" SELECT id, nama, alamat, kota, bag_pembelian, bag_pembelian2, bag_keuangan, 
						kepala_bagian, bag_admstok, bag_admstok2,
						id_gudang_admstok, id_gudang_admstok2, spv_bag_admstok FROM tm_perusahaan ");
				
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
		$bag_pembelian = $hasilrow->bag_pembelian;
		$bag_pembelian2 = $hasilrow->bag_pembelian2;
		$bag_keuangan = $hasilrow->bag_keuangan;
		$kepala_bagian = $hasilrow->kepala_bagian;
		$bag_admstok = $hasilrow->bag_admstok;
		$bag_admstok2 = $hasilrow->bag_admstok2;
		$id_gudang_admstok = $hasilrow->id_gudang_admstok;
		$id_gudang_admstok2 = $hasilrow->id_gudang_admstok2;
		$spv_bag_admstok = $hasilrow->spv_bag_admstok;
	}
	else {
		$id	= '';
		$nama = '';
		$kota = '';
		$alamat = '';
		$bag_pembelian = '';
		$bag_pembelian2 = '';
		$bag_keuangan = '';
		$kepala_bagian = '';
		$bag_admstok = '';
		$bag_admstok2 = '';
		$id_gudang_admstok = '';
		$id_gudang_admstok2 = '';
		$spv_bag_admstok = '';
	}
		
	$datasetting = array('id'=> $id,
						  'nama'=> $nama,
						  'alamat'=> $alamat,
						  'kota'=> $kota,
						  'bag_pembelian'=> $bag_pembelian,
						  'bag_pembelian2'=> $bag_pembelian2,
						  'bag_keuangan'=> $bag_keuangan,
						  'kepala_bagian'=> $kepala_bagian,
						  'bag_admstok'=> $bag_admstok,
						  'bag_admstok2'=> $bag_admstok2,
						  'id_gudang_admstok'=> $id_gudang_admstok,
						  'id_gudang_admstok2'=> $id_gudang_admstok2,
						  'spv_bag_admstok'=> $spv_bag_admstok
					);
							
	return $datasetting;
  }
  
  function get_pembelian($id_pembelian) {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian where id = '$id_pembelian' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// 10-07-2015 AMBIL NO BONM MASUK DAN TGLNYA
				$no_bonmnya = "";
				$sqlxx = " SELECT DISTINCT a.no_bonm, a.tgl_bonm FROM tm_apply_stok_pembelian_detail a INNER JOIN tm_pembelian_detail b ON a.id_pembelian_detail = b.id
							INNER JOIN tm_pembelian c ON b.id_pembelian = c.id
							WHERE c.id='$row1->id' AND a.status_stok = 't' ORDER BY a.no_bonm ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						if ($rowxx->tgl_bonm != '') {
							$pisah1 = explode("-", $rowxx->tgl_bonm);
							$thn1= $pisah1[0];
							$bln1= $pisah1[1];
							$tgl1= $pisah1[2];
							$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
							$no_bonmnya.= $rowxx->no_bonm." (".$tgl_bonm.")"."<br>";
						}
					}// end for
				}
				else {
					$no_bonmnya = '';
				}
				if ($no_bonmnya == '')
					$no_bonmnya = '-';
				
				// 18-06-2015, query utk ambil no PP / OP
				
				// 10-07-2015 DIMODIF
				$no_opnya = ""; $no_ppnya = "";
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_op, c.tgl_op FROM tm_pembelian_detail a INNER JOIN tm_op_detail b ON a.id_op_detail = b.id 
						INNER JOIN tm_op c ON c.id = b.id_op
						WHERE a.id_pembelian='$row1->id' ORDER BY c.no_op ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$pisah1 = explode("-", $rowxx->tgl_op);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_op = $tgl1."-".$bln1."-".$thn1;
						$no_opnya.= $rowxx->no_op." (".$tgl_op.")"."<br>";
					}// end for
				}
				else {
					$no_opnya = '-';
					$no_ppnya = '';
				}
				
				/*$sqlxx = " SELECT DISTINCT id_op_detail, id_pp_detail FROM tm_pembelian_detail WHERE id_pembelian='$row1->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$id_op_detail	= $rowxx->id_op_detail;
						$id_pp_detail	= $rowxx->id_pp_detail;
						
						if ($id_op_detail != '0') {
							$sqlxx2= " SELECT distinct a.no_op FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op 
									 WHERE b.id = '$id_op_detail' ";
							$queryxx2	= $this->db->query($sqlxx2);
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$no_opnya.= $hasilxx2->no_op.";";
							}
						}
						
						if ($id_pp_detail != '0') {
							$sqlxx2= " SELECT distinct a.no_pp FROM tm_pp a INNER JOIN tm_pp_detail b ON a.id = b.id_pp 
									 WHERE b.id = '$id_pp_detail' ";
							$queryxx2	= $this->db->query($sqlxx2);
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$no_ppnya.= $hasilxx2->no_pp.";";
							}
						}
					}// end for
				}
				else {
					$no_opnya = '';
					$no_ppnya = '';
				} */
				
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						$hasilrow = $query3->row();
						$nama_satuan = $hasilrow->nama;
						
						// 11-07-2015 ambil qty di apply_stok_pembelian_detail jika status_stok = t
						$query3	= $this->db->query(" SELECT qty FROM tm_apply_stok_pembelian_detail WHERE id_pembelian_detail = '$row2->id' AND status_stok='t' ");
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->row();
							$qty_fisik	= $hasil3->qty;
						}
						else
							$qty_fisik = '-';
						
						// 18-06-2015
						// 1. ambil total qty op / pp
						// 2. ambil sum beli di tabel pembelian
						//'qty_op'=> $sum_qty,
						//'jum_beli'=> $sum_beli,
						$sum_qty = 0;
						$sum_beli = 0;
					  
					  if ($row2->satuan_lain != 0) {
						  $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
						  $hasilrow = $query3->row();
						  $nama_satuan_lain = $hasilrow->nama;
					  }
					  else
						$nama_satuan_lain = '';
					  
					    $query3	= $this->db->query(" SELECT c.kode_kel_brg FROM tm_barang a INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
									WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
						
						// 31-07-2015 hitung sisa OP
						//echo $row2->id_op_detail."<br>";
						// 1. ambil qty di OP sesuai id_op_detail
						$sqlop = " SELECT b.qty FROM tm_op a INNER JOIN tm_op_detail b ON a.id = b.id_op
									WHERE b.id = '$row2->id_op_detail' ";
						$queryop	= $this->db->query($sqlop);
						if($queryop->num_rows()>0) {
							$hasilop = $queryop->row();
							$qty_op_detail = $hasilop->qty;
						}
						else
							$qty_op_detail = 0;
						
						if ($row2->id_op_detail != '0') {
							$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelian_detail a 
										INNER JOIN tm_pembelian b ON b.id=a.id_pembelian
										WHERE a.id_op_detail='$row2->id_op_detail' AND a.id_brg='$row2->id_brg' 
										AND b.status_aktif='t'
										AND b.tgl_sj <= '$row1->tgl_sj' ");
							
							if($query3->num_rows()>0) {
								$hasilrow4 = $query3->row();
								$pemenuhan = $hasilrow4->pemenuhan;
								$sisa = $qty_op_detail-$pemenuhan;
							}else{
								$pemenuhan = 0;
								$sisa = $qty_op_detail-$pemenuhan;
								
								if($pemenuhan=='')
									$pemenuhan = 0;
								
								if($sisa=='')
									$sisa = 0;
							}
						}
						else {
							$sisa = '-';
							$pemenuhan = 0;
						}
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_kel_brg'=> $kode_kel_brg,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $this->db->escape_str($nama_brg),
												'nama_satuan'=> $nama_satuan,
												'harga'=> $row2->harga,
												'qty'=> $row2->qty,
												'id_satuan'=> $row2->id_satuan,
												'id_satuan_konversi'=> $row2->id_satuan_konversi,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total,
												'qty_op'=> $sum_qty,
												'jum_beli'=> $sum_beli,
												'satuan_lain'=> $row2->satuan_lain,
												'nama_satuan_lain'=> $nama_satuan_lain,
												'qty_sat_lain'=> $row2->qty_satuan_lain,
												'id_op_detail'=> $row2->id_op_detail,
												'id_pp_detail'=> $row2->id_pp_detail,
												'qty_fisik'=> $qty_fisik,
												//31-07-2015
												'pemenuhan'=> $pemenuhan,
												'sisa'=> $sisa
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama, top FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				$top	= $hasilrow->top;
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
				
				// 31-07-2015, ambil id gudang pake distinct utk keperluan nama staf adm stok di cetak BTB
				$sqlxx = " SELECT DISTINCT d.id_gudang FROM tm_apply_stok_pembelian_detail a 
							INNER JOIN tm_pembelian_detail b ON a.id_pembelian_detail = b.id
							INNER JOIN tm_pembelian c ON b.id_pembelian = c.id
							INNER JOIN tm_barang d ON d.id = a.id_brg
							WHERE c.id='$row1->id' AND a.status_stok = 't'  ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$list_id_gudang='';
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$list_id_gudang.= $rowxx->id_gudang.";";
					}// end for
				}
				else {
					$list_id_gudang='';
				}
				
				// 27-08-2015 ambil uid_update_by dari tabel tm_apply_stok_pembelian
				$sqlxx = " SELECT uid_update_by FROM tm_apply_stok_pembelian WHERE status_aktif = 't'
						AND no_sj = '$row1->no_sj' AND id_supplier = '$row1->id_supplier' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$admgudang_uid_update_by = $hasilxx->uid_update_by;
				}
				else
					$admgudang_uid_update_by = 0;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_op'=> $no_opnya,
											'no_bonm'=> $no_bonmnya,
											'no_pp'=> $no_ppnya,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'total'=> $row1->total,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'pkp'=> $row1->pkp,
											'tipe_pajak'=> $row1->tipe_pajak,
											'dpp'=> $row1->dpp,
											'stok_masuk_lain_cash'=> $row1->stok_masuk_lain_cash,
											'stok_masuk_lain_kredit'=> $row1->stok_masuk_lain_kredit,
											'detail_fb'=> $detail_fb,
											'list_id_gudang'=> $list_id_gudang,
											'admgudang_uid_update_by'=> $admgudang_uid_update_by
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }  
  function get_sj_pembelian($date_from, $date_to, $id_unit_jahit,$jenis_keluar){
	 
		$sql = "  select distinct a.id, c.nama,a.tgl_sjpembelianretur, a.no_sjkeluarpembelian , c.kode_unit, c.administrator ,a.total, c.pkp ,c.perusahaan, c.penanggungjawab
				FROM tm_pembelianretur_wip a INNER JOIN tm_unit_jahit c ON c.id = a.id_unit_jahit WHERE a.status_aktif = 't' 
				AND a.tgl_sjpembelianretur >= to_date('$date_from','dd-mm-yyyy') AND a.tgl_sjpembelianretur <= to_date('$date_to','dd-mm-yyyy')  ";
		if ($id_unit_jahit != '0')
			$sql.= " AND c.kode_unit = '$id_unit_jahit' ";	
		$sql.= " ORDER BY c.nama ASC, a.tgl_sjpembelianretur ASC, a.no_sjkeluarpembelian  ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {		
				//print_r($row1);		
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail a
					inner join tm_sjkeluarwip b on a.id_sjkeluarwip=b.id
					LEFT JOIN tm_pembelianretur_wip_detail c ON a.id=c.id_sjkeluarwip_detail 
						WHERE c.id_pembelianretur_wip = '$row1->id' ORDER BY a.id ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT * FROM tm_barang_wip WHERE id='$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan = "Pieces";
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan = '';
						}
						
						
						
						$qty	= $row2->qty;
						$harga	= $row2->harga;
						$diskon	= $row2->diskon;
						$subtotal	= $row2->total;
						$no_sj	= $row2->no_sj;
						$tgl_sjkeluar	= $row2->tgl_sj;
						$keterangan = $row2->keterangan;
						
						$pisah1 = explode("-", $tgl_sjkeluar);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sjkeluar = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												//'kode_perk'=> $kode_perk,
												'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'subtotal'=> $subtotal,
												'no_sj'=> $no_sj,
												'tgl_sjkeluar'=> $tgl_sjkeluar,
												'keterangan'=> $keterangan,
												'diskon'=> $diskon
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_sjpembelianretur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_sjkeluarpembelian = $tgl1." ".$nama_bln." ".$thn1;
			//	$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
			
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_sjkeluarpembelian,
											'tgl_faktur'=> $tgl_sjkeluarpembelian,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $row1->nama,
											'perusahaan'=>$row1->perusahaan,
											'kontak_person'=>$row1->administrator,
											'penanggungjawab'=>$row1->penanggungjawab,
											'grandtotal'=> $row1->total,
											'pkp'=>  $row1->pkp,
											'detail_beli'=> $detail_beli
											);

				$detail_beli = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
}

