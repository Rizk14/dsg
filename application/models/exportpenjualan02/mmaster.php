<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}

	// =========================== 09-06-2015 ==============================
  function get_fakturdo($date_from, $date_to) {
	  $db2 = $this->load->database('db_external', TRUE);
	  
	  // ambil nama perusahaan (dlm hal ini duta) dari tr_initial_company
		$sqlxx = " SELECT e_initial_name, e_initial_address, e_initial_npwp, e_initial_phone
					FROM tr_initial_company ";
		$queryxx = $db2->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$e_initial_name	= $hasilxx->e_initial_name;
			$e_initial_address	= $hasilxx->e_initial_address;
			$e_initial_npwp	= $hasilxx->e_initial_npwp;
			$e_initial_phone	= $hasilxx->e_initial_phone;
		}
		else {
			$e_initial_name = '';
			$e_initial_address = '';
			$e_initial_npwp = '';
			$e_initial_phone = '';
		}
	  
	  $sql = " SELECT a.i_faktur, a.i_faktur_code, a.e_branch_name, a.d_faktur, a.v_discount,
					a.v_total_faktur, a.v_total_fppn, 
					b.i_product, b.e_product_name, v_unit_price, b.n_quantity,
					c.e_customer_name, c.e_customer_address, c.f_customer_pkp, c.e_customer_npwp,
					c.e_customer_phone
					FROM tm_faktur_do_t a 
					INNER JOIN tm_faktur_do_item_t b ON a.i_faktur = b.i_faktur
					INNER JOIN tr_customer c ON c.i_customer_code = a.e_branch_name
					
					WHERE a.d_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.d_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.f_faktur_cancel='f'
					 ";
		$sql.= " ORDER BY a.d_faktur ASC, a.i_faktur_code ASC, b.i_product ASC ";
		
		$query	= $db2->query($sql);
				
		$data_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// 10-06-2015
				// ambil bulan dan tahun dari tgl faktur utk masa pajak dan tahun pelaporan
				$pisah1 = explode("-", $row1->d_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$d_faktur = $tgl1."/".$bln1."/".$thn1;
				if ($bln1 < 10)
					$bln1new = substr($bln1, 1, 1);
				else
					$bln1new = $bln1;
		
				// ambil no faktur pajak dari tabel nomor_pajak_faktur
				$sql2 = " SELECT nomor_pajak FROM tm_nomor_pajak_faktur WHERE i_faktur_code = '$row1->i_faktur_code' ";
				$query2	= $db2->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->row();
					$nomor_pajak	= $hasil2->nomor_pajak;
					
					$nomor_pajak_baru = str_replace(".","",$nomor_pajak);
					$nomor_pajak_baru2 = str_replace("-","",$nomor_pajak_baru);
					
					$nomor_pajak_baru3 = substr($nomor_pajak_baru2, 3);
				}
				else {
					$nomor_pajak_baru3 = '';
				}
				
				// npwp hilangin titiknya
				$e_customer_npwp = str_replace(".","",$row1->e_customer_npwp);
				$ppn = $row1->v_total_fppn-$row1->v_total_faktur;
				$e_initial_npwp = str_replace(".","",$e_initial_npwp);
				
				$subtotal = $row1->n_quantity*$row1->v_unit_price;
				$ppnitem = $subtotal*0.1;
								
				$data_beli[] = array(		'i_faktur'=> $row1->i_faktur,
											'i_faktur_code'=> $row1->i_faktur_code,
											'd_faktur'=> $d_faktur,
											'tahun_pajak'=> $thn1,
											'masa_pajak'=> $bln1new,
											'nomor_pajak'=> $nomor_pajak_baru3,
											'v_discount'=> $row1->v_discount,
											'v_total_faktur'=> $row1->v_total_faktur,
											'ppn'=> $ppn,
											'v_total_fppn'=> $row1->v_total_fppn,
											'i_product'=> $row1->i_product,
											'e_product_name'=> $row1->e_product_name,
											'v_unit_price'=> $row1->v_unit_price,
											'n_quantity'=> $row1->n_quantity,
											'subtotal'=> $subtotal,
											'ppnitem'=> $ppnitem,
											
											'e_customer_name'=> $row1->e_customer_name,
											'e_customer_address'=> $row1->e_customer_address,
											'f_customer_pkp'=> $row1->f_customer_pkp,
											'e_customer_npwp'=> $e_customer_npwp,
											'e_customer_phone'=> $row1->e_customer_phone,
																						
											'e_initial_name'=> $e_initial_name,
											'e_initial_address'=> $e_initial_address,
											'e_initial_npwp'=> $e_initial_npwp,
											'e_initial_phone'=> $e_initial_phone
											);
				/*$data_beli[] = array(		$row1->i_faktur,
											$row1->i_faktur_code,
											$row1->d_faktur,
											$row1->v_discount,
											$row1->v_total_faktur,
											$ppn,
											$row1->v_total_fppn,
											$row1->i_product,
											$row1->e_product_name,
											$row1->v_unit_price,
											$row1->n_quantity,
											$row1->e_customer_name,
											$row1->e_customer_address,
											$row1->f_customer_pkp,
											$row1->e_customer_npwp,
											$row1->e_customer_phone,
																						
											$e_initial_name,
											$e_initial_address,
											$e_initial_npwp,
											$e_initial_phone
											); */
			
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  // =====================================================================
    
}

