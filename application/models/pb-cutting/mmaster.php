<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $date_from, $date_to) {	  
	//if ($cari == "all") {
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_pb_cutting) like UPPER('%$cari%') ";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl DESC, no_pb_cutting DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl DESC, no_pb_cutting DESC ";
		}
		//echo " select * FROM tm_pb_cutting WHERE TRUE ".$pencarian.""; 
		
		$this->db->select(" * FROM tm_pb_cutting WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();
	//}
	/*else {
			$this->db->select(" * FROM tm_pb_cutting WHERE UPPER(no_pb_cutting) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
	} */
		$data_pb = array();
		$detail_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pb_cutting_detail WHERE id_pb_cutting = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						//if ($row2->quilting == 'f') {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg = '';
							$satuan = '';
						}
						//}
						//else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_quilting' ");
						//}
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$satuan_quilting	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						if ($row2->kode_brg_bisbisan != '') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id 
									AND a.kode_brg = '$row2->kode_brg_bisbisan' ");
							//}
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg_bisbisan	= $hasilrow->nama_brg;
								$satuan_bisbisan	= $hasilrow->nama_satuan;
							}
						}
						else {
							$nama_brg_bisbisan = '';
							$satuan_bisbisan = '';
						}
						
						// ambil nama ukuran bisbisan
						if ($row2->id_ukuran_bisbisan != '') {
							$query3	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan 
											WHERE id = '$row2->id_ukuran_bisbisan' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_ukuran	= $hasilrow->nama;
							}
							else {
								$nama_ukuran	= '';
							}
						}
						else
							$nama_ukuran = '';
				
						$detail_pb[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'gelaran'=> $row2->gelaran,
												'jum_set'=> $row2->jum_set,
												'jum_gelar'=> $row2->jum_gelar,
												'panjang_kain'=> $row2->panjang_kain,
												'jenis_proses'=> $row2->jenis_proses,
												'bordir'=> $row2->bordir,
												'for_kekurangan_cutting'=> $row2->for_kekurangan_cutting,
												'for_bs_cutting'=> $row2->for_bs_cutting,
												'kode_brg_quilting'=> $row2->kode_brg_quilting,
												'nama_brg_quilting'=> $nama_brg_quilting,
												'kode_brg_bisbisan'=> $row2->kode_brg_bisbisan,
												'nama_brg_bisbisan'=> $nama_brg_bisbisan,
												'satuan_bisbisan'=> $satuan_bisbisan,
												'nama_ukuran'=> $nama_ukuran,
												'bagian_bs'=> $row2->bagian_bs
											);
					}
				}
				else {
					$detail_pb = '';
				}
				
				if ($row1->kode_brg_jadi != "000000") {
					$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
							WHERE i_product_motif = '$row1->kode_brg_jadi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
					}
					else {
						$nama_brg_jadi = '';
					}
				}
				else
					$nama_brg_jadi = $row1->brg_jadi_manual;
				
				// 16-07-2012, nama motif brg jadi jika id_motif != 0
				if ($row1->id_motif != '0') {
					$query3	= $this->db->query(" SELECT nama_motif FROM tm_motif_brg_jadi
										WHERE id = '$row1->id_motif' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_motif	= $hasilrow->nama_motif;
					}
					else
						$nama_motif = '';
				}
				else
					$nama_motif = '';
								
				$data_pb[] = array(			'id'=> $row1->id,	
											'no_pb_cutting'=> $row1->no_pb_cutting,
											'tgl'=> $row1->tgl,
											'tgl_update'=> $row1->tgl_update,
											'status_pb'=> $row1->status_pb,
											'status_edit'=> $row1->status_edit,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'id_motif'=> $row1->id_motif,
											'nama_motif'=> $nama_motif,
											'detail_pb'=> $detail_pb
											);
				$detail_pb = array();
			} // endforeach header
		}
		else {
			$data_pb = '';
		}
		return $data_pb;
  }
  
  function getAlltanpalimit($cari, $date_from, $date_to){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_pb_cutting) like UPPER('%$cari%') ";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl DESC, id DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl DESC, id DESC ";
		}
		//echo " select * FROM tm_pb_cutting WHERE TRUE ".$pencarian; 
		
		$query	= $this->db->query(" SELECT * FROM tm_pb_cutting WHERE TRUE ".$pencarian." ");
	/*else {
		$query	= $this->db->query(" SELECT * FROM tm_pb_cutting WHERE UPPER(no_pb_cutting) like UPPER('%$cari%') ");
	} */
    
    return $query->result();  
  }
  
  //
  function save($thn1, $no_request,$tgl_request, $ket, $kode_brg_jadi, $motif_brg_jadi, $is_kode_brg_jadi_manual, $brg_jadi_manual,
			$jenis_proses, $kode_brg_quilting, 
			$kode, $nama, $qty, $ukuran_bisbisan, $gelaran, $jum_set, $jum_gelar, $pjg_kain, $id_marker, 
			$id_kebutuhanperpcs, $kode_brg_bisbisan, $bordir, $kcutting, $bscutting, $bagian_bs, $kebbismanual, $vkebbismanual){  
    $tgl = date("Y-m-d");
    if ($id_marker == '')
		$id_marker = 0;
	if ($bordir == '')
		$bordir = 'f';
	
	if ($kcutting == '')
		$kcutting = 'f';
	
	if ($bscutting == '') {
		$bscutting = 'f';
		$bagian_bs = '';
	}
	if ($is_kode_brg_jadi_manual == '')
		$is_kode_brg_jadi_manual = 'f';
		
	if ($bordir == 't' && $jenis_proses != '2')
		$bordir = 'f';
	
	if ($is_kode_brg_jadi_manual == 't')
		$kode_brg_jadi = "000000";
	
    // cek apa udah ada datanya blm dgn no request tadi    
     $this->db->select("id from tm_pb_cutting WHERE no_pb_cutting = '$no_request' 
				AND extract(year from tgl) = '$thn1' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_pb_cutting
			$data_header = array(
			  'no_pb_cutting'=>$no_request,
			  'tgl'=>$tgl_request,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket,
			  'kode_brg_jadi'=>$kode_brg_jadi,
			  'is_brg_jadi_manual'=>$is_kode_brg_jadi_manual,
			  'brg_jadi_manual'=>$brg_jadi_manual,
			  'id_motif'=>$motif_brg_jadi
			);
			$this->db->insert('tm_pb_cutting',$data_header);
			
			// ambil data terakhir di tabel tm_pb_cutting
			$query2	= $this->db->query(" SELECT id FROM tm_pb_cutting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pb	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				// ############## wrong perception, ini ga dipake
				//cek qty, jika lebih besar dari stok maka otomatis disamakan dgn sisa stoknya
			/*	if ($quilting == 'f')
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				else
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode' ");
				
				$hasilrow = $query3->row();
				$stok = $hasilrow->stok; // ini stok terkini di tm_stok
								
				if ($qty > $stok)
					$qty = $stok;
				if ($stok == '')
					$qty = '0'; */
				
				// 26 mei 2011: qty tadi itu adalah qty rencana hasil cutting (dalam satuan meter)
				
				if ($kebbismanual == '') {
					$kebbismanual = 'f';
				}
				if ($kebbismanual == 'f')
					$vkebbismanual = '0';
					
				// jika semua data tdk kosong, insert ke tm_pb_cutting_detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'gelaran'=>$gelaran,
					'jum_set'=>$jum_set,
					'jum_gelar'=>$jum_gelar,
					'panjang_kain'=>$pjg_kain,
					'jenis_proses'=>$jenis_proses,
					'id_pb_cutting'=>$id_pb,
					'id_marker_gelaran'=>$id_marker,
					'id_ukuran_bisbisan'=>$ukuran_bisbisan,
					'kode_brg_quilting'=>$kode_brg_quilting,
					'id_kebutuhan_perpcs'=>$id_kebutuhanperpcs,
					'kode_brg_bisbisan'=>$kode_brg_bisbisan,
					'bordir'=>$bordir,
					'for_kekurangan_cutting'=>$kcutting,
					'for_bs_cutting'=>$bscutting,
					'bagian_bs'=>$bagian_bs,
					'is_keb_bisbisan_manual'=>$kebbismanual,
					'keb_bisbisan_manual'=>$vkebbismanual
				);
				$this->db->insert('tm_pb_cutting_detail',$data_detail);
				
			}

		}
		else {
			// ambil data terakhir di tabel tm_pb_cutting
			$query2	= $this->db->query(" SELECT id FROM tm_pb_cutting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pb	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				// ############## wrong perception, ini ga dipake
				//cek qty, jika lebih besar dari stok maka otomatis disamakan dgn sisa stoknya
			/*	if ($quilting == 'f')
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				else
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode' ");
				
				$hasilrow = $query3->row();
				$stok = $hasilrow->stok; // ini stok terkini di tm_stok
								
				if ($qty > $stok)
					$qty = $stok;
				if ($stok == '')
					$qty = '0'; */
				
				// 26 mei 2011: qty tadi itu adalah qty rencana hasil cutting (dalam satuan meter)
				if ($kebbismanual == '') {
					$kebbismanual = 'f';
				}
				if ($kebbismanual == 'f')
					$vkebbismanual = '0';
				// jika semua data tdk kosong, insert ke tm_pb_cutting_detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'gelaran'=>$gelaran,
					'jum_set'=>$jum_set,
					'jum_gelar'=>$jum_gelar,
					'panjang_kain'=>$pjg_kain,
					'jenis_proses'=>$jenis_proses,
					'id_pb_cutting'=>$id_pb,
					'id_marker_gelaran'=>$id_marker,
					'id_ukuran_bisbisan'=>$ukuran_bisbisan,
					'kode_brg_quilting'=>$kode_brg_quilting,
					'id_kebutuhan_perpcs'=>$id_kebutuhanperpcs,
					'kode_brg_bisbisan'=>$kode_brg_bisbisan,
					'bordir'=>$bordir,
					'for_kekurangan_cutting'=>$kcutting,
					'for_bs_cutting'=>$bscutting,
					'bagian_bs'=>$bagian_bs,
					'is_keb_bisbisan_manual'=>$kebbismanual,
					'keb_bisbisan_manual'=>$vkebbismanual
				);
				$this->db->insert('tm_pb_cutting_detail',$data_detail);
				
			}
		}
  }
    
  function delete($kode){
	$tgl = date("Y-m-d");	
	
	// 2. hapus data pb-cutting
    $this->db->delete('tm_pb_cutting_detail', array('id_pb_cutting' => $kode));
    $this->db->delete('tm_pb_cutting', array('id' => $kode));
  }
          
  function generate_nomor(){
    // generate no Permintaan Bhn ke gudang
			$th_now	= date("Y");
			$query3	= $this->db->query(" SELECT no_pb_cutting FROM tm_pb_cutting ORDER BY no_pb_cutting DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_pb	= $hasilrow->no_pb_cutting;
			else
				$no_pb = '';
			if(strlen($no_pb)==12) {
				$nopb = substr($no_pb, 3, 9);
				$n_pb	= (substr($nopb,4,5))+1;
				$th_pb	= substr($nopb,0,4);
				if($th_now==$th_pb) {
						$jml_n_pb	= $n_pb;
						switch(strlen($jml_n_pb)) {
							case "1": $kodepb	= "0000".$jml_n_pb;
							break;
							case "2": $kodepb	= "000".$jml_n_pb;
							break;	
							case "3": $kodepb	= "00".$jml_n_pb;
							break;
							case "4": $kodepb	= "0".$jml_n_pb;
							break;
							case "5": $kodepb	= $jml_n_pb;
							break;	
						}
						$nomorpb = $th_now.$kodepb;
				}
				else {
					$nomorpb = $th_now."00001";
				}
			}
			else {
				$nomorpb	= $th_now."00001";
			}
			$nomorpb = "PB-".$nomorpb;

			return $nomorpb;  
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_bhn, $quilting, $kode_brg_jadi, $jenis_proses, $kode_brg_quilting, $uk_bisbisan, $motif)
  {
	if ($cari == "all") {
		if ($quilting == 'f') {		
			$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
			
			$sql.=" order by a.kode_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			
			$sql.=" order by a.kode_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($quilting == 'f') {		
			$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')				
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.kode_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.kode_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}
						
			// ambil data marker gelaran/set
			if ($jenis_proses != 4) {
				$sql = " SELECT id, gelaran, jum_set FROM tm_marker_gelaran 
				WHERE status_aktif = 't' AND kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '$row1->kode_brg'
				AND id_motif = '$motif' ";
				if ($jenis_proses == 2)
					$sql.= " AND diprint = 't' ";
				else if ($jenis_proses == 3) {
					$sql.= " AND diquilting = 't' AND kode_brg_quilting = '$kode_brg_quilting' ";
				}
				else if ($jenis_proses == 1)
					$sql.= " AND diquilting = 'f' AND diprint = 'f' ";
				else if ($jenis_proses == 5)
					$sql.= " AND diquilting = 't' AND diprint = 't' AND kode_brg_quilting = '$kode_brg_quilting' ";
					
				$query3	= $this->db->query($sql);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_marker	= $hasilrow->id;
					$gelaran	= $hasilrow->gelaran;
					$jum_set	= $hasilrow->jum_set;
				}
				else {
					$id_marker = '';
					$gelaran	= '';
					$jum_set	= '';
				}
			}
			else {
				$id_marker = '';
				$gelaran	= '';
				$jum_set	= '';
			}	
			
			// jika jenis prosesnya bisbisan
			if ($jenis_proses == 4) {
				// ambil data kebutuhan perpcs bisbisan
				$sql = " SELECT id, jum_kebutuhan FROM tm_keb_bisbisan_perpcs 
					WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '$row1->kode_brg' 
					AND id_ukuran_bisbisan = '$uk_bisbisan' "; //echo $sql; die();
				$query3	= $this->db->query($sql);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_kebutuhan	= $hasilrow->id;
					$jum_kebutuhan	= $hasilrow->jum_kebutuhan;
				}
				else {
					$id_kebutuhan = 0;
					$jum_kebutuhan	= 0;
				}
				
				// ambil variabel2 rumus
				$sql = " SELECT var1, var2, var3 FROM tm_ukuran_bisbisan 
					WHERE id = '$uk_bisbisan' ";
				$query3	= $this->db->query($sql);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$var1	= $hasilrow->var1;
					$var2	= $hasilrow->var2;
					$var3	= $hasilrow->var3;
				}
				else {
					$var1 = 0;
					$var2 = 0;
					$var3 = 0;
				}
			}
			else {
				$id_kebutuhan = 0;
				$jum_kebutuhan	= 0;
				$var1 = 0;
				$var2 = 0;
				$var3 = 0;
			}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok,
										'gelaran'=> $gelaran,
										'jum_set'=> $jum_set,
										'id_marker'=> $id_marker,
										'id_kebutuhan'=> $id_kebutuhan,
										'jum_kebutuhan'=> $jum_kebutuhan,
										'var1'=> $var1,
										'var2'=> $var2,
										'var3'=> $var3
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_bhn, $quilting, $kode_brg_jadi){
	if ($cari == "all") {
		if ($quilting == 'f') {
			$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')				
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($quilting == 'f') {		
			$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
				FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
							WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
							AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
			if ($kel_brg != '')				
				$sql.= " AND e.kode = '$kel_brg' ";
			if ($id_jenis_bhn != '0')
				$sql.= " AND c.id = '$id_jenis_bhn' ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  // 12 des 2011
  function get_bahanbisbisan($num, $offset, $cari) {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" order by a.kode_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.kode_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok_hasil_bisbisan 
								WHERE kode_brg_bisbisan = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahanbisbisantanpalimit($cari) {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" order by a.kode_brg ";
		
		$this->db->select($sql, false);
	}
	else {
		$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_bisbisan a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.kode_brg ";
		
		$this->db->select($sql, false);	
    }
    $query = $this->db->get();
	return $query->result();  
  }
  
  function get_pb_cutting($id_pb) {
		$query	= $this->db->query(" SELECT * FROM tm_pb_cutting where id = '$id_pb' ");
	
		$data_pb = array();
		$detail_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detail pbnya
				$query2	= $this->db->query(" SELECT * FROM tm_pb_cutting_detail WHERE id_pb_cutting = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->jenis_proses == '3' || $row2->jenis_proses == '5') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_quilting' ");
								
							$hasilrow = $query3->row();
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$satuan_quilting	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
						// jika jenis prosesnya bisbisan
						if ($row2->jenis_proses == 4) {
							// 13 des 2011
							if ($row2->kode_brg_bisbisan != '') {
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
											FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id 
											AND a.kode_brg = '$row2->kode_brg_bisbisan' ");
									
								$hasilrow = $query3->row();
								$nama_brg_bisbisan	= $hasilrow->nama_brg;
								$satuan_bisbisan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg_bisbisan = '';
								$satuan_bisbisan = '';
							}
							
							// ambil data kebutuhan perpcs bisbisan
							$sql = " SELECT id, jum_kebutuhan FROM tm_keb_bisbisan_perpcs 
								WHERE kode_brg_jadi = '$row1->kode_brg_jadi' AND kode_brg = '$row2->kode_brg' 
								AND id_ukuran_bisbisan = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_kebutuhan	= $hasilrow->id;
								$jum_kebutuhan	= $hasilrow->jum_kebutuhan;
							}
							else {
								$id_kebutuhan = 0;
								$jum_kebutuhan	= 0;
							}
							
							// ambil variabel2 rumus
							$sql = " SELECT var1, var2, var3 FROM tm_ukuran_bisbisan 
								WHERE id = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$var1	= $hasilrow->var1;
								$var2	= $hasilrow->var2;
								$var3	= $hasilrow->var3;
							}
							else {
								$var1 = 0;
								$var2 = 0;
								$var3 = 0;
							}
						}
						else {
							$id_kebutuhan = 0;
							$jum_kebutuhan	= 0;
							$var1 = 0;
							$var2 = 0;
							$var3 = 0;
							$nama_brg_bisbisan = '';
							$satuan_bisbisan = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
										
						$detail_pb[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'gelaran'=> $row2->gelaran,
												'jum_set'=> $row2->jum_set,
												'jum_gelar'=> $row2->jum_gelar,
												'panjang_kain'=> $row2->panjang_kain,
												'jenis_proses'=> $row2->jenis_proses,
												'id_marker'=> $row2->id_marker_gelaran,
												'kode_brg_quilting'=> $row2->kode_brg_quilting,
												'nama_brg_quilting'=> $nama_brg_quilting,
												'kode_brg_bisbisan'=> $row2->kode_brg_bisbisan,
												'nama_brg_bisbisan'=> $nama_brg_bisbisan,
												'ukuran_bisbisan'=> $row2->id_ukuran_bisbisan,
												//'id_kebutuhanperpcs'=> $row2->id_kebutuhan_perpcs,
												'id_kebutuhan'=> $id_kebutuhan,
												'jum_kebutuhan'=> $jum_kebutuhan,
												'var1'=> $var1,
												'var2'=> $var2,
												'var3'=> $var3,
												'bordir'=> $row2->bordir,
												'for_kekurangan_cutting'=> $row2->for_kekurangan_cutting,
												'for_bs_cutting'=> $row2->for_bs_cutting,
												'bagian_bs'=> $row2->bagian_bs,
												'keb_bisbisan_manual'=> $row2->keb_bisbisan_manual,
												'is_keb_bisbisan_manual'=> $row2->is_keb_bisbisan_manual
											);
					}
				}
				else {
					$detail_pb = '';
				}
				
				$pisah1 = explode("-", $row1->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->kode_brg_jadi != '') {
					$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$row1->kode_brg_jadi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
					}
					else {
						$nama_brg_jadi = '';
					}
				}
				else {
					$nama_brg_jadi = '';
				}
												
				$data_pb[] = array(			'id'=> $row1->id,	
											'no_pb_cutting'=> $row1->no_pb_cutting,
											'tgl'=> $tgl,
											'ket'=> $row1->keterangan,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'id_motif'=> $row1->id_motif,
											'is_brg_jadi_manual'=> $row1->is_brg_jadi_manual,
											'brg_jadi_manual'=> $row1->brg_jadi_manual,
											'detail_pb'=> $detail_pb
											);
				$detail_pb = array();
			} // endforeach header
		}
		else {
			$data_pb = '';
		}
		return $data_pb;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  // new 4 april 2011
  function create_bonm($no_request, $kode, $nama, $qty){
    $tgl = date("Y-m-d");
    $th_now	= date("Y");
	
		// ====================================================
			// insert Bon M masuk secara otomatis di tm_apply_stok_proses_quilting
			
				// ambil data gudang sesuai barangnya
				$query3	= $this->db->query(" SELECT id_gudang FROM tm_barang WHERE kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$id_gudang = $hasilrow->id_gudang;
				
				// cek apakah header bonm udh ada
				$query3	= $this->db->query(" SELECT id, no_bonm FROM tm_apply_stok_proses_quilting 
				WHERE no_pb_quilting = '$no_request' AND id_gudang = '$id_gudang' ");
				if ($query3->num_rows() > 0) { // jika udh ada
					$hasilrow = $query3->row();
					$no_bonm = $hasilrow->no_bonm;
					$id_apply_stok = $hasilrow->id;
					
					//save ke tabel tm_apply_stok_proses_quilting_detail
					if ($kode!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_proses_quilting_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'id_apply_stok'=>$id_apply_stok
						);
						$this->db->insert('tm_apply_stok_proses_quilting_detail',$data_detail);
						
					} // end if kode != '' dan $qty != ''
				}
				else {
					// generate no Bon M
					$query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_proses_quilting WHERE id_gudang = '$id_gudang' 
					ORDER BY no_bonm DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() != 0) 
						$no_bonm	= $hasilrow->no_bonm;
					else
						$no_bonm = '';
					if(strlen($no_bonm)==13) {
						$nobonm = substr($no_bonm, 4, 9);
						$n_bonm	= (substr($nobonm,4,5))+1;
						$th_bonm	= substr($nobonm,0,4);
						if($th_now==$th_bonm) {
								$jml_n_bonm	= $n_bonm; //
								switch(strlen($jml_n_bonm)) {
									case "1": $kodebonm	= "0000".$jml_n_bonm;
									break;
									case "2": $kodebonm	= "000".$jml_n_bonm;
									break;	
									case "3": $kodebonm	= "00".$jml_n_bonm;
									break;
									case "4": $kodebonm	= "0".$jml_n_bonm;
									break;
									case "5": $kodebonm	= $jml_n_bonm;
									break;	
								}
								$nomorbonm = $th_now.$kodebonm;
						}
						else {
							$nomorbonm = $th_now."00001";
						}
					}
					else {
						$nomorbonm	= $th_now."00001";
					}
					$no_bonm = "BKM-".$nomorbonm;
					
					// insert di tm_apply_stok_proses_quilting
					$data_header2 = array(
					  'no_bonm'=>$no_bonm,
					  'tgl_bonm'=>$tgl,
					  'no_pb_quilting'=>$no_request,
					  'id_gudang'=>$id_gudang,
					  'tgl_input'=>$tgl,
					  'tgl_update'=>$tgl
					);
					$this->db->insert('tm_apply_stok_proses_quilting',$data_header2);
					
					// ambil data terakhir di tabel tm_apply_stok_proses_quilting
					$query2	= $this->db->query(" SELECT id FROM tm_apply_stok_proses_quilting ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_apply_stok	= $hasilrow->id;
					
					//save ke tabel tm_apply_stok_proses_quilting_detail
					if ($kode!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_proses_quilting_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'id_apply_stok'=>$id_apply_stok
						);
						$this->db->insert('tm_apply_stok_proses_quilting_detail',$data_detail);
												
					} // end if kode != '' dan $qty != ''
				} // end else

			// ====================================================
  }
  
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_ukuran_bisbisan(){
    $this->db->select("* from tm_ukuran_bisbisan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 09-04-2012
  function get_pb_cutting_for_print($tgl_request) { // to_date('$date_from','dd-mm-yyyy') 
		$query	= $this->db->query(" SELECT a.no_pb_cutting, a.tgl, a.kode_brg_jadi, a.brg_jadi_manual, a.id_motif,
					b.* FROM tm_pb_cutting a, tm_pb_cutting_detail b
					WHERE a.id = b.id_pb_cutting AND a.tgl = to_date('$tgl_request','dd-mm-yyyy')  
					ORDER BY a.id ASC, b.id ASC ");
	
		$data_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row2) {
				
				// 21-07-2012, nama motif brg jadi jika id_motif != 0
				if ($row2->id_motif != '0') {
					$query3	= $this->db->query(" SELECT nama_motif FROM tm_motif_brg_jadi
										WHERE id = '$row2->id_motif' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_motif	= $hasilrow->nama_motif;
					}
					else
						$nama_motif = '';
				}
				else
					$nama_motif = '';

						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->jenis_proses == '3') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_quilting' ");
								
							$hasilrow = $query3->row();
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$satuan_quilting	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
						// jika jenis prosesnya bisbisan
						if ($row2->jenis_proses == 4) {
							// 13 des 2011
							if ($row2->kode_brg_bisbisan != '') {
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
											FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id 
											AND a.kode_brg = '$row2->kode_brg_bisbisan' ");
									
								$hasilrow = $query3->row();
								$nama_brg_bisbisan	= $hasilrow->nama_brg;
								$satuan_bisbisan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg_bisbisan = '';
								$satuan_bisbisan = '';
							}
							
							// ambil data kebutuhan perpcs bisbisan
							$sql = " SELECT id, jum_kebutuhan FROM tm_keb_bisbisan_perpcs 
								WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_brg = '$row2->kode_brg' 
								AND id_ukuran_bisbisan = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_kebutuhan	= $hasilrow->id;
								$jum_kebutuhan	= $hasilrow->jum_kebutuhan;
							}
							else {
								$id_kebutuhan = 0;
								$jum_kebutuhan	= 0;
							}
							
							// ambil variabel2 rumus
							$sql = " SELECT var1, var2, var3 FROM tm_ukuran_bisbisan 
								WHERE id = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$var1	= $hasilrow->var1;
								$var2	= $hasilrow->var2;
								$var3	= $hasilrow->var3;
							}
							else {
								$var1 = 0;
								$var2 = 0;
								$var3 = 0;
							}
						}
						else {
							$id_kebutuhan = 0;
							$jum_kebutuhan	= 0;
							$var1 = 0;
							$var2 = 0;
							$var3 = 0;
							$nama_brg_bisbisan = '';
							$satuan_bisbisan = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
														
				$pisah1 = explode("-", $row2->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row2->kode_brg_jadi != '') {
					$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$row2->kode_brg_jadi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
					}
					else {
						if ($row2->brg_jadi_manual == '')
							$nama_brg_jadi = '';
						else
							$nama_brg_jadi = $row2->brg_jadi_manual;
					}
				}
				else
					$nama_brg_jadi = '';
												
				$data_pb[] = array(			'no_pb_cutting'=> $row2->no_pb_cutting,
											'tgl'=> $tgl,
											'kode_brg_jadi'=> $row2->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											
											'id_detail'=> $row2->id,
											'id_pb_cutting'=> $row2->id_pb_cutting,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'gelaran'=> $row2->gelaran,
												'jum_set'=> $row2->jum_set,
												'jum_gelar'=> $row2->jum_gelar,
												'panjang_kain'=> $row2->panjang_kain,
												'jenis_proses'=> $row2->jenis_proses,
												'id_marker'=> $row2->id_marker_gelaran,
												'kode_brg_quilting'=> $row2->kode_brg_quilting,
												'nama_brg_quilting'=> $nama_brg_quilting,
												'kode_brg_bisbisan'=> $row2->kode_brg_bisbisan,
												'nama_brg_bisbisan'=> $nama_brg_bisbisan,
												'ukuran_bisbisan'=> $row2->id_ukuran_bisbisan,
												//'id_kebutuhanperpcs'=> $row2->id_kebutuhan_perpcs,
												'id_kebutuhan'=> $id_kebutuhan,
												'jum_kebutuhan'=> $jum_kebutuhan,
												'var1'=> $var1,
												'var2'=> $var2,
												'var3'=> $var3,
												'bordir'=> $row2->bordir,
												'for_kekurangan_cutting'=> $row2->for_kekurangan_cutting,
												'for_bs_cutting'=> $row2->for_bs_cutting,
												'bagian_bs'=> $row2->bagian_bs,
												'id_motif'=> $row2->id_motif,
												'nama_motif'=> $nama_motif 
											);
											
			} // endforeach header
		}
		else {
			$data_pb = '';
		}
		return $data_pb;
  }
  
  function cek_data($no_request, $thn1){
    $this->db->select("id from tm_pb_cutting WHERE no_pb_cutting = '$no_request' 
				AND extract(year from tgl) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 16-07-2012
  function get_motif_brg_jadi(){
    $this->db->select("* from tm_motif_brg_jadi order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
