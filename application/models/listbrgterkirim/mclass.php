<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function namacabang($ibranch){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_branch WHERE i_branch_code='$ibranch' ");
	}
	
	function clistbrgpendding($fdate,$d_op_first,$d_op_last,$stp,$iorder,$ordermotif,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		/* Disabled 10112011
		$query	= $db2->query(" SELECT  a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount,
					a.d_op
				
				FROM tm_op_item b 
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
				WHERE ".$fdate." a.f_op_cancel='f' AND b.f_do_created='t' AND d.f_stop_produksi='$stp' ".$iorder." ".$ordermotif." 
				
				ORDER BY a.i_op_code DESC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset);
		*/

		$query	= $db2->query(" SELECT a.i_op, a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount
				
				FROM tm_op_item b
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
				WHERE ".$fdate." a.f_op_cancel='f' AND (b.f_do_created='t' 
OR (b.f_do_created='f' AND (b.n_count-b.n_residual) > 0) AND b.n_residual > 0) AND d.f_stop_produksi='$stp' ".$iorder." ".$ordermotif."
				
				ORDER BY a.i_op_code DESC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset);
				
		if($query->num_rows()>0){
			return $result	= $query->result();
		}
	}


	function clistbrgpendding2($fdate,$d_op_first,$d_op_last,$stp,$iorder,$idonya,$ordermotif,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_op, a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount
				
				FROM tm_op_item b
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do_item f ON f.i_op=a.i_op
				INNER JOIN tm_do e ON e.i_do=f.i_do
				
				WHERE ".$fdate." a.f_op_cancel='f' AND (b.f_do_created='t' 
OR (b.f_do_created='f' AND (b.n_count-b.n_residual) > 0) AND b.n_residual > 0) AND d.f_stop_produksi='$stp' ".$iorder." ".$ordermotif." ".$idonya."
				
				GROUP BY a.i_op, a.d_op, a.i_op_code, a.i_branch, b.i_product, c.e_product_motifname, b.n_count, b.n_residual, d.v_unitprice
				
				ORDER BY a.i_op_code DESC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset);
				
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
		
	
	function detaildo($iop,$iproduct) {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT b.i_do_code, b.d_do, a.n_deliver FROM tm_do_item a 
		
		INNER JOIN tm_do b ON b.i_do=a.i_do WHERE a.i_op='$iop' AND a.i_product='$iproduct' AND b.f_do_cancel='f' ORDER BY b.i_do_code ASC ");
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
		
	}
	
}
?>
