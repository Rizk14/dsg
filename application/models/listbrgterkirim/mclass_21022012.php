<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function namacabang($ibranch){
		return $this->db->query(" SELECT * FROM tr_branch WHERE i_branch_code='$ibranch' ");
	}
	
	function clistbrgpendding($fdate,$d_op_first,$d_op_last,$stp,$iorder,$ordermotif,$limit,$offset) {
		
		/* Disabled 10112011
		
		$query	= $this->db->query(" SELECT  a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount,
					a.d_op
				
				FROM tm_op_item b 
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
				WHERE ".$fdate." a.f_op_cancel='f' AND b.f_do_created='t' AND d.f_stop_produksi='$stp' ".$iorder." ".$ordermotif." 
				
				ORDER BY a.i_op_code DESC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset);
		
		*/

		$query	= $this->db->query(" SELECT a.i_op, a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount
				
				FROM tm_op_item b
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
				WHERE ".$fdate." a.f_op_cancel='f' AND a.f_do_created='t' AND d.f_stop_produksi='$stp' ".$iorder." ".$ordermotif."
				
				ORDER BY a.i_op_code DESC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset);
				
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
	
	
	function detaildo($iop,$iproduct) {
		
		$query = $this->db->query(" SELECT b.i_do_code, b.d_do, a.n_deliver FROM tm_do_item a 
		
		INNER JOIN tm_do b ON b.i_do=a.i_do WHERE a.i_op='$iop' AND a.i_product='$iproduct' AND b.f_do_cancel='f' ORDER BY b.i_do_code ASC ");
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
		
	}
	
}
?>
