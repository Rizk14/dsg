<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_printer_name ";
		$query	= $db2->query(" SELECT * FROM tr_printer ".$order." ", false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function LevelUser() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY i_level ";
		$query	= $db2->query(" SELECT * FROM tm_level_user ".$order." ", false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	
	}
	
	function CekPrinter($destination_ip) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_printer WHERE ip='$destination_ip' ORDER BY i_printer DESC ");
	}
	
	function msimpan($eprintername,$euri,$ip,$ilevel,$iuserid) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	

		$seq_tm_printer	= $db2->query(" SELECT cast(i_printer AS integer) AS iprinter FROM tr_printer ORDER BY cast(i_printer AS integer) DESC LIMIT 1 ");
		if($seq_tm_printer->num_rows() > 0 ) {
			$seqrow	= $seq_tm_printer->row();
			$i_printer	= $seqrow->iprinter+1;		
		} else {
			$i_printer	= 1;		
		}
							
		$tr_printer	= array(
			'i_printer'=>$i_printer,
			'e_printer_name'=>$eprintername,
			'e_uri'=>$euri,
			'ip'=>$ip,
			'i_user_id'=>$iuserid,
			'd_entry'=>$dentry
			);

		$db2->insert('tr_printer',$tr_printer);
		
		redirect('printer/cform/');
    }

  function getlepel($lepel){
	  $db2=$this->load->database('db_external', TRUE);
    return $db2->query(" SELECT * FROM tm_user WHERE i_level='$lepel' ORDER BY e_user_name ASC ");
  }	
  
  function get($iprinter) {
	  $db2=$this->load->database('db_external', TRUE);
    //$query = $db2->getwhere('tr_printer',array('i_printer'=>$iprinter));
    $query = $db2->query(" SELECT * FROM tr_printer WHERE i_printer='$iprinter' ");
    return $query->row_array();		
  }
  
  function getid($i_user_id,$e_user_name,$e_password_old_enkri) {
	  $db2=$this->load->database('db_external', TRUE);
  	return $db2->query(" SELECT * FROM tm_user WHERE i_user_id='$i_user_id' AND e_user_name='$e_user_name' AND e_password='$e_password_old_enkri' AND i_user_active='1' " );
  }
  
  function update($eprintername,$euri,$ip,$iprinter) {
	  $db2=$this->load->database('db_external', TRUE);
  	$printer	= array(
		'e_printer_name'=>$eprintername,
		'e_uri'=>$euri,
		'ip'=>$ip
	);
	$db2->update('tr_printer',$printer,array('i_printer'=>$iprinter));
	redirect('printer/cform');
  }
  
  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
  	$db2->delete('tr_printer',array('i_printer'=>$id));
	redirect('printer/cform');
  }
}
?>
