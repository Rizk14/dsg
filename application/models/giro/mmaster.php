<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($igiro,$iarea)
    {
		$this->db->select(" * from tm_giro a
							inner join tm_area on(a.i_area=tm_area.kode_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where a.i_giro='$igiro' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
  	function bacadt($cari,$area,$num,$offset)
    {
		  $this->db->select("* from tm_dt where i_area = '$area' and i_dt like '%$cari%' order by i_dt", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function insert($igiro,$iarea,$icustomer,$irv,$dgiro,$drv,$dgiroduedate,$egirodescription,$egirobank,
				    $vjumlah,$vsisa,$idt,$dgiroterima)
    {
		
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
    		'i_giro'   			    => $igiro,
				'i_area' 			      => $iarea,
				'i_customer' 		    => $icustomer,
				'i_rv' 				      => $irv,
				'd_giro' 			      => $dgiro,
				'd_rv' 				      => $drv,
				'd_giro_duedate' 	  => $dgiroduedate,
				'd_entry' 			    => $dentry,
				'e_giro_description'=> $egirodescription,
				'e_giro_bank' 	    => $egirobank,
				'v_jumlah' 			    => $vjumlah,
				'v_sisa' 			      => $vsisa,
				'i_dt'              => $idt,
				'd_giro_terima'     => $dgiroterima
    		)
    	);
    	$this->db->insert('tm_giro');
    }
    function update($igiro,$iarea,$icustomer,$irv,$dgiro,$dsetor,$dgiroduedate,$dgirocair,$egirodescription,$egirobank,
				    $fgirotolak,$fgirocair,$vjumlah,$vsisa,$dgirotolak,$fgirobatal,$idt,$dgiroterima)
    {
		
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
    	$this->db->set(
    		array(
							'i_giro'	 					=> $igiro,
							'i_area' 						=> $iarea,
							'i_customer' 				=> $icustomer,
							'i_rv' 							=> $irv,
							'd_giro' 						=> $dgiro,
							'd_setor' 					=> $dsetor,
							'd_giro_duedate' 		=> $dgiroduedate,
							'd_giro_cair' 			=> $dgirocair,
							'd_giro_tolak' 			=> $dgirotolak,
							'd_update' 					=> $dupdate,
							'e_giro_description'=> $egirodescription,
							'e_giro_bank' 			=> $egirobank,
							'f_giro_tolak' 			=> $fgirotolak,
							'f_giro_cair' 			=> $fgirocair,
							'f_giro_batal' 			=> $fgirobatal,
							'v_jumlah' 					=> $vjumlah,
							'v_sisa' 						=> $vsisa,
							'i_dt'              => $idt,
      				'd_giro_terima'     => $dgiroterima
    		)
    	);
    	$this->db->where('i_giro',$igiro);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_giro');
    }	
	public function updatepelunasan($igiro,$iarea,$fgirotolak,$vjumlah,$vsisa,$fgirotolak,$fgirobatal)
	{
		$vjumlahtmp=$vjumlah;
		$this->db->select(" v_jumlah, i_dt, i_pelunasan from tm_pelunasan WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01'");
		$query = $this->db->get();
		foreach($query->result() as $row){
			$nilaipelunasan = $row->v_jumlah;
			$idt			= $row->i_dt;
			$ipelunasan		= $row->i_pelunasan;
			if($nilaipelunasan<=$vjumlah){
				if($fgirobatal=='t'){
					$this->db->query("update tm_pelunasan set f_giro_batal='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->query("update tm_pelunasan_lebih set f_giro_batal='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->select(" i_nota, v_jumlah from tm_pelunasan_item WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan'");
					$queri = $this->db->get();
					foreach($queri->result() as $raw){
						$inota=$raw->i_nota;
						$jum	=$raw->v_jumlah;
						$this->db->query("update tm_nota set v_sisa=$jum WHERE i_nota='$inota' and i_area='$iarea'");
#						$this->db->query("update tm_pelunasan_item set v_sisa=$jum WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#						$this->db->query("update tm_pelunasan_lebihitem set v_sisa=$jum WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
					}
				}elseif($fgirotolak=='t'){
					$this->db->query("update tm_pelunasan set f_giro_tolak='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->query("update tm_pelunasan_lebih set f_giro_tolak='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->select(" i_nota, v_jumlah from tm_pelunasan_item WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan'");
					$queri = $this->db->get();
					foreach($queri->result() as $raw){
						$inota=$raw->i_nota;
						$jum	=$raw->v_jumlah;
						$this->db->query("update tm_nota set v_sisa=v_sisa+$jum WHERE i_nota='$inota' and i_area='$iarea'");
#						$this->db->query("update tm_pelunasan_item set v_sisa=$jum WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#						$this->db->query("update tm_pelunasan_lebihitem set v_sisa=$jum WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
					}
				}else{
#					$this->db->query("update tm_pelunasan set v_lebih=$vjumlah-$nilaipelunasan, f_pelunasan_cancel='f', f_giro_tolak='f' 
#														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
#					$this->db->query("update tm_pelunasan_lebih set v_lebih=$vjumlah-$nilaipelunasan, f_pelunasan_cancel='f', f_giro_tolak='f' 
#														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->select(" i_nota from tm_pelunasan_item WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan'");
					$queri = $this->db->get();
					foreach($queri->result() as $raw){
						$inota=$raw->i_nota;
						$this->db->query("update tm_nota set v_sisa=0 WHERE i_nota='$inota' and i_area='$iarea'");
#						$this->db->query("update tm_pelunasan_item set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#						$this->db->query("update tm_pelunasan_lebihitem set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
					}
				}
			}else{
				if($fgirobatal=='t'){
					$this->db->query("update tm_pelunasan set f_giro_batal='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->query("update tm_pelunasan_lebih set f_giro_batal='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->select(" i_nota, v_jumlah from tm_pelunasan_item WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' order by v_jumlah");
					$queri = $this->db->get();
					foreach($queri->result() as $raw){
						$inota=$raw->i_nota;
						$jml  =$raw->v_jumlah;
						$vjumlahtmp=$vjumlahtmp-$jml;
						if($vjumlahtmp>=0){
							$this->db->query("update tm_nota set v_sisa=0 WHERE i_nota='$inota' and i_area='$iarea'");
#							$this->db->query("update tm_pelunasan_item set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#							$this->db->query("update tm_pelunasan_lebihitem set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
						}else{
							$min=$vjumlahtmp*(-1);
							$this->db->query("update tm_nota set v_sisa=$min WHERE i_nota='$inota' and i_area='$iarea'");
#							$this->db->query("update tm_pelunasan_item set v_sisa=$min WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#							$this->db->query("update tm_pelunasan_lebihitem set v_sisa=$min WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
						}
					}
				}elseif($fgirotolak=='t'){
					$this->db->query("update tm_pelunasan set f_giro_tolak='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->query("update tm_pelunasan_lebih set f_giro_tolak='t' 
														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->select(" i_nota, v_jumlah from tm_pelunasan_item WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' order by v_jumlah");
					$queri = $this->db->get();
					foreach($queri->result() as $raw){
						$inota=$raw->i_nota;
						$jml  =$raw->v_jumlah;
						$vjumlahtmp=$vjumlahtmp-$jml;
						if($vjumlahtmp>=0){
							$this->db->query("update tm_nota set v_sisa=0 WHERE i_nota='$inota' and i_area='$iarea'");
#							$this->db->query("update tm_pelunasan_item set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#							$this->db->query("update tm_pelunasan_lebihitem set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
						}else{
							$min=$vjumlahtmp*(-1);
							$this->db->query("update tm_nota set v_sisa=$min WHERE i_nota='$inota' and i_area='$iarea'");
#							$this->db->query("update tm_pelunasan_item set v_sisa=$min WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#							$this->db->query("update tm_pelunasan_lebihitem set v_sisa=$min WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
						}
					}
				}else{
#					$this->db->query("update tm_pelunasan set v_lebih=0, f_pelunasan_cancel='f', f_giro_tolak='f' 
#														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
#					$this->db->query("update tm_pelunasan_lebih set v_lebih=0, f_pelunasan_cancel='f', f_giro_tolak='f' 
#														WHERE i_giro='$igiro' and i_area='$iarea' and i_jenis_bayar='01' and i_pelunasan='$ipelunasan'");
					$this->db->select(" i_nota, v_jumlah from tm_pelunasan_item WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan'");
					$queri = $this->db->get();
					foreach($queri->result() as $raw){
						$inota=$raw->i_nota;
						$jml  =$raw->v_jumlah;
						$vjumlahtmp=$vjumlahtmp-$jml;
						if($vjumlahtmp>=0){
							$this->db->query("update tm_nota set v_sisa=0 WHERE i_nota='$inota' and i_area='$iarea'");
#							$this->db->query("update tm_pelunasan_item set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#							$this->db->query("update tm_pelunasan_lebihitem set v_sisa=0 WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
						}else{
							$min=$vjumlahtmp*(-1);
							$this->db->query("update tm_nota set v_sisa=$min WHERE i_nota='$inota' and i_area='$iarea'");
#							$this->db->query("update tm_pelunasan_item set v_sisa=$min WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
#							$this->db->query("update tm_pelunasan_lebihitem set v_sisa=$min WHERE i_dt='$idt' and i_area='$iarea' and i_pelunasan='$ipelunasan' and i_nota='$inota'");
						}
					}
				}				
			}
		}
	}
    public function delete($igiro,$iarea) 
    {
		$this->db->query("update tm_giro set f_giro_batal='t' WHERE i_giro='$igiro' and i_area='$iarea'");
    }
    function bacasemua()
    {
		//~ $this->db->select(' * from tm_giro a 
							//~ inner join tm_area on(a.i_area=tm_area.i_area)
							//~ inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							//~ order by a.i_area,a.i_giro', false);
		$this->db->select(' * from tm_giro a 
							inner join tm_area on(a.i_area=tm_area.kode_area)
							order by a.i_area,a.i_giro', false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$hasil= $query->result();
			foreach($hasil as $row){
				$db2=$this->load->database('db_external',TRUE);
				$db2->select(" * from tr_customer where cast(i_customer as integer)=".$row->i_customer."" , false);
					$query2 = $db2->get();
				}
				return $query2->result();
		}
    }
   function bacaarea($num,$offset) {
      //~ $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
       $this->db->select("* from tm_area order by kode_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    function bacacustomer($iarea,$num,$offset)
    {	$db2=$this->load->database('db_external',TRUE);
		$query = $db2->select("* from tr_customer",false)->limit($num,$offset);
		//~ $this->db->select(" * from tr_customer a
							//~ left join tr_customer_area d on
							//~ (a.i_customer=d.i_customer) 
							//~ where a.i_area='$iarea'
							//~ order by a.i_customer",false)->limit($num,$offset);
		$query = $db2->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							where a.i_area='$iarea' and
							(upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function runningnumberrv($rvth)
	{
		$this->db->select(" trim(to_char(count(i_rv)+1,'000000')) as no from tm_giro where to_char(d_rv,'yyyy')='$rvth'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
			  $rv=$row->no;
			}
			return $rv;
		}else{
			$rv='000001';
			return $rv;
		}
	}
	function bacadetailpl($iarea,$ipl,$idt){
		$this->db->select(" a.*, b.v_nota_netto as v_nota from tm_pelunasan_item a
				    inner join tm_nota b on (a.i_nota=b.i_nota and a.i_area=b.i_area)
				    
					where a.i_pelunasan = '$ipl' 
					and a.i_area='$iarea'
					and a.i_dt='$idt'
					order by a.i_pelunasan,a.i_area ",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}	
}
?>
