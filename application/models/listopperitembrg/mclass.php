<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function hargaperpelanggan($imotif,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}

	function lcustomer() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_customer ORDER BY e_customer_name ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
		
	function lbarangjadiperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC " );
	}	
	
	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		$key_upper	= strtoupper($key);
		
		if(!empty($key)) {
			return $db2->query(" SELECT 	a.i_product_base AS iproduct,
						b.i_product_motif AS imotif,	
						b.e_product_motifname AS motifname,
						b.n_quantity AS qty
						
					FROM tr_product_base a 
					
					RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product 
					
					WHERE a.i_product_base LIKE '$key_upper%' OR b.i_product_motif LIKE '$key_upper%' 
					
					ORDER BY b.i_product_motif DESC " );
		}
	}	
	
	// 10-09-2012
	function get_op_peritem($i_product,$d_op_first,$d_op_last) {
		//=============================
		$db2=$this->load->database('db_external', TRUE);
		$sql=" select distinct b.i_product, b.e_product_name, a.i_branch
				FROM tm_op a, tm_op_item b where a.i_op = b.i_op ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND a.d_op >='$d_op_first' AND a.d_op <='$d_op_last' ";
		if ($i_product != '')
			$sql.= " AND b.i_product='$i_product' ";
		$sql.= " AND a.f_op_cancel='f' ORDER BY b.i_product, a.i_branch"; //echo $sql; die();
		//=============================
		$query	= $db2->query($sql);
				
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}		
	}
	
	function explistopvsdo_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi) {
		$db2=$this->load->database('db_external', TRUE);
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		}
		
		/* 26072011	
		return $db2->query("
				SELECT c.f_stop_produksi AS stopproduct, 
				d.i_product_motif AS imotif, 
				d.e_product_motifname AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
				INNER JOIN tm_do f ON f.i_do=e.i_do 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." 
				
				GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice		
		");
		*/ 
		return $db2->query(" SELECT c.f_stop_produksi AS stopproduct, 
				d.i_product_motif AS imotif, 
				d.e_product_motifname AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tm_do_item e ON e.i_op=b.i_op 
				INNER JOIN tm_do f ON f.i_do=e.i_do 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." 
				
				GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice		
		");
		
		
	}
	
	function explistopvsdorinci_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif) {
		$db2=$this->load->database('db_external', TRUE);
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		}
		
		/* 26072011	
		return $db2->query("
			SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			*/ 

		return $db2->query(" SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON e.i_op=b.i_op 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
						
	}		
	/* End 0f 18052011 */		
	
	function jmlorder($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir) {
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		$db2=$this->load->database('db_external', TRUE);
		$sql = " SELECT SUM(a.n_count) AS jmlorder, 
			sum(c.n_deliver) as pemenuhan, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND b.d_op >='$d_op_first' AND b.d_op <='$d_op_last' ";		
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
			
		// 04-08-2012, khusus tirai
		if ($icustomer != '')
			$sql.=" AND b.i_customer = '$icustomer' ";
		$sql.=	" GROUP BY b.i_customer"; //echo $sql."<br>";
		
		return $db2->query($sql);
		
		/*if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
				
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product=='') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last=="") && ($d_op_first==0 || $d_op_last==0)) ) {
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' ";
		}else{
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' "; 
		} 
		* 
		* return $db2->query(" SELECT SUM(a.n_count) AS jmlorder, sum((a.n_count-a.n_residual)) AS pemenuhan, b.i_customer
			
			FROM tm_op_item a
			
			INNER JOIN tm_op b ON a.i_op=b.i_op
			
			".$ddate." ".$batal." ".$imotif." ".$icustomer. " ".$f_op_dropforcast." GROUP BY b.i_customer ");
		* */
		
		// 04-06-2012, pake dari DO aja om
		/*SELECT sum(a.n_deliver-a.n_residual)
FROM tm_do_item a INNER JOIN tm_do b ON a.i_do=b.i_do WHERE (b.d_do BETWEEN '2012-05-01' 
AND '2012-05-31' ) AND a.i_product='DGT710700' AND b.i_customer='1'  */

		/*$sqlnya = " SELECT sum(a.n_deliver-a.n_residual) as pemenuhan
					FROM tm_do_item a INNER JOIN tm_do b ON a.i_do=b.i_do WHERE TRUE ";
		if ($d_op_first!="" && $d_op_last!="")
			$sqlnya.= " AND (b.d_do BETWEEN '$d_op_first' AND '$d_op_last') ";
		if ($i_product!='')
			$sqlnya.= " AND a.i_product='$iproductmotif' ";
		$sqlnya.= " AND b.i_customer='$icustomer' "; //echo $sqlnya; die();
		
		return $db2->query($sqlnya); */
		// no f*** longer used anymore maybe
		
		
	}
	
	// 10-09-2012
	function jumqtypercabang($i_product,$d_op_first,$d_op_last, $i_branch) {
				$db2=$this->load->database('db_external', TRUE);
		if($i_product!='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$i_product' ";
		}else if($i_product=='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$i_product' ";

		}else if($i_product!='' && (($d_op_first=="" || $d_op_last=="") && ($d_op_first==0 || $d_op_last==0)) ) {
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$i_product' ";

		}else{
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$i_product' ";
		}
	/*echo "SELECT SUM(a.n_count) AS jmlqty
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op
			".$ddate." ".$batal." ".$imotif." AND b.i_branch = '$i_branch'"; die(); */
		return $db2->query(" SELECT SUM(a.n_count) AS jmlqty
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op
			".$ddate." ".$batal." ".$imotif." AND b.i_branch = '$i_branch' ");
	}
	
	// 02-07-2012
	/*function cekhargagrosir($var_ddofirst,$var_ddolast,$imotif) {
		$sql = "SELECT b.is_grosir, b.harga_grosir FROM tm_do a, tm_do_item b WHERE a.i_do = b.i_do
				AND a.d_do BETWEEN '$var_ddofirst' AND '$var_ddofirst' AND b.i_product = '$imotif' ";
								
		return $db2->query($sql);
	} */
	
	// 28-07-2012
	function jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir) {
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		// SELECT SUM(a.n_count) AS jmlorder, sum(c.n_deliver) as pemenuhan, b.i_customer
		$db2=$this->load->database('db_external', TRUE);
		//((b.d_op >='2012-07-01' AND b.d_op <='2012-07-31') OR (d.d_do >='2012-07-01' AND d.d_do <='2012-07-31'))
		$sql = " SELECT a.n_count as jmlorder, c.n_deliver as pemenuhan, b.i_op, c.i_product, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND ((b.d_op >='$d_op_first' AND b.d_op <='$d_op_last') OR (d.d_do >='$d_op_first' AND d.d_do <='$d_op_last')) ";		
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
		
		// 04-08-2012, khusus tirai
		if ($icustomer != '')
			$sql.=" AND b.i_customer = '$icustomer' ";
		$sql.= "ORDER BY b.i_op, c.i_product"; //if ($is_grosir != 1) echo $sql."<br>";
		$query3= $db2->query($sql);
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			$jmlorder = 0; $pemenuhan = 0; $temp_op = "";
			foreach ($hasil3 as $row3) {
				$pemenuhan+= $row3->pemenuhan;
				if ($temp_op != $row3->i_op) {
					$jmlorder+= $row3->jmlorder;
					$temp_op = $row3->i_op;
				}
			}
		/*if ($is_grosir != 1)
			echo $pemenuhan;
		die(); */
			$data_op = array(		'jmlorder'=> $jmlorder,	
									'pemenuhan'=> $pemenuhan,
									'i_customer'=> $icustomer
								);
		}
		else
			$data_op = '';
		return $data_op;
		
		/*if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
				
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product=='') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last=="") && ($d_op_first==0 || $d_op_last==0)) ) {
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' ";
		}else{
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			$icustomer = " AND b.i_customer='$icustomer' "; 
		} 
		* 
		* return $db2->query(" SELECT SUM(a.n_count) AS jmlorder, sum((a.n_count-a.n_residual)) AS pemenuhan, b.i_customer
			
			FROM tm_op_item a
			
			INNER JOIN tm_op b ON a.i_op=b.i_op
			
			".$ddate." ".$batal." ".$imotif." ".$icustomer. " ".$f_op_dropforcast." GROUP BY b.i_customer ");
		* */
		
		// 04-06-2012, pake dari DO aja om
		/*SELECT sum(a.n_deliver-a.n_residual)
FROM tm_do_item a INNER JOIN tm_do b ON a.i_do=b.i_do WHERE (b.d_do BETWEEN '2012-05-01' 
AND '2012-05-31' ) AND a.i_product='DGT710700' AND b.i_customer='1'  */

		/*$sqlnya = " SELECT sum(a.n_deliver-a.n_residual) as pemenuhan
					FROM tm_do_item a INNER JOIN tm_do b ON a.i_do=b.i_do WHERE TRUE ";
		if ($d_op_first!="" && $d_op_last!="")
			$sqlnya.= " AND (b.d_do BETWEEN '$d_op_first' AND '$d_op_last') ";
		if ($i_product!='')
			$sqlnya.= " AND a.i_product='$iproductmotif' ";
		$sqlnya.= " AND b.i_customer='$icustomer' "; //echo $sqlnya; die();
		
		return $db2->query($sqlnya); */
		// no f*** longer used anymore maybe
	}
	
	// 13-08-2012, ngantuk poll
	function clistopvsdo_dokosong($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir) {	
		$db2=$this->load->database('db_external', TRUE);
		$data_op = '';	
		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
		//echo $i_product."<br>";
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "satu ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if($i_product=='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "dua ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			//echo "tiga ";
			$ddate	= "";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
		}else{
			//echo "empat ";
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}
		/*echo "SELECT c.f_stop_produksi AS stopproduct, 
				UPPER(a.i_product) AS imotif, 
				UPPER(a.e_product_name) AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$f_op_dropforcast."
				AND (a.n_count-a.n_residual)=0
				GROUP BY c.f_stop_produksi, UPPER(a.i_product), UPPER(a.e_product_name), c.v_unitprice		
				ORDER BY UPPER(a.i_product) ASC"; */
		
		$query3	= $db2->query(" SELECT c.f_stop_produksi AS stopproduct, 
				UPPER(a.i_product) AS imotif, 
				UPPER(a.e_product_name) AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$f_op_dropforcast."
				AND (a.n_count-a.n_residual)=0
				GROUP BY c.f_stop_produksi, UPPER(a.i_product), UPPER(a.e_product_name), c.v_unitprice		
				ORDER BY UPPER(a.i_product) ASC ");
						
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			
			foreach ($hasil3 as $row3) {
				/*if ($row3->imotif == 'TPT070100')
					echo "select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif'"; */
				$queryxx	= $db2->query(" select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif' ");
				if ($queryxx->num_rows() == 0){
					$data_op[] = array(		'imotif'=> $row3->imotif,	
									'productmotif'=> $row3->productmotif,
									'unitprice'=> $row3->unitprice,
									'stopproduct'=> $row3->stopproduct
								);
				}
			}
		}
		else
			$data_op = '';
		return $data_op;		
	}
}
?>
