<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $supplier, $cari) {	  
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_retur_makloon ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_retur_makloon WHERE kode_unit = '$supplier' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_retur_makloon WHERE kode_unit = '$supplier' 
					AND UPPER(no_retur) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_retur_makloon WHERE UPPER(no_retur) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_makloon_detail WHERE id_retur_makloon = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						if ($row1->jenis_makloon != '1') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
											WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg	= '';
								$satuan	= '';
							}
						}
						else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
											FROM tm_brg_hasil_makloon a, tm_satuan b 
											WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg	= '';
								$satuan	= '';
							}
						}
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_fb = '';
				}
				
				// ambil data detail fakturnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_makloon_faktur WHERE id_retur_makloon = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) { 
						//ambil tgl faktur
						$query3	= $this->db->query(" SELECT tgl_faktur FROM tm_faktur_makloon
						WHERE no_faktur = '$row2->no_faktur' AND kode_unit = '$row1->kode_unit' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$tgl_faktur	= $hasilrow->tgl_faktur;
							
							$pisah1 = explode("-", $tgl_faktur);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							if ($bln1 == '01')
								$nama_bln = "Januari";
							else if ($bln1 == '02')
								$nama_bln = "Februari";
							else if ($bln1 == '03')
								$nama_bln = "Maret";
							else if ($bln1 == '04')
								$nama_bln = "April";
							else if ($bln1 == '05')
								$nama_bln = "Mei";
							else if ($bln1 == '06')
								$nama_bln = "Juni";
							else if ($bln1 == '07')
								$nama_bln = "Juli";
							else if ($bln1 == '08')
								$nama_bln = "Agustus";
							else if ($bln1 == '09')
								$nama_bln = "September";
							else if ($bln1 == '10')
								$nama_bln = "Oktober";
							else if ($bln1 == '11')
								$nama_bln = "November";
							else if ($bln1 == '12')
								$nama_bln = "Desember";
							$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						}
						else {
							$tgl_faktur	= '';
						}
				
						$detail_faktur[] = array('no_faktur'=> $row2->no_faktur,
												'tgl_faktur'=> $tgl_faktur
											);
					}
				}
				else {
					$detail_faktur = '';
				}
				
				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
				//ambil nomor faktur
			/*	$query3	= $this->db->query(" SELECT no_faktur FROM tm_pembelian_nofaktur WHERE id = '$row1->id_pembelian_nofaktur' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$no_faktur	= $hasilrow->no_faktur;
				 }
				 else
					$no_sj = ''; */
				
				$sql = "SELECT nama FROM tm_jenis_makloon where id = '$row1->jenis_makloon' ";
				$query3	= $this->db->query($sql);
				$hasilrow3 = $query3->row();
				$nama_makloon	= $hasilrow3->nama;
				
				$pisah1 = explode("-", $row1->faktur_date_from);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$faktur_date_from = $tgl1."-".$bln1."-".$thn1;
				
				$pisah2 = explode("-", $row1->faktur_date_to);
				$tgl2= $pisah2[2];
				$bln2= $pisah2[1];
				$thn2= $pisah2[0];
				$faktur_date_to = $tgl2."-".$bln2."-".$thn2;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_dn_retur'=> $row1->no_dn_retur,	
											'tgl_retur'=> $row1->tgl_retur,
											'kode_supplier'=> $row1->kode_unit,
											'nama_supplier'=> $nama_unit,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb,
											'detail_faktur'=> $detail_faktur,
											'faktur_date_from'=> $faktur_date_from,
											'faktur_date_to'=> $faktur_date_to,
											'status_nota'=> $row1->status_nota,
											'nama_makloon'=> $nama_makloon
											);
				$detail_fb = array();
				$detail_faktur = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			/*$query	= $this->db->query(" SELECT a.* FROM tm_retur_beli a, tm_pembelian_nofaktur b WHERE 
			a.id_pembelian_nofaktur = b.id AND b.id <> '0' "); */
			$query	= $this->db->query(" SELECT * FROM tm_retur_makloon ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_retur_makloon WHERE kode_unit = '$supplier' ");
	}
	else {
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_retur_makloon WHERE kode_unit = '$supplier' 
					AND UPPER(no_retur) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_retur_makloon WHERE UPPER(no_retur) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_dn, $jenis_makloon){
    $this->db->select("id from tm_retur_makloon WHERE no_dn_retur = '$no_dn' AND jenis_makloon = '$jenis_makloon' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_notaretur($no_nota){
    $this->db->select("id from tm_nota_retur_beli WHERE no_nota = '$no_nota' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_dn_retur, $tgl_retur,$no_faktur, $kode_supplier, $ket, $jenis_makloon, 
		$kode, $nama, $qty, $biaya, $faktur, $kode_brg_jadi){  
    
    $tgl = date("Y-m-d");
			
			// ambil data terakhir di tabel tm_retur_makloon
			$query2	= $this->db->query(" SELECT id FROM tm_retur_makloon ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_retur	= $hasilrow->id; //echo $idnya; die();
			
			//save ke tabel retur_detail,
			if ($kode!='' && $qty!='') {
				
				if ($jenis_makloon == 1) {
						$nama_tabel_header = "tm_sj_hasil_makloon";
						$nama_tabel_detail = "tm_sj_hasil_makloon_detail";
						$nama_tabel_stok = "tm_stok_hasil_makloon";
						$nama_tabel_ttstok = "tt_stok_hasil_makloon";
						$idnya = "id_sj_hasil_makloon";
						$kodenya = "kode_brg_makloon";
				}
				else if ($jenis_makloon == 2) {
						$nama_tabel_header = "tm_sj_hasil_bisbisan";
						$nama_tabel_detail = "tm_sj_hasil_bisbisan_detail";
						$nama_tabel_stok = "tm_stok_hasil_bisbisan";
						$nama_tabel_ttstok = "tt_stok_hasil_bisbisan";
						$idnya = "id_sj_hasil_bisbisan";
						$kodenya = "kode_brg";
						$nama_tabel_proses = "tm_sj_proses_bisbisan_detail";
						$idprosesnya = "id_sj_proses_bisbisan_detail";
				}
				else if ($jenis_makloon == 3) { // bordir blm ditanyain alurnya
						$nama_tabel_header = "tm_sj_hasil_bordir";
						$nama_tabel_detail = "tm_sj_hasil_bordir_detail";
						$nama_tabel_stok = "tm_stok_hasil_bordir";
						$nama_tabel_ttstok = "tt_stok_hasil_bordir";
						$idnya = "id_sj_hasil_bordir";
						$kodenya = "kode_brg";
						$nama_tabel_proses = "tm_sj_proses_bordir_detail";
						$idprosesnya = "id_sj_proses_bordir_detail";
				}
				else if ($jenis_makloon == 4) {
						$nama_tabel_header = "tm_sj_hasil_print";
						$nama_tabel_detail = "tm_sj_hasil_print_detail";
						$nama_tabel_stok = "tm_stok_hasil_print";
						$nama_tabel_ttstok = "tt_stok_hasil_print";
						$idnya = "id_sj_hasil_print";
						$kodenya = "kode_brg";
						$nama_tabel_proses = "tm_sj_proses_print_detail";
						$idprosesnya = "id_sj_proses_print_detail";
				}
				
				if ($jenis_makloon != 5) { // 11.59
						// hitung jumlah qty dari tabel SJ masuk hasil makloon
						$sql = " SELECT SUM(a.qty) as jum 
							FROM ".$nama_tabel_detail." a, ".$nama_tabel_header." b, tm_faktur_makloon c, 
							tm_faktur_makloon_sj d, ".$nama_tabel_proses." e
							WHERE a.".$idnya." = b.id 
							AND c.no_faktur = '$faktur' AND e.id = a.".$idprosesnya."
							AND c.id=d.id_faktur_makloon 
							AND d.no_sj = b.no_sj AND b.kode_unit = c.kode_unit AND c.kode_unit = '$kode_supplier' 
							AND a.".$kodenya." = '".$kode_brg."' ";
						
						if ($jenis_makloon != 1)
							$sql.= " AND e.kode_brg_jadi = '$kode_brg_jadi' ";
						
						$query3	= $this->db->query($sql); 
						$hasilrow = $query3->row();
						$qty_faktur = $hasilrow->jum;
				} // 
											
				if ($jenis_makloon != 5) {		
					$sql = "SELECT sum(a.qty) as jum FROM tm_retur_makloon_detail a, tm_retur_makloon b,
										tm_retur_makloon_faktur c
										WHERE c.id_retur_makloon = b.id 
										AND a.id_retur_makloon = b.id AND a.id_retur_makloon_faktur = c.id 
										AND c.no_faktur = '$faktur'
										AND a.kode_brg = '$kode' ";
					
					if ($jenis_makloon != 1)
							$sql.= " AND a.kode_brg_jadi = '$kode_brg_jadi' ";
					
						$query3	= $this->db->query($sql);
						$hasilrow = $query3->row();
						$jum_retur = $hasilrow->jum; // ini sum qty di retur makloon berdasarkan kode brg tsb						
						//$qty = $qty-$jum_retur;
						if ($jum_retur == '')
							$jum_retur = 0;
				}
				
					$jumtot = $jum_retur+$qty;
					if ($jumtot > $qty_faktur)
						$qty = $qty_faktur-$jum_retur;
				
				// ambil id di tabel tm_retur_makloon_faktur
					$query2	= $this->db->query(" SELECT id FROM tm_retur_makloon_faktur WHERE no_faktur = '$faktur' 
					AND id_retur_makloon = '$id_retur' ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_retur_faktur = $hasilrow->id;
				
				// jika semua data tdk kosong, insert ke tm_retur_makloon_detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'qty'=>$qty,
					'biaya'=>$biaya,
					'id_retur_makloon'=>$id_retur,
					'id_retur_makloon_faktur'=>$id_retur_faktur
				);
				$this->db->insert('tm_retur_makloon_detail',$data_detail);
				
			 
			//insert data brg keluar ke tabel history stok, dan kurangi stok di tabel tm_stok
				
				//cek stok terakhir tm_stok, dan update stoknya
				$sql= "SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$kode'";
				if ($jenis_makloon != 1)
					$sql.= " AND kode_brg_jadi = '$kode_brg_jadi' ";
				
				$query3	= $this->db->query($sql);
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = abs($stok_lama-$qty);
				
				$sql = " INSERT INTO ".$nama_tabel_ttstok." (kode_brg,";
				if ($jenis_makloon != 1)
					$sql.= " kode_brg_jadi, ";
				$sql.= " no_bukti, keluar_lain, saldo, tgl_input, biaya) 
						VALUES ('$kode',";
				if ($jenis_makloon != 1)
					$sql.= " '$kode_brg_jadi', ";
				$sql.= " '$no_dn_retur', '$qty', '$new_stok2', '$tgl', '$biaya' ) ";
				$query3	= $this->db->query($sql);
				
				$sql = "UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode'";
				if ($jenis_makloon != 1)
					$sql.= " AND kode_brg_jadi = '$kode_brg_jadi' ";
				$this->db->query($sql);
				
			} // end if kode != '' dan $qty != ''
  }
    
  function delete($kode){    
	$tgl = date("Y-m-d");
	
	// reset stoknya
			$query3	= $this->db->query(" SELECT no_dn_retur, jenis_makloon FROM tm_retur_makloon WHERE id = '$kode' ");
			$hasilrow = $query3->row();
			$no_dn_retur = $hasilrow->no_dn_retur;
			$jenis_makloon = $hasilrow->jenis_makloon;
			
			if ($jenis_makloon == 1) {
				$nama_tabel_header = "tm_sj_hasil_makloon";
				$nama_tabel_detail = "tm_sj_hasil_makloon_detail";
				$nama_tabel_stok = "tm_stok_hasil_makloon";
				$nama_tabel_ttstok = "tt_stok_hasil_makloon";
				$idnya = "id_sj_hasil_makloon";
				$kodenya = "kode_brg_makloon";
			}
			else if ($jenis_makloon == 2) {
				$nama_tabel_header = "tm_sj_hasil_bisbisan";
				$nama_tabel_detail = "tm_sj_hasil_bisbisan_detail";
				$nama_tabel_stok = "tm_stok_hasil_bisbisan";
				$nama_tabel_ttstok = "tt_stok_hasil_bisbisan";
				$idnya = "id_sj_hasil_bisbisan";
				$kodenya = "kode_brg";
			}
			else if ($jenis_makloon == 3) {
				$nama_tabel_header = "tm_sj_hasil_bordir";
				$nama_tabel_detail = "tm_sj_hasil_bordir_detail";
				$nama_tabel_stok = "tm_stok_hasil_bordir";
				$nama_tabel_ttstok = "tt_stok_hasil_bordir";
				$idnya = "id_sj_hasil_bordir";
				$kodenya = "kode_brg";
			}
			else if ($jenis_makloon == 4) {
				$nama_tabel_header = "tm_sj_hasil_print";
				$nama_tabel_detail = "tm_sj_hasil_print_detail";
				$nama_tabel_stok = "tm_stok_hasil_print";
				$nama_tabel_ttstok = "tt_stok_hasil_print";
				$idnya = "id_sj_hasil_print";
				$kodenya = "kode_brg";
			}
	
				$query2	= $this->db->query(" SELECT * FROM tm_retur_makloon_detail WHERE id_retur_makloon = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$kode_brg = $row2->kode_brg;
						$kode_brg_jadi = $row2->kode_brg_jadi;
						$qty = $row2->qty;
						$biaya = $row2->biaya;
						
						//1. ambil stok terkini di tm_stok_
						$sql = " SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg='$kode_brg' ";
						if ($jenis_makloon != 1)
							$sql.= " AND kode_brg_jadi = '$kode_brg_jadi' ";
						$query3	= $this->db->query($sql);
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
						$stokreset = $stok_lama+$qty; //
							
							// new 140611
						/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga 
											WHERE kode_brg='$kode_brg' AND harga = '$harga' ");
							$hasilrow = $query3->row();
							$stok_lama2	= $hasilrow->stok;
							$stokreset2 = $stok_lama2+$qty; */
							
						//2. insert ke tabel tt_stok dgn tipe masuk, utk membatalkan data retur
						$sql = " INSERT INTO ".$nama_tabel_ttstok." (kode_brg,";
								if ($jenis_makloon != 1)
									$sql.= " kode_brg_jadi, ";
								$sql.= " no_bukti, masuk_lain, saldo, tgl_input, biaya) 
										VALUES ('$kode_brg',";
								if ($jenis_makloon != 1)
									$sql.= " '$kode_brg_jadi', ";
								$sql.= " '$no_dn_retur', '$qty', '$stokreset', '$tgl', '$biaya' ";
								$this->db->query($sql);
														
						//3. update stok di tm_stok_hasil_
							$sql = " UPDATE ".$nama_tabel_stok." SET stok = '$stokreset', tgl_update_stok = '$tgl'
												where kode_brg= '$kode_brg' ";
							if ($jenis_makloon != 1)
								$sql.= " AND kode_brg_jadi = '$kode_brg_jadi' ";
							$this->db->query($sql);
							
					}
				}	
	
    $this->db->delete('tm_retur_makloon_detail', array('id_retur_makloon' => $kode));
    $this->db->delete('tm_retur_makloon_faktur', array('id_retur_makloon' => $kode));
    $this->db->delete('tm_retur_makloon', array('id' => $kode));
  } 
  
  //function get_faktur($num, $offset, $csupplier, $cari) {
	function get_faktur($csupplier, $cari, $pkp) {
	if ($cari == "all") {
		/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
		$query = $this->db->get(); */
		
		$sql = " SELECT * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$csupplier' "; 
		/*if ($pkp == 't')
			$sql.= " AND status_faktur_pajak = 't' "; */
		
		$sql.= " ORDER BY tgl_faktur DESC ";
							
		$query	= $this->db->query($sql);

	}
	else {
		/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
		$query = $this->db->get(); */
		
		$sql = " SELECT * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$csupplier' "; 
		/*if ($pkp == 't')
			$sql.= " AND status_faktur_pajak = 't' "; */
		$sql.= " AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ";
		
		$query	= $this->db->query($sql);
	}
		$data_faktur = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail faktur-nya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur_sj WHERE id_pembelian_nofaktur = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$no_sj	= $row2->no_sj;
						
						$sql = "SELECT tgl_sj from tm_pembelian where no_sj = '$no_sj' 
								AND kode_supplier= '$csupplier'";
						$query3	= $this->db->query($sql);
						$hasilrow3 = $query3->row();
						$tgl_sj	= $hasilrow3->tgl_sj;
						
					/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b 
									WHERE a.id_retur_beli = b.id AND b.id_pembelian = '$row1->id' 
									AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$jum_op = $hasilrow->jum; // ini sum qty di returnya berdasarkan kode brg tsb
						
						$qty = $qty-$jum_op; */
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1; 
										
						$detail_faktur[] = array('id'=> $row2->id,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj
											);
					}
				}
				else {
					$detail_faktur = '';
				}
				
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'jumlah'=> $row1->jumlah,
											'detail_faktur'=> $detail_faktur
											);
				$detail_faktur = array();
			} // endforeach header
		}
		else {
			$data_faktur = '';
		}
		return $data_faktur;
  }
  
  function get_fakturtanpalimit($csupplier, $cari){
	if ($cari == "all") { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_faktur DESC ");
	}
	else {
		$query	= $this->db->query(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ");
	}
    
    return $query->result();  
  }
  
  function get_detail_brg($date_from, $date_to, $kode_supplier, $jenis_makloon){
    $detail_brg = array();
    /*$sql = "SELECT a.kode_brg, a.harga FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
	tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.id = '$id_faktur' AND c.id=d.id_pembelian_nofaktur 
	AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier'";
	die($sql); */
    
    //=============================================
   // $list_faktur = explode(",", $no_faktur); 
  //  foreach($list_faktur as $rowutama) {
	//	$rowutama = trim($rowutama);
	//	if ($rowutama != '') { 
    //=============================================    
			if ($jenis_makloon == 1) { // quilting
				$query	= $this->db->query(" SELECT distinct(a.kode_brg_makloon), a.biaya, c.no_faktur 
					FROM tm_sj_hasil_makloon_detail a, tm_sj_hasil_makloon b, 
					tm_faktur_makloon c, tm_faktur_makloon_sj d 
					WHERE a.id_sj_hasil_makloon = b.id AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.id = d.id_faktur_makloon 
					AND d.no_sj_masuk = b.no_sj AND b.kode_unit = c.kode_unit AND c.kode_unit = '$kode_supplier'
					AND c.jenis_makloon = '1'
					ORDER BY a.kode_brg_makloon ");
			}
			else if ($jenis_makloon == 2) { // bisbisan
				$query	= $this->db->query(" SELECT distinct(a.kode_brg), e.kode_brg_jadi, a.biaya, c.no_faktur 
					FROM tm_sj_hasil_bisbisan_detail a, tm_sj_hasil_bisbisan b, 
					tm_faktur_makloon c, tm_faktur_makloon_sj d, tm_sj_proses_bisbisan_detail e 
					WHERE a.id_sj_hasil_bisbisan = b.id AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.id = d.id_faktur_makloon AND e.id = a.id_sj_proses_bisbisan_detail
					AND d.no_sj_masuk = b.no_sj AND b.kode_unit = c.kode_unit AND c.kode_unit = '$kode_supplier'
					AND c.jenis_makloon = '2' ORDER BY a.kode_brg ");
			}
			else if ($jenis_makloon == 3) { // bordir
				
			}
				
			else if ($jenis_makloon == 4) { // print
				$query	= $this->db->query(" SELECT distinct(a.kode_brg), e.kode_brg_jadi, a.biaya, c.no_faktur 
					FROM tm_sj_hasil_print_detail a, tm_sj_hasil_print b, 
					tm_faktur_makloon c, tm_faktur_makloon_sj d, tm_sj_proses_print_detail e  
					WHERE a.id_sj_hasil_print = b.id AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.id = d.id_faktur_makloon AND e.id = a.id_sj_proses_print_detail
					AND d.no_sj_masuk = b.no_sj AND b.kode_unit = c.kode_unit AND c.kode_unit = '$kode_supplier'
					AND c.jenis_makloon = '4' ORDER BY a.kode_brg ");
			}

			else if ($jenis_makloon == 5) { // asesoris
				$query	= $this->db->query(" SELECT distinct(a.kode_brg), e.kode_brg_jadi, a.biaya, c.no_faktur 
					FROM tm_sj_hasil_asesoris_detail a, tm_sj_hasil_asesoris b, 
					tm_faktur_makloon c, tm_faktur_makloon_sj d, tm_sj_proses_asesoris_detail e 
					WHERE a.id_sj_hasil_asesoris = b.id AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.id = d.id_faktur_makloon AND e.id = a.id_sj_proses_asesoris_detail
					AND d.no_sj_masuk = b.no_sj AND b.kode_unit = c.kode_unit AND c.kode_unit = '$kode_supplier'
					AND c.jenis_makloon = '5' ORDER BY a.kode_brg ");
			
			}
			
			$hasil = $query->result();
			foreach($hasil as $row1) {
				if ($jenis_makloon == 1) {
					$kode_brg = $row1->kode_brg_makloon;
					$kode_brg_jadi = '';
					$tabel_barang = "tm_brg_hasil_makloon";
				}
				else {
					$kode_brg = $row1->kode_brg;
					$kode_brg_jadi = $row1->kode_brg_jadi;
					$tabel_barang = "tm_barang";
				}
				
					$query2	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM ".$tabel_barang." a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg	= '';
						$satuan	= '';
					}
					
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
					
					if ($jenis_makloon == 1) {
						$nama_tabel_header = "tm_sj_hasil_makloon";
						$nama_tabel_detail = "tm_sj_hasil_makloon_detail";
						$idnya = "id_sj_hasil_makloon";
						$kodenya = "kode_brg_makloon";
					}
					else if ($jenis_makloon == 2) {
						$nama_tabel_header = "tm_sj_hasil_bisbisan";
						$nama_tabel_detail = "tm_sj_hasil_bisbisan_detail";
						$idnya = "id_sj_hasil_bisbisan";
						$kodenya = "kode_brg";
					}
					else if ($jenis_makloon == 3) {
						$nama_tabel_header = "tm_sj_hasil_bordir";
						$nama_tabel_detail = "tm_sj_hasil_bordir_detail";
						$idnya = "id_sj_hasil_bordir";
						$kodenya = "kode_brg";
					}
					else if ($jenis_makloon == 4) {
						$nama_tabel_header = "tm_sj_hasil_print";
						$nama_tabel_detail = "tm_sj_hasil_print_detail";
						$idnya = "id_sj_hasil_print";
						$kodenya = "kode_brg";
					}
					
					if ($jenis_makloon != 5) {
						// hitung jumlah qty dari tabel SJ masuk hasil makloon
						$query3	= $this->db->query(" SELECT SUM(a.qty) as jum 
						FROM ".$nama_tabel_detail." a, ".$nama_tabel_header." b, tm_faktur_makloon c, 
						tm_faktur_makloon_sj d WHERE a.".$idnya." = b.id 
						AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
						AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
						AND c.id=d.id_faktur_makloon 
						AND d.no_sj = b.no_sj AND b.kode_unit = c.kode_unit AND c.kode_unit = '$kode_supplier' 
						AND a.".$kodenya." = '".$kode_brg."' ");
						$hasilrow = $query3->row();
						$qty_faktur = $hasilrow->jum;
					} // 
					
					if ($jenis_makloon != 5) {		
						$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_makloon_detail a, tm_retur_makloon b,
										tm_retur_makloon_faktur c, tm_faktur_makloon d 
										WHERE c.id_retur_makloon = b.id 
										AND c.no_faktur = d.no_faktur
										AND b.kode_unit = d.kode_unit
										AND a.id_retur_makloon = b.id AND a.id_retur_makloon_faktur = c.id 
										AND d.tgl_faktur >= to_date('$date_from','dd-mm-yyyy')
										AND d.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
										AND a.kode_brg = '$kode_brg' ");
						$hasilrow = $query3->row();
						$jum_retur = $hasilrow->jum; // ini sum qty di retur makloon berdasarkan kode brg tsb						
						//$qty = $qty-$jum_retur;
						if ($jum_retur == '')
							$jum_retur = 0;
					}
							
					$detail_brg[] = array(		
												'kode_brg'=> $kode_brg,
												'kode_brg_jadi'=> $kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'biaya'=> $row1->biaya,
												'no_faktur'=> $row1->no_faktur,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty_faktur'=> $qty_faktur,
												'jum_retur'=> $jum_retur
										);
				
			}
	//	}
//	} //end foreach
	return $detail_brg;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
  function get_retur($id_retur){
	$query	= $this->db->query(" SELECT * FROM tm_retur_makloon WHERE id = '$id_retur' ");    
    $hasil = $query->result();
    
    $data_retur = array();
	$detail_retur = array();
	
	foreach ($hasil as $row1) {
		// ambil data no faktur
		$query1	= $this->db->query(" SELECT * FROM tm_retur_makloon_faktur WHERE id_retur_makloon = '$row1->id' ");
		if ($query1->num_rows() > 0) {
			$hasil1=$query1->result();
			$list_no_faktur = ''; $k=0;
			$hitung = count($hasil1);
			foreach ($hasil1 as $rownya) {
				$list_no_faktur .= $rownya->no_faktur;
				if ($k<$hitung-1)
					 echo ", ";
				$k++;
			}
		}
		
		if ($row1->jenis_makloon == 1) {
			$nama_tabel_header = "tm_sj_hasil_makloon";
			$nama_tabel_detail = "tm_sj_hasil_makloon_detail";
			$idnya = "id_sj_hasil_makloon";
			$kodenya = "kode_brg_makloon";
		}
		else if ($row1->jenis_makloon == 2) {
			$nama_tabel_header = "tm_sj_hasil_bisbisan";
			$nama_tabel_detail = "tm_sj_hasil_bisbisan_detail";
			$idnya = "id_sj_hasil_bisbisan";
			$kodenya = "kode_brg";
		}
		else if ($row1->jenis_makloon == 3) {
			$nama_tabel_header = "tm_sj_hasil_bordir";
			$nama_tabel_detail = "tm_sj_hasil_bordir_detail";
			$idnya = "id_sj_hasil_bordir";
			$kodenya = "kode_brg";
		}
		else if ($row1->jenis_makloon == 4) {
			$nama_tabel_header = "tm_sj_hasil_print";
			$nama_tabel_detail = "tm_sj_hasil_print_detail";
			$idnya = "id_sj_hasil_print";
			$kodenya = "kode_brg";
		}
		
				// ambil data detail barangnya, order by no fakturnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_makloon_detail WHERE id_retur_makloon = '$row1->id'
				ORDER BY id_retur_makloon_faktur ");
				if ($query2->num_rows() > 0) {
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						if ($row1->jenis_makloon != '1') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, 
											tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg	= '';
								$satuan	= '';
							}
						}
						else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
											tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg	= '';
								$satuan	= '';
							}
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
						
						$query3	= $this->db->query(" SELECT no_faktur FROM tm_retur_makloon_faktur WHERE 
									id = '$row2->id_retur_makloon_faktur' AND id_retur_makloon = '$row1->id' ");
						$hasilrow = $query3->row();
						$no_faktur	= $hasilrow->no_faktur;
						
						// ================================================
						
						// ambil data biaya sesuai kode brg dari tabel Sj masuk hasil makloon detail
					/*	$query3	= $this->db->query(" SELECT a.biaya FROM ".$nama_tabel_detail." a, ".$nama_tabel_header." b, 
						tm_faktur_makloon c, tm_faktur_makloon_sj d WHERE a.".$idnya." = b.id 
						AND c.id=d.id_faktur_makloon AND c.no_faktur = '$no_faktur'
						AND d.no_sj = b.no_sj AND b.kode_unit = c.kode_unit 
						AND c.kode_unit = '$row1->kode_unit' AND a.".$kodenya." = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$biayanya = $hasilrow->biaya; */
						
						$biayanya = $row2->biaya;
						//=================================================
																	
						// hitung jumlah qty dari tabel SJ/tm_pembelian
						// sampe sini 12.43
						$query3	= $this->db->query(" SELECT SUM(a.qty) as jum 
						FROM ".$nama_tabel_detail." a, ".$nama_tabel_header." b, tm_faktur_makloon c, 
						tm_faktur_makloon_sj d WHERE a.".$idnya." = b.id 
						AND c.tgl_faktur >= '$row1->faktur_date_from'
						AND c.tgl_faktur <= '$row1->faktur_date_to'
						AND c.id=d.id_faktur_makloon 
						AND d.no_sj = b.no_sj AND b.kode_unit = c.kode_unit AND c.kode_unit = '$row1->kode_unit' 
						AND a.".$kodenya." = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$qty_faktur = $hasilrow->jum;
														
						$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_makloon_detail a, tm_retur_makloon b,
									tm_retur_makloon_faktur c, tm_faktur_makloon d 
									WHERE c.id_retur_makloon = b.id 
									AND c.no_faktur = d.no_faktur
									AND b.kode_unit = d.kode_unit
									AND a.id_retur_makloon = b.id AND a.id_retur_makloon_faktur = c.id 
									AND d.tgl_faktur >= '$row1->faktur_date_from'
									AND d.tgl_faktur <= '$row1->faktur_date_to'
									AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$jum_retur = $hasilrow->jum; // ini sum qty di retur makloon berdasarkan kode brg tsb						
						//$qty = $qty-$jum_retur;
						if ($jum_retur == '')
							$jum_retur = 0;
				
						$detail_retur[] = array(		
										'id'=> $row2->id,
										'kode_brg'=> $row2->kode_brg,
										'biaya'=> $biayanya,
										'nama'=> $nama_brg,
										'satuan'=> $satuan,
										'kode_brg_jadi'=> $row2->kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'qty_faktur'=> $qty_faktur,
										'jum_retur'=> $jum_retur,
										'qty'=> $row2->qty
										);
					}
				}
				else {
					$detail_retur = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				$pisah1 = explode("-", $row1->faktur_date_from);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$faktur_date_from = $tgl1."-".$bln1."-".$thn1;
				
				$pisah2 = explode("-", $row1->faktur_date_to);
				$tgl2= $pisah2[2];
				$bln2= $pisah2[1];
				$thn2= $pisah2[0];
				$faktur_date_to = $tgl2."-".$bln2."-".$thn2;
				 
				$data_retur[] = array(		'id'=> $row1->id,	
											'list_no_faktur'=> $list_no_faktur,	
											'tgl_retur'=> $row1->tgl_retur,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'no_dn_retur'=> $row1->no_dn_retur,
											'faktur_date_from'=> $faktur_date_from,
											'faktur_date_to'=> $faktur_date_to,
											'detail_retur'=> $detail_retur
											);
				$detail_retur = array();
				$list_no_faktur = '';
	}
	return $data_retur;
  }
  
  function get_sj_retur($jnsaction, $supplier, $no_notanya, $cari, $pkp) {
	  // ambil data pembelian 
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else { // utk proses edit
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' OR no_faktur <> '' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' OR no_nota = '$no_notanya' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' AND a.kode_supplier = '$supplier' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND (a.status_nota = 'f' OR a.no_nota = '$no_notanya') 
						AND a.kode_supplier = '$supplier' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql); //
			}
		}
	}
	else {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' 
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND (a.status_nota = 'f' OR a.no_nota = '$no_notanya' )
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND a.status_nota = 'f' AND a.kode_supplier = '$supplier'
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$sql = " SELECT a.* FROM tm_retur_beli a, tm_retur_beli_faktur b, tm_pembelian_nofaktur c 
						WHERE a.id = b.id_retur_beli AND a.kode_supplier = c.kode_supplier 
						AND c.no_faktur = b.no_faktur AND (a.status_nota = 'f' OR a.no_nota = '$no_notanya' ) 
						AND a.kode_supplier = '$supplier'
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.kode_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
	}
		$data_fb = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$tot_sj = 0;
				// ambil sum total per faktur dari SJ yg dipilih (blm beres 280411)
				
				// 1. query perulangan dari tabel tm_retur_beli_faktur
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT SUM(qty*harga) as jumlahnya FROM tm_retur_beli_detail 
						WHERE id_retur_beli = '$row1->id' AND id_retur_beli_faktur = '$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jumlahnya	= $hasilrow->jumlahnya;
							$tot_sj+= $jumlahnya;
						}
						else {
							$jumlahnya	= '0';
						}
					}
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_dn_retur'=> $row1->no_dn_retur,
											'tgl_retur'=> $row1->tgl_retur,
											'tgl_update'=> $row1->tgl_update,
											'tot_sj'=> $tot_sj
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function savenota($no_fp, $tgl_fp, $supplier, $jum, $no_sj){  
    $tgl = date("Y-m-d");
	$list_sj = explode(",", $no_sj); 

	$data_header = array(
			  'no_nota'=>$no_fp,
			  'tgl_nota'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'kode_supplier'=>$supplier,
			  'jumlah'=>$jum
			);
	$this->db->insert('tm_nota_retur_beli',$data_header);
	
	// ambil data terakhir di tabel tm_nota_retur_beli
	$query2	= $this->db->query(" SELECT id FROM tm_nota_retur_beli ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_nota_retur_beli'=>$id_pf,
			  'no_dn_retur'=>$row1
			);
			$this->db->insert('tm_nota_retur_beli_sj',$data_detail);
			
			// update status_nota di tabel tm_retur_beli
			$this->db->query(" UPDATE tm_retur_beli SET no_nota = '$no_fp', status_nota = 't' 
								WHERE no_dn_retur = '$row1' AND kode_supplier = '$supplier' ");
		}
	}
   
  }
  
  function getAllnota($num, $offset, $supplier, $cari) {
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_nota_retur_beli ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_nota_retur_beli
							WHERE kode_supplier = '$supplier' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_nota_retur_beli where kode_supplier = '$supplier' AND 
							(UPPER(no_nota) like UPPER('%$cari%') ) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_nota_retur_beli where 
							(UPPER(no_nota) like UPPER('%$cari%')) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				//echo $totalnya; die();
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b, tm_nota_retur_beli a 
				WHERE a.kode_supplier = b.kode_supplier AND a.id = '$row1->id' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				// ambil detail data no & tgl SJ
				$query2	= $this->db->query(" SELECT a.no_dn_retur FROM tm_nota_retur_beli_sj a, tm_nota_retur_beli b 
							WHERE a.id_nota_retur_beli = b.id AND
							b.no_nota = '$row1->no_nota' AND b.kode_supplier = '$kode_supplier' ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_dn_retur	= $row2->no_dn_retur;
						
						$sql = "SELECT tgl_retur from tm_retur_beli where no_dn_retur = '$no_dn_retur' 
								AND kode_supplier= '$kode_supplier'";
						$query3	= $this->db->query($sql);
					//	if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_sj	= $hasilrow3->tgl_retur;
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_dn_retur'=> $no_dn_retur,
											'tgl_sj'=> $tgl_sj
											);		
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_nota);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_nota = $tgl1." ".$nama_bln." ".$thn1;
						
					$pisah1 = explode("-", $row1->tgl_input);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_input = $tgl1." ".$nama_bln." ".$thn1;
						
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,	
											'tgl_nota'=> $tgl_nota,	
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,	
											'tgl_input'=> $tgl_input,
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAllnotatanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli
							WHERE kode_supplier = '$supplier' ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where kode_supplier = '$supplier' AND 
							(UPPER(no_nota) like UPPER('%$cari%') )  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where (UPPER(no_nota) like UPPER('%$cari%'))  ");
	}
    
    return $query->result();  
  }
  
  function get_nota($id_nota){
	$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where id='$id_nota' ");    
	
	$data_nota = array();
	$detail_sj = array();
	$no_faktur_pajak = '';
	$tgl_faktur_pajak = '';
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT no_dn_retur FROM tm_nota_retur_beli_sj 
									WHERE id_nota_retur_beli = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sj = '';
				foreach ($hasil2 as $row2) {
					$no_sj .= $row2->no_dn_retur.", ";
					
					// 24 mei 2011
					// ambil salah satu no faktur (1 faktur pajak = banyak faktur beli, 
					// berarti faktur2 yg dipilih di retur itu faktur pajaknya sama)
					$query3	= $this->db->query(" SELECT b.no_faktur
							FROM tm_retur_beli a, tm_retur_beli_faktur b 
							WHERE a.id = b.id_retur_beli AND
							a.no_dn_retur = '$row2->no_dn_retur' LIMIT 1 ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$no_faktur	= $hasilrow->no_faktur;
					}
					else
						$no_faktur = '';
					
					// ambil no faktur pajak
					$query3	= $this->db->query(" SELECT a.no_faktur_pajak, a.tgl_faktur_pajak FROM 
								tm_pembelian_pajak a, tm_pembelian_pajak_detail b
								WHERE a.id = b.id_pembelian_pajak AND b.no_faktur = '$no_faktur' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$no_faktur_pajak	= $hasilrow->no_faktur_pajak;
						$tgl_faktur_pajak	= $hasilrow->tgl_faktur_pajak;
					}
					else {
						$no_faktur_pajak = '';
						$tgl_faktur_pajak= '';
					}
					
					$sql = " SELECT b.kode_brg, b.qty, b.harga
							FROM tm_retur_beli a, tm_retur_beli_detail b 
							WHERE a.id = b.id_retur_beli AND
							a.no_dn_retur = '$row2->no_dn_retur' ";
					//echo($sql."<br>");
					
					$query3	= $this->db->query(" SELECT b.kode_brg, b.qty, b.harga
							FROM tm_retur_beli a, tm_retur_beli_detail b 
							WHERE a.id = b.id_retur_beli AND
							a.no_dn_retur = '$row2->no_dn_retur' ");
					if ($query3->num_rows() > 0){
						$hasil3 = $query3->result();
						foreach ($hasil3 as $row3) {
								$query4	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row3->kode_brg' ");
								if ($query4->num_rows() > 0){
									$hasilrow = $query4->row();
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$nama_brg	= '';
									$satuan	= '';
								}
								
								$detail_brg[] = array(		
											'kode_brg'=> $row3->kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row3->qty,
											'harga'=> $row3->harga );
						}
					}
					else {
							$detail_brg = '';
					}
				}
				
				// ambil data supplier
				$query4	= $this->db->query(" SELECT nama_npwp, alamat, npwp FROM tm_supplier 
				WHERE kode_supplier = '$row1->kode_supplier' ");
					if ($query4->num_rows() > 0){
						$hasilrow = $query4->row();
						$nama_npwp	= $hasilrow->nama_npwp;
						$alamat	= $hasilrow->alamat;
						$npwp	= $hasilrow->npwp;
					}
					else {
						$nama_npwp	= '';
						$alamat	= '';
						$npwp	= '';
					}
				
				$pisah1 = explode("-", $row1->tgl_nota);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_nota = $tgl1."-".$bln1."-".$thn1;						
			
				$data_nota[] = array(		'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,
											'tgl_nota'=> $tgl_nota,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_npwp'=> $nama_npwp,
											'alamat'=> $alamat,
											'npwp'=> $npwp,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_faktur_pajak'=> $no_faktur_pajak,
											'tgl_faktur_pajak'=> $tgl_faktur_pajak,
											'no_sj'=> $no_sj,
											'detail_brg'=> $detail_brg
											);
				$detail_brg = array();
			} // endforeach header
	}
    return $data_nota; 
  }
  
  function deletenotaretur($kode){    	
	//semua no_sj di tabel tm_retur_beli yg bersesuaian dgn tm_nota_retur_beli dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.kode_supplier, b.no_dn_retur FROM tm_nota_retur_beli a, tm_nota_retur_beli_sj b
							WHERE a.id = b.id_nota_retur_beli AND a.id = '$kode' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$this->db->query(" UPDATE tm_retur_beli SET status_nota = 'f', no_nota = '' 
							WHERE kode_supplier = '$row1->kode_supplier' AND no_dn_retur = '$row1->no_dn_retur' ");
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di nota_retur_beli dan nota_retur_beli_sj
	$this->db->query(" DELETE FROM tm_nota_retur_beli_sj where id_nota_retur_beli = '$kode' ");
	$this->db->query(" DELETE FROM tm_nota_retur_beli where id = '$kode' ");

  }
  
  function get_jenis_makloon(){
	$query	= $this->db->query(" SELECT * FROM tm_jenis_makloon WHERE id <> '5' ORDER BY id ");    
    return $query->result();  
  }
  
  function konversi_angka2romawi($bln_now) {
	if ($bln_now == "01")
		$romawi_bln = "I";
	else if ($bln_now == "02")
		$romawi_bln = "II";
	else if ($bln_now == "03")
		$romawi_bln = "III";
	else if ($bln_now == "04")
		$romawi_bln = "IV";
	else if ($bln_now == "05")
		$romawi_bln = "V";
	else if ($bln_now == "06")
		$romawi_bln = "VI";
	else if ($bln_now == "07")
		$romawi_bln = "VII";
	else if ($bln_now == "08")
		$romawi_bln = "VIII";
	else if ($bln_now == "09")
		$romawi_bln = "IX";
	else if ($bln_now == "10")
		$romawi_bln = "X";
	else if ($bln_now == "11")
		$romawi_bln = "XI";
	else if ($bln_now == "12")
		$romawi_bln = "XII";
		
	return $romawi_bln;
  }
  
  function konversi_romawi2angka($blnrom) {
	if ($blnrom == "I")
		$bln_now = "01";
	else if ($blnrom == "II")
		$bln_now = "02";
	else if ($blnrom == "III")
		$bln_now = "03";
	else if ($blnrom == "IV")
		$bln_now = "04";
	else if ($blnrom == "V")
		$bln_now = "05";
	else if ($blnrom == "VI")
		$bln_now = "06";
	else if ($blnrom == "VII")
		$bln_now = "07";
	else if ($blnrom == "VIII")
		$bln_now = "08";
	else if ($blnrom == "IX")
		$bln_now = "09";
	else if ($blnrom == "X")
		$bln_now = "10";
	else if ($blnrom == "XI")
		$bln_now = "11";
	else if ($blnrom == "XII")
		$bln_now = "12";
		
	return $bln_now;
  }
  
  function konversi_bln2deskripsi($bln1) {
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";  
		return $nama_bln;
  }

}
