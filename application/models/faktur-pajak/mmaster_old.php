<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $supplier, $cari) {
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_pembelian_pajak
								ORDER BY tgl_faktur_pajak DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_pembelian_pajak WHERE kode_supplier = '$supplier'
								ORDER BY tgl_faktur_pajak DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_pembelian_pajak WHERE kode_supplier = '$supplier'
								AND UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY tgl_faktur_pajak DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_pembelian_pajak WHERE UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY tgl_faktur_pajak DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) { 
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b, tm_pembelian_pajak a 
				WHERE a.kode_supplier = b.kode_supplier AND a.id = '$row1->id' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				// ambil data detail no & tgl faktur
				$query2	= $this->db->query(" SELECT a.no_faktur FROM tm_pembelian_pajak_detail a, tm_pembelian_pajak b 
							WHERE a.id_pembelian_pajak = b.id AND
							b.no_faktur_pajak = '$row1->no_faktur_pajak' AND b.kode_supplier = '$kode_supplier' "); 
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_faktur	= $row2->no_faktur;
						
						$sql = "SELECT tgl_faktur from tm_pembelian_nofaktur where no_faktur = '$no_faktur' 
								AND kode_supplier= '$kode_supplier'"; 
						//if ($no_faktur == 'PJ-12050403') echo $sql."<br>";
						$query3	= $this->db->query($sql);
						if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_faktur	= $hasilrow3->tgl_faktur;
						}
						else {
							$sql = "SELECT tgl_faktur from tm_faktur_makloon where no_faktur = '$no_faktur' 
								AND kode_unit= '$kode_supplier' AND jenis_makloon = '1'"; //echo $sql."<br>";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0) {
								$hasilrow3 = $query3->row();
								$tgl_faktur	= $hasilrow3->tgl_faktur;
							}
							else
								$tgl_faktur	= '';
						}
							
						$pisah1 = explode("-", $tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_faktur'=> $no_faktur,
											'tgl_faktur'=> $tgl_faktur
											);
						
						}		
					}
					else {
						$detail_fb = '';
					}
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_faktur_pajak'=> $row1->no_faktur_pajak,	
											'tgl_faktur_pajak'=> $row1->tgl_faktur_pajak,	
											'jumlah'=> $row1->jumlah,	
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" select * FROM tm_pembelian_pajak ORDER BY tgl_faktur_pajak DESC  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_pajak WHERE kode_supplier = '$supplier'
								ORDER BY tgl_faktur_pajak DESC ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_pajak WHERE kode_supplier = '$supplier'
								AND UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY tgl_faktur_pajak DESC ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_pajak WHERE 
								UPPER(no_faktur_pajak) like UPPER('%$cari%')
						ORDER BY tgl_faktur_pajak DESC ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_fp, $supplier){
    $this->db->select("id from tm_pembelian_pajak WHERE kode_supplier = '$supplier' AND no_faktur_pajak = '$no_fp' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_fp, $tgl_fp, $supplier, $jum, $dpp, $no_faktur, $is_quilting){  
    $tgl = date("Y-m-d");
	$list_faktur = explode(",", $no_faktur); 
	$list_is_quilting = explode(",", $is_quilting); 
	
	$data_header = array(
			  'no_faktur_pajak'=>$no_fp,
			  'tgl_faktur_pajak'=>$tgl_fp,
			  'jumlah'=>$jum,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'kode_supplier'=>$supplier,
			  'dpp'=>$dpp
			);
			$this->db->insert('tm_pembelian_pajak',$data_header);
	
	//
	// ambil data terakhir di tabel tm_pembelian_pajak
	$query2	= $this->db->query(" SELECT id FROM tm_pembelian_pajak ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	// insert tabel detail faktur-nya
	 for($j=0; $j<count($list_faktur)-1; $j++){
		$list_faktur[$j] = trim($list_faktur[$j]);
		$list_is_quilting[$j] = trim($list_is_quilting[$j]);
		
		$data_detail = array(
			  'id_pembelian_pajak'=>$id_pf,
			  'no_faktur'=>$list_faktur[$j]
			);
			$this->db->insert('tm_pembelian_pajak_detail',$data_detail);
		
		if ($list_is_quilting[$j] == 'f')
			// update status_faktur di tabel tm_pembelian_nofaktur
			$this->db->query(" UPDATE tm_pembelian_nofaktur SET no_faktur_pajak = '$no_fp', status_faktur_pajak = 't' 
								WHERE no_faktur = '".$list_faktur[$j]."' AND kode_supplier = '$supplier' ");
		else
			// update status_faktur di tabel tm_faktur_makloon
			$this->db->query(" UPDATE tm_faktur_makloon SET no_faktur_pajak = '$no_fp', status_faktur_pajak = 't' 
								WHERE no_faktur = '".$list_faktur[$j]."' AND kode_unit = '$supplier' AND jenis_makloon = '1' ");
	 }
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	// insert tabel detail faktur-nya
/*	foreach($list_faktur as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_pembelian_pajak'=>$id_pf,
			  'no_faktur'=>$row1
			);
			$this->db->insert('tm_pembelian_pajak_detail',$data_detail);
			
			// update status_faktur di tabel tm_pembelian_nofaktur
			$this->db->query(" UPDATE tm_pembelian_nofaktur SET no_faktur_pajak = '$no_fp', status_faktur_pajak = 't' 
								WHERE no_faktur = '$row1' AND kode_supplier = '$supplier' ");
		}
	} */
   
  }
    
  function delete($kode){
	//semua no_faktur di tabel tm_pembelian_nofaktur yg bersesuaian dgn tm_pembelian_pajak 
	//dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.kode_supplier, b.no_faktur FROM tm_pembelian_pajak a, tm_pembelian_pajak_detail b
							WHERE a.id = b.id_pembelian_pajak AND a.id = '$kode' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' WHERE 
			kode_unit = '$row1->kode_supplier' AND no_faktur = '$row1->no_faktur' ");
			
			$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_faktur_pajak = 'f', no_faktur_pajak = '' WHERE 
								kode_supplier = '$row1->kode_supplier' AND no_faktur = '$row1->no_faktur' ");

		}
	}
	//---------------------------------------------  
	  
	$this->db->delete('tm_pembelian_pajak_detail', array('id_pembelian_pajak' => $kode));    	
	$this->db->delete('tm_pembelian_pajak', array('id' => $kode));

  }
  
  //function get_faktur_pembelian($num, $offset, $jnsaction, $supplier, $cari) {
	function get_faktur_pembelian($jnsaction, $supplier, $cari) {
	  // ambil data faktur pembelian
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk add
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon
				FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' 
				UNION SELECT id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
				WHERE status_faktur_pajak = 'f'
				order by kode_supplier, tgl_faktur DESC, no_faktur DESC ");
			}
			else { // utk edit
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> '' 
								order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon 
							FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> '' 
							UNION SELECT id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
								WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> ''
								order by kode_supplier, tgl_faktur DESC, no_faktur DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk add
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND kode_supplier = '$supplier' 
									order by kode_supplier,tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon 
								FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND kode_supplier = '$supplier' 
								
								UNION SELECT id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
								WHERE status_faktur_pajak = 'f' AND kode_unit = '$supplier' 
								order by kode_supplier,tgl_faktur DESC, no_faktur DESC ");
			}
			else { // edit
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
				AND kode_supplier = '$supplier' order by kode_supplier,tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon 
							FROM tm_pembelian_nofaktur 
							WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
							AND kode_supplier = '$supplier' 
							
							UNION SELECT id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
							WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
							AND kode_unit = '$supplier' 
							order by kode_supplier,tgl_faktur DESC, no_faktur DESC ");
			}
		}
	}
	else {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk add		
			/*	$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon 
				FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND 
				UPPER(no_faktur) like UPPER('%$cari%') 
				
				UNION select id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
				WHERE status_faktur_pajak = 'f' AND 
				UPPER(no_faktur) like UPPER('%$cari%')
				order by kode_supplier, tgl_faktur DESC, no_faktur DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon 
				FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') 
				
				UNION select id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
				WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') 
				order by kode_supplier, tgl_faktur DESC, no_faktur DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk add		
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' 
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon 
				FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' 
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') 
				
				UNION select id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
				WHERE status_faktur_pajak = 'f' AND kode_unit = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') 
				order by kode_supplier, tgl_faktur DESC, no_faktur DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" SELECT id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon 
				FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') 
				UNION select id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon FROM tm_faktur_makloon
				WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
				AND kode_unit = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') 
				order by kode_supplier, tgl_faktur DESC, no_faktur DESC ");
			}
		}
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query2	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query2->row();
				$nama_supplier	= $hasilrow->nama;
				
			/*	$query2	= $this->db->query(" SELECT SUM(a.total) as totnya FROM tm_pembelian a, tm_pembelian_nofaktur b, tm_pembelian_nofaktur_sj c 
									WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
									AND b.id = c.id_pembelian_nofaktur
									AND b.no_faktur = '$row1->no_faktur' ");
				if ($query2->num_rows() > 0) { //
					$hasilrow = $query2->row();
					$totalnya	= $hasilrow->totnya;				
				}
				else
					$totalnya = '0'; */
				if ($row1->is_makloon == 'f')
					$query2	= $this->db->query(" SELECT SUM(a.total_pajak) as totnya FROM tm_pembelian a, tm_pembelian_nofaktur b, tm_pembelian_nofaktur_sj c 
									WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
									AND b.id = c.id_pembelian_nofaktur AND a.status_aktif = 't'
									AND b.no_faktur = '$row1->no_faktur' AND a.kode_supplier = '$row1->kode_supplier' ");
				else
					$query2	= $this->db->query(" SELECT SUM(a.total_pajak) as totnya FROM tm_sj_hasil_makloon a, tm_faktur_makloon b, tm_faktur_makloon_sj c 
									WHERE a.kode_unit = b.kode_unit AND a.no_sj = c.no_sj_masuk 
									AND b.id = c.id_faktur_makloon
									AND b.no_faktur = '$row1->no_faktur' AND a.kode_unit = '$row1->kode_supplier' ");
									
				if ($query2->num_rows() > 0) { //
					$hasilrow = $query2->row();
					$totalpajaknya	= $hasilrow->totnya;
					
					// 04 okt 2011, cek apakah suppliernya pkp atau bukan
					$query3	= $this->db->query(" SELECT pkp, tipe_pajak from tm_supplier
									WHERE kode_supplier = '$row1->kode_supplier' ");
					if ($query3->num_rows() > 0) { //
						$hasil3 = $query3->row();
						$pkpnya = $hasil3->pkp;
						$tipe_pajaknya = $hasil3->tipe_pajak;
						
						if ($pkpnya == 't' && $totalpajaknya == 0) {
							// update semua data pajaknya di detail SJ 
							if ($row1->is_makloon == 'f')
								$query4	= $this->db->query(" SELECT b.id, b.total FROM tm_pembelian a, tm_pembelian_detail b, 
										tm_pembelian_nofaktur c, tm_pembelian_nofaktur_sj d
										WHERE a.id = b.id_pembelian AND a.no_sj = d.no_sj AND c.id = d.id_pembelian_nofaktur
										AND a.kode_supplier = c.kode_supplier AND c.no_faktur = '$row1->no_faktur'
										AND a.kode_supplier = '$row1->kode_supplier' AND a.status_aktif = 't' ");
							else
								$query4	= $this->db->query(" SELECT b.id, b.biaya FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b, 
										tm_faktur_makloon c, tm_faktur_makloon_sj d
										WHERE a.id = b.id_sj_hasil_makloon AND a.no_sj = d.no_sj_masuk AND c.id = d.id_faktur_makloon
										AND a.kode_unit = c.kode_unit AND c.no_faktur = '$row1->no_faktur'
										AND a.kode_unit = '$row1->kode_supplier' ");
							
							if ($query4->num_rows() > 0) { //
								$hasil4 = $query4->result();
								foreach ($hasil4 as $row4) {
									if ($row1->is_makloon == 'f')
										$totalsj = $row4->total;
									else
										$totalsj = $row4->biaya;
										
									$idsj = $row4->id;
									if ($tipe_pajaknya == 'I') {
										$pajaksj = $totalsj/11;
										$totskrg = $totalsj;
									}
									else {
										$pajaksj = $totalsj * 0.1;
										$totskrg = $totalsj + $pajaksj;
									}
									
									if ($row1->is_makloon == 'f')
										$this->db->query(" UPDATE tm_pembelian_detail SET pajak = '$pajaksj', total = '$totskrg' 
												WHERE id = '$idsj' ");
									else
										$this->db->query(" UPDATE tm_sj_hasil_makloon_detail SET pajak = '$pajaksj', total = '$totskrg' 
												WHERE id = '$idsj' ");
									
								} // end foreach
							} // end if
							
							//dan header pembelian
							if ($row1->is_makloon == 'f')
								$query4	= $this->db->query(" SELECT a.id, a.total FROM tm_pembelian a,  
										tm_pembelian_nofaktur c, tm_pembelian_nofaktur_sj d
										WHERE a.no_sj = d.no_sj AND c.id = d.id_pembelian_nofaktur
										AND a.kode_supplier = c.kode_supplier AND c.no_faktur = '$row1->no_faktur'
										AND a.kode_supplier = '$row1->kode_supplier' AND a.status_aktif = 't' ");
							else
								$query4	= $this->db->query(" SELECT a.id, a.biaya FROM tm_sj_hasil_makloon a,  
										tm_faktur_makloon c, tm_faktur_makloon_sj d
										WHERE a.no_sj = d.no_sj_masuk AND c.id = d.id_faktur_makloon
										AND a.kode_unit = c.kode_unit AND c.no_faktur = '$row1->no_faktur'
										AND a.kode_unit = '$row1->kode_supplier' ");
							
							if ($query4->num_rows() > 0) { //
								$hasil4 = $query4->result();
								foreach ($hasil4 as $row4) {
									$id_pembeliannya = $row4->id;
									
									if ($row1->is_makloon == 'f')
										$totalsj2 = $row4->total;
									else
										$totalsj2 = $row4->biaya;
									
									$idsj2 = $row4->id;
									if ($tipe_pajaknya == 'I') {
										$pajaksj2 = $totalsj2/11;
										$totskrg2 = $totalsj2;
										if ($row1->is_makloon == 'f')
											$this->db->query(" UPDATE tm_pembelian SET tipe_pajak = 'I', total_pajak = '$pajaksj2' 
														where id = '$id_pembeliannya' ");
										else
											$this->db->query(" UPDATE tm_sj_hasil_makloon SET total_pajak = '$pajaksj2' 
														where id = '$id_pembeliannya' ");
									}
									else {
										$pajaksj2 = $totalsj2 * 0.1;
										$totskrg2 = $totalsj2 + $pajaksj2;
										if ($row1->is_makloon == 'f')
											$this->db->query(" UPDATE tm_pembelian SET tipe_pajak = 'E', total = '$totskrg2',
													total_pajak = '$pajaksj2' where id = '$id_pembeliannya' ");
										else
											$this->db->query(" UPDATE tm_sj_hasil_makloon SET total = '$totskrg2',
													total_pajak = '$pajaksj2' where id = '$id_pembeliannya' ");
									}
								} // end foreach
							} // end if
							
							if ($row1->is_makloon == 'f')
								$query4	= $this->db->query(" SELECT SUM(a.total_pajak) as totnya, SUM(a.total) as totsj FROM tm_pembelian a, tm_pembelian_nofaktur b, tm_pembelian_nofaktur_sj c 
										WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
										AND b.id = c.id_pembelian_nofaktur AND a.status_aktif = 't'
										AND b.no_faktur = '$row1->no_faktur' AND a.kode_supplier = '$row1->kode_supplier' ");
							else
								$query4	= $this->db->query(" SELECT SUM(a.total_pajak) as totnya, SUM(a.total) as totsj 
										FROM tm_sj_hasil_makloon a, tm_faktur_makloon b, tm_faktur_makloon_sj c 
										WHERE a.kode_unit = b.kode_unit AND a.no_sj = c.no_sj_masuk 
										AND b.id = c.id_faktur_makloon
										AND b.no_faktur = '$row1->no_faktur' AND a.kode_unit = '$row1->kode_supplier' ");
							
							if ($query4->num_rows() > 0) { //
								$hasilrow = $query4->row();
								$totalpajaknya	= $hasilrow->totnya;
								$totalsjnya = $hasilrow->totsj;
								
								if ($row1->is_makloon == 'f')
									$this->db->query(" UPDATE tm_pembelian_nofaktur SET jumlah = '$totalsjnya' where id = '$row1->id' ");
								else
									$this->db->query(" UPDATE tm_faktur_makloon SET jumlah = '$totalsjnya' where id = '$row1->id' ");
							} // sampe sini
						} // end if pkpnya == 't' && totalpajaknya == 0
						else if ($pkpnya == 't' && $totalpajaknya != 0)
							$totalsjnya = $row1->jumlah;
					}
					
				}
				else
					$totalpajaknya = '0';
				
				$dpp = $row1->jumlah/1.1;
				if ($pkpnya == 't') {
					$data_fb[] = array(		'id'=> $row1->id,	
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'tgl_update'=> $row1->tgl_update,
											'is_makloon'=> $row1->is_makloon,
											'dpp'=> $dpp,
											//'totalnya'=> $row1->jumlah,
											'totalnya'=> $totalsjnya,
											'pajaknya'=> $totalpajaknya
											);
				}
				else
					$data_fb = '';
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  // ini ga dipake 2 des 2011
  /*function get_faktur_pembeliantanpalimit($jnsaction, $supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' order by kode_supplier, tgl_faktur DESC ");
			}
			else { // utk edit				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' OR no_faktur_pajak <> '' 
								order by kode_supplier, tgl_faktur DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk add				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND kode_supplier = '$supplier' 
									order by kode_supplier,tgl_faktur DESC ");
			}
			else { // edit				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') 
				AND kode_supplier = '$supplier' order by kode_supplier,tgl_faktur DESC ");
			}
		}
	}
	else {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk add						
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ");
			}
			else {				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '') AND 
				UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk add						
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 'f' 
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ");
			}
			else {				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE (status_faktur_pajak = 'f' OR no_faktur_pajak <> '' )
				AND kode_supplier = '$supplier' AND UPPER(no_faktur) like UPPER('%$cari%') order by kode_supplier, tgl_faktur DESC ");
			}
								
		}
	}
    
    return $query->result();  
  } */
        
  function get_pajak($id_pajak){
	$query	= $this->db->query(" SELECT * FROM tm_pembelian_pajak where id='$id_pajak' ");    
	
	$data_pajak = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT no_faktur FROM tm_pembelian_pajak_detail 
									WHERE id_pembelian_pajak = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_faktur = ''; $is_makloon = '';
				foreach ($hasil2 as $row2) {
					$no_faktur .= $row2->no_faktur.", ";
					// cek apakah fakturnya itu termasuk faktur biasa atau faktur makloon
					$query3	= $this->db->query(" SELECT is_makloon FROM tm_pembelian_nofaktur WHERE no_faktur = '$row2->no_faktur'
									AND kode_supplier = '$row1->kode_supplier' ");
					if ($query3->num_rows() > 0){
						$hasil3 = $query3->row();
						$is_makloon .= $hasil3->is_makloon.", ";
					}
					else {
						$query3	= $this->db->query(" SELECT is_makloon FROM tm_faktur_makloon WHERE no_faktur = '$row2->no_faktur'
									AND kode_unit = '$row1->kode_supplier' ");
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->row();
							$is_makloon .= $hasil3->is_makloon.", ";
						}
					}
				}
				
				$pisah1 = explode("-", $row1->tgl_faktur_pajak);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_faktur_pajak = $tgl1."-".$bln1."-".$thn1;						
			
				$data_pajak[] = array(		'id'=> $row1->id,	
											'no_faktur_pajak'=> $row1->no_faktur_pajak,
											'tgl_faktur_pajak'=> $tgl_faktur_pajak,
											'kode_supplier'=> $row1->kode_supplier,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'dpp'=> $row1->dpp,
											'no_faktur'=> $no_faktur,
											'is_makloon'=> $is_makloon
											);
			} // endforeach header
	}
    return $data_pajak; 
  }
  
  function get_supplier(){
	//$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori= '1' ORDER BY kode_supplier ");    
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");
    return $query->result();  
  }
  
  function get_detail_supplier($kode_sup){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kode_supplier = '$kode_sup' ");    
    return $query->result();  
  }  

}
