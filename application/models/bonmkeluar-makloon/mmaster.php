<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $cari) {	  
	if ($cari == "all") {
			$this->db->select(" distinct a.* FROM tm_apply_stok_proses_quilting a, tm_apply_stok_proses_quilting_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' ORDER BY a.id_gudang ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
	else {
			$this->db->select(" distinct a.* FROM tm_apply_stok_proses_quilting a, tm_apply_stok_proses_quilting_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't'  
			AND UPPER(a.no_bonm) like UPPER('%$cari%') ORDER BY a.id_gudang ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_quilting_detail WHERE id_apply_stok = '$row1->id' 
				AND status_stok = 't' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'status_sj'=> $row2->status_sj
											);
					}
				}
				else {
					$detail_fb = '';
				}
				
				//ambil tgl pb
				$query3	= $this->db->query(" SELECT tgl FROM tm_pb_quilting WHERE no_pb_quilting = '$row1->no_pb_quilting' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$tgl_pb	= $hasilrow->tgl;
				 }
				 else
					$tgl_pb = '';
				 
				 // ambil data nama gudang
					$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				 
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_pb_quilting'=> $row1->no_pb_quilting,
											'tgl_pb'=> $tgl_pb,	
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'status_stok'=> $row1->status_stok,
											'status_sj'=> $row1->status_sj,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($cari){
	if ($cari == "all") {
			$query	= $this->db->query(" SELECT distinct a.* FROM tm_apply_stok_proses_quilting a, tm_apply_stok_proses_quilting_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' ");
	}
	else {
			$query	= $this->db->query(" SELECT distinct a.* FROM tm_apply_stok_proses_quilting a, tm_apply_stok_proses_quilting_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't'  
			AND UPPER(a.no_bonm) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_detail_bonm(){
    $headernya = array();    
    $detail_brg = array();    
    
    $query	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_quilting WHERE status_stok = 'f' 
    ORDER BY tgl_bonm, id_gudang, no_pb_quilting ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach($hasil as $row1) {
			$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_quilting_detail WHERE status_stok = 'f' 
						AND id_apply_stok = '$row1->id' ");  
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
						foreach ($hasil2 as $row2) {
							$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE 
							kode_brg = '$row2->kode_brg' ");
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							
							$detail_brg[] = array(		
											'id'=> $row2->id,
											'kode_brg'=> $row2->kode_brg,
											'nama'=> $nama_brg,
											'qty'=> $row2->qty
									);
						}
					}
					else
						$detail_brg = '';
			//=====================================================================
							
				// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				}
				
				//ambil tgl pb-quilting
				$query3	= $this->db->query(" SELECT tgl FROM tm_pb_quilting WHERE no_pb_quilting = '$row1->no_pb_quilting' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$tgl_pb	= $hasilrow->tgl;
				 }
				 else
					$tgl_pb = '';
				
				$headernya[] = array(		
											'id'=> $row1->id,
											'no_pb_quilting'=> $row1->no_pb_quilting,
											'tgl_pb'=> $tgl_pb,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'id_gudang'=> $row1->id_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'detail_brg'=> $detail_brg
									);
				$detail_brg = array();
		}
		
	}
	else {
			$headernya = '';
	}
	return $headernya;
  }

}
