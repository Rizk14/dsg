<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function getAll($num, $offset, $cari)
  {
	if ($cari == "all") {
		$this->db->select(" b.id, b.nama_brg, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tm_barang_wip b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1'
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.id ", false)->limit($num,$offset);
	}
	else {
		$this->db->select(" b.id, b.nama_brg, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tm_barang_wip b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1' AND (UPPER(b.id) like UPPER('%$cari%') 
				OR UPPER(b.nama_brg) like UPPER('%$cari%'))
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.id ", false)->limit($num,$offset);		
    }
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari){
	if ($cari == "all") {
		$this->db->select(" b.id, b.nama_brg, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tm_barang_wip b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1'
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.id ", false);
	}
	else {
		$this->db->select(" b.id, b.nama_brg, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tm_barang_wip b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1' AND (UPPER(b.id) like UPPER('%$cari%') 
				OR UPPER(b.nama_brg) like UPPER('%$cari%'))
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.id ", false);
	}
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get($id){
    $query = $this->db->query(" SELECT * FROM tm_barang_wip WHERE id = '$id' ");
    return $query->result();
  }
        
  function cek_data($kode){
    $this->db->select("id from tm_barang_wip WHERE i_product = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit){  
    $tgl = date("Y-m-d");
    if ($goedit == '') {
		$data = array(
		  'i_product_base'=>$kode,
		  'e_product_basename'=>$nama,
		  'd_entry'=>$tgl,
		  'd_update'=>$tgl
		);
		$this->db->insert('tr_product_base',$data); 
		
		$id_barang_wip = $kode."00";
		$data2 = array(
		  'id'=>$id_barang_wip,
		  'i_product'=>$kode,
		  'nama_brg'=>$nama,
		  'n_active'=>'1',
		  'd_entry'=>$tgl,
		  'd_update'=>$tgl,
		  'id_kel_brg_jadi'=>$kel_brg_jadi
		);
		$this->db->insert('tm_barang_wip',$data2); 
	}

	else {			
		$data = array(
				  'e_product_basename'=>$nama
				);
		
		$this->db->where('i_product_base',$kode);
		$this->db->update('tr_product_base',$data);  
		
		$data = array(
				  'nama_brg'=>$nama,
				  'id_kel_brg_jadi'=>$kel_brg_jadi
				);
		$id_barang_wip = $kode."00";
		$this->db->where('id',$id_barang_wip);
		$this->db->update('tm_barang_wip',$data);  
		
		// 11-12-14 edit kode brg di tabel master, sekaligus update kode brg di tabel2 lain
		// updatenya ke tabel: tr_product_base OK, tm_barang_wip OK, tm_bonmkeluarcutting_detail, tm_bonmmasukcutting_detail,
		// tm_hpp_gudang_jadi, tm_hpp_packing, tm_hpp_pengadaan, tm_hpp_wip, tm_material_brg_jadi,
		// tm_sjkeluarwip_detail, tm_sjmasukbhnbakupic_detail, tm_sjmasukwip_detail, tm_stok_hasil_cutting, tm_stok_hasil_jahit,
		// tm_stok_unit_jahit, tt_stok_opname_hasil_jahit_detail, tt_stok_opname_unit_jahit_detail
		
		$this->db->query(" UPDATE tr_product_base SET i_product_base = '$kode' WHERE i_product_base='$kodeedit' ");
		$kode_motif_lama = $kodeedit."00";
		$this->db->query(" UPDATE tm_barang_wip SET id = '$id_barang_wip' WHERE id='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_bonmkeluarcutting_detail SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_bonmmasukcutting_detail SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_hpp_gudang_jadi SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_hpp_packing SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_hpp_pengadaan SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		
		$this->db->query(" UPDATE tm_hpp_wip SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_material_brg_jadi SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_sjkeluarwip_detail SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_sjmasukbhnbakupic_detail SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_sjmasukwip_detail SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_stok_hasil_cutting SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tm_stok_hasil_jahit SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		
		$this->db->query(" UPDATE tm_stok_unit_jahit SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
		$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET id_barang_wip = '$id_barang_wip' WHERE id_barang_wip='$kode_motif_lama' ");
	}
		
  }
  
  function delete($kode){    
	  $kodebase = substr($kode, 0, 7);
    $this->db->delete('tm_barang_wip', array('id' => $kode));
    $this->db->delete('tr_product_base', array('i_product_base' => $kodebase));
  }
  
  // 28-01-2014 ----------------------------------------------------
  function getAllwarnabrgjaditanpalimit($cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.id) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%')) ";
			
		$sqlnya = $this->db->query(" SELECT distinct a.id_barang_wip, b.nama_brg 
							FROM tm_warna_brg_jadi a, tm_barang_wip b
							WHERE a.id_barang_wip = b.id ".$filter."
							ORDER BY a.id_barang_wip ");
		return $sqlnya->result();  
	}
  
  function getAllwarnabrgjadi($limit,$offset, $cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.id) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%')) ";
			
	  $sqlnya	= $this->db->query(" SELECT distinct a.id_barang_wip, b.nama_brg 
							FROM tm_warna_brg_jadi a, tm_barang_wip b
							WHERE a.id_barang_wip = b.id "
							.$filter." ORDER BY a.id_barang_wip LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				/*$queryxx = $this->db->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
										WHERE a.i_color = b.i_color AND a.id = '".$rownya->id."' "); */
				$queryxx = $this->db->query(" SELECT a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
										WHERE a.kode_warna = b.kode AND a.id_barang_wip = '".$rownya->id_barang_wip."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'kode_warna'=> $rowxx->kode_warna,
												'nama_warna'=> $rowxx->nama
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'id'=> $rownya->id_barang_wip,
										'nama_brg'=> $rownya->nama_brg,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	
	function get_warna(){
		$query = $this->db->query(" SELECT * FROM tm_warna ORDER BY kode");
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	
	function getbarangjaditanpalimit($cari){
		if ($cari == "all") {
			$sql = " select * FROM tm_barang_wip ";
			$query = $this->db->query($sql);
			return $query->result();  
		}
		else {
			$sql = " SELECT * FROM tm_barang_wip WHERE UPPER(id) like UPPER('%".$this->db->escape_str($cari)."%') 
						OR UPPER(nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') ";
			$query = $this->db->query($sql);
			return $query->result();  
		}
  }
  
  function getbarangjadi($num,$offset, $cari){    
    if ($cari == "all") {
		$sql = " * FROM tm_barang_wip ORDER BY id ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tm_barang_wip  ";		
		$sql.=" WHERE UPPER(id) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by id ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {									
			$data_bhn[] = array(		'kode_brg'=> $row1->id,	
										'nama_brg'=> $row1->nama_brg
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function savewarnabrgjadi($id_barang_wip,$kodewarna) {
		$tgl = date("Y-m-d H:i:s");
		
		$listwarna = explode(";", $kodewarna);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi 
									 WHERE id_barang_wip = '$id_barang_wip' AND kode_warna = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_warna	= $hasilrow->id;
						$new_id_warna = $id_warna+1;
					}
					else
						$new_id_warna = 1;
					
					$datanya	= array(
							 'id'=>$new_id_warna,
							 'id_barang_wip'=>$id_barang_wip,
							 'kode_warna'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_warna_brg_jadi',$datanya);
				} // end if
			}
		} // end foreach
	}
	
	function deletewarnabrgjadi($kode){
		$this->db->query(" DELETE FROM tm_warna_brg_jadi WHERE id_barang_wip = '$kode' ");
	}
	
	function updatewarnabrgjadi($id_barang_wip,$ada, $kodewarna) {
		$tgl = date("Y-m-d H:i:s");
		
		if ($ada == 0)
			$this->db->query(" DELETE FROM tm_warna_brg_jadi WHERE id_barang_wip = '$id_barang_wip' ");
		
		$listwarna = explode(";", $kodewarna);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi 
									 WHERE id_barang_wip = '$id_barang_wip' AND kode_warna = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$hasilsqlcek = $sqlcek->row();
					$query2	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_warna	= $hasilrow->id;
						$new_id_warna = $id_warna+1;
					}
					else
						$new_id_warna = 1;
					
					$datanya	= array(
							 'id'=>$new_id_warna,
							 'id_barang_wip'=>$id_barang_wip,
							 'kode_warna'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_warna_brg_jadi',$datanya);
				} // end if
				else { // jika udh ada, maka update aja (sepertinya ga akan masuk kesini, soalnya jika datanya udh pernah dipake di SJ masuk/keluar maka ga bisa diedit sama sekali
					$this->db->query(" UPDATE tm_warna_brg_jadi SET kode_warna = '$row1' WHERE id='$hasilsqlcek->id' ");
				}
			}
		} // end foreach
	}
  //-----------------------------------------------------------------
  
  // 04-02-2014
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_barang_wip ORDER BY id ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tm_barang_wip  ";		
		$sql.=" WHERE UPPER(id) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%') 
				order by id ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->id,	
										'nama_brg'=> $row1->nama_brg
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tm_barang_wip ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_barang_wip WHERE UPPER(id) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  // 05-02-2014
  function get_bahan($num, $offset, $cari, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't'  ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function savematerialbrgjadi($id_barang_wip,$kode_bhn_baku, $kode_warna) {
		$tgl = date("Y-m-d H:i:s");
		
		$listbhn = explode(";", $kode_bhn_baku);
		$listwarna = explode(";", $kode_warna);
		//foreach ($listbhn as $row1) {
		for ($i=0; $i<count($listbhn); $i++) {
			if (trim($listbhn[$i]!= '')) {
				//echo $listbhn[$i]." ".$listwarna[$i]."<br>";
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_material_brg_jadi 
									 WHERE id_barang_wip = '$id_barang_wip' AND kode_brg = '".$listbhn[$i]."' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_material_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_material	= $hasilrow->id;
						$new_id_material = $id_material+1;
					}
					else
						$new_id_material = 1;
					
					$datanya	= array(
							 'id'=>$new_id_material,
							 'id_barang_wip'=>$id_barang_wip,
							 'kode_brg'=>$listbhn[$i],
							 'kode_warna'=>$listwarna[$i],
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_material_brg_jadi',$datanya);
				} // end if
			}
		} // end foreach
	}
	
   function getAllmaterialbrgjaditanpalimit($cari) {
	   $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.id) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%')) ";
			
		$sqlnya = $this->db->query(" SELECT distinct a.id_barang_wip, b.nama_brg 
							FROM tm_material_brg_jadi a, tm_barang_wip b
							WHERE a.id_barang_wip = b.id ".$filter."
							ORDER BY a.id_barang_wip ");
		return $sqlnya->result();  
	}
  
  function getAllmaterialbrgjadi($limit,$offset, $cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.id) LIKE UPPER('%$cari%') OR UPPER(b.nama_brg) LIKE UPPER('%$cari%')) ";
			
	  $sqlnya	= $this->db->query(" SELECT distinct a.id_barang_wip, b.nama_brg 
							FROM tm_material_brg_jadi a, tm_barang_wip b
							WHERE a.id_barang_wip = b.id "
							.$filter." ORDER BY a.id_barang_wip LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailbrg = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $this->db->query(" SELECT kode_brg, kode_warna FROM tm_material_brg_jadi WHERE id_barang_wip = '".$rownya->id_barang_wip."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						// bhn baku/quilting
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
						if ($query3->num_rows() > 0){
							$quilting = 'f';
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$quilting = 't';
							$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
							if ($query31->num_rows() > 0){
								$quilting = 't';
								$hasilrow31 = $query31->row();
								$nama_brg	= $hasilrow31->nama_brg;
								$satuan	= $hasilrow31->nama_satuan;
							}
							else {
								$nama_brg = '';
								$satuan = '';
								$quilting = 'f';
							}
						}
						
						// warna
						$query3	= $this->db->query(" SELECT nama FROM tm_warna WHERE kode = '$rowxx->kode_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_warna	= $hasilrow->nama;
						}
						
						$detailbrg[] = array(	'kode_brg'=> $rowxx->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'quilting'=> $quilting,
												'kode_warna'=> $rowxx->kode_warna,
												'nama_warna'=> $nama_warna,
												);
					}
				}
				else
					$detailbrg = '';
					
				$outputdata[] = array(	'id'=> $rownya->id_barang_wip,
										'nama_brg'=> $rownya->nama_brg,
										'detailbrg'=> $detailbrg
								);
				
				$detailbrg = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	
	function deletematerialbrgjadi($kode){
		$this->db->query(" DELETE FROM tm_material_brg_jadi WHERE id_barang_wip = '$kode' ");
	}
	
	function updatedatamaterialbrgjadi($id_barang_wip,$ada,$kode_bhn_baku, $kode_warna) {
		$tgl = date("Y-m-d H:i:s");
		
		if ($ada == 0)
			$this->db->query(" DELETE FROM tm_material_brg_jadi WHERE id_barang_wip = '$id_barang_wip' ");
		
		$listbhn = explode(";", $kode_bhn_baku);
		$listwarna = explode(";", $kode_warna);
		for ($i=0; $i<count($listbhn); $i++) {
			if (trim($listbhn[$i]!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_material_brg_jadi 
									 WHERE id_barang_wip = '$id_barang_wip' AND kode_brg = '".$listbhn[$i]."' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_material_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_material	= $hasilrow->id;
						$new_id_material = $id_material+1;
					}
					else
						$new_id_material = 1;
					
					$datanya	= array(
							 'id'=>$new_id_material,
							 'id_barang_wip'=>$id_barang_wip,
							 'kode_brg'=>$listbhn[$i],
							 'kode_warna'=>$listwarna[$i],
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_material_brg_jadi',$datanya);
				} // end if
			}
		} // end foreach
	}
	
	// 23-05-2014
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  /*function get_stok_unit_jahit($unit_jahit) {
	$sql = " SELECT a.id_barang_wip, b.nama_brg as nama_brg_jadi 
			FROM tm_stok_unit_jahit a, tm_barang_wip b WHERE a.id_barang_wip=b.id 
			AND a.kode_unit='$unit_jahit' ORDER BY a.id_barang_wip ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT harga FROM tm_hpp_wip WHERE id_barang_wip = '$row1->id_barang_wip'
									AND kode_unit = '$unit_jahit' ");
			if ($query3->num_rows() == 0){
				$harga	= '';
			}
			else {
				$hasilrow = $query3->row();
				$harga	= $hasilrow->harga;
			}
				$data_harga[] = array(			
								'id_barang_wip'=> $row1->id_barang_wip,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  } */
  
  function cek_data_hpp($id_barang_wip,$bulan,$tahun){
    $this->db->select("id from tm_hpp_wip WHERE id_barang_wip = '$id_barang_wip' and bulan='$bulan' AND and tahun='$tahun", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savehpp($id_barang_wip,$kode_barang_wip, $harga,$bulan,$tahun){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
    
    if ($harga != 0) {
		$data = array(
		  'id_barang_wip'=>$id_barang_wip,
		 ' kode_brg_jadi'=>$kode_barang_wip,
		 'bulan'=>$bulan,
		 'tahun'=>$tahun,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_hpp_wip',$data); 
	}
  }
  
  function deletehpp($id){    
    $this->db->delete('tm_hpp_wip', array('id' => $id));
  }
  
  function getAllhpp($num, $offset, $cari)
  {
	   $filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	 	  $this->db->select(" a.*, c.nama_brg FROM tm_hpp_wip a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter." ORDER BY a.id_barang_wip ", false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllhpptanpalimit($cari){
	   $filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.nama_brg FROM tm_hpp_wip a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter, false);
			
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get_hpp($idharga){
	$query = $this->db->query(" SELECT * FROM tm_hpp_wip WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			// ambil data nama barang jadi
			$query3	= $this->db->query(" SELECT nama_brg ,kode_brg FROM tm_barang_wip WHERE id = '$row1->id_barang_wip' ");
			$hasilrow = $query3->row();
			$nama_brg_jadi	= $hasilrow->nama_brg;
			$kode_brg_jadi	= $hasilrow->kode_brg;
			$dataharga[] = array(			'id'=> $row1->id,	
											'id_barang_wip'=> $row1->id_barang_wip,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'kode_brg_jadi'=> $kode_brg_jadi,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }
  
  // 30-05-2014
  function get_hpp_brgjadi() {
	$sql = " SELECT a.id_barang_wip, b.nama_brg as nama_brg_jadi, a.harga 
			FROM tm_hpp_wip a, tm_barang_wip b WHERE a.id_barang_wip=b.id 
			ORDER BY a.id_barang_wip ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$data_harga[] = array(			
								'id_barang_wip'=> $row1->id_barang_wip,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $row1->harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
  
  // 12-09-2014
  function getAllkelbrgjadi(){
	$query = $this->db->query(" SELECT * FROM tm_kel_brg_jadi ORDER BY kode ");    
    return $query->result();
  }
  
  function get_kel_brg_jadi($id){
	$query = $this->db->query(" SELECT * FROM tm_kel_brg_jadi WHERE id='$id' ");
    //$query = $this->db->getwhere('tm_kelompok_barang',array('kode'=>$id));
    return $query->result();
  }
  
  function save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $data = array(
      'kode'=>$kode,
      'nama'=>$nama,
      'keterangan'=>$ket,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_kel_brg_jadi',$data); }
	else {
		
		$data = array(
		  'kode'=>$kode,
		  'nama'=>$nama,
		  'keterangan'=>$ket,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$kodeedit);
		$this->db->update('tm_kel_brg_jadi',$data);  
	}
		
  }
  
  function delete_kel_brg_jadi($id){    
    $this->db->delete('tm_kel_brg_jadi', array('id' => $id));
  }
  
  function cek_data_kel_brg_jadi($kode){
    $this->db->select("* FROM tm_kel_brg_jadi WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 26-09-2014
  function get_hpp_brgjadi_pengadaan() {
	$sql = " SELECT a.id_barang_wip, b.nama_brg as nama_brg_jadi, a.harga 
			FROM tm_hpp_pengadaan a, tm_barang_wip b WHERE a.id_barang_wip=b.id 
			ORDER BY a.id_barang_wip ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$data_harga[] = array(			
								'id_barang_wip'=> $row1->id_barang_wip,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $row1->harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
  
  function getAllhpppengadaan($num, $offset, $cari)
  {
	$filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	   $this->db->select(" a.*, c.nama_brg FROM tm_hpp_pengadaan a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter." ORDER BY a.id_barang_wip ", false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllhpppengadaantanpalimit($cari){
	  $filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.nama_brg FROM tm_hpp_pengadaan a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter, false);
			
    $query = $this->db->get();
    return $query->result();  
  }
  
  function cek_data_hpp_pengadaan($id_barang_wip,$bulan,$tahun){
    $this->db->select("id from tm_hpp_pengadaan WHERE id_barang_wip = '$id_barang_wip' AND bulan='$bulan' AND tahun='$tahun' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_hpp_pengadaan($idharga){
	$query = $this->db->query(" SELECT * FROM tm_hpp_pengadaan WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			// ambil data nama barang jadi
			$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang_wip WHERE id = '$row1->id_barang_wip' ");
			$hasilrow = $query3->row();
			$nama_brg_jadi	= $hasilrow->nama_brg;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'id_barang_wip'=> $row1->id_barang_wip,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }
  
  function savehpppengadaan($kode_barang_wip,$id_barang_wip, $harga,$bulan,$tahun){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
    
    if ($harga != 0) {
		$data = array(
		  'id_barang_wip'=>$id_barang_wip,
		  'kode_brg_jadi'=>$kode_barang_wip,
		   'bulan'=>$bulan,
		  'tahun'=>$tahun,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_hpp_pengadaan',$data); 
	}
  }
  
  function deletehpppengadaan($id){    
    $this->db->delete('tm_hpp_pengadaan', array('id' => $id));
  }
  
  // hpp packing
  function get_hpp_brgjadi_packing() {
	$sql = " SELECT a.id_barang_wip, b.nama_brg as nama_brg_jadi, a.harga 
			FROM tm_hpp_packing a, tm_barang_wip b WHERE a.id_barang_wip=b.id 
			ORDER BY a.id_barang_wip ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$data_harga[] = array(			
								'id_barang_wip'=> $row1->id_barang_wip,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $row1->harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
  
  function getAllhpppacking($num, $offset, $cari)
  {
	$filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.id_barang_wip) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.nama_brg FROM tm_hpp_packing a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter." ORDER BY a.id_barang_wip ", false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllhpppackingtanpalimit($cari){
	  $filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.id_barang_wip) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.nama_brg FROM tm_hpp_packing a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter, false);
			
    $query = $this->db->get();
    return $query->result();  
  }
  
  function cek_data_hpp_packing($id_barang_wip,$bulan,$tahun){
    $this->db->select("id from tm_hpp_packing WHERE id_barang_wip = '$id_barang_wip' AND bulan='$bulan' AND tahun='$tahun'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_hpp_packing($idharga){
	$query = $this->db->query(" SELECT * FROM tm_hpp_packing WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			// ambil data nama barang jadi
			$query3	= $this->db->query(" SELECT nama_brg,kode_brg FROM tm_barang_wip WHERE id = '$row1->id_barang_wip' ");
			$hasilrow = $query3->row();
			$nama_brg_jadi	= $hasilrow->nama_brg;
			$kode_brg_jadi	= $hasilrow->kode_brg;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'kode_brg_jadi'=>$kode_brg_jadi,
											'id_barang_wip'=> $row1->id_barang_wip,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }
  
  function savehpppacking($id_barang_wip,$kode_brg_jadi, $harga,$bulan,$tahun){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
    
    if ($harga != 0) {
		$data = array(
		  'id_barang_wip'=>$id_barang_wip,
		  'kode_brg_jadi'=>$kode_brg_jadi,
		   'bulan'=>$bulan,
		  'tahun'=>$tahun,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_hpp_packing',$data); 
	}
  }
  
  function deletehpppacking($id){    
    $this->db->delete('tm_hpp_packing', array('id' => $id));
  }
  
  // hpp gudang jadi
  function get_hpp_brgjadi_gudangjadi() {
	$sql = " SELECT a.id_barang_wip, b.nama_brg as nama_brg_jadi, a.harga 
			FROM tm_hpp_gudang_jadi a, tm_barang_wip b WHERE a.id_barang_wip=b.id 
			ORDER BY a.id_barang_wip ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$data_harga[] = array(			
								'id_barang_wip'=> $row1->id_barang_wip,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $row1->harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
  
  function getAllhppgudangjadi($num, $offset, $cari)
  {
	$filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.id_barang_wip) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.nama_brg FROM tm_hpp_gudang_jadi a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter." ORDER BY a.id_barang_wip ", false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllhppgudangjaditanpalimit($cari){
	  $filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.id_barang_wip) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.nama_brg FROM tm_hpp_gudang_jadi a, tm_barang_wip c 
			WHERE a.id_barang_wip = c.id ".$filter, false);
			
    $query = $this->db->get();
    return $query->result();  
  }
  
  function cek_data_hpp_gudangjadi($id_barang_wip){
    $this->db->select("id from tm_hpp_gudang_jadi WHERE id_barang_wip = '$id_barang_wip' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_hpp_gudangjadi($idharga){
	$query = $this->db->query(" SELECT * FROM tm_hpp_gudang_jadi WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			// ambil data nama barang jadi
			$query3	= $this->db->query(" SELECT nama_brg,kode_brg FROM tm_barang_wip WHERE id = '$row1->id_barang_wip' ");
			$hasilrow = $query3->row();
			$nama_brg_jadi	= $hasilrow->nama_brg;
			$kode_brg_jadi	= $hasilrow->kode_brg;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'id_barang_wip'=> $row1->id_barang_wip,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'kode_brg_jadi'=>$kode_brg_jadi,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }
  
  function savehppgudangjadi($id_barang_wip,$kode_brg_jadi, $harga){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
    
    if ($harga != 0) {
		$data = array(
		  'id_barang_wip'=>$id_barang_wip,
		  'kode_brg_jadi'=>$kode_brg_jadi,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_hpp_gudang_jadi',$data); 
	}
  }
  
  function deletehppgudangjadi($id){    
    $this->db->delete('tm_hpp_gudang_jadi', array('id' => $id));
  }

}
