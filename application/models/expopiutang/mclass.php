<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}
		
	function explistpiutang($i_kontrabon,$dkontrabonfirst,$dkontrabonlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT a.i_dt,
										a.i_dt_code,
										a.d_dt,
										b.i_nota,
										c.i_faktur_code AS i_nota_code,
										b.d_nota,
										trim(d.e_customer_name) AS e_customer_name,
										a.f_nota_sederhana
			
			FROM tm_dt a

				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
				INNER JOIN tr_customer d ON d.i_customer=b.i_customer

				WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$dkontrabonfirst' AND '$dkontrabonlast') AND a.f_nota_sederhana='$f_nota_sederhana'

				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, b.i_nota, c.i_faktur_code, b.d_nota, trim(d.e_customer_name), a.f_nota_sederhana

				ORDER BY a.i_dt_code ASC ");
		}else{
			return $db2->query(" SELECT a.i_dt, 
										a.i_dt_code, 
										a.d_dt, 
										b.i_nota, 
										c.i_faktur_code AS i_nota_code, 
										b.d_nota, 
										trim(d.e_customer_name) AS e_customer_name, 
										a.f_nota_sederhana 
			
			FROM tm_dt a

				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
				INNER JOIN tr_customer d ON d.i_customer=b.i_customer

				WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$dkontrabonfirst' AND '$dkontrabonlast') AND a.f_nota_sederhana='$f_nota_sederhana'

				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, b.i_nota, c.i_faktur_code, b.d_nota, trim(d.e_customer_name), a.f_nota_sederhana

				ORDER BY a.i_dt_code ASC ");			
		}
	}
	
	function totalkontrabon($nokontrabon,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.v_total_grand, a.v_grand_sisa AS piutang FROM tm_dt a
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
						
						WHERE a.f_dt_cancel='f' AND a.i_dt_code='$nokontrabon' AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY a.v_total_grand, a.v_grand_sisa ");		
	}
	
	function clistkontrabon1($limit,$offset,$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$f_nota_sederhana) {
$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa AS piutang FROM tm_dt a 
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndkontrabonfirst' AND '$ndkontrabonlast') AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
							
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function clistkontrabon2($limit,$offset,$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
		$query	= $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
						
						INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
						
						INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
						
						WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
						
						ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
		$query	= $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
						
						INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
						
						INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
						
						WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
						
						ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");			
		}	
					
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}	
	}

	function vtotalfaktur($limit,$offset,$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		/*
		if($f_nota_sederhana=='f') {
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota						
							WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana'
			
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}else{
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
							WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana'
							
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");			
		}
		*/

		if($ndkontrabonfirst!='' && $ndkontrabonlast!='') {
			if($ndkontrabonfirst!='0' && $ndkontrabonlast!='0') {
				$filter = " WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndkontrabonfirst' AND '$ndkontrabonlast') AND a.f_nota_sederhana='$f_nota_sederhana' ";
			}else{
				$filter = " WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana' ";
			}
		}else{
			$filter = " WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}
		
		if($f_nota_sederhana=='f') {
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
							
							".$filter."
			
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}else{
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
							
							".$filter."
							
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}

		if($query->num_rows()>0) {
			return $query->result();
		}
	}
				
	function clistkontrabonallpage1($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa AS piutang FROM tm_dt a 
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndkontrabonfirst' AND '$ndkontrabonlast') AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total_grand, a.v_grand_sisa ORDER BY a.i_dt ASC ");
	}

	function clistkontrabonallpage2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$f_nota_sederhana) {	
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
			
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							
				INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
							
				INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
							
				WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana'
							
				GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
							
				ORDER BY a.i_dt ASC ");
		}else{
			return $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
			
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							
				INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
							
				INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
							
				WHERE a.f_dt_cancel='f' AND a.i_dt='$ikontrabon' AND a.f_nota_sederhana='$f_nota_sederhana'
							
				GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt

				ORDER BY a.i_dt ASC ");			
		}		
	}
	
	function lkontrabon($fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana FROM tm_dt a 
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana ORDER BY a.i_dt ASC ");
	}	

	function lkontrabonperpages($limit,$offset,$fnotasederhana) {		
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana FROM tm_dt a 
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");

		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function flkontrabon($key,$fnotasederhana) {	
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana FROM tm_dt a 
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND a.i_dt_code='$key' AND a.f_nota_sederhana='$fnotasederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana ORDER BY a.i_dt ASC ");
	}
	 
}
?>
