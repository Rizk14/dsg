<?php
class Mgudang extends CI_Model{
  function __construct() { 

  parent::__construct();
  }
  
  // 14-02-2014
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
    
  function getbarangjadibygudangtanpalimit($gudang,$cari){
	  $sql = " SELECT b.kode_brg_jadi, b.stok, a.e_product_motifname FROM tr_product_motif a, tm_stok_hasil_jahit b WHERE a.i_product_motif = b.kode_brg_jadi
				AND b.id_gudang='$gudang' ";
		if ($cari != "all") {
			$sql.= " AND (UPPER(a.i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
						OR UPPER(a.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%')) ";
		}
			$query = $this->db->query($sql);
			return $query->result();  
  }
  
  function getbarangjadibygudang($num,$offset,$gudang, $cari){    
    $sql = " b.id, b.kode_brg_jadi, b.stok, a.e_product_motifname FROM tr_product_motif a, tm_stok_hasil_jahit b WHERE a.i_product_motif = b.kode_brg_jadi
				AND b.id_gudang='$gudang' ";
		if ($cari != "all") {
			$sql.= " AND (UPPER(a.i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
						OR UPPER(a.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%')) ";
		}
		$sql.= " ORDER BY a.i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);	
    
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {									
			$data_bhn[] = array(		'id'=> $row1->id,	
										'kode_brg'=> $row1->kode_brg_jadi,	
										'nama_brg'=> $row1->e_product_motifname,
										'stok'=> $row1->stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  // =======================================================================================
  
  // 17-02-2014
  function savekonversistok($no_bukti, $gudang1, $gudang2, $kode_brg_jadi1, $kode_brg_jadi2, $idwarna1, $idwarna2){  
    $tgl = date("Y-m-d H:i:s");
	
	// ----------- start update stok ------------------------------------
	//ambil stok terakhir tm_stok_hasil_jahit utk gudang1 dan kode_brg_jadi1
	$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$kode_brg_jadi1'
					AND id_gudang='$gudang1' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id_stok	= $hasilrow->id;
		$stok_lama	= $hasilrow->stok;
		
		$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '0', tgl_update_stok = '$tgl' WHERE id='$id_stok' ");
		
		//ambil stok terakhir tm_stok_hasil_jahit utk gudang2 dan kode_brg_jadi2
		$query31	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$kode_brg_jadi2'
						AND id_gudang='$gudang2' ");
		if ($query31->num_rows() > 0){
			$hasilrow31 = $query31->row();
			$id_stok2	= $hasilrow31->id;
			$stok_lama2	= $hasilrow31->stok;
			
			$stokkonversi = $stok_lama2+$stok_lama;
			$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$stokkonversi', tgl_update_stok = '$tgl' WHERE id='$id_stok2' ");
		}
		
		// insert di tm_konversi_stok_wip
			$data_header = array(
			  'no_bukti'=>$no_bukti,
			  'qty'=>$stok_lama,
			  'id_gudang1'=>$gudang1,
			  'kode_brg_jadi1'=>$kode_brg_jadi1,
			  'id_gudang2'=>$gudang2,
			  'kode_brg_jadi2'=>$kode_brg_jadi2,
			  'tgl_input'=>$tgl
			);
			$this->db->insert('tm_konversi_stok_wip',$data_header);
		
		// reset stok per warna
		$listwarna1 = explode(";", $idwarna1);
		for ($xx=0; $xx<count($listwarna1); $xx++) {
			if (trim($listwarna1[$xx]) != '') {
				//cek stok terakhir data lama tm_stok_hasil_jahit_warna, dan update stoknya
				$query32	= $this->db->query(" SELECT a.id, a.stok FROM tm_stok_hasil_jahit_warna a
							WHERE a.kode_warna = '".$listwarna1[$xx]."'
							AND a.id_stok_hasil_jahit='$id_stok' ");
				if ($query32->num_rows() > 0){
					$hasilrow32 = $query32->row();
					$id_stok_warna = $hasilrow32->id;
					$stok_warna_lama	= $hasilrow32->stok;
					
					$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '0', tgl_update_stok = '$tgl' WHERE id='$id_stok_warna' ");
					
					// tambahkan stok ke data baru
					//cek stok terakhir data baru tm_stok_hasil_jahit_warna, dan update stoknya
					$query32	= $this->db->query(" SELECT a.id, a.stok FROM tm_stok_hasil_jahit_warna a
							WHERE a.kode_warna = '".$listwarna1[$xx]."'
							AND a.id_stok_hasil_jahit='$id_stok2' ");
					if ($query32->num_rows() > 0){
						$hasilrow32 = $query32->row();
						$id_stok_warna2 = $hasilrow32->id;
						$stok_warna_lama2	= $hasilrow32->stok;
						
						$stokkonversi_warna = $stok_warna_lama2+$stok_warna_lama;
						$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$stokkonversi_warna', tgl_update_stok = '$tgl' WHERE id='$id_stok_warna2' ");
					}
					
					// insert di tm_konversi_stok_wip_warna
					$seqxx	= $this->db->query(" SELECT id FROM tm_konversi_stok_wip_warna ORDER BY id DESC LIMIT 1 ");
					if($seqxx->num_rows() > 0) {
						$hasilseqxx	= $seqxx->row();
						$id_detail	= $hasilseqxx->id+1;
					}else{
						$id_detail	= 1;
					}
							
					$data_detail = array(
					  'id'=>$id_detail,
					  'no_bukti'=>$no_bukti,
					  'kode_warna'=>$listwarna1[$xx],
					  'qty'=>$stok_warna_lama
					);
					$this->db->insert('tm_konversi_stok_wip_warna',$data_detail);
					
				} // end if
			} // end if trim
		} // end for
	}
	// ------------------------------------------------------------------
  }
  
  // 18-02-2014
  function get_konversistok($num, $offset, $gudang, $cari) {
	
	$sql = " a.no_bukti, a.qty, a.id_gudang1, a.kode_brg_jadi1, a.id_gudang2, a.kode_brg_jadi2, a.tgl_input FROM tm_konversi_stok_wip a
			LEFT JOIN tr_product_motif d ON (d.i_product_motif = a.kode_brg_jadi1 OR d.i_product_motif = a.kode_brg_jadi2) WHERE TRUE ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.i_product_motif) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%')) ";
	if ($gudang != '0')
		$sql.= " AND (a.id_gudang1 = '$gudang' OR a.id_gudang2 = '$gudang') ";
	$sql.= " ORDER BY a.no_bukti ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_konversi = array();
	$detailwarna = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				// detail warnanya
				$queryxx	= $this->db->query(" SELECT b.kode, b.nama, a.qty FROM tm_konversi_stok_wip_warna a, 
									tm_warna b
									WHERE a.kode_warna = b.kode 
									AND a.no_bukti = '$row1->no_bukti' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'kode_warna'=> $rowxx->kode,
													'nama_warna'=> $rowxx->nama,
													'qty'=> $rowxx->qty
												);
					}
				}
				else
					$detailwarna = '';
				// --------------------------
				
					// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row1->kode_brg_jadi1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi1	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi1	= '';
						}
						
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row1->kode_brg_jadi2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi2	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi2	= '';
						}
					
					// nama gudang
					$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang1' ");
					$hasilrow = $query3->row();
					$kode_gudang1	= $hasilrow->kode_gudang;
					$nama_gudang1	= $hasilrow->nama;
					$nama_lokasi1	= $hasilrow->nama_lokasi;
					
					// nama gudang2
					$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang2' ");
					$hasilrow = $query3->row();
					$kode_gudang2	= $hasilrow->kode_gudang;
					$nama_gudang2	= $hasilrow->nama;
					$nama_lokasi2	= $hasilrow->nama_lokasi;
								
				$data_stok[] = array(		'kode_brg_jadi1'=> $row1->kode_brg_jadi1,	
											'nama_brg_jadi1'=> $nama_brg_jadi1,
											'kode_brg_jadi2'=> $row1->kode_brg_jadi2,	
											'nama_brg_jadi2'=> $nama_brg_jadi2,
											'qty'=> $row1->qty,
											'tgl_input'=> $row1->tgl_input,
											'kode_gudang1'=> $kode_gudang1,	
											'nama_gudang1'=> $nama_gudang1,
											'nama_lokasi1'=> $nama_lokasi1,
											'kode_gudang2'=> $kode_gudang2,	
											'nama_gudang2'=> $nama_gudang2,
											'nama_lokasi2'=> $nama_lokasi2,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_konversistoktanpalimit($gudang, $cari){
	$sql = " SELECT a.no_bukti, a.qty, a.id_gudang1, a.kode_brg_jadi1, a.id_gudang2, a.kode_brg_jadi2, a.tgl_input FROM tm_konversi_stok_wip a
			LEFT JOIN tr_product_motif d ON (d.i_product_motif = a.kode_brg_jadi1 OR d.i_product_motif = a.kode_brg_jadi2) WHERE TRUE ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.i_product_motif) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%')) ";
	if ($gudang != '0')
		$sql.= " AND (a.id_gudang1 = '$gudang' OR a.id_gudang2 = '$gudang') ";
	$sql.= " ORDER BY a.no_bukti ";
	  
		$query	= $this->db->query($sql);
    
    return $query->result();  
  }
  
}
