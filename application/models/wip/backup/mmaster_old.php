<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAllsjmasuk($num, $offset, $cari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";

		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit_jahit = '$kode_unit_jahit' ";
		if ($kode_unit_packing != "0")
			$pencarian.= " AND a.kode_unit_packing = '$kode_unit_packing' ";

		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$this->db->select(" distinct a.* FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tr_product_motif c 
						WHERE a.id = b.id_sjmasukwip AND b.kode_brg_jadi = c.i_product_motif ".$pencarian, false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(b.e_product_motifname) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_sjmasukwip_detail a, tr_product_motif b  
								WHERE a.kode_brg_jadi=b.i_product_motif AND a.id_sjmasukwip = '$row1->id' ".$pencarian2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname as nama_brg FROM tr_product_motif 
										 WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$nama_brg = '';
						}
						
						// 30-01-2014, detail qty warna -------------
						$strwarna = "";
						// 01-03-2014, skrg ga baca dari tabel warna_brg_jadi, tapi tm_warna
					/*	$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjmasukwip_detail_warna a, tm_warna_brg_jadi b, tm_warna c
									WHERE a.id_warna_brg_jadi = b.id AND b.kode_warna = c.kode
									AND a.id_sjmasukwip_detail = '$row2->id' ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg_jadi.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						} */
						$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjmasukwip_detail_warna a, tm_warna c
									WHERE a.kode_warna = c.kode
									AND a.id_sjmasukwip_detail = '$row2->id' ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg_jadi.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						}
						if ($strwarna == "")
							$strwarna = $row2->ket_qty_warna;
						//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Masuk bagus hasil jahit";
				else if ($row1->jenis_masuk == '2')
					$nama_jenis = "Lain-lain (Perbaikan dari unit jahit)";
				else if ($row1->jenis_masuk == '3')
					$nama_jenis = "Lain-lain (Retur dari unit packing)";
				else if ($row1->jenis_masuk == '4')
					$nama_jenis = "Lain-lain (Retur dari gudang jadi)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_masuk'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'keterangan'=> $row1->keterangan,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function getAllsjmasuktanpalimit($cari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit_jahit = '$kode_unit_jahit' ";
		if ($kode_unit_packing != "0")
			$pencarian.= " AND a.kode_unit_packing = '$kode_unit_packing' ";

		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tr_product_motif c 
						WHERE a.id = b.id_sjmasukwip AND b.kode_brg_jadi = c.i_product_motif ".$pencarian);

    return $query->result();  
  }
  
  //
  function savesjmasuk($id_sj, $id_gudang, $kode_unit_jahit, $kode_unit_packing, $jenis_masuk, 
					$kode, $nama, $temp_qty, $kode_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
    // cek apa udah ada datanya blm dgn no bon M tadi. 05-03-2014: dikomen
   /* $this->db->select("id from tm_sjmasukwip WHERE no_sj = '".$this->db->escape_str($no_sj)."' AND tgl_sj = '$tgl_sj' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_sjmasukwip
			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'jenis_masuk'=>$jenis,
			  'id_gudang'=>$id_gudang,
			  'kode_unit_jahit'=>$kode_unit_jahit,
			  'kode_unit_packing'=>$kode_unit_packing,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_sjmasukwip',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_sjmasukwip
			$query2	= $this->db->query(" SELECT id FROM tm_sjmasukwip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			*/
			
			// 30-01-2014, insert ke tabel detail ----------------------------------
			//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
													
					$qtytotal+= $qty_warna[$xx];
				} // end for
			// ---------------------------------------------------------------------
				
				// ======== update stoknya! =============
				//$nama_tabel_stok = "tm_stok_hasil_jahit";
				
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$kode'
							AND id_gudang='$id_gudang' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode,
							'id_gudang'=>$id_gudang,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode' AND id_gudang='$id_gudang' ");
					}
				
				//----------------------- 03-02-2014 ---------------------------------------------------
				//update stok unit jahit jika kode_unit_jahit != 0
				if ($kode_unit_jahit != '0') {
					// 25-03-2014
					if ($jenis_masuk == '1')
						$field_stok = "stok_bagus";
					else if ($jenis_masuk == '2')
						$field_stok = "stok_perbaikan";
					
					//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok, ".$field_stok." FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode'
							AND kode_unit='$kode_unit_jahit' ");
					if ($query3->num_rows() == 0){
						$stok_unit_lama = 0;
						$stok_unitxx_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok_unit	= $hasilrow->id;
						$stok_unit_lama	= $hasilrow->stok;
						
						if ($jenis_masuk == '1')
							$stok_unitxx_lama = $hasilrow->stok_bagus;
						else if ($jenis_masuk == '2')
							$stok_unitxx_lama = $hasilrow->stok_perbaikan;
					}
					$new_stok_unit = $stok_unit_lama-$qtytotal; // berkurang stok karena keluar dari unit
					$new_stok_unitxx = $stok_unitxx_lama-$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
						$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrowxx	= $seqxx->row();
							$id_stok_unit	= $seqrowxx->id+1;
						}else{
							$id_stok_unit	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_unit,
							'kode_brg_jadi'=>$kode,
							'kode_unit'=>$kode_unit_jahit,
							'stok'=>$new_stok_unit,
							 $field_stok=>$new_stok_unitxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_jahit', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
						".$field_stok." = '$new_stok_unitxx', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode' AND kode_unit='$kode_unit_jahit' ");
					}
				} // end if
				//-------------------------------------------------------------------------------------
				
				// jika semua data tdk kosong, insert ke tm_sjmasukwip_detail
				$data_detail = array(
					'id_sjmasukwip'=>$id_sj,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
					//'ket_qty_warna'=>$ket_warna
				);
				$this->db->insert('tm_sjmasukwip_detail',$data_detail);
				
				// ambil id detail sjmasukwip_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
			
				// ----------------------------------------------
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_sjmasukwip_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_sjmasukwip_detail'=>$iddetail,
						 'id_warna_brg_jadi'=>'0',
						 'kode_warna'=>$kode_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_sjmasukwip_detail_warna',$tm_sjmasukwip_detail_warna);
					
					// ========================= 03-02-2014, stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_jahit='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama+$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_jahit'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_jahit='$id_stok' ");
					}
					
					// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit jika kode_unit_jahit != 0
					if ($kode_unit_jahit != '0') {
						// 25-03-2014
						if ($jenis_masuk == '1')
							$field_stok = "stok_bagus";
						else if ($jenis_masuk == '2')
							$field_stok = "stok_perbaikan";
						
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, ".$field_stok." FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warnaxx_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							
							if ($jenis_masuk == '1')
								$stok_unit_warnaxx_lama	= $hasilrow->stok_bagus;
							else if ($jenis_masuk == '2')
								$stok_unit_warnaxx_lama	= $hasilrow->stok_perbaikan;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama-$qty_warna[$xx]; // berkurang stok karena keluar dari unit
						$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama-$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								$field_stok=>$new_stok_unit_warnaxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							".$field_stok." = '$new_stok_unit_warnaxx', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
					} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
					
				} // end for
				// ----------------------------------------------
		
  }
    
  function deletesjmasuk($kode){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_gudang, a.kode_unit_jahit, a.jenis_masuk, b.* 
								 FROM tm_sjmasukwip_detail b, tm_sjmasukwip a 
								 WHERE a.id = b.id_sjmasukwip AND b.id_sjmasukwip = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$jenis_masuk = $row2->jenis_masuk;
						
						// 1. stok total
						//$nama_tabel_stok = "tm_stok_hasil_jahit";
						// ============ update stok pabrik =====================
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit
									 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row2->id_gudang' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama-$row2->qty; // berkurang stok karena reset dari SJ masuk
								
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
									AND id_gudang = '$row2->id_gudang' ");
						
						// ================= update stok unit jahit jika kode_unit_jahit != 0 ==========================
						if ($row2->kode_unit_jahit != '0') {
							// 25-03-2014
							if ($jenis_masuk == '1')
								$field_stok = "stok_bagus";
							else if ($jenis_masuk == '2')
								$field_stok = "stok_perbaikan";
						
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, ".$field_stok." FROM tm_stok_unit_jahit
										 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$row2->kode_unit_jahit' ");
							if ($query3->num_rows() == 0){
								$stok_unit_lama = 0;
								$id_stok_unit = 0;
								$stok_unitxx_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								
								if ($jenis_masuk == '1')
									$stok_unitxx_lama = $hasilrow->stok_bagus;
								else if ($jenis_masuk == '2')
									$stok_unitxx_lama = $hasilrow->stok_perbaikan;
							}
							$new_stok_unit = $stok_unit_lama+$row2->qty; // bertambah stok ke unit karena reset dari SJ masuk
							$new_stok_unitxx = $stok_unitxx_lama+$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										".$field_stok." = '$new_stok_unitxx', tgl_update_stok = '$tgl' 
										WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
										AND kode_unit = '$row2->kode_unit_jahit' ");
						} // end if update stok unit jahit
						
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_sjmasukwip_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail_warna 
												WHERE id_sjmasukwip_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna
											 WHERE id_stok_hasil_jahit = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama-$rowwarna->qty; // berkurang stok karena reset dari SJ masuk
										
								$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_jahit= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ============= update stok unit jahit jika kode_unit_jahit != 0
								if ($row2->kode_unit_jahit != '0') {
									// 25-03-2014
									if ($jenis_masuk == '1')
										$field_stok = "stok_bagus";
									else if ($jenis_masuk == '2')
										$field_stok = "stok_perbaikan";
								
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, ".$field_stok." FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warnaxx_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										
										if ($jenis_masuk == '1')
											$stok_unit_warnaxx_lama	= $hasilrow->stok_bagus;
										else if ($jenis_masuk == '2')
											$stok_unit_warnaxx_lama	= $hasilrow->stok_perbaikan;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama+$rowwarna->qty; // bertambah stok ke unit karena reset dari SJ masuk
									$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama+$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												".$field_stok." = '$new_stok_unit_warnaxx', tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
								} // end if kode_unit_jahit != 0
							}
						} // end if detail warna
						
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_sjmasukwip_detail_warna 03-02-2014
						$this->db->query(" DELETE FROM tm_sjmasukwip_detail_warna WHERE id_sjmasukwip_detail='".$row2->id."' ");	
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_sjmasukwip_detail dan tm_sjmasukwip
    $this->db->delete('tm_sjmasukwip_detail', array('id_sjmasukwip' => $kode));
    $this->db->delete('tm_sjmasukwip', array('id' => $kode));
  }
    
  function get_sjmasuk($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sjmasukwip where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail WHERE id_sjmasukwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						
						$tabelstok = "tm_stok_hasil_jahit";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." 
									 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row1->id_gudang' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_warna_brg_jadi dan tm_sjmasukwip_detail_warna
						// modif 01-03-2014, ga pake relasi ke tm_warna_brg_jadi tapi langsung ke tm_warna
					/*	$sqlxx	= $this->db->query(" SELECT a.id, a.kode_warna, b.nama, c.qty FROM tm_warna_brg_jadi a, tm_warna b,
												tm_sjmasukwip_detail_warna c
												 WHERE a.kode_warna=b.kode AND c.id_warna_brg_jadi = a.id
												 AND c.id_sjmasukwip_detail = '$row2->id' AND a.kode_brg_jadi = '$row2->kode_brg_jadi' "); */
						$sqlxx	= $this->db->query(" SELECT c.kode_warna, b.nama, c.qty FROM tm_warna b, 
												tm_sjmasukwip_detail_warna c WHERE c.kode_warna = b.kode
												AND c.id_sjmasukwip_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								//$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$kode_warna = $row3->kode_warna;
								
								$detail_warna[] = array(	'kode_warna'=> $kode_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
								
								/*$listwarna .= $rownya->e_color_name." 
								
								<input type='text' name='qty_warna_".$iter."[]' value='".$rownya->qty."'>
								<input type='hidden' name='i_product_color_".$iter."[]' value='".$rownya->i_product_color."'><br>"; */
							}
						}
						else
							$detail_warna = '';
						//-------------------------------------------------------------------------------------
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'ket_qty_warna'=> $row2->ket_qty_warna,
												'keterangan'=> $row2->keterangan,
												'jum_stok'=> $jum_stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Masuk bagus hasil jahit";
				else if ($row1->jenis_masuk == '2')
					$nama_jenis = "Lain-lain (Perbaikan dari unit jahit)";
				else if ($row1->jenis_masuk == '3')
					$nama_jenis = "Lain-lain (Retur dari unit packing)";
				else if ($row1->jenis_masuk == '4')
					$nama_jenis = "Lain-lain (Retur dari gudang jadi)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
      
  function get_brgjadi($num, $offset, $gudang, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$row1->i_product_motif'
										AND id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
			}
			else
				$stok = 0;
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname,
										'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($gudang, $cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function cek_data_sjmasukwip($no_sj, $thn1, $kode_unit_jahit, $kode_unit_packing, $jenis_masuk){
	  $filterjahit = ""; $filterpacking = "";
	  if ($kode_unit_jahit != 0)
		$filterjahit = " AND kode_unit_jahit = '$kode_unit_jahit' ";
	
	if ($kode_unit_packing != 0)
		$filterpacking = " AND kode_unit_packing = '$kode_unit_packing' ";

    $this->db->select("id from tm_sjmasukwip WHERE no_sj = '$no_sj' ".$filterjahit." ".$filterpacking."
				AND jenis_masuk = '$jenis_masuk' AND extract(year from tgl_sj) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 14-03-2013, SJ keluar WIP
  
  function cek_data_sjkeluarwip($no_sj, $thn1, $kode_unit_jahit, $kode_unit_packing, $jenis_keluar){
	  $filterjahit = ""; $filterpacking = "";
	  if ($kode_unit_jahit != 0)
		$filterjahit = " AND kode_unit_jahit = '$kode_unit_jahit' ";
	
	if ($kode_unit_packing != 0)
		$filterpacking = " AND kode_unit_packing = '$kode_unit_packing' ";
		
    $this->db->select("id from tm_sjkeluarwip WHERE no_sj = '$no_sj' ".$filterjahit." ".$filterpacking." 
						AND jenis_keluar = '$jenis_keluar' AND extract(year from tgl_sj) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAllsjkeluar($num, $offset, $cari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit_jahit = '$kode_unit_jahit' ";
		if ($kode_unit_packing != "0")
			$pencarian.= " AND a.kode_unit_packing = '$kode_unit_packing' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$this->db->select(" distinct a.* FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tr_product_motif c  
				WHERE a.id = b.id_sjkeluarwip AND b.kode_brg_jadi = c.i_product_motif ".$pencarian, false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(b.e_product_motifname) like UPPER('%$caribrg%')) ";
					
				$query2	= $this->db->query(" SELECT a.* FROM tm_sjkeluarwip_detail a, tr_product_motif b  
								WHERE a.kode_brg_jadi=b.i_product_motif AND a.id_sjkeluarwip = '$row1->id' ".$pencarian2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname as nama_brg FROM tr_product_motif 
										 WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$nama_brg = '';
						}
						
						// 30-01-2014, detail qty warna -------------
						$strwarna = "";
						$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjkeluarwip_detail_warna a, tm_warna c
									WHERE a.kode_warna = c.kode
									AND a.id_sjkeluarwip_detail = '$row2->id' ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg_jadi.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						}
						if ($strwarna == "")
							$strwarna = $row2->ket_qty_warna;
						//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar bagus ke unit packing";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Keluar bagus ke gudang jadi";
				else if ($row1->jenis_keluar == '3')
					$nama_jenis = "Lain-lain (Retur ke unit jahit)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'keterangan'=> $row1->keterangan,
											'status_nota_retur'=> $row1->status_nota_retur,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function getAllsjkeluartanpalimit($cari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit_jahit = '$kode_unit_jahit' ";
		if ($kode_unit_packing != "0")
			$pencarian.= " AND a.kode_unit_packing = '$kode_unit_packing' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tr_product_motif c  
				WHERE a.id = b.id_sjkeluarwip AND b.kode_brg_jadi = c.i_product_motif ".$pencarian);

    return $query->result();  
  }
  
  //
  function savesjkeluar($id_sj, $id_gudang, $kode_unit_jahit, $kode_unit_packing, $jenis_keluar,
			$kode, $nama, $temp_qty, $kode_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
	// 05-03-2014, dikomen
    // cek apa udah ada datanya blm dgn no SJ tadi
   /* $filter = "";
	if ($kode_unit_jahit != 0)
		$filter = " AND kode_unit_jahit = '$kode_unit_jahit' ";
    
    $this->db->select("id from tm_sjkeluarwip WHERE no_sj = '".$this->db->escape_str($no_sj)."' ".$filter." ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_sjkeluarwip
			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'jenis_keluar'=>$jenis,
			  'id_gudang'=>$id_gudang,
			  'kode_unit_jahit'=>$kode_unit_jahit,
			  'kode_unit_packing'=>$kode_unit_packing,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_sjkeluarwip',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_sjkeluarwip
			$query2	= $this->db->query(" SELECT id FROM tm_sjkeluarwip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; */
			
			// 30-01-2014, insert ke tabel detail ----------------------------------
			//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
													
					$qtytotal+= $qty_warna[$xx];
				} // end for
			// ---------------------------------------------------------------------
			
				// ======== update stoknya! =============
				//$nama_tabel_stok = "tm_stok_hasil_jahit";
				
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$kode'
							AND id_gudang='$id_gudang' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode,
							'id_gudang'=>$id_gudang,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
						
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode' AND id_gudang='$id_gudang' ");
					}
					
					//----------------------- 03-02-2014 ---------------------------------------------------
					//update stok unit jahit jika kode_unit_jahit != 0
					if ($kode_unit_jahit != '0') {
						//25-03-2014 hanya jika jenis = 3 maka stok perbaikan
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_perbaikan FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode'
								AND kode_unit='$kode_unit_jahit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_lama = 0;
							$stok_unitxx_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unitxx_lama	= $hasilrow->stok_perbaikan;
						}
						$new_stok_unit = $stok_unit_lama+$qtytotal; // bertambah stok karena masuk ke unit
						$new_stok_unitxx = $stok_unitxx_lama+$qtytotal;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'kode_brg_jadi'=>$kode,
								'kode_unit'=>$kode_unit_jahit,
								'stok'=>$new_stok_unit,
								'stok_perbaikan'=>$new_stok_unitxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
							stok_perbaikan = '$new_stok_unitxx', tgl_update_stok = '$tgl' 
							where kode_brg_jadi= '$kode' AND kode_unit='$kode_unit_jahit' ");
						}
					} // end if
					//-------------------------------------------------------------------------------------					
				
				// jika semua data tdk kosong, insert ke tm_sjkeluarwip_detail
				$data_detail = array(
					'id_sjkeluarwip'=>$id_sj,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				//	'ket_qty_warna'=>$ket_warna
				);
				$this->db->insert('tm_sjkeluarwip_detail',$data_detail);
				
				// ambil id detail sjkeluarwip_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// ----------------------------------------------
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_sjkeluarwip_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_sjkeluarwip_detail'=>$iddetail,
						 'id_warna_brg_jadi'=>'0',
						 'kode_warna'=>$kode_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_sjkeluarwip_detail_warna',$tm_sjkeluarwip_detail_warna);
					
					// ========================= 03-02-2014, stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_jahit='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_jahit'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_jahit='$id_stok' ");
					}
					
					// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit jika kode_unit_jahit != 0
					if ($kode_unit_jahit != '0') {
						//25-03-2014 hanya jika jenis = 3 maka stok perbaikan
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_perbaikan FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warnaxx_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warnaxx_lama = $hasilrow->stok_perbaikan;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama+$qty_warna[$xx]; // bertambah stok karena masuk ke unit
						$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_perbaikan'=>$new_stok_unit_warnaxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_perbaikan = '$new_stok_unit_warnaxx', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
					} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
					
				} // end for
				// ----------------------------------------------
  }
    
  function deletesjkeluar($kode){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_gudang, a.kode_unit_jahit, b.* 
								 FROM tm_sjkeluarwip_detail b, tm_sjkeluarwip a 
								 WHERE a.id = b.id_sjkeluarwip AND b.id_sjkeluarwip = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok pabrik =====================
						//$nama_tabel_stok = "tm_stok_hasil_jahit";
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit
									 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row2->id_gudang' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
						
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
									AND id_gudang = '$row2->id_gudang' ");
									
						// ================= update stok unit jahit jika kode_unit_jahit != 0 ==========================
						if ($row2->kode_unit_jahit != '0') {
							// 25-03-2014, hanya jenis = 3 ambil stok_perbaikan
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_perbaikan FROM tm_stok_unit_jahit
										 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$row2->kode_unit_jahit' ");
							if ($query3->num_rows() == 0){
								$stok_unit_lama = 0;
								$id_stok_unit = 0;
								$stok_unitxx_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unitxx_lama = $hasilrow->stok_perbaikan;
							}
							$new_stok_unit = $stok_unit_lama-$row2->qty; // berkurang stok karena reset dari SJ keluar
							$new_stok_unitxx = $stok_unitxx_lama-$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_perbaikan = '$new_stok_unitxx', 
										tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
										AND kode_unit = '$row2->kode_unit_jahit' ");
						} // end if update stok unit jahit
						
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_sjkeluarwip_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail_warna 
												WHERE id_sjkeluarwip_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna
											 WHERE id_stok_hasil_jahit = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari SJ keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_jahit= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ============= update stok unit jahit jika kode_unit_jahit != 0
								if ($row2->kode_unit_jahit != '0') {
									// 25-03-2014, hanya jenis = 3 ambil stok_perbaikan
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_perbaikan FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warnaxx_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warnaxx_lama = $hasilrow->stok_perbaikan;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama-$rowwarna->qty; // berkurang stok di unit karena reset dari SJ keluar
									$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama-$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												stok_perbaikan = '$new_stok_unit_warnaxx', tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
								} // end if kode_unit_jahit != 0
							}
						} // end if detail warna
						
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_sjkeluarwip_detail_warna 03-02-2014
						$this->db->query(" DELETE FROM tm_sjkeluarwip_detail_warna WHERE id_sjkeluarwip_detail='".$row2->id."' ");
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_sjkeluarwip_detail dan tm_sjkeluarwip
    $this->db->delete('tm_sjkeluarwip_detail', array('id_sjkeluarwip' => $kode));
    $this->db->delete('tm_sjkeluarwip', array('id' => $kode));
  }
    
  function get_sjkeluar($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						
						$tabelstok = "tm_stok_hasil_jahit";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." 
									 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row1->id_gudang' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_sjkeluarwip_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.kode_warna, b.nama, c.qty FROM tm_warna b,
												tm_sjkeluarwip_detail_warna c
												 WHERE c.kode_warna=b.kode
												 AND c.id_sjkeluarwip_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								//$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$kode_warna = $row3->kode_warna;
								
								$detail_warna[] = array(	'kode_warna'=> $kode_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else {
							// 12-02-2014, ini jika di master warna udh diedit datanya, maka query langsung ke tabel warna
							// setelah itu, update id_warna_brg_jadi di tabel detail_warna sesuai dgn hasil query dgn tabel warna_brg_jadi
							// sementara ga dipake
						/*	$sqlxx	= $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
													 WHERE a.kode_warna=b.kode
													 AND a.kode_brg_jadi = '$row2->kode_brg_jadi' ");
							if ($sqlxx->num_rows() > 0){
								$hasilxx = $sqlxx->result();
								
								foreach ($hasilxx as $row3) {
									$nama_warna = $row3->nama;
									$id_warna_brg_jadi = $row3->id;
									
									$detail_warna[] = array(	'id_warna_brg_jadi'=> $id_warna_brg_jadi,
													'nama_warna'=> $nama_warna,
													'qty_warna'=> 0
												);
								}
							}
							else */
								$detail_warna = '';
						}
						//-------------------------------------------------------------------------------------
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'ket_qty_warna'=> $row2->ket_qty_warna,
												'keterangan'=> $row2->keterangan,
												'jum_stok'=> $jum_stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar bagus ke unit packing";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Keluar bagus ke gudang jadi";
				else if ($row1->jenis_keluar == '3')
					$nama_jenis = "Lain-lain (Retur ke unit jahit)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  // 04-12-2014
  // 11-07-2013
  function getAllstokawal($num, $offset, $cari) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.id_gudang, a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.id_gudang, a.kode_brg_jadi ";
		}
		
		$this->db->select(" a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.id_gudang, a.tgl_update_stok 
						FROM tm_stok_hasil_jahit a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				// 15-02-2014, stok per warna
					$queryxx	= $this->db->query(" SELECT c.kode, c.nama, a.stok FROM tm_stok_hasil_jahit_warna a, 
									tm_warna c
									WHERE a.kode_warna = c.kode AND a.id_stok_hasil_jahit = '$row1->id' ORDER BY c.nama ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'kode_warna'=> $rowxx->kode,
													'nama_warna'=> $rowxx->nama,
													'stok'=> $rowxx->stok
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
				
				$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
								
				$data_stok[] = array(		'id'=> $row1->id,	
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'stok'=> $row1->stok,
											'nama_brg_jadi'=> $row1->e_product_motifname,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $kode_gudang." - ".$nama_gudang,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  function getAllstokawaltanpalimit($cari){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.id_gudang, a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.id_gudang, a.kode_brg_jadi ";
		}
		
		$query	= $this->db->query(" SELECT a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.id_gudang, a.tgl_update_stok 
						FROM tm_stok_hasil_jahit a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ");

    return $query->result();  
  }
  
  // modif 15-02-2014
  function savestokawal($id_gudang, $kode, $nama, $temp_qty, $kode_warna, $qty_warna){  
    $tgl = date("Y-m-d H:i:s");
	
	//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotal = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$qty_warna[$xx] = trim($qty_warna[$xx]);
			$qtytotal+= $qty_warna[$xx];
		} // end for
	// ---------------------------------------------------------------------
	
	$datanya = array(
			  'kode_brg_jadi'=>$kode,
			  'stok'=>$qtytotal,
			  'id_gudang'=>$id_gudang,
			  'tgl_update_stok'=>$tgl
			);
	$this->db->insert('tm_stok_hasil_jahit',$datanya);
	
	// ambil id_stok utk dipake di stok warna
	$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
	if($sqlxx->num_rows() > 0) {
		$hasilxx	= $sqlxx->row();
		$id_stok	= $hasilxx->id;
	}
	
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
					
					// ========================= stok per warna ===============================================
					//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_jahit='$id_stok' ");
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_jahit'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							'stok'=>$qty_warna[$xx],
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
					}
				} // end for
				// ----------------------------------------------
  }
  
  function get_brgjadi_stokawal($num, $offset, $gudang, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang') ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang')  ";		
		$sql.=" AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$row1->i_product_motif'
										AND id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
			}
			else
				$stok = 0; */
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
										//'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjadi_stokawaltanpalimit($gudang, $cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang')";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_jahit 
					 WHERE id_gudang = '$gudang')
					AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  // end 04-12-2014
  
  // 25-07-2013
  function get_sj_keluar2($cari, $unit_jahit)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' 
				AND status_nota_retur = 'f' order by tgl_sj DESC ";
		$this->db->select($sql, false);
	}
	else {
		$sql = " * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' 
				AND status_nota_retur = 'f' AND UPPER(no_sj) like UPPER('%$cari%') 
				order by tgl_sj DESC ";
		$this->db->select($sql, false);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {		
			
			// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1->id' 
											ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ambil nama brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
												WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else
							$nama_brg_jadi = '';
																		
						$detail_bhn[] = array('id'=> $row2->id,
												'id_sjkeluarwip'=> $row2->id_sjkeluarwip,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty
									);
						
					} 
				} 
				else {
					$detail_bhn = '';
				} // end detail
			
			$data_bhn[] = array(		'id'=> $row1->id,	
										'no_sj'=> $row1->no_sj,	
										'tgl_sj'=> $row1->tgl_sj,
										'tgl_update'=> $row1->tgl_update,
										'detail_bhn'=> $detail_bhn
								);
			
			$detail_bhn = array();
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_sj_keluar2tanpalimit($cari, $unit_jahit){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' AND status_nota_retur = 'f' order by tgl_sj DESC ";
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_sjkeluarwip where kode_unit_jahit = '$unit_jahit' AND jenis_keluar = '3' AND status_nota_retur = 'f' AND UPPER(no_sj) like UPPER('%$cari%') 
				order by tgl_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_detail_sj_keluar($no_sj_keluar, $id_sj_keluar, $unit_jahit){
    $detail_sj = array();
    
    $list_id_sj = explode(",", $id_sj_keluar); 
    foreach($list_id_sj as $row1) {
		if ($row1 != '') {
			// ====================================
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1' ORDER BY id ASC ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$id_sj_keluarwip_detail	= $row2->id;
						$kode_brg_jadi	= $row2->kode_brg_jadi;
						$qty	= $row2->qty;
						
						$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sjkeluarwip WHERE id = '$row1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$no_sj	= $hasilrow->no_sj;
							$tgl_sj	= $hasilrow->tgl_sj;
							
							$pisah1 = explode("-", $tgl_sj);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							if ($bln1 == '01')
								$nama_bln = "Januari";
							else if ($bln1 == '02')
								$nama_bln = "Februari";
							else if ($bln1 == '03')
								$nama_bln = "Maret";
							else if ($bln1 == '04')
								$nama_bln = "April";
							else if ($bln1 == '05')
								$nama_bln = "Mei";
							else if ($bln1 == '06')
								$nama_bln = "Juni";
							else if ($bln1 == '07')
								$nama_bln = "Juli";
							else if ($bln1 == '08')
								$nama_bln = "Agustus";
							else if ($bln1 == '09')
								$nama_bln = "September";
							else if ($bln1 == '10')
								$nama_bln = "Oktober";
							else if ($bln1 == '11')
								$nama_bln = "November";
							else if ($bln1 == '12')
								$nama_bln = "Desember";
							$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
						}
						else {
							$no_sj	= '';
							$tgl_sj	= '';
						}
																		
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
						
						// 13-01-2014
						// ambil harga barang jahit
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_hasil_jahit
										WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_unit = '$unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$harganya	= $hasilrow->harga;
						}
						else {
							$harganya	= 0;
						}
						
						// ambil data nama unit
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
						
						$detail_sj[] = array(		'id'=> $id_sj_keluarwip_detail,
										'id_sj_keluarwip'=> $row1,
										'kode_brg_jadi'=> $kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'qty'=> $qty,
										'no_sj'=> $no_sj,
										'tgl_sj'=> $tgl_sj,
										'kode_unit'=> $unit_jahit,
										'nama_unit'=> $nama_unit,
										'harganya'=> $harganya
								);
					}
				}
			// ====================================
		
		}
	} // end foreach
	return $detail_sj;
  }
  
  // 26-07-2013
  function cek_data_notaretur($no_nota){
    $this->db->select("id from tm_nota_retur_wip WHERE no_nota = '$no_nota' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savenotaretur($no_nota, $tgl_nota, $ket, $gtotal, $unit_jahit, 
						$id_sj_keluar_detail, $kode_brg_jadi, $qty, $harga, $harga_lama, $diskon, $total){  
    $tgl = date("Y-m-d H:i:s");
	//$uid = $this->session->userdata('uid');
	
    // cek apa udah ada datanya blm dgn no nota tadi
    $this->db->select("id from tm_nota_retur_wip WHERE no_nota = '".$this->db->escape_str($no_nota)."' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			$query3	= $this->db->query(" SELECT id FROM tm_nota_retur_wip ORDER BY id DESC LIMIT 1 ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				$last_id	= $hasilrow3->id;
				$new_id = $last_id+1;
			}
			else
				$new_id = 1;
	
			// insert di tm_nota_retur_wip
			$data_header = array(
			  'id'=>$new_id,
			  'no_nota'=>$no_nota,
			  'tgl_nota'=>$tgl_nota,
			  'kode_unit_jahit'=>$unit_jahit,
			  'keterangan'=>$ket,
			  'total'=>$gtotal,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			$this->db->insert('tm_nota_retur_wip',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_nota_retur_wip
			$query2	= $this->db->query(" SELECT id FROM tm_nota_retur_wip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_nota	= $hasilrow->id; 
			
			$query3	= $this->db->query(" SELECT id FROM tm_nota_retur_wip_detail ORDER BY id DESC LIMIT 1 ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				$last_id	= $hasilrow3->id;
				$new_id = $last_id+1;
			}
			else 
				$new_id = 1;
				
			// insert ke tm_nota_retur_wip_detail
			$data_detail = array(
				'id'=>$new_id,
				'id_nota_retur_wip'=>$id_nota,
				'kode_brg_jadi'=>$kode_brg_jadi,
				'harga'=>$harga,
				'qty'=>$qty,
				'diskon'=>$diskon,
				'subtotal'=>$total,
				'id_sjkeluarwip_detail'=>$id_sj_keluar_detail
			);
			$this->db->insert('tm_nota_retur_wip_detail',$data_detail);
			// done, 11:29
			
			// 13-01-2014
			// ambil harga barang jahit
			$query3	= $this->db->query(" SELECT id FROM tm_harga_hasil_jahit
							WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_unit = '$unit_jahit' ");
			if ($query3->num_rows() == 0){
				$data_harga = array(
					'kode_brg_jadi'=>$kode_brg_jadi,
					'kode_unit'=>$unit_jahit,
					'harga'=>$harga,
					'tgl_input'=>$tgl,
					'tgl_update'=>$tgl
					);
				$this->db->insert('tm_harga_hasil_jahit',$data_harga);
			}
			else {
				if ($harga != $harga_lama) {
					$hasilrow3 = $query3->row();
					$id_harga	= $hasilrow3->id;
					$this->db->query(" UPDATE tm_harga_hasil_jahit SET harga = '$harga', tgl_update='$tgl' 
										WHERE id = '$id_harga' ");
				}
			}
			
			// update status nota retur di tabel tm_sjkeluarwip
			$query3	= $this->db->query(" SELECT id_sjkeluarwip FROM tm_sjkeluarwip_detail WHERE id = '$id_sj_keluar_detail' ");
			$hasilrow3 = $query3->row();
			$id_sjkeluarwip	= $hasilrow3->id_sjkeluarwip;
			
			$this->db->query(" UPDATE tm_sjkeluarwip SET status_nota_retur = 't' WHERE id = '$id_sjkeluarwip' ");
  }
  
  function getAllnotareturwip($num, $offset, $cari, $date_from, $date_to) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_nota) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_nota >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_nota <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_nota DESC, no_nota DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_nota >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_nota <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_nota DESC, no_nota DESC ";
		}
		
		$this->db->select(" * FROM tm_nota_retur_wip WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_nota = array();
		$detail_nota = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_nota_retur_wip_detail WHERE id_nota_retur_wip = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname as nama_brg FROM tr_product_motif 
										 WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$nama_brg = '';
						}
						
										
						$detail_nota[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'diskon'=> $row2->diskon,
												'subtotal'=> $row2->subtotal
											);
					}
				}
				else {
					$detail_nota = '';
				}
																
				// unit jahit
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else
					$nama_unit_jahit = "";
								
				$data_nota[] = array(		'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,
											'tgl_nota'=> $row1->tgl_nota,
											'tgl_update'=> $row1->tgl_update,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'ket'=> $row1->keterangan,
											'gtotal'=> $row1->total,
											'detail_nota'=> $detail_nota
											);
				$detail_nota = array();
			} // endforeach header
		}
		else {
			$data_nota = '';
		}
		return $data_nota;
  }
  
  function getAllnotareturwiptanpalimit($cari, $date_from, $date_to){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_nota) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_nota >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_nota <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_nota DESC, no_nota DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_nota >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_nota <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_nota DESC, no_nota DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_nota_retur_wip WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  function deletenotareturwip($kode){
	$tgl = date("Y-m-d");	
	
		// 1. reset status nota returnya
		// ambil data detail
		$query2	= $this->db->query(" SELECT * FROM tm_nota_retur_wip_detail WHERE id_nota_retur_wip = '$kode' ");
		if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
									
			foreach ($hasil2 as $row2) {
										
				$query3	= $this->db->query(" SELECT id_sjkeluarwip FROM tm_sjkeluarwip_detail
							 WHERE id='$row2->id_sjkeluarwip_detail' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_sjkeluarwip	= $hasilrow->id_sjkeluarwip;
				}
						
				$this->db->query(" UPDATE tm_sjkeluarwip SET status_nota_retur = 'f' WHERE id = '$id_sjkeluarwip' ");
																						
			} // end foreach
		} // end if
	
	// 2. hapus data tm_nota_retur_wip_detail dan tm_nota_retur_wip
    $this->db->delete('tm_nota_retur_wip_detail', array('id_nota_retur_wip' => $kode));
    $this->db->delete('tm_nota_retur_wip', array('id' => $kode));
  }
  
  function get_notareturwip($id_nota) {
		$query	= $this->db->query(" SELECT * FROM tm_nota_retur_wip where id = '$id_nota' ");
	
		$data_nota = array();
		$detail_nota = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_nota_retur_wip_detail WHERE id_nota_retur_wip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						
						// no SJ
						$query3	= $this->db->query(" SELECT a.no_sj, a.tgl_sj FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b
											WHERE a.id = b.id_sjkeluarwip AND b.id = '$row2->id_sjkeluarwip_detail' ");
						$hasilrow = $query3->row();
						$no_sj	= $hasilrow->no_sj;
						$tgl_sj	= $hasilrow->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
																												
						$detail_nota[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'diskon'=> $row2->diskon,
												'total'=> $row2->subtotal,
												'id_sjkeluarwip_detail'=> $row2->id_sjkeluarwip_detail,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj
											);
					}
				}
				else {
					$detail_nota = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_nota);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
								
				// unit jahit
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
								
				$data_nota[] = array(		'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,
											'tgl_nota'=> $tgl,
											'gtotal'=> $row1->total,
											'keterangan'=> $row1->keterangan,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'detail_nota'=> $detail_nota
											);
				$detail_nota = array();
			} // endforeach header
		}
		else {
			$data_nota = '';
		}
		return $data_nota;
  }
  
  // 21-01-2014
  function get_notareturwipgroupbyproduk($id_nota) {
		$query	= $this->db->query(" SELECT * FROM tm_nota_retur_wip where id = '$id_nota' ");
	
		$data_nota = array();
		$detail_nota = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$no_sj = '';
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT kode_brg_jadi, harga, sum(qty) as jumqty, sum(subtotal) as jumsubtotal 
									FROM tm_nota_retur_wip_detail WHERE id_nota_retur_wip = '$row1->id'
									GROUP BY kode_brg_jadi, harga ORDER BY kode_brg_jadi ");
									
				/*echo " SELECT kode_brg_jadi, harga, sum(qty) as jumqty, sum(subtotal) as jumsubtotal 
									FROM tm_nota_retur_wip_detail WHERE id_nota_retur_wip = '$row1->id'
									GROUP BY kode_brg_jadi, harga ORDER BY kode_brg_jadi "; die(); */
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						
						// no SJ
					/*	$query3	= $this->db->query(" SELECT a.no_sj, a.tgl_sj FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b
											WHERE a.id = b.id_sjkeluarwip AND b.id = '$row2->id_sjkeluarwip_detail' ");
						$hasilrow = $query3->row();
						$no_sj	= $hasilrow->no_sj;
						$tgl_sj	= $hasilrow->tgl_sj;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1; */
																												
						$detail_nota[] = array(	//'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->jumqty,
												'harga'=> $row2->harga,
												//'diskon'=> $row2->diskon,
												'total'=> $row2->jumsubtotal
												//'id_sjkeluarwip_detail'=> $row2->id_sjkeluarwip_detail,
												//'no_sj'=> $no_sj,
												//'tgl_sj'=> $tgl_sj
											);
					}
				}
				else {
					$detail_nota = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_nota);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$tgl = $tgl1." ".$nama_bln." ".$thn1;
								
				// unit jahit
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				
				// no SJ
				/*echo "SELECT distinct a.no_sj FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tm_nota_retur_wip c, tm_nota_retur_wip_detail d
											WHERE a.id = b.id_sjkeluarwip AND c.id = d.id_nota_retur_wip AND d.id_sjkeluarwip_detail = b.id
											AND c.id = '$row1->id' ORDER BY a.no_sj"; die(); */
				$queryxx	= $this->db->query(" SELECT distinct a.no_sj FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tm_nota_retur_wip c, tm_nota_retur_wip_detail d
											WHERE a.id = b.id_sjkeluarwip AND c.id = d.id_nota_retur_wip AND d.id_sjkeluarwip_detail = b.id
											AND c.id = '$row1->id' ORDER BY a.no_sj ");
				if ($queryxx->num_rows() > 0){
					//echo "didieu"; die();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$no_sj.= $rowxx->no_sj." ";
					}
				}
				else
					$no_sj = '';
										
				$data_nota[] = array(		'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,
											'tgl_nota'=> $tgl,
											'gtotal'=> $row1->total,
											'keterangan'=> $row1->keterangan,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'no_sj'=> $no_sj,
											'detail_nota'=> $detail_nota
											);
				$detail_nota = array();
				$no_sj = '';
			} // endforeach header
		}
		else {
			$data_nota = '';
		}
		return $data_nota;
  }
  
  // 04-12-2014 copy dari BLN
  // 11-11-2014, set stok awal unit jahit
  function getAllstokawalunit($num, $offset, $cari) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.kode_unit, a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.kode_unit, a.kode_brg_jadi ";
		}
		
		$this->db->select(" a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.kode_unit, a.tgl_update_stok 
						FROM tm_stok_unit_jahit a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				// 15-02-2014, stok per warna
					$queryxx	= $this->db->query(" SELECT c.kode, c.nama, a.stok, a.stok_bagus, a.stok_perbaikan 
									FROM tm_stok_unit_jahit_warna a, tm_warna c
									WHERE a.kode_warna = c.kode AND a.id_stok_unit_jahit = '$row1->id' ORDER BY c.nama ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'kode_warna'=> $rowxx->kode,
													'nama_warna'=> $rowxx->nama,
													'stok'=> $rowxx->stok,
													'stok_bagus'=> $rowxx->stok_bagus,
													'stok_perbaikan'=> $rowxx->stok_perbaikan
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit	= $hasilrow->nama;
				}
				else {
					$nama_unit = '';
				}
								
				$data_stok[] = array(		'id'=> $row1->id,	
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'stok'=> $row1->stok,
											'nama_brg_jadi'=> $row1->e_product_motifname,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $row1->kode_unit." - ".$nama_unit,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  function getAllstokawalunittanpalimit($cari){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.kode_unit, a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.kode_unit, a.kode_brg_jadi ";
		}
		
		$query	= $this->db->query(" SELECT a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.kode_unit, a.tgl_update_stok 
						FROM tm_stok_unit_jahit a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ");

    return $query->result();  
  }
  
  function savestokawalunit($kode_unit, $kode, $nama, $temp_qty, $kode_warna, $qty_warna, $temp_qty2, $kode_warna2, $qty_warna2){  
    $tgl = date("Y-m-d H:i:s");
	
	//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotal = 0; $qtytotalbagus = 0; $qtytotalperbaikan = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$qty_warna[$xx] = trim($qty_warna[$xx]);
			$qty_warna2[$xx] = trim($qty_warna2[$xx]);
			$qtytotal+= $qty_warna[$xx]+$qty_warna2[$xx];
			$qtytotalbagus+=$qty_warna[$xx];
			$qtytotalperbaikan+=$qty_warna2[$xx];
		} // end for
	// ---------------------------------------------------------------------
	
	//cek apakah udh ada stok di tm_stok_unit_jahit. Jika blm ada, insert. Jika udh ada, ga usah ada aksi apa2
	$query3	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit WHERE kode_unit = '".$kode_unit."'
							AND kode_brg_jadi='$kode' ");
	if ($query3->num_rows() == 0){
		$seq1	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
		if($seq1->num_rows() > 0) {
			$seq1row	= $seq1->row();
			$id_stok	= $seq1row->id+1;
		}else{
			$id_stok	= 1;
		}
							
		$datanya = array(
			  'id'=>$id_stok,
			  'kode_brg_jadi'=>$kode,
			  'stok'=>$qtytotal,
			  'stok_bagus'=>$qtytotalbagus,
			  'stok_perbaikan'=>$qtytotalperbaikan,
			  'kode_unit'=>$kode_unit,
			  'tgl_update_stok'=>$tgl
			);
		$this->db->insert('tm_stok_unit_jahit',$datanya);
	
		// ambil id_stok utk dipake di stok warna
		/*$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
		if($sqlxx->num_rows() > 0) {
			$hasilxx	= $sqlxx->row();
			$id_stok	= $hasilxx->id;
		} */
		
					for ($xx=0; $xx<count($kode_warna); $xx++) {
						$kode_warna[$xx] = trim($kode_warna[$xx]);
						$qty_warna[$xx] = trim($qty_warna[$xx]);
						$qty_warna2[$xx] = trim($qty_warna2[$xx]);
						
						$qtywarnatotal = $qty_warna[$xx]+$qty_warna2[$xx];
						// ========================= stok per warna ===============================================
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok' ");
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_unit_jahit'=>$id_stok,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$qtywarnatotal,
								'stok_bagus'=>$qty_warna[$xx],
								'stok_perbaikan'=>$qty_warna2[$xx],
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
					} // end for
					// ----------------------------------------------
		} // end
  }
  
  function get_brgjadi_stokawalunit($num, $offset, $kode_unit, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_unit_jahit 
					 WHERE kode_unit = '$kode_unit') ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_unit_jahit 
					 WHERE kode_unit = '$kode_unit')  ";		
		$sql.=" AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$row1->i_product_motif'
										AND id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
			}
			else
				$stok = 0; */
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
										//'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjadi_stokawalunittanpalimit($kode_unit, $cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_unit_jahit 
					 WHERE kode_unit = '$kode_unit')";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_unit_jahit 
					 WHERE kode_unit = '$kode_unit')
					AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
}
