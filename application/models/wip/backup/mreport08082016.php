<?php
class Mreport extends CI_Model{
  function Mreport() {

  parent::__construct();
  }
  
  // 15-03-2014. pembaharuan 02-11-2015
  function get_sowip($num, $offset, $gudang, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_hasil_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($gudang != '0')
		$sql.= " AND id_gudang = '$gudang' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.id_lokasi = b.id AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di gudang QC ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di gudang QC ?";
								
				$data_so[] = array(		'id'=> $row1->id,	
										'bulan'=> $row1->bulan,	
										'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,	
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sowiptanpalimit($gudang, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_hasil_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($gudang != '0')
		$sql.= " AND id_gudang = '$gudang' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.jenis = '2'
						ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 18-03-2014
  function get_sohasiljahit($id_so) {
		$query	= $this->db->query(" SELECT a.id as id_header, a.id_gudang, a.bulan, a.tahun, a.tgl_so, a.jenis_perhitungan_stok, 
					b.*, d.kode_brg, d.nama_brg
					FROM tt_stok_opname_hasil_jahit_detail b INNER JOIN tt_stok_opname_hasil_jahit a ON b.id_stok_opname_hasil_jahit = a.id 
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.id = '$id_so' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
		
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
								WHERE a.id = '$row->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_hasil_jahit_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_stok_opname_hasil_jahit_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a INNER JOIN tm_stok_hasil_jahit_warna b ON a.id = b.id_stok_hasil_jahit
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_gudang = '$row->id_gudang'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				if ($row->tgl_so != '') {
					$pisah1 = explode("-", $row->tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
				}
				else
					$tgl_so = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id_gudang'=> $row->id_gudang,
										'kode_gudang'=> $kode_gudang,
										'nama_gudang'=> $nama_gudang,
										'nama_lokasi'=> $nama_lokasi,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'tgl_so'=> $tgl_so,
										'nama_bln'=> $nama_bln,
										'jenis_perhitungan_stok'=>$row->jenis_perhitungan_stok,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  // 03-11-2015
  function get_detail_stokbrgbaru_hasiljahit($id_so) {
	  // ambil id_gudang dari acuan id_so
	  $sqlxx = " SELECT id_gudang FROM tt_stok_opname_hasil_jahit WHERE id='$id_so' ";
	  $queryxx = $this->db->query($sqlxx);
	
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_gudang = $hasilxx->id_gudang;
		}
		else
			$id_gudang = 0;
		
		$sql = " SELECT b.id as id_brg_wip, b.kode_brg, b.nama_brg, a.id as id_stok
							FROM tm_barang_wip b 
							LEFT JOIN tm_stok_hasil_jahit a ON a.id_brg_wip = b.id
							WHERE a.id_gudang = '$id_gudang' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select b.id_brg_wip FROM tt_stok_opname_hasil_jahit a
							INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
							WHERE a.id = '$id_so')
							ORDER BY b.nama_brg  ";
	  
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// stok warna
				$sql2 = " SELECT a.stok, a.id_warna, b.nama as nama_warna FROM tm_stok_hasil_jahit_warna a
						INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_stok_hasil_jahit = '$row->id_stok'
						ORDER BY b.nama";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					$detail_warna = array();			
					foreach ($hasil2 as $row2) {
						$detail_warna[] = array(
									'id_warna'=> $row2->id_warna,
									'nama_warna'=> $row2->nama_warna,
									'saldo_akhir'=> $row2->stok,
									'stok_opname'=> $row2->stok
								);
					}
				}
				
				$detail_bahan[] = array('id'=> 0,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesohasiljahit($id_so, $tgl_so, $jenis_perhitungan_stok, $id_gudang, $id_itemnya, $id_brg_wip, 
			$id_warna, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			// 11-03-2015
			//$auto_saldo_akhir_warna[$xx] = trim($auto_saldo_akhir_warna[$xx]);
			
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
	// ---------------------------------------------------------------------
		$totalxx = $qtytotalstokfisik;
		
		// ambil id detail id_stok_opname_hasil_jahit_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail 
					where id_brg_wip = '$id_brg_wip' 
					AND id_stok_opname_hasil_jahit = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;

			// 03-11-2015 sementara dikeluarin auto_saldo_akhir='$auto_saldo_akhir', 
		
		// =============================== 03-11-2015 SEKALIGUS UPDATE STOKNYA =======================================================
		// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// 01-02-2016 GA DIPAKE LG!
			
			//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
		/*	$sql3 = "SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
					INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
					WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
			
			$sql3.=" AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang = '$id_gudang'";
							
			$query3	= $this->db->query($sql3);
				
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluarnya = $hasilrow->jum_keluar;
					
				if ($keluarnya == '')
					$keluarnya = 0;
			}
			else
				$keluarnya = 0;
			
			// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.=" AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang = '$id_gudang' ";
			$query3	= $this->db->query($sql3);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$masuknya = $hasilrow->jum_masuk;
					
					if ($masuknya == '')
						$masuknya = 0;
				}
				else
					$masuknya = 0;
			
			$jum_stok_akhir = $masuknya-$keluarnya;
			$totalxx = $totalxx + $jum_stok_akhir;			
			// 04-03-2015
			//$totalxx_saldo_akhir = $totalxx_saldo_akhir + $jum_stok_akhir;
			//------------------------ END HITUNG TRANSAKSI di sjkeluarwip dan sjmasukwip ------------------------------------------------
			*/
			
			// ======== update stoknya! =============
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE id_brg_wip = '$id_brg_wip'
							AND id_gudang='$id_gudang' ");
					if ($query3->num_rows() == 0){
						$data_stok = array(
							'id_brg_wip'=>$id_brg_wip,
							'id_gudang'=>$id_gudang,
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$totalxx', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_gudang='$id_gudang' ");
					}
					
				// stok_hasil_jahit_warna
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					
					$totalxx = $stok_fisik[$xx];
					
					//------------------------------------------------------------------------------
					// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
					// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
					
					// 01-02-2016 GA DIPAKE LG
					
					//1. hitung brg keluar dari gudang WIP ke packing/gd jadi, dari tm_sjkeluarwip
				/*	$sql3 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
					if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
						$sql3.=" a.tgl_sj > '$tgl_so' ";
					else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
						$sql3.=" a.tgl_sj >= '$tgl_so' ";
					
					$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
							AND a.id_gudang = '$id_gudang'
							AND c.id_warna = '".$id_warna[$xx]."' ";
					
					$query3	= $this->db->query($sql3);
					
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$keluarnya = $hasilrow->jum_keluar;
							
						if ($keluarnya == '')
							$keluarnya = 0;
					}
					else
						$keluarnya = 0;
					
					// 2. hitung brg masuk ke gudang WIP, dari tm_sjmasukwip
					$sql3 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE ";
					if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
						$sql3.=" a.tgl_sj > '$tgl_so' ";
					else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						$sql3.=" a.tgl_sj >= '$tgl_so' ";
						
					$sql3.=" AND b.id_brg_wip = '$id_brg_wip'
								AND a.id_gudang = '$id_gudang' 
								AND c.id_warna = '".$id_warna[$xx]."' ";
					
					$query3	= $this->db->query($sql3);
					
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuknya = $hasilrow->jum_masuk;
							
							if ($masuknya == '')
								$masuknya = 0;
						}
						else
							$masuknya = 0;
					
					$jum_stok_akhir = $masuknya-$keluarnya;
					$totalxx = $totalxx + $jum_stok_akhir;
					// 04-03-2015. 21-10-2015 GA DIPAKE!
					//$totalxx_saldo_akhir = $totalxx_saldo_akhir + $jum_stok_akhir;
					// ------------ end hitung transaksi keluar/masuk ------------------------------
					*/
			// ========================= stok per warna ===============================================
					//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_hasil_jahit='$id_stok' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_jahit'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_jahit='$id_stok' ");
					}
					
					if ($id_itemnya != '0') {
						$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."'
							WHERE id_stok_opname_hasil_jahit_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
					}
				} // end for
				// ----------------------------------------------
				
			// 04-02-2016
			if ($id_itemnya != '0')
				$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$qtytotalstokfisik',
						status_approve='t' where id = '$iddetail' ");
			
			if ($id_itemnya == '0') {				
				$databaru = array(
							'id_stok_opname_hasil_jahit'=>$id_so,
							'id_brg_wip'=>$id_brg_wip, 
							'stok_awal'=>0,
							'jum_stok_opname'=>$qtytotalstokfisik,
							'status_approve'=> 't'
						);
			   $this->db->insert('tt_stok_opname_hasil_jahit_detail',$databaru);
			   
			   	$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
				if($seq->num_rows() > 0) {
					$seqrow	= $seq->row();
					$iddetailnew	= $seqrow->id;
				}else{
					$iddetailnew	= 0;
				}

			   // insert ke tabel SO warna
			   for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);				
					
					$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_warna->num_rows() > 0) {
								$seqrow	= $seq_warna->row();
								$idbaru	= $seqrow->id+1;
							}else{
								$idbaru	= 1;
							}
							
							$tt_stok_opname_hasil_jahit_detail_warna	= array(
									 'id'=>$idbaru,
									 'id_stok_opname_hasil_jahit_detail'=>$iddetailnew,
									 'id_warna'=>$id_warna[$xx],
									 'jum_stok_opname'=>$stok_fisik[$xx],
									 'saldo_akhir'=>0
								);
								$this->db->insert('tt_stok_opname_hasil_jahit_detail_warna',$tt_stok_opname_hasil_jahit_detail_warna);
				} // end for
			} // end if item baru
			 // ====================================================================================
		// ========================================================================================================================
  }
  
  // 06-11-2015
  function get_sowipunit($num, $offset, $id_unit, $bulan, $tahun, $s_approve) {	
	$sql = " * FROM tt_stok_opname_unit_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_unit != '0')
		$sql.= " AND id_unit = '$id_unit' ";
	if ($s_approve != '0')
		$sql.= " AND status_approve = '$s_approve' ";	
		
	$sql.= " ORDER BY tahun DESC, bulan DESC, id_unit ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				$data_so[] = array(			'id'=> $row1->id,	
											'bulan'=> $row1->bulan,	
											'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'id_unit'=> $row1->id_unit,	
											'kode_unit'=> $kode_unit,	
											'nama_unit'=> $nama_unit
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sowipunittanpalimit($id_unit, $bulan, $tahun, $s_approve){
	$sql = " select * FROM tt_stok_opname_unit_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_unit != '0')
		$sql.= " AND id_unit = '$id_unit' ";
	if ($s_approve != '0')
		$sql.= " AND status_approve = '$s_approve' ";	
		
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 14-11-2015
  function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 17-03-2014. modif 06-11-2015
  function get_sounitjahit($id_so) {

		$query	= $this->db->query(" SELECT a.id as id_header, a.id_unit, a.bulan, a.tahun, a.tgl_so, a.jenis_perhitungan_stok, b.*, 
					d.kode_brg, d.nama_brg 
					FROM tt_stok_opname_unit_jahit_detail b INNER JOIN tt_stok_opname_unit_jahit a ON b.id_stok_opname_unit_jahit = a.id
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.id = '$id_so' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
				
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE id = '$row->id_unit' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_unit_jahit_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_stok_opname_unit_jahit_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_unit_jahit a INNER JOIN tm_stok_unit_jahit_warna b ON a.id = b.id_stok_unit_jahit
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_unit = '$row->id_unit'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				// 06-11-2015 SEMENTARA DIKOMEN, MUNGKIN NANTI DIPAKE DI MUTASI STOK UNTUK ACUAN SALDO AWAL
	
				// ---------- 11-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir, b.auto_saldo_akhir_bagus, b.auto_saldo_akhir_perbaikan
									FROM tt_stok_opname_unit_jahit a INNER JOIN tt_stok_opname_unit_jahit_detail b ON 
									a.id = b.id_stok_opname_unit_jahit
									WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
					$saldo_awal_bagus = $hasilrow->auto_saldo_akhir_bagus;
					$saldo_awal_perbaikan = $hasilrow->auto_saldo_akhir_perbaikan;
				}
				else {
					$saldo_awal = 0;
					$saldo_awal_bagus = 0;
					$saldo_awal_perbaikan = 0;
				}
				
				// 2. hitung keluar bln ini
				// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus = 0;
				
				// 2.1.2 tm_sjmasukwip, jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perbaikan = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perbaikan = 0;
				
				// 2.2. dari tabel tm_sjmasukbhnbakupic
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$row->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus+= $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus+= 0;
				
				// 3. hitung masuk bln ini
				// 3.1.1 dari tabel tm_sjkeluarwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND a.jenis_keluar<>'3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus = 0;
				
				// 3.1.2 dari tabel tm_sjkeluarwip. jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND a.jenis_keluar='3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_perbaikan = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_perbaikan = 0;
				
				// 3.2. dari tabel tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$row->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus+= $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus+= 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal_bagus+$saldo_awal_perbaikan+$jum_masuk_bagus+$jum_masuk_perbaikan-$jum_keluar_bagus-$jum_keluar_perbaikan;
				$auto_saldo_akhir_bagus = $saldo_awal_bagus+$jum_masuk_bagus-$jum_keluar_bagus;
				$auto_saldo_akhir_perbaikan = $saldo_awal_perbaikan+$jum_masuk_perbaikan-$jum_keluar_perbaikan;
				//-------------------------------------------------------------------------------------
				
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_unit_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_unit_jahit_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok, b.stok_bagus, b.stok_perbaikan
							FROM tm_stok_unit_jahit a, tm_stok_unit_jahit_warna b
							WHERE a.id = b.id_stok_unit_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ---------- 11-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir, c.auto_saldo_akhir_bagus, c.auto_saldo_akhir_perbaikan
											FROM tt_stok_opname_unit_jahit a 
											INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
											INNER JOIN tt_stok_opname_unit_jahit_detail_warna c ON b.id = c.id_stok_opname_unit_jahit_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
							$saldo_awal_bagus_warna = $hasilrow->auto_saldo_akhir_bagus;
							$saldo_awal_perbaikan_warna = $hasilrow->auto_saldo_akhir_perbaikan;
						}
						else {
							$saldo_awal_warna = 0;
							$saldo_awal_bagus_warna = 0;
							$saldo_awal_perbaikan_warna = 0;
						}
						
						// 2. hitung keluar bln ini
						// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna = 0;
						
						// 2.1.2 dari tabel tm_sjmasukwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_perbaikan_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_perbaikan_warna = 0;
						
						// 2.2. dari tabel tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna+= $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna+= 0;
						
						// 3. hitung masuk bln ini
						// 3.1.1 hitung dari tm_sjkeluarwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND a.jenis_keluar <>'3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna= 0;
						
						// 3.1.2 hitung dari tm_sjkeluarwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND a.jenis_keluar ='3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_perbaikan_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_perbaikan_warna= 0;
						
						// 3.2. dari tabel tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna+= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna+= 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_bagus_warna+$saldo_awal_perbaikan_warna+$jum_masuk_bagus_warna+$jum_masuk_perbaikan_warna-$jum_keluar_bagus_warna-$jum_keluar_perbaikan_warna;
						$auto_saldo_akhir_bagus_warna = $saldo_awal_bagus_warna+$jum_masuk_bagus_warna-$jum_keluar_bagus_warna;
						$auto_saldo_akhir_perbaikan_warna = $saldo_awal_perbaikan_warna+$jum_masuk_perbaikan_warna-$jum_keluar_perbaikan_warna;
						
						//$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna;
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'stok_opname_bgs'=> $rowxx->jum_bagus,
									'stok_opname_perbaikan'=> $rowxx->jum_perbaikan,
									
									// 11-03-2015
									'auto_saldo_akhir_warna'=> $auto_saldo_akhir_warna,
									'auto_saldo_akhir_bagus_warna'=> $auto_saldo_akhir_bagus_warna,
									'auto_saldo_akhir_perbaikan_warna'=> $auto_saldo_akhir_perbaikan_warna
								);
					}
				}
				else
					$detail_warna = ''; */
				
				// 22-12-2014
				if ($row->tgl_so != '') {
					$pisah1 = explode("-", $row->tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
				}
				else
					$tgl_so = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id_unit'=> $row->id_unit,
										'kode_unit'=> $kode_unit,
										'nama_unit'=> $nama_unit,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'tgl_so'=> $tgl_so,
										'nama_bln'=> $nama_bln,
										'jenis_perhitungan_stok'=> $row->jenis_perhitungan_stok,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  // 07-11-2015
  function savesounitjahit($id_so, $tgl_so, $unit_jahit, $jenis_perhitungan_stok, $id_itemnya, $id_brg_wip, 
		$id_warna, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  //-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			// 11-03-2015
			//$auto_saldo_akhir_warna[$xx] = trim($auto_saldo_akhir_warna[$xx]);
			
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
		// ---------------------------------------------------------------------
		$totalxx = $qtytotalstokfisik;
	
		//$totalxx = $stok_fisik;
			
		// ambil id detail id_stok_opname_unit_jahit_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail 
					where id_brg_wip = '$id_brg_wip' 
					AND id_stok_opname_unit_jahit = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
			
		//echo $id_itemnya."<br>";
		
		// 04-02-2016 SKRIP DIBAWAH INI GA DIPAKE LG
		
		// ======== update stoknya! =============
			// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
		
			//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
		/*	$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '1' ";
			$query3	= $this->db->query($sql3);
						// AND a.tgl_bonm >= '$tglawalplus'
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_bgs = $hasilrow->jum_masuk;
							
				if ($masuk_bgs == '')
					$masuk_bgs = 0;
			}
			else
				$masuk_bgs = 0;
						
			//2. hitung brg masuk retur brg wip dari QC, dari tm_sjkeluarwip
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.=" AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_returbrgwip = $hasilrow->jum_masuk;
							
				if ($masuk_returbrgwip == '')
					$masuk_returbrgwip = 0;
			}
			else
				$masuk_returbrgwip = 0;
						
			//3. hitung brg masuk pengembalian dari QC, dari tm_bonmkeluarcutting
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_pengembalian = $hasilrow->jum_masuk;
							
				if ($masuk_pengembalian == '')
					$masuk_pengembalian = 0;
			}
			else
				$masuk_pengembalian = 0;
						
			$jum_masuk = $masuk_bgs+$masuk_returbrgwip+$masuk_pengembalian;
			// ------------------------------------------ END MASUK --------------------------------
						
			// 4. hitung brg keluar bagus, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs = $hasilrow->jum_keluar;
							
				if ($keluar_bgs == '')
					$keluar_bgs = 0;
			}
			else
				$keluar_bgs = 0;
						
			// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_perbaikan = $hasilrow->jum_keluar;
							
				if ($keluar_perbaikan == '')
					$keluar_perbaikan = 0;
			}
			else
				$keluar_perbaikan = 0;
						
			// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
				if ($keluar_retur_bhnbaku == '')
					$keluar_retur_bhnbaku = 0;
			}
			else
				$keluar_retur_bhnbaku = 0;
			
			// 30-10-2015
			// 7. hitung brg keluar bgs ke gudang jadi, dari tm_sjmasukgudangjadi
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs_gudangjadi = $hasilrow->jum_keluar;
							
				if ($keluar_bgs_gudangjadi == '')
					$keluar_bgs_gudangjadi = 0;
			}
			else
				$keluar_bgs_gudangjadi = 0;
						
			$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku+$keluar_bgs_gudangjadi;
			// -------------------------------- END BARANG KELUAR -----------------------------------------------
			
			$jum_stok_akhir = $jum_masuk-$jum_keluar;
			$totalxx = $totalxx + $jum_stok_akhir; */
			
			//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_jahit 
							WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$unit_jahit' ");
					if ($query3->num_rows() == 0){
						$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrow	= $seqxx->row();
							$id_stok	= $seqrow->id+1;
						}else{
							$id_stok	= 1;
						}
						
						// 30-10-2015:
						// pada waktu input SO, field stok_bagus dan stok_perbaikan di-nol-kan lagi (utk duta). nanti diterapkan juga di perusahaan lain
						
						$data_stok = array(
							'id'=>$id_stok,
							'id_brg_wip'=>$id_brg_wip,
							'id_unit'=>$unit_jahit,
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl,
							'stok_bagus'=>0,
							'stok_perbaikan'=>0
							);
						$this->db->insert('tm_stok_unit_jahit', $data_stok);
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
												
						$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$totalxx', 
						stok_bagus='0',
						stok_perbaikan='0', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_unit='$unit_jahit' ");
					}
					
			// 04-02-2016
			// stok_unit_jahit_warna
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					
					$totalxx = $stok_fisik[$xx];
					
					//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_unit_jahit='$id_stok' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_unit_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_unit_jahit'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_jahit='$id_stok' ");
					}
					
					if ($id_itemnya != '0') {
						$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."'
							WHERE id_stok_opname_unit_jahit_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
					}
				} // end for
		
		// 09-11-2015
		 //$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$totalxx', 
		//			status_approve='t' where id = '$iddetail' ");
		
		if ($id_itemnya != '0') {
			$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$qtytotalstokfisik',
					status_approve='t' where id = '$iddetail' ");
		}
		
		if ($id_itemnya == '0') {
			$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailnew	= $seqrow->id+1;
			}else{
				$iddetailnew	= 1;
			}
			
			$databaru = array(
						'id'=>$iddetailnew,
						'id_stok_opname_unit_jahit'=>$id_so,
						'id_brg_wip'=>$id_brg_wip, 
						'stok_awal'=>0,
						'tot_jum_stok_opname'=>$qtytotalstokfisik,
						//'jum_stok_opname'=>$qtytotalstokfisik,
						'status_approve'=> 't'
					);
		   $this->db->insert('tt_stok_opname_unit_jahit_detail',$databaru);
		   
		   // insert ke tabel SO warna
		   for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);				
				
				$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}
						
						$tt_stok_opname_unit_jahit_detail_warna	= array(
								 'id'=>$idbaru,
								 'id_stok_opname_unit_jahit_detail'=>$iddetailnew,
								 'id_warna'=>$id_warna[$xx],
								 'jum_stok_opname'=>$stok_fisik[$xx],
								 'saldo_akhir'=>0
							);
							$this->db->insert('tt_stok_opname_unit_jahit_detail_warna',$tt_stok_opname_unit_jahit_detail_warna);
			} // end for
		} // end if item baru
			 // ====================================================================================
  }
  
  function get_detail_stokbrgbaru_unitjahit($id_so) {
	  // ambil id_gudang dari acuan id_so
	  $sqlxx = " SELECT id_unit FROM tt_stok_opname_unit_jahit WHERE id='$id_so' ";
	  $queryxx = $this->db->query($sqlxx);
	
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_unit = $hasilxx->id_unit;
		}
		else
			$id_unit = 0;
		
		$sql = " SELECT b.id as id_brg_wip, b.kode_brg, b.nama_brg, a.id as id_stok
							FROM tm_barang_wip b 
							LEFT JOIN tm_stok_unit_jahit a ON a.id_brg_wip = b.id
							WHERE a.id_unit = '$id_unit' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select b.id_brg_wip FROM tt_stok_opname_unit_jahit a
							INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit 
							WHERE a.id = '$id_so')
							ORDER BY b.kode_brg  ";
	  
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// stok terkini
				/*$sql2	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit WHERE id_brg_wip = '$row->id_brg_wip' ");
				if($sql2->num_rows() > 0) {
					$hasil2	= $sql2->row();
					$stok	= $hasil2->stok;
				}else{
					$stok	= 0;
				} */
				
				// stok warna
				$sql2 = " SELECT a.stok, a.id_warna, b.nama as nama_warna FROM tm_stok_unit_jahit_warna a
						INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_stok_unit_jahit = '$row->id_stok'
						ORDER BY b.nama";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					$detail_warna = array();			
					foreach ($hasil2 as $row2) {
						$detail_warna[] = array(
									'id_warna'=> $row2->id_warna,
									'nama_warna'=> $row2->nama_warna,
									'saldo_akhir'=> $row2->stok,
									'stok_opname'=> $row2->stok
								);
					}
				}
				
				$detail_bahan[] = array('id'=> 0,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  // 09-11-2015
  function get_sounitpacking($id_so) {
		$query	= $this->db->query(" SELECT a.id as id_header, a.id_unit, a.bulan, a.tahun, a.tgl_so, a.jenis_perhitungan_stok, 
					b.*, d.kode_brg, d.nama_brg
					FROM tt_stok_opname_unit_packing_detail b INNER JOIN tt_stok_opname_unit_packing a ON b.id_stok_opname_unit_packing = a.id 
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.id = '$id_so' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
		
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_packing WHERE id = '$row->id_unit' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_unit_packing_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_stok_opname_unit_packing_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_packing_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_unit_packing a INNER JOIN tm_stok_unit_packing_warna b ON a.id = b.id_stok_unit_packing
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_unit = '$row->id_unit'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				if ($row->tgl_so != '') {
					$pisah1 = explode("-", $row->tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
				}
				else
					$tgl_so = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id_unit'=> $row->id_unit,
										'kode_unit'=> $kode_unit,
										'nama_unit'=> $nama_unit,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'tgl_so'=> $tgl_so,
										'nama_bln'=> $nama_bln,
										'jenis_perhitungan_stok'=>$row->jenis_perhitungan_stok,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function get_detail_stokbrgbaru_unitpacking($id_so) {
	  // ambil id_unit dari acuan id_so
	  $sqlxx = " SELECT id_unit FROM tt_stok_opname_unit_packing WHERE id='$id_so' ";
	  $queryxx = $this->db->query($sqlxx);
	
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_unit = $hasilxx->id_unit;
		}
		else
			$id_unit = 0;
		
		$sql = " SELECT b.id as id_brg_wip, b.kode_brg, b.nama_brg, a.id as id_stok
							FROM tm_barang_wip b 
							LEFT JOIN tm_stok_unit_packing a ON a.id_brg_wip = b.id
							WHERE a.id_unit = '$id_unit' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select b.id_brg_wip FROM tt_stok_opname_unit_packing a
							INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing 
							WHERE a.id = '$id_so')
							ORDER BY b.nama_brg  ";
	  
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// stok warna
				$sql2 = " SELECT a.stok, a.id_warna, b.nama as nama_warna FROM tm_stok_unit_packing_warna a
						INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_stok_unit_packing = '$row->id_stok'
						ORDER BY b.nama";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					$detail_warna = array();			
					foreach ($hasil2 as $row2) {
						$detail_warna[] = array(
									'id_warna'=> $row2->id_warna,
									'nama_warna'=> $row2->nama_warna,
									'saldo_akhir'=> $row2->stok,
									'stok_opname'=> $row2->stok
								);
					}
				}
				
				$detail_bahan[] = array('id'=> 0,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  // 12-11-2015
   function get_mutasi_unit($date_from, $date_to, $unit_jahit,$tahun,$bulan, $tgldari,$tglke) {		
	  /* 
	   	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			*/
	   
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('t',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		$tgldariback = $thn_query."-".$bln_query."-".$firstDay;
		
		$timeStamp            =    mktime(0,0,0,$bln_query,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('t',$lastDayTimeStamp);// Find last day of the month
		
		$tglkeback = $thn_query."-".$bln_query."-".$lastDay;
		//$datekeback=date("Y-m-t", strtotime($datekeback));
				//print_r($tglkeback);print_r($tgldariback);
	  
		// 1. ambil data2 unit jahit dari rentang tanggal yg dipilih
		$filter = "";
		$filter2 = "";
		$filter3 = "";
		$filter4 = "";
		if ($unit_jahit != '0') {
			$filter = " AND a.id_unit_jahit = '$unit_jahit' ";
			$filter2= " AND a.id_unit = '$unit_jahit' ";
			$filter3 = " AND a.id_unit_asal = '$unit_jahit' ";
			$filter4 = " AND a.id_unit_tujuan = '$unit_jahit' ";
		}
		
		$sql = "SELECT a.id_unit_jahit, a.kode_unit FROM (
				select a.id_unit_jahit, b.kode_unit FROM tm_sjmasukwip a INNER JOIN tm_unit_jahit b ON a.id_unit_jahit = b.id
				WHERE a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.id_unit_jahit <> '0' GROUP BY a.id_unit_jahit, b.kode_unit
				
				UNION SELECT a.id_unit_jahit, b.kode_unit FROM tm_sjkeluarwip a INNER JOIN tm_unit_jahit b ON a.id_unit_jahit = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.id_unit_jahit <> '0' GROUP BY a.id_unit_jahit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_jahit, b.kode_unit FROM tm_bonmkeluarcutting a INNER JOIN tm_unit_jahit b ON a.id_unit = b.id
				WHERE a.tgl_bonm >='".$tgldari."'  
				AND a.tgl_bonm <='".$tglke."' ".$filter2." AND a.id_unit <> '0' GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_jahit, b.kode_unit FROM tm_sjmasukbhnbakupic a INNER JOIN tm_unit_jahit b ON a.id_unit = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter2." AND a.id_unit <> '0' GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit_jahit, b.kode_unit FROM tm_sjmasukgudangjadi a INNER JOIN tm_unit_jahit b ON a.id_unit_jahit = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.id_unit_jahit <> '0' GROUP BY a.id_unit_jahit, b.kode_unit
				
				UNION SELECT a.id_unit_asal as id_unit_jahit, b.kode_unit FROM tm_sjantarunit a INNER JOIN tm_unit_jahit b ON a.id_unit_asal = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter3." GROUP BY a.id_unit_asal, b.kode_unit
				UNION SELECT a.id_unit_tujuan as id_unit_jahit, b.kode_unit FROM tm_sjantarunit a INNER JOIN tm_unit_jahit b ON a.id_unit_tujuan = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter4." GROUP BY a.id_unit_tujuan, b.kode_unit
				
				UNION SELECT a.id_unit_asal as id_unit_jahit, b.kode_unit FROM tm_sjkeluarother_unit_jahit a INNER JOIN tm_unit_jahit b ON a.id_unit_asal = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter3." GROUP BY a.id_unit_asal, b.kode_unit
				UNION SELECT a.id_unit_tujuan as id_unit_jahit, b.kode_unit FROM tm_sjmasukother_unit_jahit a INNER JOIN tm_unit_jahit b ON a.id_unit_tujuan = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter4." GROUP BY a.id_unit_tujuan, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_jahit, b.kode_unit FROM tt_stok_opname_unit_jahit a INNER JOIN tm_unit_jahit b ON a.id_unit = b.id
				WHERE a.bulan='$bln_query' AND a.tahun='$t'  ".$filter2." GROUP BY a.id_unit, b.kode_unit
				UNION SELECT a.id_unit as id_unit_jahit, b.kode_unit FROM tt_stok_opname_unit_jahit a INNER JOIN tm_unit_jahit b ON a.id_unit = b.id
				WHERE a.bulan='$bulan' AND a.tahun='$tahun'  ".$filter2." GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_jahit, b.kode_unit FROM tt_adjustment_unit_jahit a INNER JOIN tm_unit_jahit b ON a.id_unit = b.id
				WHERE a.bulan='$bln_query' AND a.tahun='$t'  ".$filter2." GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_jahit, b.kode_unit FROM tt_adjustment_unit_jahit a INNER JOIN tm_unit_jahit b ON a.id_unit = b.id
				WHERE a.bulan='$bulan' AND a.tahun='$tahun'  ".$filter2." GROUP BY a.id_unit, b.kode_unit
				)
				a GROUP BY a.id_unit_jahit, a.kode_unit ORDER BY a.kode_unit";
				// AND a.status_approve='t'
				
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$total_so_rupiah=0; $total_sisa_stok_rupiah=0;
			foreach ($hasil as $row) {
				// 22-03-2014 -----------------------------------------------------
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = "SELECT a.id_brg_wip FROM (
					select b.id_brg_wip FROM tm_sjmasukwip a 
					INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_jahit = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjkeluarwip a 
					INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_jahit = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_bonmkeluarcutting a 
					INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_bonm >='".$tgldari."' 
					AND a.tgl_bonm <='".$tglke."' AND a.id_unit = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjmasukbhnbakupic a 
					INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjmasukgudangjadi a 
					INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_jahit = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjantarunit a 
					INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_asal = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					UNION SELECT b.id_brg_wip FROM tm_sjantarunit a 
					INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_tujuan = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjkeluarother_unit_jahit a 
					INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_asal = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					UNION SELECT b.id_brg_wip FROM tm_sjmasukother_unit_jahit a 
					INNER JOIN tm_sjmasukother_unit_jahit_detail b ON a.id = b.id_sjmasukother_unit_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_tujuan = '$row->id_unit_jahit' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tt_stok_opname_unit_jahit a 
					INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id=b.id_stok_opname_unit_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.id_unit = '$row->id_unit_jahit' AND b.jum_stok_opname <> 0 
					GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tt_adjustment_unit_jahit a 
					INNER JOIN tt_adjustment_unit_jahit_detail b ON a.id=b.id_adjustment_unit_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bulan' AND a.tahun='$tahun' 
					AND a.id_unit = '$row->id_unit_jahit' AND b.jum_adjustment <> 0 
					GROUP BY b.id_brg_wip
					)
					a GROUP BY a.id_brg_wip
					ORDER BY id_brg_wip";
					// AND a.status_approve='t'
				/*
				 * UNION SELECT kode_brg_jadi FROM tm_stok_unit_jahit WHERE kode_unit='$row->kode_unit_jahit'
					GROUP BY kode_brg_jadi
				 */ 
				$query2	= $this->db->query($sql2);
				
				/*$jum_keluar1 = 0;
				$jum_keluar2 = 0;
				$jum_keluar_lain1 = 0;
				$jum_keluar_lain2 = 0;
				$jum_masuk = 0;
				$jum_masuk_lain1 = 0;
				$jum_masuk_lain2 = 0;
				$jum_masuk_lain3 = 0;
				$jum_masuk_lain4 = 0; 
				*/
				$jum_saldo_awal = 0;
				$jum_masuk = 0;
				$jum_keluar = 0;
				$jum_stok_akhir = 0;
				$jum_so = 0;
				$selisih_bgs = 0;
				$selisih_perbaikan = 0;
				$sisa_stok = 0;
				
				// 04-02-2016
				$saldo_awal_warna = array();
				$saldo_akhir_warna = array();
				
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg_wip = $hasilrow->kode_brg;
							$nama_brg_wip = $hasilrow->nama_brg;
						}
						else {
							$kode_brg_wip = '';
							$nama_brg_wip = '';
						}
							
							
							
						// SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA. koreksi: ambil dari saldo akhir bulan sebelumnya (kebijakan duta)
							$queryx	= $this->db->query(" SELECT b.id, b.jum_stok_opname, b.saldo_akhir, b.tot_jum_stok_opname
											FROM tt_stok_opname_unit_jahit a 
											INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
											WHERE b.id_brg_wip = '$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' ");
									// AND b.status_approve = 't' AND a.status_approve = 't'
						
						
						if ($queryx->num_rows() > 0){
							
							$hasilrow = $queryx->row();
							// 07-12-2015. SALDO AWAL PAKE ACUAN PERHITUNGAN SALDO AKHIR BULAN SEBELUMNYA
							$saldo_awal = $hasilrow->tot_jum_stok_opname;
						//	$saldo_awal = $hasilrow->jum_stok_opname;
							$id_so_detail = $hasilrow->id;
							
							// 04-02-2016 ambil perwarna
							$sqlx2 = " SELECT a.saldo_akhir,a.jum_stok_opname, b.nama FROM tt_stok_opname_unit_jahit_detail_warna a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE id_stok_opname_unit_jahit_detail = '$id_so_detail' ";
							$queryx2	= $this->db->query($sqlx2);
							if ($queryx2->num_rows() > 0){
								$hasilx2 = $queryx2->result();
								
								foreach ($hasilx2 as $rowx2) {
									//$saldo_awal2 = $rowx2->saldo_akhir;
									$saldo_awal2 = $rowx2->jum_stok_opname;
									$nama_warna = $rowx2->nama;
									$saldo_awal_warna[] = array(		'nama_warna'=> $nama_warna,
																	'saldo_awal'=> $saldo_awal2
																	);
								}
							}
							
							// 06-02-2016. algoritma:
							/* 1. bikin variabel array saldo_akhir_warna. isinya adalah nama_warna dan saldo_akhir
							   2.
							    
							*/
						}
						else {
							$saldo_awal = 0;
							$saldo_awal_warna = '';
						}

					
							$queryxyz2	= $this->db->query(" SELECT b.id, b.jum_adjustment
											FROM tt_adjustment_unit_jahit a 
											INNER JOIN tt_adjustment_unit_jahit_detail b ON a.id = b.id_adjustment_unit_jahit
											WHERE b.id_brg_wip = '$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' ");
	
						if ($queryxyz2->num_rows() > 0){
							
							$hasilrowxyz2 = $queryx->row();
							$saldo_awal_jum_adjustment = $hasilrowxyz2->tot_jum_adjustment;
							$id_so_detail_jum_adjustment = $hasilrowxyz2->id;
							
	
							$sqlxyz22 = " SELECT a.jum_adjustment, b.nama FROM tt_adjustment_unit_jahit_detail_warna a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE id_adjustment_unit_jahit_detail = '$id_so_detail_jum_adjustment' ";
							$queryxyz22	= $this->db->query($sqlxyz22);
							if ($queryxyz22->num_rows() > 0){
								$hasilxyz22 = $queryxyz22->result();
								
								foreach ($hasilxyz22 as $rowxyz22) {
									$saldo_awal_jum_adjustment_warna = $rowxyz22->jum_adjustment;
									$nama_warna_jum_adjustment = $rowxyz22->nama;
									$saldo_awal_warna_jum_adjustment[] = array(		'nama_warna'=> $nama_warna_jum_adjustment,
																	'saldo_awal'=> $saldo_awal_warna_jum_adjustment
																	);
								}
							}
							
						}
						else {
							$saldo_awal_jum_adjustment = 0;
							$saldo_awal_jum_adjustment_warna = '';
						}
						// 09-03-2015, sementara pake SO dulu, nanti diganti jadi dari auto saldo akhir
						// 18-04-2015 PAKE ACUAN SO LAGI. 12-11-2015 sementara pake acuan SO dulu
						$jum_saldo_awal = $saldo_awal+$saldo_awal_jum_adjustment;
						
						// 08-04-2015, diganti pake auto saldo
						//$jum_saldo_awal = $auto_saldo_awal_bagus+$auto_saldo_awal_perbaikan;
						
						//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari gudang QC, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0;
						
						//3. hitung brg masuk pengembalian dari gudang pengadaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0;
						
						// 07-12-2015. hitung brg masuk lain2 dari tm_bonmkeluarcutting jenis_keluar = '3'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain = $hasilrow->jum_masuk;
							
							if ($masuk_lain == '')
								$masuk_lain = 0;
						}
						else
							$masuk_lain = 0;
						
						// 14-01-2016
						// hitung brg masuk lain2, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_keluar = '4' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain2 = $hasilrow->jum_masuk;
							
							if ($masuk_lain2 == '')
								$masuk_lain2 = 0;
						}
						else
							$masuk_lain2 = 0;
						
						// 1.1. 14-01-2016. tambahan barang masuk bagus dari antar unit
						// koreksi 15-01-2016, masuk lain2
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjantarunit a 
									INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_tujuan = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain3 = $hasilrow->jum_masuk;
							
							if ($masuk_lain3 == '')
								$masuk_lain3 = 0;
						}
						else
							$masuk_lain3 = 0;
							
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_unit_jahit a 
									INNER JOIN tm_sjmasukother_unit_jahit_detail b ON a.id = b.id_sjmasukother_unit_jahit
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_tujuan = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_other = $hasilrow->jum_masuk;
							
							if ($masuk_other == '')
								$masuk_other = 0;
						}
						else
							$masuk_other = 0;
						
						// 07-12-2015 masuk_pengembalian ga dipake
						//$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian+$masuk_lain;
						$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_lain+$masuk_lain2+$masuk_lain3+$masuk_other;
						
						$masuk_lain+=$masuk_lain2+$masuk_lain3+$masuk_other;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						// 7. KIRIM LANGSUNG KE GUDANG JADI dari tm_sjmasukgudangjadi
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit'  AND a.jenis_masuk='1'");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_gudangjadi = $hasilrow->jum_keluar;
							
							if ($keluar_gudangjadi == '')
								$keluar_gudangjadi = 0;
						}
						else
							$keluar_gudangjadi = 0;
							
						// 07-12-2015, keluar lain dari tm_sjantarunit
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjantarunit a 
									INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_asal = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_lain = $hasilrow->jum_keluar;
							
							if ($keluar_lain == '')
								$keluar_lain = 0;
						}
						else
							$keluar_lain = 0;
						
						// 15-01-2016 hitung brg keluar lain2, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '5' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_lain2 = $hasilrow->jum_keluar;
							
							if ($keluar_lain2 == '')
								$keluar_lain2 = 0;
						}
						else
							$keluar_lain2 = 0;
							
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk='3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_unit_packing = $hasilrow->jum_keluar;
							
							if ($keluar_unit_packing == '')
								$keluar_unit_packing = 0;
						}
						else
							$keluar_unit_packing = 0;
							
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_unit_jahit a 
									INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_asal = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_other = $hasilrow->jum_keluar;
							
							if ($keluar_other == '')
								$keluar_other = 0;
						}
						else
							$keluar_other = 0;
						
						
						$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku+$keluar_gudangjadi+$keluar_lain+$keluar_lain2+$keluar_unit_packing+$keluar_other;
						
						$keluar_lain+=$keluar_lain2+$keluar_other;
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						// 09-03-2015, $saldo_awal_bgs dan perbaikan diganti $auto_saldo_awal_bagus dan $auto_saldo_awal_perbaikan
						// nanti akhir maret diganti lagi jadi pake auto_saldo_awal_bagus
						//$stok_akhir_bgs = $auto_saldo_awal_bagus + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						//$stok_akhir_perbaikan = $auto_saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						
						// 08-04-2015 pake auto saldo
						
						// 18-04-2015 PAKE ACUAN SO LAGI. 12-11-2015: sementara acuan SO aja dulu. lg dipikirin utk acuan saldo akhir
						//$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku - $keluar_gudangjadi;
						//$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
						//$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
						$jum_saldo_akhir = $saldo_awal + $jum_masuk - $jum_keluar;
						
						// ============================= END STOK AKHIR ==============================================1
						
						
						
						
						
						//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >= '$tgldariback'
									AND a.tgl_bonm <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgsback = $hasilrow->jum_masuk;
							
						//	print_r($masuk_bgsback);
							
							if ($masuk_bgsback == '')
								$masuk_bgsback = 0;
						}
						else
							$masuk_bgsback = 0;
							
						
						//2. hitung brg masuk retur brg jadi dari gudang QC, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadiback = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadiback == '')
								$masuk_returbrgjadiback = 0;
						}
						else
							$masuk_returbrgjadiback = 0;
						
						//3. hitung brg masuk pengembalian dari gudang pengadaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >= '$tgldariback'
									AND a.tgl_bonm <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalianback = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalianback == '')
								$masuk_pengembalianback = 0;
						}
						else
							$masuk_pengembalianback = 0;
						
						//4. 07-12-2015. hitung brg masuk lain2 dari tm_bonmkeluarcutting jenis_keluar = '3'
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >= '$tgldariback'
									AND a.tgl_bonm <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lainback = $hasilrow->jum_masuk;
							
							if ($masuk_lainback == '')
								$masuk_lainback = 0;
						}
						else
							$masuk_lainback = 0;
						
						// 14-01-2016
						//5. hitung brg masuk lain2, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE a.tgl_sj >= '$tgldariback' 
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_keluar = '4' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain2back = $hasilrow->jum_masuk;
							
							if ($masuk_lain2back == '')
								$masuk_lain2back = 0;
						}
						else
							$masuk_lain2back = 0;
						
						//6. 1.1. 14-01-2016. tambahan barang masuk bagus dari antar unit
						// koreksi 15-01-2016, masuk lain2
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjantarunit a 
									INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_tujuan = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain3back = $hasilrow->jum_masuk;
							
							if ($masuk_lain3back == '')
								$masuk_lain3back = 0;
						}
						else
							$masuk_lain3back = 0;
							
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_unit_jahit a 
									INNER JOIN tm_sjmasukother_unit_jahit_detail b ON a.id = b.id_sjmasukother_unit_jahit
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_tujuan = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_otherback = $hasilrow->jum_masuk;
							
							if ($masuk_otherback == '')
								$masuk_otherback = 0;
						}
						else
							$masuk_otherback = 0;
						
						// 07-12-2015 masuk_pengembalian ga dipake
						//$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian+$masuk_lain;
						$jum_masukback = $masuk_bgsback+$masuk_returbrgjadiback+$masuk_lainback+$masuk_lain2back+$masuk_lain3back+$masuk_otherback;
						// +$masuk_pengembalianback
						$masuk_lainback+=$masuk_lain2back+$masuk_lain3back+$masuk_otherback;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgsback = $hasilrow->jum_keluar;
							
							if ($keluar_bgsback == '')
								$keluar_bgsback = 0;
						}
						else
							$keluar_bgsback = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikanback = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikanback == '')
								$keluar_perbaikanback = 0;
						}
						else
							$keluar_perbaikanback = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									WHERE a.tgl_sj >= '$tgldariback' 
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbakuback = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbakuback == '')
								$keluar_retur_bhnbakuback = 0;
						}
						else
							$keluar_retur_bhnbakuback = 0;
						
						// 7. KIRIM LANGSUNG KE GUDANG JADI dari tm_sjmasukgudangjadi
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE a.tgl_sj >= '$tgldariback' 
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk='1'");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_gudangjadiback = $hasilrow->jum_keluar;
							
							if ($keluar_gudangjadiback == '')
								$keluar_gudangjadiback = 0;
						}
						else
							$keluar_gudangjadiback = 0;
							
						//8. 07-12-2015, keluar lain dari tm_sjantarunit
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjantarunit a 
									INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									WHERE a.tgl_sj >= '$tgldariback' 
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_asal = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_lainback = $hasilrow->jum_keluar;
							
							if ($keluar_lainback == '')
								$keluar_lainback = 0;
						}
						else
							$keluar_lainback = 0;
						
						//9. 15-01-2016 hitung brg keluar lain2, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '5' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_lain2back = $hasilrow->jum_keluar;
							
							if ($keluar_lain2back == '')
								$keluar_lain2back = 0;
						}
						else
							$keluar_lain2back = 0;
							//10.
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk='3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_unit_packingback = $hasilrow->jum_keluar;
							
							if ($keluar_unit_packingback == '')
								$keluar_unit_packingback = 0;
						}
						else
							$keluar_unit_packingback = 0;
							
									$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarother_unit_jahit a 
									INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
									WHERE a.tgl_sj >= '$tgldariback'
									AND a.tgl_sj <= '$tglkeback' AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_asal = '$row->id_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_otherback = $hasilrow->jum_masuk;
							
							if ($keluar_otherback == '')
								$keluar_otherback = 0;
						}
						else
							$keluar_otherback = 0;
						
						$jum_keluarback = $keluar_bgsback+$keluar_perbaikanback+$keluar_retur_bhnbakuback+$keluar_gudangjadiback+$keluar_lainback+$keluar_lain2back+$keluar_unit_packingback+$keluar_otherback;
						
						$keluar_lainback+=$keluar_lain2back+$keluar_otherback;
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						// 09-03-2015, $saldo_awal_bgs dan perbaikan diganti $auto_saldo_awal_bagus dan $auto_saldo_awal_perbaikan
						// nanti akhir maret diganti lagi jadi pake auto_saldo_awal_bagus
						//$stok_akhir_bgs = $auto_saldo_awal_bagus + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						//$stok_akhir_perbaikan = $auto_saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						
						// 08-04-2015 pake auto saldo
						
						// 18-04-2015 PAKE ACUAN SO LAGI. 12-11-2015: sementara acuan SO aja dulu. lg dipikirin utk acuan saldo akhir
						//$stok_akhir_bgs = $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku - $keluar_gudangjadi;
						//$stok_akhir_perbaikan = $masuk_returbrgjadi - $keluar_perbaikan;
						//$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
						$jum_saldo_akhir = $saldo_awal + $jum_masuk - $jum_keluar;
						
						// ============================= END STOK AKHIR ==============================================2
						
						$queryxyx	= $this->db->query(" SELECT b.id, b.jum_adjustment FROM tt_adjustment_unit_jahit a 
												INNER JOIN tt_adjustment_unit_jahit_detail b ON a.id = b.id_adjustment_unit_jahit 
												WHERE b.id_brg_wip ='$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' ");
									if($queryxyx->num_rows() > 0 ){
										$hasilrowxyz=$queryxyx->row();
										$jum_adjustment=$queryxyx->jum_adjustment;
										
										}
										else
										$jum_adjustment='0';
						
												
						// STOK OPNAME. DI DUTA GA PAKE PER WARNA SO-NYA
						$sql = "SELECT a.jenis_perhitungan_stok, a.tgl_so, b.id, b.jum_stok_opname 
									FROM tt_stok_opname_unit_jahit a 
									INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit 
									WHERE b.id_brg_wip = '$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_jahit'
									AND a.bulan = '$bulan' AND a.tahun = '$tahun'  ";
						// AND b.status_approve = 't'
														
						$query3	= $this->db->query($sql);
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$id_so_detail = $hasilrow->id;
							$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
							$tgl_so = $hasilrow->tgl_so;
							$jum_so = $hasilrow->jum_stok_opname;
							
						// ------------25-01-2016 perhitungkan tambah/kurang dari masuk/keluar barang, dihitung dari tgl so-----
							
							//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql3.=" a.tgl_bonm > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql3.=" a.tgl_bonm >= '$tgl_so' ";
						$sql3.=" AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
								AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '1' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgsx = $hasilrow->jum_masuk;
							
							if ($masuk_bgsx == '')
								$masuk_bgsx = 0;
						}
						else
							$masuk_bgsx = 0;
						
						//2. hitung brg masuk retur brg jadi dari gudang QC, dari tm_sjkeluarwip
						$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
								
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_keluar = '3' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadix = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadix == '')
								$masuk_returbrgjadix = 0;
						}
						else
							$masuk_returbrgjadix = 0;
					
						//3. hitung brg masuk pengembalian dari gudang pengadaan, dari tm_bonmkeluarcutting
						$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
									//a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_bonm > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_bonm >= '$tgl_so' ";
						$sql3.=" AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '2' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalianx = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalianx == '')
								$masuk_pengembalianx = 0;
						}
						else
							$masuk_pengembalianx = 0;
						
						//4. 07-12-2015. hitung brg masuk lain2 dari tm_bonmkeluarcutting jenis_keluar = '3'
						$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
							//a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_bonm > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_bonm >= '$tgl_so' ";
						$sql3.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' AND a.jenis_keluar = '3' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lainx = $hasilrow->jum_masuk;
							
							if ($masuk_lainx == '')
								$masuk_lainx = 0;
						}
						else
							$masuk_lainx = 0;
						
						// 14-01-2016
						// 5. hitung brg masuk lain2, dari tm_sjkeluarwip
						$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
									//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_keluar = '4' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain2x = $hasilrow->jum_masuk;
							
							if ($masuk_lain2x == '')
								$masuk_lain2x = 0;
						}
						else
							$masuk_lain2x = 0;
						
						// 6. 14-01-2016. tambahan barang masuk bagus dari antar unit
						// koreksi 15-01-2016, masuk lain2
						$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjantarunit a 
									INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									WHERE ";
									//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_tujuan = '$row->id_unit_jahit' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain3x = $hasilrow->jum_masuk;
							
							if ($masuk_lain3x == '')
								$masuk_lain3x = 0;
						}
						else
							$masuk_lain3x = 0;
							
							$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_unit_jahit a 
									INNER JOIN tm_sjmasukother_unit_jahit_detail b ON a.id = b.id_sjmasukother_unit_jahit
									WHERE ";
									//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_tujuan = '$row->id_unit_jahit' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_otherx = $hasilrow->jum_masuk;
							
							if ($masuk_otherx == '')
								$masuk_otherx = 0;
						}
						else
							$masuk_otherx = 0;
						
						// 07-12-2015 masuk_pengembalian ga dipake
						//$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian+$masuk_lain;
						$jum_masukx = $masuk_bgsx+$masuk_returbrgjadix+$masuk_lainx+$masuk_lain2x+$masuk_lain3x+$masuk_otherx;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							// $sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
							
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '1' ";
						
						//if ($kode_brg_wip == 'DGG4219')
						//	echo $jenis_perhitungan_stok." ".$sql3."<br>";
						
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgsx = $hasilrow->jum_keluar;
							
							if ($keluar_bgsx == '')
								$keluar_bgsx = 0;
						}
						else
							$keluar_bgsx = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						//	$sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '2' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikanx = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikanx == '')
								$keluar_perbaikanx = 0;
						}
						else
							$keluar_perbaikanx = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						//	$sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit = '$row->id_unit_jahit' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbakux = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbakux == '')
								$keluar_retur_bhnbakux = 0;
						}
						else
							$keluar_retur_bhnbakux = 0;
						
						// 7. KIRIM LANGSUNG KE GUDANG JADI dari tm_sjmasukgudangjadi
						$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						//	$sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_gudangjadix = $hasilrow->jum_keluar;
							
							if ($keluar_gudangjadix == '')
								$keluar_gudangjadix = 0;
						}
						else
							$keluar_gudangjadix = 0;
							
						// 07-12-2015, keluar lain dari tm_sjantarunit
						$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjantarunit a 
									INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						//	$sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
							
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_asal = '$row->id_unit_jahit' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_lainx = $hasilrow->jum_keluar;
							
							if ($keluar_lainx == '')
								$keluar_lainx = 0;
						}
						else
							$keluar_lainx = 0;
						
						// 15-01-2016 hitung brg keluar lain2, dari tm_sjmasukwip
						$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						//	$sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
							
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_jahit = '$row->id_unit_jahit' AND a.jenis_masuk = '5' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_lain2x = $hasilrow->jum_keluar;
							
							if ($keluar_lain2x == '')
								$keluar_lain2x = 0;
						}
						else
							$keluar_lain2x = 0;
							
							$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_unit_jahit a 
									INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
									WHERE ";
									//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_asal = '$row->id_unit_jahit' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_otherx = $hasilrow->jum_keluar;
							
							if ($keluar_otherx == '')
								$keluar_otherx = 0;
						}
						else
							$keluar_otherx = 0;
						
						$jum_keluarx = $keluar_bgsx+$keluar_perbaikanx+$keluar_retur_bhnbakux+$keluar_gudangjadix+$keluar_lainx+$keluar_lain2x+$keluar_otherx;
					
					$tambahkurang = $jum_masukx - $jum_keluarx;
					//echo $jum_masukx." ".$jum_keluarx." ".$tambahkurang."<br>";
					
					// 26-01-2016 qty SO ($jum_so) yg udh ditambahkurang ini ga usah diupdate ke database
					$jum_so = $jum_so + $tambahkurang;
					//--------------------------END 25-01-2016-----------------------------------------------------------
							
							// 07-12-2015, update saldo akhir di tabel stok opname nya
							$sqlupdate = " UPDATE tt_stok_opname_unit_jahit_detail SET 
							saldo_akhir = '$jum_saldo_akhir',
							tot_jum_stok_opname= '$jum_so'
							WHERE id = '$id_so_detail' ";
							$this->db->query($sqlupdate);
							
							// 25-01-2016
							$sqlupdate = " UPDATE tm_stok_unit_jahit SET stok = '$jum_so', stok_bagus = '0', stok_perbaikan = '0' 
									WHERE id_brg_wip = '$row2->id_brg_wip'
									AND id_unit='$row->id_unit_jahit' ";
							$this->db->query($sqlupdate);
						}
						else {
							$id_so_detail = 0;
							$jum_so = 0;
						}
						//$jum_so = $so_bgs+$so_perbaikan;
						//=========================================
						
						//$selisih_bgs = $so_bgs-$stok_akhir_bgs;
						//$selisih_perbaikan = $so_perbaikan-$stok_akhir_perbaikan;
						
						$selisih = $jum_so-$jum_saldo_akhir+$jum_adjustment;
						
						//23-05-2014 ambil data stok schedule dari tabel tm_stok_schedule
						/*$sqlxx = "SELECT qty FROM tm_stok_schedule_wip WHERE kode_brg_jadi = '$row2->kode_brg_jadi' 
								AND kode_unit = '$row->kode_unit_jahit'
								AND bulan='$bln1' AND tahun='$thn1' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$stok_schedule = $hasilxx->qty;
						}
						else
							$stok_schedule = 0;
						
						$sisa_stok = $jum_so-$stok_schedule;
						
						// 23-05-2014, harga HPP, pake dari tabel hpp_wip
						$sqlxx = "SELECT harga FROM tm_hpp_wip WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ";
						$queryxx	= $this->db->query($sqlxx);
						
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$hpp = $hasilxx->harga;
						}
						else
							$hpp = 0;
						
						// jumlah nominal rupiah SO
						$jum_so_rupiah = $jum_so*$hpp;
						$total_so_rupiah+= $jum_so_rupiah;
						
						// jumlah nominal sisa stok
						$sisa_stok_rupiah = $sisa_stok*$hpp;
						$total_sisa_stok_rupiah+= $sisa_stok_rupiah; */
																		
						$data_stok[] = array(		'kode_brg_wip'=> $kode_brg_wip,
													'nama_brg_wip'=> $nama_brg_wip,
													//'hpp'=> $hpp,
													//09-03-2015, dikomen
													//'saldo_awal_bgs'=> $saldo_awal_bgs,
													//'saldo_awal_perbaikan'=> $saldo_awal_perbaikan,
													
													// per 08-04-2015 pake auto saldo akhir
													// 18-04-2015 PAKE ACUAN SO LAGI
													//'saldo_awal_bgs'=> $auto_saldo_awal_bagus,
													//'saldo_awal_perbaikan'=> $auto_saldo_awal_perbaikan,
													'jum_saldo_awal'=> $jum_saldo_awal,
													'saldo_awal_warna'=> $saldo_awal_warna,
													
													'masuk_bgs'=> $masuk_bgs,
													'masuk_returbrgjadi'=> $masuk_returbrgjadi,
													'masuk_pengembalian'=> $masuk_pengembalian,
													'masuk_lain'=> $masuk_lain,
													'masuk_other'=>$masuk_other,
													'jum_masuk'=> $jum_masuk,
													
													'keluar_bgs'=> $keluar_bgs,
													'keluar_perbaikan'=> $keluar_perbaikan,
													'keluar_retur_bhnbaku'=> $keluar_retur_bhnbaku,
													'keluar_gudangjadi'=> $keluar_gudangjadi,
													'keluar_lain'=> $keluar_lain,
													'keluar_other'=>$keluar_other,
													'keluar_unit_packing'=>$keluar_unit_packing,
													'jum_keluar'=> $jum_keluar,
													
													'masuk_bgsback'=> $masuk_bgsback,
													'masuk_returbrgjadiback'=> $masuk_returbrgjadiback,
													'masuk_pengembalianback'=> $masuk_pengembalianback,
													'masuk_lainback'=> $masuk_lainback,
													'jum_masukback'=> $jum_masukback,
													
													'keluar_bgsback'=> $keluar_bgsback,
													'keluar_perbaikanback'=> $keluar_perbaikanback,
													'keluar_retur_bhnbakuback'=> $keluar_retur_bhnbakuback,
													'keluar_gudangjadiback'=> $keluar_gudangjadiback,
													'keluar_lainback'=> $keluar_lainback,
													'jum_keluarback'=> $jum_keluarback,
													
													//'stok_akhir_bgs'=> $stok_akhir_bgs,
													//'stok_akhir_perbaikan'=> $stok_akhir_perbaikan,
													'jum_saldo_akhir'=> $jum_saldo_akhir,
													'jum_adjustment'=>$jum_adjustment,
													'jum_so'=> $jum_so,
													//'jum_so_rupiah'=> $jum_so_rupiah,
													
													'selisih'=> $selisih,
													//'stok_schedule'=> $stok_schedule,
													//'sisa_stok'=> $sisa_stok,
													//'sisa_stok_rupiah'=> $sisa_stok_rupiah
													);
						$saldo_awal_warna = array();
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------

				// array unit jahit
				$data_unit_jahit[] = array(		'kode_unit'=> $kode_unit,
												'nama_unit'=> $nama_unit,
												'data_stok'=> $data_stok,
												//'total_so_rupiah'=> $total_so_rupiah,
												//'total_sisa_stok_rupiah'=> $total_sisa_stok_rupiah
									);
				$data_stok = array(); // done
				$total_so_rupiah = 0; $total_sisa_stok_rupiah = 0;
			} // endforeach header
		}
		else {
			$data_unit_jahit = '';
		}
		//print_r($data_unit_jahit); die();
		return $data_unit_jahit;
  }
  
  // 14-11-2015
  function get_mutasi_unit_packing($date_from, $date_to, $unit_packing) {		
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
	  
		// 1. ambil data2 unit jahit dari rentang tanggal yg dipilih
		$filter = "";
		$filter2 = "";
		if ($unit_packing != '0') {
			$filter = " AND a.id_unit_packing = '$unit_packing' ";
			$filter2= " AND a.id_unit = '$unit_packing' ";
			$filter3= " AND a.id_unit_tujuan = '$unit_packing' ";
			$filter4= " AND a.id_unit_asal = '$unit_packing' ";
		}
		
		$sql = "SELECT a.id_unit_packing, a.kode_unit FROM (
				SELECT a.id_unit_packing, b.kode_unit FROM tm_sjkeluarwip a INNER JOIN tm_unit_packing b ON a.id_unit_packing = b.id
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.id_unit_packing <> '0' GROUP BY a.id_unit_packing, b.kode_unit
				
				UNION SELECT a.id_unit_packing, b.kode_unit FROM tm_sjmasukwip a INNER JOIN tm_unit_packing b ON a.id_unit_packing = b.id
				WHERE a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.id_unit_packing <> '0' GROUP BY a.id_unit_packing, b.kode_unit
				
				UNION SELECT a.id_unit_packing, b.kode_unit FROM tm_sjmasukgudangjadi a INNER JOIN tm_unit_packing b ON a.id_unit_packing = b.id
				WHERE a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.id_unit_packing <> '0' GROUP BY a.id_unit_packing, b.kode_unit
				
				UNION SELECT a.id_unit_tujuan as id_unit_packing, b.kode_unit FROM tm_sjmasukother_unit_packing a INNER JOIN tm_unit_packing b ON a.id_unit_tujuan = b.id
				WHERE a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' ".$filter3." AND a.id_unit_tujuan <> '0' GROUP BY a.id_unit_tujuan, b.kode_unit
				
				UNION SELECT a.id_unit_asal as id_unit_packing, b.kode_unit FROM tm_sjkeluarother_unit_packing a INNER JOIN tm_unit_packing b ON a.id_unit_asal = b.id
				WHERE a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."'".$filter4." AND a.id_unit_asal<> '0' GROUP BY a.id_unit_asal, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_packing, b.kode_unit FROM tt_stok_opname_unit_packing a INNER JOIN tm_unit_packing b ON a.id_unit = b.id
				WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' ".$filter2." GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_packing, b.kode_unit FROM tt_stok_opname_unit_packing a INNER JOIN tm_unit_packing b ON a.id_unit = b.id
				WHERE a.bulan='$bln1' AND a.tahun='$thn1' ".$filter2." GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_packing, b.kode_unit FROM tt_adjustment_unit_packing a INNER JOIN tm_unit_packing b ON a.id_unit = b.id
				WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' ".$filter2." GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_packing, b.kode_unit FROM tt_adjustment_unit_packing a INNER JOIN tm_unit_packing b ON a.id_unit = b.id
				WHERE a.bulan='$bln1' AND a.tahun='$thn1' ".$filter2." GROUP BY a.id_unit, b.kode_unit
				
				UNION SELECT a.id_unit as id_unit_packing, b.kode_unit FROM tt_adjustment_unit_packing a INNER JOIN tm_unit_packing b ON a.id_unit = b.id
				WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' ".$filter2." GROUP BY a.id_unit, b.kode_unit
				)
				a GROUP BY a.id_unit_packing, a.kode_unit ORDER BY a.kode_unit";
				// AND a.status_approve='t'
				
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$total_so_rupiah=0; $total_sisa_stok_rupiah=0;
			foreach ($hasil as $row) {
				// 22-03-2014 -----------------------------------------------------
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row->id_unit_packing' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query utk ambil item2 barang berdasarkan perulangan unit packing
				$sql2 = "SELECT a.id_brg_wip FROM (
					SELECT b.id_brg_wip FROM tm_sjkeluarwip a 
					INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_packing = '$row->id_unit_packing' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjmasukwip a 
					INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_packing = '$row->id_unit_packing' GROUP BY b.id_brg_wip
										
					UNION SELECT b.id_brg_wip FROM tm_sjmasukgudangjadi a 
					INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_packing = '$row->id_unit_packing' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjmasukother_unit_packing a 
					INNER JOIN tm_sjmasukother_unit_packing_detail b ON a.id = b.id_sjmasukother_unit_packing
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_tujuan = '$row->id_unit_packing' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tm_sjkeluarother_unit_packing a 
					INNER JOIN tm_sjkeluarother_unit_packing_detail b ON a.id = b.id_sjkeluarother_unit_packing
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.id_unit_asal = '$row->id_unit_packing' GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
					INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id=b.id_stok_opname_unit_packing
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.id_unit = '$row->id_unit_packing' AND b.jum_stok_opname <> 0 
					GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tt_stok_opname_unit_packing a 
					INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id=b.id_stok_opname_unit_packing
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1' 
					AND a.id_unit = '$row->id_unit_packing' AND b.jum_stok_opname <> 0 
					GROUP BY b.id_brg_wip
					
					UNION SELECT b.id_brg_wip FROM tt_adjustment_unit_packing a 
					INNER JOIN tt_adjustment_unit_packing_detail b ON a.id=b.id_adjustment_unit_packing
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1' 
					AND a.id_unit = '$row->id_unit_packing' AND b.jum_adjustment <> 0 
					GROUP BY b.id_brg_wip
					
					)
					a GROUP BY a.id_brg_wip
					ORDER BY id_brg_wip";
					// AND a.status_approve='t'
					
				$query2	= $this->db->query($sql2);
				
				/*$jum_keluar1 = 0;
				$jum_keluar2 = 0;
				$jum_keluar_lain1 = 0;
				$jum_keluar_lain2 = 0;
				$jum_masuk = 0;
				$jum_masuk_lain1 = 0;
				$jum_masuk_lain2 = 0;
				$jum_masuk_lain3 = 0;
				$jum_masuk_lain4 = 0; 
				*/
				$jum_saldo_awal = 0;
				$jum_masuk = 0;
				$jum_keluar = 0;
				$jum_stok_akhir = 0;
				$jum_so = 0;
				$selisih_bgs = 0;
				$selisih_perbaikan = 0;
				$sisa_stok = 0;
				
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg_wip = $hasilrow->kode_brg;
							$nama_brg_wip = $hasilrow->nama_brg;
						}
						else {
							$kode_brg_wip = '';
							$nama_brg_wip = '';
						}
							
						// SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA 
							$queryx	= $this->db->query(" SELECT b.jum_stok_opname, b.saldo_akhir ,b.tot_jum_stok_opname
											FROM tt_stok_opname_unit_packing a 
											INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
											WHERE b.id_brg_wip = '$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_packing'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query'  ");
						// AND b.status_approve = 't' AND a.status_approve = 't'
											
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							// 15-12-2015. SALDO AWAL PAKE ACUAN PERHITUNGAN SALDO AKHIR BULAN SEBELUMNYA
							//$saldo_awal = $hasilrow->tot_jum_stok_opname;
							$saldo_awal = $hasilrow->jum_stok_opname;
							//$saldo_awal = $hasilrow->saldo_akhir;
						}
						else {
							$saldo_awal = 0;
						}
						
						$queryxyz2	= $this->db->query(" SELECT b.id, b.jum_adjustment
											FROM tt_adjustment_unit_packing a 
											INNER JOIN tt_adjustment_unit_packing_detail b ON a.id = b.id_adjustment_unit_packing
											WHERE b.id_brg_wip = '$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_packing'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' ");
	
						if ($queryxyz2->num_rows() > 0){
							
							$hasilrowxyz2 = $queryx->row();
							$saldo_awal_jum_adjustment = $hasilrowxyz2->tot_jum_adjustment;
							$id_so_detail_jum_adjustment = $hasilrowxyz2->id;
							
	
							$sqlxyz22 = " SELECT a.jum_adjustment, b.nama FROM tt_adjustment_unit_packing_detail_warna a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE id_adjustment_unit_packing_detail = '$id_so_detail_jum_adjustment' ";
							$queryxyz22	= $this->db->query($sqlxyz22);
							if ($queryxyz22->num_rows() > 0){
								$hasilxyz22 = $queryxyz22->result();
								
								foreach ($hasilxyz22 as $rowxyz22) {
									$saldo_awal_jum_adjustment_warna = $rowxyz22->jum_adjustment;
									$nama_warna_jum_adjustment = $rowxyz22->nama;
									$saldo_awal_warna_jum_adjustment[] = array(		'nama_warna'=> $nama_warna_jum_adjustment,
																	'saldo_awal'=> $saldo_awal_warna_jum_adjustment
																	);
								}
							}
							
						}
						else {
							$saldo_awal_jum_adjustment = 0;
							$saldo_awal_jum_adjustment_warna = '';
						}
						
						// 09-03-2015, sementara pake SO dulu, nanti diganti jadi dari auto saldo akhir
						// 18-04-2015 PAKE ACUAN SO LAGI. 12-11-2015 sementara pake acuan SO dulu
						$jum_saldo_awal = $saldo_awal+$saldo_awal_jum_adjustment;
						
						// 08-04-2015, diganti pake auto saldo
						//$jum_saldo_awal = $auto_saldo_awal_bagus+$auto_saldo_awal_perbaikan;
						
						//1. hitung brg masuk bagus dari gudang QC (jenis=1) dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
							
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_masuk = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_dr_unitjahit = $hasilrow->jum_masuk;
							
							if ($masuk_dr_unitjahit == '')
								$masuk_dr_unitjahit = 0;
						}
						else
							$masuk_dr_unitjahit = 0;
						
						// 20-01-2016
						//1.1 hitung brg masuk lain dari gudang QC (jenis=4) dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_keluar = '4' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lain = $hasilrow->jum_masuk;
							
							if ($masuk_lain == '')
								$masuk_lain = 0;
						}
						else
							$masuk_lain = 0;
							
												// 22-02-2016
												
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_unit_packing a 
									INNER JOIN tm_sjmasukother_unit_packing_detail b ON a.id = b.id_sjmasukother_unit_packing
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_tujuan = '$row->id_unit_packing'  ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_other = $hasilrow->jum_masuk;
							
							if ($masuk_other == '')
								$masuk_other = 0;
						}
						else
							$masuk_other = 0;
							
							
						
						$jum_masuk = $masuk_bgs+$masuk_lain+$masuk_other+$masuk_dr_unitjahit;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 2. hitung brg keluar retur ke gudang QC, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_masuk = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur = $hasilrow->jum_keluar;
							
							if ($keluar_retur == '')
								$keluar_retur = 0;
						}
						else
							$keluar_retur = 0;
						
						// 3. hitung brg keluar bagus ke gudang jadi, dari tm_sjmasukgudangjadi
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
							
							
						// 3. hitung brg keluar bagus ke gudang jadi, dari tm_sjmasukgudangjadi
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_masuk = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_dr_unitpacking = $hasilrow->jum_keluar;
							
							if ($keluar_dr_unitpacking == '')
								$keluar_dr_unitpacking = 0;
						}
						else
							$keluar_dr_unitpacking = 0;
						
						// 20-01-2016
						// 3.1 hitung brg keluar lain2 ke gudang QC
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_masuk = '5' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_lain = $hasilrow->jum_keluar;
							
							if ($keluar_lain == '')
								$keluar_lain = 0;
						}
						else
							$keluar_lain = 0;
							
							$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_unit_packing a 
									INNER JOIN tm_sjkeluarother_unit_packing_detail b ON a.id = b.id_sjkeluarother_unit_packing
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_asal = '$row->id_unit_packing'  ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_other = $hasilrow->jum_keluar;
							
							if ($keluar_other == '')
								$keluar_other = 0;
						}
						else
							$keluar_other = 0;
						
						$jum_keluar = $keluar_bgs+$keluar_dr_unitpacking+$keluar_retur+$keluar_lain+$keluar_other;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						// 09-03-2015, $saldo_awal_bgs dan perbaikan diganti $auto_saldo_awal_bagus dan $auto_saldo_awal_perbaikan
						// nanti akhir maret diganti lagi jadi pake auto_saldo_awal_bagus
						//$stok_akhir_bgs = $auto_saldo_awal_bagus + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						//$stok_akhir_perbaikan = $auto_saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						
						// 08-04-2015 pake auto saldo
						// 18-04-2015 PAKE ACUAN SO LAGI. 12-11-2015: sementara acuan SO aja dulu. lg dipikirin utk acuan saldo akhir
						
						//$jum_saldo_akhir = $jum_saldo_awal+$masuk_bgs - $keluar_bgs - $keluar_retur;
						
						//$tambahkurang = $jum_masuk - $jum_keluar;
						//$jum_so += $tambahkurang;
						$jum_saldo_akhir = $jum_saldo_awal+$jum_masuk - $jum_keluar;
						
						// ============================= END STOK AKHIR ==============================================

						$queryxyx	= $this->db->query(" SELECT b.id, b.jum_adjustment FROM tt_adjustment_unit_packing a 
												INNER JOIN tt_adjustment_unit_packing_detail b ON a.id = b.id_adjustment_unit_packing
												WHERE b.id_brg_wip ='$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_packing'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' ");
									if($queryxyx->num_rows() > 0 ){
										$hasilrowxyz=$queryxyx->row();
										$jum_adjustment=$queryxyx->jum_adjustment;
										
										}
										else
										$jum_adjustment='0';


							// STOK OPNAME
						$sql = "SELECT a.jenis_perhitungan_stok, a.tgl_so, b.id, b.jum_stok_opname 
									FROM tt_stok_opname_unit_packing a 
									INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing 
									WHERE b.id_brg_wip = '$row2->id_brg_wip' AND a.id_unit = '$row->id_unit_packing'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1'  ";
						// AND b.status_approve = 't'
														
						$query3	= $this->db->query($sql);
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$id_so_detail = $hasilrow->id;
							$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
							$tgl_so = $hasilrow->tgl_so;
							$jum_so = $hasilrow->jum_stok_opname;
					//=============================================================================			
					//1. hitung brg masuk bagus (jenis=1) dari tm_sjkeluarwip
		$sql3 ="			SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
		$sql3.=				" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
		$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
								AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_keluar = '1' ";
					$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgsx = $hasilrow->jum_masuk;
							
							if ($masuk_bgsx == '')
								$masuk_bgsx = 0;
						}
						else
							$masuk_bgsx = 0;
					//2. hitung brg masuklain (jenis=4) dari tm_sjkeluarwip
		$sql3 ="			SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
		$sql3.=				" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
		$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
								AND a.id_unit_packing= '$row->id_unit_packing' AND a.jenis_keluar = '4' ";
					$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_lainx = $hasilrow->jum_masuk;
							
							if ($masuk_lainx == '')
								$masuk_lainx = 0;
						}
						else
							$masuk_lainx = 0;
						//=============================================================================				
							
							
							
							$jum_masukx = $masuk_bgsx+$masuk_lainx;
						// ------------------------------------------ END MASUK --------------------------------
							// 3. KIRIM LANGSUNG KE GUDANG JADI dari tm_sjmasukgudangjadi
						$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						//	$sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_gudangjadix = $hasilrow->jum_keluar;
							
							if ($keluar_gudangjadix == '')
								$keluar_gudangjadix = 0;
						}
						else
							$keluar_gudangjadix = 0;
						//=============================================================================		
							// 4. KIRIM retur dari tm_sjmasukwip
							$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
								//a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						//else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
						//	$sql3.=" a.tgl_sj >= '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '3')
							$sql3.=" a.tgl_sj > '$tgl_so' ";
						else if ($jenis_perhitungan_stok == '4')
							$sql3.=" a.tgl_sj >= '$tgl_so' ";
						$sql3.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row2->id_brg_wip'
									AND a.id_unit_packing = '$row->id_unit_packing' AND a.jenis_masuk = '3' ";
						$query3	= $this->db->query($sql3);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_returx = $hasilrow->jum_keluar;
							
							if ($keluar_returx == '')
								$keluar_returx = 0;
						}
						else
							$keluar_returx = 0;
							
					$jum_keluarx = $keluar_gudangjadix+$keluar_returx;
					$tambahkurang = $jum_masukx - $jum_keluarx;
					$jum_so = $jum_so + $tambahkurang;
							
							// 15-12-2015, update saldo akhir di tabel stok opname nya
							$sqlupdate = " UPDATE tt_stok_opname_unit_packing_detail 
							SET saldo_akhir = '$jum_saldo_akhir',
							tot_jum_stok_opname = '$jum_so'
							WHERE id = '$id_so_detail' ";
							$this->db->query($sqlupdate);
						}
						else {
							$id_so_detail = 0;
							$jum_so = 0;
						}
						//$jum_so = $so_bgs+$so_perbaikan;
						//=========================================
						
						//$selisih_bgs = $so_bgs-$stok_akhir_bgs;
						//$selisih_perbaikan = $so_perbaikan-$stok_akhir_perbaikan;
						
						$selisih = $jum_so-$jum_saldo_akhir+$jum_adjustment;
																		
						$data_stok[] = array(		'kode_brg_wip'=> $kode_brg_wip,
													'nama_brg_wip'=> $nama_brg_wip,
													//'hpp'=> $hpp,
													//09-03-2015, dikomen
													//'saldo_awal_bgs'=> $saldo_awal_bgs,
													//'saldo_awal_perbaikan'=> $saldo_awal_perbaikan,
													
													// per 08-04-2015 pake auto saldo akhir
													// 18-04-2015 PAKE ACUAN SO LAGI
													//'saldo_awal_bgs'=> $auto_saldo_awal_bagus,
													//'saldo_awal_perbaikan'=> $auto_saldo_awal_perbaikan,
													'jum_saldo_awal'=> $jum_saldo_awal,
													
													'masuk_bgs'=> $masuk_bgs,
													'masuk_other'=> $masuk_other,
													'masuk_dr_unitjahit' => $masuk_dr_unitjahit,
													'masuk_lain'=> $masuk_lain,
													'jum_masuk'=> $jum_masuk,
													
													'keluar_bgs'=> $keluar_bgs,
													'keluar_dr_unitpacking' => $keluar_dr_unitpacking,
													'keluar_retur'=> $keluar_retur,
													'keluar_other'=> $keluar_other,
													'keluar_lain'=> $keluar_lain,
													'jum_keluar'=> $jum_keluar,
													
													//'stok_akhir_bgs'=> $stok_akhir_bgs,
													//'stok_akhir_perbaikan'=> $stok_akhir_perbaikan,
													'jum_saldo_akhir'=> $jum_saldo_akhir,
													'jum_adjustment'=> $jum_adjustment,
													'jum_so'=> $jum_so,
													//'jum_so_rupiah'=> $jum_so_rupiah,
													
													'selisih'=> $selisih,
													//'stok_schedule'=> $stok_schedule,
													//'sisa_stok'=> $sisa_stok,
													//'sisa_stok_rupiah'=> $sisa_stok_rupiah
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------

				// array unit packing
				$data_unit_packing[] = array(		'kode_unit'=> $kode_unit,
												'nama_unit'=> $nama_unit,
												'data_stok'=> $data_stok,
												//'total_so_rupiah'=> $total_so_rupiah,
												//'total_sisa_stok_rupiah'=> $total_sisa_stok_rupiah
									);
				$data_stok = array(); // done
				$total_so_rupiah = 0; $total_sisa_stok_rupiah = 0;
			} // endforeach header
		}
		else {
			$data_unit_packing = '';
		}
		return $data_unit_packing;
  }
  
  
  
  // 05-12-2015
  //function get_transaksi_unit_jahitget_transaksi_unit_jahit($unit_jahit, $bulan, $tahun, $list_id_brg_wip) {		
  function get_transaksi_unit_jahit($unit_jahit, $date_from, $date_to, $list_id_brg_wip) {		
	  // 14-01-2016 dikomen
	/*	$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		} */
		
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgldari = $thn1."-".$bln1."-".$tgl1;
		
		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		$tgldariback=$thn_query."-".$bln_query."-".$tgl1;
	
		$pisah1 = explode("-", $date_to);
		$tgl2= $pisah1[0];
		$bln2= $pisah1[1];
		$thn2= $pisah1[2];
		$tglke = $thn2."-".$bln2."-".$tgl2;
		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		
		$timeStamp            =    mktime(0,0,0,$bln_query,1,$thn_query);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('t',$lastDayTimeStamp);// Find last day of the month
		
		$tglkeback = $thn_query."-".$bln_query."-".$lastDay;
		//$tglke = $thn_query."-".$bln_query."-".$tgl2;
		//$tglkeback = date("Y-m-t", strtotime($tglke));
			//print_r($tglkeback);
			
				
		$data_brg_wip = array();
		$data_warna = array();
		$data_so_warna = array();
		$jum_stok_opname = 0;
		
		// ------------------------------ 12-01-2016 ---------------------------------------------------------------
		$id_brg_wip_exp = explode(";", $list_id_brg_wip);
		foreach($id_brg_wip_exp as $row1) {
			if ($row1 != '') {
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$row1' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk1'=> 0,
												'tot_keluar1'=> 0,
												'tot_keluar_gdgjadi1'=> 0,
												'tot_masuk_lain1'=> 0,
												'tot_keluar_lain1'=> 0,
												'tot_masuk_pengembalian_retur1'=> 0,
												'tot_masuk_retur_perb1'=> 0,
												'tot_keluar_hasil_perb1'=> 0,
												'tot_keluar_retur_bhnbaku1'=> 0,
												'tot_keluar_packing'=>0
											);
					}
				}
				
				// ambil SO dari tt_stok_opname_unit_jahit_detail
				$sqlso = " SELECT b.id, b.jum_stok_opname, b.saldo_akhir , b.tot_jum_stok_opname
							FROM tt_stok_opname_unit_jahit a INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND a.id_unit = '$unit_jahit' AND b.id_brg_wip = '$row1' ";
				$queryso	= $this->db->query($sqlso);
				if ($queryso->num_rows() > 0){
					$hasilso = $queryso->row();
					$id_sawal_detail = $hasilso->id;
					$jum_stok_opname = $hasilso->jum_stok_opname;
					$saldo_akhir = $hasilso->saldo_akhir;
					$tot_jum_stok_opname = $hasilso->tot_jum_stok_opname;
				}
				else {
					$saldo_akhir = 0;
					$tot_jum_stok_opname = 0;
					$id_sawal_detail = 0;
				}
				
				// 04-02-2016 saldo awal per warna
				$sqlsowarna = " SELECT b.id_warna, b.saldo_akhir, b.jum_stok_opname, c.nama FROM tt_stok_opname_unit_jahit_detail_warna b
							INNER JOIN tm_warna c ON b.id_warna = c.id
							WHERE b.id_stok_opname_unit_jahit_detail = '$id_sawal_detail' "; //echo $sqlsowarna."<br>";
							
				$querysowarna	= $this->db->query($sqlsowarna);
				if ($querysowarna->num_rows() > 0){
					$hasilsowarna = $querysowarna->result();
						
					foreach ($hasilsowarna as $rowxx) {
						$data_so_warna[] = array('id_warna'=> $rowxx->id_warna,
												'nama_warna'=> $rowxx->nama,
												//'saldo'=> $rowxx->saldo_akhir
												'saldo'=> $rowxx->jum_stok_opname
												);
					}
				}
				
				
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
				
				
				//edit 22/02/16
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				$sql2 = " SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, a.id_unit, 1 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other, 0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_keluar = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
				$sql2.= " AND a.tgl_bonm >= '".$tgldari."' AND a.tgl_bonm <= '".$tglke."'
						
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, a.id_unit, 0 as masuk_bgs, 1 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other, 0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_keluar = '3' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_bonm >= '".$tgldari."' AND a.tgl_bonm <= '".$tglke."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 1 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '4' AND b.id_brg_wip='$row1' 
						AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_asal as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 1 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjantarunit a INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit 
						WHERE a.id_unit_tujuan = '$unit_jahit' AND b.id_brg_wip='$row1'
						AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 1 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_keluar = '2' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_bonm >= '".$tgldari."' AND a.tgl_bonm <= '".$tglke."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 1 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_tujuan , 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 1 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjmasukother_unit_jahit a INNER JOIN tm_sjmasukother_unit_jahit_detail b ON a.id = b.id_sjmasukother_unit_jahit
						WHERE a.id_unit_tujuan = '$unit_jahit'  AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 1 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 1 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_asal as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 1 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjantarunit a INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit 
						WHERE a.id_unit_asal = '$unit_jahit' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 1 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '5' AND b.id_brg_wip='$row1'
						AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 1 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 1 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 1 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_masuk = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_asal , 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 1 as keluar_other, 0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback,0 as keluar_unit_packingback
						FROM tm_sjkeluarother_unit_jahit a INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
						WHERE a.id_unit_asal = '$unit_jahit'  AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other, 1 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packingback
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '3' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'


						
						ORDER BY tgl_sj ASC "; //echo $sql2;
								
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				// 18-01-2016, ini tadinya ke stok opname. diganti ke saldo_akhir
				//$tot_saldo=$saldo_akhir;
				$tot_saldo=$tot_jum_stok_opname;
				
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$id_unit = $row2->id_unit;
						$masuk_bgs = $row2->masuk_bgs;
						$masuk_lain = $row2->masuk_lain;
						$masuk_lain2 = $row2->masuk_lain2;
						$masuk_lain3 = $row2->masuk_lain3;
						$masuk_pengembalian_retur = $row2->masuk_pengembalian_retur;
						$masuk_retur_perb = $row2->masuk_retur_perb;
						$masuk_other= $row2->masuk_other;
						$keluar_bgs = $row2->keluar_bgs;
						$keluar_bgs_gdgjadi = $row2->keluar_bgs_gdgjadi;
						$keluar_lain = $row2->keluar_lain;
						$keluar_lain2 = $row2->keluar_lain2;
						$keluar_hasil_perb = $row2->keluar_hasil_perb;
						$keluar_retur_bhnbaku = $row2->keluar_retur_bhnbaku;
						$keluar_other= $row2->keluar_other;
						$keluar_unit_packing= $row2->keluar_unit_packing;
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						
						
						if ($masuk_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '1'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						
						
						if ($masuk_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '3'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						// 15-01-2016
						if ($masuk_lain2 == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '4'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_lain3 == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjantarunit a INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									INNER JOIN tm_sjantarunit_detail_warna c ON b.id = c.id_sjantarunit_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						// -------------------
						if ($masuk_pengembalian_retur == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '2'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_retur_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '3'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						//edit 22/02/16
						if ($masuk_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukother_unit_jahit a INNER JOIN tm_sjmasukother_unit_jahit_detail b ON a.id = b.id_sjmasukother_unit_jahit
									INNER JOIN tm_sjmasukother_unit_jahit_detail_warna c ON b.id = c.id_sjmasukother_unit_jahit_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' 
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						// keluar
						if ($keluar_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '1'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_bgs_gdgjadi == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '1'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						if ($keluar_hasil_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '2'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
							//echo $sql3;
						}
						
						if ($keluar_retur_bhnbaku == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '1'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						// 07-12-2015
						if ($keluar_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjantarunit a INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit
									INNER JOIN tm_sjantarunit_detail_warna c ON b.id = c.id_sjantarunit_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						// 15-01-2016
						if ($keluar_lain2 == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '5'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						//edit 22/02/16
						if ($keluar_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarother_unit_jahit a INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
									INNER JOIN tm_sjkeluarother_unit_jahit_detail_warna c ON b.id = c.id_sjkeluarother_unit_jahit_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' 
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						if ($keluar_unit_packing == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '3'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						
						// --------------------
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											// 05-12-2015
											$tot_saldo+= $row3->qty;
											
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											//echo "kesini"; die();
											// 05-12-2015
											$tot_saldo-= $row3->qty;
											
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_bgs == '1') {
											$data_warna[$xx]['tot_masuk1']+= $row3->qty;
										}
										else if ($masuk_lain== '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_lain2== '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_lain3== '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_pengembalian_retur == '1') {
											$data_warna[$xx]['tot_masuk_pengembalian_retur1']+= $row3->qty;
										}
										else if ($masuk_retur_perb == '1') {
											$data_warna[$xx]['tot_masuk_retur_perb1']+= $row3->qty;
										}
										else if ($masuk_other == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($keluar_bgs == '1') {
											$data_warna[$xx]['tot_keluar1']+= $row3->qty;
										}
										else if ($keluar_bgs_gdgjadi == '1') {
											$data_warna[$xx]['tot_keluar_gdgjadi1']+= $row3->qty;
										}
										else if ($keluar_lain == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										else if ($keluar_lain2 == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										else if ($keluar_hasil_perb == '1') {
											$data_warna[$xx]['tot_keluar_hasil_perb1']+= $row3->qty;
										}
										else if ($keluar_retur_bhnbaku == '1') {
											$data_warna[$xx]['tot_keluar_retur_bhnbaku1']+= $row3->qty;
										}
										else if ($keluar_other == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										else if ($keluar_unit_packing == '1') {
											$data_warna[$xx]['tot_keluar_packing']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						if ($id_unit != 0) {
							$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit' ");
							$hasilrow = $query3->row();
							$kode_unit	= $hasilrow->kode_unit;
							$nama_unit	= $hasilrow->nama;
						}
						else {
							$kode_unit = '';
							$nama_unit = '';
						}
						

					
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'id_unit'=> $id_unit,
												'kode_unit'=> $kode_unit,
												'nama_unit'=> $nama_unit,
												'masuk_bgs'=> $masuk_bgs,
												'masuk_lain'=> $masuk_lain,
												'masuk_lain2'=> $masuk_lain2,
												'masuk_lain3'=> $masuk_lain3,
												'masuk_pengembalian_retur'=> $masuk_pengembalian_retur,
												'masuk_retur_perb'=> $masuk_retur_perb,
												'masuk_other'=> $masuk_other,
												'keluar_bgs'=> $keluar_bgs,
												'keluar_bgs_gdgjadi'=> $keluar_bgs_gdgjadi,
												'keluar_lain'=> $keluar_lain,
												'keluar_lain2'=> $keluar_lain2,
												'keluar_hasil_perb'=> $keluar_hasil_perb,
												'keluar_retur_bhnbaku'=> $keluar_retur_bhnbaku,
												'keluar_other'=> $keluar_other,
												'keluar_unit_packing'=> $keluar_unit_packing,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												// 05-12-2015
												'tot_saldo'=> $tot_saldo
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end row2
				}
				else
					$data_tabel1='';
					
				
				// -----------------------------------------------------------------------------------------------------------------
				
				$sql7 = " SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 1 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_keluar = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
				$sql7.= " AND a.tgl_bonm >= '".$tgldariback."' AND a.tgl_bonm <= '".$tglkeback."'
						
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 1 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_keluar = '3' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_bonm >= '".$tgldariback."' AND a.tgl_bonm <= '".$tglkeback."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 1 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '4' AND b.id_brg_wip='$row1' 
						AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_asal as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 1 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjantarunit a INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit 
						WHERE a.id_unit_tujuan = '$unit_jahit' AND b.id_brg_wip='$row1'
						AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
						
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 1 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_keluar = '2' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_bonm >= '".$tgldariback."' AND a.tgl_bonm <= '".$tglkeback."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 1 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
				
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_tujuan , 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 1 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjmasukother_unit_jahit a INNER JOIN tm_sjmasukother_unit_jahit_detail b ON a.id = b.id_sjmasukother_unit_jahit
						WHERE a.id_unit_tujuan = '$unit_jahit'  AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 1 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 1 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_asal as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 1 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjantarunit a INNER JOIN tm_sjantarunit_detail b ON a.id = b.id_sjantarunit 
						WHERE a.id_unit_asal = '$unit_jahit' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 1 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '5' AND b.id_brg_wip='$row1'
						AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 1 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 1 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 1 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_masuk = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_asal , 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 1 as keluar_otherback, 0 as keluar_unit_packing_back
						FROM tm_sjkeluarother_unit_jahit a INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
						WHERE a.id_unit_asal = '$unit_jahit'  AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
						
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_lain2, 0 as masuk_lain3, 0 as masuk_pengembalian_retur, 0 as masuk_retur_perb, 0 as masuk_other, 0 as keluar_bgs, 0 as keluar_bgs_gdgjadi, 0 as keluar_lain, 0 as keluar_lain2, 0 as keluar_hasil_perb, 0 as keluar_retur_bhnbaku, 0 as keluar_other,0 as keluar_unit_packing,
						 0 as masuk_bgsback, 0 as masuk_lainback, 0 as masuk_lain2back, 0 as masuk_lain3back, 0 as masuk_pengembalian_returback, 0 as masuk_retur_perbback, 0 as masuk_otherback, 0 as keluar_bgsback, 0 as keluar_bgs_gdgjadiback, 0 as keluar_lainback, 0 as keluar_lain2back, 0 as keluar_hasil_perbback, 0 as keluar_retur_bhnbakuback, 0 as keluar_otherback, 1 as keluar_unit_packing_back
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '3' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
				$sql7.= " AND a.tgl_sj >= '".$tgldariback."' AND a.tgl_sj <= '".$tglkeback."'
					
						ORDER BY tgl_sj ASC "; //echo $sql7;
						
						$query7	= $this->db->query($sql7);
						$data_tabel7 = array();
						if ($query7->num_rows() > 0){
							
						$hasil7 = $query7->result();
					
					foreach ($hasil7 as $row7) {
						$id_data = $row7->id;
						$no_sj = $row7->no_sj;
						$tgl_sj = $row7->tgl_sj;
						$id_unit = $row7->id_unit;
						$masuk_bgsback = $row7->masuk_bgsback;
						$masuk_lainback = $row7->masuk_lainback;
						$masuk_lain2back = $row7->masuk_lain2back;
						$masuk_lain3back = $row7->masuk_lain3back;
						$masuk_pengembalian_returback = $row7->masuk_pengembalian_returback;
						$masuk_retur_perbback = $row7->masuk_retur_perbback;
						$masuk_otherback= $row7->masuk_otherback;
						$keluar_bgsback = $row7->keluar_bgsback;
						$keluar_bgs_gdgjadiback = $row7->keluar_bgs_gdgjadiback;
						$keluar_lainback = $row7->keluar_lainback;
						$keluar_lain2back = $row7->keluar_lain2back;
						$keluar_hasil_perbback = $row7->keluar_hasil_perbback;
						$keluar_retur_bhnbakuback = $row7->keluar_retur_bhnbakuback;
						$keluar_otherback= $row7->keluar_otherback;
						$keluar_unit_packing_back= $row7->keluar_unit_packing_back;
						
						$data_tabel7[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'id_unit'=> $id_unit,
												//'kode_unit'=> $kode_unit,
												//'nama_unit'=> $nama_unit,
												'masuk_bgsback'=> $masuk_bgsback,
												'masuk_lainback'=> $masuk_lainback,
												'masuk_lain2back'=> $masuk_lain2back,
												'masuk_lain3back'=> $masuk_lain3back,
												'masuk_pengembalian_returback'=> $masuk_pengembalian_returback,
												'masuk_retur_perbback'=> $masuk_retur_perbback,
												'masuk_otherback'=> $masuk_otherback,
												'keluar_bgsback'=> $keluar_bgsback,
												'keluar_bgs_gdgjadiback'=> $keluar_bgs_gdgjadiback,
												'keluar_lainback'=> $keluar_lainback,
												'keluar_lain2back'=> $keluar_lain2back,
												'keluar_hasil_perbback'=> $keluar_hasil_perbback,
												'keluar_retur_bhnbakuback'=> $keluar_retur_bhnbakuback,
												'keluar_otherback'=> $keluar_otherback,
												'keluar_unit_packing_back'=> $keluar_unit_packing_back
												
												//'masuk'=> $masuk,
												//'keluar'=> $keluar,
												//'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												//'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												//'tot_saldo'=> $tot_saldo
										);
						
					}
				}
				else
					$data_tabel7[] = array(	'no_sj'=> 0,
												'tgl_sj'=> 0,
												'id_unit'=> 0,
												//'kode_unit'=> $kode_unit,
												//'nama_unit'=> $nama_unit,
												'masuk_bgsback'=> 0,
												'masuk_lainback'=> 0,
												'masuk_lain2back'=> 0,
												'masuk_lain3back'=> 0,
												'masuk_pengembalian_returback'=> 0,
												'masuk_retur_perbback'=> 0,
												'masuk_otherback'=> 0,
												'keluar_bgsback'=> 0,
												'keluar_bgs_gdgjadiback'=> 0,
												'keluar_lainback'=> 0,
												'keluar_lain2back'=> 0,
												'keluar_hasil_perbback'=> 0,
												'keluar_retur_bhnbakuback'=> 0,
												'keluar_otherback'=> 0,
												'keluar_unit_packing_back'=> 0);
				// =================================== END NEW =======================================================
				
				// array brg wip
				$data_brg_wip[] = array(		'id_brg_wip'=> $row1,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,
												'data_tabel7'=> $data_tabel7,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna,
												// 16-01-2016 ga pake SO, tapi saldo akhir
												//'jum_stok_opname'=> $jum_stok_opname,
												//'saldo_akhir'=> $saldo_akhir,
												'tot_jum_stok_opname'=> $tot_jum_stok_opname,
												'gtot_saldo'=> $tot_saldo
									);
				$data_tabel1 = array();
				$data_tabel7 = array();
				$data_warna = array();
				$data_so_warna = array();
				
			} // END IF ROW1
		} // END FOREACH
		//-----------------------------------------------------------------------------------
		
			//} // endforeach header
		//}
		//else {
			//$data_brg_jadi = '';
		//}
		return $data_brg_wip;
  }
  
  // 08-12-2015
  function get_transaksi_unit_packing($unit_packing, $bulan, $tahun, $id_brg_wip) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
				
		$data_brg_wip = array();
		$data_warna = array();
		$data_so_warna = array();
		$jum_stok_opname = 0;
		
				
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$id_brg_wip' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk1'=> 0,
												'tot_masuk_lain1'=> 0,
												'tot_masuk_dr_unitjahit'=> 0,
												'tot_keluar1'=> 0,
												'tot_keluar_retur1'=> 0,
												'tot_keluar_lain1'=> 0,
												
											);
					}
				}
				
				// ambil SO dari tt_stok_opname_unit_packing_detail
				$sqlwarna = " SELECT c.id_warna, d.nama, c.jum_stok_opname ,c.saldo_akhir
							FROM tt_stok_opname_unit_packing a INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing 
							INNER JOIN tt_stok_opname_unit_packing_detail_warna c ON b.id = c.id_stok_opname_unit_packing_detail 
							INNER JOIN tm_warna d ON d.id = c.id_warna
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$id_brg_wip' AND a.id_unit='$unit_packing' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['id_warna'] == $rowwarna->id_warna) {
								
								$data_warna[$xx]['saldo']= $rowwarna->jum_stok_opname;
								//ambil saldo awal dari SO 13.3.2016
								//$data_warna[$xx]['saldo']= $rowwarna->saldo_akhir;
							}
						} // end for
						
						$data_so_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_stok_opname'=> $rowwarna->jum_stok_opname
												//ambil saldo akhir dari saldo_akhir 13.3.2016
												//'saldo_akhir_warna'=> $rowwarna->saldo_akhir
											);
					}
				}
				else
					$data_so_warna = '';
				
				// 20-01-2016
				// ambil SO global dari tt_stok_opname_unit_packing_detail
				$sqlso = " SELECT b.jum_stok_opname, b.saldo_akhir ,b.tot_jum_stok_opname
							FROM tt_stok_opname_unit_packing a INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND a.id_unit = '$unit_packing' AND b.id_brg_wip = '$id_brg_wip' ";
				$queryso	= $this->db->query($sqlso);
				if ($queryso->num_rows() > 0){
					$hasilso = $queryso->row();
					//ambil saldo awal dari SO 13.3.2016
					//$jum_stok_opname = $hasilso->jum_stok_opname;
					$tot_jum_stok_opname=$hasilso->tot_jum_stok_opname;
					//$saldo_akhir = $hasilso->saldo_akhir;
				}
				else
						//ambil saldo awal dari SO 13.3.2016
						//$jum_stok_opname = 0;
						$tot_jum_stok_opname=0;
						$saldo_akhir =0;
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
							
							//edit 22/02/16	
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_packing as id_unit,1 as masuk_bgs, 0 as masuk_lain, 0 as masuk_other,0 as masuk_dr_unitjahit, 0 as keluar_bgs, 0 as keluar_retur, 0 as keluar_lain, 0 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_unit_packing = '$unit_packing' AND a.jenis_keluar = '1' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_packing as id_unit,0 as masuk_bgs, 1 as masuk_lain, 0 as masuk_other,0 as masuk_dr_unitjahit, 0 as keluar_bgs, 0 as keluar_retur, 0 as keluar_lain, 0 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_unit_packing = '$unit_packing' AND a.jenis_keluar = '4' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_tujuan as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 1 as masuk_other,0 as masuk_dr_unitjahit, 0 as keluar_bgs, 0 as keluar_retur, 0 as keluar_lain, 0 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjmasukother_unit_packing a INNER JOIN tm_sjmasukother_unit_packing_detail b ON a.id = b.id_sjmasukother_unit_packing
						WHERE a.id_unit_tujuan = '$unit_packing' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_packing as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_other,1 as masuk_dr_unitjahit, 0 as keluar_bgs, 0 as keluar_retur, 0 as keluar_lain, 0 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '3' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_packing as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_other,0 as masuk_dr_unitjahit, 1 as keluar_bgs, 0 as keluar_retur, 0 as keluar_lain, 0 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '2' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_packing as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_other,0 as masuk_dr_unitjahit, 0 as keluar_bgs, 1 as keluar_retur, 0 as keluar_lain, 0 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '3' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_packing as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_other,0 as masuk_dr_unitjahit, 0 as keluar_bgs, 0 as keluar_retur, 1 as keluar_lain, 0 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '5' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_asal as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_other,0 as masuk_dr_unitjahit, 0 as keluar_bgs, 0 as keluar_retur, 0 as keluar_lain, 1 as keluar_other, 0 as keluar_dr_unitpacking
						FROM tm_sjkeluarother_unit_packing a INNER JOIN tm_sjkeluarother_unit_packing_detail b ON a.id = b.id_sjkeluarother_unit_packing
						WHERE a.id_unit_asal = '$unit_packing' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_packing as id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_other,0 as masuk_dr_unitjahit, 0 as keluar_bgs, 0 as keluar_retur, 0 as keluar_lain, 0 as keluar_other, 1 as keluar_dr_unitpacking
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.id_unit_packing = '$unit_packing' AND a.jenis_masuk = '3' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
												
						ORDER BY tgl_sj ASC "; //echo $sql2; die();
								
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				//ambil saldo awal dari saldo_so 13.3.2016
				$tot_saldo=$tot_jum_stok_opname;
				//$tot_saldo=$jum_stok_opname;
				
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
								//edit 22/02/16	
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$id_unit = $row2->id_unit;
						$masuk_bgs = $row2->masuk_bgs;
						$masuk_lain = $row2->masuk_lain;
						$masuk_other = $row2->masuk_other;
						$masuk_dr_unitjahit = $row2->masuk_dr_unitjahit;
						$keluar_bgs = $row2->keluar_bgs;
						$keluar_retur = $row2->keluar_retur;
						$keluar_lain = $row2->keluar_lain;
						$keluar_other = $row2->keluar_other;
						$keluar_dr_unitpacking =$row2->keluar_dr_unitpacking;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($masuk_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '1'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '4'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
									//edit 22/02/16	
						if ($masuk_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukother_unit_packing a INNER JOIN tm_sjmasukother_unit_packing_detail b ON a.id = b.id_sjmasukother_unit_packing
									INNER JOIN tm_sjmasukother_unit_packing_detail_warna c ON b.id = c.id_sjmasukother_unit_packing_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' 
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_dr_unitjahit == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '3'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
							//echo $sql3."<br>";
						}
												
						// keluar
						if ($keluar_retur == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '3'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
							//echo $sql3."<br>";
						}
						if ($keluar_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '2'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
							//echo $sql3."<br>";
						}
						if ($keluar_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '5'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
							//echo $sql3."<br>";
						}
						
						if ($keluar_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarother_unit_packing a INNER JOIN tm_sjkeluarother_unit_packing_detail b ON a.id = b.id_sjkeluarother_unit_packing
									INNER JOIN tm_sjkeluarother_unit_packing_detail_warna c ON b.id = c.id_sjkeluarother_unit_packing_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' 
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
							//echo $sql3."<br>";
						}
						
						if ($keluar_dr_unitpacking == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '3'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
							//echo $sql3."<br>";
						}
						
						// --------------------
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											// 05-12-2015
											$tot_saldo+= $row3->qty;
											
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											// 05-12-2015
											$tot_saldo-= $row3->qty;
											
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_bgs == '1') {
											$data_warna[$xx]['tot_masuk1']+= $row3->qty;
										}
										else if ($masuk_lain == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_other == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_dr_unitjahit == '1') {
											$data_warna[$xx]['tot_masuk_dr_unitjahit']+= $row3->qty;
										}
										else if ($keluar_bgs == '1'||$keluar_dr_unitpacking == 1) {
											$data_warna[$xx]['tot_keluar1']+= $row3->qty;
										}
										else if ($keluar_retur == '1') {
											$data_warna[$xx]['tot_keluar_retur1']+= $row3->qty;
										}
										else if ($keluar_lain == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										else if ($keluar_other == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						if ($id_unit != 0) {
							$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit' ");
							$hasilrow = $query3->row();
							$kode_unit	= $hasilrow->kode_unit;
							$nama_unit	= $hasilrow->nama;
						}
						else {
							$kode_unit = '';
							$nama_unit = '';
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'id_unit'=> $id_unit,
												'kode_unit'=> $kode_unit,
												'nama_unit'=> $nama_unit,
												'masuk_bgs'=> $masuk_bgs,
												'masuk_lain'=> $masuk_lain,
												'masuk_other'=> $masuk_other,
												'masuk_dr_unitjahit' => $masuk_dr_unitjahit,
												'keluar_bgs'=> $keluar_bgs,
												'keluar_retur'=> $keluar_retur,
												'keluar_lain'=> $keluar_lain,
												'keluar_other'=> $keluar_other,
												'keluar_dr_unitpacking' => $keluar_dr_unitpacking,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												// 20-01-2016
												'tot_saldo'=> $tot_saldo
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
				// =================================== END NEW =======================================================
				
				// array brg wip
				$data_brg_wip[] = array(		'id_brg_wip'=> $id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna,
												// 20-01-2016
												//ambil saldo awal dari saldo_so 13.3.2016
												'saldo_akhir'=> $tot_jum_stok_opname,
												//'jum_stok_opname'=> $jum_stok_opname,
												'gtot_saldo'=> $tot_saldo
									);
				$data_tabel1 = array();
				$data_warna = array();
				$data_so_warna = array();
			//} // endforeach header
		//}
		//else {
			//$data_brg_jadi = '';
		//}
		return $data_brg_wip;
  }
  
  // 11-12-2015
  function get_transaksi_hasil_jahit($id_gudang, $bulan, $tahun, $id_brg_wip) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
				
		$data_brg_wip = array();
		$data_warna = array();
		$data_so_warna = array();
		$jum_stok_opname = 0;
				
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$id_brg_wip' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk_bgs1'=> 0,
												'tot_masuk_hasil_perb1'=> 0,
												'tot_masuk_retur_packing1'=> 0,
												'tot_masuk_retur_gdgjadi1'=> 0,
												'tot_masuk_lain1'=> 0,
												
												'tot_keluar_bgs_packing1'=> 0,
												'tot_keluar_bgs_gdgjadi1'=> 0,
												'tot_keluar_retur_perb1'=> 0,
												'tot_keluar_lain1'=> 0
											);
					}
				}
				
				// ambil SO dari tt_stok_opname_hasil_jahit_detail
				$sqlwarna = " SELECT c.id_warna, d.nama, c.jum_stok_opname 
							FROM tt_stok_opname_hasil_jahit a INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
							INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail 
							INNER JOIN tm_warna d ON d.id = c.id_warna
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang='$id_gudang' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['id_warna'] == $rowwarna->id_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_stok_opname;
							}
						} // end for
						
						$data_so_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_stok_opname'=> $rowwarna->jum_stok_opname
											);
					}
				}
				else
					$data_so_warna = '';
				
				// 19-01-2016, tambahan. SO-nya ambil dari global juga
				// ambil SO dari tt_stok_opname_unit_jahit_detail
				$sqlso = " SELECT b.jum_stok_opname
							FROM tt_stok_opname_hasil_jahit a INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang='$id_gudang' ";
				$queryso	= $this->db->query($sqlso);
				if ($queryso->num_rows() > 0){
					$hasilso = $queryso->row();
					$jum_stok_opname = $hasilso->jum_stok_opname;
				}
				
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
								
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				//$sql2 edit by yefta 15-02-2016
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 1 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain , 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '1' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 1 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '2' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 1 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '3' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 1 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '4' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 1 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '5' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 1 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukother_gudangqc a INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc 
						WHERE a.id_gudang = '$id_gudang' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 1 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '1' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 1 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '2' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 1 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '3' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 1 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '4' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 1 as keluar_other
						FROM tm_sjkeluarother_gudangqc a INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc  
						WHERE a.id_gudang = '$id_gudang' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
												
						ORDER BY tgl_sj ASC "; //echo $sql2; die();
								
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				$tot_saldo=$jum_stok_opname;
				
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$id_gudang = $row2->id_gudang;
						$masuk_bgs = $row2->masuk_bgs;
						$masuk_hasil_perb = $row2->masuk_hasil_perb;
						$masuk_retur_packing = $row2->masuk_retur_packing;
						$masuk_retur_gdgjadi = $row2->masuk_retur_gdgjadi;
						$masuk_lain = $row2->masuk_lain;
						//$masuk_other edit 24-02-2016
						$masuk_other = $row2->masuk_other;
						$keluar_bgs_packing = $row2->keluar_bgs_packing;
						$keluar_bgs_gdgjadi = $row2->keluar_bgs_gdgjadi;
						$keluar_retur_perb = $row2->keluar_retur_perb;
						$keluar_lain = $row2->keluar_lain;
						//$keluar_other edit 15-02-2016
						$keluar_other = $row2->keluar_other;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($masuk_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '1'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_hasil_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '2'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_retur_packing == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '3'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_retur_gdgjadi == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '4'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '5'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						// edit 24-02-2016
						if ($masuk_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukother_gudangqc a INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
									INNER JOIN tm_sjmasukother_gudangqc_detail_warna c ON b.id = c.id_sjmasukother_gudangqc_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' 
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
												
						// keluar
						if ($keluar_bgs_packing == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '1'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_bgs_gdgjadi == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '2'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_retur_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '3'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '4'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						// edit 15-02-2016
						if ($keluar_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarother_gudangqc a INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
									INNER JOIN tm_sjkeluarother_gudangqc_detail_warna c ON b.id = c.id_sjkeluarother_gudangqc_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' 
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						// --------------------
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											// 11-12-2015
											$tot_saldo+= $row3->qty;
											
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											// 11-12-2015
											$tot_saldo-= $row3->qty;
											
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_bgs == '1') {
											$data_warna[$xx]['tot_masuk_bgs1']+= $row3->qty;
										}
										else if ($masuk_hasil_perb == '1') {
											$data_warna[$xx]['tot_masuk_hasil_perb1']+= $row3->qty;
										}
										else if ($masuk_retur_packing == '1') {
											$data_warna[$xx]['tot_masuk_retur_packing1']+= $row3->qty;
										}
										else if ($masuk_retur_gdgjadi == '1') {
											$data_warna[$xx]['tot_masuk_retur_gdgjadi1']+= $row3->qty;
										}
										else if ($masuk_other == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_lain == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($keluar_bgs_packing == '1') {
											$data_warna[$xx]['tot_keluar_bgs_packing1']+= $row3->qty;
										}
										else if ($keluar_bgs_gdgjadi == '1') {
											$data_warna[$xx]['tot_keluar_bgs_gdgjadi1']+= $row3->qty;
										}
										else if ($keluar_retur_perb == '1') {
											$data_warna[$xx]['tot_keluar_retur_perb1']+= $row3->qty;
										}
										else if ($keluar_lain == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										else if ($keluar_other == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										
									}
								}
								
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						if ($id_gudang != '0') {
							$query3	= $this->db->query(" SELECT a.nama as nama_lokasi, b.kode_gudang, b.nama as nama_gudang FROM tm_lokasi_gudang a 
									INNER JOIN tm_gudang b ON a.id = b.id_lokasi 
									WHERE b.id = '$id_gudang' ");
							$hasilrow = $query3->row();
							$nama_lokasi	= $hasilrow->nama_lokasi;
							$nama_gudang	= $hasilrow->nama_gudang;
							$kode_gudang	= $hasilrow->kode_gudang;
							$gudangnya = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
						}
						else {
							//$gudangnya = "Semua";
							$kode_gudang = "";
							$nama_gudang = "";
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'id_gudang'=> $id_gudang,
												'nama_lokasi'=> $nama_lokasi,
												'kode_gudang'=> $kode_gudang,
												'nama_gudang'=> $nama_gudang,
												'masuk_bgs'=> $masuk_bgs,
												'masuk_hasil_perb'=> $masuk_hasil_perb,
												'masuk_retur_packing'=> $masuk_retur_packing,
												'masuk_retur_gdgjadi'=> $masuk_retur_gdgjadi,
												'masuk_lain'=> $masuk_lain,
													//$keluar_other edit by 24-02-2016
												'masuk_other'=> $masuk_other,
												'keluar_bgs_packing'=> $keluar_bgs_packing,
												'keluar_bgs_gdgjadi'=> $keluar_bgs_gdgjadi,
												'keluar_retur_perb'=> $keluar_retur_perb,
												'keluar_lain'=> $keluar_lain,
												//$keluar_other edit by 15-02-2016
												'keluar_other'=> $keluar_other,
												
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												// 19-01-2016
												'tot_saldo'=> $tot_saldo
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
		// =================================== END NEW =======================================================
				
		// array brg wip
		$data_brg_wip[] = array(		'id_brg_wip'=> $id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna,
												'jum_stok_opname'=> $jum_stok_opname,
												'gtot_saldo'=> $tot_saldo
									);
		$data_tabel1 = array();
		$data_warna = array();
		$data_so_warna = array();
		return $data_brg_wip;
  }
  
  // 07-01-2016
  function get_sowipunitpacking($num, $offset, $id_unit, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_unit_packing WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_unit != '0')
		$sql.= " AND id_unit = '$id_unit' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC, id_unit ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				$data_so[] = array(			'id'=> $row1->id,	
											'bulan'=> $row1->bulan,	
											'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'id_unit'=> $row1->id_unit,	
											'kode_unit'=> $kode_unit,	
											'nama_unit'=> $nama_unit
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sowipunitpackingtanpalimit($id_unit, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_unit_packing WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_unit != '0')
		$sql.= " AND id_unit = '$id_unit' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  // 13-02-2016 lanjutan desta
  // contek dari transaksi_unit_jahit
  function get_transaksi_gudang_jadi($date_from, $date_to, $list_id_brg_wip) {		
	  // 14-01-2016 dikomen
	/*	$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		} */
		
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgldari = $thn1."-".$bln1."-".$tgl1;
		
		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$pisah1 = explode("-", $date_to);
		$tgl2= $pisah1[0];
		$bln2= $pisah1[1];
		$thn2= $pisah1[2];
		$tglke = $thn2."-".$bln2."-".$tgl2;
				
		$data_brg_wip = array();
		$data_warna = array();
		$data_so_warna = array();
		$jum_stok_opname = 0;
		
		// ------------------------------ 12-01-2016 ---------------------------------------------------------------
		$id_brg_wip_exp = explode(";", $list_id_brg_wip);
		foreach($id_brg_wip_exp as $row1) {
			if ($row1 != '') {
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$row1' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk_qc1'=> 0,
												'tot_masuk_unit_jahit1'=> 0,
												'tot_masuk_unit_packing1'=> 0,
												'tot_masuk_unit_packing_jahit1'=>0,
												'tot_keluar_retur_qc1'=> 0
											);
					}
				}
				
				// ambil SO dari tt_stok_opname_unit_jahit_detail
			/*	$sqlso = " SELECT b.id, b.jum_stok_opname, b.saldo_akhir
							FROM tt_stok_opname_unit_jahit a INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND a.id_unit = '$unit_jahit' AND b.id_brg_wip = '$row1' ";
				$queryso	= $this->db->query($sqlso);
				if ($queryso->num_rows() > 0){
					$hasilso = $queryso->row();
					$id_sawal_detail = $hasilso->id;
					$jum_stok_opname = $hasilso->jum_stok_opname;
					$saldo_akhir = $hasilso->saldo_akhir;
				}
				else {
					$saldo_akhir = 0;
					$id_sawal_detail = 0;
				} */
				$saldo_akhir = 0;
				$id_sawal_detail = 0;
				
				// 04-02-2016 saldo awal per warna
			/*	$sqlsowarna = " SELECT b.id_warna, b.saldo_akhir, c.nama FROM tt_stok_opname_unit_jahit_detail_warna b
							INNER JOIN tm_warna c ON b.id_warna = c.id
							WHERE b.id_stok_opname_unit_jahit_detail = '$id_sawal_detail' "; //echo $sqlsowarna."<br>";
							
				$querysowarna	= $this->db->query($sqlsowarna);
				if ($querysowarna->num_rows() > 0){
					$hasilsowarna = $querysowarna->result();
						
					foreach ($hasilsowarna as $rowxx) {
						$data_so_warna[] = array('id_warna'=> $rowxx->id_warna,
												'nama_warna'=> $rowxx->nama,
												'saldo'=> $rowxx->saldo_akhir
												);
					}
				} */				
				
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
				
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				$sql2= "SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as id_unit_jahit, 0 as id_unit_packing, 1 as masuk_qc, 0 as masuk_unit_jahit, 0 as masuk_unit_packing, 0 as masuk_unit_packing_jahit, 0 as keluar_retur_qc
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.jenis_keluar = '2' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'

						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit, 0 as id_unit_packing, 0 as masuk_qc, 1 as masuk_unit_jahit, 0 as masuk_unit_packing, 0 as masuk_unit_packing_jahit, 0 as keluar_retur_qc
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.jenis_masuk = '1' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as id_unit_jahit, a.id_unit_packing, 0 as masuk_qc, 0 as masuk_unit_jahit, 1 as masuk_unit_packing, 0 as masuk_unit_packing_jahit, 0 as keluar_retur_qc
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.jenis_masuk = '2' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
				UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit, a.id_unit_packing, 0 as masuk_qc, 0 as masuk_unit_jahit, 0 as masuk_unit_packing, 1 as masuk_unit_packing_jahit, 0 as keluar_retur_qc
						FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
						WHERE a.jenis_masuk = '3' AND b.id_brg_wip='$row1' ";
						//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				$sql2.= " AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
				
				UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as id_unit_jahit, 0 as id_unit_packing, 0 as masuk_qc, 0 as masuk_unit_jahit, 0 as masuk_unit_packing, 0 as masuk_unit_packing_jahit, 1 as keluar_retur_qc
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.jenis_masuk = '4' AND b.id_brg_wip='$row1'
						AND a.tgl_sj >= '".$tgldari."' AND a.tgl_sj <= '".$tglke."'
						ORDER BY tgl_sj ASC ";
								
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				// 18-01-2016, ini tadinya ke stok opname. diganti ke saldo_akhir
				$tot_saldo=$saldo_akhir;
				
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$id_unit_jahit = $row2->id_unit_jahit;
						$id_unit_packing = $row2->id_unit_packing;
						$masuk_qc = $row2->masuk_qc;
						$masuk_unit_jahit = $row2->masuk_unit_jahit;
						$masuk_unit_packing = $row2->masuk_unit_packing;
						$masuk_unit_packing_jahit = $row2->masuk_unit_packing_jahit;
						$keluar_retur_qc = $row2->keluar_retur_qc;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($masuk_qc == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '2'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_unit_jahit == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '1'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_unit_packing == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '2'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_unit_packing_jahit == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									INNER JOIN tm_sjmasukgudangjadi_detail_warna c ON b.id = c.id_sjmasukgudangjadi_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '3'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						// keluar
						if ($keluar_retur_qc == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '4'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						// --------------------
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											// 05-12-2015
											$tot_saldo+= $row3->qty;
											
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											// 05-12-2015
											$tot_saldo-= $row3->qty;
											
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_qc == '1') {
											$data_warna[$xx]['tot_masuk_qc1']+= $row3->qty;
										}
										else if ($masuk_unit_jahit== '1') {
											$data_warna[$xx]['tot_masuk_unit_jahit1']+= $row3->qty;
										}
										else if ($masuk_unit_packing== '1') {
											$data_warna[$xx]['tot_masuk_unit_packing1']+= $row3->qty;
										}
										else if ($masuk_unit_packing_jahit == '1') {
											$data_warna[$xx]['tot_masuk_unit_packing_jahit1']+= $row3->qty;
										}
										else if ($keluar_retur_qc == '1') {
											$data_warna[$xx]['tot_keluar_retur_qc1']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						if ($id_unit_jahit != 0) {
							$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit_jahit' ");
							$hasilrow = $query3->row();
							$kode_unit_jahit	= $hasilrow->kode_unit;
							$nama_unit_jahit	= $hasilrow->nama;
						}
						else {
							$kode_unit_jahit = '';
							$nama_unit_jahit = '';
						}
						
						if ($id_unit_packing != 0) {
							$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$id_unit_packing' ");
							$hasilrow = $query3->row();
							$kode_unit_packing	= $hasilrow->kode_unit;
							$nama_unit_packing	= $hasilrow->nama;
						}
						else {
							$kode_unit_packing = '';
							$nama_unit_packing = '';
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'kode_unit_jahit'=> $kode_unit_jahit,
												'nama_unit_jahit'=> $nama_unit_jahit,
												'kode_unit_packing'=> $kode_unit_packing,
												'nama_unit_packing'=> $nama_unit_packing,
												'masuk_qc'=> $masuk_qc,
												'masuk_unit_jahit'=> $masuk_unit_jahit,
												'masuk_unit_packing'=> $masuk_unit_packing,
												'masuk_unit_packing_jahit'=> $masuk_unit_packing_jahit,
												'keluar_retur_qc'=> $keluar_retur_qc,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												// 05-12-2015
												'tot_saldo'=> $tot_saldo
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
				
				// -----------------------------------------------------------------------------------------------------------------
				// =================================== END NEW =======================================================
				
				// array brg wip
				$data_brg_wip[] = array(		'id_brg_wip'=> $row1,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna,
												// 16-01-2016 ga pake SO, tapi saldo akhir
												//'jum_stok_opname'=> $jum_stok_opname,
												'saldo_akhir'=> $saldo_akhir,
												'gtot_saldo'=> $tot_saldo
									);
				$data_tabel1 = array();
				$data_warna = array();
				$data_so_warna = array();
				
			} // END IF ROW1
		} // END FOREACH
		//-----------------------------------------------------------------------------------
		
			//} // endforeach header
		//}
		//else {
			//$data_brg_jadi = '';
		//}
		return $data_brg_wip;
  }
  
  // 13-02-2016 lanjutan desta
  // contek dari savesounitjahit
  function savesounitpacking($id_so, $tgl_so, $unit_packing, $jenis_perhitungan_stok, $id_itemnya, $id_brg_wip, 
		$id_warna, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  //-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			// 11-03-2015
			//$auto_saldo_akhir_warna[$xx] = trim($auto_saldo_akhir_warna[$xx]);
			
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
		// ---------------------------------------------------------------------
		$totalxx = $qtytotalstokfisik;
	
		//$totalxx = $stok_fisik;
			
		// ambil id detail id_stok_opname_unit_packing_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail 
					where id_brg_wip = '$id_brg_wip' 
					AND id_stok_opname_unit_packing = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
			
		//echo $id_itemnya."<br>";
		
		// 04-02-2016 SKRIP DIBAWAH INI GA DIPAKE LG
		
		// ======== update stoknya! =============
			// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
		
			//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
		/*	$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '1' ";
			$query3	= $this->db->query($sql3);
						// AND a.tgl_bonm >= '$tglawalplus'
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_bgs = $hasilrow->jum_masuk;
							
				if ($masuk_bgs == '')
					$masuk_bgs = 0;
			}
			else
				$masuk_bgs = 0;
						
			//2. hitung brg masuk retur brg wip dari QC, dari tm_sjkeluarwip
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.=" AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_returbrgwip = $hasilrow->jum_masuk;
							
				if ($masuk_returbrgwip == '')
					$masuk_returbrgwip = 0;
			}
			else
				$masuk_returbrgwip = 0;
						
			//3. hitung brg masuk pengembalian dari QC, dari tm_bonmkeluarcutting
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_pengembalian = $hasilrow->jum_masuk;
							
				if ($masuk_pengembalian == '')
					$masuk_pengembalian = 0;
			}
			else
				$masuk_pengembalian = 0;
						
			$jum_masuk = $masuk_bgs+$masuk_returbrgwip+$masuk_pengembalian;
			// ------------------------------------------ END MASUK --------------------------------
						
			// 4. hitung brg keluar bagus, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs = $hasilrow->jum_keluar;
							
				if ($keluar_bgs == '')
					$keluar_bgs = 0;
			}
			else
				$keluar_bgs = 0;
						
			// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_perbaikan = $hasilrow->jum_keluar;
							
				if ($keluar_perbaikan == '')
					$keluar_perbaikan = 0;
			}
			else
				$keluar_perbaikan = 0;
						
			// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
				if ($keluar_retur_bhnbaku == '')
					$keluar_retur_bhnbaku = 0;
			}
			else
				$keluar_retur_bhnbaku = 0;
			
			// 30-10-2015
			// 7. hitung brg keluar bgs ke gudang jadi, dari tm_sjmasukgudangjadi
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs_gudangjadi = $hasilrow->jum_keluar;
							
				if ($keluar_bgs_gudangjadi == '')
					$keluar_bgs_gudangjadi = 0;
			}
			else
				$keluar_bgs_gudangjadi = 0;
						
			$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku+$keluar_bgs_gudangjadi;
			// -------------------------------- END BARANG KELUAR -----------------------------------------------
			
			$jum_stok_akhir = $jum_masuk-$jum_keluar;
			$totalxx = $totalxx + $jum_stok_akhir; */
			
			//cek stok terakhir tm_stok_unit_packing, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_packing 
							WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$unit_packing' ");
					if ($query3->num_rows() == 0){
						$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_packing ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrow	= $seqxx->row();
							$id_stok	= $seqrow->id+1;
						}else{
							$id_stok	= 1;
						}
												
						$data_stok = array(
							'id'=>$id_stok,
							'id_brg_wip'=>$id_brg_wip,
							'id_unit'=>$unit_jahit,
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_packing', $data_stok);
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
												
						$this->db->query(" UPDATE tm_stok_unit_packing SET stok = '$totalxx', 
						tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_unit='$unit_packing' ");
					}
					
			// 04-02-2016
			// stok_unit_jahit_warna
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					
					$totalxx = $stok_fisik[$xx];
					
					//cek stok terakhir tm_stok_unit_packing_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_packing_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_unit_packing='$id_stok' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_unit_packing_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_packing_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_unit_packing'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$totalxx,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_packing_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_unit_packing_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_unit_packing='$id_stok' ");
					}
					
					if ($id_itemnya != '0') {
						$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."'
							WHERE id_stok_opname_unit_packing_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
					}
				} // end for
				
		if ($id_itemnya != '0') {
			$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET jum_stok_opname = '$qtytotalstokfisik',
					status_approve='t' where id = '$iddetail' ");
		}
		
		if ($id_itemnya == '0') {
			$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailnew	= $seqrow->id+1;
			}else{
				$iddetailnew	= 1;
			}
			
			$databaru = array(
						'id'=>$iddetailnew,
						'id_stok_opname_unit_packing'=>$id_so,
						'id_brg_wip'=>$id_brg_wip, 
						'stok_awal'=>0,
						'jum_stok_opname'=>$qtytotalstokfisik,
						'status_approve'=> 't'
					);
		   $this->db->insert('tt_stok_opname_unit_packing_detail',$databaru);
		   
		   // insert ke tabel SO warna
		   for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);				
				
				$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}
						
						$tt_stok_opname_unit_packing_detail_warna	= array(
								 'id'=>$idbaru,
								 'id_stok_opname_unit_packing_detail'=>$iddetailnew,
								 'id_warna'=>$id_warna[$xx],
								 'jum_stok_opname'=>$stok_fisik[$xx],
								 'saldo_akhir'=>0
							);
							$this->db->insert('tt_stok_opname_unit_packing_detail_warna',$tt_stok_opname_unit_packing_detail_warna);
			} // end for
		} // end if item baru
			 // ====================================================================================
  }
  
  // 12-03-2014
 /* 
  
  function cek_presentasikerjaunit($unit_jahit, $bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit
						WHERE kode_unit='$unit_jahit' AND bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  // new 16-04-2014
  function get_brgjadi_unit_jahit($unit_jahit) {		
		$sql = " SELECT distinct a.kode_brg_jadi, b.e_product_motifname FROM tm_stok_unit_jahit a, tr_product_motif b 
				WHERE a.kode_brg_jadi = b.i_product_motif AND a.kode_unit = '$unit_jahit'
				ORDER BY a.kode_brg_jadi ";
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$databrgjadi = array();			
			foreach ($hasil as $row) {				
				$databrgjadi[] = array( 'id'=> '0',
										'total_rata'=> '0',
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'datapergrup'=> ''
									);
			}
		}
		else {
			$databrgjadi = '';
		}
		return $databrgjadi;
  }
  
  function get_presentasi_kerja_unit($unit_jahit, $bulan, $tahun) {
		$query	= $this->db->query(" SELECT a.total_rata as total_rata, b.id as id, b.kode_brg_jadi, c.e_product_motifname FROM 
					tm_presentasi_kerja_unit_detail b, 
					tm_presentasi_kerja_unit a, 
					tr_product_motif c
					WHERE b.id_presentasi_kerja_unit = a.id 
					AND b.kode_brg_jadi = c.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.kode_unit = '$unit_jahit'
					
					UNION SELECT 0 as total_rata, 0 as id, a.kode_brg_jadi, b.e_product_motifname FROM
					tm_stok_unit_jahit a, tr_product_motif b
					WHERE a.kode_brg_jadi = b.i_product_motif AND a.kode_unit = '$unit_jahit'
					AND a.kode_brg_jadi NOT IN (select y.kode_brg_jadi FROM tm_presentasi_kerja_unit_detail y, 
					tm_presentasi_kerja_unit x WHERE x.id=y.id_presentasi_kerja_unit 
					AND x.bulan='$bulan' AND x.tahun='$tahun' AND x.kode_unit = '$unit_jahit' )
					ORDER BY kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			$datapergrup = array();
			foreach ($hasil as $row) {
				
				$sqlxx = " SELECT * FROM tm_presentasi_kerja_unit_detail_grupjahit WHERE id_presentasi_kerja_unit_detail='$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$datapergrup[] = array( 
										'hari'=> $rowxx->hari,
										'kapasitas'=> $rowxx->kapasitas,
										'minggu1'=> $rowxx->minggu1,
										'minggu2'=> $rowxx->minggu2,
										'minggu3'=> $rowxx->minggu3,
										'minggu4'=> $rowxx->minggu4,
										'total'=> $rowxx->total,
										'persentase'=> $rowxx->persentase,
										'nama_grup_jahit'=> $rowxx->nama_grup_jahit
									);
					}
				}
				else
					$datapergrup = '';
				
				$datadetail[] = array( 'total_rata'=> $row->total_rata,
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'datapergrup'=> $datapergrup
									);
				$datapergrup = array();
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function savepresentasikerjaunit($iddetail, $hari, $kapasitas, $minggu1, $minggu2, $minggu3, $minggu4, $total, $persentase, $nama_grup){ 
	  $seq	= $this->db->query(" SELECT id FROM tm_presentasi_kerja_unit_detail_grupjahit ORDER BY id DESC LIMIT 1 ");
	  if($seq->num_rows() > 0) {
		$seqrow	= $seq->row();
		$iditem	= $seqrow->id+1;
	  }else{
		$iditem	= 1;
	  }
	  
	  $data_detail = array(
						'id'=>$iditem,
						'id_presentasi_kerja_unit_detail'=>$iddetail,
						'hari'=>$hari,
						'kapasitas'=>$kapasitas,
						'minggu1'=>$minggu1,
						'minggu2'=>$minggu2,
						'minggu3'=>$minggu3,
						'minggu4'=>$minggu4,
						'total'=>$total,
						'persentase'=>$persentase,
						'nama_grup_jahit'=>$nama_grup
					);
		   $this->db->insert('tm_presentasi_kerja_unit_detail_grupjahit',$data_detail);
  } 
  // ----------------------------------------------------------------------------------------
  
  function cek_forecast($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tm_forecast_wip WHERE bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_brgjadi() {
		$sql = " SELECT distinct b.i_product_motif, b.e_product_motifname FROM tr_product_base a, tr_product_motif b 
				WHERE a.i_product_base = b.i_product ORDER BY b.i_product_motif ";
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$databrgjadi = array();			
			foreach ($hasil as $row) {				
				$databrgjadi[] = array( 'id'=> '0',
										'kode_brg_jadi'=> $row->i_product_motif,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> '0',
										'keterangan'=> ''
									);
			}
		}
		else {
			$databrgjadi = '';
		}
		return $databrgjadi;
  }
  
  function get_forecast($bulan, $tahun) {
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_detail b, 
					tm_forecast_wip a, 
					tr_product_motif c
					WHERE b.id_forecast_wip = a.id 
					AND b.kode_brg_jadi = c.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY b.id ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function saveforecast($is_new, $id_fc, $iddetail, $kode_brg_jadi, $fc, $ket){ 
	  $tgl = date("Y-m-d H:i:s"); 
	 
	 // 02-04-2014, ga pake is_new lagi. langsung save aja setelah delete detail sebelumnya
	//  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailx	= $seqrow->id+1;
			}else{
				$iddetailx	= 1;
			}
		  
		   $data_detail = array(
						'id'=>$iddetailx,
						'id_forecast_wip'=>$id_fc,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'qty'=>$fc,
						'keterangan'=>$ket
					);
		   $this->db->insert('tm_forecast_wip_detail',$data_detail);
  }
  
  // 02-04-2014
  function get_stokmingguan_unit($unit_jahit) {		
	  	  
		// 1. ambil data2 unit jahit
		$filter = "";
		if ($unit_jahit != '0')
			$filter = " WHERE a.kode_unit = '$unit_jahit' ";
		$sql = "SELECT distinct a.kode_unit FROM tm_stok_unit_jahit a
				INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit
				".$filter." ORDER BY a.kode_unit";
		
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row) {
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				//echo "SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit'  <br>";
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = " SELECT kode_brg_jadi, stok, stok_bagus, stok_perbaikan FROM tm_stok_unit_jahit
						WHERE kode_unit = '$row->kode_unit' ";				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';

						$data_stok[] = array(		'kode_brg'=> $row2->kode_brg_jadi,
													'nama_brg'=> $e_product_motifname,
													'stok'=> $row2->stok,
													'stok_bagus'=> $row2->stok_bagus,
													'stok_perbaikan'=> $row2->stok_perbaikan
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------

				// array unit jahit
				$data_unit_jahit[] = array(		'kode_unit'=> $row->kode_unit,
												'nama_unit'=> $nama_unit,
												'data_stok'=> $data_stok
									);
				$data_stok = array();
			} // endforeach header
		}
		else {
			$data_unit_jahit = '';
		}
		return $data_unit_jahit;
  }
  
  // 03-04-2014
  function get_bhnbaku_unit($date_from, $date_to) {		
	  
	  // ambil data forecast bulan sebelumnya (bulan sebelum date_from)
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		//$tgldari = $thn1."-".$bln1."-".$tgl1;
		
		
		if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
		// 1. ambil data2 distinct kode brg jadi dari rentang tanggal yg dipilih		
		$sql = " SELECT distinct b.kode_brg_jadi FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
				WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND a.jenis_keluar = '1'
				ORDER BY b.kode_brg_jadi ";
		
		$query	= $this->db->query($sql);
		$data_brg_jadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row) {
				// ambil nama brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				$query3	= $this->db->query(" SELECT b.qty FROM tm_forecast_wip a, tm_forecast_wip_detail b 
							WHERE a.id = b.id_forecast_wip AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.bulan='$bln1' AND a.tahun='$thn1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$qty_forecast = $hasilrow->qty;
				}
				else
					$qty_forecast = 0;
				// --------------------------------------------------------------
						
				// 2. Query utk ambil unit2 jahit berdasarkan perulangan brg jadi
				$sql2 = " SELECT distinct a.kode_unit FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
				WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi='$row->kode_brg_jadi' 
				AND a.jenis_keluar = '1' ORDER BY a.kode_unit ";				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					$jmldipenuhi = 0;
					foreach ($hasil2 as $row2) { // perulangan unit2 jahitnya
						$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
						
						// ambil jumlah qty barang berdasarkan unit
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
								WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
								AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND kode_unit = '$row2->kode_unit'
								AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.jenis_keluar = '1' ");
						$hasilrow = $query3->row();
						$jum	= $hasilrow->jum;
						
						$jmldipenuhi+=$jum;
						
						$data_stok[] = array(		'kode_unit'=> $row2->kode_unit,
													'nama_unit'=> $nama_unit,
													'jum'=> $jum
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------
				$sisa_forecast = $qty_forecast-$jmldipenuhi;
				// array brg jadi
				$data_brg_jadi[] = array(		'kode_brg_jadi'=> $row->kode_brg_jadi,
												'nama_brg_jadi'=> $e_product_motifname,
												'bln_forecast'=> $nama_bln,
												'thn_forecast'=> $thn1,
												'qty_forecast'=> $qty_forecast,
												'sisa_forecast'=> $sisa_forecast,
												'data_stok'=> $data_stok
									);
				$data_stok = array();
			} // endforeach header
		}
		else {
			$data_brg_jadi = '';
		}
		return $data_brg_jadi;
  }
  
  // 05-04-2014
  function get_grup_jahit($unit_jahit){
    $query = $this->db->query(" SELECT * FROM tm_grup_jahit WHERE kode_unit='$unit_jahit' ORDER BY nama_grup ");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 19-04-2014
  function get_rekap_presentasi_kerja_unit($bulan, $tahun, $unit_jahit) {		
	  
		// 1. ambil data2 unit jahit dari periode yg dipilih
		$filter = "";
		if ($unit_jahit != '0')
			$filter = " AND kode_unit = '$unit_jahit' ";
		
		$sql = " SELECT id, total_rata, kode_unit FROM tm_presentasi_kerja_unit WHERE bulan='$bulan' AND tahun='$tahun' ".$filter."
				ORDER BY kode_unit ";
					
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
						
			foreach ($hasil as $row) {
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query distinct nama grup jahit
				// modif 15-05-2014
				$sql2 = " SELECT DISTINCT c.nama_grup_jahit FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.bulan='$bulan' AND a.tahun='$tahun' AND a.kode_unit='$row->kode_unit' 
						AND c.persentase <> '0'
						ORDER BY c.nama_grup_jahit ";
				$query2	= $this->db->query($sql2);
				
				$data_grup_jahit = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						// 2. Query item2 brgnya berdasarkan nama grup
						// modif 15-05-2014
						$sql2x = " SELECT b.kode_brg_jadi, c.* FROM tm_presentasi_kerja_unit a, 
								tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
								WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
								AND a.bulan='$bulan' AND a.tahun='$tahun' AND a.kode_unit='$row->kode_unit'
								AND c.nama_grup_jahit='$row2->nama_grup_jahit' 
								AND c.persentase <> '0'
								ORDER BY b.kode_brg_jadi "; 
						$query2x	= $this->db->query($sql2x);
						
						$data_item_brg = array();
						if ($query2x->num_rows() > 0){
							$hasil2x = $query2x->result();
							foreach ($hasil2x as $row2x) {
								$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2x->kode_brg_jadi' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$e_product_motifname = $hasilrow->e_product_motifname;
								}
								else
									$e_product_motifname = '';
									
								$data_item_brg[] = array('kode_brg_jadi'=> $row2x->kode_brg_jadi,
														'nama_brg_jadi'=> $e_product_motifname,
														'hari'=> $row2x->hari,
														'kapasitas'=> $row2x->kapasitas,
														'minggu1'=> $row2x->minggu1,
														'minggu2'=> $row2x->minggu2,
														'minggu3'=> $row2x->minggu3,
														'minggu4'=> $row2x->minggu4,
														'total'=> $row2x->total,
														'persentase'=> $row2x->persentase
													);
								
							} // end for2x
						} // end if2x
						else
							$data_item_brg = '';
									
						$data_grup_jahit[] = array('nama_grup_jahit'=> $row2->nama_grup_jahit,
												   'data_item_brg'=> $data_item_brg
													);
						$data_item_brg = array();
					//-----------------------------------------------------------------
				} // end for2
			} // end if2
			else
				$data_grup_jahit = '';
				
			$data_unit_jahit[] = array('kode_unit'=> $row->kode_unit,
									   'nama_unit'=> $nama_unit,
									   'total_rata'=> $row->total_rata,
									   'data_grup_jahit'=> $data_grup_jahit);
		} // end for
	} // end if
	else {
		$data_unit_jahit = '';
	}
	return $data_unit_jahit;
  }
  
  // 28-04-2014
  function get_rekap_presentasi_kerja_unit_tahunan($tahun) {		
	  // 16-10-2014
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautama = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql = " SELECT distinct a.kode_unit, c.nama_grup_jahit FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.tahun='$tahun' AND c.persentase<>'0' AND a.kode_unit = '".$rowutama->kode_unit."'
						ORDER BY a.kode_unit, c.nama_grup_jahit ";
			  
			   $query	= $this->db->query($sql);
			   $data_rekap = array();
				
				if ($query->num_rows() > 0){
				$hasil = $query->result();
								
				foreach ($hasil as $row) {
					// hitung masing2 total presentase utk tiap2 grup jahit
					for ($bulan=1; $bulan<=12; $bulan++) {
						if ($bulan < 10)
							$bln = "0".$bulan;
						else
							$bln = $bulan;
												
						$query3	= $this->db->query(" SELECT AVG(persentase) AS rata FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c 
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.tahun='$tahun' AND a.bulan='$bln' AND c.nama_grup_jahit='$row->nama_grup_jahit'
						AND a.kode_unit = '$row->kode_unit' AND c.persentase<>'0' ");
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$rata = $hasilrow->rata;
							if ($rata == '')
								$rata = 0;
						}
						else
							$rata= 0;
						
						$listratabulan[] = array($bulan => $rata);
					} // end for
					
					// ambil nama unit
					$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
					$hasilrow = $query3->row();
					$nama_unit	= $hasilrow->nama;
					
					$data_rekap[] = array('kode_unit'=> $row->kode_unit,
										  'nama_unit'=> $nama_unit,
										  'nama_grup_jahit'=> $row->nama_grup_jahit,
										  'listratabulan'=> $listratabulan);
					$listratabulan=array();
				} // end for
			  }
			  else
				$data_rekap = '';
				
			  $datautama[] = array(		'kode'=> $rowutama->kode,
										'nama'=> $rowutama->nama,
										'data_rekap'=> $data_rekap
													);
			} // end for
		} // end if	  
	  
		return $datautama;
	  //----------------------------------------------------------------------------------------------------------------------
  }
  
  // 30-04-2014
  function get_forecastvsschedule($bulan, $tahun) {
	  
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// modif 17-09-2014
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_detail b 
					INNER JOIN tm_forecast_wip a ON b.id_forecast_wip = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY d.kode, b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				// 17-09-2014, ambil SO di semua unit jahit di bulan sebelumnya
				$queryx	= $this->db->query(" SELECT sum(b.jum_bagus) as jumbagus FROM tt_stok_opname_unit_jahit a, 
											tt_stok_opname_unit_jahit_detail b
											WHERE a.id = b.id_stok_opname_unit_jahit
											AND b.kode_brg_jadi = '$row->kode_brg_jadi'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' ");
						
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal_bgs = $hasilrow->jumbagus;
					if ($saldo_awal_bgs == '')
						$saldo_awal_bgs = 0;
				}
				else {
					$saldo_awal_bgs = 0;
				}
				
				// KOLOM SCHEDULE ambil data barang masuk ke unit jahit
				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip

						
						//$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						$jum_masuk = $masuk_bgs;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit <> '0' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip

						
						//$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						$jum_keluar = $keluar_bgs;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						// tambahan 27-08-2014, kolom schedule dimodif perhitungannya
						//kolom schedule = brg masuk - stock schedule bln trsbt + stock schedule 1 bln sblmnya
						
						// a. stok schedule bulan tsb
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bulan' AND tahun='$tahun' AND kode_brg_jadi = '$row->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule = $hasilrow->qty;
							
							if ($qty_schedule == '')
								$qty_schedule = 0;
						}
						else
							$qty_schedule = 0;
						
						// b. stok schedule 1 bulan sebelumnya
						if ($bulan == 1) {
							$bln_query = 12;
							$thn_query = $tahun-1;
						}
						else {
							$bln_query = $bulan-1;
							$thn_query = $tahun;
							if ($bln_query < 10)
								$bln_query = "0".$bln_query;
						}
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bln_query' AND tahun='$thn_query' AND kode_brg_jadi = '$row->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule_sebelumnya = $hasilrow->qty;
							
							if ($qty_schedule_sebelumnya == '')
								$qty_schedule_sebelumnya = 0;
						}
						else
							$qty_schedule_sebelumnya = 0;
						
						$totalqtyschedule = $jum_masuk-$qty_schedule+$qty_schedule_sebelumnya;
						//===========================================================
						if ($row->qty != 0) {
							//$persenschedule = ($jum_masuk/$row->qty)*100;
							$persenschedule = ($totalqtyschedule/$row->qty)*100;
							$persenforecast = ($jum_keluar/$row->qty)*100;
						}
						else {
						//echo $row->qty."<br>";
							$persenschedule = 0;
							$persenforecast = 0;
						}
						// 17-09-2014
						//echo $totalqtyschedule." ".$saldo_awal_bgs."<br>";
						$temptotal = $totalqtyschedule+$saldo_awal_bgs;
						if ($temptotal != 0)
							$persenforecast2 = ($jum_keluar/($totalqtyschedule+$saldo_awal_bgs))*100;
						else
							$persenforecast2 = 0;
						
				//17-09-2014, ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan,
										//'schedule'=> $jum_masuk,
										'schedule'=> $totalqtyschedule,
										'brgmasuk'=> $jum_keluar,
										'persenschedule'=> $persenschedule,
										//'persenforecast'=> $persenforecast,
										'persenforecast2'=> $persenforecast2,
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel,
										'saldo_awal_bgs'=> $saldo_awal_bgs
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  // 24-05-2014
  function get_mutasi_unit_stokschedule($unit_jahit, $bulan, $tahun) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
	  
				// Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = "SELECT a.kode_brg_jadi FROM (
					select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
					WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit_jahit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
					WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01'   
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit_jahit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b 
					WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01'   
					AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b 
					WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tt_stok_opname_unit_jahit a, tt_stok_opname_unit_jahit_detail b
					WHERE a.id=b.id_stok_opname_unit_jahit AND a.bulan='$bulan' AND a.tahun='$tahun' 
					AND a.kode_unit = '$unit_jahit' AND a.status_approve='t' AND b.jum_stok_opname <> 0 
					GROUP BY b.kode_brg_jadi
					)
					a GROUP BY a.kode_brg_jadi ORDER BY a.kode_brg_jadi";
				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
																		
						$data_stok[] = array(		'id'=> 0,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'stok_schedule'=> 0
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				return $data_stok;
				//-----------------------------------------------------------------
  }
  
  function get_stok_schedule($unit_jahit, $bulan, $tahun) {
		$query	= $this->db->query(" SELECT a.id, a.kode_brg_jadi, a.qty, b.e_product_motifname 
					FROM tm_stok_schedule_wip a, tr_product_motif b
					WHERE a.kode_brg_jadi = b.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.kode_unit = '$unit_jahit'
					ORDER BY a.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datastok = array();
			foreach ($hasil as $row) {
				$datastok[] = array( 	'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'stok_schedule'=> $row->qty
									);
			}
		}
		else {
			$datastok = '';
		}
		return $datastok;
  }
  
  function cek_stokschedule($unit_jahit, $bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tm_stok_schedule_wip
						WHERE kode_unit='$unit_jahit' AND bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  // 28-06-2014
  function get_transaksi_hasil_jahit($bulan, $tahun, $id_gudang, $kode_brg_jadi) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// 1. ambil data2 stok di gudang WIP perusahaan
		$sql = " SELECT id, kode_brg_jadi FROM tm_stok_hasil_jahit WHERE id_gudang = '$id_gudang' ";
		if ($kode_brg_jadi != '')
			$sql.= " AND kode_brg_jadi = '$kode_brg_jadi' ";
		$sql.=" ORDER BY kode_brg_jadi ";
		$query	= $this->db->query($sql);
		$data_brg_jadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row) {
				$data_warna = array();
				$data_so_warna = array();
				
				//ambil data2 warna dari tm_stok_hasil_jahit_warna
				$sqlwarna = " SELECT a.kode_warna, b.nama FROM tm_stok_hasil_jahit_warna a, tm_warna b 
							WHERE a.kode_warna=b.kode AND a.id_stok_hasil_jahit='$row->id' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk1'=> 0,
												'tot_keluar1'=> 0,
												'tot_masuk2'=> 0,
												'tot_keluar2'=> 0,
												'tot_masuk3'=> 0,
												'tot_keluar3'=> 0,
												'tot_masuk4'=> 0,
												'tot_keluar4'=> 0
											);
					}
				}
				
				// ambil SO brg jadi dari tt_stok_opname_hasil_jahit_detail_warna
				$sqlwarna = " SELECT c.kode_warna, d.nama, c.jum_stok_opname 
							FROM tt_stok_opname_hasil_jahit a, tt_stok_opname_hasil_jahit_detail b, 
							tt_stok_opname_hasil_jahit_detail_warna c, tm_warna d 
							WHERE a.id = b.id_stok_opname_hasil_jahit AND b.id = c.id_stok_opname_hasil_jahit_detail 
							AND c.kode_warna=d.kode AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
							ORDER BY d.nama "; //echo $sqlwarna; die();
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['kode_warna'] == $rowwarna->kode_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_stok_opname;
							}
						} // end for
						
						$data_so_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_stok_opname'=> $rowwarna->jum_stok_opname
											);
					}
				}
				
				// ambil nama brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				// QUERY UTK KELUAR MASUK BRG JADI KE UNIT DAN KE PACKING/GDG JADI
				// 13-11-2014: utk BLN, packing dan gdg jadi dipisah. tambahkan juga query dgn jenis Lain-lain
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, a.jenis_masuk as jenis, 1 as masukwipunit, 0 as keluarwipunit, 0 as masukwippacking, 0 as keluarwippacking, 0 as masuklain, 0 as keluarlain
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip 
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						 AND (a.jenis_masuk = '1' OR a.jenis_masuk = '2')
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, a.jenis_keluar as jenis, 0 as masukwipunit, 1 as keluarwipunit, 0 as masukwippacking, 0 as keluarwippacking, 0 as masuklain, 0 as keluarlain
						FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						AND a.jenis_keluar = '3'
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, a.jenis_masuk as jenis, 0 as masukwipunit, 0 as keluarwipunit, 1 as masukwippacking, 0 as keluarwippacking, 0 as masuklain, 0 as keluarlain
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip 
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						 AND (a.jenis_masuk = '3' OR a.jenis_masuk = '4')
						 UNION
						 SELECT distinct a.id, a.no_sj, a.tgl_sj, a.jenis_keluar as jenis, 0 as masukwipunit, 0 as keluarwipunit, 0 as masukwippacking, 1 as keluarwippacking, 0 as masuklain, 0 as keluarlain
						FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						AND (a.jenis_keluar = '1' OR a.jenis_keluar = '2')
						
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, a.jenis_masuk as jenis, 0 as masukwipunit, 0 as keluarwipunit, 0 as masukwippacking, 0 as keluarwippacking, 1 as masuklain, 0 as keluarlain
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip 
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						 AND a.jenis_masuk = '5'
						UNION
						 SELECT distinct a.id, a.no_sj, a.tgl_sj, a.jenis_keluar as jenis, 0 as masukwipunit, 0 as keluarwipunit, 0 as masukwippacking, 0 as keluarwippacking, 0 as masuklain, 1 as keluarlain
						FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						AND a.jenis_keluar = '4'
						ORDER BY tgl_sj ASC ";
				
				// 10-05-2014 contoh union di modul info-pembelian mmaster.php.
				
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$jenis = $row2->jenis;
						$is_masukwipunit = $row2->masukwipunit;
						$is_keluarwipunit = $row2->keluarwipunit;
						$is_masukwippacking = $row2->masukwippacking;
						$is_keluarwippacking = $row2->keluarwippacking;
						$is_masuklain = $row2->masuklain;
						$is_keluarlain = $row2->keluarlain;
												
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($is_masukwipunit == '1' || $is_masukwippacking == '1' || $is_masuklain == '1') {
							$sql3 = " SELECT c.kode_warna, d.nama, c.qty FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c, tm_warna d 
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND c.kode_warna = d.kode
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						else if ($is_keluarwipunit == '1' || $is_keluarwippacking == '1' || $is_keluarlain == '1') {
							$sql3 = " SELECT c.kode_warna, d.nama, c.qty FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c, tm_warna d 
									WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
									AND c.kode_warna = d.kode
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$sqlxx = " SELECT nama FROM tm_warna WHERE kode='$row3->kode_warna' ";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nama_warna = $hasilxx->nama;
								}
								else
									$nama_warna = '';
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['kode_warna'] == $row3->kode_warna) {
										if ($masuk == "ya") {
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											$data_warna[$xx]['saldo']-= $row3->qty;
											//$data_warna[$xx]['tot_keluar']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($is_masukwipunit == '1') {
											$data_warna[$xx]['tot_masuk1']+= $row3->qty;
										}
										else if ($is_keluarwipunit == '1') {
											$data_warna[$xx]['tot_keluar1']+= $row3->qty;
										}
										else if ($is_masukwippacking == '1') {
											if ($jenis == 3)
												$data_warna[$xx]['tot_masuk2']+= $row3->qty;
											else
												$data_warna[$xx]['tot_masuk3']+= $row3->qty;
										}
										else if ($is_keluarwippacking == '1') {
											if ($jenis == 1)
												$data_warna[$xx]['tot_keluar2']+= $row3->qty;
											else
												$data_warna[$xx]['tot_keluar3']+= $row3->qty;
										}
										else if ($is_masuklain == '1') {
											$data_warna[$xx]['tot_masuk4']+= $row3->qty;
										}
										else if ($is_keluarlain == '1') {
											$data_warna[$xx]['tot_keluar4']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'jenis'=> $jenis,
												'is_masukwipunit'=> $is_masukwipunit,
												'is_keluarwipunit'=> $is_keluarwipunit,
												'is_masukwippacking'=> $is_masukwippacking,
												'is_keluarwippacking'=> $is_keluarwippacking,
												// 13-11-2014
												'is_masuklain'=> $is_masuklain,
												'is_keluarlain'=> $is_keluarlain,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
				// =================================== END NEW =======================================================
				
				// array brg jadi
				$data_brg_jadi[] = array(		'kode_brg_jadi'=> $row->kode_brg_jadi,
												'nama_brg_jadi'=> $e_product_motifname,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna
									);
				$data_tabel1 = array();
				$data_tabel2 = array();
				$data_warna = array();
				$data_so_warna = array();
			} // endforeach header
		}
		else {
			$data_brg_jadi = '';
		}
		return $data_brg_jadi;
  }
  
  // 29-08-2014
  function get_mutasi_unit_qc($bulan, $tahun) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
		// 1. query brg bagus
		// 16-10-2014
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautamabagus = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
						INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						AND a.jenis_masuk = '1' AND a.kode_unit_jahit = '".$rowutama->kode_unit."'
					  UNION SELECT DISTINCT a.kode_unit as kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a
					  INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
					  WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
					  AND a.kode_unit = '".$rowutama->kode_unit."'
					 ORDER BY kode_unit_jahit, kode_brg_jadi
					 ";
				$query2	= $this->db->query($sql2);
				
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
						
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
																		
						// ambil jum masuk wip (SJ masuk WIP)=====================
						// 1. hitung brg masuk WIP bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row2->kode_unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
												
						// 2. hitung brg masuk retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_retur_bhnbaku = $hasilrow->jum_masuk;
							
							if ($masuk_retur_bhnbaku == '')
								$masuk_retur_bhnbaku = 0;
						}
						else
							$masuk_retur_bhnbaku = 0;
						
						$jum_masuk_bgs = $masuk_bgs+$masuk_retur_bhnbaku;
						//$jum_masuk_perbaikan = $masuk_perbaikan;
						//$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						// -------------------------------- END BARANG MASUK -----------------------------------------------
					
						//========================================================
						
						// ambil retur, dari tabel tm_retur_qc_wip
						$query3	= $this->db->query(" SELECT qty FROM tm_retur_qc_wip WHERE kode_unit = '$row2->kode_unit_jahit'
											AND bulan='$bulan' AND tahun='$tahun' AND jenis = 'BAGUS'
											AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_retur_bgs = $hasilrow->qty;
						}
						else {
							$jum_retur_bgs = 0;
						}
						
						$gradea_bgs = $jum_masuk_bgs-$jum_retur_bgs;
						//$gradea_perbaikan = $jum_masuk_perbaikan-$jum_retur_perbaikan;
						
						if ($jum_masuk_bgs != 0)
							$persena_bgs = ($gradea_bgs/$jum_masuk_bgs)*100;
						else
							$persena_bgs = 0;
						//$persena_perbaikan = $gradea_perbaikan/$jum_masuk_perbaikan;
						
						if ($jum_masuk_bgs != 0)
							$persen_retur_bgs = ($jum_retur_bgs/$jum_masuk_bgs)*100;
						else
							$persen_retur_bgs = 0;
						//$persen_retur_perbaikan = $jum_retur_perbaikan/$jum_masuk_perbaikan;
						
						$data_stok[] = array(		'id'=> 0,
													'kode_unit_jahit'=> $row2->kode_unit_jahit,
													'nama_unit_jahit'=> $nama_unit_jahit,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'qty'=> $jum_masuk_bgs,
													'retur'=> $jum_retur_bgs,
													'gradea'=> $gradea_bgs,
													'persena'=> $persena_bgs,
													'persen_retur'=> $persen_retur_bgs
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				
				$datautamabagus[] = array(		'kode'=> $rowutama->kode,
												'nama'=> $rowutama->nama,
												'data_stok'=> $data_stok
													);
					
			} // end foreach
		} // end if queryutama
		

		//$query2	= $this->db->query($sql2);
								
				
		// 2. barang perbaikan
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautamaperbaikan = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
							WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
							AND a.jenis_masuk = '2' AND a.kode_unit_jahit = '".$rowutama->kode_unit."' 
							ORDER BY a.kode_unit_jahit, b.kode_brg_jadi ";
				$query2	= $this->db->query($sql2);
				
				$data_stok2 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
						
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
																		
						// ambil jum masuk wip (SJ masuk WIP)=====================
						// 1. hitung brg masuk WIP PERBAIKAN, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row2->kode_unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_perbaikan = $hasilrow->jum_masuk;
							
							if ($masuk_perbaikan == '')
								$masuk_perbaikan = 0;
						}
						else
							$masuk_perbaikan = 0;
						
						$jum_masuk_perbaikan = $masuk_perbaikan;
						// -------------------------------- END BARANG MASUK -----------------------------------------------
					
						//========================================================
						
						// ambil retur, dari tabel tm_retur_qc_wip
						$query3	= $this->db->query(" SELECT qty FROM tm_retur_qc_wip WHERE kode_unit = '$row2->kode_unit_jahit'
											AND bulan='$bulan' AND tahun='$tahun' AND jenis = 'PERBAIKAN'
											AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_retur_perbaikan = $hasilrow->qty;
						}
						else {
							$jum_retur_perbaikan = 0;
						}
						
						//$gradea_bgs = $jum_masuk_bgs-$jum_retur_bgs;
						$gradea_perbaikan = $jum_masuk_perbaikan-$jum_retur_perbaikan;
						
						//$persena_bgs = $gradea_bgs/$jum_masuk_bgs;
						$persena_perbaikan = ($gradea_perbaikan/$jum_masuk_perbaikan)*100;
						
						//$persen_retur_bgs = $jum_retur_bgs/$jum_masuk_bgs;
						$persen_retur_perbaikan = ($jum_retur_perbaikan/$jum_masuk_perbaikan)*100;
						
						$data_stok2[] = array(		'id'=> 0,
													'kode_unit_jahit'=> $row2->kode_unit_jahit,
													'nama_unit_jahit'=> $nama_unit_jahit,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'qty'=> $jum_masuk_perbaikan,
													'retur'=> $jum_retur_perbaikan,
													'gradea'=> $gradea_perbaikan,
													'persena'=> $persena_perbaikan,
													'persen_retur'=> $persen_retur_perbaikan
													);
					} // end foreach2
				}
				else
					$data_stok2 = '';
				
				$datautamaperbaikan[] = array(		'kode'=> $rowutama->kode,
													'nama'=> $rowutama->nama,
													'data_stok2'=> $data_stok2
													);
			} // end foreachutama
		} // end ifutama
		
		// 2. barang perbaikan								
				
					
		$hasil= array('datautamabagus'=>$datautamabagus,
					'datautamaperbaikan'=>$datautamaperbaikan);
				
		return $hasil;
				//-----------------------------------------------------------------
  }
  
  // 30-08-2014
  function cek_returqc($bulan, $tahun, $jenis) {
	$query3	= $this->db->query(" SELECT id FROM tm_retur_qc_wip
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND jenis='$jenis' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_retur_qc($bulan, $tahun, $jenis) {
		$query	= $this->db->query(" SELECT a.id, a.kode_unit, a.kode_brg_jadi, a.qty, b.e_product_motifname 
					FROM tm_retur_qc_wip a, tr_product_motif b
					WHERE a.kode_brg_jadi = b.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.jenis = '$jenis'
					ORDER BY a.kode_unit, a.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datastok = array();
			foreach ($hasil as $row) {
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
				
				$datastok[] = array( 	'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'kode_unit_jahit'=> $row->kode_unit,
										'nama_unit_jahit'=> $nama_unit_jahit,
										'retur'=> $row->qty
									);
			}
		}
		else {
			$datastok = '';
		}
		return $datastok;
  }
  
  // 23-09-2014
  function cek_forecast_unit($bulan, $tahun, $id_kel_unit) {
	
	$sql = " SELECT id FROM tm_forecast_wip_unit WHERE bulan = '$bulan' AND tahun = '$tahun'
			 AND id_kel_forecast_wip_unit = '$id_kel_unit' ";
	
	$query3	= $this->db->query($sql);
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_forecast_unit($bulan, $tahun, $id_kel_unit) {
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.id_kel_forecast_wip_unit = '$id_kel_unit'
					ORDER BY b.id ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function saveforecastunit($is_new, $id_fc, $iddetail, $kode_brg_jadi, $fc, $ket){ 
	  $tgl = date("Y-m-d H:i:s"); 
	 // 23-09-2014, adopsi dari forecast biasa
	 // 02-04-2014, ga pake is_new lagi. langsung save aja setelah delete detail sebelumnya
	//  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip_unit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailx	= $seqrow->id+1;
			}else{
				$iddetailx	= 1;
			}
		  
		   $data_detail = array(
						'id'=>$iddetailx,
						'id_forecast_wip_unit'=>$id_fc,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'qty'=>$fc,
						'keterangan'=>$ket
					);
		   $this->db->insert('tm_forecast_wip_unit_detail',$data_detail);
  }
  
  function get_forecastvsscheduleunit($bulan, $tahun, $id_kel_unit) {
	  
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// modif 17-09-2014
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// ambil list unit jahit di tabel kel_forecast_wip_unit_listunit
		$query3	= $this->db->query(" SELECT a.kode_unit, b.nama FROM tm_kel_forecast_wip_unit_listunit a 
							INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit 
							WHERE a.id_kel_forecast_wip_unit = '".$id_kel_unit."' ORDER BY a.kode_unit ");
							
		$kodeunit=""; $new_kodeunit="";
		if ($query3->num_rows() > 0){
			$hasil3 = $query3->result();
				
			foreach ($hasil3 as $row3) {
				$kodeunit.= "'".$row3->kode_unit."',";
			}
		}
		$jumstring = strlen($kodeunit);
		$new_kodeunit = substr($kodeunit, 0, $jumstring-1);
		
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.id_kel_forecast_wip_unit = '$id_kel_unit'
					ORDER BY d.kode, b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				// ambil SO di unit2 jahit yg dipilih di bulan sebelumnya
				$queryx	= $this->db->query(" SELECT sum(b.jum_bagus) as jumbagus FROM tt_stok_opname_unit_jahit a 
											INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't'
											AND a.kode_unit IN (".$new_kodeunit.") ");
						
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal_bgs = $hasilrow->jumbagus;
					if ($saldo_awal_bgs == '')
						$saldo_awal_bgs = 0;
				}
				else {
					$saldo_awal_bgs = 0;
				}
				
				// KOLOM SCHEDULE ambil data barang masuk ke unit jahit
				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.jenis_keluar = '1'
									AND a.kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						$jum_masuk = $masuk_bgs;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit <> '0' AND a.jenis_masuk = '1'
									AND a.kode_unit_jahit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						$jum_keluar = $keluar_bgs;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						// tambahan 27-08-2014, kolom schedule dimodif perhitungannya
						//kolom schedule = brg masuk - stock schedule bln trsbt + stock schedule 1 bln sblmnya
						
						// a. stok schedule bulan tsb
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bulan' AND tahun='$tahun' AND kode_brg_jadi = '$row->kode_brg_jadi'
									AND kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule = $hasilrow->qty;
							
							if ($qty_schedule == '')
								$qty_schedule = 0;
						}
						else
							$qty_schedule = 0;
						
						// b. stok schedule 1 bulan sebelumnya
						if ($bulan == 1) {
							$bln_query = 12;
							$thn_query = $tahun-1;
						}
						else {
							$bln_query = $bulan-1;
							$thn_query = $tahun;
							if ($bln_query < 10)
								$bln_query = "0".$bln_query;
						}
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bln_query' AND tahun='$thn_query' AND kode_brg_jadi = '$row->kode_brg_jadi'
									AND kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule_sebelumnya = $hasilrow->qty;
							
							if ($qty_schedule_sebelumnya == '')
								$qty_schedule_sebelumnya = 0;
						}
						else
							$qty_schedule_sebelumnya = 0;
						
						$totalqtyschedule = $jum_masuk-$qty_schedule+$qty_schedule_sebelumnya;
						//===========================================================
						if ($row->qty != 0) {
							//$persenschedule = ($jum_masuk/$row->qty)*100;
							$persenschedule = ($totalqtyschedule/$row->qty)*100;
							$persenforecast = ($jum_keluar/$row->qty)*100;
						}
						else {
						//echo $row->qty."<br>";
							$persenschedule = 0;
							$persenforecast = 0;
						}
						// 17-09-2014
						//echo $totalqtyschedule." ".$saldo_awal_bgs."<br>";
						$temptotal = $totalqtyschedule+$saldo_awal_bgs;
						if ($temptotal != 0)
							$persenforecast2 = ($jum_keluar/($totalqtyschedule+$saldo_awal_bgs))*100;
						else
							$persenforecast2 = 0;
						
				//17-09-2014, ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan,
										//'schedule'=> $jum_masuk,
										'schedule'=> $totalqtyschedule,
										'brgmasuk'=> $jum_keluar,
										'persenschedule'=> $persenschedule,
										//'persenforecast'=> $persenforecast,
										'persenforecast2'=> $persenforecast2,
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel,
										'saldo_awal_bgs'=> $saldo_awal_bgs
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  // 30-09-2014
  function get_kel_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_kel_forecast_wip_unit ORDER BY kode");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 20-10-2014
  function get_transaksiglobal($bulan, $tahun) {
	  
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 1. minggu 1: tgl 1 s/d 7
		
		//	1.1. produksi ke unit jahit
		//	1.2. unit jahit ke wip
		//	1.3. wip ke packing
		//	1.4. gudang jadi ke distributor
		  
		 //  the 'w' modifier, to get to number of the day (0 to 6, 0 being sunday, and 6 being saturday) :
		 $minggu = 1;
		for ($tgl=1; $tgl<=$lastDay; $tgl++) {
			if ($tgl<10)
				$tglx = "0".$tgl;
			else
				$tglx = $tgl;
			
			if ($tgl == 1)
				$awal = $tahun."-".$bulan."-".$tglx;
				
			$tglexist = $tahun."-".$bulan."-".$tglx;
			$timeexist = strtotime($tglexist);
			$hari = date("w", $timeexist);
			
			//echo $hari." ";
			if ($hari == '1') {
				$awal = $tahun."-".$bulan."-".$tglx;
			}
			
			if (($hari == '0' && $tgl != $lastDay) || ($hari == '0' && $tgl == $lastDay) || $tgl == $lastDay) {
				if ($hari == '0')
					$tglxx = $tgl-1;
				else
					$tglxx = $tgl;
				if ($tglxx < 10)
					$tglxx = "0".$tglxx;

				$akhir = $tahun."-".$bulan."-".$tglxx;
				// 1.1. produksi ke unit jahit
				$query = $this->db->query(" SELECT sum(b.qty) as produksi2unit FROM tm_bonmkeluarcutting a INNER JOIN 
											tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
											WHERE a.tgl_bonm >= '".$awal."' AND a.tgl_bonm <= '".$akhir."' ");
				if ($query->num_rows() > 0){
					$hasilrow = $query->row();
					$produksi2unit1 = $hasilrow->produksi2unit;
					if ($produksi2unit1 == '')
						$produksi2unit1 = 0;
				}
				else
					$produksi2unit1=0;
				
				// 1.2. unit jahit ke wip
				$query = $this->db->query(" SELECT sum(b.qty) as jahit2wip FROM tm_sjmasukwip a INNER JOIN 
											tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
											WHERE a.tgl_sj >= '".$awal."' AND a.tgl_sj <= '".$akhir."'
											AND a.kode_unit_jahit <> '0' ");
				if ($query->num_rows() > 0){
					$hasilrow = $query->row();
					$jahit2wip1 = $hasilrow->jahit2wip;
					if ($jahit2wip1 == '')
						$jahit2wip1 = 0;
				}
				else
					$jahit2wip1=0;
				
				// 1.3. wip ke packing	
				$query = $this->db->query(" SELECT sum(b.qty) as wip2packing FROM tm_sjkeluarwip a INNER JOIN 
											tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
											WHERE a.tgl_sj >= '".$awal."' AND a.tgl_sj <= '".$akhir."'
											AND a.kode_unit_packing <> '0' ");
				if ($query->num_rows() > 0){
					$hasilrow = $query->row();
					$wip2packing1 = $hasilrow->wip2packing;
					if ($wip2packing1 == '')
						$wip2packing1 = 0;
				}
				else
					$wip2packing1=0;
				
				// 1.4. gudang jadi ke distributor --> DO			
				$db2 = $this->load->database('db_external', TRUE);
				$sqlx = " SELECT sum(b.n_deliver) as gdjadi2dist FROM tm_do a INNER JOIN 
											tm_do_item b ON a.i_do = b.i_do
											WHERE a.d_do >= '".$awal."' AND a.d_do <= '".$akhir."'
											AND a.f_do_cancel='f' ";
				$queryx	= $db2->query($sqlx);
						
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$gdjadi2dist1 = $hasilx->gdjadi2dist;
					if ($gdjadi2dist1 == '')
						$gdjadi2dist1 = 0;
				}
				else
					$gdjadi2dist1 = 0;
				
				$datadetail[] = array( 		'produksi2unit1'=> $produksi2unit1,
									'jahit2wip1'=> $jahit2wip1,
									'wip2packing1'=> $wip2packing1,
									'gdjadi2dist1'=> $gdjadi2dist1,
									'minggu'=> $minggu,
									'awal'=> $awal,
									'akhir'=> $akhir
									);
				$minggu++;
			} // end if hari
				
		} // end for
		return $datadetail;
  } */
}
