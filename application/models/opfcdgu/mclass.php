<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function cari_op($nop,$icust) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_opfc WHERE i_op_code=trim('$nop') AND i_customer='$icust' AND f_op_cancel='f' ");
	}

	function cari_sop($nsop) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_opfc WHERE i_sop=trim('$nsop') ");
	}
	
	function getnomorop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),5,(LENGTH(cast(i_op_code AS character varying))-4)) AS iopcode FROM tm_opfc WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 " );
	}

	function getthnop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),1,4) AS thn FROM tm_opfc WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
	}

	function getnomorsop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),5,(LENGTH(cast(i_sop AS character varying))-4)) AS isop FROM tm_opfc ORDER BY i_op DESC LIMIT 1 ");
	}

	function getnomorop_inc_cabang($i_customer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),5,(LENGTH(cast(i_op_code AS character varying))-4)) AS iopcode FROM tm_opfc WHERE i_customer='$i_customer' AND f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 " );
	}
	
	function getthnop_inc_cabang($i_customer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),1,4) AS thn FROM tm_opfc WHERE i_customer='$i_customer' AND f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
	}
		
	function getthnsop() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),1,4) AS thisop FROM tm_opfc ORDER BY i_op DESC LIMIT 1 " );
	}
				
	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		
		$query	= $db2->get();
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lcabang() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		
		$db2->select(" a.i_branch AS codebranch, 
				    a.i_customer AS codecustomer, 
				    a.e_branch_name AS branch FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
#				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer_code ".$order." ",false);
#Dedy 2016-11-11
#		
		$query	= $db2->get();
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lcabangxx($i_customer) {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_branch_name ASC ";
		$db2->select(" * FROM tr_branch WHERE i_customer='$i_customer' ".$order." ",false);
		
		$query	= $db2->get();
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function getcabang($icusto) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_customer',$icusto);
		$db2->order_by('e_branch_name');
		
		return $db2->get('tr_branch');
	}
	
	function getproduct() {
		$db2=$this->load->database('db_external', TRUE);
		$db2->order_by('e_product_basename');
		
		return $db2->get('tr_product_base');
	}

	function listpquery($tabel,$order,$filter) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		
		if ($query->num_rows()>0) {
			return $result	= $query->result(); 
		}
	}

	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		/*
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND d.d_so = '2010-12%' ORDER BY b.i_product_motif DESC		
		*/
		
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false ORDER BY b.i_product_motif DESC ");
		*/
		
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' ORDER BY b.i_product_motif ASC ");		
	}

	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		//$tgl	= date("Y-m-d");
		/*
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0' AND d.d_so = '$tgl%' ORDER BY b.i_product_motif DESC LIMIT		
		*/
		
		/* Disabled 07-02-2011
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false 
				
				ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset, false);
		*/
		
		/*
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       b.n_quantity AS qty
					   
				FROM tr_product_motif b INNER JOIN tr_product_base a
				
					ON a.i_product_base=b.i_product WHERE b.i_product_motif='XX00100' ", false);
		*/			
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset, false);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$ky_upper' ) 
				
				ORDER BY b.i_product_motif DESC ");
		*/
		
		return $db2->query( "
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$ky_upper' ) 
				
				ORDER BY b.i_product_motif ASC ");		
	}

	function flbarangjadiperpages($txt,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		/* Disabled 07-02-2011
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false 
				
				ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset, false);
		*/
		
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product AS ip,		
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
								
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset, false);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}


	function msimpan($i_op,$i_customer,$i_branch,$d_op,$d_delivery_limit,$i_sop,$f_op_dropforcast,$v_count,$i_product,
					$e_product_name,$e_note,$iteration, $jenis_op) {	
		$db2=$this->load->database('db_external', TRUE);
		$i_op_item	= array();
		$tm_opfc_item	= array();
		$tm_sopfc_item= array();
		
		$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;	
				
		$q_cek_oder	= $db2->query(" SELECT * FROM tm_opfc WHERE i_op_code=trim('$i_op') AND i_customer='$i_customer' AND f_op_cancel='f' ");
		
		if($q_cek_oder->num_rows()>0) {
			$ada=1;
		}else{
			$ada=0;
		}

		$seq_tm_opfc	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_opfc ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
		
		if($seq_tm_opfc->num_rows() > 0 ) {
			$seqrow	= $seq_tm_opfc->row();
			$iop	= $seqrow->i_op+1;
		}else{
			$iop	= 1;
		}

		$seq_tm_sopfc	= $db2->query(" SELECT i_sop FROM tm_sopfc ORDER BY i_sop DESC LIMIT 1 ");
		
		if($seq_tm_sopfc->num_rows() > 0 ) {
			$seqrow2= $seq_tm_sopfc->row();
			$isop	= $seqrow2->i_sop+1;
		}else{
			$isop	= 1;
		}
				
		if($ada==0) {
			$tm_opfc	= array(
				 'i_op'=>$iop,
				 'i_op_code'=>$i_op,
				 'i_customer'=>$i_customer,
				 'i_branch'=>$i_branch,
				 'd_op'=>$d_op,
				 'd_delivery_limit'=>$d_delivery_limit,
				 'i_sop'=>$i_sop,
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry,
				 'f_op_dropforcast'=>$f_op_dropforcast,
				 'jenis_op'=>$jenis_op);

			$tm_sopfc	= array(
				 'i_sop'=>$isop,
				 'i_sop_code'=>$i_sop,
				 'i_customer'=>$i_customer,
				 'i_branch'=>$i_branch,
				 'd_sop'=>$d_op,
				 'i_op'=>$iop,
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry
			);
				 	
			if($db2->insert('tm_opfc',$tm_opfc)) {
				
				$db2->insert('tm_sopfc',$tm_sopfc);
				
				for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
					
					$seq_tm_opfc_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_opfc_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
					if($seq_tm_opfc_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_opfc_item->row();
						$i_op_item[$jumlah]	= $seqrow->i_op_item+1;
					}else{
						$i_op_item[$jumlah]	= 1;
					}

					$seq_tm_sopfc_item	= $db2->query(" SELECT i_sop_item FROM tm_sopfc_item ORDER BY i_sop_item DESC LIMIT 1 ");
					if($seq_tm_sopfc_item->num_rows() > 0 ) {
						$seqrow2	= $seq_tm_sopfc_item->row();
						$i_sop_item	= $seqrow2->i_sop_item+1;
					}else{
						$i_sop_item	= 1;
					}
						
					$tm_opfc_item[$jumlah]	= array(
									 'i_op_item'=>$i_op_item[$jumlah],
									 'i_op'=>$iop,
									 'i_product'=>$i_product[$jumlah],
									 'n_count'=>$v_count[$jumlah],
									 'e_product_name'=>$e_product_name[$jumlah],
									 'f_delivered'=>'FALSE',
									 'n_residual'=>$v_count[$jumlah],
									 'e_note'=>$e_note[$jumlah],
									 'd_entry'=>$dentry);

					$tm_sopfc_item[$jumlah]	= array(
									 'i_sop_item'=>$i_sop_item,
									 'i_sop'=>$isop,
									 'i_product'=>$i_product[$jumlah],
									 'e_product_name'=>$e_product_name[$jumlah],
									 'd_entry'=>$dentry);
					
					$db2->insert('tm_opfc_item',$tm_opfc_item[$jumlah]);
					$db2->insert('tm_sopfc_item',$tm_sopfc_item[$jumlah]);
				}
			}else{
			}

			if($db2->trans_status()===FALSE || $db2->trans_status()==false) {
				$db2->trans_rollback();
				$ok = 0;
			}else{
				$db2->trans_commit();
				$ok = 1;
			}
						
			if($ok==1) {
				/*** redirect('op/cform/'); ***/
				print "<script>alert(\"Nomor OP : '\"+$i_op+\"' telah disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}else{
				/*** redirect('op/cform/'); ***/
				print "<script>alert(\"Maaf, Order Pembelian (OP) gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
			}
			
		}else{
			print "<script>alert(\"Maaf, Nomor OP tsb sebelumnya telah diinput. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
		
	}
	
	// 02-06-2012
	function get_opfc_dgu(){
		
		//$konek = "host=192.168.0.100 user=duta dbname=dialogue port=5432 password=dutayey";
		//~ $konek = "host=192.168.0.107 user=dedy dbname=dialogue port=5432 password=dedyalamsyah";
		//~ $db = pg_connect($konek);
		/*
		 * skrip asli dari view
		 * CREATE OR REPLACE VIEW v_opfc_itemduta AS 
			 SELECT b.i_op, b.i_product, b.i_product_motif, b.i_product_grade, b.n_order, b.n_delivery, b.v_product_mill, 
			   b.e_product_name, b.n_item_no
			   FROM tm_op a, tm_opfc_item b
			  WHERE a.i_op = b.i_op AND a.i_supplier = 'SP030'::bpchar AND a.f_opfc_close = false;
		 * 
		 * */
		 $dbpel=$this->load->database('db_pelanggan',TRUE);
		 $db2=$this->load->database('db_external', TRUE);
		 $data_op = array(); $i=1;
		 $d_opyes=date("Y-m-d",strtotime("-5 days"));
		 
		//~ $query	= pg_query(" SELECT * FROM v_opfc_itemduta a, v_opfc_duta b where a.i_op = b.i_op and d_reff >= '2016-12-21'");
		$query	=$dbpel->query ("SELECT * FROM  v_opfc_itemduta a, v_opfc_duta b where a.i_op = b.i_op and d_op >= '".$d_opyes."' order by a.i_op");
		//~ if(pg_num_rows($query)>0) {
			if($query->num_rows() > 0) {
				$hasil=$query->result();
				foreach($hasil as $row){
				//~ //echo pg_num_rows($query); die();
				//~ while ($row = pg_fetch_array($query)) {
				//~ // ambil nama cabang berdasarkan i_area
				$qbranch	= $db2->query(" SELECT i_branch_code, e_initial FROM tr_branch
									WHERE i_customer = '1' AND i_code = '".$row->i_area."' ");
				if($qbranch->num_rows() > 0 ) {
					$rbranch	= $qbranch->row();
					$i_branch_code	= $rbranch->i_branch_code;
					$inisial	= $rbranch->e_initial;
				}
				//~ $pisah1 = explode("-", $row['i_op']);
				$pisah1 = explode("-", $row->i_op);
				$nomoreuy= trim($pisah1[2]);
				$nomoreuy2 = substr($nomoreuy, 1, 5);
				$tahun	= date("Y");
				$nomoreuy2 = $tahun.$nomoreuy2;
				// cek apakah 5 digit terakhir nomor OP DGU = 5 digit terakhir nomor OP duta, jika sama maka tidak ditampilkan
				$qcekop	= $db2->query(" SELECT i_op FROM tm_opfc where i_op_code = '$nomoreuy2' AND f_op_cancel = 'f' ");
				if($qcekop->num_rows() == 0 ) {
			
					$data_op[] = array(			'no'=> $i,
												'i_op'=> $row->i_op,	
												'd_op'=> $row->d_op,	
												'i_area'=> $row->i_area,	
												'nama_cabang'=> $inisial,	
												'i_branch_code'=> $i_branch_code,	
												'i_product'=> $row->i_product,	
												'i_product_motif'=> $row->i_product_motif,	
												'i_product_grade'=> $row->i_product_grade,	
												'n_order'=> $row->n_order,	
												'n_delivery'=> $row->n_delivery,
												'v_product_mill'=> $row->v_product_mill,
												'e_product_name'=> $row->e_product_name,
												'n_item_no'=> $row->n_item_no
												//~ 'i_op'=> $row['i_op'],	
												//~ 'd_op'=> $row['d_op'],	
												//~ 'i_area'=> $row['i_area'],	
												//~ 'nama_cabang'=> $inisial,	
												//~ 'i_branch_code'=> $i_branch_code,	
												//~ 'i_product'=> $row['i_product'],	
												//~ 'i_product_motif'=> $row['i_product_motif'],	
												//~ 'i_product_grade'=> $row['i_product_grade'],	
												//~ 'n_order'=> $row['n_order'],	
												//~ 'n_delivery'=> $row['n_delivery'],
												//~ 'v_product_mill'=> $row['v_product_mill'],
												//~ 'e_product_name'=> $row['e_product_name'],
												//~ 'n_item_no'=> $row['n_item_no']
												); 
					$i++;
				}
			
			//~ } // end while
		} // end if
	}
		//~ else
			//~ $data_op = '';
		return $data_op;	
	}
	
	function msimpanopdgu($no_op_dgu,$tgl_op,$i_product, $i_product_motif,$e_product_name, $qty, $fck,$iterasi, 
						$i_customer, $i_branch_code, $d_delivery_limit, $f_op_dropforcast, $jenis_op) {	
		
		// ====================================================
		$tahun	= date("Y");
			$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;	

		for($xx=1;$xx<=$iterasi-1;$xx++) {
		  if ($fck[$xx] == 'y') {
			// ambil nama brg dari tabel tr_product_motif
			$qbrgjadi	= $db2->query(" SELECT e_product_motifname FROM tr_product_motif
									WHERE i_product_motif = '".$i_product_motif[$xx]."' ");
			if($qbrgjadi->num_rows() > 0 ) {
				$rbrgjadi	= $qbrgjadi->row();
				$nama_brg_jadi_pbrk	= $rbrgjadi->e_product_motifname;
				$kode_brg_jadi = $i_product_motif[$xx];
			}
			else {
				$qbrgjadi	= $db2->query(" SELECT e_product_motifname FROM tr_product_motif
									WHERE i_product_motif = '".$i_product[$xx]."00' ");
				if($qbrgjadi->num_rows() > 0 ) {
					$rbrgjadi	= $qbrgjadi->row();
					$nama_brg_jadi_pbrk	= $rbrgjadi->e_product_motifname;
					$kode_brg_jadi = $i_product[$xx]."00";
				}
				else {
					$nama_brg_jadi_pbrk = "";
					$kode_brg_jadi = "";
				}
			}
			
			// cek nomor OP DGU apakah udh ada atau blm
			// 07-06-2012
			$pisah1 = explode("-", $no_op_dgu[$xx]);
			$nomoreuy= trim($pisah1[2]);
			$nomoreuy2 = substr($nomoreuy, 1, 5);
			$nomoreuy2 = $tahun.$nomoreuy2;
			
			/*$qopdgu	= $db2->query("SELECT i_op FROM tm_opfc WHERE f_transfer_op = 't' 
							AND no_op_dgu = '".$no_op_dgu[$xx]."' AND f_op_cancel = 'f' "); */
			$qopdgu	= $db2->query("SELECT i_op FROM tm_opfc WHERE  
							i_op_code = '".$nomoreuy2."' AND f_op_cancel = 'f' ");
			if($qopdgu->num_rows()== 0) {
				// ini jika header blm ada. save ke tm_opfc, tm_opfc_item, tm_sopfc, dan tm_sopfc_item
				// *) sequence i_op
				$seq_tm_opfc	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_opfc 
									ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
				if($seq_tm_opfc->num_rows() > 0 ) {
					$seqrow	= $seq_tm_opfc->row();
					$iop	= $seqrow->i_op+1;
				}else{
					$iop	= 1;
				}
				
				// *) sequence i_sop
				$seq_tm_sopfc	= $db2->query(" SELECT i_sop FROM tm_sopfc ORDER BY i_sop DESC LIMIT 1 ");
				if($seq_tm_sopfc->num_rows() > 0 ) {
					$seqrow2= $seq_tm_sopfc->row();
					$isop	= $seqrow2->i_sop+1;
				}else{
					$isop	= 1;
				}
				
				// *) generate nomor OP utk duta dan nomor SOP utk duta (update 07-06-2012: ga perlu generate, tapi ambil dari OP DGU)
				/*
				 * function getnomorop() {
						return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),5,(LENGTH(cast(i_op_code AS character varying))-4)) AS iopcode FROM tm_opfc WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 " );
					}
					function getthnop() {
						return $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),1,4) AS thn FROM tm_opfc WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
					}
					function getnomorsop() {
						return $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),5,(LENGTH(cast(i_sop AS character varying))-4)) AS isop FROM tm_opfc ORDER BY i_op DESC LIMIT 1 ");
					}
					function getthnsop() {
						return $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),1,4) AS thisop FROM tm_opfc ORDER BY i_op DESC LIMIT 1 " );
					}
				 * 
				 * */
				 
				 // ga dipake lagi om
				/*$qryth	= $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),1,4) AS thn FROM tm_opfc WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
				$qryop	= $db2->query(" SELECT SUBSTRING(cast(i_op_code AS character varying),5,(LENGTH(cast(i_op_code AS character varying))-4)) AS iopcode FROM tm_opfc WHERE f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 " );
				
				if($qryth->num_rows() > 0) {
					$th		= $qryth->row_array();
					$thn	= $th['thn'];
				} else {
					$thn	= $tahun;
				}
				
				if($thn==$tahun) {
					if($qryop->num_rows() > 0)  {
						$row	= $qryop->row_array();
						$op		= $row['iopcode']+1;		
						switch(strlen($op)) {
							case "1": $nomorop	= "0000".$op;
							break;
							case "2": $nomorop	= "000".$op;
							break;	
							case "3": $nomorop	= "00".$op;
							break;
							case "4": $nomorop	= "0".$op;
							break;
							case "5": $nomorop	= $op;
							break;	
						}
					} else {
						$nomorop		= "00001";
					}
					$nomor	= $tahun.$nomorop;
				} else {
					$nomor	= $tahun."00001";
				} */
				
				$qrytsop = $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),1,4) AS thisop FROM tm_opfc ORDER BY i_op DESC LIMIT 1 " );
				$qrysop	 = $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),5,(LENGTH(cast(i_sop AS character varying))-4)) AS isop FROM tm_opfc ORDER BY i_op DESC LIMIT 1 ");
				if($qrytsop->num_rows() > 0) {
					$th		= $qrytsop->row_array();
					$thn2	= $th['thisop'];
				} else {
					$thn2	= $tahun;
				}
					
					if($thn2==$tahun) {
						if($qrysop->num_rows() > 0)  {
							$row2	= $qrysop->row_array();
							$sop		= $row2['isop']+1;		
							switch(strlen($sop)) {
								case "1": $nomorsop	= "0000".$sop;
								break;
								case "2": $nomorsop	= "000".$sop;
								break;	
								case "3": $nomorsop	= "00".$sop;
								break;
								case "4": $nomorsop	= "0".$sop;
								break;
								case "5": $nomorsop	= $sop;
								break;	
							}
						} else {
							$nomorsop		= "00001";
						}
						$nomor2	= $tahun.$nomorsop;
					} else {
						$nomor2	= $tahun."00001";
					}
					//echo $nomor." ".$nomor2; die(); good
				//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end generate nomor OP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				
				// *) insert ke tm_opfc dan tm_sopfc
				$tm_opfc	= array(
					 'i_op'=>$iop,
					 'i_op_code'=>$nomoreuy2,
					 'i_customer'=>$i_customer,
					 'i_branch'=>$i_branch_code[$xx],
					 'd_op'=>$tgl_op[$xx],
					 'd_delivery_limit'=>$d_delivery_limit,
					 'i_sop'=>$nomor2,
					 'd_entry'=>$dentry,
					 'd_update'=>$dentry,
					 'f_op_dropforcast'=>$f_op_dropforcast,
					 'f_transfer_op'=>'t',
					 'no_op_dgu'=>$no_op_dgu[$xx],
					 'jenis_op'=>$jenis_op);
				$db2->insert('tm_opfc',$tm_opfc);

				$tm_sopfc	= array(
					 'i_sop'=>$isop,
					 'i_sop_code'=>$nomor2,
					 'i_customer'=>$i_customer,
					 'i_branch'=>$i_branch_code[$xx],
					 'd_sop'=>$tgl_op[$xx],
					 'i_op'=>$iop,
					 'd_entry'=>$dentry,
					 'd_update'=>$dentry,
					 'f_transfer_op'=>'t');
				$db2->insert('tm_sopfc',$tm_sopfc);
				
				// tabel2 item
				$seq_tm_opfc_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_opfc_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_opfc_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_opfc_item->row();
					$i_op_item = $seqrow->i_op_item+1;
				}else{
					$i_op_item	= 1;
				}

				$seq_tm_sopfc_item	= $db2->query(" SELECT i_sop_item FROM tm_sopfc_item ORDER BY i_sop_item DESC LIMIT 1 ");
				if($seq_tm_sopfc_item->num_rows() > 0 ) {
					$seqrow2	= $seq_tm_sopfc_item->row();
					$i_sop_item	= $seqrow2->i_sop_item+1;
				}else{
					$i_sop_item	= 1;
				}
				
				$tm_opfc_item	= array(
									 'i_op_item'=>$i_op_item,
									 'i_op'=>$iop,
									// 'i_product'=>$i_product_motif[$xx],
									 'i_product'=>$kode_brg_jadi,
									 'n_count'=>$qty[$xx],
									 'e_product_name'=>$nama_brg_jadi_pbrk,
									 'f_delivered'=>'f',
									 'n_residual'=>$qty[$xx],
									 'e_note'=>'',
									 'd_entry'=>$dentry);

				$tm_sopfc_item	= array(
									 'i_sop_item'=>$i_sop_item,
									 'i_sop'=>$isop,
									 //'i_product'=>$i_product_motif[$xx],
									 'i_product'=>$kode_brg_jadi,
									 'e_product_name'=>$nama_brg_jadi_pbrk,
									 'd_entry'=>$dentry);
					
				$db2->insert('tm_opfc_item',$tm_opfc_item);
				$db2->insert('tm_sopfc_item',$tm_sopfc_item);
			}
			else {
				// save tabel item aja
				// *) ambil id dari tabel tm_opfc dan tm_sopfc
				// *) sequence i_op
				
				$x=$qopdgu->row();
				$iop	= $x->i_op;
###### ditutup sikasep 11-07-2017
#				$seq_tm_opfc	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_opfc 
#									ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
#				if($seq_tm_opfc->num_rows() > 0 ) {
#					$seqrow	= $seq_tm_opfc->row();
#					$iop	= $seqrow->i_op;
#				}else{
#					$iop	= 1;
#				}
				
				// *) sequence i_sop
#				$seq_tm_sopfc	= $db2->query(" SELECT i_sop FROM tm_sopfc ORDER BY i_sop DESC LIMIT 1 ");
#				if($seq_tm_sopfc->num_rows() > 0 ) {
#					$seqrow2= $seq_tm_sopfc->row();
#					$isop	= $seqrow2->i_sop;
#				}else{
#					$isop	= 1;
#				}
######				
				// *) generate i_op_item dan i_sop_item
				$seq_tm_opfc_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_opfc_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_opfc_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_opfc_item->row();
					$i_op_item = $seqrow->i_op_item+1;
				}else{
					$i_op_item	= 1;
				}

				$seq_tm_sopfc_item	= $db2->query(" SELECT i_sop_item FROM tm_sopfc_item ORDER BY i_sop_item DESC LIMIT 1 ");
				if($seq_tm_sopfc_item->num_rows() > 0 ) {
					$seqrow2	= $seq_tm_sopfc_item->row();
					$i_sop_item	= $seqrow2->i_sop_item+1;
				}else{
					$i_sop_item	= 1;
				}
				
				$tm_opfc_item	= array(
									 'i_op_item'=>$i_op_item,
									 'i_op'=>$iop,
									 //'i_product'=>$i_product_motif[$xx],
									 'i_product'=>$kode_brg_jadi,
									 'n_count'=>$qty[$xx],
									 'e_product_name'=>$nama_brg_jadi_pbrk,
									 'f_delivered'=>'f',
									 'n_residual'=>$qty[$xx],
									 'e_note'=>'',
									 'd_entry'=>$dentry);

				$tm_sopfc_item	= array(
									 'i_sop_item'=>$i_sop_item,
									 'i_sop'=>$isop,
									// 'i_product'=>$i_product_motif[$xx],
									 'i_product'=>$kode_brg_jadi,
									 'e_product_name'=>$nama_brg_jadi_pbrk,
									 'd_entry'=>$dentry);
					
				$db2->insert('tm_opfc_item',$tm_opfc_item);
				$db2->insert('tm_sopfc_item',$tm_sopfc_item);
			}
		  } // end seleksi dari checkbox fck
		} // end for
		
		if($db2->trans_status()===FALSE || $db2->trans_status()==false) {
			$db2->trans_rollback();
			$ok = 0;
		}else{
			$db2->trans_commit();
			$ok = 1;
		}
		if($ok==1) {
				/*** redirect('op/cform/'); ***/
				print "<script>alert(\"Transfer OP DGU berhasil disimpan. Terimakasih.\");window.open(\"opdgu\", \"_self\");</script>";
		}
		else{
				/*** redirect('op/cform/'); ***/
				print "<script>alert(\"Maaf, transfer Order Pembelian (OP) DGU gagal disimpan. Terimakasih.\");window.open(\"opdgu\", \"_self\");</script>";
		}
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& END MY SCRIPT 06-06-12 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	}
}
?>
