<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	/* Disabled 09012011
	function lbarangjadi() {
		$query	= $this->db->query( "
			SELECT c.i_bbk,
				c.i_product AS iproduct,
				a.e_product_basename AS productname

			FROM tm_bbk_item c 
			
			RIGHT JOIN tm_bbk d ON c.i_bbk=d.i_bbk 
			INNER JOIN tr_product_motif b ON b.i_product_motif=c.i_product
			INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
			
			WHERE d.i_status_bbk='2' " );
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	*/
	
	/* Disabled 19-02-2011
	// Awalnya dari tabel tr_product_motif & tr_product_base
	// jd mengacu ke tabel SJ (dengan input barang manual)
	*/
	/*
	function lbarangjadiperpages($limit,$offset) {
		
		$query	= $this->db->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
						
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi() {
	
		return $this->db->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}

	function flbarangjadi($key) {
	
		return $this->db->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				
				WHERE a.i_faktur_code='$key' 
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}
	*/

	function jmlFaktur($ifakturcode){
		return	$this->db->query(" SELECT * FROM tm_faktur_item a INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur WHERE b.i_faktur_code='$ifakturcode' AND b.f_faktur_cancel='f' ");
	}
	
	function lbarangjadiperpages($limit,$offset) {
		
		$query	= $this->db->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
						
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_faktur_cancel='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi() {
	
		return $this->db->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
						
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_faktur_cancel='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}

	function flbarangjadi($key) {
	
		return $this->db->query( "
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE a.i_faktur_code='$key' AND d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_faktur_cancel='f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC " );
	}

	function lsjpperpages($ibranch,$limit,$offset) {
		
		$query	= $this->db->query(" SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode, 
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit) AS nilai
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 a.v_unit_price, b.i_sj_code
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	LIMIT ".$limit." OFFSET ".$offset);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}	
	}

	function lsjp($ibranch) {
		
		/*** 10042012
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND c.e_initial='$ibranch' AND a.n_unit_akhir > 0
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 a.v_unit_price, b.i_sj_code
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";
		***/

		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit) AS nilai
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 a.v_unit_price, b.i_sj_code
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";
						
		return $this->db->query($qstr);
	}

	function flsjp($ibranch,$key) {
		$ky_upper	= $key;
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit) AS nilai
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (b.i_sj_code='$ky_upper' OR a.i_product='$ky_upper') AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj,
					 b.i_customer,
					 b.i_branch,
					 a.i_product,
					 a.e_product_name,
					 a.v_product_price,
					 a.n_unit,
					 a.n_unit_akhir,
					 a.v_unit_price, b.i_sj_code
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";

		return $this->db->query($qstr);
	}

	function lklsbrg() {
		$query	= $this->db->query( " SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC " );
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
	
	/* Disabled 09012011 
	function clistpenjualanndo($iproduct,$ecategory,$fstopproduksi) {
	*/
	function clistpenjualanndo($nofaktur) {
		
		/* Disabled 09012011
		$query	= $this->db->query( "
			SELECT 	a.i_bbk AS ibbk,
				c.i_product_motif AS imotif,
				c.e_product_motifname AS motifname,
				a.n_unit AS qty,
				a.v_unit_price AS unitprice,
				(a.n_unit * a.v_unit_price) AS amount
				
			FROM tm_bbk_item a
			
			RIGHT JOIN tm_bbk b ON b.i_bbk=a.i_bbk
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product

			WHERE b.i_status_bbk='2' AND ( d.i_product_base='$iproduct' OR c.i_product='$iproduct' ) ");
		*/
		$testajaom= "SELECT b.i_faktur, b.i_faktur_code, b.f_kontrabon, c.i_product AS imotif,
					c.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_item a
				
				INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON TRIM(c.i_product)=TRIM(a.i_product)
				INNER JOIN tm_sj d ON TRIM(d.i_sj)=TRIM(c.i_sj)

				WHERE b.i_faktur_code=TRIM('$nofaktur') AND d.f_sj_cancel='f' AND d.f_faktur_created='t' 
				
				GROUP BY b.i_faktur, b.i_faktur_code, b.f_kontrabon, c.i_product, c.e_product_name, a.n_quantity, a.v_unit_price
				
				ORDER BY c.i_product DESC, c.e_product_name ASC";
		$qstr	= " SELECT b.i_faktur, b.i_faktur_code, b.f_kontrabon, c.i_product AS imotif,
					c.e_product_name AS motifname,
					a.n_quantity AS qty,
					a.v_unit_price AS unitprice,
					(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_item a
				
				INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj_item c ON TRIM(c.i_product)=TRIM(a.i_product)
				INNER JOIN tm_sj d ON TRIM(d.i_sj)=TRIM(c.i_sj)

				WHERE b.i_faktur_code=TRIM('$nofaktur') AND d.f_sj_cancel='f' AND d.f_faktur_created='t' 
				
				GROUP BY b.i_faktur, b.i_faktur_code, b.f_kontrabon, c.i_product, c.e_product_name, a.n_quantity, a.v_unit_price
				
				ORDER BY c.i_product DESC, c.e_product_name ASC ";
		
		$query	= $this->db->query($qstr);
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
		
	}

	function getfakheader($ifakturcode) {
		return $this->db->query(" SELECT * FROM tm_faktur WHERE cast(i_faktur_code AS integer)='$ifakturcode' ");
	}
	
	function geticustomer($ibranch) {
		return $this->db->query(" SELECT * FROM tr_branch WHERE e_initial='$ibranch' ORDER BY i_branch DESC ");
	}
	
	function lfakturitem($ifaktur) {
		$query	= $this->db->query(" SELECT i_faktur_item, i_faktur, i_sj, i_product, e_product_name, n_quantity, v_unit_price, (n_quantity*v_unit_price) AS amount FROM tm_faktur_item WHERE i_faktur='$ifaktur' ");
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
	
	function lcabang($icustomer) {
		
		if(!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.i_branch AS codebranch,
					a.i_branch_code AS ibranchcode,
				    a.i_customer AS codecustomer,
					a.e_branch_name AS ebranchname,
				    a.e_branch_name AS branch,
					a.e_initial AS einitial
					
					FROM tr_branch a 
					
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$filter." ".$order;
					
		$query	= $this->db->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}	

	function cari_fpenjualan($fpenj) {
		return $this->db->query(" SELECT * FROM tm_faktur WHERE i_faktur_code=trim('$fpenj') ");
	}

	function mupdate($i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$isjcode,$ifakturcode,$ifakturX) {
		
		$i_faktur_item	= array();
		$tm_faktur_item	= array();
		$qty_akhir	= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$this->db->trans_begin();

		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fcetak	= $f_cetak=='1'?'TRUE':'FALSE';
		
		$qfakturitem	= $this->db->query(" SELECT * FROM tm_faktur_item WHERE i_faktur='$ifakturX' ");
		
		if($qfakturitem->num_rows()>0){
			
			foreach($qfakturitem->result() as $row) {
				
				$qdoitem	= $this->db->query(" SELECT * FROM tm_sj_item WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' AND f_faktur_created='t' ");
				
				if($qdoitem->num_rows()>0) {
					
					$rdoitem	= $qdoitem->row();
					
					$ndeliver	= $rdoitem->n_unit;
					$nresidual	= (($rdoitem->n_unit_akhir)+($row->n_quantity));
					
					$qupdtdoitem	= array(
						'n_unit_akhir'=>$nresidual
					);
					
					$this->db->query(" UPDATE tm_sj_item SET n_unit_akhir='$nresidual' WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' ");
				}
			}
		}
		
		$this->db->delete('tm_faktur_item', array('i_faktur' => $ifakturX)); 							

		$tmfaktur = array(
			'i_faktur_code' => $i_faktur,
			'd_faktur' => $nw_d_faktur,
			'e_branch_name' => $i_branch,
			'd_due_date' => $nw_d_due_date,
			'i_faktur_pajak' => $i_faktur_pajak,
			'd_pajak' => $nw_d_pajak,
			'n_discount' => $n_discount,
			'v_discount' => $nw_v_discount,
			'v_total_faktur' => $nw_v_total_faktur,
			'v_total_fppn' => $nw_v_total_fppn,
			'f_printed' => $fcetak,
			'e_note_faktur' => $e_note_faktur,
			'd_update' => $dentry
		);
		
		$this->db->where('i_faktur', $ifakturX);
		$this->db->update('tm_faktur', $tmfaktur); 
		
		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
			
			$seq_tm_i_faktur_item	= $this->db->query(" SELECT cast(i_faktur_item AS integer) AS i_faktur_item FROM tm_faktur_item ORDER BY cast(i_faktur_item AS integer) DESC LIMIT 1 ");
			
			if($seq_tm_i_faktur_item->num_rows()>0) {
				$seqrow	= $seq_tm_i_faktur_item->row();
				$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
			}else{
				$i_faktur_item[$jumlah]	= 1;
			}
				
			$tm_faktur_item[$jumlah]	= array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$ifakturX,
					 'i_sj'=>$isjcode[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_hjp[$jumlah],
					 'd_entry'=>$dentry );
			
			$q_qty_sj_item	= $this->db->query(" SELECT * FROM tm_sj_item WHERE i_sj='$isjcode[$jumlah]' AND i_product='$i_product[$jumlah]' ");
			
			if($q_qty_sj_item->num_rows()>0) {
				
				$row_item_br	= $q_qty_sj_item->row();
				$jml_item_br[$jumlah]	= $row_item_br->n_unit_akhir;
				
				if($n_quantity[$jumlah]==($row_item_br->n_unit_akhir)) {
					
					$qty_akhir[$jumlah]	= (($row_item_br->n_unit_akhir)-$n_quantity[$jumlah]);
					
					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created'=>'TRUE'
					);
		
					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir'=>$qty_akhir[$jumlah],
						'f_faktur_created'=>'TRUE'
					);
				} else if($n_quantity[$jumlah] < ($row_item_br->n_unit_akhir)) {
					$qty_akhir[$jumlah]	= (($row_item_br->n_unit_akhir) - $n_quantity[$jumlah]);
					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created'=>'TRUE'
					);
		
					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir'=>$qty_akhir[$jumlah],
						'f_faktur_created'=>'FALSE'
					);						
				}else{
					$qty_akhir[$jumlah]	= (($row_item_br->n_unit_akhir)-$n_quantity[$jumlah]);
					
					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created'=>'TRUE'
					);
		
					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir'=>$qty_akhir[$jumlah],
						'f_faktur_created'=>'TRUE'
					);
				}						
			}
								
			$this->db->update('tm_sj',$arrfakturupdate[$jumlah],array('i_sj'=>$isjcode[$jumlah],'f_faktur_created'=>'f'));
			$this->db->update('tm_sj_item',$arrfakturupdate2[$jumlah],array('i_sj'=>$isjcode[$jumlah],'i_product'=>$i_product[$jumlah]));
			$this->db->insert('tm_faktur_item',$tm_faktur_item[$jumlah]);

		}
		
		if($this->db->trans_status()===FALSE || $this->db->trans_status()==false) {
			$ok	= 0;
			$this->db->trans_rollback();
		}else{
			$ok	= 1;
			$this->db->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Nomor Faktur : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");show(\"listpenjualanndo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"listpenjualanndo/cform\",\"#content\");</script>";
		}
	}

	function mupdate_old($i_faktur,$nw_d_faktur,$i_branch,$e_note_faktur,$v_total_nilai,$n_discount,$nw_v_discount,$nw_d_due_date,$nw_v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$nw_v_total_fppn,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration,$isjcode,$ifakturcode,$ifakturX) {
		
		$i_faktur_item	= array();
		$tm_faktur_item	= array();
		
		$jml_item_br	= array();
		$qty_akhir	= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$this->db->trans_begin();

		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	
				
		$fcetak	= $f_cetak=='1'?'TRUE':'FALSE';

		$qfakturitem	= $this->db->query(" SELECT * FROM tm_faktur_item WHERE i_faktur='$ifakturX' ");
		if($qfakturitem->num_rows()>0){
			foreach($qfakturitem->result() as $row) {
				$qdoitem	= $this->db->query(" SELECT * FROM tm_sj_item WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' AND f_faktur_created='t' ");
				if($qdoitem->num_rows()>0){
					$rdoitem	= $qdoitem->row();
					$ndeliver	= $rdoitem->n_unit;
					$nresidual	= (($rdoitem->n_unit_akhir)+($row->n_quantity));
					
					$qupdtdoitem	= array(
						'n_unit_akhir'=>$nresidual
					);
					$this->db->query(" UPDATE tm_sj_item SET n_unit_akhir='$nresidual' WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' ");
				}
			}
		}
		
		//$this->db->delete('tm_faktur', array('i_faktur' => $ifakturX)); 
		$this->db->delete('tm_faktur_item', array('i_faktur' => $ifakturX)); 
		
		/***
		$seq_tm_faktur	= $this->db->query(" SELECT cast(i_faktur AS integer) AS i_faktur FROM tm_faktur ORDER BY cast(i_faktur AS integer) DESC LIMIT 1 ");
		
		if($seq_tm_faktur->num_rows() >0 ) {
			$seqrow	= $seq_tm_faktur->row();
			$ifaktur	= $seqrow->i_faktur+1;
		} else {
			$ifaktur	= 1;
		}
		***/
		/***
		$this->db->set(
			array(
			 'i_faktur'=>$ifaktur,
			 'i_faktur_code'=>$i_faktur,
			 'd_faktur'=>$nw_d_faktur,
			 'e_branch_name'=>$i_branch,
			 'd_due_date'=>$nw_d_due_date,
			 'i_faktur_pajak'=>$i_faktur_pajak,
			 'd_pajak'=>$nw_d_pajak,
			 'n_discount'=>$n_discount,
			 'v_discount'=>$nw_v_discount,
			 'v_total_faktur'=>$nw_v_total_faktur,
			 'v_total_fppn'=>$nw_v_total_fppn,
			 'f_printed'=>'FALSE',
			 'f_do_or_nota'=>'FALSE',
			 'e_note_faktur'=>$e_note_faktur,
			 'd_entry'=>$dentry ));
		***/
		/***
		$sq_updt_faktur	= " UPDATE tm_faktur SET i_faktur_code='$i_faktur', d_faktur='$nw_d_faktur', 
											e_branch_name='$i_branch', 
											d_due_date='$nw_d_due_date', 
											i_faktur_pajak='$i_faktur_pajak',
											d_pajak='$nw_d_pajak', 
											n_discount='$n_discount', 
											v_discount='$nw_v_discount',
											v_total_faktur='$nw_v_total_faktur', 
											v_total_fppn='$nw_v_total_fppn', e_note_faktur='$e_note_faktur' WHERE i_faktur='$ifakturX' ";
		print $sq_updt_faktur;
		***/							

		$tmfaktur = array(
			'i_faktur_code' => $i_faktur,
			'd_faktur' => $nw_d_faktur,
			'e_branch_name' => $i_branch,
			'd_due_date' => $nw_d_due_date,
			'i_faktur_pajak' => $i_faktur_pajak,
			'd_pajak' => $nw_d_pajak,
			'n_discount' => $n_discount,
			'v_discount' => $nw_v_discount,
			'v_total_faktur' => $nw_v_total_faktur,
			'v_total_fppn' => $nw_v_total_fppn,
			'f_printed' => $fcetak,
			'e_note_faktur' => $e_note_faktur,
			'd_update' => $dentry
		);
		
		$this->db->where('i_faktur', $ifakturX);
		$this->db->update('tm_faktur', $tmfaktur); 
		
		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
			$seq_tm_i_faktur_item	= $this->db->query(" SELECT cast(i_faktur_item AS integer) AS i_faktur_item FROM tm_faktur_item ORDER BY cast(i_faktur_item AS integer) DESC LIMIT 1 ");
				
			if($seq_tm_i_faktur_item->num_rows() > 0 ) {
				$seqrow	= $seq_tm_i_faktur_item->row();
				$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item+1;
			} else {
				$i_faktur_item[$jumlah]	= 1;
			}
				
			$tm_faktur_item[$jumlah]	= array(
					 'i_faktur_item'=>$i_faktur_item[$jumlah],
					 'i_faktur'=>$ifakturX,
					 'i_sj'=>$isjcode[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_quantity'=>$n_quantity[$jumlah],
					 'v_unit_price'=>$v_hjp[$jumlah],
					 'd_entry'=>$dentry );
			
			$q_qty_sj_item	= $this->db->query(" SELECT * FROM tm_sj_item WHERE i_sj='$isjcode[$jumlah]' AND i_product='$i_product[$jumlah]' ");
			if($q_qty_sj_item->num_rows()>0){
				$row_item_br	= $q_qty_sj_item->row();
				$jml_item_br[$jumlah]	= $row_item_br->n_unit_akhir;
				
				if($n_quantity[$jumlah] == $jml_item_br[$jumlah]){
					$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
					//$qty_akhir[$jumlah]	= 0;
					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created'=>'TRUE'
					);
		
					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir'=>$qty_akhir[$jumlah],
						'f_faktur_created'=>'TRUE'
					);
				} else if($n_quantity[$jumlah] < $jml_item_br[$jumlah]) { // jika jmlitwm kurang dari qty brg yg ada
					$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created'=>'TRUE'
					);
		
					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir'=>$qty_akhir[$jumlah],
						'f_faktur_created'=>'FALSE'
					);						
				}else{
					$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
					//$qty_akhir[$jumlah]	= 0;
					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created'=>'TRUE'
					);
		
					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir'=>$qty_akhir[$jumlah],
						'f_faktur_created'=>'TRUE'
					);
				}						
			}
								
			$this->db->update('tm_sj',$arrfakturupdate[$jumlah],array('i_sj'=>$isjcode[$jumlah],'f_faktur_created'=>'f'));
			$this->db->update('tm_sj_item',$arrfakturupdate2[$jumlah],array('i_sj'=>$isjcode[$jumlah],'i_product'=>$i_product[$jumlah]));
			$this->db->insert('tm_faktur_item',$tm_faktur_item[$jumlah]);

		} // End 0f for
		
		if ($this->db->trans_status()===FALSE) {
			$ok	= 0;
			$this->db->trans_rollback();
		} else {
			$ok	= 1;
			$this->db->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Nomor Faktur : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");show(\"listpenjualanndo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"listpenjualanndo/cform\",\"#content\");</script>";
		}
	}


	function mbatal($i_faktur,$i_faktur_code) {
		
		$this->db->trans_begin();
		
		$qfakturitem	= $this->db->query(" SELECT * FROM tm_faktur_item WHERE i_faktur='$i_faktur' ");
		$numfakturitem	= $qfakturitem->num_rows();
		
		if($numfakturitem>0) {
			
			foreach($qfakturitem->result() as $row) {
				
				$qdoitem	= $this->db->query(" SELECT * FROM tm_sj_item WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
				
				if($qdoitem->num_rows()>0) {
					
					$rdoitem	= $qdoitem->row();
					
					$nresidual	= (($rdoitem->n_unit_akhir)+($row->n_quantity));

					$this->db->query(" UPDATE tm_sj_item SET n_unit_akhir='$nresidual' WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");
					
					$this->db->query(" UPDATE tm_sj SET f_faktur_created='f' WHERE i_sj='$row->i_sj' AND f_faktur_created='t' ");
					
				}
				
			}
			
			$this->db->query(" UPDATE tm_faktur SET f_faktur_cancel='t' WHERE i_faktur='$i_faktur' AND f_faktur_cancel='f' ");	
		}

		if ($this->db->trans_status()===FALSE || $this->db->trans_status()==false) {
			$ok	= 0;
			$this->db->trans_rollback();
		}else{
			$ok	= 1;
			$this->db->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Nomor Faktur Penjualan : '\"+$i_faktur_code+\"' telah dibatalkan, terimakasih.\");show(\"listpenjualanndo/cform\",\"#content\");</script>";
		}else{
			print "<script>alert(\"Maaf, Data Faktur Penjuaan tdk ditemukan.\");show(\"listpenjualanndo/cform\",\"#content\");</script>";
		}
		
	}	
}
?>
