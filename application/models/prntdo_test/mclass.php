<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function clistdobrg($kddo) {
		 $db2 = $this->load->database('db_external', TRUE);
		/* 26072011
		$qrystr	= "
				SELECT  a.d_do AS dop,
					a.i_do_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_op AS iop,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					sum(b.n_deliver) AS qty,
					sum(b.n_residual) AS belumfaktur,
					b.e_note AS keterangan,
					e.i_op_code AS iopcode
					
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
				INNER JOIN tm_op e ON e.i_op_code=trim(cast(b.i_op AS character varying))

				WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f'

				GROUP BY a.d_do, a.i_do_code, a.i_branch, b.i_op, b.i_product, c.e_product_motifname, b.e_note, e.i_op_code ORDER BY b.i_product ASC ";
		*/
		
		// 08-09-2014, tambahin field i_do_item
		$qrystr	= " SELECT  a.d_do AS dop,
					a.i_do_code AS iopcode,
					a.i_branch AS ibranch,
					e.i_op_code AS iop,
					b.i_do_item,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					sum(b.n_deliver) AS qty,
					sum(b.n_residual) AS belumfaktur,
					b.e_note AS keterangan,
					e.i_op_code AS iopcode
				
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
				INNER JOIN tm_op e ON e.i_op=b.i_op
				
				WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f'

				GROUP BY a.d_do, a.i_do_code, a.i_branch, b.i_op, b.i_do_item, b.i_product, c.e_product_motifname, b.e_note, e.i_op_code 
				
				ORDER BY b.i_product ASC ";
						
		$query	= $db2->query($qrystr);

		if($query->num_rows() > 0 )	{
			return $result	= $query->result();
		}	
	}
	
	function cnmrop($kddo) {
		 $db2 = $this->load->database('db_external', TRUE);
		/* 26072011
		return $db2->query(" SELECT b.i_op AS iop,  e.i_op_code AS iopcode FROM tm_do_item b 
				
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op_code=trim(cast(b.i_op AS character varying))
			
			WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' ");
		*/ 

		return $db2->query(" SELECT b.i_op AS iop,  e.i_op_code AS iopcode FROM tm_do_item b 
				
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
			
			WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' ");
					
	}
	
	function ctgldobrg($kddo) {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query( " SELECT a.d_do AS tgl
		
			FROM tm_do_item b
				
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' " );
	}
	
	/* AND d.f_stop_produksi=false */
	function lbarangjadiperpages($limit,$offset) {
		 $db2 = $this->load->database('db_external', TRUE);
		/* 26072011
		$query	= $db2->query("
			SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code 
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op_code=trim(cast(b.i_op AS character varying))
					
			WHERE c.n_active='1' AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code

			ORDER BY a.d_do DESC, a.i_do_code DESC LIMIT ".$limit." OFFSET ".$offset);
		*/

		$query	= $db2->query(" SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code 
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
					
			WHERE c.n_active='1' AND a.f_do_cancel='f' /* AND a.f_printed ='f' */ 

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code

			ORDER BY a.d_do DESC, a.i_do_code DESC LIMIT ".$limit." OFFSET ".$offset);
								
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	
	/*  AND d.f_stop_produksi=false */
	function lbarangjadi() {
		 $db2 = $this->load->database('db_external', TRUE);
		/* 26072011
		return $db2->query("
			SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code 
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op_code=trim(cast(b.i_op AS character varying))
					
			WHERE c.n_active='1' AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code

			ORDER BY a.d_do DESC, a.i_do_code DESC ");
			*/ 

		return $db2->query(" SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code 
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
			
			WHERE c.n_active='1' AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code

			ORDER BY a.d_do DESC, a.i_do_code DESC ");
						
	}
	
	/* AND d.f_stop_produksi=false */
	function flbarangjadi($key) {
		 $db2 = $this->load->database('db_external', TRUE);
		/* 26072011
		return $db2->query("
			SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code  
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op_code=trim(cast(b.i_op AS character varying))
					
			WHERE c.n_active='1' AND (b.i_op='$key' OR a.i_do_code='$key' ) AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code 

			ORDER BY a.d_do DESC, a.i_do_code DESC ");	
		*/ 
		return $db2->query(" SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code  
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
					
			WHERE c.n_active='1' AND (e.i_op_code='$key' OR a.i_do_code='$key') AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code 

			ORDER BY a.d_do DESC, a.i_do_code DESC ");	
					
	}
	
	function getcabang($kddo){
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT  a.i_do_code AS iopcode, e.e_branch_name AS cabangname
			
			FROM tm_do_item b
			
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch
			INNER JOIN tr_customer f ON f.i_customer=e.i_customer
			
			WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' GROUP BY a.i_do_code, e.e_branch_name ");
	}
	
	/*
	function remote($destination_ip) {
		return $db2->query(" SELECT * FROM tr_printer WHERE ip='$destination_ip' ORDER BY i_printer DESC LIMIT 1 ");
	}
	*/
	
	function remote($id, $ip_address) {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' AND ip='$ip_address' ORDER BY i_printer DESC LIMIT 1 ");
	}

	function getinitial() {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial_code DESC LIMIT 1");
	}	

	function fprinted($kddo){
		 $db2 = $this->load->database('db_external', TRUE);
			$db2->query(" UPDATE tm_do SET f_printed='t' WHERE i_do_code='$kddo' ");
	}	
}
?>
