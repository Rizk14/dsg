<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $jenis_pembelian) {
	if ($cari == "all") {
		$sql = " * FROM tm_payment_pembelian  ";
		if ($jenis_pembelian != 0)
			$sql.= " WHERE jenis_pembelian = '$jenis_pembelian' ORDER BY tgl DESC, no_voucher ASC ";
		else
			$sql.= " ORDER BY tgl DESC, no_voucher ASC ";
		
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$sql = " * FROM tm_payment_pembelian  ";
		if ($jenis_pembelian != 0)
			$sql.= " WHERE jenis_pembelian = '$jenis_pembelian' ORDER BY tgl DESC, no_voucher ASC ";
		else
			$sql.= " WHERE UPPER(no_voucher) like UPPER('%$cari%') ORDER BY tgl DESC, no_voucher ASC ";
		
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// 11-06-2015, ambil total jumlah bayar
				$sqlxx = " SELECT SUM(jumlah_bayar) AS jumbayar FROM tm_payment_pembelian_detail WHERE id_payment = '$row1->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$totjumbayar	= $hasilxx->jumbayar;
					if ($totjumbayar == '')
						$totjumbayar = 0;
				}
				else
					$totjumbayar = 0;
				
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_payment_pembelian_detail WHERE id_payment = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						//----------------
						
						// ambil data nama supplier
						$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row2->id_supplier' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_supplier	= $hasilrow->kode_supplier;
							$nama_supplier	= $hasilrow->nama;
						}
						else {
							$kode_supplier	= '';
							$nama_supplier	= '';
							/*$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_supplier' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_supplier	= $hasilrow->nama;
							} */
						}
								
						$detail_fb[] = array( 'id'=> $row2->id,
												'jumlah_bayar'=> $row2->jumlah_bayar,
												'pembulatan'=> $row2->pembulatan,
												'jenis_pembulatan'=> $row2->jenis_pembulatan,
												'id_supplier'=> $row2->id_supplier,
												'kode_supplier'=> $kode_supplier,
												'nama_supplier'=> $nama_supplier,
												'subtotal'=> $row2->subtotal,
												'deskripsi'=> $row2->deskripsi,
												'cndn'=> $row2->cndn,
												'keterangan_cndn'=> $row2->keterangan_cndn
											);
					}
				}
				else {
					$detail_fb = '';
				}
				
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'no_voucher'=> $row1->no_voucher,
											'tgl'=> $row1->tgl,
											'tgl_update'=> $row1->tgl_update,
											'total'=> $row1->total,
											'totjumbayar'=> $totjumbayar,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($cari, $jenis_pembelian){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_payment_pembelian  ";
		if ($jenis_pembelian != 0)
			$sql.= " WHERE jenis_pembelian = '$jenis_pembelian' ";
		
		$query	= $this->db->query($sql);
	}
	else {
		$sql = " SELECT * FROM tm_payment_pembelian  ";
		if ($jenis_pembelian != 0)
			$sql.= " WHERE jenis_pembelian = '$jenis_pembelian' ";
		else
			$sql.= " WHERE UPPER(no_voucher) like UPPER('%$cari%') ";
		
		$query	= $this->db->query($sql);
	}
    
    return $query->result();  
  }
      
  function cek_data($no_voucher, $thn1){
    $this->db->select("id from tm_payment_pembelian WHERE no_voucher = '$no_voucher' 
				AND extract(year from tgl) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
 /* function save($no_voucher,$tgl_voucher,$total, $kode_supplier, $no_faktur, $jum_hutang, $jum_gabung, 
  $jum_bayar, $sisa_bayar, $jum_retur, $jenis_pembulatan){  
    $tgl = date("Y-m-d");
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_payment_pembelian WHERE no_voucher = '$no_voucher' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_payment_pembelian
			$data_header = array(
			  'no_voucher'=>$no_voucher,
			  'tgl'=>$tgl_voucher,
			  //'kode_supplier'=>$kode_supplier,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'total'=>$total
			);
			$this->db->insert('tm_payment_pembelian',$data_header);
						
			// ambil data terakhir di tabel tm_payment_pembelian
			$query2	= $this->db->query(" SELECT id FROM tm_payment_pembelian ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_payment	= $hasilrow->id;
						
			if ($jum_bayar!='0' || $jum_bayar!='') {
				// jika data jum_bayar tdk kosong, insert ke tm_payment_pembelian_detail
				$akumulasi_bayar = $jum_gabung+$jum_bayar;
				$sisa = $jum_hutang-$jum_retur-$akumulasi_bayar;
				
				if ($sisa < 0)
					$sisa = 0;
				$this->db->query(" UPDATE tm_pembelian_nofaktur SET sisa = '$sisa' WHERE no_faktur = '$no_faktur' 
					AND kode_supplier = '$kode_supplier' ");
				
				if ($jum_bayar > $sisa_bayar) {
					$pembulatan = $jum_bayar-$sisa_bayar;
					$jum_bayar = $sisa_bayar;
					$jenis_pembulatan = '1';
				}
				
				if (($jum_bayar < $sisa_bayar) && $jenis_pembulatan == '2') {
					$pembulatan = $sisa_bayar-$jum_bayar;
					
					// update status lunas di tabel faktur
					$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 't', sisa = '0' 
					WHERE no_faktur = '$no_faktur' AND kode_supplier = '$kode_supplier' ");
				}
				else {
					if ($sisa == 0) {
						// update status lunas di tabel faktur
						$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 't', sisa = '0' 
						WHERE no_faktur = '$no_faktur' AND kode_supplier = '$kode_supplier' ");
					}
				}
				
				$data_detail = array(
					'id_payment'=>$id_payment,
					'kode_supplier'=>$kode_supplier,
					'no_faktur'=>$no_faktur,
					'jumlah_bayar'=>$jum_bayar,
					'pembulatan'=>$pembulatan,
					'jenis_pembulatan'=>$jenis_pembulatan

				);
				$this->db->insert('tm_payment_pembelian_detail',$data_detail);
			}
			// ---------------------------

		} // end jika header blm ada
		else {
			// ambil data terakhir di tabel tm_payment_pembelian
			$query2	= $this->db->query(" SELECT id FROM tm_payment_pembelian ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_payment	= $hasilrow->id;
						
			if ($jum_bayar!='0' || $jum_bayar!='') {
				// jika data jum_bayar tdk kosong, insert ke tm_payment_pembelian_detail
				$akumulasi_bayar = $jum_gabung+$jum_bayar;
				$sisa = $jum_hutang-$jum_retur-$akumulasi_bayar;
				
				if ($sisa < 0)
					$sisa = 0;
				$this->db->query(" UPDATE tm_pembelian_nofaktur SET sisa = '$sisa' WHERE no_faktur = '$no_faktur' 
					AND kode_supplier = '$kode_supplier' ");
				
				if ($jum_bayar > $sisa_bayar) {
					$pembulatan = $jum_bayar-$sisa_bayar;
					$jum_bayar = $sisa_bayar;
				}
				
				if (($jum_bayar < $sisa_bayar) && $jenis_pembulatan == '2') {
					$pembulatan = $sisa_bayar-$jum_bayar;
					
					// update status lunas di tabel faktur
					$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 't', sisa = '0' 
					WHERE no_faktur = '$no_faktur' AND kode_supplier = '$kode_supplier' ");
				}
				else {
					if ($sisa == 0) {
						// update status lunas di tabel faktur
						$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 't', sisa = '0' 
						WHERE no_faktur = '$no_faktur' AND kode_supplier = '$kode_supplier' ");
					}
				}
				
				$data_detail = array(
					'id_payment'=>$id_payment,
					'kode_supplier'=>$kode_supplier,
					'no_faktur'=>$no_faktur,
					'jumlah_bayar'=>$jum_bayar,
					'pembulatan'=>$pembulatan,
					'jenis_pembulatan'=>$jenis_pembulatan

				);
				$this->db->insert('tm_payment_pembelian_detail',$data_detail);
			}
			// ---------------------------
			
		} // end else utk insert payment pembelian
  } */
    
  function delete($id){    
	// 1. rubah status_lunas = 'f' utk tiap2 fakturnya, dan update sisa hutangnya
	/*$query3	= $this->db->query(" SELECT kode_supplier FROM tm_payment_pembelian WHERE id = '$kode' ");
	$hasilrow = $query3->row();
	$kode_supplier	= $hasilrow->kode_supplier;
	
	$query2	= $this->db->query(" SELECT * FROM tm_payment_pembelian_detail WHERE id_payment = '$kode' ");
	if ($query2->num_rows() > 0){
		$hasil2=$query2->result();
		
		foreach ($hasil2 as $row2) {
			$jumlah_bayar = $row2->jumlah_bayar;
						
			$query2	= $this->db->query(" SELECT jumlah FROM tm_pembelian_nofaktur WHERE no_faktur = '$row2->no_faktur' 
						AND kode_supplier = '$kode_supplier' ");
			$hasilrow = $query2->row();
			$jum_hutang	= $hasilrow->jumlah; // 1. ini jumlah hutang dagang faktur tsb
			
			// 2. hitung jumlah uang muka di tabel SJ/pembelian
			$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_pembelian a, tm_pembelian_nofaktur b, 
			tm_pembelian_nofaktur_sj c WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
			AND b.id = c.id_pembelian_nofaktur AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$kode_supplier' ");
			$hasilrow = $query2->row();
			$uang_muka	= $hasilrow->tot_uang_muka;
			
			// 3. hitung jumlah yg sudah dibayar di tabel payment_pembelian
			$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar FROM tm_payment_pembelian a, 
			tm_payment_pembelian_detail b WHERE a.id = b.id_payment
			AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$kode_supplier' ");
			$hasilrow = $query2->row();
			$tot_bayar	= $hasilrow->tot_bayar; 
			
			// 4. jumlah retur utk faktur tsb jika ada
			$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
			tm_retur_beli_faktur b, tm_retur_beli_detail c WHERE a.id = b.id_retur_beli
			AND b.id = c.id_retur_beli_faktur AND a.status_nota = 't'
			AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$kode_supplier' ");
			
			$hasilrow = $query2->row();
			$tot_retur	= $hasilrow->tot_retur;	
			
			if ($tot_retur == '')
				$tot_retur = '0';
			
			// 5. hitung sisa
			$sisa = $jum_hutang-$tot_retur-($uang_muka+$tot_bayar-$jumlah_bayar);
			//--------------------------------
			
			$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 'f', sisa='$sisa' 
					WHERE no_faktur = '$row2->no_faktur' AND kode_supplier = '$kode_supplier' ");
		}
	}
			
		$this->db->delete('tm_payment_pembelian_detail', array('id_payment' => $kode));
		$this->db->delete('tm_payment_pembelian', array('id' => $kode));
    */
    
    $query2	= $this->db->query(" SELECT a.is_makloon, b.* FROM tm_payment_pembelian a 
						INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
						WHERE b.id_payment = '$id' ");
	if ($query2->num_rows() > 0){
		$hasil2=$query2->result();
		// update status lunasnya menjadi 'f'
		foreach ($hasil2 as $row2) {
			$query3	= $this->db->query(" SELECT * FROM tm_payment_pembelian_nofaktur WHERE id_payment_pembelian_detail = '$row2->id' ");
			if ($query3->num_rows() > 0){
				$hasil3= $query3->result();
				foreach ($hasil3 as $row3) {
					if ($row2->is_makloon == 'f')
						$this->db->query(" UPDATE tm_pembelian_nofaktur SET status_lunas = 'f', sisa= NULL 
										WHERE id = '$row3->id_pembelian_nofaktur' ");
					else
						$this->db->query(" UPDATE tm_pembelian_makloon_faktur SET status_lunas = 'f'
										WHERE id = '$row3->id_pembelian_nofaktur' ");
				}
			}
		} // end foreach
		
		// hapus data di tm_payment_pembelian_nofaktur
		$this->db->query(" DELETE FROM tm_payment_pembelian_nofaktur WHERE id_payment_pembelian_detail = '$row2->id' ");
	}
	$this->db->delete('tm_payment_pembelian_detail', array('id_payment' => $id));
	$this->db->delete('tm_payment_pembelian', array('id' => $id));
  } // end delete
  
  //function get_faktur($num, $offset, $supplier, $cari) {
	  // skrg ga pake paging
  //function get_faktur($supplier, $cari, $pkp) {
  function get_faktur($cari, $jenis_pembelian, $is_makloon) {
	// ambil data faktur pembelian
	//if ($cari == "all") {		
		// &&&&&&&&&&&& 2 des 2011 ^^^^^^^^^^^^^^^^^
		// 25-06-2015 DIGANTI KARENA GA PAKE ACUAN FAKTUR QUILTING
		// QUERY KE SJ MASUK HASIL JAHIT SEMENTARA GA DISERTAKAN
		/*$sql = "id, no_faktur, tgl_faktur, tgl_update, kode_supplier, jumlah, is_makloon, is_jahit
				FROM tm_pembelian_nofaktur WHERE status_lunas = 'f' AND jenis_pembelian = '$jenis_pembelian' 
				UNION SELECT id, no_faktur, tgl_faktur, tgl_update, kode_unit, jumlah, is_makloon, is_jahit 
				FROM tm_faktur_makloon
				WHERE status_lunas = 'f' AND jenis_pembelian = '$jenis_pembelian' ";
		if ($jenis_pembelian == 1)
			$sql.= " UNION SELECT id, no_sj, tgl_sj, tgl_update, kode_unit, total, is_makloon, is_jahit
				FROM tm_sj_hasil_jahit WHERE status_lunas = 'f' AND jenis_pembelian = '$jenis_pembelian' ";
		$sql.=" order by kode_supplier, tgl_faktur DESC "; */
		
		if ($is_makloon == 'f') {
			$sql = "id, no_faktur, tgl_faktur, tgl_update, id_supplier, jumlah, '0' as jenis_makloon
					FROM tm_pembelian_nofaktur WHERE status_lunas = 'f' AND jenis_pembelian = '$jenis_pembelian' ";
			if ($cari != "all")
				$sql.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
			$sql.=" order by id_supplier, tgl_faktur DESC ";
		}
		else {
			$sql = "id, no_faktur, tgl_faktur, tgl_update, id_supplier, jumlah, jenis_makloon
					FROM tm_pembelian_makloon_faktur WHERE status_lunas = 'f' AND jenis_pembelian = '$jenis_pembelian' ";
			if ($cari != "all")
				$sql.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
			$sql.=" order by id_supplier, tgl_faktur DESC ";
		}
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		$this->db->select($sql, false);
		$query = $this->db->get();

		//}
	/*	else {
			$sql = " * FROM tm_pembelian_nofaktur WHERE status_lunas = 'f' AND kode_supplier = '$supplier'  ";
			if ($pkp == 't')
				$sql.= " AND status_faktur_pajak = 't' ";
			$sql.= " order by kode_supplier, tgl_faktur DESC ";
			
			$this->db->select($sql, false);
			$query = $this->db->get();
		} */
	//}
	/*else {
		// &&&&&&&&&&&& 2 des 2011 ^^^^^^^^^^^^^^^^^
		$sql = "SELECT a.id, a.no_faktur, a.tgl_faktur, a.tgl_update, a.kode_supplier, a.jumlah, a.is_makloon, a.is_jahit
				FROM tm_pembelian_nofaktur a, tm_supplier b WHERE a.kode_unit = b.kode_supplier AND a.status_lunas = 'f' 
				AND a.jenis_pembelian = '$jenis_pembelian' 
				AND UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER ('%$cari%')
				UNION SELECT a.id, a.no_faktur, a.tgl_faktur, a.tgl_update, a.kode_unit, a.jumlah, a.is_makloon, a.is_jahit 
				FROM tm_faktur_makloon a, tm_supplier b
				WHERE a.kode_unit = b.kode_supplier AND a.status_lunas = 'f' AND a.jenis_pembelian = '$jenis_pembelian' 
				AND UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER ('%$cari%') ";
		
		if ($jenis_pembelian == 1)
			$sql.= " UNION SELECT a.id, a.no_sj, a.tgl_sj, a.tgl_update, a.kode_unit, a.total, a.is_makloon, a.is_jahit
				FROM tm_sj_hasil_jahit a, tm_unit_jahit b WHERE a.status_lunas = 'f' AND a.jenis_pembelian = '$jenis_pembelian'
				AND a.kode_unit = b.kode_unit AND UPPER(a.no_sj) like UPPER('%$cari%') ";
				
		$sql.=	" order by kode_supplier, tgl_faktur DESC ";
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

		$this->db->select($sql, false);
		$query = $this->db->get();
	} */
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				/*if ($is_makloon == 'f')
					$sup = $row1->kode_supplier;
				else
					$sup = $row1->kode_unit; */
				$sup = $row1->id_supplier;
				$query2	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$sup' ");
				if ($query2->num_rows() > 0){
					$hasilrow = $query2->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else {
					$kode_supplier	= '';
					$nama_supplier	= '';
				}
				// 25-06-2015 DIKOMEN
				/*else {
					$query2	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$sup' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$nama_supplier	= $hasilrow->nama;
					}
				} */
																
				$data_fb[] = array(			'id'=> $row1->id,	
											'id_supplier'=> $sup,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'tgl_update'=> $row1->tgl_update,
											'totalnya'=> $row1->jumlah,
											'jenis_makloon'=> $row1->jenis_makloon
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  //function get_detail_faktur($list_faktur, $list_is_quilting, $list_is_jahit){
  function get_detail_faktur($list_faktur, $is_makloon){
    $detail_faktur = array();
    
    $iskeluar = 'f';
    for($j=0; $j<count($list_faktur)-1; $j++){
		$list_faktur[$j] = trim($list_faktur[$j]);
		//$list_is_quilting[$j] = trim($list_is_quilting[$j]);
		//$list_is_jahit[$j] = trim($list_is_jahit[$j]);
		
		// 25-06-2015 is quilting ga dipake lagi, karena bhn quilting digabung ke master bhn baku
		// JAHIT JUGA GA USAH
		
		//if ($list_is_quilting[$j] == 'f') {
			/*echo $list_is_quilting[$j]."<br>";
			$sql = "SELECT no_faktur, tgl_faktur, kode_supplier, tgl_update, jumlah 
									FROM tm_pembelian_nofaktur WHERE id = '".$list_faktur[$j]."'"; echo $sql."<br>"; */
			
			if ($is_makloon == '')
				$query2	= $this->db->query(" SELECT no_faktur, tgl_faktur, id_supplier, tgl_update, jumlah 
									FROM tm_pembelian_nofaktur WHERE id = '".$list_faktur[$j]."'  ");
			else
				$query2	= $this->db->query(" SELECT no_faktur, tgl_faktur, id_supplier, tgl_update, jumlah 
									FROM tm_pembelian_makloon_faktur WHERE id = '".$list_faktur[$j]."'  ");
		//}
		/*else {
			if ($list_is_jahit[$j] == 'f')
				$query2	= $this->db->query(" SELECT no_faktur, tgl_faktur, kode_unit, tgl_update, jumlah 
									FROM tm_faktur_makloon WHERE id = '".$list_faktur[$j]."'  ");
			else
				$query2	= $this->db->query(" SELECT no_sj, tgl_sj, kode_unit, tgl_update, total 
									FROM tm_sj_hasil_jahit WHERE id = '".$list_faktur[$j]."'  ");
		} */
			
		if ($query2->num_rows() > 0){
			$hasilrow = $query2->row();
			//if ($list_is_quilting[$j] == 'f')
				$id_supplier	= $hasilrow->id_supplier;
			//else
			//	$kode_supplier	= $hasilrow->kode_unit;
				
			//if ($list_is_jahit[$j] == 'f') {
				$no_faktur	= $hasilrow->no_faktur;
				$tgl_faktur	= $hasilrow->tgl_faktur;
				$jum_hutang	= $hasilrow->jumlah; // 1. ini jumlah hutang dagang faktur tsb
			//}
			/*else {
				$no_faktur	= $hasilrow->no_sj;
				$tgl_faktur	= $hasilrow->tgl_sj;
				$jum_hutang	= $hasilrow->total;
			} */
			
			$tgl_update	= $hasilrow->tgl_update;
			
			$pisah1 = explode("-", $tgl_faktur);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			
			$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
				
			// 4. jumlah retur utk faktur tsb jika ada
			// catatan: field status_nota di tabel retur_beli apakah harus yg 't' atau 'f'? sementara status_nota = 't'
			// koreksi 25 okt 2011: ga perlu ada acuan ke status_nota di nota retur
			// 28 nov 2011: sum retur ini hanya utk pembelian non-quilting. utk quilting, ga pernah ada nota debet
			
			//11-04-2012. ga perlu pake relasi ke tm_retur_beli_faktur. cek dulu utk data lama
			
			if ($is_makloon == '') {
				$sqlcek2 = " SELECT id, jumlah FROM tm_retur_beli WHERE extract(month from tgl_retur) = '$bln1' 
							AND extract(year from tgl_retur) = '$thn1' AND id_supplier = '$id_supplier' "; //echo $sqlcek2;
				$querycek2 = $this->db->query($sqlcek2);
				if ($querycek2->num_rows() > 0){
					$hasilcek2 = $querycek2->row();
					$id_retur = $hasilcek2->id;
					
					$sqlcek3 = " SELECT id FROM tm_retur_beli_detail where id_retur_beli = '$id_retur' ";
					$querycek3 = $this->db->query($sqlcek3);
					if ($querycek3->num_rows() == 0){
						$hasilcek3 = $querycek3->row();
						
						$selisihnya = $jum_hutang-$hasilcek2->jumlah;
						if ($iskeluar == 'f' && $selisihnya > 0) {
							$tot_retur	= $hasilcek2->jumlah;
							$iskeluar = 't';
						}
						else
							$tot_retur = 0;
					}
					else {
						$sqlcek = "SELECT b.id FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
							WHERE b.id_pembelian_nofaktur = '".$list_faktur[$j]."' AND a.id_supplier = '$id_supplier' ";
						//echo $sqlcek."<br>";
						// 11-08-2015, $no_faktur dikeluarin dari query
						$querycek = $this->db->query($sqlcek);
						if ($querycek->num_rows() > 0){
							//11-08-2015: ACUAN FAKTUR DIPAKE
							$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
							INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli 
							INNER JOIN tm_retur_beli_detail c ON b.id = c.id_retur_beli_faktur 
							WHERE b.id_pembelian_nofaktur = '".$list_faktur[$j]."' AND a.id_supplier = '$id_supplier' ");
							// 11-08-2015, no_faktur dikeluarin dari query
							$hasilrow = $query2->row();
							$tot_retur	= $hasilrow->tot_retur;	
							
							if ($tot_retur == '')
								$tot_retur = '0';
							// 11-08-2015
							//else
							//	$tot_retur = number_format($tot_retur, 2, '.','');
						}
						else {
							$tot_retur = 0;
						}
					} // end if baru
				}
				else
					$tot_retur = 0;
				
				//==========================================================================================================
			}
			else
				$tot_retur = 0;
			
			//$jum_bayar = $jum_hutang-$tot_retur-$jum_gabung;
			$jum_bayar = $jum_hutang-$tot_retur;
			$jum_bayar = number_format($jum_bayar, 2, '.','');
			//$pembulatan = number_format($pembulatan, 2, '.','');
			
			//if ($list_is_jahit[$j] == 'f') {
				$query2	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier' ");
				$hasilrow = $query2->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
			//}
			/*else {
				$query2	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$kode_supplier' ");
				$hasilrow = $query2->row();
				$nama_supplier	= $hasilrow->nama;
			} */
		
			$detail_faktur[] = array(		'id'=> $list_faktur[$j],
											//'is_quilting'=> $list_is_quilting[$j],
											//'is_jahit'=> $list_is_jahit[$j],
											'no_faktur'=> $no_faktur,
											'id_supplier'=> $id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_faktur'=> $tgl_faktur,
											'tgl_update'=> $tgl_update,
											'jum_hutang'=> $jum_hutang,
											//'jum_gabung'=> $jum_gabung,
											'tot_retur'=> $tot_retur,
											'jum_bayar'=> $jum_bayar
											//'pembulatan'=> $pembulatan
								);
		}									
	}
    // $$$$$$$$$$$
    
   /* foreach($list_faktur as $row1) {
		if ($row1 != '') {			
			$row1 = trim($row1);
			$query2	= $this->db->query(" SELECT no_faktur, tgl_faktur, kode_supplier, tgl_update, jumlah 
									FROM tm_pembelian_nofaktur WHERE id = '$row1'  ");
			if ($query2->num_rows() > 0){
				$hasilrow = $query2->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$no_faktur	= $hasilrow->no_faktur;
				$tgl_faktur	= $hasilrow->tgl_faktur;
				$tgl_update	= $hasilrow->tgl_update;
				$jum_hutang	= $hasilrow->jumlah; // 1. ini jumlah hutang dagang faktur tsb
			} */
			// 2. hitung jumlah uang muka di tabel SJ/pembelian
			/*$query2	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_pembelian a, tm_pembelian_nofaktur b, 
			tm_pembelian_nofaktur_sj c WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
			AND b.id = c.id_pembelian_nofaktur AND b.id = '$row1' ");
			$hasilrow = $query2->row();
			$uang_muka	= $hasilrow->tot_uang_muka; */
			
			// 3. hitung jumlah yg sudah dibayar di tabel payment_pembelian
		/*	$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as bulat 
			FROM tm_payment_pembelian a, 
			tm_payment_pembelian_detail b WHERE a.id = b.id_payment
			AND b.no_faktur = '$row1' ");
			$hasilrow = $query2->row();
			$tot_bayar	= $hasilrow->tot_bayar;
			$pembulatan	= $hasilrow->bulat;
		
			$jum_gabung = $uang_muka+$tot_bayar; */
			
	/*		$pisah1 = explode("-", $tgl_faktur);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
			
			$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
				
			// 4. jumlah retur utk faktur tsb jika ada
			// catatan: field status_nota di tabel retur_beli apakah harus yg 't' atau 'f'? sementara status_nota = 't'
			// koreksi 25 okt 2011: ga perlu ada acuan ke status_nota di nota retur
			// 28 nov 2011: sum retur ini hanya utk pembelian non-quilting. utk quilting, ga pernah ada nota debet
			$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
			tm_retur_beli_faktur b, tm_retur_beli_detail c WHERE a.id = b.id_retur_beli
			AND b.id = c.id_retur_beli_faktur 
			AND b.no_faktur = '$no_faktur' AND a.kode_supplier = '$kode_supplier' ");
				
			$hasilrow = $query2->row();
			$tot_retur	= $hasilrow->tot_retur;	
			
			if ($tot_retur == '')
				$tot_retur = '0';
			else
				$tot_retur = number_format($tot_retur, 2, '.','');
			
			//$jum_bayar = $jum_hutang-$tot_retur-$jum_gabung;
			$jum_bayar = $jum_hutang-$tot_retur;
			$jum_bayar = number_format($jum_bayar, 2, '.','');
			//$pembulatan = number_format($pembulatan, 2, '.','');
			
			$query2	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$kode_supplier' ");
			$hasilrow = $query2->row();
			$nama_supplier	= $hasilrow->nama;
			
			$detail_faktur[] = array(		'id'=> $row1,
											'no_faktur'=> $no_faktur,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_faktur'=> $tgl_faktur,
											'tgl_update'=> $tgl_update,
											'jum_hutang'=> $jum_hutang,
											//'jum_gabung'=> $jum_gabung,
											'tot_retur'=> $tot_retur,
											'jum_bayar'=> $jum_bayar
											//'pembulatan'=> $pembulatan
								);
		} 
	} */
	return $detail_faktur;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_pkp_tipe_pajak_bykodesup($id_sup){
	$query	= $this->db->query(" SELECT pkp, tipe_pajak, top FROM tm_supplier where id = '$id_sup' ");    
    return $query->result();  
  }
  
  function get_payment($id_payment) {
		$query	= $this->db->query(" SELECT * FROM tm_payment_pembelian where id = '$id_payment' ");
	
		$data_pay = array();
		$detail_pay = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_payment_pembelian_detail WHERE id_payment = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT tgl_faktur, jumlah 
						FROM tm_pembelian_nofaktur WHERE no_faktur = '$row2->no_faktur' AND kode_supplier = '$row2->kode_supplier' ");
						$hasilrow = $query3->row();
						$tgl_faktur	= $hasilrow->tgl_faktur;
						$jum_hutang	= $hasilrow->jumlah;
						
						// 2. hitung jumlah uang muka di tabel SJ/pembelian
						$query3	= $this->db->query(" SELECT SUM(a.uang_muka) as tot_uang_muka FROM tm_pembelian a, tm_pembelian_nofaktur b, 
						tm_pembelian_nofaktur_sj c WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
						AND b.id = c.id_pembelian_nofaktur AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$row2->kode_supplier' ");
						$hasilrow = $query3->row();
						$uang_muka	= $hasilrow->tot_uang_muka;
						
						// 3. hitung jumlah yg sudah dibayar dan pembulatannya di tabel payment_pembelian
						$query3	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as bulat FROM tm_payment_pembelian a, 
						tm_payment_pembelian_detail b WHERE a.id = b.id_payment
						AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$row2->kode_supplier' ");
						$hasilrow = $query3->row();
						$tot_bayar	= $hasilrow->tot_bayar;
						$pembulatan	= $hasilrow->bulat;
												
						// 4. jumlah retur utk faktur tsb jika ada
						$query3	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a, 
						tm_retur_beli_faktur b, tm_retur_beli_detail c WHERE a.id = b.id_retur_beli
						AND b.id = c.id_retur_beli_faktur AND a.status_nota = 't'
						AND b.no_faktur = '$row2->no_faktur' AND a.kode_supplier = '$row2->kode_supplier' ");
										
						$hasilrow = $query3->row();
						$tot_retur	= $hasilrow->tot_retur;	
						
						if ($tot_retur == '')
							$tot_retur = '0';
						else
							$tot_retur = number_format($tot_retur, 2, '.','');
					
						$jum_gabung = $uang_muka+$tot_bayar;
										
						$pisah1 = explode("-", $tgl_faktur);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
						
						$jum_bayar = number_format($row2->jumlah_bayar, 2, '.','');
						$jum_gabung = number_format($jum_gabung, 2, '.','');
						$pembulatan = number_format($pembulatan, 2, '.','');
						
						// ambil data nama supplier
						$query3	= $this->db->query(" SELECT nama, top, pkp, tipe_pajak FROM tm_supplier WHERE 
												kode_supplier = '$row2->kode_supplier' ");
								$hasilrow = $query3->row();
								$nama_supplier	= $hasilrow->nama;
								$top	= $hasilrow->top;
								$pkp	= $hasilrow->pkp;
								$tipe_pajak	= $hasilrow->tipe_pajak;
						
						$detail_pay[] = array(	'id'=> $row2->id,
												'kode_supplier'=> $row2->kode_supplier,
												'nama_supplier'=> $nama_supplier,
												'no_faktur'=> $row2->no_faktur,
												'tgl_faktur'=> $tgl_faktur,
												'jum_hutang'=> $jum_hutang,
												'jum_gabung'=> $jum_gabung,
												'jum_bayar'=> $jum_bayar,
												'tot_retur'=> $tot_retur,
												'pembulatan'=> $pembulatan,
												'jenis_pembulatan'=> $row2->jenis_pembulatan
											);
					}
				}
				else {
					$detail_pay = '';
				}
				
				
				$pisah1 = explode("-", $row1->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_voucher = $tgl1."-".$bln1."-".$thn1;
								
				$data_pay[] = array(		'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'no_voucher'=> $row1->no_voucher,	
											'tgl_voucher'=> $tgl_voucher,
											'total'=> $row1->total,
											//'kode_supplier'=> $row1->kode_supplier,
											//'nama_supplier'=> $nama_supplier,
											//'top'=> $top,
											//'pkp'=> $pkp,
											//'tipe_pajak'=> $tipe_pajak,
											'detail_pay'=> $detail_pay
											);
				$detail_pay = array();
			} // endforeach header
		}
		else {
			$data_pay = '';
		}
		return $data_pay;
  }  

}

