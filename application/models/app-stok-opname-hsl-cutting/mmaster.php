<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so_hasil_cutting($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id, tgl_so, status_approve FROM tt_stok_opname_hasil_cutting
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
				
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so
							);
							
		return $so_bahan;
  }
  
  /*function get_all_stok_opname($bulan, $tahun) {

		$query	= $this->db->query(" SELECT a.id as id_header, b.* FROM tt_stok_opname_hasil_cutting_detail b, 
					tt_stok_opname_hasil_cutting a
					WHERE b.id_stok_opname_hasil_cutting = a.id 
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY b.kode_brg_jadi, b.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
						// bhn baku
						$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang 
										WHERE kode_brg = '$row->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$query3	= $this->db->query(" SELECT nama_brg FROM tm_brg_hasil_makloon
										WHERE kode_brg = '$row->kode_brg' ");
							if ($query3->num_rows() > 0){
								$nama_brg	= $hasilrow->nama_brg;
							}
							else
								$nama_brg = '';
						}
				
				// ambil nama brg jadi dan stok terkini
				$query3	= $this->db->query(" SELECT a.kode_brg, a.kode_brg_jadi, a.stok, c.e_product_motifname
					FROM tm_stok_hasil_cutting a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi
					AND a.kode_brg = '$row->kode_brg' AND a.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg_jadi = '';
					$stok = '';
				}
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $nama_brg,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  } */
  
  function get_all_stok_opname_hasil_cutting($bulan, $tahun) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.kode_brg, d.nama_brg
					FROM tt_stok_opname_hasil_cutting a 
					INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
					INNER JOIN tm_barang_wip d ON b.id_brg_wip = d.id
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' 
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				// 06-04-2015. 08-10-2015 GA DIPAKE LAGI
				// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a, 
									tt_stok_opname_hasil_cutting_detail b
									WHERE a.id = b.id_stok_opname_hasil_cutting
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' 
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				//	2.1. masuk bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//	2.2. masuk retur bhn baku
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk+= $hasilrow->jum_masuk;
				}
				
				// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar;
				*/ 
				//-------------------------------------------------------------------------------------
				
				// 1. berdasarkan warna brg jadi
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_hasil_cutting_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_stok_opname_hasil_cutting_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_cutting_warna
						$query3	= $this->db->query(" SELECT b.stok FROM tm_stok_hasil_cutting a 
							INNER JOIN tm_stok_hasil_cutting_warna b ON a.id = b.id_stok_hasil_cutting
							WHERE a.id_brg_wip = '$row->id_brg_wip' 
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// 06-04-2015. 08-10-2015 GA DIPAKE!
						// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
					/*	$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a 
											INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
											INNER JOIN tt_stok_opname_hasil_cutting_detail_warna c ON b.id = c.id_stok_opname_hasil_cutting_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' 
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						//	2.1. masuk bonmmasukcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmmasukcutting a 
									INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
									INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						//	2.2. masuk retur bhn baku
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna+= $hasilrow->jum_masuk;
						}
						
						// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna;
						*/
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
				$detail_bhnbaku = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }

}
