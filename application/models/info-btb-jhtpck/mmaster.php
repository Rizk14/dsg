<?php
class Mmaster extends CI_model{
	function __construct() { 
		parent::__construct();
	}
	
	function getunitjahit(){
		$query = $this->db->query('SELECT * from tm_unit_jahit order by Kode_unit');
		
		return $query->result();
		}
	function getunitpacking(){
		$query = $this->db->query('SELECT * from tm_unit_packing order by Kode_unit');
		
		return $query->result();
		}
		
	function getallviewlapbtb($id_unit_jahit,$id_unit_packing,$date_from,$date_to){
			$pencarian='';
		if($id_unit_jahit!=0){
			$pencarian.="AND id_unit_jahit='$id_unit_jahit' ";
			}
		elseif($id_unit_packing!=0){
			$pencarian.="and id_unit_packing='$id_unit_packing'";
			}
		elseif($id_unit_packing!=0 && $id_unit_jahit){
			$pencarian.="and id_unit_packing='$id_unit_packing' AND id_unit_jahit='$id_unit_jahit'";
			}
		
		$sql="select id from tm_pembelianpackjht_wip where status_aktif='t' 
		and tgl_sjpembelianpackjht >=to_date('$date_from','dd-mm-yyyy') 
		and tgl_sjpembelianpackjht <=to_date('$date_to','dd-mm-yyyy') 
		".$pencarian."
		 order by no_sjmasukpembelianpackjht ASC";
		$query = $this->db->query($sql);
		
		$data_beli= array();
		$detail_beli=array();
		
		if($query->num_rows() > 0){
		$hasil= $query->result();
		foreach ($hasil as $row1){
			$query2= $this->db->query("select c.id_brg_wip,d.id_unit_jahit,d.id_unit_packing, no_sj,no_sjmasukpembelianpackjht,tgl_sj,c.qty,c.harga_j,c.harga_p,c.diskon,c.total as subtotal, d.total from tm_sjmasukgudangjadi_detail a
			inner join tm_sjmasukgudangjadi b on a.id_sjmasukgudangjadi=b.id 
			inner join tm_pembelianpackjht_wip_detail c on c.id_sjmasukgudangjadi_detail=a.id 
			inner join tm_pembelianpackjht_wip d on d.id=c.id_pembelianpackjht_wip
			where d.id='$row1->id' order by a.id asc");
			
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->result();
		foreach ($hasil2 as $row2){
		$query3 = $this->db->query("select * from tm_unit_jahit where id='$row2->id_unit_jahit'");
		if($query3->num_rows() > 0){
			$hasil3 = $query3->row();
			$nama_unit_jahit=$hasil3->nama;
			}
			else
			{
			$nama_unit_jahit='';	
				}
				

		$query3 = $this->db->query("select * from tm_unit_packing where id='$row2->id_unit_packing'");
		if($query3->num_rows() > 0){
			$hasil3 = $query3->row();
			$nama_unit_packing=$hasil3->nama;
			}
			else
			{
			$nama_unit_packing='';	
				}
			
		$query5 =  $this->db->query("select * from tm_barang_wip where id='$row2->id_brg_wip'");
		if($query5->num_rows() > 0){
			$hasil5 =$query5->row();
			$kode_brg_wip = $hasil5->kode_brg;
			$nama_brg_wip = $hasil5->nama_brg;
			}
		else{
			$kode_brg_wip='';
			$nama_brg_wip='';
			}	
					
				$no_sj = $row2->no_sj;
				$no_sjmasukpembelianpackjht =$row2->no_sjmasukpembelianpackjht;
				$tgl_sj = $row2->tgl_sj;
				//$kode_brg = $hasil2->kode_brg;
				//$nama_brg_wip = $hasil2->nama_brg_wip;
				$qty =  $row2->qty;
				$harga_p = $row2->harga_p;
				$harga_j = $row2->harga_j;
				$diskon = $row2->diskon;
				$total = $row2->total;
			$subtotal = $row2->subtotal;
		
		
		$detail_beli[]=array(
		'kode_brg_wip'=>$kode_brg_wip,
		'nama_brg_wip'=>$nama_brg_wip,
		'qty'=>$qty,
		'harga_p'=>$harga_p,
		'harga_j'=>$harga_j,
		'diskon'=>$diskon,
		'subtotal'=>$subtotal
		
		);					
			
		}	
			$data_beli[]=array(
			'no_sj'=>$no_sj,
			'no_sjmasukpembelianpackjht'=>$no_sjmasukpembelianpackjht,
			'tgl_sj'=>$tgl_sj,
			'nama_unit_jahit'=>$nama_unit_jahit,
			'nama_unit_packing'=>$nama_unit_packing,
			'total'=>$total,
			'detail_beli'=> $detail_beli
		);
		
		$detail_beli=array();
		}
	}
}
		else{
			$data_beli[]=array(
			'no_sj'=>'',
			'no_sjmasukpembelianpackjht'=>'',
			'tgl_sj'=>'',
			'nama_unit_jahit'=>'',
			'nama_unit_packing'=>'',
			'total'=>'',
			'detail_beli'=> ''
			);
			}	
		return $data_beli;
	
}
}
?>
