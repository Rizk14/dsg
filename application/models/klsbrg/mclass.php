<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_class ORDER BY i_class DESC ", false);
	}

	function viewperpages($limit,$offset) {	
		 $db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_class ORDER BY i_class DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($iclass,$eclassname) {
		 $db2=$this->load->database('db_external', TRUE);
	/*
    	$db2->set(
    		array(
    			'i_class' => $iclass,
    			'e_class_name' => $eclassname
    		)
    	);
    	$db2->insert('tr_class');
	*/

	$str = array(
		'i_class'=>$iclass,
		'e_class_name'=>$eclassname
	);
	$db2->insert('tr_class',$str);
	
	/*
	if($db2->table_exists()) {
		$db2->set('i_class',$iclass);
		$db2->set('e_class_name',$eclassname);
		$db2->insert('tr_class');
	} else {
		echo "Tabel belum didefinisikan!";			
	}*/
	redirect('klsbrg/cform/');
    }
	
	function mupdate($iclass,$eclassname) {
		 $db2=$this->load->database('db_external', TRUE);
		$class_item	= array(
			'e_class_name'=>$eclassname
		);
		
		$db2->update('tr_class',$class_item,array('i_class'=>$iclass));
		redirect('klsbrg/cform');
	}

	function classcode() {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT cast(i_class AS integer)+1 AS classcode FROM tr_class ORDER BY i_class DESC LIMIT 1");
	}

	function medit($id) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_class WHERE i_class='$id' ");
	}
	
	function viewcari($txt_i_class,$txt_e_class_name) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_class WHERE i_class='$txt_i_class' OR e_class_name LIKE '%$txt_e_class_name%' ");
	}
	
	function mcari($txt_i_class,$txt_e_class_name,$limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_class WHERE i_class='$txt_i_class' OR e_class_name LIKE '%$txt_e_class_name%' LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

  function delete($id) {
	   $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_class',array('i_class'=>$id));
		redirect('klsbrg/cform/');	 
  }		
}
?>
