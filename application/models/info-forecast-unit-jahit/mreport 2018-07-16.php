<?php
class Mreport extends CI_Model{
  function Mreport() {

  parent::__construct();
  }
  
 function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
   function get_mutasi_unit($bulan, $tahun, $unit_jahit){
	   
	  $tgl=date('Y-m-d');
	   if ($bulan == 1) {
				$bln_query = 12;
				$thn_query = $tahun-1;
			}
			else {
				$bln_query = $bulan-1;
				$thn_query = $tahun;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
	   
	   $filter2="";  $filter3=""; 
	 if ($unit_jahit != '0') {
			$filter2 .= " AND id_unit = '$unit_jahit' ";	
		}  
	 if ($unit_jahit != '0') {
			$filter3 .= " AND id_unit_jahit = '$unit_jahit' ";	
		}  	
	   
    $query = $this->db->query(" SELECT id,id_unit FROM tt_forecast_unit_jahit 
    WHERE bulan='$bulan' AND tahun='$tahun' ".$filter2." ORDER BY id_unit");
    
    if ($query->num_rows() > 0){
		$hasil= $query->result();
		foreach($hasil as $row){
			
			$sql3=" SELECT id,id_brg_wip,jum_forecast FROM tt_forecast_unit_jahit_detail WHERE id_forecast_unit_jahit= '$row->id' order by id_brg_wip ";	
			$query3	= $this->db->query($sql3);
			$hasil3= $query3->result();
		
			foreach($hasil3 as $row3){
					$query4	= $this->db->query(" SELECT * FROM tm_barang_wip WHERE id = '$row3->id_brg_wip' order by id ");
				$hasilrow4 = $query4->row();
				$kode_brg	= $hasilrow4->kode_brg;
				$nama_brg	= $hasilrow4->nama_brg;
				
				$query7	= $this->db->query(" select sum(qty) as tot from tm_bonmkeluarcutting a inner join tm_bonmkeluarcutting_detail b  on a.id=b.id_bonmkeluarcutting 
			where b.id_brg_wip = '$row3->id_brg_wip' and a.bln_forecast='$bulan' and a.thn_forecast='$tahun'  and jenis_keluar='1' AND id_unit = '$row->id_unit'
			");
				$hasilrow7 = $query7->row();
				$total_bb	= $hasilrow7->tot;
				
				$query8	= $this->db->query(" select sum(sisa) as tot from tt_forecast_unit_jahit a inner join tt_forecast_unit_jahit_detail b  on a.id=b.id_forecast_unit_jahit
			where b.id_brg_wip = '$row3->id_brg_wip' and a.bulan='$bln_query' and a.tahun='$thn_query'  AND id_unit = '$row->id_unit'
			");
				$hasilrow8 = $query8->row();
				$total_sfk	= $hasilrow8->tot;
				//var_dump($row3->id_brg_wip);
				//var_dump($total_sfk);
				
					$query5	= $this->db->query(" select sum(qty) as tot from tm_sjkeluarwip a inner join tm_sjkeluarwip_detail b  on a.id=b.id_sjkeluarwip
			where b.id_brg_wip = '$row3->id_brg_wip' and a.bln_forecast='$bulan' and a.thn_forecast='$tahun'  and jenis_keluar='3' AND id_unit_jahit = '$row->id_unit'
			");
				$hasilrow5 = $query5->row();
				$total_tp	= $hasilrow5->tot;	

				
					$query6	= $this->db->query(" select sum(qty) as tot from tm_sjmasukwip a inner join tm_sjmasukwip_detail b  on a.id=b.id_sjmasukwip 
			where b.id_brg_wip = '$row3->id_brg_wip' and a.bln_forecast='$bulan' and a.thn_forecast='$tahun'  and jenis_masuk='1' AND id_unit_jahit = '$row->id_unit'
			");
				$hasilrow6 = $query6->row();
				$total_tb	= $hasilrow6->tot;
				
			$query10	= $this->db->query(" select sum(qty) as tot from tm_sjmasukwip a inner join tm_sjmasukwip_detail b  on a.id=b.id_sjmasukwip 
			where b.id_brg_wip = '$row3->id_brg_wip' and a.bln_forecast='$bulan' and a.thn_forecast='$tahun'  and jenis_masuk='2' AND id_unit_jahit = '$row->id_unit'
			");
				$hasilrow10 = $query10->row();
				$total_kp	= $hasilrow10->tot;		
				
					$query9	= $this->db->query(" select sum(qty) as tot from tm_sjmasukbhnbakupic a inner join tm_sjmasukbhnbakupic_detail b  on a.id=b.id_sjmasukbhnbakupic
			where b.id_brg_wip = '$row3->id_brg_wip' and a.bln_forecast='$bulan' and a.thn_forecast='$tahun'  and jenis_masuk='1' AND id_unit = '$row->id_unit'
			");
				$hasilrow9 = $query9->row();
				$total_rbb	= $hasilrow9->tot;
				
			
			$total_kel=$total_bb+$total_tp+$total_sfk;
			$total_msk=$total_tb+$total_kp+$total_rbb;
			$total_sf=$total_kel-$total_msk;
		
				
			$query14	= $this->db->query(" update tt_forecast_unit_jahit_detail set (sisa) =($total_sf) where id='$row3->id'  ");
			//var_dump($row3->id);
			//die;
				
			if($total_bb != 0){
			$per1=($total_tb/$total_bb)*100;
			}
			else
			$per1= 0;
			if($row3->jum_forecast != 0){
			$per2=($total_bb/$row3->jum_forecast)*100;
			}
			else
			$per2= 0;
			if($total_tp != 0){
			$per3=($total_kp/100)*$total_tp;
			}
			else
			$per3= 0;
		//var_dump($total_tb);
				
					$data_detail[]=array(
					'kode_brg'=>$kode_brg,
					'nama_brg'=>$nama_brg,
					'jum_forecast'=>$row3->jum_forecast,
					'total_bb'=>$total_bb,
					'total_sfk'=>$total_sfk,
					'total_kp'=>$total_kp,
					'total_tb'=>$total_tb,
					'total_tp'=>$total_tp,
					'total_rbb'=>$total_rbb,
					'total_kel'=>$total_kel,
					'total_msk'=>$total_msk,
					'total_sf'=>$total_sf,
					'per1'=>$per1,
					'per2'=>$per2,
					'per3'=>$per3,
				);
				}
				
				$query2	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row->id_unit' ");
				$hasilrow2 = $query2->row();
				$kode_unit	= $hasilrow2->kode_unit;
				$nama_unit	= $hasilrow2->nama;
				
				
				$data_header[]=array(
					'kode_unit'=>$kode_unit,
					'nama_unit'=>$nama_unit,
					'data_detail'=>$data_detail
				);
				$data_detail = array();
			}
			
			return	$data_header;		
		}
	}
	  function get_transaksi_unit_jahit($unit_jahit, $bulan, $tahun, $list_id_brg_wip) {		
	  // 14-01-2016 dikomen
	/*	$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		} */
		
		//~ $pisah1 = explode("-", $date_from);
		//~ $tgl1= $pisah1[0];
		//~ $bln1= $pisah1[1];
		//~ $thn1= $pisah1[2];
		//~ $tgldari = $thn1."-".$bln1."-".$tgl1;
		//~ 
		//~ if ($bln1 == 1) {
			//~ $bln_query = 12;
			//~ $thn_query = $thn1-1;
		//~ }
		//~ else {
			//~ $bln_query = $bln1-1;
			//~ $thn_query = $thn1;
			//~ if ($bln_query < 10)
				//~ $bln_query = "0".$bln_query;
		//~ }
		//~ $tgldariback=$thn_query."-".$bln_query."-".$tgl1;
	//~ 
		//~ $pisah1 = explode("-", $date_to);
		//~ $tgl2= $pisah1[0];
		//~ $bln2= $pisah1[1];
		//~ $thn2= $pisah1[2];
		//~ $tglke = $thn2."-".$bln2."-".$tgl2;
		//~ if ($bln1 == 1) {
			//~ $bln_query = 12;
			//~ $thn_query = $thn1-1;
		//~ }
		//~ else {
			//~ $bln_query = $bln1-1;
			//~ $thn_query = $thn1;
			//~ if ($bln_query < 10)
				//~ $bln_query = "0".$bln_query;
		//~ }
		//~ 
		//~ 
		//~ $timeStamp            =    mktime(0,0,0,$bln_query,1,$thn_query);    //Create time stamp of the first day from the give date.
		//~ $firstDay            =     date('d',$timeStamp);    //get first day of the given month
		//~ list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		//~ $lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		//~ $lastDay            =    date('t',$lastDayTimeStamp);// Find last day of the month
		//~ 
		//~ $tglkeback = $thn_query."-".$bln_query."-".$lastDay;
		//$tglke = $thn_query."-".$bln_query."-".$tgl2;
		//$tglkeback = date("Y-m-t", strtotime($tglke));
			//print_r($tglkeback);
			
				
		$data_brg_wip = array();
		$data_warna = array();
		$data_so_warna = array();
		$tot_saldo = 0;
		
		// ------------------------------ 12-01-2016 ---------------------------------------------------------------
		$id_brg_wip_exp = explode(";", $list_id_brg_wip);
		foreach($id_brg_wip_exp as $row1) {
			if ($row1 != '') {
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$row1' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_keluar_bgs'=> 0,
												'tot_keluar_perb'=> 0,
												'tot_masuk_bgs'=> 0,
												'tot_masuk_perb'=> 0,
												'tot_keluar_bb'=> 0,
												
											);
					}
				}
				
				// ambil SO dari tt_stok_opname_unit_jahit_detail
				$sqlso = " SELECT b.id, b.jum_forecast
							FROM tt_forecast_unit_jahit a INNER JOIN tt_forecast_unit_jahit_detail b ON a.id = b.id_forecast_unit_jahit
							WHERE a.bulan='$bulan' AND a.tahun='$tahun' 
							AND a.id_unit = '$unit_jahit' AND b.id_brg_wip = '$row1' ";
				$queryso	= $this->db->query($sqlso);
				if ($queryso->num_rows() > 0){
					$hasilso = $queryso->row();
					$id_sawal_detail = $hasilso->id;
					$jum_forecast = $hasilso->jum_forecast;
					
				}
				else {
					$jum_forecast = 0;
					$id_sawal_detail = 0;
				}
				
				// 04-02-2016 saldo awal per warna
				
				
				
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
				
				
				//edit 22/02/16
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				$sql2 = " SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, a.id_unit,
				 1 as masuk_bgs, 0 as masuk_perb, 0 as keluar_bgs, 0 as keluar_perb,0 as keluar_bb
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_keluar = '1' AND b.id_brg_wip='$row1' ";
				$sql2.= " AND thn_forecast=' ".$tahun."'  AND bln_forecast=' ".$bulan."'
						UNION
				SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit,
				 0 as masuk_bgs, 1 as masuk_perb, 0 as keluar_bgs, 0 as keluar_perb,0 as keluar_bb
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' AND b.id_brg_wip='$row1' ";
				$sql2.= " AND thn_forecast=' ".$tahun."'  AND bln_forecast=' ".$bulan."'		
						UNION
				SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit,
				 0 as masuk_bgs, 0 as masuk_perb, 1 as keluar_bgs, 0 as keluar_perb,0 as keluar_bb
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' AND b.id_brg_wip='$row1'";
				$sql2.= " AND thn_forecast=' ".$tahun."'  AND bln_forecast=' ".$bulan."'	
						UNION
				SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_unit_jahit as id_unit,
				 0 as masuk_bgs, 0 as masuk_perb, 0 as keluar_bgs, 1 as keluar_perb,0 as keluar_bb
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' AND b.id_brg_wip='$row1'";
				$sql2.= " AND thn_forecast=' ".$tahun."'  AND bln_forecast=' ".$bulan."'	
						UNION
				SELECT distinct a.id,  a.no_sj, a. tgl_sj, id_unit,
				 0 as masuk_bgs, 0 as masuk_perb, 0 as keluar_bgs, 0 as keluar_perb,1 as keluar_bb
						FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
						WHERE a.id_unit = '$unit_jahit' AND a.jenis_masuk = '2' AND b.id_brg_wip='$row1'";
				$sql2.= " AND thn_forecast=' ".$tahun."'  AND bln_forecast=' ".$bulan."'ORDER BY tgl_sj ASC	
						 "; //echo $sql2;
								
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$id_unit = $row2->id_unit;
						
						$masuk_bgs = $row2->masuk_bgs;
						$masuk_perb= $row2->masuk_perb;
						$keluar_bb = $row2->keluar_bb;
						$keluar_bgs = $row2->keluar_bgs;
						$keluar_perb= $row2->keluar_perb;
						
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						
						if ($id_unit != 0) {
							$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit' ");
							$hasilrow = $query3->row();
							$kode_unit	= $hasilrow->kode_unit;
							$nama_unit	= $hasilrow->nama;
						}
						else {
							$kode_unit = '';
							$nama_unit = '';
						}
						
						if ($masuk_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '1'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '3'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						// 15-01-2016
						if ($keluar_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '1'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_masuk = '2'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_bb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$row1' AND a.jenis_keluar = '2'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
// --------------------
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											// 05-12-2015
											$tot_saldo+= $row3->qty;
											
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											//echo "kesini"; die();
											// 05-12-2015
											$tot_saldo-= $row3->qty;
											
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_bgs == '1') {
											$data_warna[$xx]['tot_masuk_bgs']+= $row3->qty;
										}
										else if ($masuk_perb== '1') {
											$data_warna[$xx]['tot_masuk_perb']+= $row3->qty;
										}
										else if ($keluar_bgs== '1') {
											$data_warna[$xx]['tot_keluar_bgs']+= $row3->qty;
										}
										else if ($keluar_perb== '1') {
											$data_warna[$xx]['tot_keluar_perb']+= $row3->qty;
										}
										else if ($keluar_bb == '1') {
											$data_warna[$xx]['tot_keluar_bb']+= $row3->qty;
										}
										
									}
								}
								
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
					
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'id_unit'=> $id_unit,
												'kode_unit'=> $kode_unit,
												'nama_unit'=> $nama_unit,
												'keluar_bgs'=> $keluar_bgs,
												'keluar_perb'=> $keluar_perb,
												'masuk_bgs'=> $masuk_bgs,
												'masuk_perb'=> $masuk_perb,
												'keluar_bb'=> $keluar_bb,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												// 05-12-2015
												'tot_saldo'=> $tot_saldo
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end row2
				}
				else
					$data_tabel1='';
					
				
				// -----------------------------------------------------------------------------------------------------------------
				
			
				// =================================== END NEW =======================================================
				
				// array brg wip
				$data_brg_wip[] = array(		'id_brg_wip'=> $row1,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,	
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna
												
									);
				$data_tabel1 = array();
				$data_tabel7 = array();
				$data_warna = array();
				$data_so_warna = array();
				
			} // END IF ROW1
		} // END FOREACH
		//-----------------------------------------------------------------------------------
		
			//} // endforeach header
		//}
		//else {
			//$data_brg_jadi = '';
		//}
		return $data_brg_wip;
  }
  
}
