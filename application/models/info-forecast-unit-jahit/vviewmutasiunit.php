<style type="text/css">
  table {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;}
	
	.fieldsetdemo {
		background-color:#DDD;
		max-width:500px;
		padding:14px;
	}
	.judulnya {
		background-color:#DDD;
		font-size: 12px;
	}
</style>

<h3>Laporan Forecast Barang WIP di Unit Jahit</h3><br>

<div>

Unit Jahit: <?php if ($unit_jahit!= 0) { echo $kode_unit."-".$nama_unit; } else echo "Semua"; ?><br>
Periode: <?php echo $bulan." - ". $tahun ?> <br><br>

<?php 
$attributes = array('name' => 'f_stok', 'id' => 'f_stok');
echo form_open('info-forecast-unit-jahit/creport/export_excel_funit', $attributes); ?>
<input type="hidden" name="bulan" value="<?php echo $bulan ?>" >
<input type="hidden" name="tahun" value="<?php echo $tahun ?>" >
<input type="hidden" name="unit_jahit" value="<?php echo $unit_jahit ?>" >
<input type="hidden" name="kode_unit" value="<?php echo $kode_unit ?>" >
<input type="hidden" name="nama_unit" value="<?php echo $nama_unit ?>" >

<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel (Khusus Window)">
<input type="submit" name="export_ods" id="export_ods" value="Export ke ODS (Khusus Linux)">
<?php echo form_close();  ?>
<br>
<?php
	if (is_array($query)) {
		for($a=0;$a<count($query);$a++){
			echo "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b>"."<br>";
?>
	<table border="1" cellpadding= "1" cellspacing = "1" width="100%">
	<thead>
	 <tr class="judulnya">
		 <th width='1%' rowspan='2'>No</th>
		 <th width='4%'rowspan='2'>Kode</th>
		 <th width='16%'rowspan='2'>Nama Brg WIP</th>
		 <th width='2%' rowspan='2'>Forecast (FC)</th>
		<th colspan='4'>Kirim</th>
		<th colspan='4'>Terima</th>
		<th rowspan="2">Sisa Forecast</th>
		<th colspan='3'>Persentase</th>
		
		
	 </tr>
	 <tr class="judulnya">
		 <th width='7%'>Bahan Baku (BB)</th>
		 <th width='7%'>Sisa Bulan Sebelumnya</th>
		 <th width='7%'>Perbaikan (KP)</th>
		 <th width='7%'>Total</th>
		 <th width='7%'>Barang Jadi (BJ)</th>
		 <th width='7%'>Perbaikan (TP)</th>
		 <th width='7%'>Retur Bahan Baku</th>
		 <th width='7%'>Total</th>
		 <th width='7%'>BJ / BB (%)</th>
		 <th width='7%'>BB / FC (%)</th>
		 <th width='7%'>KP / TP (%)</th>
	 </tr>
	</thead>
	<tbody>
		<?php 
	
		$detail_fc= $query[$a]['data_detail'];
		if (is_array($detail_fc)) {
				for($j=0;$j<count($detail_fc);$j++){
					?>
					<td align="center"><?php echo ($j+1) ?></td>
					<td>&nbsp;<?php echo $detail_fc[$j]['kode_brg'] ?></td>
					<td>&nbsp;<?php echo $detail_fc[$j]['nama_brg'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['jum_forecast'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_bb'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_sfk'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_kp'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_kel'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_tb'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_tp'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_rbb'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_msk'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['total_sf'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['per1'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['per2'] ?></td>
					<td align ='center'>&nbsp;<?php echo $detail_fc[$j]['per3'] ?></td>
					<?php					
					}
				}
		?>
		
	</tbody>
	</table><br><br>		

</div>
	<?php
	}
}
	
	?>
