<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacakas($periode)
	{
/*
		$query=$this->db->query("select 'Kas & Bank' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								             from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '111%'
								             and a.i_coa=b.i_coa",false);
*/
    $bcajkt=BankPinjaman;
	  $query=$this->db->query(" select 'Kas & Bank' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                            sum(b.kredit) as kredit, 
                              (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                              select 0 as v_saldo_awal, sum(a.debet) as debet, sum(a.kredit) as kredit from (
                              select 0 as debet, sum(jumlah) as kredit from tt_kas_besar where d_periode='$periode' and f_cancel_kb='f' 
                              and is_debet='t'
                              union all
                              select sum(jumlah) as debet, 0 as kredit from tt_kas_besar where d_periode='$periode' and f_cancel_kb='f' 
                              and is_debet='f'
                              union all
                              select 0 as debet, sum(jumlah) as kredit from tt_kas_kecil where d_periode='$periode' and f_cancel_kk='f' 
                              and is_debet='t'
                              union all
                              select sum(jumlah) as debet, 0 as kredit from tt_kas_kecil where d_periode='$periode' and f_cancel_kk='f' 
                              and is_debet='f'
                              union all
                              select 0 as debet, sum(jumlah) as kredit from tt_kas_bank where d_periode='$periode' and f_cancel_bk='f' 
                              and is_debet='t' and kode_coa<>'$bcajkt'
                              union all
                              select sum(jumlah) as debet, 0 as kredit from tt_kas_bank where d_periode='$periode' and f_cancel_bk='f' 
                              and is_debet='f' and kode_coa<>'$bcajkt') as a
                              union all
                              select sum(b.saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit
                              from tm_coa a, tt_saldo_akun b where i_periode = '$periode' and i_coa_group='1' and a.kode like '111%' 
                              and  b.kode_coa<>'$bcajkt' and a.id=b.id_coa) as b",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacapiutang($periode)
	{$db2=$this->load->database('db_external', TRUE);
/*
		$query=$this->db->query("select 'Piutang Dagang' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '112.2%' 
								 and a.i_coa=b.i_coa",false);
*/
		$query=$db2->query("select 'Piutang Dagang' as ket, sum(x.v_grand_sisa) as v_saldo_akhir
								             FROM (
		SELECT i_faktur, v_grand_sisa,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, v_grand_sisa,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, v_grand_sisa,d_faktur FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		) x where to_char(d_faktur,'yyyymm') = '$periode'  and not i_faktur isnull",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutanggiromundur($periode)
	{
/*
		$query=$this->db->query("select 'Piutang Giro Mundur' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '112.1%' 
								 and a.i_coa=b.i_coa",false);
*/
		$query=$this->db->query("select 'Piutang Giro Mundur' as ket, sum(v_jumlah) as v_saldo_akhir
            								 from tm_giro where d_giro_cair isnull and v_jumlah=v_sisa and f_giro_batal='f' and f_giro_tolak='f'
            								 and to_char(d_giro,'yyyymm')<='$periode'",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapossementaradebet($periode)
	{
		$query=$this->db->query("select 'Pos Sementara Debet' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '119.100%' 
								 and a.id=b.id_coa",false);
		
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutangkaryawan($periode)
	{
		$query=$this->db->query("select 'Piutang Karyawan' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '112.4%' 
								 and a.id=b.id_coa",false);
	
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaaktivatetapkel1($periode)
	{
		$query=$this->db->query("select 'Aktiva Tetap Kelompok I' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '121.100%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumaktiva1($periode)
	{
		$query=$this->db->query("select 'Akum. Peny. Aktiva Tetap Kel. I' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '121.900%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaaktivatetapkel2($periode)
	{
		$query=$this->db->query("select 'Aktiva Tetap Kelompok II' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '122.100%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumaktiva2($periode)
	{
		$query=$this->db->query("select 'Akum. Peny. Aktiva Tetap Kel. II' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '121.900%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumamortisasi($periode)
	{
		$query=$this->db->query("select 'Akum. Amortisasi B. Sewa' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '131.900%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacabiayayangditangguhkan($periode)
	{
		$query=$this->db->query("select 'Biaya Yang Ditangguhkan' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '132.100%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumamortisasiyangditangguhkan($periode)
	{
		$query=$this->db->query("select 'Akum. Amortisasi Biaya Yang Ditangguhkan' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '132.900%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangbank($periode)
	{
/*
		$query=$this->db->query("select 'Hutang Bank' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '221.%' 
								 and a.i_coa=b.i_coa",false);
*/
    $bcajkt=BankPinjaman;
	  $query=$this->db->query(" select 'Hutang Bank' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                            sum(b.kredit) as kredit, 
                              ((sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit)) * -1 as v_saldo_akhir from (
                              select 0 as v_saldo_awal, sum(a.debet) as debet, sum(a.kredit) as kredit from (
                              select 0 as debet, sum(jumlah) as kredit from tt_kas_bank where d_periode='$periode' and f_cancel_bk='f' 
                              and is_debet='t' and kode_coa='$bcajkt'
                              union all
                              select sum(jumlah) as debet, 0 as kredit from tt_kas_bank where d_periode='$periode' and f_cancel_bk='f' 
                              and is_debet='f' and kode_coa='$bcajkt') as a
                              union all
                              select sum(b.saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit
                              from tm_coa a, tt_saldo_akun b where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '111%' 
                              and  b.kode_coa='$bcajkt' and a.id=b.id_coa) as b",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutanglainlain($periode)
	{
			$query=$this->db->query("select 'Piutang Lain-Lain' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '112.3%' 
								 and a.id=b.id_coa",false);
		
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacabiayadibayardimuka($periode)
	{
		$query=$this->db->query("select 'Biaya Yang Dibayar Dimuka' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '114.2%' 
								 and a.id=b.id_coa",false);
	
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph21ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 21 YDM' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '114.301%' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph22ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 22 YDM' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '114.302%' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph23ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 23,26,4,2 YDM' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '114.303%' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph25ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 25/29 YDM' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '114.304%' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapersediaanbarangdagang($periode)
	{
		$query=$this->db->query("select 'Persediaan Barang Dagangan' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='1' and b.kode_coa like '113.%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangdagang($periode)
	{
/*
		$query=$this->db->query("select 'Hutang Dagang' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='211.100%' 
								 and a.i_coa=b.i_coa",false);
*/
		$query=$this->db->query("select 'Hutang Dagang' as ket, sum(total) as v_saldo_akhir
								             from tm_pembelian where to_char(tgl_sj,'yyyymm') = '$periode' ",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacayhmdibayar($periode)
	{
		$query=$this->db->query("select 'Biaya YHM Dibayar' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='213.100%' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph21($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 21 ' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='2' and b.kode_coa like '213.201%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph22($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 22 ' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='2' and b.kode_coa like '213.202%' 
								 and a.id=b.id_coa",false);
		
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph23($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 23/26,4(2) ' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='2' and b.kode_coa like '213.203%' 
								 and a.id=b.id_coa",false);
		
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph24($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 25/29 ' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='2' and b.kode_coa like '213.204%' 
								 and a.id=b.id_coa",false);
		
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakppn($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak Pertambahan Nilai (PPN)' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='2' and b.kode_coa like '213.205%' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutanglain($periode)
	{
		$query=$this->db->query("select 'Hutang Lain Lain' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='2' and b.kode_coa like '214.109%' 
								 and a.id=b.id_coa",false);
	
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacauangmukaleasing($periode)
	{
		$query=$this->db->query("select 'Uang Muka Leasing' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and a.kode='114.500' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapossementara($periode)
	{
		$query=$this->db->query("select 'Pos Sementara Kredit' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and a.kode='214.109' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangbankjangkapanjang($periode)
	{
		$query=$this->db->query("select 'Hutang Bank Jk. Panjang' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and a.kode='221.100' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacamodalyangdisetor($periode)
	{
		$query=$this->db->query("select 'Modal Yang Disetor' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and a.kode='311.100' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahan($periode)
	{
		$query=$this->db->query("select 'Laba/Rugi Yang Ditahan' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and a.kode='312.100' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahantahunberjalan($periode)
	{
			$query=$this->db->query("select 'Laba/Rugi Yang Ditahan Tahun Berjalan' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and a.kode='312.200' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahanbulanberjalan($periode)
	{
		$query=$this->db->query("select 'Laba/Rugi Yang Ditahan Bulan Berjalan' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and a.kode='312.300' 
								 and a.id=b.id_coa",false);
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacamodal($periode)
	{
		$query=$this->db->query("select 'Modal' as ket, sum(b.saldo_akhir) as v_saldo_akhir
								  from tm_coa a, tt_saldo_akun b  where i_periode = '$periode' and i_coa_group='3' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa where upper(i_coa) like '%$cari%' or upper(e_coa_name) like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function saldoawal($periode,$icoa)
    {
		$this->db->select("	v_saldo_awal from tm_coa_saldo
							where i_periode = '$periode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s":
		        $sec += $number;
		        break;
		    case "n":
		        $min += $number;
		        break;
		    case "h":
		        $hr += $number;
		        break;
		    case "d":
		        $day += $number;
		        break;
		    case "ww":
		        $day += ($number * 7);
		        break;
		    case "m": 
		        $mon += $number;
		        break;
		    case "yyyy": 
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function NamaBulan($bln){
		switch($bln){
			case "01" 	:
				$NMbln = "Januari";
				break;
			case "02" 	:
				$NMbln = "Februari";
				break;
			case "03" 	:
				$NMbln = "Maret";
				break;
			case "04" 	:
				$NMbln = "April";
				break;
			case "05" 	:
				$NMbln = "Mei";
				break;
			case "06" 	:
				$NMbln = "Juni";
				break;
			case "07" 	:
				$NMbln = "Juli";
				break;
			case "08" 	:
				$NMbln = "Agustus";
				break;
			case "09" 	:
				$NMbln = "September";
				break;
			case "10" 	:
				$NMbln = "Oktober";
				break;
			case "11" 	:
				$NMbln = "November";
				break;
			case "12"  	:
				$NMbln = "Desember";
				break;
		}
		return ($NMbln);
	}
}
?>
