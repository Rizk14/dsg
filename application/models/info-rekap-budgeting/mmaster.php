<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
function get_jenis_barang(){
	$query	= $this->db->query("select * from tm_jenis_barang ORDER BY kode ");    
    return $query->result();  
  }
function get_kelompok_barang(){
	$query	= $this->db->query("select * from tm_kelompok_barang ORDER BY kode ");    
    return $query->result();  
  }
function get_supplier(){
    $query = $this->db->query(" SELECT id, kode_supplier, nama from tm_supplier ORDER BY kode_supplier");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
			
function get_budgeting($date_from, $date_to, $supplier,$jns_data) {
	$pencarian ="";$pencarian2 ="";$pencarian3 ="";
	$dari =	explode('-',$date_from);
	$tanggal1 =$dari[0];
	$bulan1 =$dari[1];
	$tahun1 =$dari[2];
	$exdari =$tahun1."-".$bulan1."-".$tanggal1;
	
	$sampai =	explode('-',$date_to);
	$tanggal2 =$sampai[0];
	$bulan2 =$sampai[1];
	$tahun2 =$sampai[2];
	$exsampai =$tahun2."-".$bulan2."-".$tanggal2;
		
		if($supplier != 0){
			$pencarian .= "AND id = '$supplier'";
			}
		if($jns_data != 0){
			$pencarian2 .= "AND b.jenis_pembelian = '$jns_data'";
			}
		/* if($jns_data != 0){
			$pencarian3 .= "AND d.jenis_pembelian = '$jns_data'";
			}			
		$query	= $this->db->query(" SELECT * from tm_supplier WHERE TRUE ".$pencarian." order by kode_supplier "); */


  $query = $this->db->query("SELECT distinct a.id_supplier , b.nama as nama_supplier, b.kode_supplier 
							 FROM tm_op a left join tm_supplier b on (a.id_supplier=b.id) WHERE a.tgl_op >= '$exdari' AND a.tgl_op <= '$exsampai' 
							 order by id_supplier ".$pencarian2."  ");		
		$data_budgeting = array();
		$detail_budgeting = array();
		$total_permintaan	=0;
		$total_pemenuhan	=0;
		
		if ($query->num_rows() > 0){
			$hasil1 = $query->result();
			foreach ($hasil1 as $row1) {
				$nama_supplier= $row1->nama_supplier;
				if($row1->nama_supplier==''){
					$nama_supplier = 'Lain -Lain';
				}
				$query5	= $this->db->query( " SELECT sum(a.qty*a.harga) as total_permintaan FROM tm_op_detail a inner join tm_op b ON a.id_op=b.id 
				WHERE b.id_supplier = '$row1->id_supplier' AND b.tgl_op >='$exdari'AND b.tgl_op <='$exsampai' ".$pencarian2." 
				");
				if ($query5->num_rows() > 0){
					$hasil5=$query5->row();
					$total_permintaan=	$hasil5->total_permintaan;
				}
				else{
				$total_permintaan=	0;
				if($total_permintaan=='')
				$total_permintaan = 0;
			}

						//$exdarinext = date('Y-m-d',strtotime($exdari . "first day of next month"));
						//$exsampainext = date('Y-m-d',strtotime($exdari . "last day of next month"));
						$exdarinext = date('d-m-Y',strtotime($exdari . "first day of next month"));
						$exsampainext = date('d-m-Y',strtotime($exdari . "last day of next month"));

/*
						$query3	= $this->db->query("
								SELECT sum(a.qty * a.harga) as total_pemenuhan 
								FROM tm_pembelian_detail a 
								INNER JOIN tm_pembelian b ON b.id=a.id_pembelian
								INNER JOIN tm_op_detail c ON c.id=a.id_op_detail
								inner join tm_op d ON c.id_op=d.id 
								WHERE 
								d.id_supplier = '$row1->id_supplier' AND b.tgl_sj >='$exdarinext'AND b.tgl_sj <='$exsampainext'
								");

*/


$query3	= $this->db->query("
SELECT sum(a.total_pemenuhan) as total_pemenuhan from
(
SELECT  b.no_op, b.tgl_op, d.tgl_sj, c.total as total_pemenuhan  
FROM tm_op_detail a 
INNER JOIN tm_op b ON a.id_op=b.id
INNER JOIN tm_pembelian_detail c ON a.id=c.id_op_detail
INNER JOIN tm_pembelian d ON d.id=c.id_pembelian
WHERE b.id_supplier = '$row1->id_supplier' 
AND b.tgl_op >= '$exdari'
AND b.tgl_op <= '$exsampai'
AND d.status_aktif = 't'
) a ");
/*
						$query3	= $this->db->query("
						SELECT sum(b.total) as total_pemenuhan FROM tm_pembelian a 
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						INNER JOIN tm_barang g ON b.id_brg = g.id 
						INNER JOIN tm_jenis_barang f ON f.id = g.id_jenis_barang 
						WHERE a.id_supplier = '$row1->id_supplier' 
						AND a.tgl_sj >= to_date('$exdarinext','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$exsampainext','dd-mm-yyyy') 
						AND a.status_aktif = 't' ");
*/
						if($query3->num_rows()>0) {
						$hasilrow3 = $query3->row();
						$total_pemenuhan = $hasilrow3->total_pemenuhan;
					}else{
						$total_pemenuhan = 0;
						if($total_pemenuhan=='')
							$total_pemenuhan = 0;
					}
				
				if($total_permintaan != 0 ){
				$data_budgeting[] = array(
					'nama_supplier' => $nama_supplier,
					'kode_supplier' => $row1->kode_supplier,
					'total_pemenuhan' => $total_pemenuhan,
					'total_permintaan' => $total_permintaan,
					);
					}
				}
			} // endforeach header
		
		
		return $data_budgeting;
  }			
				
 
}

