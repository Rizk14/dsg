<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
#	function bacanilai($dfrom,$dto)
	function bacanilai($iperiode)
	{
		$tahun= substr($iperiode,0,4);
		$bulan= substr($iperiode,4,2);
		$this->db->select("	 a.* from (
                          select 		
                          substring(b.kode,1,5)||'00' as i_coa, b.f_coa_status, sum(a.saldo_awal) as v_saldo_awal, 
                          sum(a.debet) as v_mutasi_debet, sum(a.kredit) as v_mutasi_kredit, 
                          sum(a.saldo_akhir) as v_saldo_akhir
                          from tt_saldo_akun a, tm_coa b
                          where bulan = '$bulan' and tahun=$tahun and a.id_coa=b.id and substring(b.kode,1,5) !='111.4'
                          group by substring(b.kode,1,5), b.f_coa_status

                          union all

                          select b.kode as i_coa, b.f_coa_status, 
                          a.saldo_awal as v_saldo_awal, a.debet as v_mutasi_debet, a.kredit as v_mutasi_kredit, 
                          a.saldo_akhir as v_saldo_akhir from tt_saldo_akun a, tm_coa b
                          where  bulan = '$bulan' and tahun=$tahun and a.id_coa=b.id
                          and substring(b.kode,1,5)='111.4'
                          ) as a order by i_coa",false);
/*
		$this->db->select("	i_coa, e_coa_name, sum(v_mutasi_debet) as v_mutasi_debet, sum(v_mutasi_kredit) as v_mutasi_kredit
							          from tm_general_ledger 
							          where d_mutasi >= '$dfrom' and d_mutasi <= '$dto' group by i_coa, e_coa_name order by i_coa ",false);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacaperiode($periode)
    {
		$query=$this->db->query("	select i_coa from tr_coa order by i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa where upper(i_coa) like '%$cari%' or e_coa_name like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function saldoawal($periode,$icoa)
    {
		$this->db->select("	v_saldo_awal from tm_coa_saldo
							where i_periode = '$periode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function NamaBulan($bln){
		switch($bln){
			case "01" 	:
				$NMbln = "Januari";
				break;
			case "02" 	:
				$NMbln = "Februari";
				break;
			case "03" 	:
				$NMbln = "Maret";
				break;
			case "04" 	:
				$NMbln = "April";
				break;
			case "05" 	:
				$NMbln = "Mei";
				break;
			case "06" 	:
				$NMbln = "Juni";
				break;
			case "07" 	:
				$NMbln = "Juli";
				break;
			case "08" 	:
				$NMbln = "Agustus";
				break;
			case "09" 	:
				$NMbln = "September";
				break;
			case "10" 	:
				$NMbln = "Oktober";
				break;
			case "11" 	:
				$NMbln = "November";
				break;
			case "12"  	:
				$NMbln = "Desember";
				break;
		}
		return ($NMbln);
	}
}
?>
