<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAllopbisbisan($num, $offset, $supplier, $cari) {
	  $sql = " a.* FROM tm_op_makloon a WHERE a.jenis_makloon='1' ";
	  if ($supplier != 'xx')
		$sql.= " AND a.id_supplier = '$supplier' ";
	  if ($cari != "all")
		$sql.= " AND UPPER(a.no_op) like UPPER('%".$this->db->escape_str($cari)."%')  ";
	  $sql.= " ORDER BY a.tgl_op DESC, no_op DESC ";
	  $this->db->select($sql, false)->limit($num,$offset);
	  $query = $this->db->get();
	  
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$no_bonmkeluarnya = ""; $list_no_bonmkeluarnya = "";
				$id_detailnya = ""; 
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_op_makloon_detail WHERE id_op_makloon = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// 18-08-2015 jenis potong dan ukuran bisbisan
						if ($row2->jenis_potong == '1')
							$jenis_potong = "Potong Serong";
						else if ($row2->jenis_potong == '2')
							$jenis_potong = "Potong Lurus";
						else
							$jenis_potong = "Potong Spiral";
						
						$query3	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id = '$row2->id_ukuran_bisbisan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_ukuran_bisbisan	= $hasilrow->nama;
						}
						else
							$nama_ukuran_bisbisan = '';
						// -----------------------------------------------------------
						
						$id_detailnya.= $row2->id_bonmkeluar_detail."-";
													
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.nama_brg_supplier, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$nama_brg_sup	= $hasilrow->nama_brg_supplier;
							$satuan	= $hasilrow->nama_satuan;
							
							if ($nama_brg_sup != '')
								$nama_brg = $nama_brg_sup;
						}
						else {
							$nama_brg	= '';
							$nama_brg_sup = '';
							$satuan	= '';
						}
												
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail WHERE id_op_makloon_detail = '".$row2->id."' ");
						if ($query4->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty_konversi) as jum FROM tm_pembelian_makloon_detail a 
											INNER JOIN tm_pembelian_makloon b ON a.id_pembelian_makloon = b.id
											WHERE a.id_op_makloon_detail = '$row2->id' 
											AND b.status_aktif = 't'
											AND a.id_brg = '$row2->id_brg' ");
							
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_makloon_detail berdasarkan kode brg tsb
						}//cek didieu 240212
						else {
							$jum_beli = 0;
						}
						
						$sisanya = $row2->qty-$jum_beli;
						if ($sisanya < 0)
							$sisanya = 0;
						
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail WHERE id_op_makloon_detail = '".$row2->id."' ");
						if ($query4->num_rows() > 0){
							$query3	= $this->db->query(" SELECT count(b.id) as jum FROM tm_pembelian_makloon_detail a INNER JOIN tm_pembelian_makloon b
									ON a.id_pembelian_makloon = b.id WHERE b.status_aktif = 't'
									AND a.id_op_makloon_detail = '$row2->id' 
									AND a.id_brg = '$row2->id_brg' ");
							$hasilrow = $query3->row();
							$count_beli = $hasilrow->jum;
						}
						else {
							$count_beli = 0;
						}
												
						$sqlxx = " SELECT * FROM tm_op_makloon_detail where id_bonmkeluar_detail = '$row2->id_bonmkeluar_detail' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							//ambil nomor bon M Keluar
							$query3	= $this->db->query(" SELECT a.no_manual FROM tm_bonmkeluar_detail b INNER JOIN tm_bonmkeluar a ON a.id=b.id_bonmkeluar 
												WHERE b.id = '$row2->id_bonmkeluar_detail' ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$no_bonmkeluar	= $hasilrow->no_manual;
							 }
							 else
								$no_bonmkeluar = '';
							if ($no_bonmkeluarnya != $no_bonmkeluar) {
								$list_no_bonmkeluarnya.= $no_bonmkeluar.";";
							}
							$no_bonmkeluarnya = $no_bonmkeluar;
						}
						
						$detail_fb[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'status_op'=> $row2->status_op,
												'jum_beli'=> $jum_beli,
												'count_beli'=> $count_beli,
												'sisanya'=> $sisanya,
												'jenis_potong'=> $jenis_potong,
												'nama_ukuran_bisbisan'=> $nama_ukuran_bisbisan
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				if ($row1->id_supplier != '0') {
					$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else {
					$kode_supplier	= '';
					$nama_supplier	= '';
				}
				 
				 // berdasarkan id_op_makloon, cek jumlah item brg yg status_op = 'f'. jika > 0, maka munculkan link edit per OP
				  
				 $query3	= $this->db->query(" SELECT count(id) as jum_item FROM tm_op_makloon_detail 
								WHERE id_op_makloon = '$row1->id' AND status_op = 'f' ");
				 if ($query3->num_rows() > 0) {
					$hasilrow = $query3->row();
					$jum_item_f	= $hasilrow->jum_item;
				 }
				 else
					$jum_item_f = 0;
			
				$sql = " SELECT sum(a.qty_satawal) as jum FROM tm_pembelian_makloon_detail a INNER JOIN tm_pembelian_makloon b ON a.id_pembelian_makloon = b.id
						INNER JOIN tm_op_makloon_detail d ON a.id_op_makloon_detail = d.id 
						INNER JOIN tm_op_makloon c ON c.id = d.id_op_makloon 
						WHERE b.status_aktif = 't'
						AND b.id_supplier = '$row1->id_supplier' AND c.id = '$row1->id' ";
								
				$query3	= $this->db->query($sql);
				$hasilrow = $query3->row();
				$jum_beli2 = $hasilrow->jum;
																 
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_bonmkeluar'=> $list_no_bonmkeluarnya,	
											'no_op'=> $row1->no_op,
											'tgl_op'=> $row1->tgl_op,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_op'=> $row1->status_op,
											'status_aktif'=> $row1->status_aktif,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											'jum_item_f'=> $jum_item_f,
											'jum_beli2'=> $jum_beli2,
											'jenis_pembelian'=> $row1->jenis_pembelian
											);
				$detail_fb = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAllopbisbisantanpalimit($supplier, $cari){	
	$sql = " SELECT a.* FROM tm_op_makloon a WHERE a.jenis_makloon='1' ";
	  if ($supplier != 'xx')
		$sql.= " AND a.id_supplier = '$supplier' ";
	  if ($cari != "all")
		$sql.= " AND UPPER(a.no_op) like UPPER('%".$this->db->escape_str($cari)."%')  ";
    $query	= $this->db->query($sql);
    return $query->result();  
  }
  
  // 15-08-2015
  function get_ukuran_bisbisan(){
	$query	= $this->db->query(" SELECT id, nama FROM tm_ukuran_bisbisan ORDER BY id ");    
    return $query->result();  
  }  
      
  function cek_data_opbisbisan($no_op){
    $this->db->select("id from tm_op_makloon WHERE no_op = '$no_op' AND jenis_makloon='1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 15-08-2015
  function saveopbisbisan($no_op,$tgl_op, $id_supplier,$jenis_pembelian, $ket,
				$id_bonmkeluar_detail, $id_brg, $nama, $qty, $qty_satawal, $jenis_potong, $id_ukuran_bisbisan,
				$keterangan){  
	
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
        
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_op_makloon WHERE no_op = '$no_op' AND jenis_makloon = '1' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_op
			$data_header = array(
			  'no_op'=>$no_op,
			  'tgl_op'=>$tgl_op,
			  'id_supplier'=>$id_supplier,
			  'jenis_pembelian'=>$jenis_pembelian,
			  'keterangan'=>$ket,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'uid_update_by'=>$uid_update_by
			);
			$this->db->insert('tm_op_makloon',$data_header);
		}
			
			// ambil data terakhir di tabel tm_op_makloon
			$query2	= $this->db->query(" SELECT id FROM tm_op_makloon ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_op_makloon	= $hasilrow->id;
			
			if ($id_brg!='' && $qty!='') {
				//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di bonmkeluar
				$query3	= $this->db->query(" SELECT qty FROM tm_bonmkeluar_detail WHERE id = '$id_bonmkeluar_detail' ");
				$hasilrow = $query3->row();
				$qty_bonm = $hasilrow->qty;
				
				$sqlxx = " SELECT * FROM tm_op_makloon_detail where id_bonmkeluar_detail = '$id_bonmkeluar_detail' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_makloon_detail a 
							INNER JOIN tm_op_makloon b ON a.id_op_makloon = b.id
							WHERE a.id_bonmkeluar_detail = '$id_bonmkeluar_detail' ");
					$hasilrow = $query3->row();
					$jum_op = $hasilrow->jum;
				}
				else {
					$jum_op = 0;
				}
								
				$jumtot = $jum_op+$qty;
				if ($jumtot > $qty_bonm) {
					$qty = $qty_bonm-$jum_op;
					
					// hitung qty_satawal pake rumus konversi
					$query2	= $this->db->query(" SELECT id_satuan_konversi, rumus_konversi, 
										angka_faktor_konversi FROM tm_barang WHERE id = '$id_brg' ");
					$hasilrow = $query2->row();
					$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
					$rumus_konversi = $hasilrow->rumus_konversi;
					$angka_faktor_konversi = $hasilrow->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {						
						if ($rumus_konversi == '1') {
								$qty_satawal = $qty/$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '2') {
								$qty_satawal = $qty*$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '3') {
								$qty_satawal = $qty-$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '4') {
								$qty_satawal = $qty+$angka_faktor_konversi;
							}
							else
								$qty_satawal = $qty;
					}
					else {
						$qty_satawal = $qty;
					}
				} // end if jumtot
				
				// jika semua data tdk kosong, insert ke tm_op_makloon_detail
				$data_detail = array(
					'id_brg'=>$id_brg,
					'qty'=>$qty,
					'qty_satawal'=>$qty_satawal,
					'jenis_potong'=>$jenis_potong,
					'id_ukuran_bisbisan'=>$id_ukuran_bisbisan,
					'keterangan'=>$keterangan,
					'id_op_makloon'=>$id_op_makloon,
					'id_bonmkeluar_detail'=>$id_bonmkeluar_detail
				);
				$this->db->insert('tm_op_makloon_detail',$data_detail);
				
			}
				// cek sum qty di OP. jika sum di OP = qty bonmkeluar, maka update status_op_bisbisannya menjadi true
				$query3	= $this->db->query(" SELECT qty FROM tm_bonmkeluar_detail WHERE id= '$id_bonmkeluar_detail' ");
				$hasilrow = $query3->row();
				$qty_bonm = $hasilrow->qty; // ini qty di bonmkeluar_detail
				
				$sqlxx = " SELECT * FROM tm_op_makloon_detail where id_bonmkeluar_detail = '$id_bonmkeluar_detail' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_makloon_detail a 
							INNER JOIN tm_op_makloon b ON a.id_op_makloon = b.id
							WHERE a.id_bonmkeluar_detail = '$id_bonmkeluar_detail' ");
					$hasilrow = $query3->row();
					$jum_op = $hasilrow->jum;
				}
				else {
					$jum_op = 0;
				}
				
				if ($qty_bonm <= $jum_op) {
					$this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 't' where id= '$id_bonmkeluar_detail' ");
				}
  }
    
  // 18-08-2015
  function deleteopbisbisan($id, $list_brg){    
	if ($list_brg != '') {
		$id_brg = explode("-", $list_brg);
					
		//update status_op_bisbisan = 'f' di tm_bonmkeluar_detail, ini peritem sesuai di OP tsb
		foreach($id_brg as $row1) {
			if ($row1 != '') {
				$this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 'f' where id= '$row1' ");
			}
		}
	} // end if id_bonmkeluar_detail
	
	$this->db->delete('tm_op_makloon_detail', array('id_op_makloon' => $id));
	$this->db->delete('tm_op_makloon', array('id' => $id));
  }
  
  function updatestatusopbisbisan($id, $aksi){    
	  if ($aksi == "on")
		$this->db->query(" UPDATE tm_op_makloon SET status_aktif = 't' where id= '$id' ");
	  else
		$this->db->query(" UPDATE tm_op_makloon SET status_aktif = 'f' where id= '$id' ");
  }
  
  function get_bonmkeluar($cari) {
	  // ambil data bonmkeluar yg untuk_bisbisan = 't' dan status_op_bisbisan = 'f'
	if ($cari == "all") {		
		$this->db->select(" distinct a.id, a.no_manual, a.tgl_bonm, a.id_gudang, a.tgl_update FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE b.untuk_bisbisan = 't' AND b.status_op_bisbisan = 'f' order by a.tgl_bonm DESC, a.id DESC ", false);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" distinct a.id, a.no_manual, a.tgl_bonm, a.id_gudang, a.tgl_update FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE b.untuk_bisbisan = 't' AND b.status_op_bisbisan = 'f' 
							AND UPPER(no_manual) like UPPER('%$cari%') order by a.tgl_bonm DESC, a.id DESC ", false);
		$query = $this->db->get();
	}
		$data_bonmkeluar = array();
		$detail_bonmkeluar = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
							
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$row1->id' 
										AND untuk_bisbisan = 't' AND status_op_bisbisan = 'f' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.satuan, a.id_satuan_konversi,  
										b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						//b.nama as nama_satuan_konv FROM tm_barang a
						//				LEFT JOIN tm_satuan b ON a.id_satuan_konversi = b.id
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->satuan;
						$nama_satuan	= $hasilrow->nama_satuan;
						$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
						
						if ($id_satuan_konversi != 0) {
							$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan
											WHERE id = '$id_satuan_konversi' ");
							$hasilrow = $query3->row();
							$nama_satuan	= $hasilrow->nama_satuan;
						}

						$sqlxx = " SELECT * FROM tm_op_makloon_detail where id_bonmkeluar_detail = '$row2->id' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_makloon_detail a INNER JOIN tm_op_makloon b 
									ON a.id_op_makloon = b.id WHERE a.id_bonmkeluar_detail = '$row2->id' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan id_bonmkeluar_detail
						}
						else {
							$jum_op = 0;
						}
						
						$qty = $row2->qty-$jum_op;
						
						// 13-08-2015
						if ($qty <= 0) {
							$this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 't' WHERE id='".$row2->id."' ");
						}
						//--------------------
						
						if ($qty > 0) {
						$detail_bonmkeluar[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $nama_satuan,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan,
												'no_bonmkeluar'=> $row1->no_manual
											);
						}
					}
				}
				else {
					$detail_bonmkeluar = '';
				}
				
				$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$kode_gudang = '';
					$nama_gudang	= '';
				}
				
				$data_bonmkeluar[] = array(	'id'=> $row1->id,
											'no_bonm'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'tgl_update'=> $row1->tgl_update,
											'detail_bonmkeluar'=> $detail_bonmkeluar
											);
				$detail_bonmkeluar = array();
			} // endforeach header
		}
		else {
			$data_bonmkeluar = '';
		}
		return $data_bonmkeluar;
  }
  
  function get_bonmkeluartanpalimit($cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT a.id, a.no_manual, a.tgl_bonm, a.id_gudang FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE b.untuk_bisbisan = 't' AND b.status_op_bisbisan = 'f' ");
	}
	else {
		$query	= $this->db->query(" SELECT a.id, a.no_manual, a.tgl_bonm, a.id_gudang FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE b.untuk_bisbisan = 't' AND b.status_op_bisbisan = 'f' 
							AND UPPER(no_manual) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_detail_bonmkeluar($list_brg){
    $detail_bonmkeluar = array();
        
    foreach($list_brg as $row1) {
		if ($row1 != '') {
			$query2	= $this->db->query(" SELECT a.id, a.no_manual, a.tgl_bonm, b.id_brg, b.qty
								FROM tm_bonmkeluar_detail b INNER JOIN tm_bonmkeluar a ON a.id=b.id_bonmkeluar 
								WHERE b.id = '$row1' ");
			if ($query2->num_rows() > 0) {
				$hasilrow = $query2->row();
				$id_bonmkeluar	= $hasilrow->id;
				$no_bonmkeluar	= $hasilrow->no_manual;
				$tgl_bonm	= $hasilrow->tgl_bonm;
				$id_brg	= $hasilrow->id_brg;
				$qty	= $hasilrow->qty;
									
						//cek apakah di tm_op_makloon sudah ada yg dipenuhi
						$sqlxx = " SELECT * FROM tm_op_makloon_detail where id_bonmkeluar_detail = '$row1' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_makloon_detail a 
									INNER JOIN tm_op_makloon b ON a.id_op_makloon = b.id WHERE a.id_bonmkeluar_detail = '$row1' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan id_bonmkeluar_detail
						}
						else {
							$jum_op = 0;
						}
												
						$qty = $qty-$jum_op;
			
				$query2	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.id_satuan_konversi, a.rumus_konversi, 
									a.angka_faktor_konversi, b.nama as nama_satuan 
									FROM tm_barang a INNER JOIN tm_satuan b ON b.id = a.satuan
									WHERE a.id = '$id_brg' ");
				$hasilrow = $query2->row();
				$kode_brg	= $hasilrow->kode_brg;
				$nama_brg	= $hasilrow->nama_brg;
				$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
				$rumus_konversi = $hasilrow->rumus_konversi;
				$angka_faktor_konversi = $hasilrow->angka_faktor_konversi;
				$nama_satuan	= $hasilrow->nama_satuan;
				
				if ($id_satuan_konversi != 0) {
					$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan
										WHERE id = '$id_satuan_konversi' ");
					$hasilrow = $query3->row();
					$nama_satuan_konv	= $hasilrow->nama_satuan;
					
					if ($rumus_konversi == '1') {
							$qty_satawal = $qty/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$qty_satawal = $qty*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$qty_satawal = $qty-$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$qty_satawal = $qty+$angka_faktor_konversi;
						}
						else
							$qty_satawal = $qty;
				}
				else {
					$nama_satuan_konv = "Tidak Ada";
					$qty_satawal = $qty;
				}
				
				$pisah1 = explode("-", $tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				
				$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
				$detail_bonmkeluar[] = array(	'id'=> $row1,
										'id_brg'=> $id_brg,
										'kode_brg'=> $kode_brg,
										'nama'=> $nama_brg,
										'nama_satuan'=> $nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'qty'=> $qty,
										'qty_satawal'=> $qty_satawal,
										'rumus_konversi'=> $rumus_konversi,
										'angka_faktor_konversi'=> $angka_faktor_konversi,
										'qty'=> $qty,
										'no_bonmkeluar'=> $no_bonmkeluar,
										'tgl_bonm'=> $tgl_bonm
								);
			} // end if num_row > 0
		}
	}
	return $detail_bonmkeluar;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_pkp_tipe_pajak_bykodesup($id_sup){
	$query	= $this->db->query(" SELECT pkp, tipe_pajak, top FROM tm_supplier where id = '$id_sup' ");    
    return $query->result();  
  }
  
  function get_op_bisbisan($id_op_makloon, $is_cetak){
	$query	= $this->db->query(" SELECT * FROM tm_op_makloon WHERE id = '$id_op_makloon' ");    
    $hasil = $query->result();
    
    $data_op = array();
	$detail_op = array();
	// 29-07-2015
	$status_op = "";
	if ($is_cetak == '0')
		$status_op = " AND status_op = 'f' ";
					
	foreach ($hasil as $row1) {
		$no_bonmkeluarnya = ""; $list_no_bonmkeluarnya = ""; 
		
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_op_makloon_detail WHERE id_op_makloon = '$row1->id' ".$status_op." ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						
						$query2	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.nama_brg_supplier, a.id_satuan_konversi, a.rumus_konversi, 
											a.angka_faktor_konversi, b.nama as nama_satuan 
											FROM tm_barang a INNER JOIN tm_satuan b ON b.id = a.satuan
											WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query2->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$nama_brg_sup	= $hasilrow->nama_brg_supplier;
						$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
						$rumus_konversi = $hasilrow->rumus_konversi;
						$angka_faktor_konversi = $hasilrow->angka_faktor_konversi;
						$nama_satuan	= $hasilrow->nama_satuan;
						
						if ($id_satuan_konversi != 0) {
							$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan
												WHERE id = '$id_satuan_konversi' ");
							$hasilrow = $query3->row();
							$nama_satuan_konv	= $hasilrow->nama_satuan;
							
							/*if ($rumus_konversi == '1') {
									$qty_satawal = $qty/$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '2') {
									$qty_satawal = $qty*$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '3') {
									$qty_satawal = $qty-$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '4') {
									$qty_satawal = $qty+$angka_faktor_konversi;
								}
								else
									$qty_satawal = $qty; */
						}
						else {
							$nama_satuan_konv = "Tidak Ada";
							//$qty_satawal = $qty;
						}
				
						/*$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.nama_brg_supplier, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$nama_brg_sup	= $hasilrow->nama_brg_supplier;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$nama_brg_sup = '';
							$satuan	= '';
						} */
												
						// cek apakah status_op = 'f' dan jum_beli == 0 ?
						$query3	= $this->db->query(" SELECT status_op FROM tm_op_makloon_detail WHERE id = '$row2->id' ");
						 if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$status_op	= $hasilrow->status_op;
						 }
						 else
							$status_op = 0;
						
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail WHERE id_op_makloon_detail = '".$row2->id."' ");
						if ($query4->num_rows() > 0){
								$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b
											WHERE a.id_pembelian = b.id AND a.id_op_detail = '$row2->id' AND b.status_aktif = 't'
											AND a.kode_brg = '$row2->kode_brg'
											AND b.kode_supplier = '$row1->kode_supplier' "); //
							$hasilrow = $query3->row();
							$jum_beli2 = $hasilrow->jum; // ini sum qty di pembelian_detail
						}
						else {
							$jum_beli2 = 0;
						}
												
						$sqlxx = " SELECT * FROM tm_op_makloon_detail where id_bonmkeluar_detail = '$row2->id_bonmkeluar_detail' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							//ambil nomor bonm Keluar
							$query3	= $this->db->query(" SELECT a.no_manual, a.tgl_bonm FROM tm_bonmkeluar_detail b 
												INNER JOIN tm_bonmkeluar a ON a.id=b.id_bonmkeluar WHERE b.id = '$row2->id_bonmkeluar_detail' ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$no_bonmkeluar	= $hasilrow->no_manual;
								$tgl_bonm	= $hasilrow->tgl_bonm;
								
								$pisah1 = explode("-", $tgl_bonm);
								$thn1= $pisah1[0];
								$bln1= $pisah1[1];
								$tgl1= $pisah1[2];
								
								$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
							 }
							 else {
								$no_bonmkeluar = '';
								$tgl_bonm = '';
							 }
							if ($no_bonmkeluarnya != $no_bonmkeluar) {
								$list_no_bonmkeluarnya.= $no_bonmkeluar."; ";
							}
							$no_bonmkeluarnya = $no_bonmkeluar;
						}
						
						if ($is_cetak == '0') {
							if ($status_op == 'f' && $jum_beli2 == 0) {
								$detail_op[] = array(	'id'=> $row2->id,
													'id_brg'=> $row2->id_brg,
													'kode_brg'=> $kode_brg,
													'nama'=> $nama_brg,
													'nama_brg_sup'=> $nama_brg_sup,
													'satuan'=> $nama_satuan,
													'nama_satuan_konv'=> $nama_satuan_konv,
													'qty'=> $row2->qty,
													'qty_satawal'=> $row2->qty_satawal,
													'keterangan'=> $row2->keterangan,
													'id_bonmkeluar_detail'=> $row2->id_bonmkeluar_detail,
													'no_bonmkeluar'=> $no_bonmkeluarnya,
													'tgl_bonm'=> $tgl_bonm,
													'id_satuan_konversi'=> $id_satuan_konversi,
													'rumus_konversi'=> $rumus_konversi,
													'angka_faktor_konversi'=> $angka_faktor_konversi,
													'jenis_potong'=> $row2->jenis_potong,
													'id_ukuran_bisbisan'=> $row2->id_ukuran_bisbisan
												);
							}
							else
								$detail_op = '';
						}
						else {
							$detail_op[] = array(	'id'=> $row2->id,
													'id_brg'=> $row2->id_brg,
													'kode_brg'=> $kode_brg,
													'nama'=> $nama_brg,
													'nama_brg_sup'=> $nama_brg_sup,
													'satuan'=> $nama_satuan,
													'nama_satuan_konv'=> $nama_satuan_konv,
													'qty'=> $row2->qty,
													'qty_satawal'=> $row2->qty_satawal,
													'keterangan'=> $row2->keterangan,
													'id_bonmkeluar_detail'=> $row2->id_bonmkeluar_detail,
													'no_bonmkeluar'=> $no_bonmkeluarnya,
													'tgl_bonm'=> $tgl_bonm,
													'id_satuan_konversi'=> $id_satuan_konversi,
													'rumus_konversi'=> $rumus_konversi,
													'angka_faktor_konversi'=> $angka_faktor_konversi,
													'jenis_potong'=> $row2->jenis_potong,
													'id_ukuran_bisbisan'=> $row2->id_ukuran_bisbisan
												);
						}
					}
				}
				else {
					$detail_op = '';
				}
				// ambil data nama supplier
				if ($row1->id_supplier != '0') {
					$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp, kontak_person FROM tm_supplier WHERE id = '$row1->id_supplier' ");
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
					$pkp	= $hasilrow->pkp;
					$kontak_person	= $hasilrow->kontak_person;
				}
				else {
					$kode_supplier	= '';
					$pkp	= 'f';
					$nama_supplier	= 'Lain-lain';
					$kontak_person	= '-';
				}
														
				$data_op[] = array(			'id'=> $row1->id,	
											'no_bonmkeluar'=> $list_no_bonmkeluarnya,	
											'no_op'=> $row1->no_op,
											'tgl_op'=> $row1->tgl_op,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'pkp'=> $pkp,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_op'=> $row1->status_op,
											'detail_op'=> $detail_op,
											'kontak_person'=> $kontak_person,
											'jenis_pembelian'=> $row1->jenis_pembelian
											);
				$detail_op = array();
	}
	return $data_op;
  }
  
  function get_item_op_bisbisan($iddetail){
	$query = $this->db->query(" SELECT a.no_op, a.tgl_op, a.id_supplier, b.* FROM tm_op_makloon a INNER JOIN tm_op_makloon_detail b 
					ON a.id = b.id_op_makloon WHERE a.jenis_makloon='1' AND b.id = '$iddetail' ");
	$datadetail = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			// ambil data nama barang
			$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.id_satuan_konversi, a.rumus_konversi, 
									a.angka_faktor_konversi, b.nama as nama_satuan FROM tm_barang a
									INNER JOIN tm_satuan b ON a.satuan = b.id
									WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
			$rumus_konversi	= $hasilrow->rumus_konversi;
			$angka_faktor_konversi	= $hasilrow->angka_faktor_konversi;
			$nama_satuan	= $hasilrow->nama_satuan;
			
			if ($id_satuan_konversi != 0) {
				$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan
											WHERE id = '$id_satuan_konversi' ");
				$hasilrow = $query3->row();
				$nama_satuan_konv	= $hasilrow->nama_satuan;
			}
			else
				$nama_satuan_konv = $nama_satuan;
			
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
			
			$datadetail[] = array(			'id'=> $row1->id,	
											'id_op_makloon'=> $row1->id_op_makloon,	
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'no_op'=> $row1->no_op,
											'tgl_op'=> $row1->tgl_op,
											'nama_brg'=> $nama_brg,
											
											'id_satuan_konversi'=> $id_satuan_konversi,
											'rumus_konversi'=> $rumus_konversi,
											'angka_faktor_konversi'=> $angka_faktor_konversi,
											
											'nama_satuan'=> $nama_satuan,
											'nama_satuan_konv'=> $nama_satuan_konv,
											'nama_supplier'=> $nama_supplier,
											'qty'=> $row1->qty,
											'qty_satawal'=> $row1->qty_satawal,
											'id_bonmkeluar_detail'=> $row1->id_bonmkeluar_detail	
											);
		}
	}
	return $datadetail;
  }
  
  function deleteopbisbisanitem($iddetail) { 
	  $query2	= $this->db->query(" SELECT b.id_op_makloon, b.id_bonmkeluar_detail, b.id_brg FROM tm_op_makloon_detail b 
							INNER JOIN tm_op_makloon a ON a.id = b.id_op_makloon WHERE b.id = '$iddetail' ");
	  $hasilrow = $query2->row();
	  $id_op_makloon	= $hasilrow->id_op_makloon;
	  $id_bonmkeluar_detail	= $hasilrow->id_bonmkeluar_detail;
	  $id_brg	= $hasilrow->id_brg;
	  
	  $this->db->query(" UPDATE tm_bonmkeluar_detail SET status_op_bisbisan = 'f' where id= '$id_bonmkeluar_detail' ");

	  $this->db->delete('tm_op_makloon_detail', array('id' => $iddetail));
	  
	  // 18-08-2015 cek apakah item brgnya kosong. kalo kosong, hapus headernya
	  $query2	= $this->db->query(" SELECT count(id) as jumitem FROM tm_op_makloon_detail WHERE id_op_makloon='$id_op_makloon' ");
	  $hasilrow = $query2->row();
	  $jumitem	= $hasilrow->jumitem;
	  
	  if ($jumitem == 0)
		$this->db->query(" DELETE FROM tm_op_makloon WHERE id='$id_op_makloon' ");
  }
  
  // 09-07-2015
  function get_perusahaan() {
	$query3	= $this->db->query(" SELECT id, nama, alamat, kota, bag_pembelian, bag_pembelian2, bag_keuangan, 
						kepala_bagian, bag_admstok, bag_admstok2,
						id_gudang_admstok, id_gudang_admstok2, spv_bag_admstok FROM tm_perusahaan ");
				
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
		$bag_pembelian = $hasilrow->bag_pembelian;
		$bag_pembelian2 = $hasilrow->bag_pembelian2;
		$bag_keuangan = $hasilrow->bag_keuangan;
		$kepala_bagian = $hasilrow->kepala_bagian;
		$bag_admstok = $hasilrow->bag_admstok;
		$bag_admstok2 = $hasilrow->bag_admstok2;
		$id_gudang_admstok = $hasilrow->id_gudang_admstok;
		$id_gudang_admstok2 = $hasilrow->id_gudang_admstok2;
		$spv_bag_admstok = $hasilrow->spv_bag_admstok;
	}
	else {
		$id	= '';
		$nama = '';
		$kota = '';
		$alamat = '';
		$bag_pembelian = '';
		$bag_pembelian2 = '';
		$bag_keuangan = '';
		$kepala_bagian = '';
		$bag_admstok = '';
		$bag_admstok2 = '';
		$id_gudang_admstok = '';
		$id_gudang_admstok2 = '';
		$spv_bag_admstok = '';
	}
		
	$datasetting = array('id'=> $id,
						  'nama'=> $nama,
						  'alamat'=> $alamat,
						  'kota'=> $kota,
						  'bag_pembelian'=> $bag_pembelian,
						  'bag_pembelian2'=> $bag_pembelian2,
						  'bag_keuangan'=> $bag_keuangan,
						  'kepala_bagian'=> $kepala_bagian,
						  'bag_admstok'=> $bag_admstok,
						  'bag_admstok2'=> $bag_admstok2,
						  'id_gudang_admstok'=> $id_gudang_admstok,
						  'id_gudang_admstok2'=> $id_gudang_admstok2,
						  'spv_bag_admstok'=> $spv_bag_admstok
					);
							
	return $datasetting;
  }
  
  function konversi_angka2romawi($bln_now) {
	if ($bln_now == "01")
		$romawi_bln = "I";
	else if ($bln_now == "02")
		$romawi_bln = "II";
	else if ($bln_now == "03")
		$romawi_bln = "III";
	else if ($bln_now == "04")
		$romawi_bln = "IV";
	else if ($bln_now == "05")
		$romawi_bln = "V";
	else if ($bln_now == "06")
		$romawi_bln = "VI";
	else if ($bln_now == "07")
		$romawi_bln = "VII";
	else if ($bln_now == "08")
		$romawi_bln = "VIII";
	else if ($bln_now == "09")
		$romawi_bln = "IX";
	else if ($bln_now == "10")
		$romawi_bln = "X";
	else if ($bln_now == "11")
		$romawi_bln = "XI";
	else if ($bln_now == "12")
		$romawi_bln = "XII";
		
	return $romawi_bln;
  }
  
  function konversi_romawi2angka($blnrom) {
	if ($blnrom == "I")
		$bln_now = "01";
	else if ($blnrom == "II")
		$bln_now = "02";
	else if ($blnrom == "III")
		$bln_now = "03";
	else if ($blnrom == "IV")
		$bln_now = "04";
	else if ($blnrom == "V")
		$bln_now = "05";
	else if ($blnrom == "VI")
		$bln_now = "06";
	else if ($blnrom == "VII")
		$bln_now = "07";
	else if ($blnrom == "VIII")
		$bln_now = "08";
	else if ($blnrom == "IX")
		$bln_now = "09";
	else if ($blnrom == "X")
		$bln_now = "10";
	else if ($blnrom == "XI")
		$bln_now = "11";
	else if ($blnrom == "XII")
		$bln_now = "12";
		
	return $bln_now;
  }
  
  function konversi_bln2deskripsi($bln1) {
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";  
		return $nama_bln;
  }
  
  // 18-08-2015
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_detail_opbisbisan($list_brg, $id_supplier){
    $detail_pp = array();
    
    foreach($list_brg as $row1) {
			if ($row1 != '') {
					$query2	= $this->db->query(" SELECT a.no_op, b.id_op_makloon, b.id_brg, b.qty, b.qty_satawal, b.jenis_potong, b.id_ukuran_bisbisan
											FROM tm_op_makloon a INNER JOIN tm_op_makloon_detail b ON a.id = b.id_op_makloon
											WHERE b.id = '$row1' ");
					$hasilrow = $query2->row();
					$id_op_makloon	= $hasilrow->id_op_makloon;
					$id_brg	= $hasilrow->id_brg;
					$qty	= $hasilrow->qty;
					$qty_satawal	= $hasilrow->qty_satawal;
					$no_op	= $hasilrow->no_op;
					$jenis_potong	= $hasilrow->jenis_potong;
					$id_ukuran_bisbisan	= $hasilrow->id_ukuran_bisbisan;

				$query2	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi,
							b.nama as nama_satuan, e.kode as kode_kel 
							FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
							INNER JOIN tm_jenis_barang d ON a.id_jenis_barang = d.id
							INNER JOIN tm_kelompok_barang e ON d.kode_kel_brg = e.kode
							WHERE a.id = '$id_brg' ");
				$hasilrow = $query2->row();
				$kode_brg	= $hasilrow->kode_brg;
				$nama_brg	= $hasilrow->nama_brg;
				$nama_satuan	= $hasilrow->nama_satuan;
				$kode_kel	= $hasilrow->kode_kel;
				
				$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
				$rumus_konversi	= $hasilrow->rumus_konversi;
				$angka_faktor_konversi	= $hasilrow->angka_faktor_konversi;
				
				if ($id_satuan_konversi != 0) {
					$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan
										WHERE id = '$id_satuan_konversi' ");
					$hasilrow = $query3->row();
					$nama_satuan_konv	= $hasilrow->nama_satuan;
				}
				else {
					$nama_satuan_konv = "Tidak Ada";
				}
				
				// 29-08-2015. konversi pake rumus khusus dari patokan awalnya qty konversi
				// rumus contek dari vmainformbtbbisbisanmanual, tapi dibalik
				if (($nama_satuan == "Meter" && $nama_satuan_konv == "Tidak Ada") || $nama_satuan == "Yard") {
					$qty_satawal = $qty*1.1;
					$qty_satawal = round($qty_satawal, 4);
				}
				else if ($nama_satuan == "Roll") {
					$qty_satawal = $qty*60;
					$qty_satawal = round($qty_satawal, 4);
				}
				
				if ($id_supplier != '0') {
					$query2	= $this->db->query(" SELECT harga FROM tm_harga_bisbisan WHERE jenis_potong = '$jenis_potong'
										AND id_supplier = '$id_supplier' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$hargamaster	= $hasilrow->harga;
					}
					else
						$hargamaster = 0;
				}
				else {
					$hargamaster = 0;
				}
				
				$qty_op = $qty;
				
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail WHERE id_op_makloon_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
						// ambil sum qty pembelian detail
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_makloon_detail a 
										INNER JOIN tm_pembelian_makloon b ON a.id_pembelian_makloon = b.id
										WHERE a.id_op_makloon_detail = '$row1' AND b.status_aktif = 't' 
										AND a.id_brg = '".$id_brg."' ");
						
						if($query3->num_rows()>0) {
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum;
						}
						else
							$jum_beli = 0;
					}
					else {
						$jum_beli = 0;
					}
					
				$jum_op = $jum_beli;							
				$qty = $qty-$jum_op;
					
				$detail_op[] = array(	'id'=> $row1,
											'id_brg'=> $id_brg,
											'kode_brg'=> $kode_brg,
											'nama'=> $nama_brg,
											'nama_satuan'=> $nama_satuan,
											'nama_satuan_konv'=> $nama_satuan_konv,
											'rumus_konversi'=> $rumus_konversi,
											'angka_faktor_konversi'=> $angka_faktor_konversi,
											'kode_kel'=> $kode_kel,
											'qty'=> $qty,
											'qty_satawal'=> $qty_satawal,
											'harga'=> $hargamaster,
											'qty_op'=> $qty_op,
											'jum_beli'=> $jum_beli,
											'no_op'=> $no_op,
											'id_op'=> $id_op_makloon,
											'jenis_potong'=> $jenis_potong,
											'id_ukuran_bisbisan'=> $id_ukuran_bisbisan
									);
				
				
		}
	}
	return $detail_op;
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_brg)
  {
	  $sql= " a.*, b.nama as nj_brg, d.nama as nama_satuan 
			FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id 
			INNER JOIN tm_satuan d ON a.satuan = d.id 
			INNER JOIN tm_kelompok_barang e ON b.kode_kel_brg = e.kode
			WHERE a.status_aktif = 't'  ";
	  if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
	  if ($id_jenis_brg != '0')
			$sql.= " AND b.id = '$id_jenis_brg' ";
	  if ($cari != "all")
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') ) ";
	  $sql.=" order by a.kode_brg";
	  
	  $this->db->select($sql, false)->limit($num,$offset);

    $query = $this->db->get();
    return $query->result();
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_brg){
	$sql= " SELECT a.*, b.nama as nj_brg, d.nama as nama_satuan 
			FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id 
			INNER JOIN tm_satuan d ON a.satuan = d.id 
			INNER JOIN tm_kelompok_barang e ON b.kode_kel_brg = e.kode
			WHERE a.status_aktif = 't'  ";
	  if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
	  if ($id_jenis_brg != '0')
			$sql.= " AND b.id = '$id_jenis_brg' ";
	  if ($cari != "all")
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%".$this->db->escape_str($cari)."%')  
					OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($cari)."%') ) ";
		
		$query	= $this->db->query($sql);
		return $query->result();
  }
  
  function get_jenis_brg($kel_brg) {
	$sql = " SELECT b.nama as nama_kel_brg, c.* FROM tm_kelompok_barang b INNER JOIN tm_jenis_barang c ON b.kode = c.kode_kel_brg
				WHERE b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode ASC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  function cek_data_btbbisbisan($no_sj, $id_supplier){
    $this->db->select("id from tm_pembelian_makloon WHERE no_sj = '$no_sj' AND id_supplier = '$id_supplier' AND status_aktif = 't' AND jenis_makloon='1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savebtbbisbisan($no_sj,$tgl_sj,$id_supplier,$gtotal,$asligtotal,$total_pajak, $dpp,$ket,
				$is_no_op, $jenis_pembelian, $id_op_detail, $id_op2,
				$jenis_potong, $id_ukuran_bisbisan, $id_brg, $nama, $qty, $qty_konv, $harga, $harga_lama, 
				$pajak, $diskon, $total, $aslitotal){  
    
    $tgl = date("Y-m-d H:i:s");
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pembelian_makloon WHERE no_sj = '$no_sj' AND id_supplier = '$id_supplier' 
					AND status_aktif = 't' AND jenis_makloon='1' ", false);
    $query = $this->db->get();
    
	// jika data header blm ada 
	if ($query->num_rows() == 0){
		$uid_update_by = $this->session->userdata('uid');
			// insert di tm_pembelian			
			$no_faktur = '';
			$tgl_faktur = '';
			$status_faktur = 'f';
			
				$data_header = array(
				  'no_sj'=>$no_sj,
				  'tgl_sj'=>$tgl_sj,
				  'no_faktur'=>$no_faktur,
				  'id_supplier'=>$id_supplier,
				  'total'=>$asligtotal,
				  'keterangan'=>$ket,
				  'total_pajak'=>$total_pajak,
				  'dpp'=>$dpp,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'status_faktur'=>$status_faktur,
				  'jenis_pembelian'=>$jenis_pembelian,
				  'uid_update_by'=>$uid_update_by );
			
		$this->db->insert('tm_pembelian_makloon',$data_header);
	}

	// ambil data terakhir di tabel tm_pembelian_makloon
	$query2	= $this->db->query(" SELECT id FROM tm_pembelian_makloon ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pembelian	= $hasilrow->id;
			
			if ($id_brg!='' && $qty!='0' && $harga!='') {
				// jika semua data tdk kosong, insert ke tm_pembelian_makloon_detail
				if ($is_no_op == 't') {
					$data_detail = array(
						'id_brg'=>$id_brg,
						'qty_satawal'=>$qty,
						'qty_konversi'=>$qty_konv,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						'total'=>$aslitotal,
						'id_pembelian_makloon'=>$id_pembelian,
						'jenis_potong'=>$jenis_potong,
						'id_ukuran_bisbisan'=>$id_ukuran_bisbisan,
						//'nama_brg'=>$this->db->escape_str($nama)
						'nama_brg'=>$nama
					);
				}
				
				if ($is_no_op == '' ) {
					$data_detail = array(
						'id_brg'=>$id_brg,
						'qty_satawal'=>$qty,
						'qty_konversi'=>$qty_konv,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						'total'=>$aslitotal,
						'id_pembelian_makloon'=>$id_pembelian,
						'id_op_makloon_detail'=>$id_op_detail,
						'jenis_potong'=>$jenis_potong,
						'id_ukuran_bisbisan'=>$id_ukuran_bisbisan,
						//'nama_brg'=>$this->db->escape_str($nama)
						'nama_brg'=>$nama
					);
				}
				
				$this->db->insert('tm_pembelian_makloon_detail',$data_detail);
				
				// ambil data terakhir di tabel tm_pembelian_makloon_detail
				$queryxx	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail ORDER BY id DESC LIMIT 1 ");
				$hasilxx = $queryxx->row();
				$id_pembelian_detail	= $hasilxx->id;
				
				// 19-08-2015, untuk data apply_stok makloon, sementara ga usah dibikin dulu
		
				//update harga di tabel tm_harga_bisbisan
				if (($harga != $harga_lama) && $id_supplier != '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_bisbisan WHERE jenis_potong = '$jenis_potong'
									AND id_supplier = '$id_supplier' ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_bisbisan (jenis_potong, id_supplier, harga, 
						tgl_input, tgl_update) VALUES ('$jenis_potong', '$id_supplier', '$harga', '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_bisbisan SET harga = '$harga', tgl_update='$tgl'
									where jenis_potong= '$jenis_potong' AND id_supplier = '$id_supplier' ");
					}
				}
			}
			
			// ---------------------------
			if ($is_no_op == '') {
					// cek sum qty di pembelian. jika sum di pembelian = qty OP, maka update status OPnya menjadi true
					$query3	= $this->db->query(" SELECT a.qty FROM tm_op_makloon_detail a 
												INNER JOIN tm_op_makloon b ON a.id_op_makloon = b.id
												WHERE a.id = '$id_op_detail' ");
								
					$hasilrow = $query3->row();
					$qty_op = $hasilrow->qty; // ini qty di OP detail
					
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail WHERE id_op_makloon_detail = '".$id_op_detail."' ");
					if ($query4->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty_konversi) as jum FROM tm_pembelian_makloon_detail a 
									INNER JOIN tm_pembelian_makloon b ON a.id_pembelian_makloon = b.id
									WHERE a.id_op_makloon_detail = '$id_op_detail' AND b.status_aktif = 't' ");
						
						$hasilrow = $query3->row();
						$jum_beli = $hasilrow->jum;
					}
					else {
						$jum_beli = 0;
					}
					
					if ($jum_beli >= $qty_op) {
						$this->db->query(" UPDATE tm_op_makloon_detail SET status_op = 't' where id= '$id_op_detail' ");
						//cek di tabel tm_op_detail, apakah status_op sudah 't' semua?
						$this->db->select("id from tm_op_makloon_detail WHERE status_op = 'f' AND id_op_makloon = '$id_op2' ", false);
						$query = $this->db->get();
						//jika sudah t semua, maka update tabel tm_op_makloon di field status_op menjadi t
						if ($query->num_rows() == 0){
							$this->db->query(" UPDATE tm_op_makloon SET status_op = 't' where id= '$id_op2' ");
						}
					}

			} // end is_no_op
			// ====================================================
  }
  
  // 19-08-2015
  function getAllbtbbisbisan($num, $offset, $supplier, $cari, $date_from, $date_to, $caribrg, $filterbrg) {		
	$pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')  ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
	if ($supplier != '0')
		$pencarian.= " AND a.id_supplier = '$supplier' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
						OR UPPER(c.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
	$pencarian.= " ORDER BY a.tgl_sj DESC, a.id DESC ";
	
	$this->db->select(" distinct a.* FROM tm_pembelian_makloon a 
						LEFT JOIN tm_pembelian_makloon_detail b ON a.id=b.id_pembelian_makloon 
						LEFT JOIN tm_barang c ON c.id = b.id_brg WHERE  
						a.status_aktif = 't' AND a.jenis_makloon='1' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	  
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
									OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_makloon_detail a INNER JOIN tm_barang b ON a.id_brg = b.id
							WHERE a.id_pembelian_makloon = '$row1->id' ".$pencarian2." ORDER BY id ASC ");
				
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					$id_detailnya = "";
					foreach ($hasil2 as $row2) {
					  
					  // ambil id_op_detail ? ini skip aja 17-06-2015
					  // 18-06-2015, id_detail ini adalah id detail dari OP atau PPnya. ini diset 0 aja dah. diquery ulang di function edit/delete
					  $id_detailnya = "";
						
						$query3	= $this->db->query(" SELECT a.kode_brg, a.id_satuan_konversi, a.rumus_konversi, 
												a.angka_faktor_konversi, b.nama as nama_satuan FROM tm_barang a 
												INNER JOIN tm_satuan b ON a.satuan = b.id
												WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
							$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
							$rumus_konversi = $hasilrow->rumus_konversi;
							$angka_faktor_konversi = $hasilrow->angka_faktor_konversi;
							
							if ($id_satuan_konversi != '0') {
								$queryxx	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id ='$id_satuan_konversi' ");
								$hasilxx = $queryxx->row();
								$satuan_konv	= $hasilxx->nama;
							}
							else
								$satuan_konv = $satuan;
						}
						else {
							$kode_brg = '';
							$nama_brg = '';
							$satuan = '';
							$id_satuan_konversi = 0;
							$rumus_konversi = 0;
							$angka_faktor_konversi = 0;
							$satuan_konv = '';
						}
						
						if ($row2->jenis_potong == '1')
							$jenis_potong = "Potong Serong";
						else if ($row2->jenis_potong == '2')
							$jenis_potong = "Potong Lurus";
						else
							$jenis_potong = "Potong Spiral";
						
						$queryxx	= $this->db->query(" SELECT nama FROM tm_ukuran_bisbisan WHERE id ='$row2->id_ukuran_bisbisan' ");
						$hasilxx = $queryxx->row();
						$nama_ukuran = $hasilxx->nama;
				
						$detail_fb[] = array(	'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $row2->nama_brg,
												'satuan'=> $satuan,
												'satuan_konv'=> $satuan_konv,
												'jenis_potong'=> $jenis_potong,
												'nama_ukuran'=> $nama_ukuran,
												'qty_konversi'=> $row2->qty_konversi,
												'qty_satawal'=> $row2->qty_satawal,
												'qty_konversi'=> $row2->qty_konversi,
												'harga'=> $row2->harga,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$id_detailnya = "";
					$detail_fb = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				
				// 18-06-2015, query utk ambil no PP / OP
				$no_opnya = "";;
				
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_op FROM tm_pembelian_makloon_detail a INNER JOIN tm_op_makloon_detail b 
						ON a.id_op_makloon_detail = b.id 
						INNER JOIN tm_op_makloon c ON c.id = b.id_op_makloon
						WHERE a.id_pembelian_makloon='$row1->id' ORDER BY c.no_op ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$no_opnya.= $rowxx->no_op."<br>";
					}// end for
				}
				else {
					$no_opnya = '';
				}
								
				// 13-08-2015. cek apakah sudah diupdate stoknya.
				// 19-08-2015, yg ini jgn dulu dimunculin. 14-09-2015, skrg dimunculin cetakbtbnya
				if ($row1->status_stok == 't')
					$cetakbtb = '1';
				else
					$cetakbtb = '0';
						
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_op'=> $no_opnya,	
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'total'=> $row1->total,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'status_stok'=> $row1->status_stok,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											'cetakbtb'=> $cetakbtb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAllbtbbisbisantanpalimit($supplier, $cari, $date_from, $date_to, $caribrg, $filterbrg){
    $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sj) like UPPER('%".$this->db->escape_str($cari)."%')  ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
	if ($supplier != '0')
		$pencarian.= " AND a.id_supplier = '$supplier' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
						OR UPPER(c.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
	$pencarian.= " ORDER BY a.tgl_sj DESC, a.id DESC ";
	
	$query = $this->db->query(" SELECT distinct a.* FROM tm_pembelian_makloon a LEFT JOIN tm_pembelian_makloon_detail b ON a.id=b.id_pembelian_makloon 
						LEFT JOIN tm_barang c ON c.id = b.id_brg WHERE  
						a.status_aktif = 't' AND a.jenis_makloon='1' ".$pencarian);
	      
    return $query->result();  
  }
  
  // 19-08-2015
  function deletebtbbisbisan($id){    
	  $tgl = date("Y-m-d H:i:s");
	  	  
	    $sqlxx = " SELECT DISTINCT id_op_makloon_detail FROM tm_pembelian_makloon_detail WHERE id_pembelian_makloon='$id' ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx=$queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$id_op_detail	= $rowxx->id_op_makloon_detail;
				
				if ($id_op_detail != '0') {
					$sqlxx2= " SELECT distinct a.id FROM tm_op_makloon a INNER JOIN tm_op_makloon_detail b ON a.id = b.id_op_makloon
							 WHERE b.id = '$id_op_detail' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_op	= $hasilxx2->id;
						$this->db->query(" UPDATE tm_op_makloon SET status_op = 'f' where id= '$id_op' ");
					}
					// reset status di detailnya dari OP pake perulangan
					$this->db->query(" UPDATE tm_op_makloon_detail SET status_op = 'f' where id= '$id_op_detail' ");
				}
			} // end for
		}
			
		$this->db->query(" UPDATE tm_pembelian_makloon SET status_aktif = 'f', status_stok = 'f' WHERE id = '$id' ");
		$this->db->query(" UPDATE tm_pembelian_makloon_detail SET status_stok = 'f' WHERE id_pembelian_makloon = '$id' ");
	//}
  }
  
  function get_pembelian_bisbisan($id_pembelian) {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon where id = '$id_pembelian' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// 10-07-2015 AMBIL NO BONM MASUK DAN TGLNYA
				$no_bonmnya = "";
				$sqlxx = " SELECT DISTINCT a.no_bonm, a.tgl_bonm FROM tm_pembelian_makloon_detail a 
							WHERE a.id_pembelian_makloon='$row1->id' AND a.status_stok = 't' ORDER BY a.no_bonm ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						if ($rowxx->tgl_bonm != '') {
							$pisah1 = explode("-", $rowxx->tgl_bonm);
							$thn1= $pisah1[0];
							$bln1= $pisah1[1];
							$tgl1= $pisah1[2];
							$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
							$no_bonmnya.= $rowxx->no_bonm." (".$tgl_bonm.")"."<br>";
						}
					}// end for
				}
				else {
					$no_bonmnya = '';
				}
				if ($no_bonmnya == '')
					$no_bonmnya = '-';
								
				$no_opnya = "";
				$sqlxx = " SELECT DISTINCT c.no_op, c.tgl_op FROM tm_pembelian_makloon_detail a INNER JOIN tm_op_makloon_detail b ON a.id_op_makloon_detail = b.id 
						INNER JOIN tm_op_makloon c ON c.id = b.id_op_makloon
						WHERE a.id_pembelian_makloon='$row1->id' ORDER BY c.no_op ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$pisah1 = explode("-", $rowxx->tgl_op);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_op = $tgl1."-".$bln1."-".$thn1;
						$no_opnya.= $rowxx->no_op." (".$tgl_op.")"."<br>";
					}// end for
				}
				else {
					$no_opnya = '-';
				}
								
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_makloon_detail WHERE id_pembelian_makloon = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.id_satuan_konversi, a.rumus_konversi, 
												a.angka_faktor_konversi, b.nama as nama_satuan FROM tm_barang a 
												INNER JOIN tm_satuan b ON a.satuan = b.id
												WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							$nama_satuan	= $hasilrow->nama_satuan;
							$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
							$rumus_konversi = $hasilrow->rumus_konversi;
							$angka_faktor_konversi = $hasilrow->angka_faktor_konversi;
							
							if ($id_satuan_konversi != '0') {
								$queryxx	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id ='$id_satuan_konversi' ");
								$hasilxx = $queryxx->row();
								$nama_satuan_konv	= $hasilxx->nama;
							}
							else
								$nama_satuan_konv = $nama_satuan;
						}
												
						// 14-09-2015 ambil qty di pembelian_makloon_detail jika status_stok = t
						if ($row1->status_stok == 't'){
							$qty_fisik	= $row2->qty_satawal;
						}
						else
							$qty_fisik = '-';
						
						$sum_qty = 0;
						$sum_beli = 0;
					  					  
					    $query3	= $this->db->query(" SELECT c.kode_kel_brg FROM tm_barang a INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
									WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
						
						// 31-07-2015 hitung sisa OP
						// 1. ambil qty di OP sesuai id_op_makloon_detail
						$sqlop = " SELECT b.qty FROM tm_op_makloon a INNER JOIN tm_op_makloon_detail b ON a.id = b.id_op_makloon
									WHERE b.id = '$row2->id_op_makloon_detail' ";
						$queryop	= $this->db->query($sqlop);
						if($queryop->num_rows()>0) {
							$hasilop = $queryop->row();
							$qty_op_detail = $hasilop->qty;
						}
						else
							$qty_op_detail = 0;
						
						if ($row2->id_op_makloon_detail != '0') {
							$query3	= $this->db->query(" SELECT SUM(a.qty_konversi) AS pemenuhan FROM tm_pembelian_makloon_detail a 
										INNER JOIN tm_pembelian_makloon b ON b.id=a.id_pembelian_makloon
										WHERE a.id_op_makloon_detail='$row2->id_op_makloon_detail' AND a.id_brg='$row2->id_brg' 
										AND b.status_aktif='t'
										AND b.tgl_sj <= '$row1->tgl_sj' ");
							
							if($query3->num_rows()>0) {
								$hasilrow4 = $query3->row();
								$pemenuhan = $hasilrow4->pemenuhan;
								$sisa = $qty_op_detail-$pemenuhan;
							}else{
								$pemenuhan = 0;
								$sisa = $qty_op_detail-$pemenuhan;
								
								if($pemenuhan=='')
									$pemenuhan = 0;
								
								if($sisa=='')
									$sisa = 0;
							}
						}
						else {
							$sisa = '-';
							$pemenuhan = 0;
						}
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_kel_brg'=> $kode_kel_brg,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $row2->nama_brg,
												'nama_satuan'=> $nama_satuan,
												'nama_satuan_konv'=> $nama_satuan_konv,
												'rumus_konversi'=> $rumus_konversi,
												'angka_faktor_konversi'=> $angka_faktor_konversi,
												'jenis_potong'=> $row2->jenis_potong,
												'id_ukuran_bisbisan'=> $row2->id_ukuran_bisbisan,
												'harga'=> $row2->harga,
												'qty_satawal'=> $row2->qty_satawal,
												'qty_konversi'=> $row2->qty_konversi,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total,
												'qty_op'=> $sum_qty,
												'jum_beli'=> $sum_beli,
												'id_op_detail'=> $row2->id_op_makloon_detail,
												'qty_fisik'=> $qty_fisik,
												//31-07-2015
												'pemenuhan'=> $pemenuhan,
												'sisa'=> $sisa
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama, top FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				$top	= $hasilrow->top;
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
												
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_op'=> $no_opnya,
											'no_bonm'=> $no_bonmnya,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'total'=> $row1->total,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'dpp'=> $row1->dpp,
											'detail_fb'=> $detail_fb,
											//'list_id_gudang'=> $list_id_gudang
											'uid_updatestok_by'=> $row1->uid_updatestok_by
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  // 20-08-2015
  function getAllfakturbisbisan($num, $offset, $jenis_beli, $status_lunas, $supplier, $cari, $date_from, $date_to) {
	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$this->db->select(" * FROM tm_pembelian_makloon_faktur WHERE jenis_makloon = '1' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b 
								INNER JOIN tm_pembelian_makloon_faktur a ON b.id = a.id_supplier
								WHERE a.id = '$row1->id' ");
				if ($query3->num_rows() > 0) {
					$hasilrow3 = $query3->row();
					$kode_supplier	= $hasilrow3->kode_supplier;
					$nama_supplier	= $hasilrow3->nama;
				}
				else {
					$kode_supplier	= '';
					$nama_supplier	= '';
				}
				
				// ambil detail data no & tgl SJ
				$query2	= $this->db->query(" SELECT a.id_pembelian_makloon FROM tm_pembelian_makloon_faktur_detail a
							INNER JOIN tm_pembelian_makloon_faktur b ON a.id_pembelian_makloon_faktur = b.id
							WHERE a.id_pembelian_makloon_faktur = '$row1->id' ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$id_pembelian_makloon = $row2->id_pembelian_makloon; 
						
						$sql = "SELECT no_sj, tgl_sj from tm_pembelian_makloon where id = '$id_pembelian_makloon' ";
						$query3	= $this->db->query($sql);
						$hasilrow3 = $query3->row();
						$tgl_sj	= $hasilrow3->tgl_sj;
						$no_sjx	= $hasilrow3->no_sj;
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;

						$detail_fb[] = array('no_sj'=> $no_sjx,
											'tgl_sj'=> $tgl_sj,
											'id_pembelian_makloon'=> $id_pembelian_makloon
											);		
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
						$exptgl1 = explode(" ", $tgl1);
						$tgl1nya= $exptgl1[0];
						$jam1nya= $exptgl1[1];
						$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,	
											'no_faktur'=> $row1->no_faktur,	
											'tgl_faktur'=> $tgl_faktur,	
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,	
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb,
											'status_faktur_pajak'=> $row1->status_faktur_pajak,
											'status_lunas'=> $row1->status_lunas
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAllfakturbisbisantanpalimit($jenis_beli, $status_lunas, $supplier, $cari, $date_from, $date_to){
	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$query = $this->db->query(" SELECT * FROM tm_pembelian_makloon_faktur WHERE jenis_makloon='1' ".$pencarian);
    
    return $query->result();  
  }
  
  function get_btbbisbisan($jnsaction, $supplier, $jenis_pembelian, $no_fakturnya, $cari) {
	  // ambil data SJ pembelian 
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE status_faktur = 'f' AND status_aktif = 't'
								AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1' order by id_supplier, tgl_sj DESC ");
			}
			else { // utk proses edit
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' OR no_faktur <> '' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') AND status_aktif = 't'
				AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1' order by id_supplier, tgl_sj DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE status_faktur = 'f' 
						AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
						order by id_supplier, tgl_sj DESC ");
			}
			else {				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE ( status_faktur = 'f' OR no_faktur = '$no_fakturnya' ) 
							AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
							order by id_supplier, tgl_sj DESC "); //
			}
		}
	}
	else {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE status_faktur = 'f' AND status_aktif = 't'
				AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by id_supplier, tgl_sj DESC ");
			}
			else {				
				$query	= $this->db->query(" select * FROM tm_pembelian_makloon WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) AND status_aktif = 't'
				order by id_supplier, tgl_sj DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" select * FROM tm_pembelian_makloon WHERE status_faktur = 'f' 
				AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by id_supplier, tgl_sj DESC ");
			}
			else {				
				$query	= $this->db->query(" select * FROM tm_pembelian_makloon WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by id_supplier, tgl_sj DESC ");
			}
		}
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query2	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query2->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'total'=> $row1->total,
											'total_pajak'=> $row1->total_pajak,
											'jenis_pembelian'=> $row1->jenis_pembelian
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function get_btbbisbisantanpalimit($jnsaction, $supplier, $jenis_pembelian, $no_fakturnya, $cari){
	if ($cari == "all") {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$query = $this->db->getwhere('tm_pembelian',array('status_faktur'=>'f' ));
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE status_faktur = 'f' AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE (status_faktur = 'f'
									OR no_faktur = '$no_fakturnya') AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1' ");
			}			
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE status_faktur = 'f'
							AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1' AND id_supplier='$supplier' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE (status_faktur = 'f' 
									OR no_faktur = '$no_fakturnya') AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1' AND id_supplier = '$supplier' ");
			}
		}
	}
	else {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE status_faktur = 'f' AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya')
									AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1' AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE status_faktur = 'f' AND id_supplier = '$supplier' 
								AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
								AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
									AND id_supplier = '$supplier' AND jenis_pembelian='$jenis_pembelian' AND jenis_makloon='1'
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
	}
    
    return $query->result();  
  }
  
  function cek_data_fakturbisbisan($no_fp, $supplier){
    $this->db->select("id from tm_pembelian_makloon_faktur WHERE no_faktur = '$no_fp' AND id_supplier = '$supplier' AND jenis_makloon='1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savefakturbisbisan($no_fp, $tgl_fp, $supplier, $jenis_pembelian, $jum, $id_sj){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
	$list_sj = explode(";", $id_sj); 

	$data_header = array(
			  'no_faktur'=>$no_fp,
			  'tgl_faktur'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'id_supplier'=>$supplier,
			  'jenis_pembelian'=>$jenis_pembelian,
			  'jumlah'=>$jum,
			  'sisa'=>$jum,
			  'uid_update_by'=>$uid_update_by
			);
	$this->db->insert('tm_pembelian_makloon_faktur',$data_header);
	
	// ambil data terakhir di tabel tm_pembelian_makloon_faktur
	$query2	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_faktur ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_pembelian_makloon_faktur'=>$id_pf,
			  'id_pembelian_makloon'=>$row1,
			);
			$this->db->insert('tm_pembelian_makloon_faktur_detail',$data_detail);
						
			$this->db->query(" UPDATE tm_pembelian_makloon SET no_faktur = '$no_fp', status_faktur = 't', jenis_pembelian = '$jenis_pembelian' 
								WHERE id = '$row1' ");
		}
	}
  }
  
  function deletefakturbisbisan($id){    	
	//semua no_sj di tabel tm_pembelian_makloon yg bersesuaian dgn tm_pembelian_makloon_faktur dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.id_supplier, b.id_pembelian_makloon FROM tm_pembelian_makloon_faktur a 
							INNER JOIN tm_pembelian_makloon_faktur_detail b ON a.id = b.id_pembelian_makloon_faktur
							WHERE a.id = '$id' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
				$this->db->query(" UPDATE tm_pembelian_makloon SET status_faktur = 'f', no_faktur = '' 
							WHERE id = '$row1->id_pembelian_makloon' ");
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di tm_pembelian_makloon_faktur dan pembelian_makloon_faktur_detail
	$this->db->query(" DELETE FROM tm_pembelian_makloon_faktur_detail where id_pembelian_makloon_faktur= '$id' ");
	$this->db->query(" DELETE FROM tm_pembelian_makloon_faktur where id = '$id' ");
  }
  
  function get_faktur_bisbisan($id_faktur){
	$query	= $this->db->query(" SELECT * FROM tm_pembelian_makloon_faktur where id='$id_faktur' ");    
	
	$data_faktur = array();
	$detail_sj = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT id_pembelian_makloon FROM tm_pembelian_makloon_faktur_detail 
									WHERE id_pembelian_makloon_faktur = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sj = ''; $id_sj = '';
				foreach ($hasil2 as $row2) {
						$sqlxx = " SELECT id, no_sj FROM tm_pembelian_makloon WHERE id='$row2->id_pembelian_makloon' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$no_sj .= $hasilxx->no_sj.",";
							$id_sj .= $hasilxx->id.";";
						}
				}
				
				$pisah1 = explode("-", $row1->tgl_faktur);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_faktur = $tgl1."-".$bln1."-".$thn1;						
			
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'id_supplier'=> $row1->id_supplier,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_sj'=> $no_sj,
											'id_sj'=> $id_sj
											);
			} // endforeach header
	}
    return $data_faktur;
  }
  
  // 28-08-2015
  function get_opbisbisan_forbtb($supplier, $jenis_pembelian, $cari) {
  // ambil data OP yg status_op = FALSE
	
	if ($cari == "all") {
		$sql = " * FROM tm_op_makloon WHERE status_op = 'f' AND status_aktif = 't' AND id_supplier = '$supplier' 
				AND jenis_pembelian = '$jenis_pembelian' AND jenis_makloon='1' order by tgl_op DESC, no_op DESC ";
		$this->db->select($sql, false);
		$query = $this->db->get();
	}
	else {
		$sql = " * FROM tm_op_makloon WHERE status_op = 'f' AND status_aktif = 't' AND jenis_pembelian = '$jenis_pembelian' 
				AND id_supplier = '$supplier' AND jenis_makloon='1' AND UPPER(no_op) like UPPER('%$cari%') order by tgl_op DESC, no_op DESC ";

		$this->db->select($sql, false);
		$query = $this->db->get();
	}
		$data_op = array();
		$detail_op = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$sql2 = "SELECT * FROM tm_op_makloon_detail WHERE id_op_makloon = '$row1->id' AND status_op = 'f' ORDER BY id ASC ";
				
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan, a.id_satuan_konversi 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
						
						if ($id_satuan_konversi != '0') {
							$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan
												WHERE a.id = '$id_satuan_konversi' ");
							$hasilrow = $query3->row();
							$satuan_konv	= $hasilrow->nama_satuan;
						}
						else
							$satuan_konv = $satuan;
			
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_makloon_detail WHERE id_op_makloon_detail = '".$row2->id."' ");
						if ($query4->num_rows() > 0){
							$sql3 = " SELECT sum(a.qty_konversi) as jum FROM tm_pembelian_makloon_detail a 
										INNER JOIN tm_pembelian_makloon b ON a.id_pembelian_makloon = b.id WHERE b.status_aktif = 't' ";
							$sql3.= " AND a.id_op_makloon_detail = '$row2->id' AND a.id_brg = '$row2->id_brg' ";
									
							$query3	= $this->db->query($sql3);
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan OP detail tsb
						}
						else {
							$jum_op = 0;
						}

						$qty = $row2->qty-$jum_op;
						if ($qty <= 0) {
							$this->db->query(" UPDATE tm_op_makloon_detail SET status_op = 't' WHERE id='".$row2->id."' ");
							
							// 13-08-2015, cek apakah sudah t semua. jika sudah, maka update headernya juga							
							$queryxx	= $this->db->query(" SELECT id FROM tm_op_makloon_detail WHERE id_op_makloon = '".$row1->id."' AND status_op = 'f' ");
							if ($queryxx->num_rows() == 0){
								$this->db->query(" UPDATE tm_op_makloon SET status_op = 't' WHERE id='".$row1->id."' ");
							}
						}
						
						if ($qty > 0) {
							$detail_op[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'satuan_konv'=> $satuan_konv,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan
											);
						}
					}
				}
				else {
					$detail_op = '';
				}
				
				if ($row1->id_supplier != '0') {
					$query3	= $this->db->query(" SELECT kode_supplier, nama from tm_supplier where id = '$row1->id_supplier' ");
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else {
					$kode_supplier	= '';
					$nama_supplier	= '';
				}
				
				$no_op = $row1->no_op;
				$tgl_op = $row1->tgl_op;
				
				$data_op[] = array(			'id'=> $row1->id,	
											'no_op'=> $no_op,
											'tgl_op'=> $tgl_op,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'detail_op'=> $detail_op
											);
				$detail_op = array();
			} // endforeach header
		}
		else {
			$data_op = '';
		}
		return $data_op;
  }

}

