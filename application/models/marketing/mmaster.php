<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();
  }
  
  // 23-04-2015
  function getAll($num, $offset, $cari)
  {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	if ($cari == "all") {
		$db2->select(" b.i_product_motif, b.e_product_motifname, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tr_product_motif b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1'
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.i_product_motif ", false)->limit($num,$offset);
	}
	else {
		$db2->select(" b.i_product_motif, b.e_product_motifname, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tr_product_motif b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1' AND (UPPER(b.i_product_motif) like UPPER('%$cari%') 
				OR UPPER(b.e_product_motifname) like UPPER('%$cari%'))
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.i_product_motif ", false)->limit($num,$offset);		
    }
    $query = $db2->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari){
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	if ($cari == "all") {
		$db2->select(" b.i_product_motif, b.e_product_motifname, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tr_product_motif b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1'
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.i_product_motif ", false);
	}
	else {
		$db2->select(" b.i_product_motif, b.e_product_motifname, b.id_kel_brg_jadi, xx.kode, xx.nama FROM tr_product_motif b
				LEFT JOIN tr_product_base a ON a.i_product_base = b.i_product
				LEFT JOIN tm_kel_brg_jadi xx ON b.id_kel_brg_jadi = xx.id
				WHERE b.n_active = '1' AND (UPPER(b.i_product_motif) like UPPER('%$cari%') 
				OR UPPER(b.e_product_motifname) like UPPER('%$cari%'))
				ORDER BY a.f_stop_produksi ASC, xx.kode, b.i_product_motif ", false);
	}
    $query = $db2->get();
    return $query->result();  
  }
  
  function get($i_product_motif){
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
	
    $query = $db2->query(" SELECT * FROM tr_product_motif WHERE i_product_motif = '$i_product_motif' ");
    return $query->result();
  }
        
  function cek_data($kode){
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
	
    $db2->select("i_product_motif from tr_product_motif WHERE i_product = '$kode' ", false);
    $query = $db2->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode,$nama, $kel_brg_jadi, $kodeedit, $goedit){  
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
	
    $tgl = date("Y-m-d");
    if ($goedit == '') {
		$data = array(
		  'i_product_base'=>$kode,
		  'e_product_basename'=>$nama,
		  'd_entry'=>$tgl,
		  'd_update'=>$tgl
		);
		$db2->insert('tr_product_base',$data); 
		
		$kode_brg_jadi = $kode."00";
		$data2 = array(
		  'i_product_motif'=>$kode_brg_jadi,
		  'i_product'=>$kode,
		  'e_product_motifname'=>$nama,
		  'n_active'=>'1',
		  'd_entry'=>$tgl,
		  'd_update'=>$tgl,
		  'id_kel_brg_jadi'=>$kel_brg_jadi
		);
		$db2->insert('tr_product_motif',$data2); 
	}

	else {			
		$data = array(
				  'e_product_basename'=>$nama
				);
		
		$db2->where('i_product_base',$kode);
		$db2->update('tr_product_base',$data);  
		
		$data = array(
				  'e_product_motifname'=>$nama,
				  'id_kel_brg_jadi'=>$kel_brg_jadi
				);
		$kode_brg_jadi = $kode."00";
		$db2->where('i_product_motif',$kode_brg_jadi);
		$db2->update('tr_product_motif',$data);  
	}
		
  }
  
  function delete($kode){    
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	  $kodebase = substr($kode, 0, 7);
    $db2->delete('tr_product_motif', array('i_product_motif' => $kode));
    $db2->delete('tr_product_base', array('i_product_base' => $kodebase));
  }
  
  // 28-01-2014 ----------------------------------------------------
  function getAllwarnabrgjaditanpalimit($cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')) ";
			
		$sqlnya = $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_warna_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif ".$filter."
							ORDER BY a.kode_brg_jadi ");
		return $sqlnya->result();  
	}
  
  function getAllwarnabrgjadi($limit,$offset, $cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')) ";
			
	  $sqlnya	= $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_warna_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif "
							.$filter." ORDER BY a.kode_brg_jadi LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailwarna = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				/*$queryxx = $this->db->query(" SELECT a.i_color, b.e_color_name FROM tr_product_color a, tr_color b
										WHERE a.i_color = b.i_color AND a.i_product_motif = '".$rownya->i_product_motif."' "); */
				$queryxx = $this->db->query(" SELECT a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
										WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$rownya->kode_brg_jadi."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailwarna[] = array(	'kode_warna'=> $rowxx->kode_warna,
												'nama_warna'=> $rowxx->nama
												);
					}
				}
				else
					$detailwarna = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->kode_brg_jadi,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailwarna'=> $detailwarna
								);
				
				$detailwarna = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	
	function get_warna(){
		$query = $this->db->query(" SELECT * FROM tm_warna ORDER BY kode");
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	
	function getbarangjaditanpalimit($cari){
		if ($cari == "all") {
			$sql = " select * FROM tr_product_motif ";
			$query = $this->db->query($sql);
			return $query->result();  
		}
		else {
			$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
						OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
			$query = $this->db->query($sql);
			return $query->result();  
		}
  }
  
  function getbarangjadi($num,$offset, $cari){    
    if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {									
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function savewarnabrgjadi($kode_brg_jadi,$kodewarna) {
		$tgl = date("Y-m-d H:i:s");
		
		$listwarna = explode(";", $kodewarna);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_warna = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_warna	= $hasilrow->id;
						$new_id_warna = $id_warna+1;
					}
					else
						$new_id_warna = 1;
					
					$datanya	= array(
							 'id'=>$new_id_warna,
							 'kode_brg_jadi'=>$kode_brg_jadi,
							 'kode_warna'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_warna_brg_jadi',$datanya);
				} // end if
			}
		} // end foreach
	}
	
	function deletewarnabrgjadi($kode){
		$this->db->query(" DELETE FROM tm_warna_brg_jadi WHERE kode_brg_jadi = '$kode' ");
	}
	
	function updatewarnabrgjadi($kode_brg_jadi,$ada, $kodewarna) {
		$tgl = date("Y-m-d H:i:s");
		
		if ($ada == 0)
			$this->db->query(" DELETE FROM tm_warna_brg_jadi WHERE kode_brg_jadi = '$kode_brg_jadi' ");
		
		$listwarna = explode(";", $kodewarna);
		foreach ($listwarna as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_warna = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$hasilsqlcek = $sqlcek->row();
					$query2	= $this->db->query(" SELECT id FROM tm_warna_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_warna	= $hasilrow->id;
						$new_id_warna = $id_warna+1;
					}
					else
						$new_id_warna = 1;
					
					$datanya	= array(
							 'id'=>$new_id_warna,
							 'kode_brg_jadi'=>$kode_brg_jadi,
							 'kode_warna'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_warna_brg_jadi',$datanya);
				} // end if
				else { // jika udh ada, maka update aja (sepertinya ga akan masuk kesini, soalnya jika datanya udh pernah dipake di SJ masuk/keluar maka ga bisa diedit sama sekali
					$this->db->query(" UPDATE tm_warna_brg_jadi SET kode_warna = '$row1' WHERE id='$hasilsqlcek->id' ");
				}
			}
		} // end foreach
	}
  //-----------------------------------------------------------------
  
  // 04-02-2014
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  // 05-02-2014
  function get_bahan($num, $offset, $cari, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't'  ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function savematerialbrgjadi($kode_brg_jadi,$kode_bhn_baku, $kode_warna, $qty_perpcs, $rumus_forecast, $nama_bhn_baku, $is_manual, $satuan_konv) {
		$tgl = date("Y-m-d H:i:s");
		
		$listbhn = explode(";", $kode_bhn_baku);
		$listwarna = explode(";", $kode_warna);
		$listqty_perpcs = explode(";", $qty_perpcs);
		$listrumus_forecast = explode(";", $rumus_forecast);
		$listnama_bhn_baku = explode(";", $nama_bhn_baku);
		$listis_manual = explode(";", $is_manual);
		$listsatuan_konv = explode(";", $satuan_konv);
		
		for ($i=0; $i<count($listbhn); $i++) {
			if (trim($listbhn[$i]!= '')) {
				//echo $listbhn[$i]." ".$listwarna[$i]."<br>";
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				if ($listis_manual[$i] == '') {
					$sqlcek	= $this->db->query(" SELECT id FROM tm_material_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '".$listbhn[$i]."' ");
					/*echo "SELECT id FROM tm_material_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '".$listbhn[$i]."'"; die(); */
					$nama_brg_forsave = "";
				}
				else {
					$sqlcek	= $this->db->query(" SELECT id FROM tm_material_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '".$listbhn[$i]."'
									 AND nama_brg_manual = '".$listnama_bhn_baku[$i]."' ");
					/*echo "SELECT id FROM tm_material_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '".$listbhn[$i]."'
									 AND nama_brg_manual = '".$listnama_bhn_baku[$i]."'"; die(); */
					$nama_brg_forsave = $listnama_bhn_baku[$i];
				}
							 
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_material_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_material	= $hasilrow->id;
						$new_id_material = $id_material+1;
					}
					else
						$new_id_material = 1;
					
					$datanya	= array(
							 'id'=>$new_id_material,
							 'kode_brg_jadi'=>$kode_brg_jadi,
							 'kode_brg'=>$listbhn[$i],
							 'kode_warna'=>$listwarna[$i],
							 'qty_perpcs_brgjadi'=>$listqty_perpcs[$i],
							 'rumus_forecast'=>$listrumus_forecast[$i],
							 'id_satuan_konversi'=>$listsatuan_konv[$i],
							 'nama_brg_manual'=>$nama_brg_forsave,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_material_brg_jadi',$datanya);
				} // end if
			}
		} // end foreach
	}
	
   function getAllmaterialbrgjaditanpalimit($cari) {
	   $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')) ";
			
		$sqlnya = $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_material_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif ".$filter."
							ORDER BY a.kode_brg_jadi ");
		return $sqlnya->result();  
	}
  
  function getAllmaterialbrgjadi($limit,$offset, $cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')) ";
			
	  $sqlnya	= $this->db->query(" SELECT distinct a.kode_brg_jadi, b.e_product_motifname 
							FROM tm_material_brg_jadi a, tr_product_motif b
							WHERE a.kode_brg_jadi = b.i_product_motif "
							.$filter." ORDER BY a.kode_brg_jadi LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailbrg = array();
			
			foreach ($hasilnya as $rownya) {
				// query ambil data2 warna berdasarkan kode brgnya
				$queryxx = $this->db->query(" SELECT kode_brg, kode_warna, qty_perpcs_brgjadi, rumus_forecast, nama_brg_manual, id_satuan_konversi 
											FROM tm_material_brg_jadi WHERE kode_brg_jadi = '".$rownya->kode_brg_jadi."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						if ($rowxx->nama_brg_manual == '') {
							// bhn baku/quilting
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
										FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
							if ($query3->num_rows() > 0){
								$quilting = 'f';
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$quilting = 't';
								$query31	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
									FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$rowxx->kode_brg' ");
								if ($query31->num_rows() > 0){
									$quilting = 't';
									$hasilrow31 = $query31->row();
									$nama_brg	= $hasilrow31->nama_brg;
									$satuan	= $hasilrow31->nama_satuan;
								}
								else {
									$nama_brg = '';
									$satuan = '';
									$quilting = 'f';
								}
							}
						} // end if
						else {
							$nama_brg	= $rowxx->nama_brg_manual;
							$satuan	= '';
							$quilting = 'f';
						}
						
						// warna
						$query3	= $this->db->query(" SELECT nama FROM tm_warna WHERE kode = '$rowxx->kode_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_warna	= $hasilrow->nama;
						}
						else
							$nama_warna = 'Tidak Ada';
						
						if ($rowxx->rumus_forecast == '0')
							$rumus_forecast = "standar (pengali x forecast)";
						else if ($rowxx->rumus_forecast == '1')
							$rumus_forecast = "bisbisan 4,5 (pengali x forecast/43/20*30)";
						else if ($rowxx->rumus_forecast == '2')
							$rumus_forecast = "bisbisan 2,8 (pengali x forecast/43)";
						else
							$rumus_forecast = "busa SS / RE (forecast/pengali)";
							
						// 08-11-2014
						if ($rowxx->id_satuan_konversi != 0) {
							$query3	= $this->db->query(" SELECT nama as nama_satuan 
											FROM tm_satuan WHERE id = '$rowxx->id_satuan_konversi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_satuan_konv	= $hasilrow->nama_satuan;
							}
						}
						else
							$nama_satuan_konv = "Tidak Ada";
						
						$detailbrg[] = array(	'kode_brg'=> $rowxx->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'quilting'=> $quilting,
												'kode_warna'=> $rowxx->kode_warna,
												'nama_warna'=> $nama_warna,
												'qty_perpcs_brgjadi'=> $rowxx->qty_perpcs_brgjadi,
												'rumus_forecast'=> $rumus_forecast,
												'nama_satuan_konv'=> $nama_satuan_konv
												);
					}
				}
				else
					$detailbrg = '';
					
				$outputdata[] = array(	'i_product_motif'=> $rownya->kode_brg_jadi,
										'e_product_motifname'=> $rownya->e_product_motifname,
										'detailbrg'=> $detailbrg
								);
				
				$detailbrg = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	
	function deletematerialbrgjadi($kode){
		$this->db->query(" DELETE FROM tm_material_brg_jadi WHERE kode_brg_jadi = '$kode' ");
	}
	
	function updatedatamaterialbrgjadi($kode_brg_jadi,$ada,$kode_bhn_baku, $kode_warna, $qty_perpcs, 
			$rumus_forecast, $nama_bhn_baku, $is_manual, $satuan_konv) {
		$tgl = date("Y-m-d H:i:s");
		
		// 24-10-2014 jika blm pernah dipake di transaksi bon m masuk hasil cutting, maka hapus dulu semuanya
		if ($ada == 0)
			$this->db->query(" DELETE FROM tm_material_brg_jadi WHERE kode_brg_jadi = '$kode_brg_jadi' ");
		
		$listbhn = explode(";", $kode_bhn_baku);
		$listwarna = explode(";", $kode_warna);
		$listqty_perpcs = explode(";", $qty_perpcs);
		$listrumus_forecast = explode(";", $rumus_forecast);
		$listnama_bhn_baku = explode(";", $nama_bhn_baku);
		$listis_manual = explode(";", $is_manual);
		$listsatuan_konv = explode(";", $satuan_konv);
		
		for ($i=0; $i<count($listbhn); $i++) {
			if (trim($listbhn[$i]!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				if ($listis_manual[$i] == '') {
					$sqlcek	= $this->db->query(" SELECT id FROM tm_material_brg_jadi 
										 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '".$listbhn[$i]."' ");
					$nama_brg_forsave = "";
				}
				else {
					$sqlcek	= $this->db->query(" SELECT id FROM tm_material_brg_jadi 
									 WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '".$listbhn[$i]."'
									 AND nama_brg_manual = '".$listnama_bhn_baku[$i]."' ");
					$nama_brg_forsave = $listnama_bhn_baku[$i];
				}
				
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_material_brg_jadi ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_material	= $hasilrow->id;
						$new_id_material = $id_material+1;
					}
					else
						$new_id_material = 1;
					
					$datanya	= array(
							 'id'=>$new_id_material,
							 'kode_brg_jadi'=>$kode_brg_jadi,
							 'kode_brg'=>$listbhn[$i],
							 'kode_warna'=>$listwarna[$i],
							 'qty_perpcs_brgjadi'=>$listqty_perpcs[$i],
							 'rumus_forecast'=>$listrumus_forecast[$i],
							 'id_satuan_konversi'=>$listsatuan_konv[$i],
							 'nama_brg_manual'=>$nama_brg_forsave,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_material_brg_jadi',$datanya);
				} // end if
				else {
					if ($listis_manual[$i] == '') {
						$this->db->query(" UPDATE tm_material_brg_jadi SET qty_perpcs_brgjadi='".$listqty_perpcs[$i]."',
									kode_warna = '".$listwarna[$i]."', rumus_forecast = '".$listrumus_forecast[$i]."',
									nama_brg_manual = '".$nama_brg_forsave."', id_satuan_konversi = '".$listsatuan_konv[$i]."'
									WHERE kode_brg='".$listbhn[$i]."' AND kode_brg_jadi='".$kode_brg_jadi."' ");
					}
					else {
						$this->db->query(" UPDATE tm_material_brg_jadi SET qty_perpcs_brgjadi='".$listqty_perpcs[$i]."',
									kode_warna = '".$listwarna[$i]."', rumus_forecast = '".$listrumus_forecast[$i]."'
									WHERE kode_brg = '00' AND nama_brg_manual = '".$nama_brg_forsave."',
									id_satuan_konversi = '".$listsatuan_konv[$i]."' 
									AND kode_brg_jadi='".$kode_brg_jadi."' ");
					}
				}
			}
		} // end foreach
	}
	
	// 23-05-2014
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  /*function get_stok_unit_jahit($unit_jahit) {
	$sql = " SELECT a.kode_brg_jadi, b.e_product_motifname as nama_brg_jadi 
			FROM tm_stok_unit_jahit a, tr_product_motif b WHERE a.kode_brg_jadi=b.i_product_motif 
			AND a.kode_unit='$unit_jahit' ORDER BY a.kode_brg_jadi ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT harga FROM tm_hpp_wip WHERE kode_brg_jadi = '$row1->kode_brg_jadi'
									AND kode_unit = '$unit_jahit' ");
			if ($query3->num_rows() == 0){
				$harga	= '';
			}
			else {
				$hasilrow = $query3->row();
				$harga	= $hasilrow->harga;
			}
				$data_harga[] = array(			
								'kode_brg_jadi'=> $row1->kode_brg_jadi,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  } */
  
  function cek_data_hpp($kode_brg_jadi){
    $this->db->select("id from tm_hpp_wip WHERE kode_brg_jadi = '$kode_brg_jadi' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savehpp($kode_brg_jadi, $harga){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
    
    if ($harga != 0) {
		$data = array(
		  'kode_brg_jadi'=>$kode_brg_jadi,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_hpp_wip',$data); 
	}
  }
  
  function deletehpp($id){    
    $this->db->delete('tm_hpp_wip', array('id' => $id));
  }
  
  function getAllhpp($num, $offset, $cari)
  {
	$filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR 
			UPPER(c.e_product_motifname) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.e_product_motifname FROM tm_hpp_wip a, tr_product_motif c 
			WHERE a.kode_brg_jadi = c.i_product_motif ".$filter." ORDER BY a.kode_brg_jadi ", false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllhpptanpalimit($cari){
	  $filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$cari%') OR 
			UPPER(c.e_product_motifname) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.*, c.e_product_motifname FROM tm_hpp_wip a, tr_product_motif c 
			WHERE a.kode_brg_jadi = c.i_product_motif ".$filter, false);
			
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get_hpp($idharga){
	$query = $this->db->query(" SELECT * FROM tm_hpp_wip WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			// ambil data nama barang jadi
			$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row1->kode_brg_jadi' ");
			$hasilrow = $query3->row();
			$nama_brg_jadi	= $hasilrow->e_product_motifname;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }
  
  // 30-05-2014
  function get_hpp_brgjadi() {
	$sql = " SELECT a.kode_brg_jadi, b.e_product_motifname as nama_brg_jadi, a.harga 
			FROM tm_hpp_wip a, tr_product_motif b WHERE a.kode_brg_jadi=b.i_product_motif 
			ORDER BY a.kode_brg_jadi ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$data_harga[] = array(			
								'kode_brg_jadi'=> $row1->kode_brg_jadi,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $row1->harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
  
  // 23-04-2015
  function getAllkelbrgjadi(){
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$query = $db2->query(" SELECT * FROM tm_kel_brg_jadi ORDER BY kode ");    
    return $query->result();
  }
  
  function get_kel_brg_jadi($id){
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$query = $db2->query(" SELECT * FROM tm_kel_brg_jadi WHERE id='$id' ");
    return $query->result();
  }
  
  function save_kel_brg_jadi($kode, $kodeedit, $nama, $ket, $goedit){  
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
    
    $tgl = date("Y-m-d H:i:s");
    $data = array(
      'kode'=>$kode,
      'nama'=>$nama,
      'keterangan'=>$ket,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$db2->insert('tm_kel_brg_jadi',$data); }
	else {
		
		$data = array(
		  'kode'=>$kode,
		  'nama'=>$nama,
		  'keterangan'=>$ket,
		  'tgl_update'=>$tgl
		);
		
		$db2->where('id',$kodeedit);
		$db2->update('tm_kel_brg_jadi',$data);  
	}
		
  }
  
  function delete_kel_brg_jadi($id){    
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
    $db2->delete('tm_kel_brg_jadi', array('id' => $id));
  }
  
  function cek_data_kel_brg_jadi($kode){
	  // Database External
	$db2 = $this->load->database('db_external', TRUE);
    $db2->select("* FROM tm_kel_brg_jadi WHERE kode = '$kode' ", false);
    $query = $db2->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 08-11-2014
  function get_satuan(){
    $query = $this->db->query(" SELECT * FROM tm_satuan ORDER BY id");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
