<?php
class Mreport extends CI_Model{
  function Mreport() {

  parent::__construct();
  }
  
  // 18-04-2015
  function get_pelanggan(){
	  // Database External
	  $db2 = $this->load->database('db_external', TRUE);
	  
      $query = $db2->query(" SELECT i_customer, i_customer_code, e_customer_name FROM tr_customer ORDER BY i_customer_code");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
   function get_brand(){
	  // Database External
	  $db2 = $this->load->database('db_external', TRUE);
	  
      $query = $db2->query(" SELECT * FROM tr_brand ORDER BY i_brand");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
  function get_perusahaan() {
	  $db2 = $this->load->database('db_external', TRUE);
	  $sql="SELECT * from tr_initial_company";
	  $query=$db2->query($sql);
	  if($query ->num_rows() > 0){
		  return $query->result_array();
		  }
	  
  }
  
  function get_laprealisasibrand_old($date_from, $date_to, $i_brand, $is_stp) {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
	$blndari = $bln1;
	$tahundari = $thn1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
					
	$sql = " SELECT distinct e.i_product_base, c.i_product_motif, c.e_product_motifname, d.i_brand ,c_brand_code
					FROM tm_op a INNER JOIN tm_op_item b ON a.i_op = b.i_op
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.i_product
					INNER JOIN tr_product_base e ON e.i_product_base = c.i_product
					LEFT JOIN tr_brand d ON d.i_brand = e.i_brand
					WHERE (a.d_op BETWEEN '".$tgldari."' AND '".$tglke."') 
					AND a.f_op_cancel='f' ";
				if ($i_brand != 0)
					$sql.= " AND a.i_brand = '".$i_brand."' ";
				if ($is_stp == 0)
					$sql.= " AND e.f_stop_produksi = 'f' ";
				else
					$sql.= " AND e.f_stop_produksi = 't' ";
	$sql.="		UNION
			SELECT distinct e.i_product_base, c.i_product_motif, c.e_product_motifname, d.i_brand  ,c_brand_code
				FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b ON a.id=b.id_forecast_distributor
				INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
				INNER JOIN tr_product_base e ON e.i_product_base = c.i_product
				LEFT JOIN tr_brand d ON d.i_brand = e.i_brand
				WHERE a.bulan='$blndari' AND a.tahun='$tahundari' ";
	
				if ($i_brand != 0)
					$sql.= " AND d.i_brand = '".$i_brand."' ";
				if ($is_stp == 0)
					$sql.= " AND e.f_stop_produksi = 'f' ";
				else
					$sql.= " AND e.f_stop_produksi = 't' ";
	$sql.= " ORDER BY i_brand, i_product_motif ";
	
	$query = $db2->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				$i_product_base = $row->i_product_base;
				$i_product_motif = $row->i_product_motif;
				$e_product_motifname = $row->e_product_motifname;
				
				// 1. ambil data HJP
				$queryx	= $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='".$i_product_motif."'  ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$v_price = $hasilx->v_price;
					if ($v_price == '')
						$v_price = 0;
				}
				else {
					$queryx2	= $db2->query(" SELECT v_unitprice FROM tr_product_base WHERE i_product_base='".$i_product_base."'  ");
					if ($queryx2->num_rows() > 0){
						$hasilx2 = $queryx2->row();
						$v_price = $hasilx2->v_unitprice;
						if ($v_price == '')
							$v_price = 0;
					}
					else
						$v_price = 0;
				} // end else
				
				// ambil data FC
				if ($i_brand != 0) {
					$queryx	= $db2->query(" SELECT b.qty FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
									ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
									AND a.bulan='".$blndari."' AND a.tahun='".$tahundari."'
									AND a.i_customer='".$i_customer."'  ");
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$fc = $hasilx->qty;
						if ($fc == '')
							$fc = 0;
					}
					else
						$fc=0;
				}
				else {
					$queryx	= $db2->query(" SELECT sum(b.qty) as jum FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
									ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
									AND a.bulan='".$blndari."' AND a.tahun='".$tahundari."' ");
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$fc = $hasilx->jum;
						if ($fc == '')
							$fc = 0;
					}
					else
						$fc=0;
				}
				
				// 2. ambil data jumlah OP di range tgl
				$sqlop=" SELECT sum(n_count) as jumop FROM tm_op a INNER JOIN tm_op_item b ON a.i_op=b.i_op
						WHERE (a.d_op BETWEEN '".$tgldari."' AND '".$tglke."') AND b.i_product='".$i_product_motif."'
						AND a.f_op_cancel='f' ";
				if ($i_brand != 0)
					$sqlop.=" AND a.i_customer='".$i_customer."' ";
					
				$queryx	= $db2->query($sqlop);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_op = $hasilx->jumop;
					if ($jum_op == '')
						$jum_op = 0;
				}
				else
					$jum_op = 0;
				
				$jum_op_rp = $jum_op*$v_price;
				
				// 3. ambil data jumlah DO di range tgl
				$sqldo = " SELECT sum(n_deliver) as jumdo FROM tm_do a INNER JOIN tm_do_item b ON a.i_do=b.i_do
							WHERE (a.d_do BETWEEN '".$tgldari."' AND '".$tglke."') AND b.i_product='".$i_product_motif."'
							AND a.f_do_cancel='f' 
							AND b.i_op IN (SELECT i_op FROM tm_op WHERE d_op BETWEEN '".$tgldari."' AND '".$tglke."') ";
				if ($i_brand != 0)
					$sqldo.=" AND a.i_customer='".$i_customer."' ";				
				$queryx	= $db2->query($sqldo);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_do = $hasilx->jumdo;
					if ($jum_do == '')
						$jum_do = 0;
				}
				else
					$jum_do = 0;
				
				
				$jum_do_rp = $jum_do*$v_price;
				
				$pendingan = $jum_op-$jum_do;
				$pendingan_rp = $pendingan*$v_price;
				
				if ($jum_op != 0)
					$persendovsop= ($jum_do/$jum_op)*100;
				else
					$persendovsop = '';
				
				if ($fc != 0) {
					$persenopvsfc= ($jum_op/$fc)*100;
					$persendovsfc= ($jum_do/$fc)*100;
				}
				else {
					$persenopvsfc = '';
					$persendovsfc = '';
				}
				
						
				//ambil nama kel brg jadi
				$sqlxx = " SELECT e_brand_name FROM tr_brand WHERE i_brand='$row->i_brand' ";
				$queryxx	= $db2->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_brand = $hasilxx->e_brand_name;
				}
				else {
					$nama_brand = '';
				}
					$dropping= $fc-$jum_op;
					$dropping_rp= $dropping*$v_price;
				
				$datadetail[] = array( 
										'kode_brg_jadi'=> $row->i_product_motif,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'hjp'=> $v_price,
										'fc'=> $fc,
										'jum_op'=> $jum_op,
										'jum_op_rp'=> $jum_op_rp,
										'jum_do'=> $jum_do,
										'jum_do_rp'=> $jum_do_rp,
										'pendingan'=> $pendingan,
										'pendingan_rp'=> $pendingan_rp,
										'persendovsop'=>$persendovsop,
										'persenopvsfc'=>$persenopvsfc,
										'persendovsfc'=>$persendovsfc,
										'kode_brand'=> $row->c_brand_code,
										'nama_brand'=> $nama_brand,
										'dropping'=>$dropping,
										'dropping_rp'=>$dropping_rp
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
   function get_laprealisasi_brand($date_from, $date_to, $i_brand, $is_stp) {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
	$blndari = $bln1;
	$tahundari = $thn1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	if ($i_brand != 0){
	$sql = " SELECT * FROM f_forecast_op_do_brand('$tgldari','$tglke') WHERE 'T' 
	 AND i_brand = '".$i_brand."' ";
				if ($is_stp == 0)
					$sql.= " AND f_stop_produksi = 'f' ";
				else
					$sql.= " AND f_stop_produksi = 't' ";
				}
				else{
		$sql = " SELECT * FROM f_forecast_op_do_brkel_all('$tgldari','$tglke') WHERE 'T'";		
	if ($is_stp == 0)
					$sql.= " AND f_stop_produksi = 'f' ";
				else
					$sql.= " AND f_stop_produksi = 't' ";
				}
	
	$query = $db2->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$datadetail = array();
			foreach ($hasil as $row)
      {
		  $dropping=($row->n_forecast-$row->n_op);
		  $dropping_rp= ($dropping*$row->v_price);
				$datadetail[] = array( 
										'kode_brg_jadi'=> $row->i_product,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'hjp'=> $row->v_price,
										'fc'=> $row->n_forecast,
										'jum_op'=> $row->n_op,
										'jum_op_rp'=> $row->v_op,
										'jum_do'=> $row->n_do,
										'jum_do_rp'=> $row->v_do,
										'pendingan'=> $row->n_op_do,
										'pendingan_rp'=> $row->v_op_do,
										'persendovsop'=>$row->n_persendo_op,
										'persenopvsfc'=>$row->n_persenop_fc,
										'persendovsfc'=>$row->n_persendo_fc,
										'kode_brand'=> $row->kode_brand,
										'nama_brand'=> $row->nama_brand,
										'kode_kel'=> $row->kode_kel,
										'nama_kel'=> $row->nama_kel,
										'dropping'=>$dropping,
										'dropping_rp'=> $dropping_rp
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
   function get_laprealisasi_old($date_from, $date_to, $i_customer, $is_stp) {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
	$blndari = $bln1;
	$tahundari = $thn1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	/*SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_detail b 
					INNER JOIN tm_forecast_wip a ON b.id_forecast_wip = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY d.kode, b.kode_brg_jadi */
					
	$sql = " SELECT distinct e.i_product_base, c.i_product_motif, c.e_product_motifname, d.kode 
					FROM tm_op a INNER JOIN tm_op_item b ON a.i_op = b.i_op
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.i_product
					INNER JOIN tr_product_base e ON e.i_product_base = c.i_product
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE (a.d_op BETWEEN '".$tgldari."' AND '".$tglke."') 
					AND a.f_op_cancel='f' ";
				if ($i_customer != 0)
					$sql.= " AND a.i_customer = '".$i_customer."' ";
				if ($is_stp == 0)
					$sql.= " AND e.f_stop_produksi = 'f' ";
				else
					$sql.= " AND e.f_stop_produksi = 't' ";
	$sql.="		UNION
			SELECT distinct e.i_product_base, c.i_product_motif, c.e_product_motifname, d.kode 
				FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b ON a.id=b.id_forecast_distributor
				INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
				INNER JOIN tr_product_base e ON e.i_product_base = c.i_product
				LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
				WHERE a.bulan='$blndari' AND a.tahun='$tahundari' ";
	
				if ($i_customer != 0)
					$sql.= " AND a.i_customer = '".$i_customer."' ";
				if ($is_stp == 0)
					$sql.= " AND e.f_stop_produksi = 'f' ";
				else
					$sql.= " AND e.f_stop_produksi = 't' ";
	$sql.= " ORDER BY kode, i_product_motif ";
	
	$query = $db2->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				$i_product_base = $row->i_product_base;
				$i_product_motif = $row->i_product_motif;
				$e_product_motifname = $row->e_product_motifname;
				
				// 1. ambil data HJP
				$queryx	= $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='".$i_product_motif."'  ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$v_price = $hasilx->v_price;
					if ($v_price == '')
						$v_price = 0;
				}
				else {
					$queryx2	= $db2->query(" SELECT v_unitprice FROM tr_product_base WHERE i_product_base='".$i_product_base."'  ");
					if ($queryx2->num_rows() > 0){
						$hasilx2 = $queryx2->row();
						$v_price = $hasilx2->v_unitprice;
						if ($v_price == '')
							$v_price = 0;
					}
					else
						$v_price = 0;
				} // end else
				
				// ambil data FC
				if ($i_customer != 0) {
					$queryx	= $db2->query(" SELECT b.qty FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
									ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
									AND a.bulan='".$blndari."' AND a.tahun='".$tahundari."'
									AND a.i_customer='".$i_customer."'  ");
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$fc = $hasilx->qty;
						if ($fc == '')
							$fc = 0;
					}
					else
						$fc=0;
				}
				else {
					$queryx	= $db2->query(" 1SELECT sum(b.qty) as jum FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
									ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
									AND a.bulan='".$blndari."' AND a.tahun='".$tahundari."' ");
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$fc = $hasilx->jum;
						if ($fc == '')
							$fc = 0;
					}
					else
						$fc=0;
				}
				
				// 2. ambil data jumlah OP di range tgl
				$sqlop=" SELECT sum(n_count) as jumop FROM tm_op a INNER JOIN tm_op_item b ON a.i_op=b.i_op
						WHERE (a.d_op BETWEEN '".$tgldari."' AND '".$tglke."') AND b.i_product='".$i_product_motif."'
						AND a.f_op_cancel='f' ";
				if ($i_customer != 0)
					$sqlop.=" AND a.i_customer='".$i_customer."' ";
					
				$queryx	= $db2->query($sqlop);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_op = $hasilx->jumop;
					if ($jum_op == '')
						$jum_op = 0;
				}
				else
					$jum_op = 0;
				
				$jum_op_rp = $jum_op*$v_price;
				
				// 3. ambil data jumlah DO di range tgl
				$sqldo = " SELECT sum(n_deliver) as jumdo FROM tm_do a INNER JOIN tm_do_item b ON a.i_do=b.i_do
							WHERE (a.d_do BETWEEN '".$tgldari."' AND '".$tglke."') AND b.i_product='".$i_product_motif."'
							AND a.f_do_cancel='f' 
							AND b.i_op IN (SELECT i_op FROM tm_op WHERE d_op BETWEEN '".$tgldari."' AND '".$tglke."') ";
				if ($i_customer != 0)
					$sqldo.=" AND a.i_customer='".$i_customer."' ";				
				$queryx	= $db2->query($sqldo);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_do = $hasilx->jumdo;
					if ($jum_do == '')
						$jum_do = 0;
				}
				else
					$jum_do = 0;
				
				$jum_do_rp = $jum_do*$v_price;
				
				$pendingan = $jum_op-$jum_do;
				$pendingan_rp = $pendingan*$v_price;
				
				if ($jum_op != 0)
					$persendovsop= ($jum_do/$jum_op)*100;
				else
					$persendovsop = '';
				
				if ($fc != 0) {
					$persenopvsfc= ($jum_op/$fc)*100;
					$persendovsfc= ($jum_do/$fc)*100;
				}
				else {
					$persenopvsfc = '';
					$persendovsfc = '';
				}
				
						
				//ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $db2->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
					$dropping= $jum_op-$fc;
					$dropping_rp= $dropping*$v_price;
				
				$datadetail[] = array( 
										'kode_brg_jadi'=> $row->i_product_motif,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'hjp'=> $v_price,
										'fc'=> $fc,
										'jum_op'=> $jum_op,
										'jum_op_rp'=> $jum_op_rp,
										'jum_do'=> $jum_do,
										'jum_do_rp'=> $jum_do_rp,
										'pendingan'=> $pendingan,
										'pendingan_rp'=> $pendingan_rp,
										'persendovsop'=>$persendovsop,
										'persenopvsfc'=>$persenopvsfc,
										'persendovsfc'=>$persendovsfc,
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel,
										'dropping'=>$dropping,
										'dropping_rp'=>$dropping_rp
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  // 20-04-2015
  function get_laprealisasi($date_from, $date_to, $i_customer, $is_stp) {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
	$blndari = $bln1;
	$tahundari = $thn1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	if ($i_customer != 0){
	$sql = " SELECT * FROM f_forecast_op_do_brkel('$tgldari','$tglke') WHERE 'T' 
	 AND i_customer = '".$i_customer."' ";
				if ($is_stp == 0)
					$sql.= " AND f_stop_produksi = 'f' ";
				else
					$sql.= " AND f_stop_produksi = 't' ";
				}
				else{
		$sql = " SELECT * FROM f_forecast_op_do_brkel_all('$tgldari','$tglke') WHERE 'T'";		
	if ($is_stp == 0)
					$sql.= " AND f_stop_produksi = 'f' ";
				else
					$sql.= " AND f_stop_produksi = 't' ";
				}
	
	$query = $db2->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$datadetail = array();
			foreach ($hasil as $row)
      {
		  
		  $jum_op_rp=(1*$row->v_op);
		  $dropping=($row->n_forecast-$row->n_op);
		  $dropping_rp= ($dropping*$row->v_price);
		  
		  	if ($row->n_persendo_op == 0)
			$row->n_persendo_op = '';
				
			if ($row->n_persenop_fc == 0)
			$row->n_persenop_fc = '';
		  
		  	if ($row->n_persendo_fc == 0)
			$row->n_persendo_fc = '';
		  
				$datadetail[] = array( 
										'kode_brg_jadi'=> $row->i_product,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'hjp'=> $row->v_price,
										'fc'=> $row->n_forecast,
										'jum_op'=> $row->n_op,
										'jum_op_rp'=> $row->v_op,
										'jum_do'=> $row->n_do,
										'jum_do_rp'=> $row->v_do,
										'pendingan'=> $row->n_op_do,
										'pendingan_rp'=> $row->v_op_do,
										'persendovsop'=>$row->n_persendo_op,
										'persenopvsfc'=>$row->n_persenop_fc,
										'persendovsfc'=>$row->n_persendo_fc,
										'kode_kel'=> $row->kode_kel,
										'nama_kel'=> $row->nama_kel,
										'kode_brand'=> $row->kode_brand,
										'nama_brand'=> $row->nama_brand,
										'dropping'=>$dropping,
										'dropping_rp'=> $dropping_rp
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  function get_laprealisasi_y($date_from, $date_to, $i_customer, $is_stp) {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
	$blndari = $bln1;
	$tahundari = $thn1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	if ($i_customer != 0){
	$sql = " SELECT * FROM f_forecast_op_do('$tgldari','$tglke') WHERE 'T' 
	 AND i_customer = '".$i_customer."' ";
				if ($is_stp == 0)
					$sql.= " AND f_stop_produksi = 'f' ";
				else
					$sql.= " AND f_stop_produksi = 't' ";
				}
				else{
		$sql = " SELECT * FROM f_forecast_op_do_all('$tgldari','$tglke') WHERE 'T'";		
	if ($is_stp == 0)
					$sql.= " AND f_stop_produksi = 'f' ";
				else
					$sql.= " AND f_stop_produksi = 't' ";
				}
	
	$query = $db2->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$datadetail = array();
			foreach ($hasil as $row)
      {
		  $dropping=($row->n_forecast-$row->n_op);
		  $dropping_rp= ($dropping*$row->v_price);
				$datadetail[] = array( 
										'kode_brg_jadi'=> $row->i_product,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'hjp'=> $row->v_price,
										'fc'=> $row->n_forecast,
										'jum_op'=> $row->n_op,
										'jum_op_rp'=> $row->v_op,
										'jum_do'=> $row->n_do,
										'jum_do_rp'=> $row->v_do,
										'pendingan'=> $row->n_op_do,
										'pendingan_rp'=> $row->v_op_do,
										'persendovsop'=>$row->n_persendo_op,
										'persenopvsfc'=>$row->n_persenop_fc,
										'persendovsfc'=>$row->n_persendo_fc,
										'kode_kel'=> $row->kode_kel,
										'nama_kel'=> $row->nama_kel,
										'dropping'=>$dropping,
										'dropping_rp'=> $dropping_rp
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  function get_laprealisasi_x($date_from, $date_to, $i_customer, $is_stp) {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
	$blndari = $bln1;
	$tahundari = $thn1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	/*SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_detail b 
					INNER JOIN tm_forecast_wip a ON b.id_forecast_wip = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY d.kode, b.kode_brg_jadi */
					
	$sql = " SELECT distinct e.i_product_base, c.i_product_motif, c.e_product_motifname, d.kode 
					FROM tm_op a INNER JOIN tm_op_item b ON a.i_op = b.i_op
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.i_product
					INNER JOIN tr_product_base e ON e.i_product_base = c.i_product
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE (a.d_op BETWEEN '".$tgldari."' AND '".$tglke."') 
					AND a.f_op_cancel='f' ";
				if ($i_customer != 0)
					$sql.= " AND a.i_customer = '".$i_customer."' ";
				if ($is_stp == 0)
					$sql.= " AND e.f_stop_produksi = 'f' ";
				else
					$sql.= " AND e.f_stop_produksi = 't' ";
	$sql.="		UNION
			SELECT distinct e.i_product_base, c.i_product_motif, c.e_product_motifname, d.kode 
				FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b ON a.id=b.id_forecast_distributor
				INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
				INNER JOIN tr_product_base e ON e.i_product_base = c.i_product
				LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
				WHERE a.bulan='$blndari' AND a.tahun='$tahundari' ";
	
				if ($i_customer != 0)
					$sql.= " AND a.i_customer = '".$i_customer."' ";
				if ($is_stp == 0)
					$sql.= " AND e.f_stop_produksi = 'f' ";
				else
					$sql.= " AND e.f_stop_produksi = 't' ";
	$sql.= " ORDER BY kode, i_product_motif ";
	
	$query = $db2->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				$i_product_base = $row->i_product_base;
				$i_product_motif = $row->i_product_motif;
				$e_product_motifname = $row->e_product_motifname;
				
				// 1. ambil data HJP
				$queryx	= $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='".$i_product_motif."'  ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$v_price = $hasilx->v_price;
					if ($v_price == '')
						$v_price = 0;
				}
				else {
					$queryx2	= $db2->query(" SELECT v_unitprice FROM tr_product_base WHERE i_product_base='".$i_product_base."'  ");
					if ($queryx2->num_rows() > 0){
						$hasilx2 = $queryx2->row();
						$v_price = $hasilx2->v_unitprice;
						if ($v_price == '')
							$v_price = 0;
					}
					else
						$v_price = 0;
				} // end else
				
				// ambil data FC
				if ($i_customer != 0) {
					$queryx	= $db2->query(" SELECT b.qty FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
									ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
									AND a.bulan='".$blndari."' AND a.tahun='".$tahundari."'
									AND a.i_customer='".$i_customer."'  ");
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$fc = $hasilx->qty;
						if ($fc == '')
							$fc = 0;
					}
					else
						$fc=0;
				}
				else {
					$queryx	= $db2->query(" SELECT sum(b.qty) as jum FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
									ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
									AND a.bulan='".$blndari."' AND a.tahun='".$tahundari."' ");
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$fc = $hasilx->jum;
						if ($fc == '')
							$fc = 0;
					}
					else
						$fc=0;
				}
				
				// 2. ambil data jumlah OP di range tgl
				$sqlop=" SELECT sum(n_count) as jumop FROM tm_op a INNER JOIN tm_op_item b ON a.i_op=b.i_op
						WHERE (a.d_op BETWEEN '".$tgldari."' AND '".$tglke."') AND b.i_product='".$i_product_motif."'
						AND a.f_op_cancel='f' ";
				if ($i_customer != 0)
					$sqlop.=" AND a.i_customer='".$i_customer."' ";
					
				$queryx	= $db2->query($sqlop);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_op = $hasilx->jumop;
					if ($jum_op == '')
						$jum_op = 0;
				}
				else
					$jum_op = 0;
				
				$jum_op_rp = $jum_op*$v_price;
				
				// 3. ambil data jumlah DO di range tgl
				$sqldo = " SELECT sum(n_deliver) as jumdo FROM tm_do a INNER JOIN tm_do_item b ON a.i_do=b.i_do
							WHERE (a.d_do BETWEEN '".$tgldari."' AND '".$tglke."') AND b.i_product='".$i_product_motif."'
							AND a.f_do_cancel='f' 
							AND b.i_op IN (SELECT i_op FROM tm_op WHERE d_op BETWEEN '".$tgldari."' AND '".$tglke."') ";
				if ($i_customer != 0)
					$sqldo.=" AND a.i_customer='".$i_customer."' ";				
				$queryx	= $db2->query($sqldo);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_do = $hasilx->jumdo;
					if ($jum_do == '')
						$jum_do = 0;
				}
				else
					$jum_do = 0;
				
				$jum_do_rp = $jum_do*$v_price;
				
				$pendingan = $jum_op-$jum_do;
				$pendingan_rp = $pendingan*$v_price;
				
				if ($jum_op != 0)
					$persendovsop= ($jum_do/$jum_op)*100;
				else
					$persendovsop = '';
				
				if ($fc != 0) {
					$persenopvsfc= ($jum_op/$fc)*100;
					$persendovsfc= ($jum_do/$fc)*100;
				}
				else {
					$persenopvsfc = '';
					$persendovsfc = '';
				}
				
						
				//ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $db2->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
				
				$datadetail[] = array( 
										'kode_brg_jadi'=> $row->i_product_motif,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'hjp'=> $v_price,
										'fc'=> $fc,
										'jum_op'=> $jum_op,
										'jum_op_rp'=> $jum_op_rp,
										'jum_do'=> $jum_do,
										'jum_do_rp'=> $jum_do_rp,
										'pendingan'=> $pendingan,
										'pendingan_rp'=> $pendingan_rp,
										'persendovsop'=>$persendovsop,
										'persenopvsfc'=>$persenopvsfc,
										'persendovsfc'=>$persendovsfc,
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }

  // 22-04-2015
  function cek_forecast_distributor($bulan, $tahun, $i_customer) {
	  // Database External
	  $db2 = $this->load->database('db_external', TRUE);
	  
	$sql = " SELECT id FROM tm_forecast_distributor WHERE bulan = '$bulan' AND tahun = '$tahun'
			 AND i_customer = '$i_customer' ";
	
	$query3	= $db2->query($sql);
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_forecast_distributor($bulan, $tahun, $i_customer) {
	  // Database External
	  $db2 = $this->load->database('db_external', TRUE);
	  
		$query	= $db2->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_distributor_detail b 
					INNER JOIN tm_forecast_distributor a ON b.id_forecast_distributor = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.i_customer = '$i_customer'
					ORDER BY b.id ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function saveforecastdistributor($is_new, $id_fc, $iddetail, $kode_brg_jadi, $fc, $ket){ 
	  // Database External
	  $db2 = $this->load->database('db_external', TRUE);
	  
	  $tgl = date("Y-m-d H:i:s"); 
	 // 02-04-2014, ga pake is_new lagi. langsung save aja setelah delete detail sebelumnya
	//  if ($is_new == '1') {
		  $seq	= $db2->query(" SELECT id FROM tm_forecast_distributor_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailx	= $seqrow->id+1;
			}else{
				$iddetailx	= 1;
			}
		  
		   $data_detail = array(
						'id'=>$iddetailx,
						'id_forecast_distributor'=>$id_fc,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'qty'=>$fc,
						'keterangan'=>$ket
					);
		   $db2->insert('tm_forecast_distributor_detail',$data_detail);
	/*  }
	  else {
		  $iddetailx = $iddetail;
		  
		  $this->db->query(" UPDATE tm_forecast_wip_detail SET qty = '$fc', keterangan='$ket'
						where id = '$iddetailx' ");
			// ====================
	  } */
  }
  
  // 27-04-2015
  function get_laprealisasiperiode($bulan1, $bulan2, $tahun, $i_customer, $is_stp) {
	// Database External
	$db2 = $this->load->database('db_external', TRUE);
	
	$timeStamp            =    mktime(0,0,0,$bulan2,1,$tahun);    //Create time stamp of the first day from the give date.
	$firstDay            =     date('d',$timeStamp);    //get first day of the given month
	list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
	$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
	$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
							
	//AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
	$tgldari = $tahun."-".$bulan1."-01";
	$tglke = $tahun."-".$bulan2."-".$lastDay;
	
	//echo $tgldari; die();
	
	$sql = " SELECT distinct e.i_product_base, c.i_product_motif, c.e_product_motifname, d.kode 
					FROM tm_op a INNER JOIN tm_op_item b ON a.i_op = b.i_op
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.i_product
					INNER JOIN tr_product_base e ON e.i_product_base = c.i_product
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE (a.d_op BETWEEN '".$tgldari."' AND '".$tglke."') 
					AND a.f_op_cancel='f' ";
	
	if ($i_customer != 0)
		$sql.= " AND a.i_customer = '".$i_customer."' ";
	if ($is_stp == 0)
		$sql.= " AND e.f_stop_produksi = 'f' ";
	else
		$sql.= " AND e.f_stop_produksi = 't' ";
	$sql.= " ORDER BY d.kode, c.i_product_motif ";
	
	$query = $db2->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				$i_product_base = $row->i_product_base;
				$i_product_motif = $row->i_product_motif;
				$e_product_motifname = $row->e_product_motifname;
								
				for ($xx=$bulan1; $xx<=$bulan2; $xx++) {
					if (strlen($xx) < 2)
						$bulanxx = "0".$xx;
					else
						$bulanxx = $xx;
						
					$timeStamp            =    mktime(0,0,0,$bulanxx,1,$tahun);    //Create time stamp of the first day from the give date.
					$firstDay            =     date('d',$timeStamp);    //get first day of the given month
					list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
					$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
					$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
					
					$tgldarixx = $tahun."-".$bulanxx."-01";
					$tglkexx = $tahun."-".$bulanxx."-".$lastDay;
						
					// ambil data FC
					if ($i_customer != 0) {
						$queryx	= $db2->query(" SELECT b.qty FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
										ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
										AND a.bulan='".$bulanxx."' AND a.tahun='".$tahun."'
										AND a.i_customer='".$i_customer."'  ");
						if ($queryx->num_rows() > 0){
							$hasilx = $queryx->row();
							$fc = $hasilx->qty;
							if ($fc == '')
								$fc = 0;
						}
						else
							$fc=0;
					}
					else {
						$queryx	= $db2->query(" SELECT sum(b.qty) as jum FROM tm_forecast_distributor a INNER JOIN tm_forecast_distributor_detail b 
										ON a.id = b.id_forecast_distributor WHERE b.kode_brg_jadi='".$i_product_motif."'
										AND a.bulan='".$bulanxx."' AND a.tahun='".$tahun."' ");
						if ($queryx->num_rows() > 0){
							$hasilx = $queryx->row();
							$fc = $hasilx->jum;
							if ($fc == '')
								$fc = 0;
						}
						else
							$fc=0;
					}
					
					// 2. ambil data jumlah OP di range tgl
					$sqlop=" SELECT sum(n_count) as jumop FROM tm_op a INNER JOIN tm_op_item b ON a.i_op=b.i_op
							WHERE (a.d_op BETWEEN '".$tgldarixx."' AND '".$tglkexx."') AND b.i_product='".$i_product_motif."'
							AND a.f_op_cancel='f' ";
					if ($i_customer != 0)
						$sqlop.=" AND a.i_customer='".$i_customer."' ";
						
					$queryx	= $db2->query($sqlop);
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$jum_op = $hasilx->jumop;
						if ($jum_op == '')
							$jum_op = 0;
					}
					else
						$jum_op = 0;
					
					// 3. ambil data jumlah DO di range tgl
					$sqldo = " SELECT sum(n_deliver) as jumdo FROM tm_do a INNER JOIN tm_do_item b ON a.i_do=b.i_do
								WHERE (a.d_do BETWEEN '".$tgldarixx."' AND '".$tglkexx."') AND b.i_product='".$i_product_motif."'
								AND a.f_do_cancel='f' 
								AND b.i_op IN (SELECT i_op FROM tm_op WHERE d_op BETWEEN '".$tgldarixx."' AND '".$tglkexx."') ";
					if ($i_customer != 0)
						$sqldo.=" AND a.i_customer='".$i_customer."' ";				
					$queryx	= $db2->query($sqldo);
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$jum_do = $hasilx->jumdo;
						if ($jum_do == '')
							$jum_do = 0;
					}
					else
						$jum_do = 0;
					
					if ($jum_op > 0)
						$persendovsop= ($jum_do/$jum_op)*100;
					else
						$persendovsop = '';
					
					if ($fc != 0) {
						$persenopvsfc= ($jum_op/$fc)*100;
						$persendovsfc= ($jum_do/$fc)*100;
					}
					else {
						$persenopvsfc = '';
						$persendovsfc = '';
					}
					
					if ($bulanxx == '01')
						$nama_bln = "Januari";
					else if ($bulanxx == '02')
						$nama_bln = "Februari";
					else if ($bulanxx == '03')
						$nama_bln = "Maret";
					else if ($bulanxx == '04')
						$nama_bln = "April";
					else if ($bulanxx == '05')
						$nama_bln = "Mei";
					else if ($bulanxx == '06')
						$nama_bln = "Juni";
					else if ($bulanxx == '07')
						$nama_bln = "Juli";
					else if ($bulanxx == '08')
						$nama_bln = "Agustus";
					else if ($bulanxx == '09')
						$nama_bln = "September";
					else if ($bulanxx == '10')
						$nama_bln = "Oktober";
					else if ($bulanxx == '11')
						$nama_bln = "November";
					else if ($bulanxx == '12')
						$nama_bln = "Desember";
					
					$databulanan[] = array( 
										'bulanxx'=> $bulanxx,
										'namabulanxx'=> $nama_bln,
										'fc'=> $fc,
										'jum_op'=> $jum_op,
										'jum_do'=> $jum_do,
										'persendovsop'=>$persendovsop,
										'persenopvsfc'=>$persenopvsfc,
										'persendovsfc'=>$persendovsfc,
									);
				} // end for
						
				//ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $db2->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
				
				$datadetail[] = array( 
										'kode_brg_jadi'=> $row->i_product_motif,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'databulanan'=> $databulanan,
										/*'hjp'=> $v_price,
										'fc'=> $fc,
										'jum_op'=> $jum_op,
										'jum_op_rp'=> $jum_op_rp,
										'jum_do'=> $jum_do,
										'jum_do_rp'=> $jum_do_rp,
										'pendingan'=> $pendingan,
										'pendingan_rp'=> $pendingan_rp,
										'persendovsop'=>$persendovsop,
										'persenopvsfc'=>$persenopvsfc,
										'persendovsfc'=>$persendovsfc, */
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel
									);
				$databulanan = array();
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  // =======================================================================================================
  
  
  function cek_forecast($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tm_forecast_wip WHERE bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_brgjadi() {
		$sql = " SELECT distinct b.i_product_motif, b.e_product_motifname FROM tr_product_base a, tr_product_motif b 
				WHERE a.i_product_base = b.i_product ORDER BY b.i_product_motif ";
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$databrgjadi = array();			
			foreach ($hasil as $row) {				
				$databrgjadi[] = array( 'id'=> '0',
										'kode_brg_jadi'=> $row->i_product_motif,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> '0',
										'keterangan'=> ''
									);
			}
		}
		else {
			$databrgjadi = '';
		}
		return $databrgjadi;
  }
  
  function get_forecast($bulan, $tahun) {
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_detail b, 
					tm_forecast_wip a, 
					tr_product_motif c
					WHERE b.id_forecast_wip = a.id 
					AND b.kode_brg_jadi = c.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY b.id ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function saveforecast($is_new, $id_fc, $iddetail, $kode_brg_jadi, $fc, $ket){ 
	  $tgl = date("Y-m-d H:i:s"); 
	 
	 // 02-04-2014, ga pake is_new lagi. langsung save aja setelah delete detail sebelumnya
	//  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailx	= $seqrow->id+1;
			}else{
				$iddetailx	= 1;
			}
		  
		   $data_detail = array(
						'id'=>$iddetailx,
						'id_forecast_wip'=>$id_fc,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'qty'=>$fc,
						'keterangan'=>$ket
					);
		   $this->db->insert('tm_forecast_wip_detail',$data_detail);
	/*  }
	  else {
		  $iddetailx = $iddetail;
		  
		  $this->db->query(" UPDATE tm_forecast_wip_detail SET qty = '$fc', keterangan='$ket'
						where id = '$iddetailx' ");
			// ====================
	  } */
  }
  
  // 15-03-2014
  function get_sowip($num, $offset, $gudang, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_hasil_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($gudang != '0')
		$sql.= " AND id_gudang = '$gudang' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di perusahaan ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di perusahaan ?";
								
				$data_so[] = array(		'id'=> $row1->id,	
										'bulan'=> $row1->bulan,	
										'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,	
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sowiptanpalimit($gudang, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_hasil_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($gudang != '0')
		$sql.= " AND id_gudang = '$gudang' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_sowipunit($num, $offset, $kode_unit, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_unit_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($kode_unit != '0')
		$sql.= " AND kode_unit = '$kode_unit' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC, kode_unit ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				$data_so[] = array(			'id'=> $row1->id,	
											'bulan'=> $row1->bulan,	
											'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'kode_unit'=> $row1->kode_unit,	
											'nama_unit'=> $nama_unit
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sowipunittanpalimit($kode_unit, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_unit_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($kode_unit != '0')
		$sql.= " AND kode_unit = '$kode_unit' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  // 17-03-2014
  function get_sounitjahit($id_so) {

		$query	= $this->db->query(" SELECT a.id as id_header, a.kode_unit, a.bulan, a.tahun, b.*, 
					d.e_product_motifname as nama_brg_jadi 
					FROM tt_stok_opname_unit_jahit_detail b, 
					tt_stok_opname_unit_jahit a, tr_product_motif d
					WHERE b.id_stok_opname_unit_jahit = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.id = '$id_so' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_unit_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_unit_jahit_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok, b.stok_bagus, b.stok_perbaikan
							FROM tm_stok_unit_jahit a, tm_stok_unit_jahit_warna b
							WHERE a.id = b.id_stok_unit_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'stok_opname_bgs'=> $rowxx->jum_bagus,
									'stok_opname_perbaikan'=> $rowxx->jum_perbaikan
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'kode_unit'=> $row->kode_unit,
										'nama_unit'=> $nama_unit,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'nama_bln'=> $nama_bln,
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->nama_brg_jadi,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesounitjahit($id_so, $kode_brg_jadi, $kode_warna, $stok_fisik_bgs, $stok_fisik_perbaikan){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisikbgs = 0;
		$qtytotalstokfisikperbaikan = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$stok_fisik_bgs[$xx] = trim($stok_fisik_bgs[$xx]);
			$stok_fisik_perbaikan[$xx] = trim($stok_fisik_perbaikan[$xx]);
			$qtytotalstokfisikbgs+= $stok_fisik_bgs[$xx];
			$qtytotalstokfisikperbaikan+= $stok_fisik_perbaikan[$xx];
		} // end for
	// ---------------------------------------------------------------------
		// 24-03-2014
		$totalxx = $qtytotalstokfisikbgs+$qtytotalstokfisikperbaikan;
			
		// ambil id detail id_stok_opname_unit_jahit_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail 
					where kode_brg_jadi = '$kode_brg_jadi' 
					AND id_stok_opname_unit_jahit = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
		  
		 //if ($kode_brg_jadi == 'TPB220100') {
		//	echo $totalxx." ".$qtytotalstokfisikbgs."<br>";
			/*echo "UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$totalxx', status_approve='f' 
					where id = '$iddetail'"; die(); */
		 //}
		 $this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$totalxx', 
					jum_bagus = '".$qtytotalstokfisikbgs."', jum_perbaikan = '".$qtytotalstokfisikperbaikan."',
					status_approve='f' 
					where id = '$iddetail' ");
			
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			// 24-03-2014
			$totalxx = $stok_fisik_bgs[$xx]+$stok_fisik_perbaikan[$xx];
			
			/*if ($kode_brg_jadi == 'TPB220100' && $kode_warna[$xx] == '10') {
				echo "detail=".$stok_fisik_bgs[$xx]." ".$stok_fisik_perbaikan[$xx]."<br>";
				echo " UPDATE tt_stok_opname_unit_jahit_detail_warna SET jum_stok_opname = '".$totalxx."',
						jum_bagus = '".$stok_fisik_bgs[$xx]."', jum_perbaikan = '".$stok_fisik_perbaikan[$xx]."'
						WHERE id_stok_opname_unit_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ";
				die(); 
			} */
			$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail_warna SET jum_stok_opname = '".$totalxx."',
						jum_bagus = '".$stok_fisik_bgs[$xx]."', jum_perbaikan = '".$stok_fisik_perbaikan[$xx]."'
						WHERE id_stok_opname_unit_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ");
		}
		// ====================  
  }
  
  // 18-03-2014
  function get_sohasiljahit($id_so) {

		$query	= $this->db->query(" SELECT a.id as id_header, a.id_gudang, a.bulan, a.tahun, b.*, 
					d.e_product_motifname as nama_brg_jadi 
					FROM tt_stok_opname_hasil_jahit_detail b, 
					tt_stok_opname_hasil_jahit a, tr_product_motif d
					WHERE b.id_stok_opname_hasil_jahit = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.id = '$id_so' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_hasil_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_hasil_jahit_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a, tm_stok_hasil_jahit_warna b
							WHERE a.id = b.id_stok_hasil_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$row->id_gudang'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id_gudang'=> $row->id_gudang,
										'kode_gudang'=> $kode_gudang,
										'nama_gudang'=> $nama_gudang,
										'nama_lokasi'=> $nama_lokasi,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'nama_bln'=> $nama_bln,
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->nama_brg_jadi,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesohasiljahit($id_so, $kode_brg_jadi, $kode_warna, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
	// ---------------------------------------------------------------------
	  
		// ambil id detail id_stok_opname_hasil_jahit_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail 
					where kode_brg_jadi = '$kode_brg_jadi' 
					AND id_stok_opname_hasil_jahit = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
		  
		 $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$qtytotalstokfisik',
					status_approve='f' where id = '$iddetail' ");
			
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."'
						WHERE id_stok_opname_hasil_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ");
		}
		// ====================  
  }
  
  // 22-03-2014
  function get_mutasi_unit($date_from, $date_to, $unit_jahit) {		
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
	  
		// 1. ambil data2 unit jahit dari rentang tanggal yg dipilih
		$filter = "";
		$filter2 = "";
		if ($unit_jahit != '0') {
			$filter = " AND a.kode_unit_jahit = '$unit_jahit' ";
			$filter2= " AND a.kode_unit = '$unit_jahit' ";
		}
		
		// 06-06-2014 DIMODIF. tambahin query ke tabel stok unit jahit
		$sql = "SELECT a.kode_unit_jahit FROM (
				select a.kode_unit_jahit FROM tm_sjmasukwip a
				WHERE a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.kode_unit_jahit <> '0' GROUP BY a.kode_unit_jahit
				
				UNION SELECT a.kode_unit_jahit FROM tm_sjkeluarwip a
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.kode_unit_jahit <> '0' GROUP BY a.kode_unit_jahit
				
				UNION SELECT a.kode_unit as kode_unit_jahit FROM tm_bonmkeluarcutting a
				WHERE a.tgl_bonm >='".$tgldari."'  
				AND a.tgl_bonm <='".$tglke."' ".$filter2." AND a.kode_unit <> '0' GROUP BY a.kode_unit
				
				UNION SELECT a.kode_unit as kode_unit_jahit FROM tm_sjmasukbhnbakupic a
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter2." AND a.kode_unit <> '0' GROUP BY a.kode_unit
				
				UNION SELECT a.kode_unit as kode_unit_jahit FROM tt_stok_opname_unit_jahit a
				WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' AND a.status_approve='t' ".$filter2." GROUP BY a.kode_unit
				)
				a GROUP BY a.kode_unit_jahit ORDER BY a.kode_unit_jahit";
		
		/*UNION SELECT kode_unit as kode_unit_jahit FROM tm_stok_unit_jahit
				WHERE TRUE ".$filter2." GROUP BY kode_unit */
		
		/*$sql = "SELECT a.kode_brg_jadi FROM (
				select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
				a GROUP BY a.kode_brg_jadi"; */
		
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$total_so_rupiah=0; $total_sisa_stok_rupiah=0;
			foreach ($hasil as $row) {
				// 22-03-2014 -----------------------------------------------------
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit_jahit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = "SELECT a.kode_brg_jadi FROM (
					select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
					WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.kode_unit_jahit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
					WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tgldari."'  
					AND a.tgl_sj <='".$tglke."' AND a.kode_unit_jahit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b 
					WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tgldari."'  
					AND a.tgl_bonm <='".$tglke."' AND a.kode_unit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b 
					WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tgldari."'  
					AND a.tgl_sj <='".$tglke."' AND a.kode_unit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tt_stok_opname_unit_jahit a, tt_stok_opname_unit_jahit_detail b
					WHERE a.id=b.id_stok_opname_unit_jahit AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.kode_unit = '$row->kode_unit_jahit' AND a.status_approve='t' AND b.jum_stok_opname <> 0 
					GROUP BY b.kode_brg_jadi
					)
					a GROUP BY a.kode_brg_jadi
					ORDER BY kode_brg_jadi";
				/*
				 * UNION SELECT kode_brg_jadi FROM tm_stok_unit_jahit WHERE kode_unit='$row->kode_unit_jahit'
					GROUP BY kode_brg_jadi
				 */ 
				$query2	= $this->db->query($sql2);
				
				/*$jum_keluar1 = 0;
				$jum_keluar2 = 0;
				$jum_keluar_lain1 = 0;
				$jum_keluar_lain2 = 0;
				$jum_masuk = 0;
				$jum_masuk_lain1 = 0;
				$jum_masuk_lain2 = 0;
				$jum_masuk_lain3 = 0;
				$jum_masuk_lain4 = 0; 
				*/
				$jum_saldo_awal = 0;
				$jum_masuk = 0;
				$jum_keluar = 0;
				$jum_stok_akhir = 0;
				$jum_so = 0;
				$selisih_bgs = 0;
				$selisih_perbaikan = 0;
				$sisa_stok = 0;
				
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
							
						// SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA 
						//(dikomen heula, PERLU MODIF DULU DI FITUR INPUT SO UNIT JAHIT KARENA BLM ADA PEMISAHAN BAGUS & PERBAIKAN)
						// udh tambahin field jum_bagus dan jum_perbaikan di tabel tt_stok_opname_unit_jahit_detail dan warna
						// jadi bikin querynya aja dulu
						
						// saldo awal bagus dan perbaikan
					/*	echo "SELECT b.jum_bagus, b.jum_perbaikan FROM tt_stok_opname_unit_jahit a, 
											tt_stok_opname_unit_jahit_detail b
											WHERE a.id = b.id_stok_opname_unit_jahit
											AND b.kode_brg_jadi = '$row2->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't'"; echo "<br>"; */
						
							$queryx	= $this->db->query(" SELECT b.jum_bagus, b.jum_perbaikan FROM tt_stok_opname_unit_jahit a, 
											tt_stok_opname_unit_jahit_detail b
											WHERE a.id = b.id_stok_opname_unit_jahit
											AND b.kode_brg_jadi = '$row2->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' ");
						
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_bgs = $hasilrow->jum_bagus;
							$saldo_awal_perbaikan = $hasilrow->jum_perbaikan;
						}
						else {
							$saldo_awal_bgs = 0;
							$saldo_awal_perbaikan = 0;
						}
						$jum_saldo_awal = $saldo_awal_bgs+$saldo_awal_perbaikan;
						
						//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit_jahit' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit_jahit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0;
						
						//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit_jahit' AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						$stok_akhir_bgs = $saldo_awal_bgs + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
						
						// ============================= END STOK AKHIR ==============================================
						
						// STOK OPNAME BAGUS DAN PERBAIKAN
						$sql = "SELECT b.jum_bagus, b.jum_perbaikan FROM tt_stok_opname_unit_jahit a, 
									tt_stok_opname_unit_jahit_detail b
									WHERE a.id = b.id_stok_opname_unit_jahit 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit_jahit'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
														
						$query3	= $this->db->query($sql);
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$so_bgs = $hasilrow->jum_bagus;
							$so_perbaikan = $hasilrow->jum_perbaikan;
						}
						else {
							$so_bgs = 0;
							$so_perbaikan = 0;
						}
						$jum_so = $so_bgs+$so_perbaikan;
						//=========================================
						
						$selisih_bgs = $so_bgs-$stok_akhir_bgs;
						$selisih_perbaikan = $so_perbaikan-$stok_akhir_perbaikan;
						
						//23-05-2014 ambil data stok schedule dari tabel tm_stok_schedule
						$sqlxx = "SELECT qty FROM tm_stok_schedule_wip WHERE kode_brg_jadi = '$row2->kode_brg_jadi' 
								AND kode_unit = '$row->kode_unit_jahit'
								AND bulan='$bln1' AND tahun='$thn1' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$stok_schedule = $hasilxx->qty;
						}
						else
							$stok_schedule = 0;
						
						$sisa_stok = $jum_so-$stok_schedule;
						
						// 23-05-2014, harga HPP, pake dari tabel hpp_wip
						$sqlxx = "SELECT harga FROM tm_hpp_wip WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ";
						$queryxx	= $this->db->query($sqlxx);
						
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$hpp = $hasilxx->harga;
						}
						else
							$hpp = 0;
						
						// jumlah nominal rupiah SO
						$jum_so_rupiah = $jum_so*$hpp;
						$total_so_rupiah+= $jum_so_rupiah;
						
						// jumlah nominal sisa stok
						$sisa_stok_rupiah = $sisa_stok*$hpp;
						$total_sisa_stok_rupiah+= $sisa_stok_rupiah;
																		
						$data_stok[] = array(		'kode_brg'=> $row2->kode_brg_jadi,
													'nama_brg'=> $e_product_motifname,
													'hpp'=> $hpp,
													'saldo_awal_bgs'=> $saldo_awal_bgs,
													'saldo_awal_perbaikan'=> $saldo_awal_perbaikan,
													'jum_saldo_awal'=> $jum_saldo_awal,
													
													'masuk_bgs'=> $masuk_bgs,
													'masuk_returbrgjadi'=> $masuk_returbrgjadi,
													'masuk_pengembalian'=> $masuk_pengembalian,
													'jum_masuk'=> $jum_masuk,
													
													'keluar_bgs'=> $keluar_bgs,
													'keluar_perbaikan'=> $keluar_perbaikan,
													'keluar_retur_bhnbaku'=> $keluar_retur_bhnbaku,
													'jum_keluar'=> $jum_keluar,
													
													'stok_akhir_bgs'=> $stok_akhir_bgs,
													'stok_akhir_perbaikan'=> $stok_akhir_perbaikan,
													'jum_stok_akhir'=> $jum_stok_akhir,
													
													'so_bgs'=> $so_bgs,
													'so_perbaikan'=> $so_perbaikan,
													'jum_so'=> $jum_so,
													'jum_so_rupiah'=> $jum_so_rupiah,
													
													'selisih_bgs'=> $selisih_bgs,
													'selisih_perbaikan'=> $selisih_perbaikan,
													'stok_schedule'=> $stok_schedule,
													'sisa_stok'=> $sisa_stok,
													'sisa_stok_rupiah'=> $sisa_stok_rupiah
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------

				// array unit jahit
				$data_unit_jahit[] = array(		'kode_unit'=> $row->kode_unit_jahit,
												'nama_unit'=> $nama_unit,
												'data_stok'=> $data_stok,
												'total_so_rupiah'=> $total_so_rupiah,
												'total_sisa_stok_rupiah'=> $total_sisa_stok_rupiah
									);
				$data_stok = array(); // done
				$total_so_rupiah = 0; $total_sisa_stok_rupiah = 0;
			} // endforeach header
		}
		else {
			$data_unit_jahit = '';
		}
		return $data_unit_jahit;
  }
  
  // 02-04-2014
  function get_stokmingguan_unit($unit_jahit) {		
	  	  
		// 1. ambil data2 unit jahit
		$filter = "";
		if ($unit_jahit != '0')
			$filter = " WHERE a.kode_unit = '$unit_jahit' ";
		$sql = "SELECT distinct a.kode_unit FROM tm_stok_unit_jahit a
				INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit
				".$filter." ORDER BY a.kode_unit";
		
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row) {
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				//echo "SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit'  <br>";
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = " SELECT kode_brg_jadi, stok, stok_bagus, stok_perbaikan FROM tm_stok_unit_jahit
						WHERE kode_unit = '$row->kode_unit' ";				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';

						// 14-04-2015
						// harga HPP, pake dari tabel hpp_wip
						$sqlxx = "SELECT harga FROM tm_hpp_wip WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ";
						$queryxx	= $this->db->query($sqlxx);
						
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$hpp = $hasilxx->harga;
						}
						else
							$hpp = 0;
						
						$total_rp_bagus = $hpp*$row2->stok_bagus;
						$total_rp_perbaikan = $hpp*$row2->stok_perbaikan;
						
						$data_stok[] = array(		'kode_brg'=> $row2->kode_brg_jadi,
													'nama_brg'=> $e_product_motifname,
													'stok'=> $row2->stok,
													'stok_bagus'=> $row2->stok_bagus,
													'stok_perbaikan'=> $row2->stok_perbaikan,
													'hpp'=> $hpp,
													'total_rp_bagus'=> $total_rp_bagus,
													'total_rp_perbaikan'=> $total_rp_perbaikan
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------

				// array unit jahit
				$data_unit_jahit[] = array(		'kode_unit'=> $row->kode_unit,
												'nama_unit'=> $nama_unit,
												'data_stok'=> $data_stok
									);
				$data_stok = array();
			} // endforeach header
		}
		else {
			$data_unit_jahit = '';
		}
		return $data_unit_jahit;
  }
  
  // 03-04-2014
  function get_bhnbaku_unit($date_from, $date_to) {		
	  
	  // ambil data forecast bulan sebelumnya (bulan sebelum date_from)
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		//$tgldari = $thn1."-".$bln1."-".$tgl1;
		
		// koreksi 11-04-2014, skrg ga dipake. yg dipake adalah bulan yg ada di tgl awal
		/*if ($bln1 == '01') {
			$bln_sebelumnya = 12;
			$thn_sebelumnya = $thn1-1;
		}
		else {
			$bln_sebelumnya = $bln1-1;
			$thn_sebelumnya = $thn1;
			
			if ($bln_sebelumnya < 10)
				$bln_sebelumnya = "0".$bln_sebelumnya;
		} */
		
		if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
		// 1. ambil data2 distinct kode brg jadi dari rentang tanggal yg dipilih		
		$sql = " SELECT distinct b.kode_brg_jadi FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
				WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND a.jenis_keluar = '1'
				ORDER BY b.kode_brg_jadi ";
		
		$query	= $this->db->query($sql);
		$data_brg_jadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row) {
				// ambil nama brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				$query3	= $this->db->query(" SELECT b.qty FROM tm_forecast_wip a, tm_forecast_wip_detail b 
							WHERE a.id = b.id_forecast_wip AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.bulan='$bln1' AND a.tahun='$thn1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$qty_forecast = $hasilrow->qty;
				}
				else
					$qty_forecast = 0;
				// --------------------------------------------------------------
						
				// 2. Query utk ambil unit2 jahit berdasarkan perulangan brg jadi
				$sql2 = " SELECT distinct a.kode_unit FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
				WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi='$row->kode_brg_jadi' 
				AND a.jenis_keluar = '1' ORDER BY a.kode_unit ";				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					$jmldipenuhi = 0;
					foreach ($hasil2 as $row2) { // perulangan unit2 jahitnya
						$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
						
						// ambil jumlah qty barang berdasarkan unit
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
								WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
								AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND kode_unit = '$row2->kode_unit'
								AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.jenis_keluar = '1' ");
						$hasilrow = $query3->row();
						$jum	= $hasilrow->jum;
						
						$jmldipenuhi+=$jum;
						
						$data_stok[] = array(		'kode_unit'=> $row2->kode_unit,
													'nama_unit'=> $nama_unit,
													'jum'=> $jum
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------
				$sisa_forecast = $qty_forecast-$jmldipenuhi;
				// array brg jadi
				$data_brg_jadi[] = array(		'kode_brg_jadi'=> $row->kode_brg_jadi,
												'nama_brg_jadi'=> $e_product_motifname,
												'bln_forecast'=> $nama_bln,
												'thn_forecast'=> $thn1,
												'qty_forecast'=> $qty_forecast,
												'sisa_forecast'=> $sisa_forecast,
												'data_stok'=> $data_stok
									);
				$data_stok = array();
			} // endforeach header
		}
		else {
			$data_brg_jadi = '';
		}
		return $data_brg_jadi;
  }
  
  // 05-04-2014
  function get_grup_jahit($unit_jahit){
    $query = $this->db->query(" SELECT * FROM tm_grup_jahit WHERE kode_unit='$unit_jahit' ORDER BY nama_grup ");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 19-04-2014
  function get_rekap_presentasi_kerja_unit($bulan, $tahun, $unit_jahit) {		
	  
		// 1. ambil data2 unit jahit dari periode yg dipilih
		$filter = "";
		if ($unit_jahit != '0')
			$filter = " AND kode_unit = '$unit_jahit' ";
		
		$sql = " SELECT id, total_rata, kode_unit FROM tm_presentasi_kerja_unit WHERE bulan='$bulan' AND tahun='$tahun' ".$filter."
				ORDER BY kode_unit ";
					
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
						
			foreach ($hasil as $row) {
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query distinct nama grup jahit
				// modif 15-05-2014
				$sql2 = " SELECT DISTINCT c.nama_grup_jahit FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.bulan='$bulan' AND a.tahun='$tahun' AND a.kode_unit='$row->kode_unit' 
						AND c.persentase <> '0'
						ORDER BY c.nama_grup_jahit ";
				$query2	= $this->db->query($sql2);
				
				$data_grup_jahit = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						// 2. Query item2 brgnya berdasarkan nama grup
						// modif 15-05-2014
						$sql2x = " SELECT b.kode_brg_jadi, c.* FROM tm_presentasi_kerja_unit a, 
								tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
								WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
								AND a.bulan='$bulan' AND a.tahun='$tahun' AND a.kode_unit='$row->kode_unit'
								AND c.nama_grup_jahit='$row2->nama_grup_jahit' 
								AND c.persentase <> '0'
								ORDER BY b.kode_brg_jadi "; 
						$query2x	= $this->db->query($sql2x);
						
						$data_item_brg = array();
						if ($query2x->num_rows() > 0){
							$hasil2x = $query2x->result();
							foreach ($hasil2x as $row2x) {
								$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2x->kode_brg_jadi' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$e_product_motifname = $hasilrow->e_product_motifname;
								}
								else
									$e_product_motifname = '';
									
								$data_item_brg[] = array('kode_brg_jadi'=> $row2x->kode_brg_jadi,
														'nama_brg_jadi'=> $e_product_motifname,
														'hari'=> $row2x->hari,
														'kapasitas'=> $row2x->kapasitas,
														'minggu1'=> $row2x->minggu1,
														'minggu2'=> $row2x->minggu2,
														'minggu3'=> $row2x->minggu3,
														'minggu4'=> $row2x->minggu4,
														'total'=> $row2x->total,
														'persentase'=> $row2x->persentase
													);
								
							} // end for2x
						} // end if2x
						else
							$data_item_brg = '';
									
						$data_grup_jahit[] = array('nama_grup_jahit'=> $row2->nama_grup_jahit,
												   'data_item_brg'=> $data_item_brg
													);
						$data_item_brg = array();
					//-----------------------------------------------------------------
				} // end for2
			} // end if2
			else
				$data_grup_jahit = '';
				
			$data_unit_jahit[] = array('kode_unit'=> $row->kode_unit,
									   'nama_unit'=> $nama_unit,
									   'total_rata'=> $row->total_rata,
									   'data_grup_jahit'=> $data_grup_jahit);
		} // end for
	} // end if
	else {
		$data_unit_jahit = '';
	}
	return $data_unit_jahit;
  }
  
  // 28-04-2014
  function get_rekap_presentasi_kerja_unit_tahunan($tahun) {		
	  // 16-10-2014
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautama = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql = " SELECT distinct a.kode_unit, c.nama_grup_jahit FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.tahun='$tahun' AND c.persentase<>'0' AND a.kode_unit = '".$rowutama->kode_unit."'
						ORDER BY a.kode_unit, c.nama_grup_jahit ";
			  
			   $query	= $this->db->query($sql);
			   $data_rekap = array();
				
				if ($query->num_rows() > 0){
				$hasil = $query->result();
								
				foreach ($hasil as $row) {
					// hitung masing2 total presentase utk tiap2 grup jahit
					for ($bulan=1; $bulan<=12; $bulan++) {
						if ($bulan < 10)
							$bln = "0".$bulan;
						else
							$bln = $bulan;
												
						$query3	= $this->db->query(" SELECT AVG(persentase) AS rata FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c 
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.tahun='$tahun' AND a.bulan='$bln' AND c.nama_grup_jahit='$row->nama_grup_jahit'
						AND a.kode_unit = '$row->kode_unit' AND c.persentase<>'0' ");
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$rata = $hasilrow->rata;
							if ($rata == '')
								$rata = 0;
						}
						else
							$rata= 0;
						
						$listratabulan[] = array($bulan => $rata);
					} // end for
					
					// ambil nama unit
					$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
					$hasilrow = $query3->row();
					$nama_unit	= $hasilrow->nama;
					
					$data_rekap[] = array('kode_unit'=> $row->kode_unit,
										  'nama_unit'=> $nama_unit,
										  'nama_grup_jahit'=> $row->nama_grup_jahit,
										  'listratabulan'=> $listratabulan);
					$listratabulan=array();
				} // end for
			  }
			  else
				$data_rekap = '';
				
			  $datautama[] = array(		'kode'=> $rowutama->kode,
										'nama'=> $rowutama->nama,
										'data_rekap'=> $data_rekap
													);
			} // end for
		} // end if	  
	  
		return $datautama;
	  //----------------------------------------------------------------------------------------------------------------------
  }
  
  
  
  // 10-05-2014
  function get_transaksi_unit_jahit($bulan, $tahun, $unit_jahit) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// 1. ambil data2 stok di unit jahit
		$sql = " SELECT id, kode_brg_jadi FROM tm_stok_unit_jahit WHERE kode_unit = '$unit_jahit' ORDER BY kode_brg_jadi ";
		$query	= $this->db->query($sql);
		$data_brg_jadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row) {
				$data_warna = array();
				$data_so_warna = array();
				
				// 14-05-2014, ambil data2 warna dari tm_stok_unit_jahit_warna
				$sqlwarna = " SELECT a.kode_warna, b.nama FROM tm_stok_unit_jahit_warna a, tm_warna b 
							WHERE a.kode_warna=b.kode AND a.id_stok_unit_jahit='$row->id' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'saldo_retur'=> 0,
												'tot_masuk1'=> 0,
												'tot_keluar1'=> 0,
												'tot_masuk_retur1'=> 0,
												'tot_keluar_retur1'=> 0,
												'tot_masuk_retur2'=> 0,
												'tot_keluar_retur2'=> 0
											);
					}
				}
				
				// 17-05-2014, ambil SO brg jadi dari tt_stok_opname_unit_jahit_detail_warna
				$sqlwarna = " SELECT c.kode_warna, d.nama, c.jum_bagus, c.jum_perbaikan 
							FROM tt_stok_opname_unit_jahit a, tt_stok_opname_unit_jahit_detail b, 
							tt_stok_opname_unit_jahit_detail_warna c, tm_warna d 
							WHERE a.id = b.id_stok_opname_unit_jahit AND b.id = c.id_stok_opname_unit_jahit_detail 
							AND c.kode_warna=d.kode AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit' "; //echo $sqlwarna; die();
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['kode_warna'] == $rowwarna->kode_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_bagus;
								$data_warna[$xx]['saldo_retur']= $rowwarna->jum_perbaikan;
							}
						} // end for
						
						$data_so_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_bagus'=> $rowwarna->jum_bagus,
												'jum_perbaikan'=> $rowwarna->jum_perbaikan
											);
					}
				}
				
				// ambil nama brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				/*  $sql = "SELECT a.kode_brg_jadi, sum(a.masuk) as masuk, sum(a.keluar) as keluar FROM (
				select b.kode_brg_jadi, sum(b.qty) as masuk, 0 as keluar FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi, 0 as masuk, sum(b.qty) as keluar FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
				a GROUP BY a.kode_brg_jadi"; */
				
				// lanjutan 13-05-2014
				
				// 1. UNTUK TABEL 1
				// QUERY UTK KELUAR MASUK BRG JADI BAGUS DAN RETUR BHN BAKU
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, 1 as masukwip, 0 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip AND a.jenis_masuk = '1'
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit_jahit='$unit_jahit'
						UNION
						SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, 0 as masukwip, 1 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
						WHERE a.id = b.id_bonmkeluarcutting AND a.jenis_keluar = '1'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						
						UNION
						SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, 0 as masukwip, 0 as bonmkeluarcutting, 1 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
						WHERE a.id = b.id_bonmkeluarcutting AND a.jenis_keluar = '2'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwip, 0 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 1 as sjmasukbhnbakupic 
						FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b
						WHERE a.id = b.id_sjmasukbhnbakupic
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						ORDER BY tgl_sj ASC
						 "; // 10-05-2014 contoh union di modul info-pembelian mmaster.php.
				
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$is_masukwip = $row2->masukwip;
						$is_bonmkeluarcutting = $row2->bonmkeluarcutting;
						$is_bonmkeluarcutting2 = $row2->bonmkeluarcutting2;
						$is_sjmasukbhnbakupic = $row2->sjmasukbhnbakupic;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($is_masukwip == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c 
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						else if ($is_bonmkeluarcutting == '1' || $is_bonmkeluarcutting2 == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c 
									WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "ya";
							$keluar= "tidak";
						}
						else if ($is_sjmasukbhnbakupic == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b, tm_sjmasukbhnbakupic_detail_warna c 
									WHERE a.id = b.id_sjmasukbhnbakupic AND b.id = c.id_sjmasukbhnbakupic_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "tidak";
							$keluar= "ya";
						}
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$sqlxx = " SELECT nama FROM tm_warna WHERE kode='$row3->kode_warna' ";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nama_warna = $hasilxx->nama;
								}
								else
									$nama_warna = '';
								
								// saldo per row 14-05-2014
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['kode_warna'] == $row3->kode_warna) {
										if ($masuk == "ya") {
											$data_warna[$xx]['saldo']+= $row3->qty;
											//$data_warna[$xx]['tot_masuk']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											$data_warna[$xx]['saldo']-= $row3->qty;
											//$data_warna[$xx]['tot_keluar']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($is_masukwip == '1') {
											$data_warna[$xx]['tot_keluar1']+= $row3->qty;
										}
										else if ($is_bonmkeluarcutting == '1') {
											$data_warna[$xx]['tot_masuk1']+= $row3->qty;
										}
										else if ($is_bonmkeluarcutting2 == '1') {
											$data_warna[$xx]['tot_masuk_retur1']+= $row3->qty;
										}
										else if ($is_sjmasukbhnbakupic == '1') {
											$data_warna[$xx]['tot_keluar_retur1']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'is_masukwip'=> $is_masukwip,
												'is_bonmkeluarcutting'=> $is_bonmkeluarcutting,
												'is_bonmkeluarcutting2'=> $is_bonmkeluarcutting2,
												'is_sjmasukbhnbakupic'=> $is_sjmasukbhnbakupic,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
					
				// 2. UNTUK TABEL 2
				// QUERY UTK KELUAR MASUK RETUR BRG JADI
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, 1 as masukwip, 0 as keluarwip
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip AND a.jenis_masuk = '2'
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit_jahit='$unit_jahit'
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwip, 1 as keluarwip
						FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip AND a.jenis_keluar = '3'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit_jahit='$unit_jahit'
						ORDER BY tgl_sj ASC
						 ";
				
				$query2	= $this->db->query($sql2);
				$data_tabel2 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$is_masukwip = $row2->masukwip;
						$is_keluarwip = $row2->keluarwip;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($is_masukwip == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c 
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						else if ($is_keluarwip == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c 
									WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "ya";
							$keluar = "tidak";
						}
												
						$query3	= $this->db->query($sql3);
						$data_tabel2_perwarna = array();
						$data_tabel2_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$sqlxx = " SELECT nama FROM tm_warna WHERE kode='$row3->kode_warna' ";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nama_warna = $hasilxx->nama;
								}
								else
									$nama_warna = '';
								
								// saldo per row 14-05-2014
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['kode_warna'] == $row3->kode_warna) {
										if ($masuk == "ya") {
											$data_warna[$xx]['saldo_retur']+= $row3->qty;
											//$data_warna[$xx]['tot_masuk_retur']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo_retur'];
											$data_tabel2_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											$data_warna[$xx]['saldo_retur']-= $row3->qty;
											//$data_warna[$xx]['tot_keluar_retur']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo_retur'];
											$data_tabel2_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($is_masukwip == '1') {
											$data_warna[$xx]['tot_keluar_retur2']+= $row3->qty;
										}
										else if ($is_keluarwip == '1') {
											$data_warna[$xx]['tot_masuk_retur2']+= $row3->qty;
										}
									}
								}
									
								$data_tabel2_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						$data_tabel2[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'is_masukwip'=> $is_masukwip,
												'is_keluarwip'=> $is_keluarwip,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel2_perwarna'=> $data_tabel2_perwarna,
												'data_tabel2_saldo_perwarna'=> $data_tabel2_saldo_perwarna
										);
						$data_tabel2_perwarna = array();
						$data_tabel2_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel2='';
				
				// =================================== END NEW =======================================================
				
				// array brg jadi
				$data_brg_jadi[] = array(		'kode_brg_jadi'=> $row->kode_brg_jadi,
												'nama_brg_jadi'=> $e_product_motifname,
												'data_tabel1'=> $data_tabel1,
												'data_tabel2'=> $data_tabel2,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna
									);
				$data_tabel1 = array();
				$data_tabel2 = array();
				$data_warna = array();
				$data_so_warna = array();
			} // endforeach header
		}
		else {
			$data_brg_jadi = '';
		}
		return $data_brg_jadi;
  }
  
  // 24-05-2014
  function get_mutasi_unit_stokschedule($unit_jahit, $bulan, $tahun) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
	  
				// Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = "SELECT a.kode_brg_jadi FROM (
					select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
					WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit_jahit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
					WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01'   
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit_jahit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b 
					WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01'   
					AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b 
					WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tt_stok_opname_unit_jahit a, tt_stok_opname_unit_jahit_detail b
					WHERE a.id=b.id_stok_opname_unit_jahit AND a.bulan='$bulan' AND a.tahun='$tahun' 
					AND a.kode_unit = '$unit_jahit' AND a.status_approve='t' AND b.jum_stok_opname <> 0 
					GROUP BY b.kode_brg_jadi
					)
					a GROUP BY a.kode_brg_jadi ORDER BY a.kode_brg_jadi";
				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
																		
						$data_stok[] = array(		'id'=> 0,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'stok_schedule'=> 0
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				return $data_stok;
				//-----------------------------------------------------------------
  }
  
  function get_stok_schedule($unit_jahit, $bulan, $tahun) {
		$query	= $this->db->query(" SELECT a.id, a.kode_brg_jadi, a.qty, b.e_product_motifname 
					FROM tm_stok_schedule_wip a, tr_product_motif b
					WHERE a.kode_brg_jadi = b.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.kode_unit = '$unit_jahit'
					ORDER BY a.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datastok = array();
			foreach ($hasil as $row) {
				$datastok[] = array( 	'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'stok_schedule'=> $row->qty
									);
			}
		}
		else {
			$datastok = '';
		}
		return $datastok;
  }
  
  function cek_stokschedule($unit_jahit, $bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tm_stok_schedule_wip
						WHERE kode_unit='$unit_jahit' AND bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  // 28-06-2014
  function get_transaksi_hasil_jahit($bulan, $tahun, $id_gudang, $kode_brg_jadi) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// 1. ambil data2 stok di gudang WIP perusahaan
		$sql = " SELECT id, kode_brg_jadi FROM tm_stok_hasil_jahit WHERE id_gudang = '$id_gudang' ";
		if ($kode_brg_jadi != '')
			$sql.= " AND kode_brg_jadi = '$kode_brg_jadi' ";
		$sql.=" ORDER BY kode_brg_jadi ";
		$query	= $this->db->query($sql);
		$data_brg_jadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row) {
				$data_warna = array();
				$data_so_warna = array();
				
				//ambil data2 warna dari tm_stok_hasil_jahit_warna
				$sqlwarna = " SELECT a.kode_warna, b.nama FROM tm_stok_hasil_jahit_warna a, tm_warna b 
							WHERE a.kode_warna=b.kode AND a.id_stok_hasil_jahit='$row->id' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk1'=> 0,
												'tot_keluar1'=> 0,
												'tot_masuk2'=> 0,
												'tot_keluar2'=> 0
											);
					}
				}
				
				// ambil SO brg jadi dari tt_stok_opname_hasil_jahit_detail_warna
				$sqlwarna = " SELECT c.kode_warna, d.nama, c.jum_stok_opname 
							FROM tt_stok_opname_hasil_jahit a, tt_stok_opname_hasil_jahit_detail b, 
							tt_stok_opname_hasil_jahit_detail_warna c, tm_warna d 
							WHERE a.id = b.id_stok_opname_hasil_jahit AND b.id = c.id_stok_opname_hasil_jahit_detail 
							AND c.kode_warna=d.kode AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang='$id_gudang' "; //echo $sqlwarna; die();
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['kode_warna'] == $rowwarna->kode_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_stok_opname;
							}
						} // end for
						
						$data_so_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_stok_opname'=> $rowwarna->jum_stok_opname
											);
					}
				}
				
				// ambil nama brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				// QUERY UTK KELUAR MASUK BRG JADI KE UNIT DAN KE PACKING/GDG JADI
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, 1 as masukwipunit, 0 as keluarwipunit, 0 as masukwippacking, 0 as keluarwippacking
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip 
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						 AND (a.jenis_masuk = '1' OR a.jenis_masuk = '2')
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwipunit, 1 as keluarwipunit, 0 as masukwippacking, 0 as keluarwippacking
						FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						AND a.jenis_keluar = '3'
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwipunit, 0 as keluarwipunit, 1 as masukwippacking, 0 as keluarwippacking
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip 
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						 AND (a.jenis_masuk = '3' OR a.jenis_masuk = '4')
						 UNION
						 SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwipunit, 0 as keluarwipunit, 0 as masukwippacking, 1 as keluarwippacking
						FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.id_gudang='$id_gudang'
						AND (a.jenis_keluar = '1' OR a.jenis_keluar = '2')
						ORDER BY tgl_sj ASC ";
				
				// 10-05-2014 contoh union di modul info-pembelian mmaster.php.
				
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$is_masukwipunit = $row2->masukwipunit;
						$is_keluarwipunit = $row2->keluarwipunit;
						$is_masukwippacking = $row2->masukwippacking;
						$is_keluarwippacking = $row2->keluarwippacking;
												
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($is_masukwipunit == '1' || $is_masukwippacking == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c 
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						else if ($is_keluarwipunit == '1' || $is_keluarwippacking == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c 
									WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$sqlxx = " SELECT nama FROM tm_warna WHERE kode='$row3->kode_warna' ";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nama_warna = $hasilxx->nama;
								}
								else
									$nama_warna = '';
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['kode_warna'] == $row3->kode_warna) {
										if ($masuk == "ya") {
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											$data_warna[$xx]['saldo']-= $row3->qty;
											//$data_warna[$xx]['tot_keluar']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($is_masukwipunit == '1') {
											$data_warna[$xx]['tot_masuk1']+= $row3->qty;
										}
										else if ($is_keluarwipunit == '1') {
											$data_warna[$xx]['tot_keluar1']+= $row3->qty;
										}
										else if ($is_masukwippacking == '1') {
											$data_warna[$xx]['tot_masuk2']+= $row3->qty;
										}
										else if ($is_keluarwippacking == '1') {
											$data_warna[$xx]['tot_keluar2']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'is_masukwipunit'=> $is_masukwipunit,
												'is_keluarwipunit'=> $is_keluarwipunit,
												'is_masukwippacking'=> $is_masukwippacking,
												'is_keluarwippacking'=> $is_keluarwippacking,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
				// =================================== END NEW =======================================================
				
				// array brg jadi
				$data_brg_jadi[] = array(		'kode_brg_jadi'=> $row->kode_brg_jadi,
												'nama_brg_jadi'=> $e_product_motifname,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna
									);
				$data_tabel1 = array();
				$data_tabel2 = array();
				$data_warna = array();
				$data_so_warna = array();
			} // endforeach header
		}
		else {
			$data_brg_jadi = '';
		}
		return $data_brg_jadi;
  }
  
  // 29-08-2014
  function get_mutasi_unit_qc($bulan, $tahun) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
		// 1. query brg bagus
		// 16-10-2014
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautamabagus = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
						INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						AND a.jenis_masuk = '1' AND a.kode_unit_jahit = '".$rowutama->kode_unit."'
					  UNION SELECT DISTINCT a.kode_unit as kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a
					  INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
					  WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
					  AND a.kode_unit = '".$rowutama->kode_unit."'
					 ORDER BY kode_unit_jahit, kode_brg_jadi
					 "; //echo $sql2; die();
				$query2	= $this->db->query($sql2);
				
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
						
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
																		
						// ambil jum masuk wip (SJ masuk WIP)=====================
						// 1. hitung brg masuk WIP bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row2->kode_unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
												
						// 2. hitung brg masuk retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_retur_bhnbaku = $hasilrow->jum_masuk;
							
							if ($masuk_retur_bhnbaku == '')
								$masuk_retur_bhnbaku = 0;
						}
						else
							$masuk_retur_bhnbaku = 0;
						
						$jum_masuk_bgs = $masuk_bgs+$masuk_retur_bhnbaku;
						//$jum_masuk_perbaikan = $masuk_perbaikan;
						//$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						/*
						 * $stok_akhir_bgs = $saldo_awal_bgs + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						 */
						// -------------------------------- END BARANG MASUK -----------------------------------------------
					
						//========================================================
						
						// ambil retur, dari tabel tm_retur_qc_wip
						$query3	= $this->db->query(" SELECT qty FROM tm_retur_qc_wip WHERE kode_unit = '$row2->kode_unit_jahit'
											AND bulan='$bulan' AND tahun='$tahun' AND jenis = 'BAGUS'
											AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_retur_bgs = $hasilrow->qty;
						}
						else {
							$jum_retur_bgs = 0;
						}
						
						$gradea_bgs = $jum_masuk_bgs-$jum_retur_bgs;
						//$gradea_perbaikan = $jum_masuk_perbaikan-$jum_retur_perbaikan;
						
						if ($jum_masuk_bgs != 0)
							$persena_bgs = ($gradea_bgs/$jum_masuk_bgs)*100;
						else
							$persena_bgs = 0;
						//$persena_perbaikan = $gradea_perbaikan/$jum_masuk_perbaikan;
						
						if ($jum_masuk_bgs != 0)
							$persen_retur_bgs = ($jum_retur_bgs/$jum_masuk_bgs)*100;
						else
							$persen_retur_bgs = 0;
						//$persen_retur_perbaikan = $jum_retur_perbaikan/$jum_masuk_perbaikan;
						
						$data_stok[] = array(		'id'=> 0,
													'kode_unit_jahit'=> $row2->kode_unit_jahit,
													'nama_unit_jahit'=> $nama_unit_jahit,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'qty'=> $jum_masuk_bgs,
													'retur'=> $jum_retur_bgs,
													'gradea'=> $gradea_bgs,
													'persena'=> $persena_bgs,
													'persen_retur'=> $persen_retur_bgs
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				
				$datautamabagus[] = array(		'kode'=> $rowutama->kode,
												'nama'=> $rowutama->nama,
												'data_stok'=> $data_stok,
												'data_stok2'=> ''
													);
					
			} // end foreach
		} // end if queryutama
		
		/*$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
					INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
					WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
					AND a.jenis_masuk = '1'
				  UNION SELECT DISTINCT a.kode_unit as kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a
				  INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
				  WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				 ORDER BY kode_unit_jahit, kode_brg_jadi
				 "; */
		//$query2	= $this->db->query($sql2);
								
				
		// 2. barang perbaikan
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautamaperbaikan = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
							WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
							AND a.jenis_masuk = '2' AND a.kode_unit_jahit = '".$rowutama->kode_unit."' 
							ORDER BY a.kode_unit_jahit, b.kode_brg_jadi ";
				$query2	= $this->db->query($sql2);
				
				$data_stok2 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
						
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
																		
						// ambil jum masuk wip (SJ masuk WIP)=====================
						// 1. hitung brg masuk WIP PERBAIKAN, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row2->kode_unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_perbaikan = $hasilrow->jum_masuk;
							
							if ($masuk_perbaikan == '')
								$masuk_perbaikan = 0;
						}
						else
							$masuk_perbaikan = 0;
						
						$jum_masuk_perbaikan = $masuk_perbaikan;
						// -------------------------------- END BARANG MASUK -----------------------------------------------
					
						//========================================================
						
						// ambil retur, dari tabel tm_retur_qc_wip
						$query3	= $this->db->query(" SELECT qty FROM tm_retur_qc_wip WHERE kode_unit = '$row2->kode_unit_jahit'
											AND bulan='$bulan' AND tahun='$tahun' AND jenis = 'PERBAIKAN'
											AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_retur_perbaikan = $hasilrow->qty;
						}
						else {
							$jum_retur_perbaikan = 0;
						}
						
						//$gradea_bgs = $jum_masuk_bgs-$jum_retur_bgs;
						$gradea_perbaikan = $jum_masuk_perbaikan-$jum_retur_perbaikan;
						
						//$persena_bgs = $gradea_bgs/$jum_masuk_bgs;
						if ($jum_masuk_perbaikan != 0)
							$persena_perbaikan = ($gradea_perbaikan/$jum_masuk_perbaikan)*100;
						else
							$persena_perbaikan = 0;
						
						//$persen_retur_bgs = $jum_retur_bgs/$jum_masuk_bgs;
						if ($jum_masuk_perbaikan != 0)
							$persen_retur_perbaikan = ($jum_retur_perbaikan/$jum_masuk_perbaikan)*100;
						else
							$persen_retur_perbaikan = 0;
						
						$data_stok2[] = array(		'id'=> 0,
													'kode_unit_jahit'=> $row2->kode_unit_jahit,
													'nama_unit_jahit'=> $nama_unit_jahit,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'qty'=> $jum_masuk_perbaikan,
													'retur'=> $jum_retur_perbaikan,
													'gradea'=> $gradea_perbaikan,
													'persena'=> $persena_perbaikan,
													'persen_retur'=> $persen_retur_perbaikan
													);
					} // end foreach2
				}
				else
					$data_stok2 = '';
				
				$datautamaperbaikan[] = array(		'kode'=> $rowutama->kode,
													'nama'=> $rowutama->nama,
													'data_stok'=> '',
													'data_stok2'=> $data_stok2
													);
			} // end foreachutama
		} // end ifutama
		
		// 2. barang perbaikan
		/*$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
					INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
					WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
					AND a.jenis_masuk = '2' ORDER BY a.kode_unit_jahit, b.kode_brg_jadi
				 ";
		$query2	= $this->db->query($sql2); */
								
				
					
		$hasil= array('datautamabagus'=>$datautamabagus,
					'datautamaperbaikan'=>$datautamaperbaikan);
				
		return $hasil;
				//-----------------------------------------------------------------
  }
  
  // 30-08-2014
  function cek_returqc($bulan, $tahun, $jenis) {
	$query3	= $this->db->query(" SELECT id FROM tm_retur_qc_wip
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND jenis='$jenis' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_retur_qc($bulan, $tahun, $jenis) {
		$query	= $this->db->query(" SELECT a.id, a.kode_unit, a.kode_brg_jadi, a.qty, b.e_product_motifname 
					FROM tm_retur_qc_wip a, tr_product_motif b
					WHERE a.kode_brg_jadi = b.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.jenis = '$jenis'
					ORDER BY a.kode_unit, a.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datastok = array();
			foreach ($hasil as $row) {
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
				
				$datastok[] = array( 	'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'kode_unit_jahit'=> $row->kode_unit,
										'nama_unit_jahit'=> $nama_unit_jahit,
										'retur'=> $row->qty
									);
			}
		}
		else {
			$datastok = '';
		}
		return $datastok;
  }
  
  // 23-09-2014
  function cek_forecast_unit($bulan, $tahun, $id_kel_unit) {
	/*$jumstring = strlen($kodeunit);
	  
	$new_kodeunit = substr($kodeunit, 0, $jumstring-1);
	$sql = " SELECT distinct id FROM tm_forecast_wip_unit WHERE bulan = '$bulan' AND tahun = '$tahun'
					AND id IN (SELECT distinct id_forecast_wip_unit FROM tm_forecast_wip_unit_listunit WHERE 
							kode_unit IN (".$new_kodeunit.") ) "; */
	
	$sql = " SELECT id FROM tm_forecast_wip_unit WHERE bulan = '$bulan' AND tahun = '$tahun'
			 AND id_kel_forecast_wip_unit = '$id_kel_unit' ";
	
	$query3	= $this->db->query($sql);
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_forecast_unit($bulan, $tahun, $id_kel_unit) {
	  /*$jumstring = strlen($kodeunit);
	  $new_kodeunit = substr($kodeunit, 0, $jumstring-1); */
	  
		/*$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.id IN (SELECT distinct id_forecast_wip_unit FROM tm_forecast_wip_unit_listunit WHERE 
							kode_unit IN (".$new_kodeunit.") )
					ORDER BY b.id "); */
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.id_kel_forecast_wip_unit = '$id_kel_unit'
					ORDER BY b.id ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function saveforecastunit($is_new, $id_fc, $iddetail, $kode_brg_jadi, $fc, $ket){ 
	  $tgl = date("Y-m-d H:i:s"); 
	 // 23-09-2014, adopsi dari forecast biasa
	 // 02-04-2014, ga pake is_new lagi. langsung save aja setelah delete detail sebelumnya
	//  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip_unit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailx	= $seqrow->id+1;
			}else{
				$iddetailx	= 1;
			}
		  
		   $data_detail = array(
						'id'=>$iddetailx,
						'id_forecast_wip_unit'=>$id_fc,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'qty'=>$fc,
						'keterangan'=>$ket
					);
		   $this->db->insert('tm_forecast_wip_unit_detail',$data_detail);
	/*  }
	  else {
		  $iddetailx = $iddetail;
		  
		  $this->db->query(" UPDATE tm_forecast_wip_detail SET qty = '$fc', keterangan='$ket'
						where id = '$iddetailx' ");
			// ====================
	  } */
  }
  
  function get_forecastvsscheduleunit($bulan, $tahun, $id_kel_unit) {
	  
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// modif 17-09-2014
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		/*$jumstring = strlen($kodeunit);
		$new_kodeunit = substr($kodeunit, 0, $jumstring-1);
		
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.id IN (SELECT distinct id_forecast_wip_unit FROM tm_forecast_wip_unit_listunit WHERE 
							kode_unit IN (".$new_kodeunit.") )
					ORDER BY d.kode, b.kode_brg_jadi "); */
		
		// ambil list unit jahit di tabel kel_forecast_wip_unit_listunit
		$query3	= $this->db->query(" SELECT a.kode_unit, b.nama FROM tm_kel_forecast_wip_unit_listunit a 
							INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit 
							WHERE a.id_kel_forecast_wip_unit = '".$id_kel_unit."' ORDER BY a.kode_unit ");
							
		$kodeunit=""; $new_kodeunit="";
		if ($query3->num_rows() > 0){
			$hasil3 = $query3->result();
				
			foreach ($hasil3 as $row3) {
				$kodeunit.= "'".$row3->kode_unit."',";
			}
		}
		$jumstring = strlen($kodeunit);
		$new_kodeunit = substr($kodeunit, 0, $jumstring-1);
		
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.id_kel_forecast_wip_unit = '$id_kel_unit'
					ORDER BY d.kode, b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				// ambil SO di unit2 jahit yg dipilih di bulan sebelumnya
				$queryx	= $this->db->query(" SELECT sum(b.jum_bagus) as jumbagus FROM tt_stok_opname_unit_jahit a 
											INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't'
											AND a.kode_unit IN (".$new_kodeunit.") ");
						
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal_bgs = $hasilrow->jumbagus;
					if ($saldo_awal_bgs == '')
						$saldo_awal_bgs = 0;
				}
				else {
					$saldo_awal_bgs = 0;
				}
				
				// KOLOM SCHEDULE ambil data barang masuk ke unit jahit
				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.jenis_keluar = '1'
									AND a.kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						$jum_masuk = $masuk_bgs;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit <> '0' AND a.jenis_masuk = '1'
									AND a.kode_unit_jahit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						$jum_keluar = $keluar_bgs;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						// tambahan 27-08-2014, kolom schedule dimodif perhitungannya
						//kolom schedule = brg masuk - stock schedule bln trsbt + stock schedule 1 bln sblmnya
						
						// a. stok schedule bulan tsb
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bulan' AND tahun='$tahun' AND kode_brg_jadi = '$row->kode_brg_jadi'
									AND kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule = $hasilrow->qty;
							
							if ($qty_schedule == '')
								$qty_schedule = 0;
						}
						else
							$qty_schedule = 0;
						
						// b. stok schedule 1 bulan sebelumnya
						if ($bulan == 1) {
							$bln_query = 12;
							$thn_query = $tahun-1;
						}
						else {
							$bln_query = $bulan-1;
							$thn_query = $tahun;
							if ($bln_query < 10)
								$bln_query = "0".$bln_query;
						}
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bln_query' AND tahun='$thn_query' AND kode_brg_jadi = '$row->kode_brg_jadi'
									AND kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule_sebelumnya = $hasilrow->qty;
							
							if ($qty_schedule_sebelumnya == '')
								$qty_schedule_sebelumnya = 0;
						}
						else
							$qty_schedule_sebelumnya = 0;
						
						$totalqtyschedule = $jum_masuk-$qty_schedule+$qty_schedule_sebelumnya;
						//===========================================================
						if ($row->qty != 0) {
							//$persenschedule = ($jum_masuk/$row->qty)*100;
							$persenschedule = ($totalqtyschedule/$row->qty)*100;
							$persenforecast = ($jum_keluar/$row->qty)*100;
						}
						else {
						//echo $row->qty."<br>";
							$persenschedule = 0;
							$persenforecast = 0;
						}
						// 17-09-2014
						//echo $totalqtyschedule." ".$saldo_awal_bgs."<br>";
						$temptotal = $totalqtyschedule+$saldo_awal_bgs;
						if ($temptotal != 0)
							$persenforecast2 = ($jum_keluar/($totalqtyschedule+$saldo_awal_bgs))*100;
						else
							$persenforecast2 = 0;
						
				//17-09-2014, ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan,
										//'schedule'=> $jum_masuk,
										'schedule'=> $totalqtyschedule,
										'brgmasuk'=> $jum_keluar,
										'persenschedule'=> $persenschedule,
										//'persenforecast'=> $persenforecast,
										'persenforecast2'=> $persenforecast2,
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel,
										'saldo_awal_bgs'=> $saldo_awal_bgs
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
}
