<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	
	function listbhn() {
	
	return $this->db->query("SELECT id as id_brg, kode_brg, nama_brg from tm_barang where status_aktif='t' ");

	}
	

	function listbhnperpages($limit,$offset) {
		//$db2=$this->load->database('db_external', TRUE);
	
	$query	= $this->db->query(" SELECT id as id_brg, kode_brg, nama_brg from tm_barang where status_aktif='t'  order by kode_brg LIMIT ".$limit." OFFSET ".$offset);
				
		if ($query->num_rows() > 0 ) {
			return $query->result();
		}
	}


	function flbahan($key) {

	return $this->db->query(" SELECT id as id_brg, kode_brg, nama_brg from tm_barang where status_aktif='t' and kode_brg LIKE '%$key%' ");		
	}


	function flbarangjadi($key) {

	return $this->db->query(" SELECT a.id as id_product, a.kode_brg as i_product, nama_brg as e_product_name, b.id_warna, c.nama as e_color_name 
									FROM tm_barang_wip a 
									INNER JOIN tm_warna_brg_wip b ON a.id=b.id_brg_wip
									INNER JOIN tm_warna c ON b.id_warna=c.id 
									WHERE a.kode_brg LIKE '%$key%'
									ORDER BY a.kode_brg ");		
	}


	function listdetailbrg(){

		$query = $this->db->query(" SELECT a.id as id_product, a.kode_brg as i_product, nama_brg as e_product_name, b.id_warna, c.nama as e_color_name 
									FROM tm_barang_wip a 
									INNER JOIN tm_warna_brg_wip b ON a.id=b.id_brg_wip
									INNER JOIN tm_warna c ON b.id_warna=c.id order by a.kode_brg");

		if($query->num_rows()>0) {
			return $query->result();
		}
	}


	function listdetailbrgperpage($limit, $offset){
	//ganti database salah narik data
		$query = $this->db->query(" SELECT * FROM( 
									SELECT a.id as id_product, a.kode_brg as i_product, nama_brg as e_product_name, b.id_warna, 
									c.nama as e_color_name,  d.id_brg_wip as leftid_brg
									FROM tm_barang_wip a 
									INNER JOIN tm_warna_brg_wip b ON a.id=b.id_brg_wip 
									INNER JOIN tm_warna c ON b.id_warna=c.id
									LEFT JOIN tr_barang_bhn d ON a.id=d.id_brg_wip AND b.id_warna=d.id_warna)
									a
									WHERE a.leftid_brg IS NULL
									order by a.i_product LIMIT ".$limit." OFFSET ".$offset);

		if($query->num_rows()>0) {
			return $query->result();
			}
		
	}

	function get_view(){
	//ganti database salah narik data
		$query = $this->db->query(" SELECT a.id_brg_wip, b.kode_brg as i_product, b.nama_brg as e_product_name, d.id as id_warna, 
									d.nama as e_color_name, 
									c.kode_brg as i_material, a.id_brg, c.nama_brg as e_material_name  
									from tr_barang_bhn a
									INNER JOIN tm_barang_wip b ON a.id_brg_wip=b.id
									INNER JOIN tm_barang c ON a.id_brg=c.id
									INNER JOIN tm_warna d ON a.id_warna=d.id
									where b.status_aktif='t'
									AND c.status_aktif='t' ORDER BY b.kode_brg ");

		if($query->num_rows()>0) {
			return $query->result();
			}
		
	}

	function get_view2($limit, $offset){
	//ganti database salah narik data
		$query = $this->db->query(" SELECT a.id_brg_wip, b.kode_brg as i_product, b.nama_brg as e_product_name, d.id as id_warna, 
									d.nama as e_color_name, 
									c.kode_brg as i_material, a.id_brg, c.nama_brg as e_material_name  
									from tr_barang_bhn a
									INNER JOIN tm_barang_wip b ON a.id_brg_wip=b.id
									INNER JOIN tm_barang c ON a.id_brg=c.id
									INNER JOIN tm_warna d ON a.id_warna=d.id
									where b.status_aktif='t'
									AND c.status_aktif='t' ORDER BY b.kode_brg LIMIT ".$limit." OFFSET ".$offset);

		if($query->num_rows()>0) {
			return $query->result();
			}
		
	}

	function get_viewx(){
	//ganti database salah narik data
		$query = $this->db->query(" SELECT 
									a.id_brg_wip, 
									a.i_product, 
									a.e_product_name, 
									a.id_warna, 
									a.e_color_name, 
									a.id_brg, 
									a.e_material_name, 
									a.qty_brg, 
									a.i_material, 
									a.qty_bhn, 
									a.qty_akhir
									,case when a.qty_akhir >= a.qty_brg then a.qty_brg when 
									 a.qty_akhir <= a.qty_brg then a.qty_akhir end as tot_compare
									from
									(
									SELECT a.id_brg_wip, 
									a.i_product, 
									c.nama_brg as e_product_name, 
									a.id_warna, 
									b.nama as e_color_name, 
									a.id_brg, e.nama_brg as e_material_name, 
									sum(a.qty_brg) as qty_brg, 
									a.i_material, 
									sum(a.qty_bhn) as qty_bhn, 
									sum(a.qty_bhn) - SUM(SUM(a.qty_brg)) OVER (PARTITION BY a.id_brg ORDER BY a.i_product, a.id_warna ROWS UNBOUNDED PRECEDING) AS qty_akhir
									FROM ( 
									SELECT e.id_brg_wip, e.i_product, e.id_warna, c.saldo_akhir as qty_brg, e.id_brg, e.i_material, 0 as qty_bhn 
									FROM tt_stok_opname_hasil_jahit a INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
									INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail 
									INNER JOIN tr_barang_bhn e ON e.id_brg_wip=b.id_brg_wip AND e.id_warna=c.id_warna 
									WHERE a.id_gudang = '7' 
									AND a.bulan = '09' 
									AND a.tahun = '2018' 
									UNION ALL 
									SELECT f.id_brg_wip, f.i_product, f.id_warna, 0 as qty_brg, f.id_brg, f.i_material, b.auto_saldo_akhir as qty_bhn 
									FROM tt_stok_opname_bahan_baku a INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
									INNER JOIN tr_barang_bhn f ON f.id_brg=b.id_brg 
									WHERE a.bulan = '09' 
									AND a.tahun = '2018' 
									AND b.status_approve = 't' 
									AND a.status_approve = 't' ) a 
									INNER JOIN tm_warna b ON a.id_warna=b.id 
									INNER JOIN tm_barang_wip c ON a.id_brg_wip=c.id 
									INNER JOIN tm_barang e ON a.id_brg=e.id 
									GROUP BY a.id_brg_wip, a.i_product, c.nama_brg, a.id_warna, b.nama, a.id_brg, a.i_material, e.nama_brg
									) a 
									group by a.id_brg_wip, a.i_product, 
									a.e_product_name, 
									a.id_warna, 
									a.e_color_name, 
									a.id_brg, 
									a.e_material_name, 
									a.i_material,
									a.qty_brg, 
									a.qty_bhn, 
									a.qty_akhir
									ORDER BY a.i_product, a.id_warna ");

		if($query->num_rows()>0) {
			return $query->result();
			}
		
	}

	function get_viewx2($date_from, $date_to){
	//ganti database salah narik data
		$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	// explode date_from utk keperluan saldo awal 
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
			
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}
// to_date('$date_from','dd-mm-yyyy')
//  a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')



		$query = $this->db->query(" 

									SELECT a.id_brg_wip, a.i_product, a.e_product_name, a.id_warna, a.e_color_name, a.id_brg, a.e_material_name, 
									a.qty_brg, a.i_material, a.qty_bhn, a.qty_akhir , 
									case when a.qty_akhir >= 0 then a.qty_brg when a.qty_akhir <= a.qty_brg then a.qty_akhir end as tot_compare 
									from ( 
									SELECT a.id_brg_wip, a.i_product, c.nama_brg as e_product_name, a.id_warna, b.nama as e_color_name, a.id_brg, 
									e.nama_brg as e_material_name, sum(a.qty_brg) as qty_brg, a.i_material, sum(a.qty_bhn) as qty_bhn, 
									sum(a.qty_bhn) - SUM(SUM(a.qty_brg)) 
									OVER (PARTITION BY a.id_brg ORDER BY a.i_product, a.id_warna ROWS UNBOUNDED PRECEDING) AS qty_akhir 
									FROM ( 

									SELECT b.id_brg_wip, b.i_product, b.id_warna, a.saldo_akhir as qty_brg, b.id_brg, b.i_material, 0 as qty_bhn 
									FROM f_mutasi_qc('$bln_query', '$thn_query', to_date('$date_from','dd-mm-yyyy'), to_date('$date_to','dd-mm-yyyy')) a
									INNER JOIN tr_barang_bhn b ON a.id_brg_wip=b.id_brg_wip AND a.id_warna=b.id_warna

									UNION ALL 

									SELECT b.id_brg_wip, b.i_product, b.id_warna, 0 as qty_brg, b.id_brg, b.i_material, a.saldo_akhir as qty_bhn 
									FROM f_mutasi_gudangstoksaja('$bln_query' , '$thn_query', to_date('$date_from','dd-mm-yyyy'), to_date('$date_to','dd-mm-yyyy'), '$bln1', '$thn1') a 
									INNER JOIN tr_barang_bhn b ON a.id_brg=b.id_brg ) a 

									INNER JOIN tm_warna b ON a.id_warna=b.id 
									INNER JOIN tm_barang_wip c ON a.id_brg_wip=c.id 
									INNER JOIN tm_barang e ON a.id_brg=e.id 
									GROUP BY a.id_brg_wip, a.i_product, c.nama_brg, a.id_warna, b.nama, a.id_brg, a.i_material, e.nama_brg ) a 
									group by a.id_brg_wip, a.i_product, a.e_product_name, a.id_warna, a.e_color_name, a.id_brg, a.e_material_name, 
									a.i_material, a.qty_brg, a.qty_bhn, a.qty_akhir 
									ORDER BY a.i_product, a.id_warna, a.i_material");

		if($query->num_rows()>0) {
			return $query->result();
			}
		
	}


	
	function msimpanbaru($idproduct,$iproduct,$icolor,$id_brg,$i_material,$iteration) {
  	 $query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
	    for($x=0;$x<=$iteration;$x++) {
		    for ($xx=0; $xx<count($i_material[$x]); $xx++) {

          $this->db->query("insert into tr_barang_bhn
          						 (id_brg_wip, i_product, id_warna, id_brg, i_material, d_entry)
						          	  values
					          	 ('$idproduct','$iproduct','$icolor','$id_brg[$x]','$i_material[$x]', '$dentry')");
		    }
	    }
    
	  redirect('barangbhn/cformbaru/');
	}


	function delete($iproduct,$id_warna) 
    {
          $this->db->query("DELETE FROM tr_barang_bhn WHERE id_brg_wip='$iproduct' AND id_warna='$id_warna'");
    }

}
?>
