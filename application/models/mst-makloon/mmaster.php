<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAllukuranbisbisan(){
    $this->db->select('*');
    $this->db->from('tm_ukuran_bisbisan');
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get_ukuran_bisbisan($id){
    $query	= $this->db->query(" SELECT * FROM tm_ukuran_bisbisan WHERE id = '$id' ");
    return $query->result();		  
  }
  
  //
  function saveukuranbisbisan($id_satuan, $nama, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $data = array(
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_ukuran_bisbisan',$data); }
	else {
		
		$data = array(
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_satuan);
		$this->db->update('tm_ukuran_bisbisan',$data);  
	}
		
  }
  
  function deleteukuranbisbisan($id){    
    $this->db->delete('tm_ukuran_bisbisan', array('id' => $id));
  }
  
  function cek_data_ukuran_bisbisan($nama){
    $this->db->select("* FROM tm_ukuran_bisbisan WHERE UPPER(nama) = UPPER('$nama') ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }
  
  function getAllhargabisbisan(){
    $query	= $this->db->query(" SELECT a.id, a.id_supplier, a.jenis_potong, a.harga, a.tgl_update, b.kode_supplier, b.nama as nama_supplier 
					FROM tm_harga_bisbisan a
					INNER JOIN tm_supplier b ON a.id_supplier = b.id ORDER BY a.id_supplier, a.id ");    
    
    return $query->result();
  }
  
  function get_harga_bisbisan($id){
    $query	= $this->db->query(" SELECT * FROM tm_harga_bisbisan WHERE id = '$id' ");
    return $query->result();		  
  }
  
  function cek_data_harga_bisbisan($id_supplier, $jenis_potong){
    $this->db->select("* FROM tm_harga_bisbisan WHERE id_supplier = '$id_supplier' AND jenis_potong = '$jenis_potong' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savehargabisbisan($id_harga, $id_supplier, $jenis_potong, $harga, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $data = array(
      'id_supplier'=>$id_supplier,
      'jenis_potong'=>$jenis_potong,
      'harga'=>$harga,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_harga_bisbisan',$data); }
	else {
		
		$data = array(
		  'harga'=>$harga,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_harga);
		$this->db->update('tm_harga_bisbisan',$data);  
	}
		
  }
  
  function deletehargabisbisan($id){    
    $this->db->delete('tm_harga_bisbisan', array('id' => $id));
  }

}

