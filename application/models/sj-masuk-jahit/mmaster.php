<?php
class Mmaster extends CI_Model {
	
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $unit_jahit, $cari) {
	
	if ($cari=="all") {
		if ($unit_jahit=='0') {
			$this->db->select(" * FROM tm_sj_hasil_jahit ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_hasil_jahit WHERE kode_unit='$unit_jahit' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($unit_jahit!='0') {
			$this->db->select(" * FROM tm_sj_hasil_jahit WHERE kode_unit='$unit_jahit' AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_hasil_jahit WHERE UPPER(no_sj) like UPPER('%$cari%') ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	
		$data_masuk = array();
		$data_masuk_detail = array();

		if ($query->num_rows() > 0) {
			
			$hasil = $query->result();
			
			foreach ($hasil as $row1) {

				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$row1->kode_unit' ");
				
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit_detail WHERE id_sj_hasil_jahit='$row1->id' ");
				
				if ($query2->num_rows() > 0) {
					
					$hasil2=$query2->result();
					
					$idnya	= "";			
					$id_detailnya = "";
					
					foreach ($hasil2 as $row2) {
						
						$idnya	.= $row2->id_sj_proses_jahit."-";
						$id_detailnya .= $row2->id_sj_proses_jahit_detail."-";
						
						if($row2->kode_brg_jadi!='') {
							
							$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							
							if ($query3->num_rows() > 0) {
								
								$hasilrow = $query3->row();
								$kode_brg	= $row2->kode_brg_jadi;
								$nama_brg	= $hasilrow->e_product_motifname;
							}
							else {
								$kode_brg	= '';
								$nama_brg	= '';
							}
						}else{
							$kode_brg	= '';
							$nama_brg	= '';
						}

						if ($row2->id_sj_proses_jahit != '0' || $row2->id_sj_proses_jahit != '') {
							$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sj_proses_jahit WHERE id='$row2->id_sj_proses_jahit' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$no_sj_keluar	= $hasilrow->no_sj;
								$tgl_sj_keluar = $hasilrow->tgl_sj;
							}
							else {
								$no_sj_keluar	= '';
								$tgl_sj_keluar = '';
							}
						}
						else {
							$no_sj_keluar	= '';
							$tgl_sj_keluar = '';
						}
										
						$data_masuk_detail[] = array(
							'kode_brg' => $kode_brg,
							'nama' => $nama_brg,
							'qty' => $row2->qty_brg_jadi,
							'no_sj_keluar' => $no_sj_keluar,
							'tgl_sj_keluar' => $tgl_sj_keluar,							
							'idnya' => $idnya,
							'id_detailnya' => $id_detailnya,
							'ket_qty_warna' => $row2->ket_qty_warna );
					}
				}
				else {
					$data_masuk_detail = '';
				}
				
				/*
				$data_masuk[] = array('id' => $row1->id,	
								'id_sj_proses_jahit' => $row1->id_sj_proses_jahit,	
								'no_sj' => $row1->no_sj,
								'tgl_sj' => $row1->tgl_sj,
								'no_sj_keluar' => $no_sj_keluar,
								'tgl_sj_keluar' => $tgl_sj_keluar,
								'kode_unit' => $row1->kode_unit,
								'nama_unit' => $nama_unit,
								'keterangan' => $row1->keterangan,
								'tgl_update' => $row1->tgl_update,
								'data_masuk_detail' => $data_masuk_detail,
								'id_detailnya' => $id_detailnya,
								'status_edit' => $row1->status_edit );
				*/

				$data_masuk[] = array('id' => $row1->id,	
						'no_sj' => $row1->no_sj,
						'tgl_sj' => $row1->tgl_sj,
						'kode_unit' => $row1->kode_unit,
						'nama_unit' => $nama_unit,
						'keterangan' => $row1->keterangan,
						'tgl_update' => $row1->tgl_update,
						'data_masuk_detail' => $data_masuk_detail,
						'idnya' => $idnya,
						'id_detailnya' => $id_detailnya,
						'status_edit' => $row1->status_edit,
						'status_lunas' => $row1->status_lunas,
						'gtotal' => $row1->total );
				
				$id_detailnya = "";
				$idnya = "";
				
				$data_masuk_detail = array();
			}
		}
		else {
			$data_masuk = '';
		}
		
		return $data_masuk;
  }
  
  function getAlltanpalimit($unit_jahit, $cari) {
	  
	if ($cari=="all") {
		if ($unit_jahit=='0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit WHERE kode_unit='$unit_jahit' ");
	}
	else {
		if ($unit_jahit!='0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit WHERE kode_unit='$unit_jahit' 
			AND UPPER(no_sj) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit WHERE UPPER(no_sj) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
 
  function get_jahit(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit ASC ");    
    return $query->result();  
  } 
   
  function generate_nomor(){
			$th_now	= date("Y");
			
			$query3	= $this->db->query(" SELECT no_sj FROM tm_sj_hasil_packing ORDER BY id DESC, no_sj DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
			if(strlen($no_sj)==13) {
				$nosj = substr($no_sj, 4, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}
			$nomorsj = "SJM-".$nomorsj;

			return $nomorsj;  
  }
     
  function cek_data($no_sj) {
	  
    $this->db->select(" * FROM tm_sj_hasil_jahit WHERE no_sj='$no_sj' ", false);
    $query = $this->db->get();
    
    if ($query->num_rows()>0) {
		return $query->result();
	}
	
  }
  
  // function save($id_sj_keluar, $no_sj_masuk, $tgl_sj, $unit_jahit, $ket, $totalnya, $kode, $nama, $qty, $biaya, $id_sj_keluar, $id_sj_keluar_detail) {
  function save($id_sj_keluar, $no_sj_masuk, $tgl_sj, $unit_jahit, $ket, $kode, $nama, $qty, 
				$id_sj_keluar, $id_sj_keluar_detail, $is_no_sjkeluar, $gtotal, $harga, $harga_lama, $ket_warna, $total) {
  
    $tgl = date("Y-m-d");
    
    $this->db->select(" * FROM tm_sj_hasil_jahit WHERE no_sj='$no_sj_masuk' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
	
		if(count($hasil)==0) {
			/*
			$data_header = array(
			  'no_sj'=>$no_sj_masuk,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_jahit,
			  'keterangan'=>$ket,
			  'total'=>$totalnya,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'id_sj_proses_jahit'=>$id_sj_keluar
			);
			*/
			$data_header = array(
			  'no_sj'=>$no_sj_masuk,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_jahit,
			  'keterangan'=>$ket,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'total'=>$gtotal
			);
			$this->db->insert('tm_sj_hasil_jahit',$data_header);
			
			if ($is_no_sjkeluar == '')
				$this->db->query(" UPDATE tm_sj_proses_jahit SET status_jahit='t' WHERE id='$id_sj_keluar' ");
			
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_jahit ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj_hsl_jahit	= $hasilrow->id;
			
			if ($qty!='') {
					
				$data_detail = array(
					'id_sj_hasil_jahit'=>$id_sj_hsl_jahit,
					'kode_brg_jadi'=>$kode,
					'qty_brg_jadi'=>$qty,
					'id_sj_proses_jahit_detail'=>$id_sj_keluar_detail,
					'id_sj_proses_jahit'=>$id_sj_keluar,
					'harga'=>$harga,
					'biaya'=>$total,
					'ket_qty_warna'=>$ket_warna
				);
				$this->db->insert('tm_sj_hasil_jahit_detail',$data_detail);
				
				if ($is_no_sjkeluar == '')
					$this->db->query(" UPDATE tm_sj_proses_jahit_detail SET status_jahit='t' WHERE id='$id_sj_keluar_detail' ");
				
				if ($harga_lama != $harga) {
					// ========= 13-03-2012, insert ke tabel master tm_harga_hasil_jahit =================
					$sql3	= $this->db->query(" SELECT id FROM tm_harga_hasil_jahit WHERE kode_brg_jadi='$kode' 
											AND kode_unit = '$unit_jahit' AND harga = '$harga_lama' ");
					if($sql3->num_rows() > 0){
						$hasil3	= $sql3->row();
						$id_harga = $hasil3->id;
							
						$this->db->query(" UPDATE tm_harga_hasil_jahit SET harga='$harga', tgl_update ='$tgl' 
									WHERE id='$id_harga' ");
					}
					else {
						$data_detail2 = array(
							'kode_brg_jadi'=>$kode,
							'kode_unit'=>$unit_jahit,
							'harga'=>$harga,
							'tgl_input'=>$tgl,
							'tgl_update'=>$tgl
						);
						$this->db->insert('tm_harga_hasil_jahit',$data_detail2);
					}
				}
				// ================================================================
				
				$kodenya	= trim($kode);
				
				if($kodenya!=''){
					$qstokhsiljahit	= $this->db->query(" SELECT * FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kode' ORDER BY id DESC LIMIT 1 ");
					if($qstokhsiljahit->num_rows()>0){
						$rstokhsiljahit	= $qstokhsiljahit->row();
						$qtystokhsljahit= $rstokhsiljahit->stok;
						$idstokhsljahit	= $rstokhsiljahit->id;
	
						$stokhsiljahit_upt	= $qtystokhsljahit+$qty;
						$saldo_tt_stok	= $stokhsiljahit_upt;
						
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$stokhsiljahit_upt', tgl_update_stok='$tgl' WHERE kode_brg_jadi='$kode' AND id='$idstokhsljahit' ");					
					} else {
						$saldo_tt_stok	= $qty;
						$this->db->query(" INSERT INTO tm_stok_hasil_jahit(kode_brg_jadi,stok,tgl_update_stok) VALUES('$kode','$qty','$tgl') ");	
					}
					
					//$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,masuk,saldo,biaya,tgl_input) VALUES('$kode','$no_sj_masuk','$qty','$saldo_tt_stok','$biaya','$tgl') ");	
					//$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,masuk,saldo,tgl_input) VALUES('$kode','$no_sj_masuk','$qty','$saldo_tt_stok','$tgl') ");	
				}
			}

		}
		else {
			if ($is_no_sjkeluar == '')
				$this->db->query(" UPDATE tm_sj_proses_jahit SET status_jahit='t' WHERE id='$id_sj_keluar' ");
			
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_jahit ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj_hsl_jahit	= $hasilrow->id;
			
			if ($qty!='') {
					
				$data_detail = array(
					'id_sj_hasil_jahit'=>$id_sj_hsl_jahit,
					'kode_brg_jadi'=>$kode,
					'qty_brg_jadi'=>$qty,
					'id_sj_proses_jahit_detail'=>$id_sj_keluar_detail,
					'id_sj_proses_jahit'=>$id_sj_keluar,
					'harga'=>$harga,
					'biaya'=>$total,
					'ket_qty_warna'=>$ket_warna
				);
				
				$this->db->insert('tm_sj_hasil_jahit_detail',$data_detail);
				if ($is_no_sjkeluar == '')
					$this->db->query(" UPDATE tm_sj_proses_jahit_detail SET status_jahit='t' WHERE id='$id_sj_keluar_detail' ");
				
				if ($harga_lama != $harga) {
					// ========= 13-03-2012, insert ke tabel master tm_harga_hasil_jahit =================
					$sql3	= $this->db->query(" SELECT id FROM tm_harga_hasil_jahit WHERE kode_brg_jadi='$kode' 
											AND kode_unit = '$unit_jahit' AND harga = '$harga_lama' ");
					if($sql3->num_rows() > 0){
						$hasil3	= $sql3->row();
						$id_harga = $hasil3->id;
							
						$this->db->query(" UPDATE tm_harga_hasil_jahit SET harga='$harga', tgl_update ='$tgl' 
									WHERE id='$id_harga' ");
					}
					else {
						$data_detail2 = array(
							'kode_brg_jadi'=>$kode,
							'kode_unit'=>$unit_jahit,
							'harga'=>$harga,
							'tgl_input'=>$tgl,
							'tgl_update'=>$tgl
						);
						$this->db->insert('tm_harga_hasil_jahit',$data_detail2);
					}
				}
				// ================================================================
				
				$kodenya	= trim($kode);
				
				if($kodenya!=''){
					$qstokhsiljahit	= $this->db->query(" SELECT * FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kode' ORDER BY id DESC LIMIT 1 ");
					if($qstokhsiljahit->num_rows()>0){
						$rstokhsiljahit	= $qstokhsiljahit->row();
						$qtystokhsljahit= $rstokhsiljahit->stok;
						$idstokhsljahit	= $rstokhsiljahit->id;
	
						$stokhsiljahit_upt	= $qtystokhsljahit+$qty;
						$saldo_tt_stok	= $stokhsiljahit_upt;
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$stokhsiljahit_upt', tgl_update_stok='$tgl' WHERE kode_brg_jadi='$kode' AND id='$idstokhsljahit' ");					
					} else {
						$saldo_tt_stok	= $qty;
						$this->db->query(" INSERT INTO tm_stok_hasil_jahit(kode_brg_jadi,stok,tgl_update_stok) VALUES('$kode','$qty','$tgl') ");	
					}
					//$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,masuk,saldo,biaya,tgl_input) VALUES('$kode','$no_sj_masuk','$qty','$saldo_tt_stok','$biaya','$tgl') ");
					//$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,masuk,saldo,tgl_input) VALUES('$kode','$no_sj_masuk','$qty','$saldo_tt_stok','$tgl') ");
				}
			}
		}

  }
  
  function delete($idsj, $id_sj_proses_jahit_detail) {   
	   
	  $tgl = date("Y-m-d");
	  $idprodetail = explode("-", $id_sj_proses_jahit_detail);
	  
	  $query1	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit WHERE id='$idsj' ");
	  //$hasilrow1 = $query3->row();
	  //$id_sj_keluar	= $hasilrow1->id_sj_proses_jahit;
	  $hasilrow1 = $query1->row();
	  $idsjjahit = $hasilrow1->id;
	  $nosj		 = $hasilrow1->no_sj;
	  
	  $query2	= $this->db->query(" SELECT kode_brg_jadi, qty_brg_jadi, id_sj_proses_jahit, id_sj_proses_jahit_detail FROM tm_sj_hasil_jahit_detail WHERE id_sj_hasil_jahit='$idsjjahit' ");
	  
	  foreach($query2->result() as $row) {
		  
			$this->db->query(" UPDATE tm_sj_proses_jahit SET status_jahit='f', status_edit='f' WHERE id='$row->id_sj_proses_jahit' "); 
			$this->db->query(" UPDATE tm_sj_proses_jahit_detail SET status_jahit='f' WHERE id='$row->id_sj_proses_jahit_detail' ");

			if($row->kode_brg_jadi!='') {
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$row->kode_brg_jadi' ");
				if ($query3->num_rows()==0) {
					$stok_lama = 0;
				}
				else {
					$hasilrow3 = $query3->row();
					$stok_lama	= $hasilrow3->stok;
				}
				$new_stok = (($stok_lama)-($row->qty_brg_jadi));
					
				if ($query3->num_rows()==0) {
						$data_stok = array(
						'kode_brg_jadi'=>$row->kode_brg_jadi,
						'stok'=>$new_stok,
						'tgl_update_stok'=>$tgl
						);
						
					$this->db->insert('tm_stok_hasil_jahit',$data_stok);
				}
				else {
					$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$new_stok', tgl_update_stok='$tgl' WHERE kode_brg_jadi='$row->kode_brg_jadi' ");
				}
					
			//	$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,keluar,saldo,tgl_input) VALUES('$row->kode_brg_jadi','$nosj','$row->qty_brg_jadi','$new_stok','$tgl') ");	
			}			
	  }
	  
	  /*
	  $this->db->query(" UPDATE tm_sj_proses_jahit SET status_jahit='f', status_edit='f' WHERE id='$id_sj_keluar' "); 
	  
	  foreach($idprodetail as $row1) {
			if ($row1!='') {
				$this->db->query(" UPDATE tm_sj_proses_jahit_detail SET status_jahit='f' WHERE id='$row1' ");
			}
	  }
	  */
	  
	  /*
	  $query3	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit WHERE id='$idsj' ");
	  $hasilrow2 = $query3->row();
	  $id_sj	= $hasilrow2->id; 
	  $nosj		= $hasilrow2->no_sj; 
	  
	  $query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit_detail WHERE id_sj_hasil_jahit='$id_sj' ");
	  if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
										
			foreach ($hasil2 as $row2) {
				
				if($row2->kode_brg_jadi!=''){
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$row2->kode_brg_jadi' ");
					if ($query3->num_rows()==0){
						$stok_lama = 0;
					}
					else {
						$hasilrow3 = $query3->row();
						$stok_lama	= $hasilrow3->stok;
					}
					$new_stok = (($stok_lama)-($row2->qty_brg_jadi));
					
					if ($query3->num_rows()==0){
							$data_stok = array(
							'kode_brg_jadi'=>$row2->kode_brg_jadi,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
							
						$this->db->insert('tm_stok_hasil_jahit',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$new_stok', tgl_update_stok='$tgl' WHERE kode_brg_jadi='$row2->kode_brg_jadi' ");
					}
					
					$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi,no_bukti,keluar,saldo,biaya,tgl_input) VALUES('$row2->kode_brg_jadi','$nosj','$row2->qty_brg_jadi','$new_stok','$row2->biaya','$tgl') ");	
				}		
			}
	 }
	 */
	
	 $this->db->delete('tm_sj_hasil_jahit_detail', array('id_sj_hasil_jahit' => $idsj));
	 $this->db->delete('tm_sj_hasil_jahit', array('id' => $idsj));
  } 
  
  // function get_sj($id_sj_hsl_jahit,$id_sj_proses_jahit) {
  
  function get_sj($id_sj_hsl_jahit) {
	
	$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit WHERE id='$id_sj_hsl_jahit' ");    
    
    $hasil = $query->result();
    
    $data_sj = array();
    $detail_sj = array();
	
	foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_jahit_detail WHERE id_sj_hasil_jahit='$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						
						if($row2->kode_brg_jadi!=''){
							$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg_jd	= $row2->kode_brg_jadi;
								$nama_brg		= $hasilrow->e_product_motifname;
							}
							else {
								$kode_brg_jd	= '';
								$nama_brg		= '';
							}
						}else{
							$kode_brg_jd	= '';
							$nama_brg		= '';
						}

						$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sj_proses_jahit WHERE id='$row2->id_sj_proses_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$no_sj_keluar	= $hasilrow->no_sj;
							$tgl_sj_keluar	= $hasilrow->tgl_sj;
						}
						else {
							$no_sj_keluar	= '';
							$tgl_sj_keluar	= '';
						}

						$query4		= $this->db->query(" SELECT b.no_sj, a.id, a.id_sj_proses_jahit, a.kode_brg_jadi, a.kode_brg, a.qty_bhn, a.status_jahit, a.keterangan 
								FROM tm_sj_proses_jahit_detail a 
								INNER JOIN tm_sj_proses_jahit b ON a.id_sj_proses_jahit=b.id 
								
								WHERE a.id='$row2->id_sj_proses_jahit_detail' AND a.status_jahit='t' ");
						$hasilrow4 	= $query4->row();
						$kode_brg	= $hasilrow4->kode_brg;
									
						/*				
						$detail_sj[] = array('id' => $row2->id,
										'id_sj_hasil_jahit' => $row2->id_sj_hasil_jahit,
										'kode_brg_jadi' => $kode_brg_jd,
										'nama' => $nama_brg,
										'qty' => $row2->qty_brg_jadi,
										'id_sj_proses_jahit_detail' => $row2->id_sj_proses_jahit_detail,
										'biaya' => $row2->biaya );
						*/
						$detail_sj[] = array('id' => $row2->id,
								'id_sj_hasil_jahit' => $row2->id_sj_hasil_jahit,
								'kode_brg' => $kode_brg,
								'kode_brg_jadi' => $kode_brg_jd,
								'nama' => $nama_brg,
								'qty' => $row2->qty_brg_jadi,
								'no_sj_keluar' => $no_sj_keluar,
								'tgl_sj_keluar' => $tgl_sj_keluar,								
								'id_sj_proses_jahit' => $row2->id_sj_proses_jahit,
								'id_sj_proses_jahit_detail' => $row2->id_sj_proses_jahit_detail);
					}
				}
				else {
					$detail_sj = '';
				}		

				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				/*
				$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sj_proses_jahit WHERE id='$row1->id_sj_proses_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$no_sj_keluar	= $hasilrow->no_sj;
					$tgl_sj_keluar	= $hasilrow->tgl_sj;
				}
				else {
					$no_sj_keluar	= '';
					$tgl_sj_keluar	= '';
				}
				*/
				
				/*
				$data_sj[]	= array('id'=> $row1->id,	
								'id_sj_proses_jahit'=> $row1->id_sj_proses_jahit,	
								'no_sj'=> $row1->no_sj,
								'tgl_sj'=> $row1->tgl_sj,
								'no_sj_keluar'=> $no_sj_keluar,
								'tgl_sj_keluar'=> $tgl_sj_keluar,
								'kode_unit'=> $row1->kode_unit,
								'nama_unit'=> $nama_unit,
								'keterangan'=> $row1->keterangan,
								'tgl_update'=> $row1->tgl_update,
								'total'=> $row1->total,
								'detail_sj'=> $detail_sj );
				*/
				
				$data_sj[]	= array('id'=> $row1->id,	
								'id_sj_proses_jahit'=> $row1->id_sj_proses_jahit,	
								'no_sj'=> $row1->no_sj,
								'tgl_sj'=> $row1->tgl_sj,
								'kode_unit'=> $row1->kode_unit,
								'nama_unit'=> $nama_unit,
								'keterangan'=> $row1->keterangan,
								'tgl_update'=> $row1->tgl_update,
								'detail_sj'=> $detail_sj );
												
				$detail_sj = array();		
			}
	
	return $data_sj;
  }
  
  /***
  function get_unit_quilting(){
    $this->db->select(" * from tm_unit_quilting order by kode_unit ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_makloon(){
    $this->db->select("* from tm_jenis_makloon order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_bhn)
  {
	if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_bhn){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  ***/
  
  function get_sj_keluar($num, $offset, $cari, $unit_jahit) {
	
	if ($cari=="all") {

		$sql = " a.id AS id_proses_jahit, a.no_sj, a.tgl_sj, a.kode_unit, a.tgl_update, a.keterangan FROM  tm_sj_proses_jahit a 
		
		INNER JOIN tm_sj_proses_jahit_detail b ON a.id=b.id_sj_proses_jahit 
		
		WHERE a.kode_unit='$unit_jahit' AND b.status_jahit='f' 
		
		GROUP BY a.id, a.no_sj, a.tgl_sj, a.kode_unit, a.tgl_update, a.keterangan 
		
		ORDER BY no_sj DESC ";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {

		$sql = " a.id AS id_proses_jahit, a.no_sj, a.tgl_sj, a.kode_unit, a.tgl_update, a.keterangan FROM  tm_sj_proses_jahit a INNER JOIN tm_sj_proses_jahit_detail b ON a.id=b.id_sj_proses_jahit WHERE a.kode_unit='$unit_jahit' AND b.status_jahit='f' AND UPPER(a.no_sj) LIKE UPPER('%$cari%') 
		GROUP BY a.id, a.no_sj, a.tgl_sj, a.kode_unit, a.tgl_update, a.keterangan ORDER BY no_sj DESC ";
		$this->db->select($sql, false)->limit($num,$offset);
    }

    $query = $this->db->get();
    
    if ($query->num_rows() > 0){ 
		
		$hasil = $query->result();
		
		foreach ($hasil as $row1) {		
			
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_jahit_detail WHERE id_sj_proses_jahit='$row1->id_proses_jahit' AND status_jahit='f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						
						//if($row2->kel_bhn!='5'){

							$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
							
							if ($query3->num_rows() > 0){
								
								$hasilrow = $query3->row();
								
								$kode_brg_jd	= $row2->kode_brg_jadi;
								$nama_brg_jd	= $hasilrow->e_product_motifname;
							} else {
								$kode_brg_jd	= "";
								$nama_brg_jd	= "";
							}
						//} else {
							//$kode_brg_jd	= "";
							//$nama_brg_jd	= "";
						//}
						
						/***
						if($row2->kel_bhn=='1')
							$nama_kel	= "Bisbisan";
						else if($row2->kel_bhn=='2')
							$nama_kel	= "Bordir";
						else if($row2->kel_bhn=='3')
							$nama_kel	= "Print";
						else if($row2->kel_bhn=='4')
							$nama_kel	= "Asesoris";
						else if($row2->kel_bhn=='5')
							$nama_kel	= "Asesoris Murni";
						else if($row2->kel_bhn=='6')
							$nama_kel	= "Hasil Cutting";
						***/
						
						$query3	= $this->db->query(" SELECT sum(qty_brg_jadi) AS jml FROM tm_sj_hasil_jahit_detail WHERE id_sj_proses_jahit_detail='$row2->id' AND kode_brg_jadi='$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$jum_hasil = $hasilrow->jml;
						
						if ($jum_hasil==0)
							$qty_belum_jahit = $row2->qty_bhn;
						else
							$qty_belum_jahit = $row2->qty_bhn-$jum_hasil;
						
						/***
						$detail_bhn[] = array('id'=> $row2->id,
									'id_sj_proses_jahit'=> $row2->id_sj_proses_jahit,
									'kel_bhn' => $row2->kel_bhn,
									'nama_kel' => $nama_kel,
									'kode_brg' => $row2->kode_brg,
									'kode_brg_jd' => $kode_brg_jd,
									'nama_brg_jd' => $nama_brg_jd,
									'qty' => $qty_belum_jahit );
						***/
						
						$detail_bhn[] = array('id'=> $row2->id,
									'id_sj_proses_jahit'=> $row2->id_sj_proses_jahit,
									'kode_brg' => $row2->kode_brg,
									'kode_brg_jd' => $kode_brg_jd,
									'nama_brg_jd' => $nama_brg_jd,
									'qty' => $qty_belum_jahit );						
					}
				}
				else {
					$detail_bhn = '';
				}

			$data_bhn[] = array('id' => $row1->id_proses_jahit,	
							'no_sj' => $row1->no_sj,
							'tgl_sj' => $row1->tgl_sj,
							'kode_unit' => $row1->kode_unit,
							'tgl_update' => $row1->tgl_update,
							'keterangan' => $row1->keterangan,
							//'makloon_internal' => $row1->makloon_internal,
							'detail_bhn'=> $detail_bhn );
			
			$detail_bhn = array();
								
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_sj_keluartanpalimit($cari, $unit_jahit) {
	
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_sj_proses_jahit WHERE kode_unit='$unit_jahit' AND status_jahit='f' ORDER BY no_sj DESC ";
		
		$query	= $this->db->query($sql);
		
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	else {
		$sql = " SELECT * FROM tm_sj_proses_jahit WHERE kode_unit='$unit_jahit' AND status_jahit='f' ";
		$sql.=" AND UPPER(no_sj) LIKE UPPER('%$cari%') ORDER BY no_sj DESC ";
		
		$query	= $this->db->query($sql);

		if($query->num_rows()>0) {
			return $query->result();
		}
	}
  }
 

  function get_detail_sj_keluar($id_sj_keluar, $id_proses_jahit_detail, $unit_jahit){
	  
    $detail_sj = array();
        
    foreach($id_proses_jahit_detail as $row1) {
		
		if($row1!='') {
			
			$query2		= $this->db->query(" SELECT b.no_sj, b.tgl_sj, a.id, a.id_sj_proses_jahit, a.kode_brg_jadi, a.kode_brg, a.qty_bhn, a.status_jahit, a.keterangan 
					FROM tm_sj_proses_jahit_detail a 
					INNER JOIN tm_sj_proses_jahit b ON a.id_sj_proses_jahit=b.id 
					
					WHERE a.id='$row1' AND a.status_jahit='f' ");
					
			$hasilrow 	= $query2->row();
			$kode_brg_jd= $hasilrow->kode_brg_jadi;
			$qty		= $hasilrow->qty_bhn;
			
			//$query4	= $this->db->query(" SELECT kode_brg_jadi FROM tm_sj_proses_jahit_detail WHERE kode_brg_jadi='$kode_brg_jd' AND status_jahit='f' GROUP BY kode_brg_jadi ");
			//if($query4->num_rows()>0){
			//}
			
			/* 30072011
			$query3	= $this->db->query(" SELECT sum(qty_brg_jadi) AS jml FROM tm_sj_hasil_jahit_detail WHERE id_sj_proses_jahit_detail='$hasilrow->id' AND kode_brg_jadi='$kode_brg_jd' ");
			*/
			
			$query3	= $this->db->query(" SELECT sum(qty_brg_jadi) AS jml FROM tm_sj_hasil_jahit_detail WHERE kode_brg_jadi='$kode_brg_jd' ");
			$hasilrow2 = $query3->row();
			$jum_hasil = $hasilrow2->jml;
			
			$qty_belum_jahit = $qty-$jum_hasil;
			
			$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$kode_brg_jd' ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				//$kode_brg_jd	= $hasilrow->kode_brg_jadi;
				$nama_brg_jd	= $hasilrow3->e_product_motifname;
			}
			else {
				$kode_brg_jd	= '';
				$nama_brg_jd	= '';
			}
			
			$pisah1 = explode("-", $hasilrow->tgl_sj);
			$tgl1= $pisah1[2];
			$bln1= $pisah1[1];
			$thn1= $pisah1[0];
					
			$tgl_sj = $tgl1."-".$bln1."-".$thn1;
			
			// 13-03-2012, ambil harga per item brg
			$query3	= $this->db->query(" SELECT harga FROM tm_harga_hasil_jahit WHERE kode_brg_jadi='$kode_brg_jd'
								AND kode_unit = '$unit_jahit' ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				$harga	= $hasilrow3->harga;
			}
			else {
				$harga	= '0';
			}
		
			$detail_sj[] = array('idsjprosesjahitdetail' => $hasilrow->id,
					'idsjprosesjahit' => $hasilrow->id_sj_proses_jahit,
					'no_sj' => $hasilrow->no_sj,
					'tgl_sj' => $tgl_sj,
					'kode_brg' => $hasilrow->kode_brg,
					'kode_brg_jadi' => $kode_brg_jd,
					'nama_brg_jd' => $nama_brg_jd,
					'qty' => $qty_belum_jahit,
					'harga' => $harga
			);
		}
	}
	return $detail_sj;
  }
  
  // 12-03-2012
  function get_brgjadi($num, $offset, $cari, $kode_unit)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			
			$query3	= $this->db->query(" SELECT harga FROM tm_harga_hasil_jahit WHERE kode_brg_jadi='$row1->i_product_motif'
								AND kode_unit = '$kode_unit' ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				$harganya = $hasilrow3->harga;
			}
			else
				$harganya = 0;
			
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname,
										'harga_jahit'=> $harganya
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari, $kode_unit){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }

}
