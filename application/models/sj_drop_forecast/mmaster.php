<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE a.jenis='3' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
   function get_branch(){
	   $db2=$this->load->database('db_external',TRUE);
    $query = $db2->query(" SELECT * from tr_branch order by e_branch_name");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAll($num, $offset, $cari, $date_from, $date_to,$gudang, $caribrg, $filterbrg) {	  
		$pencarian = "";
		
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_sj) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			if ($gudang != "0")
				$pencarian.= " AND id_gudang = '$gudang' ";	
			
			if($caribrg != "all"){
				$pencarian.= " AND c.kode_brg = '$caribrg' ";
			}	
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
					if ($gudang != "0")
				$pencarian.= " AND a.id_gudang = '$gudang' ";	
					if ($caribrg != "all"){
				$pencarian.= " AND c.kode_brg = '$caribrg' ";	
			}
			$pencarian.= "GROUP BY a.id ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		//~ $this->db->select(" * FROM tm_sj_drop_forecast WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		
			$this->db->select(" a.* FROM tm_sj_drop_forecast a inner join tm_sj_drop_forecast_detail b ON a.id=b.id_sj_drop_forecast
		Inner join tm_barang_wip c on b.id_brg=c.id  WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_drop_forecast_detail WHERE id_sj_drop_forecast = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						
							$query3	= $this->db->query(" SELECT kode_brg, nama_brg
									FROM tm_barang_wip  WHERE id = '$row2->id_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg	= $hasilrow->kode_brg;
								$nama_brg	= $hasilrow->nama_brg;
								
							}
							else {
								$kode_brg = '';
								$nama_brg = '';
							
							}
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							
						}
						else {
							$kode_brg = '';
							$nama_brg = '';
							
						}
						
						
						
						
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												
												'qty'=> $row2->qty,
												
												'keterangan'=> $row2->keterangan,
												
												//~ 'harga_diluar'=> $harga
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
					if ($row1->jenis_keluar=='1')
					$nama_jenis = "Pengiriman Drop Forecast";
				else if ($row1->jenis_keluar=='2')
					$nama_jenis = "Penggantian Retur";
				else if ($row1->jenis_keluar=='3')
					$nama_jenis = "Dibebankan";
				else{
					$nama_jenis = "lain - Lain";
						}
					
									$query41 = $this->db->query("SELECT * FROM tm_periode_bb order by id desc limit 1");
					if($query41->num_rows() > 0){
					$hasilrow41 = $query41->row();
					$tahun_periode = $hasilrow41->i_year;
					$bulan_periode = $hasilrow41->i_month;
					$tanggal_periode = $hasilrow41->i_periode;
}
else{
	$tahun_periode = '';
					$bulan_periode = '';
					$tanggal_periode = '';
	}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'no_manual'=> $row1->no_manual,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'keterangan'=> $row1->keterangan,
											'tahun_periode'=> $tahun_periode,
											'bulan_periode'=> $bulan_periode,
											'tanggal_periode'=>$tanggal_periode,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function getAlltanpalimit($cari, $date_from, $date_to,$gudang, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_sj) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			if ($gudang != "0")
				$pencarian.= " AND id_gudang = '$gudang' ";	
				if ($caribrg != "all"){
				$pencarian.= " AND c.kode_brg = '$caribrg' ";	
			}
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
					if ($gudang != "0")
				$pencarian.= " AND a.id_gudang = '$gudang' ";	
					if ($caribrg != "all"){
				$pencarian.= " AND c.kode_brg = '$caribrg' ";	
			}
			$pencarian.= "GROUP BY a.id ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		//~ $query	= $this->db->query(" SELECT * FROM tm_sj_drop_forecast WHERE TRUE ".$pencarian." ");
		
$query	=  $this->db->query(" SELECT a.* FROM tm_sj_drop_forecast a inner join tm_sj_drop_forecast_detail b ON a.id=b.id_sj_drop_forecast
		Inner join tm_barang_wip c on b.id_brg=c.id  WHERE TRUE ".$pencarian." ");
    return $query->result();  
  }
  
  //
  function save($id_sj, $id_gudang, $jenis_masuk,$ket, 
					$id_brg_wip, $kode_brg_wip, $temp_qty, $id_warna, $qty_warna, $ket_detail){  

    $tgl = date("Y-m-d H:i:s");
			
			$qtytotal = 0;
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
													
					$qtytotal+= $qty_warna[$xx];
				} // end for
		if ($id_brg_wip!='' && ($temp_qty!='' || $temp_qty!=0)) {
			
				$data_detail = array(
					'id_sj_drop_forecast'=>$id_sj,
					'id_brg'=>$id_brg_wip,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail,
					'kode_brg'=>$kode_brg_wip
				);
				$this->db->insert('tm_sj_drop_forecast_detail',$data_detail);
				
				
				// ambil id detail sjmasukwip_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_sj_drop_forecast_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
			
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_sj_drop_forecast_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_sj_drop_forecast_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_sj_drop_forecast_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_sj_drop_forecast_detail_warna',$tm_sj_drop_forecast_detail_warna);			
			} 
  }
}    
  function delete($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				//~ $query2	= $this->db->query(" SELECT * FROM tm_sj_drop_forecast_detail WHERE id_sj_drop_forecast = '$id' ");
				//~ if ($query2->num_rows() > 0){
					//~ $hasil2=$query2->result();
										
					//~ foreach ($hasil2 as $row2) {
						//~ // cek dulu satuan brgnya. jika satuannya yard, maka qty di detail ini konversikan lagi ke yard
							//~ $query3	= $this->db->query(" SELECT satuan FROM tm_barang_wip WHERE id = '$row2->id_brg' ");
								//~ if ($query3->num_rows() > 0){
									//~ $hasilrow = $query3->row();
									//~ $satuan	= $hasilrow->satuan;
								//~ }
								//~ else {
									//~ $satuan	= '';
								//~ }
								//~ 
							//~ /*	if ($satuan == '2') {
									//~ $qty_sat_awal = $row2->qty / 0.90; //meter ke yard 07-03-2012, rubah dari 0.91 ke 0.90
								//~ }
								//~ else if ($satuan == '7') {
									//~ $qty_sat_awal = $row2->qty / 12; // pcs ke lusin
								//~ }
								//~ else */
								//~ 
								//~ // 28-10-2014
								//~ if ($row2->id_satuan_konversi == '0')
									//~ $qty_sat_awal = $row2->qty;
								//~ else
									//~ $qty_sat_awal = $row2->qty_satawal;
								//~ //$qty_sat_awal = $row2->qty;
								//~ 
						//~ 
						//~ // ============ update stok =====================
						//~ 
						//~ //$nama_tabel_stok = "tm_stok";
						//~ if ($row2->is_quilting == 't')
							//~ $nama_tabel_stok = "tm_stok_hasil_makloon";
						//~ else
							//~ $nama_tabel_stok = "tm_stok";
				//~ 
						//~ //cek stok terakhir tm_stok, dan update stoknya
								//~ $query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE id_brg = '$row2->id_brg' ");
								//~ if ($query3->num_rows() == 0){
									//~ $stok_lama = 0;
								//~ }
								//~ else {
									//~ $hasilrow = $query3->row();
									//~ $stok_lama	= $hasilrow->stok;
								//~ }
								//~ $new_stok = $stok_lama-$qty_sat_awal; // berkurang stok karena reset dari bon M masuk
								//~ 
								//~ if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									//~ $data_stok = array(
										//~ 'id_brg'=>$row2->id_brg,
										//~ 'stok'=>$new_stok,
										//~ 'tgl_update_stok'=>$tgl
										//~ );
									//~ $this->db->insert($nama_tabel_stok, $data_stok);
								//~ }
								//~ else {
									//~ $this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									//~ where id_brg= '$row2->id_brg' ");
								//~ }
								//~ 
								//28-10-2014 ini dimodif
								//~ $sqlxx = " SELECT qty, harga, qty_satawal FROM tm_sj_drop_forecast_detail_harga 
											//~ WHERE id_sj_drop_forecast_detail = '".$row2->id."' ";
								//~ $queryxx	= $this->db->query($sqlxx);
								//~ if ($queryxx->num_rows() > 0){
									//~ $hasilxx=$queryxx->result();
																				//~ 
									//~ foreach ($hasilxx as $rowxx) {
										//~ $qty_lamanya = $rowxx->qty_satawal; 
										//~ $harganya = $rowxx->harga; 
										//~ 
										//~ $sqlxx2 = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
										//~ if ($row2->is_quilting == 't')
											//~ $sqlxx2.= " id_brg_quilting = '".$row2->id_brg."' ";
										//~ else
											//~ $sqlxx2.= " id_brg = '".$row2->id_brg."' ";
										//~ $sqlxx2.= " AND quilting = '".$row2->is_quilting."' 
													//~ AND harga = '".$harganya."' ORDER BY id ASC";
													//~ 
										//~ $queryxx2	= $this->db->query($sqlxx2);
										//~ if ($queryxx2->num_rows() > 0){
											//~ $hasilxx2 = $queryxx2->row();
											//~ $id_stok_harga	= $hasilxx2->id;
											//~ $stok_lama	= $hasilxx2->stok;
											//~ 
											//~ $stokreset = $stok_lama-$qty_lamanya;
											//~ 
											//~ //if ($row2->is_quilting == 'f')
											//~ $this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
																//~ where id = '$id_stok_harga' "); //
											//~ //else
											//~ //	$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
											//~ //					where id='$id_stok_harga' ");
										//~ }
										//~ 
									//~ } // end forxx
									//~ 
									//~ $this->db->query(" DELETE FROM tm_sj_drop_forecast_detail_harga 
													//~ WHERE id_sj_drop_forecast_detail='".$row2->id."' ");
								//~ } // end ifxx
								
								// 28-10-2014 ini dikomen
								// ========== 25-01-2013 ==============
							/*	$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
								if ($row2->is_quilting == 't')
									$sqlxx.= " kode_brg_quilting = '$row2->kode_brg' ";
								else
									$sqlxx.= " kode_brg = '$row2->kode_brg' ";
								$sqlxx.= " AND quilting = '$row2->is_quilting' ORDER BY id DESC LIMIT 1";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilrow = $queryxx->row();
									$stok_lama	= $hasilrow->stok;
									$harganya	= $hasilrow->harga;
								}
								
								$stokreset = $stok_lama-$qty_sat_awal;
								if ($row2->is_quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg= '$row2->kode_brg' AND harga = '$harganya' "); //			
								else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row2->kode_brg' AND harga = '$harganya' "); //
								*/
								// ========== end 25-01-2013 ===========					
								
						// ==============================================
					//~ } // end foreach
				//~ } // end if
	
	// 2. hapus data sj_drop_forecast
    $this->db->delete('tm_sj_drop_forecast_detail', array('id_sj_drop_forecast' => $id));
    $this->db->delete('tm_sj_drop_forecast', array('id' => $id));
  }
          
  function generate_nomor(){
    // generate no Permintaan Bhn ke gudang
			$th_now	= date("Y");
			$query3	= $this->db->query(" SELECT no_pb_cutting FROM tm_pb_cutting ORDER BY no_pb_cutting DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_pb	= $hasilrow->no_pb_cutting;
			else
				$no_pb = '';
			if(strlen($no_pb)==12) {
				$nopb = substr($no_pb, 3, 9);
				$n_pb	= (substr($nopb,4,5))+1;
				$th_pb	= substr($nopb,0,4);
				if($th_now==$th_pb) {
						$jml_n_pb	= $n_pb;
						switch(strlen($jml_n_pb)) {
							case "1": $kodepb	= "0000".$jml_n_pb;
							break;
							case "2": $kodepb	= "000".$jml_n_pb;
							break;	
							case "3": $kodepb	= "00".$jml_n_pb;
							break;
							case "4": $kodepb	= "0".$jml_n_pb;
							break;
							case "5": $kodepb	= $jml_n_pb;
							break;	
						}
						$nomorpb = $th_now.$kodepb;
				}
				else {
					$nomorpb = $th_now."00001";
				}
			}
			else {
				$nomorpb	= $th_now."00001";
			}
			$nomorpb = "PB-".$nomorpb;

			return $nomorpb;  
  }
  
  function get_bahan($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = "  * FROM tm_barang_wip_wip WHERE status_aktif = 't' ORDER BY nama_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		
			$sql = "  * FROM tm_barang_wip_wip WHERE status_aktif = 't' ";
			$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) order by nama_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			
			
						
			$data_bhn[] = array(		'id_brg'=> $row1->id,
										'kode_brg'=> $row1->kode_brg,
										'nama_brg'=> $row1->nama_brg,
										'tgl_update'=> $row1->tgl_update,
										//~ 'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari){
	if ($cari == "all") {
		
		$sql = " select * FROM tm_barang_wip_wip WHERE status_aktif = 't' ORDER BY nama_brg ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		
			$sql = " select * FROM tm_barang_wip_wip WHERE status_aktif = 't' ";
			$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) order by nama_brg ";
		
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
   function get_perusahaan() {
	$query3	= $this->db->query(" SELECT id, nama, alamat, kota, bag_pembelian, bag_keuangan, kepala_bagian, bag_admstok 
						FROM tm_perusahaan ");
				
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
		$bag_pembelian = $hasilrow->bag_pembelian;
		$bag_keuangan = $hasilrow->bag_keuangan;
		$kepala_bagian = $hasilrow->kepala_bagian;
		$bag_admstok = $hasilrow->bag_admstok;
	}
	else {
		$id	= '';
		$nama = '';
		$kota = '';
		$alamat = '';
		$bag_pembelian = '';
		$bag_keuangan = '';
		$kepala_bagian = '';
		$bag_admstok = '';
	}
		
	$datasetting = array('id'=> $id,
						  'nama'=> $nama,
						  'alamat'=> $alamat,
						  'kota'=> $kota,
						  'bag_pembelian'=> $bag_pembelian,
						  'bag_keuangan'=> $bag_keuangan,
						  'kepala_bagian'=> $kepala_bagian,
						  'bag_admstok'=> $bag_admstok
					);
							
	return $datasetting;
  }
    
  function get_sj($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sj_drop_forecast where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		$detail_sj_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_drop_forecast_detail WHERE id_sj_drop_forecast = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						//~ if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT kode_brg, nama_brg 
										FROM tm_barang_wip  WHERE id = '$row2->id_brg' ");
						//~ }
						//~ else {
							//~ $query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										//~ FROM tm_brg_hasil_makloon a INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row2->id_brg' ");
						//~ }
						$hasilrow = $query3->row();
						
						//~ if ($row2->is_quilting == 'f') {
							// 29-10-2014
							//~ $satuan_konv	= $hasilrow->id_satuan_konversi;
							//~ $rumus_konv	= $hasilrow->rumus_konversi;
							//~ $angka_faktor_konversi	= $hasilrow->angka_faktor_konversi;
						//~ }
						//~ else {
							//~ // 29-10-2014
							//~ $satuan_konv	= 0;
							//~ $rumus_konv	= 0;
							//~ $angka_faktor_konversi	= 0;
						//~ }
						
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						//~ $id_satuan	= $hasilrow->id_satuan;
						//~ $satuan	= $hasilrow->nama_satuan;
						
						//~ if ($row2->is_quilting == 'f')
							//~ $tabelstok = "tm_stok";
						//~ else
							//~ $tabelstok = "tm_stok_hasil_makloon";
						//~ $query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE id_brg = '$row2->id_brg' ");
						//~ $hasilrow = $query3->row();
						//~ if ($query3->num_rows() != 0) 
							//~ $jum_stok	= $hasilrow->jstok;
						//~ else
							//~ $jum_stok = 0;
																						//~ 
						//~ // 04-11-2014 ambil field harga diluar supplier
						//~ $query3	= $this->db->query(" SELECT harga_diluar_supplier, harga 
								//~ FROM tm_sj_drop_forecast_detail_harga WHERE id_sj_drop_forecast_detail = '$row2->id' ");
						//~ if ($query3->num_rows() > 0){
							//~ $hasilrow = $query3->row();
							//~ $is_harga_diluar	= $hasilrow->harga_diluar_supplier;
							//~ if ($is_harga_diluar == 't') {
								//~ $harga_diluar = $hasilrow->harga;
							//~ }
							//~ else
								//~ $harga_diluar = 0;
						//~ }
						//~ else {
							//~ $harga_diluar = 0;
						//~ }
						
						$query4	= $this->db->query(" select a.qty,b.nama from tm_sj_drop_forecast_detail_warna a
						INNER JOIN tm_warna b ON a.id_warna=b.id 
						 WHERE id_sj_drop_forecast_detail = '$row2->id' ");
						if($query4 ->num_rows() > 0){
						$hasilrow4 = $query4->result();
						foreach($hasilrow4 as $row4){
						
						$warna = $row4->nama;
						$qtywarna = $row4->qty;
						
					$detail_sj_warna[] = array(	'warna'=>$warna,
										'qtywarna'=>$qtywarna
									);
						}	
						
					}
						
						$detail_sj[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												//~ 'satuan'=> $satuan,
												//~ 'id_satuan'=> $id_satuan,
												'qty'=> $row2->qty,
												//~ 'is_quilting'=> $row2->is_quilting,
												'keterangan'=> $row2->keterangan,
												
												//~ 'konversi_satuan'=> $row2->konversi_satuan,
												//~ 'jum_stok'=> $jum_stok,
												'detail_sj_warna'=>$detail_sj_warna
												//~ 'id_satuan_konversi'=> $row2->id_satuan_konversi,
												//~ 'qty_satawal'=> $row2->qty_satawal,
												// 29-10-2014
												//~ 'rumus_konv'=> $rumus_konv,
												//~ 'satuan_konv'=> $satuan_konv,
												//~ 'angka_faktor_konversi'=> $angka_faktor_konversi,
												// 04-11-2014
												//~ 'harga_diluar'=> $harga_diluar
											);
						$detail_sj_warna = array();	
											
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
			if ($row1->jenis_keluar=='1')
					$nama_jenis = "Pengiriman Drop Forecast";
				else if ($row1->jenis_keluar=='2')
					$nama_jenis = "Penggantian Retur";
				else if ($row1->jenis_keluar=='3')
					$nama_jenis = "Dibebankan";
				else{
					$nama_jenis = "lain - Lain";
						}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'no_manual'=> $row1->no_manual,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_ukuran_bisbisan(){
    $this->db->select("* from tm_ukuran_bisbisan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 09-04-2012
  function get_pb_cutting_for_print($tgl_request) { // to_date('$date_from','dd-mm-yyyy') 
		$query	= $this->db->query(" SELECT a.no_pb_cutting, a.tgl, a.kode_brg_jadi, a.brg_jadi_manual, a.id_motif,
					b.* FROM tm_pb_cutting a, tm_pb_cutting_detail b
					WHERE a.id = b.id_pb_cutting AND a.tgl = to_date('$tgl_request','dd-mm-yyyy')  
					ORDER BY a.id ASC, b.id ASC ");
	
		$data_pb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row2) {
				
				// 21-07-2012, nama motif brg jadi jika id_motif != 0
				if ($row2->id_motif != '0') {
					$query3	= $this->db->query(" SELECT nama_motif FROM tm_motif_brg_jadi
										WHERE id = '$row2->id_motif' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_motif	= $hasilrow->nama_motif;
					}
					else
						$nama_motif = '';
				}
				else
					$nama_motif = '';

						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang_wip a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->jenis_proses == '3') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_quilting' ");
								
							$hasilrow = $query3->row();
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$satuan_quilting	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
						// jika jenis prosesnya bisbisan
						if ($row2->jenis_proses == 4) {
							// 13 des 2011
							if ($row2->kode_brg_bisbisan != '') {
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
											FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id 
											AND a.kode_brg = '$row2->kode_brg_bisbisan' ");
									
								$hasilrow = $query3->row();
								$nama_brg_bisbisan	= $hasilrow->nama_brg;
								$satuan_bisbisan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg_bisbisan = '';
								$satuan_bisbisan = '';
							}
							
							// ambil data kebutuhan perpcs bisbisan
							$sql = " SELECT id, jum_kebutuhan FROM tm_keb_bisbisan_perpcs 
								WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_brg = '$row2->kode_brg' 
								AND id_ukuran_bisbisan = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$id_kebutuhan	= $hasilrow->id;
								$jum_kebutuhan	= $hasilrow->jum_kebutuhan;
							}
							else {
								$id_kebutuhan = 0;
								$jum_kebutuhan	= 0;
							}
							
							// ambil variabel2 rumus
							$sql = " SELECT var1, var2, var3 FROM tm_ukuran_bisbisan 
								WHERE id = '$row2->id_ukuran_bisbisan' ";
							$query3	= $this->db->query($sql);
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$var1	= $hasilrow->var1;
								$var2	= $hasilrow->var2;
								$var3	= $hasilrow->var3;
							}
							else {
								$var1 = 0;
								$var2 = 0;
								$var3 = 0;
							}
						}
						else {
							$id_kebutuhan = 0;
							$jum_kebutuhan	= 0;
							$var1 = 0;
							$var2 = 0;
							$var3 = 0;
							$nama_brg_bisbisan = '';
							$satuan_bisbisan = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
														
				$pisah1 = explode("-", $row2->tgl);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row2->kode_brg_jadi != '') {
					$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$row2->kode_brg_jadi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
					}
					else {
						if ($row2->brg_jadi_manual == '')
							$nama_brg_jadi = '';
						else
							$nama_brg_jadi = $row2->brg_jadi_manual;
					}
				}
				else
					$nama_brg_jadi = '';
												
				$data_pb[] = array(			'no_pb_cutting'=> $row2->no_pb_cutting,
											'tgl'=> $tgl,
											'kode_brg_jadi'=> $row2->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											
											'id_detail'=> $row2->id,
											'id_pb_cutting'=> $row2->id_pb_cutting,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'gelaran'=> $row2->gelaran,
												'jum_set'=> $row2->jum_set,
												'jum_gelar'=> $row2->jum_gelar,
												'panjang_kain'=> $row2->panjang_kain,
												'jenis_proses'=> $row2->jenis_proses,
												'id_marker'=> $row2->id_marker_gelaran,
												'kode_brg_quilting'=> $row2->kode_brg_quilting,
												'nama_brg_quilting'=> $nama_brg_quilting,
												'kode_brg_bisbisan'=> $row2->kode_brg_bisbisan,
												'nama_brg_bisbisan'=> $nama_brg_bisbisan,
												'ukuran_bisbisan'=> $row2->id_ukuran_bisbisan,
												//'id_kebutuhanperpcs'=> $row2->id_kebutuhan_perpcs,
												'id_kebutuhan'=> $id_kebutuhan,
												'jum_kebutuhan'=> $jum_kebutuhan,
												'var1'=> $var1,
												'var2'=> $var2,
												'var3'=> $var3,
												'bordir'=> $row2->bordir,
												'for_kekurangan_cutting'=> $row2->for_kekurangan_cutting,
												'for_bs_cutting'=> $row2->for_bs_cutting,
												'bagian_bs'=> $row2->bagian_bs,
												'id_motif'=> $row2->id_motif,
												'nama_motif'=> $nama_motif 
											);
											
			} // endforeach header
		}
		else {
			$data_pb = '';
		}
		return $data_pb;
  }
  
  function cek_data( $thn1){
    $this->db->select("id from tm_sj_drop_forecast WHERE  extract(year from tgl_sj) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 16-07-2012
  function get_motif_brg_jadi(){
    $this->db->select("* from tm_motif_brg_jadi order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 28-10-2014
  function get_satuan(){
    $query = $this->db->query(" SELECT * FROM tm_satuan ORDER BY id");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
