<?php
class Login_model extends MY_Model
{
    public $_tabel = 'tb_pengguna';

    public $form_rules = array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|xss_clean|required'
        ),
    );

    public function login($login)
    {
        $login = (object) $login;

        // Cari data pengguna di DB.
        $where = array(
            'username' => $login->username,
            'password' =>md5($login->password),
			 'status_verifikasi' => '1',
            // Hanya yang status pendaftaran masih aktif yang boleh login
            'status_pendaftaran' => '1',
        );
        $pengguna = $this->get($where);

        if ($pengguna) {
            // Buat data session jika login benar.
            $data_session = array(
                'nama' => $pengguna->nama,
                'pengguna_id' => $pengguna->id,
                'login_status' => true,
                'user_bagian' => $pengguna->bagian,
            );
            $this->session->set_userdata($data_session);

            // Return true jika login sukses.
            return true;
        }

        // Return false jika login salah.
        return false;
    }

    public function logout()
    {
        $this->session->unset_userdata(
            array(
                'nama_panggilan' => '',
                'no_pengguna' => '',
                'login_status' => false,
                'user_level' => ''
            )
        );
        $this->session->sess_destroy();
    }
}
