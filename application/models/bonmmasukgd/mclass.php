<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function get_nomorbonmmasuk() {
		$db2=$this->load->database('db_external', TRUE);
		/*
		return $db2->query(" SELECT SUBSTRING(cast(i_inbonm_code AS character varying),5,(LENGTH(cast(i_inbonm_code AS character varying))-4)) AS icode FROM tm_inbonm WHERE f_inbonm_cancel='f' ORDER BY cast(i_inbonm AS integer) DESC LIMIT 1 ");
		*/ 
		return $db2->query(" SELECT SUBSTRING(cast(i_inbonm_code AS character varying),5,(LENGTH(cast(i_inbonm_code AS character varying))-4)) AS icode FROM tm_inbonm WHERE f_inbonm_cancel='f' ORDER BY i_inbonm DESC LIMIT 1 ");
	}

	function get_thnbonmmasuk() {
		$db2=$this->load->database('db_external', TRUE);
		/*
		return $db2->query(" SELECT SUBSTRING(cast(i_inbonm_code AS character varying),1,4) AS thn FROM tm_inbonm WHERE f_inbonm_cancel='f' ORDER BY cast(i_inbonm AS integer) DESC LIMIT 1 ");
		*/ 
		return $db2->query(" SELECT SUBSTRING(cast(i_inbonm_code AS character varying),1,4) AS thn FROM tm_inbonm WHERE f_inbonm_cancel='f' ORDER BY i_inbonm DESC LIMIT 1 ");
	}
	
	function msimpan($i_inbonm,$d_inbonm,$i_product,$e_product_name,
					$qty_warna, $i_product_color, $i_color, $qty_product,$iteration,$f_stp,$iso,$i_sj) {
		// $n_count_product
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$tm_bonmmasuk_item	= array();
		$i_inbonm_item	= array();
		
		$is2	= array();
		$ins_temp_tmso	= array();
		
		//$ncountproduct	= array();

		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$seq_tm_inbonm	= $db2->query(" SELECT i_inbonm FROM tm_inbonm ORDER BY i_inbonm DESC LIMIT 1 ");
		
		if($seq_tm_inbonm->num_rows() >0 ) {
			$seqrow	= $seq_tm_inbonm->row();
			$inbonm	= $seqrow->i_inbonm+1;
		} else {
			$inbonm	= 1;
		}

		
		$s	= array();
		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
			$s[$jumlah]	= $iso[$jumlah];
			if($s[$jumlah]!="") {
				$ss	= $s[$jumlah];
				break;
			}	
		}
		
		$db2->set(
			array(
			 'i_inbonm'=>$inbonm,
			 'i_inbonm_code'=>$i_inbonm,
			 'd_inbonm'=>$d_inbonm,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry,
			 'i_sj_manual'=>$i_sj));

			 
		if($db2->insert('tm_inbonm')) {
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				
				$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				}
				
			//	$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];				

				$seq_tm_inbonm_item	= $db2->query(" SELECT i_inbonm_item FROM tm_inbonm_item ORDER BY i_inbonm_item DESC LIMIT 1 ");
				
				if($seq_tm_inbonm_item->num_rows() > 0) {
					$seqrow	= $seq_tm_inbonm_item->row();
					$i_inbonm_item[$jumlah]	= $seqrow->i_inbonm_item+1;
				}else{
					$i_inbonm_item[$jumlah]	= 1;
				}
				
				//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($i_color[$jumlah]); $xx++) {

					$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
					$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
					$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
													
					$qtytotal+= $qty_warna[$jumlah][$xx];
				} // end for
				// ---------------------------------- END -----------------------------------

				$tm_bonmmasuk_item[$jumlah]	= array(
					 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
					 'i_inbonm'=>$inbonm,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 //'n_count_product'=>$ncountproduct[$jumlah],
					 'n_count_product'=>$qtytotal,
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah]
				);
								
				if($db2->insert('tm_inbonm_item',$tm_bonmmasuk_item[$jumlah])) {
					
					// 06-10-2014 update stok di tm_stokopname_item
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
					
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;
						
						$i_so_item = $row_stok_item->i_so_item;

						$back_qty_awal[$jumlah]	= $qtyawal[$jumlah];
						//$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]+$ncountproduct[$jumlah];
						$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]+$qtytotal;
						
						if($back_qty_akhir[$jumlah]=="")
							$back_qty_akhir[$jumlah]	= 0;
							
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz,'i_product'=>$i_product[$jumlah]));
					}
					
					// -------------------
					
					// ------------------------------------------
					for ($xx=0; $xx<count($i_color[$jumlah]); $xx++) {
					//foreach ($i_product_color[$jumlah] as $row1) {
						
						//if (trim($row1) != '') {
							$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
							$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
							$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
							
							$seq_tm_inbonm_item_color	= $db2->query(" SELECT i_inbonm_item_color FROM tm_inbonm_item_color 
														ORDER BY i_inbonm_item_color DESC LIMIT 1 ");
						
							if($seq_tm_inbonm_item_color->num_rows() > 0) {
								$seqrow	= $seq_tm_inbonm_item_color->row();
								$i_inbonm_item_color[$jumlah]	= $seqrow->i_inbonm_item_color+1;
							}else{
								$i_inbonm_item_color[$jumlah]	= 1;
							}

							$tm_bonmmasuk_item_color[$jumlah]	= array(
								 'i_inbonm_item_color'=>$i_inbonm_item_color[$jumlah],
								 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
								 'i_product_color'=>$i_product_color[$jumlah][$xx],
								 'i_color'=>$i_color[$jumlah][$xx],
								 'qty'=>$qty_warna[$jumlah][$xx]
							);
							$db2->insert('tm_inbonm_item_color',$tm_bonmmasuk_item_color[$jumlah]);
							
							// 06-10-2014, update stok per warna
								$sqlwarna = " SELECT * FROM tm_stokopname_item_color WHERE i_so_item = '$i_so_item'
											AND i_color = '".$i_color[$jumlah][$xx]."' ";
								$querywarna = $db2->query($sqlwarna);
								
								if($querywarna->num_rows() > 0) {
									$hasilwarna = $querywarna->row();
									
									$qtyawalwarna	= $hasilwarna->n_quantity_awal;
									$qtyakhirwarna	= $hasilwarna->n_quantity_akhir;

									$back_qty_awalwarna	= $qtyawalwarna;
									$back_qty_akhirwarna	= $qtyakhirwarna+$qty_warna[$jumlah][$xx];
									
									if($back_qty_akhirwarna=="")
										$back_qty_akhirwarna	= 0;
																			
									$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir = '$back_qty_akhirwarna'
														WHERE i_so_item = '$i_so_item' AND i_color = '".$i_color[$jumlah][$xx]."' ");
									
								} // end if
							
						//} // end if
					} // end for
					// ------------------------------------------
					
					if ($db2->trans_status()===FALSE || $db2->trans_status()==FALSE) {
						$db2->trans_rollback();
					}else{
						$db2->trans_commit();
					}
					
					//reset kembali
					$qtytotal = 0;
				}else{
					$db2->delete('tm_inbonm',array('i_inbonm'=>$inbonm));
				}
				
			}
			
		}else{
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}

		$qbonmmasuk	= $db2->query("SELECT * FROM tm_inbonm WHERE i_inbonm='$inbonm' AND f_inbonm_cancel='f' ");
		
		if($qbonmmasuk->num_rows()==0) {
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}else{
			print "<script>alert(\"Nomor Bon M Masuk : '\"+$i_inbonm+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}

	}

	function msimpan_old($i_inbonm,$d_inbonm,$i_product,$e_product_name,$n_count_product,$qty_product,$iteration,$f_stp,$iso,$i_sj) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$tm_bonmmasuk_item	= array();
		$i_inbonm_item	= array();
		
		/* 06082011
		$qty_product_update	= array();
		*/
		
		$is2	= array();
		$ins_temp_tmso	= array();
		$ncountproduct	= array();

		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		/* 08082011
		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$isoz	= $row_stokopname->i_so;
		} else {
			$isoz	= "";
		}
		*/
		
		/*		
		$seq_tm_inbonm	= $db2->query(" SELECT cast(i_inbonm AS integer) AS i_inbonm FROM tm_inbonm ORDER BY cast(i_inbonm AS integer) DESC LIMIT 1 ");
		*/ 
		$seq_tm_inbonm	= $db2->query(" SELECT i_inbonm FROM tm_inbonm ORDER BY i_inbonm DESC LIMIT 1 ");
		
		if($seq_tm_inbonm->num_rows() >0 ) {
			$seqrow	= $seq_tm_inbonm->row();
			$inbonm	= $seqrow->i_inbonm+1;
		} else {
			$inbonm	= 1;
		}
		
		$s	= array();
		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
			$s[$jumlah]	= $iso[$jumlah];
			if($s[$jumlah]!="") {
				$ss	= $s[$jumlah];
				break;
			}	
		}
		
		/*
		$db2->set(
			array(
			 'i_inbonm'=>$inbonm,
			 'i_inbonm_code'=>$i_inbonm,
			 'd_inbonm'=>$d_inbonm,
			 'd_entry'=>$dentry,
			 'i_so'=>$ss ));
		*/
		
		$db2->set(
			array(
			 'i_inbonm'=>$inbonm,
			 'i_inbonm_code'=>$i_inbonm,
			 'd_inbonm'=>$d_inbonm,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry,
			 'i_sj_manual'=>$i_sj));
			 
		if($db2->insert('tm_inbonm')) {
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

				//$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				}
				
				$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];
				
				/*
				$seq_tm_inbonm_item	= $db2->query(" SELECT cast(i_inbonm_item AS integer) AS i_inbonm_item FROM tm_inbonm_item ORDER BY cast(i_inbonm_item AS integer) DESC LIMIT 1 ");
				*/
				$seq_tm_inbonm_item	= $db2->query(" SELECT i_inbonm_item FROM tm_inbonm_item ORDER BY i_inbonm_item DESC LIMIT 1 ");
				
				if($seq_tm_inbonm_item->num_rows() > 0) {
					$seqrow	= $seq_tm_inbonm_item->row();
					$i_inbonm_item[$jumlah]	= $seqrow->i_inbonm_item+1;
				}else{
					$i_inbonm_item[$jumlah]	= 1;
				}
				
				/* 06082011
				if($qty_product[$jumlah]=="")
					$qty_product[$jumlah]	= 0;
					
				$qty_product_update[$jumlah]	= $qty_product[$jumlah]+$ncountproduct[$jumlah];
				*/
				
				/* 20092011
				$tm_bonmmasuk_item[$jumlah]	= array(
					 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
					 'i_inbonm'=>$i_inbonm,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$ncountproduct[$jumlah],
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah]
				);
				*/

				$tm_bonmmasuk_item[$jumlah]	= array(
					 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
					 'i_inbonm'=>$inbonm,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$ncountproduct[$jumlah],
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah]
				);
								
				if($db2->insert('tm_inbonm_item',$tm_bonmmasuk_item[$jumlah])) {

					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
					
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;

						$back_qty_awal[$jumlah]	= $qtyawal[$jumlah];
						$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]+$ncountproduct[$jumlah];
						
						/*
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_awal'=>$back_qty_awal[$jumlah],
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						*/
						
						if($back_qty_akhir[$jumlah]=="")
							$back_qty_akhir[$jumlah]	= 0;
							
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz,'i_product'=>$i_product[$jumlah]));
					}
									
					$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.i_status_do='1' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
					
					if($get_tmso->num_rows() > 0 ) {
					
						$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						
						if($get_tmso2->num_rows() > 0) {
							$row_tmso2	= $get_tmso2->row();
							$is2[$jumlah]	= $row_tmso2->iso+1;
						}else{
							$is2[$jumlah]	= 1;
						}
						
						$row_tmso	= $get_tmso->row_array();
						
						/* 06082011
						$temp_iso[$jumlah]	= $row_tmso['i_so'];
						$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
						$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
						$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
						$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
						$temp_ddo[$jumlah]	= $row_tmso['d_do'];
						$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
						$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
						$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
						$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
						$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
						$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
						$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
						 
						$ins_temp_tmso[$jumlah]	= array(
							'i_so'=>$iso[$jumlah],
							'i_product'=>$temp_iproduct[$jumlah],
							'i_product_motif'=>$temp_imotif[$jumlah],
							'e_product_motifname'=>$temp_productname[$jumlah],
							'i_status_do'=>$temp_istatusdo[$jumlah],
							'd_do'=>$temp_ddo[$jumlah],
							'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
							'n_saldo_akhir'=>$saldo_akhir[$jumlah],
							'n_inbonm'=>$tambah_inbonm[$jumlah],
							'n_outbonm'=>$temp_n_outbonm[$jumlah],
							'n_bbm'=>$temp_n_bbm[$jumlah],
							'n_bbk'=>$temp_n_bbk[$jumlah],
							'd_entry'=>$dentry
						);
						*/

						$temp_iso[$jumlah]	= $row_tmso['i_so'];
						
						$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
						
						if($temp_saldo_awal[$jumlah]=="")
							$temp_saldo_awal[$jumlah]	= 0;
							
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
						if($temp_saldo_akhir[$jumlah]=="")
							$temp_saldo_akhir[$jumlah] = 0;
							
						$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
						if($temp_n_inbonm[$jumlah]=="")
							$temp_n_inbonm[$jumlah]	= 0;
							
						$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
						if($temp_n_outbonm[$jumlah]=="")
							$temp_n_outbonm[$jumlah] = 0;
						
						$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
						if($temp_n_bbm[$jumlah]=="")
							$temp_n_bbm[$jumlah] = 0;
							
						$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
						if($temp_n_bbk[$jumlah]=="")
							$temp_n_bbk[$jumlah] = 0;
							
						$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
						
						if($saldo_akhir[$jumlah]=="")
							$saldo_akhir[$jumlah]	= 0;
							
						$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
						
						if($tambah_inbonm[$jumlah]=="")
							$tambah_inbonm[$jumlah]	= 0;
							 
						$ins_temp_tmso[$jumlah]	= array(
							'i_so'=>$is2[$jumlah],
							'i_product'=>$row_tmso['i_product'],
							'i_product_motif'=>$row_tmso['i_product_motif'],
							'e_product_motifname'=>$row_tmso['e_product_motifname'],
							'i_status_do'=>$row_tmso['i_status_do'],
							'd_do'=>$row_tmso['d_do'],
							'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
							'n_saldo_akhir'=>$saldo_akhir[$jumlah],
							'n_inbonm'=>$tambah_inbonm[$jumlah],
							'n_outbonm'=>$temp_n_outbonm[$jumlah],
							'n_bbm'=>$temp_n_bbm[$jumlah],
							'n_bbk'=>$temp_n_bbk[$jumlah],
							'd_entry'=>$dentry
						);
												
						$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						
					}else{
						
						$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
						
						if($get_tmso->num_rows()>0) {
							
							$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
							
							if($get_tmso2->num_rows() > 0) {
								$row_tmso2	= $get_tmso2->row();
								$is2[$jumlah]	= $row_tmso2->iso+1;
							}else{
								$is2[$jumlah]	= 1;
							}
							
							$row_tmso	= $get_tmso->row_array();
							
							/* 06082011
							$temp_iso[$jumlah]	= $row_tmso['i_so'];
							$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
							$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
							$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
							$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
							$temp_ddo[$jumlah]	= $row_tmso['d_do'];
							$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
							$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
							$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
							$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
							$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
							$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
							$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
							$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
							 
							$ins_temp_tmso[$jumlah]	= array(
								'i_so'=>$iso[$jumlah],
								'i_product'=>$temp_iproduct[$jumlah],
								'i_product_motif'=>$temp_imotif[$jumlah],
								'e_product_motifname'=>$temp_productname[$jumlah],
								'i_status_do'=>'1',
								'd_do'=>$temp_ddo[$jumlah],
								'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
								'n_saldo_akhir'=>$saldo_akhir[$jumlah],
								'n_inbonm'=>$tambah_inbonm[$jumlah],
								'n_outbonm'=>$temp_n_outbonm[$jumlah],
								'n_bbm'=>$temp_n_bbm[$jumlah],
								'n_bbk'=>$temp_n_bbk[$jumlah],
								'd_entry'=>$dentry
							);
							*/

							$temp_iso[$jumlah]	= $row_tmso['i_so'];
							$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
							
							$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
							if($temp_saldo_awal[$jumlah]=="")
								$temp_saldo_awal[$jumlah] = 0;
								
							$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
							if($temp_saldo_akhir[$jumlah]=="")
								$temp_saldo_akhir[$jumlah] = 0;
								
							$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
							
							$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
							if($temp_n_outbonm[$jumlah]=="")
								$temp_n_outbonm[$jumlah] = 0;
							
							$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
							if($temp_n_bbm[$jumlah]=="")
								$temp_n_bbm[$jumlah] = 0;
							
							$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
							if($temp_n_bbk[$jumlah]=="")
								$temp_n_bbk[$jumlah] = 0;
							
							$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
							if($saldo_akhir[$jumlah]=="")
								$saldo_akhir[$jumlah] = 0;
							
							$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
							if($tambah_inbonm[$jumlah]=="")
								$tambah_inbonm[$jumlah] = 0;
							 
							$ins_temp_tmso[$jumlah]	= array(
								'i_so'=>$is2[$jumlah],
								'i_product'=>$row_tmso['i_product'],
								'i_product_motif'=>$row_tmso['i_product_motif'],
								'e_product_motifname'=>$row_tmso['e_product_motifname'],
								'i_status_do'=>'1',
								'd_do'=>$row_tmso['d_do'],
								'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
								'n_saldo_akhir'=>$saldo_akhir[$jumlah],
								'n_inbonm'=>$tambah_inbonm[$jumlah],
								'n_outbonm'=>$temp_n_outbonm[$jumlah],
								'n_bbm'=>$temp_n_bbm[$jumlah],
								'n_bbk'=>$temp_n_bbk[$jumlah],
								'd_entry'=>$dentry
							);
														
							$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
							
						}else{
							/* Hapus data pd tabel tm_inbonm dgn ID :
							i_inbonm = $inbonm,
							
							Hapus data pd tabel tm_inbonm_item dgn :
							i_inbonm_item = $i_inbonm_item[$jumlah],
							i_inbonm = $i_inbonm,
							*/
							
							$db2->delete('tm_inbonm',array('i_inbonm'=>$inbonm));
							
							/* 20092011
							$db2->delete('tm_inbonm_item',array('i_inbonm'=>$i_inbonm));
							*/ 
							$db2->delete('tm_inbonm_item',array('i_inbonm'=>$inbonm));
							
							/***
							print "<script>alert(\"Maaf, Bon M Masuk gagal disimpan, kesalahan pd saat input Master Motif Barang. 1\");window.open(\"index\", \"_self\");</script>";
							***/
						}
					}
					
					/* Disabled 06212010
					$arrmotifupdate	= array(
						'n_quantity'=>$qty_product_update[$jumlah]
						);
					$db2->update('tr_product_motif', $arrmotifupdate, array('i_product_motif' => $i_product[$jumlah]));
					*/
					
					if ($db2->trans_status()===FALSE) {
						$db2->trans_rollback();
					} else {
						$db2->trans_commit();
					}
					
					/***
					print "<script>alert(\"Nomor Bon M Masuk : '\"+$i_inbonm+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
					***/
				}else{
					/* Hapus data pd tabel tm_inbonm dgn ID :
					i_inbonm = $inbonm,
					*/
					
					$db2->delete('tm_inbonm',array('i_inbonm'=>$inbonm));
					
					/***
					print "<script>alert(\"Maaf, Bon M Masuk gagal disimpan, kesalahan pd saat input Master Motif Barang. 2\");window.open(\"index\", \"_self\");</script>";
					***/
				}
				
			}
			
		}else{
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}

		$qbonmmasuk	= $db2->query("SELECT * FROM tm_inbonm WHERE i_inbonm='$inbonm' AND f_inbonm_cancel='f' ");
		
		if($qbonmmasuk->num_rows()<0 || $qbonmmasuk->num_rows()==0) {
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}else{
			print "<script>alert(\"Nomor Bon M Masuk : '\"+$i_inbonm+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}

		//redirect('bonmmasuk/cform/');
	}
	
	
	function lbarangjadiperpages($limit,$offset,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		/* Disabled 13122010
		$query = $db2->query(" 
			SELECT  a.i_product_base AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				b.e_product_motifname AS motifname,
				b.n_quantity AS qty
				
			FROM tr_product_base a 
			
			RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product;	
		");
		*/
		
		/* Disabled 14122010
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND d.d_so = '$tgl%' ORDER BY b.i_product_motif DESC			
		*/
		
		/* Disabled 07-02-2011
		$query = $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false 
				
				ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset );
		*/
		
		/* 09-08-2011		
		$query = $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset );
		*/		
		$query = $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset );
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi($bulan,$tahun) {	
		$db2=$this->load->database('db_external', TRUE);
		/* Disabled 07-02-2011
		return $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false
				
				ORDER BY b.i_product_motif DESC " );
		*/
		
		/* 09-08-2011		
		return $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
								
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC " );
				*/ 
		return $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
								
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan'
				
				ORDER BY b.i_product_motif ASC " );				
	}

	function flbarangjadi($key,$bulan,$tahun) {		
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		/* Disabled 07-02-2011
		$qrystring	= " 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') 
				ORDER BY b.i_product_motif DESC ";
		*/
		/* 09-08-2011
		$qrystring	= " 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') 
				ORDER BY b.i_product_motif ASC ";
		*/
		$qrystring	= " 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') 
				ORDER BY b.i_product_motif ASC ";
						
		return $db2->query($qrystring);
	}

	function lbarangjadiperpagesopsi($limit,$offset,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);

		$query = $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset );
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadiopsi($bulan,$tahun) {	
		$db2=$this->load->database('db_external', TRUE);
	
		return $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
								
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC " );
			
	}

	function flbarangjadiopsi($key,$bulan,$tahun) {		
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);

		$qrystring	= " 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') 
				ORDER BY b.i_product_motif ASC ";
						
		return $db2->query($qrystring);
	}
	
	function caribonmmasuk($nbonmmasuk) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_inbonm WHERE i_inbonm_code=trim('$nbonmmasuk') AND f_inbonm_cancel='f' ");
	}
}
