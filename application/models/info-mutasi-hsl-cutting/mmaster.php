<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_mutasi_stok($num, $offset, $date_from, $date_to, $kode_brg, $kode_brg_jadi) {
		$this->db->select(" * FROM tt_stok_hasil_cutting
					WHERE tgl_input >= to_date('$date_from','dd-mm-yyyy') 
					AND tgl_input <= to_date('$date_to','dd-mm-yyyy')
					AND kode_brg = '$kode_brg' AND kode_brg_jadi = '$kode_brg_jadi'
					ORDER BY tgl_input ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
		
		$data_mutasi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row1) {
								
				$data_mutasi[] = array(		'id'=> $row1->id,	
											'kode_brg'=> $row1->kode_brg,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'no_bukti'=> $row1->no_bukti,
											'masuk'=> $row1->masuk,
											'masuk_lain'=> $row1->masuk_lain,
											'keluar'=> $row1->keluar,
											'keluar_lain'=> $row1->keluar_lain,
											'tgl_input'=> $row1->tgl_input,
											'saldo'=> $row1->saldo
											);
			} // endforeach header
		}
		else {
			$data_mutasi = '';
		}
		return $data_mutasi;
  }
  
  function get_mutasi_stoktanpalimit($date_from, $date_to, $kode_brg, $kode_brg_jadi){
	$query	= $this->db->query(" SELECT * FROM tt_stok_hasil_cutting
					WHERE tgl_input >= to_date('$date_from','dd-mm-yyyy') 
					AND tgl_input <= to_date('$date_to','dd-mm-yyyy')
					AND kode_brg = '$kode_brg' AND kode_brg_jadi = '$kode_brg_jadi' ");
    
    return $query->result();  
  }
  
  function get_bahan($num, $offset, $cari, $kode_brg_jadi)
  {
	if ($cari == "all") {
		$sql = " kode_brg, kode_brg_jadi, stok, tgl_update_stok 
				 FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' 
				 order by kode_brg_jadi, kode_brg";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " a.kode_brg, a.kode_brg_jadi, a.stok, a.tgl_update_stok FROM tm_stok_hasil_cutting a
				LEFT JOIN tm_barang b ON b.kode_brg=a.kode_brg
				LEFT JOIN tm_brg_hasil_makloon c ON c.kode_brg=a.kode_brg
				WHERE a.kode_brg_jadi = '$kode_brg_jadi' AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')
				OR UPPER(c.nama_brg) like UPPER('%$cari%') OR UPPER(c.kode_brg) like UPPER('%$cari%') )
				ORDER BY a.kode_brg_jadi, a.kode_brg";
				
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					//$satuan	= '';
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg = '';
						$satuan = '';
					}
				}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $nama_brg,
										//'satuan'=> $satuan,
										'jum_stok'=> $row1->stok,
										'tgl_update_stok'=> $row1->tgl_update_stok,
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kode_brg_jadi){
	if ($cari == "all") {
		$sql = " SELECT kode_brg, kode_brg_jadi, stok, tgl_update_stok 
				 FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' ";
		$query	= $this->db->query($sql);
		return $query->result();  

	}
	else {
		$sql = " SELECT a.kode_brg, a.kode_brg_jadi, a.stok, a.tgl_update_stok FROM tm_stok_hasil_cutting a
				LEFT JOIN tm_barang b ON b.kode_brg=a.kode_brg
				LEFT JOIN tm_brg_hasil_makloon c ON c.kode_brg=a.kode_brg
				WHERE a.kode_brg_jadi = '$kode_brg_jadi' AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')
				OR UPPER(c.nama_brg) like UPPER('%$cari%') OR UPPER(c.kode_brg) like UPPER('%$cari%') ) ";
		
		$query	= $this->db->query($sql);
		return $query->result();  

	}
  }
  
  function get_brg_jadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif  ";
		
		$sql.=" order by i_product_motif";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif WHERE (UPPER(i_product_motif) like UPPER('%$cari%') 
				OR UPPER(e_product_motifname) like UPPER('%$cari%')) order by e_product_motifname";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg_jadi'=> $row1->i_product_motif,	
										'nama_brg_jadi'=> $row1->e_product_motifname,
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brg_jaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " SELECT * FROM tr_product_motif  ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select * FROM tr_product_motif WHERE (UPPER(i_product_motif) like UPPER('%$cari%') 
				OR UPPER(e_product_motifname) like UPPER('%$cari%')) ";
		
		$query	= $this->db->query($sql);
		return $query->result();  

	}
  }

}
