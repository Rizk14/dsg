<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
  if($this->session->userdata('gid') == 16){
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
	    }else{
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
					                        }
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so_bahanbaku($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, tgl_so, status_approve FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
		
	/*	$sql = " SELECT id, status_approve FROM tt_stok_opname_bahan_baku
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' "; die($sql); */
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
		}
		
		$so_bahan_baku = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so
							);
							
		return $so_bahan_baku;
  }
  
  // 01-07-2015
  function cek_sokosong_bahanbaku($gudang) {
	$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku WHERE id_gudang = '$gudang' ");
	if ($query3->num_rows() > 0){
		return 'f';
	}
	else {
		return 't';
	}
  }

  function get_all_stok_bahanbaku($bulan, $tahun, $gudang) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
	  // 30-10-2014, perlu dimodif supaya ambil dari tabel stok dan stok harga
	  // dan qty-nya dlm satuan konversi (jika ada)
		$sql = " SELECT a.id, d.id_brg, b.kode_brg, d.jum_stok_opname as stok, b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi, 
				b.nama_brg, c.nama as nama_satuan FROM tt_stok_opname_bahan_baku a 
				INNER JOIN tt_stok_opname_bahan_baku_detail d ON a.id = d.id_stok_opname_bahan_baku
				INNER JOIN tm_barang b ON d.id_brg = b.id
				INNER JOIN tm_satuan c ON c.id = b.satuan
				WHERE b.id_gudang = '$gudang' AND b.status_aktif='t' 
				AND a.bulan='$bln_query' AND a.tahun='$thn_query'
				ORDER BY b.nama_brg,b.kode_brg "; //echo $sql; die();
		$query	= $this->db->query($sql);
	  
		/*$query	= $this->db->query(" SELECT b.kode_brg, b.nama_brg, c.id as id_satuan, c.nama as satuan 
					FROM tm_barang b, tm_satuan c 
					WHERE b.satuan = c.id AND b.id_gudang = '$gudang' AND b.status_aktif = 't'
					ORDER BY b.nama_brg "); */
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();
			$detail_harga = array();
			foreach ($hasil as $row) {
				// 01-11-2014
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				// 13-05-2015
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;
					
					$stokinduk = round($stokinduk, 4);
				} 
				else {
					$stokinduk = $row->stok;
				}
				
				$auto_saldo_akhir = 0;
				
				$detail_bahan[] = array('id_header'=>0,
										'id'=> 0,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										//'detail_harga'=> $detail_harga,
										'auto_saldo_akhir'=> $auto_saldo_akhir,
										//13-05-2015
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $stokinduk,
										'bintang'=>0
									);
				$detail_harga = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		
		// 12-08-2015 DATA BRG BARU YG BLM ADA STOKNYA
		$query	= $this->db->query(" SELECT b.id as id_brg, b.kode_brg, b.nama_brg, c.nama as nama_satuan, 
							b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi
							FROM tm_barang b 
							LEFT JOIN tm_satuan c ON c.id = b.satuan
							WHERE b.id_gudang = '$gudang' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select id_brg FROM tm_stok)
							ORDER BY b.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			//$detail_bahan = array();			
			foreach ($hasil as $row) {
				/*if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;
					
					$stokinduk = round($stokinduk, 4);
				} 
				else {
					$stokinduk = $row->stok;
					
					if ($stokinduk == '')
						$stokinduk = 0;
				} */
								
				$detail_bahan[] = array(
										'id_header'=>0,
										'id'=> 0,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0,
										'auto_saldo_akhir'=>0,
										'bintang'=>1
									);
			}
		}
		
		return $detail_bahan;
  }
   function get_all_stok_bahanbaku_old($bulan, $tahun, $gudang) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
	  // 30-10-2014, perlu dimodif supaya ambil dari tabel stok dan stok harga
	  // dan qty-nya dlm satuan konversi (jika ada)
		$sql = " SELECT a.id, a.id_brg, b.kode_brg, a.stok, b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi, 
				b.nama_brg, c.nama as nama_satuan FROM tm_stok a 
				INNER JOIN tm_barang b ON a.id_brg = b.id
				INNER JOIN tm_satuan c ON c.id = b.satuan
				WHERE b.id_gudang = '$gudang' AND b.status_aktif='t' ORDER BY b.nama_brg "; //echo $sql; die();
		$query	= $this->db->query($sql);
	  
		/*$query	= $this->db->query(" SELECT b.kode_brg, b.nama_brg, c.id as id_satuan, c.nama as satuan 
					FROM tm_barang b, tm_satuan c 
					WHERE b.satuan = c.id AND b.id_gudang = '$gudang' AND b.status_aktif = 't'
					ORDER BY b.nama_brg "); */
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();
			$detail_harga = array();
			foreach ($hasil as $row) {
				// 01-11-2014
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				// 13-05-2015
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;
					
					$stokinduk = round($stokinduk, 4);
				} 
				else {
					$stokinduk = $row->stok;
				}
				
				$auto_saldo_akhir = 0;
				
				$detail_bahan[] = array('id_header'=>0,
										'id'=> 0,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										//'detail_harga'=> $detail_harga,
										'auto_saldo_akhir'=> $auto_saldo_akhir,
										//13-05-2015
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $stokinduk,
										'bintang'=>0
									);
				$detail_harga = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		
		// 12-08-2015 DATA BRG BARU YG BLM ADA STOKNYA
		$query	= $this->db->query(" SELECT b.id as id_brg, b.kode_brg, b.nama_brg, c.nama as nama_satuan, 
							b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi
							FROM tm_barang b 
							LEFT JOIN tm_satuan c ON c.id = b.satuan
							WHERE b.id_gudang = '$gudang' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select id_brg FROM tm_stok)
							ORDER BY b.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			//$detail_bahan = array();			
			foreach ($hasil as $row) {
				/*if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;
					
					$stokinduk = round($stokinduk, 4);
				} 
				else {
					$stokinduk = $row->stok;
					
					if ($stokinduk == '')
						$stokinduk = 0;
				} */
								
				$detail_bahan[] = array(
										'id_header'=>0,
										'id'=> 0,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0,
										'auto_saldo_akhir'=>0,
										'bintang'=>1
									);
			}
		}
		
		return $detail_bahan;
  }
  function get_all_stok_opname_bahanbaku($bulan, $tahun, $gudang) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT distinct a.id as id_header, b.*, c.kode_brg, c.nama_brg, c.satuan, 
					c.id_satuan_konversi, c.rumus_konversi, c.angka_faktor_konversi
					FROM tt_stok_opname_bahan_baku_detail b 
					INNER JOIN tt_stok_opname_bahan_baku a ON b.id_stok_opname_bahan_baku = a.id 
					INNER JOIN tm_barang c ON b.id_brg = c.id
					WHERE a.id_gudang = '$gudang' AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY c.kode_brg  ");
		// 26-08-2015 ganti jadi order by nama brg. ORDER BY b.id
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// 01-11-2014
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				// 13-05-2015 HITUNG SALDO AKHIR UTK SEMENTARA GA DIPAKE DULU
				// 22-06-2015 DIKOMEN AJAA
				
				// ---------- 12-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a, 
									tt_stok_opname_bahan_baku_detail b
									WHERE a.id = b.id_stok_opname_bahan_baku
									AND b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$saldo_awal = $hasilx->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE b.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND b.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND b.status_stok = 't' AND e.id_gudang = '$gudang' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_masuk = $hasilx->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$jum_masuk = $jum_masuk * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$jum_masuk = $jum_masuk / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$jum_masuk = $jum_masuk + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$jum_masuk = $jum_masuk - $row->angka_faktor_konversi;
				} 
				$jum_masuk = round($jum_masuk, 2);
				
				// 3. masuk lain2
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$gudang' AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;
				
				// 3. hitung keluar bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty_satawal) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$gudang' AND b.is_quilting = 'f' ");
				
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_keluar = $hasilx->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
					
					// 27-02-2015
					if ($row->id_satuan_konversi != 0) {
						if ($row->rumus_konversi == '1') {
							$jum_keluar = $jum_keluar*$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '2') {
							$jum_keluar = $jum_keluar/$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '3') {
							$jum_keluar = $jum_keluar+$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '4') {
							$jum_keluar = $jum_keluar-$row->angka_faktor_konversi;
						}
					}
				}
				else
					$jum_keluar = 0;
								
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk+$jum_masuk_lain-$jum_keluar; */
				$auto_saldo_akhir = 0;
				//-------------------------------------------------------------------------------------
				
				// 31-10-2014
			/*	$sqlxx = " SELECT * FROM tt_stok_opname_bahan_baku_detail_harga
							WHERE id_stok_opname_bahan_baku_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_harga
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga
							WHERE kode_brg = '$row->kode_brg' AND
							harga = '$rowxx->harga' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ---------- 12-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a 
											INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											WHERE b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.harga = '".$rowxx->harga."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_harga = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_harga = 0;
						
						// 2. jum masuk bln ini
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
									INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND b.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND b.status_stok = 't' AND e.id_gudang = '$gudang'
									AND b.harga = '$rowxx->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_harga = $hasilrow->jum_masuk;
							if ($jum_masuk_harga == '')
								$jum_masuk_harga = 0;
						}
						else
							$jum_masuk_harga = 0;
						
						if ($row->id_satuan_konversi != '0') {
							if ($row->rumus_konversi == '1')
								$jum_masuk_harga = $jum_masuk_harga * $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '2')
								$jum_masuk_harga = $jum_masuk_harga / $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '3')
								$jum_masuk_harga = $jum_masuk_harga + $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '4')
								$jum_masuk_harga = $jum_masuk_harga - $row->angka_faktor_konversi;
						} 
						$jum_masuk_harga = round($jum_masuk_harga, 2);
						
						// 3. jum masuk lain2 bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
									INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
									INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$gudang' AND c.harga = '$rowxx->harga' AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_lain_harga = $hasilrow->jum_masuk_lain;
							if ($jum_masuk_lain_harga == '')
								$jum_masuk_lain_harga = 0;
						}
						else
							$jum_masuk_lain_harga = 0;
						
						// 4. jum keluar bln ini
						// ambilnya bukan qty, tapi qty_satawal
						$query3	= $this->db->query(" SELECT sum(c.qty_satawal) as jum_keluar FROM tm_bonmkeluar a
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$gudang' AND c.harga = '$rowxx->harga'
									AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_harga = $hasilrow->jum_keluar;
							if ($jum_keluar_harga == '')
								$jum_keluar_harga = 0;
							
							// 27-02-2015
							if ($row->id_satuan_konversi != 0) {
								if ($row->rumus_konversi == '1') {
									$jum_keluar_harga = $jum_keluar_harga*$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '2') {
									$jum_keluar_harga = $jum_keluar_harga/$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '3') {
									$jum_keluar_harga = $jum_keluar_harga+$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '4') {
									$jum_keluar_harga = $jum_keluar_harga-$row->angka_faktor_konversi;
								}
							}
							else
								$jum_keluar_harga = 0;
						}
						// 5. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_harga = $saldo_awal_harga+$jum_masuk_harga-$jum_keluar_harga;
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_harga[] = array(
									'harga'=> $rowxx->harga,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'auto_saldo_akhir_harga'=> $auto_saldo_akhir_harga
								);
					}
				}
				else
					$detail_harga = '';
				// --------------------------------
				*/
				
			/*	
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				$detail_bahan[] = array('kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'id_satuan'=> $row->satuan,
										'satuan'=> $nama_satuan,
										'stok'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									); */
				
				// 13-05-2015
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg= '$row->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				}
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $stok;
					
					$stokinduk = round($stokinduk, 2);
				} 
				else {
					$stokinduk = $stok;
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										// 13-05-2015
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $row->jum_stok_opname,
										//'detail_harga'=> $detail_harga,
										'auto_saldo_akhir'=> $auto_saldo_akhir,
										'bintang'=> 0
									);
				//$detail_harga = array();
			}
		}
		else {
			$detail_bahan = '';
		}
			
			// 19 nov 2011, tambahkan pengecekan jika ada barang baru yg blm masuk ke tabel tt_stok_opname_bahan_baku_detail
		/*	$sql = "SELECT kode_brg, nama_brg, satuan FROM tm_barang where id_gudang = '$gudang' AND status_aktif = 't' AND kode_brg NOT IN 
		  			(select b.kode_brg FROM tt_stok_opname_bahan_baku a, 
		  			tt_stok_opname_bahan_baku_detail b where a.id = b.id_stok_opname_bahan_baku AND a.id_gudang = '$gudang' 
		  			AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.status_approve = 'f' AND b.status_approve = 'f')";
					
			$query2	= $this->db->query($sql);
			
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->result();
				foreach ($hasil2 as $row2) {
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg= '$row2->kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$stok	= $hasilrow->stok;
					}
					else {
						$stok = 0;
					}
					
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row2->satuan' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan	= $hasilrow->nama;
					}
					else {
						$nama_satuan = "";
					}
					
					$detail_bahan[] = array('kode_brg'=> $row2->kode_brg,
											'nama_brg'=> $row2->nama_brg,
											'id_satuan'=> $row2->satuan,
											'satuan'=> $nama_satuan,
											'stok'=> $stok,
											'stok_opname'=> '0'
										);
				}
			}
		}
		else {
			$detail_bahan = '';
		} */
		
		// 12-08-2015 DATA BRG BARU. GABUNG DISINI AJA
		$query	= $this->db->query(" SELECT b.id as id_brg, b.kode_brg, a.stok, b.nama_brg, c.nama as nama_satuan, 
							b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi
							FROM tm_barang b 
							LEFT JOIN tm_stok a ON a.id_brg = b.id
							LEFT JOIN tm_satuan c ON c.id = b.satuan
							WHERE b.id_gudang = '$gudang' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select b.id_brg FROM tt_stok_opname_bahan_baku a
							INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
							WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang='$gudang')
							ORDER BY b.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			//$detail_bahan = array();			
			foreach ($hasil as $row) {
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;
					
					$stokinduk = round($stokinduk, 4);
				} 
				else {
					$stokinduk = $row->stok;
					
					if ($stokinduk == '')
						$stokinduk = 0;
				}
				
				/*$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										// 13-05-2015
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $row->jum_stok_opname,
										//'detail_harga'=> $detail_harga,
										'auto_saldo_akhir'=> $auto_saldo_akhir */
				
				$detail_bahan[] = array(
										'id_header'=>0,
										'id'=> 0,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $stokinduk,
										'auto_saldo_akhir'=>0,
										'bintang'=>1
									);
			}
		}
		
		return $detail_bahan;
  }
  
  //function save($is_new, $id_stok, $kode_brg, $stok, $stok_fisik, $harga, $auto_saldo_akhir, $auto_saldo_akhir_harga){ 
  // 13-05-2015 diganti supaya ga usah per harga
  function save($is_new, $id_stok, $id_brg, $brgbaru, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  //-------------- hitung total qty dari detail tiap2 warna -------------------
	  // 13-05-2015 dikomen
	/*	$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($harga); $xx++) {
			$harga[$xx] = trim($harga[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			
			// 14-03-2015
			$auto_saldo_akhir_harga[$xx] = trim($auto_saldo_akhir_harga[$xx]);
			
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
	  */
	  
	  if ($is_new == '1') {
		   $data_detail = array(
						'id_stok_opname_bahan_baku'=>$id_stok,
						'id_brg'=>$id_brg, 
						//'stok_awal'=>$qtytotalstokawal,
						'stok_awal'=>$stok,
						//'jum_stok_opname'=>$qtytotalstokfisik,
						'jum_stok_opname'=>$stok_fisik
						// 14-03-2015, 13-05-2015 dikomen
						//'auto_saldo_akhir'=>$auto_saldo_akhir
					);
		   $this->db->insert('tt_stok_opname_bahan_baku_detail',$data_detail);
		   
		   // 31-10-2014, 13-05-2015 dikomen. tt_stok_opname_bahan_baku_detail_harga udah ga dipake lagi
		   
		   /*$sqlxx	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail ORDER BY id DESC LIMIT 1 ");
			if($sqlxx->num_rows() > 0) {
				$hasilxx	= $sqlxx->row();
				$iddetail	= $hasilxx->id;
			}else{
				$iddetail	= 0;
			}
		   // ----------------------------------------------
			for ($xx=0; $xx<count($harga); $xx++) {
				$harga[$xx] = trim($harga[$xx]);
				$stok[$xx] = trim($stok[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);
				$auto_saldo_akhir_harga[$xx] = trim($auto_saldo_akhir_harga[$xx]);
				
				$totalxx = $stok_fisik[$xx];
				
				$tt_stok_opname_bahan_baku_detail_harga	= array(
						 'id_stok_opname_bahan_baku_detail'=>$iddetail,
						 'harga'=>$harga[$xx],
						 'jum_stok_opname'=>$totalxx,
						 'auto_saldo_akhir'=>$auto_saldo_akhir_harga[$xx]
					);
					$this->db->insert('tt_stok_opname_bahan_baku_detail_harga',$tt_stok_opname_bahan_baku_detail_harga);
			} // end for
			*/
	  }
	  else {
		  // 31-10-2014
		  // ambil id detail id_stok_opname_bahan_baku_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail 
						where id_brg = '$id_brg' 
						AND id_stok_opname_bahan_baku = '$id_stok' ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
		  
		  // 13-05-2015 diganti
		  //$totalxx = $qtytotalstokfisik;
		  $totalxx = $stok_fisik;
		  
		  if ($brgbaru == '0')
			  $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET jum_stok_opname = '$totalxx',
							auto_saldo_akhir='0' where id = '$iddetail' ");
		  else {
			  $databaru = array(
						'id_stok_opname_bahan_baku'=>$id_stok,
						'id_brg'=>$id_brg, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik,
						'status_approve'=> 'f'
					);
		   $this->db->insert('tt_stok_opname_bahan_baku_detail',$databaru);
		  }
			
			// 13-05-2015 dikomen
			/*for ($xx=0; $xx<count($harga); $xx++) {
				$totalxx = $stok_fisik[$xx];
				
				$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail_harga SET jum_stok_opname = '".$totalxx."',
							auto_saldo_akhir='".$auto_saldo_akhir_harga[$xx]."'
							WHERE id_stok_opname_bahan_baku_detail='$iddetail' AND harga = '".$harga[$xx]."' ");
			} */
			// ====================
		  
		  
		  // 19 nov 2011, cek di tt_stok_opname_bahan_baku_detail. apakah kode brgnya ada yg blm ada
		/* $query3	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail WHERE kode_brg = '$kode_brg' ");
		 if ($query3->num_rows() == 0){
			$data_detail = array(
						'id_stok_opname_bahan_baku'=>$id_stok,
						'kode_brg'=>$kode_brg, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_bahan_baku_detail',$data_detail);
		 }
		 else {
		  $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET jum_stok_opname = '$stok_fisik', stok_awal = '$stok'
						where kode_brg= '$kode_brg' AND id_stok_opname_bahan_baku = '$id_stok' ");
		 } */
	  }
  }
  
  // 13-06-2015
  function get_detail_stokbrgbaru($bulan, $tahun, $gudang) {	  
		$query	= $this->db->query(" SELECT distinct b.id as id_brg, b.kode_brg, a.stok, b.nama_brg, c.nama as nama_satuan, 
							b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi
							FROM tm_barang b 
							LEFT JOIN tm_stok a ON a.id_brg = b.id
							LEFT JOIN tm_satuan c ON c.id = b.satuan
							WHERE b.id_gudang = '$gudang' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select b.id_brg FROM tt_stok_opname_bahan_baku a
							INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
							WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang='$gudang')
							ORDER BY b.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;
					
					$stokinduk = round($stokinduk, 4);
				} 
				else {
					$stokinduk = $row->stok;
					
					if ($stokinduk == '')
						$stokinduk = 0;
				}
				
				$detail_bahan[] = array(
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $stokinduk
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  } 
  
  // 01-07-2015
  function get_bahan($num, $offset, $cari, $id_gudang)
  {
	if ($cari == "all") {
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id 
					WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
				WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$tabelstok = "tm_stok";
				
			// 29-10-2014
			$id_satuan_konversi = $row1->id_satuan_konversi;
			$rumus_konversi = $row1->rumus_konversi;
			$angka_faktor_konversi = $row1->angka_faktor_konversi;

			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE id_brg = '$row1->id' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(	'id_brg'=> $row1->id,	
									'kode_brg'=> $row1->kode_brg,	
									'nama_brg'=> $row1->nama_brg,
									'satuan'=> $satuan,
									// 29-10-2014
									'id_satuan_konversi'=> $id_satuan_konversi,
									'rumus_konversi'=> $rumus_konversi,
									'angka_faktor_konversi'=> $angka_faktor_konversi,
									//------------
									'tgl_update'=> $row1->tgl_update,
									'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $id_gudang){
	if ($cari == "all") {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
					WHERE a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
							WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
				
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  // 27-07-2015. 28-07-2015 ga dipake
  /*function save2($id_stok, $id_brg, $brgbaru, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  $data_detail = array(
						'id_stok_opname_bahan_baku'=>$id_stok,
						'id_brg'=>$id_brg, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_bahan_baku_detail',$data_detail);
  } */
  
  // 12-08-2015
  /*function savebrgbaru($id_stok, $id_brg, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  $databaru = array(
						'id_stok_opname_bahan_baku'=>$id_stok,
						'id_brg'=>$id_brg, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik,
						'status_approve'=> 'f'
					);
	   $this->db->insert('tt_stok_opname_bahan_baku_detail',$databaru);
  } hapus aja nanti*/

}

