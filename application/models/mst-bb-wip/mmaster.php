<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  function get_unit_jahit(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit ");    
	// kategori = '1'
    return $query->result();  
  }
  
    function get_kel_brg(){
    $this->db->select("* from tm_kel_brg_wip order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
    function get_jenis_barangtanpalimit($kel_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query("SELECT * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  function get_jenis_barang($num, $offset, $kel_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_brg_wip WHERE id_kel_brg_wip = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  /*
 function get_barang_old($kel_brg, $id_jenis, $unit_jahit) {
	$sql = " SELECT a.* FROM tm_barang_wip a 
INNER JOIN tm_jenis_brg_wip c ON a.id_jenis_brg_wip = c.id
INNER JOIN tm_kel_brg_wip b ON c.id_kel_brg_wip = b.id
				WHERE b.id = '$kel_brg' AND a.status_aktif = 't' "; 
	if ($id_jenis != '')








		$sql.= " AND c.id = '$id_jenis' "; 
			
	$sql.= " ORDER BY a.kode_brg ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT harga FROM tm_perb_harga_hasil_jahit WHERE id_brg_wip = '$row1->id'
									AND id_unit_jahit = '$unit_jahit' ");
			if ($query3->num_rows() == 0){
				$harga	= '';
			}
			else {
				$hasilrow = $query3->row();
				$harga	= $hasilrow->harga;
			}
				
			$data_harga[] = array(			
								'id_brg'=> $row1->id,
								'kode_brg_jadi'=> $row1->kode_brg,
								'nama_brg_jadi'=> $row1->nama_brg,
								'harga'=> $harga
							);

		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }  
  */
  function get_barang($kel_brg, $id_jenis, $unit_jahit) {
	$sql = " select a.kode_brg,a.nama_brg, a.id,d.harga from tm_barang_wip a 
INNER JOIN tm_jenis_brg_wip b ON a.id_jenis_brg_wip = b.id
 INNER JOIN tm_kel_brg_wip c ON a.id_kel_brg_wip = c.id 
INNER JOIN tm_harga_brg_unit_jahit d ON a.id=d.id_brg
				WHERE c.id = '$kel_brg' AND a.status_aktif = 't' AND d.id_unit_jahit='$unit_jahit'"; 
	if ($id_jenis != '')
		$sql.= " AND b.id = '$id_jenis' "; 
			
	$sql.= " ORDER BY a.kode_brg ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			/*
			$query3	= $this->db->query(" SELECT harga FROM  tm_harga_brg_unit_jahit WHERE id_brg = '$row1->id'
									AND id_unit_jahit = '$unit_jahit' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$harga_brg	= $hasilrow->harga;
			}
			else {
				$harga_brg	= '';
			}
		*/		
			$data_harga[] = array(			
								'id_brg'=> $row1->id,
								'kode_brg_jadi'=> $row1->kode_brg,
								'nama_brg_jadi'=> $row1->nama_brg,
								'harga'=> $row1->harga
							);

		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }  
   function save($id_unit_jahit, $id_brg_jadi, $harga,$kode_unit_jahit,$kode_brg_jadi){  
    $tgl = date("Y-m-d");
    if ($harga == '')
		$harga = 0;
		
	if ($harga != 0) {
		$data = array(
		'id_unit_jahit'=>$id_unit_jahit,
		  'id_brg'=>$id_brg_jadi,
		  'harga'=>$harga,
		  'kode_unit_jahit'=>$kode_unit_jahit,
		   'kode_brg'=>$kode_brg_jadi,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_harga_brg_unit_jahit',$data); 
		
	
	}
		
  }
   function getAlltanpalimit($cari, $unit_jahit){
	if ($cari == "all") {
		if ($unit_jahit == '0') {
			$this->db->select(" a.*, b.nama as nama_ujh, c.nama_brg FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
			WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg AND c.status_aktif = 't' ", false);
		}
		else {
			$this->db->select("a.*, b.nama as nama_ujh, c.nama_brg FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
			WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg 
			AND a.id_unit_jahit = '$unit_jahit' AND c.status_aktif = 't'", false);
		}
	}
	else {
		if ($unit_jahit == '0') {
			$this->db->select(" a.*, b.nama as nama_ujh, c.nama_brg FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
			WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg AND (UPPER(a.kode_unit_jahit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) AND c.status_aktif = 't' ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_ujh, c.nama_brg
 FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
 WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg
			AND a.id_unit_jahit = '$unit_jahit' AND (UPPER(a.kode_unit_jahit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%')) AND c.status_aktif = 't' ", false);
		}
	}
    $query = $this->db->get();
    return $query->result();  
  }
   function getAll($num, $offset, $cari, $unit_jahit)
  {
	if ($cari == "all") {
		if ($unit_jahit == '0') {
			$this->db->select(" a.*, b.nama as nama_ujh, c.nama_brg FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
			WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg AND c.status_aktif = 't' ", false);
		}
		else {
			$this->db->select("a.*, b.nama as nama_ujh, c.nama_brg FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
			WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg 
			AND a.id_unit_jahit = '$unit_jahit' AND c.status_aktif = 't'", false);
		}
	}
	else {
		if ($unit_jahit == '0') {
			$this->db->select(" a.*, b.nama as nama_ujh, c.nama_brg FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
			WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg AND (UPPER(a.kode_unit_jahit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) AND c.status_aktif = 't' ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_ujh, c.nama_brg
 FROM tm_harga_brg_unit_jahit a, tm_unit_jahit b, tm_barang_wip c 
 WHERE a.kode_unit_jahit = b.kode_unit AND a.kode_brg = c.kode_brg
			AND a.id_unit_jahit = '$unit_jahit' AND (UPPER(a.kode_unit_jahit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%')) AND c.status_aktif = 't' ", false);
		}
	
    }
    $query = $this->db->get();
    return $query->result();
  }
  
   function cek_data($id_unit_jahit, $id_brg_jadi){
    $this->db->select("id from tm_harga_brg_unit_jahit WHERE id_brg = '$id_brg_jadi' AND id_unit_jahit = '$id_unit_jahit' ", false);
   
 //echo "select id from tm_harga_brg_unit_jahit WHERE id_brg = '$id_brg_jadi' AND id_unit_jahit = '$id_unit_jahit' <br>"; 
 
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
   
  function delete($id){    
    $this->db->delete('tm_harga_brg_unit_jahit', array('id' => $id));
  }
  
  function get_harga_for_print($unit_jahit) { 
		$sql = "SELECT * from  tm_harga_brg_unit_jahit ";
		if ($unit_jahit != '0')
			$sql.= " WHERE id_unit_jahit = '$unit_jahit' ";
		$sql.= " ORDER BY kode_unit_jahit, kode_brg ";
		$query	= $this->db->query($sql);
	
		$data_harga = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row2) {
				$sql2 = " SELECT * from tm_barang_wip
							WHERE id = '$row2->id_brg' ";
				$query3	= $this->db->query($sql2);
				$hasilrow = $query3->row();
				$kode_brg	= $hasilrow->kode_brg;
				$nama_brg	= $hasilrow->nama_brg;
				
				$query3	= $this->db->query(" SELECT kode_unit, nama, pkp FROM tm_unit_jahit WHERE id = '$row2->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$nama_unit_jahit	= $hasilrow->nama;
				$kode_unit_jahit	= $hasilrow->kode_unit;
				$pkp	= $hasilrow->pkp;
																																
				$data_harga[] = array(		'kode_brg'=> $kode_brg,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
										
										
											'nama_brg'=> $nama_brg,
											
											'harga'=> $row2->harga,
											'pkp'=> $pkp
											);
											
			} // endforeach header
		}
		else {
			$data_harga = '';
		}
		return $data_harga;
  }
}
