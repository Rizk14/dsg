<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}
		
	function statusSO($stop) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT i_so, d_so, i_status_so, f_stop_produksi FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$stop' ");
	}
	
	function view($cari,$iso,$stop) {
		$db2=$this->load->database('db_external', TRUE);
		$stp	= " c.f_stop_produksi='$stop' ";
		if($cari!='kosong' && $cari!=''){
			$paramcari	= " AND ".$stp." AND (a.i_product='$cari' OR upper(b.e_product_motifname) LIKE '%$cari%') ";
		}else{
			$paramcari	= " AND (".$stp.") ";
		}
		return $db2->query("
			SELECT a.i_product, b.e_product_motifname, a.n_quantity_awal, a.n_quantity_akhir, a.e_note from tm_stokopname_item a
			
			INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
			INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
			
			WHERE a.i_so='$iso' ".$paramcari." ORDER BY a.i_product ASC ");
	}
	
	function viewperpages($limit,$offset,$cari,$iso,$stop) {
		$db2=$this->load->database('db_external', TRUE);
		$stp	= " c.f_stop_produksi='$stop' ";
		if($cari!='kosong' && $cari!=''){
			$paramcari	= " AND ".$stp." AND (a.i_product='$cari' OR upper(b.e_product_motifname) LIKE '%$cari%') ";
		}else{
			$paramcari	= " AND (".$stp.") ";
		}
		$query	= $db2->query("
			SELECT a.i_product AS imotif, b.e_product_motifname AS eproductname, a.n_quantity_awal AS qtyawal, a.n_quantity_akhir AS qtyakhir, a.e_note AS enota from tm_stokopname_item a
						
						INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
						INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
						
						WHERE a.i_so='$iso' ".$paramcari." ORDER BY a.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows()>0)	{
			return $result	= $query->result();
		}	
	}

	function expolaporanstok($cari,$iso,$stop) {
		$db2=$this->load->database('db_external', TRUE);
		$stp	= " c.f_stop_produksi='$stop' ";
		if($cari!='kosong' && $cari!=''){
			$paramcari	= " AND ".$stp." AND (a.i_product='$cari' OR upper(b.e_product_motifname) LIKE '%$cari%') ";
		}else{
			$paramcari	= " AND (".$stp.") ";
		}
		
		return $db2->query("
			SELECT a.i_product AS imotif, b.e_product_motifname AS eproductname, a.n_quantity_awal AS qtyawal, a.n_quantity_akhir AS qtyakhir from tm_stokopname_item a
						
						INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
						INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
						
						WHERE a.i_so='$iso' ".$paramcari." ORDER BY a.i_product ASC ");	
	}

	function lbonkeluar($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonkeluar FROM tm_outbonm_item a
				INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm 
				
				WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_outbonm_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbonmasuk($dfirst,$dlast,$productmotif) {
$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonmasuk FROM tm_inbonm_item a
				INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
				
				WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_inbonm_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbbk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbk FROM tm_bbk_item a
				INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
				
				WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbk_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbbm($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbm FROM tm_bbm_item a
				INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying) 
				
				WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbm_cancel='f'
				
				GROUP BY a.i_product ");
	}
	
	function ldo($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT a.i_product, sum(a.n_deliver) AS jdo FROM tm_do_item a
			INNER JOIN tm_do b ON b.i_do=a.i_do
			
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_do_cancel='f'
			
			GROUP BY a.i_product ");
	}

	function lsj($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jsj FROM tm_sj_item a
			INNER JOIN tm_sj b ON b.i_sj=a.i_sj
			
			WHERE (b.d_sj BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_sj_cancel='f'
			
			GROUP BY a.i_product ");
	}	
}
?>
