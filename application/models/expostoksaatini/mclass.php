<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
			
		//$db2->set($fields);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
		
	}
	
	function cliststoksaatini($n_d_do_first,$n_d_do_last,$stopproduk,$iclass) {
		$db2=$this->load->database('db_external', TRUE);
		$qrycategory	= $db2->query(" SELECT * FROM tr_categories WHERE i_class='$iclass' ");
		
		if( (int) $qrycategory->num_rows() > 0) {
			$row	= $qrycategory->row_array();
	
			$strqry	= " SELECT a.i_product, a.i_product_motif
					FROM tm_so a
					
					INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product_motif 
					INNER JOIN tr_product_base c ON c.i_product_base=b.i_product 
					INNER JOIN tr_categories d ON d.i_category=c.i_category 
					
					WHERE (a.d_do BETWEEN '$n_d_do_first' AND '$n_d_do_last') AND c.f_stop_produksi='$stopproduk'
					
					GROUP BY a.i_product, a.i_product_motif ";
			
			return $db2->query($strqry);
		}
	}
	
	function cliststoksaatini2($iproduct,$iproductmotif) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= "
			SELECT 	a.i_so,
					a.i_product,
					a.i_product_motif,
					a.e_product_motifname,
					c.v_unitprice,
					a.i_status_do,
					a.n_do,
					a.d_do,
					a.n_saldo_awal,
					a.n_saldo_akhir,
					a.n_inbonm,
					a.n_outbonm,
					a.n_bbm,
					a.n_bbk,
					a.d_entry
					
				FROM tr_product_motif b
				
				INNER JOIN tm_so a ON b.i_product_motif=a.i_product_motif
				INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
				INNER JOIN tr_categories d ON d.i_category=c.i_category
					
				WHERE a.i_product='$iproduct' AND a.i_product_motif='$iproductmotif'
				
				ORDER BY a.i_so DESC LIMIT 1 ";
				
		return $db2->query($qstr);
	}
	
	// Desc : Saldo Akhir 
	function expliststoksaatini1($iproduct,$iproductmotif) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= "
			SELECT 	a.i_so,
					a.i_product,
					a.i_product_motif,
					a.e_product_motifname,
					c.v_unitprice,
					a.i_status_do,
					a.n_do,
					a.d_do,
					a.n_saldo_awal,
					a.n_saldo_akhir,
					a.n_inbonm,
					a.n_outbonm,
					a.n_bbm,
					a.n_bbk,
					a.d_entry					
				FROM tr_product_motif b										
				INNER JOIN tm_so a ON b.i_product_motif=a.i_product_motif
				INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
				INNER JOIN tr_categories d ON d.i_category=c.i_category					
				WHERE a.i_product='$iproduct' AND a.i_product_motif='$iproductmotif'					
				ORDER BY a.i_so DESC LIMIT 1 ";
				
		return $db2->query($qstr);
		// var_dump($qstr); die();
	}
	
	// Asc : Saldo Awal 
	function expliststoksaatini2($iproduct,$iproductmotif) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= "
			SELECT 	a.i_so,
					a.i_product,
					a.i_product_motif,
					a.e_product_motifname,
					c.v_unitprice,
					a.i_status_do,
					a.n_do,
					a.d_do,
					a.n_saldo_awal,
					a.n_saldo_akhir,
					a.n_inbonm,
					a.n_outbonm,
					a.n_bbm,
					a.n_bbk,
					a.d_entry				
				FROM tr_product_motif b										
				INNER JOIN tm_so a ON b.i_product_motif=a.i_product_motif
				INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
				INNER JOIN tr_categories d ON d.i_category=c.i_category					
				WHERE a.i_product='$iproduct' AND a.i_product_motif='$iproductmotif'				
				ORDER BY a.i_so ASC LIMIT 1 ";
				
		return $db2->query($qstr);
	}
		
	function lklsbrg() {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT i_class, e_class_name FROM tr_class ORDER BY e_class_name " );
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
	
	// Baru 19/01/2011
	function lbrgjadi($stopproduk,$filter_tgl_stokopname) {
		$db2=$this->load->database('db_external', TRUE);
		/*echo "SELECT a.i_product_motif,
				a.i_product,
				a.e_product_motifname,
				b.f_stop_produksi,
				b.v_unitprice,
				c.n_quantity_awal,
				c.n_quantity_akhir
			
			FROM tr_product_motif a
			
			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so
			
			WHERE b.f_stop_produksi='$stopproduk' AND a.n_active='1' ".$filter_tgl_stokopname."
			
			ORDER BY a.i_product_motif ASC"; */
			
		// CARI DATA SALDO AWAL DIAMBIL DARI STOKOPNAME
		return $db2->query(" SELECT DISTINCT ON (a.i_product_motif)
				a.i_product_motif,
				a.i_product,
				a.e_product_motifname,
				b.f_stop_produksi,
				b.v_unitprice,
				c.i_so, c.i_so_item,
				c.n_quantity_awal,
				c.n_quantity_akhir			
			FROM tr_product_motif a			
			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so			
			WHERE b.f_stop_produksi='$stopproduk' AND d.f_stop_produksi='$stopproduk' AND a.n_active='1' ".$filter_tgl_stokopname."					
			ORDER BY a.i_product_motif ASC ");
	}
	//BON KELUAR
	function lbonkeluar($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonkeluar FROM tm_outbonm_item a
				INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm				
				WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_outbonm_cancel='f'				
				GROUP BY a.i_product ");
	}
	// Query untuk Cek Nominal Saldo Akhir 
	// UPDATE : BON M MASUK QTY
	function lbonmasuk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonmasuk FROM tm_inbonm_item a
				INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm				
				WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_inbonm_cancel='f'				
				GROUP BY a.i_product ");
	}

	function lbbk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbk FROM tm_bbk_item a
				INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 				
				WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbk_cancel='f'				
				GROUP BY a.i_product ");
	}

	function lbbm($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbm FROM tm_bbm_item a
				INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying) 				
				WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbm_cancel='f'				
				GROUP BY a.i_product ");
	}
	
	function ldo($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		/*if ($productmotif == 'DGB301400')
			echo "SELECT a.i_product, sum(a.n_deliver) AS jdo FROM tm_do_item a
			INNER JOIN tm_do b ON b.i_do=a.i_do
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_do_cancel='f'
			GROUP BY a.i_product"; */
			
		return $db2->query("
			SELECT a.i_product, sum(a.n_deliver) AS jdo FROM tm_do_item a
			INNER JOIN tm_do b ON b.i_do=a.i_do			
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_do_cancel='f'			
			GROUP BY a.i_product ");
	}

	function lsj($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jsj FROM tm_sj_item a
			INNER JOIN tm_sj b ON b.i_sj=a.i_sj			
			WHERE (b.d_sj BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_sj_cancel='f'			
			GROUP BY a.i_product ");
	}
	
	function ljumlahfsk($productmotif,$sproduksi) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" aSELECT * FROM tm_stokmutasi WHERE i_product='$productmotif' AND f_stop_produksi='$sproduksi' AND f_active_month='t' ");
	}	
}
?>
