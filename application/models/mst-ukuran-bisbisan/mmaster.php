<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_ukuran_bisbisan');
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_ukuran_bisbisan',array('id'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_ukuran_bisbisan WHERE id = '$id' ");
    return $query->result();		  
  }
  
  //
  function save($id_ukuran, $nama, $var1, $var2, $var3, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'nama'=>$nama,
      'var1'=>$var1,
      'var2'=>$var2,
      'var3'=>$var3,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_ukuran_bisbisan',$data); }
	else {
		
		$data = array(
		  'nama'=>$nama,
		  'var1'=>$var1,
		  'var2'=>$var2,
		  'var3'=>$var3,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_ukuran);
		$this->db->update('tm_ukuran_bisbisan',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_ukuran_bisbisan', array('id' => $id));
  }
  
  function cek_data($nama){
    $this->db->select("* FROM tm_ukuran_bisbisan WHERE nama = '$nama' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
}
