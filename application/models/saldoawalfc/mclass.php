<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function ckstokopname($stopproduksi) {
		$db2=$this->load->database('db_external', TRUE);
		$sproduksi	= ($stopproduksi==1 || $stopproduksi=='1')?'TRUE':'FALSE';
		return $db2->query(" SELECT * FROM tm_saldo_awalfc WHERE i_status_so='0' AND f_stop_produksi='$sproduksi' ORDER BY i_so DESC LIMIT 1 ");
	}
	
	function mutasistockopname($d_so_baru,$d_so_lama,$i_product,$e_product_name,$n_quantity_akhir,$n_quantity_fisik,$iteration,$iterasi,$stopproduksi, $stok_fisik, $i_color) {		
		$db2=$this->load->database('db_external', TRUE);
		$iso_item	= array();
		$i_so_item	= array();
		
		$sproduksi	= ($stopproduksi==1 || $stopproduksi=='1')?'TRUE':'FALSE';
		$d_input	= date("Y-m-d");
		
		$qcariopname	= $db2->query(" SELECT * FROM tm_saldo_awalfc WHERE f_stop_produksi='$sproduksi' AND d_so='$d_so_baru' ");
		// 17-03-2015
		$hasilcariopname	= $qcariopname->row();
		$ncariopname	= $qcariopname->num_rows();
		
		if($ncariopname==0){
			$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
			$drow	= $qdate->row();
			$dentry	= $drow->date;
			
			$seq_tm_saldo_awalfc	= $db2->query(" SELECT cast(i_so AS integer) AS iso FROM tm_saldo_awalfc ORDER BY cast(i_so AS integer) DESC LIMIT 1 ");
			if($seq_tm_saldo_awalfc->num_rows() > 0 ) {
				$seqrow	= $seq_tm_saldo_awalfc->row();
				$i_so	= $seqrow->iso+1;
			} else {
				$i_so	= 1;
			}
			
			/*
			status	= 1 (telah dilakukan stokopname)
			status	= 0 (stok yg digunakan saat ini/ belum di stokopname)
			*/
			
			$qiso_lama	= $db2->query(" SELECT i_so FROM tm_saldo_awalfc WHERE d_so='$d_so_lama' AND f_stop_produksi='$sproduksi' AND i_status_so='0' "); 
			$riso_lama	= $qiso_lama->row();
			
			$db2->set(array(
					'i_so'=>$i_so,
					'd_so'=>$d_so_baru,
					'i_status_so'=>'0',
					'f_stop_produksi'=>$sproduksi,
					'd_entry'=>$d_input,
					'd_update'=>$d_input,
					'i_so_lama'=>$riso_lama->i_so));
			
			if($db2->insert('tm_saldo_awalfc')) {
				
				$update_tm_saldo_awalfc	= array(
						'i_status_so'=>'1'
				);
				$db2->update('tm_saldo_awalfc', $update_tm_saldo_awalfc, array('d_so'=>$d_so_lama,'i_status_so'=>'0','f_stop_produksi'=>$sproduksi));
				
				$j=0;
				
				$imutasinya	= array();
				$insert_stokmutasi	= array();
				$update_stokmutasi	= array();
				$ii_mutasi_old	= array();
				$insert_stokmutasi_new	= array();
				
				for($jumlah=0; $jumlah<=$iterasi; $jumlah++) {
					
					$seq_tm_saldo_awalfc_item	= $db2->query(" SELECT cast(i_so_item AS integer) AS iso_item FROM tm_saldo_awalfc_item ORDER BY cast(i_so_item AS integer) DESC LIMIT 1 ");
					if($seq_tm_saldo_awalfc_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_saldo_awalfc_item->row();
						$i_so_item[$j]	= $seqrow->iso_item+1;
					}else{
						$i_so_item[$j]	= 1;
					}
					
					$db2->set(array(
						'i_so_item'=>$i_so_item[$j],
						'i_so'=>$i_so,
						'i_product'=>$i_product[$j],
						'n_quantity_awal'=>$n_quantity_fisik[$j],
						'n_quantity_akhir'=>$n_quantity_fisik[$j]
					));
					
					$db2->insert('tm_saldo_awalfc_item');
					
					$imutasi	= $db2->query(" SELECT cast(i_mutasi AS integer)+1 AS imutasi FROM tm_stokmutasi ORDER BY cast(i_mutasi AS integer) DESC LIMIT 1 ");
					if($imutasi->num_rows()>0) {
						$row_imutasi	= $imutasi->row();
						$imutasinya[$j]	= $row_imutasi->imutasi;
					}else{
						$imutasinya[$j]	= 1;
					}
					
					$qstokmutasi	= $db2->query(" SELECT * FROM tm_stokmutasi WHERE i_product='$i_product[$j]' AND f_active_month='t' ORDER BY cast(i_mutasi AS integer) DESC LIMIT 1 ");
					if($qstokmutasi->num_rows()>0) { // Update kemudian insert
						$row_stokmutasi	= $qstokmutasi->row();
						$ii_mutasi_old[$j]	= $row_stokmutasi->i_mutasi;

						$insert_stokmutasi[$j]	= array(
							'i_mutasi'=>$imutasinya[$j],
							'i_product'=>$i_product[$j],
							'f_stop_produksi'=>$sproduksi,
							'n_quantity_trans'=>$n_quantity_akhir[$j],
							'n_quantity_fisik'=>$n_quantity_fisik[$j],
							'f_active_month'=>'TRUE',
							'd_entry'=>$dentry,
							'i_so'=>$i_so
						);
						
						$db2->insert('tm_stokmutasi',$insert_stokmutasi[$j]);

						$update_stokmutasi[$j]	= array(
								'f_active_month'=>'FALSE'
						);
						$db2->update('tm_stokmutasi', $update_stokmutasi[$j], array('i_mutasi'=>$ii_mutasi_old[$j],'i_product'=>$i_product[$j],'f_stop_produksi'=>$sproduksi));
						
					}else{ // Insert Baru
						$insert_stokmutasi_new[$j]	= array(
							'i_mutasi'=>$imutasinya[$j],
							'i_product'=>$i_product[$j],
							'f_stop_produksi'=>$sproduksi,
							'n_quantity_trans'=>$n_quantity_akhir[$j],
							'n_quantity_fisik'=>$n_quantity_fisik[$j],
							'f_active_month'=>'TRUE',
							'd_entry'=>$dentry,
							'i_so'=>$i_so
						);
						$db2->insert('tm_stokmutasi',$insert_stokmutasi_new[$j]);				
					}
					
					// 10-10-2014, SO per warna =================
						for ($xx=0; $xx<count($i_color[$j]); $xx++) {
							if (is_array($i_color[$j])) {
								$stokwarna	= array(
									'i_so_item'=>$i_so_item[$j],
									'i_color'=>$i_color[$j][$xx],
									'n_quantity_awal'=>$stok_fisik[$j][$xx],
									'n_quantity_akhir'=>$stok_fisik[$j][$xx]
								);
								$db2->insert('tm_saldo_awalfc_item_color',$stokwarna);	
							}
						}
					
					//===========================================
					
					$j+=1;
				}
				print "<script>alert(\"Stokopname bln ini telah disimpan, terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
			}else{
				print "<script>alert(\"Stokopname bln ini gagal disimpan, terimakasih.\");
				window.open(\"index\", \"_self\");</script>";
			}
		}else{
		//	print "<script>alert(\"Stokopname gagal disimpan, Tgl. Opname sdh ada!\");show(\"stokopname/cformmutasi\",\"#content\");</script>";
			// modif 17-03-2015. disini fungsi utk edit SO. HANYA TEMP AJA NANTINYA HRS DI-OFF-KAN LAGI SKRIP INI
			// ambil id so, trus ambil i_so_item ke tabel stokopname_item, trus query ke tabel stokopname_item_color, kemudian update
			
			// 18-03-2015, ditutup lagi
			// 19-12-2015 DIBUKA UTK UPDATE QTY PER WARNA YG BLM ADA
// 21-12-2015 tutup lagi, soalnya mau ditambah manual aja di database
// 15-01-2016 dibuka utk edit data SO
			$i_so	= $hasilcariopname->i_so;
			for($jj=0; $jj<=$iterasi; $jj++) {
				$queryxx	= $db2->query(" SELECT i_so_item FROM tm_saldo_awalfc_item 
										 WHERE i_so = '$i_so' AND i_product='".$i_product[$jj]."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$i_so_item	= $hasilxx->i_so_item;

					$db2->query(" update tm_saldo_awalfc_item set n_quantity_awal='".$n_quantity_fisik[$jj]."',
						n_quantity_akhir = '".$n_quantity_fisik[$jj]."' where i_so_item = '$i_so_item' ");
					
					for ($xx=0; $xx<count($i_color[$jj]); $xx++) {
						if (is_array($i_color[$jj])) {
							
							// 18-03-2015
							// ambil n_quantity_akhir dari query, nantinya quantity_akhir ditambahkan dgn quantity_awal
							$queryxx2	= $db2->query(" SELECT n_quantity_akhir FROM tm_saldo_awalfc_item_color 
										 WHERE i_so_item = '$i_so_item' AND i_color='".$i_color[$jj][$xx]."' ");
							if ($queryxx2->num_rows() > 0){
								$hasilxx2 = $queryxx2->row();
								$qtyakhir	= $hasilxx2->n_quantity_akhir;
								
								$qtyakhirnew = $qtyakhir+$stok_fisik[$jj][$xx];
								// n_quantity_akhir diganti dari $qtyakhirnew jadi $stok_fisik
								$db2->query(" UPDATE tm_saldo_awalfc_item_color SET n_quantity_awal='".$stok_fisik[$jj][$xx]."',
									n_quantity_akhir='".$stok_fisik[$jj][$xx]."' WHERE i_so_item='$i_so_item' 
									AND i_color='".$i_color[$jj][$xx]."' "); 
							}
							/*else {
								$stokwarna	= array(
									'i_so_item'=>$i_so_item,
									'i_color'=>$i_color[$jj][$xx],
									'n_quantity_awal'=>$stok_fisik[$jj][$xx],
									'n_quantity_akhir'=>$stok_fisik[$jj][$xx]
								);
								$db2->insert('tm_saldo_awalfc_item_color',$stokwarna);	
							} */
							
						} // end if
					} // end for
				} // end if
			} // end for utama
			
			print "<script>alert(\"Update Stokopname per warna berhasil disimpan, terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
                       
			// ------------------------------------------------------------
		}	
		//redirect('stokopname/cform/');
	}
	
	function tso($stopproduksi) {
		$db2=$this->load->database('db_external', TRUE);
	return 	$db2->query(" SELECT  a.i_so,
				a.d_so,
				b.i_so_item
				
		FROM tm_saldo_awalfc a 
		
		RIGHT JOIN tm_saldo_awalfc_item b ON a.i_so=b.i_so
		INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product 
		INNER JOIN tr_product_base d ON d.i_product_base=c.i_product

	 	WHERE a.i_status_so='0' AND d.f_stop_produksi=$stopproduksi
 
        ORDER BY b.i_product ASC ");	
	}
	
	function lso($d_date_n,$stopproduksi) {
		$db2=$this->load->database('db_external', TRUE);
	
	/* WHERE a.d_so >= '$d_date_n' AND a.d_so <= '$d_date_n' */
	return 	$db2->query(" SELECT  a.i_so,
				a.d_so,
				b.i_so_item,
				b.i_product AS iproduct,
				b.n_quantity_awal AS awal,
				b.n_quantity_akhir AS akhir,
				c.e_product_motifname AS motifname
				
		FROM tm_saldo_awalfc a 
		
		RIGHT JOIN tm_saldo_awalfc_item b ON a.i_so=b.i_so
		INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product 
		INNER JOIN tr_product_base d ON d.i_product_base=c.i_product

	 	WHERE a.d_so = '$d_date_n' AND a.i_status_so='0' AND d.f_stop_produksi='$stopproduksi' ORDER BY b.i_product ASC ");
	}
	
	function listbrgjadi() {
	$db2=$this->load->database('db_external', TRUE);
	/* Disabled 09-02-2011
	return $db2->query(" SELECT  a.i_product_base,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty
			
		FROM tr_product_base a 
		
		INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product 
		
		WHERE b.i_product_motif NOT IN (SELECT c.i_product FROM tm_saldo_awalfc d LEFT JOIN tm_saldo_awalfc_item c ON c.i_so=d.i_so) " );
	*/
/*
	return $db2->query(" SELECT  a.i_product_base AS ibaseproduct,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty,
			a.f_stop_produksi
		FROM tr_product_motif b 
		INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
		WHERE trim(b.i_product_motif) NOT IN (SELECT trim(c.i_product) AS iproduct FROM tm_saldo_awalfc d INNER JOIN tm_saldo_awalfc_item c ON c.i_so=d.i_so GROUP BY c.i_product) AND b.n_active=1 
		GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update
		ORDER BY a.d_update DESC ");
*/
	return $db2->query("SELECT  a.i_product_base AS ibaseproduct, b.i_product_motif AS imotif, b.e_product_motifname AS motifname, 
	                    b.n_quantity AS qty
                      FROM tr_product_motif b 
                      INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
                      WHERE trim(b.i_product_motif) NOT IN (SELECT trim(d.i_product) AS iproduct FROM tm_saldo_awalfc d ) 
                      AND b.n_active=1
                      GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update
                      ORDER BY b.i_product_motif, a.d_update DESC  ");
}
	
	function listbrgjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
	/* Disabled 09-02-2011
	$query	= $db2->query(" SELECT  a.i_product_base,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty
			
		FROM tr_product_base a 
		
		INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product 
		
		WHERE b.i_product_motif NOT IN (SELECT c.i_product FROM tm_saldo_awalfc d LEFT JOIN tm_saldo_awalfc_item c ON c.i_so=d.i_so) LIMIT ".$limit." OFFSET ".$offset );
	*/
	/* 06-05-2011
	 AND b.n_quantity!=0
	*/	
/*	
	$query	= $db2->query(" SELECT  a.i_product_base AS ibaseproduct,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty,
			a.f_stop_produksi
				
		FROM tr_product_motif b 
				
		INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
			
		WHERE trim(b.i_product_motif) NOT IN (SELECT trim(c.i_product) AS iproduct FROM tm_saldo_awalfc d INNER JOIN tm_saldo_awalfc_item c ON c.i_so=d.i_so GROUP BY c.i_product) AND b.n_active=1
		
		GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update ORDER BY a.d_update DESC LIMIT ".$limit." OFFSET ".$offset );
*/
	$query	= $db2->query(" SELECT  a.i_product_base AS ibaseproduct, b.i_product_motif AS imotif, b.e_product_motifname AS motifname, 
                          b.n_quantity AS qty
                          FROM tr_product_motif b 
                          INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
                          WHERE trim(b.i_product_motif) NOT IN (SELECT trim(d.i_product) AS iproduct FROM tm_saldo_awalfc d ) 
                          AND b.n_active=1
                          GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update
                          ORDER BY b.i_product_motif, a.d_update DESC  LIMIT ".$limit." OFFSET ".$offset);
				
		if ($query->num_rows() > 0 ) {
			return $query->result();
		}
	}
	
	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		//$ky_upper	= strtoupper($key);
		/* Disabled 09-02-2011
		return $db2->query(" SELECT  a.i_product_base,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty
			
		FROM tr_product_base a 
		
		INNER JOIN tr_product_motif b ON a.i_product_base=b.i_product 
		
		WHERE b.i_product_motif NOT IN (SELECT c.i_product FROM tm_saldo_awalfc d LEFT JOIN tm_saldo_awalfc_item c ON c.i_so=d.i_so) AND b.i_product_motif='$ky_upper' " );
		*/
	return $db2->query(" SELECT  a.i_product_base AS ibaseproduct,
			b.i_product_motif AS imotif,
			b.e_product_motifname AS motifname,
			b.n_quantity AS qty,
			a.f_stop_produksi
			
		FROM tr_product_motif b 
		INNER JOIN tr_product_base a ON trim(a.i_product_base)=trim(b.i_product)
		WHERE b.n_active=1 AND b.i_product_motif=trim('$key')
		GROUP BY a.i_product_base, b.i_product_motif, b.e_product_motifname, b.n_quantity, a.f_stop_produksi, a.d_update ORDER BY a.d_update DESC ");		
	}
	
	function msimpanbaru($periode,$i_product,$e_product_name,$qty_warna, $i_product_color, $i_color,$iteration) {
		$db2=$this->load->database('db_external', TRUE);
  	$query 	= $db2->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$q_tm_opname = $db2->query(" SELECT * FROM tm_saldo_awalfc WHERE i_periode='$periode'");
		if($q_tm_opname->num_rows()==0) {
	    for($x=0;$x<=$iteration;$x++) {
		    for ($xx=0; $xx<count($i_color[$x]); $xx++) {
			    $iproductcolor = trim($i_product_color[$x][$xx]);
			    $iproduct = trim($i_product[$x][$xx]);
			    $icolor = trim($i_color[$x][$xx]);
			    $qty = trim($qty_warna[$x][$xx]);
          $db2->query("insert into tm_saldo_awalfc
          						 (i_periode, i_product, i_color, e_product_name, qty, d_entry )
						          	  values
					          	 ('$periode','$i_product[$x]',$icolor,'$e_product_name[$x]',$qty,'$dentry')");
		    }
	    }
#	    $db2->insert('tm_saldo_awalfc',$item[$x]);
    }
	  redirect('saldoawalfc/cformbaru/');
	}
}
?>
