<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
							order by a.i_area,a.i_giro", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
							order by a.i_area,a.i_giro", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {		
    if($iarea=='NA'){
		$this->db->select("	tr_area.e_area_name, 
						a.i_giro, 
						a.d_giro, 
						a.i_rv, 
						a.d_rv, 
						tm_dt.i_dt, 
						tm_dt.d_dt, 
						tr_customer.i_customer, 
						tr_customer.e_customer_name, 
						a.e_giro_bank, 
						a.v_jumlah, 
						a.v_sisa, 
						a.f_posting, 
						a.f_giro_batal, 
						a.i_area, tm_pelunasan.i_pelunasan from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area 
                                        and tm_pelunasan.f_pelunasan_cancel='f')
							left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area and tm_dt.f_dt_cancel='f')
							where ((tm_pelunasan.i_jenis_bayar!='02' and 
							tm_pelunasan.i_jenis_bayar!='03' and 
							tm_pelunasan.i_jenis_bayar!='04' and 
							tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
							(a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
							a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
							and a.f_giro_tolak='f' 
  						order by a.i_area, a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);		
    }else{
		  $this->db->select("	tr_area.e_area_name, 
						  a.i_giro, 
						  a.d_giro, 
						  a.i_rv, 
						  a.d_rv, 
						  tm_dt.i_dt, 
						  tm_dt.d_dt, 
						  tr_customer.i_customer, 
						  tr_customer.e_customer_name, 
						  a.e_giro_bank, 
						  a.v_jumlah, 
						  a.v_sisa, 
						  a.f_posting, 
						  a.f_giro_batal, 
						  a.i_area, tm_pelunasan.i_pelunasan from tm_giro a 
							  inner join tr_area on(a.i_area=tr_area.i_area)
							  inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							  left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area 
                                          and tm_pelunasan.f_pelunasan_cancel='f')
							  left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area and tm_dt.f_dt_cancel='f')
							  where ((tm_pelunasan.i_jenis_bayar!='02' and 
							  tm_pelunasan.i_jenis_bayar!='03' and 
							  tm_pelunasan.i_jenis_bayar!='04' and 
							  tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
							  a.i_area='$iarea' and
							  (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
							  a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
  							and a.f_giro_tolak='f' 
							  order by a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);		
    }		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
    if($cari!=''){
      if($iarea=='NA'){
		    $this->db->select("	tr_area.e_area_name, 
						    a.i_giro, 
						    a.d_giro, 
						    a.i_rv, 
						    a.d_rv, 
						    tm_dt.i_dt, 
						    tm_dt.d_dt, 
						    tr_customer.i_customer, 
						    tr_customer.e_customer_name, 
						    a.e_giro_bank, 
						    a.v_jumlah, 
						    a.v_sisa, 
						    a.f_posting, 
						    a.f_giro_batal, 
						    a.i_area, tm_pelunasan.i_pelunasan from tm_giro a 
							    inner join tr_area on(a.i_area=tr_area.i_area)
							    inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							    left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area 
                                            and tm_pelunasan.f_pelunasan_cancel='f')
							    left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area and tm_dt.f_dt_cancel='f')
							    where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                          or a.v_jumlah=$cari) and 
							    ((tm_pelunasan.i_jenis_bayar!='02' and 
							    tm_pelunasan.i_jenis_bayar!='03' and 
							    tm_pelunasan.i_jenis_bayar!='04' and 
							    tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
							    (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
							    a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
    							and a.f_giro_tolak='f' 
							    order by a.i_area, a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
      }else{
		    $this->db->select("	tr_area.e_area_name, 
						    a.i_giro, 
						    a.d_giro, 
						    a.i_rv, 
						    a.d_rv, 
						    tm_dt.i_dt, 
						    tm_dt.d_dt, 
						    tr_customer.i_customer, 
						    tr_customer.e_customer_name, 
						    a.e_giro_bank, 
						    a.v_jumlah, 
						    a.v_sisa, 
						    a.f_posting, 
						    a.f_giro_batal, 
						    a.i_area, tm_pelunasan.i_pelunasan from tm_giro a 
							    inner join tr_area on(a.i_area=tr_area.i_area)
							    inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							    left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area 
                                            and tm_pelunasan.f_pelunasan_cancel='f')
							    left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area and tm_dt.f_dt_cancel='f')
							    where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                          or a.v_jumlah=$cari) and 
							    ((tm_pelunasan.i_jenis_bayar!='02' and 
							    tm_pelunasan.i_jenis_bayar!='03' and 
							    tm_pelunasan.i_jenis_bayar!='04' and 
							    tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
							    a.i_area='$iarea' and
							    (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
							    a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
    							and a.f_giro_tolak='f' 
							    order by a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
      }
    }else{
      if($iarea=='NA'){
	      $this->db->select("	tr_area.e_area_name, 
					      a.i_giro, 
					      a.d_giro, 
					      a.i_rv, 
					      a.d_rv, 
					      tm_dt.i_dt, 
					      tm_dt.d_dt, 
					      tr_customer.i_customer, 
					      tr_customer.e_customer_name, 
					      a.e_giro_bank, 
					      a.v_jumlah, 
					      a.v_sisa, 
					      a.f_posting, 
					      a.f_giro_batal, 
					      a.i_area, tm_pelunasan.i_pelunasan from tm_giro a 
						      inner join tr_area on(a.i_area=tr_area.i_area)
						      inner join tr_customer on(a.i_customer=tr_customer.i_customer)
						      left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area
                                            and tm_pelunasan.f_pelunasan_cancel='f')
						      left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area and tm_dt.f_dt_cancel='f')
						
						      where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%') and 
						      ((tm_pelunasan.i_jenis_bayar!='02' and 
						      tm_pelunasan.i_jenis_bayar!='03' and 
						      tm_pelunasan.i_jenis_bayar!='04' and 
						      tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
						      (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
						      a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
    							and a.f_giro_tolak='f' 
						      order by a.i_area, a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
      }else{
	      $this->db->select("	tr_area.e_area_name, 
					      a.i_giro, 
					      a.d_giro, 
					      a.i_rv, 
					      a.d_rv, 
					      tm_dt.i_dt, 
					      tm_dt.d_dt, 
					      tr_customer.i_customer, 
					      tr_customer.e_customer_name, 
					      a.e_giro_bank, 
					      a.v_jumlah, 
					      a.v_sisa, 
					      a.f_posting, 
					      a.f_giro_batal, 
					      a.i_area, tm_pelunasan.i_pelunasan from tm_giro a 
						      inner join tr_area on(a.i_area=tr_area.i_area)
						      inner join tr_customer on(a.i_customer=tr_customer.i_customer)
						      left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area
                                            and tm_pelunasan.f_pelunasan_cancel='f')
						      left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=a.i_area and tm_dt.f_dt_cancel='f')
						
						      where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%') and 
						      ((tm_pelunasan.i_jenis_bayar!='02' and 
						      tm_pelunasan.i_jenis_bayar!='03' and 
						      tm_pelunasan.i_jenis_bayar!='04' and 
						      tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
						      a.i_area='$iarea' and
						      (a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
						      a.d_giro <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' 
    							and a.f_giro_tolak='f' 
						      order by a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
      }
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } 
	function bacadetailpl($iarea,$ipl,$idt){
		$this->db->select(" a.*, b.v_nota_netto as v_nota from tm_pelunasan_item a
				    inner join tm_nota b on (a.i_nota=b.i_nota and a.i_area=b.i_area)
				    
					where a.i_pelunasan = '$ipl' 
					and a.i_area='$iarea'
					and a.i_dt='$idt'
					order by a.i_pelunasan,a.i_area ",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}  
    function bacagiro($igiro,$iarea)
    {
		$this->db->select(" * from tm_giro a
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where a.i_giro='$igiro' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }	  
    function update($igiro,$iarea,$dgirocair,$fgirocair)
    {
    	$this->db->set(
    		array(
				'd_giro_cair' => $dgirocair,
				'f_giro_cair' => $fgirocair
    		)
    	);
    	$this->db->where('i_giro',$igiro);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_giro');
    }       
}
?>
