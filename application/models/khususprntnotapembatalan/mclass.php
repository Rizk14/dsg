<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function penyetorpajak() {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT e_penyetor FROM tr_penyetor ORDER BY i_penyetor DESC LIMIT 1 ");
	}
	
	function clistfpenjbrgjadiperdo($nofaktur) {
				$db2=$this->load->database('db_external', TRUE);
		//if (strlen($nfaktur)!=0) {
			$query	= $db2->query( "
			
				SELECT 	e.i_do_code AS idocode, b.i_faktur_code,
						a.i_product AS imotif,
						a.e_product_name AS motifname,
						a.n_quantity AS qty,
						a.v_unit_price AS unitprice,
						(a.n_quantity * a.v_unit_price) AS amount
						
				FROM tm_faktur_do_item_t a
					
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tm_do e ON e.i_do=a.i_do WHERE b.i_faktur_code='$nofaktur'
				AND b.f_faktur_cancel='f'
				
				GROUP BY e.i_do_code, b.i_faktur_code, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ORDER BY a.i_product ASC ");
								
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}
		//}
	}
	
	function clistfpenjbrgjadiperdo2($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
			
			$query	= $db2->query("
			
				SELECT 	b.i_faktur, b.i_faktur_code,
						a.i_product AS imotif,
						a.e_product_name AS motifname
					
				FROM tm_faktur_do_item_t a
					
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do WHERE b.i_faktur_code='$nofaktur'
				AND b.f_faktur_cancel='f'
				
				GROUP BY b.i_faktur, b.i_faktur_code, a.i_product, a.e_product_name ORDER BY a.i_product ASC ");
				
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}
		
	}
		
	function clistfpenjperdo($nofaktur) {		
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
		
				SELECT 	count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.d_pajak AS dpajak
					
				FROM tm_faktur_do_item_t a
					
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do 
				WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f'
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.d_pajak " );
			
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
		
	function clistfpenjperdo_jml($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		
		return $db2->query(" SELECT sum(a.n_quantity * a.v_unit_price) totalnyaini
				FROM tm_faktur_do_item_t a INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur 
				INNER JOIN tm_do c ON c.i_do=a.i_do WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f' ");
	}
		
	function lbarangjadiperpages($limit,$offset) {		
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( "			
			SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
									
			FROM tm_faktur_do_item_t a 
				
			INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do c ON c.i_do=a.i_do 
			INNER JOIN tm_do_item d ON d.i_do=c.i_do 
			INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
			INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
			
			WHERE b.f_faktur_cancel='f' 
			
			GROUP BY b.d_faktur, b.i_faktur_code
			
			ORDER BY b.d_faktur DESC, b.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");

		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "	
			SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
									
			FROM tm_faktur_do_item_t a 
				
			INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do c ON c.i_do=a.i_do 
			INNER JOIN tm_do_item d ON d.i_do=c.i_do 
			INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
			INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
			
			WHERE b.f_faktur_cancel='f'
			
			GROUP BY b.d_faktur, b.i_faktur_code
			
			ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}
	
	function flbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "	
			SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
									
			FROM tm_faktur_do_item_t a 
			
			INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do c ON c.i_do=a.i_do 
			INNER JOIN tm_do_item d ON d.i_do=c.i_do 
			INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product
			INNER JOIN tr_product_base f ON f.i_product_base=e.i_product
			
			WHERE b.i_faktur_code='$key' AND b.f_faktur_cancel='f'
			
			GROUP BY b.d_faktur, b.i_faktur_code
			
			ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}
	
	function ititas() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT a.* FROM tr_initial_company a ORDER BY i_initial, i_initial_code DESC LIMIT 1 " );
	}
	
	function pelanggan($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT  a.e_customer_name AS customername,
					a.e_customer_address AS customeraddress,
					a.e_customer_npwp AS npwp
			
			FROM tr_customer a
			
			INNER JOIN tr_branch b ON b.i_customer=a.i_customer
			INNER JOIN tm_faktur_do_t c ON c.e_branch_name=b.e_initial
			INNER JOIN tm_faktur_do_item_t d ON d.i_faktur=c.i_faktur
			
			WHERE c.i_faktur_code='$nofaktur' AND c.f_faktur_cancel='f'
						
			GROUP BY a.e_customer_name, a.e_customer_address, a.e_customer_npwp " );
	}
	
	function pajak($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT 	b.i_faktur_pajak AS ifakturpajak,
					b.d_pajak
			
			FROM tm_faktur_do_t b
			
			INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			INNER JOIN tm_do e ON e.i_do=a.i_do 
			
			WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f'
			
			GROUP BY b.i_faktur_pajak, b.d_pajak		
		");
		/*
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
		*/
	}
	
	/*
	function remote($destination_ip) {
		return $db2->query(" SELECT * FROM tr_printer WHERE ip='$destination_ip' ORDER BY i_printer DESC LIMIT 1 ");
	}
	*/
	function remote($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' ORDER BY i_printer DESC LIMIT 1 ");
	}
	
	/*
	sum(n_quantity * v_unit_price) AS amount
	*/
	/*
	sum(v_unit_price) AS amount
	*/	
	function jmlitemharga($ifaktur,$iproduct) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(n_quantity * v_unit_price) AS amount FROM tm_faktur_do_item_t  WHERE i_faktur='$ifaktur' AND i_product='$iproduct' GROUP BY i_product, i_faktur, v_unit_price ");			
	}	
	
	function fprinted($nofaktur){
		$db2=$this->load->database('db_external', TRUE);
		$db2->query("UPDATE tm_faktur_do_t SET f_printed='t' WHERE i_faktur_code='$nofaktur' ");
	}
}
?>
