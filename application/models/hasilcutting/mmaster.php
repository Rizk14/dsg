<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  // start 13-12-2014
  function get_sohasilcutting($num, $offset, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_hasil_cutting WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
								
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO hasil cutting pengadaan ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO hasil cutting pengadaan ?";
								
				$data_so[] = array(		'id'=> $row1->id,	
										'bulan'=> $row1->bulan,	
										'nama_bln'=> $nama_bln,	
										'tahun'=> $row1->tahun,
										'status_approve'=> $row1->status_approve,
										'pesan'=> $pesan,
										'tgl_update'=> $row1->tgl_update
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sohasilcuttingtanpalimit($bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_hasil_cutting WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  // 15-12-2014
  function get_sohasilcuttingbyid($id_so) {

		$query	= $this->db->query(" SELECT a.id as id_header, a.bulan, a.tahun, a.tgl_so, b.*, 
					d.kode_brg, d.nama_brg
					FROM tt_stok_opname_hasil_cutting_detail b 
					INNER JOIN tt_stok_opname_hasil_cutting a ON b.id_stok_opname_hasil_cutting = a.id  
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.id = '$id_so' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// 06-04-2015
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
				
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
					// 06-04-2015
				// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a, 
									tt_stok_opname_hasil_cutting_detail b
									WHERE a.id = b.id_stok_opname_hasil_cutting
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' 
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				//	2.1. masuk bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//	2.2. masuk retur bhn baku
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk+= $hasilrow->jum_masuk;
				}
				
				// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar; */
				//-------------------------------------------------------------------------------------
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_hasil_cutting_detail_warna a 
							INNER JOIN tm_warna c ON c.id = a.id_warna
							WHERE a.id_stok_opname_hasil_cutting_detail = '$row->id' ORDER BY a.id ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_cutting_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_cutting a INNER JOIN tm_stok_hasil_cutting_warna b ON a.id = b.id_stok_hasil_cutting
							WHERE a.id_brg_wip = '$row->id_brg_wip'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// 06-04-2015
						// ========= hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
					/*	$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_cutting a 
											INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
											INNER JOIN tt_stok_opname_hasil_cutting_detail_warna c ON b.id = c.id_stok_opname_hasil_cutting_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' 
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						//	2.1. masuk bonmmasukcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a 
									INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
									INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						//	2.2. masuk retur bhn baku
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna+= $hasilrow->jum_masuk;
						}
						
						// 3. hitung keluar bln ini dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND c.kode_warna= '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna; */
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				$tgl_so = $row->tgl_so;
				$pisah1 = explode("-", $tgl_so);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_so = $tgl1."-".$bln1."-".$thn1;
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'nama_bln'=> $nama_bln,
										'tgl_so'=> $tgl_so,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesohasilcutting($id_so, $tgl_so, $iddetail, $id_brg_wip, $id_warna, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		if (isset($id_warna) && is_array($id_warna)) {
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);
				$qtytotalstokfisik+= $stok_fisik[$xx];
			} // end for
			$totalxx = $qtytotalstokfisik;
		}
		else
			$totalxx = 0;
	// ---------------------------------------------------------------------
		
		// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelah tgl SO
		// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar setelah tgl so keatas
		
		//1. hitung brg masuk dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a
								INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
								WHERE a.tgl_bonm > '$tgl_so'
								AND b.id_brg_wip = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$masuk_bgs = $hasilrow->jum_masuk;
					
					if ($masuk_bgs == '')
						$masuk_bgs = 0;
				}
				else
					$masuk_bgs = 0;
			  
			  // 2. hitung brg masuk retur dari tm_sjmasukbhnbakupic
			  $query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
								INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
								WHERE  a.tgl_sj > '$tgl_so'
								AND b.id_brg_wip = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$masuk_retur = $hasilrow->jum_masuk;
					
					if ($masuk_retur == '')
						$masuk_retur = 0;
				}
				else
					$masuk_retur = 0;
			  
			  $jum_masuk = $masuk_bgs+$masuk_retur;
			  // ========================= END MASUK =========================================================
			  
			  // 3. hitung brg keluar ke unit jahit
			  $query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm > '$tgl_so'
									AND b.id_brg_wip = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$keluar = $hasilrow->jum_keluar;
					
					if ($keluar == '')
						$keluar = 0;
				}
				else
					$keluar = 0;
			  
			  $jum_keluar = $keluar;
			  // ========================= END KELUAR =========================================================
			  $jum_stok_akhir = $jum_masuk - $jum_keluar;
			  $totalxx = $totalxx + $jum_stok_akhir;
			  
			  //cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
			$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
			if ($query3->num_rows() == 0){
				$data_stok = array(
					'id_brg_wip'=>$id_brg_wip,
					'stok'=>$totalxx,
					'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok_hasil_cutting', $data_stok);
						
				// ambil id_stok utk dipake di stok warna
				$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
				if($sqlxx->num_rows() > 0) {
					$hasilxx	= $sqlxx->row();
					$id_stok	= $hasilxx->id;
				}else{
					$id_stok	= 1;
				}
			}
			else {
				$hasilrow = $query3->row();
				$id_stok	= $hasilrow->id;
				
				// $totalxx
				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$totalxx', tgl_update_stok = '$tgl' 
				where id_brg_wip= '$id_brg_wip' ");
			}
			
			// A. stok_hasil_cutting_warna
				// ----------------------------------------------
				if (isset($id_warna) && is_array($id_warna)) {
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);						
						$totalxx = $stok_fisik[$xx];
						
						//------------------------------------------------------------------------------
						// tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
						// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar tgl setelah pencatatan SO
						
						//1. hitung brg masuk dari tm_bonmmasukcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmmasukcutting a 
										INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
										INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
										WHERE a.tgl_bonm > '$tgl_so'
										AND b.id_brg_wip = '$id_brg_wip'
										AND c.id_warna = '".$id_warna[$xx]."'
										 ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
					  
					  // 2. hitung brg masuk retur dari tm_sjmasukbhnbakupic
					  $query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a 
										INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
										INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
										WHERE a.tgl_sj > '$tgl_so'
										AND b.id_brg_wip = '$id_brg_wip'
										AND c.id_warna = '".$id_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_retur = $hasilrow->jum_masuk;
							
							if ($masuk_retur == '')
								$masuk_retur = 0;
						}
						else
							$masuk_retur = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_retur;
						// ========================= END MASUK warna =========================================================
						
						// 3. hitung brg keluar ke unit jahit
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluarcutting a 
											INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
											INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
											WHERE a.tgl_bonm > '$tgl_so'
											AND b.id_brg_wip = '$id_brg_wip'
											AND c.id_warna = '".$id_warna[$xx]."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar = $hasilrow->jum_keluar;
							
							if ($keluar == '')
								$keluar = 0;
						}
						else
							$keluar = 0;
					  
						$jum_keluar = $keluar;
						// ========================= END KELUAR =========================================================
						$jum_stok_akhir = $jum_masuk - $jum_keluar;
						$totalxx = $totalxx + $jum_stok_akhir;						
						// ------------ end hitung transaksi keluar/masuk ------------------------------
						
						// ========================= stok per warna ===============================================
						//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '".$id_warna[$xx]."'
								AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() == 0){
							// jika blm ada data di tm_stok_hasil_cutting_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_cutting'=>$id_stok,
								'id_warna'=>$id_warna[$xx],
								'stok'=>$totalxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
						}
						else {
							// $totalxx
							$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '".$totalxx."', tgl_update_stok = '$tgl' 
							where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
						}
					} // end for warna
				}
		// --------------------------------------------------------------------------------------------------------------
		
		// ambil id detail id_stok_opname_hasil_cutting_detail
		/*$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_cutting_detail 
					where id_brg_wip = '$id_brg_wip' 
					AND id_stok_opname_hasil_cutting = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0; */
		  
		 $this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail SET jum_stok_opname = '$qtytotalstokfisik',
					status_approve='t' where id = '$iddetail' ");
			
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$this->db->query(" UPDATE tt_stok_opname_hasil_cutting_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."'
						WHERE id_stok_opname_hasil_cutting_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
		}
		// ====================  
  }
  
  // 17-12-2014
  function getAllstokawal($num, $offset, $cari) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}
		
		$this->db->select(" a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.tgl_update_stok 
						FROM tm_stok_hasil_cutting a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				// 15-02-2014, stok per warna
					$queryxx	= $this->db->query(" SELECT c.kode, c.nama, a.stok FROM tm_stok_hasil_cutting_warna a, 
									tm_warna c
									WHERE a.kode_warna = c.kode AND a.id_stok_hasil_cutting = '$row1->id' ORDER BY c.nama ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'kode_warna'=> $rowxx->kode,
													'nama_warna'=> $rowxx->nama,
													'stok'=> $rowxx->stok
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
				
				/*$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				} */
								
				$data_stok[] = array(		'id'=> $row1->id,	
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'stok'=> $row1->stok,
											'nama_brg_jadi'=> $row1->e_product_motifname,
											//'id_gudang'=> $row1->id_gudang,
											//'nama_gudang'=> $kode_gudang." - ".$nama_gudang,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  function getAllstokawaltanpalimit($cari){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%".$this->db->escape_str($cari)."%')
							OR UPPER(b.e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') )";
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}else{
			$pencarian.= " ORDER BY a.kode_brg_jadi ";
		}
		
		$query	= $this->db->query(" SELECT a.id, a.kode_brg_jadi, a.stok, b.e_product_motifname, a.tgl_update_stok 
						FROM tm_stok_hasil_cutting a, tr_product_motif b 
						WHERE a.kode_brg_jadi = b.i_product_motif ".$pencarian." ");

    return $query->result();  
  }
  
  function savestokawal($kode, $nama, $temp_qty, $kode_warna, $qty_warna){  
    $tgl = date("Y-m-d H:i:s");
	
	//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotal = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$qty_warna[$xx] = trim($qty_warna[$xx]);
			$qtytotal+= $qty_warna[$xx];
		} // end for
	// ---------------------------------------------------------------------
	
	$datanya = array(
			  'kode_brg_jadi'=>$kode,
			  'stok'=>$qtytotal,
			  'tgl_update_stok'=>$tgl
			);
	$this->db->insert('tm_stok_hasil_cutting',$datanya);
	
	// ambil id_stok utk dipake di stok warna
	$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
	if($sqlxx->num_rows() > 0) {
		$hasilxx	= $sqlxx->row();
		$id_stok	= $hasilxx->id;
	}
	
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
					
					// ========================= stok per warna ===============================================
					//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_cutting'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							'stok'=>$qty_warna[$xx],
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
					}
				} // end for
				// ----------------------------------------------
  }
  
  function get_brgjadi_stokawal($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_cutting) 
					ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_cutting)  ";		
		$sql.=" AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$row1->i_product_motif'
										AND id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
			}
			else
				$stok = 0; */
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
										//'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjadi_stokawaltanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_cutting)";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE i_product_motif NOT IN (SELECT kode_brg_jadi FROM tm_stok_hasil_cutting)
					AND UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
}
