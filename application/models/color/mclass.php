<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
q = query
s = source
l = link
 **/
class Mclass extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function view()
	{
		return $this->db->query(" SELECT * FROM tr_color ORDER BY e_color_name ASC, i_color DESC ", false);
	}

	function viewperpages($limit, $offset)
	{
		$query = $this->db->query(" SELECT * FROM tr_color ORDER BY e_color_name ASC, i_color DESC LIMIT " . $limit . " OFFSET " . $offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function msimpan($icolor, $ecolorname)
	{
		$query     = $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
		$row       = $query->row();
		$entry    = $row->c;

		$str = array(
			'i_color' => $icolor,
			'e_color_name' => $ecolorname
		);

		$this->db->insert('tr_color', $str);

		/* DISINI INSERT KE tm_warna JUGA KARENA MENU mst-warna DICOMMENT TIDAK TAU KENAPA JADI DIBIKIN OTOMATIS INSERT JUGA ke tm_warna DISINI (22 Sep 2022) */
		$no = $this->db->query(" SELECT id + 1 AS id FROM tm_warna ORDER BY id::NUMERIC DESC LIMIT 1  ")->row();

		$val = array(
			'id' 		=> $no->id,
			'nama'		=> $ecolorname,
			'tgl_input' => $entry
		);

		$this->db->insert('tm_warna', $val);

		redirect('color/cform/');
	}

	function colorcode()
	{
		return $this->db->query(" SELECT cast(i_color AS integer)+1 AS colorcode FROM tr_color ORDER BY i_color DESC LIMIT 1 ");
	}

	function medit($id)
	{
		return $this->db->query(" SELECT * FROM tr_color WHERE i_color='$id' ORDER BY i_color DESC LIMIT 1 ");
	}

	function mupdate($icolor, $ecolorname)
	{
		$color_item	= array(
			'e_color_name' => $ecolorname
		);

		$this->db->update('tr_color', $color_item, array('i_color' => $icolor));
		redirect('color/cform');
	}

	function viewcari($txticolor, $txtecolorname)
	{
		if ($txticolor != "") {
			$filter	= " WHERE i_color='$txticolor' OR e_color_name='$txtecolorname' ";
		} else {
			$filter	= " WHERE e_color_name LIKE '%$txtecolorname%' ";
		}
		return $this->db->query(" SELECT * FROM tr_color " . $filter . " ORDER BY e_color_name ASC ");
	}

	function mcari($txticolor, $txtecolorname, $limit, $offset)
	{
		if ($txticolor != "") {
			$filter	= " WHERE i_color='$txticolor' OR e_color_name='$txtecolorname' ";
		} else {
			$filter	= " WHERE e_color_name LIKE '%$txtecolorname%' ";
		}
		$query	= $this->db->query(" SELECT * FROM tr_color " . $filter . " ORDER BY e_color_name ASC ");
		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function delete($id)
	{
		$this->db->delete('tr_color', array('i_color' => $id));
		redirect('color/cform/');
	}
}
