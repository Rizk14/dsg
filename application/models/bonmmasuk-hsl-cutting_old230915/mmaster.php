<?php
class Mmaster extends CI_Model {
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $cari) {	  

	if ($cari == "all") {
		/*distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 'f' ORDER BY b.no_bonm DESC, a.id_gudang ASC */
		
		$sql = " distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
			tm_apply_stok_hasil_cutting_detail b
			WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't' ORDER BY b.no_bonm DESC ";
		
		//$this->db->select(" b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok FROM tm_apply_stok_hasil_cutting b 
		//	INNER JOIN tm_apply_stok_hasil_cutting_detail a ON b.id=a.id_apply_stok WHERE a.status_stok='t' GROUP BY b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok ");
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();	
	}
	else {
		//$this->db->query(" b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok FROM tm_apply_stok_hasil_cutting b
		//	INNER JOIN tm_apply_stok_hasil_cutting_detail a ON b.id=a.id_apply_stok WHERE a.status_stok='t' AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(a.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(a.kode_brg) LIKE UPPER('%$cari%')) GROUP BY b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok ");
		$sql = " distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
				tm_apply_stok_hasil_cutting_detail b
				WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't' 
				AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(b.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(b.kode_brg) LIKE UPPER('%$cari%'))
				ORDER BY b.no_bonm DESC ";
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();
	}

	$data_fb = array();
	$detail_fb = array();
		
	if ($query->num_rows() > 0){
			
		$hasil = $query->result();
			
		foreach ($hasil as $row1) {
				
			/*$qrealisasi	= $this->db->query(" SELECT a.no_realisasi, a.tgl_realisasi, b.diprint FROM tt_realisasi_cutting a, 
						tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting 
						AND b.id='$row1->id_realisasi_cutting_detail' ");
			$rrealisasi	= $qrealisasi->row();
				
			$no_realisasi	= $rrealisasi->no_realisasi;
			$tgl_realisasi	= $rrealisasi->tgl_realisasi;
			$diprint	= $rrealisasi->diprint; */
				
			/*$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail WHERE id_apply_stok='$row1->id' 
								AND status_stok = 't' "); */
			
			$query2	= $this->db->query(" SELECT b.* FROM tm_apply_stok_hasil_cutting_detail b, tm_apply_stok_hasil_cutting a 
				WHERE a.id = b.id_apply_stok AND b.no_bonm = '$row1->no_bonm' AND b.status_stok = 't' ");
				
			if ($query2->num_rows() > 0) {
				$hasil2=$query2->result();
				foreach ($hasil2 as $row2) {
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");										
					if($query3->num_rows() > 0){
						$hasilrow	= $query3->row();
						$kode_brg	= $row2->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
					}
					else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
						if($query3->num_rows() > 0){
							$hasilrow	= $query3->row();
							$kode_brg	= $row2->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
						}
					}
					if ($row2->kode_brg_jadi != '') {	
						$query4	= $this->db->query(" SELECT i_product_motif, e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if($query4->num_rows()>0){
							$hasilrow2	= $query4->row();
							$iproductmotif	= $hasilrow2->i_product_motif;
							$productname	= $hasilrow2->e_product_motifname;
						}else{
							if ($row2->brg_jadi_manual == '')
								$productname = '';
							else
								$productname = $row2->brg_jadi_manual;
							/*$iproductmotif	= '';
							$productname	= ''; */
						}
					}
					else
						$productname = '';
					
					$qrealisasi	= $this->db->query(" SELECT a.no_realisasi, a.tgl_realisasi, b.diprint FROM tt_realisasi_cutting a, 
						tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting 
						AND b.id='$row2->id_realisasi_cutting_detail' ");
					$rrealisasi	= $qrealisasi->row();
						
					$no_realisasi	= $rrealisasi->no_realisasi;
					$tgl_realisasi	= $rrealisasi->tgl_realisasi;
					$diprint	= $rrealisasi->diprint;
					
					$query5	= $this->db->query(" SELECT hasil_potong FROM tt_realisasi_cutting_detail WHERE id='$row2->id_realisasi_cutting_detail' ");
					if($query5->num_rows()>0){
						$hasilrow3	= $query5->row();
						$hasil_potong	= $hasilrow3->hasil_potong;
					}else{
						$hasil_potong	= '';
					}
						
					$detail_fb[] = array('id' => $row2->id,
						'kode_brg_jadi' => $iproductmotif,
						'nama_brg_motif' => $productname,
						'kode_brg' => $kode_brg,
						'nama_brg' => $nama_brg,
						'diprint' => $diprint,
						'qty' => $hasil_potong );
				}
			}
			else {
				$detail_fb = '';
			}			 
				 
			$data_fb[] = array(
				'no_realisasi' => $no_realisasi,
				'tgl_realisasi' => $tgl_realisasi,
				'no_bonm' => $row1->no_bonm,	
				'tgl_bonm' => $row1->tgl_bonm,
				'tgl_update' => $row1->tgl_update,
				//'status_stok' => $row1->status_stok,
				'detail_fb' => $detail_fb );
					
			$detail_fb = array();
			$id_detailnya = "";
			
		}
	}
	else {
		$data_fb = '';
	}
	return $data_fb;
  }
  
  function getAlltanpalimit($cari){
	if ($cari=="all") {
		/*$query	= $this->db->query(" SELECT a.kode_brg_jadi, a.kode_brg, a.qty FROM tm_apply_stok_hasil_cutting_detail a 
			INNER JOIN tm_apply_stok_hasil_cutting b ON b.id=a.id_apply_stok WHERE a.status_stok='t' GROUP BY a.kode_brg_jadi, a.kode_brg, a.qty "); */
		$sql = " select distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
					tm_apply_stok_hasil_cutting_detail b
					WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't'";
		$query	= $this->db->query($sql);
	}
	else {
		/*$query	= $this->db->query(" SELECT a.kode_brg_jadi, a.kode_brg, a.qty FROM tm_apply_stok_hasil_cutting_detail a 
			INNER JOIN tm_apply_stok_hasil_cutting b ON b.id=a.id_apply_stok WHERE a.status_stok='t' AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(a.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(a.kode_brg) LIKE UPPER('%$cari%')) GROUP BY a.kode_brg_jadi, a.kode_brg, a.qty "); */
		$sql = " select distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
				tm_apply_stok_hasil_cutting_detail b
				WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't' 
				AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(b.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(b.kode_brg) LIKE UPPER('%$cari%'))";
		$query	= $this->db->query($sql);
	}
    return $query->result();  
  }
      
  function cek_data($no_bonm, $gudang){
    $this->db->select("id from tm_apply_stok_pembelian WHERE no_bonm = '$no_bonm' AND id_gudang = '$gudang' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_sj($num, $offset, $csupplier, $id_gudang, $cari) {
	if ($cari == '') {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail brg-nya

				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id' 
				AND a.status_stok = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama FROM tm_barang a, tm_satuan b
								WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama;
							
						$detail_sj[] = array('id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'qty'=> $row2->qty,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_sjtanpalimit($csupplier, $cari){
	if ($cari == '') { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ");
	}
    
    return $query->result();  
  }
 
  
  function get_detail_bonm() {
	  
    $headernya = array();    
    $detail_brg = array();    
    
    $query	= $this->db->query(" SELECT a.* FROM tm_apply_stok_hasil_cutting a, tt_realisasi_cutting b WHERE 
					a.id_realisasi_cutting = b.id AND b.for_eksternal = 'f' AND 
					a.status_stok='f' ORDER BY a.tgl_bonm ASC, a.no_bonm ASC ");
    
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach($hasil as $row1) {
			$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail WHERE status_stok='f'	AND id_apply_stok='$row1->id' ORDER BY id ASC ");  
			
					if ($query2->num_rows() > 0){
						
						$hasil2=$query2->result();
						
						foreach ($hasil2 as $row2) {
							if ($row2->kode_brg_jadi != '') {
								$qbrgjadi	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' LIMIT 1 ");
								if ($qbrgjadi->num_rows() > 0){
									$rbrgjadi	= $qbrgjadi->row();
									$nama_brg_jadi = $rbrgjadi->e_product_motifname;
								}
								else {
									if ($row2->brg_jadi_manual == '')
										$nama_brg_jadi = '';
									else
										$nama_brg_jadi = $row2->brg_jadi_manual;
								}
							}
							else
								$nama_brg_jadi = '';
							
							$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a	WHERE a.kode_brg='$row2->kode_brg' ");
							if($query3->num_rows()>0){
								$hasilrow = $query3->row();
								$kode_brg	= $row2->kode_brg;
								$nama_brg	= $hasilrow->nama_brg;
							}else{
								$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_brg_hasil_makloon a
										WHERE a.kode_brg='$row2->kode_brg' ");
								if($query3->num_rows()>0){
									$kode_brg	= $row2->kode_brg;
									$nama_brg	= $hasilrow->nama_brg;
								}
								else {
									$kode_brg	= '';
									$nama_brg	= '';	
								}
							}
							
							$query5	= $this->db->query(" SELECT id, qty_realisasi, jum_gelar, hasil_potong, diprint 
											FROM tt_realisasi_cutting_detail WHERE id='$row2->id_realisasi_cutting_detail' ");
							$row3	= $query5->row();
							
							$detail_brg[] = array(		
								'id' => $row2->id,
								'kode_brg' => $kode_brg,
								'nama' => $nama_brg,
								'kode_brg_jadi' => $row2->kode_brg_jadi,
								'nama_brg_jadi' => $nama_brg_jadi,
								'nama_brg_jadi_other' => $row2->brg_jadi_manual,
								'qty' => $row2->qty,
								'hasil_potong' => $row3->hasil_potong, // ini yg dipake, bukan qty_realisasi
								'id_realisasi_cutting_detail' => $row3->id,
								'jum_gelar' => $row3->jum_gelar,
								'diprint' => $row3->diprint );
						}
					}
					else
						$detail_brg = '';

				$query4	= $this->db->query(" SELECT * FROM tt_realisasi_cutting WHERE id='$row1->id_realisasi_cutting' ");
				if($query4->num_rows()>0) {
						$hasilrow = $query4->row();
						$no_realisasi	= $hasilrow->no_realisasi;
						$tgl_realisasi	= $hasilrow->tgl_realisasi;
				} else {
						$no_realisasi	= '';
						$tgl_realisasi	= '';
				}
				
				$headernya[] = array('id' => $row1->id,
							'id_realisasi_cutting' => $row1->id_realisasi_cutting,
							'no_bonm' => $row1->no_bonm,
							'tgl_bonm' => $row1->tgl_bonm,
							'tgl_update' => $row1->tgl_update,
							'no_realisasi' => $no_realisasi,
							'tgl_realisasi' => $tgl_realisasi,
							'detail_brg' => $detail_brg);
				
				$detail_brg = array();
		}
		
	} else {
			$headernya = '';
	}
	return $headernya;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
  function get_bonm($id_apply_stok){
	$query	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian WHERE id = '$id_apply_stok' ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	$detail_bonm = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b, tm_pembelian c, 
				tm_apply_stok_pembelian d 
				WHERE a.kode_brg = b.kode_brg AND a.id_pembelian = c.id AND d.no_sj = c.no_sj 
				AND d.kode_supplier = c.kode_supplier AND b.id_gudang = '$row1->id_gudang' 
				AND c.no_sj = '$row1->no_sj' 
				AND d.kode_supplier = '$row1->kode_supplier' ");  
		
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id'
								AND kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$cek = 'y';
						}
						else
							$cek = 't';
						
						$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE 
									kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE 
									kode_brg = '$row2->kode_brg' AND kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$harga	= $hasilrow->harga;
				
						$detail_bonm[] = array(		
										'id'=> $row2->id,
										'kode_brg'=> $row2->kode_brg,
										'nama'=> $nama_brg,
										'harga'=> $harga,
										'qty'=> $row2->qty,
										'cek'=> $cek
										);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$query2	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE 
									kode_supplier = '$row1->kode_supplier' AND no_sj = '$row1->no_sj' ");
						$hasilrow = $query2->row();
						$tgl_sj	= $hasilrow->tgl_sj;
				
				$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				}
				 
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,	
											'tgl_sj'=> $tgl_sj,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
	}
	return $data_bonm;
  }
  
  function delete($no_bonm){   
	  $tgl = date("Y-m-d");
	   
	  $sql = "SELECT a.id_realisasi_cutting, b.* FROM tm_apply_stok_hasil_cutting a, 
					tm_apply_stok_hasil_cutting_detail b WHERE a.id = b.id_apply_stok
					AND b.no_bonm = '$no_bonm'
					AND b.status_stok = 't' "; 
							
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sj;
				$id_realisasi_cutting = $row4->id_realisasi_cutting;
				$id_apply_stok = $row4->id_apply_stok;
				$id_detail = $row4->id;
				$kode_brg = $row4->kode_brg;
				$kode_brg_jadi = $row4->kode_brg_jadi;
				$brg_jadi_manual = $row4->brg_jadi_manual;
				$id_realisasi_cutting_detail = $row4->id_realisasi_cutting_detail;
				$qty_hasil_potong = $row4->qty; // new 29-02-2012
				
				$query3	= $this->db->query(" SELECT a.is_dacron, b.* FROM tt_realisasi_cutting a, 
											tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting AND
											b.id='$id_realisasi_cutting_detail' ");
						
				if($query3->num_rows()>0){
					$ridrealisasi	= $query3->row();
					$id_realisasi	= $ridrealisasi->id_realisasi_cutting;
					$jum_gelar		= $ridrealisasi->jum_gelar;
					$diprint		= $ridrealisasi->diprint;
					$is_dacron		= $ridrealisasi->is_dacron;
				}
								
				//2. ambil stok terkini di tm_stok 
				$sqlxx = " SELECT * FROM tm_stok_hasil_cutting WHERE 
												kode_brg_jadi='$kode_brg_jadi' AND kode_brg='$kode_brg' AND diprint = '$diprint'
												AND is_dacron = '$is_dacron' ";
				if ($brg_jadi_manual != '')
					$sqlxx.= " AND brg_jadi_manual = '$brg_jadi_manual' ";
				else
					$sqlxx.= " AND (brg_jadi_manual is NULL OR brg_jadi_manual = '') ";
				
				$query3	= $this->db->query($sqlxx);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_stok = $hasilrow->id;
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty_hasil_potong;
																									
				//4. update stok di tm_stok_hasil_cutting
				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where id= '$id_stok' ");
																			
				$this->db->query(" UPDATE tt_realisasi_cutting SET status_apply_stok = 'f' where id = '$id_realisasi' ");
				$this->db->query(" UPDATE tt_realisasi_cutting_detail SET status_apply_stok = 'f' where id = '$id_realisasi_cutting_detail' ");
													
				$this->db->query(" UPDATE tm_apply_stok_hasil_cutting SET 
														status_stok = 'f', tgl_update = '$tgl'
														where id= '$id_apply_stok'  ");
														
				$this->db->query(" UPDATE tm_apply_stok_hasil_cutting_detail SET 
														no_bonm = '', tgl_bonm = NULL, status_stok = 'f'
														where id= '$id_detail'  ");
		}
	  }
										
		
  }

}
