<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function hargaperpelanggan($imotif,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}

	function lcustomer() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_customer ORDER BY e_customer_name ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}
		
	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC " );
	}	
	
	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($key)) {
			return $db2->query("
					SELECT 	a.i_product_base AS iproduct,
						b.i_product_motif AS imotif,	
						b.e_product_motifname AS motifname,
						b.n_quantity AS qty
						
					FROM tr_product_base a 
					
					RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product 
					
					WHERE a.i_product_base LIKE '$key%' OR b.i_product_motif LIKE '$key%' 
					
					ORDER BY b.i_product_motif DESC ");
		}
	}
	
	function expbrgjadi($i_product, $allitem) {
		$db2=$this->load->database('db_external', TRUE);
		//=============================
		$sql=" SELECT a.i_product_motif, a.i_product, a.e_product_motifname, b.v_unitprice, b.f_stop_produksi
		 FROM tr_product_motif a, tr_product_base b
				WHERE a.i_product = b.i_product_base AND a.n_active = '1' ";
		
		if ($allitem != 'y')
			$sql.= " AND a.i_product_motif='$i_product' ";

		$sql.= " ORDER BY b.f_stop_produksi, a.i_product_motif ASC";
		//=============================
				
		$query	= $db2->query($sql);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
		
	function jmlorder($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir) {
		$db2=$this->load->database('db_external', TRUE);
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		$sql = " SELECT SUM(a.n_count) AS jmlorder, 
			sum(c.n_deliver) as pemenuhan, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product  AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND b.d_op >='$d_op_first' AND b.d_op <='$d_op_last' ";		
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
		if ($icustomer != '')
			$sql.=" AND b.i_customer = '$icustomer' ";
		$sql.=	" GROUP BY b.i_customer"; //echo $sql."<br>";
		
		return $db2->query($sql);
				
	}
	
	// 03-07-2012
	function jumopall($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast) {

		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
				
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product=='') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last=="") && ($d_op_first==0 || $d_op_last==0)) ) {
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else{
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' "; 
		}
		
		return $db2->query(" SELECT SUM(a.n_count) AS jmlorder
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op
			".$ddate." ".$batal." ".$imotif." ".$icustomer. " ".$f_op_dropforcast." ");
	}
	
	// 30-07-2012
	function jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir) {
		$db2=$this->load->database('db_external', TRUE);
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		// SELECT SUM(a.n_count) AS jmlorder, sum(c.n_deliver) as pemenuhan, b.i_customer
		$sql = " SELECT a.n_count as jmlorder, c.n_deliver as pemenuhan, b.i_op, c.i_product, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product  AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			//$sql.= " AND b.d_op >='$d_op_first' AND b.d_op <='$d_op_last' ";		
			$sql.= " AND ((b.d_op >='$d_op_first' AND b.d_op <='$d_op_last') OR (d.d_do >='$d_op_first' AND d.d_do <='$d_op_last')) ";
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
		if ($icustomer != '')
			$sql.= " AND b.i_customer = '$icustomer' ";
		$sql.= "ORDER BY b.i_op, c.i_product"; //echo $sql; die();
		$query3= $db2->query($sql);
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			$jmlorder = 0; $pemenuhan = 0; $temp_op = "";
			foreach ($hasil3 as $row3) {
				$pemenuhan+= $row3->pemenuhan;
				if ($temp_op != $row3->i_op) {
					$jmlorder+= $row3->jmlorder;
					$temp_op = $row3->i_op;
				}
			}
			
			$data_op = array(		'jmlorder'=> $jmlorder,	
									'pemenuhan'=> $pemenuhan,
									'i_customer'=> $icustomer
								);
		}
		else
			$data_op = '';
		return $data_op;
	}
	
	// 13-08-2012, ngantuk poll
	function clistopvsdo_dokosong($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir) {		
		$db2=$this->load->database('db_external', TRUE);
		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
		//echo $i_product."<br>";
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "satu ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if($i_product=='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "dua ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			//echo "tiga ";
			$ddate	= "";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
		}else{
			//echo "empat ";
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}
		/*echo "SELECT c.f_stop_produksi AS stopproduct, 
				UPPER(a.i_product) AS imotif, 
				UPPER(a.e_product_name) AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$f_op_dropforcast."
				AND (a.n_count-a.n_residual)=0
				GROUP BY c.f_stop_produksi, UPPER(a.i_product), UPPER(a.e_product_name), c.v_unitprice		
				ORDER BY UPPER(a.i_product) ASC"; */
		
		$query3	= $db2->query(" SELECT c.f_stop_produksi AS stopproduct, 
				UPPER(a.i_product) AS imotif, 
				UPPER(a.e_product_name) AS productmotif, 
				c.v_unitprice AS unitprice
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$f_op_dropforcast."
				AND (a.n_count-a.n_residual)=0
				GROUP BY c.f_stop_produksi, UPPER(a.i_product), UPPER(a.e_product_name), c.v_unitprice		
				ORDER BY UPPER(a.i_product) ASC ");
						
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			
			foreach ($hasil3 as $row3) {
				/*if ($row3->imotif == 'TPT070100')
					echo "select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif'"; */
				$queryxx	= $db2->query(" select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif' ");
				if ($queryxx->num_rows() == 0){
					$data_op[] = array(		'imotif'=> $row3->imotif,	
									'productmotif'=> $row3->productmotif,
									'unitprice'=> $row3->unitprice,
									'stopproduct'=> $row3->stopproduct
								);
				}
			}
		}
		else
			$data_op = '';
		return $data_op;		
	}
	
	// 15-10-2012
	function soawal($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		$tahun	= substr($dfirst,0,4); // YYYY-mm-dd
		$bulan	= substr($dlast,5,2);
		
		return $db2->query("
			SELECT b.n_quantity_awal FROM tm_stokopname a, tm_stokopname_item b WHERE a.i_so = b.i_so
			AND SUBSTRING(CAST(a.d_so AS character varying),1,4)='$tahun' 
			AND SUBSTRING(CAST(a.d_so AS character varying),6,2)='$bulan'
			AND b.i_product = '$productmotif'
		 ");
	}
	
	function lbonkeluar($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonkeluar FROM tm_outbonm_item a
				INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
				
				WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_outbonm_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbonmasuk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonmasuk FROM tm_inbonm_item a
				INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm
				
				WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_inbonm_cancel='f'
				
				GROUP BY a.i_product ");
	}
	
	function lbbk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbk FROM tm_bbk_item a
				INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
				
				WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbk_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbbm($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbm FROM tm_bbm_item a
				INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying) 
				
				WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbm_cancel='f'
				
				GROUP BY a.i_product ");
	}
	
	function ldo($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_deliver) AS jdo FROM tm_do_item a
			INNER JOIN tm_do b ON b.i_do=a.i_do
			
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_do_cancel='f'
			
			GROUP BY a.i_product ");
	}

	function lsj($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jsj FROM tm_sj_item a
			INNER JOIN tm_sj b ON b.i_sj=a.i_sj
			
			WHERE (b.d_sj BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_sj_cancel='f'
			
			GROUP BY a.i_product ");
	}
}
?>
