<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so_unit_jahit($bulan, $tahun, $unit_jahit) {
	$query3	= $this->db->query(" SELECT id, tgl_so, status_approve FROM tt_stok_opname_unit_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND kode_unit = '$unit_jahit' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			// 04-12-2014
			$tgl_so = $hasilrow->tgl_so;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so
							);
							
		return $so_bahan;
  }

  function get_stok_unit_jahit($bulan, $tahun, $unit_jahit) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
	  		
		$sql = " SELECT id, kode_brg_jadi FROM tm_stok_unit_jahit WHERE kode_unit = '$unit_jahit' ORDER BY kode_brg_jadi ";
		
		$query	= $this->db->query($sql);
			
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {	
				// 24-03-2014 
				// 1. ini ambil stok, stok_bagus, dan stok_perbaikan berdasarkan warna			
				$sqlxx = " SELECT c.stok, c.stok_bagus, c.stok_perbaikan, c.kode_warna, b.nama as nama_warna 
							FROM tm_warna b, tm_stok_unit_jahit_warna c
						WHERE c.kode_warna = b.kode AND c.id_stok_unit_jahit = '$row->id' ";
				
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					
					foreach ($hasilxx as $rowxx) {
						//2. ini query ambil data warna berdasarkan bgs dan perbaikan. ini sementara dikomen dulu, dan defaultnya 0 utk persiapan
						
						// ==> barang bagus
						//hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
					/*	$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c
									WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
									AND a.tgl_bonm >= '".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <= '".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND a.jenis_keluar = '1' AND c.kode_warna='$rowxx->kode_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
						}
						else
							$masuk_bgs = 0;
						
						//hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c
									WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
									AND a.tgl_bonm >= '".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <= '".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND a.jenis_keluar = '2' AND c.kode_warna='$rowxx->kode_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
						}
						else
							$masuk_pengembalian = 0;
						
						// hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.tgl_sj >= '".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <= '".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' AND c.kode_warna='$rowxx->kode_warna' ");
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
						}
						else
							$keluar_bgs = 0;
						
						// hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b, tm_sjmasukbhnbakupic_detail_warna c
									WHERE a.id = b.id_sjmasukbhnbakupic AND b.id = c.id_sjmasukbhnbakupic_detail
									AND a.tgl_sj >= '".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <= '".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND c.kode_warna='$rowxx->kode_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						$jum_bgs = $masuk_bgs+$masuk_pengembalian-$keluar_bgs-$keluar_retur_bhnbaku;
						// ==================================================================================
						
						// ==> barang perbaikan
						//hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c
									WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
									AND a.tgl_sj >= '".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <= '".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' AND c.kode_warna='$rowxx->kode_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
						}
						else
							$masuk_returbrgjadi = 0;
						
						// hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.tgl_sj >= '".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <= '".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' AND c.kode_warna='$rowxx->kode_warna' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
						}
						else
							$keluar_perbaikan = 0;
						
						$jum_perbaikan = $masuk_returbrgjadi-$keluar_perbaikan;
						//===================================================================================
						*/
						$detail_warna[] = array(
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> $rowxx->stok,
										'stok_opname'=> $rowxx->stok,
										
										//'stok_opname_bgs'=> $jum_bgs,
										'stok_opname_bgs'=> $rowxx->stok_bagus,
										//'stok_opname_perbaikan'=> $jum_perbaikan,
										'stok_opname_perbaikan'=> $rowxx->stok_perbaikan);
					}
				}
				else {
					$sqlxx = " SELECT a.id, b.kode, b.nama FROM tm_warna_brg_jadi a, tm_warna b 
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$row->kode_brg_jadi' ";
					
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						
						foreach ($hasilxx as $rowxx) {
							$detail_warna[] = array(
										'kode_warna'=> $rowxx->kode,
										'nama_warna'=> $rowxx->nama,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0,
										'stok_opname_bgs'=> 0,
										'stok_opname_perbaikan'=> 0
									);
						}
					}
					else
						$detail_warna = '';
				}

				$detail_bahan[] = array('kode_brg_jadi'=> $row->kode_brg_jadi,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else
			$detail_bahan = '';
		
		return $detail_bahan;
  }
  
  function get_all_stok_opname_unit_jahit($bulan, $tahun, $unit_jahit) {

		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.e_product_motifname as nama_brg_jadi FROM tt_stok_opname_unit_jahit_detail b, 
					tt_stok_opname_unit_jahit a, tr_product_motif d
					WHERE b.id_stok_opname_unit_jahit = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.kode_unit = '$unit_jahit'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_unit_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_unit_jahit_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_unit_jahit a, tm_stok_unit_jahit_warna b
							WHERE a.id = b.id_stok_unit_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$unit_jahit'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'stok_opname_bgs'=> $rowxx->jum_bagus,
									'stok_opname_perbaikan'=> $rowxx->jum_perbaikan
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->nama_brg_jadi,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesounitjahit($is_new, $id_stok, $kode_brg_jadi, $kode_warna, $stok, $stok_fisik_bgs, $stok_fisik_perbaikan){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  // 06-02-2014
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokawal = 0;
		$qtytotalstokfisikbgs = 0;
		$qtytotalstokfisikperbaikan = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik_bgs[$xx] = trim($stok_fisik_bgs[$xx]);
			$stok_fisik_perbaikan[$xx] = trim($stok_fisik_perbaikan[$xx]);
			
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisikbgs+= $stok_fisik_bgs[$xx];
			$qtytotalstokfisikperbaikan+= $stok_fisik_perbaikan[$xx];
		} // end for
	// ---------------------------------------------------------------------
	  
	  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetail	= $seqrow->id+1;
			}else{
				$iddetail	= 1;
			}
			
			// 24-03-2014
			$totalxx = $qtytotalstokfisikbgs+$qtytotalstokfisikperbaikan;
		  
		   $data_detail = array(
						'id'=>$iddetail,
						'id_stok_opname_unit_jahit'=>$id_stok,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'stok_awal'=>$qtytotalstokawal,
						'jum_stok_opname'=>$totalxx,
						// 24-03-2014
						'jum_bagus'=>$qtytotalstokfisikbgs,
						'jum_perbaikan'=>$qtytotalstokfisikperbaikan
					);
		   $this->db->insert('tt_stok_opname_unit_jahit_detail',$data_detail);
		   
			// ambil id detail id_stok_opname_unit_jahit_detail
		/*	$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0; */
			
			// ----------------------------------------------
			for ($xx=0; $xx<count($kode_warna); $xx++) {
				$kode_warna[$xx] = trim($kode_warna[$xx]);
				$stok[$xx] = trim($stok[$xx]);
				//$stok_fisik[$xx] = trim($stok_fisik[$xx]);
				$stok_fisik_bgs[$xx] = trim($stok_fisik_bgs[$xx]);
				$stok_fisik_perbaikan[$xx] = trim($stok_fisik_perbaikan[$xx]);
				
				$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
				if($seq_warna->num_rows() > 0) {
					$seqrow	= $seq_warna->row();
					$idbaru	= $seqrow->id+1;
				}else{
					$idbaru	= 1;
				}
				
				// 24-03-2014
				$totalxx = $stok_fisik_bgs[$xx]+$stok_fisik_perbaikan[$xx];
				
				$tt_stok_opname_unit_jahit_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_stok_opname_unit_jahit_detail'=>$iddetail,
						 'id_warna_brg_jadi'=>'0',
						 'kode_warna'=>$kode_warna[$xx],
						 'jum_stok_opname'=>$totalxx,
						 //24-03-2014
						 'jum_bagus'=>$stok_fisik_bgs[$xx],
						 'jum_perbaikan'=>$stok_fisik_perbaikan[$xx]
					);
					$this->db->insert('tt_stok_opname_unit_jahit_detail_warna',$tt_stok_opname_unit_jahit_detail_warna);
			} // end for
	  }
	  else {
		  // ambil id detail id_stok_opname_hasil_jahit_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail 
						where kode_brg_jadi = '$kode_brg_jadi' 
						AND id_stok_opname_unit_jahit = '$id_stok' ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
		  
		  // 24-03-2014
		  $totalxx = $qtytotalstokfisikbgs+$qtytotalstokfisikperbaikan;
		  
		  $this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$totalxx',
						jum_bagus = '".$qtytotalstokfisikbgs."', jum_perbaikan = '".$qtytotalstokfisikperbaikan."'
						where id = '$iddetail' ");
			
			for ($xx=0; $xx<count($kode_warna); $xx++) {
				// 24-03-2014
				$totalxx = $stok_fisik_bgs[$xx]+$stok_fisik_perbaikan[$xx];
				
				$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail_warna SET jum_stok_opname = '".$totalxx."',
									jum_bagus = '".$stok_fisik_bgs[$xx]."', jum_perbaikan = '".$stok_fisik_perbaikan[$xx]."'
							WHERE id_stok_opname_unit_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ");
			}
			// ====================
	  }
  } 

}
