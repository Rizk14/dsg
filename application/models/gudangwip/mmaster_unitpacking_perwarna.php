<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so_unit_jahit($bulan, $tahun, $unit_jahit) {
	$query3	= $this->db->query(" SELECT id, tgl_so, jenis_perhitungan_stok, status_approve FROM tt_stok_opname_unit_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_unit = '$unit_jahit' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
			$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
			$jenis_perhitungan_stok = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so,
							   'jenis_perhitungan_stok'=> $jenis_perhitungan_stok
							);
							
		return $so_bahan;
  }

  function get_stok_unit_jahit($bulan, $tahun, $unit_jahit) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 05-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
	  		
		$sql = " SELECT a.id, a.id_brg_wip, b.kode_brg, b.nama_brg FROM tm_stok_unit_jahit a 
				INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id WHERE a.id_unit = '$unit_jahit' ORDER BY b.kode_brg ";
		
		$query	= $this->db->query($sql);
			
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {	
				// 30-10-2015 GA DIPAKE!
				// ---------- 05-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir, b.auto_saldo_akhir_bagus, b.auto_saldo_akhir_perbaikan
									FROM tt_stok_opname_unit_jahit a INNER JOIN tt_stok_opname_unit_jahit_detail b ON 
									a.id = b.id_stok_opname_unit_jahit
									WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$unit_jahit'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
					$saldo_awal_bagus = $hasilrow->auto_saldo_akhir_bagus;
					$saldo_awal_perbaikan = $hasilrow->auto_saldo_akhir_perbaikan;
				}
				else {
					$saldo_awal = 0;
					$saldo_awal_bagus = 0;
					$saldo_awal_perbaikan = 0;
				}
				
				// 2. hitung keluar bln ini
				// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus = 0;
				
				// 2.1.2 tm_sjmasukwip, jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perbaikan = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perbaikan = 0;
				
				// 2.2. dari tabel tm_sjmasukbhnbakupic
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$unit_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus+= $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus+= 0;
				
				// 3. hitung masuk bln ini
				// 3.1.1 dari tabel tm_sjkeluarwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_keluar<>'3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus = 0;
				
				// 3.1.2 dari tabel tm_sjkeluarwip. jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_keluar='3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_perbaikan = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_perbaikan = 0;
				
				// 3.2. dari tabel tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$unit_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus+= $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus+= 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal_bagus+$saldo_awal_perbaikan+$jum_masuk_bagus+$jum_masuk_perbaikan-$jum_keluar_bagus-$jum_keluar_perbaikan;
				$auto_saldo_akhir_bagus = $saldo_awal_bagus+$jum_masuk_bagus-$jum_keluar_bagus;
				$auto_saldo_akhir_perbaikan = $saldo_awal_perbaikan+$jum_masuk_perbaikan-$jum_keluar_perbaikan; */
				//-------------------------------------------------------------------------------------
				
				// 24-03-2014 
				// 1. ini ambil stok, stok_bagus, dan stok_perbaikan berdasarkan warna			
				$sqlxx = " SELECT c.stok, c.stok_bagus, c.stok_perbaikan, c.id_warna, b.nama as nama_warna 
							FROM tm_warna b INNER JOIN tm_stok_unit_jahit_warna c ON b.id = c.id_warna
							WHERE c.id_stok_unit_jahit = '$row->id' ";
				
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					
					foreach ($hasilxx as $rowxx) {
						// 30-10-2015 GA DIPAKE!
						// ---------- 05-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						/*$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir, c.auto_saldo_akhir_bagus, c.auto_saldo_akhir_perbaikan
											FROM tt_stok_opname_unit_jahit a 
											INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
											INNER JOIN tt_stok_opname_unit_jahit_detail_warna c ON b.id = c.id_stok_opname_unit_jahit_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
							$saldo_awal_bagus_warna = $hasilrow->auto_saldo_akhir_bagus;
							$saldo_awal_perbaikan_warna = $hasilrow->auto_saldo_akhir_perbaikan;
						}
						else {
							$saldo_awal_warna = 0;
							$saldo_awal_bagus_warna = 0;
							$saldo_awal_perbaikan_warna = 0;
						}
						
						// 2. hitung keluar bln ini
						// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna = 0;
						
						// 2.1.2 dari tabel tm_sjmasukwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_perbaikan_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_perbaikan_warna = 0;
						
						// 2.2. dari tabel tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna+= $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna+= 0;
						
						// 3. hitung masuk bln ini
						// 3.1.1 hitung dari tm_sjkeluarwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND a.jenis_keluar <>'3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna= 0;
						
						// 3.1.2 hitung dari tm_sjkeluarwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$unit_jahit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND a.jenis_keluar ='3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_perbaikan_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_perbaikan_warna= 0; // sampe sini 4:01 kamis 05-03-2015. lanjut besok. 07-03-2015 udh dilanjutkan
						
						// 3.2. dari tabel tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$unit_jahit' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna+= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna+= 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_bagus_warna+$saldo_awal_perbaikan_warna+$jum_masuk_bagus_warna+$jum_masuk_perbaikan_warna-$jum_keluar_bagus_warna-$jum_keluar_perbaikan_warna;
						$auto_saldo_akhir_bagus_warna = $saldo_awal_bagus_warna+$jum_masuk_bagus_warna-$jum_keluar_bagus_warna;
						$auto_saldo_akhir_perbaikan_warna = $saldo_awal_perbaikan_warna+$jum_masuk_perbaikan_warna-$jum_keluar_perbaikan_warna;
						
						//$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna; */
						//-------------------------------------------------------------------------------------
						
						$detail_warna[] = array(
										'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> $rowxx->stok,
										'stok_opname'=> $rowxx->stok,
										'stok_opname_bgs'=> $rowxx->stok_bagus,
										'stok_opname_perbaikan'=> $rowxx->stok_perbaikan
										);
					}
				}
				else {
					$sqlxx = " SELECT a.id_warna, b.nama as nama_warna FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip = '$row->id_brg_wip' ";
					
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						
						foreach ($hasilxx as $rowxx) {
							$detail_warna[] = array(
										'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0,
										'stok_opname_bgs'=> 0,
										'stok_opname_perbaikan'=> 0
									);
						}
					}
					else
						$detail_warna = '';
				}

				$detail_bahan[] = array('id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else
			$detail_bahan = '';
		
		return $detail_bahan;
  }
  
  function get_all_stok_opname_unit_jahit($bulan, $tahun, $unit_jahit) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.kode_brg, d.nama_brg FROM tt_stok_opname_unit_jahit_detail b 
					INNER JOIN tt_stok_opname_unit_jahit a ON b.id_stok_opname_unit_jahit = a.id 
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_unit = '$unit_jahit'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// 30-10-2015 GA DIPAKE!
				// ---------- 09-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir, b.auto_saldo_akhir_bagus, b.auto_saldo_akhir_perbaikan
									FROM tt_stok_opname_unit_jahit a INNER JOIN tt_stok_opname_unit_jahit_detail b ON 
									a.id = b.id_stok_opname_unit_jahit
									WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$unit_jahit'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
					$saldo_awal_bagus = $hasilrow->auto_saldo_akhir_bagus;
					$saldo_awal_perbaikan = $hasilrow->auto_saldo_akhir_perbaikan;
				}
				else {
					$saldo_awal = 0;
					$saldo_awal_bagus = 0;
					$saldo_awal_perbaikan = 0;
				}
				
				// 2. hitung keluar bln ini
				// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus = 0;
				
				// 2.1.2 tm_sjmasukwip, jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perbaikan = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perbaikan = 0;
				
				// 2.2. dari tabel tm_sjmasukbhnbakupic
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$unit_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus+= $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus+= 0;
				
				// 3. hitung masuk bln ini
				// 3.1.1 dari tabel tm_sjkeluarwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_keluar<>'3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus = 0;
				
				// 3.1.2 dari tabel tm_sjkeluarwip. jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$unit_jahit' AND a.jenis_keluar='3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_perbaikan = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_perbaikan = 0;
				
				// 3.2. dari tabel tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
							AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$unit_jahit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus+= $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus+= 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal_bagus+$saldo_awal_perbaikan+$jum_masuk_bagus+$jum_masuk_perbaikan-$jum_keluar_bagus-$jum_keluar_perbaikan;
				$auto_saldo_akhir_bagus = $saldo_awal_bagus+$jum_masuk_bagus-$jum_keluar_bagus;
				$auto_saldo_akhir_perbaikan = $saldo_awal_perbaikan+$jum_masuk_perbaikan-$jum_keluar_perbaikan; */
				//-------------------------------------------------------------------------------------
				
				// ambil stok terkini di tabel tm_stok_unit_jahit
				$query3	= $this->db->query(" SELECT stok
							FROM tm_stok_unit_jahit
							WHERE id_brg_wip = '$row->id_brg_wip' AND id_unit = '$unit_jahit' ");
							
				if ($query3->num_rows() > 0){
					$hasilrow3 = $query3->row();
					$stok	= $hasilrow3->stok;
				}
				else {
					$stok = '0';
				}
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'jum_stok_opname'=> $row->jum_stok_opname,
										'stok'=> $stok,
										'saldo_akhir'=> $row->saldo_akhir
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesounitjahit($is_new, $id_stok, $id_brg_wip, $stok, $stok_fisik, $saldo_akhir){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  	  
	  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetail	= $seqrow->id+1;
			}else{
				$iddetail	= 1;
			}
					  
		   $data_detail = array(
						'id'=>$iddetail,
						'id_stok_opname_unit_jahit'=>$id_stok,
						'id_brg_wip'=>$id_brg_wip, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik,
						//01-12-2015
						'saldo_akhir'=>$saldo_akhir
					);
		   $this->db->insert('tt_stok_opname_unit_jahit_detail',$data_detail);
	  }
	  else {
		  // ambil id detail id_stok_opname_hasil_jahit_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail 
						where id_brg_wip = '$id_brg_wip' 
						AND id_stok_opname_unit_jahit = '$id_stok' ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
		  
			$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$stok_fisik',
						saldo_akhir = '$saldo_akhir'
						where id = '$iddetail' ");
	  }
  }
  
  // 30-10-2015
  function cek_sokosong($unit_jahit) {
	$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit WHERE id_unit = '$unit_jahit' ");
	if ($query3->num_rows() > 0){
		return 'f';
	}
	else {
		return 't';
	}
  }
  
  // 31-10-2015
  function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so_unit_packing($bulan, $tahun, $unit_packing) {
	$query3	= $this->db->query(" SELECT id, tgl_so, jenis_perhitungan_stok, status_approve FROM tt_stok_opname_unit_packing
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_unit = '$unit_packing' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
			$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
			$jenis_perhitungan_stok = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so,
							   'jenis_perhitungan_stok'=> $jenis_perhitungan_stok
							);
							
		return $so_bahan;
  }
  
  function cek_sokosong_unitpacking($unit_packing) {
	$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE id_unit='$unit_packing' ");
	if ($query3->num_rows() > 0){
		return 'f';
	}
	else {
		return 't';
	}
  }
  
  function get_stok_unit_packing($bulan, $tahun, $unit_packing) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
	  		
		$sql = " SELECT a.id, a.id_brg_wip, b.kode_brg, b.nama_brg FROM tm_stok_unit_packing a 
				INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id WHERE a.id_unit = '$unit_packing' ORDER BY b.kode_brg ";
		
		$query	= $this->db->query($sql);
			
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {	
				// 1. ini ambil stok berdasarkan warna			
				$sqlxx = " SELECT c.stok, c.id_warna, b.nama as nama_warna 
							FROM tm_warna b INNER JOIN tm_stok_unit_packing_warna c ON b.id = c.id_warna
							WHERE c.id_stok_unit_packing = '$row->id' ";
				
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					
					foreach ($hasilxx as $rowxx) {
						$detail_warna[] = array(
										'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> $rowxx->stok,
										'stok_opname'=> $rowxx->stok
										);
					}
				}
				else {
					$sqlxx = " SELECT a.id_warna, b.nama as nama_warna FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip = '$row->id_brg_wip' ";
					
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						
						foreach ($hasilxx as $rowxx) {
							$detail_warna[] = array(
										'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0
									);
						}
					}
					else
						$detail_warna = '';
				}

				$detail_bahan[] = array('id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else
			$detail_bahan = '';
		
		return $detail_bahan;
  }
  
  function get_all_stok_opname_unit_packing($bulan, $tahun, $unit_packing) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.kode_brg, d.nama_brg FROM tt_stok_opname_unit_packing_detail b 
					INNER JOIN tt_stok_opname_unit_packing a ON b.id_stok_opname_unit_packing = a.id 
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_unit = '$unit_packing'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// 05-11-2015
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_unit_packing_detail_warna a 
							INNER JOIN tm_warna c ON c.id = a.id_warna  
							WHERE a.id_stok_opname_unit_packing_detail = '$row->id'
							ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_packing_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_unit_packing a INNER JOIN tm_stok_unit_packing_warna b ON a.id = b.id_stok_unit_packing
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_unit = '$unit_packing'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									'stok'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'saldo_akhir'=> $rowxx->saldo_akhir
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array( 'id_header'=> $row->id_header,
										'id'=> $row->id,
										'kode_brg_wip'=> $row->kode_brg,
										'id_brg_wip'=> $row->id_brg_wip,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
				
				// ambil stok terkini di tabel tm_stok_unit_packing
				/*$query3	= $this->db->query(" SELECT stok
							FROM tm_stok_unit_packing
							WHERE id_brg_wip = '$row->id_brg_wip' AND id_unit = '$unit_packing' ");
							
				if ($query3->num_rows() > 0){
					$hasilrow3 = $query3->row();
					$stok	= $hasilrow3->stok;
				}
				else {
					$stok = '0';
				}
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'jum_stok_opname'=> $row->jum_stok_opname,
										'stok'=> $stok
									); */
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesounitpacking($is_new, $id_stok, $is_pertamakali, $id_brg_wip, $id_warna, $stok, $stok_fisik,
				$id_warna2, $saldo_akhir){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  //-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		$qtytotalsaldoakhir = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			
			//01-12-2015
			$id_warna2[$xx] = trim($id_warna2[$xx]);
			$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);
			
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
			$qtytotalsaldoakhir+= $saldo_akhir[$xx];
		} // end for
	// ---------------------------------------------------------------------
	  	  
	  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetail	= $seqrow->id+1;
			}else{
				$iddetail	= 1;
			}
					  
		   $data_detail = array(
						'id'=>$iddetail,
						'id_stok_opname_unit_packing'=>$id_stok,
						'id_brg_wip'=>$id_brg_wip, 
						'stok_awal'=>$qtytotalstokawal,
						'jum_stok_opname'=>$qtytotalstokfisik,
						'saldo_akhir'=>$qtytotalsaldoakhir
					);
		   $this->db->insert('tt_stok_opname_unit_packing_detail',$data_detail);
		   
		   // ambil id detail id_stok_opname_hasil_jahit_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
			
			// ----------------------------------------------
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok[$xx] = trim($stok[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);
				//01-12-2015
				$id_warna2[$xx] = trim($id_warna2[$xx]);
				$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);
				
				$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
				if($seq_warna->num_rows() > 0) {
					$seqrow	= $seq_warna->row();
					$idbaru	= $seqrow->id+1;
				}else{
					$idbaru	= 1;
				}
				
				$tt_stok_opname_unit_packing_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_stok_opname_unit_packing_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'jum_stok_opname'=>$stok_fisik[$xx],
						 'saldo_akhir'=>$saldo_akhir[$xx]
					);
					$this->db->insert('tt_stok_opname_unit_packing_detail_warna',$tt_stok_opname_unit_packing_detail_warna);
			} // end for
	  }
	  else {
		  // ambil id detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail 
						where id_brg_wip = '$id_brg_wip' 
						AND id_stok_opname_unit_packing = '$id_stok' ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
		  
			$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET jum_stok_opname = '$qtytotalstokfisik',
						saldo_akhir = '$qtytotalsaldoakhir' where id = '$iddetail' ");
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."',
							saldo_akhir = '".$saldo_akhir[$xx]."'
							WHERE id_stok_opname_unit_packing_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
			}
			// ====================
	  }
  }

}
