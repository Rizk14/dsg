<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function getAllgradeb($num, $offset, $cari)
  {
	$filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	  $this->db->select(" a.id as idharga,a.tgl_update,a.harga,c.id as id_brg_wip,c.kode_brg as kode_brg_wip ,
		c.nama_brg ,a.bulan ,a.tahun FROM tm_gradeb a, tm_barang_wip c 
			WHERE a.id_barang_wip  = c.id ".$filter." ORDER BY c.kode_brg ", false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();
  }
  
  function deletegradeb($id){    
     $this->db->delete('tm_gradeb', array('id' => $id));
  }
  function getAllgradebtanpalimit($cari){
	  $filter = "";
	  if ($cari != "all")
		$filter.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	  	  
	$this->db->select(" a.id as idharga,a.tgl_update,a.harga,c.id as id_brg_wip,c.kode_brg FROM tm_gradeb a, tm_barang_wip c 
			WHERE a.id_barang_wip  = c.id ".$filter, false);
			
    $query = $this->db->get();
    return $query->result();  
  }
  
  function cek_data_gradeb($kode_brg_wip){
    $this->db->select("id from tm_gradeb WHERE kode_brg_wip = '$kode_brg_wip' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function savegradeb($kode_brg_wip, $harga, $id_brg_wip,$bulan,$tahun){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
    
    if ($harga != 0) {
		$data = array(
		  'kode_brg_wip'=>$kode_brg_wip,
		  'id_barang_wip'=>$id_brg_wip,
		  'bulan'=>$bulan,
		  'tahun'=>$tahun,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_gradeb',$data); 
	}
  }
  function get_gradeb() {
	$sql = " SELECT b.id as idharga ,a.id as id_barang_wip ,a.kode_brg, a.nama_brg as nama_brg_jadi, b.harga 
			FROM tm_barang_wip a, tm_gradeb b WHERE a.id=b.id_barang_wip 
			ORDER BY a.kode_brg ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$data_harga[] = array(	
								'id_barang_wip'=> $row1->id_barang_wip,
								'idharga'=> $row1->idharga,
								'kode_brg_jadi'=> $row1->kode_brg,
								'nama_brg_jadi'=> $row1->nama_brg_jadi,
								'harga'=> $row1->harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
}
