<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_satuan ORDER BY e_satuan ASC  ", false);
	}

	function viewperpages($limit,$offset) {	
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_satuan ORDER BY e_satuan DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($esatuan) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qijenisvoucher = $db2->query(" SELECT i_satuan+1 AS isatuan FROM tr_satuan ORDER BY i_satuan DESC LIMIT 1 ");
		$row_ijenisvoucher = $qijenisvoucher->row();
		
		$str = array(
			'i_satuan'=>$row_ijenisvoucher->isatuan,
			'e_satuan'=>$esatuan,
			'd_entry'=>$dentry
		);
		
		$db2->insert('tr_satuan',$str);
		redirect('satuanbhnbaku/cform/');
    }
	
	function mupdate($isatuan,$esatuan) {
		$db2=$this->load->database('db_external', TRUE);
		$class_item	= array(
			'e_satuan'=>$esatuan
		);
		$db2->update('tr_satuan',$class_item,array('i_satuan'=>$isatuan));
		redirect('satuanbhnbaku/cform');
	}
	
	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_satuan WHERE i_satuan='$id' ");
	}
	
	function viewcari($esatuan) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_satuan WHERE e_satuan LIKE '$esatuan%' ");
	}
	
	function mcari($esatuan,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_satuan WHERE e_satuan LIKE '$esatuan%' LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function delete($id) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_satuan',array('i_satuan'=>$id));
		redirect('satuanbhnbaku/cform/');	 
	}
}

?>
