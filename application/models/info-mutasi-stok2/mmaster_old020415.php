<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_mutasi_stok($date_from, $date_to, $gudang) {		
	  		
		$sql = "SELECT b.kode_brg, b.nama_brg, c.id as id_satuan, c.nama as satuan 
					FROM tm_barang b, tm_satuan c 
					WHERE b.satuan = c.id AND b.id_gudang = '$gudang' AND b.status_aktif = 't' ";
		if ($gudang == '12') // gudang bhn baku, berarti gabungkan dgn bhn2 quilting
			$sql.= " UNION select b.kode_brg, b.nama_brg, c.id as id_satuan, c.nama as satuan 
					FROM tm_brg_hasil_makloon b, tm_satuan c
					WHERE b.satuan = c.id AND b.status_aktif = 't' ";
		$sql.= " ORDER BY nama_brg";
	  
		$query	= $this->db->query($sql);
		
		$jum_keluar = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {	
				// 12-07-2013, ambil data saldo awal dari SO bulan sebelumnya (ambil bulan dan tahun dari date_from)
				// cek apakah termasuk quilting atau bukan (utk menentukan query di tabel SO)
				$queryx	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg = '$row1->kode_brg'
										AND status_aktif = 't' ");
				if ($queryx->num_rows() > 0){
					$is_quilting = 'f';
				}
				else
					$is_quilting = 't';
				
				if ($is_quilting == 'f') {
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a, 
									tt_stok_opname_bahan_baku_detail b
									WHERE a.id = b.id_stok_opname_bahan_baku
									AND b.kode_brg = '$row1->kode_brg' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				}
				else {
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_quilting a, 
									tt_stok_opname_hasil_quilting_detail b
									WHERE a.id = b.id_stok_opname_hasil_quilting
									AND b.kode_brg = '$row1->kode_brg' 
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				}
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
				}
				else
					$saldo_awal = 0;
				
				// cek apakah brg quilting atau bukan
				$query3	= $this->db->query(" SELECT * FROM tm_barang where kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$quilting = 'f';
					
					//1. hitung brg masuk dari tm_apply_stok_pembelian
					/*if ($row1->kode_brg == 'BB030000030031')
						echo " SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a, 
							tm_apply_stok_pembelian_detail b
							WHERE a.id = b.id_apply_stok AND b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$gudang' "; */
					
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a, 
							tm_apply_stok_pembelian_detail b
							WHERE a.id = b.id_apply_stok AND b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk = $hasilrow->jum_masuk;
					}
					else
						$jum_masuk = 0;
					
					// --------- 25-09-2013 ----------------------
					// konversi ke satuan baru (dari yard/lusin) ke meter atau pcs
					 if ($row1->id_satuan == 2) { // yard
						$jum_masuk = $jum_masuk*0.91; // ini konversi dari yard ke meter
					 }
					 else if ($row1->id_satuan == 7) { // lusin
						$jum_masuk = $jum_masuk*12; // ini konversi dari lusin ke pcs
					 }
					 
					// -------------------------------------------
					
					// 2. hitung retur sebagai keluar lain (28-01-2013: no longer f***in' used anymore)
				/*	$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar_lain FROM tm_retur_beli a, tm_retur_beli_detail b
						WHERE a.id = b.id_retur_beli AND a.tgl_retur >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_retur <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_keluar_lain = $hasilrow->jum_keluar_lain;
					} */
				}
				else {
					$quilting = 't';
					
					//1. hitung brg masuk dari tm_sj_hasil_makloon
					$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b
							WHERE a.id = b.id_sj_hasil_makloon AND b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_makloon = '$row1->kode_brg'
							AND b.status_stok = 't' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk = $hasilrow->jum_masuk;
					}
					else
						$jum_masuk = 0;
				}
				
				// 2. hitung brg keluar bagus dari tm_bonmkeluar
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a, 
							tm_bonmkeluar_detail b
							WHERE a.id = b.id_bonmkeluar AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND a.id_gudang = '$gudang' AND b.is_quilting = '$quilting' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 3. hitung brg keluar lain dari tm_bonmkeluar
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a, 
							tm_bonmkeluar_detail b
							WHERE a.id = b.id_bonmkeluar AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND a.id_gudang = '$gudang' AND b.is_quilting = '$quilting' AND a.tujuan > '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
				
				// 4. hitung brg masuk lain dari tm_bonmmasuklain
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a, 
							tm_bonmmasuklain_detail b
							WHERE a.id = b.id_bonmmasuklain AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND a.id_gudang = '$gudang' AND b.is_quilting = '$quilting' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;
				
				//==================================== end new 28-01-2013 ==========================================
				
				// 12-07-2013 --------------------------
				if ($is_quilting == 'f') {
					$sql = "SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a, 
							tt_stok_opname_bahan_baku_detail b
							WHERE a.id = b.id_stok_opname_bahan_baku 
							AND b.kode_brg = '$row1->kode_brg' 
							AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
					//if ($row1->kode_brg == 'PKS010000370001') { echo $sql; die(); }
					
				}
				else
					$sql = "SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_quilting a, 
								tt_stok_opname_hasil_quilting_detail b
								WHERE a.id = b.id_stok_opname_hasil_quilting 
								AND b.kode_brg = '$row1->kode_brg' 
								AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
												
				$query3	= $this->db->query($sql);
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					$jum_stok_opname = round($jum_stok_opname, 2);
					
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					/*if ($id_satuan == '2' && $row1->quilting == 'f') {
						$jum_stok_opname = $jum_stok_opname * 0.91;
						$jum_stok_opname = round($jum_stok_opname, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$jum_stok_opname = $jum_stok_opname*12;
						$jum_stok_opname = round($jum_stok_opname, 2);
					} */

				}
				else {
					$jum_stok_opname = 0;
					$jum_stok_opname = round($jum_stok_opname, 2);
				}
				//--------------------------------------
				
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $row1->nama_brg,
											'id_satuan'=> $row1->id_satuan,
											'satuan'=> $row1->satuan,
											'saldo_awal'=> $saldo_awal,
											'jum_masuk'=> $jum_masuk,
											'jum_masuk_lain'=> $jum_masuk_lain,
											'jum_keluar'=> $jum_keluar,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'jum_stok_opname'=> $jum_stok_opname
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 14-03-2013, mutasi stok WIP
  function get_mutasi_stok_wip($date_from, $date_to, $gudang) {		
	  
	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
	  
		/*$sql = "SELECT b.i_product_motif, b.e_product_motifname FROM tr_product_motif b
				INNER JOIN tr_product_base a ON a.i_product_base = b.i_product
				WHERE b.n_active = '1'
				ORDER BY a.f_stop_produksi ASC, b.i_product_motif "; */
		$sql = "SELECT a.kode_brg_jadi FROM (
				select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi FROM tt_stok_opname_hasil_jahit a, tt_stok_opname_hasil_jahit_detail b
					WHERE a.id=b.id_stok_opname_hasil_jahit AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					AND a.status_approve='t' GROUP BY b.kode_brg_jadi
				)
				a GROUP BY a.kode_brg_jadi"; //echo $sql; die();
		
		$query	= $this->db->query($sql);
		
		$jum_keluar1 = 0;
		$jum_keluar2 = 0;
		$jum_keluar_lain1 = 0;
		$jum_keluar_lain2 = 0;
		$jum_masuk = 0;
		$jum_masuk_lain1 = 0;
		$jum_masuk_lain2 = 0;
		$jum_masuk_lain3 = 0;
		$jum_masuk_lain4 = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row1->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
					
				// 13-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_jahit a, 
									tt_stok_opname_hasil_jahit_detail b
									WHERE a.id = b.id_stok_opname_hasil_jahit
									AND b.kode_brg_jadi = '$row1->kode_brg_jadi' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
				}
				else
					$saldo_awal = 0;
				
				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain1 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain1 = 0;
				
				//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain2 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain2 = 0;
				
				//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain3 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain3 = 0;
				
				//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '5' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain4 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain4 = 0;
				
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar1 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar1 = 0;
				
				// hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar2 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar2 = 0;
				
				// hitung brg keluar lain jenis=3 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain1 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain1 = 0;
				
				// hitung brg keluar lain jenis=4 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain2 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain2 = 0;
				
				// 13-09-2013 ================================
				$sql = "SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_jahit a, 
							tt_stok_opname_hasil_jahit_detail b
							WHERE a.id = b.id_stok_opname_hasil_jahit 
							AND b.kode_brg_jadi = '$row1->kode_brg_jadi' AND a.id_gudang = '$gudang'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
												
				$query3	= $this->db->query($sql);
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
				}
				else {
					$jum_stok_opname = 0;
				}
				//=========================================
																
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg_jadi,
											'nama_brg'=> $e_product_motifname,
											'saldo_awal'=> $saldo_awal,
											'jum_masuk'=> $jum_masuk,
											'jum_masuk_lain1'=> $jum_masuk_lain1,
											'jum_masuk_lain2'=> $jum_masuk_lain2,
											'jum_masuk_lain3'=> $jum_masuk_lain3,
											'jum_masuk_lain4'=> $jum_masuk_lain4,
											'jum_keluar1'=> $jum_keluar1,
											'jum_keluar2'=> $jum_keluar2,
											'jum_keluar_lain1'=> $jum_keluar_lain1,
											'jum_keluar_lain2'=> $jum_keluar_lain2,
											'jum_stok_opname'=> $jum_stok_opname
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  // ======================= 26-09-2013 =======================================
  function get_mutasi_stok_hasilcutting($date_from, $date_to) {		
	  		
		$sql = "SELECT * FROM tm_stok_hasil_cutting ORDER BY kode_brg_jadi, kode_brg ";	  
		$query	= $this->db->query($sql);
		
		$jum_keluar = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$jum_masuk_lain1 = 0;
		$jum_masuk_lain2 = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {	
				// 26-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_cutting a, 
									tt_stok_opname_hasil_cutting_detail b
									WHERE a.id = b.id_stok_opname_hasil_cutting
									AND b.kode_brg_jadi = '$row1->kode_brg_jadi' AND b.kode_brg = '$row1->kode_brg'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
				}
				else
					$saldo_awal = 0;
				
				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND b.kode_brg = '$row1->kode_brg' AND a.jenis_masuk = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//2. hitung brg masuk perbaikan (jenis = 2) dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND b.kode_brg = '$row1->kode_brg' AND a.jenis_masuk = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain1 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain1 = 0;
				
				//3. hitung brg masuk lain jenis = 3 dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND b.kode_brg = '$row1->kode_brg' AND a.jenis_masuk = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain2 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain2 = 0;
												
				// 4. hitung brg keluar bagus jenis=1 dari tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND b.kode_brg = '$row1->kode_brg' AND a.jenis_keluar = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
								
				// hitung brg keluar lain jenis=2 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND b.kode_brg = '$row1->kode_brg' AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
								
				// 26-09-2013 ================================
				$sql = "SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_cutting a, 
							tt_stok_opname_hasil_cutting_detail b
							WHERE a.id = b.id_stok_opname_hasil_cutting 
							AND b.kode_brg_jadi = '$row1->kode_brg_jadi' 
							AND b.kode_brg = '$row1->kode_brg' 
							AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
												
				$query3	= $this->db->query($sql);
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
				}
				else {
					$jum_stok_opname = 0;
				}
				//=========================================
				
				// bhn baku
				$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
				}
				else {
					$query3	= $this->db->query(" SELECT nama_brg FROM tm_brg_hasil_makloon WHERE kode_brg = '$row1->kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
					}
					else
						$nama_brg = '';
				}
				
				// brg jadi
				$sql3 = "SELECT e_product_motifname FROM tr_product_motif
						WHERE i_product_motif = '$row1->kode_brg_jadi' ";	  
				$query3	= $this->db->query($sql3);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
				}
				else
					$nama_brg_jadi = '';
																
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi,
											'saldo_awal'=> $saldo_awal,
											'jum_masuk'=> $jum_masuk,
											'jum_masuk_lain1'=> $jum_masuk_lain1,
											'jum_masuk_lain2'=> $jum_masuk_lain2,
											'jum_keluar'=> $jum_keluar,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'jum_stok_opname'=> $jum_stok_opname
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  //===========================================================================
  
  // 04-06-2014
  function get_sobhnbaku($num, $offset, $id_gudang, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_bahan_baku WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_gudang != '0')
		$sql.= " AND id_gudang = '$id_gudang' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di gudang ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di gudang ini ?";
				$data_so[] = array(			'id'=> $row1->id,	
											'bulan'=> $row1->bulan,	
											'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'id_gudang'=> $id_gudang,	
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sobhnbakutanpalimit($id_gudang, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_bahan_baku WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_gudang != '0')
		$sql.= " AND id_gudang = '$id_gudang' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function get_detail_sobhnbaku($id_so) {
		$query	= $this->db->query(" SELECT a.id as id_header, a.id_gudang, a.bulan, a.tahun, b.*, 
					d.nama_brg, d.satuan
					FROM tt_stok_opname_bahan_baku_detail b, 
					tt_stok_opname_bahan_baku a, tm_barang d
					WHERE b.id_stok_opname_bahan_baku = a.id 
					AND b.kode_brg = d.kode_brg
					AND a.id = '$id_so' 
					ORDER BY d.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'kode_gudang'=> $kode_gudang,
										'nama_gudang'=> $nama_gudang,
										'nama_lokasi'=> $nama_lokasi,
										'id_gudang'=> $row->id_gudang,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'nama_bln'=> $nama_bln,
										'id'=> $row->id,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $nama_satuan,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesobhnbaku($id_so, $kode_brg, $stok_fisik){ 
	  //$tgl = date("Y-m-d H:i:s"); 
	  $tgl = date("Y-m-d"); 
	  			
		// ambil id detail id_stok_opname_bahan_baku_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail 
					where kode_brg = '$kode_brg' 
					AND id_stok_opname_bahan_baku = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
		  
		 $this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET jum_stok_opname = '$stok_fisik',
					status_approve='f' WHERE id = '$iddetail' ");			
		// ====================  
  }
      
}

