<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
    function get_jenis_barang(){
	$query	= $this->db->query("select * from tm_jenis_barang ORDER BY kode ");    
    return $query->result();  
  }
   function get_kelompok_barang(){
	$query	= $this->db->query("select * from tm_kelompok_barang ORDER BY kode ");    
    return $query->result();  
  }
  
 
function get_mutasi_stok_gudang($date_from, $date_to, $gudang,$kel_brg,$jns_brg) {
	//ganti database salah narik data

			$dfromback = date('d-m-Y',strtotime($date_from . "first day of previous month"));
			$dtoback = date('d-m-Y',strtotime($date_from . "last day of previous month"));
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$pisah2 = explode("-", $dfromback);
			$tglback= $pisah2[0];
			$blnback= $pisah2[1];
			$thnback= $pisah2[2];

					$query = $this->db->query(" SELECT
					a.id_brg,
					b.kode_brg,
					b.nama_brg,
					c.nama as satuan,
					CASE WHEN d.nama IS NULL THEN 'Tidak Ada' ELSE d.nama END as satuan_konversi,
					SUM(a.saldo_awal) as saldo_awal, 
					SUM(a.jum_masuk) as jum_masuk, 
					SUM(a.jum_masuklain) as jum_masuklain, 
					SUM(a.jum_keluar) as jum_keluar, 
					SUM(a.jum_keluarlain) as jum_keluarlain,
					SUM(a.saldo_awal+a.jum_masuk+a.jum_masuklain-a.jum_keluar-a.jum_keluarlain) as saldo_akhir,
					SUM(a.so) as so,
					SUM((a.so)-(a.saldo_awal+a.jum_masuk+a.jum_masuklain-a.jum_keluar-a.jum_keluarlain)) as selisih 
					FROM (
					--SALDO AWAL
					SELECT b.id_brg, b.jum_stok_opname AS saldo_awal, 0 as jum_masuk, 0 as jum_masuklain, 0 as jum_keluar, 0 as jum_keluarlain, 0 as so    
					FROM tt_stok_opname_bahan_baku a 
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_gudang d ON d.id = e.id_gudang 
					WHERE 
					--b.id_brg = '646' AND 
					d.id = '$gudang' 
					AND a.bulan = '$blnback' 
					AND a.tahun = '$thnback' 
					AND b.status_approve = 't' 
					AND a.status_approve = 't'

					UNION ALL

					--Ambil brg masuk hasil pembelian
					SELECT b.id_brg, 0 AS saldo_awal, sum(b.qty) as jum_masuk, 0 as jum_masuklain, 0 as jum_keluar, 0 as jum_keluarlain, 0 as so 
					FROM tm_apply_stok_pembelian a 
					INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_gudang d ON d.id = e.id_gudang 
					WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
					AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') 
					--AND b.id_brg = '646' 
					AND b.status_stok = 't' 
					AND a.status_aktif='t' 
					AND e.id_gudang = '$gudang'
					group by b.id_brg

					UNION ALL

					--Barang masuk pembelian MANUAL
					SELECT b.id_brg, 0 AS saldo_awal, 0 as jum_masuk, sum(b.qty) as jum_masuklain, 0 as jum_keluar, 0 as jum_keluarlain, 0 as so 
					FROM tm_bonmmasukmanual a 
					INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_gudang d ON d.id = e.id_gudang 
					WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') 
					--AND b.id_brg = '646' 
					AND d.id = '$gudang'
					group by b.id_brg

					UNION ALL

					--hitung brg keluar bagus dari tm_bonmkeluar
					SELECT b.id_brg, 0 AS saldo_awal, 0 as jum_masuk, 0 as jum_masuklain, sum(b.qty) as jum_keluar, 0 as jum_keluarlain, 0 as so  
					FROM tm_bonmkeluar a 
					INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_gudang d ON d.id = e.id_gudang 
					WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') 
					--AND b.id_brg = '646' 
					AND d.id = '$gudang'
					AND d.id != '6' 
					AND b.is_quilting = 'f' 
					AND ((a.tujuan < '3' OR a.tujuan > '5') 
					AND a.keterangan <> 'DIBEBANKAN')
					group by b.id_brg

					UNION ALL
					--hitung brg keluar lain dari tm_bonmkeluar
					SELECT b.id_brg, 0 AS saldo_awal, 0 as jum_masuk, 0 as jum_masuklain, 0 as jum_keluar, sum(b.qty) as jum_keluarlain, 0 as so 
					FROM tm_bonmkeluar a 
					INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_gudang d ON d.id = e.id_gudang 
					WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') 
					--AND b.id_brg = '646' 
					AND d.id = '$gudang' 
					AND b.is_quilting = 'f' 
					AND ((a.tujuan >= '3' 
					AND a.tujuan <='5') OR a.keterangan='DIBEBANKAN')
					group by b.id_brg

					UNION ALL

					SELECT b.id_brg, 0 AS saldo_awal, 0 as jum_masuk, 0 as jum_masuklain, 0 as jum_keluar, 0 as jum_keluarlain, b.jum_stok_opname as so 
					FROM tt_stok_opname_bahan_baku a 
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_gudang d ON d.id = e.id_gudang 
					WHERE 
					--b.id_brg = '646' AND 
					d.id = '$gudang' 
					AND a.bulan = '$bln1' 
					AND a.tahun = '$thn1' 
					AND b.status_approve = 't' 
					AND a.status_approve = 't'
					) a
					INNER JOIN tm_barang b ON a.id_brg=b.id 
					INNER JOIN tm_satuan c ON b.satuan=c.id
					LEFT JOIN tm_satuan d ON b.id_satuan_konversi=d.id
					GROUP BY a.id_brg, b.kode_brg, b.nama_brg, c.nama, d.nama
					ORDER BY b.kode_brg");

		if($query->num_rows()>0) {
			return $query->result();
			}
		
	}


  
  function get_gudang(){
	if($this->session->userdata('gid') == 16){
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
	    }else{
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
					                        }
	    if ($query->num_rows() > 0){
			return $query->result();
	}
  }

    function get_gudang_qc(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
						WHERE a.kel_gudang = '2' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }


  function get_mutasi_stok_wip($date_from, $date_to, $gudang) {	

	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	// explode date_from utk keperluan saldo awal 
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
			
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}


		$query = $this->db->query(" 

SELECT a.id_gudang, 
a.id_brg_wip, 
b.kode_brg, 
b.nama_brg, 
sum(a.saldo_awal) AS saldo_awal, 
sum(a.masuk_bgs) AS masuk_bgs, 
sum(a.masuk_prbaikn) AS masuk_prbaikn, 
sum(a.masuk_rtrpacking) AS masuk_rtrpacking, 
sum(a.masuk_rtrgdjdi) AS masuk_rtrgdjdi, 
sum(a.masuk_lain) AS masuk_lain, 
sum(a.masuk_lainother) AS masuk_lainother, 
sum(a.tot_masuk) AS tot_masuk, 
sum(a.masuk_bgsqc) AS masuk_bgsqc, 
sum(a.masuk_rtrqc) AS masuk_rtrqc, 
sum(a.keluar_bgspcking) AS keluar_bgspcking, 
sum(a.keluar_gdjdi) AS keluar_gdjdi, 
sum(a.keluar_rtrprbaikan) AS keluar_rtrprbaikan, 
sum(a.keluar_lain) AS keluar_lain, 
sum(a.keluar_other) AS keluar_other, 
sum(a.keluar_bgsqc) AS keluar_bgsqc, 
sum(a.keluar_rtrqc) AS keluar_rtrqc, 
sum(a.tot_keluar) AS tot_keluar, 
sum(a.saldo_akhir) AS saldo_akhir, 
sum(a.stokopname) as stokopname, 
sum(a.stokopname-a.saldo_akhir) AS selisih FROM ( 
SELECT id_gudang, 
id_brg_wip, 
id_warna, 
saldo_awal, 
masuk_bgs, 
masuk_prbaikn, 
masuk_rtrpacking, 
masuk_rtrgdjdi, 
masuk_lain, masuk_lainother, tot_masuk, masuk_bgsqc, masuk_rtrqc, keluar_bgspcking, keluar_gdjdi, keluar_rtrprbaikan, 
keluar_lain, keluar_other, keluar_bgsqc, keluar_rtrqc, tot_keluar, saldo_akhir, 0 AS stokopname 
FROM f_mutasi_qc('$bln_query', '$thn_query', '$tgldari', '$tglke')
 
UNION ALL 

SELECT id_gudang, 
id_brg_wip, 
id_warna, 
0 AS saldo_awal, 
0 AS masuk_bgs, 
0 AS masuk_prbaikn, 
0 AS masuk_rtrpacking, 
0 AS masuk_rtrgdjdi, 
0 AS masuk_lain, 
0 AS masuk_lainother, 
0 AS tot_masuk, 
0 AS masuk_bgsqc, 
0 AS masuk_rtrqc, 
0 AS keluar_bgspcking, 
0 AS keluar_gdjdi, 
0 AS keluar_rtrprbaikan, 
0 AS keluar_lain, 
0 AS keluar_other, 
0 AS keluar_bgsqc, 
0 AS keluar_rtrqc, 
0 AS tot_keluar, 
0 AS saldo_akhir, 
saldo_akhir as stokopname 
FROM f_stokopname_qc('$bln1', '$thn1', '$tglke') 

) a 
INNER JOIN tm_barang_wip b ON a.id_brg_wip=id 
INNER JOIN tm_warna c ON a.id_warna=c.id 
WHERE id_gudang='7' 
group by id_gudang, a.id_brg_wip, b.kode_brg, b.nama_brg
order by b.kode_brg


");

		if($query->num_rows()>0) {
			return $query->result();
			}


   }
  
 

 function get_mutasi_stok_wip_dg_w($date_from, $date_to, $gudang) {	

	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	// explode date_from utk keperluan saldo awal 
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
			
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}


		$query = $this->db->query(" 

SELECT 
a.id_gudang, 
a.id_brg_wip,
b.kode_brg,
b.nama_brg,
a.id_warna,
c.nama as nama_warna,  
sum(a.saldo_awal) AS saldo_awal, 
sum(a.masuk_bgs) AS masuk_bgs, 
sum(a.masuk_prbaikn) AS masuk_prbaikn, 
sum(a.masuk_rtrpacking) AS masuk_rtrpacking,
sum(a.masuk_rtrgdjdi) AS masuk_rtrgdjdi,
sum(a.masuk_lain) AS masuk_lain, 
sum(a.masuk_lainother) AS masuk_lainother,
sum(a.tot_masuk) AS tot_masuk,
sum(a.masuk_bgsqc) AS masuk_bgsqc,
sum(a.masuk_rtrqc) AS masuk_rtrqc,
sum(a.keluar_bgspcking) AS keluar_bgspcking,
sum(a.keluar_gdjdi) AS keluar_gdjdi,
sum(a.keluar_rtrprbaikan) AS keluar_rtrprbaikan,
sum(a.keluar_lain) AS keluar_lain,
sum(a.keluar_other) AS keluar_other,
sum(a.keluar_bgsqc) AS keluar_bgsqc,
sum(a.keluar_rtrqc) AS keluar_rtrqc,
sum(a.tot_keluar) AS tot_keluar, 
sum(a.saldo_akhir) AS saldo_akhir,
sum(a.stokopname) as stokopname,
sum(a.stokopname-a.saldo_akhir) AS selisih
FROM (
SELECT
id_gudang,  
id_brg_wip,
id_warna, 
saldo_awal, 
masuk_bgs, 
masuk_prbaikn, 
masuk_rtrpacking,
masuk_rtrgdjdi,
masuk_lain, 
masuk_lainother,
tot_masuk,
masuk_bgsqc,
masuk_rtrqc,
keluar_bgspcking,
keluar_gdjdi,
keluar_rtrprbaikan,
keluar_lain,
keluar_other,
keluar_bgsqc,
keluar_rtrqc,
tot_keluar, 
saldo_akhir,
0 AS stokopname 
FROM f_mutasi_qc('$bln_query', '$thn_query', '$tgldari', '$tglke')

UNION ALL

SELECT
id_gudang,   
id_brg_wip,
id_warna, 
0 AS saldo_awal, 
0 AS masuk_bgs, 
0 AS masuk_prbaikn, 
0 AS masuk_rtrpacking,
0 AS masuk_rtrgdjdi,
0 AS masuk_lain, 
0 AS masuk_lainother,
0 AS tot_masuk,
0 AS masuk_bgsqc,
0 AS masuk_rtrqc,
0 AS keluar_bgspcking,
0 AS keluar_gdjdi,
0 AS keluar_rtrprbaikan,
0 AS keluar_lain,
0 AS keluar_other,
0 AS keluar_bgsqc,
0 AS keluar_rtrqc,
0 AS tot_keluar, 
0 AS saldo_akhir,
saldo_akhir as stokopname 
FROM f_stokopname_qc('$bln1', '$thn1', '$tglke')
) a
INNER JOIN tm_barang_wip b ON a.id_brg_wip=id
INNER JOIN tm_warna c ON a.id_warna=c.id
WHERE id_gudang='$gudang'
group by id_gudang,  a.id_brg_wip, b.kode_brg, b.nama_brg, a.id_warna, c.nama
order by b.kode_brg, a.id_warna


");

		if($query->num_rows()>0) {
			return $query->result();
			}


   }

      
}

