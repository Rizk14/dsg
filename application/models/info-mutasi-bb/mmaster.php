<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_mutasi_stok($num, $offset, $date_from, $date_to, $kode_brg) {
		$this->db->select(" a.*, b.nama_brg, c.nama as nama_satuan FROM tt_stok a, tm_barang b, tm_satuan c 
					WHERE a.kode_brg = b.kode_brg AND b.satuan = c.id
					AND a.tgl_input >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_input <= to_date('$date_to','dd-mm-yyyy')
					AND a.kode_brg = '$kode_brg'
					ORDER BY a.tgl_input ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
		
		$data_mutasi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row1) {
								
				$data_mutasi[] = array(		'id'=> $row1->id,	
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $row1->nama_brg,
											'satuan'=> $row1->nama_satuan,
											'harga'=> $row1->harga,
											'no_bukti'=> $row1->no_bukti,
											'masuk'=> $row1->masuk,
											'masuk_lain'=> $row1->masuk_lain,
											'keluar'=> $row1->keluar,
											'keluar_lain'=> $row1->keluar_lain,
											'tgl_input'=> $row1->tgl_input,
											'saldo'=> $row1->saldo
											);
			} // endforeach header
		}
		else {
			$data_mutasi = '';
		}
		return $data_mutasi;
  }
  
  function get_mutasi_stoktanpalimit($date_from, $date_to, $kode_brg){
	$query	= $this->db->query(" SELECT a.*, b.nama_brg, c.nama as nama_satuan FROM tt_stok a, tm_barang b, tm_satuan c 
					WHERE a.kode_brg = b.kode_brg AND b.satuan = c.id
					AND a.kode_brg = '$kode_brg'
					AND a.tgl_input >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_input <= to_date('$date_to','dd-mm-yyyy') ");
    
    return $query->result();  
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_bhn)
  {
	if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_bhn){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }

}
