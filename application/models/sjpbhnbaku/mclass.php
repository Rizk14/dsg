<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function iterasionsjpbhnbaku() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT CAST(i_iterasi_code AS integer)+1 AS iiterasicode FROM tm_iterasi_sjpbhnbaku ORDER BY i_iterasi DESC LIMIT 1 ");
	}
	
	function lsatuanbhnbaku(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_satuan a ORDER BY a.e_satuan ASC ");
	}
	
	function lsatuanbhnbakuperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT * FROM tr_satuan a ORDER BY a.e_satuan ASC LIMIT ".$limit." OFFSET ".$offset." ";
		$query = $db2->query($qstr);
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}		
	}
	
	function get_nomorsj() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_sj_bhnbaku WHERE f_sj_cancel='f' ORDER BY cast(i_sj_code AS integer) DESC, d_entry DESC LIMIT 1 " );
	}

	function get_nomor(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sj_code AS character varying),5,(LENGTH(cast(i_sj_code AS character varying))-4)) AS isjcode FROM tm_sj_bhnbaku WHERE f_sj_cancel='f' ORDER BY cast(i_sj_code AS integer) DESC LIMIT 1 " );
	}

	function get_thn(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sj_code AS character varying),1,4) AS thn FROM tm_sj_bhnbaku WHERE f_sj_cancel='f' ORDER BY cast(i_sj_code AS integer) DESC LIMIT 1 " );
	}

	function carisj($nsj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_sj_bhnbaku WHERE i_sj_code=trim('$nsj') AND f_sj_cancel='f' ");
	}

	function getcabang($icusto) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_customer',$icusto);
		$db2->order_by('e_branch_name');
		return $db2->get('tr_branch');
	}

	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}	
			
	function msimpan($i_sj,$dsj,$v_sj_total,$e_note,$i_product,$i_code_references,$e_product_name,$v_product_price,$n_unit,$v_unit_price,$iterasi,$i_customer,$i_branch,$e_satuan,$i_satuan_hidden,$i_gudang) {
		$db2=$this->load->database('db_external', TRUE);
		$iteration	= $iterasi;
		
		$i_sj_item	= array();
			
		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$seq_tm_sj	= $db2->query(" SELECT cast(i_sj AS integer) AS i_sj FROM tm_sj_bhnbaku ORDER BY cast(i_sj AS integer) DESC LIMIT 1 ");
		if($seq_tm_sj->num_rows() > 0 ) {
			$seqrow	= $seq_tm_sj->row();
			$isj	= $seqrow->i_sj+1;
		} else {
			$isj	= 1;
		}
		
		/* Disabled 05122011
		
		$totalnbarang	= 0;
		
		for($item=0;$item<=$iteration;$item++) {
			$totalnbarang += $v_unit_price[$item];
		}
		 
		if($v_sj_total==$totalnbarang) {
			$v_sj_total2 = $v_sj_total;
		} else {
			$v_sj_total2 = $totalnbarang;
		}
		
		*/
		
		$tm_sj	= array(
			 'i_sj'=>$isj,
			 'i_sj_code'=>$i_sj,
			 'i_customer'=>$i_customer,
			 'i_branch'=>$i_branch,
			 'd_sj'=>$dsj,
			 'e_note'=>$e_note,
			 'd_entry'=>$dentry
		);

		if(isset($iteration)){
			
			$db2->insert('tm_sj_bhnbaku', $tm_sj);
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				$seq_tm_sj_item	= $db2->query(" SELECT cast(i_sj_item AS integer) AS i_sj_item FROM tm_sj_bhnbaku_item ORDER BY cast(i_sj_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_sj_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_sj_item->row();
					$i_sj_item[$jumlah]	= $seqrow->i_sj_item+1;
				} else {
					$i_sj_item[$jumlah]	= 1;				
				}
				
				$isjitem[$jumlah]	=
					array(
					 'i_sj_item'=>$i_sj_item[$jumlah],
					 'i_sj'=>$isj,
					 'i_gudang'=>$i_gudang[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'i_code_references'=>$i_code_references[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'v_product_price'=>$v_product_price[$jumlah],
					 'n_unit'=>$n_unit[$jumlah],
					 'n_unit_akhir'=>$n_unit[$jumlah],
					 'e_satuan'=>$i_satuan_hidden[$jumlah],
					 'd_entry'=>$dentry );
				
				$db2->insert('tm_sj_bhnbaku_item',$isjitem[$jumlah]);
				
				//if($i_gudang[$jumlah]=='0')	{
					$db2->query(" INSERT INTO tm_iterasi_sjpbhnbaku(i_iterasi_code,d_entry) VALUES('$i_product[$jumlah]','$dentry') ");
				//}
			}
			
			print "<script>alert(\"Nomor SJ : '\"+$i_sj+\"' telah disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		} else {
			print "<script>alert(\"Maaf, Data SJ gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}

	}
	
	function itemiproduct() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT cast(i_product AS integer)+1 AS i_product FROM tm_sj_item ORDER BY i_sj_item DESC LIMIT 1 ");
	}
	
	/* Disabled */
	function lbarangjadiperpages($limit,$offset,$bulan,$tahun) {
$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' ORDER BY a.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset." ";
					
		$query = $db2->query($qstr);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi($bulan,$tahun) {
$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' ORDER BY a.i_product_motif ASC ");
	}

	function flbarangjadi($key,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);

		return $db2->query(" SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' AND a.i_product_motif='$key' ORDER BY a.i_product_motif ASC "); 		
	}
	/* End 0f Disabled */
	
	
	function lbarangjadiperpagesopsi($limit,$offset,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' ORDER BY a.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset." ";
					
		$query = $db2->query($qstr);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadiopsi($bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' ORDER BY a.i_product_motif ASC ");
	}

	function flbarangjadiopsi($key,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);

		return $db2->query(" SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' AND a.i_product_motif='$key' ORDER BY a.i_product_motif ASC "); 
	}	
}
?>
