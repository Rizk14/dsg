<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
	if($this->session->userdata('gid') == 16){
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
	    }else{
	    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
					                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
					                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
					                        }
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
    function get_data_index($limit=10, $offset=0, $date_from, $date_to, $cari=''){

    	$uid = $this->session->userdata('uid');
	    $sql = "SELECT id, i_sjp, l1.e_area_name, to_char(d_sjp, 'dd-mm-yyyy') AS d_sjp, 
            to_char(d_sjp_receive, 'dd-mm-yyyy') AS d_sjp_receive, 
            l2.e_sender, l3.e_recipient, a.e_remark, f_sjp_cancel,
            CASE WHEN f_sjp_cancel = true THEN 'Batal' 
                ELSE 'Aktif' 
            END AS f_sjp_cancel_name, f_printed, 
            CASE WHEN n_print = 0 THEN 'Belum' 
                ELSE 'Sudah ' || n_print::text || 'X' 
            END AS n_print,
            a.d_approve,
            a.d_notapprove,
            a.e_remark_notapprove,
            u1.nama AS e_user_create,
            u2.nama AS e_user_approve,
            a.uid_approve,
            CASE 	
                WHEN f_sjp_cancel = true THEN 'Batal'
                WHEN d_approve IS NULL AND d_notapprove IS NULL THEN 'Menunggu Approval'
                WHEN d_approve IS NULL AND d_notapprove IS NOT NULL THEN 'Rejected [' ||  coalesce(e_remark_notapprove, '') || ']'
                WHEN d_approve IS NOT NULL AND d_sjp_receive IS NULL THEN 'Approved'
                WHEN d_sjp_receive IS NOT NULL THEN 'Received'
            END AS status_sjp
            FROM tm_sjp_umum a 
            INNER JOIN tm_user u1 ON u1.uid = a.uid
            INNER JOIN tm_user u2 ON u2.uid = a.uid_approve
            INNER JOIN tr_area_sjp b ON (a.i_area = b.i_area)
                ,lateral (SELECT b.i_area || ' - ' || b.e_area_name AS e_area_name) l1
                ,lateral (SELECT coalesce(e_sender,'') || CASE WHEN e_sender IS NULL AND e_sender_company IS NULL THEN '' ELSE ' - ' || coalesce(a.e_sender_company,'') END AS e_sender) l2
                ,lateral (SELECT coalesce(e_recipient ,'') || CASE WHEN e_recipient  IS NULL AND e_recipient_company  IS NULL THEN '' ELSE ' - ' || coalesce(a.e_recipient_company ,'') END AS e_recipient) l3            
            WHERE a.d_sjp BETWEEN '$date_from' AND '$date_to' and ( a.uid = '$uid'  or a.uid_approve = '$uid') 
                AND (a.i_sjp ILIKE '%$cari%' OR a.e_sender ILIKE '%$cari%' OR 
                    l1.e_area_name ILIKE '%$cari%' OR a.e_recipient ILIKE '%$cari%' OR a.e_recipient_company ILIKE '%$cari%')";
        // var_dump($sql); die();
        return $this->db->query($sql);
    }
  
  //
  //function save($no_bonm,$tgl_bonm, $no_bonm_manual, $id_gudang, $tujuan, $ket, 
  function save($id_bonm, $id_gudang, 
			 $id_brg, $nama, $qty, $stok, $satuan, $satuan_konvx, 
			 $qty_satawalx, $ket_detail, $untuk_bisbisan, $id_brg_wip){  
	// 26-06-2015 $is_quilting dihilangkan
    $tgl = date("Y-m-d H:i:s");

	//if ($is_quilting == '')
		$is_quilting = 'f';
	
	// 13-08-2015
	if ($untuk_bisbisan == '')
		$untuk_bisbisan = 'f';
			
			if ($id_brg!='' && ($qty!='' || $qty!=0)) {
				if ($is_quilting == '')
					$is_quilting = 'f';
				
				// cek satuan barangnya, jika yard (qty = meter) maka konversi balik ke yard
				$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE id = '$id_brg' ");
				if ($queryxx->num_rows() > 0){
					$hasilrow = $queryxx->row();
					$id_satuan	= $hasilrow->satuan;
				}
				else
					$id_satuan	= '';
						
				//$hasilrow = $queryxx->row();
				//$id_satuan	= $hasilrow->satuan; 
				
				// 29-10-2014, ini ga dipake, soalnya udh ada setting satuan konversi di master bhn baku
				/*if ($id_satuan == '2') { // jika satuan awalnya yard
					$qty_sat_awal = $qty / 0.90;
					$konv = 't';
				}
				else if ($id_satuan == '7') { // jika satuan awalnya lusin
					$qty_sat_awal = $qty / 12;
					$konv = 't';
				} */
				//else {
					
					if ($satuan_konvx != 0) {
						$konv = 't';
						$qty_sat_awal = $qty_satawalx;
					}
					else {
						$qty_sat_awal = $qty;
						$konv = 'f';
					}
				//}
				
				if ($id_brg_wip == '')
					$id_brg_wip = 0;
					
				// 27-10-2014 pindah kesini. jika semua data tdk kosong, insert ke tm_bonmkeluar_detail
				$data_detail = array(
					'id_bonmkeluar'=>$id_bonm,
					'id_brg'=>$id_brg,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					'konversi_satuan'=>$konv,
					'is_quilting'=>$is_quilting,
					'qty_satawal'=>$qty_satawalx,
					'id_satuan_konversi'=>intval($satuan_konvx),
					'untuk_bisbisan'=>$untuk_bisbisan,
					'id_brg_wip'=>$id_brg_wip
				);
				$this->db->insert('tm_bonmkeluar_detail',$data_detail);
				
				// 27-10-2014, ambil data terakhir di tabel tm_bonmkeluar_detail
				$query3	= $this->db->query(" SELECT id FROM tm_bonmkeluar_detail ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query3->row();
				$id_bonmkeluar_detail	= $hasilrow->id; 
				
				// ======== update stoknya! =============
				// 05-09-2015 DIKOMEN AJA BIAR LANGSUNG KE tm_stok
				//if ($is_quilting == 't')
				//	$nama_tabel_stok = "tm_stok_hasil_makloon";
				//else
				$nama_tabel_stok = "tm_stok";
				
				//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE id_brg = '$id_brg' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty_sat_awal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'id_brg'=>$id_brg,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id_brg= '$id_brg' ");
					}
					
					// ==================================== 05-09-2015 STOK HARGA GA DIPAKE LAGI DI PROSES INI ==================================================
					// dan juga save ke tabel bonmkeluar_detail_harga ga dipake lagi
				
				/*	$selisih = 0;
					$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
					if ($is_quilting == 't')
						$sqlxx.= " id_brg_quilting = '$id_brg' ";
					else
						$sqlxx.= " id_brg = '$id_brg' ";
					$sqlxx.= " AND quilting = '$is_quilting' AND stok > '0' ORDER BY id ASC";
					
					// 22-01-2013, utk tujuan=3 (retur) maka order by id DESC. utk tujuan=4 (peminjaman) maka order by id ASC
					// ralat: perubahan order by ini dipending dulu, mau ditanyain ke mindra nanti
					// 24-10-2014, abaikan ajalah yg 22-01-2013 diatas. kata rohmat, berdasarkan harga terlama s/d terbaru
						
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx=$queryxx->result();
							
						$temp_selisih = 0; $list_harga = array();
						foreach ($hasilxx as $row2) {
							$stoknya = $row2->stok; 
							$harganya = $row2->harga; 
							
							//if ($stoknya > 0) { 
								// ================= 27-10-2014, skrip baru ================================================
								if ($temp_selisih == 0)
									$selisih = $stoknya-$qty_sat_awal;
								else
									$selisih = $stoknya+$temp_selisih;
								
								if ($selisih < 0) {
									// isinya minus. stok utk harga ini = 0
									$temp_selisih = $selisih;
									// 27-10-2014, tambahkan skrip simpan ke tabel tm_bonmkeluar_detail_harga
								//	$detail_harga = array(
								//		'id_bonmkeluar_detail'=>$id_bonmkeluar_detail,
								//		'qty'=>$stoknya,
								//		'harga'=>$harganya
								//		);
								//	$this->db->insert('tm_bonmkeluar_detail_harga', $detail_harga);
									
									if ($is_quilting == 'f') {										
										$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where id_brg= '$id_brg' AND harga = '$harganya' "); //
									}
									else {										
										$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where id_brg_quilting= '$id_brg' AND harga = '$harganya' "); //
									}
									$list_harga[] = array('harganya'=>$harganya,
														 'qty_satawal'=>$stoknya);
								}
								else if ($selisih >= 0) {
									if ($temp_selisih != 0) {
										$qty_tambahan = abs($temp_selisih);
										$temp_selisih = 0;
									}
									else
										$qty_tambahan = $qty_sat_awal;
									
									// 27-10-2014, tambahkan skrip simpan ke tabel tm_bonmkeluar_detail_harga
									//$detail_harga = array(
									//	'id_bonmkeluar_detail'=>$id_bonmkeluar_detail,
									//	'qty'=>$qty_tambahan, // 12:34
									//	'harga'=>$harganya
									//	);
									//$this->db->insert('tm_bonmkeluar_detail_harga', $detail_harga);
									
									if ($is_quilting == 'f') {										
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$selisih', tgl_update_stok = '$tgl' 
												where id_brg= '$id_brg' AND harga = '$harganya' "); //
										
									}
									else {										
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$selisih', tgl_update_stok = '$tgl' 
												where id_brg_quilting= '$id_brg' AND harga = '$harganya' "); //
									}
									$list_harga[] = array('harganya'=>$harganya,
														 'qty_satawal'=>$qty_tambahan);
									break;
									
								} // logikanya udh good kayaknya
								//-----------------------------------------------------------------------------------------
						//	}
						} // end for stok harga
						
						// 28-10-2014
						$jum_data_harga = count($list_harga);
						
						if ($jum_data_harga > 0) {
							// catatan 27-02-2015: qty ini bukan dibagi rata, tapi dari data qty_satawal trus dikonversi
							$query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
										a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
										INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
							if ($query3->num_rows() > 0){
								$hasilnya = $query3->row();
								$kode_brg = $hasilnya->kode_brg;
								$nama_brg = $hasilnya->nama_brg;
								$id_satuan = $hasilnya->satuan;
								$satuan = $hasilnya->nama_satuan;
								
								$id_satuan_konversi = $hasilnya->id_satuan_konversi;
								$rumus_konversi = $hasilnya->rumus_konversi;
								$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
							}
							else {
								$id_satuan_konversi = 0;
								$id_satuan = 0;
								$satuan = '';
							}
							
							//$qty_dipecah = $qty/$jum_data_harga;
							//foreach ($list_harga as $rowharga) {
							for ($i=0; $i<count($list_harga); $i++) {
								// 27-02-2015
								if ($id_satuan_konversi != 0) {
									if ($rumus_konversi == '1') {
										$qty_dipecah = $list_harga[$i]['qty_satawal']*$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '2') {
										$qty_dipecah = $list_harga[$i]['qty_satawal']/$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '3') {
										$qty_dipecah = $list_harga[$i]['qty_satawal']+$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '4') {
										$qty_dipecah = $list_harga[$i]['qty_satawal']-$angka_faktor_konversi;
									}
									else
										$qty_dipecah = $list_harga[$i]['qty_satawal'];
								}
								else
									$qty_dipecah = $list_harga[$i]['qty_satawal'];
								
								$detail_harga = array(
										'id_bonmkeluar_detail'=>$id_bonmkeluar_detail,
										'qty'=>$qty_dipecah,
										'harga'=>$list_harga[$i]['harganya'],
										'qty_satawal'=>$list_harga[$i]['qty_satawal']
										);
									$this->db->insert('tm_bonmkeluar_detail_harga', $detail_harga);
							}
						}
						
					} // end if
					*/
					
				// ================================================== 05-09-2015 END STOK HARGA DAN BONMKELUAR_DETAIL_HARGA ======================
				
				// ============================= end 22-12-2012 ============================
				
				// 27-10-2014, ini dipindah keatas
				// jika semua data tdk kosong, insert ke tm_bonmkeluar_detail
				/*$data_detail = array(
					'id_bonmkeluar'=>$id_bonm,
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					'konversi_satuan'=>$konv,
					'is_quilting'=>$is_quilting,
					'qty_satawal'=>$qty_satawalx,
					'id_satuan_konversi'=>$satuan_konvx
				);
				$this->db->insert('tm_bonmkeluar_detail',$data_detail); */
			} 
  }
    
  function delete($id){
    $data = [
        'f_sjp_cancel' => 't'
    ];

    $this->db->where('id', $id);
    $this->db->update('tm_sjp_umum', $data);
  }
  
  function get_bahan($num, $offset, $cari, $id_gudang, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id 
					WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a INNER JOIN tm_satuan b ON a.satuan = b.id
					WHERE a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
				WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a INNER JOIN tm_satuan b ON a.satuan = b.id
					WHERE a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			if ($is_quilting == 'f') {
				$tabelstok = "tm_stok";
				
				// 29-10-2014
				$id_satuan_konversi = $row1->id_satuan_konversi;
				$rumus_konversi = $row1->rumus_konversi;
				$angka_faktor_konversi = $row1->angka_faktor_konversi;
			}
			else {
				$tabelstok = "tm_stok_hasil_makloon";
				$id_satuan_konversi = 0;
				$rumus_konversi = 0;
				$angka_faktor_konversi = 0;
			}
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE id_brg = '$row1->id' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;
			
			// 06-08-2015
			if ($id_satuan_konversi != 0) {
				
				// 04-08-2015 satuan konversi
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$id_satuan_konversi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan_konversi	= $hasilrow->nama;
				}
				else
					$satuan_konversi = "Tidak Ada";
				
				if ($rumus_konversi == '1') {
					$qty_konversi = $jum_stok*$angka_faktor_konversi;
				}
				else if ($rumus_konversi == '2') {
					$qty_konversi = $jum_stok/$angka_faktor_konversi;
				}
				else if ($rumus_konversi == '3') {
					$qty_konversi = $jum_stok+$angka_faktor_konversi;
				}
				else if ($rumus_konversi == '4') {
					$qty_konversi = $jum_stok-$angka_faktor_konversi;
				}
				else
					$qty_konversi = $jum_stok;
			}
			else {
				$qty_konversi = $jum_stok;
				$satuan_konversi = "Tidak Ada";
			}

			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(		'id_brg'=> $row1->id,	
										'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'satuan_konversi'=> $satuan_konversi,
										// 29-10-2014
										'id_satuan_konversi'=> $id_satuan_konversi,
										'rumus_konversi'=> $rumus_konversi,
										'angka_faktor_konversi'=> $angka_faktor_konversi,
										//------------
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok,
										'jum_stok_konversi'=> $qty_konversi
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_wip($num, $offset, $cari)
  {

    
    if ($cari == "all") {
		
			$sql = "  * from tm_barang_wip WHERE status_aktif = 't'  ORDER BY nama_brg ";
		
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		
			$sql = "  * from tm_barang_wip where TRUE  ";
			$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) 
					AND status_aktif = 't' ORDER BY nama_brg desc";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
    
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			
						
			$data_bhn[] = array(		'id_brg'=> $row1->id,	
										'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										
										'tgl_update'=> $row1->tgl_update,
										
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $id_gudang, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
					WHERE a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a INNER JOIN tm_satuan b ON a.satuan = b.id
					WHERE a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
							WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a INNER JOIN tm_satuan b ON a.satuan = b.id
					WHERE a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
    
    function get_wiptanpalimit($cari){
	if ($cari == "all") {
		
			$sql = " select * from tm_barang_wip WHERE status_aktif = 't'  ORDER BY nama_brg ";
		
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		
			$sql = " select * from tm_barang_wip where TRUE  ";
			$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) 
					AND status_aktif = 't' ORDER BY nama_brg desc";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
    
  function get_bonm($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluar where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, a.id_satuan_konversi, a.rumus_konversi,
										a.angka_faktor_konversi, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row2->id_brg' ");
						}
						else {
							$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row2->id_brg' ");
							
						}
						$hasilrow = $query3->row();
						if ($row2->is_quilting == 'f') {
							// 29-10-2014
							$satuan_konv	= $hasilrow->id_satuan_konversi;
							$rumus_konv	= $hasilrow->rumus_konversi;
							$angka_faktor_konversi	= $hasilrow->angka_faktor_konversi;
						}
						else {
							// 29-10-2014
							$satuan_konv	= 0;
							$rumus_konv	= 0;
							$angka_faktor_konversi	= 0;
						}
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$id_satuan	= $hasilrow->id_satuan;
						$satuan	= $hasilrow->nama_satuan;
						
						
						if ($row2->is_quilting == 'f')
							$tabelstok = "tm_stok";
						else
							$tabelstok = "tm_stok_hasil_makloon";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE id_brg = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
						
						// 04-11-2015
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg_wip	= $hasilrow->kode_brg;
							$nama_brg_wip	= $hasilrow->nama_brg;
						}
						else {
							$kode_brg_wip	= '';
							$nama_brg_wip	= '';
						}
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'id_satuan'=> $id_satuan,
												'qty'=> $row2->qty,
												'is_quilting'=> $row2->is_quilting,
												'keterangan'=> $row2->keterangan,
												'konversi_satuan'=> $row2->konversi_satuan,
												'jum_stok'=> $jum_stok,
												
												'id_satuan_konversi'=> $row2->id_satuan_konversi,
												'qty_satawal'=> $row2->qty_satawal,
												// 29-10-2014
												'rumus_konv'=> $rumus_konv,
												'satuan_konv'=> $satuan_konv,
												'angka_faktor_konversi'=> $angka_faktor_konversi,
												// 13-08-2015
												'untuk_bisbisan'=> $row2->untuk_bisbisan,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
											);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				/*if ($row1->tujuan=='1')
					$nama_tujuan = "Cutting";
				else if ($row1->tujuan=='2')
					$nama_tujuan = "Unit";
				else
					$nama_tujuan = "Lain-Lain"; */
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'tujuan'=> $row1->tujuan,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											//'nama_tujuan'=> $nama_tujuan,
											'id_gudang'=> $row1->id_gudang,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  
  function cek_data($no_bonm_manual, $thn1, $tgl_bonm, $tujuan, $id_unit_jahit){
    $this->db->select("id from tm_bonmkeluar WHERE no_manual = '$no_bonm_manual' AND tgl_bonm = '$tgl_bonm'
				AND tujuan = '$tujuan' AND id_unit_jahit='$id_unit_jahit' ", false);
	// 08-06-2015
	//AND extract(year from tgl_bonm) = '$thn1'
    
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 21-09-2013, modif 27-01-2014
  function getbarangjaditanpalimit($cari){
  	/*if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')
						OR UPPER(c.kode_brg) LIKE UPPER('%$cari%') OR UPPER(c.nama_brg) LIKE UPPER('%$cari%') )";
	}
	$sql = "SELECT a.kode_brg_jadi, b.e_product_motifname, a.kode_brg, c.nama_brg, a.stok FROM tm_stok_hasil_cutting a, tr_product_motif b, tm_barang c 
			WHERE a.kode_brg_jadi = b.i_product_motif 
			AND a.kode_brg = c.kode_brg AND b.n_active='1' AND c.status_aktif = 't' ".$pencarian." ORDER BY a.kode_brg_jadi ASC";
  	$query	= $this->db->query($sql);
	return $query->result(); */
	
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  // modif 27-01-2014
  function getbarangjadi($num,$offset, $cari){
  	/*if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')
						OR UPPER(c.kode_brg) LIKE UPPER('%$cari%') OR UPPER(c.nama_brg) LIKE UPPER('%$cari%') )";
	}
	$sql = " a.kode_brg_jadi, b.e_product_motifname, a.kode_brg, c.nama_brg, a.stok FROM tm_stok_hasil_cutting a, tr_product_motif b, tm_barang c 
			WHERE a.kode_brg_jadi = b.i_product_motif 
			AND a.kode_brg = c.kode_brg AND b.n_active='1' AND c.status_aktif = 't' ".$pencarian." ORDER BY a.kode_brg_jadi ASC";	
	
	$this->db->select($sql, false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();  */
    
    // new 27-01-2014
    if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row1->i_product_motif' ");
									
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
				if ($stok == '')
					$stok = 0;
			}
			else
				$stok = 0;
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname,
										'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  // 23-09-2013, modif 27-01-2014. 04-11-2015 DIKOMEN
  /*function getAllbonmkeluarcutting($num, $offset, $cari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND (UPPER(a.no_bonm) like UPPER('%$cari%') OR UPPER(a.no_manual) like UPPER('%$cari%'))";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit = '$kode_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_bonm DESC, a.no_bonm DESC ";
		
		$this->db->select(" distinct a.* FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b, tr_product_motif c 
					WHERE a.id=b.id_bonmkeluarcutting AND b.kode_brg_jadi = c.i_product_motif ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(b.e_product_motifname) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_bonmkeluarcutting_detail a, tr_product_motif b  
									WHERE a.kode_brg_jadi=b.i_product_motif AND a.id_bonmkeluarcutting = '$row1->id' ".$pencarian2
									." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname 
									FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg_jadi	= $hasilrow->e_product_motifname;
							}
							else {
								$nama_brg_jadi = '';
							}
							
							// 30-01-2014, detail qty warna -------------
							$strwarna = "";
							$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_bonmkeluarcutting_detail_warna a, tm_warna c
										WHERE a.kode_warna = c.kode
										AND a.id_bonmkeluarcutting_detail = '$row2->id' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx=$queryxx->result();
								$strwarna.= $row2->kode_brg_jadi.": <br>";
								foreach ($hasilxx as $rowxx) {
									$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
								}
							}
							//-------------------------------------------
										
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_bonm = '';
				}
																
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar Bagus";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Pengembalian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else
					$nama_unit_jahit = "";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit_jahit,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  // 24-02-2015. 
  function getAllbonmkeluarcutting_export($cari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND (UPPER(a.no_bonm) like UPPER('%$cari%') OR UPPER(a.no_manual) like UPPER('%$cari%'))";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit = '$kode_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_bonm DESC, a.no_bonm DESC ";
		
		$this->db->select(" distinct a.* FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b, tr_product_motif c 
					WHERE a.id=b.id_bonmkeluarcutting AND b.kode_brg_jadi = c.i_product_motif ".$pencarian." ", false);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(b.e_product_motifname) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_bonmkeluarcutting_detail a, tr_product_motif b  
									WHERE a.kode_brg_jadi=b.i_product_motif AND a.id_bonmkeluarcutting = '$row1->id' ".$pencarian2
									." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname 
									FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg_jadi	= $hasilrow->e_product_motifname;
							}
							else {
								$nama_brg_jadi = '';
							}
							
							// 30-01-2014, detail qty warna -------------
							$strwarna = "";
							$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_bonmkeluarcutting_detail_warna a, tm_warna c
										WHERE a.kode_warna = c.kode
										AND a.id_bonmkeluarcutting_detail = '$row2->id' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx=$queryxx->result();
								$strwarna.= $row2->kode_brg_jadi.": <br>";
								foreach ($hasilxx as $rowxx) {
									$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
								}
							}
							//-------------------------------------------
										
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_bonm = '';
				}
																
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar Bagus";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Pengembalian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else
					$nama_unit_jahit = "";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit_jahit,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  // -------------------------------
  
  function getAllbonmkeluarcuttingtanpalimit($cari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND (UPPER(a.no_bonm) like UPPER('%$cari%') OR UPPER(a.no_manual) like UPPER('%$cari%'))";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit = '$kode_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
	  
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b, tr_product_motif c 
					WHERE a.id=b.id_bonmkeluarcutting AND b.kode_brg_jadi = c.i_product_motif ".$pencarian);

    return $query->result();  
  } */
  
  function cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $kode_unit_jahit, $jenis){
    $this->db->select("id from tm_bonmkeluarcutting WHERE no_manual = '$no_bonm_manual' 
				AND extract(year from tgl_bonm) = '$thn1' AND kode_unit='$kode_unit_jahit'
				AND jenis_keluar = '$jenis' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savebonmkeluarcutting($id_bonm, $jenis, $kode_unit_jahit,
			$kode_brg_jadi, $temp_qty, $kode_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
    // cek apa udah ada datanya blm dgn no bon M tadi
   /* $this->db->select("id from tm_bonmkeluarcutting WHERE no_bonm = '$no_bonm' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_bonmkeluarcutting
			$data_header = array(
			  'no_bonm'=>$no_bonm,
			  'no_manual'=>$no_bonm_manual,
			  'tgl_bonm'=>$tgl_bonm,
			  'jenis_keluar'=>$jenis,
			  'kode_unit'=>$kode_unit_jahit,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_bonmkeluarcutting',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_bonmkeluarcutting
			$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_bonm	= $hasilrow->id; */
			
			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx=0; $xx<count($kode_warna); $xx++) {
				$kode_warna[$xx] = trim($kode_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal+= $qty_warna[$xx];
			} // end for
			
			// ======== update stoknya! =============
			//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode_brg_jadi,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
						
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode_brg_jadi' ");
					}
					
					//----------------------- 03-02-2014 ---------------------------------------------------
					//update stok unit jahit
					//if ($kode_unit_jahit != '0') {
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode_brg_jadi'
								AND kode_unit='$kode_unit_jahit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_lama = 0;
							$stok_unit_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unit_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit = $stok_unit_lama+$qtytotal; // bertambah stok karena masuk ke unit
						$new_stok_unit_bagus = $stok_unit_bagus_lama+$qtytotal;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'kode_brg_jadi'=>$kode_brg_jadi,
								'kode_unit'=>$kode_unit_jahit,
								'stok'=>$new_stok_unit,
								'stok_bagus'=>$new_stok_unit_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
							stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
							where kode_brg_jadi= '$kode_brg_jadi' AND kode_unit='$kode_unit_jahit' ");
						}
					//} // end if
					//-------------------------------------------------------------------------------------	
							
				// ======== update stoknya! =============
				// modif 27-01-2014: sementara dikomen dulu karena fitur bon M masuk hasil cutting blm jalan
				// dan nanti stok hasil cutting ini di tabel baru aja per brg jadi, ga ada acuan ke bhn baku
				// revisi 04-02-2014: skrip yg dikomen ini udh ga dipake, skrg pake skrip update stok terbaru diatas
				
				//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
			/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting 
										WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode_brg_jadi,
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode_brg_jadi' AND kode_brg= '$kode' ");
					}
				*/
				// jika semua data tdk kosong, insert ke tm_bonmkeluarcutting_detail
				$data_detail = array(
					'id_bonmkeluarcutting'=>$id_bonm,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				);
				$this->db->insert('tm_bonmkeluarcutting_detail',$data_detail);
				
				// ambil id detail tm_bonmkeluarcutting_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// ----------------------------------------------
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_bonmkeluarcutting_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_bonmkeluarcutting_detail'=>$iddetail,
						 'id_warna_brg_jadi'=>'0',
						 'kode_warna'=>$kode_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_bonmkeluarcutting_detail_warna',$tm_bonmkeluarcutting_detail_warna);
					
					// ========================= 04-02-2014, stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_cutting'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
					}
					
					// ====================	15-03-2014, stok hasil cutting bhn baku ==================
					//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_bhn_lama = 0;
						$new_stok_bhnbaku = $stok_bhn_lama-$qty_warna[$xx];
						
						// ambil data2 material brg jadi berdasarkan brg jadi, trus insert langsung ke tabel stok_hasil_cutting_bhnbaku. qty-nya sesuai dgn warnanya
						// 05-12-2014, kode brg tidak boleh 00
						$sqlxx = " SELECT * FROM tm_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' 
									AND kode_warna='".$kode_warna[$xx]."' AND kode_brg <> '00' ";
						$queryxx = $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->result();
							foreach ($hasilxx as $rowxx) {			
								$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
								if($seq_stokwarna->num_rows() > 0) {
									$seq_stokwarnarow	= $seq_stokwarna->row();
									$id_stok_warna	= $seq_stokwarnarow->id+1;
								}else{
									$id_stok_warna	= 1;
								}
								
								$data_stok = array(
									'id'=>$id_stok_warna,
									'id_stok_hasil_cutting'=>$id_stok,
									'kode_brg'=>$rowxx->kode_brg,
									'kode_warna'=>$kode_warna[$xx],
									'stok'=>$new_stok_bhnbaku,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting_bhnbaku', $data_stok);
							} // end xxx
						} // end if
						
					}
					else {
						//$hasilrow = $query3->row();
						//$stok_warna_lama	= $hasilrow->stok;
						$hasil3 = $query3->result();
						foreach ($hasil3 as $row3) {	
							$new_stok_bhnbaku = $row3->stok-$qty_warna[$xx];
							$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
									where id= '".$row3->id."' ");
						}
					} // end if
					// ==========================================================
					
					// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit
					//if ($kode_unit_jahit != '0') {
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama+$qty_warna[$xx]; // bertambah stok karena masuk ke unit
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_bagus'=>$new_stok_unit_warna_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
				//	} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
					
				} // end for
				
				// 05-12-2014, update stok hasil cutting bhn baku yg tidak ada warna
				//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE id_stok_hasil_cutting='$id_stok'
					AND kode_warna = '00' ");
					if ($query3->num_rows() == 0){
						$stok_bhn_lama = 0;
						$new_stok_bhnbaku = $stok_bhn_lama-$qtytotal;
						
						// ambil data2 material brg jadi berdasarkan brg jadi, trus insert langsung ke tabel stok_hasil_cutting_bhnbaku. qty-nya sesuai dgn warnanya
						// 05-12-2014, kode brg tidak boleh 00
						$sqlxx = " SELECT * FROM tm_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' 
									AND kode_brg <> '00' AND kode_warna = '00' ";
						$queryxx = $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->result();
							foreach ($hasilxx as $rowxx) {			
								$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
								if($seq_stokwarna->num_rows() > 0) {
									$seq_stokwarnarow	= $seq_stokwarna->row();
									$id_stok_warna	= $seq_stokwarnarow->id+1;
								}else{
									$id_stok_warna	= 1;
								}
								
								$data_stok = array(
									'id'=>$id_stok_warna,
									'id_stok_hasil_cutting'=>$id_stok,
									'kode_brg'=>$rowxx->kode_brg,
									'kode_warna'=>'00',
									'stok'=>$new_stok_bhnbaku,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting_bhnbaku', $data_stok);
							} // end xxx
						} // end if
						
					}
					else {
						//$hasilrow = $query3->row();
						//$stok_warna_lama	= $hasilrow->stok;
						$hasil3 = $query3->result();
						foreach ($hasil3 as $row3) {	
							$new_stok_bhnbaku = $row3->stok-$qtytotal;
							$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
									where id= '".$row3->id."' ");
						}
					} // end if
					//DONE
				// ----------------------------------------------
  }
  
  function deletebonmkeluarcutting($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
			/* SELECT a.id_gudang, a.kode_unit_jahit, b.* 
								 FROM tm_sjkeluarwip_detail b, tm_sjkeluarwip a 
								 WHERE a.id = b.id_sjkeluarwip AND b.id_sjkeluarwip = '$kode' */
				$query2	= $this->db->query(" SELECT a.kode_unit, b.*  FROM tm_bonmkeluarcutting_detail b, 
							tm_bonmkeluarcutting a WHERE a.id = b.id_bonmkeluarcutting AND b.id_bonmkeluarcutting = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok pabrik =====================
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
						
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi' ");
									
						// ================= update stok unit jahit ==========================
					//	if ($row2->kode_unit_jahit != '0') {
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
										 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$row2->kode_unit' ");
							if ($query3->num_rows() == 0){
								$stok_unit_lama = 0;
								$id_stok_unit = 0;
								$stok_unit_bagus_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unit_bagus_lama = $hasilrow->stok_bagus;
							}
							$new_stok_unit = $stok_unit_lama-$row2->qty; // berkurang stok karena reset dari bon M keluar
							$new_stok_unit_bagus = $stok_unit_bagus_lama-$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
										WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
										AND kode_unit = '$row2->kode_unit' ");
					//	} // end if update stok unit jahit
						
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_bonmkeluarcutting_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna 
												WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari bon M keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ====================	15-03-2014, stok hasil cutting bhn baku ==================
								//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
								$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$rowwarna->kode_warna."'
										AND id_stok_hasil_cutting='$id_stok' ");
								if ($query3->num_rows() > 0){
									$hasil3 = $query3->result();
									foreach ($hasil3 as $row3) {	
										$new_stok_bhnbaku = $row3->stok+$rowwarna->qty;
										$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
												where id= '".$row3->id."' ");
									}
								}
								// ==========================================================
								
								// ============= update stok unit jahit
							//	if ($row2->kode_unit_jahit != '0') {
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warna_bagus_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama-$rowwarna->qty; // berkurang stok di unit karena reset dari bon M keluar
									$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama-$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
												WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
							//	} // end if kode_unit_jahit != 0
							}
						} // end if detail warna
						
						// 05-12-2014, reset stok hasil cutting bhn baku yg kode warnanya 00
						// ====================	==================
						//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '00'
									AND id_stok_hasil_cutting='$id_stok' ");
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
							foreach ($hasil3 as $row3) {	
								$new_stok_bhnbaku = $row3->stok+$row2->qty;
								$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
										where id= '".$row3->id."' ");
							}
						}
						// ==========================================================
						
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_bonmkeluarcutting_detail_warna 04-02-2014
						$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail='".$row2->id."' ");
						
						// ============ update stok =====================				
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						// modif 27-01-2014, sementara stok hasil cutting blm dipake
						/*		$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting 
												WHERE kode_brg = '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'kode_brg_jadi'=>$row2->kode_brg_jadi,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
												where kode_brg= '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								}	*/
						// ==============================================						
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_bonmkeluarcutting
    $this->db->delete('tm_bonmkeluarcutting_detail', array('id_bonmkeluarcutting' => $id));
    $this->db->delete('tm_bonmkeluarcutting', array('id' => $id));
  }
  
  function get_bonmkeluarcutting($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail WHERE id_bonmkeluarcutting = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$stok	= $hasilrow->stok;
						else
							$stok = 0;
						
						$query4	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if($query4->num_rows()>0){
							$hasilrow2	= $query4->row();
							$nama_brg_jadi	= $hasilrow2->e_product_motifname;
						}else{
							$nama_brg_jadi	= '';
						}
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_warna dan tm_bonmkeluarcutting_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.kode_warna, b.nama, c.qty FROM tm_warna b,
												tm_bonmkeluarcutting_detail_warna c
												 WHERE c.kode_warna=b.kode
												 AND c.id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$kode_warna = $row3->kode_warna;
								
								$detail_warna[] = array(	'kode_warna'=> $kode_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else
							$detail_warna = '';
						//-------------------------------------------------------------------------------------
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'stok'=> $stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->jenis_keluar=='1')
					$nama_jenis = "Keluar Bagus";
				else if ($row1->jenis_keluar=='2')
					$nama_jenis = "Pengembalian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else
					$nama_unit_jahit = "";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit_jahit,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  // 27-01-2014
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
   function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_satuan(){
    $query = $this->db->query(" SELECT * FROM tm_satuan ORDER BY id");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

  function get_list_area()
  {
    $query = $this->db->query(" SELECT * FROM tr_area_sjp WHERE f_active = TRUE");

    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

  function generate_nomor_dokumen($date, $i_area, $prefix='')
  {
    $periode = date('ym', strtotime($date));
    $periode_year = date('y', strtotime($date));
    $code = 'SJP';

    if ($prefix != '') {
        $code = $prefix;
    }

    $sql = "SELECT count(*)::numeric
            FROM tm_sjp_umum
            WHERE i_area = '$i_area'
            AND to_char(d_sjp, 'yy') = '$periode_year'";

    $query = $this->db->query($sql);
    $result = $query->row()->count;
    $count = intval($result) + 1;
    $generated = $code . '-' . $periode . '-' . sprintf('%06d', $count);

    return $generated;
  }

  function insert_header($i_sjp, $i_area, $d_sjp, $e_sender, $e_sender_company, $e_recipient, $e_recipient_company, $e_remark='', $uid, $uid_approve)
  {
    $sql = "INSERT INTO 
            public.tm_sjp_umum (
                i_sjp, i_area, d_sjp, e_sender, e_sender_company, e_recipient, e_recipient_company, e_remark, uid, uid_approve
            ) VALUES(
                '$i_sjp', '$i_area', '$d_sjp', '$e_sender', '$e_sender_company', '$e_recipient', '$e_recipient_company', '$e_remark', '$uid', '$uid_approve'
            )
            RETURNING id";
                    
    return $this->db->query($sql);        
  }

  function update_header($id, $i_sjp, $i_area, $d_sjp, $e_sender, $e_sender_company, $e_recipient, $e_recipient_company, $e_remark='', $uid, $uid_approve)
  {
    $data = [
        'i_sjp' => $i_sjp,
        'i_area' => $i_area,
        'd_sjp' => $d_sjp,
        'e_sender' => $e_sender,
        'e_sender_company' => $e_sender_company,
        'e_recipient' => $e_recipient,
        'e_recipient_company' => $e_recipient_company,
        'e_remark' => $e_remark,
        'uid' => $uid,
        'uid_approve' => $uid_approve
    ];

    /** default kondisi update status reject menjadi null */
    $data['d_notapprove'] = null;
    $data['e_remark_notapprove'] = null;
                    
    $this->db->where('id', $id);
    $this->db->update('tm_sjp_umum', $data);
  }

  function insert_detail($id_sjp, $i_product, $e_product_name, $n_quantity, $n_satuan, $e_remark, $n_item_no)
  {
    $data = [
        'id_sjp' => $id_sjp,
        'i_product' => $i_product,
        'e_product_name' => $e_product_name,
        'n_quantity' => $n_quantity,
        'n_satuan' => $n_satuan,
        'e_remark' => $e_remark,
        'n_item_no' => $n_item_no
    ];

    $this->db->insert('tm_sjp_umum_item', $data);
  }

  function delete_detail($id_header)
  {
    $sql = "DELETE FROM tm_sjp_umum_item WHERE id_sjp = '$id_header'";
    $this->db->query($sql);
  }

  function get_gudang_by_id($id)
  {
    $sql = "SELECT * FROM tm_gudang WHERE id='$id'";

    return $this->db->query($sql);
  }

  function get_data_edit_header($id)
  {
    $sql = "SELECT id, i_sjp, a.i_area, b.e_area_name, d_sjp, d_sjp_receive, e_sender, e_sender_company, 
            e_recipient, e_recipient_company, e_remark, e_receive FROM tm_sjp_umum a 
            INNER JOIN tr_area_sjp b ON (a.i_area = b.i_area)
            WHERE id = '$id'";

    return $this->db->query($sql);
  }

  function get_data_edit_detail($id)
  {
    $sql = "SELECT i_product, e_product_name, n_quantity, n_quantity_receive , n_satuan, e_remark, id AS id_detail
            FROM tm_sjp_umum_item WHERE id_sjp = '$id'";

    return $this->db->query($sql);
  }

  function increment_cetak($id)
  {
    /** update f_printed */
    $sql = "UPDATE tm_sjp_umum SET n_print = n_print + 1, f_printed = 't' WHERE id = '$id'";
    
    $this->db->query($sql);
  }

  function get_uid_sjp_approve($uid)
  {
    return $this->db->query("SELECT coalesce(uid_sjp_approve, 1) AS uid_sjp_approve FROM tm_user WHERE uid = '$uid'")
                    ->row()->uid_sjp_approve;
  }

  function approve($id)
  {
    $d_approve = date('Y-m-d');

    $data = [
        'd_approve' => $d_approve
    ];

    $this->db->where('id', $id);
    $this->db->update('tm_sjp_umum', $data);
  }

  function reject($id, $e_remark_notapprove)
  {
    $d_notapprove = date('Y-m-d');

    $data = [
        'd_notapprove' => $d_notapprove,
        'e_remark_notapprove' => $e_remark_notapprove
    ];

    $this->db->where('id', $id);
    $this->db->update('tm_sjp_umum', $data);
  }

  function update_accept($id, $d_sjp_receive, $e_receive)
  {
    $data = [
        'd_sjp_receive' => $d_sjp_receive,
        'e_receive' => $e_receive
    ];

    $this->db->where('id', $id);
    $this->db->update('tm_sjp_umum', $data);
  }

  function update_accept_item($id_detail, $n_quantity_receive)
  {
    $data = [
        'n_quantity_receive' => $n_quantity_receive,
    ];

    $this->db->where('id', $id_detail);
    $this->db->update('tm_sjp_umum_item', $data);
  }

  function get_status_print($id)
  {
    $sql = "SELECT f_printed FROM tm_sjp_umum WHERE id='$id'";

    return $this->db->query($sql)->row()->f_printed;
  }
  
}
