<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAll($num, $offset, $cari, $date_from, $date_to) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_bonm) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$this->db->select(" * FROM tm_bonmkeluar WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$id_satuan	= $hasilrow->id_satuan;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg = '';
								$id_satuan = 0;
								$satuan = '';
							}
						}
						else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$id_satuan	= $hasilrow->id_satuan;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg = '';
							$id_satuan = 0;
							$satuan = '';
						}
										
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'id_satuan'=> $id_satuan,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'konversi_satuan'=> $row2->konversi_satuan,
												'is_quilting'=> $row2->is_quilting
											);
					}
				}
				else {
					$detail_bonm = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				if ($row1->tujuan == '1')
					$nama_tujuan = "Cutting";
				else if ($row1->tujuan == '2')
					$nama_tujuan = "Unit";
				else if ($row1->tujuan == '3')
					$nama_tujuan = "Lain-Lain (Retur)";
				else if ($row1->tujuan == '4')
					$nama_tujuan = "Lain-Lain (Peminjaman)";
				else
					$nama_tujuan = "Lain-Lain (Lainnya)";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'tujuan'=> $row1->tujuan,
											'nama_tujuan'=> $nama_tujuan,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function getAlltanpalimit($cari, $date_from, $date_to){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_bonm) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluar WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  //
  function save($no_bonm,$tgl_bonm, $no_bonm_manual, $id_gudang, $tujuan, $ket, 
			$is_quilting, $kode, $nama, $qty, $stok, $satuan, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");

	if ($is_quilting == '')
		$is_quilting = 'f';
	
    // cek apa udah ada datanya blm dgn no request tadi
    $this->db->select("id from tm_bonmkeluar WHERE no_bonm = '$no_bonm' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_bonm
			$data_header = array(
			  'no_bonm'=>$no_bonm,
			  'no_manual'=>$no_bonm_manual,
			  'tgl_bonm'=>$tgl_bonm,
			  'tujuan'=>$tujuan,
			  'id_gudang'=>$id_gudang,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_bonmkeluar',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_bonmkeluar
			$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluar ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_bonm	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				if ($is_quilting == '')
					$is_quilting = 'f';
				
				// cek satuan barangnya, jika yard (qty = meter) maka konversi balik ke yard
				$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$kode' ");
				if ($queryxx->num_rows() > 0){
					$hasilrow = $queryxx->row();
					$id_satuan	= $hasilrow->satuan;
				}
				else
					$id_satuan	= '';
						
				//$hasilrow = $queryxx->row();
				//$id_satuan	= $hasilrow->satuan; 
				
				if ($id_satuan == '2') { // jika satuan awalnya yard
					$qty_sat_awal = $qty / 0.90;
					$konv = 't';
				}
				else if ($id_satuan == '7') { // jika satuan awalnya lusin
					$qty_sat_awal = $qty / 12;
					$konv = 't';
				}
				else {
					$qty_sat_awal = $qty;
					$konv = 'f';
				}
				
				// ======== update stoknya! =============
				if ($is_quilting == 't')
					$nama_tabel_stok = "tm_stok_hasil_makloon";
				else
					$nama_tabel_stok = "tm_stok";
				
				//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty_sat_awal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' ");
					}
					
					$selisih = 0;
					$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
					if ($is_quilting == 't')
						$sqlxx.= " kode_brg_quilting = '$kode' ";
					else
						$sqlxx.= " kode_brg = '$kode' ";
					$sqlxx.= " AND quilting = '$is_quilting' ORDER BY id ASC";
					
					// 22-01-2013, utk tujuan=3 (retur) maka order by id DESC. utk tujuan=4 (peminjaman) maka order by id ASC
					// ralat: perubahan order by ini dipending dulu, mau ditanyain ke mindra nanti
						
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx=$queryxx->result();
							
						$temp_selisih = 0;				
						foreach ($hasilxx as $row2) {
							$stoknya = $row2->stok; 
							$harganya = $row2->harga; 
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) 
									$selisih = $stoknya-$qty_sat_awal;
								else
									$selisih = $stoknya+$temp_selisih;
								
								if ($selisih < 0) {
									$temp_selisih = $selisih;
									if ($is_quilting == 'f')
										$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode' AND harga = '$harganya' "); //
									else
										$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg_quilting= '$kode' AND harga = '$harganya' "); //

								}
									
								if ($selisih > 0) {
									if ($temp_selisih == 0)
										$temp_selisih = $qty_sat_awal;
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih;
						if ($is_quilting == 'f')
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode' AND harga = '$harganya' ");
						else
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg_quilting= '$kode' AND harga = '$harganya' ");
					}
				
				// ============================= end 22-12-2012 ============================
				
				// jika semua data tdk kosong, insert ke tm_bonmkeluar_detail
				$data_detail = array(
					'id_bonmkeluar'=>$id_bonm,
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					'konversi_satuan'=>$konv,
					'is_quilting'=>$is_quilting
				);
				$this->db->insert('tm_bonmkeluar_detail',$data_detail);
			} 
		
  }
    
  function delete($kode){
	$tgl = date("Y-m-d");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// cek dulu satuan brgnya. jika satuannya yard, maka qty di detail ini konversikan lagi ke yard
							$query3	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->satuan;
								}
								else {
									$satuan	= '';
								}
								
								if ($satuan == '2') {
									$qty_sat_awal = $row2->qty / 0.90; //meter ke yard 07-03-2012, rubah dari 0.91 ke 0.90
								}
								else if ($satuan == '7') {
									$qty_sat_awal = $row2->qty / 12; // pcs ke lusin
								}
								else
									$qty_sat_awal = $row2->qty;
								
						
						// ============ update stok =====================
						
						//$nama_tabel_stok = "tm_stok";
						if ($row2->is_quilting == 't')
							$nama_tabel_stok = "tm_stok_hasil_makloon";
						else
							$nama_tabel_stok = "tm_stok";
				
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty_sat_awal; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' ");
								}
								
								// ========== 24-12-2012 ==============
								$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
								if ($row2->is_quilting == 't')
									$sqlxx.= " kode_brg_quilting = '$row2->kode_brg' ";
								else
									$sqlxx.= " kode_brg = '$row2->kode_brg' ";
								$sqlxx.= " AND quilting = '$row2->is_quilting' ORDER BY id ASC LIMIT 1";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilrow = $queryxx->row();
									$stok_lama	= $hasilrow->stok;
									$harganya	= $hasilrow->harga;
								}
								// ========== end 24-12-2012 ===========
								
								// 16-02-2012: update stok harga for reset
								/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$row2->kode_brg' 
															AND harga = '$row2->harga' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								} */
								$stokreset = $stok_lama+$qty_sat_awal;
								if ($row2->is_quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg= '$row2->kode_brg' AND harga = '$harganya' "); //
								else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row2->kode_brg' AND harga = '$harganya' "); //
								//&&&&&&& end 16-02-2012 &&&&&&&&&&&
								
						// ==============================================
					} // end foreach
				} // end if
	
	// 2. hapus data bonmkeluar
    $this->db->delete('tm_bonmkeluar_detail', array('id_bonmkeluar' => $kode));
    $this->db->delete('tm_bonmkeluar', array('id' => $kode));
  }
  
  function get_bahan($num, $offset, $cari, $id_gudang, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			if ($is_quilting == 'f')
				$tabelstok = "tm_stok";
			else
				$tabelstok = "tm_stok_hasil_makloon";
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $id_gudang, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
    
  function get_bonm($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluar where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$id_satuan	= $hasilrow->id_satuan;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->is_quilting == 'f')
							$tabelstok = "tm_stok";
						else
							$tabelstok = "tm_stok_hasil_makloon";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'id_satuan'=> $id_satuan,
												'qty'=> $row2->qty,
												'is_quilting'=> $row2->is_quilting,
												'keterangan'=> $row2->keterangan,
												'konversi_satuan'=> $row2->konversi_satuan,
												'jum_stok'=> $jum_stok
											);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->tujuan=='1')
					$nama_tujuan = "Cutting";
				else if ($row1->tujuan=='2')
					$nama_tujuan = "Unit";
				else
					$nama_tujuan = "Lain-Lain";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'tujuan'=> $row1->tujuan,
											'nama_tujuan'=> $nama_tujuan,
											'id_gudang'=> $row1->id_gudang,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  
  function cek_data($no_bonm_manual, $thn1){
    $this->db->select("id from tm_bonmkeluar WHERE no_manual = '$no_bonm_manual' 
				AND extract(year from tgl_bonm) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
}
