<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAll($num, $offset, $cari, $date_from, $date_to) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_bonm) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$this->db->select(" * FROM tm_bonmkeluar WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
									FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$id_satuan	= $hasilrow->id_satuan;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$nama_brg = '';
								$id_satuan = 0;
								$satuan = '';
							}
						}
						else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
								FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$id_satuan	= $hasilrow->id_satuan;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg = '';
							$id_satuan = 0;
							$satuan = '';
						}
										
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'id_satuan'=> $id_satuan,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'konversi_satuan'=> $row2->konversi_satuan,
												'is_quilting'=> $row2->is_quilting
											);
					}
				}
				else {
					$detail_bonm = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				if ($row1->tujuan == '1')
					$nama_tujuan = "Cutting";
				else if ($row1->tujuan == '2')
					$nama_tujuan = "Unit";
				else if ($row1->tujuan == '3')
					$nama_tujuan = "Lain-Lain (Retur)";
				else if ($row1->tujuan == '4')
					$nama_tujuan = "Lain-Lain (Peminjaman)";
				else if ($row1->tujuan == '5')
					$nama_tujuan = "Lain-Lain (Lainnya)";
				else if ($row1->tujuan == '6')
					$nama_tujuan = "Pengadaan";
				else if ($row1->tujuan == '7')
					$nama_tujuan = "Quilting";
				else
					$nama_tujuan = "WIP";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'tujuan'=> $row1->tujuan,
											'nama_tujuan'=> $nama_tujuan,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function getAlltanpalimit($cari, $date_from, $date_to){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_bonm) like UPPER('%$cari%') OR UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_bonm DESC, no_bonm DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluar WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  //
  function save($no_bonm,$tgl_bonm, $no_bonm_manual, $id_gudang, $tujuan, $ket, 
			$is_quilting, $kode, $nama, $qty, $stok, $satuan, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");

	if ($is_quilting == '')
		$is_quilting = 'f';
	
    // cek apa udah ada datanya blm dgn no request tadi
    $this->db->select("id from tm_bonmkeluar WHERE no_bonm = '$no_bonm' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_bonm
			$data_header = array(
			  'no_bonm'=>$no_bonm,
			  'no_manual'=>$no_bonm_manual,
			  'tgl_bonm'=>$tgl_bonm,
			  'tujuan'=>$tujuan,
			  'id_gudang'=>$id_gudang,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_bonmkeluar',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_bonmkeluar
			$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluar ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_bonm	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				if ($is_quilting == '')
					$is_quilting = 'f';
				
				// cek satuan barangnya, jika yard (qty = meter) maka konversi balik ke yard
				$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$kode' ");
				if ($queryxx->num_rows() > 0){
					$hasilrow = $queryxx->row();
					$id_satuan	= $hasilrow->satuan;
				}
				else
					$id_satuan	= '';
						
				//$hasilrow = $queryxx->row();
				//$id_satuan	= $hasilrow->satuan; 
				
				if ($id_satuan == '2') { // jika satuan awalnya yard
					$qty_sat_awal = $qty / 0.90;
					$konv = 't';
				}
				else if ($id_satuan == '7') { // jika satuan awalnya lusin
					$qty_sat_awal = $qty / 12;
					$konv = 't';
				}
				else {
					$qty_sat_awal = $qty;
					$konv = 'f';
				}
				
				// ======== update stoknya! =============
				if ($is_quilting == 't')
					$nama_tabel_stok = "tm_stok_hasil_makloon";
				else
					$nama_tabel_stok = "tm_stok";
				
				//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty_sat_awal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' ");
					}
					
					$selisih = 0;
					$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
					if ($is_quilting == 't')
						$sqlxx.= " kode_brg_quilting = '$kode' ";
					else
						$sqlxx.= " kode_brg = '$kode' ";
					$sqlxx.= " AND quilting = '$is_quilting' ORDER BY id ASC";
					
					// 22-01-2013, utk tujuan=3 (retur) maka order by id DESC. utk tujuan=4 (peminjaman) maka order by id ASC
					// ralat: perubahan order by ini dipending dulu, mau ditanyain ke mindra nanti
						
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx=$queryxx->result();
							
						$temp_selisih = 0;				
						foreach ($hasilxx as $row2) {
							$stoknya = $row2->stok; 
							$harganya = $row2->harga; 
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) 
									$selisih = $stoknya-$qty_sat_awal;
								else
									$selisih = $stoknya+$temp_selisih;
								
								if ($selisih < 0) {
									$temp_selisih = $selisih;
									if ($is_quilting == 'f')
										$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode' AND harga = '$harganya' "); //
									else
										$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg_quilting= '$kode' AND harga = '$harganya' "); //

								}
									
								if ($selisih > 0) {
									if ($temp_selisih == 0)
										$temp_selisih = $qty_sat_awal;
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih;
						if ($is_quilting == 'f')
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg= '$kode' AND harga = '$harganya' ");
						else
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
								where kode_brg_quilting= '$kode' AND harga = '$harganya' ");
					}
				
				// ============================= end 22-12-2012 ============================
				
				// jika semua data tdk kosong, insert ke tm_bonmkeluar_detail
				$data_detail = array(
					'id_bonmkeluar'=>$id_bonm,
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					'konversi_satuan'=>$konv,
					'is_quilting'=>$is_quilting
				);
				$this->db->insert('tm_bonmkeluar_detail',$data_detail);
			} 
		
  }
    
  function delete($kode){
	$tgl = date("Y-m-d");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// cek dulu satuan brgnya. jika satuannya yard, maka qty di detail ini konversikan lagi ke yard
							$query3	= $this->db->query(" SELECT satuan FROM tm_barang WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->satuan;
								}
								else {
									$satuan	= '';
								}
								
								if ($satuan == '2') {
									$qty_sat_awal = $row2->qty / 0.90; //meter ke yard 07-03-2012, rubah dari 0.91 ke 0.90
								}
								else if ($satuan == '7') {
									$qty_sat_awal = $row2->qty / 12; // pcs ke lusin
								}
								else
									$qty_sat_awal = $row2->qty;
								
						
						// ============ update stok =====================
						
						//$nama_tabel_stok = "tm_stok";
						if ($row2->is_quilting == 't')
							$nama_tabel_stok = "tm_stok_hasil_makloon";
						else
							$nama_tabel_stok = "tm_stok";
				
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$qty_sat_awal; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert($nama_tabel_stok, $data_stok);
								}
								else {
									$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' ");
								}
								
								// ========== 24-12-2012 ==============
								$sqlxx = "SELECT id, stok, harga FROM tm_stok_harga WHERE ";
								if ($row2->is_quilting == 't')
									$sqlxx.= " kode_brg_quilting = '$row2->kode_brg' ";
								else
									$sqlxx.= " kode_brg = '$row2->kode_brg' ";
								$sqlxx.= " AND quilting = '$row2->is_quilting' ORDER BY id ASC LIMIT 1";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilrow = $queryxx->row();
									$stok_lama	= $hasilrow->stok;
									$harganya	= $hasilrow->harga;
								}
								// ========== end 24-12-2012 ===========
								
								// 16-02-2012: update stok harga for reset
								/*$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$row2->kode_brg' 
															AND harga = '$row2->harga' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								} */
								$stokreset = $stok_lama+$qty_sat_awal;
								if ($row2->is_quilting == 'f')
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg= '$row2->kode_brg' AND harga = '$harganya' "); //
								else
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row2->kode_brg' AND harga = '$harganya' "); //
								//&&&&&&& end 16-02-2012 &&&&&&&&&&&
								
						// ==============================================
					} // end foreach
				} // end if
	
	// 2. hapus data bonmkeluar
    $this->db->delete('tm_bonmkeluar_detail', array('id_bonmkeluar' => $kode));
    $this->db->delete('tm_bonmkeluar', array('id' => $kode));
  }
  
  function get_bahan($num, $offset, $cari, $id_gudang, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			if ($is_quilting == 'f')
				$tabelstok = "tm_stok";
			else
				$tabelstok = "tm_stok_hasil_makloon";
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $id_gudang, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
    
  function get_bonm($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluar where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluar_detail WHERE id_bonmkeluar = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						if ($row2->is_quilting == 'f') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.id as id_satuan, b.nama as nama_satuan 
										FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						}
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$id_satuan	= $hasilrow->id_satuan;
						$satuan	= $hasilrow->nama_satuan;
						
						if ($row2->is_quilting == 'f')
							$tabelstok = "tm_stok";
						else
							$tabelstok = "tm_stok_hasil_makloon";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'id_satuan'=> $id_satuan,
												'qty'=> $row2->qty,
												'is_quilting'=> $row2->is_quilting,
												'keterangan'=> $row2->keterangan,
												'konversi_satuan'=> $row2->konversi_satuan,
												'jum_stok'=> $jum_stok
											);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				/*if ($row1->tujuan=='1')
					$nama_tujuan = "Cutting";
				else if ($row1->tujuan=='2')
					$nama_tujuan = "Unit";
				else
					$nama_tujuan = "Lain-Lain"; */
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'tujuan'=> $row1->tujuan,
											//'nama_tujuan'=> $nama_tujuan,
											'id_gudang'=> $row1->id_gudang,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  
  function cek_data($no_bonm_manual, $thn1){
    $this->db->select("id from tm_bonmkeluar WHERE no_manual = '$no_bonm_manual' 
				AND extract(year from tgl_bonm) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 21-09-2013, modif 27-01-2014
  function getbarangjaditanpalimit($cari){
  	/*if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')
						OR UPPER(c.kode_brg) LIKE UPPER('%$cari%') OR UPPER(c.nama_brg) LIKE UPPER('%$cari%') )";
	}
	$sql = "SELECT a.kode_brg_jadi, b.e_product_motifname, a.kode_brg, c.nama_brg, a.stok FROM tm_stok_hasil_cutting a, tr_product_motif b, tm_barang c 
			WHERE a.kode_brg_jadi = b.i_product_motif 
			AND a.kode_brg = c.kode_brg AND b.n_active='1' AND c.status_aktif = 't' ".$pencarian." ORDER BY a.kode_brg_jadi ASC";
  	$query	= $this->db->query($sql);
	return $query->result(); */
	
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  // modif 27-01-2014
  function getbarangjadi($num,$offset, $cari){
  	/*if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(b.i_product_motif) LIKE UPPER('%$cari%') OR UPPER(b.e_product_motifname) LIKE UPPER('%$cari%')
						OR UPPER(c.kode_brg) LIKE UPPER('%$cari%') OR UPPER(c.nama_brg) LIKE UPPER('%$cari%') )";
	}
	$sql = " a.kode_brg_jadi, b.e_product_motifname, a.kode_brg, c.nama_brg, a.stok FROM tm_stok_hasil_cutting a, tr_product_motif b, tm_barang c 
			WHERE a.kode_brg_jadi = b.i_product_motif 
			AND a.kode_brg = c.kode_brg AND b.n_active='1' AND c.status_aktif = 't' ".$pencarian." ORDER BY a.kode_brg_jadi ASC";	
	
	$this->db->select($sql, false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result();  */
    
    // new 27-01-2014
    if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row1->i_product_motif' ");
									
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
				if ($stok == '')
					$stok = 0;
			}
			else
				$stok = 0;
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname,
										'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  // 23-09-2013, modif 27-01-2014
  function getAllbonmkeluarcutting($num, $offset, $cari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND (UPPER(a.no_bonm) like UPPER('%$cari%') OR UPPER(a.no_manual) like UPPER('%$cari%'))";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit = '$kode_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_bonm DESC, a.no_bonm DESC ";
		
		$this->db->select(" distinct a.* FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b, tr_product_motif c 
					WHERE a.id=b.id_bonmkeluarcutting AND b.kode_brg_jadi = c.i_product_motif ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(a.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(b.e_product_motifname) like UPPER('%$caribrg%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_bonmkeluarcutting_detail a, tr_product_motif b  
									WHERE a.kode_brg_jadi=b.i_product_motif AND a.id_bonmkeluarcutting = '$row1->id' ".$pencarian2
									." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname 
									FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg_jadi	= $hasilrow->e_product_motifname;
							}
							else {
								$nama_brg_jadi = '';
							}
							
							// 30-01-2014, detail qty warna -------------
							$strwarna = "";
							$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_bonmkeluarcutting_detail_warna a, tm_warna c
										WHERE a.kode_warna = c.kode
										AND a.id_bonmkeluarcutting_detail = '$row2->id' ");
							if ($queryxx->num_rows() > 0){
								$hasilxx=$queryxx->result();
								$strwarna.= $row2->kode_brg_jadi.": <br>";
								foreach ($hasilxx as $rowxx) {
									$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
								}
							}
							//-------------------------------------------
										
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_bonm = '';
				}
																
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar Bagus";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Pengembalian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else
					$nama_unit_jahit = "";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit_jahit,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function getAllbonmkeluarcuttingtanpalimit($cari, $date_from, $date_to, $kode_unit_jahit, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND (UPPER(a.no_bonm) like UPPER('%$cari%') OR UPPER(a.no_manual) like UPPER('%$cari%'))";
		
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
		if ($kode_unit_jahit != "0")
			$pencarian.= " AND a.kode_unit = '$kode_unit_jahit' ";
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(b.kode_brg_jadi) like UPPER('%$caribrg%') OR UPPER(c.e_product_motifname) like UPPER('%$caribrg%')) ";
	  
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b, tr_product_motif c 
					WHERE a.id=b.id_bonmkeluarcutting AND b.kode_brg_jadi = c.i_product_motif ".$pencarian);

    return $query->result();  
  }
  
  function cek_data_bonmkeluarcutting($no_bonm_manual, $thn1, $kode_unit_jahit, $jenis){
    $this->db->select("id from tm_bonmkeluarcutting WHERE no_manual = '$no_bonm_manual' 
				AND extract(year from tgl_bonm) = '$thn1' AND kode_unit='$kode_unit_jahit'
				AND jenis_keluar = '$jenis' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savebonmkeluarcutting($id_bonm, $jenis, $kode_unit_jahit,
			$kode_brg_jadi, $temp_qty, $kode_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
    // cek apa udah ada datanya blm dgn no bon M tadi
   /* $this->db->select("id from tm_bonmkeluarcutting WHERE no_bonm = '$no_bonm' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_bonmkeluarcutting
			$data_header = array(
			  'no_bonm'=>$no_bonm,
			  'no_manual'=>$no_bonm_manual,
			  'tgl_bonm'=>$tgl_bonm,
			  'jenis_keluar'=>$jenis,
			  'kode_unit'=>$kode_unit_jahit,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_bonmkeluarcutting',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_bonmkeluarcutting
			$query2	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_bonm	= $hasilrow->id; */
			
			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx=0; $xx<count($kode_warna); $xx++) {
				$kode_warna[$xx] = trim($kode_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal+= $qty_warna[$xx];
			} // end for
			
			// ======== update stoknya! =============
			//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$kode_brg_jadi' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode_brg_jadi,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
						
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode_brg_jadi' ");
					}
					
					//----------------------- 03-02-2014 ---------------------------------------------------
					//update stok unit jahit
					//if ($kode_unit_jahit != '0') {
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode_brg_jadi'
								AND kode_unit='$kode_unit_jahit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_lama = 0;
							$stok_unit_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unit_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit = $stok_unit_lama+$qtytotal; // bertambah stok karena masuk ke unit
						$new_stok_unit_bagus = $stok_unit_bagus_lama+$qtytotal;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'kode_brg_jadi'=>$kode_brg_jadi,
								'kode_unit'=>$kode_unit_jahit,
								'stok'=>$new_stok_unit,
								'stok_bagus'=>$new_stok_unit_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
							stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
							where kode_brg_jadi= '$kode_brg_jadi' AND kode_unit='$kode_unit_jahit' ");
						}
					//} // end if
					//-------------------------------------------------------------------------------------	
							
				// ======== update stoknya! =============
				// modif 27-01-2014: sementara dikomen dulu karena fitur bon M masuk hasil cutting blm jalan
				// dan nanti stok hasil cutting ini di tabel baru aja per brg jadi, ga ada acuan ke bhn baku
				// revisi 04-02-2014: skrip yg dikomen ini udh ga dipake, skrg pake skrip update stok terbaru diatas
				
				//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
			/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting 
										WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode_brg_jadi,
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode_brg_jadi' AND kode_brg= '$kode' ");
					}
				*/
				// jika semua data tdk kosong, insert ke tm_bonmkeluarcutting_detail
				$data_detail = array(
					'id_bonmkeluarcutting'=>$id_bonm,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				);
				$this->db->insert('tm_bonmkeluarcutting_detail',$data_detail);
				
				// ambil id detail tm_bonmkeluarcutting_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// ----------------------------------------------
				for ($xx=0; $xx<count($kode_warna); $xx++) {
					$kode_warna[$xx] = trim($kode_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmkeluarcutting_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_bonmkeluarcutting_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_bonmkeluarcutting_detail'=>$iddetail,
						 'id_warna_brg_jadi'=>'0',
						 'kode_warna'=>$kode_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_bonmkeluarcutting_detail_warna',$tm_bonmkeluarcutting_detail_warna);
					
					// ========================= 04-02-2014, stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_cutting'=>$id_stok,
							'id_warna_brg_jadi'=>'0',
							'kode_warna'=>$kode_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
					}
					
					// ====================	15-03-2014, stok hasil cutting bhn baku ==================
					//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$kode_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					if ($query3->num_rows() == 0){
						//$stok_warna_lama = 0;
						// ambil data2 material brg jadi berdasarkan brg jadi, trus insert langsung ke tabel stok_hasil_cutting_bhnbaku. qty-nya sesuai dgn warnanya
						$sqlxx = " SELECT * FROM tm_material_brg_jadi WHERE kode_brg_jadi='$kode_brg_jadi' AND kode_warna='".$kode_warna[$xx]."' ";
						$queryxx = $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->result();
							foreach ($hasilxx as $rowxx) {			
								$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_bhnbaku ORDER BY id DESC LIMIT 1 ");
								if($seq_stokwarna->num_rows() > 0) {
									$seq_stokwarnarow	= $seq_stokwarna->row();
									$id_stok_warna	= $seq_stokwarnarow->id+1;
								}else{
									$id_stok_warna	= 1;
								}
								
								$data_stok = array(
									'id'=>$id_stok_warna,
									'id_stok_hasil_cutting'=>$id_stok,
									'kode_brg'=>$rowxx->kode_brg,
									'kode_warna'=>$kode_warna[$xx],
									'stok'=>$qty_warna[$xx],
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_cutting_bhnbaku', $data_stok);
							} // end xxx
						} // end if
						
					}
					else {
						//$hasilrow = $query3->row();
						//$stok_warna_lama	= $hasilrow->stok;
						$hasil3 = $query3->result();
						foreach ($hasil3 as $row3) {	
							$new_stok_bhnbaku = $row3->stok-$qty_warna[$xx];
							$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
									where id= '".$row3->id."' ");
						}
					} // end if
					// ==========================================================
					
					// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit
					//if ($kode_unit_jahit != '0') {
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warna_bagus_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama+$qty_warna[$xx]; // bertambah stok karena masuk ke unit
						$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_bagus'=>$new_stok_unit_warna_bagus,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
				//	} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
					
				} // end for
				// ----------------------------------------------
  }
  
  function deletebonmkeluarcutting($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
			/* SELECT a.id_gudang, a.kode_unit_jahit, b.* 
								 FROM tm_sjkeluarwip_detail b, tm_sjkeluarwip a 
								 WHERE a.id = b.id_sjkeluarwip AND b.id_sjkeluarwip = '$kode' */
				$query2	= $this->db->query(" SELECT a.kode_unit, b.*  FROM tm_bonmkeluarcutting_detail b, 
							tm_bonmkeluarcutting a WHERE a.id = b.id_bonmkeluarcutting AND b.id_bonmkeluarcutting = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok pabrik =====================
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
						
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi' ");
									
						// ================= update stok unit jahit ==========================
					//	if ($row2->kode_unit_jahit != '0') {
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_bagus FROM tm_stok_unit_jahit
										 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$row2->kode_unit' ");
							if ($query3->num_rows() == 0){
								$stok_unit_lama = 0;
								$id_stok_unit = 0;
								$stok_unit_bagus_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unit_bagus_lama = $hasilrow->stok_bagus;
							}
							$new_stok_unit = $stok_unit_lama-$row2->qty; // berkurang stok karena reset dari bon M keluar
							$new_stok_unit_bagus = $stok_unit_bagus_lama-$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_bagus = '$new_stok_unit_bagus', tgl_update_stok = '$tgl' 
										WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
										AND kode_unit = '$row2->kode_unit' ");
					//	} // end if update stok unit jahit
						
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_bonmkeluarcutting_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail_warna 
												WHERE id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari bon M keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ====================	15-03-2014, stok hasil cutting bhn baku ==================
								//cek stok terakhir tm_stok_hasil_cutting_bhnbaku, dan update stoknya
								$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting_bhnbaku WHERE kode_warna = '".$rowwarna->kode_warna."'
										AND id_stok_hasil_cutting='$id_stok' ");
								if ($query3->num_rows() > 0){
									$hasil3 = $query3->result();
									foreach ($hasil3 as $row3) {	
										$new_stok_bhnbaku = $row3->stok+$rowwarna->qty;
										$this->db->query(" UPDATE tm_stok_hasil_cutting_bhnbaku SET stok = '$new_stok_bhnbaku', tgl_update_stok = '$tgl' 
												where id= '".$row3->id."' ");
									}
								}
								// ==========================================================
								
								// ============= update stok unit jahit
							//	if ($row2->kode_unit_jahit != '0') {
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_bagus FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warna_bagus_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warna_bagus_lama = $hasilrow->stok_bagus;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama-$rowwarna->qty; // berkurang stok di unit karena reset dari bon M keluar
									$new_stok_unit_warna_bagus = $stok_unit_warna_bagus_lama-$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												stok_bagus = '$new_stok_unit_warna_bagus', tgl_update_stok = '$tgl' 
												WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
							//	} // end if kode_unit_jahit != 0
							}
						} // end if detail warna
						
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_bonmkeluarcutting_detail_warna 04-02-2014
						$this->db->query(" DELETE FROM tm_bonmkeluarcutting_detail_warna WHERE id_bonmkeluarcutting_detail='".$row2->id."' ");
						
						// ============ update stok =====================				
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						// modif 27-01-2014, sementara stok hasil cutting blm dipake
						/*		$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting 
												WHERE kode_brg = '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset dari bon M keluar
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'kode_brg_jadi'=>$row2->kode_brg_jadi,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_cutting', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
												where kode_brg= '$row2->kode_brg' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
								}	*/
						// ==============================================						
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_bonmkeluarcutting
    $this->db->delete('tm_bonmkeluarcutting_detail', array('id_bonmkeluarcutting' => $id));
    $this->db->delete('tm_bonmkeluarcutting', array('id' => $id));
  }
  
  function get_bonmkeluarcutting($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmkeluarcutting_detail WHERE id_bonmkeluarcutting = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$stok	= $hasilrow->stok;
						else
							$stok = 0;
						
						$query4	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if($query4->num_rows()>0){
							$hasilrow2	= $query4->row();
							$nama_brg_jadi	= $hasilrow2->e_product_motifname;
						}else{
							$nama_brg_jadi	= '';
						}
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_warna dan tm_bonmkeluarcutting_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.kode_warna, b.nama, c.qty FROM tm_warna b,
												tm_bonmkeluarcutting_detail_warna c
												 WHERE c.kode_warna=b.kode
												 AND c.id_bonmkeluarcutting_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$kode_warna = $row3->kode_warna;
								
								$detail_warna[] = array(	'kode_warna'=> $kode_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else
							$detail_warna = '';
						//-------------------------------------------------------------------------------------
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'stok'=> $stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->jenis_keluar=='1')
					$nama_jenis = "Keluar Bagus";
				else if ($row1->jenis_keluar=='2')
					$nama_jenis = "Pengembalian Retur";
				else
					$nama_jenis = "Lain-Lain";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_jahit	= $hasilrow->nama;
				}
				else
					$nama_unit_jahit = "";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit_jahit,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  // 27-01-2014
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
}
