<?php
class Mmaster extends CI_Model {
  function __construct() { 

  parent::__construct();

}
  
  /*function barangjdi($num, $offset, $cari) {
   	if($cari!='0' && $cari!=''){
		$pencarian	= " WHERE (i_product_motif LIKE '%$cari%' OR e_product_motifname LIKE '%$cari%') ";
	} else {
		$pencarian	= "";
	} 
  	$query	= $this->db->select(" * FROM tr_product_motif ".$pencarian." ORDER BY i_product_motif ASC ")->limit($num,$offset);
	$query = $this->db->get();
	
	return $query->result();
  }
  
  function barangjditanpalimit($cari) {
  	if($cari!='0' && $cari!=''){
		$pencarian	= " WHERE (i_product_motif LIKE '%$cari%' OR e_product_motifname LIKE '%$cari%')";
	} else {
		$pencarian	= "";
	}	
  	return $this->db->query(" SELECT * FROM tr_product_motif ".$pencarian." ORDER BY i_product_motif ASC ");
  } */
  
  function thnschedule() {
  	return $this->db->query(" SELECT substring(no_schedule,4,4) AS tahun FROM tt_schedule_cutting ORDER BY id DESC LIMIT 1 ");
  }

  function nomorschedule() {
  	return $this->db->query(" SELECT substring(no_schedule,8,5) AS nomor FROM tt_schedule_cutting ORDER BY id DESC LIMIT 1 ");
  }

  function getAll($num, $offset, $cari) {
		if($cari!='0'){
			$pencarian	= " WHERE UPPER(no_schedule) like UPPER('%$cari%') ORDER BY tgl_cutting DESC ";
		}else{
			$pencarian	= " ORDER BY tgl_cutting DESC ";
		}
		
		$this->db->select(" * FROM tt_schedule_cutting ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_fb = array();
		$detail_fb = array();
		
		if ($query->num_rows() > 0){
			
			$hasil = $query->result();
			
			$j	= 0;
			foreach ($hasil as $row1) {
			
				//$x	= 0;
				$query2	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id_schedule_cutting='$row1->id' ");
				
				foreach($query2->result() as $row2) {

					$qbrgjadi	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
					$rbrgjadi	= $qbrgjadi->row();

					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
									WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
					if ($query3->num_rows()>0){
						$hasilrow = $query3->row();
						$kode_brg	= $row2->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$satuan		= $hasilrow->nama_satuan;
					} 
					else {
						$kode_brg = '';
						$nama_brg = '';
						$satuan = '';
					}
					
					if ($row2->kode_brg_quilting != '') {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
											WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg_quilting' ");
						if ($query3->num_rows()>0){
							$hasilrow = $query3->row();
							$kode_brg_quilting	= $row2->kode_brg;
							$nama_brg_quilting	= $hasilrow->nama_brg;
							$satuan_quilting	= $hasilrow->nama_satuan;
						}
					}
					else {
						$kode_brg_quilting	= '';
						$nama_brg_quilting = '';
						$satuan_quilting = '';
					}
					
					if ($row2->id_bagian_brg_jadi != '') {
						$query3	= $this->db->query(" SELECT nama_bagian FROM tm_bagian_brg_jadi
											WHERE id ='$row2->id_bagian_brg_jadi' ");
						if ($query3->num_rows()>0){
							$hasilrow = $query3->row();
							$nama_bagian_brg_jadi	= $hasilrow->nama_bagian;
						}
					}
					else
						$nama_bagian_brg_jadi = '';
									
					$detail_fb[] = array('id_schedule_cutting' => $row2->id_schedule_cutting,
									'id_pb_cutting' => $row2->id_pb_cutting,
									'kode_brg_jadi' => $row2->kode_brg_jadi,
									'nm_brg_jadi' => $rbrgjadi->e_product_motifname,
									'kode_brg' => $kode_brg,
									'nama_brg' => $nama_brg,
									'kode_brg_quilting' => $kode_brg_quilting,
									'nama_brg_quilting' => $nama_brg_quilting,
									'qty_bhn' => $row2->qty_bhn,
									'jam_mulai' => $row2->jam_mulai,
									'jam_selesai' => $row2->jam_selesai,
									'operator_cutting' => $row2->operator_cutting,
									'status_realisasi' => $row2->status_realisasi,
									'id_bagian_brg_jadi' => $row2->id_bagian_brg_jadi,
									'nama_bagian_brg_jadi' => $nama_bagian_brg_jadi,
									'ukuran_pola' => $row2->ukuran_pola);
					//$x+=1;						
				}
				// 0 (sunday) to 6 (saturday)
				//$nama_hari = date("w", $row1->tgl_cutting);
				// mktime(hour,minute,second,month,day,year,is_dst) 
				$pisah1 = explode("-", $row1->tgl_cutting);
				$tgl1	= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tglnya = mktime(0,0,0,$bln1,$tgl1,$thn1);
				$hari = date("w", $tglnya);
				
				if ($hari == 0)
					$nama_hari = "Minggu";
				else if ($hari == 1)
					$nama_hari = "Senin";
				else if ($hari == 2)
					$nama_hari = "Selasa";
				else if ($hari == 3)
					$nama_hari = "Rabu";
				else if ($hari == 4)
					$nama_hari = "Kamis";
				else if ($hari == 5)
					$nama_hari = "Jumat";
				else
					$nama_hari = "Sabtu";
				//echo $nama_hari."<br>";
				$data_fb[] = array('id'=> $row1->id,	
							'no_schedule'=> $row1->no_schedule,
							'tgl_cutting'=> $row1->tgl_cutting,
							'nama_hari'=> $nama_hari,
							'is_dacron'=> $row1->is_dacron,
							'detail_fb'=> $detail_fb,
							'status_realisasi' => $row1->status_realisasi );
				//$j+=1;					
				$detail_fb = array();
			}
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($cari){
 	if($cari!='0'){
		$pencarian	= " WHERE UPPER(no_schedule) like '%$cari%' ";
	}else{
		$pencarian	= "";
	}		 
	$query	= $this->db->query(" SELECT * FROM tt_schedule_cutting ".$pencarian." ");
    return $query->result();  
  }
      
  function cek_data($noschedule){
    return $this->db->query(" SELECT * from tt_schedule_cutting WHERE no_schedule='$noschedule' ", false);
  }
  
  //
  function save($eksternal, $non_bonm, $noschedule, $tglschedule, $no_pb_cutting, $nmbrgjadi, $imotif, $qty, $plan_qty, $qty_pjg_kain, $nmbrg, $kode, 
			$diprint, $kode_brg_quilting, $jammulai, $jamselesai, $keterangan, $operator, $id_detail, 
			$idapplystok_detail,$jumlah_input){
    $tgl = date("Y-m-d");
	
    $this->db->select(" * from tt_schedule_cutting WHERE no_schedule='$noschedule' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
	
	$qid_sch_cutting	= $this->db->query(" select id+1 as id from tt_schedule_cutting order by id desc limit 1 ");
	if($qid_sch_cutting->num_rows()>0){
		$rid	= $qid_sch_cutting->row();
		$iid	= $rid->id;
	}else{
		$iid	= 1;
	}
	
	$tgl = date("Y-m-d");
	
	if(count($hasil)==0) {
		$data_header = array(
			'id'=>$iid,
			'no_schedule'=>$noschedule,
			'tgl_cutting'=>$tglschedule,
			'tgl_input'=>$tgl,
			'tgl_update'=>$tgl,
			'for_eksternal'=>$eksternal
			);		
		//print_r($data_header); die();
		
		$this->db->insert('tt_schedule_cutting',$data_header);
	}
	
	$data_detail	= array();
	
	for($x=0;$x<=$jumlah_input;$x++){
		if($imotif[$x]!='' && $qty[$x]!='' && $nmbrg[$x]!='' && $kode[$x]!='' && $jammulai[$x]!='' && $jamselesai[$x]!=''){
			// insert ke tt_schedule_cutting_detail dipindah kebawah, setelah skrip stok_harga
			if ($id_detail[$x] != 0) {
				// 16-01-2012,cek apakah sum qty_bhn di schedule_cutting = yg di pemenuhan. kalo udh =, maka update status cutting = 't'
				$query3 = $this->db->query(" select sum(qty_bhn) as jum FROM tt_schedule_cutting_detail 
								WHERE id_apply_stok_detail = '$id_detail[$x]' ");
				if ($query3->num_rows()>0){
					$hasilrow = $query3->row();
					$jum_dischedule = $hasilrow->jum;
				}
				else
					$jum_dischedule = 0;
				
				$selisihnya = $plan_qty[$x] - $jum_dischedule;
				if ($selisihnya <= 0)
					$this->db->query(" UPDATE tm_apply_stok_proses_cutting_detail SET status_cutting='t' WHERE id='$id_detail[$x]' ");
				
				//cek di tabel tm_apply_stok_proses_cutting_detail, apakah status_cutting sudah 't' semua?
				$this->db->select("id from tm_apply_stok_proses_cutting_detail WHERE status_cutting = 'f' 
							AND id_apply_stok = '$idapplystok_detail[$x]' ", false);
				$query = $this->db->get();
				//jika sudah t semua, maka update tabel tm_apply_stok_proses_cutting di field status_cutting menjadi t
				if ($query->num_rows() == 0){
					$this->db->query(" UPDATE tm_apply_stok_proses_cutting SET status_cutting='t' WHERE id='$idapplystok_detail[$x]' ");
				}
				
				$harganya = 0;
				//16-02-2012, kurangi stok bhn quilting
				if ($kode_brg_quilting[$x] != '') {
					//cek stok terakhir tm_stok_hasil_makloon, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode_brg_quilting[$x]' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					// 29-03-2012, masih meragukan....tp setelah ditelusuri, ternyata udh bener
					$qty_quilting_m = $qty_pjg_kain[$x];
					// end
					
					$qty_sat_awal = $qty_quilting_m / 0.91; // konversi dari meter ke satuan awalnya utk quilting (yard)
					$new_stok = $stok_lama-$qty_sat_awal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg_quilting[$x],
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_makloon', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg_quilting[$x]' ");
					}
					
					// warning 16-02-2012: tabel tt_stok udh ga akan dipake lagi utk rekap stok
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE 
											kode_brg_quilting = '$kode_brg_quilting[$x]' AND quilting = 't' ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; // data ke-1: 500		data ke-2: 10
							$harganya = $row2->harga; // data ke-1: 20000	data ke-2: 370000
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) // temp_selisih = -6
									$selisih = $stoknya-$qty_sat_awal; // selisih1 = 500-506 = -6
								else
									$selisih = $stoknya+$temp_selisih; // selisih2 = 10-6 = 4
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; // temp_selisih = -6
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg_quilting= '$kode_brg_quilting[$x]' AND harga = '$harganya' "); //
									
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $qty_sat_awal; // temp_selisih = -6
										
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih; // new_stok = 4
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg_quilting= '$kode_brg_quilting[$x]' AND harga = '$harganya' ");
					}
				} // end if pengurangan stok bhn quilting
				// &&&&&&& end 17-02-2012
			} // end if id_detail !=0
			
			//insert ke tt_schedule_cutting_detail
			if ($kode_brg_quilting[$x] != '')
				$qty_quilting_m = $qty_pjg_kain[$x];
			else
				$qty_quilting_m = 0;
			
			$data_detail[$x] = array(
				'id_schedule_cutting'=>$iid,
				'id_pb_cutting'=>$no_pb_cutting[$x],
				'kode_brg_jadi'=>$imotif[$x],
				'diprint'=>$diprint[$x],
				'kode_brg'=>$kode[$x],
				'kode_brg_quilting'=>$kode_brg_quilting[$x],
				'qty_bhn'=>$qty[$x],
				'jam_mulai'=>$jammulai[$x],
				'jam_selesai'=>$jamselesai[$x],
				'operator_cutting'=>$operator[$x],
				'keterangan'=>$keterangan[$x],
				'id_apply_stok_detail'=>$id_detail[$x],
				'qty_quilting_m'=>$qty_quilting_m,
				'harga_quilting'=>$harganya
			);
			$this->db->insert('tt_schedule_cutting_detail',$data_detail[$x]);
		}
	} // end for
  }
  
  function savedacron($eksternal, $noschedule, $tglschedule, $kode_brg_jadi, $qty, $kode_bhn_baku,
					$jammulai, $jamselesai, $bagian_brg_jadi, $uk_pola, $ket, $jumlah_input){
    $tgl = date("Y-m-d");
	
    $this->db->select(" * from tt_schedule_cutting WHERE no_schedule='$noschedule' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
    
    $tgl = date("Y-m-d");
		
	if(count($hasil)==0) {
		$data_header = array(
			'no_schedule'=>$noschedule,
			'tgl_cutting'=>$tglschedule,
			'is_dacron'=>'t',
			'tgl_input'=>$tgl,
			'tgl_update'=>$tgl,
			'for_eksternal'=>$eksternal
			);
		$this->db->insert('tt_schedule_cutting',$data_header);
	}
	
	$qid_sch_cutting	= $this->db->query(" select id from tt_schedule_cutting order by id desc limit 1 ");
	if($qid_sch_cutting->num_rows()>0){
		$rid	= $qid_sch_cutting->row();
		$iid	= $rid->id;
	}else{
		$iid	= 1;
	}
	
	$data_detail	= array();
	
	for($x=1;$x<=$jumlah_input;$x++){
		if($kode_brg_jadi[$x]!='' && $qty[$x]!=''){
			$data_detail[$x] = array(
				'id_schedule_cutting'=>$iid,
				'kode_brg_jadi'=>$kode_brg_jadi[$x],
				'kode_brg'=>$kode_bhn_baku,
				'qty_bhn'=>$qty[$x],
				//'qty_dacron'=>$qty_bhn[$x],
				'jam_mulai'=>$jammulai[$x],
				'jam_selesai'=>$jamselesai[$x],
				//'operator_cutting'=>$operator[$x],
				'keterangan'=>$ket[$x],
				'ukuran_pola'=>$uk_pola[$x],
				'id_bagian_brg_jadi'=>$bagian_brg_jadi[$x]
			);
			$this->db->insert('tt_schedule_cutting_detail',$data_detail[$x]);
						
		}
	}
  }
  
  // 19-01-2012
  function saverealisasi($eksternal, $noschedule, $tglschedule, $nmbrgjadi, $imotif, $qty, 
								$nmbrg, $kode, $diprint, $kode_brg_quilting, $jammulai, $jamselesai, 
								$keterangan, $operator, $id_realisasi_detail, $id_realisasi_cutting, 
								$qty_schedule, $qty_potong, $jumlah_input){
    $tgl = date("Y-m-d");
	
    $this->db->select(" * from tt_schedule_cutting WHERE no_schedule='$noschedule' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
	
	$qid_sch_cutting	= $this->db->query(" select id+1 as id from tt_schedule_cutting order by id desc limit 1 ");
	if($qid_sch_cutting->num_rows()>0){
		$rid	= $qid_sch_cutting->row();
		$iid	= $rid->id;
	}else{
		$iid	= 1;
	}
	
	$tgl = date("Y-m-d");
	
	if(count($hasil)==0) {
		$data_header = array(
			'id'=>$iid,
			'no_schedule'=>$noschedule,
			'tgl_cutting'=>$tglschedule,
			'tgl_input'=>$tgl,
			'tgl_update'=>$tgl,
			'is_realisasi'=>'t',
			'for_eksternal'=>$eksternal
			);		
		
		$this->db->insert('tt_schedule_cutting',$data_header);
	}
	
	$data_detail	= array();
	
	for($x=0;$x<=$jumlah_input;$x++){
		if($imotif[$x]!='' && $qty[$x]!='' && $nmbrg[$x]!='' && $kode[$x]!='' && $jammulai[$x]!='' && $jamselesai[$x]!=''){
			$data_detail[$x] = array(
				'id_schedule_cutting'=>$iid,
				'kode_brg_jadi'=>$imotif[$x],
				'diprint'=>$diprint[$x],
				'kode_brg'=>$kode[$x],
				'kode_brg_quilting'=>$kode_brg_quilting[$x],
				'qty_bhn'=>$qty[$x],
				'jam_mulai'=>$jammulai[$x],
				'jam_selesai'=>$jamselesai[$x],
				'operator_cutting'=>$operator[$x],
				'keterangan'=>$keterangan[$x],
				'id_realisasi_detail'=>$id_realisasi_detail[$x]
			);
			
			$this->db->insert('tt_schedule_cutting_detail',$data_detail[$x]);
			
			// update status_beres = 't' di realisasi_cutting_detail
			$this->db->query(" UPDATE tt_realisasi_cutting_detail SET status_beres = 't' WHERE id = '$id_realisasi_detail[$x]' ");
			
			//cek di tabel tt_realisasi_cutting_detail, apakah status_beres sudah 't' semua?
			$this->db->select("id from tt_realisasi_cutting_detail WHERE status_beres = 'f' 
						AND id_realisasi_cutting = '$id_realisasi_cutting[$x]' ", false);
			$query = $this->db->get();
			//jika sudah t semua, maka update tabel tt_realisasi_cutting di field status_beres menjadi t
			if ($query->num_rows() == 0){
				$this->db->query(" UPDATE tt_realisasi_cutting SET status_beres='t' WHERE id='$id_realisasi_cutting[$x]' ");
			}

		}
	} // end for
  }
    
  function delete($kode){    
	$tgl = date("Y-m-d");
	$q	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id_schedule_cutting='$kode' ");
	if($q->num_rows()>0){
		foreach($q->result() as $row){
			$id_detail	= $row->id_apply_stok_detail;
			
			if ($id_detail != '' || $id_detail != '0') {
				$q2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting_detail WHERE id='$id_detail' ");
				if($q2->num_rows()>0){
					foreach($q2->result() as $row2){
						$q3	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting WHERE id='$row2->id_apply_stok' ");
						$row3	= $q3->row();
						
						// proses update status tm_apply_stok_proses_cutting, tm_apply_stok_proses_cutting_detail
						$this->db->query(" UPDATE tm_apply_stok_proses_cutting SET status_cutting='f' WHERE id='$row3->id' ");
					}
					$this->db->query(" UPDATE tm_apply_stok_proses_cutting_detail SET status_cutting='f' WHERE id='$id_detail' ");
				}
				
				// 17-02-2012
				// ============ update stok utk bhn quilting =====================
				if ($row->kode_brg_quilting != '') {
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon 
											WHERE kode_brg = '$row->kode_brg_quilting' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								
								$qty_sat_awal = $row->qty_quilting_m / 0.91; // konversi dari meter ke satuan awalnya utk quilting (yard)
								$new_stok = $stok_lama+$qty_sat_awal; // bertambah stok karena reset
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_makloon, insert
									$data_stok = array(
										'kode_brg'=>$row->kode_brg_quilting,
										'stok'=>$new_stok,
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok_hasil_makloon', $data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row->kode_brg_quilting' ");
								}
								
								// 16-02-2012: update stok harga for reset
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE 
															kode_brg_quilting = '$row->kode_brg_quilting' 
															AND harga = '$row->harga_quilting' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$stokreset = $stok_lama+$qty_sat_awal;
								$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row->kode_brg_quilting' AND harga = '$row->harga_quilting' "); //
													// sampe sini 11:32 17-02-2012
								//&&&&&&& end 16-02-2012 &&&&&&&&&&&
				}
				// &&& end 17-02-2012 &&&&
			}
			else { // utk yg ambil dari realisasi
				$q3	= $this->db->query(" SELECT id_realisasi_cutting FROM tt_realisasi_cutting_detail 
										WHERE id='$row->id_realisasi_detail' ");
				$row3	= $q3->row();
				$this->db->query(" UPDATE tt_realisasi_cutting SET status_beres = 'f' 
										WHERE id = '$row3->id_realisasi_cutting' ");
			
				$this->db->query(" UPDATE tt_realisasi_cutting_detail SET status_beres = 'f' 
										WHERE id = '$row->id_realisasi_detail' ");
			}
		}
	}
	
	$this->db->delete('tt_schedule_cutting', array('id'=>$kode));
	$this->db->delete('tt_schedule_cutting_detail', array('id_schedule_cutting'=>$kode));
  }
  
  //function get_pemenuhan($num, $offset, $cari) {
  function get_pemenuhan($cari) {
   		if($cari=="all"){
			$pencarian	= "";
		}else{
			$pencarian	= " AND (UPPER(a.no_bonm) LIKE UPPER('%$cari%') OR UPPER(a.no_pb_cutting) LIKE UPPER('%$cari%') 
							OR UPPER(b.kode_brg) LIKE UPPER('%$cari%')) ";
		}
		 
		$sql = " distinct a.* FROM tm_apply_stok_proses_cutting a INNER JOIN tm_apply_stok_proses_cutting_detail b 
				ON a.id=b.id_apply_stok WHERE b.status_cutting='f' AND b.status_stok = 't' AND a.status_stok = 't'
				AND b.kode_brg_bisbisan = '' AND b.for_kekurangan_cutting='f' AND b.for_bs_cutting = 'f' 
				".$pencarian. " ORDER BY a.tgl_bonm DESC, a.no_bonm ";

		//$this->db->select($sql, false)->limit($num,$offset);
		$this->db->select($sql, false);
		$query = $this->db->get();

		$data_proses_cutting = array();
		$detail_proses_cutting = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$j=0;
			foreach ($hasil as $row1) {
				$sql2 = " SELECT * FROM tm_apply_stok_proses_cutting_detail WHERE id_apply_stok='$row1->id' 
						AND status_cutting='f' AND status_stok = 't' AND kode_brg_bisbisan = ''
						AND for_kekurangan_cutting='f' AND for_bs_cutting = 'f' ";
									
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					$jj=0;
					foreach ($hasil2 as $row2) {
						$kode_brg_jadi = $row2->kode_brg_jadi;
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi = '';
						}
						
						// bhn baku
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
									WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
						if ($query3->num_rows()>0){
							$hasilrow = $query3->row();
							$kode_brg	= $row2->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan		= $hasilrow->nama_satuan;
						} 
						
						// brg quilting
						if ($row2->kode_brg_quilting != '') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
										tm_satuan b WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg_quilting' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg_quilting	= $row2->kode_brg_quilting;
								$nama_brg_quilting	= $hasilrow->nama_brg;
								$satuan_quilting	= $hasilrow->nama_satuan;
							}
						}
						else {
							$kode_brg_quilting	= '';
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						// &&&&&&&&&&&&&&&&&&&&&&& 16 jan 2012
						// 16-01-2012,cek apakah sum qty_bhn di schedule_cutting = yg di pemenuhan. kalo udh =, maka update status cutting = 't'
						$query3 = $this->db->query(" select sum(qty_bhn) as jum FROM tt_schedule_cutting_detail 
										WHERE id_apply_stok_detail = '$row2->id' ");
						if ($query3->num_rows()>0){
							$hasilrow = $query3->row();
							$jum_dischedule = $hasilrow->jum;
						}
						else
							$jum_dischedule = 0;
						
						$selisihnya = $row2->plan_qty_cutting - $jum_dischedule;
						
						// &&&&&&&&&&&&&&&&&&&&&&& end 16 jan 2012
																
						$detail_proses_cutting[] = array('id'=> $row2->id,
								'id_apply_stok'	=> $row2->id_apply_stok,
								'kode_brg'	=> $kode_brg,
								'nama_brg'	=> $nama_brg,
								'kode_brg_quilting'	=> $kode_brg_quilting,
								'nama_brg_quilting'	=> $nama_brg_quilting,
								'qty_pjg_kain'	=> $row2->qty_pjg_kain,
								'status_stok'	=> $row2->status_stok,
								'gelar_pjg_kain'	=> $row2->gelar_pjg_kain,
								//'plan_qty_cutting'	=>	$row2->plan_qty_cutting
								'plan_qty_cutting'	=>	$selisihnya);
						//$jj+=1;
					}
				}
				else {
					$detail_proses_cutting = '';
				}
				
				$data_proses_cutting[] = array('id'	=> $row1->id,
											'no_bonm'	=> $row1->no_bonm,
											'tgl_bonm'	=> $row1->tgl_bonm,
											'no_pb_cutting'	=> $row1->no_pb_cutting,
											'status_stok'	=> $row1->status_stok,
											'status_cutting'	=> $row1->status_cutting,
											'kode_brg_jadi'	=> $kode_brg_jadi,
											'nama_brg_jadi'	=> $nama_brg_jadi,
											'detail_proses_cutting'=> $detail_proses_cutting);
				//$j+=1;
				$detail_proses_cutting = array();
			}
		}
		else {
			$data_proses_cutting = '';
		}
		return $data_proses_cutting;
  }
  
  /*function get_pemenuhantanpalimit($cari){
  		if($cari=="all"){
			$pencarian	= "";
		}else{
			$pencarian	= " AND (UPPER(a.no_bonm) LIKE UPPER('%$cari%') OR UPPER(a.no_pb_cutting) LIKE UPPER('%$cari%') 
							OR UPPER(b.kode_brg) LIKE UPPER('%$cari%'))) ";
		}
			
  		$query = $this->db->query(" SELECT distinct a.* FROM tm_apply_stok_proses_cutting a INNER JOIN tm_apply_stok_proses_cutting_detail b 
								ON a.id=b.id_apply_stok WHERE b.status_cutting='f' AND a.status_stok = 't' 
								AND b.status_stok = 't' AND b.kode_brg_bisbisan = '' ".$pencarian);
		return $query->result();  
  } */
  
  function get_detail_pemenuhan($idapply, $idapplystokitem, $nopbcutting){

    $detail_pemenuhan = array();
        
    foreach($idapplystokitem as $row1) {
		if ($row1 != '') {
			$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting_detail WHERE id='$row1' ");
			$hasilrow = $query2->row();
			
			$qnopbcutting	= $this->db->query(" SELECT no_pb_cutting FROM tm_apply_stok_proses_cutting WHERE id='$hasilrow->id_apply_stok' LIMIT 1 ");
			$row_pb_cutting	= $qnopbcutting->row();
			
			$qnamabarang	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg='$hasilrow->kode_brg' ");
			$nmbrgrow		= $qnamabarang->row();
			
			// brg jadi
			$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$hasilrow->kode_brg_jadi' ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				$nama_brg_jadi	= $hasilrow3->e_product_motifname;
			}
			else {
				$nama_brg_jadi = '';
			}
			
			if ($hasilrow->kode_brg_quilting != '') {
				$qnamaquilting	= $this->db->query(" SELECT * FROM tm_brg_hasil_makloon WHERE kode_brg='$hasilrow->kode_brg_quilting' ");
				$kodebrgquiltingrow = $hasilrow->kode_brg_quilting;
				$nmbrgquiltingrow = $qnamaquilting->row();
				$nmbrgquilting = $nmbrgquiltingrow->nama_brg;
				
				$pos = strpos($nmbrgquilting, "\"");
				  if ($pos > 0)
					$nmbrgquilting = str_replace("\"", "&quot;", $nmbrgquilting);
				  else
					$nmbrgquilting = str_replace("'", "\'", $nmbrgquilting);
			}
			else {
				$kodebrgquiltingrow = '';
				$nmbrgquilting = '';
			}
			
			// &&&&&&&&&&&&&&&&&&&&&&& 16 jan 2012
			// 16-01-2012,cek apakah sum qty_bhn di schedule_cutting = yg di pemenuhan. kalo udh =, maka update status cutting = 't'
			$query4 = $this->db->query(" select sum(qty_bhn) as jum FROM tt_schedule_cutting_detail 
							WHERE id_apply_stok_detail = '$row1' ");
			if ($query4->num_rows()>0){
				$hasil4 = $query4->row();
				$jum_dischedule = $hasil4->jum;
			}
			else
				$jum_dischedule = 0;
				
			$selisihnya = $hasilrow->plan_qty_cutting - $jum_dischedule;
			
			$detail_pemenuhan[] = array('id_detail'=> $row1,
				'id_apply_stok_detail' => $hasilrow->id_apply_stok,
				'kode_brg'=> $hasilrow->kode_brg,
				'nmbrg'	=> $nmbrgrow->nama_brg,
				'kode_brg_quilting'=> $kodebrgquiltingrow,
				'nmbrgquilting'	=> $nmbrgquilting,
				'qty_pjg_kain'=> $hasilrow->qty_pjg_kain,
				'status_stok'=> $hasilrow->status_stok,
				'gelar_pjg_kain'=> $hasilrow->gelar_pjg_kain,
				'status_cutting'=> $hasilrow->status_cutting,
				//'plan_qty_cutting'=> $hasilrow->plan_qty_cutting,
				'plan_qty_cutting'=> $selisihnya,
				'id_pb_cutting'=> $row_pb_cutting->no_pb_cutting,
				'kode_brg_jadi'=> $hasilrow->kode_brg_jadi,
				'nama_brg_jadi'	=> $nama_brg_jadi,
				'jenis_proses'=> $hasilrow->jenis_proses );
		}
	}
	return $detail_pemenuhan;
  }
    
  function get_schedule_cutting($id) {
		$query	= $this->db->query(" SELECT * FROM tt_schedule_cutting where id='$id' ");
	
		$data_fb = array();
		$detail_fb = array();
		
		if ($query->num_rows() > 0){
			$j=0;
			$kode_bhn_baku = '';
			$bhn_baku = '';
			foreach ($query->result() as $row1) {
				$query2	= $this->db->query(" SELECT * FROM tt_schedule_cutting_detail WHERE id_schedule_cutting='$row1->id' ");
				$x=0;
				if ($query2->num_rows() > 0){
					foreach ($query2->result() as $row2) {
						$qnamabarang	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg='$row2->kode_brg' ");
						$nmbrg		= $qnamabarang->row();
						
						if ($row1->is_dacron == 't') {
							$kode_bhn_baku = $row2->kode_brg;
							$bhn_baku = $kode_bhn_baku." - ".$nmbrg->nama_brg;
						}
						
						$qnmmotif	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						$nmmotif	= $qnmmotif->row();
						
						if ($row2->kode_brg_quilting != '') {
							$qnamaquilting	= $this->db->query(" SELECT * FROM tm_brg_hasil_makloon WHERE kode_brg='$row2->kode_brg_quilting' ");
							$kodebrgquiltingrow = $hasilrow->kode_brg_quilting;
							$nmbrgquiltingrow = $qnamaquilting->row();
							$nmbrgquilting = $nmbrgquiltingrow->nama_brg;
							
							$pos = strpos($nmbrgquilting, "\"");
							  if ($pos > 0)
								$nmbrgquilting = str_replace("\"", "&quot;", $nmbrgquilting);
							  else
								$nmbrgquilting = str_replace("'", "\'", $nmbrgquilting);
						}
						else {
							$kodebrgquiltingrow = '';
							$nmbrgquilting = '';
						}
						
						/*if ($row1->is_dacron == 'f') {
							if ($row2->id_apply_stok_detail != '0' ) {
								$qpermintaan	= $this->db->query(" SELECT * FROM tm_apply_stok_proses_cutting_detail WHERE id='$row2->id_apply_stok_detail' ");
								$permintaan		= $qpermintaan->row();
								$id_apply_stok	= $permintaan->id_apply_stok;
							}
							else
								$id_apply_stok = 0;
						}
						else
							$id_apply_stok = 0; */
						
						$detail_fb[] = array('idx' => $row2->id,
									'id_schedule_cutting' => $row2->id_schedule_cutting,
									'id_pb_cutting' => $row2->id_pb_cutting,
									'kode_brg_jadi' => $row2->kode_brg_jadi,
									'nmmotif' => $nmmotif->e_product_motifname,
									'kode_brg' => $row2->kode_brg,
									'nmbrg' => $nmbrg->nama_brg,
									'qty_bhn' => $row2->qty_bhn,
									'jam_mulai' => $row2->jam_mulai,
									'jam_selesai' => $row2->jam_selesai,
									'operator_cutting' => $row2->operator_cutting,
									'keterangan' => $row2->keterangan,
									'id_detail' => $row2->id_apply_stok_detail,
									//'id_apply_stok' => $id_apply_stok,
									//'id_pb_cutting' => $row2->id_pb_cutting,
									'id_bagian_brg_jadi' => $row2->id_bagian_brg_jadi,
									'ukuran_pola' => $row2->ukuran_pola,
									'diprint' => $row2->diprint,
									'kode_brg_quilting' => $kodebrgquiltingrow,
									'nmbrgquilting' => $nmbrgquilting);
						$x+=1;			
					}
				}else{
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_cutting);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$tgl_cutting = $tgl1."-".$bln1."-".$thn1;
								
				$data_fb[] = array('id'=> $row1->id,
						'no_schedule'=> $row1->no_schedule,	
						'tgl_cutting'=> $tgl_cutting,
						'is_dacron'=> $row1->is_dacron,
						'is_realisasi'=> $row1->is_realisasi,
						'kode_bhn_baku'=> $kode_bhn_baku,
						'bhn_baku'=> $bhn_baku,
						'detail_fb'=> $detail_fb);
				
				$j+=1;			
				$detail_fb = array();
			} // end 0f for
		}else{
			$data_fb = '';
		}
		return $data_fb;
  }
  
  // 28 des 2011
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  // 31-12-2011
  function get_bahan($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = "a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id order by a.nama_brg";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
				OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg";		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;
			
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $row1->nama_satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari){
	if ($cari == "all") {
			$sql = "SELECT a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id order by a.nama_brg";				
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
			$sql = " SELECT a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
				OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg";		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_bagian_brg_jadi(){
    $this->db->select("* from tm_bagian_brg_jadi order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 18-01-2012
  function get_item_realisasi() {
	  $sql = " * FROM tt_realisasi_cutting WHERE status_beres = 'f' ORDER BY tgl_realisasi DESC ";

		$this->db->select($sql, false);
		$query = $this->db->get();

		$data_realisasi = array();
		$detail_realisasi = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			//$j=0;
			foreach ($hasil as $row1) {
				$sql2 = " SELECT * FROM tt_realisasi_cutting_detail WHERE id_realisasi_cutting='$row1->id' 
						AND status_beres='f' "; 
									
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					//$jj=0;
					foreach ($hasil2 as $row2) {
						$kode_brg_jadi = $row2->kode_brg_jadi;
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
						WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi = '';
						}
						
						// bhn baku
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
									WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
						if ($query3->num_rows()>0){
							$hasilrow = $query3->row();
							$kode_brg	= $row2->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan		= $hasilrow->nama_satuan;
						} 
						
						// brg quilting
						if ($row2->kode_brg_quilting != '') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
										tm_satuan b WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg_quilting' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$kode_brg_quilting	= $row2->kode_brg_quilting;
								$nama_brg_quilting	= $hasilrow->nama_brg;
								$satuan_quilting	= $hasilrow->nama_satuan;
							}
						}
						else {
							$kode_brg_quilting	= '';
							$nama_brg_quilting = '';
							$satuan_quilting = '';
						}
						
						// ambil plan qty di schedule cutting
						$query3	= $this->db->query(" SELECT qty_bhn FROM tt_schedule_cutting_detail 
									WHERE id ='$row2->id_schedule_cutting_detail' ");
						if ($query3->num_rows()>0){
							$hasilrow = $query3->row();
							$qty_schedule	= $hasilrow->qty_bhn;
						} 
						else
							$qty_schedule = 0;
																						
						$detail_realisasi[] = array('id'=> $row2->id,
								'id_realisasi_cutting'	=> $row2->id_realisasi_cutting,
								'kode_brg_jadi'	=> $kode_brg_jadi,
								'nama_brg_jadi'	=> $nama_brg_jadi,
								'kode_brg'	=> $kode_brg,
								'nama_brg'	=> $nama_brg,
								'kode_brg_quilting'	=> $kode_brg_quilting,
								'nama_brg_quilting'	=> $nama_brg_quilting,
								'qty_schedule'	=> $qty_schedule,
								'jum_gelar'	=> $row2->jum_gelar,
								'hasil_potong'	=> $row2->hasil_potong);
						//$jj+=1;
					}
				}
				else {
					$detail_realisasi = '';
				}
				
				$data_realisasi[] = array('id'	=> $row1->id,
											'no_realisasi'	=> $row1->no_realisasi,
											'tgl_realisasi'	=> $row1->tgl_realisasi,
											'is_dacron'	=> $row1->is_dacron,
											'detail_realisasi'=> $detail_realisasi);
				$detail_realisasi = array();
				//$j+=1;
			}
		}
		else {
			$data_realisasi = '';
		}
		return $data_realisasi;
  }
  
  // 19-01-2012
  function get_detail_realisasi($id_realisasi_detail){
    $detail_realisasi = array();
        
    foreach($id_realisasi_detail as $row1) {
		if ($row1 != '') {
			$query2	= $this->db->query(" SELECT * FROM tt_realisasi_cutting_detail WHERE id='$row1' ");
			$hasilrow = $query2->row();
						
			$qnamabarang	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg='$hasilrow->kode_brg' ");
			$nmbrgrow		= $qnamabarang->row();
			
			// brg jadi
			$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$hasilrow->kode_brg_jadi' ");
			if ($query3->num_rows() > 0){
				$hasilrow3 = $query3->row();
				$nama_brg_jadi	= $hasilrow3->e_product_motifname;
			}
			else {
				$nama_brg_jadi = '';
			}
			
			if ($hasilrow->kode_brg_quilting != '') {
				$qnamaquilting	= $this->db->query(" SELECT * FROM tm_brg_hasil_makloon WHERE kode_brg='$hasilrow->kode_brg_quilting' ");
				$kodebrgquiltingrow = $hasilrow->kode_brg_quilting;
				$nmbrgquiltingrow = $qnamaquilting->row();
				$nmbrgquilting = $nmbrgquiltingrow->nama_brg;
				
				$pos = strpos($nmbrgquilting, "\"");
				  if ($pos > 0)
					$nmbrgquilting = str_replace("\"", "&quot;", $nmbrgquilting);
				  else
					$nmbrgquilting = str_replace("'", "\'", $nmbrgquilting);
			}
			else {
				$kodebrgquiltingrow = '';
				$nmbrgquilting = '';
			}
			
			// 19-01-2012
			// ambil plan qty di schedule cutting
			$query3	= $this->db->query(" SELECT qty_bhn, diprint FROM tt_schedule_cutting_detail 
						WHERE id ='$hasilrow->id_schedule_cutting_detail' ");
			if ($query3->num_rows()>0){
				$hasil3 = $query3->row();
				$qty_schedule	= $hasil3->qty_bhn;
				$diprint	= $hasil3->diprint;
			} 
			else {
				$qty_schedule = 0;
				$diprint = 'f';
			}
			
			$selisihnya = $qty_schedule-$hasilrow->hasil_potong;
						
			$detail_realisasi[] = array('id_realisasi_detail'=> $row1,
				'id_realisasi_cutting' => $hasilrow->id_realisasi_cutting,
				'kode_brg'=> $hasilrow->kode_brg,
				'nmbrg'	=> $nmbrgrow->nama_brg,
				'kode_brg_quilting'=> $kodebrgquiltingrow,
				'nmbrgquilting'	=> $nmbrgquilting,
				'qty_schedule'=> $qty_schedule,
				'jum_gelar'=> $hasilrow->jum_gelar,
				'hasil_potong'=> $hasilrow->hasil_potong,
				'selisihnya'=> $selisihnya,
				'kode_brg_jadi'=> $hasilrow->kode_brg_jadi,
				'nama_brg_jadi'	=> $nama_brg_jadi,
				'diprint'	=> $diprint );
		}
	}
	return $detail_realisasi;
  }
  
}

