<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $jenis_beli, $status_lunas, $supplier, $cari, $date_from, $date_to) {
	//09-05-2012
	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$this->db->select(" * FROM tm_pembelian_bordir_nofaktur WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	
	/*if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_pembelian_bordir_nofaktur ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_pembelian_bordir_nofaktur
							WHERE kode_supplier = '$supplier' ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_pembelian_bordir_nofaktur where kode_supplier = '$supplier' AND 
							(UPPER(no_faktur) like UPPER('%$cari%') ) ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_pembelian_bordir_nofaktur where 
							(UPPER(no_faktur) like UPPER('%$cari%')) ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	} */
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			/*	$query2	= $this->db->query(" SELECT SUM(a.total) as totnya FROM tm_pembelian_bordir a, tm_pembelian_bordir_nofaktur b, tm_pembelian_bordir_nofaktur_sj c 
									WHERE a.kode_supplier = b.kode_supplier AND a.no_sj = c.no_sj 
									AND b.id = c.id_pembelian_nofaktur
									AND b.no_faktur = '$row1->no_faktur' ");
				if ($query2->num_rows() > 0) { //
					$hasilrow = $query2->row();
					$totalnya	= $hasilrow->totnya;				
				}
				else
					$totalnya = '0'; */
				
				//echo $totalnya; die();
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b 
								INNER JOIN tm_pembelian_bordir_nofaktur a ON b.id = a.id_supplier
								WHERE a.id = '$row1->id' ");
				if ($query3->num_rows() > 0) {
					$hasilrow3 = $query3->row();
					$kode_supplier	= $hasilrow3->kode_supplier;
					$nama_supplier	= $hasilrow3->nama;
				}
				else {
					$kode_supplier	= '';
					$nama_supplier	= '';
				}
				
				// ambil detail data no & tgl SJ
				// 25-06-2015, a.no_sj ga dipake
				$query21= $this->db->query(" SELECT a.id_sj_pembelian_bordir FROM tm_pembelian_bordir_nofaktur_sj a
							INNER JOIN tm_pembelian_bordir_nofaktur b ON a.id_pembelian_bordir_nofaktur = b.id
							WHERE a.id_pembelian_bordir_nofaktur = '$row1->id' ");
				//AND b.no_faktur = '$row1->no_faktur' AND b.kode_supplier = '$kode_supplier'
				
				if ($query21->num_rows() > 0) { //
					$hasil21 = $query21->result();
					foreach ($hasil21 as $row21) {

						$id_sj_pembelian_bordir = $row21->id_sj_pembelian_bordir; 
						
						// 10-06-2014, 23-06-2015 dikomen if-nya
						//if ($id_sj_pembelian_bordir != 0)
							$sql3 = "SELECT no_sjmasukpembelian_bordir, tgl_sj from tm_pembelian_bordir where id = '$id_sj_pembelian_bordir' ";
						//else
						//	$sql = "SELECT no_sj, tgl_sj from tm_pembelian_bordir where no_sj like '%$no_sj%' 
						//		AND kode_supplier= '$kode_supplier' AND status_aktif = 't' ";
						
						/*$sql = "SELECT tgl_sj from tm_pembelian_bordir where no_sj = '$no_sj' 
								AND kode_supplier= '$kode_supplier' AND status_aktif = 't'"; */
						$query3	= $this->db->query($sql3);
					//	if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_sj	= $hasilrow3->tgl_sj;
							$no_sjx	= $hasilrow3->no_sjmasukpembelian_bordir;
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_sj'=> $no_sjx,
											'tgl_sj'=> $tgl_sj,
											'id_sj_pembelian_bordir'=> $id_sj_pembelian_bordir
											);		
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
						$exptgl1 = explode(" ", $tgl1);
						$tgl1nya= $exptgl1[0];
						$jam1nya= $exptgl1[1];
						$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,	
											'no_faktur'=> $row1->no_faktur,	
											'tgl_faktur'=> $tgl_faktur,	
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,	
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb,
											'status_faktur_pajak'=> $row1->status_faktur_pajak,
											'status_lunas'=> $row1->status_lunas
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($jenis_beli, $status_lunas, $supplier, $cari, $date_from, $date_to){
	/*if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir_nofaktur ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir_nofaktur
							WHERE kode_supplier = '$supplier' ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir_nofaktur where kode_supplier = '$supplier' AND 
							(UPPER(no_faktur) like UPPER('%$cari%') )  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir_nofaktur where (UPPER(no_faktur) like UPPER('%$cari%'))  ");
	} */
	//09-05-2012
	$pencarian = "";
	if($cari!="all"){
		$pencarian.= " AND UPPER(no_faktur) like UPPER('%$cari%') ";
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}else{
		if ($date_from != "00-00-0000")
			$pencarian.= " AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND tgl_faktur <= to_date('$date_to','dd-mm-yyyy') ";
		if ($jenis_beli != '0')
			$pencarian.= " AND jenis_pembelian = '$jenis_beli' ";
		if ($status_lunas != '0')
			$pencarian.= " AND status_lunas = '$status_lunas' ";
		if ($supplier != '0')
			$pencarian.= " AND id_supplier = '$supplier' ";
		$pencarian.= " ORDER BY tgl_faktur DESC, id DESC ";
	}
	
	$query = $this->db->query(" SELECT * FROM tm_pembelian_bordir_nofaktur WHERE TRUE ".$pencarian);
    
    return $query->result();  
  }
      
  function cek_data($no_fp, $supplier){
    $this->db->select("id from tm_pembelian_bordir_nofaktur WHERE no_faktur = '$no_fp' AND id_supplier = '$supplier' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_fp, $tgl_fp, $supplier, $jenis_pembelian, $jum, $id_sj){  
    //$tgl = date("Y-m-d");
    $tgl = date("Y-m-d H:i:s");
	$list_sj = explode(";", $id_sj); 
	$uid_update_by = $this->session->userdata('uid');

	$data_header = array(
			  'no_faktur'=>$no_fp,
			  'tgl_faktur'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'id_supplier'=>$supplier,
			  'jenis_pembelian'=>$jenis_pembelian,
			  'jumlah'=>$jum,
			  'sisa'=>$jum,
			  'uid_update_by'=>$uid_update_by
			);
	$this->db->insert('tm_pembelian_bordir_nofaktur',$data_header);
	
	// ambil data terakhir di tabel tm_pembelian_bordir_nofaktur
	$query2	= $this->db->query(" SELECT id FROM tm_pembelian_bordir_nofaktur ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_pembelian_bordir_nofaktur'=>$id_pf,
			  'id_sj_pembelian_bordir'=>$row1,
			  'no_sj'=>''
			);
			$this->db->insert('tm_pembelian_bordir_nofaktur_sj',$data_detail);
			
			//10-04-2012, update jenis pembelian
			// update status_faktur di tabel tm_pembelian_bordir
			/*$this->db->query(" UPDATE tm_pembelian_bordir SET no_faktur = '$no_fp', status_faktur = 't', jenis_pembelian = '$jenis_pembelian' 
								WHERE no_sj = '$row1' AND kode_supplier = '$supplier' "); */
			
			// revisi 10-06-2014
			$this->db->query(" UPDATE tm_pembelian_bordir SET no_faktur = '$no_fp', status_faktur = 't', jenis_pembelian = '$jenis_pembelian' 
								WHERE id = '$row1' ");
		}
	}
   
  }
    
  function delete($id){    	
	//semua no_sj di tabel tm_pembelian_bordir yg bersesuaian dgn tm_pembelian_bordir_nofaktur dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.id_supplier, b.no_sj, b.id_sj_pembelian_bordir FROM tm_pembelian_bordir_nofaktur a 
							INNER JOIN tm_pembelian_bordir_nofaktur_sj b ON a.id = b.id_pembelian_bordir_nofaktur
							WHERE a.id = '$id' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// 10-06-2014. 23-06-2015 dikomen if-nya
			/*if ($row1->id_sj_pembelian_bordir == '0') {
				$this->db->query(" UPDATE tm_pembelian_bordir SET status_faktur = 'f', no_faktur = '' 
							WHERE kode_supplier = '$row1->kode_supplier' AND no_sj = '$row1->no_sj' ");
			} */
			//else {
				$this->db->query(" UPDATE tm_pembelian_bordir SET status_faktur = 'f', no_faktur = '' 
							WHERE id = '$row1->id_sj_pembelian_bordir' ");
			//}
			
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di pembelian_nofaktur dan pembelian_nofaktur_sj
	$this->db->query(" DELETE FROM tm_pembelian_bordir_nofaktur_sj where id_pembelian_bordir_nofaktur= '$id' ");
	$this->db->query(" DELETE FROM tm_pembelian_bordir_nofaktur where id = '$id' ");

  }
    
  //function get_pembelian($num, $offset, $jnsaction, $supplier, $cari) {
	function get_pembelian($jnsaction, $supplier, $jenis_pembelian, $no_fakturnya, $cari) {
	  // ambil data pembelian 
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian_bordir WHERE status_faktur = 'f' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND status_aktif = 't'
								AND jenis_pembelian='$jenis_pembelian' order by id_supplier, tgl_sj DESC ");
			}
			else { // utk proses edit
				//$this->db->select(" * FROM tm_pembelian_bordir WHERE status_faktur = 'f' OR no_faktur <> '' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') AND status_aktif = 't'
				AND jenis_pembelian='$jenis_pembelian'  order by id_supplier, tgl_sj DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND kode_supplier = '$supplier' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE status_faktur = 'f' 
						AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian'
						order by id_supplier, tgl_sj DESC ");
			}
			else {
				//$this->db->select(" * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE ( status_faktur = 'f' OR no_faktur = '$no_fakturnya' ) 
							AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian'
							order by id_supplier, tgl_sj DESC "); //
			}
		}
	}
	else {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND 
				//(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND status_aktif = 't'
				AND jenis_pembelian='$jenis_pembelian'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by id_supplier, tgl_sj DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur <> '') AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" select * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND jenis_pembelian='$jenis_pembelian'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) AND status_aktif = 't'
				order by id_supplier, tgl_sj DESC ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$this->db->select(" * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" select * FROM tm_pembelian_bordir WHERE status_faktur = 'f' 
				AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by id_supplier, tgl_sj DESC ");
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$query	= $this->db->query(" select * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND id_supplier = '$supplier' AND status_aktif = 't' AND jenis_pembelian='$jenis_pembelian'
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by id_supplier, tgl_sj DESC ");
			}
		}
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query2	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query2->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_sj'=> $row1->no_sjmasukpembelian_bordir,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'total'=> $row1->total,
											'total_pajak'=> $row1->total_pajak,
											'jenis_pembelian'=> $row1->jenis_pembelian
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function get_pembeliantanpalimit($jnsaction, $supplier, $jenis_pembelian, $no_fakturnya, $cari){
	if ($cari == "all") {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$query = $this->db->getwhere('tm_pembelian_bordir',array('status_faktur'=>'f' ));
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND jenis_pembelian='$jenis_pembelian' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE (status_faktur = 'f'
									OR no_faktur = '$no_fakturnya') AND jenis_pembelian='$jenis_pembelian' ");
			}			
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$query = $this->db->getwhere('tm_pembelian_bordir',array('status_faktur'=>'f',
																'kode_supplier'=>$supplier)); */
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE status_faktur = 'f'
							AND jenis_pembelian='$jenis_pembelian' AND id_supplier='$supplier' ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' 
									OR no_faktur = '$no_fakturnya') AND jenis_pembelian='$jenis_pembelian' AND id_supplier = '$supplier' ");
			}
		}
	}
	else {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND jenis_pembelian='$jenis_pembelian'
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya')
									AND jenis_pembelian='$jenis_pembelian' AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE status_faktur = 'f' AND id_supplier = '$supplier' 
								AND jenis_pembelian='$jenis_pembelian'
								AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
			else {
				$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
									AND id_supplier = '$supplier' AND jenis_pembelian='$jenis_pembelian'
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) ");
			}
		}
	}
    
    return $query->result();  
  }
      
  function get_faktur($id_faktur){
	$query	= $this->db->query(" SELECT * FROM tm_pembelian_bordir_nofaktur where id='$id_faktur' ");    
	
	$data_faktur = array();
	$detail_sj = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT id_sj_pembelian_bordir FROM tm_pembelian_bordir_nofaktur_sj 
									WHERE id_pembelian_bordir_nofaktur = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sj = ''; $id_sj = '';
				foreach ($hasil2 as $row2) {
					// 10-06-2014
					// 23-06-2015 dikomen ifnya
					//if ($row2->id_sj_pembelian_bordir != 0) {
						$sqlxx = " SELECT id, no_sjmasukpembelian_bordir FROM tm_pembelian_bordir WHERE id='$row2->id_sj_pembelian_bordir' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$no_sj .= $hasilxx->no_sjmasukpembelian_bordir.",";
							$id_sj .= $hasilxx->id.";";
						}
					//}
					// 25-06-2015 dikomen
					/*else {
						$no_sj .= $row2->no_sj.",";
						$id_sj .= $row2->id_sj_pembelian_bordir.";";
					} */
				}
				
				$pisah1 = explode("-", $row1->tgl_faktur);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_faktur = $tgl1."-".$bln1."-".$thn1;						
			
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'id_supplier'=> $row1->id_supplier,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_sj'=> $no_sj,
											'id_sj'=> $id_sj
											);
			} // endforeach header
	}
    return $data_faktur;
  }
  
  function get_supplier(){
	//$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori= '1' ORDER BY kode_supplier ");    
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  

}
