<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
   function getkelunit(){
	$sql = " * FROM tm_kelompok_unit ORDER BY id ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
   function getlistunitjahit(){
	$sql = " * FROM tm_unit_jahit ORDER BY kode_unit ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
  
   function getlistunitpacking(){
	$sql = " * FROM tm_unit_packing ORDER BY kode_unit ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    
    return $query->result();
  }
  
  
function get_all_fakturwip($date_from, $date_to, $kelompok, $jenis_masuk) {
	$pencarian="";
				$sql ="select a.* FROM tm_pembelianpackjht_wip_nofaktur a
				INNER JOIN tm_link_unit b ON a.id_unit_jahit=b.id_unit_jahit AND a.id_unit_packing=b.id_unit_packing
				INNER JOIN tm_kelompok_unit_detail d ON d.id_unit_jahit=b.id_unit_jahit 
				INNER JOIN tm_kelompok_unit c ON d.id_kelompok_unit=c.id 
				where  tgl_faktur >= to_date('$date_from','dd-mm-yyyy') and tgl_faktur <= to_date('$date_to','dd-mm-yyyy')";
	if($pencarian!= 0){
		$pencarian.= " AND  d.id='$kelompok'";
	}
		$sql.= " ORDER BY  tgl_faktur ASC, no_faktur ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang 
					$query2	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip_nofaktur_sj a 
INNER JOIN tm_pembelianpackjht_wip b ON a.id_sj_pembelianwippackjht = b.id 
INNER JOIN tm_pembelianpackjht_wip_detail c ON c.id_pembelianpackjht_wip=b.id 
WHERE a.id_pembelianwippackjht_nofaktur = '$row1->id' ORDER BY no_sjmasukpembelianpackjht ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query("  SELECT * FROM tm_sjmasukgudangjadi_detail a 
						inner join tm_barang_wip b on a.id_brg_wip=b.id 
						inner join tm_sjmasukgudangjadi c on a.id_sjmasukgudangjadi=c.id 
						WHERE a.id='$row2->id_sjmasukgudangjadi_detail' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan = "Pieces";
							$qty	= $hasilrow->qty;
							$no_sj	= $hasilrow->no_sj;
							$tgl_sj	= $hasilrow->tgl_sj;
							//$harga	= $hasilrow->harga;
							//$diskon	= $hasilrow->diskon;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan		= '';
							$qty		= '';
							$no_sj		= '';
							$tgl_sj		= '';
						}
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'no_sj'	=> $no_sj,
												'tgl_sj'	=> $tgl_sj,
												//'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'no_sj'=> $no_sj,
												'harga_j'=>$row2->harga_j,
												'harga_p'=>$row2->harga_p,
												'diskon'=>$row2->diskon,
												'subtotal'=>$row2->total
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				

				
				//$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $row1->nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $row1->nama_unit_packing,
											'grandtotal'=> $row1->jumlah,
											'detail_beli'=> $detail_beli
											
											);
		$detail_beli = array();
			} // endforeach header
			//print_r($data_beli);
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
	}
  function get_all_fakturwip_for_print($date_from, $date_to, $kelompok, $jenis_masuk) {
		$pencarian="";
				$sql ="select a.* FROM tm_pembelianpackjht_wip_nofaktur a
				INNER JOIN tm_link_unit b ON a.id_unit_jahit=b.id_unit_jahit AND a.id_unit_packing=b.id_unit_packing
				INNER JOIN tm_kelompok_unit_detail d ON d.id_unit_jahit=b.id_unit_jahit 
				INNER JOIN tm_kelompok_unit c ON d.id_kelompok_unit=c.id 
				AND tgl_faktur >= to_date('$date_from','dd-mm-yyyy') and tgl_faktur <= to_date('$date_to','dd-mm-yyyy')";
		if($pencarian!= 0){
		$pencarian.= " AND  d.id='$kelompok'";
	}
		$sql.= " ORDER BY  tgl_faktur ASC, no_faktur ";
		$query	= $this->db->query($sql);
				
		$data_beli = array();
		$detail_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				// =============================== detailnya ================================
				// ambil detail data list barang dari tm_sjmasukwip
					$query2	= $this->db->query(" SELECT * FROM tm_pembelianpackjht_wip_nofaktur_sj a 
INNER JOIN tm_pembelianpackjht_wip b ON a.id_sj_pembelianwippackjht = b.id 
INNER JOIN tm_pembelianpackjht_wip_detail c ON c.id_pembelianpackjht_wip=b.id 
WHERE a.id_pembelianwippackjht_nofaktur = '$row1->id' ORDER BY no_sjmasukpembelianpackjht ASC ");
				
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query("  SELECT * FROM tm_sjmasukgudangjadi_detail a 
						inner join tm_barang_wip b on a.id_brg_wip=b.id 
						inner join tm_sjmasukgudangjadi c on a.id_sjmasukgudangjadi=c.id 
						WHERE a.id='$row2->id_sjmasukgudangjadi_detail' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan = "Pieces";
							$qty	= $hasilrow->qty;
							$no_sj	= $hasilrow->no_sj;
							$tgl_sj	= $hasilrow->tgl_sj;
							//$harga	= $hasilrow->harga;
							//$diskon	= $hasilrow->diskon;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan		= '';
							$qty		= '';
							$no_sj		= '';
							$tgl_sj		= '';
						}
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
											
						$detail_beli[] = array( 'kode_brg'=> $kode_brg,
												'nama_brg'=> $nama_brg,
												'no_sj'	=> $no_sj,
												'tgl_sj'	=> $tgl_sj,
												//'harga'=> $harga,
												'qty'=> $qty,
												'satuan'=> $satuan,
												'no_sj'=> $no_sj,
												'harga_j'=>$row2->harga_j,
												'harga_p'=>$row2->harga_p,
												'diskon'=>$row2->diskon,
												'subtotal'=>$row2->total
											);		
					} // end foreach
				} // end if
				
				// ===========================================================================
												
				$pisah1 = explode("-", $row1->tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
				$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
				

				
				//$grandtotal = round(($row1->grandtotal-($row1->grandtotal*0.04)), 2);
				
				$data_beli[] = array(		'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $row1->nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $row1->nama_unit_packing,
											'grandtotal'=> $row1->jumlah,
											'detail_beli'=> $detail_beli
											
											);
		$detail_beli = array();
			} // endforeach header
			//print_r($data_beli);
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
	}
}
