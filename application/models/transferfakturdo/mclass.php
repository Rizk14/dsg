<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function tampilkandata(){
		$db2=$this->load->database('db_external', TRUE);
		/*$query	= $db2->query(" SELECT a.i_do, b.i_do_code, b.d_do, c.i_op_code, c.d_op, b.i_customer, d.i_code AS i_branch, a.i_product, a.e_product_name, a.n_deliver, a.v_do_gross, a.e_note FROM tm_do_item a 
			INNER JOIN tm_do b ON b.i_do=a.i_do
			INNER JOIN tm_op c ON c.i_op=a.i_op
			INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
			WHERE b.f_do_cancel='f' AND b.f_transfer='f' AND b.d_do >= '2011-09-01' AND length(a.e_product_name)>10 ORDER BY b.i_do_code ASC, a.i_product ASC LIMIT 100 "); */
		
		$query	= $db2->query("SELECT a.i_faktur, b.i_faktur_code, b.d_faktur, 
			b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn, a.i_faktur_item, a.i_product, 
			a.e_product_name, a.n_quantity, a.v_unit_price, c.i_do_code, c.d_do, c.i_branch 
			FROM tm_faktur_do_item_t a 
			INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do c ON c.i_do=a.i_do
			WHERE b.f_faktur_cancel='f' AND a.f_transfer='f'
		AND b.d_faktur >= '2013-01-01' AND c.i_customer = '1'
		 ORDER BY b.i_faktur_code ASC, a.i_product ASC LIMIT 200 ");
			
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}
	
	function simpan($i_customer,$i_code,$i_faktur, $i_faktur_item, $i_faktur_code,$d_faktur, $n_discount, $v_discount, 
								$v_total_faktur,$v_total_fppn,
								$i_do_code, $d_do,$i_product,$e_product_name, $n_quantity,$v_unit_price, $fck,$iterasi) {			
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$jml = 0;
		for($j=0;$j<$iterasi;$j++){

			if($fck[$j]==1){
				$jml++;
				/*$ifakturcode = trim($i_faktur_code[$j]);
				$qcek	= $db2->query(" SELECT i_trans FROM tm_trans_fakturdo WHERE i_faktur_code='$ifakturcode' 
								AND i_product='$i_product[$j]' ");
				
				if($qcek->num_rows()==0){ */
					
					$insert	= $db2->query(" INSERT INTO tm_trans_fakturdo (i_faktur_code,d_faktur,n_discount,v_discount,
					v_total_faktur,v_total_fppn,i_do_code,d_do,i_customer,i_code_branch,i_product,e_product_name, n_quantity, 
					v_unit_price, d_entry) 
					VALUES('$i_faktur_code[$j]','$d_faktur[$j]','$n_discount[$j]','$v_discount[$j]','$v_total_faktur[$j]',
					'$v_total_fppn[$j]','$i_do_code[$j]','$d_do[$j]','$i_customer','$i_code[$j]','$i_product[$j]',
					'$e_product_name[$j]','$n_quantity[$j]','$v_unit_price[$j]','$dentry') ");
					
					$db2->query(" UPDATE tm_faktur_do_item_t SET f_transfer='t' 
								WHERE i_faktur_item='$i_faktur_item[$j]' AND i_faktur = '$i_faktur[$j]' ");
					
					$query3	= $db2->query(" SELECT i_faktur_item FROM tm_faktur_do_item_t WHERE 
										 i_faktur = '$i_faktur[$j]' AND f_transfer = 'f'  ");
					if ($query3->num_rows() == 0){
						$db2->query(" UPDATE tm_faktur_do_t SET f_transfer='t' 
								WHERE i_faktur = '$i_faktur[$j]' ");
					}
					//$jml+=1;
				//}
				/*else{
					$db2->query(" UPDATE tm_trans_do SET n_deliver='$n_jmldo[$j]', v_do_gross='$v_do_gross[$j]' WHERE i_do_code='$idocode' AND i_op_code='$i_op_code[$j]' AND i_branch='$i_area[$j]' AND i_product='$i_product[$j]' ");				
					$db2->query(" UPDATE tm_do SET f_transfer='t' WHERE i_do='$i_do[$j]' AND f_do_cancel='f' ");
				} */
			}
		}
		
		//print "<script>alert(\"telah dites sebanyak $jml item data, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		print "<script>alert(\"Data Faktur DO telah ditransfer sebanyak $jml item data, terimakasih.\");window.open(\"index\", \"_self\");</script>";
	}
}
?>
