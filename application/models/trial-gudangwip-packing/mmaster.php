<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
   function cek_so_unit_packing($bulan, $tahun, $unit_packing) {
$query3	= $this->db->query(" SELECT id, tgl_so, jenis_perhitungan_stok, status_approve FROM tt_stok_opname_unit_packing
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_unit = '$unit_packing' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
			$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
			$jenis_perhitungan_stok = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so,
							   'jenis_perhitungan_stok'=> $jenis_perhitungan_stok
							);
							
		return $so_bahan;
  }
   function cek_sokosong_unitpacking($unit_packing) {
	$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE id_unit='$unit_packing' ");
	if ($query3->num_rows() > 0){
		return 'f';
	}
	else {
		return 't';
	}
  }
  function get_stok_unit_packing($bulan, $tahun, $unit_packing) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 05-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
	  		
		$sql = " SELECT a.id, a.id_brg_wip, b.kode_brg, b.nama_brg FROM tm_stok_unit_packing a 
				INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id WHERE a.id_unit = '$unit_packing' ORDER BY b.kode_brg ";
		
		$query	= $this->db->query($sql);
			
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {	
				
				$sqlxx = " SELECT c.stok,  c.id_warna, b.nama as nama_warna 
							FROM tm_warna b INNER JOIN tm_stok_unit_packing_warna c ON b.id = c.id_warna
							WHERE c.id_stok_unit_packing = '$row->id' ";
				
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					
					foreach ($hasilxx as $rowxx) {
						
						
						// 22-01-2016 DIKOMEN. 04-02-2016 DIPAKE LAGI PERWARNA
						$detail_warna[] = array(
										'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0,
									
										);
					}
				}
				else {
					$sqlxx = " SELECT a.id_warna, b.nama as nama_warna FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip = '$row->id_brg_wip' ";
					
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						
						foreach ($hasilxx as $rowxx) {
							$detail_warna[] = array(
										'id_warna'=> $rowxx->id_warna,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0,
										'stok_opname_bgs'=> 0,
										'stok_opname_perbaikan'=> 0
									);
						}
					}
					else
						$detail_warna = '';
				}

				$detail_bahan[] = array('id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										// 22-01-2016 ini ga usah dimunculin lg di form inputnya. cukup 0 aja
										//'jum_stok_opname'=> 0,
										//'stok'=> $row->stok,
										// 22-01-2016 dikomen, karena skrg global. 04-02-2016 PER WARNA DIPAKE LAGI
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else
			$detail_bahan = '';
		
		return $detail_bahan;
  }
  function get_all_stok_opname_unit_packing($bulan, $tahun, $unit_packing) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT a.id as id_header, b.*, d.kode_brg, d.nama_brg FROM tt_stok_opname_unit_packing_detail b 
					INNER JOIN tt_stok_opname_unit_packing a ON b.id_stok_opname_unit_packing = a.id 
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_unit = '$unit_packing'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_unit_packing_detail_warna a 
							INNER JOIN tm_warna c ON c.id = a.id_warna  
							WHERE a.id_stok_opname_unit_packing_detail = '$row->id'
							ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_packing_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_unit_packing a INNER JOIN tm_stok_unit_packing_warna b ON a.id = b.id_stok_unit_packing
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_unit = '$unit_packing'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									//'saldo_akhir'=> $stok,
									'saldo_akhir'=> $rowxx->saldo_akhir,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'stok'=> $stok,
								);
					}
				}
				else
					$detail_warna = '';
				
			
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id'=> $row->id,
										'id_brg_wip'=> $row->id_brg_wip,
										'kode_brg_wip'=> $row->kode_brg,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
   function cek_sopertamakali_unitpacking($unit_packing) {
	$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing WHERE id_unit = '$unit_packing' ");
	if ($query3->num_rows() > 1){
		return 'f';
	}
	else {
		return 't';
	}
  }
  function savesounitpacking($is_new, $id_stok, $is_pertamakali, $id_brg_wip, $id_warna, $stok, $stok_fisik, 
		$id_warna2, $saldo_akhir, $unit_packing, $hapusitem){ 
	
		$tgl = date("Y-m-d H:i:s"); 
	  
	  if ($is_pertamakali != '1') {
		// 06-02-2014
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		//$qtytotalsaldoakhir = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			
			//01-12-2015
			//$id_warna2[$xx] = trim($id_warna2[$xx]);
			//$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);
						
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
			//$qtytotalsaldoakhir+= $saldo_akhir[$xx];
		} // end for
		// ---------------------------------------------------------------------
	  
		  if ($is_new == '1') {
			  // 26-01-2016 tambahkan skrip utk hapus ke tm_stok_unit_packing jika diceklis
			  if ($hapusitem == 'y') {
				  $this->db->query(" DELETE FROM tm_stok_unit_packing_warna  WHERE id_stok_unit_packing IN 
							(select id FROM tm_stok_unit_packing WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$unit_packing') ");
				  $this->db->query(" DELETE FROM tm_stok_unit_packing WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$unit_packing' ");
			  }
			  else {
				    $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
					if($seq->num_rows() > 0) {
						$seqrow	= $seq->row();
						$iddetail	= $seqrow->id+1;
					}else{
						$iddetail	= 1;
					}
				
				   $data_detail = array(
								'id'=>$iddetail,
								'id_stok_opname_unit_packing'=>$id_stok,
								'id_brg_wip'=>$id_brg_wip, 
								'stok_awal'=>$qtytotalstokawal,
								'jum_stok_opname'=>$qtytotalstokfisik,
								'saldo_akhir'=>0
							);
				   $this->db->insert('tt_stok_opname_unit_packing_detail',$data_detail);
				   					
					// ----------------------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok[$xx] = trim($stok[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);
						//01-12-2015
						//$id_warna2[$xx] = trim($id_warna2[$xx]);
						//$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);
						
						$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}
						
						$tt_stok_opname_unit_packing_detail_warna	= array(
								 'id'=>$idbaru,
								 'id_stok_opname_unit_packing_detail'=>$iddetail,
								 'id_warna'=>$id_warna[$xx],
								 'jum_stok_opname'=>$stok_fisik[$xx],
								 //'saldo_akhir'=>$saldo_akhir[$xx]
								 'saldo_akhir'=>0
							);
							$this->db->insert('tt_stok_opname_unit_packing_detail_warna',$tt_stok_opname_unit_packing_detail_warna);
					} // end for
			 } // end pengecekan
		  }
		  else {
			  // ambil id detail id_stok_opname_unit_packing_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail 
							where id_brg_wip = '$id_brg_wip' 
							AND id_stok_opname_unit_packing = '$id_stok' ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
			  
			  $this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET jum_stok_opname = '$qtytotalstokfisik', 
								saldo_akhir = '0'
								where id = '$iddetail' ");
				// saldo_akhir = '$qtytotalsaldoakhir'
				
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."',
								saldo_akhir = '0'
								WHERE id_stok_opname_unit_packing_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
					// saldo_akhir = '".$saldo_akhir[$xx]."'
				}
				// ====================
		  }
	  }
	  else {
		  // 04-02-2016
		$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		$qtytotalsaldoakhir = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			
			//01-12-2015
			$id_warna2[$xx] = trim($id_warna2[$xx]);
			$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);
						
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
			$qtytotalsaldoakhir+= $saldo_akhir[$xx];
		} // end for
		// ---------------------------------------------------------------------
	  
		  if ($is_new == '1') {
			  $seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
				if($seq->num_rows() > 0) {
					$seqrow	= $seq->row();
					$iddetail	= $seqrow->id+1;
				}else{
					$iddetail	= 1;
				}
			
				$data_detail = array(
								'id'=>$iddetail,
								'id_stok_opname_unit_packing'=>$id_stok,
								'id_brg_wip'=>$id_brg_wip, 
								'stok_awal'=>$qtytotalstokawal,
								'jum_stok_opname'=>$qtytotalstokfisik,
								'saldo_akhir'=>$qtytotalsaldoakhir
							);
				$this->db->insert('tt_stok_opname_unit_packing_detail',$data_detail);
				   					
				// ----------------------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok[$xx] = trim($stok[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);
						//01-12-2015
						$id_warna2[$xx] = trim($id_warna2[$xx]);
						$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);
						
						$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}
						
						$tt_stok_opname_unit_packing_detail_warna	= array(
								 'id'=>$idbaru,
								 'id_stok_opname_unit_packing_detail'=>$iddetail,
								 'id_warna'=>$id_warna[$xx],
								 'jum_stok_opname'=>$stok_fisik[$xx],
								 'saldo_akhir'=>$saldo_akhir[$xx]
							);
							$this->db->insert('tt_stok_opname_unit_packing_detail_warna',$tt_stok_opname_unit_packing_detail_warna);
					} // end for
		  }
		  else {
			  // ambil id detail id_stok_opname_unit_packing_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail 
							where id_brg_wip = '$id_brg_wip' 
							AND id_stok_opname_unit_packing = '$id_stok' ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
			  
			  $this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET jum_stok_opname = '$qtytotalstokfisik', 
								saldo_akhir = '$qtytotalsaldoakhir'
								where id = '$iddetail' ");
				
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."',
								saldo_akhir = '".$saldo_akhir[$xx]."'
								WHERE id_stok_opname_unit_packing_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
				}
				// ====================
		  }
	  } // END IF
  }
  
  
  
}
