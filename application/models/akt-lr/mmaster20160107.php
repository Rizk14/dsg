<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacapendapatan($periode)
	{
		$query=$this->db->query("select b.v_saldo_akhir as v_saldo_akhir, a.e_coa_name, a.i_coa
								             from tr_coa a, tm_coa_saldo b 
								             where i_periode = '$periode' and i_coa_group='4' and a.i_coa=b.i_coa
								             order by a.i_coa",false);
		if ($query->num_rows() > 0){
      return $query->result();
    }
}

	function bacahasilpenjualan($periode)
	{
		$query=$this->db->query("select cast(notakotor/1.1 as numeric) as v_gross 
								from(select sum(v_nota_gross) as notakotor
								from tm_nota
								where f_nota_cancel='f' and to_char(d_nota,'yyyymm')='$periode' and not i_nota isnull
								)as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}
 }	
 function bacapersediaanakhirbarangjadi($periode)
	{
		$query=$this->db->query("select * from tm_hpp where e_periode='$periode'",false);
		if ($query->num_rows() > 0){
		  $total=0;
		  $grandtotal=0;
			foreach($query->result() as $kotor){
			  $total=($kotor->n_opname_total)*$kotor->v_harga;
        $grandtotal=$grandtotal+$total;
			}
			return $grandtotal;
		}
 }
  function bacapersediaanawalbarangjadi($periodekurang)
	{
		$query=$this->db->query("select * from tm_hpp where e_periode='$periodekurang'",false);
		if ($query->num_rows() > 0){
		  $total=0;
		  $grandtotal=0;
			foreach($query->result() as $kotor){
			  $total=($kotor->n_opname_total)*$kotor->v_harga;
        $grandtotal=$grandtotal+$total;
			}
			return $grandtotal;
		}
 }

 function bacabiayapenjualan($periode)
	{
		$query=$this->db->query("select cast(sum(data.bank+data.kk+data.kb) as integer) as penjualan
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$periode' and i_coa like '%612.%' and f_kbank_cancel='f'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$periode' and i_coa like '%612.%' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$periode' and i_coa like '%612.%' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->penjualan;
			}
			return $tmp;
		}
 }

 function bacabiayaadmumum($periode)
	{
		$query=$this->db->query("select cast(sum(data.bank+data.kk+data.kb) as integer) as umum
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$periode' and not i_coa like '%612.%' and i_coa like '6%' 
														and f_kbank_cancel='f'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$periode' and not i_coa like '%612.%' and i_coa like '6%'  
														and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$periode' and not i_coa like '%612.%' and i_coa like '6%' 
														and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->umum;
			}
			return $tmp;
		}
 }

 function bacabiayabungabank($periode)
	{
		$query=$this->db->query("select v_saldo_akhir from tm_coa_saldo 
								where i_periode='$periode' and i_coa like '%720.100%'",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
 }

  	function bacapendapatanlainlain($periode)
	{
		$query=$this->db->query("select sum(data.bank+data.kk+data.kb) as lain
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$periode' and i_coa like '%710.400%' and f_kbank_cancel='f'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$periode' and i_coa like '%710.400%' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$periode' and i_coa like '%710.400%' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->lain;
			}
			return $tmp;
		}
 }

	function bacareturpenjualan($periode)
	{
		$query=$this->db->query("select cast(knkotor/1.1 as numeric) as v_gross 
								from(
								 select sum(v_gross) as knkotor
								 from tm_kn
								 where f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$periode' 
								 )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}

		 }

	function bacapotonganpenjualan($periode)
	{
		$query=$this->db->query("select cast(diskon as numeric) as diskontotal
								from(
								select sum(v_nota_discounttotal) as diskon
								from tm_nota
								where f_nota_cancel='f' and to_char(d_nota,'yyyymm')='$periode' 
								)as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->diskontotal;
			}
			return $tmp;
		}


/*
		$query=$this->db->query("select sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='4' and b.i_coa like '411%'
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			$tmp1=0;
			foreach($query->result() as $kotor){
				$tmp1=$kotor->v_saldo_akhir;
			}
		}
		$query=$this->db->query("select sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='4' and b.i_coa like '412%'
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			$tmp2=0;
			foreach($query->result() as $kotor){
				$tmp2=$tmp2+$kotor->v_saldo_akhir;
			}
		}
		$tmp=$tmp1-$tmp2;
		return $tmp;

*/
	}
	function bacapembelian($periode)
	{
		$query=$this->db->query("select b.v_saldo_akhir as v_saldo_akhir, a.e_coa_name, a.i_coa
								             from tr_coa a, tm_coa_saldo b 
								             where i_periode = '$periode' and i_coa_group='5' and a.i_coa=b.i_coa and a.i_coa like '51%'
								             order by a.i_coa",false);
		if ($query->num_rows() > 0){
      return $query->result();
    }
  }
	function bacabebanoperasional($periode)
	{
		$query=$this->db->query("select sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='6' and b.i_coa like '61%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
	}

		function bacapembelianbarangjadi($periode)
	{
		$query=$this->db->query(" select cast(notakotor as numeric) as v_gross 
								              from(select sum(v_gross) as notakotor from tm_dtap where f_dtap_cancel='f' 
								              and to_char(d_dtap,'yyyymm')='$periode')as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}
 }
 		function bacareturpembelian($periode)
	{
		$query=$this->db->query("select cast(knkotor/1.1 as numeric) as v_gross from( select sum(v_bbkretur) as knkotor from tm_bbkretur
							               where f_bbkretur_cancel='f' and to_char(d_bbkretur,'yyyymm')='$periode')as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}
  }
	function bacapotonganpembelian($periode)
	{
		$query=$this->db->query(" select cast(diskon as numeric) as diskontotal from(select sum(v_discount) as diskon from tm_dtap
								              where f_dtap_cancel='f' and to_char(d_dtap,'yyyymm')='$periode')as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->diskontotal;
			}
			return $tmp;
		}
 }
	function bacapendapatanlain($periode)
	{
		$query=$this->db->query("select b.v_saldo_akhir as v_saldo_akhir, a.e_coa_name, a.i_coa
								             from tr_coa a, tm_coa_saldo b 
								             where i_periode = '$periode' and a.i_coa='710.400' and a.i_coa=b.i_coa
								             order by a.i_coa",false);
		if ($query->num_rows() > 0){
      return $query->result();
    }
  }
	function bacabebanadministrasi($periode)
	{
		$query=$this->db->query("select sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='6' and b.i_coa like '62%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
	}
	function bacabebanlainnya($periode)
	{
		$query=$this->db->query("select sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='6' and not b.i_coa like '61%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
	}
	function bacahadiah($periode)
	{
		$query=$this->db->query(" select sum(b.n_quantity*b.v_unit_price) as ju from tm_bbk a, tm_bbk_item b where a.f_bbk_cancel = 'f' 
                              and a.i_bbk=b.i_bbk and a.i_bbk_type=b.i_bbk_type AND to_char(a.d_bbk,'yyyymm')='$periode' 
                              AND a.i_bbk_type='03'",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->ju;
			}
			return $tmp;
		}
	}
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select i_coa, e_coa_name, e_coa_name1, e_coa_name2, f_coa_status, i_coa_group from tr_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select i_coa, e_coa_name, e_coa_name1, e_coa_name2, f_coa_status, i_coa_group from tr_coa where upper(i_coa) like '%$cari%' or e_coa_name like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function saldoawal($periode,$icoa)
    {
		$this->db->select("	v_saldo_awal from tm_coa_saldo
							where i_periode = '$periode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s":
		        $sec += $number;
		        break;
		    case "n":
		        $min += $number;
		        break;
		    case "h":
		        $hr += $number;
		        break;
		    case "d":
		        $day += $number;
		        break;
		    case "ww":
		        $day += ($number * 7);
		        break;
		    case "m": 
		        $mon += $number;
		        break;
		    case "yyyy": 
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function NamaBulan($bln){
		switch($bln){
			case "01" 	:
				$NMbln = "Januari";
				break;
			case "02" 	:
				$NMbln = "Februari";
				break;
			case "03" 	:
				$NMbln = "Maret";
				break;
			case "04" 	:
				$NMbln = "April";
				break;
			case "05" 	:
				$NMbln = "Mei";
				break;
			case "06" 	:
				$NMbln = "Juni";
				break;
			case "07" 	:
				$NMbln = "Juli";
				break;
			case "08" 	:
				$NMbln = "Agustus";
				break;
			case "09" 	:
				$NMbln = "September";
				break;
			case "10" 	:
				$NMbln = "Oktober";
				break;
			case "11" 	:
				$NMbln = "November";
				break;
			case "12"  	:
				$NMbln = "Desember";
				break;
		}
		return ($NMbln);
	}
}
?>
