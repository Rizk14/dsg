<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacapendapatan($periode)
	{
		$query=$this->db->query("select b.saldo_akhir as v_saldo_akhir, a.nama as e_coa_name, a.kode as i_coa 
								             from tm_coa a, tt_saldo_akun b 
								             where i_periode = '$periode' and i_coa_group='4' and a.id=b.id_coa
								             order by a.id",false);
		if ($query->num_rows() > 0){
      return $query->result();
    }
  }

	function bacahasilpenjualan($periode)
	{
		$db2=$this->load->database('db_external', TRUE);
		
		$query=$db2->query("select cast(notakotor/1.1 as numeric) as v_gross 
		FROM(
		Select sum(x.v_grand_sisa) as notakotor
		FROM (
		SELECT i_faktur, v_grand_sisa,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, v_grand_sisa,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, v_grand_sisa,d_faktur FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		) as x where to_char(d_faktur,'yyyymm')='$periode' and not i_faktur isnull )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}
 }	
 function bacapersediaanakhirbarangjadi($periode)
	{
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query("select * from tm_so a inner join tr_product_price b on a.i_product_motif=b.i_product_motif 
		where to_char(d_do,'yyyymm')='$periode'",false);
		if ($query->num_rows() > 0){
		  $total=0;
		  $grandtotal=0;
			foreach($query->result() as $kotor){
			  $total=($kotor->n_saldo_akhir)*$kotor->v_price;
        $grandtotal=$grandtotal+$total;
			}
			return $grandtotal;
		}
 }
  function bacapersediaanawalbarangjadi($periode)
	{
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query("select * from tm_so a inner join tr_product_price b on a.i_product_motif=b.i_product_motif 
		where to_char(d_do,'yyyymm')='$periode'",false);
		if ($query->num_rows() > 0){
		  $total=0;
		  $grandtotal=0;
			foreach($query->result() as $kotor){
			  $total=($kotor->n_saldo_awal)*$kotor->v_price;
        $grandtotal=$grandtotal+$total;
			}
			return $grandtotal;
		}
 }

 function bacabiayapenjualan($periode)
	{
		$query=$this->db->query("select cast(sum(data.bank+data.kk+data.kb) as integer) as penjualan
                            from(
                            select kode_coa, sum(jumlah) as bank, 0 as kk, 0 as kb from tt_kas_bank
                            where to_char(tgl,'yyyymm')='$periode' and kode_coa like '%612.%' and f_cancel_bk='f'
                            group by kode_coa
                            union all
                            select kode_coa, 0 as bank,sum(jumlah) as kk, 0 as kb from tt_kas_kecil
                            where to_char(tgl,'yyyymm')='$periode' and kode_coa like '%612.%' and f_cancel_kk='f'
                            group by kode_coa 
                            union all
                            select kode_coa, 0 as bank,0 as kk, sum(jumlah) as kb from tt_kas_besar
                            where to_char(tgl,'yyyymm')='$periode' and kode_coa like '%612.%' and f_cancel_kb='f'
                            group by kode_coa 
                            )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->penjualan;
			}
			return $tmp;
		}
 }

 function bacabiayaadmumum($periode)
	{
		$query=$this->db->query("select cast(sum(data.bank+data.kk+data.kb) as integer) as umum
                            from(
                            select kode_coa, sum(jumlah) as bank, 0 as kk, 0 as kb from tt_kas_bank
                            where to_char(tgl,'yyyymm')='$periode' and not kode_coa like '%612.%' and kode_coa like '6%' 
														and f_cancel_bk='f'
                            group by kode_coa
                            union all
                            select kode_coa, 0 as bank,sum(jumlah) as kk, 0 as kb from tt_kas_kecil
                            where to_char(tgl,'yyyymm')='$periode' and not kode_coa like '%612.%' and kode_coa like '6%'  
														and f_cancel_kk='f'
                            group by kode_coa 
                            union all
                            select kode_coa, 0 as bank,0 as kk, sum(jumlah) as kb from tt_kas_besar
                            where to_char(tgl,'yyyymm')='$periode' and not kode_coa like '%612.%' and kode_coa like '6%' 
														and f_cancel_kb='f'
                            group by kode_coa 
                            )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->umum;
			}
			return $tmp;
		}
 }

 function bacabiayabungabank($periode)
	{
		$query=$this->db->query("select saldo_akhir as v_saldo_akhir from tt_saldo_akun a inner join tm_coa b ON a.id_coa=b.id
								where i_periode='$periode' and kode like '%720.100%'",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
 }

  	function bacapendapatanlainlain($periode)
	{
		$query=$this->db->query("select sum(data.bank+data.kk+data.kb) as lain
                            from(
                            select kode_coa, sum(jumlah) as bank, 0 as kk, 0 as kb from tt_kas_bank
                            where to_char(tgl,'yyyymm')='$periode' and kode_coa like '%710.400%' and f_cancel_bk='f'
                            group by kode_coa
                            union all
                            select kode_coa, 0 as bank,sum(jumlah) as kk, 0 as kb from tt_kas_kecil
                            where to_char(tgl,'yyyymm')='$periode' and kode_coa like '%710.400%' and f_cancel_kk='f'
                            group by kode_coa 
                            union all
                            select kode_coa, 0 as bank,0 as kk, sum(jumlah) as kb from tt_kas_besar
                            where to_char(tgl,'yyyymm')='$periode' and kode_coa like '%710.400%' and f_cancel_kb='f'
                            group by kode_coa 
                            )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->lain;
			}
			return $tmp;
		}
 }

	function bacareturpenjualan($periode)
	{
		$query=$this->db->query("select cast(knkotor as numeric) as v_gross 
								from(
								 select sum(a.knkotor) as knkotor from(
								 select sum(v_netto/1.1) as knkotor
								 from tm_kn
								 where f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$periode' and i_kn_type = '01'
								 union all
								 select sum(v_netto) as knkotor
								 from tm_kn
								 where f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$periode' and i_kn_type = '01' and i_pajak isnull
								 ) as a
								 )as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}

	}

	function bacapotonganpenjualan($periode)
	{
		$db2=$this->load->database('db_external', TRUE);
		$query=$this->db->query("
		select cast(diskon as numeric) as diskontotal
								from(
								Select sum(x.v_discount) as diskon
		FROM (
		SELECT i_faktur, v_discount,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, v_discount,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, v_discount,d_faktur FROM tm_faktur_do_t
		) as x  WHERE  to_char(d_faktur,'yyyymm')='$periode' 
								)as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->diskontotal;
			}
			return $tmp;
		}


/*
		$query=$this->db->query("select sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b where i_periode = '$periode' and i_coa_group='4' and b.i_coa like '411%'
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			$tmp1=0;
			foreach($query->result() as $kotor){
				$tmp1=$kotor->v_saldo_akhir;
			}
		}
		$query=$this->db->query("select sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b where i_periode = '$periode' and i_coa_group='4' and b.i_coa like '412%'
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			$tmp2=0;
			foreach($query->result() as $kotor){
				$tmp2=$tmp2+$kotor->v_saldo_akhir;
			}
		}
		$tmp=$tmp1-$tmp2;
		return $tmp;

*/
	}
	function bacapembelian($periode)
	{
		$query=$this->db->query(		
		"select b.saldo_akhir as v_saldo_akhir, a.nama as e_coa_name, a.kode as i_coa
								             from tm_coa a, tt_saldo_akun b 
								             where i_periode = '$periode' and i_coa_group='5' and a.id=b.id_coa and a.kode like '51%'
								             order by a.kode",false);
		if ($query->num_rows() > 0){
      return $query->result();
    }
  }
	function bacabebanoperasional($periode)
	{
		$query=$this->db->query("
		select sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b where i_periode = '$periode' and i_coa_group='6' and a.kode like '61%' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
	}

		function bacapembelianbb($periode)
	{
		$query=$this->db->query(" select cast(notakotor as numeric) as v_gross 
								              from(select sum(total) as notakotor from tm_pembelian where 
								              to_char(tgl_sj,'yyyymm')='$periode')as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}
 }
 		function bacareturpembelian($periode)
	{
		$query=$this->db->query("select cast(knkotor/1.1 as numeric) as v_gross from( select sum(jumlah) as knkotor from tm_retur_beli
							               where  to_char(tgl_retur,'yyyymm')='$periode')as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_gross;
			}
			return $tmp;
		}
  }
	function bacapotonganpembelian($periode)
	{
		$query=$this->db->query(" select cast(diskon as numeric) as diskontotal from(select sum(diskon) as diskon 
							from tm_pembelian a inner join tm_pembelian_detail b ON b.id_pembelian=a.id
								      AND a.status_aktif = 't' and to_char(tgl_sj,'yyyymm')='$periode')as data",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->diskontotal;
			}
			return $tmp;
		}
 }
	function bacapendapatanlain($periode)
	{
		$query=$this->db->query("select b.saldo_akhir as v_saldo_akhir, a.nama as e_coa_name, a.kode as i_coa
								             from tm_coa a, tt_saldo_akun b 
								             where i_periode = '$periode' and a.kode='710.400' and a.id=b.id_coa
								             order by a.id",false);
		if ($query->num_rows() > 0){
      return $query->result();
    }
  }
	function bacabebanadministrasi($periode)
	{
		$query=$this->db->query("select sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b where i_periode = '$periode' and i_coa_group='6' and a.kode like '62%' 
								 and a.id=b.id_coa",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
	}
	function bacabebanlainnya($periode)
	{
		$query=$this->db->query("
		select sum(b.saldo_akhir) as v_saldo_akhir
								 from tm_coa a, tt_saldo_akun b where i_periode = '$periode' and i_coa_group='6' and a.kode like '61%' 
								 and a.id=b.id_coa
		
		",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->v_saldo_akhir;
			}
			return $tmp;
		}
	}
	function bacahadiah($periode)
	{
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" select sum(b.n_unit*b.v_unit_price) as ju from tm_bbk a, tm_bbk_item b where a.f_bbk_cancel = 'f' 
                              and a.i_bbk=b.i_bbk AND to_char(a.d_bbk,'yyyymm')='$periode' 
                              ",false);
		if ($query->num_rows() > 0){
			foreach($query->result() as $kotor){
				$tmp=$kotor->ju;
			}
			return $tmp;
		}
	}
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select i_coa, e_coa_name, e_coa_name1, e_coa_name2, f_coa_status, i_coa_group from tm_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select i_coa, e_coa_name, e_coa_name1, e_coa_name2, f_coa_status, i_coa_group from tm_coa where upper(i_coa) like '%$cari%' or e_coa_name like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function saldoawal($periode,$icoa)
    {
		$this->db->select("	v_saldo_awal from tt_saldo_akun
							where i_periode = '$periode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s":
		        $sec += $number;
		        break;
		    case "n":
		        $min += $number;
		        break;
		    case "h":
		        $hr += $number;
		        break;
		    case "d":
		        $day += $number;
		        break;
		    case "ww":
		        $day += ($number * 7);
		        break;
		    case "m": 
		        $mon += $number;
		        break;
		    case "yyyy": 
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function NamaBulan($bln){
		switch($bln){
			case "01" 	:
				$NMbln = "Januari";
				break;
			case "02" 	:
				$NMbln = "Februari";
				break;
			case "03" 	:
				$NMbln = "Maret";
				break;
			case "04" 	:
				$NMbln = "April";
				break;
			case "05" 	:
				$NMbln = "Mei";
				break;
			case "06" 	:
				$NMbln = "Juni";
				break;
			case "07" 	:
				$NMbln = "Juli";
				break;
			case "08" 	:
				$NMbln = "Agustus";
				break;
			case "09" 	:
				$NMbln = "September";
				break;
			case "10" 	:
				$NMbln = "Oktober";
				break;
			case "11" 	:
				$NMbln = "November";
				break;
			case "12"  	:
				$NMbln = "Desember";
				break;
		}
		return ($NMbln);
	}
}
?>
