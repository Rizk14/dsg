<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function jmlFaktur($ifakturcode)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return	$db2->query(" SELECT * FROM tm_faktur_bhnbaku_item a INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur WHERE b.i_faktur_code='$ifakturcode' ");
	}

	function lbarangjadiperpages($limit, $offset)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query("
				SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
						
				FROM tm_faktur_bhnbaku_item b
				
				INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t' AND a.f_faktur_cancel = 'f'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT " . $limit . " OFFSET " . $offset . " ");

		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi()
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
			a.i_faktur_code AS ifakturcode,
			a.d_faktur AS dfaktur
						
		FROM tm_faktur_bhnbaku_item b
				
		INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
		INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
		INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
		WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
		GROUP BY a.d_faktur, a.i_faktur_code
				
		ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}

	function flbarangjadi($key)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
				a.i_faktur_code AS ifakturcode,
				a.d_faktur AS dfaktur
				
		FROM tm_faktur_bhnbaku_item b
				
		INNER JOIN tm_faktur_bhnbaku a ON a.i_faktur=b.i_faktur
		INNER JOIN tm_sj_bhnbaku_item c ON trim(c.i_product)=trim(b.i_product)
		INNER JOIN tm_sj_bhnbaku d ON trim(d.i_sj)=trim(c.i_sj)
				
		WHERE a.i_faktur_code='$key' AND d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
		GROUP BY a.d_faktur, a.i_faktur_code
				
		ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}

	function lsjbhnbakuperpages($ibranch, $limit, $offset)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT b.i_sj AS isj, b.i_sj_code AS isjcode, 
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.i_code_references,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai,
				a.e_satuan
			
			FROM tm_sj_bhnbaku_item a
			
			INNER JOIN tm_sj_bhnbaku b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.i_code_references,
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 b.i_sj_code,
					 a.e_satuan
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	LIMIT " . $limit . " OFFSET " . $offset);

		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lsjbhnbaku($ibranch)
	{
		$db2 = $this->load->database('db_external', TRUE);
		/*** 10042012
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.i_code_references,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai,
				a.e_satuan
			
			FROM tm_sj_bhnbaku_item a
			
			INNER JOIN tm_sj_bhnbaku b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND c.e_initial='$ibranch' AND a.n_unit_akhir > 0
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.i_code_references,
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 b.i_sj_code,
					 a.e_satuan
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";
		 ***/

		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.i_code_references,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai,
				a.e_satuan
			
			FROM tm_sj_bhnbaku_item a
			
			INNER JOIN tm_sj_bhnbaku b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj, 
					 b.i_customer, 
					 b.i_branch, 
					 a.i_product, 
					 a.i_code_references,
					 a.e_product_name, 
					 a.v_product_price, 
					 a.n_unit, 
					 a.n_unit_akhir,
					 b.i_sj_code,
					 a.e_satuan
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";

		return $db2->query($qstr);
	}

	function flsjbhnbaku($ibranch, $key)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$ky_upper	= $key;
		$qstr	= " SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode,
				b.i_customer, 
				b.i_branch,
				a.i_product AS imotif,
				a.i_code_references,
				a.e_product_name AS productname,
				a.v_product_price AS hjp,
				a.n_unit AS qty,
				a.n_unit_akhir AS qtyakhir,
				(a.v_product_price * a.n_unit_akhir) AS nilai,
				a.e_satuan
			
			FROM tm_sj_bhnbaku_item a
			
			INNER JOIN tm_sj_bhnbaku b ON trim(a.i_sj)=trim(b.i_sj)
			INNER JOIN tr_branch c ON c.i_branch_code=b.i_branch
			
			WHERE b.f_sj_cancel='f' AND (b.i_sj_code='$ky_upper' OR a.i_product='$ky_upper') AND c.e_initial='$ibranch'
			
			GROUP BY b.i_sj,
					 b.i_customer,
					 b.i_branch,
					 a.i_product,
					 a.i_code_references,
					 a.e_product_name,
					 a.v_product_price,
					 a.n_unit,
					 a.n_unit_akhir,
					 b.i_sj_code,
					 a.e_satuan
			
			ORDER BY b.i_sj_code DESC, a.e_product_name ASC	";

		return $db2->query($qstr);
	}

	function lklsbrg()
	{
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC ");

		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function clistpenjualanndo($nofaktur)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$qstr	= " SELECT
						b.i_faktur,
						b.i_faktur_code,
						c.i_product AS imotif,
						c.i_code_references,
						c.e_product_name AS motifname,
						a.n_quantity AS qty,
						a.v_unit_price AS unitprice,
						(a.n_quantity * a.v_unit_price) AS amount,
						b.f_printed,
						b.f_kontrabon
					FROM
						tm_faktur_bhnbaku_item a
					RIGHT JOIN tm_faktur_bhnbaku b ON
						b.i_faktur = a.i_faktur
					INNER JOIN tm_sj_bhnbaku_item c ON
						trim(c.e_product_name)= trim(a.e_product_name)
					INNER JOIN tm_sj_bhnbaku d ON
						trim(d.i_sj)= trim(c.i_sj)
					WHERE
						b.i_faktur_code = trim('$nofaktur')
						AND d.f_sj_cancel = 'f'
						AND d.f_faktur_created = 't'
						AND b.f_faktur_cancel = 'f'
					GROUP BY
						b.i_faktur,
						b.i_faktur_code,
						c.i_product,
						c.e_product_name,
						a.n_quantity,
						a.v_unit_price,
						c.i_code_references,
						b.f_printed,
						b.f_kontrabon
					ORDER BY
						c.i_product DESC,
						c.e_product_name ASC ";

		$query	= $db2->query($qstr);

		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function getfakheader($ifakturcode)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_bhnbaku WHERE cast(i_faktur_code AS integer)='$ifakturcode' AND f_faktur_cancel='f' ");
	}

	function geticustomer($ibranch)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_branch WHERE e_initial='$ibranch' ORDER BY i_branch DESC ");
	}

	function lfakturitem($ifaktur)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT i_faktur_item, i_faktur, i_sj, i_product, e_product_name, n_quantity, v_unit_price, (n_quantity*v_unit_price) AS amount, e_satuan  AS i_satuan
			FROM tm_faktur_bhnbaku_item WHERE i_faktur='$ifaktur' ");
		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lcabang($icustomer)
	{
		$db2 = $this->load->database('db_external', TRUE);
		if (!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}

		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.i_branch AS codebranch,
					a.i_branch_code AS ibranchcode,
				    a.i_customer AS codecustomer,
					a.e_branch_name AS ebranchname,
				    a.e_branch_name AS branch,
					a.e_initial AS einitial
					
					FROM tr_branch a 
					
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer " . $filter . " " . $order;

		$query	= $db2->query($strq);

		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function cari_fpenjualan($fpenj)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_faktur_bhnbaku WHERE i_faktur_code=trim('$fpenj') ");
	}

	function mupdate($i_faktur, $nw_d_faktur, $i_branch, $e_note_faktur, $v_total_nilai, $n_discount, $nw_v_discount, $nw_d_due_date, $nw_v_total_faktur, $i_faktur_pajak, $nw_d_pajak, $n_ppn, $f_cetak, $nw_v_total_fppn, $i_product, $e_product_name, $v_hjp, $n_quantity, $v_unit_price, $iteration, $isjcode, $ifakturcode, $ifakturX, $f_include_ppn, $e_satuan)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$i_faktur_item	= array();
		$tm_faktur_item	= array();

		$jml_item_br	= array();
		$qty_akhir	= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();

		$db2->trans_begin();

		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$fcetak	= $f_cetak == '1' ? 'TRUE' : 'FALSE';

		$qfakturitem	= $db2->query(" SELECT * FROM tm_faktur_bhnbaku_item WHERE i_faktur='$ifakturX' ");

		if ($qfakturitem->num_rows() > 0) {

			foreach ($qfakturitem->result() as $row) {

				/*** 10042012
				$qdoitem	= $db2->query(" SELECT * FROM tm_sj_bhnbaku_item WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' AND f_faktur_created='t' ");
				 ***/

				$qdoitem	= $db2->query(" SELECT * FROM tm_sj_bhnbaku_item WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' ");

				if ($qdoitem->num_rows() > 0) {

					$rdoitem	= $qdoitem->row();

					$ndeliver	= $rdoitem->n_unit;
					$nresidual	= (($rdoitem->n_unit_akhir) + ($row->n_quantity));

					if ($nresidual == '')
						$nresidual = 0;

					$qupdtdoitem	= array(
						'n_unit_akhir' => $nresidual
					);

					$db2->query(" UPDATE tm_sj_bhnbaku_item SET n_unit_akhir='$nresidual' WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' ");
				}
			}
		}

		$db2->delete('tm_faktur_bhnbaku_item', array('i_faktur' => $ifakturX));

		$tmfaktur = array(
			'i_faktur_code' => $i_faktur,
			'd_faktur' => $nw_d_faktur,
			'e_branch_name' => $i_branch,
			'd_due_date' => $nw_d_due_date,
			'i_faktur_pajak' => $i_faktur_pajak,
			'd_pajak' => $nw_d_pajak,
			'n_discount' => $n_discount,
			'v_discount' => $nw_v_discount,
			'v_total_faktur' => $nw_v_total_faktur,
			'v_total_fppn' => $nw_v_total_fppn,
			'f_include_ppn' => $f_include_ppn,
			'f_printed' => $fcetak,
			'e_note_faktur' => $e_note_faktur,
			'd_update' => $dentry
		);

		$db2->where('i_faktur', $ifakturX);
		$db2->update('tm_faktur_bhnbaku', $tmfaktur);

		for ($jumlah = 0; $jumlah <= $iteration; $jumlah++) {

			$seq_tm_i_faktur_item	= $db2->query(" SELECT cast(i_faktur_item AS integer) AS i_faktur_item FROM tm_faktur_bhnbaku_item ORDER BY cast(i_faktur_item AS integer) DESC LIMIT 1 ");

			if ($seq_tm_i_faktur_item->num_rows() > 0) {

				$seqrow	= $seq_tm_i_faktur_item->row();
				$i_faktur_item[$jumlah]	= $seqrow->i_faktur_item + 1;
			} else {
				$i_faktur_item[$jumlah]	= 1;
			}

			$tm_faktur_item[$jumlah]	= array(
				'i_faktur_item' => $i_faktur_item[$jumlah],
				'i_faktur' => $ifakturX,
				'i_sj' => $isjcode[$jumlah],
				'i_product' => $i_product[$jumlah],
				'e_product_name' => $e_product_name[$jumlah],
				'n_quantity' => $n_quantity[$jumlah],
				'v_unit_price' => $v_hjp[$jumlah],
				'e_satuan' => $e_satuan[$jumlah],
				'd_entry' => $dentry
			);

			$q_qty_sj_item	= $db2->query(" SELECT * FROM tm_sj_bhnbaku_item WHERE i_sj='$isjcode[$jumlah]' AND i_product='$i_product[$jumlah]' ");

			if ($q_qty_sj_item->num_rows() > 0) {

				$row_item_br	= $q_qty_sj_item->row();

				$jml_item_br[$jumlah]	= $row_item_br->n_unit_akhir;

				if ($n_quantity[$jumlah] == $jml_item_br[$jumlah]) {

					$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail

					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created' => 'TRUE'
					);

					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir' => $qty_akhir[$jumlah],
						'f_faktur_created' => 'TRUE'
					);
				} else if ($n_quantity[$jumlah] < $jml_item_br[$jumlah]) { // jika jmlitwm kurang dari qty brg yg ada

					$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created' => 'TRUE'
					);

					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir' => $qty_akhir[$jumlah],
						'f_faktur_created' => 'FALSE'
					);
				} else {
					$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_quantity[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail

					$arrfakturupdate[$jumlah]	= array(
						'f_faktur_created' => 'TRUE'
					);

					$arrfakturupdate2[$jumlah]	= array(
						'n_unit_akhir' => $qty_akhir[$jumlah],
						'f_faktur_created' => 'TRUE'
					);
				}
			}

			$db2->update('tm_sj_bhnbaku', $arrfakturupdate[$jumlah], array('i_sj' => $isjcode[$jumlah], 'f_faktur_created' => 'f'));
			$db2->update('tm_sj_bhnbaku_item', $arrfakturupdate2[$jumlah], array('i_sj' => $isjcode[$jumlah], 'i_product' => $i_product[$jumlah]));
			$db2->insert('tm_faktur_bhnbaku_item', $tm_faktur_item[$jumlah]);
		}

		if ($db2->trans_status() === FALSE || $db2->trans_status() == false) {
			$ok	= 0;
			$db2->trans_rollback();
		} else {
			$ok	= 1;
			$db2->trans_commit();
		}

		if ($ok == 1) {
			print "<script>alert(\"Nomor Faktur : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		} else {
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
	}

	function mbatal($i_faktur, $i_faktur_code)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qfakturitem	= $db2->query(" SELECT * FROM tm_faktur_bhnbaku_item WHERE i_faktur='$i_faktur' ");
		$numfakturitem	= $qfakturitem->num_rows();

		if ($numfakturitem > 0) {

			foreach ($qfakturitem->result() as $row) {

				$qdoitem	= $db2->query(" SELECT * FROM tm_sj_bhnbaku_item WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");

				if ($qdoitem->num_rows() > 0) {

					$rdoitem	= $qdoitem->row();

					//$ndeliver	= $rdoitem->n_deliver;
					$nresidual	= (($rdoitem->n_unit_akhir) + ($row->n_quantity));

					$db2->query(" UPDATE tm_sj_bhnbaku_item SET n_unit_akhir='$nresidual' WHERE i_sj='$row->i_sj' AND i_product='$row->i_product' AND (f_faktur_created='t' OR f_faktur_created='f') ");

					$db2->query(" UPDATE tm_sj_bhnbaku SET f_faktur_created='f' WHERE i_sj='$row->i_sj' AND f_faktur_created='t' ");
				}
			}

			$db2->query(" UPDATE tm_faktur_bhnbaku SET f_faktur_cancel='t' WHERE i_faktur='$i_faktur' AND f_faktur_cancel='f' ");
		} else {
		}

		if ($db2->trans_status() === FALSE || $db2->trans_status() == false) {
			$ok	= 0;
			$db2->trans_rollback();
		} else {
			$ok	= 1;
			$db2->trans_commit();
		}

		if ($ok == 1) {
			print "<script>alert(\"Nomor Faktur Penjualan : '\"+$i_faktur_code+\"' telah dibatalkan, terimakasih.\");window.open(\"../../index\", \"_self\");</script>";
		} else {
			print "<script>alert(\"Maaf, Data Faktur Penjuaan tdk ditemukan.\");window.open(\"../../index\", \"_self\");</script>";
		}
	}

	public function mreprint($ifakturcode, $inota)
	{
		$db2 = $this->load->database('db_external', TRUE);

		$data = array(
			'f_printed' => 'f',
		);
		$db2->where('i_faktur', $inota);
		$db2->update('tm_faktur_bhnbaku', $data);
		print "<script>alert(\"Nomor Nota : '\"+$ifakturcode+\"' Bisa dicetak ulang, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		// print "<script>alert(\"Nomor Nota : '\"+$ifakturcode+\"' Bisa dicetak ulang, terimakasih.\");show(\"index.php/listpenjualanperdo/cform/carilistpenjualanperdo/$ifakturcode/0/0/\",\"#content\");</script>";
	}
}
