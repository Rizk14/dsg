<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $id_jenis, $cgudang, $cstatus)
  {
	if ($cari == "all") {
		if ($id_jenis == '0') {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC ";
			$sql.= " order by a.kode_brg ";
			//echo $sql;
			$this->db->select($sql, false)->limit($num,$offset);
		}
		else {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND a.id_jenis_bahan = '$id_jenis' ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC "; echo $sql;
			$sql.= " order by a.kode_brg ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
	}
	else {
		if ($id_jenis == '0') {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
					FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
					WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC ";
			$sql.= " order by a.kode_brg ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
		else {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id 
						AND a.id_jenis_bahan = '$id_jenis' AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
						OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC ";
			$sql.= " order by a.kode_brg ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
    }
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari, $id_jenis, $cgudang, $cstatus){
	if ($cari == "all") {
		if ($id_jenis == '0') {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false);
		}
		else {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND a.id_jenis_bahan = '$id_jenis' ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false); //
		}
	}
	else {
		if ($id_jenis == '0') {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
					FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
					WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false);
		}
		else {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id 
						AND a.id_jenis_bahan = '$id_jenis' AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
						OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false);
		}
	}
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_barang',array('kode_brg'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg = '$id' ");
    return $query->result();
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang WHERE kode <> 'Z' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_satuan(){
    $this->db->select("* from tm_satuan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
    
  //function cek_data($kode, $id_jenis_bhn){
	  function cek_data($kode){
    $this->db->select("kode_brg from tm_barang WHERE kode_brg = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode,$nama, $nama_brg_sup, $id_jenis, $item, $motif, $warna, $satuan, $deskripsi, $gudang, $status_aktif, $kodeedit, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'kode_brg'=>$kode,
      'nama_brg'=>$nama,
      'nama_brg_supplier'=>$nama_brg_sup,
      'satuan'=>$satuan,
      'id_jenis_bahan'=>$id_jenis,
      'deskripsi'=>$deskripsi,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'id_gudang'=>$gudang,
      'kode_item'=>$item,
      'kode_motif'=>$motif,
      'kode_warna'=>$warna,
      'status_aktif'=>'t'
    );

    if ($goedit == '') {
		$this->db->insert('tm_barang',$data); 
	}
	else {
		/*$data = array(
				  'kode_brg'=>$kode,
				  'nama_brg'=>$nama,
				  'satuan'=>$satuan,
				  'id_jenis_bahan'=>$id_jenis,
				  'deskripsi'=>$deskripsi,
				  'tgl_update'=>$tgl,
				  'id_gudang'=>$gudang,
				  'kode_item'=>$item,
				  'kode_motif'=>$motif,
				  'kode_warna'=>$warna
				); */
		if ($status_aktif == '')
			$status_aktif = 'f';
		$data = array(
				  'nama_brg'=>$nama,
				  'nama_brg_supplier'=>$nama_brg_sup,
				  'satuan'=>$satuan,
				  'deskripsi'=>$deskripsi,
				  'tgl_update'=>$tgl,
				  'id_gudang'=>$gudang,
				  'status_aktif'=>$status_aktif
				);
		
		$this->db->where('kode_brg',$kodeedit);
		$this->db->update('tm_barang',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_barang', array('kode_brg' => $kode));
  }
  
  function get_jenis_barang($num, $offset, $kel_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' order by kode ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) 
		order by kode ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_barangtanpalimit($kel_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_jns_bhn(){
    $this->db->select("a.kode as kj_brg, a.nama as nj_brg, b.* from tm_jenis_barang a, tm_jenis_bahan b 
    WHERE a.id = b.id_jenis_barang order by a.kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bahan($num, $offset, $jns_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_bahantanpalimit($jns_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_item(){
    $this->db->select("* from tm_item order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_motif(){
    $this->db->select("* from tm_motif order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_warna(){
    $this->db->select("* from tm_warna order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
