<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();
  }
  
  function getAll($num, $offset, $cari, $id_jenis, $cgudang, $cstatus,$ckelompok_barang)
  {
	  $addcari=addslashes($cari);
	  $sql = " a.*, c.kode as kode_kel_brg, c.nama as nama_kel_brg, b.kode as kj_brg, b.nama as nj_brg, d.nama as nama_satuan 
						FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id
						INNER JOIN tm_kelompok_barang c ON b.kode_kel_brg = c.kode
						INNER JOIN tm_satuan d ON a.satuan = d.id WHERE TRUE ";
	  
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$addcari%') OR UPPER(a.nama_brg) like UPPER('%$addcari%')) ";
	  if ($id_jenis != '0')
		$sql.= " AND a.id_jenis_barang = '$id_jenis' ";
	  if ($cgudang != '0')
		$sql.= " AND a.id_gudang = '$cgudang' ";
	  if ($cstatus != '0')
		$sql.= " AND a.status_aktif = '$cstatus' ";
	 if ($ckelompok_barang != '0')
		$sql.= " AND c.id = '$ckelompok_barang' ";
	  
	  $sql.= " ORDER BY a.kode_brg ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
	/*if ($cari == "all") {
		if ($id_jenis == '0') {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id
						INNER JOIN tm_satuan d ON a.satuan = d.id
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC ";
			$sql.= " order by a.kode_brg ";
			$this->db->select($sql, false)->limit($num,$offset);
		}
		else {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND a.id_jenis_bahan = '$id_jenis' ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC "; echo $sql;
			$sql.= " order by a.kode_brg ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
	}
	else {
		if ($id_jenis == '0') {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
					FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
					WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC ";
			$sql.= " order by a.kode_brg ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
		else {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id 
						AND a.id_jenis_bahan = '$id_jenis' AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
						OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			//$sql.= " order by b.kode, c.kode, a.nama_brg, a.tgl_update DESC ";
			$sql.= " order by a.kode_brg ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
    } */
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari, $id_jenis, $cgudang, $cstatus,$ckelompok_barang){
	  $addcari=addslashes($cari);
	/*if ($cari == "all") {
		if ($id_jenis == '0') {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false);
		}
		else {
			$sql = " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND a.id_jenis_bahan = '$id_jenis' ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false); //
		}
	}
	else {
		if ($id_jenis == '0') {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
					FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
					WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false);
		}
		else {
			$sql= " a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
						FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d 
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id 
						AND a.id_jenis_bahan = '$id_jenis' AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
						OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			if ($cgudang != '0')
				$sql.= " AND a.id_gudang = '$cgudang' ";
			if ($cstatus != '0')
				$sql.= " AND a.status_aktif = '$cstatus' ";
			
			$this->db->select($sql, false);
		}
	} */
	
	$sql = " a.*, c.kode as kode_kel_brg, c.nama as nama_kel_brg, b.kode as kj_brg, b.nama as nj_brg, d.nama as nama_satuan 
						FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id
						INNER JOIN tm_kelompok_barang c ON b.kode_kel_brg = c.kode
						INNER JOIN tm_satuan d ON a.satuan = d.id WHERE TRUE ";
	  
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$addcari%') OR UPPER(a.nama_brg) like UPPER('%$addcari%')) ";
	  if ($id_jenis != '0')
		$sql.= " AND a.id_jenis_barang = '$id_jenis' ";
	  if ($cgudang != '0')
		$sql.= " AND a.id_gudang = '$cgudang' ";
	  if ($cstatus != '0')
		$sql.= " AND a.status_aktif = '$cstatus' ";
		
		if ($ckelompok_barang != '0')
		$sql.= " AND c.id = '$ckelompok_barang' ";
	
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get($id_barang){
    //$query = $this->db->getwhere('tm_barang',array('kode_brg'=>$id));
    $this->db->select(" * from tm_barang WHERE id='$id_barang' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang WHERE kode <> 'Z' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_satuan(){
    $this->db->select("* from tm_satuan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
    
 function cek_data($kode, $nama){
	$addnama=addslashes($nama);
    $this->db->select("kode_brg from tm_barang WHERE kode_brg = '$kode' OR nama_brg='$addnama' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //$item,
  function save($kode,$nama, $nama_brg_sup, $id_jenis_brg, $satuan, $deskripsi, 
				$gudang, $status_aktif, $id_barang, $goedit, 
				$satuan_konv, $rumus_konv, $angka_faktor_konversi, $is_mutasi_stok_harga){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_barang ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
	
	
			
    $data = array(
	  'id'=>$idbaru,
      'kode_brg'=>$kode,
      'nama_brg'=>$nama,
      'nama_brg_supplier'=>$nama_brg_sup,
      'satuan'=>$satuan,
      'id_jenis_barang'=>$id_jenis_brg,
      'deskripsi'=>$deskripsi,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'id_gudang'=>$gudang,
      'status_aktif'=>'t',
      // 29-10-2014
      'id_satuan_konversi'=>$satuan_konv,
      'rumus_konversi'=>$rumus_konv,
      'angka_faktor_konversi'=>$angka_faktor_konversi,
      'is_mutasi_stok_harga'=>$is_mutasi_stok_harga,
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_barang',$data); 
	}
	else {
		/*$data = array(
				  'kode_brg'=>$kode,
				  'nama_brg'=>$nama,
				  'satuan'=>$satuan,
				  'id_jenis_bahan'=>$id_jenis,
				  'deskripsi'=>$deskripsi,
				  'tgl_update'=>$tgl,
				  'id_gudang'=>$gudang,
				  'kode_item'=>$item,
				  'kode_motif'=>$motif,
				  'kode_warna'=>$warna
				); */
		if ($status_aktif == '')
			$status_aktif = 'f';
		$data = array(
				  'kode_brg'=>$kode,
				  'nama_brg'=>$nama,
				  'nama_brg_supplier'=>$nama_brg_sup,
				  'satuan'=>$satuan,
				  'id_jenis_barang'=>$id_jenis_brg,
				  'deskripsi'=>$deskripsi,
				  'tgl_update'=>$tgl,
				  'id_gudang'=>$gudang,
				  'status_aktif'=>$status_aktif,
				  // 29-10-2014
				  'id_satuan_konversi'=>$satuan_konv,
				  'rumus_konversi'=>$rumus_konv,
				  'angka_faktor_konversi'=>$angka_faktor_konversi,
				  'is_mutasi_stok_harga'=>$is_mutasi_stok_harga,
				  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_barang);
		$this->db->update('tm_barang',$data);  
	}
		
  }
  
  function delete($id_barang){    
    $this->db->delete('tm_barang', array('id' => $id_barang));
  }
  
  function get_jenis_barang($num, $offset, $kel_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' order by kode ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) 
		order by kode ASC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_barangtanpalimit($kel_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_jns_brg(){
    $this->db->select("a.kode as kode_kel_brg, a.nama as nama_kel_brg, b.* from tm_kelompok_barang a 
			INNER JOIN tm_jenis_barang b ON b.kode_kel_brg = a.kode
			ORDER BY a.kode, b.kode", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bahan($num, $offset, $jns_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_bahantanpalimit($jns_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_item(){
    $this->db->select("* from tm_item order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_motif(){
    $this->db->select("* from tm_motif order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_warna(){
    $this->db->select("* from tm_warna order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
