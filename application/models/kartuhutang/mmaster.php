<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
	// 04-12-2015
	function get_all_sjpembelian($date_from, $date_to, $supplier) {
		// cek apakah suppliernya itu makloon atau bukan
		$sqlcek = " SELECT kategori FROM tm_supplier WHERE id='$supplier' ";
		$querycek	= $this->db->query($sqlcek);
		if ($querycek->num_rows() > 0){
			$hasilcek = $querycek->row();
			$kategori = $hasilcek->kategori;
			
			if ($kategori == 1)
				$sql = " SELECT id, no_sj as no_bukti, tgl_sj as tgl_bukti, total, status_lunas,
						0 as pembulatan, 1 as pembelian, 'xx' as deskripsi
						FROM tm_pembelian WHERE status_aktif = 't'
						AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND id_supplier = '$supplier' ";
			else
				$sql = " SELECT id, no_sj as no_bukti, tgl_sj as tgl_bukti, total, status_lunas,
						0 as pembulatan, 1 as pembelian, 'xx' as deskripsi
						FROM tm_pembelian_makloon WHERE status_aktif = 't'
						AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND id_supplier = '$supplier' ";
						
			$sql.= " UNION SELECT a.id, a.no_voucher as no_bukti, a.tgl as tgl_bukti, 
					b.jumlah_bayar as total, '0' as status_lunas,
					b.pembulatan as pembulatan, 0 as pembelian, b.deskripsi as deskripsi
						FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
						WHERE a.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl <= to_date('$date_to','dd-mm-yyyy')
						AND b.id_supplier = '$supplier'
					ORDER BY tgl_bukti ASC, no_bukti ASC ";
			
			$query	= $this->db->query($sql);
				
			$data_beli = array();
			if ($query->num_rows() > 0){
				$hasil = $query->result();
				foreach ($hasil as $row1) {
					$list_brg = "";
					if ($kategori == 1)
						$sql2 = " SELECT nama_brg FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' ";
					else
						$sql2 = " SELECT nama_brg FROM tm_pembelian_makloon_detail WHERE id_pembelian_makloon = '$row1->id' ";
					$query2	= $this->db->query($sql2);
					if ($query2->num_rows() > 0) {
						$hasil2 = $query2->result();
						$hitung = count($hasil2);
						$k = 0;
						
						foreach ($hasil2 as $row2) {
							$list_brg.= $row2->nama_brg;
							if ($k<$hitung-1)
								$list_brg.= ", ";
						    $k++;
						} // end for2
					} // end if2
					
					$pisah1 = explode("-", $row1->tgl_bukti);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_bukti = $tgl1."-".$bln1."-".$thn1;
					
					if ($row1->pembelian == 1) {
						$pembelian = $row1->total;
						$pelunasan = '';
					}
					else {
						$pelunasan = $row1->total;
						$pembelian = '';
					}
					$data_beli[] = array(	'no_bukti'=> $row1->no_bukti,
											'tgl_bukti'=> $tgl_bukti,
											'pembelian'=> $pembelian,
											'pelunasan'=> $pelunasan,
											'pembulatan'=> $row1->pembulatan,
											'is_pembelian'=> $row1->pembelian,
											'deskripsi_pelunasan'=> $row1->deskripsi,
											'list_brg'=> $list_brg,
											'status_lunas'=> $row1->status_lunas
											);
				
					$list_brg = "";
				}
			}
			else
				$data_beli = '';
		}
		else
			$data_beli = '';
		
		// --------------------- END 04-12-2015 ----------------------------------------------------
		return $data_beli;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
}

