<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function clistdobrg($kddo)
	{
		$db2 = $this->load->database('db_external', TRUE);

		$query	= $db2->query(" SELECT
									a.d_do AS dop,
									a.i_do_code AS iopcode,
									a.i_branch AS ibranch,
									e.i_op_code AS iop,
									b.i_do_item,
									b.i_product AS iproduct,
									c.e_product_motifname AS motifname,
									sum(b.n_deliver) AS qty,
									sum(b.n_residual) AS belumfaktur,
									b.e_note AS keterangan,
									e.i_op_code AS iopcode
								FROM
									tm_do_item b
								RIGHT JOIN tm_do a ON
									a.i_do = b.i_do
								INNER JOIN tr_product_motif c ON
									trim(c.i_product_motif) = trim(b.i_product)
								INNER JOIN tr_product_base d ON
									trim(d.i_product_base) = trim(c.i_product)
								INNER JOIN tm_op e ON
									e.i_op = b.i_op
								WHERE
									a.i_do_code = '$kddo'
									AND a.f_do_cancel = 'f' 
									/*AND f_printed = 'f'*/
									/* AND n_print < 2 */
								GROUP BY
									a.d_do,
									a.i_do_code,
									a.i_branch,
									b.i_op,
									b.i_do_item,
									b.i_product,
									c.e_product_motifname,
									b.e_note,
									e.i_op_code
								ORDER BY
									b.i_product ASC ");

		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function cnmrop($kddo)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT b.i_op AS iop,  e.i_op_code AS iopcode FROM tm_do_item b 
				
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
			
			WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' ");
	}

	function ctgldobrg($kddo)
	{
		$db2 = $this->load->database('db_external', TRUE);

		return $db2->query(" SELECT a.d_do AS tgl
		
			FROM tm_do_item b
				
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' ");
	}

	function lbarangjadiperpages($limit, $offset)
	{

		$query	= $this->db->query(" SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code 
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
					
			WHERE c.n_active='1' AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code

			ORDER BY a.d_do DESC, a.i_do_code DESC LIMIT " . $limit . " OFFSET " . $offset);

		if ($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lbarangjadi()
	{

		return $this->db->query(" SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code 
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
			
			WHERE c.n_active='1' AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code

			ORDER BY a.d_do DESC, a.i_do_code DESC ");
	}

	function flbarangjadi($key)
	{

		return $this->db->query(" SELECT distinct(a.i_do_code), a.d_do, b.i_op, e.i_op_code  
				
			FROM tm_do_item b 
					
			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			INNER JOIN tm_op e ON e.i_op=b.i_op
					
			WHERE c.n_active='1' AND (e.i_op_code='$key' OR a.i_do_code='$key') AND a.f_do_cancel='f'

			GROUP BY a.i_do_code, a.d_do, b.i_op, e.i_op_code 

			ORDER BY a.d_do DESC, a.i_do_code DESC ");
	}

	function getcabang($kddo)
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT  a.i_do_code AS iopcode, e.e_branch_name AS cabangname, e.e_branch_address as addr, 
							f.e_customer_name AS pusatname, f.e_customer_address AS pusataddress
							FROM tm_do_item b
							INNER JOIN tm_do a ON a.i_do=b.i_do
							INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
							INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
							INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch
							INNER JOIN tr_customer f ON f.i_customer=e.i_customer
							WHERE a.i_do_code='$kddo' AND a.f_do_cancel='f' GROUP BY a.i_do_code, e.e_branch_name,f.e_customer_name, f.e_customer_address,e.e_branch_address ");
	}

	function remote($id)
	{
		return $this->db->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' ORDER BY i_printer DESC LIMIT 1 ");
	}

	function getinitial()
	{
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial_code DESC LIMIT 1");
	}

	function fprinted($kddo)
	{
		$db2 = $this->load->database('db_external', TRUE);
		$db2->query(" UPDATE tm_do SET f_printed='t', n_print = n_print+1 WHERE i_do_code='$kddo' ");
	}
}
