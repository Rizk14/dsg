<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}

	function lpelanggan(){	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_customer_name ASC, i_customer_code DESC ";
		$db2->select("i_customer_code AS code, e_customer_name AS customer FROM tr_customer WHERE i_customer_code in ('SAB','DGU','KAB','ANGRH') ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

		
	// function clistpenjualanperdoperpages($nofaktur,$ddo_first,$ddo_last,$limit,$offset,$pelanggan)
	/*Update 30-07-2022*/
	function clistpenjualanperdoperpages($nofaktur,$ddo_first,$ddo_last,$limit,$offset)
	 {
		$db2=$this->load->database('db_external', TRUE);
		//var_dump($pelanggan);
		if(!empty($nofaktur) && ($ddo_first!='0' && $ddo_last!='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$pelanggan1	= "";	
			$fbatal		= " AND b.f_faktur_cancel='f' ";		
		}else if(!empty($nofaktur) && ($ddo_first=='0' || $ddo_last=='0') ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";
			$pelanggan1	= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else if(!empty($pelanggan) && ($ddo_first=='0' || $ddo_last=='0')) {
			$pelanggan1	= "AND d.i_customer_code = '$pelanggan'";
			$ddo		= "";
			$nfaktur	= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}else if(!empty($pelanggan) && ($ddo_first!='0' && $ddo_last!='0')){
			$pelanggan1	= "AND d.i_customer_code = '$pelanggan'";
			$nfaktur	= "";
			$ddo		= "WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last')";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else if($ddo_first!='0' && $ddo_last!='0'){
			$nfaktur	= "";
			$pelanggan1	= "";
			$ddo		= " WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else{
			$pelanggan1	= "";
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}

			/* 08062011
			$strq	= " 
				SELECT 	e.d_do AS ddo,
						e.i_do_code AS idocode,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname,
						a.n_quantity AS qty,
						a.v_unit_price AS unitprice,
						sum((a.n_quantity * a.v_unit_price)) AS amount
					
				FROM tm_faktur_do_item a
					
				RIGHT JOIN tm_faktur_do b ON cast(b.i_faktur_code AS integer)=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo."
				
				GROUP BY e.i_do_code, c.i_product_motif, c.e_product_motifname, a.n_quantity, a.v_unit_price, e.d_do ORDER BY e.d_do DESC LIMIT ".$limit." OFFSET ".$offset;	
			*/
			/*
					(a.v_unit_price / a.n_quantity) AS unitprice, 
					sum(a.v_unit_price) AS amount 
			*/
			$strq	= "
			SELECT  b.i_faktur_code, c.i_do_code AS idocode, c.d_do AS ddo, a.i_product AS imotif, 
					a.e_product_name AS motifname, 
					sum(a.n_quantity) AS qty, 
					a.v_unit_price AS unitprice,
					sum((a.n_quantity * a.v_unit_price)) AS amount
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				INNER JOIN tr_customer d ON c.i_customer=d.i_customer 
				 
				".$nfaktur." ".$ddo." ".$fbatal." ".$pelanggan1."
				
				GROUP BY b.i_faktur_code, c.i_do_code, c.d_do, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ORDER BY c.i_do_code ASC, a.i_product ASC LIMIT ".$limit;
								
		$query	= $db2->query($strq);
			
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	// function clistpenjualanperdo($nofaktur,$ddo_first,$ddo_last, $pelanggan)
	
	/*Update 30-07-2022 */
	function clistpenjualanperdo($nofaktur,$ddo_first,$ddo_last)
	 {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && ($ddo_first!='0' && $ddo_last!='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$pelanggan1	= "";	
			$fbatal		= " AND b.f_faktur_cancel='f' ";		
		}else if(!empty($nofaktur) && ($ddo_first=='0' || $ddo_last=='0') ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";
			$pelanggan1	= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else if(!empty($pelanggan) && ($ddo_first=='0' || $ddo_last=='0')) {
			$pelanggan1	= "AND d.i_customer_code = '$pelanggan'";
			$ddo		= "";
			$nfaktur	= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}else if(!empty($pelanggan) && ($ddo_first!='0' && $ddo_last!='0')){
			$pelanggan1	= "AND d.i_customer_code = '$pelanggan'";
			$nfaktur	= "";
			$ddo		= "WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last')";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else if($ddo_first!='0' && $ddo_last!='0'){
			$nfaktur	= "";
			$pelanggan1	= "";
			$ddo		= " WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else{
			$pelanggan1	= "";
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}

			/* 10062011
			$strq	= " 
				SELECT 	e.d_do AS ddo,
						e.i_do_code AS idocode,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname,
						a.n_quantity AS qty,
						a.v_unit_price AS unitprice,
						sum((a.n_quantity * a.v_unit_price)) AS amount
					
				FROM tm_faktur_do_item a
					
				RIGHT JOIN tm_faktur_do b ON cast(b.i_faktur_code AS integer)=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo."
				
				GROUP BY e.i_do_code, c.i_product_motif, c.e_product_motifname, a.n_quantity, a.v_unit_price, e.d_do ORDER BY e.d_do DESC ";	
				*/
				
				/*
					(a.v_unit_price / a.n_quantity) AS unitprice, 
					sum(a.v_unit_price) AS amount 
				*/
			$strq	= "
				SELECT  b.i_faktur_code, c.i_do_code AS idocode, c.d_do AS ddo, a.i_product AS imotif, 
					a.e_product_name AS motifname, 
					sum(a.n_quantity) AS qty, 
					a.v_unit_price AS unitprice,
					sum((a.n_quantity * a.v_unit_price)) AS amount
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				INNER JOIN tr_customer d ON c.i_customer=d.i_customer 
				 
				".$nfaktur." ".$ddo." ".$fbatal." ".$pelanggan1."
				
				GROUP BY b.i_faktur_code, c.i_do_code, c.d_do, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ORDER BY c.i_do_code ASC, a.i_product ASC ";
			
		return $db2->query($strq);
	}

	function clistpenjualanperdototal($nofaktur,$ddo_first,$ddo_last, $pelanggan) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && ($ddo_first!='0' && $ddo_last!='0')) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$pelanggan1	= "";	
			$fbatal		= " AND b.f_faktur_cancel='f' ";		
		}else if(!empty($nofaktur) && ($ddo_first=='0' || $ddo_last=='0') ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";
			$pelanggan1	= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else if(!empty($pelanggan) && ($ddo_first=='0' || $ddo_last=='0')) {
			$pelanggan1	= "AND d.i_customer_code = '$pelanggan'";
			$ddo		= "";
			$nfaktur	= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}else if(!empty($pelanggan) && ($ddo_first!='0' && $ddo_last!='0')){
			$pelanggan1	= "AND d.i_customer_code = '$pelanggan'";
			$nfaktur	= "";
			$ddo		= "WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last')";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else if($ddo_first!='0' && $ddo_last!='0'){
			$nfaktur	= "";
			$pelanggan1	= "";
			$ddo		= " WHERE (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		}else{
			$pelanggan1	= "";
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
		
		$strq	= "SELECT  sum(a.n_quantity) AS qty, sum((a.n_quantity * a.v_unit_price)) AS amount
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				INNER JOIN tr_customer d ON c.i_customer=d.i_customer 
				 
				".$nfaktur." ".$ddo." ".$fbatal." ".$pelanggan1." ";
			
		return $db2->query($strq);
	}
		
	function lbarangjadi() {
	$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}	

	function lbarangjadiperpages($limit,$offset) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
									
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flbarangjadi($key) {
	$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.i_faktur_code='$key' AND b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC ");
	}
	
	function explistpenjualanperdo($nofaktur,$tfirst,$tlast) {
		$db2=$this->load->database('db_external', TRUE);
		if( (!empty($nofaktur) && $nofaktur!='0') && ( (!empty($tfirst) && !empty($tlast) ) && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= " AND (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$fbatal		= " AND c.f_faktur_cancel='f' ";
			
		} else if( (!empty($nofaktur) && $nofaktur!='0') && ($tfirst=='0' || $tlast=='0') ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= "";
			$fbatal		= " AND c.f_faktur_cancel='f' ";		
		} else if( $nofaktur=='0' && ( (!empty($tfirst) && !empty($tlast))  && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$fbatal		= " AND c.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$dfaktur	= "";
			$fbatal		= " WHERE c.f_faktur_cancel='f' ";
		}
		
			$strq	= " SELECT  c.i_faktur AS ifaktur, c.i_faktur_code AS ifakturcode,	
					a.e_customer_name AS customername,
					a.e_customer_npwp AS npwp,
					c.d_faktur AS dfaktur,
					c.d_pajak AS dpajak,
					c.d_due_date AS dduedate,
					c.n_discount AS discountpersen,
					c.v_discount AS nilaidiscon,
					c.v_total_faktur AS totalfaktur,
					(c.v_total_faktur-c.v_discount) AS dpp,
					c.v_total_fppn AS totalsetelahppndiscount
				
				
				FROM tr_customer a
							
					INNER JOIN tr_branch b ON b.i_customer=a.i_customer
					INNER JOIN tm_faktur_do_t c ON c.e_branch_name=b.e_initial
					INNER JOIN tm_faktur_do_item_t d ON d.i_faktur=c.i_faktur
					
					".$nfaktur." ".$dfaktur." ".$fbatal."
					
					GROUP BY a.e_customer_name, 
						a.e_customer_npwp,
						c.i_faktur, 
						c.i_faktur_code, 
						c.d_faktur,
						c.d_pajak,
						c.d_due_date, 
						c.v_total_faktur, 
						c.n_discount,
						c.v_discount,
						c.v_total_fppn ORDER BY c.i_faktur_code ASC, c.d_faktur ASC ";
		
		return $db2->query($strq);
		
		/*
		$query	= $db2->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
		*/		
	}
function explistpenjualanperdonofakpajak($nofaktur,$tfirst,$tlast,$pelanggan) {
		$db2=$this->load->database('db_external', TRUE);
		//var_dump($pelanggan);

		if( !empty($pelanggan) && (!empty($nofaktur) && $nofaktur!='0') && ( (!empty($tfirst) && !empty($tlast) ) && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= " AND (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$fbatal		= " AND c.f_faktur_cancel='f' ";
			$qpel		= " AND a.i_customer_code = '$pelanggan'";

		} else if( !empty($pelanggan) && (!empty($nofaktur) && $nofaktur!='0') && ($tfirst=='0' || $tlast=='0') ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= "";
			$fbatal		= " AND c.f_faktur_cancel='f' ";	
			$qpel		= " AND a.i_customer_code = '$pelanggan'";	
		} else if ( !empty($pelanggan) && $nofaktur=='0' && ($tfirst=='0' || $tlast=='0') ) {
			$nfaktur	= "";
			$dfaktur	= "";
			$fbatal		= " WHERE c.f_faktur_cancel='f' ";	
			$qpel		= " AND a.i_customer_code = '$pelanggan'";
		}else if ( !empty($pelanggan) && ( (!empty($tfirst) && !empty($tlast))  && ($tfirst!='0' && $tlast!='0') ) ){
			$nfaktur	= "";
			$dfaktur	= "	WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast')";
			$fbatal		= " AND c.f_faktur_cancel='f' ";	
			$qpel		= " AND a.i_customer_code = '$pelanggan'";
		}else if( $nofaktur=='0' && ( (!empty($tfirst) && !empty($tlast))  && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$fbatal		= " AND c.f_faktur_cancel='f' ";
			$qpel		= "";
		} elseif ($nofaktur=='0' && ( (!empty($tfirst) && !empty($tlast))  && ($tfirst!='0' && $tlast!='0') && (!empty($pelanggan) && $pelanggan!='0') )) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			$fbatal		= " AND c.f_faktur_cancel='f' ";
			$pelanggan1 = " AND a.i_customer_code = '$pelanggan'";
		} else {
			$nfaktur	= "";
			$dfaktur	= "";
			$fbatal		= " WHERE c.f_faktur_cancel='f' ";
			$qpel		= "";
		}
		
			$strq	= " SELECT  c.i_faktur AS ifaktur, c.i_faktur_code AS ifakturcode,	
					a.e_customer_name AS customername,
					a.e_customer_npwp AS npwp,
					e.nomor_pajak AS nomorpajak,
					c.d_faktur AS dfaktur,
					c.d_pajak AS dpajak,
					c.d_due_date AS dduedate,
					c.n_discount AS discountpersen,
					c.v_discount AS nilaidiscon,
					c.v_total_faktur AS totalfaktur,
					(c.v_total_faktur-c.v_discount) AS dpp,
					c.v_total_fppn AS totalsetelahppndiscount
				
				
				FROM tr_customer a
							
					INNER JOIN tr_branch b ON b.i_customer=a.i_customer
					INNER JOIN tm_faktur_do_t c ON c.e_branch_name=b.e_initial
					INNER JOIN tm_faktur_do_item_t d ON d.i_faktur=c.i_faktur
					INNER JOIN tm_nomor_pajak_faktur e ON c.i_faktur_code = e.i_faktur_code
					".$nfaktur." ".$dfaktur." ".$fbatal." ".$qpel."
					
					GROUP BY a.e_customer_name, 
						a.e_customer_npwp,
						e.nomor_pajak,
						c.i_faktur, 
						c.i_faktur_code, 
						c.d_faktur,
						c.d_pajak,
						c.d_due_date, 
						c.v_total_faktur, 
						c.n_discount,
						c.v_discount,
						c.v_total_fppn ORDER BY c.i_faktur_code ASC, c.d_faktur ASC ";
		
		return $db2->query($strq);
		
		/*
		$query	= $db2->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
		*/		
	}
	function pajak($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT b.i_faktur_pajak AS ifakturpajak, b.d_pajak
			
			FROM tm_faktur_do_t b
			
			INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			INNER JOIN tm_do e ON e.i_do=a.i_do 
			
			WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f'
			
			GROUP BY b.i_faktur_pajak, b.d_pajak ");
	}	
}
?>
