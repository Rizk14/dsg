<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function clistfpenjbrgjadiperdo($nofaktur,$ddo_first,$ddo_last) {
		$db2 = $this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
			
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
		
		if( strlen($nfaktur)!=0 || strlen($ddo)!=0 ) {
			/* Disabled 19-03-2011
			$query	= $db2->query( "
			
				SELECT 	e.d_do AS ddo,
						e.i_do_code AS idocode,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname,
						f.n_deliver AS qty,
						f.v_do_gross AS unitprice,
						(f.n_deliver * f.v_do_gross) AS amount
					
				FROM tm_faktur_do_item a
					
				RIGHT JOIN tm_faktur_do b ON cast(b.i_faktur_code AS integer)=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo."
				
				GROUP BY e.i_do_code, c.i_product_motif, c.e_product_motifname, f.n_deliver, f.v_do_gross, e.d_do
				
				" );
			*/

			$query	= $db2->query( "
			
				SELECT 	e.d_do AS ddo,
						e.i_do_code AS idocode,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname,
						sum(a.n_quantity)  AS qty,
						a.v_unit_price AS unitprice,
						sum((a.n_quantity * a.v_unit_price)) AS amount
					
				FROM tm_faktur_do_item_t a
					
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY e.i_do_code, c.i_product_motif, c.e_product_motifname, a.n_quantity, a.v_unit_price, e.d_do
				ORDER BY c.i_product_motif ASC
				" );
								
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}
		}
	}


	function clistfpenjperdo($nofaktur,$ddo_first,$ddo_last) {
		$db2 = $this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
			
		/*
		Ini dipakai utk menmpikan info : Nomor Faktur, Tanggal Faktur, Tgl Jatuh Tempo
		*/
		return $db2->query( "
		
				SELECT 	count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname
					
				FROM tm_faktur_do_item_t a
					
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do 
				
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name " );
			
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	
	function clistfpenjperdo_detail_opdo($nofaktur,$ddo_first,$ddo_last) {
		$db2 = $this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='".trim($nofaktur)."'";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
			
		/*
		Ini dipakai utk menmpikan info : Nomor OP, Nomor DO
		*/
		/*
		$strqry	= "
			SELECT 	count(cast(e.i_do_code AS integer)) AS jmldo,
					count(cast(f.i_op AS integer)) AS jmlop,
					e.i_do_code AS ido,
					f.i_op AS iopcode
			
			FROM tm_faktur_do_item a
				
			RIGHT JOIN tm_faktur_do b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			INNER JOIN tm_do e ON e.i_do=a.i_do 
			
			".$nfaktur." ".$ddo."
			
			GROUP BY e.i_do_code, f.i_op ";
		*/
		
		/* 26072011
		$strqry	= "
			SELECT  e.i_do_code AS ido, 
				f.i_op AS iopcode 
			
			FROM tm_faktur_do_item_t a 
			
			RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do 
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product 
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product 
			INNER JOIN tm_do e ON e.i_do=a.i_do 
			
			".$nfaktur." ".$ddo."
			
			GROUP BY e.i_do_code, f.i_op ";
		*/

		$strqry	= " SELECT  e.i_do_code AS idocode, g.i_op_code AS iopcode
				
				FROM tm_faktur_do_item_t a
				
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tm_do e ON e.i_do=a.i_do
				INNER JOIN tm_op g ON g.i_op=f.i_op
				
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY e.i_do_code, g.i_op_code ";
						
		$query	= $db2->query($strqry);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	
	function clistfpenjperdo_jml($nofaktur,$ddo_first,$ddo_last) {
	$db2 = $this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
			
		/*
		Untuk Perhitungan : Jumlah, Diskon, DPP, PPN, Nilai Faktur 
		*/	
		return $db2->query( "		
			SELECT 	b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					sum((a.n_quantity * a.v_unit_price)) AS total
			
			FROM tm_faktur_do_item_t a
				
			RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			INNER JOIN tm_do e ON e.i_do=a.i_do
				
			".$nfaktur." ".$ddo." ".$fbatal."
			
			GROUP BY b.n_discount, b.v_discount " );
	}
	
	function getcabang($nofaktur){
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT c.e_branch_name AS cabangname, c.e_branch_address AS address
							
			FROM tm_faktur_do_t a
				
			INNER JOIN tm_faktur_do_item_t b ON a.i_faktur=b.i_faktur
			INNER JOIN tr_branch c ON c.e_initial=a.e_branch_name
			INNER JOIN tr_customer d ON d.i_customer=c.i_customer
			
			WHERE a.i_faktur_code='$nofaktur' AND a.f_faktur_cancel='f'
			
			GROUP BY c.e_branch_name, c.e_branch_address
		" );
	}
	
	function getinitial() {
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial_code DESC LIMIT 1");
	}
	
	function lbarangjadiperpages($limit,$offset) {
		$db2 = $this->load->database('db_external', TRUE);
		/*
			SELECT 	count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
					a.i_faktur_code AS ifakturcode
							
			FROM tm_faktur_do_item b
										
			INNER JOIN tm_faktur_do a ON a.i_faktur=b.i_faktur
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product) 
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			
			ORDER BY a.i_faktur_code DESC
			
			GROUP BY a.i_faktur_code
			*/
		
		$query	= $db2->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
									
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				INNER JOIN tm_do_item d ON d.i_do=c.i_do
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product
				
				WHERE b.f_printed='f' AND b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.f_printed='f' AND b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}

	function flbarangjadi($key) {
		$db2 = $this->load->database('db_external', TRUE);
		return $db2->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.f_printed='f' AND b.i_faktur_code='$key' AND b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}		
}
?>
