<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

function lcustomer() {
	$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_customer ORDER BY e_customer_name ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
		
	function jmlop($iopcode) {
		$db2=$this->load->database('db_external', TRUE);
		return	$db2->query(" SELECT * FROM tm_op_item a INNER JOIN tm_op b ON a.i_op=b.i_op 
				WHERE i_op_code='$iopcode' AND b.f_op_cancel='f' ");
	}
	
	function cekstatusdo($iopcode) {
		$db2=$this->load->database('db_external', TRUE);
		/*
		return $db2->query("
			SELECT  a.d_op,
					a.i_op_code
			
			FROM tm_op a
			
			INNER JOIN tm_op_item b ON a.i_op=b.i_op
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=cast(trim(b.i_product) AS character varying)
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product 
			INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch 
			INNER JOIN tm_do_item f ON cast(f.i_op AS character varying)=trim(a.i_op_code) 
			INNER JOIN tm_do g ON g.i_do=f.i_do
			
			WHERE a.i_op_code='$iopcode'
			GROUP BY a.i_op_code, a.d_op
			
			ORDER BY a.i_op_code DESC	
		");
		*/	
		
		return $db2->query(" SELECT * FROM tm_op a WHERE a.i_op_code='$iopcode' AND a.f_do_created='t' AND a.f_op_cancel='f' ");
		
	}
	
	function cekstatusdo2($iop) {
		$db2=$this->load->database('db_external', TRUE);
		// return $db2->query(" SELECT * FROM tm_op_item WHERE i_op='$iop' AND (f_do_created='t' OR n_count=(SELECT n_residual FROM tm_op_item WHERE i_op='$iop')) ");
		return $db2->query(" SELECT n_count, i_product FROM tm_op_item WHERE i_op='$iop' ");
	}

	function cekstatusdo3($iopcode) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_op a WHERE a.i_op_code='$iopcode' AND a.f_op_cancel='f' ");
	}
		
	function clistopbrg_f($d_op_first,$d_op_last,$kdop,$icustomer) {
	$db2=$this->load->database('db_external', TRUE);
	/*
	clistopbrg($n_d_op_first,$n_d_op_last,$kdop)
	*/
	
		/* Disabled 13052011
		if( (!empty($d_op_first) && !empty($d_op_last)) && (!empty($kdop) || strlen($kdop)!=0) ) {
			$dop	= " WHERE ( a.d_op BETWEEN '$d_op_first' AND '$d_op_first' )";
			$op		= " AND a.i_op_code='$kdop' ";
			$stat	= " AND a.f_op_cancel=false ";
		} else if( (!empty($d_op_first) && !empty($d_op_last)) && (empty($kdop) || strlen($kdop)==0) ) {
			$dop	= " WHERE ( a.d_op BETWEEN '$d_op_first' AND '$d_op_first' )";
			$op		= " ";
			$stat	= " AND a.f_op_cancel=false ";
		} else if((empty($d_op_first) || empty($d_op_last)) && !empty($kdop) ) {
			$dop	= "";
			$op		= " WHERE a.i_op_code='$kdop' ";
			$stat	= " AND a.f_op_cancel=false ";
		} else {
			$dop	= "";
			$op		= "";
			$stat	= " WHERE a.f_op_cancel=false ";
		}
		*/
		
		/* Disabled 06012011		
		$query	= $db2->query( "
				
				SELECT  a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					TEXTCAT(cast(trim(b.i_product) AS character varying),'00') AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_count AS qty,
					a.d_delivery_limit AS deliver,
					a.i_sop	AS isop
					
				FROM tm_op_item b
				
				RIGHT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=TEXTCAT(cast(trim(b.i_product) AS character varying),'00')
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product ".$dop." ".$op  );
		*/
		
		if(!empty($kdop)) {
			$op		= " WHERE a.i_op_code='$kdop' ";
			$stat	= " AND a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		}else{
			$op		= "";
			$stat	= " WHERE a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		}		
		
		$query	= $db2->query(" SELECT a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					e.e_branch_name AS ebranchname,
					cast(trim(b.i_product) AS character varying) AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_count AS qty,
					b.n_residual AS sisaorder,
					a.d_delivery_limit AS deliver,
					a.i_sop	AS isop,
					a.f_op_cancel
				
				FROM tm_op_item b
				
				RIGHT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON TRIM(c.i_product_motif)=cast(TRIM(b.i_product) AS character varying)
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product 
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$op." ".$stat." ".$i_customer." 
				
				ORDER BY a.i_op_code DESC ");
				
		if($query->num_rows()>0) {
			return $query->result();
		}
		
	}
	
	function clistopbrg_t($d_op_first,$d_op_last,$kdop,$icustomer) {
	$db2=$this->load->database('db_external', TRUE);
		if( (!empty($d_op_first) && !empty($d_op_last))) {
			$dop	= " WHERE ( a.d_op BETWEEN '$d_op_first' AND '$d_op_last' )";
			$stat	= " AND a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		} else if( (!empty($d_op_first) && !empty($d_op_last)) ) {
			$dop	= " WHERE ( a.d_op BETWEEN '$d_op_first' AND '$d_op_last' )";
			$stat	= " AND a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		} else {
			$dop	= "";
			$stat	= " WHERE a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		}
		
		return $db2->query("
			SELECT  a.d_op AS dop,
				a.i_op_code AS iopcode,
				a.i_branch AS ibranch,
				e.e_branch_name AS ebranchname,
				a.i_sop	AS isop,
				a.f_op_cancel,
				a.f_do_created,
				a.d_entry
			
			FROM tm_op_item b
							
				RIGHT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch
				
			".$dop." ".$stat." ".$i_customer."
			GROUP BY a.d_op, a.i_op_code, a.i_branch, e.e_branch_name, a.i_sop, a.f_op_cancel, a.f_do_created, a.d_entry
			");			
	}
	
	function viewperpages_f($limit,$offset,$d_op_first,$d_op_last,$kdop) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($kdop)){
			$op		= " WHERE a.i_op_code='$kdop' ";
			$stat	= " AND a.f_op_cancel='f' ";
		} else {
			$op		= "";
			$stat	= " WHERE a.f_op_cancel='f' ";
		}		
		$query	= $db2->query( "			
				SELECT  a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					e.e_branch_name AS ebranchname,
					cast(trim(b.i_product) AS character varying) AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_count AS qty,
					b.n_residual AS sisaorder,
					a.d_delivery_limit AS deliver,
					a.i_sop	AS isop
					
				FROM tm_op_item b
				
				RIGHT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product 
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$op." ".$stat." ORDER BY a.i_op_code DESC LIMIT $limit OFFSET $offset " );
		
		if($query->num_rows() > 0 )	{
			return $result	= $query->result();
		}	
	}
	
	function viewperpages_t($limit,$offset,$d_op_first,$d_op_last,$kdop,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		if((!empty($d_op_first) && !empty($d_op_last))) {
			$dop	= " WHERE (a.d_op BETWEEN '$d_op_first' AND '$d_op_last')";
			$stat	= " AND a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		} else if((!empty($d_op_first) && !empty($d_op_last))) {
			$dop	= " WHERE (a.d_op BETWEEN '$d_op_first' AND '$d_op_last')";
			$stat	= " AND a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		} else {
			$dop	= "";
			$stat	= " WHERE a.f_op_cancel='f' ";
			$i_customer	= " AND a.i_customer='$icustomer' ";
		}
		
		$query	= $db2->query("
			SELECT  a.d_op AS dop,
				a.i_op_code AS iopcode,
				a.i_branch AS ibranch,
				e.e_branch_name AS ebranchname,
				a.i_sop	AS isop,
				a.f_op_cancel,
				a.f_do_created,
				a.d_entry
			
			FROM tm_op_item b
							
				RIGHT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$dop." ".$stat." ".$i_customer." 
				
				GROUP BY a.d_op, a.i_op_code, a.i_branch, e.e_branch_name, a.i_sop, a.f_op_cancel, a.f_do_created, a.d_entry LIMIT ".$limit." OFFSET ".$offset." ");
				
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
		
	/* AND e.f_stop_produksi=FALSE  */
	function lopperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		/* Disabled 05012011
		$query	= $db2->query("

				SELECT	a.i_op_code AS op,
						TEXTCAT(cast(trim(b.i_product) AS character varying),'00') AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						d.n_quantity AS qtyproduk,
						(b.n_count * e.v_unitprice) AS harga
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=TEXTCAT(cast(trim(b.i_product) AS character varying),'00') 
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=TEXTCAT(cast(trim(b.i_product) AS character varying),'00')
				
				WHERE d.n_active='1' AND e.f_stop_produksi=FALSE
				
				ORDER BY a.d_op DESC LIMIT ".$limit." OFFSET ".$offset);
		*/
		$thn_skrg = date("Y");
		$query	= $db2->query("

				SELECT	a.i_op_code AS op,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						b.n_residual AS sisaorder,
						d.n_quantity AS qtyproduk,
						(b.n_count * e.v_unitprice) AS harga
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				
				WHERE d.n_active='1' AND a.f_op_cancel='f' AND a.d_op >='".$thn_skrg."-01-01'
				
				ORDER BY a.d_op DESC, a.i_op_code DESC LIMIT ".$limit." OFFSET ".$offset);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	/*  AND e.f_stop_produksi=FALSE */
	
	function lop() {
		$db2=$this->load->database('db_external', TRUE);
		/* Disabled 05012011
		return $db2->query("

				SELECT	a.i_op_code AS op,
						TEXTCAT(cast(trim(b.i_product) AS character varying),'00') AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						d.n_quantity AS qtyproduk,
						(b.n_count * e.v_unitprice) AS harga
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=TEXTCAT(cast(trim(b.i_product) AS character varying),'00') 
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=TEXTCAT(cast(trim(b.i_product) AS character varying),'00')
				
				WHERE d.n_active='1' AND e.f_stop_produksi=FALSE
				
				ORDER BY a.d_op DESC ");
		*/		
		$thn_skrg = date("Y");
		return $db2->query("

				SELECT	a.i_op_code AS op,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						b.n_residual AS sisaorder,
						d.n_quantity AS qtyproduk,
						(b.n_count * e.v_unitprice) AS harga
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				
				WHERE d.n_active='1' AND a.f_op_cancel='f' AND a.d_op >='".$thn_skrg."-01-01'
				
				ORDER BY a.d_op DESC, a.i_op_code DESC ");		
	}
	
	
	/* AND e.f_stop_produksi=FALSE */
	function flop($key) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($key)) {
			
			return $db2->query("
	
					SELECT	a.i_op_code AS op,
							cast(trim(b.i_product) AS character varying) AS iproduct,
							d.e_product_motifname AS productname,
							b.n_count AS qty,
							b.n_residual AS sisaorder,
							d.n_quantity AS qtyproduk,
							(b.n_count * e.v_unitprice) AS harga
					
					FROM tm_op a
					
					INNER JOIN tm_op_item b ON a.i_op=b.i_op
					INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
					INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
					INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
					
					WHERE d.n_active='1' AND (a.i_op_code LIKE '$key%' OR b.i_product LIKE '$key%' OR d.e_product_motifname LIKE '$key%') AND a.f_op_cancel=FALSE
					
					ORDER BY a.d_op DESC, a.i_op_code DESC ");				
		}
	}
	
	function mbatal($iopcode,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($iopcode)) {
			
			$qcariiopcode	= $db2->query(" SELECT * FROM tm_op WHERE i_op_code='$iopcode' AND i_customer='$icustomer' AND f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
			
			if($qcariiopcode->num_rows()>0) {
				
				$rcariiopcode = $qcariiopcode->row();
				
				$tbl_op	= array(
					'f_op_cancel'=>'TRUE'
				);
				
				$qsop = $db2->query(" SELECT i_sop, i_op FROM tm_sop WHERE i_op='$rcariiopcode->i_op' AND i_customer='$icustomer' AND f_op_cancel='f' ");
				if($qsop->num_rows()>0) {
					$rsop = $qsop->row();
					$db2->query(" UPDATE tm_sop SET f_op_cancel='t' WHERE i_sop='$rsop->i_sop' AND i_op='$rsop->i_op' AND f_op_cancel='f' ");
				}
				
				$db2->update('tm_op',$tbl_op,array('i_op_code'=>$iopcode));

				print "<script>alert(\"Nomor OP : '\"+$iopcode+\"' telah dibatalkan, terimakasih.\");show(\"listop/cform\",\"#content\");</script>";
			}else{
				print "<script>alert(\"Maaf, Order Pembelian (OP) tdk dpt dibatalkan.\");show(\"listop/cform\",\"#content\");</script>";
			}
			
		} else {
			print "<script>alert(\"Maaf, Order Pembelian (OP) tdk dpt dibatalkan.\");show(\"listop/cform\",\"#content\");</script>";		
		}
	}

	function mbatal_old($iopcode,$icustomer) {
		
		if(!empty($iopcode)) {
			
			$qcariiopcode	= $db2->query(" SELECT * FROM tm_op WHERE i_op_code='$iopcode' AND i_customer='$icustomer' AND f_op_cancel='f' ORDER BY i_op DESC LIMIT 1 ");
			if($qcariiopcode->num_rows()>0) {
				$tbl_op	= array(
					'f_op_cancel'=>'TRUE'
				);
				$db2->update('tm_op',$tbl_op,array('i_op_code'=>$iopcode));
				/* redirect('listop/cform'); */
				print "<script>alert(\"Nomor OP : '\"+$iopcode+\"' telah dibatalkan, terimakasih.\");show(\"listop/cform\",\"#content\");</script>";
			} else {
				print "<script>alert(\"Maaf, Order Pembelian (OP) tdk dpt dibatalkan.\");show(\"listop/cform\",\"#content\");</script>";
			}
			
		} else {
			print "<script>alert(\"Maaf, Order Pembelian (OP) tdk dpt dibatalkan.\");show(\"listop/cform\",\"#content\");</script>";		
		}
	}
		
	function getopheader($iopcode,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tm_op WHERE i_op_code='$iopcode' AND i_customer='$icustomer' AND f_op_cancel='f' " );
	}
	
	function lopitem($iop) {
	$db2=$this->load->database('db_external', TRUE);
		/* Disabled 07-02-2011
		$qstr	= "
				SELECT	a.i_op_code AS op,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						d.n_quantity AS qtyproduk,
						(b.n_count * e.v_unitprice) AS harga
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				
				WHERE d.n_active='1' AND e.f_stop_produksi=FALSE AND a.f_op_cancel=FALSE AND b.i_op='$iop'
				
				ORDER BY a.d_op DESC, a.i_op_code DESC ";
		*/
		
		/* 04082011
		$qstr	= "
				SELECT	a.i_op_code AS op,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						d.n_quantity AS qtyproduk,
						(b.n_count * e.v_unitprice) AS harga
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				
				WHERE d.n_active='1' AND a.f_op_cancel='f' AND b.i_op='$iop'
				
				ORDER BY a.d_op DESC, a.i_op_code DESC ";			
		*/
		
		$qstr	= " SELECT a.i_op_code AS op, 
				cast(trim(b.i_product) AS character varying) AS iproduct, 
				d.e_product_motifname AS productname, 
				b.n_count AS qty, 
				f.n_quantity_akhir AS qty_akhir,
				(b.n_count * e.v_unitprice) AS harga,
				b.e_note 
				
				FROM tm_op a 
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying) 
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying) 
				INNER JOIN tm_stokopname_item f ON f.i_product=d.i_product_motif
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
								
				WHERE d.n_active='1' AND a.f_op_cancel='f' AND b.i_op='$iop' AND g.i_status_so='0' ORDER BY a.d_op DESC, a.i_op_code DESC ";
		
		$query	= $db2->query($qstr);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function getpelanggan($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_customer',$icustomer);
		$db2->order_by('i_op');
		
		return $db2->get('tm_op');
	}
	
	function lcabang($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.i_branch AS codebranch, 
					a.i_branch_code AS ibranchcode,
				    a.i_customer AS codecustomer, 
					a.e_branch_name AS ebranchname,
				    a.e_branch_name AS branch 
					
					FROM tr_branch a 
					
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$filter." ".$order;
					
		$query	= $db2->query($strq);
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function getcabang($ibranch) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_branch',$ibranch);
		$db2->order_by('i_op');
		
		return $db2->get('tm_op');
	}

	function cari_op($nop,$noplama,$icustomer) {
		
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_op WHERE i_op_code=trim('$nop') AND i_op_code!=trim('$noplama') AND i_customer='$icustomer' AND f_op_cancel='f' ");
	}

	function cari_sop($nsop) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_op WHERE i_sop=trim('$nsop') ");
	}	
	
	function mupdate($i_op,$i_op_code_hidden,$i_op_hidden,$i_customer_hidden,$i_branch,$nw_d_op,$nw_d_delivery_limit,$i_sop_hidden,$f_op_dropforcast,$v_count,$i_product,$e_product_name,$e_note, $jenis_op, $iteration) {
		$db2=$this->load->database('db_external', TRUE);
		$i_op_item	= array();
		$tm_op_item	= array();
		$tm_sop_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;	
		
		$qsop = $db2->query(" SELECT i_sop, i_op FROM tm_sop WHERE i_op='$i_op_hidden' ");
		if($qsop->num_rows()>0) {
			$rsop = $qsop->row();
			
			$isop_old = $rsop->i_sop;
			
			$db2->query(" DELETE FROM tm_sop_item WHERE i_sop='$rsop->i_sop' ");
			$db2->query(" DELETE FROM tm_sop WHERE i_sop='$rsop->i_sop' AND i_op='$rsop->i_op' ");
		}else{
			$isop_old = '';
		}
		
		if($isop_old=='') {
			$seq_tm_sop	= $db2->query(" SELECT i_sop FROM tm_sop ORDER BY i_sop DESC LIMIT 1 ");
			
			if($seq_tm_sop->num_rows()>0) {
				$seqrow2= $seq_tm_sop->row();
				$isop	= $seqrow2->i_sop+1;
			}else{
				$isop	= 1;
			}
		}else{
			$isop	= $isop_old;
		}
		
		if($db2->delete('tm_op_item',array('i_op'=>$i_op_hidden))) {
			
			$db2->delete('tm_op',array('i_op'=>$i_op_hidden,'f_op_cancel'=>'f'));
			
			$tm_op	= array(
				 'i_op'=>$i_op_hidden,
				 'i_op_code'=>$i_op,
				 'i_customer'=>$i_customer_hidden,
				 'i_branch'=>$i_branch,
				 'd_op'=>$nw_d_op,
				 'd_delivery_limit'=>$nw_d_delivery_limit,
				 'i_sop'=>$i_sop_hidden,
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry,
				 'f_op_dropforcast'=>$f_op_dropforcast,
				 // 13-04-2015
				 'jenis_op'=>$jenis_op);

			$tm_sop	= array(
				 'i_sop'=>$isop,
				 'i_sop_code'=>$i_sop_hidden,
				 'i_customer'=>$i_customer_hidden,
				 'i_branch'=>$i_branch,
				 'd_sop'=>$nw_d_op,
				 'i_op'=>$i_op_hidden,
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry
			);
				 			
			if($db2->insert('tm_op',$tm_op)) {
				
				$db2->insert('tm_sop',$tm_sop);
				
				for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
					
					$seq_tm_op_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_op_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
					
					if($seq_tm_op_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_op_item->row();
						$i_op_item[$jumlah]	= $seqrow->i_op_item+1;
					}else{
						$i_op_item[$jumlah]	= 1;
					}

					$seq_tm_sop_item	= $db2->query(" SELECT i_sop_item FROM tm_sop_item ORDER BY i_sop_item DESC LIMIT 1 ");
					if($seq_tm_sop_item->num_rows() > 0 ) {
						$seqrow2	= $seq_tm_sop_item->row();
						$i_sop_item	= $seqrow2->i_sop_item+1;
					}else{
						$i_sop_item	= 1;
					}
						
					$tm_op_item[$jumlah]	= array(
						 'i_op_item'=>$i_op_item[$jumlah],
						 'i_op'=>$i_op_hidden,
						 'i_product'=>$i_product[$jumlah],
						 'n_count'=>$v_count[$jumlah],
						 'e_product_name'=>$e_product_name[$jumlah],
						 'f_delivered'=>'FALSE',
						 'n_residual'=>$v_count[$jumlah],
						 'e_note'=>$e_note[$jumlah],
						 'd_entry'=>$dentry );

					$tm_sop_item[$jumlah]	= array(
									 'i_sop_item'=>$i_sop_item,
									 'i_sop'=>$isop,
									 'i_product'=>$i_product[$jumlah],
									 'e_product_name'=>$e_product_name[$jumlah],
									 'd_entry'=>$dentry);
									 
					$db2->insert('tm_op_item',$tm_op_item[$jumlah]);
					$db2->insert('tm_sop_item',$tm_sop_item[$jumlah]);
					
				}
				$ok	= 1;
			}else{
			   $ok	= 0;
			}
		}else{
			$ok	= 0;
		}

		if($ok=='1') {
			print "<script>alert(\"Nomor OP : '\"+$i_op_code_hidden+\"' telah diupdate, terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
			
		}else{
			print "<script>alert(\"Maaf, Order Pembelian (OP) gagal diupdate. Terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
			
		}
		
	}
	
	function mupdate_old($i_op,$i_op_code_hidden,$i_op_hidden,$i_customer_hidden,$i_branch,$nw_d_op,$nw_d_delivery_limit,$i_sop_hidden,$f_op_dropforcast,$v_count,$i_product,$e_product_name,$e_note,$iteration) {
		$db2=$this->load->database('db_external', TRUE);
		$i_op_item	= array();
		$tm_op_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;	
		
		/* 19122011
		$qhps = $db2->query(" SELECT i_op FROM tm_op WHERE i_op='$i_op_hidden' AND f_op_cancel='f' ");
		$rhps = $qhps->row();
		*/
		
		/* Disabled 05102011
		if($db2->delete('tm_op_item',array('i_op'=>$i_op_hidden))) {
			$db2->delete('tm_op',array('i_op'=>$i_op_hidden,'f_op_cancel'=>'f'));
		*/
		
		//if($db2->delete('tm_op_item',array('i_op'=>$rhps->i_op))) {
		if($db2->delete('tm_op_item',array('i_op'=>$i_op_hidden))) {
				
			//$db2->delete('tm_op',array('i_op'=>$rhps->i_op,'f_op_cancel'=>'f'));
			$db2->delete('tm_op',array('i_op'=>$i_op_hidden,'f_op_cancel'=>'f'));
			
			$tm_op	= 
				array(
				 'i_op'=>$i_op_hidden,
				 'i_op_code'=>$i_op,
				 'i_customer'=>$i_customer_hidden,
				 'i_branch'=>$i_branch,
				 'd_op'=>$nw_d_op,
				 'd_delivery_limit'=>$nw_d_delivery_limit,
				 'i_sop'=>$i_sop_hidden,
				 'd_entry'=>$dentry,
				 'd_update'=>$dentry,
				 'f_op_dropforcast'=>$f_op_dropforcast);
			
			if($db2->insert('tm_op',$tm_op)) {

				for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
					$seq_tm_op_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_op_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
					if($seq_tm_op_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_op_item->row();
						$i_op_item[$jumlah]	= $seqrow->i_op_item+1;
					} else {
						$i_op_item[$jumlah]	= 1;
					}
	
					$tm_op_item[$jumlah]	=
						array(
						 'i_op_item'=>$i_op_item[$jumlah],
						 'i_op'=>$i_op_hidden,
						 'i_product'=>$i_product[$jumlah],
						 'n_count'=>$v_count[$jumlah],
						 'e_product_name'=>$e_product_name[$jumlah],
						 'f_delivered'=>'FALSE',
						 'n_residual'=>$v_count[$jumlah],
						 'e_note'=>$e_note[$jumlah],
						 'd_entry'=>$dentry );

					$db2->insert('tm_op_item',$tm_op_item[$jumlah]);				

				}

				$ok	= 1;
			} else {
			   $ok	= 0;
			}

		} else {
			$ok	= 0;
		}

		if($ok=='1') {
			print "<script>alert(\"Nomor OP : '\"+$i_op_code_hidden+\"' telah diupdate, terimakasih.\");show(\"listop/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Maaf, Order Pembelian (OP) gagal diupdate. Terimakasih.\");show(\"listop/cform\",\"#content\");</script>";
		}
	}	
}
?>
