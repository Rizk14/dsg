<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();
  }
  
  function getArea(){
    $this->db->select('*');
    $this->db->from('tm_area');
    $this->db->order_by('kode_area','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function inserttransheader(	$inota,$iarea,$eremark,$fclose,$dkn,$i_kk )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi,i_jurnal_transharian)
						  	  values
					  	 ('$inota','$iarea','$dentry','$eremark','$fclose',to_date('$dkn','DD-MM-YYYY'),to_date('$dkn','DD-MM-YYYY'),'$i_kk')");
	}
function inserttransheaderkbout(	$inota,$iarea,$eremark,$fclose,$dkn,$i_kk )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi,i_jurnal_transharian)
						  	  values
					  	 ('$inota','$iarea','$dentry','$eremark','$fclose',to_date('$dkn','YYYY-MM-DD'),to_date('$dkn','YYYY-MM-DD'),'$i_kk')");
	}
  function inserttransitemdebet($accdebet,$ikn,$namadebet,$fdebet,$fposting,$iarea,$eremark,$vjumlah,$dkn,$i_kk)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry, i_area,i_jurnal_transharian_item)
						  	  values
					  	 ('$accdebet','$ikn','$namadebet','$fdebet','$fposting','$vjumlah',to_date('$dkn','DD-MM-YYYY'),to_date('$dkn','DD-MM-YYYY'),'$dentry','$iarea','$i_kk')");
	}
	function inserttransitemkredit($acckredit,$ikn,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dkn,$i_kk)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry, i_area,i_jurnal_transharian_item)
						  	  values
					  	 ('$acckredit','$ikn','$namakredit','$fdebet','$fposting','$vjumlah',to_date('$dkn','DD-MM-YYYY'),to_date('$dkn','DD-MM-YYYY'),'$dentry','$iarea','$i_kk')");
	}
	 function inserttransitemdebetout($accdebet,$ikn,$namadebet,$fdebet,$fposting,$iarea,$eremark,$vjumlah,$dkn,$i_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry, i_area,i_jurnal_transharian_item)
						  	  values
					  	 ('$accdebet','$ikn','$namadebet','$fdebet','$fposting','$vjumlah',to_date('$dkn','YYYY-MM-DD'),to_date('$dkn','YYYY-MM-DD'),'$dentry','$iarea','$i_kb')");
	}
	function inserttransitemkreditout($acckredit,$ikn,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dkn,$i_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry, i_area,i_jurnal_transharian_item)
						  	  values
					  	 ('$acckredit','$ikn','$namakredit','$fdebet','$fposting','$vjumlah',to_date('$dkn','YYYY-MM-DD'),to_date('$dkn','YYYY-MM-DD'),'$dentry','$iarea','$i_kb')");
	}
  function insertgldebet($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,id_area,d_refference,deskripsi,tgl_input,id_coa)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','DD-MM-YYYY'),'$namadebet','$fdebet',$vjumlah,'$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa')");
	}
	function insertglkredit($acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$accdebet)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa)
						  	  values
					  	 ('$ikn','$acckredit',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa')");
	}
	function insertgldebetkbout($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$id_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,id_area,d_refference,deskripsi,tgl_input,id_coa,i_general_ledger)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','YYYY-MM-DD'),'$namadebet','$fdebet',$vjumlah,'$iarea',to_date('$dkn','YYYY-MM-DD'),'$eremark','$dentry','$id_coa','$id_kb')");
	}
	function insertglkreditkbout($acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$accdebet,$id_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa,i_general_ledger)
						  	  values
					  	 ('$ikn','$acckredit',to_date('$dkn','YYYY-MM-DD'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','YYYY-MM-DD'),'$eremark','$dentry','$id_coa','$id_kb')");
	}
	function insertgldebetkk($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$id_kk)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,id_area,d_refference,deskripsi,tgl_input,id_coa,i_general_ledger)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','DD-MM-YYYY'),'$namadebet','$fdebet',$vjumlah,'$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$id_kk')");
	}
	function insertglkreditkk($acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$id_kk)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,i_general_ledger)
						  	  values
						   ('$ikn','$acckredit',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_kk')");
						   
						//    $this->db->query("insert into tm_general_ledger
						//  (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa,i_general_ledger)
						//   	  values
					  	//  ('$ikn','$acckredit',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$id_kk')");
	}
	
	 function insertgldebetkb($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$id_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,id_area,d_refference,deskripsi,tgl_input,id_coa,i_general_ledger)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','DD-MM-YYYY'),'$namadebet','$fdebet',$vjumlah,'$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$id_kb')");
	}
	function insertglkreditkb($acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$accdebet,$kode_bank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa,i_coa_bank,kode_bank)
						  	  values
					  	 ('$ikn','$acckredit',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$accdebet','$kode_bank')");
	}
	function insertglkreditkbin($acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$id_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa,i_general_ledger)
						  	  values
					  	 ('$ikn','$acckredit',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$id_kb')");
	}
	 function insertgldebetbm($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$i_bank,$id_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,id_area,d_refference,deskripsi,tgl_input,id_coa,i_coa_bank,kode_bank,i_general_ledger)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','DD-MM-YYYY'),'$namadebet','$fdebet',$vjumlah,'$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$accdebet','$i_bank','$id_kb')");
	}
	 function insertgldebetbk($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$i_bank,$id_kb, $acckredit)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,id_area,d_refference,deskripsi,tgl_input,id_coa,i_coa_bank,kode_bank,i_general_ledger)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','DD-MM-YYYY'),'$namadebet','$fdebet',$vjumlah,'$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$acckredit','$i_bank','$id_kb')");
	}
	function insertglkreditbm($accdebet,$acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$i_bank,$id_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa,i_coa_bank,kode_bank,i_general_ledger)
						  	  values
					  	 ('$ikn','$acckredit',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$accdebet','$i_bank','$id_kb')");
	}
	function insertglkreditbk($accdebet,$acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$i_bank,$id_kb)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa,i_coa_bank,kode_bank,i_general_ledger)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$accdebet','$i_bank','$id_kb')");
	}
	 function insertgldebetout($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$i_bank,$i_general_ledger)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,id_area,d_refference,deskripsi,tgl_input,id_coa,i_coa_bank,kode_bank,i_general_ledger)
						  	  values
					  	 ('$ikn','$accdebet',to_date('$dkn','DD-MM-YYYY'),'$namadebet','$fdebet',$vjumlah,'$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$accdebet','$i_bank','$i_general_ledger')");
	}
	function insertglkreditout($accdebet,$acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark,$id_coa,$i_bank,$i_general_ledger)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,kode_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,id_area,d_refference,deskripsi,tgl_input,id_coa,i_coa_bank,kode_bank,i_general_ledger)
						  	  values
					  	 ('$ikn','$acckredit',to_date('$dkn','DD-MM-YYYY'),'$namakredit','$fdebet','$vjumlah','$iarea',to_date('$dkn','DD-MM-YYYY'),'$eremark','$dentry','$id_coa','$accdebet','$i_bank','$i_general_ledger')");
	}
  function getAllcoa($num, $offset, $cari)
  {
	  $sql = " a.*,b.nama as nama_area FROM tm_coa a LEFT JOIN tm_area b ON a.id_area=b.id WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.kode) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
   function getAllsubledgercoa($num, $offset, $cari)
  {
	  $sql = " * FROM tm_subledger_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
   function getAllcoasubledgertanpalimit($cari)
  {
	  $sql = " * FROM tm_subledger_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
   function getAllledgercoa($num, $offset, $cari)
  {
	  $sql = " * FROM tm_ledger_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
   function getAllcoageneralledgertanpalimit($cari)
  {
	  $sql = " * FROM tm_generalledger_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
   function getAllgeneralledgercoa($num, $offset, $cari)
  {
	  $sql = " * FROM tm_generalledger_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
   function getAllcoaledgertanpalimit( $cari)
  {
	  $sql = " * FROM tm_ledger_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllgroupcoa($num, $offset, $cari)
  {
	  $sql = " * FROM tm_group_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  function get_area()
  {
	  $sql = " * FROM tm_area  ORDER BY kode_area";
	  $this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllcoatanpalimit($cari) {
	$sql = " * FROM tm_coa a LEFT JOIN tm_area b ON a.id_area=b.id WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.kode) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%')) ";	  
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
   function getAllcoagrouptanpalimit($cari) {
	$sql = " * FROM tm_group_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function getcoa($id){
    $this->db->select(" * from tm_coa WHERE id='$id' ", false);
    $query = $this->db->get();
    return $query->result();
  }
    
    function getcoagroup($id){
    $this->db->select(" * from tm_group_coa WHERE id='$id' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
   function getcoageneralledger($id){
    $this->db->select(" * from tm_generalledger_coa WHERE id='$id' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getcoasubledgercoa($id){
    $this->db->select(" * from tm_subledger_coa WHERE id='$id' ", false);
    $query = $this->db->get();
    return $query->result();
  }
   
   function getcoasubledger($id){
    $this->db->select(" * from tm_subledger_coa WHERE id='$id' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
   function getcoaledger($id){
    $this->db->select(" * from tm_ledger_coa WHERE id='$id' ", false);
    $query = $this->db->get();
    return $query->result();
  }
      
 function cek_data_coa_group($kode){
    $this->db->select("id from tm_group_coa WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_coaledger($kode){
    $this->db->select("id from tm_ledger_coa WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_coa($kode){
    $this->db->select("id from tm_coa WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_coa_generalledger($kode){
    $this->db->select("id from tm_generalledger_coa WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_coa_subledger($kode){
    $this->db->select("id from tm_subledger_coa WHERE kode = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savecoa($kode,$nama, $id_coa, $goedit,$kode_group,$kode_subledger,$kode_ledger,$kode_generalledger,$id_area){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_coa ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'kode'=>$kode,
      'nama'=>$nama,
       'id_area'=>$id_area,
      'i_coa_group'=>$kode_group,
      'i_coa_subledger'=>$kode_subledger,
      'i_coa_ledger'=>$kode_ledger,
      'i_coa_generalledger'=>$kode_generalledger,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
     
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_coa',$data); 
	}
	else {
		$data = array(
				'id_area'=>$id_area,
				  'kode'=>$kode,
				  'nama'=>$nama,
				  'i_coa_group'=>$kode_group,
				  'i_coa_subledger'=>$kode_subledger,
				  'i_coa_ledger'=>$kode_ledger,
				  'i_coa_generalledger'=>$kode_generalledger,
				  'tgl_update'=>$tgl,
				  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_coa);
		$this->db->update('tm_coa',$data);  
	}
		
  }
  
  function savecoa_generalledger($kode,$nama, $id_coa, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_generalledger_coa ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'kode'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_generalledger_coa',$data); 
	}
	else {
		$data = array(
				  'kode'=>$kode,
				  'nama'=>$nama,
				  'tgl_update'=>$tgl,
				  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_coa);
		$this->db->update('tm_generalledger_coa',$data);  
	}
		
  }
  
    function savecoa_group($kode,$nama, $id_coa, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_group_coa ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'kode'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_group_coa',$data); 
	}
	else {
		$data = array(
				  'kode'=>$kode,
				  'nama'=>$nama,
				  'tgl_update'=>$tgl,
				  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_coa);
		$this->db->update('tm_group_coa',$data);  
	}
		
  }
  
  function savecoaledger($kode,$nama, $id_coa, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_ledger_coa ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'kode'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_ledger_coa',$data); 
	}
	else {
		$data = array(
				  'kode'=>$kode,
				  'nama'=>$nama,
				  'tgl_update'=>$tgl,
				  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_coa);
		$this->db->update('tm_ledger_coa',$data);  
	}
		
  }
  
  
  function savecoa_subledger($kode,$nama, $id_coa, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_subledger_coa ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'kode'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_subledger_coa',$data); 
	}
	else {
		$data = array(
				  'kode'=>$kode,
				  'nama'=>$nama,
				  'tgl_update'=>$tgl,
				  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_coa);
		$this->db->update('tm_subledger_coa',$data);  
	}
		
  }
  
  function deletecoa($id){    
    $this->db->delete('tm_coa', array('id' => $id));
  }
  function deletecoagroup($id){    
    $this->db->delete('tm_group_coa', array('id' => $id));
  }
   function deletecoasubledger($id){    
    $this->db->delete('tm_subledger_coa', array('id' => $id));
  }
  function deletecoaledger($id){    
    $this->db->delete('tm_ledger_coa', array('id' => $id));
  }
   function deletecoageneralledger($id){    
    $this->db->delete('tm_generalledger_coa', array('id' => $id));
  }
  
  // 17-12-2015
  function getAllbank($num, $offset, $cari)
  {
	  $sql = " a.*, b.kode as kode_coa, b.nama as nama_coa FROM tm_bank a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.kode) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY a.kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllbanktanpalimit($cari) {
	$sql = " a.*, b.kode as kode_coa, b.nama as nama_coa FROM tm_bank a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.kode) like UPPER('%$cari%') OR UPPER(a.nama) like UPPER('%$cari%')) ";	  
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function getbank($id){
    $this->db->select(" a.*, b.kode as kode_coa, b.nama as nama_coa from tm_bank a INNER JOIN tm_coa b ON a.id_coa = b.id 
						WHERE a.id='$id' ", false);
    $query = $this->db->get();
    return $query->result();
  }
      
 function cek_data_bank($kode, $id_coa){
    $this->db->select("id from tm_bank WHERE kode = '$kode' OR id_coa = '$id_coa' ", false);
//    echo "id from tm_bank WHERE kode = '$kode' OR id_coa = '$id_coa'"; die();
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savebank($kode,$nama, $id_bank, $id_coa, $goedit,$kode_coa){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_bank ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'kode'=>$kode,
      'nama'=>$nama,
      'kode_coa'=>$kode_coa,
      'id_coa'=>$id_coa,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_bank',$data); 
	}
	else {
		$data = array(
				  'kode'=>$kode,
				  'nama'=>$nama,
				  'kode_coa'=>$kode_coa,
				  'id_coa'=>$id_coa,
				  'tgl_update'=>$tgl,
				  'uid_update_by'=>$uid_update_by
				);
		
		$this->db->where('id',$id_bank);
		$this->db->update('tm_bank',$data);  
	}
		
  }
  
  function deletebank($id){    
    $this->db->delete('tm_bank', array('id' => $id));
  }
  
  // 28-12-2015
  function getAllsaldoakun($num, $offset, $cari)
  {
	  $sql = " a.*, b.kode, a.nama_coa as nama FROM tt_saldo_akun a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY a.tahun DESC, a.bulan DESC, b.kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllsaldoakuntanpalimit($cari) {
	$sql = " a.*, b.kode, b.nama FROM tt_saldo_akun a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get_listcoa($num, $offset, $cari)
  {
	  $sql = " * FROM tm_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
   function get_listalo($num, $offset, $cari,$coa,$kode_coa)
  { 
	 
	  if($kode_coa=='110-41SM'){
	   $db2 = $this->load->database('db_external', TRUE);
	  $sql = " * FROM  (
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur WHERE f_faktur_cancel='f'
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' 
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f' 
			)x WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(i_faktur_code) like UPPER('%$cari%')) ";	  
		$sql.= " ORDER BY i_faktur_code ";
	   $db2->select($sql, false)->limit($num,$offset);
	   $query = $db2->get();
	   return $query->result();
  }
  else
  {
	  $sql="";
	  }
	    
  }
  
  function get_listcoatanpalimit($cari) {
	  
	 
	$sql = " * FROM tm_coa WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ";
			
	 $this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
   function get_listalotanpalimit($cari,$coa,$kode_coa) {
	   
	 
	   if($kode_coa=='110-41SM'){
	   
	 // Database External
	  $db2 = $this->load->database('db_external', TRUE);
	  $pencarian="";
	   if ($cari != "all")
		$pencarian.= " AND (UPPER(i_faktur_code) like UPPER('%$cari%')) ";	  
	  
      $query = $db2->query("SELECT * FROM (
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur WHERE f_faktur_cancel='f'
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' 
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f' 
			)x" .$pencarian." ORDER BY i_faktur_code ");
		
		
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
	 }
  else
  {
	  $sql="";}
  }
  
  // 08-01-2016
  function cek_data_saldo($id_coa, $bulan, $tahun){
    $this->db->select("id from tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan = '$bulan' AND tahun='$tahun' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savesaldoakun($id_coa, $nama_coa, $bulan, $tahun, $saldo_awal, $debet, $kredit, $saldo_akhir){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
    $i_periode=$tahun.$bulan;
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tt_saldo_akun ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
			
    $data = array(
	  'id'=>$idbaru,
      'id_coa'=>$id_coa,
      'nama_coa'=>$nama_coa,
      'bulan'=>$bulan,
      'tahun'=>$tahun,
       'i_periode'=>$i_periode,
      'saldo_awal'=>$saldo_awal,
      'debet'=>$debet,
      'kredit'=>$kredit,
      'saldo_akhir'=>$saldo_akhir,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'uid_update_by'=>$uid_update_by
    );

	$this->db->insert('tt_saldo_akun',$data); 
  }
  
  function getsaldoakun($id_saldo){
    $this->db->select(" * from tt_saldo_akun WHERE id='$id_saldo' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  function deletesaldoakun($id){    
    $this->db->delete('tt_saldo_akun', array('id' => $id));
  }
  
  // ----------- 09-01-2016 ------------------
  function getAllkasbesarin($num, $offset, $cari)
  {
	  $sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_besar a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_debet = 'f' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";	  
	  //~ $sql.= " ORDER BY a.tahun DESC, a.bulan DESC, a.tgl DESC, b.kode ";
	  $sql.= " ORDER BY a.id DESC";
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllkasbesarintanpalimit($cari) {
	$sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_besar a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_debet = 'f' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  // 12-01-2016
  function runningnumberkb($bulan,$tahun)
	{
		/*$this->db->select(" max(substr(no_transaksi,9,5)) as max from tt_kas_besar where substr(no_transaksi,4,2)='$tahun' and substr(no_transaksi,6,2)='$bulan'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="KB-".$tahun.$bulan."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="KB-".$tahun.$bulan."-".$nogj;
			return $nogj;
		} */
		
		$query3	= $this->db->query(" SELECT no_transaksi FROM tt_kas_besar WHERE bulan='$bulan' AND tahun = '$tahun' 
							ORDER BY no_transaksi DESC LIMIT 1 ");
		$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_transaksi;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==15) {
				$nobonm = substr($no_bonm, 0, 15);
				$n_bonm	= (substr($nobonm,10,5))+1;

				$jml_n_bonm	= $n_bonm;
				switch(strlen($jml_n_bonm)) {
					case "1": $kodebonm	= "0000".$jml_n_bonm;
					break;
					case "2": $kodebonm	= "000".$jml_n_bonm;
					break;	
					case "3": $kodebonm	= "00".$jml_n_bonm;
					break;
					case "4": $kodebonm	= "0".$jml_n_bonm;
					break;
					case "5": $kodebonm	= $jml_n_bonm;
					break;	
				}
				$nomortrans = "KB-".$tahun.$bulan."-".$kodebonm;
			}
			else {
				$nomortrans = "KB-".$tahun.$bulan."-00001";
			}
		return $nomortrans;
    }
  
  function runningnumberkk($bulan,$tahun)
	{
		/*$this->db->select(" max(substr(no_transaksi,9,5)) as max from tt_kas_besar where substr(no_transaksi,4,2)='$tahun' and substr(no_transaksi,6,2)='$bulan'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="KB-".$tahun.$bulan."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="KB-".$tahun.$bulan."-".$nogj;
			return $nogj;
		} */
		
		$query3	= $this->db->query(" SELECT no_transaksi FROM tt_kas_kecil WHERE bulan='$bulan' AND tahun = '$tahun' 
							ORDER BY no_transaksi DESC LIMIT 1 ");
		$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_transaksi;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==15) {
				$nobonm = substr($no_bonm, 0, 15);
				$n_bonm	= (substr($nobonm,10,5))+1;

				$jml_n_bonm	= $n_bonm;
				switch(strlen($jml_n_bonm)) {
					case "1": $kodebonm	= "0000".$jml_n_bonm;
					break;
					case "2": $kodebonm	= "000".$jml_n_bonm;
					break;	
					case "3": $kodebonm	= "00".$jml_n_bonm;
					break;
					case "4": $kodebonm	= "0".$jml_n_bonm;
					break;
					case "5": $kodebonm	= $jml_n_bonm;
					break;	
				}
				$nomortrans = "KK-".$tahun.$bulan."-".$kodebonm;
			}
			else {
				$nomortrans = "KK-".$tahun.$bulan."-00001";
			}
		return $nomortrans;
    }
  
  function savekasbesarin($bulan, $tahun, $id_coa, $nama_coa, $tgl_trans, $deskripsi, $jumlah, $id_area){  
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		$ikb=$this->mmaster->runningnumberkb($bulan, $tahun);
		
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
		
		$seqxx	= $this->db->query(" SELECT id FROM tt_kas_besar ORDER BY id DESC LIMIT 1 ");
		if($seqxx->num_rows() > 0) {
			$seqrowxx	= $seqxx->row();
			$id_kb	= $seqrowxx->id+1;
		}else{
			$id_kb	= 1;
		}
	$query4	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$id_coa'  ");
		if ($query4->num_rows() > 0){
			$hasilrow4 = $query4->row();
			$kode_coa=$hasilrow4->kode;
		}
		
		
		// insert ke tt_kas_besar
		$data_detail = array(
				'id'=>$id_kb,
				'bulan'=>$bulan,
				'tahun'=>$tahun,
				'no_transaksi'=>$ikb,
				'tgl'=>$tgl_trans,
				'id_coa'=>$id_coa,
				'kode_coa'=>$kode_coa,
				'nama_coa'=>$nama_coa,
				'deskripsi'=>$deskripsi,
				'jumlah'=>$jumlah,
				'is_debet'=>'f',
				'is_kredit'=>'t',
				'id_area' => $id_area,
				'tgl_input'=>$tgl,
				'tgl_update'=>$tgl,
				'uid_update_by' => $uid_update_by
		);
		$this->db->insert('tt_kas_besar',$data_detail);
	
		// tambah saldo akun di debet dan saldo akhirnya sesuai coa-nya
		$query3	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet+= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
			
		return $data=array('i_kb'=>$ikb,
								'id_kb'=>$id_kb,
							'kode_coa'=>$kode_coa
						);
  }
  
  function deletekasbesarin($id_kb){    
	  $query3	= $this->db->query(" SELECT id_coa, bulan, tahun, jumlah,no_transaksi FROM tt_kas_besar WHERE id='$id_kb' ");
	  if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$jumlah = $hasilrow->jumlah;
		$id_coa = $hasilrow->id_coa;
		$bulan = $hasilrow->bulan;
		$tahun = $hasilrow->tahun;
		$no_transaksi = $hasilrow->no_transaksi;
		
		// tambah saldo akun di debet dan saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet-= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
	  }
		
		$this->db->delete('tt_kas_besar', array('id' => $id_kb));
	   $this->db->delete('tm_general_ledger', array('i_refference' => $no_transaksi));
	   $this->db->delete('tm_jurnal_transharian', array('i_refference' => $no_transaksi));
	   $this->db->delete('tm_jurnal_transharianitem', array('i_refference' => $no_transaksi));
  }
  
  // ------------------- 18-01-2016 ------------------------------------------------------
  function getkasbesarin($id_kb){
    $this->db->select(" * from tt_kas_besar WHERE id='$id_kb' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllkasbesarout($num, $offset, $cari)
  {
	  $sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_besar a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_debet = 't' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY a.id  DESC";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllkasbesarouttanpalimit($cari) {
	$sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_besar a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_debet = 't' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function savekasbesarout($bulan, $tahun, $id_coa, $nama_coa, $tgl_trans, $deskripsi, $jumlah, $id_area){  
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		$ikb=$this->mmaster->runningnumberkb($bulan, $tahun);
		
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
		
		$seqxx	= $this->db->query(" SELECT id FROM tt_kas_besar ORDER BY id DESC LIMIT 1 ");
		if($seqxx->num_rows() > 0) {
			$seqrowxx	= $seqxx->row();
			$id_kb	= $seqrowxx->id+1;
		}else{
			$id_kb	= 1;
		}
	$query4	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$id_coa'  ");
		if ($query4->num_rows() > 0){
			$hasilrow4 = $query4->row();
			$kode_coa=$hasilrow4->kode;
		}
		// insert ke tt_kas_besar
		$data_detail = array(
				'id'=>$id_kb,
				'bulan'=>$bulan,
				'tahun'=>$tahun,
				'no_transaksi'=>$ikb,
				'tgl'=>$tgl_trans,
				'id_coa'=>$id_coa,
				'kode_coa'=>$kode_coa,
				'nama_coa'=>$nama_coa,
				'deskripsi'=>$deskripsi,
				'jumlah'=>$jumlah,
				'id_area'=>$id_area,
				'is_kredit'=>'f',
				'is_debet'=>'t',
				'tgl_input'=>$tgl,
				'tgl_update'=>$tgl,
				'uid_update_by' => $uid_update_by
		);
		$this->db->insert('tt_kas_besar',$data_detail);
		
		// tambah saldo akun di kredit dan saldo akhirnya sesuai coa-nya
		$query3	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit+= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		return $data=array('i_kb'=>$ikb,
							
							'id_kb'=>$id_kb
						);
  }
  
  function deletekasbesarout($id_kb){    
	  $query3	= $this->db->query(" SELECT id_coa, bulan, tahun, jumlah,no_transaksi FROM tt_kas_besar WHERE id='$id_kb' ");
	  if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$jumlah = $hasilrow->jumlah;
		$id_coa = $hasilrow->id_coa;
		$bulan = $hasilrow->bulan;
		$tahun = $hasilrow->tahun;
		$no_transaksi = $hasilrow->no_transaksi;
		
		// reset saldo akun di kredit dan saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit-= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
	  }
		
	  $this->db->delete('tt_kas_besar', array('id' => $id_kb));
	    $this->db->delete('tm_general_ledger', array('i_refference' => $no_transaksi));
  }
  
  // 19-01-2016
  function getkasbesarout($id_kb){
    $this->db->select(" * from tt_kas_besar WHERE id='$id_kb' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  // BANK KELUAR
  function getAllbankout($num, $offset, $cari)
  {
	  $sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_bank a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_kredit = 'f' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";	  
	  //~ $sql.= " ORDER BY a.tahun DESC, a.bulan DESC, a.tgl_update DESC ";
	  $sql.= " ORDER BY a.id DESC ";
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllbankouttanpalimit($cari) {
	$sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_bank a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_kredit = 'f' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get_bank_by_coa(){
    $query = $this->db->query(" SELECT a.*, b.kode as kode_coa_bank, b.nama as nama_coa_bank FROM tm_bank a 
							INNER JOIN tm_coa b ON a.id_coa = b.id ORDER BY a.kode");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
  
  function savebankout($bulan, $tahun, $id_coa_bank, $id_coa, $nama_coa, $tgl_trans, $deskripsi, $jumlah,$id_area){  
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		$ikb=$this->mmaster->runningnumberkasbankout($bulan, $tahun, $id_coa_bank);
		
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
		
		$seqxx	= $this->db->query(" SELECT id FROM tt_kas_bank ORDER BY id DESC LIMIT 1 ");
		if($seqxx->num_rows() > 0) {
			$seqrowxx	= $seqxx->row();
			$id_kb	= $seqrowxx->id+1;
		}else{
			$id_kb	= 1;
		}
		$query4	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$id_coa'  ");
		if ($query4->num_rows() > 0){
			$hasilrow4 = $query4->row();
			$kode_coa=$hasilrow4->kode;
		}	
		
		$query5	= $this->db->query(" SELECT kode,nama,kode_coa FROM tm_bank WHERE id_coa = '$id_coa_bank'  ");
		if ($query5->num_rows() > 0){
			$hasilrow5 = $query5->row();
			$kode_bank=$hasilrow5->kode;
			$kode_coa_bank=$hasilrow5->kode_coa;
			$nama_coa_bank=$hasilrow5->nama;
		}
		// insert ke tt_kas_bank
		$data_detail = array(
				'id'=>$id_kb,
				'id_coa_bank'=>$id_coa_bank,
				'bulan'=>$bulan,
				'tahun'=>$tahun,
				'no_transaksi'=>$ikb,
				'tgl'=>$tgl_trans,
				'id_coa'=>$id_coa,
				'kode_bank'=>$kode_bank,
				'kode_coa'=>$kode_coa,
				'nama_coa'=>$nama_coa,
				'deskripsi'=>$deskripsi,
				'jumlah'=>$jumlah,
				'v_sisa'=>$jumlah,
				'is_kredit'=>'f',
				'is_debet'=>'t',
				'id_area' => $id_area,
				'tgl_input'=>$tgl,
				'tgl_update'=>$tgl,
				'uid_update_by' => $uid_update_by
		);
		$this->db->insert('tt_kas_bank',$data_detail);
		
		// tambah saldo akun di kredit dan saldo akhirnya sesuai coa-nya
		$query3	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit+= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
						
						
		}
		return $data=array('i_kb'=>$ikb,
							'kode_coa'=>$kode_coa,
							'id_kb'=>$id_kb
							
						);
  }
  
  function runningnumberkasbankin($bulan,$tahun, $id_coa_bank)
	{		
		$query3	= $this->db->query(" SELECT no_transaksi FROM tt_kas_bank WHERE bulan='$bulan' AND tahun = '$tahun' 
							AND id_coa_bank = '$id_coa_bank' AND no_transaksi like '%BM%' ORDER BY no_transaksi DESC LIMIT 1 ");
		$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_transaksi;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==15) {
				$nobonm = substr($no_bonm, 0, 15);
				$n_bonm	= (substr($nobonm,10,5))+1;

				$jml_n_bonm	= $n_bonm;
				switch(strlen($jml_n_bonm)) {
					case "1": $kodebonm	= "0000".$jml_n_bonm;
					break;
					case "2": $kodebonm	= "000".$jml_n_bonm;
					break;	
					case "3": $kodebonm	= "00".$jml_n_bonm;
					break;
					case "4": $kodebonm	= "0".$jml_n_bonm;
					break;
					case "5": $kodebonm	= $jml_n_bonm;
					break;	
				}
				$nomortrans = "BM-".$tahun.$bulan."-".$kodebonm;
			}
			else {
				$nomortrans = "BM-".$tahun.$bulan."-00001";
			}
		return $nomortrans;
    }
    function runningnumberkasbankout($bulan,$tahun, $id_coa_bank)
	{		
		$query3	= $this->db->query(" SELECT no_transaksi FROM tt_kas_bank WHERE bulan='$bulan' AND tahun = '$tahun' 
							AND id_coa_bank = '$id_coa_bank' AND no_transaksi like '%BK%' ORDER BY no_transaksi DESC LIMIT 1 ");
		$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bonm	= $hasilrow->no_transaksi;
			else
				$no_bonm = '';
			if(strlen($no_bonm)==15) {
				$nobonm = substr($no_bonm, 0, 15);
				$n_bonm	= (substr($nobonm,10,5))+1;

				$jml_n_bonm	= $n_bonm;
				switch(strlen($jml_n_bonm)) {
					case "1": $kodebonm	= "0000".$jml_n_bonm;
					break;
					case "2": $kodebonm	= "000".$jml_n_bonm;
					break;	
					case "3": $kodebonm	= "00".$jml_n_bonm;
					break;
					case "4": $kodebonm	= "0".$jml_n_bonm;
					break;
					case "5": $kodebonm	= $jml_n_bonm;
					break;	
				}
				$nomortrans = "BK-".$tahun.$bulan."-".$kodebonm;
			}
			else {
				$nomortrans = "BK-".$tahun.$bulan."-00001";
			}
		return $nomortrans;
    }
    // 20-01-2016
    function deletebankout($id_kbank){    
	  $query3	= $this->db->query(" SELECT id_coa, bulan, tahun, jumlah,no_transaksi FROM tt_kas_bank WHERE id='$id_kbank' ");
	  if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$jumlah = $hasilrow->jumlah;
		$id_coa = $hasilrow->id_coa;
		$bulan = $hasilrow->bulan;
		$tahun = $hasilrow->tahun;
		$no_transaksi = $hasilrow->no_transaksi;
		
		// reset saldo akun di kredit dan saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit-= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
	  }
		
	  $this->db->delete('tt_kas_bank', array('id' => $id_kbank));
	    $this->db->delete('tm_general_ledger', array('i_refference' => $no_transaksi));
  }
  
  // 20-01-2016
  function getbankout($id_kbank){
    $this->db->select(" * from tt_kas_bank WHERE id='$id_kbank' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  // BANK MASUK
  function getAllbankin($num, $offset, $cari)
  {
	  $sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_bank a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_debet = 'f' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";	  
	  //~ $sql.= " ORDER BY a.tahun DESC, a.bulan DESC, a.tgl_update DESC";
	  $sql.= " ORDER BY a.id DESC";
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAllbankintanpalimit($cari) {
	$sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_bank a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_debet = 'f' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function savebankin($bulan, $tahun, $id_coa_bank, $id_coa, $nama_coa, $tgl_trans, $deskripsi, $jumlah,$id_area,$kode_alo,$id_alo){  
	
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		$ikb=$this->mmaster->runningnumberkasbankin($bulan, $tahun, $id_coa_bank);
		
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
		
		$seqxx	= $this->db->query(" SELECT id FROM tt_kas_bank ORDER BY id DESC LIMIT 1 ");
		if($seqxx->num_rows() > 0) {
			$seqrowxx	= $seqxx->row();
			$id_kb	= $seqrowxx->id+1;
		}else{
			$id_kb	= 1;
		}

		$query4	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$id_coa'  ");
		if ($query4->num_rows() > 0){
			$hasilrow4 = $query4->row();
			$kode_coa=$hasilrow4->kode;
		}
		
		$query5	= $this->db->query(" SELECT kode,nama,kode_coa FROM tm_bank WHERE id_coa = '$id_coa_bank'  ");
		if ($query5->num_rows() > 0){
			$hasilrow5 = $query5->row();
			$kode_bank=$hasilrow5->kode;
			$kode_coa_bank=$hasilrow5->kode_coa;
			$nama_coa_bank=$hasilrow5->nama;
		}

		// insert ke tt_kas_bank
		$data_detail = array(
				'id'=>$id_kb,
				'id_coa_bank'=>$id_coa_bank,
				'bulan'=>$bulan,
				'tahun'=>$tahun,
				'no_transaksi'=>$ikb,
				'tgl'=>$tgl_trans,
				'id_coa'=>$id_coa,
				'nama_coa'=>$nama_coa,
				'kode_coa'=>$kode_coa,
				'kode_bank'=>$kode_bank,
				'deskripsi'=>$deskripsi,
				'jumlah'=>$jumlah,
				'is_debet'=>'f',
				'is_kredit'=>'t',
				'tgl_input'=>$tgl,
				'tgl_update'=>$tgl,
				'uid_update_by' => $uid_update_by,
				'id_area'=> $id_area,
				'v_sisa'=>$jumlah
		);
		$this->db->insert('tt_kas_bank',$data_detail);
		
		
		// tambah saldo akun di debet dan saldo akhirnya sesuai coa-nya
		$query3	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet+= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
			return $data=array('i_kb'=>$ikb,
							'id_kb'=>$id_kb,
							'kode_coa'=>$kode_coa,
							'kode_bank'=>$kode_bank,
						
							
						);
  }
  
  // 22-01-2016
  function deletebankin($id_kbank){    
	  $query3	= $this->db->query(" SELECT id_coa, bulan, tahun, jumlah,no_transaksi FROM tt_kas_bank WHERE id='$id_kbank' ");
	  if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$jumlah = $hasilrow->jumlah;
		$id_coa = $hasilrow->id_coa;
		$bulan = $hasilrow->bulan;
		$tahun = $hasilrow->tahun;
		$no_transaksi = $hasilrow->no_transaksi;
		
		// tambah saldo akun di debet dan saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, debet, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$sdebet	= $hasilrow->debet;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$sdebet-= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET debet = '$sdebet', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
	  }
		
	  $this->db->delete('tt_kas_bank', array('id' => $id_kbank));
	  $this->db->delete('tm_general_ledger', array('i_refference' => $no_transaksi));
  }
  
  function getbankin($id_kbank){
    $this->db->select(" * from tt_kas_bank WHERE id='$id_kbank' ", false);
    $query = $this->db->get();
    return $query->result();
  }

 function getAllkaskeciltanpalimit($cari) {
	$sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_kecil a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_kredit = 'f' ";
	
	  if ($cari != "all"){
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";
		}
			
			$sql.= " ORDER BY a.tahun DESC, a.bulan DESC, a.tgl DESC, b.kode ";
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
function getAllkaskecil($num, $offset, $cari)
  {
	  $sql = " a.*, b.kode, a.nama_coa as nama FROM tt_kas_kecil a INNER JOIN tm_coa b ON a.id_coa = b.id WHERE a.is_kredit = 'f' ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(b.kode) like UPPER('%$cari%') OR UPPER(b.nama) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY a.tahun DESC, a.bulan DESC,  a.tgl_update DESC ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
  function savekaskecil($bulan, $tahun, $id_coa, $nama_coa, $tgl_trans, $deskripsi, $jumlah,$id_area,$iperiode){  
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		$ikk=$this->mmaster->runningnumberkk($bulan, $tahun);
		
		$pisah1 = explode("-", $tgl_trans);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_trans = $thn1."-".$bln1."-".$tgl1;
		
		$seqxx	= $this->db->query(" SELECT id FROM tt_kas_kecil ORDER BY id DESC LIMIT 1 ");
		if($seqxx->num_rows() > 0) {
			$seqrowxx	= $seqxx->row();
			$id_kk	= $seqrowxx->id+1;
		}else{
			$id_kk	= 1;
		}

		$query4	= $this->db->query(" SELECT kode FROM tm_coa WHERE id = '$id_coa'  ");
		if ($query4->num_rows() > 0){
			$hasilrow4 = $query4->row();
			$kode_coa=$hasilrow4->kode;
		}
		
		$query5	= $this->db->query(" SELECT id,kode,nama FROM tm_coa WHERE id_area = '$id_area'  ");
		if ($query5->num_rows() > 0){
			$hasilrow5 = $query5->row();
			$kode_coa_area=$hasilrow5->kode;
			$nama_coa_area=$hasilrow5->nama;
			$id_coa_area=$hasilrow5->id;
		}
		else{
			$kode_coa_area='';
			$nama_coa_area='';
			$id_coa_area=0;
			}
		// insert ke tt_kas_kecil
		$data_detail = array(
				'id'=>$id_kk,
				'bulan'=>$bulan,
				'tahun'=>$tahun,
				'no_transaksi'=>$ikk,
				'tgl'=>$tgl_trans,
				'id_coa'=>$id_coa,
				'nama_coa'=>$nama_coa,
				'kode_coa'=>$kode_coa,
				'deskripsi'=>$deskripsi,
				'jumlah'=>$jumlah,
				'id_area'=>$id_area,
				'is_kredit'=>'f',
				'is_debet'=>'t',
				'd_periode'=>$iperiode,
				'tgl_input'=>$tgl,
				'tgl_update'=>$tgl,
				'uid_update_by' => $uid_update_by
		);
		$this->db->insert('tt_kas_kecil',$data_detail);
		
		// tambah saldo akun di kredit dan saldo akhirnya sesuai coa-nya
		$query3	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit+= $jumlah;
			$ssaldo_akhir-= $jumlah;
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		//~ return $data=array(
		//~ 'id_kk'=>$id_kk,
		//~ 'i_kb'=>$ikk,
							//~ 'kode_coa'=>$kode_coa,
							//~ 'kode_coa_area'=>$kode_coa_area,
							//~ 'nama_coa_area'=>$nama_coa_area,
							//~ 'id_coa_area'=>$id_coa_area
						//~ );
						
						
						
						return $data=array(
							'id_kk'=>$id_kk,
							'i_kk'=>$ikk
							
						);
  }
  
   function deletekaskecil($id_kk){    
	  $query3	= $this->db->query(" SELECT id_coa, bulan, tahun, jumlah,no_transaksi FROM tt_kas_kecil WHERE id='$id_kk' ");
	  if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$jumlah = $hasilrow->jumlah;
		$id_coa = $hasilrow->id_coa;
		$bulan = $hasilrow->bulan;
		$tahun = $hasilrow->tahun;
		$no_transaksi = $hasilrow->no_transaksi;
		
		// reset saldo akun di kredit dan saldo akhirnya sesuai coa-nya
		$query31	= $this->db->query(" SELECT id, kredit, saldo_akhir FROM tt_saldo_akun WHERE id_coa = '$id_coa' AND bulan='$bulan'
							AND tahun='$tahun' ");
		if ($query31->num_rows() > 0){
			$hasilrow = $query31->row();
			$id_saldo_akun = $hasilrow->id;
			$skredit	= $hasilrow->kredit;
			$ssaldo_akhir	= $hasilrow->saldo_akhir;
			
			$skredit-= $jumlah;
			$ssaldo_akhir+= $jumlah;
			
			$tgl = date("Y-m-d H:i:s");
			$uid_update_by = $this->session->userdata('uid');
			
			$this->db->query(" UPDATE tt_saldo_akun SET kredit = '$skredit', saldo_akhir = '$ssaldo_akhir', 
						tgl_update = '$tgl', uid_update_by = '$uid_update_by' 
						WHERE id = '$id_saldo_akun' ");
		}
		
	  }
		
	  $this->db->delete('tt_kas_kecil', array('id' => $id_kk));
	   $this->db->delete('tm_general_ledger', array('i_refference' => $no_transaksi));
  }
  
    function getkaskecil($id_kk){
    $this->db->select(" * from tt_kas_kecil WHERE id='$id_kk' ", false);
    $query = $this->db->get();
    return $query->result();
  }
  
function savejurnalumum($no_jurnal, $tgl_jurnal, $id_area,$keterangan, $debet, $kredit){  
		$tgl = date("Y-m-d H:i:s");
		$uid_update_by = $this->session->userdata('uid');
		
		$seqxx	= $this->db->query(" SELECT id FROM tt_jurnal_umum ORDER BY id DESC LIMIT 1 ");
		if($seqxx->num_rows() > 0) {
			$seqrowxx	= $seqxx->row();
			$id_ju	= $seqrowxx->id+1;
		}else{
			$id_ju	= 1;
		}

	
		$data_header = array(
				'id'=>$id_ju,
				'no_jurnal'=>$no_jurnal,
				'tgl_jurnal'=>$tgl_jurnal,
				'deskripsi'=>$keterangan,
				'total_debet'=>$debet,
				'total_kredit'=>$kredit,
				'tgl_input'=>$tgl,
				'tgl_update'=>$tgl,
				'uid_update_by'=>$uid_update_by,
				'id_area'=>$id_area	
		);
		$this->db->insert('tt_jurnal_umum',$data_header);
		
		return $id_ju;
		
  }
  
   function savejurnalumum_detail( $id_ju,$kode_coa,$nama_coa, $id_coa, $deskripsi,$debet_detail, $kredit_detail){  
		
		
		$seqxx2	= $this->db->query(" SELECT id FROM tt_jurnal_umum_detail ORDER BY id DESC LIMIT 1 ");
		if($seqxx2->num_rows() > 0) {
			$seqrowxx2	= $seqxx2->row();
			$id_ju_det	= $seqrowxx2->id+1;
		}else{
			$id_ju_det	= 1;
		}
		
		$data_detail = array(
				'id'=>$id_ju_det,
				'id_jurnal_umum'=>$id_ju,
				'id_coa'=>$id_coa,
				'nama_coa'=>$nama_coa,
				'kode_coa'=>$kode_coa,
				'deskripsi'=>$deskripsi,
				'debet'=>$debet_detail,
				'kredit'=>$kredit_detail
		);
		$this->db->insert('tt_jurnal_umum_detail',$data_detail);
		
  }
  
    function getAlljurnalumum($cari)
  {
	 $sql = " * FROM tt_jurnal_umum  WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.no_jurnal) like UPPER('%$cari%') ";  
	  $sql.= " ORDER BY tgl_input DESC ";
	  
	  $this->db->select($sql);
	  
    $query = $this->db->get();
    return $query->result();
  }
  
 function getAlljurnalumumtanpalimit($num,$offset,$cari) {
	  
	$sql = " * FROM tt_jurnal_umum WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(a.no_jurnal) like UPPER('%$cari%') ";
		$sql.= " ORDER BY id asc ";
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();
		$detail_ju=array();
		$header_ju=array();
      if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		
	foreach($hasil as $row){
		$query2=$this->db->query("SELECT * from tt_jurnal_umum_detail WHERE id_jurnal_umum='$row->id' order by id");
		if($query2->num_rows()>0) {	
		$hasil2 = $query2->result();
		
	foreach($hasil2 as $row2){
		$query3=$this->db->query("SELECT * from tt_jurnal_umum_detail WHERE id='$row2->id' order by id");
		if($query3->num_rows()>0) {	
		$hasilrow3 = $query3->row();
		$nama_coa=$hasilrow3->nama_coa;
		$kode_coa=$hasilrow3->kode_coa;
		$debet=$hasilrow3->debet;
			$kredit=$hasilrow3->kredit;	
			$deskripsi=$hasilrow3->deskripsi;
			$id_detail=$hasilrow3->id;
			}

			
		 $detail_ju[]= array(
		'nama_coa'=>$nama_coa,
		'kode_coa'=>$kode_coa,
		'debet_detail'=>$debet,
		'kredit_detail'=>$kredit,
		'deskripsi'=>$deskripsi
	 );
 }
}
      $header_ju[]= array(
		'id'=>$row->id,
		'no_jurnal'=>$row->no_jurnal,
		'tgl_jurnal'=>$row->tgl_jurnal,
		'deskripsi'=>$row->deskripsi,
		'tgl_update'=>$row->tgl_update,
		'id_area'=>$row->id_area,
		'detail_ju'=>$detail_ju
	 );
      $detail_ju=array();
     }
  
}

return $header_ju; 
}
  
 function update_data_header($id_jurnal,$no_jurnal,$id_area,$keterangan,$total_debet,$total_kredit,$tgl,$uid_update_by,$tgl_jurnal ) {
	
	$this->db->query(" UPDATE tt_jurnal_umum  SET no_jurnal = '$no_jurnal', tgl_jurnal = '$tgl_jurnal', id_area = '$id_area',
	deskripsi = '$keterangan', total_debet = '$total_debet', total_kredit = '$total_kredit',tgl_update = '$tgl',uid_update_by='$uid_update_by'
							WHERE id= '$id_jurnal' ");
	return TRUE;
}

function getjurnalumum($id_jurnal) {
	  
	$sql = " * FROM tt_jurnal_umum WHERE id='$id_jurnal' order by id desc ";
	
		$this->db->select($sql);
		$query = $this->db->get();
		$detail_ju=array();
		$header_ju=array();
      if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		
	foreach($hasil as $row){
		$query2=$this->db->query("SELECT * from tt_jurnal_umum_detail WHERE id_jurnal_umum='$row->id' order by id");
		if($query2->num_rows()>0) {	
		$hasil2 = $query2->result();
		
	foreach($hasil2 as $row2){
		$query3=$this->db->query("SELECT * from tt_jurnal_umum_detail WHERE id='$row2->id' order by id");
		if($query3->num_rows()>0) {	
		$hasilrow3 = $query3->row();
		$id_coa=$hasilrow3->id_coa;
		$nama_coa=$hasilrow3->nama_coa;
		$kode_coa=$hasilrow3->kode_coa;
		$debet=$hasilrow3->debet;
		$kredit=$hasilrow3->kredit;	
		$deskripsi=$hasilrow3->deskripsi;
		$id_detail=$hasilrow3->id;
			}

			
		  $detail_ju[]= array(
		'nama_coa'=>$nama_coa,
		'kode_coa'=>$kode_coa,
		'id_coa'=>$id_coa,
		'debet_detail'=>$debet,
		'kredit_detail'=>$kredit,
		'deskripsi'=>$deskripsi,
		'id_detail'=>$id_detail
	 );
 }
}
         $header_ju[]= array(
		'id'=>$row->id,
		'no_jurnal'=>$row->no_jurnal,
		'tgl_jurnal'=>$row->tgl_jurnal,
		'deskripsi'=>$row->deskripsi,
		'tgl_update'=>$row->tgl_update,
		'id_area'=>$row->id_area,
		'total_debet'=>$row->total_debet,
		'total_kredit'=>$row->total_kredit,
		'detail_ju'=>$detail_ju
	 );
      $detail_ju=array();
     } 
}

return $header_ju; 
}
  function updatedatadetail($id_jurnal,$id_coa,$nama_coa,$kode_coa,$deskripsi,$debet,$kredit, $id_jurnal_detail) {
	
	if($id_jurnal_detail!=''){
	$cekdata=$this->db->query("SELECT id from tt_jurnal_umum_detail where id='$id_jurnal_detail'" );
	
	 if ($cekdata->num_rows() > 0){
			$hasilrow = $cekdata->row();
			$id_tt_det = $hasilrow->id;
			
			$this->db->query(" UPDATE tt_jurnal_umum_detail SET id_coa = '$id_coa', nama_coa = '$nama_coa', 
						kode_coa = '$kode_coa', deskripsi = '$deskripsi' ,debet = '$debet', kredit = '$kredit'	
						WHERE id = '$id_tt_det' ");
		}
	}
		else{
			$cekdata2=$this->db->query("SELECT id from tt_jurnal_umum_detail order by id desc" );
			if($cekdata2->num_rows >= 0){
				$hasilrow2 = $cekdata2->row();
					$idcari =$hasilrow2->id + 1 ;
				
					
				}
			$data=array('id'=>$idcari,'id_jurnal_umum'=>$id_jurnal,'id_coa'=>$id_coa , 'nama_coa'=>$nama_coa , 
			'kode_coa'=>$kode_coa , 'deskripsi'=>$deskripsi ,'debet'=>$debet , 'kredit'=>$kredit);
			
			$this->db->insert( 'tt_jurnal_umum_detail',$data );
			
			
			}
  }
function deljurnalumum($id){  
	
	$this->db->select("id from tt_jurnal_umum where id='$id'");
	$query=$this->db->get();
	if($query->num_rows() > 0){	
		$this->db->delete('tt_jurnal_umum', array('id' => $id));
		$this->db->delete('tt_jurnal_umum_detail', array('id_jurnal_umum' => $id));	
		}
		else return false;
  }
  
  	function runningnumberrvkbin($th,$bl,$iarea,$irvtype)
	{
	$this->db->select ("kode_area FROM tm_area where id='$iarea'  ");
	$query2= $this->db->get();
		if($query2->num_rows() > 0)
		{
			$hasilrow2=$query2->row();
			$kode_area=$hasilrow2->kode_area;
			}
		
		$this->db->select(" max(substr(i_rv,15,8)) as max 
		                    from tm_rv 
		                    where substr(i_rv,4,4)='$th' and substr(i_rv,8,2)='$bl' and kode_area='$kode_area'
		                    and i_rv_type='$irvtype'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$norv  =$terakhir+1;
			settype($norv,"string");
			$a=strlen($norv);
			while($a<6){
			  $norv="0".$norv;
			  $a=strlen($norv);
			}
			$norv  ="RV-".$th.$bl."-".$kode_area.$irvtype.$norv;
			return $irv[]=array('norv'=>$norv,'kode_area'=>$kode_area
						);
		}else{
			$norv  ="000001";
			$norv  ="RV-".$th.$bl."-".$kode_area.$irvtype.$norv;
			return $irv[]=array('norv'=>$norv,'kode_area'=>$kode_area
						);
		}
  }
  function insertrvitemkbin($irv,$iarea,$icoa,$ecoaname,$vrv,$edescription,$ikk,$irvtype,$iareax,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	            => $iarea,
				'i_rv'	            	=> $irv,
				'i_coa'              	=> $icoa,
				'e_coa_name'	        => $ecoaname,
				'v_rv'		            => $vrv,
				'e_remark'    	      => $edescription,
				'i_kk'                => $ikk,
				'i_rv_type'           => $irvtype,
				'i_area_kb'           => $iareax,
				'kode_area'           => $kode_area,
    		)
    	);
    	$this->db->insert('tm_rv_item');
    }
    function insertrvkbin($irv,$iarea,$iperiode,$icoa,$drv,$tot,$eremark,$irvtype,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_rv'	            	=> $irv,
				'i_area'	            => $iarea,
				'i_periode'         	=> $iperiode,
				'i_coa' 	            => $icoa,
				'd_rv'		            => $drv,
				'v_rv'		            => $tot,
				'd_entry'           	=> $dentry,
				'i_rv_type'           => $irvtype,
				'kode_area'           => $kode_area,
				
    		)
    	);
    	$this->db->insert('tm_rv');
    }
    	function runningnumberpvkbout($th,$bl,$iarea,$ipvtype)
	{
		$this->db->select ("kode_area FROM tm_area where id='$iarea'  ");
	$query2= $this->db->get();
		if($query2->num_rows() > 0)
		{
			$hasilrow2=$query2->row();
			$kode_area=$hasilrow2->kode_area;
			}
			
		$this->db->select(" max(substr(i_pv,15,8)) as max 
		                    from tm_pv 
		                    where substr(i_pv,4,4)='$th' and substr(i_pv,8,2)='$bl' and kode_area='$kode_area'
		                    and i_pv_type='$ipvtype'", false);
		$query = $this->db->get();

		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nopv  =$terakhir+1;
			settype($nopv,"string");
			$a=strlen($nopv);
			while($a<6){
			  $nopv="0".$nopv;
			  $a=strlen($nopv);
			}
			$nopv  ="PV-".$th.$bl."-".$iarea.$ipvtype.$nopv;
			return $ipv[]=array('nopv'=>$nopv,'kode_area'=>$kode_area
						);
		
		}else{
			$nopv  ="000001";
			$nopv  ="PV-".$th.$bl."-".$iarea.$ipvtype.$nopv;
			return $ipv[]=array('nopv'=>$nopv,'kode_area'=>$kode_area
						);
		}
  }
   function insertpvkbout($ipv,$iarea,$iperiode,$icoa,$dpv,$tot,$eremark,$ipvtype,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_pv'	            	=> $ipv,
				'i_area'	            => $iarea,
				'i_periode'         	=> $iperiode,
				'i_coa' 	            => $icoa,
				'd_pv'		            => $dpv,
				'v_pv'		            => $tot,
				'd_entry'           	=> $dentry,
				'i_pv_type'           => $ipvtype,
				'kode_area'           => $kode_area,
    		)
    	);
    	$this->db->insert('tm_pv');
    }
      function insertpvitemkbout($ipv,$iarea,$icoa,$ecoaname,$vpv,$edescription,$ikk,$ipvtype,$iareax,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	            => $iarea,
				'i_pv'	            	=> $ipv,
				'i_coa'              	=> $icoa,
				'e_coa_name'	        => $ecoaname,
				'v_pv'		            => $vpv,
				'e_remark'    	      => $edescription,
				'i_kk'                => $ikk,
				'i_pv_type'           => $ipvtype,
				'kode_area'           => $kode_area,
				'i_area_kb'           => $iareax,
				
    		)
    	);
    	$this->db->insert('tm_pv_item');
    }
    function namaacc($icoa)
    {
		$this->db->select(" nama from tm_coa where kode='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->nama;
			
			}
			return $xxx;
		}
  }
   function idacc($icoa)
    {
		$this->db->select(" id from tm_coa where kode='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{

				$xxxid=$tmp->id;
			}
			return $xxxid;
			
			
		}
  }
  function runningnumberbankmasuk($th,$bl,$iarea,$icoabank)
	{
		$this->db->select(" max(substr(i_kbank,9,5)) as max 
		from tt_kas_bank 
		                    where substr(i_kbank,4,4)='$th' and substr(i_kbank,8,2)='$bl' and i_coa_bank='$icoabank'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="BM-".$th.$bl."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="BM-".$th.$bl."-".$nogj;
			return $nogj;
		}
    }
    function insertx($iareax,$ikbank,$iperiode,$icoa,$vkb,$dkb,$ecoaname,$edescription,$fdebet,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	            => $iareax,
				'i_kbank'	            => $ikbank,				
				'i_periode'         	=> $iperiode,
				'i_coa'	            	=> $icoa,
				'v_bank'	            => $vkb,
				'v_sisa'	            => $vkb,
				'd_bank'	            => $dkb,
				'e_coa_name'	        => $ecoaname,
				'e_description'	      => $edescription,
				'd_entry'           	=> $dentry,
				'f_debet'	            => $fdebet,
				'i_coa_bank'          => $icoabank
    		)
    	);
    	$this->db->insert('tt_kas_bank');
    }
    function runningnumberrvb($th,$bl,$icoabank,$iarea)
	{
		$this->db->select("  max(substr(i_rvb,15,8)) as max 
		                    from tm_rvb 
		                    where substr(i_rvb,4,4)='$th' and substr(i_rvb,8,2)='$bl' and i_area='$iarea'
		                     and i_coa_bank='$icoabank'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$norvb  =$terakhir+1;
			settype($norvb,"string");
			$a=strlen($norvb);
			while($a<6){
			  $norvb="0".$norvb;
			  $a=strlen($norvb);
			}
			$norvb  ="RV-".$th.$bl."-".$iarea.$icoabank.$norvb;
			return $norvb;
		}else{
			$norvb  ="000001";
			$norvb  ="RV-".$th.$bl."-".$iarea.$icoabank.$norvb;
			return $norvb;
		}
  }
  function runningnumberrv($th,$bl,$iarea,$irvtype)
	{
			$this->db->select ("kode_area FROM tm_area where id='$iarea'  ");
	$query2= $this->db->get();
		if($query2->num_rows() > 0)
		{
			$hasilrow2=$query2->row();
			$kode_area=$hasilrow2->kode_area;
			}
		
		$this->db->select(" max(substr(i_rv,15,8)) as max 
		                    from tm_rv 
		                    where substr(i_rv,4,4)='$th' and substr(i_rv,8,2)='$bl' and i_area='$iarea'
		                    and i_rv_type='$irvtype'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$norv  =$terakhir+1;
			settype($norv,"string");
			$a=strlen($norv);
			while($a<6){
			  $norv="0".$norv;
			  $a=strlen($norv);
			}
			$norv  ="RV-".$th.$bl."-".$kode_area.$irvtype.$norv;
			return $irv[]=array('norv'=>$norv,'kode_area'=>$kode_area
						);
			//return $norv;
		}else{
			$norv  ="000001";
			$norv  ="RV-".$th.$bl."-".$kode_area.$irvtype.$norv;
			return $irv[]=array('norv'=>$norv,'kode_area'=>$kode_area
						);
			//return $norv;
		}
  }
    function insertrvbm($irv,$iarea,$iperiode,$icoa,$drv,$tot,$eremark,$irvtype,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_rv'	            	=> $irv,
				'i_area'	            => $iarea,
				'kode_area'	            => $kode_area,
				'i_periode'         	=> $iperiode,
				'i_coa' 	            => $icoa,
				'd_rv'		            => $drv,
				'v_rv'		            => $tot,
				'd_entry'           	=> $dentry,
				'i_rv_type'           => $irvtype
    		)
    	);
    	$this->db->insert('tm_rv');
    }
    function insertrvitembm($irv,$iarea,$icoa,$ecoaname,$vrv,$edescription,$ikk,$irvtype,$iareax,$icoabank,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	            => $iarea,
				'i_rv'	            	=> $irv,
				'i_coa'              	=> $icoa,
				'e_coa_name'	        => $ecoaname,
				'v_rv'		            => $vrv,
				'e_remark'    	      => $edescription,
				'i_kk'                => $ikk,
				'i_rv_type'           => $irvtype,
				'i_area_kb'           => $iareax,
				'kode_area'           => $kode_area,
				'i_coa_bank'          => $icoabank
    		)
    	);
    	$this->db->insert('tm_rv_item');
    }
     function insertrvbbm($irvb,$icoabank,$irv,$iarea,$irvtype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_rvb'               => $irvb,
				'i_coa_bank' 	        => $icoabank,
				'i_rv'	            	=> $irv,
				'i_area'	            => $iarea,
				'i_rv_type'           => $irvtype,
				'd_entry'           	=> $dentry,
    		)
    	);
    	$this->db->insert('tm_rvb');
    }
    function runningnumberpvb($th,$bl,$icoabank,$iarea)
	{
		$this->db->select(" max(substr(i_pvb,15,8)) as max 
		                    from tm_pvb 
		                    where substr(i_pvb,4,4)='$th' and substr(i_pvb,8,2)='$bl' and i_coa_bank='$icoabank'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nopvb  =$terakhir+1;
			settype($nopvb,"string");
			$a=strlen($nopvb);
			while($a<6){
			  $nopvb="0".$nopvb;
			  $a=strlen($nopvb);
			}
			$nopvb  ="PV-".$th.$bl."-".$iarea.$icoabank.$nopvb;
			return $nopvb;
		}else{
			$nopvb  ="000001";
			$nopvb  ="PV-".$th.$bl."-".$iarea.$icoabank.$nopvb;
			return $nopvb;
		}
  }
  function runningnumberpv($th,$bl,$iarea,$ipvtype)
	{
		$this->db->select ("kode_area FROM tm_area where id='$iarea'  ");
	$query2= $this->db->get();
		if($query2->num_rows() > 0)
		{
			$hasilrow2=$query2->row();
			$kode_area=$hasilrow2->kode_area;
			}
			
		$this->db->select(" max(substr(i_pv,15,8)) as max 
		                    from tm_pv 
		                    where substr(i_pv,4,4)='$th' and substr(i_pv,8,2)='$bl' and i_area='$iarea'
		                    and i_pv_type='$ipvtype'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nopv  =$terakhir+1;
			settype($nopv,"string");
			$a=strlen($nopv);
			while($a<6){
			  $nopv="0".$nopv;
			  $a=strlen($nopv);
			}
			$nopv  ="PV-".$th.$bl."-".$iarea.$ipvtype.$nopv;
				return $ipv[]=array('nopv'=>$nopv,'kode_area'=>$kode_area
						);
		}else{
			$nopv  ="000001";
			$nopv  ="PV-".$th.$bl."-".$iarea.$ipvtype.$nopv;
				return $ipv[]=array('nopv'=>$nopv,'kode_area'=>$kode_area
						);
		}
  }
    function insertpvitem($ipv,$iarea,$icoa,$ecoaname,$vpv,$edescription,$idkk,$ipvtype,$iareax,$icoabank,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	          => $iarea,
				'i_pv'	              => $ipv,
				'i_coa'               => $icoa,
				'e_coa_name'	      => $ecoaname,
				'v_pv'		          => $vpv,
				'e_remark'    	      => $edescription,
				'i_kk'                => $idkk,
				'i_pv_type'           => $ipvtype,
				'i_area_kb'           => $iareax,
				'i_coa_bank'          => $icoabank,
				'kode_area'          => $kode_area
    		)
    	);
    	$this->db->insert('tm_pv_item');
    }
       function insertpvitemkk($ipv,$iarea,$icoa,$ecoaname,$vpv,$edescription,$idkk,$ipvtype,$iareax,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	          => $iarea,
				'i_pv'	              => $ipv,
				'i_coa'               => $icoa,
				'e_coa_name'	      => $ecoaname,
				'v_pv'		          => $vpv,
				'e_remark'    	      => $edescription,
				'i_kk'                => $idkk,
				'i_pv_type'           => $ipvtype,
				'i_area_kb'           => $iareax,
			
				'kode_area'          => $kode_area
    		)
    	);
    	$this->db->insert('tm_pv_item');
    }
      function insertpv($ipv,$iarea,$iperiode,$icoa,$dpv,$tot,$eremark,$ipvtype,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_pv'	            	=> $ipv,
				'i_area'	            => $iarea,
				'i_periode'         	=> $iperiode,
				'i_coa' 	            => $icoa,
				'd_pv'		            => $dpv,
				'v_pv'		            => $tot,
				'd_entry'           	=> $dentry,
				'i_pv_type'             => $ipvtype,
				'kode_area'          => $kode_area
    		)
    	);
    	$this->db->insert('tm_pv');
    }
     function insertpvb($ipvb,$icoabank,$ipv,$iarea,$ipvtype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_pvb'               => $ipvb,
				'i_coa_bank' 	        => $icoabank,
				'i_pv'	            	=> $ipv,
				'i_area'	            => $iarea,
				'i_pv_type'           => $ipvtype,
				'd_entry'           	=> $dentry,
    		)
    	);
    	$this->db->insert('tm_pvb');
    }
      function insertkk($iareax,$ikk,$iperiode,$icoa,$vkk,$dkk,$ecoaname,$edescription,$fdebet,$id_coa,$bulan,$tahun)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'id_area'	            => $iareax,
				'no_transaksi'	            	=> $ikk,				
				'd_periode'         	=> $iperiode,
				'bulan'	            	=> $bulan,				
				'tahun'         	=> $tahun,
				'kode_coa'	            	=> $icoa,
				'id_coa'	            	=> $id_coa,
				'jumlah'		            => $vkk,
				'tgl'		            => $dkk,
				'nama_coa'	        => $ecoaname,
				'deskripsi'	        => $edescription,
				'tgl_input'           	=> $dentry,
				'tgl_update'           	=> $dentry,
				'is_debet'	            => $fdebet,
    		)
    	);
    	$this->db->insert('tt_kas_kecil');
    }
    
       function insertrv($irv,$iarea,$iperiode,$icoa,$drv,$tot,$eremark,$irvtype,$kode_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_rv'	            	=> $irv,
				'i_area'	            => $iarea,
				'i_periode'         	=> $iperiode,
				'i_coa' 	            => $icoa,
				'd_rv'		            => $drv,
				'v_rv'		            => $tot,
				'd_entry'           	=> $dentry,
				'i_rv_type'           => $irvtype,
					'kode_area'           => $kode_area
    		)
    	);
    	$this->db->insert('tm_rv');
    }
     function insertrvitem( $irv,$iarea,$icoabank,$ecoaname,$vrv,$ireff,$ikodebm,$irvtype,$iareax,$icoa,$kode_area,$id_area)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	          => $iarea,
				'i_rv'	              => $irv,
				'i_coa'               => $icoa,
				'e_coa_name'	      => $ecoaname,
				'v_rv'		          => $vrv,
				'e_remark'    	      => $ireff,
				'i_kk'                => $ikodebm,
				'i_rv_type'           => $irvtype,
				'i_area_kb'           => $iareax,
				'kode_area'           => $kode_area,
				'i_coa_bank'          => $icoabank,
				'i_area_kb'          => $id_area
    		)
    	);
    	$this->db->insert('tm_rv_item');
    }
     function insertkb($iareax,$ikb,$iperiode,$icoa,$vkb,$dbukti,$ecoaname,$edescription,$fdebet,$id_coa)
     {
		 $seqxx	= $this->db->query(" SELECT id FROM tt_kas_besar ORDER BY id DESC LIMIT 1 ");
		if($seqxx->num_rows() > 0) {
			$seqrowxx	= $seqxx->row();
			$id_kb	= $seqrowxx->id+1;
		}else{
			$id_kb	= 1;
		}
		 
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->set(
    		array(
				'id_area'	            => $iareax,
				'id_coa'	            => $id_coa,
				'no_transaksi'	            	=> $ikb,				
				'd_periode'         	=> $iperiode,
				'kode_coa'	            	=> $icoa,
				'd_bukti'                => $dbukti,
				'jumlah'		            => $vkb,
				'tgl'		            => $dbukti,
				'nama_coa'	        => $ecoaname,
				'deskripsi'	     	 => $edescription,
				'tgl_input'           	=> $dentry,
				'tgl_update'           	=> $dentry,
				'id'           	=> $id_kb,
				'is_debet'	            => $fdebet
    		)
    	);
    	$this->db->insert('tt_kas_besar');
      } 
}
