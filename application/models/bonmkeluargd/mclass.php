<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function get_nomorbonmkeluar() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_outbonm_code AS character varying),5,(LENGTH(cast(i_outbonm_code AS character varying))-4)) AS icode FROM tm_outbonm WHERE f_outbonm_cancel='f' ORDER BY i_outbonm_code DESC LIMIT 1 ");
	}

	function get_thnbonmkeluar() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_outbonm_code AS character varying),1,4) AS thn FROM tm_outbonm WHERE f_outbonm_cancel='f' ORDER BY i_outbonm DESC LIMIT 1 ");
	}
	
	function lkepada() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY i_to_outbonm ASC, e_to DESC ";
		$db2->select(" a.i_to_outbonm AS i, a.e_to AS to FROM tr_to_outbomm a ".$order." ",false);
		
		$query	= $db2->get();
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		} else {
			return false;
		}
	}
	
	
	function msimpan($e_to_outbonm,$i_outbonm,$d_outbonm,$i_product,$e_product_name,
				$qty_warna, $i_product_color, $i_color, $n_count_product,$qty_product,$iteration,$f_stp,$iso) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$i_outbonm_item	= array();
		$qty_product_update	= array();
		$is2	= array();
		$ins_temp_tmso	= array();
		$tmoutbonmkeluar_item	= array();
		$ncountproduct	= array();
		
		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query( " SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date " );
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$seq_tm_outbonm	= $db2->query( " SELECT cast(i_outbonm AS integer) AS ioutbonm FROM tm_outbonm ORDER BY cast(i_outbonm AS integer) DESC LIMIT 1 " );
		if($seq_tm_outbonm->num_rows() > 0 ) {
			$seqrow	= $seq_tm_outbonm->row();
			$ioutbonm	= $seqrow->ioutbonm+1;
		} else {
			$ioutbonm	= 1;
		}

		$s	= array();
		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
			$s[$jumlah]	= $iso[$jumlah];
			if($s[$jumlah]!=""){
				$ss	= $s[$jumlah];
				break;
			}	
		}

		$test	= $db2->set(
			array(
			 'i_outbonm'=>$ioutbonm,
			 'i_outbonm_code'=>$i_outbonm,
			 'e_to_outbonm'=>$e_to_outbonm,
			 'd_outbonm'=>$d_outbonm,
			 'f_outbonm_cancel'=>'FALSE',
			 'd_entry'=>$dentry));
			 		 
		if(isset($iteration)){	
			
			$db2->insert('tm_outbonm');
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

				$qstokopname	= $db2->query(" SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1 ");
				
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				}else{
					$isoz	= $iso[$jumlah];
				}
				
				//$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];
				
				$seq_tm_outbonm_item	= $db2->query(" SELECT cast(i_outbonm_item AS integer) AS i_outbonm_item FROM tm_outbonm_item ORDER BY cast(i_outbonm_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_outbonm_item->num_rows() > 0) {
					$seqrow	= $seq_tm_outbonm_item->row();
					$i_outbonm_item[$jumlah]	= $seqrow->i_outbonm_item+1;
				}else{
					$i_outbonm_item[$jumlah]	= 1;
				}
				
				//-------------- 23-10-2014 hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($i_color[$jumlah]); $xx++) {

					$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
					$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
					$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
													
					$qtytotal+= $qty_warna[$jumlah][$xx];
				} // end for
				// ---------------------------------- END -----------------------------------

				//$qty_product_update[$jumlah]	= $qty_product[$jumlah]-$ncountproduct[$jumlah];
				
				$tmoutbonmkeluar_item[$jumlah]	= array(
					 'i_outbonm_item'=>$i_outbonm_item[$jumlah],
					 'i_outbonm'=>$ioutbonm,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 //'n_count_product'=>$ncountproduct[$jumlah],
					 'n_count_product'=>$qtytotal,
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah]);
				
				if($db2->insert('tm_outbonm_item',$tmoutbonmkeluar_item[$jumlah])) {
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
					
					if($qstok_item->num_rows()>0) {
						
						$row_stok_item	= $qstok_item->row();
						$i_so_item = $row_stok_item->i_so_item;
						
						//$back_qty_akhir[$jumlah]	= ($row_stok_item->n_quantity_akhir)-$ncountproduct[$jumlah];
						$back_qty_akhir[$jumlah]	= $row_stok_item->n_quantity_akhir-$qtytotal;
						
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz,'i_product'=>$i_product[$jumlah]));
					}
					
					// ------------------------------------------ 23-10-2014
					for ($xx=0; $xx<count($i_color[$jumlah]); $xx++) {
						
						//if (trim($row1) != '') {
							$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
							$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
							$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
							
							$seq_tm_outbonm_item_color	= $db2->query(" SELECT i_outbonm_item_color FROM tm_outbonm_item_color 
														ORDER BY i_outbonm_item_color DESC LIMIT 1 ");
						
							if($seq_tm_outbonm_item_color->num_rows() > 0) {
								$seqrow	= $seq_tm_outbonm_item_color->row();
								$i_outbonm_item_color[$jumlah]	= $seqrow->i_outbonm_item_color+1;
							}else{
								$i_outbonm_item_color[$jumlah]	= 1;
							}

							$tm_bonmkeluar_item_color[$jumlah]	= array(
								 'i_outbonm_item_color'=>$i_outbonm_item_color[$jumlah],
								 'i_outbonm_item'=>$i_outbonm_item[$jumlah],
								 'i_product_color'=>$i_product_color[$jumlah][$xx],
								 'i_color'=>$i_color[$jumlah][$xx],
								 'qty'=>$qty_warna[$jumlah][$xx]
							);
							$db2->insert('tm_outbonm_item_color',$tm_bonmkeluar_item_color[$jumlah]);
							
							// 06-10-2014, update stok per warna
								$sqlwarna = " SELECT * FROM tm_stokopname_item_color WHERE i_so_item = '$i_so_item'
											AND i_color = '".$i_color[$jumlah][$xx]."' ";
								$querywarna = $db2->query($sqlwarna);
								
								if($querywarna->num_rows() > 0) {
									$hasilwarna = $querywarna->row();
									
									$qtyawalwarna	= $hasilwarna->n_quantity_awal;
									$qtyakhirwarna	= $hasilwarna->n_quantity_akhir;

									$back_qty_awalwarna	= $qtyawalwarna;
									$back_qty_akhirwarna	= $qtyakhirwarna-$qty_warna[$jumlah][$xx];
									
									if($back_qty_akhirwarna=="")
										$back_qty_akhirwarna	= 0;
																			
									$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir = '$back_qty_akhirwarna'
														WHERE i_so_item = '$i_so_item' AND i_color = '".$i_color[$jumlah][$xx]."' ");
									
								} // end if
							
						//} // end if
					} // end for
					 
					if($db2->trans_status()===FALSE || $db2->trans_status()==FALSE) {
						$db2->trans_rollback();
					}else{
						$db2->trans_commit();
					}
					
				}else{
				}
			}
		}else{
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");show(\"bonmkeluar/cform\",\"#content\");</script>";
		}
				
		$qbonmkeluar	= $db2->query(" SELECT * FROM tm_outbonm WHERE i_outbonm='$ioutbonm' ");
		
		if($qbonmkeluar->num_rows()==0) {
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");show(\"bonmkeluar/cform\",\"#content\");</script>";
		}else{
			print "<script>alert(\"Nomor Bon M Keluar : '\"+$i_outbonm+\"' telah disimpan, terimakasih.\");show(\"bonmkeluar/cform\",\"#content\");</script>";
		}

	}

	function msimpan_old($e_to_outbonm,$i_outbonm,$d_outbonm,$i_product,$e_product_name,$n_count_product,$qty_product,$iteration,$f_stp,$iso) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$i_outbonm_item	= array();
		$qty_product_update	= array();
		$is2	= array();
		$ins_temp_tmso	= array();
		$tmoutbonmkeluar_item	= array();
		$ncountproduct	= array();
		
		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query( " SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date " );
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		/* 08082011
		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$isoz	= $row_stokopname->i_so;
		} else {
			$isoz	= 0;
		}
		*/
		
		$seq_tm_outbonm	= $db2->query( " SELECT cast(i_outbonm AS integer) AS ioutbonm FROM tm_outbonm ORDER BY cast(i_outbonm AS integer) DESC LIMIT 1 " );
		if($seq_tm_outbonm->num_rows() > 0 ) {
			$seqrow	= $seq_tm_outbonm->row();
			$ioutbonm	= $seqrow->ioutbonm+1;
		} else {
			$ioutbonm	= 1;
		}

		$s	= array();
		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
			$s[$jumlah]	= $iso[$jumlah];
			if($s[$jumlah]!=""){
				$ss	= $s[$jumlah];
				break;
			}	
		}
		/*		
		$test	= $db2->set(
			array(
			 'i_outbonm'=>$ioutbonm,
			 'i_outbonm_code'=>$i_outbonm,
			 'e_to_outbonm'=>$e_to_outbonm,
			 'd_outbonm'=>$d_outbonm,
			 'f_outbonm_cancel'=>'FALSE',
			 'd_entry'=>$dentry,
			 'i_so'=>$ss ) );
		 */
		$test	= $db2->set(
			array(
			 'i_outbonm'=>$ioutbonm,
			 'i_outbonm_code'=>$i_outbonm,
			 'e_to_outbonm'=>$e_to_outbonm,
			 'd_outbonm'=>$d_outbonm,
			 'f_outbonm_cancel'=>'FALSE',
			 'd_entry'=>$dentry));
			 		 
		//if($db2->insert('tm_outbonm') ) {
		if(isset($iteration)){	
			
			$db2->insert('tm_outbonm');
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

				//$qstokopname	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1 ");
				$qstokopname	= $db2->query(" SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1 ");
				
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				}else{
					$isoz	= $iso[$jumlah];
				}
				
				$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];
				
				$seq_tm_outbonm_item	= $db2->query(" SELECT cast(i_outbonm_item AS integer) AS i_outbonm_item FROM tm_outbonm_item ORDER BY cast(i_outbonm_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_outbonm_item->num_rows() > 0) {
					$seqrow	= $seq_tm_outbonm_item->row();
					$i_outbonm_item[$jumlah]	= $seqrow->i_outbonm_item+1;
				}else{
					$i_outbonm_item[$jumlah]	= 1;
				}

				$qty_product_update[$jumlah]	= $qty_product[$jumlah]-$ncountproduct[$jumlah];
				
				$tmoutbonmkeluar_item[$jumlah]	= array(
					 'i_outbonm_item'=>$i_outbonm_item[$jumlah],
					 'i_outbonm'=>$ioutbonm,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$ncountproduct[$jumlah],
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah]);
				
				if($db2->insert('tm_outbonm_item',$tmoutbonmkeluar_item[$jumlah])) {
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
					
					if($qstok_item->num_rows()>0) {
						
						$row_stok_item	= $qstok_item->row();
						//$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
						//$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;

						//$back_qty_awal[$jumlah]	= $qtyawal[$jumlah];
						//$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]-$ncountproduct[$jumlah];
						
						$back_qty_akhir[$jumlah]	= ($row_stok_item->n_quantity_akhir)-$ncountproduct[$jumlah];
						
						/***
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_awal'=>$back_qty_awal[$jumlah],
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						***/
						
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz,'i_product'=>$i_product[$jumlah]));
					}
					
					$get_tmso	= $db2->query( " SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.i_status_do='1' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
					
					if($get_tmso->num_rows() > 0 ) {
					
						$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						if($get_tmso2->num_rows() > 0 ) {
							$row_tmso2	= $get_tmso2->row();
							$is2[$jumlah]	= $row_tmso2->iso+1;
						}else{
							$is2[$jumlah]	= 1;
						}
						
						$row_tmso	= $get_tmso->row_array();
						
						/* 29072011
						$temp_iso[$jumlah]	= $row_tmso['i_so'];
						$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
						$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
						$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
						$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
						$temp_ddo[$jumlah]	= $row_tmso['d_do'];
						$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
						$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
						$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
						$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
						$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
						*/
						
						$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] - $ncountproduct[$jumlah];
						$tambah_outbonm[$jumlah]	= $row_tmso['n_outbonm'] + $ncountproduct[$jumlah];
						
						if($row_tmso['n_saldo_awal']==''){
							$nsaldoakhir = 0;
						}else{
							$nsaldoakhir = $row_tmso['n_saldo_awal'];
						}
						
						if($saldo_akhir[$jumlah]=='')
							$saldo_akhir[$jumlah] = 0;
						
						if($tambah_outbonm[$jumlah]=='')
							$tambah_outbonm[$jumlah] = 0;

						if($row_tmso['n_inbonm']==''){
							$ninbonm2	= 0;	
						}else{
							$ninbonm2	= $row_tmso['n_inbonm'];
						}
							
						if($row_tmso['n_bbm']==''){
							$nbbm2	= 0;
						}else{
							$nbbm2	= $row_tmso['n_bbm'];
						}
							
						if($row_tmso['n_bbk']==''){
							$nbbk2	= 0;
						}else{
							$nbbk2	= $row_tmso['n_bbk'];
						}
													
						$ins_temp_tmso[$jumlah]	= array(
							'i_so' => $is2[$jumlah],
							'i_product' => $row_tmso['i_product'],
							'i_product_motif' => $row_tmso['i_product_motif'],
							'e_product_motifname' => $row_tmso['e_product_motifname'],
							'i_status_do' => $row_tmso['i_status_do'],
							'd_do' => $row_tmso['d_do'],
							'n_saldo_awal' => $nsaldoakhir,
							'n_saldo_akhir' => $saldo_akhir[$jumlah],
							'n_inbonm' => $ninbonm2,
							'n_outbonm' => $tambah_outbonm[$jumlah],
							'n_bbm' => $nbbm2,
							'n_bbk' => $nbbk2,
							'd_entry' => $dentry );
						
						$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						
					}else{
						$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
						
						if($get_tmso->num_rows()>0) {
							
							$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
							
							if($get_tmso2->num_rows() > 0) {
								$row_tmso2	= $get_tmso2->row();
								$is2[$jumlah]	= $row_tmso2->iso+1;
							}else{
								$is2[$jumlah]	= 1;
							}
							
							$row_tmsoXx	= $get_tmso->row_array();
						
							/* 29072011
							$temp_iso[$jumlah]	= $row_tmsoXx['i_so'];
							$temp_iproduct[$jumlah]	= $row_tmsoXx['i_product'];
							$temp_imotif[$jumlah]	= $row_tmsoXx['i_product_motif'];
							$temp_productname[$jumlah]	= $row_tmsoXx['e_product_motifname'];
							$temp_istatusdo[$jumlah]	= $row_tmsoXx['i_status_do'];
							$temp_ddo[$jumlah]	= $row_tmsoXx['d_do'];
							$temp_saldo_awal[$jumlah]	= $row_tmsoXx['n_saldo_awal'];
							$temp_saldo_akhir[$jumlah]	= $row_tmsoXx['n_saldo_akhir'];
							$temp_n_inbonm[$jumlah]	= $row_tmsoXx['n_inbonm'];
							$temp_n_outbonm[$jumlah]	= $row_tmsoXx['n_outbonm'];
							$temp_n_bbm[$jumlah]	= $row_tmsoXx['n_bbm'];
							$temp_n_bbk[$jumlah]	= $row_tmsoXx['n_bbk'];
							*/
							
							if($row_tmsoXx['n_saldo_akhir']==''){
								$nsaldoakhir	= 0;
							}else{
								$nsaldoakhir	= $row_tmsoXx['n_saldo_akhir'];
							}
							
							$saldo_akhir[$jumlah]	= $row_tmsoXx['n_saldo_akhir'] - $ncountproduct[$jumlah];
							$tambah_outbonm[$jumlah]	= $row_tmsoXx['n_outbonm'] + $ncountproduct[$jumlah];
							 
							if($saldo_akhir[$jumlah]=='')
								$saldo_akhir[$jumlah]	= 0;
							
							if($tambah_outbonm[$jumlah]=='')
								$tambah_outbonm[$jumlah] = 0;
							
							if($row_tmsoXx['n_inbonm']==''){
								$ninbonm2	= 0;	
							}else{
								$ninbonm2	= $row_tmsoXx['n_inbonm'];
							}
							
							if($row_tmsoXx['n_bbm']==''){
								$nbbm2	= 0;
							}else{
								$nbbm2	= $row_tmsoXx['n_bbm'];
							}
							
							if($row_tmsoXx['n_bbk']==''){
								$nbbk2	= 0;
							}else{
								$nbbk2	= $row_tmsoXx['n_bbk'];
							}
							
							$ins_temp_tmso[$jumlah]	= array(
								'i_so' => $is2[$jumlah],
								'i_product' => $row_tmsoXx['i_product'],
								'i_product_motif' => $row_tmsoXx['i_product_motif'],
								'e_product_motifname' => $row_tmsoXx['e_product_motifname'],
								'i_status_do' => '1',
								'd_do' => $row_tmsoXx['d_do'],
								'n_saldo_awal' => $nsaldoakhir,
								'n_saldo_akhir' => $saldo_akhir[$jumlah],
								'n_inbonm' => $ninbonm2,
								'n_outbonm' => $tambah_outbonm[$jumlah],
								'n_bbm' => $nbbm2,
								'n_bbk' => $nbbk2,
								'd_entry' => $dentry );
							
							$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);

						}else{
							/* Hapus data pd tabel tm_outbonm dgn ID :
							i_outbonm = $ioutbonm,
							
							Hapus data pd tabel tm_outbonm_item dgn :
							i_outbonm_item = $i_outbonm_item[$jumlah],
							i_outbonm = $i_outbonm,
							*/
							$db2->delete('tm_outbonm',array('i_outbonm'=>$ioutbonm));
							$db2->delete('tm_outbonm_item',array('i_outbonm'=>$i_outbonm));
						}						
					}

					/* Disabled 06122010
					$arrmotifupdate	= array(
						'n_quantity'=>$qty_product_update[$jumlah]
						);
					$db2->update('tr_product_motif', $arrmotifupdate, array('i_product_motif' => $i_product[$jumlah]));
					*/
					 
					if($db2->trans_status()===FALSE) {
						$db2->trans_rollback();
					}else{
						$db2->trans_commit();
					}
					
				}else{
					/* Hapus data pd tabel tm_inbonm dgn ID :
					i_inbonm = $inbonm,
					*/
					$db2->delete('tm_outbonm',array('i_outbonm'=>$ioutbonm));
				}
			}
			/***
			print "<script>alert(\"Nomor Bon M Keluar : '\"+$i_outbonm+\"' telah disimpan, terimakasih.\");show(\"bonmkeluar/cform\",\"#content\");</script>";
			***/
		}else{
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");show(\"bonmkeluar/cform\",\"#content\");</script>";
		}
				
		$qbonmkeluar	= $db2->query(" SELECT * FROM tm_outbonm WHERE i_outbonm='$ioutbonm' ");
		
		if($qbonmkeluar->num_rows()<0 || $qbonmkeluar->num_rows()==0) {
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal disimpan. Terimakasih.\");show(\"bonmkeluar/cform\",\"#content\");</script>";
		}else{
			print "<script>alert(\"Nomor Bon M Keluar : '\"+$i_outbonm+\"' telah disimpan, terimakasih.\");show(\"bonmkeluar/cform\",\"#content\");</script>";
		}

		/*
		redirect('bonmkeluar/cform/'); */
	}
	
		
	function lbarangjadiperpages($limit,$offset,$bulan,$tahun) {
	$db2=$this->load->database('db_external', TRUE);
	
	//function lbarangjadi($tgl) {
		/*
		$query = $db2->query(" 
			SELECT  a.i_product_base AS iproduct,
				a.e_product_basename AS productname,
				b.i_product_motif AS imotif,
				b.e_product_motifname AS motifname,
				b.n_quantity AS qty
				
			FROM tr_product_base a 
			
			RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product	
		");
		*/
		
		/*
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND d.d_so = '$tgl%' ORDER BY b.i_product_motif DESC			
		*/
		
		/* Disabled 07-02-2011
		$strqry	= ( " SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false 
				
				ORDER BY b.i_product_motif DESC "." LIMIT ".$limit." OFFSET ".$offset);
		*/

		$strqry	= (" SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan'
				
				ORDER BY b.i_product_motif DESC "." LIMIT ".$limit." OFFSET ".$offset);
								
		$query = $db2->query($strqry);
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	

	function lbarangjadi($bulan,$tahun) {
	$db2=$this->load->database('db_external', TRUE);
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false 
				
				ORDER BY b.i_product_motif DESC	
		");
		*/
		return $db2->query("
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan'
				
				ORDER BY b.i_product_motif DESC ");
	}

	function flbarangjadi($key,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND a.f_stop_produksi=false AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') ORDER BY b.i_product_motif DESC	
		");
		*/
		return $db2->query( "
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') 
				
				ORDER BY b.i_product_motif DESC	
		");		
	}

	function lbarangjadiperpagesopsi($limit,$offset,$bulan,$tahun) {
$db2=$this->load->database('db_external', TRUE);
		$strqry	= (" SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif DESC "." LIMIT ".$limit." OFFSET ".$offset);
								
		$query = $db2->query($strqry);
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadiopsi($bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' ORDER BY b.i_product_motif DESC ");
	}

	function flbarangjadiopsi($key,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		return $db2->query( "
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') ORDER BY b.i_product_motif DESC	
		");		
	}
 
	function caribonmkeluar($nbonmkeluar) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_outbonm WHERE i_outbonm_code=trim('$nbonmkeluar') AND f_outbonm_cancel='f' ");
	}
}
?>
