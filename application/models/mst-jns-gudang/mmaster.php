<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_jenis_gudang');
    $this->db->order_by('kode_jenis','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_jenis_gudang',array('kode_jenis'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_jenis_gudang WHERE id = '$id' ");
    return $query->result();
  }
  
  //
  function save($kode,$id_jenis,$nama,$goedit){  
    $tgl = date("Y-m-d H:i:s");
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_jenis_gudang ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
		
    $data = array(
      'id'=>$idbaru,
      'kode_jenis'=>$kode,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_jenis_gudang',$data); }
	else {
		
		$data = array(
		  'kode_jenis'=>$kode,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_jenis);
		$this->db->update('tm_jenis_gudang',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_jenis_gudang', array('id' => $id));
  }
  
  function cek_data($kode){
    $this->db->select("kode_jenis from tm_jenis_gudang WHERE kode_jenis = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
