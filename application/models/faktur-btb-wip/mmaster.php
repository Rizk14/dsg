<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  function get_detail_sjmasukwip($list_brg, $unit_jahitnya){
    $detail_pp = array();

    foreach($list_brg as $row1) {
		
			if ($row1 != '') {
				
					$query2	= $this->db->query(" SELECT a.no_sj, b.id_sjmasukwip, b.id_brg_wip, b.qty, b.harga, b.diskon
											FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
											WHERE b.id = '$row1' ");
					$hasilrow = $query2->row();
					$id_sjmasukwip	= $hasilrow->id_sjmasukwip;
					$id_brg_wip	= $hasilrow->id_brg_wip;
					$qty	= $hasilrow->qty;
					//$harga	= $hasilrow->harga;
					$diskon	= $hasilrow->diskon;
					$no_sjmasukwip	= $hasilrow->no_sj;
									
				$query2	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$id_brg_wip' ");
				$hasilrow = $query2->row();
				$kode_brg_wip	= $hasilrow->kode_brg;
				$nama_brg_wip	= $hasilrow->nama_brg;
					
				$qty_sjmasukwip = $qty;
				
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_wip_detail WHERE id_sjmasukwip_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_wip_detail a INNER JOIN tm_pembelian_wip b ON a.id_pembelian_wip = b.id
										WHERE a.id_sjmasukwip_detail = '$row1' AND b.status_aktif = 't' 
										AND a.id_brg_wip = '".$id_brg_wip."' ");
						
						if($query3->num_rows()>0) {
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum;
						}
						else
							$jum_beli = 0;
					}
					else {
						$jum_beli = 0;
					}
					
			$query5 =$this->db->query("SELECT harga from tm_harga_brg_unit_jahit where id_brg='$id_brg_wip' and id_unit_jahit='$unit_jahitnya'");
			if($query5->num_rows() > 0){
				$hasilrow=$query5->row();
				$harga=$hasilrow->harga;
				
			}
			else
			{
				$harga=0;
			}
				$jum_sjmasukwip = $jum_beli;
					
				$qty = $qty-$jum_sjmasukwip;
			
				$qty = number_format($qty, 4, '.','');

					$detail_sjmasukwip[] = array(	'id'=> $row1,
											'id_brg_wip'=> $id_brg_wip,
											'kode_brg_wip'=> $kode_brg_wip,
											'nama_brg_wip'=> $nama_brg_wip,
											'qty'=> $qty,
											'harga'=> $harga,
											'diskon'=> $diskon,
											'qty_sjmasukwip'=> $qty_sjmasukwip,
											'jum_beli'=> $jum_beli,
											'no_sjmasukwip'=> $no_sjmasukwip,
											'id_sjmasukwip'=> $id_sjmasukwip
									);
				
		}
	}
	return $detail_sjmasukwip;
}
 function get_unit_jahit(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit ");    
    return $query->result();  
  }  
  
    function get_sjmasukwiptanpalimit($id_ujh, $keywordcari, $tgldari, $tglke ,$js_msk){
		
		$pencarian1='';
		$pencarian2='';
		$pencarian3='';
		$pencarian11='';
		if ($tgldari != '0000-00-00'){
		
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		
		if ($tglke != '0000-00-00'){	
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}
		if($js_msk !=0){
		$pencarian3.=	"AND jenis_masuk='$js_msk'";
			}
		if($js_msk !=0){
		$pencarian3.=	"AND jenis_masuk='$js_msk'";
			}
	
	if($id_ujh != 0){
		$pencarian11.= "AND id_unit_jahit = '$id_ujh'";
		}
		
		
		
	if ($keywordcari == "all") {
	
				$query	= $this->db->query(" SELECT * FROM tm_sjmasukwip WHERE status_sjmasukwip = 'f' AND status_aktif = 't'AND status_app_qc='t'
											 ".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3);
	}
	else {
		
				$query	= $this->db->query(" SELECT * FROM tm_sjmasukwip WHERE status_sjmasukwip = 'f' AND status_aktif = 't' AND status_app_qc='t'
											AND jenis_masuk='1' AND id_unit_jahit = '$id_ujh' 
											".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3.
							  "AND UPPER(no_sj) like UPPER('%$keywordcari%') ");
	}
    
    return $query->result();  
  }
  function get_sjmasukwip($id_ujh, $keywordcari, $tgldari, $tglke,$js_msk){
		$pencarian1='';
		$pencarian2='';
		$pencarian3='';
		$pencarian11='';
		if ($tgldari!= '0000-00-00'){
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		if ($tglke!= '0000-00-00'){
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}	
		
		if($js_msk !=0){
		$pencarian3.=	"AND jenis_masuk='$js_msk'";
			}
	
	if($id_ujh != 0){
		$pencarian11.= "AND a.id_unit_jahit = '$id_ujh'";
		}
		
	if ($keywordcari == "all") {
				$sql = " distinct a.* FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
						WHERE a.status_sjmasukwip = 'f' AND a.status_aktif = 't' AND a.status_app_qc='t'
						 ".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3.
					"AND b.status_sjmasukwip='f'  order by a.tgl_sj DESC, a.no_sj DESC ";
			$this->db->select($sql, false);
			$query = $this->db->get();
		
	}
	else {
				$sql = " distinct a.* FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_barang_wip c ON b.id_brg_wip = c.id
							WHERE a.status_sjmasukwip = 'f' AND a.status_aktif = 't' 
							  AND b.status_sjmasukwip='f' AND a.status_app_qc='t'
								".$pencarian11." ".$pencarian1." ".$pencarian2." ".$pencarian3."
							AND UPPER(no_sj) like UPPER('%$keywordcari%') 
							order by a.tgl_sj DESC, a.no_sj DESC ";
			$this->db->select($sql, false);
			$query = $this->db->get();
	
	}
	
		$data_sjmasukwip = array();
		$detail_sjmasukwip = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
					$filterbrg = "";
					if ($keywordcari != "all")
						$filterbrg = " AND UPPER(no_sj) like UPPER('%$keywordcari%') ";
					
					$sql2 = "SELECT b.* FROM tm_sjmasukwip_detail b INNER JOIN tm_sjmasukwip a ON b.id_sjmasukwip = a.id 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							WHERE b.id_sjmasukwip = '$row1->id' AND b.status_sjmasukwip = 'f' ".$filterbrg." ORDER BY b.id ASC ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip 
										WHERE id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
											
							$query4	= $this->db->query(" SELECT id FROM tm_pembelian_wip_detail WHERE id_sjmasukwip_detail = '".$row2->id."' ");
							if ($query4->num_rows() > 0){
						
									$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_wip_detail a 
											INNER JOIN tm_pembelian_wip b ON a.id_pembelian_wip = b.id WHERE b.status_aktif = 't' ";
									$sql3.= " AND a.id_sjmasukwip_detail = '$row2->id' AND a.id_brg_wip = '$row2->id_brg_wip'
												 ";
									
									$query3	= $this->db->query($sql3);
									$hasilrow = $query3->row();
									$jum_sjmasukwip = $hasilrow->jum; 
			
							}
							else {
								$jum_sjmasukwip = 0;
							}
						
						$qty = $row2->qty-$jum_sjmasukwip;
						
						
						if ($qty <= 0) {
							$this->db->query(" UPDATE tm_sjmasukwip_detail SET status_sjmasukwip = 't' WHERE id='".$row2->id."' ");
							
							$queryxx	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail WHERE id_sjmasukwip = '".$row1->id."' AND status_sjmasukwip = 'f' ");
							if ($queryxx->num_rows() == 0){
								$this->db->query(" UPDATE tm_sjmasukwip SET status_sjmasukwip = 't' WHERE id='".$row1->id."' ");
							}
						}
						//--------------------
						
						
							$nama_satuan_lain = '';
						
						if ($qty > 0) {
							$detail_sjmasukwip[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan
												
											);
						}
					}
				}
				else {
					$detail_sjmasukwip = '';
				}
				
				
					if ($row1->id_unit_jahit != '0') {
						$query3	= $this->db->query(" SELECT kode_unit, nama from tm_unit_jahit where id = '$row1->id_unit_jahit' ");
						$hasilrow = $query3->row();
						$kode_unit_jahit	= $hasilrow->kode_unit;
						$nama_unit_jahit	= $hasilrow->nama;
					}
					
				
				
				
					//$no_sjmasukwip = $row1->no_sjmasukwip;
					$no_sj = $row1->no_sj;
					//$tgl_sjmasukwip = $row1->tgl_sjmasukwip;
					$tgl_sj = $row1->tgl_sj;
				
				$data_sjmasukwip[] = array(	'id'=> $row1->id,	
											'no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'tgl_update'=> $row1->tgl_update,
											
											'detail_sjmasukwip'=> $detail_sjmasukwip
											);
				$detail_sjmasukwip = array();
			} // endforeach header
		}
		else {
			$data_sjmasukwip = '';
		}
		return $data_sjmasukwip;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function cek_data($no_sjmasukpembelian, $id_unit_jahit){
    $this->db->select("id from tm_pembelian_wip WHERE no_sjmasukpembelian = '$no_sjmasukpembelian' AND id_unit_jahit = '$id_unit_jahit' AND status_aktif = 't' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function save($no_sjmasukpembelian,$tgl_sjpembelian,$id_unit_jahit,$id_unit_jahitbaru, $gtotal, $asligtotal, $total_pajak, $dpp, 
						$uang_muka, $sisa_hutang,$ket,$hide_pkp, $hide_tipe_pajak, $lain_cash, $lain_kredit,$jenismasuk,
				$id_sjmasukwip_detail, $id_sjmasukwip2,
				$id_brg_wip, $nama, $id_satuan, $id_satuan_konversi, $qty, $harga, $harga_lama, $pajak, $diskon, $total, $aslitotal){  
    
    // 25-06-2015 $satuan_lain, $qty_sat_lain dihilangkan
    
    $tgl = date("Y-m-d H:i:s");
    
    if ($id_unit_jahit == '0')
		$id_ujh_forsave = $id_unit_jahitbaru;
	else
		$id_ujh_forsave = $id_unit_jahit;
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pembelian_wip WHERE no_sjmasukpembelian = '$no_sjmasukpembelian' AND id_unit_jahit = '$id_ujh_forsave' 
					AND status_aktif = 't' ", false);
    $query = $this->db->get();
    
	if ($query->num_rows() == 0){
			
				$no_faktur = NULL;
				$tgl_faktur = NULL;
				$status_faktur = 'f';
				$faktur_sj = 'f';
						
			
			// 27-08-2015
			$uid_update_by = $this->session->userdata('uid');
			
				$data_header = array(
				 
				  'no_sjmasukpembelian'=>$no_sjmasukpembelian,
				  'tgl_sjpembelian'=>$tgl_sjpembelian,
				  'no_faktur'=>$no_faktur,
				  'id_unit_jahit'=>$id_ujh_forsave,
				  'total'=>$asligtotal,
				  'uang_muka'=>$uang_muka,
				  'sisa_hutang'=>$sisa_hutang,
				  'keterangan'=>$ket,
				  'pkp'=>$hide_pkp,
				  'tipe_pajak'=>$hide_tipe_pajak,
				  'total_pajak'=>$total_pajak,
				  'dpp'=>$dpp,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'faktur_sj'=>$faktur_sj,
				  'status_faktur'=>$status_faktur,
				  'stok_masuk_lain_cash'=>$lain_cash,
				  'stok_masuk_lain_kredit'=>$lain_kredit,
				  'jenismasuk'=>$jenismasuk,
				  'jenis_pembelian'=>2,
				  'uid_update_by'=>$uid_update_by );
			//}

		$this->db->insert('tm_pembelian_wip',$data_header);
	}
		$this->db->query(" UPDATE tm_sjmasukwip SET status_sjmasukwip = 't' where id= '$id_sjmasukwip2' ");
				//	$this->db->query(" UPDATE tm_sjmasukwip SET status_edit = 't' where id= '$id_sjmasukwip2' ");

			// ambil data terakhir di tabel tm_pembelian_wip
			$query2	= $this->db->query(" SELECT id FROM tm_pembelian_wip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pembelian_wip	= $hasilrow->id; //echo $idnya; die();
			
			if ($id_brg_wip!='' && $qty!='0' && $harga!='') {
				

					$data_detail = array(
						'id_brg_wip'=>$id_brg_wip,
						// 29-10-2015 diganti jadi ga pake escape
						//'nama_brg'=>$this->db->escape_str($nama),
						'nama_brg'=>$nama,
						'qty'=>$qty,
						'id_satuan'=>$id_satuan,
						'id_satuan_konversi'=>$id_satuan_konversi,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						//'total'=>$total,
						'total'=>$aslitotal,
						'id_pembelian_wip'=>$id_pembelian_wip,
						'id_sjmasukwip_detail'=>$id_sjmasukwip_detail
					);
					
				$this->db->insert('tm_pembelian_wip_detail',$data_detail);
				
				
				$queryxx	= $this->db->query(" SELECT id FROM tm_pembelian_wip_detail ORDER BY id DESC LIMIT 1 ");
				$hasilxx = $queryxx->row();
				$id_pembelian_detail	= $hasilxx->id;

				// sementara id gudang blm diisi dulu, harus nanyain ke org Duta
					$id_gudang = 0;
					$lokasi = "01"; // duta

				//4. update harga di tabel harga_brg_supplier
				if (($harga != $harga_lama) && $id_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_jahit WHERE id_brg = '$id_brg_wip'
									AND id_unit_jahit = '$id_unit_jahit'  ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_unit_jahit (id_brg, id_unit_jahit, harga, 
						tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_jahit', '$harga', '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_unit_jahit SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg_wip' AND id_unit_jahit = '$id_unit_jahit'  ");
					}
				}
				
				if ($id_unit_jahit == '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_jahit WHERE id_brg = '$id_brg_wip'
									AND id_unit_jahit = '$id_unit_jahitbaru'  ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_unit_jahit (id_brg, id_unit_jahit, harga, 
						tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_jahitbaru', '$harga',  '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_unit_jahit SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg_wip' AND id_unit_jahit = '$id_unit_jahitbaru'  ");
					}
				}
			}
			
			// ---------------------------
		
					
					$query3	= $this->db->query(" SELECT a.qty FROM tm_sjmasukwip_detail a INNER JOIN tm_sjmasukwip b ON a.id_sjmasukwip = b.id
								WHERE a.id = '$id_sjmasukwip_detail' ");
								
					$hasilrow = $query3->row();
					$qty_sjmasukwip = $hasilrow->qty; 
					
					// 19-12-2011, cek
					$query4	= $this->db->query(" SELECT id FROM tm_pembelian_wip_detail WHERE id_sjmasukwip_detail = '".$id_sjmasukwip_detail."' ");
					if ($query4->num_rows() > 0){
						
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_wip_detail a INNER JOIN tm_pembelian_wip b ON a.id_pembelian_wip = b.id
									WHERE a.id_sjmasukwip_detail = '$id_sjmasukwip_detail' AND b.status_aktif = 't' ");
						
						$hasilrow = $query3->row();
						$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
					}
					else {
						$jum_beli = 0;
					}
					
					
					if ($jum_beli >= $qty_sjmasukwip) {
						$this->db->query(" UPDATE tm_sjmasukwip_detail SET status_sjmasukwip = 't' where id= '$id_sjmasukwip_detail' ");
						
						$this->db->select("id from tm_sjmasukwip_detail WHERE status_sjmasukwip = 'f' AND id_sjmasukwip = '$id_sjmasukwip2' ", false);
						$query = $this->db->get();
					/*	
						if ($query->num_rows() == 0){
							$this->db->query(" UPDATE tm_sjmasukwip SET status_sjmasukwip = 't' where id= '$id_sjmasukwip2' ");
						}
						*/
					}

			//----------------------------
			
			//update pkp dan tipe_pajak di tabel supplier
			$this->db->query(" UPDATE tm_unit_jahit SET pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak' where id= '$id_ujh_forsave' ");
			
			// 18-06-2015, save ke apply_stok digabung kesini
			$th_now	= date("Y");
	
		// ====================================================
		
  }

  function getAlltanpalimit($cunit_jahit, $cari, $date_from, $date_to, $caribrg, $filterbrg){

    $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelian) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_jahit != '0')
		$pencarian.= " AND a.id_unit_jahit = '$cunit_jahit' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelian DESC, a.id DESC ";
	
	$query = $this->db->query(" SELECT distinct a.* FROM tm_pembelian_wip a LEFT JOIN tm_pembelian_wip_detail b ON a.id=b.id_pembelian_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian);
	      
    return $query->result();  
  }
  function getAll($num, $offset, $cunit_jahit, $cari, $date_from, $date_to, $caribrg, $filterbrg) {	
	
	
	 $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelian) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelian <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_jahit != '0')
		$pencarian.= " AND a.id_unit_jahit = '$cunit_jahit' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelian DESC, a.id DESC ";
	
	$this->db->select(" distinct a.* FROM tm_pembelian_wip a LEFT JOIN tm_pembelian_wip_detail b ON a.id=b.id_pembelian_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	  
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				//print_r($row1);
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
								OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_wip_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id
							WHERE a.id_pembelian_wip = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						//----------------

					  
				
					  $id_detailnya = "";
					  
						//----------------
						
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							
						}
						else {
							$kode_brg = '';
						
						}
						

						$detail_fb[] = array(	'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg,
												'nama_brg_wip'=> $row2->nama_brg,
											
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$id_detailnya = "";
					$detail_fb = '';
				}
		
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$kode_unit_jahit	= $hasilrow->kode_unit;
				$nama_unit_jahit	= $hasilrow->nama;
		
				$no_sjmasukwipnya = "";
				
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj FROM tm_pembelian_wip_detail a INNER JOIN tm_sjmasukwip_detail b ON a.id_sjmasukwip_detail = b.id 
						INNER JOIN tm_sjmasukwip c ON c.id = b.id_sjmasukwip
						WHERE a.id_pembelian_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$no_sjmasukwipnya.= $rowxx->no_sj."<br>";
					}// end for
				}
				else {
					$no_sjmasukwipnya = '';
				
				}
				/*
				$sqlxx = " SELECT status_stok FROM tm_apply_stok_pembelian WHERE no_sj = '$row1->no_sj' 
							AND id_supplier = '$row1->id_supplier' AND status_aktif = 't' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$status_stok= $hasilxx->status_stok;
					if ($status_stok == 't')
						$cetakbtb = '1';
					else
						$cetakbtb = '0';
				}
				else
					$cetakbtb = '0';
					*/
						
				$data_fb[] = array(			'id'=> $row1->id,	
											
											'no_sjmasukwip'=> $no_sjmasukwipnya,	
											'jenismasuk' =>$row1->jenismasuk,
											'no_sjmasukpembelian'=> $row1->no_sjmasukpembelian,
											'tgl_sjpembelian'=> $row1->tgl_sjpembelian,
											'total'=> $row1->total,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'status_stok'=> $row1->status_stok,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											//'cetakbtb'=> $cetakbtb
											//'ambil_pp'=> $ambil_pp
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function delete($id){    
	  $tgl = date("Y-m-d H:i:s");
	  
	  
	  
	    $sqlxx = " SELECT DISTINCT id_sjmasukwip_detail FROM tm_pembelian_wip_detail WHERE id_pembelian_wip='$id' ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			//$hasilxx = $queryxx->row();
			$hasilxx=$queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$id_sjmasukwip_detail	= $rowxx->id_sjmasukwip_detail;
				
				
				if ($id_sjmasukwip_detail != '0') {
					$sqlxx2= " SELECT distinct a.id FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
							 WHERE b.id = '$id_sjmasukwip_detail' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_sjmasukwip	= $hasilxx2->id;
						$this->db->query(" UPDATE tm_sjmasukwip SET status_edit = 'f', status_sjmasukwip = 'f' where id= '$id_sjmasukwip' ");
					}
					// reset status di detailnya dari sjmasukwip pake perulangan
					$this->db->query(" UPDATE tm_sjmasukwip_detail SET status_sjmasukwip = 'f' where id= '$id_sjmasukwip_detail' ");
				}
						
				
				
			} // end for
		}
	  /*
	  // ====================================================================================
		
		// batalkan di tabel tm_apply_stok_pembelian
		//1. ambil no_sj dan kode_supplier
		$query4	= $this->db->query(" SELECT no_sjmasukpembelian, id_unit_jahit FROM tm_pembelian_wip where id = '$id' ");
		$hasilrow = $query4->row();
		$no_sj	= $hasilrow->no_sj;
		$id_unit_jahit	= $hasilrow->id_unit_jahit;
			
			
			
		//2. ambil id tm_apply_stok_pembelian
		$query4	= $this->db->query(" SELECT id, status_stok, no_bonm from tm_apply_stok_pembelian 
							where no_sj = '$no_sj' AND id_supplier = '$id_supplier' AND status_aktif = 't' ");
		if ($query4->num_rows() > 0){
			$hasil4=$query4->result();
			foreach ($hasil4 as $row4) {
				// ============================================
				
				// ambil id detail.......
				$query5	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail 
									where id_apply_stok = '$row4->id' AND status_stok = 't' ");
				if ($query5->num_rows() > 0){
					$hasil5=$query5->result();
					foreach ($hasil5 as $row5) {
					// edit transaksi stoknya
									
					$status_stok_header = $row4->status_stok;
					$status_stok_detail = $row5->status_stok;
											
					if ($status_stok_header == 't' || $status_stok_detail == 't' ) {
												
						//2. ambil stok terkini di tm_stok
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg='$row5->id_brg' ");
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
						$stokreset = $stok_lama-$row5->qty;
												
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE id_brg='$row5->id_brg' 
												AND harga='$row5->harga' ");
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
						$stokreset2 = $stok_lama-$row5->qty;
												
						//3. insert ke tabel tt_stok dgn tipe keluar, utk membatalkan data brg masuk
						// 18-06-2015, MASIH BINGUNG APAKAH TT_STOK INI DIPAKE ATAU GA						
						// 24-07-2015 GA DIPAKE
						/*$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_supplier, ".$stok_keluar.", saldo, tgl_input, harga)
											VALUES ('$row5->kode_brg', '$row4->no_bonm', '$kode_supplier', '$row5->qty', '$stokreset2', '$tgl', '$row5->harga' ) ");
						
													
						//4. update stok di tm_stok
						$this->db->query(" UPDATE tm_stok SET stok = '$stokreset', tgl_update_stok = '$tgl'
											where id_brg= '$row5->id_brg'  ");
													
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl'
											where id_brg= '$row5->id_brg' AND harga='$row5->harga'  ");
											
						$this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET status_stok = 'f' 
										WHERE id = '$row5->id' ");
					} // end if status_stoknya = 't'
				} // end foreach
										
			} // end if query5
									
				// ============================================
			}
		}
		
		//4. batalkan tm_apply_stok_pembelian
		$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_aktif = 'f', status_stok = 'f' 
						WHERE no_sj = '$no_sj' AND id_supplier = '$id_supplier' ");
			
		$this->db->query(" UPDATE tm_pembelian_wip SET status_aktif = 'f', status_stok = 'f' WHERE id = '$id' ");
		$this->db->query(" UPDATE tm_pembelian_wip_detail SET status_stok = 'f' WHERE id_pembelian = '$id' ");
	
	*/
	$this->db->query(" UPDATE tm_pembelian_wip SET status_aktif = 'f', status_stok = 'f' WHERE id = '$id' ");
		$this->db->query(" UPDATE tm_pembelian_wip_detail SET status_stok = 'f' WHERE id_pembelian_wip = '$id' ");
  }
   function get_pembelian($id_pembelian) {
			$query	= $this->db->query(" SELECT * FROM tm_pembelian_wip where id = '$id_pembelian' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			
				
				
				// 10-07-2015 DIMODIF
				$no_sjmasukwipnya = ""; 
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj,c.tgl_sj FROM tm_pembelian_wip_detail a INNER JOIN tm_sjmasukwip_detail b ON a.id_sjmasukwip_detail = b.id 
						INNER JOIN tm_sjmasukwip c ON c.id = b.id_sjmasukwip
						WHERE a.id_pembelian_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$pisah1 = explode("-", $rowxx->tgl_sj);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						$no_sjmasukwipnya.= $rowxx->no_sj." (".$tgl_sj.")"."<br>";
					}// end for
				}
				else {
					$no_sjmasukwipnya = '-';
				
				}
							
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_wip_detail WHERE id_pembelian_wip = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						if($query3->num_rows > 0 ){
						$hasilrow = $query3->row();
						$nama_satuan = $hasilrow->nama;
					}
					else{
						$nama_satuan = 'pieces';
						}										
						$sum_qty = 0;
						$sum_beli = 0;
					  
					  if ($row2->satuan_lain != 0) {
						  $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
						  $hasilrow = $query3->row();
						  $nama_satuan_lain = $hasilrow->nama;
					  }
					  else
						$nama_satuan_lain = '';
					  
					    $query3	= $this->db->query(" SELECT d.kode as kode_kel_brg FROM tm_barang_wip a 
					    INNER JOIN tm_jenis_brg_wip c ON a.id_jenis_brg_wip = c.id 
						INNER JOIN tm_kel_brg_wip d ON a.id_kel_brg_wip=d.id
									WHERE a.id = '$row2->id_brg_wip' ");
						if($query3->num_rows > 0){			
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
					}
					else
						$kode_kel_brg = 0;
						
						$sqlop = " SELECT b.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE b.id = '$row2->id_sjmasukwip_detail' ";
						$queryop	= $this->db->query($sqlop);
						if($queryop->num_rows()>0) {
							$hasilop = $queryop->row();
							$qty_op_detail = $hasilop->qty;
						}
						else
							$qty_op_detail = 0;
						
						if ($row2->id_sjmasukwip_detail != '0') {
							$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelian_wip_detail a 
										INNER JOIN tm_pembelian_wip b ON b.id=a.id_pembelian_wip
										WHERE a.id_sjmasukwip_detail='$row2->id_sjmasukwip_detail' AND a.id_brg_wip='$row2->id_brg_wip' 
										AND b.status_aktif='t'
										AND b.tgl_sjpembelian <= '$row1->tgl_sjpembelian' ");
							
							if($query3->num_rows()>0) {
								$hasilrow4 = $query3->row();
								$pemenuhan = $hasilrow4->pemenuhan;
								$sisa = $qty_op_detail-$pemenuhan;
							}else{
								$pemenuhan = 0;
								$sisa = $qty_op_detail-$pemenuhan;
								
								if($pemenuhan=='')
									$pemenuhan = 0;
								
								if($sisa=='')
									$sisa = 0;
							}
						}
						else {
							$sisa = '-';
							$pemenuhan = 0;
						}
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_kel_brg'=> $kode_kel_brg,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $this->db->escape_str($nama_brg),
												'nama_satuan'=> $nama_satuan,
												'harga'=> $row2->harga,
												'qty'=> $row2->qty,
												'id_satuan'=> $row2->id_satuan,
												'id_satuan_konversi'=> $row2->id_satuan_konversi,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total,
												'qty_sjmasukwip'=> $sum_qty,
												'jum_beli'=> $sum_beli,
												'satuan_lain'=> $row2->satuan_lain,
												'nama_satuan_lain'=> $nama_satuan_lain,
												'qty_sat_lain'=> $row2->qty_satuan_lain,
												'id_sjmasukwip_detail'=> $row2->id_sjmasukwip_detail,
												
												
												'pemenuhan'=> $pemenuhan,
												'sisa'=> $sisa
											);
					}
				}
				else {
					$detail_fb = '';
				}


				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$kode_unit_jahit	= $hasilrow->kode_unit;
				$nama_unit_jahit	= $hasilrow->nama;
				$top	= '';
				
				$pisah1 = explode("-", $row1->tgl_sjpembelian);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
				
				/*
				// 31-07-2015, ambil id gudang pake distinct utk keperluan nama staf adm stok di cetak BTB
				$sqlxx = " SELECT DISTINCT d.id_gudang FROM tm_apply_stok_pembelian_detail a 
							INNER JOIN tm_pembelian_wip_detail b ON a.id_pembelian_detail = b.id
							INNER JOIN tm_pembelian_wip c ON b.id_pembelian = c.id
							INNER JOIN tm_barang d ON d.id = a.id_brg
							WHERE c.id='$row1->id' AND a.status_stok = 't'  ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$list_id_gudang='';
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$list_id_gudang.= $rowxx->id_gudang.";";
					}// end for
				}
				else {
					$list_id_gudang='';
				}
				
				
				
				// 27-08-2015 ambil uid_update_by dari tabel tm_apply_stok_pembelian
				$sqlxx = " SELECT uid_update_by FROM tm_apply_stok_pembelian WHERE status_aktif = 't'
						AND no_sj = '$row1->no_sj' AND id_supplier = '$row1->id_supplier' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$admgudang_uid_update_by = $hasilxx->uid_update_by;
				}
				else
					$admgudang_uid_update_by = 0;
				*/
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_sjmasukwip'=> $no_sjmasukwipnya,
											//'no_bonm'=> $no_bonmnya,
											//'no_pp'=> $no_ppnya,	
											'no_sjmasukpembelian'=> $row1->no_sjmasukpembelian,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'total'=> $row1->total,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'pkp'=> $row1->pkp,
											'tipe_pajak'=> $row1->tipe_pajak,
											'dpp'=> $row1->dpp,
											'stok_masuk_lain_cash'=> $row1->stok_masuk_lain_cash,
											'stok_masuk_lain_kredit'=> $row1->stok_masuk_lain_kredit,
											'detail_fb'=> $detail_fb,
											//'list_id_gudang'=> $list_id_gudang,
										//	'admgudang_uid_update_by'=> $admgudang_uid_update_by
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }  
  

  }  
  
  

