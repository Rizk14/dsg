<?php
class Mmaster extends CI_Model {
	
  function __construct() { 

  parent::__construct();

}
  
  function get_all_order($date_from, $date_to, $supplier) {
		$sql=" SELECT a.id, a.no_op, a.tgl_op, b.kode_supplier, b.nama, a.status_op, a.keterangan FROM tm_op a 
				INNER JOIN tm_supplier b ON a.id_supplier = b.id
				WHERE (a.tgl_op >= to_date('$date_from','dd-mm-yyyy')) 
				AND (a.tgl_op <= to_date('$date_to','dd-mm-yyyy')) 
				AND a.status_aktif='t' ";
		if ($supplier != '0')
			$sql.= " AND a.id_supplier = '$supplier' ";
		$sql.= " ORDER BY a.tgl_op ASC, b.kode_supplier ASC, a.no_op ASC ";
						
		$query	= $this->db->query($sql);
		
		$data = array();
		$detail = array();
		
		if ($query->num_rows() > 0) {
			
			foreach ($query->result() as $row1) {

				$query2	= $this->db->query(" SELECT a.id, a.id_op, a.id_brg, a.qty, b.kode_brg, b.nama_brg 
							FROM tm_op_detail a INNER JOIN tm_barang b ON b.id = a.id_brg
							WHERE a.id_op='$row1->id' ORDER BY a.id ASC ");
					
				foreach($query2->result() as $hasilrow3) {
						
					$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelian_detail a 
								INNER JOIN tm_pembelian b ON b.id=a.id_pembelian
								WHERE a.id_op_detail='$hasilrow3->id' AND a.id_brg='$hasilrow3->id_brg' 
								AND b.status_aktif='t' ");
					
					if($query3->num_rows()>0) {
						$hasilrow4 = $query3->row();
						$pemenuhan = $hasilrow4->pemenuhan;
						$sisa = $hasilrow3->qty-$pemenuhan;
					}else{
						$pemenuhan = 0;
						$sisa = $hasilrow3->qty-$pemenuhan;
						
						if($pemenuhan=='')
							$pemenuhan = 0;
						
						if($sisa=='')
							$sisa = 0;
					}
					
					// 24-08-2015 sisipin skrip utk update status_op = 't' atau 'f' sesuai nilai sisa
					if ($sisa > 0) {
						$this->db->query(" UPDATE tm_op_detail SET status_op='f' WHERE id = '".$hasilrow3->id."' ");
						$this->db->query(" UPDATE tm_op SET status_op='f' WHERE id = '".$row1->id."' ");
					}
					else {
						$this->db->query(" UPDATE tm_op_detail SET status_op='t' WHERE id = '".$hasilrow3->id."' ");
						
						//cek di tabel tm_op_detail, apakah status_op sudah 't' semua?
						$this->db->select("id from tm_op_detail WHERE status_op = 'f' AND id_op = '".$row1->id."' ", false);
						$query = $this->db->get();
						//jika sudah t semua, maka update tabel tm_op di field status_op menjadi t
						if ($query->num_rows() == 0){
							$this->db->query(" UPDATE tm_op SET status_op = 't' where id= '".$row1->id."' ");
						}
					}
						
					$detail[] = array('kode_brg' => $hasilrow3->kode_brg, 
							'qty' => $hasilrow3->qty, 
							'nama_brg' => $hasilrow3->nama_brg,
							'pemenuhan' => $pemenuhan,
							'sisa' => $sisa);
				}
				
				$data[] = array('no_op'=> $row1->no_op,
								'tgl_op'=> $row1->tgl_op,
								'kode_supplier'=> $row1->kode_supplier,
								'nama_supplier'=> $row1->nama,
								'status_op'=> $row1->status_op,
								'keterangan'=> $row1->keterangan,
								'detail'=> $detail
				);
				
				$detail = array();
			}
			
		}else{
			$data = '';
		}
		
		return $data;
  }
  
  function get_all_ordertanpalimit($date_from, $date_to, $supplier) {
	$sql=" SELECT a.id, a.no_op, a.tgl_op, b.kode_supplier, b.nama, a.status_op, a.keterangan FROM tm_op a
				INNER JOIN tm_supplier b ON a.id_supplier = b.id
						WHERE (a.tgl_op >= to_date('$date_from','dd-mm-yyyy')) 
						AND (a.tgl_op <= to_date('$date_to','dd-mm-yyyy')) 
						AND a.status_aktif='t' ";
		if ($supplier != '0')
			$sql.= " AND a.id_supplier = '$supplier' ";
		
	$query	= $this->db->query($sql);
	    
    return $query->result();  
    
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  // 04-08-2012
  function get_all_order_forexport($date_from, $date_to, $supplier) {
		$sql=" SELECT a.id, a.no_op, a.tgl_op, b.kode_supplier, b.nama, a.status_op, a.keterangan, 
				c.id, c.id_brg, c.qty, d.kode_brg, d.nama_brg
				FROM tm_op a INNER JOIN tm_op_detail c ON a.id = c.id_op 
				INNER JOIN tm_barang d ON c.id_brg = d.id 
				INNER JOIN tm_supplier b ON b.id = a.id_supplier
						WHERE (a.tgl_op >= to_date('$date_from','dd-mm-yyyy')) 
						AND (a.tgl_op <= to_date('$date_to','dd-mm-yyyy')) 
						AND a.status_aktif='t' ";
		if ($supplier != '0')
			$sql.= " AND a.id_supplier = '$supplier' ";
		$sql.= " ORDER BY a.tgl_op ASC, b.kode_supplier ASC, a.no_op ASC, c.id ASC ";
						
		$query	= $this->db->query($sql);
		
		$data = array();
		$detail = array();
		
		if ($query->num_rows() > 0) {
			
			foreach ($query->result() as $row1) {
				
				$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelian_detail a 
					INNER JOIN tm_pembelian b ON b.id=a.id_pembelian
					WHERE a.id_op_detail='$row1->id' AND a.id_brg='$row1->id_brg' AND  b.status_aktif='t' ");
					
					if($query3->num_rows()>0) {
						$hasilrow4 = $query3->row();
						$pemenuhan = $hasilrow4->pemenuhan;
						$sisa = ($row1->qty)-$pemenuhan;
					}else{
						$pemenuhan = 0;
						$sisa = ($row1->qty)-$pemenuhan;
						
						if($pemenuhan=='')
							$pemenuhan = 0;
						
						if($sisa=='')
							$sisa = 0;
					}
				
				$data[] = array('no_op'=> $row1->no_op,
					'tgl_op'=> $row1->tgl_op,
					'kode_supplier'=> $row1->kode_supplier,
					'nama_supplier'=> $row1->nama,
					'status_op'=> $row1->status_op,
					'keterangan'=> $row1->keterangan,
					'kode_brg' => $row1->kode_brg, 
					'qty' => $row1->qty, 
					'nama_brg' => $row1->nama_brg,
					'pemenuhan' => $pemenuhan,
					'sisa' => $sisa
				);
			}
			
		}else{
			$data = '';
		}
		
		return $data;
  }
    
}

