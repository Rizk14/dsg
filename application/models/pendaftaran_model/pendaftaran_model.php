<?php
class Pendaftaran_model extends MY_Model
{
    public $_tabel = 'tb_pengguna';

    public $form_rules = array(
       
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|xss_clean|required|max_length[12]'
        ),
        
        array(
            'field' => 'captcha',
            'label' => 'Captcha',
            'rules' => 'trim|xss_clean|required|exact_length[4]|callback__validate_captcha'
        ),
        
         array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|xss_clean|required|max_length[64]|is_unique[tb_pengguna.email]|valid_email'
        ),
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
         array(
            'field' => 'bagian',
            'label' => 'Bagian',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
    );

    public $default_values = array(
		'email' => '',
        'nama' => '',
        'username' => '',
        'bagian' => '',
        'captcha' => '',
    );

    public function daftar($pengguna)
    {
        // Data captcha tidak perlu disimpan di DB.
        unset($pengguna->captcha);

        // Generate random string username dan password untuk login user.
      
        $pengguna->password =random_string('alnum', 8);
		$random_password =$pengguna->password;
		 $pengguna->password = md5 ($pengguna->password);
        // Proses insert data ke tabel tb_pengguna.
        $id = $this->insert($pengguna);
        if ($id) {
           

            // Set data untuk ditampilkan.
            $data_session = array(
   
                'username' => $pengguna->username,
                'password' => $random_password,
               
            );
            $this->session->set_userdata($data_session);
            return true;
        }
        return false;
    }

    public function reset_pengguna()
    {
        $data_session = array(
            'id' => '',
            'username'  => '',
            'password'  => '',
            'email' => '',
        );
        $this->session->unset_userdata($data_session);
    }
}
