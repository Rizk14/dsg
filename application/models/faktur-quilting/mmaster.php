<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $unit_makloon, $cari) {	  
	if ($cari == "all") {
		if ($unit_makloon == '0') {
			$this->db->select(" * FROM tm_faktur_makloon WHERE jenis_makloon = '1' ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_faktur_makloon WHERE jenis_makloon = '1' AND kode_unit = '$unit_makloon'
								ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($unit_makloon != '0') {
			$this->db->select(" * FROM tm_faktur_makloon WHERE jenis_makloon = '1' AND kode_unit = '$unit_makloon' 
			AND UPPER(no_faktur) like UPPER('%$cari%') ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_faktur_makloon WHERE UPPER(no_faktur) like UPPER('%$cari%') 
							AND jenis_makloon = '1' ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();

		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
								
				// ===============================
					// ambil detail data no & tgl SJ
				$query2	= $this->db->query(" SELECT a.no_sj_masuk FROM tm_faktur_makloon_sj a, tm_faktur_makloon b 
							WHERE a.id_faktur_makloon = b.id AND
							b.no_faktur = '$row1->no_faktur' AND b.kode_unit = '$row1->kode_unit' ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_sj	= $row2->no_sj_masuk;
						
						$sql = "SELECT tgl_sj from tm_sj_hasil_makloon where no_sj = '$no_sj' 
									AND kode_unit= '$row1->kode_unit' ";
						$query3	= $this->db->query($sql);
						$hasilrow3 = $query3->row();
						$tgl_sj	= $hasilrow3->tgl_sj;
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj
											);		
					}
				}
				else {
					$detail_fb = '';
				}
				// ===========
				
				$pisah1 = explode("-", $row1->tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
												
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
																		
				$data_fb[] = array(			'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,	
											'no_faktur'=> $row1->no_faktur,	
											'tgl_faktur'=> $tgl_faktur,	
											'kode_unit'=> $row1->kode_unit,	
											'nama_unit'=> $nama_unit,	
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb,
											'status_faktur_pajak'=> $row1->status_faktur_pajak,
											'status_lunas'=> $row1->status_lunas
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		
		return $data_fb;
  }
  
  function getAlltanpalimit($unit_makloon, $cari){
	if ($cari == "all") {
		if ($unit_makloon == '0')
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE jenis_makloon = '1' ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE kode_unit = '$unit_makloon' 
							AND jenis_makloon = '1' ");
	}
	else {
		if ($unit_makloon != '0')
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE kode_unit = '$unit_makloon' 
						AND jenis_makloon = '1' AND UPPER(no_faktur) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE jenis_makloon = '1' 
						AND UPPER(no_faktur) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_faktur, $unit_makloon){
    $this->db->select("id from tm_faktur_makloon WHERE no_faktur = '$no_faktur' AND kode_unit = '$unit_makloon' 
						AND jenis_makloon = '1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function saveharga($unit_makloon, $id_sj_detail, $kode_brg_makloon, $harga, $harga_lama, $total) {  
    $tgl = date("Y-m-d");
    
    $this->db->query(" UPDATE tm_sj_hasil_makloon_detail SET harga = '$harga', biaya = '$total' 
					WHERE id = '$id_sj_detail' ");
	
	// cek harga, jika harga baru != harga lama, maka update ke tabel harga quilting
	if ($harga != $harga_lama) {
		$this->db->query(" UPDATE tm_harga_quilting SET harga = '$harga', tgl_update = '$tgl'
					WHERE kode_brg_quilting = '$kode_brg_makloon' AND kode_unit = '$unit_makloon' ");
		
		$datanya = array(
			  'kode_brg_quilting'=>$kode_brg_makloon,
			  'kode_unit'=>$unit_makloon,
			  'tgl_input'=>$tgl,
			  'harga'=>$harga
			);
		$this->db->insert('tt_harga_quilting',$datanya);
	}
  }
    
  function delete($kode){    
	  $tgl = date("Y-m-d");
	  
	  $query3	= $this->db->query(" SELECT kode_unit FROM tm_faktur_makloon 
								WHERE id = '$kode' ");
	  $hasilrow = $query3->row();
	  $kode_unit = $hasilrow->kode_unit;
	  
	  // status sj balikin ke false
	  $query2	= $this->db->query(" SELECT * FROM tm_faktur_makloon_sj WHERE id_faktur_makloon = '$kode' ");
	  if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
			foreach ($hasil2 as $row2) {
				$query3	= $this->db->query(" SELECT id FROM tm_sj_hasil_makloon WHERE no_sj = '$row2->no_sj_masuk'
								AND kode_unit = '$kode_unit' ");
				$hasilrow = $query3->row();
				$id_sj_hasil_makloon = $hasilrow->id;
				
				$this->db->query(" UPDATE tm_sj_hasil_makloon SET status_faktur = 'f', no_faktur = ''
								where id = '$id_sj_hasil_makloon' ");
			}
	  }
	  // ==========================================================
	  
		$this->db->delete('tm_faktur_makloon', array('id' => $kode));
		$this->db->delete('tm_faktur_makloon_sj', array('id_faktur_makloon' => $kode));

  } 
  
  function get_sjfaktur($id_faktur){
	  // 1. query dari tabel tm_faktur_makloon
	  // 2. ambil data detail no sj pake perulangan
	  // 3. di tiap2 data no sj, ambil data2 item barangnya
	  
	$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon WHERE id = '$id_faktur' ");    
    $hasil = $query->result();
    
    $data_faktur = array();
    $detail_brg = array();
    $list_sj = "";
	
	foreach ($hasil as $row1) {
		// ambil data detail SJnya
				$query2	= $this->db->query(" SELECT * FROM tm_faktur_makloon_sj WHERE id_faktur_makloon = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
							
					foreach ($hasil2 as $row2) {
						$list_sj .= $row2->no_sj_masuk.",";
						// =====
						$query4	= $this->db->query(" SELECT b.*, a.no_sj, a.tgl_sj FROM tm_sj_hasil_makloon a, 
										tm_sj_hasil_makloon_detail b
										WHERE a.id = b.id_sj_hasil_makloon AND a.no_sj = '$row2->no_sj_masuk' 
										AND a.kode_unit = '$row1->kode_unit'  ");
						if ($query4->num_rows() > 0){
							$hasil4=$query4->result();
												
							foreach ($hasil4 as $row4) {
								$id_sj_hasil_makloon = $row4->id_sj_hasil_makloon;
								$id_sj_hasil_makloon_detail = $row4->id;
								$kode_brg = $row4->kode_brg;
								$kode_brg_makloon = $row4->kode_brg_makloon;
								$kode_brg_jadi = $row4->kode_brg_jadi;
								$harga = $row4->harga;
								$subtotal = $row4->biaya;
								$qty_makloon = $row4->qty_makloon;
								$detail_pjg_quilting = $row4->detail_pjg_quilting;
								$no_sj = $row4->no_sj;
								$tgl_sj = $row4->tgl_sj;
								
								$pisah1 = explode("-", $tgl_sj);
								$tgl1= $pisah1[2];
								$bln1= $pisah1[1];
								$thn1= $pisah1[0];
								if ($bln1 == '01')
									$nama_bln = "Januari";
								else if ($bln1 == '02')
									$nama_bln = "Februari";
								else if ($bln1 == '03')
									$nama_bln = "Maret";
								else if ($bln1 == '04')
									$nama_bln = "April";
								else if ($bln1 == '05')
									$nama_bln = "Mei";
								else if ($bln1 == '06')
									$nama_bln = "Juni";
								else if ($bln1 == '07')
									$nama_bln = "Juli";
								else if ($bln1 == '08')
									$nama_bln = "Agustus";
								else if ($bln1 == '09')
									$nama_bln = "September";
								else if ($bln1 == '10')
									$nama_bln = "Oktober";
								else if ($bln1 == '11')
									$nama_bln = "November";
								else if ($bln1 == '12')
									$nama_bln = "Desember";
								$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
								
								// ambil nama brg makloon
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
														WHERE a.satuan=b.id AND a.kode_brg = '$kode_brg_makloon' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$nama_brg_makloon	= $hasilrow->nama_brg;
									$satuan_makloon = $hasilrow->nama_satuan;
								}
								else {
									$nama_brg_makloon	= '';
									$satuan_makloon = '';
								}
														
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$nama_brg	= '';
									$satuan	= '';
								}
								
								// brg jadi
								$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
												WHERE i_product_motif = '$kode_brg_jadi' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$nama_brg_jadi	= $hasilrow->e_product_motifname;
								}
								else {
									$nama_brg_jadi	= '';
								}
								
								$detail_brg[] = array(	'id'=> $id_sj_hasil_makloon_detail,
													'kode_brg_makloon'=> $kode_brg_makloon,
													'nama_brg_makloon'=> $nama_brg_makloon,
													'satuan'=> $satuan_makloon,
													'qty_makloon'=> $qty_makloon,
													'kode_brg'=> $kode_brg,
													'nama_brg'=> $nama_brg,
													'kode_brg_jadi'=> $kode_brg_jadi,
													'nama_brg_jadi'=> $nama_brg_jadi,
													'no_sj'=> $no_sj,
													'tgl_sj'=> $tgl_sj,
													'detail_pjg_quilting'=> $detail_pjg_quilting,
													'harganya'=> $harga,
													'subtotal'=> $subtotal
											);
							}
						}
						else
							$detail_brg = '';
						// =====
						
					}
				}
				else {
					$detail_brg = '';
				}
		
		// ================== end detail
		
				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				$pisah1 = explode("-", $row1->tgl_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
												
				$data_faktur[] = array(		'id'=> $row1->id,	
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'jumlah'=> $row1->jumlah,
											'detail_brg'=> $detail_brg,
											'list_sj'=> $list_sj
											);
				$detail_brg = array();
				$list_sj = "";
											
			} // endforeach header
	
	return $data_faktur;
  }
  
  function get_unit_quilting(){
    $this->db->select(" * from tm_unit_quilting order by kode_unit ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_makloon(){
    $this->db->select("* from tm_jenis_makloon order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_bhn)
  {
	if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_bhn){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  //function get_sj_masuk($num, $offset, $cari, $unit_makloon)
  function get_sj_masuk($cari, $unit_makloon)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_sj_hasil_makloon where kode_unit = '$unit_makloon' AND status_faktur = 'f' order by tgl_sj DESC ";
		//$this->db->select($sql, false)->limit($num,$offset);
		$this->db->select($sql, false);
	}
	else {
		$sql = " * FROM tm_sj_hasil_makloon where kode_unit = '$unit_makloon' AND status_faktur = 'f' ";
		$sql.=" AND UPPER(no_sj) like UPPER('%$cari%') order by tgl_sj DESC ";
		//$this->db->select($sql, false)->limit($num,$offset);	
		$this->db->select($sql, false);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {		
			
			// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon_detail 
											WHERE id_sj_hasil_makloon = '$row1->id'  ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ambil nama brg makloon
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
												WHERE a.satuan=b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							$satuan_makloon = $hasilrow->nama_satuan;
						}
						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
												
						$detail_bhn[] = array('id'=> $row2->id,
												'id_sj_hasil_makloon'=> $row2->id_sj_hasil_makloon,
												'no_sj'=> $row1->no_sj,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'kode_brg_makloon'=> $row2->kode_brg_makloon,
												'nama_brg_makloon'=> $nama_brg_makloon,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty_makloon'=> $row2->qty_makloon
									);
						
					} 
				} 
				else {
					$detail_bhn = '';
				} // end detail
			
			$data_bhn[] = array(		'id'=> $row1->id,	
										'no_sj'=> $row1->no_sj,	
										'tgl_sj'=> $row1->tgl_sj,
										'tgl_update'=> $row1->tgl_update,
										'keterangan'=> $row1->keterangan,
										'total'=> $row1->total,
										'detail_bhn'=> $detail_bhn
								);
			
			$detail_bhn = array();
								
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_sj_masuktanpalimit($cari, $unit_makloon){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_sj_hasil_makloon where kode_unit = '$unit_makloon' AND status_faktur = 'f' ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_sj_hasil_makloon where kode_unit = '$unit_makloon' AND status_faktur = 'f' ";
		$sql.=" AND UPPER(no_sj) like UPPER('%$cari%') ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_detail_sj_masuk($no_sj_masuk, $id_sj_masuk, $unit_makloon){
    $detail_sj = array();
    
    $list_id_sj = explode(",", $id_sj_masuk); 
    foreach($list_id_sj as $row1) {
		if ($row1 != '') {
			// ====================================
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon_detail
							WHERE id_sj_hasil_makloon = '$row1' ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$id_sj_hasil_makloon_detail	= $row2->id;
						$kode_brg	= $row2->kode_brg;
						$kode_brg_makloon	= $row2->kode_brg_makloon;
						$kode_brg_jadi	= $row2->kode_brg_jadi;
						$qty_makloon	= $row2->qty_makloon;
						$detail_pjg_quilting	= $row2->detail_pjg_quilting;
						
						$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sj_hasil_makloon WHERE id = '$row1' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$no_sj	= $hasilrow->no_sj;
				$tgl_sj	= $hasilrow->tgl_sj;
				
				$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
			}
			else {
				$no_sj	= '';
				$tgl_sj	= '';
			}
												
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							$satuan_makloon	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_makloon	= '';
							$satuan_makloon	= '';
						}
						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
						
						// ambil harga bhn quilting
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_quilting
										WHERE kode_brg_quilting = '$kode_brg_makloon' AND kode_unit = '$unit_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$harganya	= $hasilrow->harga;
						}
						else {
							$harganya	= 0;
						}
						
						// ambil data nama unit
						$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$unit_makloon' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
						
						$detail_sj[] = array(		'id'=> $id_sj_hasil_makloon_detail,
										'id_sj_hasil_makloon'=> $row1,
										'kode_brg'=> $kode_brg,
										'nama_brg'=> $nama_brg,
										'kode_brg_makloon'=> $kode_brg_makloon,
										'nama_brg_makloon'=> $nama_brg_makloon,
										'kode_brg_jadi'=> $kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'qty_makloon'=> $qty_makloon,
										'detail_pjg_quilting'=> $detail_pjg_quilting,
										'no_sj'=> $no_sj,
										'tgl_sj'=> $tgl_sj,
										'kode_unit'=> $unit_makloon,
										'nama_unit'=> $nama_unit,
										'harganya'=> $harganya
								);
					}
				}
			// ====================================
		
		}
	} // end foreach
	return $detail_sj;
  }

}
