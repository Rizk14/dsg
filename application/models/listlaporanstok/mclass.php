<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
		
	function statusSO($stop) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT i_so, d_so, i_status_so, f_stop_produksi FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$stop' ");
	}
	
	function listopname($stp){
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_so_item AS isoitem, a.i_so AS iso, a.i_product AS iproduct, a.n_quantity_awal AS qty, c.e_product_motifname  AS motifname, b.d_so AS tglso FROM tm_stokopname_item a 
		INNER JOIN tm_stokopname b ON b.i_so=a.i_so
		INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product WHERE b.i_status_so='0' AND b.f_stop_produksi='$stp' AND b.f_updated='f' ORDER BY a.i_product ASC ");
		
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function statusopname($stp){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$stp' AND f_updated='f' ");
	}
	
	function view($cari,$iso,$stop){
		$db2=$this->load->database('db_external', TRUE);
		$stp	= " c.f_stop_produksi='$stop' ";
		if($cari!='kosong' && $cari!=''){
			$paramcari	= " AND ".$stp." AND (a.i_product='$cari' OR upper(b.e_product_motifname) LIKE '%$cari%') ";
		}else{
			$paramcari	= " AND (".$stp.") ";
		}
		return $db2->query("
			SELECT a.i_product, b.e_product_motifname, a.n_quantity_awal, a.n_quantity_akhir, e_note as enote FROM tm_stokopname_item a
			
			INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
			INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
			
			WHERE a.i_so='$iso' ".$paramcari." ORDER BY a.i_product ASC ");
	}
	
	function viewperpages($limit,$offset,$cari,$iso,$stop) {
		$db2=$this->load->database('db_external', TRUE);
		$stp	= " c.f_stop_produksi='$stop' ";
		
		if($cari!='kosong' && $cari!='') {
			$paramcari	= " AND ".$stp." AND (a.i_product='$cari' OR upper(b.e_product_motifname) LIKE '%$cari%') ";
		}else{
			$paramcari	= " AND (".$stp.") ";
		}
		
		$query	= $db2->query(" SELECT a.i_product AS imotif, 
				b.e_product_motifname AS eproductname, 
				a.n_quantity_awal AS qtyawal, 
				a.n_quantity_akhir AS qtyakhir, 
				e_note as enote, a.i_so_item 
				FROM tm_stokopname_item a
						
				INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
				INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
						
				WHERE a.i_so='$iso' ".$paramcari." ORDER BY a.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function lbonkeluar($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonkeluar FROM tm_outbonm_item a
				INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm 
				
				WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif'
				
				GROUP BY a.i_product ");
	}

	function lbonmasuk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonmasuk FROM tm_inbonm_item a
				INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
				
				WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif'
				
				GROUP BY a.i_product ");
	}

	function lbbk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbk FROM tm_bbk_item a
				INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
				
				WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif'
				
				GROUP BY a.i_product ");
	}

	function lbbm($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbm FROM tm_bbm_item a
				INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying) 
				
				WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif'
				
				GROUP BY a.i_product ");
	}
	
	function ldo($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT a.i_product, sum(a.n_deliver) AS jdo FROM tm_do_item a
			INNER JOIN tm_do b ON b.i_do=a.i_do
			
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_do_cancel='f'
			
			GROUP BY a.i_product ");
	}

	function lsj($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_product, sum(a.n_unit) AS jsj FROM tm_sj_item a
			INNER JOIN tm_sj b ON b.i_sj=a.i_sj
			
			WHERE (b.d_sj BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_sj_cancel='f'
			
			GROUP BY a.i_product ");
	}
		
	// Bulan & Tahun masih salah !!!!!!!!!! 
	function mupdate($stp,$blnso,$thnso,$iproduct,$nquantityakhir,$isoitem,$iso,$iteration,$stp,
	$stok_fisik, $i_color, $enote,$totalhari,$iso_lama) {
		$db2=$this->load->database('db_external', TRUE);

		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
			
		$jml=0;	
		
		$blnopname	= date("m");
		$dupdate	= date("Y-m-d");
		
		if($blnopname==$blnso) {
		
			while($jml<=$iteration) {
			
				$tg_1		= "01";
				
				$dfirst	= $thnso."-".$blnso."-".$tg_1; // Y-m-d
				$dlast	= $thnso."-".$blnso."-".$totalhari;
				
				$qbonmkeluar	= $db2->query("
				SELECT a.i_product, SUM(a.n_count_product) AS jbonkeluar FROM tm_outbonm_item a
					INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm 
					
					WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
					
				if($qbonmkeluar->num_rows()>0) {	
					$rbonmkeluar	= $qbonmkeluar->row();
					if($rbonmkeluar->jbonkeluar!='') {
						$jjbonkeluar	= $rbonmkeluar->jbonkeluar;
					}else{
						$jjbonkeluar	= 0;
					}
				}else{
					$jjbonkeluar	= 0;
				}
				
				$qbonmmasuk		= $db2->query("
				SELECT a.i_product, SUM(a.n_count_product) AS jbonmasuk FROM tm_inbonm_item a
					INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
					
					WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
					
				if($qbonmmasuk->num_rows()>0) {	
					$rbonmmasuk	= $qbonmmasuk->row();
					if($rbonmmasuk->jbonmasuk!='') {
						$jjbonmasuk	= $rbonmmasuk->jbonmasuk;
					}else{
						$jjbonmasuk	= 0;
					}
				}else{
					$jjbonmasuk	= 0;
				}
				
			/*	$qbbk	= $db2->query("
				SELECT a.i_product, SUM(a.n_unit) AS jbbk FROM tm_bbk_item a
					INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
					
					WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
				
				if($qbbk->num_rows()>0){	
					$rbbk	= $qbbk->row();
					if($rbbk->jbbk!=''){
						$jjbbk	= $rbbk->jbbk;
					}else{
						$jjbbk	= 0;
					}
				}else{
					$jjbbk	= 0;
				}
							
				$qbbm	= $db2->query("
				SELECT a.i_product, SUM(a.n_unit) AS jbbm FROM tm_bbm_item a
					INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying) 
					
					WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
				
				if($qbbm->num_rows()>0) {	
					$rbbm	= $qbbm->row();
					if($rbbm->jbbm!='') {
						$jjbbm	= $rbbm->jbbm;
					}else{
						$jjbbm	= 0;
					}
				}else{
					$jjbbm	= 0;
				} */
							
				$qdo	= $db2->query( "
				SELECT a.i_product, SUM(a.n_deliver) AS jdo FROM tm_do_item a
				INNER JOIN tm_do b ON b.i_do=a.i_do
				
				WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]' AND b.f_do_cancel='f'
				
				GROUP BY a.i_product ");
				
				if($qdo->num_rows()>0) {
					
					$rdo	= $qdo->row();
					
					if($rdo->jdo!='') {
						$jjdo	= $rdo->jdo;
					}else{
						$jjdo	= 0;
					}
				}else{
					$jjdo	= 0;
				}

			/*	$qsj	= $db2->query("
				SELECT a.i_product, sum(a.n_unit) AS jsj FROM tm_sj_item a
			INNER JOIN tm_sj b ON b.i_sj=a.i_sj
			
			WHERE (b.d_sj BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]' AND b.f_sj_cancel='f'
			
			GROUP BY a.i_product ");
				
				if($qsj->num_rows()>0) {
					if($qsj->num_rows()>0) {
						$row_sj	= $qsj->row();
						$sj	= $row_sj->jsj; 
					}else{
						$sj = 0;
					}
				}else{
					$sj = 0;
				} */
								
				/*$masukan	= $jjbonmasuk + $jjbbm;
				$keluaran	= $jjdo + $jjbonkeluar + $jjbbk + $sj; */
				$masukan	= $jjbonmasuk;
				$keluaran	= $jjdo + $jjbonkeluar;
				$qty		= $nquantityakhir[$jml] + $masukan;
				
				$saldoawal	= $nquantityakhir[$jml];
				
				if($saldoawal=='')
					$saldoawal	= 0;
					
				$saldoakhir	= $qty-$keluaran;
				
				if($saldoakhir=='')
					$saldoakhir	= 0;
					
				if($enote[$jml]!='') {
					$db2->query(" UPDATE tm_stokopname_item SET n_quantity_awal='$saldoawal', n_quantity_akhir='$saldoakhir', e_note='$enote[$jml]' WHERE i_so_item='$isoitem[$jml]' AND i_so='$iso[$jml]' AND i_product='$iproduct[$jml]' ");
				}else{
					$db2->query(" UPDATE tm_stokopname_item SET n_quantity_awal='$saldoawal', n_quantity_akhir='$saldoakhir' WHERE i_so_item='$isoitem[$jml]' AND i_so='$iso[$jml]' AND i_product='$iproduct[$jml]' ");
				}
				
				$qopname_lama	= $db2->query(" SELECT n_quantity_akhir FROM tm_stokopname_item WHERE i_so='$iso_lama' AND i_product='$iproduct[$jml]' ");
				
				if($qopname_lama->num_rows()>0) {
					
					$ropname_lama	= $qopname_lama->row();
					
					$n_quantity_akhir_lama = $ropname_lama->n_quantity_akhir;
					
					if($n_quantity_akhir_lama=='')
						$n_quantity_akhir_lama = 0;	
					
					$db2->query(" UPDATE tm_stokmutasi SET n_quantity_trans='$n_quantity_akhir_lama', n_quantity_fisik='$saldoawal', d_update='$dentry' WHERE f_active_month='t' AND f_stop_produksi='$stp' AND i_product='$iproduct[$jml]' ");
					
				}else{
					$db2->query(" UPDATE tm_stokmutasi SET n_quantity_fisik='$saldoawal', d_update='$dentry' WHERE f_active_month='t' AND f_stop_produksi='$stp' AND i_product='$iproduct[$jml]' ");
				}
				
				// 23-10-2014, update stok per warna
				for ($xx=0; $xx<count($i_color[$jml]); $xx++) {
					if (is_array($i_color[$jml])) {
						/*$stokwarna	= array(
							'i_so_item'=>$i_so_item[$j],
							'i_color'=>$i_color[$j][$xx],
							'n_quantity_awal'=>$stok_fisik[$j][$xx],
							'n_quantity_akhir'=>$stok_fisik[$j][$xx]
						);
						$db2->insert('tm_stokopname_item_color',$stokwarna); */
						
						$qbonmkeluar	= $db2->query("
						SELECT c.i_color, SUM(c.qty) AS jbonkeluar FROM tm_outbonm_item a
							INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm 
							INNER JOIN tm_outbonm_item_color c ON c.i_outbonm_item = a.i_outbonm_item
							
							WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
							AND b.f_outbonm_cancel='f' AND c.i_color = '".$i_color[$jml][$xx]."' GROUP BY c.i_color ");
							
						if($qbonmkeluar->num_rows()>0) {	
							$rbonmkeluar	= $qbonmkeluar->row();
							if($rbonmkeluar->jbonkeluar!='') {
								$jjbonkeluarx	= $rbonmkeluar->jbonkeluar;
							}else{
								$jjbonkeluarx	= 0;
							}
						}else{
							$jjbonkeluarx	= 0;
						}
						
						$qbonmmasuk		= $db2->query("
						SELECT c.i_color, SUM(c.qty) AS jbonmasuk FROM tm_inbonm_item a
							INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
							INNER JOIN tm_inbonm_item_color c ON c.i_inbonm_item = a.i_inbonm_item
							WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
							AND b.f_inbonm_cancel='f' AND c.i_color = '".$i_color[$jml][$xx]."' GROUP BY c.i_color ");
							
						if($qbonmmasuk->num_rows()>0) {	
							$rbonmmasuk	= $qbonmmasuk->row();
							if($rbonmmasuk->jbonmasuk!='') {
								$jjbonmasukx	= $rbonmmasuk->jbonmasuk;
							}else{
								$jjbonmasukx	= 0;
							}
						}else{
							$jjbonmasukx	= 0;
						}
						
						$qdo	= $db2->query( "
						SELECT c.i_color, SUM(c.qty) AS jdo FROM tm_do_item a
						INNER JOIN tm_do b ON b.i_do=a.i_do
						INNER JOIN tm_do_item_color c ON c.i_do_item=a.i_do_item
						WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]' 
						AND b.f_do_cancel='f' AND c.i_color = '".$i_color[$jml][$xx]."' GROUP BY c.i_color ");
						
						if($qdo->num_rows()>0) {
							
							$rdo	= $qdo->row();
							
							if($rdo->jdo!='') {
								$jjdox	= $rdo->jdo;
							}else{
								$jjdox	= 0;
							}
						}else{
							$jjdox	= 0;
						}
						
						$masukanx	= $jjbonmasukx;
						$keluaranx	= $jjdox + $jjbonkeluarx;
						$saldoakhirx = $stok_fisik[$jml][$xx] + $masukanx - $keluaranx;
						
						$saldoawalx	= $stok_fisik[$jml][$xx];
						if($saldoawalx=='')
							$saldoawalx	= 0;
						//$saldoakhir	= $qty-$keluaran;
						
						if($saldoakhirx=='')
							$saldoakhirx	= 0;
							
							$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_awal='$saldoawalx', 
											n_quantity_akhir='$saldoakhirx' WHERE i_so_item='$isoitem[$jml]' 
											AND i_color='".$i_color[$jml][$xx]."' ");						
					} // END IF 
				} // end FOR WARNA
				// ---------------------------------
				
				$jml++;
			}
			
			$db2->query(" UPDATE tm_stokopname SET d_update='$dupdate' WHERE f_stop_produksi='$stp' AND i_status_so='0' AND f_updated='f' ");
			
		}else{
			print "<script>alert(\"Opname gagal diupdate, opname tsb bln yg lalu.\");show(\"listlaporanstok/back/$stp/\",\"#content\");</script>";
		}

	}
		
	function mupdate_old($stp,$blnso,$thnso,$iproduct,$nquantityakhir,$isoitem,$iso,$iteration,$stp,$enote,$totalhari,$iso_lama){
$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
			
		$jml=0;	
		
		$blnopname	= date("m");
		$dupdate	= date("Y-m-d");
		
		if($blnopname==$blnso) {
		
			while($jml<=$iteration) {
			
				$tg_1		= "01";
				$tg_skrng	= date("d");
				//$tg_30	= 30;
				$tg_30		= $totalhari;
				
				$dfirst	= $thnso."-".$blnso."-".$tg_1; // Y-m-d
				//$dlast	= $thnso."-".$blnso."-".$tg_skrng;
				$dlast	= $thnso."-".$blnso."-".$tg_30;
				
				$qbonmkeluar	= $db2->query("
				SELECT a.i_product, sum(a.n_count_product) AS jbonkeluar FROM tm_outbonm_item a
					INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm 
					
					WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
					
				if($qbonmkeluar->num_rows()>0){	
					$rbonmkeluar	= $qbonmkeluar->row();
					if($rbonmkeluar->jbonkeluar!=''){
						$jjbonkeluar	= $rbonmkeluar->jbonkeluar;
					}else{
						$jjbonkeluar	= 0;
					}
				}else{
					$jjbonkeluar	= 0;
				}
				
				$qbonmmasuk		= $db2->query("
				SELECT a.i_product, sum(a.n_count_product) AS jbonmasuk FROM tm_inbonm_item a
					INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
					
					WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
					
				if($qbonmmasuk->num_rows()>0){	
					$rbonmmasuk	= $qbonmmasuk->row();
					if($rbonmmasuk->jbonmasuk!=''){
						$jjbonmasuk	= $rbonmmasuk->jbonmasuk;
					}else{
						$jjbonmasuk	= 0;
					}
				}else{
					$jjbonmasuk	= 0;
				}
				
				$qbbk	= $db2->query("
				SELECT a.i_product, sum(a.n_unit) AS jbbk FROM tm_bbk_item a
					INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
					
					WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
				
				if($qbbk->num_rows()>0){	
					$rbbk	= $qbbk->row();
					if($rbbk->jbbk!=''){
						$jjbbk	= $rbbk->jbbk;
					}else{
						$jjbbk	= 0;
					}
				}else{
					$jjbbk	= 0;
				}
							
				$qbbm	= $db2->query("
				SELECT a.i_product, sum(a.n_unit) AS jbbm FROM tm_bbm_item a
					INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying) 
					
					WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]'
					
					GROUP BY a.i_product ");
				
				if($qbbm->num_rows()>0){	
					$rbbm	= $qbbm->row();
					if($rbbm->jbbm!=''){
						$jjbbm	= $rbbm->jbbm;
					}else{
						$jjbbm	= 0;
					}
				}else{
					$jjbbm	= 0;
				}
							
				$qdo	= $db2->query( "
				SELECT a.i_product, sum(a.n_deliver) AS jdo FROM tm_do_item a
				INNER JOIN tm_do b ON b.i_do=a.i_do
				
				WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$iproduct[$jml]' AND b.f_do_cancel='f'
				
				GROUP BY a.i_product ");
				
				if($qdo->num_rows()>0){	
					
					$rdo	= $qdo->row();
					
					if($rdo->jdo!=''){
						$jjdo	= $rdo->jdo;
					}else{
						$jjdo	= 0;
					}
				}else{
					$jjdo	= 0;
				}
				
				$masukan	= $jjbonmasuk + $jjbbm;
				$keluaran	= $jjdo + $jjbonkeluar + $jjbbk;
				$qty		= $nquantityakhir[$jml] + $masukan;
				
				$saldoawal	= $nquantityakhir[$jml];
				
				if($saldoawal=='')
					$saldoawal	= 0;
					
				$saldoakhir	= $qty - $keluaran;
				
				if($saldoakhir=='')
					$saldoakhir	= 0;
					
				if($enote[$jml]!=''){
					$db2->query(" UPDATE tm_stokopname_item SET n_quantity_awal='$saldoawal', n_quantity_akhir='$saldoakhir', e_note='$enote[$jml]' WHERE i_so_item='$isoitem[$jml]' AND i_so='$iso[$jml]' AND i_product='$iproduct[$jml]' ");
				}else{
					$db2->query(" UPDATE tm_stokopname_item SET n_quantity_awal='$saldoawal', n_quantity_akhir='$saldoakhir' WHERE i_so_item='$isoitem[$jml]' AND i_so='$iso[$jml]' AND i_product='$iproduct[$jml]' ");
				}
				
				$qopname_lama	= $db2->query(" SELECT n_quantity_akhir FROM tm_stokopname_item WHERE i_so='$iso_lama' AND i_product='$iproduct[$jml]' ");
				if($qopname_lama->num_rows()>0){
					$ropname_lama	= $qopname_lama->row();
					if($ropname_lama->n_quantity_akhir==''){
						$n_quantity_akhir_lama = 0;	
					}else{
						$n_quantity_akhir_lama = $ropname_lama->n_quantity_akhir;
					}
					$db2->query(" UPDATE tm_stokmutasi SET n_quantity_trans='$n_quantity_akhir_lama', n_quantity_fisik='$saldoawal', d_update='$dentry' WHERE f_active_month='t' AND f_stop_produksi='$stp' AND i_product='$iproduct[$jml]' ");
				}else{
					$db2->query(" UPDATE tm_stokmutasi SET n_quantity_fisik='$saldoawal', d_update='$dentry' WHERE f_active_month='t' AND f_stop_produksi='$stp' AND i_product='$iproduct[$jml]' ");
				}
				$jml++;
			}
			
			//$db2->query(" UPDATE tm_stokopname SET f_updated='t', d_update='$dupdate' WHERE f_stop_produksi='$stp' AND i_status_so='0' AND f_updated='f' ");
			$db2->query(" UPDATE tm_stokopname SET d_update='$dupdate' WHERE f_stop_produksi='$stp' AND i_status_so='0' AND f_updated='f' ");
			
		} else {
			print "<script>alert(\"Opname gagal diupdate, opname tsb bln yg lalu.\");show(\"listlaporanstok/back/$stp/\",\"#content\");</script>";
		}

	}	
}
?>
