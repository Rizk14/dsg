<?php
class Master_barang_bb_model extends MY_Model
{
	protected $_per_page = 50;
    protected $_tabel = 'tb_master_barang_bb';
    protected $form_rules = array(
        array(
            'field' => 'nama_barang_bb',
            'label' => 'Nama Barang BB',
            'rules' => 'trim|required|max_length[64]'
        ),
        array(
            'field' => 'kode_barang_bb',
            'label' => 'Kode Barang BB',
            'rules' => 'trim|required|max_length[10]'
        ),
        
    );

     public $default_values = array(
		'nama_barang_bb' => '',
        'kode_barang_bb' => '',
        'id'	=>	'',
		'num'		=>1,
    );
    
        public function input($kode_barang_bb,$nama_barang_bb,$id_barang_wip,$id_kelompok_bb)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 
		$data= array(
		'kode_barang_bb' => $kode_barang_bb,
		'nama_barang_bb' => $nama_barang_bb,
		'id_barang_wip' => $id_barang_wip,
		'id_kelompok_bb' => $id_kelompok_bb,
		'created_at'=> $created_at,
		'updated_at'=> $updated_at
		);
		
        $id= $this->db->insert('tb_master_barang_bb',$data);
	
        if ($id) {
		 return true;
		}
		 return false;

    }
     public function cari($offset)
    {
        $this->get_real_offset($offset);
        $kata_kunci = $this->input->get('kata_kunci', true);
       
        return $this->db->where("( kode_barang_bb LIKE '%$kata_kunci%' OR nama_barang_bb LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->result();
    }
    
     public function cari_num_rows()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
          return $this->db->where("( kode_barang_bb LIKE '%$kata_kunci%' OR nama_barang_bb LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->num_rows();
    }
    
    public function edit($id, $nama_barang_bb,$kode_barang_bb,$segment)
    {
     $updated_at = date('Y-m-d H:i:s');
     $this->db->where('id', $id);
     $data = array(
        'nama_barang_bb' => $nama_barang_bb,
       'kode_barang_bb' => $kode_barang_bb,
       'segment' => $segment,
       'updated_at' => $updated_at
);
    $this->db->update('tb_master_barang_bb', $data);
    return true;
    }
    
     public function master_barang_wip()
    {
     $this->db->select("* FROM tb_master_barang_wip where TRUE" );
     $query = $this->db->get();
     if ($query->num_rows > 0){
		$hasilrow=$query->result();
		
		return $hasilrow;
		 }
    }
    
     public function master_kelompok_bb()
    {
     $this->db->select("* FROM tb_master_kelompok_bb where TRUE" );
     $query = $this->db->get();
     if ($query->num_rows > 0){
		$hasilrow=$query->result();
		
		return $hasilrow;
		 }
    }
    
      public function master_cari_kelompok_bb($id_kel)
    {
     $this->db->select("* FROM tb_master_kelompok_bb where id='$id_kel'" );
     $query = $this->db->get();
     if ($query->num_rows > 0){
		$hasilrow=$query->row();
		//~ $nama_kelompok_bb=$hasilrow->nama_kelompok_bb;
		//~ $kode_kelompok_bb=$hasilrow->kode_kelompok_bb;
		$segment=$hasilrow->segment;
		$query2 =	$this->db->query("select max(substr(kode_barang_bb,4,4)) as max  from tb_master_barang_bb 
		where kode_barang_bb like 'D".$segment."%' group by kode_barang_bb order by kode_barang_bb  DESC limit 1 ");
		 if ($query2 ->num_rows()>0){
			 $hasilrow2=$query2->row();
			 $kd_ls=$hasilrow2->max;
			 $no_kd=$kd_ls+1;
			 settype($no_kd,"string");
			 $a=strlen($no_kd);
			while($a<4){
			  $no_kd="0".$no_kd;
			  $a=strlen($no_kd);
			}
			 $kode_last="D".$segment.$no_kd;
			 
			 }
		 }
		 else{
			$segment=''; 
			 }
		  $values[]=array(
		'nama_kelompok_bb'=>$hasilrow->nama_kelompok_bb,
		'kode_kelompok_bb'=>$hasilrow->kode_kelompok_bb,
		'segment'=>$segment,
		 'kode_last'=>$kode_last
		 );
		 return $values;
    }
    
    public function get_all_paged_inner($offset)
    {
		$pencarian="";
		$kata_kunci = $this->input->get('kata_kunci', true);
		if($kata_kunci != null){
			$pencarian.=" where a.kode_barang_bb LIKE '%$kata_kunci%' OR a.nama_barang_bb LIKE '%$kata_kunci%'";
			}
        $this->db->select("a.*,b.nama_barang_wip,b.kode_barang_wip,c.kode_kelompok_bb,c.nama_kelompok_bb FROM tb_master_barang_bb a 
        inner join tb_master_barang_wip b ON b.id=a.id_barang_wip 
        inner join tb_master_kelompok_bb c ON c.id=a.id_kelompok_bb ".$pencarian )->limit($this->_per_page,$offset);
     $query = $this->db->get();
     if ($query->num_rows > 0){
		$hasil=$query->result();
		foreach ($hasil as $hrw){
			$data[]=array(
			'id' => $hrw->id,
			'nama_barang_bb' =>$hrw->nama_barang_bb,
			'kode_barang_bb' =>$hrw->kode_barang_bb,
			'created_at'	 =>$hrw->created_at,
			'updated_at'	 =>$hrw->updated_at,
			'nama_barang_wip'=>$hrw->nama_barang_wip,
			'kode_barang_wip'=>$hrw->kode_barang_wip,
			'nama_kelompok_bb'=>$hrw->nama_kelompok_bb,
			'kode_kelompok_bb'=>$hrw->kode_kelompok_bb,
				);
			
			}
		return $data;
		 }
    }
    
     public function get_all_paged_inner_cetak()
    {
		$pencarian="";
		$kata_kunci = $this->input->get('kata_kunci', true);
		if($kata_kunci != null){
			$pencarian.=" AND kode_barang_bb LIKE '%$kata_kunci%' OR nama_barang_bb LIKE '%$kata_kunci%'";
			}
        $this->db->select("a.*,b.nama_barang_wip,b.kode_barang_wip,c.kode_kelompok_bb,c.nama_kelompok_bb FROM tb_master_barang_bb a 
        inner join tb_master_barang_wip b ON b.id=a.id_barang_wip 
        inner join tb_master_kelompok_bb c ON c.id=a.id_kelompok_bb where TRUE".$pencarian );
     $query = $this->db->get();
     if ($query->num_rows > 0){
		$hasil=$query->result();
		foreach ($hasil as $hrw){
			$data[]=array(
			'id' => $hrw->id,
			'nama_barang_bb' =>$hrw->nama_barang_bb,
			'kode_barang_bb' =>$hrw->kode_barang_bb,
			'created_at'	 =>$hrw->created_at,
			'updated_at'	 =>$hrw->updated_at,
			'nama_barang_wip'=>$hrw->nama_barang_wip,
			'kode_barang_wip'=>$hrw->kode_barang_wip,
			'nama_kelompok_bb'=>$hrw->nama_kelompok_bb,
			'kode_kelompok_bb'=>$hrw->kode_kelompok_bb,
				);
			
			}
		return $data;
		 }
    }
    
}
