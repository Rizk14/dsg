<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
    
  function getAll($num, $offset, $cari) {	  

	if ($cari == "all") {
		/*distinct b.no_bonm, b.tgl_bonm, b.tgl_update, a.id_gudang FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 'f' ORDER BY b.no_bonm DESC, a.id_gudang ASC */
		
		$sql = " distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
			tm_apply_stok_hasil_cutting_detail b
			WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't' ORDER BY b.no_bonm DESC ";
		
		//$this->db->select(" b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok FROM tm_apply_stok_hasil_cutting b 
		//	INNER JOIN tm_apply_stok_hasil_cutting_detail a ON b.id=a.id_apply_stok WHERE a.status_stok='t' GROUP BY b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok ");
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();	
	}
	else {
		//$this->db->query(" b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok FROM tm_apply_stok_hasil_cutting b
		//	INNER JOIN tm_apply_stok_hasil_cutting_detail a ON b.id=a.id_apply_stok WHERE a.status_stok='t' AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(a.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(a.kode_brg) LIKE UPPER('%$cari%')) GROUP BY b.id_realisasi_cutting, b.id, b.no_bonm, b.tgl_bonm, b.tgl_update, b.status_stok ");
		$sql = " distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
				tm_apply_stok_hasil_cutting_detail b
				WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't' 
				AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(b.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(b.kode_brg) LIKE UPPER('%$cari%'))
				ORDER BY b.no_bonm DESC ";
		$this->db->select($sql, false)->limit($num,$offset);
		$query = $this->db->get();
	}

	$data_fb = array();
	$detail_fb = array();
		
	if ($query->num_rows() > 0){
			
		$hasil = $query->result();
			
		foreach ($hasil as $row1) {
				
			/*$qrealisasi	= $this->db->query(" SELECT a.no_realisasi, a.tgl_realisasi, b.diprint FROM tt_realisasi_cutting a, 
						tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting 
						AND b.id='$row1->id_realisasi_cutting_detail' ");
			$rrealisasi	= $qrealisasi->row();
				
			$no_realisasi	= $rrealisasi->no_realisasi;
			$tgl_realisasi	= $rrealisasi->tgl_realisasi;
			$diprint	= $rrealisasi->diprint; */
				
			/*$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail WHERE id_apply_stok='$row1->id' 
								AND status_stok = 't' "); */
			
			$query2	= $this->db->query(" SELECT b.* FROM tm_apply_stok_hasil_cutting_detail b, tm_apply_stok_hasil_cutting a 
				WHERE a.id = b.id_apply_stok AND b.no_bonm = '$row1->no_bonm' AND b.status_stok = 't' ");
				
			if ($query2->num_rows() > 0) {
				$hasil2=$query2->result();
				foreach ($hasil2 as $row2) {
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");										
					if($query3->num_rows() > 0){
						$hasilrow	= $query3->row();
						$kode_brg	= $row2->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
					}
					else {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE a.satuan=b.id AND a.kode_brg='$row2->kode_brg' ");
						if($query3->num_rows() > 0){
							$hasilrow	= $query3->row();
							$kode_brg	= $row2->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
						}
					}
						
					$query4	= $this->db->query(" SELECT i_product_motif, e_product_motifname FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
					if($query4->num_rows()>0){
						$hasilrow2	= $query4->row();
						$iproductmotif	= $hasilrow2->i_product_motif;
						$productname	= $hasilrow2->e_product_motifname;
					}else{
						$iproductmotif	= '';
						$productname	= '';
					}
					
					$qrealisasi	= $this->db->query(" SELECT a.no_realisasi, a.tgl_realisasi, b.diprint FROM tt_realisasi_cutting a, 
						tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting 
						AND b.id='$row2->id_realisasi_cutting_detail' ");
					$rrealisasi	= $qrealisasi->row();
						
					$no_realisasi	= $rrealisasi->no_realisasi;
					$tgl_realisasi	= $rrealisasi->tgl_realisasi;
					$diprint	= $rrealisasi->diprint;
					
					$query5	= $this->db->query(" SELECT hasil_potong FROM tt_realisasi_cutting_detail WHERE id='$row2->id_realisasi_cutting_detail' ");
					if($query5->num_rows()>0){
						$hasilrow3	= $query5->row();
						$hasil_potong	= $hasilrow3->hasil_potong;
					}else{
						$hasil_potong	= '';
					}
						
					$detail_fb[] = array('id' => $row2->id,
						'kode_brg_jadi' => $iproductmotif,
						'nama_brg_motif' => $productname,
						'kode_brg' => $kode_brg,
						'nama_brg' => $nama_brg,
						'diprint' => $diprint,
						'qty' => $hasil_potong );
				}
			}
			else {
				$detail_fb = '';
			}			 
				 
			$data_fb[] = array(
				'no_realisasi' => $no_realisasi,
				'tgl_realisasi' => $tgl_realisasi,
				'no_bonm' => $row1->no_bonm,	
				'tgl_bonm' => $row1->tgl_bonm,
				'tgl_update' => $row1->tgl_update,
				//'status_stok' => $row1->status_stok,
				'detail_fb' => $detail_fb );
					
			$detail_fb = array();
			$id_detailnya = "";
			
		}
	}
	else {
		$data_fb = '';
	}
	return $data_fb;
  }
  
  function getAlltanpalimit($cari){
	if ($cari=="all") {
		/*$query	= $this->db->query(" SELECT a.kode_brg_jadi, a.kode_brg, a.qty FROM tm_apply_stok_hasil_cutting_detail a 
			INNER JOIN tm_apply_stok_hasil_cutting b ON b.id=a.id_apply_stok WHERE a.status_stok='t' GROUP BY a.kode_brg_jadi, a.kode_brg, a.qty "); */
		$sql = " select distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
					tm_apply_stok_hasil_cutting_detail b
					WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't'";
		$query	= $this->db->query($sql);
	}
	else {
		/*$query	= $this->db->query(" SELECT a.kode_brg_jadi, a.kode_brg, a.qty FROM tm_apply_stok_hasil_cutting_detail a 
			INNER JOIN tm_apply_stok_hasil_cutting b ON b.id=a.id_apply_stok WHERE a.status_stok='t' AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(a.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(a.kode_brg) LIKE UPPER('%$cari%')) GROUP BY a.kode_brg_jadi, a.kode_brg, a.qty "); */
		$sql = " select distinct b.no_bonm, b.tgl_bonm, b.tgl_update FROM tm_apply_stok_hasil_cutting a, 
				tm_apply_stok_hasil_cutting_detail b
				WHERE a.id = b.id_apply_stok AND a.no_bonm <> b.no_bonm AND b.status_stok = 't' 
				AND (UPPER(b.no_bonm) like UPPER('%$cari%') OR UPPER(b.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(b.kode_brg) LIKE UPPER('%$cari%'))";
		$query	= $this->db->query($sql);
	}
    return $query->result();  
  }
      
  function cek_data($no_bonm_manual, $thn1){
    $this->db->select("id from tm_bonmmasukcutting WHERE no_manual = '$no_bonm_manual' 
				AND extract(year from tgl_bonm) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_sj($num, $offset, $csupplier, $id_gudang, $cari) {
	if ($cari == '') {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail brg-nya

				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id' 
				AND a.status_stok = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama FROM tm_barang a, tm_satuan b
								WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama;
							
						$detail_sj[] = array('id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'qty'=> $row2->qty,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_sjtanpalimit($csupplier, $cari){
	if ($cari == '') { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ");
	}
    
    return $query->result();  
  }
 
  
  function get_detail_bonm() {
	  
    $headernya = array();    
    $detail_brg = array();    
    
    $query	= $this->db->query(" SELECT a.* FROM tm_apply_stok_hasil_cutting a, tt_realisasi_cutting b WHERE 
					a.id_realisasi_cutting = b.id AND b.for_eksternal = 'f' AND 
					a.status_stok='f' ORDER BY a.tgl_bonm ASC, a.no_bonm ASC ");
    
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach($hasil as $row1) {
			$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_hasil_cutting_detail WHERE status_stok='f'	AND id_apply_stok='$row1->id' ORDER BY id ASC ");  
			
					if ($query2->num_rows() > 0){
						
						$hasil2=$query2->result();
						
						foreach ($hasil2 as $row2) {
							
							$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_barang a	WHERE a.kode_brg='$row2->kode_brg' ");
							if($query3->num_rows()>0){
								$hasilrow = $query3->row();
								$kode_brg	= $row2->kode_brg;
								$nama_brg	= $hasilrow->nama_brg;
							}else{
								$query3	= $this->db->query(" SELECT a.nama_brg FROM tm_brg_hasil_makloon a
										WHERE a.kode_brg='$row2->kode_brg' ");
								if($query3->num_rows()>0){
									$kode_brg	= $row2->kode_brg;
									$nama_brg	= $hasilrow->nama_brg;
								}
								else {
									$kode_brg	= '';
									$nama_brg	= '';	
								}
							}
							
							$query5	= $this->db->query(" SELECT id, qty_realisasi, jum_gelar, hasil_potong, diprint 
											FROM tt_realisasi_cutting_detail WHERE id='$row2->id_realisasi_cutting_detail' ");
							$row3	= $query5->row();
							
							$detail_brg[] = array(		
								'id' => $row2->id,
								'kode_brg' => $kode_brg,
								'nama' => $nama_brg,
								'qty' => $row2->qty,
								'hasil_potong' => $row3->hasil_potong, // ini yg dipake, bukan qty_realisasi
								'id_realisasi_cutting_detail' => $row3->id,
								'jum_gelar' => $row3->jum_gelar,
								'diprint' => $row3->diprint );
						}
					}
					else
						$detail_brg = '';

				$query4	= $this->db->query(" SELECT * FROM tt_realisasi_cutting WHERE id='$row1->id_realisasi_cutting' ");
				if($query4->num_rows()>0) {
						$hasilrow = $query4->row();
						$no_realisasi	= $hasilrow->no_realisasi;
						$tgl_realisasi	= $hasilrow->tgl_realisasi;
				} else {
						$no_realisasi	= '';
						$tgl_realisasi	= '';
				}
				
				$headernya[] = array('id' => $row1->id,
							'id_realisasi_cutting' => $row1->id_realisasi_cutting,
							'no_bonm' => $row1->no_bonm,
							'tgl_bonm' => $row1->tgl_bonm,
							'tgl_update' => $row1->tgl_update,
							'no_realisasi' => $no_realisasi,
							'tgl_realisasi' => $tgl_realisasi,
							'detail_brg' => $detail_brg);
				
				$detail_brg = array();
		}
		
	} else {
			$headernya = '';
	}
	return $headernya;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
  function get_bonm($id_apply_stok){
	$query	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian WHERE id = '$id_apply_stok' ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	$detail_bonm = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b, tm_pembelian c, 
				tm_apply_stok_pembelian d 
				WHERE a.kode_brg = b.kode_brg AND a.id_pembelian = c.id AND d.no_sj = c.no_sj 
				AND d.kode_supplier = c.kode_supplier AND b.id_gudang = '$row1->id_gudang' 
				AND c.no_sj = '$row1->no_sj' 
				AND d.kode_supplier = '$row1->kode_supplier' ");  
		
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id'
								AND kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$cek = 'y';
						}
						else
							$cek = 't';
						
						$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE 
									kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE 
									kode_brg = '$row2->kode_brg' AND kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$harga	= $hasilrow->harga;
				
						$detail_bonm[] = array(		
										'id'=> $row2->id,
										'kode_brg'=> $row2->kode_brg,
										'nama'=> $nama_brg,
										'harga'=> $harga,
										'qty'=> $row2->qty,
										'cek'=> $cek
										);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$query2	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE 
									kode_supplier = '$row1->kode_supplier' AND no_sj = '$row1->no_sj' ");
						$hasilrow = $query2->row();
						$tgl_sj	= $hasilrow->tgl_sj;
				
				$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				}
				 
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,	
											'tgl_sj'=> $tgl_sj,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
	}
	return $data_bonm;
  }
  
  function delete($no_bonm){   
	  $tgl = date("Y-m-d");
	   
	  $sql = "SELECT a.id_realisasi_cutting, b.* FROM tm_apply_stok_hasil_cutting a, 
					tm_apply_stok_hasil_cutting_detail b WHERE a.id = b.id_apply_stok
					AND b.no_bonm = '$no_bonm'
					AND b.status_stok = 't' "; 
							
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sj;
				$id_realisasi_cutting = $row4->id_realisasi_cutting;
				$id_apply_stok = $row4->id_apply_stok;
				$id_detail = $row4->id;
				$kode_brg = $row4->kode_brg;
				$kode_brg_jadi = $row4->kode_brg_jadi;
				$id_realisasi_cutting_detail = $row4->id_realisasi_cutting_detail;
				$qty_hasil_potong = $row4->qty; // new 29-02-2012
				
				$query3	= $this->db->query(" SELECT a.is_dacron, b.* FROM tt_realisasi_cutting a, 
											tt_realisasi_cutting_detail b WHERE a.id = b.id_realisasi_cutting AND
											b.id='$id_realisasi_cutting_detail' ");
						
				if($query3->num_rows()>0){
					$ridrealisasi	= $query3->row();
					$id_realisasi	= $ridrealisasi->id_realisasi_cutting;
					$jum_gelar		= $ridrealisasi->jum_gelar;
					$diprint		= $ridrealisasi->diprint;
					$is_dacron		= $ridrealisasi->is_dacron;
				}
								
				//2. ambil stok terkini di tm_stok 
				$query3	= $this->db->query(" SELECT * FROM tm_stok_hasil_cutting WHERE 
												kode_brg_jadi='$kode_brg_jadi' AND kode_brg='$kode_brg' AND diprint = '$diprint'
												AND is_dacron = '$is_dacron' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_stok = $hasilrow->id;
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty_hasil_potong;
																									
				//4. update stok di tm_stok_hasil_cutting
				$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where id= '$id_stok' ");
																			
				$this->db->query(" UPDATE tt_realisasi_cutting SET status_apply_stok = 'f' where id = '$id_realisasi' ");
				$this->db->query(" UPDATE tt_realisasi_cutting_detail SET status_apply_stok = 'f' where id = '$id_realisasi_cutting_detail' ");
													
				$this->db->query(" UPDATE tm_apply_stok_hasil_cutting SET 
														status_stok = 'f', tgl_update = '$tgl'
														where id= '$id_apply_stok'  ");
														
				$this->db->query(" UPDATE tm_apply_stok_hasil_cutting_detail SET 
														no_bonm = '', tgl_bonm = NULL, status_stok = 'f'
														where id= '$id_detail'  ");
		}
	  }
										
		
  }
  
  // =================================== 15-07-2013 ======================================================
  function getAllbonm($num, $offset, $cari, $date_from, $date_to, $jenis, $sumber, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(a.no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			if ($jenis != "0")
				$pencarian.= " AND a.jenis_masuk = '$jenis' ";
			if ($sumber != "0")
				$pencarian.= " AND a.sumber = '$sumber' ";
			if ($filterbrg == "y" && $caribrg !="all")
				$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')  
							OR UPPER(b.nama_brg_wip) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
			$pencarian.= " ORDER BY a.tgl_bonm DESC, a.id ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			if ($jenis != "0")
				$pencarian.= " AND a.jenis_masuk = '$jenis' ";
			if ($sumber != "0")
				$pencarian.= " AND a.sumber = '$sumber' ";
			if ($filterbrg == "y" && $caribrg !="all")
				$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')  
							OR UPPER(b.nama_brg_wip) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
			$pencarian.= " ORDER BY a.tgl_bonm DESC, a.id ";
		}
		
		$this->db->select(" distinct a.* FROM tm_bonmmasukcutting a LEFT JOIN tm_bonmmasukcutting_detail b ON a.id=b.id_bonmmasukcutting 
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
								OR UPPER(a.nama_brg_wip) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
								
				// ambil data detail barangnya
				//SELECT a.* FROM tm_pembelian_detail a INNER JOIN tm_barang b ON a.id_brg = b.id
				//			WHERE a.id_pembelian = '$row1->id' ".$pencarian2." ORDER BY id ASC
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_bonmmasukcutting_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id 
									WHERE a.id_bonmmasukcutting = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						// detail qty warna -------------
						$strwarna = "";
						$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_bonmmasukcutting_detail_warna a 
										INNER JOIN tm_warna c ON a.id_warna = c.id
										WHERE a.id_bonmmasukcutting_detail = '$row2->id' ORDER BY c.nama ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						}
						//-------------------------------------------
																
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'detail_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_bonm = '';
				}
																
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Masuk Bagus";
				else if ($row1->jenis_masuk == '2')
					$nama_jenis = "Lain-Lain";
				else
					$nama_jenis = "";
				
				if ($row1->sumber == '1')
					$nama_sumber = "Langsung Dari Hasil Cutting";
				else if ($row1->sumber == '2')
					$nama_sumber = "Dari Pengesetan";
				else
					$nama_sumber = "";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update'=> $row1->tgl_update,
											'jenis_masuk'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'sumber'=> $row1->sumber,
											'nama_sumber'=> $nama_sumber,
											'keterangan'=> $row1->keterangan,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function getAllbonmtanpalimit($cari, $date_from, $date_to, $jenis, $sumber, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND (UPPER(no_manual) like UPPER('%$cari%'))";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			if ($jenis != "0")
				$pencarian.= " AND jenis_masuk = '$jenis' ";
			if ($sumber != "0")
				$pencarian.= " AND sumber = '$sumber' ";
			if ($filterbrg == "y" && $caribrg !="all")
				$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')  
							OR UPPER(b.nama_brg_wip) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
			$pencarian.= " ORDER BY tgl_bonm DESC, id ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_bonm >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_bonm <= to_date('$date_to','dd-mm-yyyy') ";
			if ($jenis != "0")
				$pencarian.= " AND jenis_masuk = '$jenis' ";
			if ($sumber != "0")
				$pencarian.= " AND sumber = '$sumber' ";
			if ($filterbrg == "y" && $caribrg !="all")
				$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')  
							OR UPPER(b.nama_brg_wip) like UPPER('%".$this->db->escape_str($caribrg)."%') ) ";
			$pencarian.= " ORDER BY tgl_bonm DESC, id ";
		}
		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_bonmmasukcutting a LEFT JOIN tm_bonmmasukcutting_detail b ON a.id=b.id_bonmmasukcutting 
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  function getbarangjaditanpalimit($cari){
  	if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(i_product_motif) LIKE UPPER('%$cari%') OR UPPER(i_product) LIKE UPPER('%$cari%') OR UPPER(e_product_motifname) LIKE UPPER('%$cari%') )";
	}
	$sql = "SELECT * FROM tr_product_motif WHERE n_active='1' ".$pencarian." ORDER BY i_product_motif ASC";		
  	$query	= $this->db->query($sql);
	return $query->result();
  }
  
  function getbarangjadi($num,$offset, $cari){
  	if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(i_product_motif) LIKE UPPER('%$cari%') OR UPPER(i_product) LIKE UPPER('%$cari%') 
						OR UPPER(e_product_motifname) LIKE UPPER('%$cari%') )";
	}
	
	$sql = "* FROM tr_product_motif WHERE n_active='1' ".$pencarian." ORDER BY i_product_motif ASC";	  
	
	$this->db->select($sql, false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result(); 
  }
  
  function get_bahan($num, $offset, $cari, $is_quilting)
  {
	if ($cari == "all") {
		if ($is_quilting == 'f') {		
			$sql = " a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					ORDER BY a.nama_brg ";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't'  ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
		}
		else {
			$sql = " a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
						
			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'tgl_update'=> $row1->tgl_update
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $is_quilting){
	if ($cari == "all") {
		if ($is_quilting == 'f') {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.status_aktif = 't' 
					ORDER BY a.nama_brg ";
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ORDER BY a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		if ($is_quilting == 'f') {		
			$sql = " select a.*, b.nama as nama_satuan 
				FROM tm_barang a, tm_satuan b
							WHERE a.satuan = b.id
							AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg ";
		
		}
		else {
			$sql = " select a.*, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b
					WHERE a.satuan = b.id AND a.status_aktif = 't' ";
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		}
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  //function savebonm($no_bonm,$tgl_bonm, $no_bonm_manual, $jenis, $ket, 
  function savebonm($id_bonm, $jenis, $ket, 
			$id_brg_wip, $nama_brg_wip, $id_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
				
			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotal = 0;
			for ($xx=0; $xx<count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$qty_warna[$xx] = trim($qty_warna[$xx]);
				$qtytotal+= $qty_warna[$xx];
			} // end for
				
				// ======== update stoknya! =============
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting WHERE id_brg_wip = '$id_brg_wip' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting, insert
						$data_stok = array(
							'id_brg_wip'=>$id_brg_wip,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' ");
					}
				
				// jika semua data tdk kosong, insert ke tm_bonmmasukcutting_detail
				$data_detail = array(
					'id_bonmmasukcutting'=>$id_bonm,
					'id_brg_wip'=>$id_brg_wip,
					'nama_brg_wip'=>$nama_brg_wip,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				);
				$this->db->insert('tm_bonmmasukcutting_detail',$data_detail);
				
				// ambil id detail tm_bonmmasukcutting_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// -----------------------save ke tabel detail warna-----------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_bonmmasukcutting_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_bonmmasukcutting_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_bonmmasukcutting_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_bonmmasukcutting_detail_warna',$tm_bonmmasukcutting_detail_warna);
					
					// ========================= stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_hasil_cutting='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama+$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_cutting_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_cutting_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_cutting'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_cutting_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_cutting='$id_stok' ");
					}
					
				} // end for ke tabel detail warna
				// ----------------------------------------------
				//==================================================
  }
  
  function deletebonm($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasukcutting_detail WHERE id_bonmmasukcutting = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ============ update stok =====================				
						//cek stok terakhir tm_stok_hasil_cutting, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_cutting 
											WHERE id_brg_wip = '$row2->id_brg_wip' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama-$row2->qty; // berkurang stok karena reset dari bon M masuk
							
						$this->db->query(" UPDATE tm_stok_hasil_cutting SET stok = '$new_stok', tgl_update_stok = '$tgl' 
											WHERE id_brg_wip = '$row2->id_brg_wip' ");
						// ==============================================
						
						// 04-03-2014
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_bonmmasukcutting_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_bonmmasukcutting_detail_warna 
												WHERE id_bonmmasukcutting_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//cek stok terakhir tm_stok_hasil_cutting_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_cutting_warna
											 WHERE id_stok_hasil_cutting = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama-$rowwarna->qty; // berkurang stok karena reset dari bon M masuk
										
								$this->db->query(" UPDATE tm_stok_hasil_cutting_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_cutting= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");
							}
						} // end if detail warna
						
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_bonmmasukcutting_detail_warna
						$this->db->query(" DELETE FROM tm_bonmmasukcutting_detail_warna WHERE id_bonmmasukcutting_detail='".$row2->id."' ");
						
					} // end foreach
				} // end if
	
	// 2. hapus data bonmmasukcutting
    $this->db->delete('tm_bonmmasukcutting_detail', array('id_bonmmasukcutting' => $id));
    $this->db->delete('tm_bonmmasukcutting', array('id' => $id));
  }
  
  // 05-03-2014
  function get_bonmcutting($id_bonm) {
		$query	= $this->db->query(" SELECT * FROM tm_bonmmasukcutting where id = '$id_bonm' ");
	
		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_bonmmasukcutting_detail WHERE id_bonmmasukcutting = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					$detail_bhn = array();
					$detail_warna = array();
					foreach ($hasil2 as $row2) {
						
						$query4	= $this->db->query(" SELECT kode_brg FROM tm_barang_wip WHERE id='$row2->id_brg_wip' ");
						if($query4->num_rows()>0){
							$hasilrow2	= $query4->row();
							$kode_brg_wip	= $hasilrow2->kode_brg;
						}else{
							$kode_brg_wip	= '';
						}
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_warna dan tm_bonmmasukcutting_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.id_warna, b.nama, c.qty FROM tm_warna b
												INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_warna
												 WHERE c.id_bonmmasukcutting_detail = '$row2->id' ORDER BY b.nama ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$id_warna = $row3->id_warna;
								
								$detail_warna[] = array(
												'id_warna'=> $id_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else
							$detail_warna = '';
						//-------------------------------------------------------------------------------------
																						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_bonm);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				if ($row1->jenis_masuk=='1')
					$nama_jenis = "Masuk Bagus";
				else if ($row1->jenis_masuk=='2')
					$nama_jenis = "Lain-Lain";
				else
					$nama_jenis = "";
				
				if ($row1->sumber=='1')
					$nama_sumber = "Langsung Dari Hasil Cutting";
				else if ($row1->sumber=='2')
					$nama_sumber = "Dari Pengesetan";
				else
					$nama_sumber = "";
				
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_manual'=> $row1->no_manual,
											'tgl_bonm'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'sumber'=> $row1->sumber,
											'nama_sumber'=> $nama_sumber,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  // =====================================================================================================

}
