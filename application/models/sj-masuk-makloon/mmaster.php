<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $unit_makloon, $cari) {	  
	if ($cari == "all") {
		if ($unit_makloon == '0') {
			$this->db->select(" * FROM tm_sj_hasil_makloon ORDER BY tgl_sj DESC, id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_hasil_makloon WHERE kode_unit = '$unit_makloon' ORDER BY tgl_sj DESC, id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($unit_makloon != '0') {
			$this->db->select(" * FROM tm_sj_hasil_makloon WHERE kode_unit = '$unit_makloon' 
			AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC, id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_hasil_makloon WHERE UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC, id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_masuk = array();
		$data_masuk_detail = array();

		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
								
				// ===============================
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon_detail WHERE id_sj_hasil_makloon = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					$id_detailnya = "";
					
					foreach ($hasil2 as $row2) {						
						$id_detailnya.= $row2->id_sj_proses_makloon_detail."-";
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							$satuan_makloon	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_makloon	= '';
							$satuan_makloon	= '';
						}
						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$nama_brg	= '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
				
						$data_masuk_detail[] = array('kode_brg_makloon'=> $row2->kode_brg_makloon,
												'nama_brg_makloon'=> $nama_brg_makloon,
												'satuan_makloon'=> $satuan_makloon,
												'qty'=> $row2->qty_makloon,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi
											);
					}
				}
				else {
					$data_masuk_detail = '';
				}
				// ===========
				
			/*	$query3	= $this->db->query(" SELECT tgl_sj FROM tm_sj_proses_quilting WHERE no_sj = '$row1->no_sj_keluar'
											AND kode_unit= '$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$tgl_sj_keluar	= $hasilrow->tgl_sj;
				}
				else {
					$tgl_sj_keluar = '';
				} */
												
				if ($row1->no_sj_keluar == '')
					$is_no_sjkeluar = 1;
				else
					$is_no_sjkeluar = 0;
				
				$data_masuk[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'no_sj_keluar'=> $row1->no_sj_keluar,
											//'tgl_sj_keluar'=> $tgl_sj_keluar,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'total'=> $row1->total,
											'data_masuk_detail'=> $data_masuk_detail,
											'id_detailnya'=> $id_detailnya,
											'is_no_sjkeluar'=> $is_no_sjkeluar
											);
				$id_detailnya = "";
				$data_masuk_detail = array();
			} // endforeach header
		}
		else {
			$data_masuk = '';
		}
		
		return $data_masuk;
  }
  
  function getAlltanpalimit($unit_makloon, $cari){
	if ($cari == "all") {
		if ($unit_makloon == '0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon WHERE kode_unit = '$unit_makloon' ");
	}
	else {
		if ($unit_makloon != '0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon WHERE kode_unit = '$unit_makloon' 
			AND UPPER(no_sj) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon WHERE UPPER(no_sj) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_sj, $unit_makloon){
    $this->db->select("id from tm_sj_hasil_makloon WHERE no_sj = '$no_sj' AND kode_unit = '$unit_makloon' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($jenis_pembelian, $no_sj_keluar, $no_sj, $tgl_sj, $unit_makloon, $ket, $is_no_sjkeluar,
				$dpp, $tot_pajak, $gtotal, $uang_muka, $sisa_hutang,
				$kode_brg, $kode_brg_makloon, $kode_brg_jadi, 
				$id_sj_detail, $id_sj_proses_quilting, $qty_meter, $qty_hasil, $detail_yard, $harga, $pajak, $total){  
    $tgl = date("Y-m-d");
    
    // cek apa udah ada datanya blm dgn no sj tadi
    $this->db->select("id from tm_sj_hasil_makloon WHERE no_sj = '$no_sj' AND kode_unit = '$unit_makloon' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di header (tm_sj_hasil_makloon)
			$data_header = array(
			  'no_sj_keluar'=>$no_sj_keluar,
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_makloon,
			  'keterangan'=>$ket,
			  'dpp'=>$dpp,
			  'total_pajak'=>$tot_pajak,
			  'total'=>$gtotal,
			  'uang_muka'=>$uang_muka,
			  'sisa_hutang'=>$sisa_hutang,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'jenis_pembelian'=>$jenis_pembelian
			);
			$this->db->insert('tm_sj_hasil_makloon',$data_header);
			
			if ($is_no_sjkeluar == '')
				$this->db->query(" UPDATE tm_sj_proses_quilting SET status_edit = 't' WHERE id = '$id_sj_proses_quilting' ");
			
			// ambil data terakhir di tabel tm_sj_hasil_makloon
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_makloon ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj_hasil_makloon	= $hasilrow->id;
			
			if ($qty_hasil !='' || $qty_hasil != 0) {
				if ($harga != 0) {
					$query3	= $this->db->query(" SELECT id FROM tm_harga_quilting WHERE kode_unit = '$unit_makloon' 
											AND kode_brg_quilting= '$kode_brg_makloon' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_harga	= $hasilrow->id;
						$this->db->query(" UPDATE tm_harga_quilting SET harga = '$harga' WHERE id = '$id_harga' ");
					}
					else {
						$data_harga = array(
							'kode_brg_quilting'=>$kode_brg_makloon,
							'kode_unit'=>$unit_makloon,
							'harga'=>$harga,
							'tgl_input'=>$tgl,
							'tgl_update'=>$tgl
						);
						$this->db->insert('tm_harga_quilting',$data_harga);
					}
				}
				
				$data_detail = array(
					'kode_brg'=>$kode_brg,
					'kode_brg_makloon'=>$kode_brg_makloon,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'qty_makloon'=>$qty_hasil,
					'id_sj_hasil_makloon'=>$id_sj_hasil_makloon,
					'id_sj_proses_makloon_detail'=>$id_sj_detail,
					'detail_pjg_quilting'=>$detail_yard,
					'harga'=>$harga,
					'pajak'=>$pajak,
					'biaya'=>$total
				);
				$this->db->insert('tm_sj_hasil_makloon_detail',$data_detail);
			}
			// =======================================
			
			// cek status_makloon di tabel tm_sj_proses_quilting
			/*$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum FROM tm_sj_hasil_makloon a, 
										tm_sj_hasil_makloon_detail b WHERE a.id = b.id_sj_hasil_makloon 
										AND a.id_sj_proses_quilting = '$id_sj_keluar'
										AND b.id_sj_proses_makloon_detail = '$id_sj_detail' 
										AND b.kode_brg_makloon = '$kode' ");
				$hasilrow = $query3->row();
				$jum_hasil = $hasilrow->jum; // ini sum qty di sj_hasil_makloon berdasarkan id sj_proses_quilting
			*/
			//if ($qty_sj_keluar == $jum_hasil) {
				if ($is_no_sjkeluar == '') {
					$this->db->query(" UPDATE tm_sj_proses_quilting_detail SET status_makloon = 't' 
									where id= '$id_sj_detail' ");
					
					//cek di tabel tm_sj_proses_quilting_detail, apakah status_makloon sudah 't' semua?
					$this->db->select(" id from tm_sj_proses_quilting_detail WHERE status_makloon = 'f' 
										AND id_sj_proses_quilting = '$id_sj_proses_quilting' ", false);
					$query = $this->db->get();
					//jika sudah t semua, maka update tabel tm_sj_proses_quilting di field status_makloon menjadi t
					if ($query->num_rows() == 0){
						$this->db->query(" UPDATE tm_sj_proses_quilting SET status_makloon = 't' 
										where id= '$id_sj_proses_quilting' ");
					}
				}
			//}
			
			// ============ update stok =====================
			// KOREKSI 9 NOV 2011: update stok nanti dibuatin fitur apply stoknya
			//cek stok terakhir tm_stok, dan update stoknya
				/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode_brg_makloon'
									AND kode_brg_jadi = '$kode_brg_jadi' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty_hasil; // bertambah stok karena SJ masuk hasil makloon
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_makloon, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg_makloon,
							'kode_brg_jadi'=>$kode_brg_jadi,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_makloon',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg_makloon' AND kode_brg_jadi = '$kode_brg_jadi'  ");
					}
					
					$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_makloon (kode_brg, kode_brg_jadi, no_bukti, kode_unit, masuk, saldo, tgl_input) 
											VALUES ('$kode_brg_makloon', '$kode_brg_jadi', '$no_sj', '$unit_makloon', '$qty_hasil', '$new_stok', '$tgl') ");
					 */
			// ==============================================

		}
		else {
			// ambil data terakhir di tabel tm_sj_hasil_makloon
			$query2	= $this->db->query(" SELECT id FROM tm_sj_hasil_makloon ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj_hasil_makloon	= $hasilrow->id;
			
			if ($qty_hasil !='' || $qty_hasil != 0) {
				if ($harga != 0) {
					$query3	= $this->db->query(" SELECT id FROM tm_harga_quilting WHERE kode_unit = '$unit_makloon' 
											AND kode_brg_quilting= '$kode_brg_makloon' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_harga	= $hasilrow->id;
						$this->db->query(" UPDATE tm_harga_quilting SET harga = '$harga' WHERE id = '$id_harga' ");
					}
					else {
						$data_harga = array(
							'kode_brg_quilting'=>$kode_brg_makloon,
							'kode_unit'=>$unit_makloon,
							'harga'=>$harga,
							'tgl_input'=>$tgl,
							'tgl_update'=>$tgl
						);
						$this->db->insert('tm_harga_quilting',$data_harga);
					}
				}
				
				$data_detail = array(
					'kode_brg'=>$kode_brg,
					'kode_brg_makloon'=>$kode_brg_makloon,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'qty_makloon'=>$qty_hasil,
					'id_sj_hasil_makloon'=>$id_sj_hasil_makloon,
					'id_sj_proses_makloon_detail'=>$id_sj_detail,
					'detail_pjg_quilting'=>$detail_yard,
					'harga'=>$harga,
					'pajak'=>$pajak,
					'biaya'=>$total
				);
				$this->db->insert('tm_sj_hasil_makloon_detail',$data_detail);
			}
			// =======================================
			if ($is_no_sjkeluar == '') {
					$this->db->query(" UPDATE tm_sj_proses_quilting_detail SET status_makloon = 't' 
									where id= '$id_sj_detail' ");
					
					//cek di tabel tm_sj_proses_quilting_detail, apakah status_makloon sudah 't' semua?
					$this->db->select(" id from tm_sj_proses_quilting_detail WHERE status_makloon = 'f' 
										AND id_sj_proses_quilting = '$id_sj_proses_quilting' ", false);
					$query = $this->db->get();
					//jika sudah t semua, maka update tabel tm_sj_proses_quilting di field status_makloon menjadi t
					if ($query->num_rows() == 0){
						$this->db->query(" UPDATE tm_sj_proses_quilting SET status_makloon = 't' 
										where id= '$id_sj_proses_quilting' ");
					}
			}
			// ============ update stok =====================
			//cek stok terakhir tm_stok, dan update stoknya
			/*		$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode_brg_makloon'
									AND kode_brg_jadi = '$kode_brg_jadi' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty_hasil; // bertambah stok karena SJ masuk hasil makloon
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_makloon, insert
						$data_stok = array(
							'kode_brg'=>$kode_brg_makloon,
							'kode_brg_jadi'=>$kode_brg_jadi,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_makloon',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg_makloon' AND kode_brg_jadi = '$kode_brg_jadi'  ");
					}
					
					$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_makloon (kode_brg, kode_brg_jadi, no_bukti, kode_unit, masuk, saldo, tgl_input) 
											VALUES ('$kode_brg_makloon', '$kode_brg_jadi', '$no_sj', '$unit_makloon', '$qty_hasil', '$new_stok', '$tgl') ");
			*/		 
			// ==============================================
		}

  }
    
  function delete($kode, $list_brg, $is_no_sjkeluar){    
	  $tgl = date("Y-m-d");
	  $id_brg = explode("-", $list_brg);
	  
	  if ($is_no_sjkeluar == 0) { 
		  foreach($id_brg as $row1) {
				if ($row1 != '') {
					$this->db->query(" UPDATE tm_sj_proses_quilting_detail SET status_makloon = 'f' where id= '$row1' ");
					
					$query3	= $this->db->query(" SELECT id_sj_proses_quilting FROM tm_sj_proses_quilting_detail 
													WHERE id = '$row1' ");
					  $hasilrow = $query3->row();
					  $id_sj_keluar	= $hasilrow->id_sj_proses_quilting;
					  
					 $this->db->query(" UPDATE tm_sj_proses_quilting SET status_makloon = 'f', status_edit = 'f' 
										where id= '$id_sj_keluar' ");
				}
		  }
	  }
	  
	  $query3	= $this->db->query(" SELECT kode_unit FROM tm_sj_hasil_makloon WHERE id = '$kode' ");
	  $hasilrow = $query3->row();
	  $kode_unit	= $hasilrow->kode_unit; 
	  
	  //
	  $query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon_detail WHERE id_sj_hasil_makloon = '$kode' ");
	  if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
										
			foreach ($hasil2 as $row2) {
				// 21 NOV 2011: RESET STOK JIKA status_stok = 't'
				if ($row2->status_stok == 't') {
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon 
											WHERE kode_brg = '".$row2->kode_brg_makloon."' ");
								
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama-$row2->qty_makloon; // berkurang stok karena reset dari SJ masuk utk makloon
								
								// 17 nov 2011 START
								//cek stok terakhir tm_stok_harga, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga 
														WHERE kode_brg_quilting = '".$row2->kode_brg_makloon."' 
														AND harga = '$row2->harga' AND quilting = 't' ");
														
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$stokreset2 = $stok_lama-$row2->qty_makloon;
								
								if ($row2->qty_makloon > 0) {
									/*if ($is_no_sjkeluar == 'f') {
										$harga = $harga_nosjkeluar;
										$kode_lama = $kode_brg_makloon;
									}
									else {
										$kode_lama = $this->input->post('kode_bhn_quilting_lama_'.$i, TRUE);
									} */
										//insert ke tabel tt_stok_hasil_makloon dgn tipe keluar, utk membatalkan data brg masuk
										$this->db->query(" INSERT INTO tt_stok_hasil_makloon (kode_brg, kode_unit, no_bukti, keluar, 
											saldo, tgl_input, harga)
											VALUES ('$row2->kode_brg_makloon', '$kode_unit', '$row2->no_bonm', '$row2->qty_makloon', 
											'$stokreset2', 
											'$tgl', '$row2->harga' ) ");
																						
									//4. update stok di tm_stok
									$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', tgl_update_stok = '$tgl'
													where kode_brg= '$row2->kode_brg_makloon' ");
									
									$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
													where kode_brg_quilting= '$row2->kode_brg_makloon' AND harga = '$row2->harga' ");
								}
				}
				
				// ============ update stok =====================
				// KOREKSI 10 NOV 2011: data SJ hasil quilting tidak boleh didelete jika sudah diupdate stoknya (nanti mau dikasih pengecekan di view data)
						//cek stok terakhir tm_stok, dan update stoknya
				/*		$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon 
						WHERE kode_brg = '$row2->kode_brg_makloon' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama-$row2->qty_makloon; // berkurang stok karena reset dari SJ msk hsl makloon
										
										if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_makloon, insert
											$data_stok = array(
												'kode_brg'=>$row2->kode_brg_makloon,
												'kode_brg_jadi'=>$row2->kode_brg_jadi,
												'stok'=>$new_stok,
												'tgl_update_stok'=>$tgl
												);
											$this->db->insert('tm_stok_hasil_makloon',$data_stok);
										}
										else {
											$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$new_stok', 
											tgl_update_stok = '$tgl' 
											where kode_brg= '$row2->kode_brg_makloon' AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
										}
										
										$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_makloon 
										(kode_brg, kode_brg_jadi, no_bukti, kode_unit, keluar, saldo, tgl_input) VALUES 
										('$row2->kode_brg_makloon', '$row2->kode_brg_jadi', '$no_sj', '$kode_unit', '$row2->qty_makloon', '$new_stok', '$tgl') ");
					*/			
								// ==============================================
			}
	 }
		$this->db->delete('tm_sj_hasil_makloon', array('id' => $kode));
		$this->db->delete('tm_sj_hasil_makloon_detail', array('id_sj_hasil_makloon' => $kode));

  } 
  
  function get_sj($id_sj, $is_no_sjkeluar){
	$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon WHERE id = '$id_sj' ");    
    $hasil = $query->result();
    
    $data_sj = array();
    $detail_sj = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon_detail WHERE id_sj_hasil_makloon = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						if ($is_no_sjkeluar == 0) {
							$query3	= $this->db->query(" SELECT a.no_sj, a.tgl_sj FROM tm_sj_proses_quilting a, tm_sj_proses_quilting_detail b 
										WHERE a.id = b.id_sj_proses_quilting AND b.id = '$row2->id_sj_proses_makloon_detail' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$no_sj	= $hasilrow->no_sj;
								$tgl_sj	= $hasilrow->tgl_sj;
								
								$pisah1 = explode("-", $tgl_sj);
								$tgl1= $pisah1[2];
								$bln1= $pisah1[1];
								$thn1= $pisah1[0];
								if ($bln1 == '01')
									$nama_bln = "Januari";
								else if ($bln1 == '02')
									$nama_bln = "Februari";
								else if ($bln1 == '03')
									$nama_bln = "Maret";
								else if ($bln1 == '04')
									$nama_bln = "April";
								else if ($bln1 == '05')
									$nama_bln = "Mei";
								else if ($bln1 == '06')
									$nama_bln = "Juni";
								else if ($bln1 == '07')
									$nama_bln = "Juli";
								else if ($bln1 == '08')
									$nama_bln = "Agustus";
								else if ($bln1 == '09')
									$nama_bln = "September";
								else if ($bln1 == '10')
									$nama_bln = "Oktober";
								else if ($bln1 == '11')
									$nama_bln = "November";
								else if ($bln1 == '12')
									$nama_bln = "Desember";
								$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
							}
							else {
								$no_sj	= '';
								$tgl_sj	= '';
							}
												
							// ambil qty meter
							$query3	= $this->db->query(" SELECT qty_meter FROM tm_sj_proses_quilting_detail
											WHERE id = '$row2->id_sj_proses_makloon_detail' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$qty_meter	= $hasilrow->qty_meter;
							}
							else {
								$qty_meter	= '';
							}
						}
						else {
							$no_sj	= '';
							$tgl_sj	= '';
							$qty_meter	= '';
						}
						
						// ambil nama brg makloon
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
												WHERE a.satuan=b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							$satuan_makloon = $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_makloon	= '';
							$satuan_makloon = '';
						}
												
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_makloon'=> $row2->kode_brg_makloon,
												'nama_brg_makloon'=> $nama_brg_makloon,
												'satuan'=> $satuan_makloon,
												'qty'=> $row2->qty_makloon,
												'id_sj_proses_makloon_detail'=> $row2->id_sj_proses_makloon_detail,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty_meter'=> $qty_meter,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'detail_pjg_quilting'=> $row2->detail_pjg_quilting,
												'harga'=> $row2->harga,
												'biaya'=> $row2->biaya,
												'pajak'=> $row2->pajak
											);
					}
				}
				else {
					$detail_sj = '';
				}
		
		// ================== end detail
		
				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama, pkp, tipe_pajak FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				if ($query3->num_rows() != 0) {
					$hasilrow = $query3->row();
					$nama_unit	= $hasilrow->nama;
					$hide_pkp	= $hasilrow->pkp;
					$hide_tipe_pajak	= $hasilrow->tipe_pajak;
				}
								
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
												
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'no_sj_keluar'=> $row1->no_sj_keluar,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'hide_pkp'=> $hide_pkp,
											'hide_tipe_pajak'=> $hide_tipe_pajak,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'detail_sj'=> $detail_sj,
											'dpp'=> $row1->dpp,
											'total_pajak'=> $row1->total_pajak,
											'total'=> $row1->total,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang
											);
				$detail_sj = array();
											
			} // endforeach header
	
	return $data_sj;
  }
  
  function get_unit_quilting(){
    $this->db->select(" * from tm_unit_quilting order by kode_unit ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_makloon(){
    $this->db->select("* from tm_jenis_makloon order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bahan($num, $offset, $cari, $kode_supplier, $jenisnya)
  {
	if ($cari == "all") {
		if ($jenisnya == 'b')
			$sql = "a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id order by a.nama_brg";
		else if ($jenisnya == 'q')
			$sql = "a.*, d.nama as nama_satuan 
				FROM tm_brg_hasil_makloon a, tm_satuan d
				WHERE a.satuan = d.id order by a.nama_brg";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		if ($jenisnya == 'b')
			$sql = "a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
				OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg";
		else if ($jenisnya == 'q')
			$sql = "a.*, d.nama as nama_satuan 
				FROM tm_brg_hasil_makloon a, tm_satuan d
				WHERE a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
				OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			if ($jenisnya == 'b')
				$tabel_stok = "tm_stok";
			else if ($jenisnya == 'q')
				$tabel_stok = "tm_stok_hasil_makloon";
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM $tabel_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;
			
			if ($jenisnya == 'b')
				$sql = " SELECT harga FROM tm_harga_brg_supplier WHERE kode_supplier = '$kode_supplier' 
						AND kode_brg = '$row1->kode_brg' ";
			else if ($jenisnya == 'q')
				$sql = " SELECT harga FROM tm_harga_quilting WHERE kode_unit = '$kode_supplier' 
						AND kode_brg_quilting = '$row1->kode_brg' ";
			
			$query3	= $this->db->query($sql);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				if ($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2)
					$harga	= $hasilrow->harga;
				else
					$harga = 0;
			}
			else
				$harga = 0;

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $row1->nama_satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok,
										'harga'=> $harga
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kode_supplier, $jenisnya){
	if ($cari == "all") {
		if ($jenisnya == 'b')
			$sql = "SELECT a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id order by a.nama_brg";
		else if ($jenisnya == 'q')
			$sql = "SELECT a.*, d.nama as nama_satuan 
				FROM tm_brg_hasil_makloon a, tm_satuan d
				WHERE a.satuan = d.id order by a.nama_brg";
				
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		if ($jenisnya == 'b')
			$sql = " SELECT a.*, d.nama as nama_satuan 
				FROM tm_barang a, tm_satuan d
				WHERE a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
				OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg";
		else if ($jenisnya == 'q')
			$sql = "SELECT a.*, d.nama as nama_satuan 
				FROM tm_brg_hasil_makloon a, tm_satuan d
				WHERE a.satuan = d.id AND (UPPER(a.kode_brg) like UPPER('%$cari%') 
				OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by a.nama_brg ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  function get_sj_keluar($num, $offset, $cari, $unit_makloon)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_sj_proses_quilting where kode_unit = '$unit_makloon' AND status_makloon = 'f' order by tgl_sj DESC ";
		//echo $sql; die();
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tm_sj_proses_quilting where kode_unit = '$unit_makloon' AND status_makloon = 'f' ";
		$sql.=" AND (UPPER(kode_brg_makloon) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by tgl_sj DESC ";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {		
			
			// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting_detail 
											WHERE id_sj_proses_quilting = '$row1->id' AND status_makloon = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
	//				$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						// ambil nama brg makloon
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
												WHERE a.satuan=b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							$satuan_makloon = $hasilrow->nama_satuan;
						}
						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
												
						$detail_bhn[] = array('id'=> $row2->id,
												'id_sj_proses_quilting'=> $row2->id_sj_proses_quilting,
												'no_sj'=> $row1->no_sj,
												'kode_brg'=> $row2->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'kode_brg_makloon'=> $row2->kode_brg_makloon,
												'nama_brg_makloon'=> $nama_brg_makloon,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama_brg_jadi'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'qty_meter'=> $row2->qty_meter
									);
						
					} 
				} 
				else {
					$detail_bhn = '';
				} // end detail
			
			$data_bhn[] = array(		'id'=> $row1->id,	
										'no_sj'=> $row1->no_sj,	
										'tgl_sj'=> $row1->tgl_sj,
										'tgl_update'=> $row1->tgl_update,
										'keterangan'=> $row1->keterangan,
										'detail_bhn'=> $detail_bhn
								);
			
			$detail_bhn = array();
								
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_sj_keluartanpalimit($cari, $unit_makloon){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_sj_proses_quilting where kode_unit = '$unit_makloon' AND status_makloon = 'f' order by no_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tm_sj_proses_quilting where kode_unit = '$unit_makloon' AND status_makloon = 'f' ";
		$sql.=" AND (UPPER(kode_brg_makloon) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by no_sj DESC ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_detail_sj_keluar($list_brg, $unit_makloon){
    $detail_sj = array();
    $detail_yard = array();
        
    foreach($list_brg as $row1) {
			if ($row1 != '') {
			$query2	= $this->db->query(" SELECT id_sj_proses_quilting, kode_brg_makloon, kode_brg, kode_brg_jadi, qty_meter,
								detail_pjg_kain	FROM tm_sj_proses_quilting_detail WHERE id = '$row1' ");
			$hasilrow = $query2->row();
			$id_sj_proses_quilting	= $hasilrow->id_sj_proses_quilting;
			$kode_brg	= $hasilrow->kode_brg;
			$kode_brg_makloon	= $hasilrow->kode_brg_makloon;
			$kode_brg_jadi	= $hasilrow->kode_brg_jadi;
			$qty_meter	= $hasilrow->qty_meter;
			$detail_pjg_kain	= $hasilrow->detail_pjg_kain;
			$explode_detail_pjg_kain = explode(";", $detail_pjg_kain);
			
			foreach($explode_detail_pjg_kain as $rownya) {
				if ($rownya != '') {
					$nilai_yard = $rownya / 0.91;
					$detail_yard[] = $nilai_yard;
				}
			}
			$detail_pjg_kain_yard = implode(";", $detail_yard);
			
			$query3	= $this->db->query(" SELECT no_sj, tgl_sj FROM tm_sj_proses_quilting WHERE id = '$id_sj_proses_quilting' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$no_sj	= $hasilrow->no_sj;
				$tgl_sj	= $hasilrow->tgl_sj;
				
				$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
			}
			else {
				$no_sj	= '';
				$tgl_sj	= '';
			}
												
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							$satuan_makloon	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_makloon	= '';
							$satuan_makloon	= '';
						}
						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
		
			$detail_sj[] = array(		'id'=> $row1,
										'id_sj_proses_quilting'=> $id_sj_proses_quilting,
										'kode_brg'=> $kode_brg,
										'nama_brg'=> $nama_brg,
										'kode_brg_makloon'=> $kode_brg_makloon,
										'nama_brg_makloon'=> $nama_brg_makloon,
										'kode_brg_jadi'=> $kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'qty_meter'=> $qty_meter,
										'detail_pjg_kain'=> $detail_pjg_kain,
										'detail_pjg_kain_yard'=> $detail_pjg_kain_yard,
										'no_sj'=> $no_sj,
										'tgl_sj'=> $tgl_sj
								);
		}
	}
	return $detail_sj;
  }

}
