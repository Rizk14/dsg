<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function clistdobrg_f($d_do_first,$d_do_last,$kddo){

		if(!empty($kddo)){
			$do		= " WHERE a.i_do_code=trim('$kddo') ";
			$stat	= "";
		} else {
			$do		= "";
			$stat	= "";
		}
		
		$query	= $this->db->query(" SELECT a.i_do_code, 
		a.i_op_code, 
		a.d_do, 
		a.d_op, 
		a.i_branch AS area, 
		a.i_product AS iproduct, 
		a.e_product_name AS eproductname, 
		a.n_deliver AS jmldo, 
		a.f_transfer 
		
		FROM tm_trans_do a  ".$do." ".$stat." ORDER BY a.i_do_code ASC, a.i_product ASC ");
		
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	
	function clistdobrg_t($d_do_first,$d_do_last,$kddo) {
		if( (!empty($d_do_first) && !empty($d_do_last))) {
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$stat	= "";
		} else if( (!empty($d_do_first) && !empty($d_do_last)) ) {
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$stat	= "";
		} else {
			$dop	= "";
			$stat	= "";
		}
		
		return $this->db->query(" SELECT a.i_do_code, a.i_op_code, a.d_do, a.d_op, a.i_branch AS area, a.i_product AS iproduct, a.e_product_name AS eproductname, a.n_deliver AS jmldo, a.f_transfer FROM tm_trans_do a  ".$ddo." ".$stat." ORDER BY a.i_do_code ASC, a.i_product ASC ");			
	}
	
	function viewperpages_f($limit,$offset,$d_do_first,$d_do_last,$kddo) {
		if(!empty($kddo)){
			$do		= " WHERE a.i_do_code='$kdop' ";
			$stat	= "";
		} else {
			$do		= "";
			$stat	= "";
		}
		$query	= $this->db->query(" SELECT a.i_do_code, a.i_op_code, a.d_do, a.d_op, a.i_branch AS area, a.i_product AS iproduct, a.e_product_name AS eproductname, a.n_deliver AS jmldo, a.f_transfer 
		
		FROM tm_trans_do a  ".$do." ".$stat." ORDER BY a.i_do_code ASC, a.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows()>0)	{
			return $result	= $query->result();
		}
	}
	
	function viewperpages_t($limit,$offset,$d_do_first,$d_do_last,$kddo) {
		if( (!empty($d_do_first) && !empty($d_do_last))) {
			$ddo	= " WHERE (a.d_do BETWEEN '$d_do_first' AND '$d_do_last')";
			$stat	= "";
		} else if( (!empty($d_do_first) && !empty($d_do_last)) ) {
			$ddo	= " WHERE (a.d_do BETWEEN '$d_do_first' AND '$d_do_last')";
			$stat	= "";
		} else {
			$ddo	= "";
			$stat	= "";
		}
		$query	= $this->db->query(" SELECT a.i_do_code, a.i_op_code, a.d_do, a.d_op, a.i_branch AS area, a.i_product AS iproduct, a.e_product_name AS eproductname, a.n_deliver AS jmldo, a.f_transfer 
		
		FROM tm_trans_do a  ".$ddo." ".$stat." ORDER BY a.i_do_code ASC, a.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");
				
		if($query->num_rows()>0)	{
			return $result	= $query->result();
		}
	}
		
	function ltransferdoperpages($limit,$offset) {
		$query	= $this->db->query(" SELECT a.i_do_code, a.d_do FROM tm_do a WHERE a.f_do_cancel='f' AND (length(a.i_do_code) >= 9) AND a.f_transfer='t' ORDER BY a.i_do_code DESC LIMIT ".$limit." OFFSET ".$offset);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function ltransferdo() {
		return $this->db->query(" SELECT a.i_do_code, a.d_do FROM tm_do a WHERE a.f_do_cancel='f' AND (length(a.i_do_code) >= 9) AND a.f_transfer='t' ORDER BY a.i_do_code DESC ");
	}
	
	function fltransferdo($key) {
		if(!empty($key)) {
			return $this->db->query(" SELECT a.i_do_code, a.d_do FROM tm_do a WHERE (a.i_do_code LIKE '$key%') AND a.f_do_cancel='f' AND (length(a.i_do_code) >= 9) AND a.f_transfer='t' ORDER BY a.i_do_code DESC ");
		}
	}
}
?>
