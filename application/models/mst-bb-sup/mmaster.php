<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $supplier)
  {
	$sql = " a.*, b.kode_supplier as kode_supplier, b.nama as nama_sup, c.kode_brg, c.nama_brg FROM tm_harga_brg_supplier a 
			INNER JOIN tm_supplier b ON a.id_supplier = b.id
			INNER JOIN tm_barang c ON c.id = a.id_brg
			WHERE c.status_aktif = 't' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(b.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(c.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	if ($supplier != '0')
		$sql.= " AND a.id_supplier = '$supplier' ";
	$sql.= " ORDER BY a.kode_supplier ";
	$this->db->select($sql, false)->limit($num,$offset);
				
	/*if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.id_brg = c.id_brg AND c.status_aktif = 't'
			ORDER BY a.kode_supplier ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.id_brg = c.id_brg AND a.kode_supplier = '$supplier' 
			AND c.status_aktif = 't' ORDER BY a.kode_supplier ", false)->limit($num,$offset);
		}
	}
	else {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.kode_brg = c.kode_brg AND (UPPER(a.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) AND c.status_aktif = 't' ORDER BY a.kode_supplier ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.kode_brg = c.kode_brg 
			AND a.kode_supplier = '$supplier' AND (UPPER(a.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%')) AND c.status_aktif = 't' ORDER BY a.kode_supplier ", false)->limit($num,$offset);
		}
    } */
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari, $supplier){
	/*if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.kode_brg = c.kode_brg AND c.status_aktif = 't' ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.kode_brg = c.kode_brg AND a.kode_supplier = '$supplier'
			AND c.status_aktif = 't' ", false);
		}
	}
	else {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.kode_brg = c.kode_brg AND (UPPER(a.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) AND c.status_aktif = 't' ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_brg_supplier a, tm_supplier b, tm_barang c 
			WHERE a.kode_supplier = b.kode_supplier AND a.kode_brg = c.kode_brg 
			AND a.kode_supplier = '$supplier' AND (UPPER(a.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%')) AND c.status_aktif = 't' ", false);
		}
	} */
	
	$sql = " a.*, b.kode_supplier as kode_supplier, b.nama as nama_sup, c.kode_brg, c.nama_brg FROM tm_harga_brg_supplier a 
			INNER JOIN tm_supplier b ON a.id_supplier = b.id
			INNER JOIN tm_barang c ON c.id = a.id_brg
			WHERE c.status_aktif = 't' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(b.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(c.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	if ($supplier != '0')
		$sql.= " AND a.id_supplier = '$supplier' ";
	
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
    
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  //function cek_data($kode, $id_jenis_bhn){
  function cek_data($id_supplier, $id_brg, $id_satuan){
    $this->db->select("id from tm_harga_brg_supplier WHERE id_brg = '$id_brg' AND id_supplier = '$id_supplier' AND id_satuan = '$id_satuan' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_tm_stok( $id_brg, $id_satuan, $id_satuan_konversi){
    $this->db->select("id from tm_stok_harga WHERE id_brg = '$id_brg' AND id_satuan = '$id_satuan' AND is_harga_pkp = '$pkp' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($id_supplier,$pkp, $id_brg, $id_satuan, $harga ,$id_satuan_konversi){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
		
	if ($harga != 0) {
		$data = array(
		  'id_brg'=>$id_brg,
		  'id_supplier'=>$id_supplier,
		  'id_satuan'=>$id_satuan,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_harga_brg_supplier',$data); 
	 /*	
	$this->db->select("id from tm_stok_harga WHERE id_brg = '$id_brg' AND id_satuan = '$id_satuan' AND is_harga_pkp = '$pkp' ", false);
  
		$query = $this->db->get();
		if ($query->num_rows() == 0){
		
		$data_stok_harga = array(
		  'id_brg'=>$id_brg,
		  'id_supplier'=>$id_supplier,
		  'id_satuan'=>$id_satuan,
		  'id_satuan_konversi'=>$id_satuan_konversi,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl,
		  'is_harga_pkp'=>$pkp
		);
	
		$this->db->insert('tm_stok_harga',$data_stok_harga); 
	}
	*/	
		
		// no longer used anymore 2015
		//$this->db->query(" INSERT INTO tt_harga (id_brg, id_supplier, harga, tgl_input) 
		//					VALUES ('$id_brg','$id_supplier', '$harga', '$tgl') ");
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_harga_brg_supplier', array('id' => $id));
  }
  
  function get_jenis_barang($num, $offset, $kel_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_barangtanpalimit($kel_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_jns_bhn(){
    $this->db->select("a.kode as kj_brg, a.nama as nj_brg, b.* from tm_jenis_barang a, tm_jenis_bahan b 
    WHERE a.id = b.id_jenis_barang order by a.kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bahan($num, $offset, $jns_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_bahantanpalimit($jns_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
	// kategori = '1'
    return $query->result();  
  }
  
  function cek_data_harga($id_supplier ,$id_brg,$harga){
	$query	= $this->db->query(" SELECT * FROM tt_harga where id_brg='$id_brg' AND id_supplier='$supplier' AND harga='$harga'");    
	
    return $query->result();  
  }
  
  function get_barang($kel_brg, $id_jenis, $id_supplier) {
	$sql = " SELECT a.*, ss.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan ss ON a.satuan = ss.id 
				INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
				INNER JOIN tm_kelompok_barang b ON c.kode_kel_brg = b.kode
				WHERE b.kode = '$kel_brg' AND a.status_aktif = 't' "; 
	if ($id_jenis != '')
		$sql.= " AND c.id = '$id_jenis' "; 
			
	$sql.= " ORDER BY a.kode_brg ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '$row1->id'
									AND id_supplier = '$id_supplier' AND id_satuan = '$row1->satuan' ");
			if ($query3->num_rows() == 0){
				$harga	= '';
			}
			else {
				$hasilrow = $query3->row();
				$harga	= $hasilrow->harga;
			}
				
			$data_harga[] = array(			
								'id_brg'=> $row1->id,
								'kode_brg'=> $row1->kode_brg,
								'nama_brg'=> $row1->nama_brg,
								'id_satuan'=> $row1->satuan,
								'id_satuan_konversi'=> $row1->id_satuan_konversi,
								'nama_satuan'=> $row1->nama_satuan,
								'harga'=> $harga
							);

		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
  
  function get_harga($idharga){
	$query = $this->db->query(" SELECT * FROM tm_harga_brg_supplier WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
			
			// ambil data nama barang
			$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang WHERE id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			
			// ambil data nama satuan
			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->id_satuan' ");
			$hasilrow = $query3->row();
			$nama_satuan	= $hasilrow->nama;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'satuan'=> $row1->id_satuan,
											'nama_satuan'=> $nama_satuan,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }
  
  function get_harga_for_print($supplier) { 
		$sql = "SELECT a.*, b.kode_supplier, b.nama, b.pkp FROM tm_harga_brg_supplier a 
				INNER JOIN tm_supplier b ON a.id_supplier = b.id
				INNER JOIN tm_barang c ON c.id = a.id_brg ";
		if ($supplier != '0')
			$sql.= " WHERE a.id_supplier = '$supplier' ";
		$sql.= " ORDER BY b.kode_supplier, c.kode_brg ";
		$query	= $this->db->query($sql);
	
		$data_harga = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row2) {
				$sql2 = " SELECT b.nama as nama_kelompok, a.kode_brg, a.nama_brg FROM tm_barang a
							INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
							INNER JOIN tm_kelompok_barang b ON c.kode_kel_brg = b.kode 
							WHERE a.id = '$row2->id_brg' ";
				$query3	= $this->db->query($sql2);
				$hasilrow = $query3->row();
				$kode_brg	= $hasilrow->kode_brg;
				$nama_brg	= $hasilrow->nama_brg;
				//$satuan	= $hasilrow->nama_satuan;
				$nama_kel_brg	= $hasilrow->nama_kelompok;
				
				// 15-10-2015 ambil nama satuan
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama;
				}
				else {
					$queryxx	= $this->db->query(" SELECT satuan FROM tm_barang WHERE id = '$row2->id_brg' ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$id_satuan	= $hasilxx->satuan;
						
						$this->db->query(" UPDATE tm_harga_brg_supplier SET id_satuan = '$id_satuan' WHERE id='$row2->id' ");
						
						$queryxx	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$id_satuan' ");
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$satuan	= $hasilxx->nama;
						}
					}
				}
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp FROM tm_supplier WHERE id = '$row2->id_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				$pkp	= $hasilrow->pkp;
																																
				$data_harga[] = array(		'kode_brg'=> $row2->kode_brg,
											'kode_supplier'=> $row2->kode_supplier,
											'nama_supplier'=> $row2->nama,
											'nama_kel_brg'=> $nama_kel_brg,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'harga'=> $row2->harga,
											'pkp'=> $row2->pkp
											);
											
			} // endforeach header
		}
		else {
			$data_harga = '';
		}
		return $data_harga;
  }

}

