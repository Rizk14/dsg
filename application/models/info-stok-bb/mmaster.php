<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_stok($num, $offset, $id_gudang, $cstatus, $cari) {
	//function get_opname_hutang($date_from, $date_to) {
	$sql = " a.id, a.kode_brg, a.nama_brg, a.id_gudang, b.stok, b.tgl_update_stok 
			FROM tm_barang a INNER JOIN tm_stok b ON a.id = b.id_brg WHERE a.status_aktif = '$cstatus' ";
	if ($id_gudang != '0')
		$sql.= " AND a.id_gudang = '$id_gudang' ";
	if ($cari != "all")
		$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
	$sql.= " ORDER BY a.nama_brg ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$ekode_gudang	= $hasilrow->kode_gudang;
				$eid_gudang	= $hasilrow->id;
				$enama_gudang	= $hasilrow->nama;
				$enama_lokasi	= $hasilrow->nama_lokasi;
				
				$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row1->id' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$satuan	= '';
				}
				
				// 04-08-2015 satuan konversi
				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
										b.nama as nama_satuan_konv FROM tm_barang a 
										INNER JOIN tm_satuan b ON a.id_satuan_konversi = b.id WHERE a.id = '$row1->id' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan_konversi	= $hasilrow->nama_satuan_konv;
					$id_satuan_konversi = $hasilrow->id_satuan_konversi;
					$rumus_konversi = $hasilrow->rumus_konversi;
					$angka_faktor_konversi = $hasilrow->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$qty_konversi = $row1->stok*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$qty_konversi = $row1->stok/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$qty_konversi = $row1->stok+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$qty_konversi = $row1->stok-$angka_faktor_konversi;
						}
						else
							$qty_konversi = $row1->stok;
					}
					else
						$qty_konversi = $row1->stok;
				}
				else {
					$satuan_konversi	= "Tidak Ada";
					$qty_konversi = $row1->stok;
				}
				
				$query3	= $this->db->query(" SELECT a.id, a.bulan, a.tahun, a.tgl_update, b.status_approve 
									FROM tt_stok_opname_bahan_baku a INNER JOIN 
									tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
									WHERE b.id_brg = '$row1->id' ORDER BY a.id DESC LIMIT 1 ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$bulan	= $hasilrow->bulan;
					$tahun	= $hasilrow->tahun;
					$tgl_update_so	= $hasilrow->tgl_update;
					$status_approve	= $hasilrow->status_approve;
				}
				else {
					$bulan	= '';
					$tahun	= '';
					$tgl_update_so	= '';
					$status_approve = 'f';
				}
								
				$data_stok[] = array(		'id_brg'=> $row1->id,	
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $row1->nama_brg,
											'ekode_gudang'=> $ekode_gudang,
											'eid_gudang'=> $eid_gudang,
											'enama_gudang'=> $enama_gudang,
											'enama_lokasi'=> $enama_lokasi,
											'satuan'=> $satuan,
											'stok'=> $row1->stok,
											// 04-08-2015
											'satuan_konversi'=> $satuan_konversi,
											'stok_satuan_konversi'=> $qty_konversi,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'bulan'=> $bulan,
											'tahun'=> $tahun,
											'tgl_update_so'=> $tgl_update_so,
											'status_approve'=> $status_approve
											);
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stoktanpalimit($id_gudang, $cstatus, $cari){
		$sql = " SELECT a.id, a.kode_brg, a.nama_brg, a.id_gudang, b.stok, b.tgl_update_stok 
			FROM tm_barang a INNER JOIN tm_stok b ON a.id = b.id_brg WHERE a.status_aktif = '$cstatus' ";
		if ($id_gudang != '0')
			$sql.= " AND a.id_gudang = '$id_gudang' ";
		if ($cari != "all")
			$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
	  
		$query	= $this->db->query($sql);
    
    return $query->result();  
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
						WHERE a.jenis = '1'
						ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
