<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $supplier, $cari) {	  
	if ($cari == "all") {
		if ($supplier == '0') {
			/*$this->db->select(" distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' ORDER BY a.id_gudang, a.kode_supplier ", false)->limit($num,$offset); */
			
			$this->db->select(" distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' AND a.status_aktif = 't' ORDER BY a.id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' AND a.status_aktif = 't' AND a.kode_supplier = '$supplier' 
			ORDER BY a.id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' AND a.status_aktif = 't' AND a.kode_supplier = '$supplier' 
			AND UPPER(a.no_bonm) like UPPER('%$cari%') ORDER BY a.id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' AND a.status_aktif = 't' 
			AND UPPER(a.no_bonm) like UPPER('%$cari%') ORDER BY a.id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id' 
				AND status_stok = 't' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				//ambil tgl SJ
				$query3	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE no_sj = '$row1->no_sj' 
							AND kode_supplier = '$row1->kode_supplier' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$tgl_sj	= $hasilrow->tgl_sj;
				 }
				 else
					$tgl_sj = '';
				 
				 // ambil data nama gudang
					$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				 
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,	
											'tgl_sj'=> $tgl_sj,	
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'status_stok'=> $row1->status_stok,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' AND a.status_aktif = 't' ");
		else
			$query	= $this->db->query(" SELECT distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' AND a.status_aktif = 't' AND a.kode_supplier = '$supplier' ");
	}
	else {
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't'  AND a.status_aktif = 't'
			AND a.kode_supplier = '$supplier' AND UPPER(a.no_bonm) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' AND a.status_aktif = 't' AND UPPER(a.no_bonm) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  function cek_data($no_bonm, $gudang){
    $this->db->select("id from tm_apply_stok_pembelian WHERE no_bonm = '$no_bonm' AND id_gudang = '$gudang' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_sj($num, $offset, $csupplier, $id_gudang, $cari) {
	if ($cari == '') {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail brg-nya
			/*	$sql = "SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id'";
				echo($sql); */
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id' 
				AND a.status_stok = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama FROM tm_barang a, tm_satuan b
								WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama;
							
						$detail_sj[] = array('id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'qty'=> $row2->qty,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_sjtanpalimit($csupplier, $cari){
	if ($cari == '') { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ");
	}
    
    return $query->result();  
  }
  
  function get_detail_bonm($id_gudang){
    $headernya = array();    
    $detail_brg = array();    
    
    $query	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian WHERE status_stok = 'f' AND status_aktif = 't'
    AND id_gudang = '$id_gudang' ORDER BY kode_supplier, no_sj ASC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach($hasil as $row1) {
			$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail WHERE status_stok = 'f' 
						AND id_apply_stok = '$row1->id' ");  
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
						foreach ($hasil2 as $row2) {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
											WHERE a.satuan = b.id AND
											a.kode_brg = '$row2->kode_brg' ");
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
							
							$detail_brg[] = array(		
											'id'=> $row2->id,
											'kode_brg'=> $row2->kode_brg,
											'nama'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row2->qty,
											'harga'=> $row2->harga
									);
						}
					}
					else
						$detail_brg = '';
			//=====================================================================
			// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				}
				
				//ambil tgl SJ
				$query3	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE no_sj = '$row1->no_sj' 
							AND kode_supplier = '$row1->kode_supplier' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$tgl_sj	= $hasilrow->tgl_sj;
				 }
				 else
					$tgl_sj = '';
				
				$headernya[] = array(		
											'id'=> $row1->id,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'id_gudang'=> $row1->id_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'detail_brg'=> $detail_brg
									);
				$detail_brg = array();
		}
		
	}
	else {
			$headernya = '';
	}
	return $headernya;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori='1' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
  function get_bonm($id_apply_stok){
	$query	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian WHERE id = '$id_apply_stok' ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	$detail_bonm = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b, tm_pembelian c, 
				tm_apply_stok_pembelian d 
				WHERE a.kode_brg = b.kode_brg AND a.id_pembelian = c.id AND d.no_sj = c.no_sj 
				AND d.kode_supplier = c.kode_supplier AND b.id_gudang = '$row1->id_gudang' 
				AND c.no_sj = '$row1->no_sj' 
				AND d.kode_supplier = '$row1->kode_supplier' ");  
		
				//$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id'
								AND kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$cek = 'y';
						}
						else
							$cek = 't';
						
						$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE 
									kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE 
									kode_brg = '$row2->kode_brg' AND kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$harga	= $hasilrow->harga;
				
						$detail_bonm[] = array(		
										'id'=> $row2->id,
										'kode_brg'=> $row2->kode_brg,
										'nama'=> $nama_brg,
										'harga'=> $harga,
										'qty'=> $row2->qty,
										'cek'=> $cek
										);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$query2	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE 
									kode_supplier = '$row1->kode_supplier' AND no_sj = '$row1->no_sj' ");
						$hasilrow = $query2->row();
						$tgl_sj	= $hasilrow->tgl_sj;
				
				$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				}
				 
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,	
											'tgl_sj'=> $tgl_sj,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
	}
	return $data_bonm;
  }

}
