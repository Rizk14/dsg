<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $cari) {	  
	if ($cari == "all") {
			$this->db->select(" distinct no_bonm, tgl_bonm, tgl_update_stok FROM tm_sj_hasil_makloon_detail WHERE status_stok = 't' 
								ORDER BY tgl_bonm DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
	else {
			$this->db->select(" distinct no_bonm, tgl_bonm, tgl_update_stok FROM tm_sj_hasil_makloon_detail WHERE status_stok = 't' AND
							UPPER(no_bonm) like UPPER('%$cari%') 
							ORDER BY tgl_bonm DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_hasil_makloon_detail
				WHERE no_bonm = '$row1->no_bonm' AND status_stok = 't' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
				
						$detail_fb[] = array('kode_brg_makloon'=> $row2->kode_brg_makloon,
											'nama'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row2->qty_makloon
											);
					}
				}
				else {
					$detail_fb = '';
				}
								 
				$data_fb[] = array(			'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT distinct no_bonm, tgl_bonm FROM tm_sj_hasil_makloon_detail 
										WHERE status_stok = 't' ");
	}
	else {
		$query	= $this->db->query(" SELECT distinct no_bonm, tgl_bonm FROM tm_sj_hasil_makloon_detail WHERE status_stok = 't' AND
							UPPER(no_bonm) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
      
  function cek_data($no_bonm){
    $this->db->select("id from tm_sj_hasil_makloon_detail WHERE no_bonm = '$no_bonm' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_sj($num, $offset, $csupplier, $id_gudang, $cari) {
	if ($cari == '') {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail brg-nya
			/*	$sql = "SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id'";
				echo($sql); */
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b 
				WHERE a.kode_brg = b.kode_brg AND b.id_gudang = '$id_gudang' AND a.id_pembelian = '$row1->id' 
				AND a.status_stok = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama FROM tm_barang a, tm_satuan b
								WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama;
							
						$detail_sj[] = array('id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'qty'=> $row2->qty,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_sjtanpalimit($csupplier, $cari){
	if ($cari == '') { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_sj DESC ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_stok = 'f' AND kode_supplier = '$csupplier' 
							AND UPPER(no_sj) like UPPER('%$cari%') ORDER BY tgl_sj DESC ");
	}
    
    return $query->result();  
  }
  
  function get_detail_bonm(){
    $headernya = array();    
    $detail_brg = array();    
    // =====================================================================================
    $query	= $this->db->query(" SELECT a.no_sj, a.tgl_sj, a.kode_unit, b.* FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b 
					WHERE a.id = b.id_sj_hasil_makloon AND a.status_stok = 'f'
					AND b.status_stok = 'f' AND a.tgl_sj >='2013-01-01'
					ORDER BY a.kode_unit, b.id DESC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		
		foreach($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
								WHERE a.satuan = b.id AND
								a.kode_brg = '$row1->kode_brg_makloon' ");
			$hasilrow = $query3->row();
			$nama_brg	= $hasilrow->nama_brg;
			$satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				$headernya[] = array(		
											'id'=> $row1->id,
											'id_sj_hasil_makloon'=> $row1->id_sj_hasil_makloon,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'kode_brg_makloon'=> $row1->kode_brg_makloon,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row1->qty_makloon,
											'harga'=> $row1->harga
									);
		}
	}
	else {
			$headernya = '';
	}
    // =====================================================================================
    
	return $headernya;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier where kategori='1' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
    
  function get_bonm($id_apply_stok){
	$query	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian WHERE id = '$id_apply_stok' ");    
    $hasil = $query->result();
    
    $data_bonm = array();
	$detail_bonm = array();
	
	foreach ($hasil as $row1) {
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelian_detail a, tm_barang b, tm_pembelian c, 
				tm_apply_stok_pembelian d 
				WHERE a.kode_brg = b.kode_brg AND a.id_pembelian = c.id AND d.no_sj = c.no_sj 
				AND d.kode_supplier = c.kode_supplier AND b.id_gudang = '$row1->id_gudang' 
				AND c.no_sj = '$row1->no_sj' 
				AND d.kode_supplier = '$row1->kode_supplier' ");  
		
				//$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE id_apply_stok = '$row1->id'
								AND kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$cek = 'y';
						}
						else
							$cek = 't';
						
						$query3	= $this->db->query(" SELECT nama_brg FROM tm_barang WHERE 
									kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE 
									kode_brg = '$row2->kode_brg' AND kode_supplier = '$row1->kode_supplier' ");
						$hasilrow = $query3->row();
						$harga	= $hasilrow->harga;
				
						$detail_bonm[] = array(		
										'id'=> $row2->id,
										'kode_brg'=> $row2->kode_brg,
										'nama'=> $nama_brg,
										'harga'=> $harga,
										'qty'=> $row2->qty,
										'cek'=> $cek
										);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				$query2	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE 
									kode_supplier = '$row1->kode_supplier' AND no_sj = '$row1->no_sj' ");
						$hasilrow = $query2->row();
						$tgl_sj	= $hasilrow->tgl_sj;
				
				$pisah1 = explode("-", $tgl_sj);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";
				$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				// nama gudang
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi FROM tm_gudang a, tm_lokasi_gudang b 
								WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) {
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
				}
				 
				$data_bonm[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,	
											'tgl_sj'=> $tgl_sj,
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
	}
	return $data_bonm;
  }
  
  function delete($no_bonm){   
	  $tgl = date("Y-m-d");
	   
	  $sql = "SELECT a.no_sj, a.tgl_sj, a.kode_unit, b.* FROM tm_sj_hasil_makloon a, 
					tm_sj_hasil_makloon_detail b WHERE a.id = b.id_sj_hasil_makloon
					AND b.no_bonm = '$no_bonm'
					AND b.status_stok = 't'  "; //echo $sql; die();
	  $query4	= $this->db->query($sql);
							
	  if ($query4->num_rows() > 0){
			//$row4 = $query4->row();
			$hasil = $query4->result();
			foreach ($hasil as $row4) {
				$no_sj = $row4->no_sj;
				$tgl_sj = $row4->tgl_sj;
				$kode_unit = $row4->kode_unit;
				$id_sj_hasil_makloon = $row4->id_sj_hasil_makloon;
				$id_detail = $row4->id;
				$kode_brg = $row4->kode_brg_makloon;
				$qty = $row4->qty_makloon;
				$harga = $row4->harga;
								
				//2. ambil stok terkini di tm_stok_hasil_makloon
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_makloon WHERE kode_brg='$kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				else
					$stok_lama = 0;
								
				$stokreset1 = $stok_lama-$qty;
				
				// 14 juni 2011, ada tabel baru, yaitu tm_stok_harga (utk membedakan stok berdasarkan harga)
				//cek stok terakhir tm_stok_harga, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg_quilting = '$kode_brg' 
											AND harga = '$harga' AND quilting = 't' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$stokreset2 = $stok_lama-$qty;
				
				//3. insert ke tabel tt_stok_hasil_makloon dgn tipe keluar, utk membatalkan data brg masuk
				// 16-02-2012: warning, tabel tt_stok udh ga dipake lagi utk rekap mutasi stok
				/*$this->db->query(" INSERT INTO tt_stok_hasil_makloon (kode_brg, kode_unit, no_bukti, keluar, 
									saldo, tgl_input, harga)
									VALUES ('$kode_brg', '$kode_unit', '$no_bonm', '$qty', '$stokreset2', 
									'$tgl_sj', '$harga' ) "); */
																					
				//4. update stok di tm_stok_hasil_makloon
				$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$stokreset1', tgl_update_stok = '$tgl'
									where kode_brg= '$kode_brg' ");
										
				$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
									where kode_brg_quilting= '$kode_brg' AND harga = '$harga' ");
																		
				$this->db->query(" UPDATE tm_sj_hasil_makloon SET 
														status_stok = 'f', tgl_update = '$tgl'
														where id= '$id_sj_hasil_makloon'  ");
														
				$this->db->query(" UPDATE tm_sj_hasil_makloon_detail SET 
														no_bonm = NULL, tgl_bonm = NULL, status_stok = 'f'
														where id= '$id_detail'  ");
		}
	  }
										
		
  }

}
