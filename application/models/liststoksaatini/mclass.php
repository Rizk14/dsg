<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function cliststoksaatini($n_d_do_first,$n_d_do_last,$stopproduk,$iclass) {
		$db2=$this->load->database('db_external', TRUE);
		/*
		cliststoksaatini($nddofirst,$nddolast,$stopproduk,$iclass)
		*/
		
		$qrycategory	= $db2->query(" SELECT * FROM tr_categories WHERE i_class='$iclass' ");
		
		if( (int) $qrycategory->num_rows() > 0) {
			$row	= $qrycategory->row_array();
			
			$strqry	= "
					SELECT 	a.i_so,
						a.i_product,
						a.i_product_motif,
						a.e_product_motifname,
						a.i_status_do,
						a.d_do,
						sum(a.n_saldo_awal) AS n_saldo_awal,
						sum(a.n_saldo_akhir) AS n_saldo_akhir,
						sum(a.n_inbonm) AS n_inbonm,
						sum(a.n_outbonm) AS n_outbonm,
						sum(a.n_bbm) AS n_bbm,
						sum(a.n_bbk) AS n_bbk,
						a.d_entry
					
					FROM tm_so a 
										
					INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product_motif
					INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
					INNER JOIN tr_categories d ON d.i_category=c.i_category
					
					WHERE (a.d_do BETWEEN '$n_d_do_first' AND '$n_d_do_last') AND c.f_stop_produksi='$stopproduk'
					
					GROUP BY a.i_so, a.i_product, a.i_product_motif, a.e_product_motifname, a.i_status_do, a.d_do, a.n_saldo_awal, a.n_saldo_akhir, a.n_inbonm, a.n_outbonm, a.n_bbm,a.n_bbk, a.d_entry
					
					ORDER BY a.e_product_motifname ASC ";
			
			$query	= $db2->query($strqry);
			
			if($query->num_rows() > 0 ) {
				return $result	= $query->result();
			}
		}
	}
	
	function lklsbrg() {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT i_class, e_class_name FROM tr_class ORDER BY e_class_name " );
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}

	// Baru 19/01/2011--25/07/2011
	function lbrgjadi($stopproduk,$limit,$offset,$filter_tgl_stokopname) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT 	a.i_product_motif, 
				a.i_product, 
				a.e_product_motifname, 
				b.v_unitprice,
				c.i_so, c.i_so_item,
				c.n_quantity_awal,
				c.n_quantity_akhir
				
			FROM tr_product_motif a
			
			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so
			
			WHERE b.f_stop_produksi='$stopproduk' AND a.n_active='1' ".$filter_tgl_stokopname."
			
			ORDER BY a.e_product_motifname ASC "." LIMIT ".$limit." OFFSET ".$offset);
	}
	
	function lbonkeluar($dfirst,$dlast,$productmotif) { //// 
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonkeluar FROM tm_outbonm_item a
				INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm 
				
				WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_outbonm_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbonmasuk($dfirst,$dlast,$productmotif) { ////
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_count_product) AS jbonmasuk FROM tm_inbonm_item a
				INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm
				
				WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_inbonm_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbbk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbk FROM tm_bbk_item a
				INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying)
				
				WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbk_cancel='f'
				
				GROUP BY a.i_product ");
	}

	function lbbm($dfirst,$dlast,$productmotif) { ////
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jbbm FROM tm_bbm_item a
				INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying)
				
				WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbm_cancel='f'
				
				GROUP BY a.i_product ");
	}
	
	function ldo($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_product, sum(a.n_deliver) AS jdo FROM tm_do_item a
			INNER JOIN tm_do b ON b.i_do=a.i_do
			
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND f_do_cancel='f'
			
			GROUP BY a.i_product ");
	}

	function lsj($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT a.i_product, sum(a.n_unit) AS jsj FROM tm_sj_item a
			INNER JOIN tm_sj b ON b.i_sj=a.i_sj
			
			WHERE (b.d_sj BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_sj_cancel='f'
			
			GROUP BY a.i_product ");
	}
		
	function popupbonmmasuk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_inbonm_code AS ibonmcode,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_count_product) AS jbonmasuk,
					b.d_inbonm AS dinbonm
			
			FROM tm_inbonm_item a
			
			INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_inbonm_cancel=false
			
			GROUP BY b.i_inbonm_code, a.i_product, c.e_product_motifname, a.n_count_product, b.d_inbonm
			
			ORDER BY b.d_inbonm DESC, b.i_inbonm_code DESC ");
	}

	function popupbonmmasukperpages($dfirst,$dlast,$productmotif,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT  b.i_inbonm_code AS ibonmcode,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_count_product) AS jbonmasuk,
					b.d_inbonm AS dinbonm
			
			FROM tm_inbonm_item a
			
			INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_inbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_inbonm_cancel=false
			
			GROUP BY b.i_inbonm_code, a.i_product, c.e_product_motifname, a.n_count_product, b.d_inbonm
			
			ORDER BY b.d_inbonm DESC, b.i_inbonm_code DESC LIMIT ".$limit." OFFSET ".$offset);
			
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
	
	function fbonmmasuk($code,$d,$d2) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_inbonm_code AS ibonmcode,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_count_product) AS jbonmasuk,
					b.d_inbonm AS dinbonm
			
			FROM tm_inbonm_item a
			
			INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm 
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_inbonm BETWEEN '$d' AND '$d2') AND (a.i_product='$code' OR b.i_inbonm_code='$code') AND b.f_inbonm_cancel=false
			
			GROUP BY b.i_inbonm_code, a.i_product, c.e_product_motifname, a.n_count_product, b.d_inbonm
			
			ORDER BY b.d_inbonm DESC, b.i_inbonm_code DESC ");
	}
	
	function popupbonmkeluar($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_outbonm_code AS ioutbonmcode,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_count_product) AS jbonkeluar,
					b.d_outbonm AS doutbonm
			
			FROM tm_outbonm_item a
			
			INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_outbonm_cancel=false
			
			GROUP BY b.i_outbonm_code, a.i_product, c.e_product_motifname, a.n_count_product, b.d_outbonm 
			
			ORDER BY b.d_outbonm DESC, b.i_outbonm_code DESC ");
	}

	function popupbonmkeluarperpages($dfirst,$dlast,$productmotif,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT  b.i_outbonm_code AS ioutbonmcode,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_count_product) AS jbonkeluar,
					b.d_outbonm AS doutbonm
			
			FROM tm_outbonm_item a
			
			INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_outbonm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_outbonm_cancel=false
			
			GROUP BY b.i_outbonm_code, a.i_product, c.e_product_motifname, a.n_count_product, b.d_outbonm 
			
			ORDER BY b.d_outbonm DESC, b.i_outbonm_code DESC LIMIT ".$limit." OFFSET ".$offset);
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function fbonmkeluar($code,$d,$d2) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_outbonm_code AS ioutbonmcode,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_count_product) AS jbonkeluar,
					b.d_outbonm AS doutbonm
			
			FROM tm_outbonm_item a
			
			INNER JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_outbonm BETWEEN '$d' AND '$d2') AND (a.i_product='$code' OR b.i_outbonm_code='$code') AND b.f_outbonm_cancel=false
			
			GROUP BY b.i_outbonm_code, a.i_product, c.e_product_motifname, a.n_count_product, b.d_outbonm 
			
			ORDER BY b.d_outbonm DESC, b.i_outbonm_code DESC ");
	}
			
	function popupbbk($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_bbk AS ibbk,
				a.i_product AS iproduct, 
				c.e_product_motifname AS eproductname,
				sum(a.n_unit) AS jbbk,
				b.d_bbk AS dbbk
			
			FROM tm_bbk_item a
			
			INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbk_cancel=false
			
			GROUP BY b.i_bbk, a.i_product, c.e_product_motifname, a.n_unit, b.d_bbk
			
			ORDER BY b.d_bbk DESC, b.i_bbk DESC ");
	}

	function popupbbkperpages($dfirst,$dlast,$productmotif,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT  b.i_bbk AS ibbk,
				a.i_product AS iproduct, 
				c.e_product_motifname AS eproductname,
				sum(a.n_unit) AS jbbk,
				b.d_bbk AS dbbk
			
			FROM tm_bbk_item a
			
			INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_bbk BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbk_cancel='f'
			
			GROUP BY b.i_bbk, a.i_product, c.e_product_motifname, a.n_unit, b.d_bbk
			
			ORDER BY b.d_bbk DESC, b.i_bbk DESC LIMIT ".$limit." OFFSET ".$offset);
			
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function fbbk($code,$d,$d2) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_bbk AS ibbk,
				a.i_product AS iproduct, 
				c.e_product_motifname AS eproductname,
				sum(a.n_unit) AS jbbk,
				b.d_bbk AS dbbk
			
			FROM tm_bbk_item a
			
			INNER JOIN tm_bbk b ON b.i_bbk=cast(a.i_bbk AS character varying) 
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_bbk BETWEEN '$d' AND '$d2') AND (a.i_product='$code' OR b.i_bbk='$code') AND b.f_bbk_cancel='f'
			
			GROUP BY b.i_bbk, a.i_product, c.e_product_motifname, a.n_unit, b.d_bbk
			
			ORDER BY b.d_bbk DESC, b.i_bbk DESC ");
	}
			
	function popupbbm($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_bbm AS ibbm,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_unit) AS jbbm,
					b.d_bbm AS dbbm
			
			FROM tm_bbm_item a
			
			INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying)
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbm_cancel='f'
			
			GROUP BY b.i_bbm, a.i_product, c.e_product_motifname, a.n_unit, b.d_bbm
			
			ORDER BY b.d_bbm DESC, b.i_bbm DESC ");
	}

	function popupbbmperpages($dfirst,$dlast,$productmotif,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT  b.i_bbm AS ibbm,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_unit) AS jbbm,
					b.d_bbm AS dbbm
			
			FROM tm_bbm_item a
			
			INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying)
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
							
			WHERE (b.d_bbm BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_bbm_cancel='f'
			
			GROUP BY b.i_bbm, a.i_product, c.e_product_motifname, a.n_unit, b.d_bbm
			
			ORDER BY b.d_bbm DESC, b.i_bbm DESC LIMIT ".$limit." OFFSET ".$offset);
		
		if($query->num_rows()>0) {
			return $query->result();
		}
	}

	function fbbm($code,$d,$d2) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_bbm AS ibbm,
					a.i_product AS iproduct,
					c.e_product_motifname AS eproductname,
					sum(a.n_unit) AS jbbm,
					b.d_bbm AS dbbm
			
			FROM tm_bbm_item a
			
			INNER JOIN tm_bbm b ON b.i_bbm=cast(a.i_bbm AS character varying)
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
							
			WHERE (b.d_bbm BETWEEN '$d' AND '$d2') AND ( a.i_product='$code' OR b.i_bbm='$code') AND b.f_bbm_cancel='f'
			
			GROUP BY b.i_bbm, a.i_product, c.e_product_motifname, a.n_unit, b.d_bbm
			
			ORDER BY b.d_bbm DESC, b.i_bbm DESC ");
	}
			
	function popupdo($dfirst,$dlast,$productmotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT  b.i_do_code AS idocode,
				a.i_product AS iproduct,
				c.e_product_motifname AS eproductname,
				sum(a.n_deliver) AS jdo,
				b.d_do AS ddo
			
			FROM tm_do_item a
			
			INNER JOIN tm_do b ON b.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_do_cancel='f'
			
			GROUP BY b.i_do_code, a.i_product, c.e_product_motifname, a.n_deliver, b.d_do
			
			ORDER BY b.d_do DESC, b.i_do_code DESC	" );
	}

	function popupdoperpages($dfirst,$dlast,$productmotif,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( "
			SELECT  b.i_do_code AS idocode,
				a.i_product AS iproduct,
				c.e_product_motifname AS eproductname,
				sum(a.n_deliver) AS jdo,
				b.d_do AS ddo
			
			FROM tm_do_item a
			
			INNER JOIN tm_do b ON b.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_do BETWEEN '$dfirst' AND '$dlast') AND a.i_product='$productmotif' AND b.f_do_cancel='f'
			
			GROUP BY b.i_do_code, a.i_product, c.e_product_motifname, a.n_deliver, b.d_do
			
			ORDER BY b.d_do DESC, b.i_do_code DESC	LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows()>0) {
			return $query->result();
		}
	}	

	function fdo($code,$d,$d2) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT  b.i_do_code AS idocode,
				a.i_product AS iproduct,
				c.e_product_motifname AS eproductname,
				sum(a.n_deliver) AS jdo,
				b.d_do AS ddo
			
			FROM tm_do_item a
			
			INNER JOIN tm_do b ON b.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			
			WHERE (b.d_do BETWEEN '$d' AND '$d2') AND (a.i_product='$code' OR b.i_do_code='$code') AND b.f_do_cancel='f' 
			
			GROUP BY b.i_do_code, a.i_product, c.e_product_motifname, a.n_deliver, b.d_do
			
			ORDER BY b.d_do DESC, b.i_do_code DESC	" );
	}	
}
?>
