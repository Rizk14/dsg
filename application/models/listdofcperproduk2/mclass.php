<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function getqtyperproduk($d_first,$d_last) {
		$db2=$this->load->database('db_external', TRUE);
		//=============================
/*
		$sql=" select UPPER(b.i_product) AS imotif, b.e_product_name AS productmotif, sum(b.n_deliver) as qty
			FROM tm_dofc a, tm_dofc_item b  
			WHERE a.i_do = b.i_do AND a.f_do_cancel='f' ";
		if ($d_first != '' && $d_last!='')
			$sql.= " AND (a.d_do >='$d_first' AND a.d_do <='$d_last') ";
		
		$sql.= " GROUP BY b.i_product, b.e_product_name ORDER BY b.i_product ASC ";
*/
		$periode=substr($d_first,0,4).substr($d_first,5,2);
/*
		$sql="select UPPER(c.i_product) AS imotif, c.e_product_name AS productmotif, qty as saldo_awal, sum(b.n_deliver) as qty, 
	        (qty-sum(b.n_deliver)) as sisa
          FROM tm_saldo_awalfc c
          left join tm_dofc_item b on(c.i_product=b.i_product)
          left join tm_dofc a on (a.i_do = b.i_do AND a.f_do_cancel='f' AND (a.d_do >='$d_first' AND a.d_do <='$d_last'))";
#		if ($d_first != '' && $d_last!='')
#			$sql.= " AND (a.d_do >='$d_first' AND a.d_do <='$d_last') ";
		
		$sql.= "WHERE c.i_periode='$periode'
            GROUP BY c.i_product, c.e_product_name, c.qty
            ORDER BY c.i_product ASC ";
*/

######Sikasep 2017-08-04
$sql=" SELECT UPPER(c.i_product) AS imotif, c.e_product_name AS productmotif, c.qty as saldo_awal, sum(e.qty) as qty, (c.qty-sum(e.qty)) as sisa, d.e_color_name
FROM tm_saldo_awalfc c
left join tm_dofc a on (a.f_do_cancel='f' AND (a.d_do >='$d_first' AND a.d_do <='$d_last'))
left join tm_dofc_item b on(c.i_product=b.i_product and a.i_do = b.i_do)
left join tm_dofc_item_color e on(e.i_do_item=b.i_do_item and c.i_color=e.i_color)
left join tr_color d on (d.i_color=c.i_color) 
WHERE c.i_periode='$periode'
GROUP BY c.i_product, c.e_product_name, c.qty, d.e_color_name
ORDER BY c.i_product ASC";


######

		//=============================
				
		$query	= $db2->query($sql);
		$data_brg = array();
		
		if($query->num_rows()>0) {
			$hasil = $query->result();
			foreach ($hasil as $row) {
				
				//--------------06-09-2014 ----------------------------
				// ambil data qty warna dari tr_product_color dan tm_dofc_item_color
/*
				$sqlxx	= $db2->query(" SELECT b.e_color_name, sum(c.qty) as jum FROM tr_color b, 
										tm_dofc_item_color c, tm_dofc d, tm_dofc_item e
										WHERE d.i_do = e.i_do AND c.i_do_item = e.i_do_item AND
										b.i_color=c.i_color 
										AND (d.d_do >='$d_first' AND d.d_do <='$d_last') AND
										e.i_product='$row->imotif'
										GROUP BY b.e_color_name ORDER BY b.e_color_name ");
				$listwarna = "";
				if ($sqlxx->num_rows() > 0){
					$hasilxx = $sqlxx->result();
							
					foreach ($hasilxx as $rownya) {
						$listwarna.= $rownya->e_color_name." : ".$rownya->jum."<br>";
					}
				}

				//-----------------------------------------------------
				
				$data_brg[] = array(		'imotif'=> $row->imotif,	
											'productmotif'=> $row->productmotif,	
											'qty'=> $row->qty,
											'saldo_awal'=> $row->saldo_awal,
											'sisa'=>$row->saldo_awal-$row->qty,
											'listwarna'=> $listwarna
											);
*/
				$data_brg[] = array('imotif'=> $row->imotif,	
											'productmotif'=> $row->productmotif,	
											'qty'=> $row->qty,
											'saldo_awal'=> $row->saldo_awal,
											'sisa'=>$row->sisa,
											'listwarna'=> $row->e_color_name
											);
			} // end foreach
		}
		else 
			$data_brg = '';
		return $data_brg;		
	}
	
}
?>
