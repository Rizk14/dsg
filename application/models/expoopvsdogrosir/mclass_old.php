<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function hargaperpelanggan($imotif,$icustomer) {
		return $this->db->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		return $this->db->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}

	function lcustomer() {
		$query = $this->db->query(" SELECT * FROM tr_customer ORDER BY e_customer_name ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function logfiles($efilename,$iuserid) {

		$this->db->trans_begin();

		$qdate	= $this->db->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$this->db->insert('tm_files_log',$fields);

		if($this->db->trans_status()===FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}
		
	function clistopvsdo($i_product,$d_op_first,$d_op_last,$f_stop_produksi) {
		
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE 't' ";
			$ipro	= " AND i_product='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE 't' ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE i_product='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE f_stop_produksi='t' ":" WHERE f_stop_produksi='f' ";
		}	
		
		$qstr	= "
			SELECT 	f_stop_produksi AS stopproduct,
				i_product,
				e_product_motifname,
				v_price,
				sum(n_op) AS n_op, 
				sum(v_op) AS v_op,
				sum(n_do) AS n_do,
				sum(v_do) AS v_do,
				sum(n_op-n_do) AS n_selopdo,
				sum(v_op-v_do) AS v_selopdo
			FROM f_op_vs_do('$d_op_first','$d_op_lasst')
			".$ddate." ".$ipro." ".$fstopproduct."
			GROUP BY i_product, f_stop_produksi, e_product_motifname, v_price ";

		$query	= $this->db->query($qstr);
		
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}

	function explistopvsdo($i_product,$d_op_first,$d_op_last,$f_stop_produksi) {
		
		if(($i_product!="" || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			//$batal	= " AND (b.f_op_cancel='f' OR b.f_op_close='f') ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product=="" || empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product!="" || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND (b.f_op_cancel='f') ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";		
		}
		
		$qstr	= "
			SELECT 	f_stop_produksi AS stopproduct,
				i_product,
				e_product_motifname,
				v_price,
				sum(n_op) AS n_op, 
				sum(v_op) AS v_op,
				sum(n_do) AS n_do,
				sum(v_do) AS v_do,
				sum(n_op-n_do) AS n_selopdo,
				sum(v_op-v_do) AS v_selopdo
			FROM f_op_vs_do('$d_op_first','$d_op_lasst')
			".$ddate." ".$ipro." ".$fstopproduct."
			GROUP BY i_product, f_stop_produksi, e_product_motifname, v_price ";
		return $this->db->query($qstr);
	}

	function lbarangjadiperpages($limit,$offset) {
		$query = $this->db->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		return $this->db->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC " );
	}	
	
	function flbarangjadi($key) {
		if(!empty($key)) {
			return $this->db->query("
					SELECT 	a.i_product_base AS iproduct,
						b.i_product_motif AS imotif,	
						b.e_product_motifname AS motifname,
						b.n_quantity AS qty
						
					FROM tr_product_base a 
					
					RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product 
					
					WHERE a.i_product_base LIKE '$key%' OR b.i_product_motif LIKE '$key%' 
					
					ORDER BY b.i_product_motif DESC ");
		}
	}
	
	function clistopvsdo_new($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir, $jenis_op_1, $jenis_op_2, $jenis_op_3) {
		//=============================
		$sql="
        f_stop_produksi,
				i_product,
				e_product_motifname,
				v_price,
				sum(n_op) AS n_op, 
				sum(v_op) AS v_op,
				sum(n_do) AS n_do,
				sum(v_do) AS v_do,
				sum(n_faktur) AS n_faktur,
				sum(v_faktur) AS v_faktur,
				sum(n_op-n_do) AS n_selopdo,
				sum(v_op-v_do) AS v_selopdo
			FROM f_op_vs_do('$d_op_first','$d_op_last')
			WHERE 't' ";
		if ($i_product != '')
			$sql.= " AND i_product='$i_product' ";
		if ($f_stop_produksi=='TRUE')
			$sql.=" AND f_stop_produksi='t' ";
		else
			$sql.=" AND f_stop_produksi='f' ";
		if($fdropforcast=='1') {
			$sql.= " AND f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$sql.= " AND f_op_dropforcast='f' ";
		}
		if ($is_grosir == '1')
			$sql.= " AND is_grosir = 't' ";
		else
			$sql.= " AND is_grosir = 'f' ";
		
		if ($icustomer != '')
			$sql.= " AND i_customer = '$icustomer'";
		
		if ($jenis_op_1 != '' || $jenis_op_2 != '' || $jenis_op_3 != '') {
			$stringjenisop = "";
			$sql.= " AND jenis_op IN (";
			if ($jenis_op_1 != '')
				$stringjenisop.= $jenis_op_1.",";
			if ($jenis_op_2 != '')
				$stringjenisop.= $jenis_op_2.",";
			if ($jenis_op_3 != '')
				$stringjenisop.= $jenis_op_3.",";
			
			$pjgstring = strlen($stringjenisop);
			$stringjenisopnow = substr($stringjenisop, 0, $pjgstring-1);
			
			$sql.= $stringjenisopnow.") ";
		}
		
		$sql.= " GROUP BY f_stop_produksi,
				i_product,
				e_product_motifname,
				v_price ORDER BY i_product ASC";

		$this->db->select($sql, false);
		$query = $this->db->get();
		
		$data_opdo = array();
		$detail_opdo = array();
		if($query->num_rows()>0) {
			//return $result	= $query->result();
			$hasil = $query->result();
			foreach ($hasil as $row) {
				$data_opdo[] = array(		'imotif'=> $row->i_product,
											'productmotif'=> $row->e_product_motifname,	
											'stopproduct'=> $row->f_stop_produksi,
											'hargaperunit'=> $row->v_price,
											'jmlorder'=> $row->n_op,
											'nilaiorder'=> $row->v_op,
											'pemenuhan'=> $row->n_do,
											'nilaipemenuhan'=> $row->v_do,
											'selisihopdo'=> $row->n_selopdo,
											'nilaiselisih'=> $row->v_selopdo,
											'qty_faktur'=> $row->n_faktur
											);
			} // end foreach
		}
		else 
			$data_opdo = '';
		return $data_opdo;		
	}
	
	// useless anymore, ga dipake
	function clistopvsdorinci_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif) {
		
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= "";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else {
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		}
		
		/* 26072011
		return $this->db->query("
			SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			*/

		return $this->db->query(" SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON e.i_op=b.i_op
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
	}
	
	function explistopvsdo_new($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir, $jenis_op_1, $jenis_op_2, $jenis_op_3) {
		
		//=============================
		$sql="select i_product, e_product_name, f_stop_produksi, 
			is_grosir,
      	sum(n_op) AS n_op, 
				sum(v_op) AS v_op,
				sum(n_do) AS n_do,
				sum(v_do) AS v_do,
				sum(n_faktur) AS n_faktur,
				sum(v_faktur) AS v_faktur,
				sum(n_op-n_do) AS n_selopdo,
				sum(v_op-v_do) AS v_selopdo
			FROM f_op_vs_do('$d_op_first','$d_op_last')
			where 't' ";
		if ($i_product != '')
			$sql.= " AND i_product='$i_product' ";
		if ($f_stop_produksi=='TRUE')
			$sql.=" AND f_stop_produksi='t' ";
		else
			$sql.=" AND f_stop_produksi='f' ";
		if($fdropforcast=='1') {
			$sql.= " AND f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$sql.= " AND f_op_dropforcast='f' ";
		}
		if ($is_grosir == '1')
			$sql.= " AND is_grosir = 't' ";
		else
			$sql.= " AND is_grosir = 'f' ";
		
		if ($icustomer != '')
			$sql.= " AND i_customer = '$icustomer' ";
		
		if ($jenis_op_1 != '' || $jenis_op_2 != '' || $jenis_op_3 != '') {
			$stringjenisop = "";
			$sql.= " AND jenis_op IN (";
			if ($jenis_op_1 != '')
				$stringjenisop.= $jenis_op_1.",";
			if ($jenis_op_2 != '')
				$stringjenisop.= $jenis_op_2.",";
			if ($jenis_op_3 != '')
				$stringjenisop.= $jenis_op_3.",";
			
			$pjgstring = strlen($stringjenisop);
			$stringjenisopnow = substr($stringjenisop, 0, $pjgstring-1);
			
			$sql.= $stringjenisopnow.") ";
		}
			
		$sql.= " GROUP BY i_product, e_product_name, f_stop_produksi, 
			is_grosir ORDER BY i_product ASC";
			echo $sql;
		//=============================
				
		return $query	= $this->db->query($sql);
				
		/*if($query->num_rows()>0) {
			return $result	= $query->result();
		} */
		// ======= 13-07-2012 ============
	}
	
	function explistopvsdorinci_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif) {
		
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		}	
		
		/* 26072011
		return $this->db->query("
			SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			*/

		return $this->db->query(" SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON e.i_op=b.i_op
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif." ".$f_op_dropforcast."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			
	}		
	
	function jmlorder($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir) {
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		$sql = " SELECT SUM(a.n_count) AS jmlorder, 
			sum(c.n_deliver) as pemenuhan, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product  AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND b.d_op >='$d_op_first' AND b.d_op <='$d_op_last' ";		
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
		if ($icustomer != '')
			$sql.=" AND b.i_customer = '$icustomer' ";
		$sql.=	" GROUP BY b.i_customer"; //echo $sql."<br>";
		
		return $this->db->query($sql);
				
	}
	
	// 03-07-2012
	function jumopall($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $jenis_op_1, $jenis_op_2, $jenis_op_3) {

		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
				
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product=='') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last=="") && ($d_op_first==0 || $d_op_last==0)) ) {
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else{
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' "; 
		}
		
		//if ($jenis_op != '0')
		//	$sqljenisop = " AND b.jenis_op = '$jenis_op' ";
		//else
		//	$sqljenisop = "";
		
		// 08-08-2014
		if ($jenis_op_1 != '' || $jenis_op_2 != '' || $jenis_op_3 != '') {
			$stringjenisop = "";
			$sqljenisop = " AND b.jenis_op IN (";
			if ($jenis_op_1 != '')
				$stringjenisop.= $jenis_op_1.",";
			if ($jenis_op_2 != '')
				$stringjenisop.= $jenis_op_2.",";
			if ($jenis_op_3 != '')
				$stringjenisop.= $jenis_op_3.",";
			
			$pjgstring = strlen($stringjenisop);
			$stringjenisopnow = substr($stringjenisop, 0, $pjgstring-1);
			
			$sqljenisop.= $stringjenisopnow.") ";
		}
		else
			$sqljenisop = "";
		
		return $this->db->query(" SELECT SUM(a.n_count) AS jmlorder
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op
			".$ddate." ".$batal." ".$imotif." ".$icustomer. " ".$f_op_dropforcast." ".$sqljenisop);
	}
	
	// 30-07-2012
	function jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir, $jenis_op_1, $jenis_op_2, $jenis_op_3) {
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		// SELECT SUM(a.n_count) AS jmlorder, sum(c.n_deliver) as pemenuhan, b.i_customer
		$sql = " SELECT a.n_count as jmlorder, c.n_deliver as pemenuhan, b.i_op, c.i_product, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product  AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			//$sql.= " AND b.d_op >='$d_op_first' AND b.d_op <='$d_op_last' ";		
			//$sql.= " AND ((b.d_op >='$d_op_first' AND b.d_op <='$d_op_last') OR (d.d_do >='$d_op_first' AND d.d_do <='$d_op_last')) ";
			$sql.= " AND (b.d_op >='$d_op_first' AND b.d_op <='$d_op_last') ";
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
		if ($icustomer != '')
			$sql.= " AND b.i_customer = '$icustomer' ";
		
		// 15-07-2014
		//if ($jenis_op != '0')
		//	$sql.= " AND b.jenis_op='$jenis_op' ";
		
		// 08-08-2014
		if ($jenis_op_1 != '' || $jenis_op_2 != '' || $jenis_op_3 != '') {
			$stringjenisop = "";
			$sql.= " AND b.jenis_op IN (";
			if ($jenis_op_1 != '')
				$stringjenisop.= $jenis_op_1.",";
			if ($jenis_op_2 != '')
				$stringjenisop.= $jenis_op_2.",";
			if ($jenis_op_3 != '')
				$stringjenisop.= $jenis_op_3.",";
			
			$pjgstring = strlen($stringjenisop);
			$stringjenisopnow = substr($stringjenisop, 0, $pjgstring-1);
			
			$sql.= $stringjenisopnow.") ";
		}
		
		$sql.= "ORDER BY b.i_op, c.i_product"; //echo $sql; die();
		$query3= $this->db->query($sql);
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			$jmlorder = 0; $pemenuhan = 0; $temp_op = "";
			foreach ($hasil3 as $row3) {
				$pemenuhan+= $row3->pemenuhan;
				if ($temp_op != $row3->i_op) {
					$jmlorder+= $row3->jmlorder;
					$temp_op = $row3->i_op;
				}
			}
			
			$data_op = array(		'jmlorder'=> $jmlorder,	
									'pemenuhan'=> $pemenuhan,
									'i_customer'=> $icustomer
								);
		}
		else
			$data_op = '';
		return $data_op;
	}
	
	// 13-08-2012, ngantuk poll
	function clistopvsdo_dokosong($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir, $jenis_op_1, $jenis_op_2, $jenis_op_3) {
		$data_op = '';	
		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
		//echo $i_product."<br>";
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "satu ";
			$ddate	= " WHERE 't' ";
			$ipro	= " AND i_product='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";
		}else if($i_product=='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "dua ";
			$ddate	= " WHERE 't' ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			//echo "tiga ";
			$ddate	= "";
			$ipro	= " WHERE i_product='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";		
		}else{
			//echo "empat ";
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE f_stop_produksi='t' ":" WHERE f_stop_produksi='f' ";
		}
		
		if ($icustomer != '')
			$filtercust = " AND i_customer = '$icustomer' ";
		else
			$filtercust = "";
		
		if ($jenis_op_1 != '' || $jenis_op_2 != '' || $jenis_op_3 != '') {
			$stringjenisop = "";
			$filterjenisop= " AND jenis_op IN (";
			if ($jenis_op_1 != '')
				$stringjenisop.= $jenis_op_1.",";
			if ($jenis_op_2 != '')
				$stringjenisop.= $jenis_op_2.",";
			if ($jenis_op_3 != '')
				$stringjenisop.= $jenis_op_3.",";
			
			$pjgstring = strlen($stringjenisop);
			$stringjenisopnow = substr($stringjenisop, 0, $pjgstring-1);
			
			$filterjenisop.= $stringjenisopnow.") ";
		}
		else
			$filterjenisop="";
		
		$query3	= $this->db->query(" SELECT f_stop_produksi, 
				i_product, 
				e_product_motifname, 
				v_price,
      	sum(n_op) AS n_op, 
				sum(v_op) AS v_op,
				sum(n_do) AS n_do,
				sum(v_do) AS v_do,
				sum(n_faktur) AS n_faktur,
				sum(v_faktur) AS v_faktur,
				sum(n_op-n_do) AS n_selopdo,
				sum(v_op-v_do) AS v_selopdo
				FROM f_op_belum_do('$d_op_first','$d_op_last')
				".$ddate." ".$ipro." ".$fstopproduct." ".$f_op_dropforcast." ".$filtercust." ".$filterjenisop." 
				GROUP BY f_stop_produksi, i_product, e_product_motifname, v_price
				ORDER BY i_product ASC ");
						
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			
			foreach ($hasil3 as $row3) {
					$data_op[] = array(		
									'imotif'=> $row3->i_product,	
											'productmotif'=> $row3->e_product_motifname,	
											'stopproduct'=> $row3->f_stop_produksi,
											'hargaperunit'=> $row3->v_price,
											'jmlorder'=> $row->n_op,
											'nilaiorder'=> $row->v_op,
											'pemenuhan'=> $row->n_do,
											'nilaipemenuhan'=> $row->v_do,
											'selisihopdo'=> $row->n_selopdo,
											'nilaiselisih'=> $row->v_selopdo,
											'qty_faktur'=> $qty_faktur
								);
			}
		}
		else
			$data_op = '';
		return $data_op;
	}
	
	// 10-12-2012
	function explistopvsdo_dokosong($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir, $jenis_op_1, $jenis_op_2, $jenis_op_3) {
		$data_op = '';	
		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
		//echo $i_product."<br>";
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "satu ";
			$ddate	= " WHERE 't' ";
			$ipro	= " AND i_product='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";
		}else if($i_product=='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "dua ";
			$ddate	= " WHERE 't' ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			//echo "tiga ";
			$ddate	= "";
			$ipro	= " WHERE i_product='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND f_stop_produksi='t' ":" AND f_stop_produksi='f' ";		
		}else{
			//echo "empat ";
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE f_stop_produksi='t' ":" WHERE f_stop_produksi='f' ";
		}
		
		if ($icustomer != '')
			$filtercust = " AND i_customer = '$icustomer' ";
		else
			$filtercust = "";
		
		if ($jenis_op_1 != '' || $jenis_op_2 != '' || $jenis_op_3 != '') {
			$stringjenisop = "";
			$filterjenisop= " AND b.jenis_op IN (";
			if ($jenis_op_1 != '')
				$stringjenisop.= $jenis_op_1.",";
			if ($jenis_op_2 != '')
				$stringjenisop.= $jenis_op_2.",";
			if ($jenis_op_3 != '')
				$stringjenisop.= $jenis_op_3.",";
			
			$pjgstring = strlen($stringjenisop);
			$stringjenisopnow = substr($stringjenisop, 0, $pjgstring-1);
			
			$filterjenisop.= $stringjenisopnow.") ";
		}
		else
			$filterjenisop="";
		
		$query3	= $this->db->query(" SELECT f_stop_produksi, 
				i_product, 
				e_product_motifname, 
				v_price,
      	sum(n_op) AS n_op, 
				sum(v_op) AS v_op,
				sum(n_do) AS n_do,
				sum(v_do) AS v_do,
				sum(n_faktur) AS n_faktur,
				sum(v_faktur) AS v_faktur,
				sum(n_op-n_do) AS n_selopdo,
				sum(v_op-v_do) AS v_selopdo
				FROM 
				".$ddate." ".$ipro." ".$fstopproduct." ".$f_op_dropforcast." ".$filterjenisop."
				AND ".$filtercust."
				GROUP BY f_stop_produksi, i_product, e_product_motifname, v_price
				ORDER BY i_product ASC ");
						
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			
			foreach ($hasil3 as $row3) {
					$data_op[] = array(		
									'imotif'=> $row3->i_product,
											'productmotif'=> $row3->e_product_motifname,
											'stopproduct'=> $row3->f_stop_produksi,
											'hargaperunit'=> $row3->v_price,
											'jmlorder'=> $row3->n_op,
											'nilaiorder'=> $row3->v_op,
											'pemenuhan'=> $row3->n_do,
											'nilaipemenuhan'=> $row3->v_do,
											'selisihopdo'=> $row3->n_selopdo,
											'nilaiselisih'=> $row3->v_Selopdo,
											'qty_faktur'=> $row3->n_faktur
								);
			}
		}
		else
			$data_op = '';
		return $data_op;			
	}
	
	// 08-10-2013, get harga dari master harga
	function get_harga_barang($i_product) {
		$i_product_base = substr($i_product, 0, 7);
		$sql = " SELECT v_unitprice FROM tr_product_base WHERE i_product_base = '$i_product_base' ";
		
		$query	= $this->db->query($sql);
		if ($query->num_rows() > 0){
			$hasilrow = $query->row();
			$harganya	= $hasilrow->v_unitprice;
		}
		else {
			$sqlxx = " SELECT v_price FROM tr_product_price WHERE i_product = '$i_product_base' ";
			
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() > 0){
				$hasilrowxx = $queryxx->row();
				$harganya	= $hasilrowxx->v_price;
			}
			else
				$harganya = 0;
		}
		return $harganya;
	}
}
?>
