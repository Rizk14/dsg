<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function hargaperpelanggan($imotif,$icustomer) {
		return $this->db->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		return $this->db->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}

	function lcustomer() {
		$query = $this->db->query(" SELECT * FROM tr_customer ORDER BY e_customer_name ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function logfiles($efilename,$iuserid) {

		$this->db->trans_begin();

		$qdate	= $this->db->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$this->db->insert('tm_files_log',$fields);

		if($this->db->trans_status()===FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}
		
	function clistopvsdo($i_product,$d_op_first,$d_op_last,$f_stop_produksi) {
		
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f' OR b.f_op_close='f') ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND (b.f_op_cancel='f') ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";		
		}	
		
		/* 26072011
		$qstr	= "
			SELECT 	c.f_stop_produksi AS stopproduct,
				d.i_product_motif AS imotif,
				d.e_product_motifname AS productmotif,
				c.v_unitprice AS unitprice,
				sum(a.n_count) AS op, 
				(sum(a.n_count) * c.v_unitprice) AS nilaiop,
				sum((a.n_count-a.n_residual)) AS delivery,
				(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido,
				(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo,
				((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo
			
			FROM tm_op_item a 
			
			INNER JOIN tm_op b ON a.i_op=b.i_op
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying)
			INNER JOIN tm_do f ON f.i_do=e.i_do
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product)
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice, a.n_count, a.n_residual ";
		*/

		$qstr	= "
			SELECT 	c.f_stop_produksi AS stopproduct,
				d.i_product_motif AS imotif,
				d.e_product_motifname AS productmotif,
				c.v_unitprice AS unitprice,
				sum(a.n_count) AS op, 
				(sum(a.n_count) * c.v_unitprice) AS nilaiop,
				sum((a.n_count-a.n_residual)) AS delivery,
				(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido,
				(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo,
				((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo
			
			FROM tm_op_item a 
			
			INNER JOIN tm_op b ON a.i_op=b.i_op
			INNER JOIN tm_do_item e ON e.i_op=b.i_op
			INNER JOIN tm_do f ON f.i_do=e.i_do
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product)
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice, a.n_count, a.n_residual ";
					
		$query	= $this->db->query($qstr);
		
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}

	function explistopvsdo($i_product,$d_op_first,$d_op_last,$f_stop_produksi) {
		
		if(($i_product!="" || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			//$batal	= " AND (b.f_op_cancel='f' OR b.f_op_close='f') ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product=="" || empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";
		} else if(($i_product!="" || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND (b.f_op_cancel='f') ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND (b.f_op_cancel='f') ";		
		}
		
		/* 26072011
		$qstr	= "
			SELECT 	c.f_stop_produksi AS stopproduct,
				d.i_product_motif AS imotif,
				d.e_product_motifname AS productmotif,
				c.v_unitprice AS unitprice,
				sum(a.n_count) AS op, 
				(sum(a.n_count) * c.v_unitprice) AS nilaiop,
				sum((a.n_count-a.n_residual)) AS delivery,
				(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido,
				(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo,
				((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo
			
			FROM tm_op_item a 
			
			INNER JOIN tm_op b ON a.i_op=b.i_op
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying)
			INNER JOIN tm_do f ON f.i_do=e.i_do
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product)
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice, a.n_count, a.n_residual ";
		*/

		$qstr	= "
			SELECT 	c.f_stop_produksi AS stopproduct,
				d.i_product_motif AS imotif,
				d.e_product_motifname AS productmotif,
				c.v_unitprice AS unitprice,
				sum(a.n_count) AS op, 
				(sum(a.n_count) * c.v_unitprice) AS nilaiop,
				sum((a.n_count-a.n_residual)) AS delivery,
				(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido,
				(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo,
				((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo
			
			FROM tm_op_item a 
			
			INNER JOIN tm_op b ON a.i_op=b.i_op
			INNER JOIN tm_do_item e ON e.i_op=b.i_op
			INNER JOIN tm_do f ON f.i_do=e.i_do
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product)
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice, a.n_count, a.n_residual ";
					
		return $this->db->query($qstr);
	}

	function lbarangjadiperpages($limit,$offset) {
		$query = $this->db->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		return $this->db->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC " );
	}	
	
	function flbarangjadi($key) {
		if(!empty($key)) {
			return $this->db->query("
					SELECT 	a.i_product_base AS iproduct,
						b.i_product_motif AS imotif,	
						b.e_product_motifname AS motifname,
						b.n_quantity AS qty
						
					FROM tr_product_base a 
					
					RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product 
					
					WHERE a.i_product_base LIKE '$key%' OR b.i_product_motif LIKE '$key%' 
					
					ORDER BY b.i_product_motif DESC ");
		}
	}
	
	function clistopvsdo_new($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir) {
		//=============================
		// 13-02-2013, (b.v_do_gross/b.n_deliver) AS unitprice dihilangkan
		$sql="select distinct UPPER(b.i_product) AS imotif, b.e_product_name AS productmotif, e.f_stop_produksi AS stopproduct, 
			 b.is_grosir 
			FROM tm_do a, tm_op c, tm_do_item b  
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(b.i_product) 
			INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
			where a.i_do = b.i_do 
			AND b.i_op=c.i_op AND c.f_op_cancel = 'f' AND a.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND c.d_op >='$d_op_first' AND c.d_op <='$d_op_last' ";
		if ($i_product != '')
			$sql.= " AND d.i_product_motif='$i_product' ";
		if ($f_stop_produksi=='TRUE')
			$sql.=" AND e.f_stop_produksi='t' ";
		else
			$sql.=" AND e.f_stop_produksi='f' ";
		if($fdropforcast=='1') {
			$sql.= " AND c.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$sql.= " AND c.f_op_dropforcast='f' ";
		}
		if ($is_grosir == '1')
			$sql.= " AND b.is_grosir = 't' ";
		else
			$sql.= " AND b.is_grosir = 'f' ";
		
		if ($icustomer != '')
			$sql.= " AND c.i_customer = '$icustomer' AND c.i_customer = a.i_customer ";
		
		$sql.= " AND c.f_op_cancel='f' ORDER BY imotif ASC";
			//echo $sql."<br>";
		//=============================
				
		$query	= $this->db->query($sql);
		$data_opdo = array();
		$detail_opdo = array();
		if($query->num_rows()>0) {
			//return $result	= $query->result();
			$hasil = $query->result();
			foreach ($hasil as $row) {
				// ambil jml OP
						$qjmlopall	= $this->mclass->jumopall($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row->imotif,$icustomer,$fdropforcast);
						if($qjmlopall->num_rows()>0) {
							$rjmlopall	= $qjmlopall->row();
							$jmlorderall	= $rjmlopall->jmlorder;
						}
						else
							$jmlorderall = 0;

						/*$qjmlopgrosir	= $this->mclass->jmlorder($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$row->imotif,$icustomer,$fdropforcast, 1);
						if($qjmlopgrosir->num_rows()>0) {
							$rjmlopgrosir	= $qjmlopgrosir->row();
							$jmlordergrosir	= $rjmlopgrosir->jmlorder;
						}
						else
							$jmlordergrosir = 0; */
						
						//13-02-2013, dikomen
						/*$qjmlopgrosir	= $this->mclass->jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row->imotif,$icustomer,$fdropforcast, 1);
												
						if (is_array($qjmlopgrosir))
							$jmlordergrosir = $qjmlopgrosir['jmlorder'];
						else */
							$jmlordergrosir = 0;
						//if ($row->imotif == 'TPT770100') echo " ".$jmlordergrosir." ";
						if ($is_grosir == '2')
							$selisihnya = $jmlorderall-$jmlordergrosir;
						else
							$selisihnya = $jmlordergrosir; 
						
						/*$qjmlorderpemenuhan	= $this->mclass->jmlorder($var_iproduct,$var_ddofirst,$var_ddolast,$var_stopproduct,
											$row->imotif,$icustomer,$fdropforcast,$is_grosir);
						if($qjmlorderpemenuhan->num_rows()>0) {
							$rjmlorderpemenuhan	= $qjmlorderpemenuhan->row(); */
						
						$qjmlorderpemenuhan	= $this->mclass->jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row->imotif,$icustomer,$fdropforcast,$is_grosir);
						if (is_array($qjmlorderpemenuhan)) {
						//	$hargaperunit = $row->unitprice;
							if ($is_grosir == '1')
								$jmlorder	= $qjmlorderpemenuhan['jmlorder'];
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $qjmlorderpemenuhan['pemenuhan'];
						//	$nilaiorder	= $jmlorder*$hargaperunit;
						//	$nilaipemenuhan	= $qjmlorderpemenuhan['pemenuhan']*$hargaperunit;
							$selisihopdo	= $jmlorder - $qjmlorderpemenuhan['pemenuhan'];
						//	$nilaiselisih	= $selisihopdo*$hargaperunit;
						}else{
							$jmlorder = $selisihnya;
							$pemenuhan = 0;
						//	$nilaiorder	= $jmlorder*$row->unitprice;
						//	$nilaipemenuhan	= 0;
							$selisihopdo	= $jmlorder;
						//	$nilaiselisih	= $jmlorder*$row->unitprice;
						//	$hargaperunit = $row->unitprice;
						}						
						
					/*	$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, 
								tm_faktur_do_item_t b, tr_product_base c, tr_product_motif d where 
								a.i_faktur = b.i_faktur AND a.f_faktur_cancel = 'f'
								AND c.i_product_base = d.i_product
								AND d.i_product_motif = b.i_product";

						$sqlxx.= " AND c.f_stop_produksi = '$row->stopproduct'
								AND a.d_faktur >='$var_ddofirst' AND a.d_faktur <='$var_ddolast' AND b.i_product ='$row->imotif'"; */
						
						// 13-02-2013, dikomen
					/*	$sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, tm_faktur_do_item_t b, tm_do c,
								tr_product_base d, tr_product_motif e  
								where a.i_faktur = b.i_faktur AND c.i_do = b.i_do
								AND d.i_product_base = e.i_product AND e.i_product_motif = b.i_product
								AND ((a.d_faktur >= '$d_op_first' AND a.d_faktur <= '$d_op_last') OR (c.d_do >='$d_op_first' 
								AND c.d_do<='$d_op_last')) AND b.i_product = '".$row->imotif."' AND a.f_faktur_cancel = 'f' 
								AND c.f_do_cancel = 'f' AND d.f_stop_produksi = '".$f_stop_produksi."' ";
												 
						if ($icustomer != '')
							$sqlxx.=" AND c.i_customer = '$icustomer' ";
						
						$query3	= $this->db->query($sqlxx);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_faktur	= $hasilrow->jumnya;
							if ($qty_faktur == '')
								$qty_faktur = 0;
						} */
				
				$data_opdo[] = array(		'imotif'=> $row->imotif,	
											'productmotif'=> $row->productmotif,	
											'stopproduct'=> $row->stopproduct,
										//	'hargaperunit'=> $hargaperunit,
											'jmlorder'=> $jmlorder,
										//	'nilaiorder'=> $nilaiorder,
											'pemenuhan'=> $pemenuhan,
										//	'nilaipemenuhan'=> $nilaipemenuhan,
											'selisihopdo'=> $selisihopdo
										//	'nilaiselisih'=> $nilaiselisih,
										//	'qty_faktur'=> $qty_faktur
											);
			} // end foreach
		}
		else 
			$data_opdo = '';
		return $data_opdo;		
	}
	
	// useless anymore, ga dipake
	function clistopvsdorinci_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif) {
		
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= "";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else {
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		}
		
		/* 26072011
		return $this->db->query("
			SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			*/

		return $this->db->query(" SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON e.i_op=b.i_op
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
	}
	
	function explistopvsdo_new($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir) {
		
		//=============================
		//13-02-2013, (b.v_do_gross/b.n_deliver) AS unitprice, dihilangkan
		$sql="select distinct UPPER(b.i_product) AS imotif, b.e_product_name AS productmotif, e.f_stop_produksi AS stopproduct, 
			 b.is_grosir 
			FROM tm_do a, tm_op c, tm_do_item b  
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(b.i_product) 
			INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
			where a.i_do = b.i_do 
			AND b.i_op=c.i_op AND c.f_op_cancel = 'f' AND a.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND c.d_op >='$d_op_first' AND c.d_op <='$d_op_last' ";
		if ($i_product != '')
			$sql.= " AND d.i_product_motif='$i_product' ";
		if ($f_stop_produksi=='TRUE')
			$sql.=" AND e.f_stop_produksi='t' ";
		else
			$sql.=" AND e.f_stop_produksi='f' ";
		if($fdropforcast=='1') {
			$sql.= " AND c.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$sql.= " AND c.f_op_dropforcast='f' ";
		}
		if ($is_grosir == '1')
			$sql.= " AND b.is_grosir = 't' ";
		else
			$sql.= " AND b.is_grosir = 'f' ";
		
		if ($icustomer != '')
			$sql.= " AND c.i_customer = '$icustomer' AND c.i_customer = a.i_customer ";
			
		$sql.= " AND c.f_op_cancel='f' ORDER BY imotif ASC";
			//echo $sql;
		//=============================
				
		return $query	= $this->db->query($sql);
				
		/*if($query->num_rows()>0) {
			return $result	= $query->result();
		} */
		// ======= 13-07-2012 ============
	}
	
	function explistopvsdorinci_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif) {
		
		if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product=='kosong') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else if(($i_product!='kosong' && !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			$ddate	= " ";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		} else {
			$ddate	= " ";
			$ipro	= " ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND d.i_product_motif='$iproductmotif' ";
		}	
		
		/* 26072011
		return $this->db->query("
			SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON cast(e.i_op AS character varying)=cast(b.i_op_code AS character varying) 
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			*/

		return $this->db->query(" SELECT sum(a.n_count) AS op, 
			(sum(a.n_count) * c.v_unitprice) AS nilaiop, 
			sum((a.n_count-a.n_residual)) AS delivery, 
			(sum((a.n_count-a.n_residual)) * c.v_unitprice) AS nilaido, 
			(sum(a.n_count)-sum((a.n_count-a.n_residual))) AS selopdo, 
			((sum(a.n_count) * c.v_unitprice)-(sum((a.n_count-a.n_residual)) * c.v_unitprice)) AS nilaiselopdo 
			
			FROM tm_op_item a 
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item e ON e.i_op=b.i_op
			INNER JOIN tm_do f ON f.i_do=e.i_do 
			INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
			
			".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$imotif." ".$f_op_dropforcast."
			
			GROUP BY a.i_product, c.f_stop_produksi, d.i_product_motif, d.e_product_motifname, c.v_unitprice ");
			
	}		
	
	function jmlorder($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir) {
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		$sql = " SELECT SUM(a.n_count) AS jmlorder, 
			sum(c.n_deliver) as pemenuhan, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product  AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			$sql.= " AND b.d_op >='$d_op_first' AND b.d_op <='$d_op_last' ";		
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
		if ($icustomer != '')
			$sql.=" AND b.i_customer = '$icustomer' ";
		$sql.=	" GROUP BY b.i_customer"; //echo $sql."<br>";
		
		return $this->db->query($sql);
				
	}
	
	// 03-07-2012
	function jumopall($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast) {

		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
				
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product=='') && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last' ) ";
			$batal	= " AND b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last=="") && ($d_op_first==0 || $d_op_last==0)) ) {
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' ";
		}else{
			$ddate	= "";
			$batal	= " WHERE b.f_op_cancel='f' ";
			$imotif	= " AND a.i_product='$iproductmotif' ";
			if ($icustomer != '')
				$icustomer = " AND b.i_customer='$icustomer' "; 
		}
		
		return $this->db->query(" SELECT SUM(a.n_count) AS jmlorder
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op
			".$ddate." ".$batal." ".$imotif." ".$icustomer. " ".$f_op_dropforcast." ");
	}
	
	// 30-07-2012
	function jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,$iproductmotif,$icustomer,$fdropforcast, $is_grosir) {
		//sum((a.n_count-a.n_residual)) AS pemenuhan, ini diganti dgn sum(c.n_deliver)
		// SELECT SUM(a.n_count) AS jmlorder, sum(c.n_deliver) as pemenuhan, b.i_customer
		$sql = " SELECT a.n_count as jmlorder, c.n_deliver as pemenuhan, b.i_op, c.i_product, b.i_customer
			FROM tm_op_item a
			INNER JOIN tm_op b ON a.i_op=b.i_op 
			INNER JOIN tm_do_item c ON b.i_op = c.i_op
			INNER JOIN tm_do d ON c.i_do = d.i_do 
			WHERE c.i_product = a.i_product  AND c.i_product = '$iproductmotif'
			AND b.f_op_cancel = 'f' AND d.f_do_cancel = 'f' ";
		if ($d_op_first != '' && $d_op_last!='')
			//$sql.= " AND b.d_op >='$d_op_first' AND b.d_op <='$d_op_last' ";		
			//$sql.= " AND ((b.d_op >='$d_op_first' AND b.d_op <='$d_op_last') OR (d.d_do >='$d_op_first' AND d.d_do <='$d_op_last')) ";
			$sql.= " AND (b.d_op >='$d_op_first' AND b.d_op <='$d_op_last') ";
		if ($is_grosir == '1')
			$sql.= " AND c.is_grosir = 't' ";
		else
			$sql.= " AND c.is_grosir = 'f' ";

		if($fdropforcast=='1')
			$sql.= " AND b.f_op_dropforcast='t' ";
		elseif($fdropforcast=='2')
			$sql.= " AND b.f_op_dropforcast='f' ";
		if ($icustomer != '')
			$sql.= " AND b.i_customer = '$icustomer' ";
		$sql.= "ORDER BY b.i_op, c.i_product"; //echo $sql; die();
		$query3= $this->db->query($sql);
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			$jmlorder = 0; $pemenuhan = 0; $temp_op = "";
			foreach ($hasil3 as $row3) {
				$pemenuhan+= $row3->pemenuhan;
				if ($temp_op != $row3->i_op) {
					$jmlorder+= $row3->jmlorder;
					$temp_op = $row3->i_op;
				}
			}
			
			$data_op = array(		'jmlorder'=> $jmlorder,	
									'pemenuhan'=> $pemenuhan,
									'i_customer'=> $icustomer
								);
		}
		else
			$data_op = '';
		return $data_op;
	}
	
	// 13-08-2012, ngantuk poll
	function clistopvsdo_dokosong($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir) {		
		$data_op = '';	
		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
		//echo $i_product."<br>";
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "satu ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if($i_product=='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "dua ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			//echo "tiga ";
			$ddate	= "";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
		}else{
			//echo "empat ";
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}
		
		if ($icustomer != '')
			$filtercust = " AND b.i_customer = '$icustomer' ";
		else
			$filtercust = "";
		
		//13-02-2013, c.v_unitprice AS unitprice dihilangkan
		$query3	= $this->db->query(" SELECT c.f_stop_produksi AS stopproduct, 
				UPPER(a.i_product) AS imotif, 
				UPPER(a.e_product_name) AS productmotif
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$f_op_dropforcast."
				AND (a.n_count-a.n_residual)=0 ".$filtercust."
				GROUP BY c.f_stop_produksi, UPPER(a.i_product), UPPER(a.e_product_name)	
				ORDER BY UPPER(a.i_product) ASC ");
						
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			
			foreach ($hasil3 as $row3) {
				/*if ($row3->imotif == 'TPT070100')
					echo "select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif'"; */
				if ($icustomer != '')
					$filtercust = " AND a.i_customer = '$icustomer' ";
				else
					$filtercust = "";
				$queryxx	= $this->db->query(" select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif' AND a.f_do_cancel = 'f' ".$filtercust);
				if ($queryxx->num_rows() == 0){
					// ambil jml OP
						$qjmlopall	= $this->mclass->jumopall($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row3->imotif,$icustomer,$fdropforcast);
						if($qjmlopall->num_rows()>0) {
							$rjmlopall	= $qjmlopall->row();
							$jmlorderall	= $rjmlopall->jmlorder;
						}
						else
							$jmlorderall = 0;
						//13-02-2013, dikomen
					/*	$qjmlopgrosir	= $this->mclass->jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row3->imotif,$icustomer,$fdropforcast, 1);
												
						if (is_array($qjmlopgrosir))
							$jmlordergrosir = $qjmlopgrosir['jmlorder'];
						else */
							$jmlordergrosir = 0; 

						if ($is_grosir == '2')
							$selisihnya = $jmlorderall-$jmlordergrosir;
						else
							$selisihnya = $jmlordergrosir; 
												
						$qjmlorderpemenuhan	= $this->mclass->jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row3->imotif,$icustomer,$fdropforcast,$is_grosir);
						if (is_array($qjmlorderpemenuhan)) {
						//	$hargaperunit = $row3->unitprice;
							if ($is_grosir == '1')
								$jmlorder	= $qjmlorderpemenuhan['jmlorder'];
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $qjmlorderpemenuhan['pemenuhan'];
						//	$nilaiorder	= $jmlorder*$hargaperunit;
						//	$nilaipemenuhan	= $qjmlorderpemenuhan['pemenuhan']*$hargaperunit;
							$selisihopdo	= $jmlorder - $qjmlorderpemenuhan['pemenuhan'];
						//	$nilaiselisih	= $selisihopdo*$hargaperunit;
						}else{
						//	$hargaperunit = $row3->unitprice;
							$jmlorder	= $selisihnya;
							$pemenuhan = 0;
						//	$nilaiorder	= $selisihnya*$hargaperunit;
						//	$nilaipemenuhan	= 0;
							$selisihopdo	= $selisihnya;
						//	$nilaiselisih	= $selisihnya*$hargaperunit;
							
						}						
						
						/* $sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, tm_faktur_do_item_t b, tm_do c,
								tr_product_base d, tr_product_motif e  
								where a.i_faktur = b.i_faktur AND c.i_do = b.i_do
								AND d.i_product_base = e.i_product AND e.i_product_motif = b.i_product
								AND ((a.d_faktur >= '$d_op_first' AND a.d_faktur <= '$d_op_last') OR (c.d_do >='$d_op_first' 
								AND c.d_do<='$d_op_last')) AND b.i_product = '".$row3->imotif."' AND a.f_faktur_cancel = 'f' 
								AND c.f_do_cancel = 'f' AND d.f_stop_produksi = '".$f_stop_produksi."' ";
						 
						if ($icustomer != '')
							$sqlxx.=" AND c.i_customer = '$icustomer' ";
						 						
						$query3	= $this->db->query($sqlxx);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_faktur	= $hasilrow->jumnya;
							if ($qty_faktur == '')
								$qty_faktur = 0;
						} */
					
					$data_op[] = array(		
									'imotif'=> $row3->imotif,	
											'productmotif'=> $row3->productmotif,	
											'stopproduct'=> $row3->stopproduct,
										//	'hargaperunit'=> $hargaperunit,
											'jmlorder'=> $jmlorder,
										//	'nilaiorder'=> $nilaiorder,
											'pemenuhan'=> $pemenuhan,
										//	'nilaipemenuhan'=> $nilaipemenuhan,
											'selisihopdo'=> $selisihopdo
										//	'nilaiselisih'=> $nilaiselisih,
										//	'qty_faktur'=> $qty_faktur
								);
				}
			}
		}
		else
			$data_op = '';
		return $data_op;			
	}
	
	// 10-12-2012
	function explistopvsdo_dokosong($icustomer,$i_product,$d_op_first,$d_op_last,$f_stop_produksi,$fdropforcast, $is_grosir) {		
		$data_op = '';	
		if($fdropforcast=='1') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='t' ";
		}elseif($fdropforcast=='2') {
			$f_op_dropforcast	= " AND b.f_op_dropforcast='f' ";
		}else{
			$f_op_dropforcast	= "";
		}
		//echo $i_product."<br>";
		if(($i_product!='' || !empty($i_product)) && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "satu ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= " AND d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if($i_product=='' && (($d_op_first!="" && $d_op_last!="") && ($d_op_first!=0 && $d_op_last!=0)) ) {
			//echo "dua ";
			$ddate	= " WHERE (b.d_op BETWEEN '$d_op_first' AND '$d_op_last') ";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}else if(($i_product!='' || !empty($i_product)) && (($d_op_first=="" || $d_op_last!="") && ($d_op_first==0 || $d_op_last!=0)) ) {
			//echo "tiga ";
			$ddate	= "";
			$ipro	= " WHERE d.i_product_motif='$i_product' ";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" AND c.f_stop_produksi='t' ":" AND c.f_stop_produksi='f' ";		
			$batal	= " AND b.f_op_cancel='f' ";
		}else{
			//echo "empat ";
			$ddate	= "";
			$ipro	= "";
			$fstopproduct	= $f_stop_produksi=='TRUE'?" WHERE c.f_stop_produksi='t' ":" WHERE c.f_stop_produksi='f' ";
			$batal	= " AND b.f_op_cancel='f' ";
		}
		
		if ($icustomer != '')
			$filtercust = " AND b.i_customer = '$icustomer' ";
		else
			$filtercust = "";
		
		//13-02-2013, c.v_unitprice AS unitprice dihilangkan
		$query3	= $this->db->query(" SELECT c.f_stop_produksi AS stopproduct, 
				UPPER(a.i_product) AS imotif, 
				UPPER(a.e_product_name) AS productmotif
				
				FROM tm_op_item a 
				INNER JOIN tm_op b ON a.i_op=b.i_op 
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=trim(a.i_product) 
				INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(d.i_product) 
				
				".$ddate." ".$ipro." ".$fstopproduct." ".$batal." ".$f_op_dropforcast."
				AND (a.n_count-a.n_residual)=0 ".$filtercust."
				GROUP BY c.f_stop_produksi, UPPER(a.i_product), UPPER(a.e_product_name)		
				ORDER BY UPPER(a.i_product) ASC ");
						
		if ($query3->num_rows() > 0){ 
			$hasil3 = $query3->result();
			
			foreach ($hasil3 as $row3) {
				/*if ($row3->imotif == 'TPT070100')
					echo "select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif'"; */
				if ($icustomer != '')
					$filtercust = " AND a.i_customer = '$icustomer' ";
				else
					$filtercust = "";
				$queryxx	= $this->db->query(" select b.* from tm_do a, tm_do_item b WHERE 
								(a.d_do BETWEEN '$d_op_first' AND '$d_op_last')
								AND b.i_do = a.i_do 
								AND b.i_product = '$row3->imotif' AND a.f_do_cancel = 'f' ".$filtercust);
				if ($queryxx->num_rows() == 0){
					// ambil jml OP
						$qjmlopall	= $this->mclass->jumopall($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row3->imotif,$icustomer,$fdropforcast);
						if($qjmlopall->num_rows()>0) {
							$rjmlopall	= $qjmlopall->row();
							$jmlorderall	= $rjmlopall->jmlorder;
						}
						else
							$jmlorderall = 0;
						
					/*	$qjmlopgrosir	= $this->mclass->jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row3->imotif,$icustomer,$fdropforcast, 1);
												
						if (is_array($qjmlopgrosir))
							$jmlordergrosir = $qjmlopgrosir['jmlorder'];
						else */
							$jmlordergrosir = 0; 

						if ($is_grosir == '2')
							$selisihnya = $jmlorderall-$jmlordergrosir;
						else
							$selisihnya = $jmlordergrosir; 
												
						$qjmlorderpemenuhan	= $this->mclass->jmlorder_new($i_product,$d_op_first,$d_op_last,$f_stop_produksi,
											$row3->imotif,$icustomer,$fdropforcast,$is_grosir);
						if (is_array($qjmlorderpemenuhan)) {
						//	$hargaperunit = $row3->unitprice;
							if ($is_grosir == '1')
								$jmlorder	= $qjmlorderpemenuhan['jmlorder'];
							else
								$jmlorder = $selisihnya;
							$pemenuhan = $qjmlorderpemenuhan['pemenuhan'];
						//	$nilaiorder	= $jmlorder*$hargaperunit;
						//	$nilaipemenuhan	= $qjmlorderpemenuhan['pemenuhan']*$hargaperunit;
							$selisihopdo	= $jmlorder - $qjmlorderpemenuhan['pemenuhan'];
						//	$nilaiselisih	= $selisihopdo*$hargaperunit;
						}else{
						//	$hargaperunit = $row3->unitprice;
							$jmlorder	= $selisihnya;
							$pemenuhan = 0;
						//	$nilaiorder	= $selisihnya*$hargaperunit;
						//	$nilaipemenuhan	= 0;
							$selisihopdo	= $selisihnya;
						//	$nilaiselisih	= $selisihnya*$hargaperunit;
							
						}						
						
						/* $sqlxx = "select sum(b.n_quantity) as jumnya from tm_faktur_do_t a, tm_faktur_do_item_t b, tm_do c,
								tr_product_base d, tr_product_motif e  
								where a.i_faktur = b.i_faktur AND c.i_do = b.i_do
								AND d.i_product_base = e.i_product AND e.i_product_motif = b.i_product
								AND ((a.d_faktur >= '$d_op_first' AND a.d_faktur <= '$d_op_last') OR (c.d_do >='$d_op_first' 
								AND c.d_do<='$d_op_last')) AND b.i_product = '".$row3->imotif."' AND a.f_faktur_cancel = 'f' 
								AND c.f_do_cancel = 'f' AND d.f_stop_produksi = '".$f_stop_produksi."' ";
						 
						if ($icustomer != '')
							$sqlxx.=" AND c.i_customer = '$icustomer' ";
						 						
						$query3	= $this->db->query($sqlxx);
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_faktur	= $hasilrow->jumnya;
							if ($qty_faktur == '')
								$qty_faktur = 0;
						} */
					
					$data_op[] = array(		
									'imotif'=> $row3->imotif,	
											'productmotif'=> $row3->productmotif,	
											'stopproduct'=> $row3->stopproduct,
										//	'hargaperunit'=> $hargaperunit,
											'jmlorder'=> $jmlorder,
										//	'nilaiorder'=> $nilaiorder,
											'pemenuhan'=> $pemenuhan,
										//	'nilaipemenuhan'=> $nilaipemenuhan,
											'selisihopdo'=> $selisihopdo
										//	'nilaiselisih'=> $nilaiselisih,
										//	'qty_faktur'=> $qty_faktur
								);
				}
			}
		}
		else
			$data_op = '';
		return $data_op;			
	}
}
?>
