<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}

  function get_gudang(){
if($this->session->userdata('gid') == 16){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
				                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
    }else{
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
				                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
				                        }
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_all_pembelian($jenis_beli, $acuan, $date_from, $date_to, $gudang) {
	  // 30-06-2015 DIMODIF JADI DARI 1 TABEL BAHAN BAKU/PEMBANTU AJA
		// ini bhn quilting		
	/*	$sql = " SELECT distinct b.kode_brg_makloon, e.nama_brg
					FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b, 
					tm_brg_hasil_makloon e
					WHERE a.id = b.id_sj_hasil_makloon 
					AND b.kode_brg_makloon = e.kode_brg
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' 
					ORDER BY e.nama_brg ASC ";
		$query	= $this->db->query($sql);

		$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
						WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg_makloon' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_qty FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b 
					WHERE a.id = b.id_sj_hasil_makloon 
					AND b.kode_brg_makloon = '$row1->kode_brg_makloon'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$qty	= $hasilrow->jum_qty;
				}
				else {
					$qty = '';
				}
																				
				$data_beli[] = array(		'kode_perk'=> "511.100",
											'kode_brg'=> $row1->kode_brg_makloon,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $qty
											);
			}
		} */
		
		// ini utk bahan baku biasa
		// 18-02-2016 DIMODIF Pencarian jenis beli=='ALL'
		 $pencarian='';
	  if ($jenis_beli != 0&& $acuan == '1'){
			$pencarian.="AND a.jenis_pembelian=$jenis_beli";
	  }
	  elseif ($jenis_beli !=0&& $acuan != '1'){
			$pencarian.="AND x1.jenis_pembelian=$jenis_beli";
	  }
		else{
			$pencarian.="";
	}
	
	
		if ($acuan == '1') {
			$sql = " SELECT distinct g.kode_kel_brg, b.id_brg, e.kode_brg, e.nama_brg, b.id_satuan, b.harga
					FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian  
					INNER JOIN tm_barang e ON b.id_brg = e.id 
					INNER JOIN tm_jenis_barang g ON g.id = e.id_jenis_barang
					WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_aktif = 't'" .$pencarian;
			if ($gudang != '0')
				$sql.= " AND e.id_gudang = '$gudang' ";
			$sql.=	" ORDER BY g.kode_kel_brg ASC, e.nama_brg ASC ";
		}
		else {
			$sql = " SELECT distinct g.kode_kel_brg, b.id_brg, e.kode_brg, e.nama_brg, b.id_satuan, b.harga
					FROM tm_payment_pembelian x1 
				INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
				INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
				INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
				INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
							
				INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
				INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian  
					INNER JOIN tm_barang e ON b.id_brg = e.id 
					INNER JOIN tm_jenis_barang g ON g.id = e.id_jenis_barang
					WHERE 
					x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
					AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_aktif = 't'" .$pencarian ;
			if ($gudang != '0')
				$sql.= " AND e.id_gudang = '$gudang' ";
			$sql.=" ORDER BY g.kode_kel_brg ASC, e.nama_brg ASC ";
		}
					
		$query	= $this->db->query($sql);
		
		//$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {					
				$query3	= $this->db->query(" SELECT kode_perkiraan FROM tm_kelompok_barang WHERE kode = '$row1->kode_kel_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_perk	= $hasilrow->kode_perkiraan; 
				}
				else {
					$kode_perk = '';
				}
				
				// modif 18-12-2015
				/*$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a 
						INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$satuan = '';
				} */
				$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$row1->id_satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$satuan = '';
				}
				
				// ------------------------------------------------------------------------------------
				$supplier='';
				if ($acuan == '1') {
					$sql2 = " SELECT distinct a.id_supplier
						FROM tm_pembelian a 
					INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian  
						WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.status_aktif = 't'AND b.id_brg = '$row1->id_brg' 
						AND b.harga = '$row1->harga'" .$pencarian ;
				}
				else {
					$sql2 = " SELECT distinct a.id_supplier
						FROM tm_payment_pembelian x1 
					INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
					INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
					INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
					INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
								
					INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
					INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian  
						WHERE x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')AND a.status_aktif = 't'
						AND b.id_brg = '$row1->id_brg' AND b.harga = '$row1->harga'".$pencarian ;
				}
						
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp FROM tm_supplier 
								WHERE id = '$row2->id_supplier' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_supplier	= $hasilrow->kode_supplier;
							$nama_supplier	= $hasilrow->nama;
							
							if ($hasilrow->pkp == 't')
								$pkp = "(PKP)";
							else
								$pkp = "(Non-PKP)";
							$supplier.= $kode_supplier." - ".$nama_supplier." ".$pkp."<br>";
						}
					}
				}
				// -----------------------------------------------------------------------------------
								
				if ($acuan == '1') {
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_qty, sum(b.diskon) as jum_diskon 
					FROM tm_pembelian a
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						WHERE b.id_brg = '$row1->id_brg'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.status_aktif = 't' AND b.id_satuan='$row1->id_satuan' 
						AND b.harga='$row1->harga'" .$pencarian);
					
					/*echo "SELECT sum(b.qty) as jum_qty, sum(b.diskon) as jum_diskon 
					FROM tm_pembelian a
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						WHERE b.id_brg = '$row1->id_brg'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' AND b.harga='$row1->harga' <br>"; */
				}
				else {
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_qty, sum(b.diskon) as jum_diskon
					FROM tm_payment_pembelian x1 
					INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
					INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
					INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
					INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
					
					INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						WHERE b.id_brg = '$row1->id_brg'
						AND x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')
						AND a.status_aktif = 't' AND b.id_satuan='$row1->id_satuan' 
						AND b.harga='$row1->harga' .$pencarian");
					
					/*echo "SELECT sum(b.qty) as jum_qty 
					FROM tm_payment_pembelian x1 
					INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
					INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
					INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
					INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
					
					INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						WHERE b.id_brg = '$row1->id_brg'
						AND x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' AND b.harga='$row1->harga' <br>"; */
				}
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$qty	= $hasilrow->jum_qty;
					$diskon	= $hasilrow->jum_diskon;
				}
				else {
					$qty = '';
					$diskon = '';
				}
				
				$nilai = ($qty*$row1->harga)-$diskon;
																
				$data_beli[] = array(		'kode_perk'=> $kode_perk,
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $row1->nama_brg,
											'satuan'=> $satuan,
											'qty'=> $qty,
											'harga'=> $row1->harga,
											'nilai'=> $nilai,
											'supplier'=> $supplier
											);
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  function get_all_pembelian_bulanan($jenis_beli, $date_from, $date_to) {
	  $pisah1 = explode("-", $date_from);
	  $bln1= $pisah1[1];
	  $thn1= $pisah1[2];
	  $pisah2 = explode("-", $date_to);
	  $bln2= $pisah2[1];
	  $thn2= $pisah2[2];
		
		// 30-06-2015 DIMODIFF, quilting ga dipake, digabung di tm_barang
		// ini bhn quilting		
	/*	$sql = " SELECT distinct b.kode_brg_makloon, e.nama_brg
					FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b, 
					tm_brg_hasil_makloon e
					WHERE a.id = b.id_sj_hasil_makloon 
					AND b.kode_brg_makloon = e.kode_brg
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' 
					ORDER BY e.nama_brg ASC ";
		$query	= $this->db->query($sql);

		$data_beli = array();
		$detail_bulanan = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
						WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg_makloon' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				// 30-08-2012, pake perulangan mulai dari bulan awal s/d bulan akhir
				if ($thn1 == $thn2) {
					$tahunnya = ''; $bulannya = '';
					for ($xx=$bln1; $xx<=$bln2; $xx++) {
						if ($xx == $bln1) {
							$tahunnya = $thn1;
							$bulannya = $bln1;
						}
						else {
							if ($xx == '1' && $bulannya == '12') { // jika bln sebelumnya adalah 12 (des) dan perulangan skrg adalah 1 (januari), maka tahunnya nambah
								$tahunnya++;
								$bulannya = $xx;
							}
							else {
								$bulannya = $xx;
							}
						}

						// ambil tgl terakhir di bln tsb
						$timeStamp1            =    mktime(0,0,0,$xx,1,$tahunnya);    //Create time stamp of the first day from the give date.
						$firstDay1            =     date('d',$timeStamp1);    //get first day of the given month
						list($y1,$m1,$t1)        =    explode('-',date('Y-m-t',$timeStamp1)); //Find the last date of the month and separating it
						$lastDayTimeStamp1    =    mktime(0,0,0,$m1,$t1,$y1);//create time stamp of the last date of the give month
						$lastDay1            =    date('d',$lastDayTimeStamp1);// Find last day of the month
						
						$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_qty FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b 
							WHERE a.id = b.id_sj_hasil_makloon 
							AND b.kode_brg_makloon = '$row1->kode_brg_makloon'
							AND a.tgl_sj >= '".$tahunnya."-".$xx."-01'
							AND a.tgl_sj <= '".$tahunnya."-".$xx."-".$lastDay1."'
							AND a.jenis_pembelian = '$jenis_beli' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty	= $hasilrow->jum_qty;
							if ($qty == '')
								$qty = 0;
						}
						else {
							$qty = '0';
						}
																		
						$detail_bulanan[] = array(	'kode_brg'=> $row1->kode_brg_makloon,
													'qty'=> $qty,
													'bulan'=> $xx,
													'tahun'=> $tahunnya
													);
						
					} // end for
				} // end if thn1==thn2
				
				$data_beli[] = array(		'kode_perk'=> "511.100",
												'kode_brg'=> $row1->kode_brg_makloon,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'detail_bulanan'=> $detail_bulanan
												);
				$detail_bulanan = array();
			}
		} */
		
		// ini utk bahan baku biasa
		$sql = " SELECT distinct g.kode_kel_brg, b.id_brg, e.kode_brg, e.nama_brg
					FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_jenis_barang g ON g.id = e.id_jenis_barang
					WHERE 
					a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					 AND a.status_aktif = 't'".$pencarian."
					ORDER BY g.kode_kel_brg ASC, e.nama_brg ASC ";
					
		$query	= $this->db->query($sql);
		
		//$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {					
				$query3	= $this->db->query(" SELECT kode_perkiraan FROM tm_kelompok_barang WHERE kode = '$row1->kode_kel_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_perk	= $hasilrow->kode_perkiraan; 
				}
				else {
					$kode_perk = '';
				}
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a
						INNER JOIN tm_satuan b ON a.satuan = b.id
						WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				// 30-08-2012, pake perulangan mulai dari bulan awal s/d bulan akhir
				if ($thn1 == $thn2) {
					$tahunnya = ''; $bulannya = '';
					for ($xx=$bln1; $xx<=$bln2; $xx++) {
						if ($xx == $bln1) {
							$tahunnya = $thn1;
							$bulannya = $bln1;
						}
						else {
							if ($xx == '1' && $bulannya == '12') { // jika bln sebelumnya adalah 12 (des) dan perulangan skrg adalah 1 (januari), maka tahunnya nambah
								$tahunnya++;
								$bulannya = $xx;
							}
							else {
								$bulannya = $xx;
							}
						}

						// ambil tgl terakhir di bln tsb
						$timeStamp1            =    mktime(0,0,0,$xx,1,$tahunnya);    //Create time stamp of the first day from the give date.
						$firstDay1            =     date('d',$timeStamp1);    //get first day of the given month
						list($y1,$m1,$t1)        =    explode('-',date('Y-m-t',$timeStamp1)); //Find the last date of the month and separating it
						$lastDayTimeStamp1    =    mktime(0,0,0,$m1,$t1,$y1);//create time stamp of the last date of the give month
						$lastDay1            =    date('d',$lastDayTimeStamp1);// Find last day of the month
						
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_qty FROM tm_pembelian a 
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
						WHERE  b.id_brg = '$row1->id_brg'
							AND a.tgl_sj >= '".$tahunnya."-".$xx."-01'
							AND a.tgl_sj <= '".$tahunnya."-".$xx."-".$lastDay1."'
							AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty	= $hasilrow->jum_qty;
							if ($qty == '')
								$qty = 0;
						}
						else {
							$qty = '0';
						}
																		
						$detail_bulanan[] = array(	'kode_brg'=> $row1->kode_brg,
													'nama_brg'=> $nama_brg,
													'qty'=> $qty,
													'bulan'=> $xx,
													'tahun'=> $tahunnya
													);
						
					} // end for
				}
				
				$data_beli[] = array(		'kode_perk'=> $kode_perk,
												'kode_brg'=> $row1->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'detail_bulanan'=> $detail_bulanan
												);
				
				$detail_bulanan = array();
				
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  // 10-11-2015
  function get_all_pembelian_gudang($jenis_beli, $acuan, $date_from, $date_to, $gudang) {
	  // 30-06-2015 DIMODIF JADI DARI 1 TABEL BAHAN BAKU/PEMBANTU AJA
	  		
		// ini utk bahan baku biasa
		if ($acuan == '1') {
			$sql = " SELECT distinct g.kode_kel_brg, b.id_brg, e.kode_brg, e.nama_brg, b.harga, 
					b.id_satuan, b.id_satuan_konversi, a.id_supplier
					FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian  
					INNER JOIN tm_barang e ON b.id_brg = e.id 
					INNER JOIN tm_jenis_barang g ON g.id = e.id_jenis_barang
					WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.status_aktif = 't' ";
			if ($jenis_beli != '0')
				$sql.= " AND a.jenis_pembelian = '$jenis_beli'  ";
			if ($gudang != '0')
				$sql.= " AND e.id_gudang = '$gudang' ";
		//	$sql.=	" ORDER BY g.kode_kel_brg ASC, e.nama_brg ASC ";
		$sql.=	" ORDER BY e.kode_brg ASC, e.nama_brg ASC ";
		}
		else {
			$sql = " SELECT distinct g.kode_kel_brg, b.id_brg, e.kode_brg, e.nama_brg, b.harga, 
					b.id_satuan, b.id_satuan_konversi, a.id_supplier
					FROM tm_payment_pembelian x1 
				INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
				INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
				INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
				INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
							
				INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
				INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian  
					INNER JOIN tm_barang e ON b.id_brg = e.id 
					INNER JOIN tm_jenis_barang g ON g.id = e.id_jenis_barang
					WHERE 
					x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
					AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')
				
					AND a.status_aktif = 't' ";
			if ($jenis_beli != '0')
				$sql.= " AND x1.jenis_pembelian = '$jenis_beli'  ";
			if ($gudang != '0')
				$sql.= " AND e.id_gudang = '$gudang' ";
			//$sql.=" ORDER BY g.kode_kel_brg ASC, e.nama_brg ASC ";
			$sql.=" ORDER BY e.kode_brg ASC, e.nama_brg ASC ";
		}
		//echo $sql;
					
		$query	= $this->db->query($sql);
		
		//$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {					
				$query3	= $this->db->query(" SELECT kode_perkiraan FROM tm_kelompok_barang WHERE kode = '$row1->kode_kel_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_perk	= $hasilrow->kode_perkiraan; 
				}
				else {
					$kode_perk = '';
				}
				
				// 10-11-2015 nama satuan dari id_satuan tabel tm_pembelian_detail
				$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_satuan b 
								WHERE b.id = '$row1->id_satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$satuan = '';
				}
				
				// satuan konversi
				$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_satuan b 
								WHERE b.id = '$row1->id_satuan_konversi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan_konversi	= $hasilrow->nama_satuan;
				}
				else {
					$satuan_konversi = 'Tidak Ada';
				}
				
				// supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp FROM tm_supplier 
								WHERE id = '$row1->id_supplier' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
					$pkp = $hasilrow->pkp;
				}
				else {
					$kode_supplier	= '';
					$nama_supplier	= '';
					$pkp = 'f';
				}
								
				if ($acuan == '1') {
					if ($jenis_beli != '0')
						$filterjenisbeli = " AND a.jenis_pembelian = '$jenis_beli' ";
					else
						$filterjenisbeli = "";
						
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_qty, sum(b.diskon) as jum_diskon 
					FROM tm_pembelian a
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						WHERE b.id_brg = '$row1->id_brg'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.status_aktif = 't' 
						AND b.harga='$row1->harga' AND a.id_supplier='$row1->id_supplier'
						AND b.id_satuan='$row1->id_satuan' ".$filterjenisbeli);
						
						//~ if ($row1->kode_brg == 'MIC0169')
							//~ echo" SELECT sum(b.qty) as jum_qty, sum(b.diskon) as jum_diskon 
					//~ FROM tm_pembelian a
						//~ INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						//~ WHERE b.id_brg = '$row1->id_brg'
						//~ AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						//~ AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						//~ AND a.status_aktif = 't' 
						//~ AND b.harga='$row1->harga' AND a.id_supplier='$row1->id_supplier'
						//~ AND b.id_satuan='$row1->id_satuan' ".$filterjenisbeli;
					
				}
				else {
					if ($jenis_beli != '0')
						$filterjenisbeli = " AND x1.jenis_pembelian = '$jenis_beli' ";
						
						
					else
						$filterjenisbeli = "";
						
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_qty, sum(b.diskon) as jum_diskon
					FROM tm_payment_pembelian x1 
					INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
					INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
					INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
					INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
					
					INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						WHERE b.id_brg = '$row1->id_brg'
						AND x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')
						 AND a.status_aktif = 't' AND b.harga='$row1->harga' AND a.id_supplier='$row1->id_supplier' ".$filterjenisbeli);
					
					//~ if ($row1->kode_brg == 'RIN0006')
						//~ echo "  SELECT sum(b.qty) as jum_qty, sum(b.diskon) as jum_diskon
					//~ FROM tm_payment_pembelian x1 
					//~ INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
					//~ INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
					//~ INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
					//~ INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
					//~ 
					//~ INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
						//~ INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						//~ WHERE b.id_brg = '$row1->id_brg'
						//~ AND x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
						//~ AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')
						 //~ AND a.status_aktif = 't' AND b.harga='$row1->harga' AND a.id_supplier='$row1->id_supplier' ".$filterjenisbeli; 
					//~ 
					
					/*echo "SELECT sum(b.qty) as jum_qty 
					FROM tm_payment_pembelian x1 
					INNER JOIN tm_payment_pembelian_detail x2 ON x1.id = x2.id_payment
					INNER JOIN tm_payment_pembelian_nofaktur x3 ON x2.id = x3.id_payment_pembelian_detail
					INNER JOIN tm_pembelian_nofaktur x4 ON x3.id_pembelian_nofaktur = x4.id
					INNER JOIN tm_pembelian_nofaktur_sj x5 ON x4.id = x5.id_pembelian_nofaktur
					
					INNER JOIN tm_pembelian a ON x5.id_sj_pembelian = a.id
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
						WHERE b.id_brg = '$row1->id_brg'
						AND x1.tgl >= to_date('$date_from','dd-mm-yyyy') 
						AND x1.tgl <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' AND b.harga='$row1->harga' <br>"; */
				}
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$qty	= $hasilrow->jum_qty;
					$diskon	= $hasilrow->jum_diskon;
				}
				else {
					$qty = '';
					$diskon = '';
				}
				
				// 10-11-2015. HARGA DAN QTY dikonversi ke satuan konversi. dan dibagi 1.1 
				// ========================= 08-10-2015 satuan konversi (dari mutasi stok berdasarkan harga ===================
				$query3	= $this->db->query(" SELECT rumus_konversi, angka_faktor_konversi, konversi_harga_mutasi_stok
									FROM tm_konversi_satuan
									WHERE id_satuan_awal = '$row1->id_satuan' AND id_satuan_konversi = '$row1->id_satuan_konversi' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					$konversi_harga_mutasi_stok = $hasilnya->konversi_harga_mutasi_stok;
				}else{
					// penambahan koding agar variable yg di bawah terbaca
					$rumus_konversi = '';
					$angka_faktor_konversi ='';
					$konversi_harga_mutasi_stok ='';
				}
				
				$hargapkp = $row1->harga;
				
				if ($pkp == 't') {
					$hargapkp = $row1->harga/1.1;
					
					if ($row1->id_satuan_konversi != 0) {
						/*if ($id_satuan == 2 && $id_satuan_konversi == 3)
							$hargapkp = $hargapkp/$angka_faktor_konversi;
						else if ($id_satuan == 6 && $id_satuan_konversi == 5)
							$hargapkp = $hargapkp/$angka_faktor_konversi;
						else if ($id_satuan == 2 && $id_satuan_konversi == 1)
							$hargapkp = $hargapkp*$angka_faktor_konversi; */
							
						// 08-10-2015
						if ($konversi_harga_mutasi_stok == '1') {
							$hargapkp = $hargapkp*$angka_faktor_konversi;
						}
						else if ($konversi_harga_mutasi_stok == '2') {
							$hargapkp = $hargapkp/$angka_faktor_konversi;
						}
						else if ($konversi_harga_mutasi_stok == '3') {
							$hargapkp = $hargapkp+$angka_faktor_konversi;
						}
						else if ($konversi_harga_mutasi_stok == '4') {
							$hargapkp = $hargapkp-$angka_faktor_konversi;
						}
					}
				}
				else {
					if ($row1->id_satuan_konversi != 0) {
						// 08-10-2015
						if ($konversi_harga_mutasi_stok == '1') {
							$hargapkp = $hargapkp*$angka_faktor_konversi;
						}
						else if ($konversi_harga_mutasi_stok == '2') {
							$hargapkp = $hargapkp/$angka_faktor_konversi;
						}
						else if ($konversi_harga_mutasi_stok == '3') {
							$hargapkp = $hargapkp+$angka_faktor_konversi;
						}
						else if ($konversi_harga_mutasi_stok == '4') {
							$hargapkp = $hargapkp-$angka_faktor_konversi;
						}
					}
				}
				
				// QTY
				if ($row1->id_satuan_konversi != 0) {
					if ($rumus_konversi == '1') {
						$qty_konv = $qty*$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '2') {
						$qty_konv = $qty/$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '3') {
						$qty_konv = $qty+$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '4') {
						$qty_konv = $qty-$angka_faktor_konversi;
					}
					else
						$qty_konv = $qty;
				}
				else
					$qty_konv = $qty;
				// ===================================================================================================
				
				$nilai = ($qty_konv*$hargapkp)-$diskon;
				
				if ($pkp == 't')
					$namapkp = 'PKP';
				else
					$namapkp = 'Non-PKP';
				$supplier = $kode_supplier." - ".$nama_supplier." (".$namapkp.")";
																
				$data_beli[] = array(		'kode_perk'=> $kode_perk,
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $row1->nama_brg,
											'satuan'=> $satuan,
											'satuan_konversi'=> $satuan_konversi,
											'qty'=> $qty_konv,
											'harga'=> $hargapkp,
											'nilai'=> $nilai,
											'supplier'=> $supplier
											);
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
    
}

