<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_all_pembelian($jenis_beli, $date_from, $date_to) {
	  // 30-06-2015 DIMODIF JADI DARI 1 TABEL BAHAN BAKU/PEMBANTU AJA
		// ini bhn quilting		
	/*	$sql = " SELECT distinct b.kode_brg_makloon, e.nama_brg
					FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b, 
					tm_brg_hasil_makloon e
					WHERE a.id = b.id_sj_hasil_makloon 
					AND b.kode_brg_makloon = e.kode_brg
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' 
					ORDER BY e.nama_brg ASC ";
		$query	= $this->db->query($sql);

		$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
						WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg_makloon' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_qty FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b 
					WHERE a.id = b.id_sj_hasil_makloon 
					AND b.kode_brg_makloon = '$row1->kode_brg_makloon'
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$qty	= $hasilrow->jum_qty;
				}
				else {
					$qty = '';
				}
																				
				$data_beli[] = array(		'kode_perk'=> "511.100",
											'kode_brg'=> $row1->kode_brg_makloon,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $qty
											);
			}
		} */
		
		// ini utk bahan baku biasa
		$sql = " SELECT distinct g.kode_kel_brg, b.id_brg, e.kode_brg, e.nama_brg
					FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian  
					INNER JOIN tm_barang e ON b.id_brg = e.id 
					INNER JOIN tm_jenis_barang g ON g.id = e.id_jenis_barang
					WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't'
					ORDER BY g.kode_kel_brg ASC, e.nama_brg ASC ";
					
		$query	= $this->db->query($sql);
		
		//$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {					
				$query3	= $this->db->query(" SELECT kode_perkiraan FROM tm_kelompok_barang WHERE kode = '$row1->kode_kel_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_perk	= $hasilrow->kode_perkiraan; 
				}
				else {
					$kode_perk = '';
				}
				
				$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a 
						INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$satuan = '';
				}
				
				$sql2 = " SELECT distinct b.harga FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
						WHERE b.id_brg = '$row1->id_brg'
						AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' ";
				$query2	= $this->db->query($sql2);
		
				$data_harga = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_qty FROM tm_pembelian a 
							INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
							WHERE b.id_brg = '$row1->id_brg'
							AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
							AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' AND b.harga='$row2->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty	= $hasilrow->jum_qty;
						}
						else {
							$qty = '';
						}
						
						$data_harga[] = array(	'harga'=> $row2->harga,
												'qty'=> $qty
											);
					}
				}
				else
					$data_harga = '';
																
				$data_beli[] = array(		'kode_perk'=> $kode_perk,
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $row1->nama_brg,
											'satuan'=> $satuan,
											'data_harga'=> $data_harga
											);
				$data_harga = array();
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
  
  /*function get_all_pembeliantanpalimit($jenis_beli, $date_from, $date_to){
		$query	= $this->db->query(" SELECT distinct g.kode_kel_brg, b.kode_brg, e.nama_brg, b.harga, a.kode_supplier 
					FROM tm_pembelian a, tm_pembelian_detail b, 
					tm_pembelian_nofaktur c, tm_pembelian_nofaktur_sj d, tm_barang e, tm_jenis_bahan f, tm_jenis_barang g
					WHERE a.id = b.id_pembelian AND c.id = d.id_pembelian_nofaktur AND a.kode_supplier = c.kode_supplier
					AND a.no_sj = d.no_sj AND b.kode_brg = e.kode_brg AND e.id_jenis_bahan = f.id AND f.id_jenis_barang = g.id
					AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.jenis_pembelian = '$jenis_beli' ");
	    
    return $query->result();  
  } */
  
  function get_all_pembelian_bulanan($jenis_beli, $date_from, $date_to) {
	  $pisah1 = explode("-", $date_from);
	  $bln1= $pisah1[1];
	  $thn1= $pisah1[2];
	  $pisah2 = explode("-", $date_to);
	  $bln2= $pisah2[1];
	  $thn2= $pisah2[2];
		
		// 30-06-2015 DIMODIFF, quilting ga dipake, digabung di tm_barang
		// ini bhn quilting		
	/*	$sql = " SELECT distinct b.kode_brg_makloon, e.nama_brg
					FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b, 
					tm_brg_hasil_makloon e
					WHERE a.id = b.id_sj_hasil_makloon 
					AND b.kode_brg_makloon = e.kode_brg
					AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' 
					ORDER BY e.nama_brg ASC ";
		$query	= $this->db->query($sql);

		$data_beli = array();
		$detail_bulanan = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
						WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg_makloon' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				// 30-08-2012, pake perulangan mulai dari bulan awal s/d bulan akhir
				if ($thn1 == $thn2) {
					$tahunnya = ''; $bulannya = '';
					for ($xx=$bln1; $xx<=$bln2; $xx++) {
						if ($xx == $bln1) {
							$tahunnya = $thn1;
							$bulannya = $bln1;
						}
						else {
							if ($xx == '1' && $bulannya == '12') { // jika bln sebelumnya adalah 12 (des) dan perulangan skrg adalah 1 (januari), maka tahunnya nambah
								$tahunnya++;
								$bulannya = $xx;
							}
							else {
								$bulannya = $xx;
							}
						}

						// ambil tgl terakhir di bln tsb
						$timeStamp1            =    mktime(0,0,0,$xx,1,$tahunnya);    //Create time stamp of the first day from the give date.
						$firstDay1            =     date('d',$timeStamp1);    //get first day of the given month
						list($y1,$m1,$t1)        =    explode('-',date('Y-m-t',$timeStamp1)); //Find the last date of the month and separating it
						$lastDayTimeStamp1    =    mktime(0,0,0,$m1,$t1,$y1);//create time stamp of the last date of the give month
						$lastDay1            =    date('d',$lastDayTimeStamp1);// Find last day of the month
						
						$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_qty FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b 
							WHERE a.id = b.id_sj_hasil_makloon 
							AND b.kode_brg_makloon = '$row1->kode_brg_makloon'
							AND a.tgl_sj >= '".$tahunnya."-".$xx."-01'
							AND a.tgl_sj <= '".$tahunnya."-".$xx."-".$lastDay1."'
							AND a.jenis_pembelian = '$jenis_beli' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty	= $hasilrow->jum_qty;
							if ($qty == '')
								$qty = 0;
						}
						else {
							$qty = '0';
						}
																		
						$detail_bulanan[] = array(	'kode_brg'=> $row1->kode_brg_makloon,
													'qty'=> $qty,
													'bulan'=> $xx,
													'tahun'=> $tahunnya
													);
						
					} // end for
				} // end if thn1==thn2
				
				$data_beli[] = array(		'kode_perk'=> "511.100",
												'kode_brg'=> $row1->kode_brg_makloon,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'detail_bulanan'=> $detail_bulanan
												);
				$detail_bulanan = array();
			}
		} */
		
		// ini utk bahan baku biasa
		$sql = " SELECT distinct g.kode_kel_brg, b.id_brg, e.kode_brg, e.nama_brg
					FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian 
					INNER JOIN tm_barang e ON e.id = b.id_brg 
					INNER JOIN tm_jenis_barang g ON g.id = e.id_jenis_barang
					WHERE 
					a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
					AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't'
					ORDER BY g.kode_kel_brg ASC, e.nama_brg ASC ";
					
		$query	= $this->db->query($sql);
		
		//$data_beli = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {					
				$query3	= $this->db->query(" SELECT kode_perkiraan FROM tm_kelompok_barang WHERE kode = '$row1->kode_kel_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_perk	= $hasilrow->kode_perkiraan; 
				}
				else {
					$kode_perk = '';
				}
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a
						INNER JOIN tm_satuan b ON a.satuan = b.id
						WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				// 30-08-2012, pake perulangan mulai dari bulan awal s/d bulan akhir
				if ($thn1 == $thn2) {
					$tahunnya = ''; $bulannya = '';
					for ($xx=$bln1; $xx<=$bln2; $xx++) {
						if ($xx == $bln1) {
							$tahunnya = $thn1;
							$bulannya = $bln1;
						}
						else {
							if ($xx == '1' && $bulannya == '12') { // jika bln sebelumnya adalah 12 (des) dan perulangan skrg adalah 1 (januari), maka tahunnya nambah
								$tahunnya++;
								$bulannya = $xx;
							}
							else {
								$bulannya = $xx;
							}
						}

						// ambil tgl terakhir di bln tsb
						$timeStamp1            =    mktime(0,0,0,$xx,1,$tahunnya);    //Create time stamp of the first day from the give date.
						$firstDay1            =     date('d',$timeStamp1);    //get first day of the given month
						list($y1,$m1,$t1)        =    explode('-',date('Y-m-t',$timeStamp1)); //Find the last date of the month and separating it
						$lastDayTimeStamp1    =    mktime(0,0,0,$m1,$t1,$y1);//create time stamp of the last date of the give month
						$lastDay1            =    date('d',$lastDayTimeStamp1);// Find last day of the month
						
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_qty FROM tm_pembelian a 
						INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
						WHERE  b.id_brg = '$row1->id_brg'
							AND a.tgl_sj >= '".$tahunnya."-".$xx."-01'
							AND a.tgl_sj <= '".$tahunnya."-".$xx."-".$lastDay1."'
							AND a.jenis_pembelian = '$jenis_beli' AND a.status_aktif = 't' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty	= $hasilrow->jum_qty;
							if ($qty == '')
								$qty = 0;
						}
						else {
							$qty = '0';
						}
																		
						$detail_bulanan[] = array(	'kode_brg'=> $row1->kode_brg,
													'nama_brg'=> $nama_brg,
													'qty'=> $qty,
													'bulan'=> $xx,
													'tahun'=> $tahunnya
													);
						
					} // end for
				}
				
				$data_beli[] = array(		'kode_perk'=> $kode_perk,
												'kode_brg'=> $row1->kode_brg,
												'nama_brg'=> $nama_brg,
												'satuan'=> $satuan,
												'detail_bulanan'=> $detail_bulanan
												);
				
				$detail_bulanan = array();
				
			} // endforeach header
		}
		else {
			$data_beli = '';
		}
		return $data_beli;
  }
    
}

