<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $kode_unit) {
		if ($cari=="all") {	
			$sql = "SELECT * FROM tm_sj_proses_jahit ";
			if ($kode_unit != '0')
				$sql.= " WHERE kode_unit = '$kode_unit' ";
			$sql.= " ORDER BY id DESC LIMIT ".$num." OFFSET ".$offset;
			
			$query	= $this->db->query($sql);
		} else {
			$sql = "SELECT * FROM tm_sj_proses_jahit WHERE UPPER(no_sj) like UPPER('%$cari%') ";
			if ($kode_unit != '0')
				$sql.= " WHERE kode_unit = '$kode_unit' ";
			$sql.= " ORDER BY id DESC LIMIT ".$num." OFFSET ".$offset;
			
			$query	= $this->db->query($sql);
		}
	
		$data_jahit = array();
		$detail_jahit = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$arr	= 0;
			
			foreach ($hasil as $row1) {
			
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit='$row1->kode_unit' " );
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				/*if($row1->kel_bhn=='1'){
					$nm_kel_brg	= "Bisbisan";
				}elseif($row1->kel_bhn=='2'){
					$nm_kel_brg	= "Bordir";
				}elseif($row1->kel_bhn=='3'){
					$nm_kel_brg	= "Print";
				}elseif($row1->kel_bhn=='4'){
					$nm_kel_brg	= "Asesoris";
				}elseif($row1->kel_bhn=='5'){
					$nm_kel_brg	= "Asesoris Murni";
				}elseif($row1->kel_bhn=='6'){
					$nm_kel_brg	= "Hasil Cutting";
				} */
				
				$query2	= $this->db->query( " SELECT * FROM tm_sj_proses_jahit_detail WHERE id_sj_proses_jahit='$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					$arr2	= 0;
					
					foreach ($hasil2 as $row2) {
						if($row2->kode_brg_jadi!='') {
							$query4	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' " );
							$hasilrow2	= $query4->row();
							$nmbr		= $hasilrow2->e_product_motifname;
							$kodebrgj	= $hasilrow2->i_product_motif;
						}else{
							$nmbr		= '';
							$kodebrgj	= '';
						}
						
						/*if ($row2->kel_bhn != '1') {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_barang a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
							else {
								$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
									FROM tm_brg_hasil_makloon a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$nama_brg = '';
									$satuan = '';
								} // 
							}
						}
						else {
							$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan 
								FROM tm_brg_hasil_bisbisan a, tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$nama_brg	= $hasilrow->nama_brg;
								$satuan	= $hasilrow->nama_satuan;
							}
						} */
						
						if($row2->kel_bhn=='1'){
							$nm_kel_brg	= "Bisbisan";
						}elseif($row2->kel_bhn=='2'){
							$nm_kel_brg	= "Bordir";
						}elseif($row2->kel_bhn=='3'){
							$nm_kel_brg	= "Print";
						}elseif($row2->kel_bhn=='4'){
							$nm_kel_brg	= "Asesoris";
						}elseif($row2->kel_bhn=='5'){
							$nm_kel_brg	= "Asesoris Murni";
						}elseif($row2->kel_bhn=='6'){
							$nm_kel_brg	= "Hasil Cutting";
						}
						
						$detail_jahit[$arr2] = array('id' => $row2->id,
									'kel_bhn' => $row2->kel_bhn,
									'nm_kel_brg' => $nm_kel_brg,
									'nm_brg' => $nmbr,
									'kode_brg_jadi' => $kodebrgj,
									'kode_brg' => $row2->kode_brg,
									//'nama_brg' => $nama_brg,
									'qty_bhn' => $row2->qty_bhn,
									'keterangan' => $row2->keterangan,
									'status_jahit' => $row2->status_jahit,
									'diprint' => $row2->diprint,
									'is_dacron' => $row2->is_dacron,
									'ukuran' => $row2->ukuran,
									'id_stok' => $row2->id_stok
								);
						
						$arr2+=1;					
					}
				}
				else {
					$detail_jahit = '';
				}
				
				$data_jahit[$arr] = array('id' => $row1->id,	
							//'kel_bhn' => $row1->kel_bhn,
							//'nm_kel_brg' => $nm_kel_brg,
							'no_sj' => $row1->no_sj,
							'tgl_sj' => $row1->tgl_sj,
							'kode_unit' => $row1->kode_unit,
							'nama_unit' => $nama_unit,
							'tgl_update' => $row1->tgl_update,
							'status_edit' => $row1->status_edit,
							'status_jahit' => $row1->status_jahit,
							//'makloon_internal' => $row1->makloon_internal,
							'keterangan' => $row1->keterangan,
							'detail_jahit' => $detail_jahit
						);
				
				$detail_jahit = array();
				
				$arr+=1;
			}
		}
		else {
			$data_jahit = '';
		}
		return $data_jahit;
  }
  
  function getAlltanpalimit($cari, $kode_unit){
	if ($cari=="all") {		
		$sql = "SELECT * FROM tm_sj_proses_jahit ";
		if ($kode_unit != '0')
			$sql.= " WHERE kode_unit = '$kode_unit' ";
		
		$query	= $this->db->query($sql);
	}  else {
		$sql = "SELECT * FROM tm_sj_proses_jahit WHERE UPPER(no_sj) like UPPER('%$cari%') ";
		if ($kode_unit != '0')
			$sql.= " WHERE kode_unit = '$kode_unit' ";
		
		$query  = $this->db->query($sql);
	}
	return $query->result();  
  }
  
  function get_header_jahit($id_jahit){
    $query = $this->db->getwhere('tm_sj_proses_jahit',array('id'=>$id_jahit));
    return $query->result();
  }
  
  //function get_detail_jahit($id_jahit, $kel_brg){
  function get_detail_jahit($id_jahit){ // 29-02-2012
    $query = $this->db->getwhere('tm_sj_proses_jahit_detail',array('id_sj_proses_jahit'=>$id_jahit));
    $hasil = $query->result();
    $detail_jahit = array();
	
    foreach ($hasil as $row1) {
			if($row1->kode_brg_jadi!='') {
				$query1	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row1->kode_brg_jadi' ");
				$hasilrow1 = $query1->row();
				$mnbr		= $hasilrow1->e_product_motifname;
				$kodebrgj	= $row1->kode_brg_jadi;
			}else{
				$mnbr		= '';
				$kodebrgj	= '';
			}
			
			// 23-02-2012
				$list_id_stok = explode(";", $row1->id_stok);
				$list_kode_brg = explode(";", $row1->kode_brg); 
				$list_qty = explode(";", $row1->qty_bhn);
				/*$list_ukuran = explode(";", $ukuran);
				$list_diprint = explode(";", $diprint);
				$list_is_dacron = explode(";", $is_dacron); */
				$jumlah_list = count($list_id_stok)-1; 
				
				$i=1; $nama_brg = "";
				for($j=0; $j<$jumlah_list; $j++) {
					/*$query2	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg='$row1->kode_brg' ");
					$hasilrow2 = $query2->row();
					
					$qstokbisbisan	= $this->db->query(" SELECT kode_brg, kode_brg_bisbisan, stok FROM tm_stok_hasil_bisbisan 
									WHERE id='$list_id_stok[$j]' "); */
					//if($kel_brg=='1'){ // bisbisan
					if($row1->kel_bhn=='1'){ // bisbisan
						$sql	= " select a.kode_brg, a.nama_brg FROM tm_brg_hasil_bisbisan a
									WHERE a.kode_brg = '$list_kode_brg[$j]' ";
						$query2	= $this->db->query($sql);
						$hasilrow2 = $query2->row();
						$nama_brg.= $hasilrow2->nama_brg.";";
					}
					else if($row1->kel_bhn=='4'){ // asesoris
						$sql	= " select a.kode_brg, a.nama_brg FROM tm_barang a
									WHERE a.kode_brg = '$list_kode_brg[$j]' ";	
						$query2	= $this->db->query($sql);
						$hasilrow2 = $query2->row();
						$nama_brg.= $hasilrow2->nama_brg.";";
					}
					else if($row1->kel_bhn=='5'){ // asesoris murni
						$sql	= " select a.kode_brg, a.nama_brg FROM tm_barang a
									WHERE a.kode_brg = '$list_kode_brg[$j]' ";
						$query2	= $this->db->query($sql);
						$hasilrow2 = $query2->row();
						$nama_brg.= $hasilrow2->nama_brg.";";
					}
					else if($row1->kel_bhn=='6'){ // hasil cutting (ini bisa dari bhn baku ataupun bhn quilting)
						/*$sql	= " b.kode_brg, a.nama_brg as bhn_baku, d.nama_brg AS bhn_quilting, 
									FROM tm_stok_hasil_cutting b
									LEFT JOIN tm_barang a ON a.kode_brg=b.kode_brg
									LEFT JOIN tm_brg_hasil_makloon d ON d.kode_brg=b.kode_brg
									LEFT JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
									WHERE a.status_aktif='t' AND b.kode_brg_jadi = '$kode_brg_jadi' "; */
						
						$sql = " SELECT nama_brg FROM tm_barang WHERE kode_brg = '$list_kode_brg[$j]' ";
						$query2	= $this->db->query($sql);
						if ($query2->num_rows() > 0){
							$query2	= $this->db->query($sql);
							$hasilrow2 = $query2->row();
							$nama_brg.= $hasilrow2->nama_brg.";";
						}
						else {
							$sql = " SELECT nama_brg FROM tm_brg_hasil_makloon WHERE kode_brg = '$list_kode_brg[$j]' ";
							$query2	= $this->db->query($sql);
							if ($query2->num_rows() > 0){
								$hasilrow2 = $query2->row();
								$nama_brg.= $hasilrow2->nama_brg.";";
							}
						}
					} // end if kel_brg 
				}
								
			$detail_jahit[] = array('id' => $row1->id,
								'id_sj_proses_jahit' => $row1->id_sj_proses_jahit,
								'kode_brg_jadi' => $kodebrgj,
								'nm_brg_jadi' => $mnbr,
								'kode_brg' => $row1->kode_brg,
								'nm_brg' => $nama_brg,
								'qty_bhn' => $row1->qty_bhn,
								'keterangan' => $row1->keterangan,
								'diprint' => $row1->diprint,
								'is_dacron' => $row1->is_dacron,
								'ukuran' => $row1->ukuran,
								'id_stok' => $row1->id_stok,
								'kel_bhn' => $row1->kel_bhn
								);
	}
	return $detail_jahit;
  }  
    
  function cek_data($no_sj){
    $this->db->select("id from tm_sj_proses_jahit WHERE no_sj='$no_sj' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
		return $query->result();
	}
  }
  
  function save($idedit,$sjedit,$nosj, $tglsj,$kode_unit, $ket, $kel_brg,
			$kode_brg,$nm_brg,$kode_brg_jd,$qty,$ket_detail,$diprint, $is_dacron, $ukuran, $id_stok, $id_detail,$goedit) {
  // 29-02-2012, kel_brg pindah ke tabel detail
    $tgl = date("Y-m-d");

    $this->db->select(" * FROM tm_sj_proses_jahit WHERE no_sj = '$nosj' ", false);
    
	$query = $this->db->get();
    $hasil = $query->result();
	
    if ($goedit=='') {

		if(count($hasil)==0) {
			$data_header = array(
			//'kel_bhn' => $kel_brg,
			  'no_sj' => $nosj,
			  'tgl_sj' => $tglsj,
			  'kode_unit' => $kode_unit,
			  'status_jahit' => 'f',
			  'status_edit' => 'f',
			//  'makloon_internal' => $makloon_internal,
			  'keterangan' => $ket,
			  'tgl_input' => $tgl,
			  'tgl_update' => $tgl
			);
			
			$this->db->insert('tm_sj_proses_jahit',$data_header);
		}
			
			$query2	= $this->db->query(" SELECT id FROM tm_sj_proses_jahit ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id	= $hasilrow->id;
			
			if ($kode_brg!='' && ($qty!='' || $qty!=0) ) {
				$data_detail = array(
				  'id_sj_proses_jahit' => $id,
				  'kel_bhn' => $kel_brg,
				  'kode_brg_jadi' => $kode_brg_jd,
				  'kode_brg' => $kode_brg,
				  'qty_bhn' => $qty,
				  'status_jahit' => 'f',
				  'keterangan' => $ket_detail,
				  'diprint' => $diprint,
				  'is_dacron' => $is_dacron,
				  'ukuran' => $ukuran,
				  'id_stok' => $id_stok
				);
				
				$this->db->insert('tm_sj_proses_jahit_detail',$data_detail); //
				
				$query2	= $this->db->query(" SELECT id FROM tm_sj_proses_jahit_detail ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_jahit_detail = $hasilrow->id;
				
				// 23-02-2012
				$list_id_stok = explode(";", $id_stok);
				$list_kode_brg = explode(";", $kode_brg);
				$list_qty = explode(";", $qty);
				$list_ukuran = explode(";", $ukuran);
				$list_diprint = explode(";", $diprint);
				$list_is_dacron = explode(";", $is_dacron); //
				$jumlah_list = count($list_id_stok)-1; 
				
				$i=1; $harga_bhn_asesoris = "";
				for($j=0; $j<$jumlah_list; $j++) {
				
				if($kel_brg=='1'){ // hasil bisbisan
					$qstokbisbisan	= $this->db->query(" SELECT kode_brg, kode_brg_bisbisan, stok FROM tm_stok_hasil_bisbisan 
									WHERE id='$list_id_stok[$j]' ");
					if($qstokbisbisan->num_rows()>0){	
						$rstokbisbisan	= $qstokbisbisan->row();
						$kode_bhn_baku = $rstokbisbisan->kode_brg;
						$stok_terakhir	= ($rstokbisbisan->stok)-$list_qty[$j];
						$this->db->query("UPDATE tm_stok_hasil_bisbisan SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
										WHERE id='$list_id_stok[$j]' ");
					}else{
						$this->db->query(" INSERT INTO tm_stok_hasil_bisbisan(kode_brg,stok,tgl_update_stok, kode_brg_bisbisan) 
										VALUES('$kode_bhn_baku','$stok_terakhir','$tgl', '$list_kode_brg[$j]') ");
					}
					$harga_bhn_asesoris .= "0;";
					// insert ke tt_stok_hasil_bisbisan
					/*$this->db->query(" INSERT INTO tt_stok_hasil_bisbisan(kode_brg,no_bukti, keluar, saldo, tgl_input, kode_unit, kode_brg_bisbisan) 
										VALUES('$kode_bhn_baku', '$nosj', '$qty', '$stok_terakhir', '$tgl', '$kode_unit', '$kode_brg') "); */

				}/*elseif($kel_brg=='2'){
					$qstokbordir	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_bordir WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					if($qstokbordir->num_rows()>0){	
						$rstokbordir	= $qstokbordir->row();
						$stok_terakhir	= ($rstokbordir->stok)-$qty;
						$this->db->query("UPDATE tm_stok_hasil_bordir SET stok='$stok_terakhir', tgl_update_stok='$tgl' WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					}else{
						$this->db->query(" INSERT INTO tm_stok_hasil_bordir(kode_brg,kode_brg_jadi,stok,id_gudang,tgl_update_stok) VALUES('$kode_brg','$kode_brg_jd','$qty','0','$tgl') ");
					}
				} */
				/*elseif($kel_brg=='3'){ // msh ragu yg ini, apakah ambil stok dari bahan pembantu print (misalnya aplikasi print gendongan), ataukah terpisah
					$qstokprint	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_print WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					if($qstokprint->num_rows()>0){	
						$rstokprint	= $qstokprint->row();
						$stok_terakhir	= ($rstokprint->stok)-$qty;
						$this->db->query("UPDATE tm_stok_hasil_print SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
										WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					}else{
						$this->db->query(" INSERT INTO tm_stok_hasil_print(kode_brg,kode_brg_jadi,stok,tgl_update_stok) 
									VALUES('$kode_brg','$kode_brg_jd','$stok_terakhir','$tgl') ");
					}
					// insert ke tt_stok_hasil_print
					$this->db->query(" INSERT INTO tt_stok_hasil_print(kode_brg,no_bukti, keluar, saldo, tgl_input, kode_unit, kode_brg_jadi) 
										VALUES('$kode_brg', '$nosj','$qty', '$stok_terakhir', '$tgl', '$kode_unit', '$kode_brg_jd') ");
					
				}*/
				elseif($kel_brg=='4'){
					$qstokasesoris	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok 
											FROM tm_stok_hasil_asesoris WHERE id='$list_id_stok[$j]' ");
					if($qstokasesoris->num_rows()>0){	
						$rstokasesoris	= $qstokasesoris->row();
						$stok_terakhir	= ($rstokasesoris->stok)-$list_qty[$j];
						$this->db->query("UPDATE tm_stok_hasil_asesoris SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
								WHERE id='$list_id_stok[$j]' ");
					}else{
						$this->db->query(" INSERT INTO tm_stok_hasil_asesoris(kode_brg,kode_brg_jadi,ukuran,stok,tgl_update_stok) 
										VALUES('$list_kode_brg[$j]','$kode_brg_jd','$list_ukuran[$j]','$stok_terakhir','$tgl') ");
					}
					$harga_bhn_asesoris .= "0;";
					// insert ke tt_stok_hasil_asesoris
				/*	$this->db->query(" INSERT INTO tt_stok_hasil_asesoris(kode_brg,ukuran, no_bukti, keluar, saldo, tgl_input, kode_unit, kode_brg_jadi) 
										VALUES('$kode_brg', '$ukuran', '$nosj','$qty', '$stok_terakhir', '$tgl', '$kode_unit', '$kode_brg_jd') "); */
					
				}elseif($kel_brg=='5'){ // asesoris murni
					$qstokmurni	= $this->db->query(" SELECT kode_brg, stok FROM tm_stok WHERE id='$list_id_stok[$j]' ");
					if($qstokmurni->num_rows()>0){	
						$rstokmurni	= $qstokmurni->row();
						$stok_terakhir	= ($rstokmurni->stok)-$list_qty[$j];
						$this->db->query("UPDATE tm_stok SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
									WHERE id='$list_id_stok[$j]' ");
					}else{
						$this->db->query(" INSERT INTO tm_stok(kode_brg,stok,tgl_update_stok) 
									VALUES('$list_kode_brg[$j]','$stok_terakhir','$tgl') ");
					}
					// insert ke tt_stok, jgn lupa pengurangan ke stok harga
					// &&&&&&&&&&&&&&&&&&&&&&& modifikasi 5 okt 2011 utk stok harga &&&&&&&&&&&&&&&&&&&&&
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg = '$list_kode_brg[$j]' 
											ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; // 
							$harganya = $row2->harga; // 
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) // 
									$selisih = $stoknya-$list_qty[$j]; // 
								else
									$selisih = $stoknya+$temp_selisih; // 
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; // temp_selisih = -6
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$list_kode_brg[$j]' AND harga = '$harganya' "); //
									
									/*$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
											VALUES ('$kode_brg','$nosj', '$stoknya', '0', '$tgl', '$harganya') "); */
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $list_qty[$j]; // temp_selisih = -6
									break;
								}
							}
						} // end for
					}
					
					if ($selisih != 0) {
						$new_stok = $selisih; // new_stok = 4
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$list_kode_brg[$j]' AND harga = '$harganya' ");
						
						/*$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
												VALUES ('$kode_brg','$nosj', '".abs($temp_selisih)."', '$new_stok', '$tgl', '$harganya') ");*/
					}
					$harga_bhn_asesoris .= $harganya.";";
					// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end modifikasi 5 okt 2011

				}elseif($kel_brg=='6'){ // hsl cutting
					$qstokcutting	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_cutting 
										WHERE id='$list_id_stok[$j]' "); //
					if($qstokcutting->num_rows()>0){
						$rstokcutting	= $qstokcutting->row();
						$stok_terakhir	= ($rstokcutting->stok)-$list_qty[$j];
						$this->db->query("UPDATE tm_stok_hasil_cutting SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
										WHERE id='$list_id_stok[$j]' ");
					}else{
						$this->db->query(" INSERT INTO tm_stok_hasil_cutting(kode_brg,kode_brg_jadi,stok,total_gelar,id_gudang,
								tgl_update_stok, diprint, is_dacron) 
								VALUES('$list_kode_brg[$j]','$kode_brg_jd','$stok_terakhir','0','0','$tgl', 
								'$list_diprint[$j]', '$list_is_dacron[$j]') ");
					}
					
					$harga_bhn_asesoris .= "0;";
					// insert ke tt_stok_hasil_cutting
				/*	$this->db->query(" INSERT INTO tt_stok_hasil_cutting(kode_brg, no_bukti, keluar, saldo, tgl_input, kode_unit, kode_brg_jadi, diprint, is_dacron) 
										VALUES('$kode_brg', '$nosj','$qty', '$stok_terakhir', '$tgl', '$kode_unit', '$kode_brg_jd', '$diprint', '$is_dacron') "); */
					
				} // end if kel_brg
			  } // end for jumlah_list
			  $this->db->query("UPDATE tm_sj_proses_jahit_detail SET harga_bhn_asesoris = '$harga_bhn_asesoris'
							WHERE id = '$id_jahit_detail' ");
			} // end if kode_brg!=''

	} // end 0f goedit
  }
  
  function update_sj_jahit($id_edit,$kel_brg,$kode,$nm_brg, $kode_brg_jd, $qty, $qty_lama, $keterangan, $id, $goedit){
  		
	  $tgl	= date("Y-m-d");
	  
	  if ($kode !='' && $qty !='') {
			
			$kode_brg	= $kode;
			
			$data_detail = array(
				'kode_brg_jadi' => $kode_brg_jd,
				'kel_bhn' => $kel_brg,
				'kode_brg' => $kode,
				'qty_bhn' => $qty,
				'keterangan' => $keterangan );
			
			//$this->db->insert('tm_sj_proses_jahit_detail',$data_detail);
			$this->db->query(" UPDATE tm_sj_proses_jahit_detail SET qty_bhn='$qty', keterangan='$keterangan' WHERE id='$id' AND status_jahit='f' ");

				if($kel_brg=='1'){
					$qstokbisbisan	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_bisbisan WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					if($qstokbisbisan->num_rows()>0){	
						$rstokbisbisan	= $qstokbisbisan->row();
						$stok_terakhir	= (($rstokbisbisan->stok) + $qty_lama) - $qty;
						$this->db->query("UPDATE tm_stok_hasil_bisbisan SET stok='$stok_terakhir', tgl_update_stok='$tgl' WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					}
				}elseif($kel_brg=='2'){
					$qstokbordir	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_bordir WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					if($qstokbordir->num_rows()>0){	
						$rstokbordir	= $qstokbordir->row();
						$stok_terakhir	= (($rstokbordir->stok) + $qty_lama) - $qty;
						$this->db->query("UPDATE tm_stok_hasil_bordir SET stok='$stok_terakhir', tgl_update_stok='$tgl' WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					}
				}elseif($kel_brg=='3'){
					$qstokprint	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_print WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					if($qstokprint->num_rows()>0){	
						$rstokprint	= $qstokprint->row();
						$stok_terakhir	= (($rstokprint->stok) + $qty_lama) - $qty;
						$this->db->query("UPDATE tm_stok_hasil_print SET stok='$stok_terakhir', tgl_update_stok='$tgl' WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					}
				}elseif($kel_brg=='4'){
					$qstokasesoris	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_asesoris WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					if($qstokasesoris->num_rows()>0){	
						$rstokasesoris	= $qstokasesoris->row();
						$stok_terakhir	= (($rstokasesoris->stok) + $qty_lama) - $qty;
						$this->db->query("UPDATE tm_stok_hasil_asesoris SET stok='$stok_terakhir', tgl_update_stok='$tgl' WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					}
				}elseif($kel_brg=='5'){
					$qstokmurni	= $this->db->query(" SELECT kode_brg, stok FROM tm_stok WHERE kode_brg='$kode_brg' ");
					if($qstokmurni->num_rows()>0){	
						$rstokmurni	= $qstokmurni->row();
						$stok_terakhir	= (($rstokmurni->stok) + $qty_lama) - $qty;
						$this->db->query("UPDATE tm_stok SET stok='$stok_terakhir', tgl_update_stok='$tgl' WHERE kode_brg='$kode_brg' ");
					}					
				}elseif($kel_brg=='6'){
					$qstokcutting	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_cutting WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					if($qstokcutting->num_rows()>0){	
						$rstokcutting	= $qstokcutting->row();
						$stok_terakhir	= (($rstokcutting->stok) + $qty_lama) - $qty;
						$this->db->query("UPDATE tm_stok_hasil_cutting SET stok='$stok_terakhir', tgl_update_stok='$tgl' WHERE kode_brg='$kode_brg' AND kode_brg_jadi='$kode_brg_jd' ");
					}					
				}
			
	  }
  }  
  
  function delete($kode){		
		$tgl	= date("Y-m-d");
		
		$qprosesjhtdetail	= $this->db->query(" SELECT b.*, a.no_sj, a.kode_unit FROM tm_sj_proses_jahit a, 
								tm_sj_proses_jahit_detail b 
								WHERE a.id = b.id_sj_proses_jahit AND b.id_sj_proses_jahit='$kode' AND b.status_jahit='f' ");
		if($qprosesjhtdetail->num_rows()>0){
			//$result	= $qprosesjhtdetail->result();
			
			foreach($qprosesjhtdetail->result() as $row1){
			
				$kode_brg	= $row1->kode_brg;
				$kode_brg_jd= $row1->kode_brg_jadi;
				$qty_bhn	= $row1->qty_bhn;
				$diprint	= $row1->diprint;
				$is_dacron	= $row1->is_dacron;
				$ukuran		= $row1->ukuran;
				$id_stok	= $row1->id_stok;
				$harga_bhn_asesoris	= $row1->harga_bhn_asesoris;
				$no_sj		= $row1->no_sj;
				$kode_unit	= $row1->kode_unit;
				$kel_brg	= $row1->kel_bhn;
				
				// 23-02-2012
				$list_id_stok = explode(";", $id_stok);
				$list_kode_brg = explode(";", $kode_brg);
				$list_qty = explode(";", $qty_bhn);
				$list_ukuran = explode(";", $ukuran);
				$list_diprint = explode(";", $diprint);
				$list_is_dacron = explode(";", $is_dacron); 
				$list_harga_bhn_asesoris = explode(";", $harga_bhn_asesoris); 
				$jumlah_list = count($list_id_stok)-1; 
				
				$i=1;
				for($j=0; $j<$jumlah_list; $j++) {
					if($kel_brg=='1'){ // hasil bisbisan
						$qstokbisbisan	= $this->db->query(" SELECT kode_brg, kode_brg_bisbisan, stok FROM tm_stok_hasil_bisbisan 
										WHERE id='$list_id_stok[$j]' ");
						if($qstokbisbisan->num_rows()>0){
							$rstokbisbisan	= $qstokbisbisan->row();
							$kode_bhn_baku = $rstokbisbisan->kode_brg;
							$stok_terakhir	= ($rstokbisbisan->stok) + $list_qty[$j];
							$this->db->query("UPDATE tm_stok_hasil_bisbisan SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
										WHERE id='$list_id_stok[$j]' ");
						}
					}
					else if($kel_brg=='4'){ // makloon asesoris
						$qstokasesoris	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_asesoris 
										WHERE id='$list_id_stok[$j]' ");
						if($qstokasesoris->num_rows()>0){	
							$rstokasesoris	= $qstokasesoris->row();
							$stok_terakhir	= ($rstokasesoris->stok) + $list_qty[$j];
							$this->db->query("UPDATE tm_stok_hasil_asesoris SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
											WHERE id='$list_id_stok[$j]' ");
						}
					}
					else if($kel_brg=='5'){ // asesoris murni
						$qstokmurni	= $this->db->query(" SELECT kode_brg, stok FROM tm_stok WHERE id='$list_id_stok[$j]' ");
						if($qstokmurni->num_rows()>0){	
							$rstokmurni	= $qstokmurni->row();
							$stok_terakhir	= ($rstokmurni->stok) + $list_qty[$j];
							$this->db->query("UPDATE tm_stok SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
											WHERE id='$list_id_stok[$j]' ");
							
							// update stok berdasarkan harga 24-02-2012
							//cek stok terakhir tm_stok_harga, dan update stoknya
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$list_kode_brg[$j]' 
														AND harga = '$list_harga_bhn_asesoris[$j]' ");
							if ($query3->num_rows() == 0){
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							$stokreset2 = $stok_lama+$list_qty[$j];
							
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl' 
									where kode_brg= '$list_kode_brg[$j]' AND harga = '$list_harga_bhn_asesoris[$j]' 
									AND quilting = 'f' ");
						}
					} 
					else if($kel_brg=='6'){ // hasil cutting
						$qstokcutting	= $this->db->query(" SELECT kode_brg, kode_brg_jadi, stok FROM tm_stok_hasil_cutting 
										WHERE id='$list_id_stok[$j]' ");
						if($qstokcutting->num_rows()>0){
							$rstokcutting	= $qstokcutting->row();
							$stok_terakhir	= ($rstokcutting->stok) + $list_qty[$j];
							$this->db->query("UPDATE tm_stok_hasil_cutting SET stok='$stok_terakhir', tgl_update_stok='$tgl' 
										WHERE id='$list_id_stok[$j]' ");
						}					
					} // end if
					
				}
			}
			$this->db->delete('tm_sj_proses_jahit_detail', array('id_sj_proses_jahit' => $kode));
			$this->db->delete('tm_sj_proses_jahit', array('id' => $kode));
					
		}
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $kode_brg_jadi){
		if($kel_brg=='1'){ // bisbisan
			//$sql	= " a.kode_brg, a.nama_brg, a.id_jenis_bahan, a.id_gudang, b.kode_brg_jadi, b.stok, c.e_product_motifname FROM tm_barang a, tm_stok_hasil_bisbisan b, tr_product_motif c WHERE a.kode_brg=b.kode_brg AND b.kode_brg_jadi=c.i_product_motif AND a.status_aktif='t' AND b.stok > 0";
			$sql	= " b.id, a.kode_brg, a.nama_brg, d.nama AS nama_satuan, a.id_gudang, b.stok, b.kode_brg_jadi, c.e_product_motifname 
						FROM tm_brg_hasil_bisbisan a, tm_stok_hasil_bisbisan b, tr_product_motif c, tm_satuan d
						WHERE a.kode_brg=b.kode_brg_bisbisan AND b.kode_brg_jadi=c.i_product_motif 
						AND a.satuan = d.id AND a.status_aktif='t' AND b.kode_brg_jadi = '$kode_brg_jadi' AND b.stok > 0 ";
		}
		/*elseif($kel_brg=='2'){ // bordir (ini dari hasil cutting, jadi ga perlu ada pilihan bordir)
			$sql	= " a.kode_brg, a.nama_brg, a.id_jenis_bahan, a.id_gudang, b.kode_brg_jadi, b.stok, c.e_product_motifname 
						FROM tm_barang a, tm_stok_hasil_bordir b, tr_product_motif c WHERE a.kode_brg=b.kode_brg 
						AND b.kode_brg_jadi=c.i_product_motif AND a.status_aktif='t' AND b.stok > 0";	
		}
		elseif($kel_brg=='3'){ // print (ini ambil dari tm_barang, dan stoknya dari tm_stok)
			$sql	= " a.kode_brg, a.nama_brg, a.satuan, a.id_jenis_bahan, a.id_gudang, b.kode_brg_jadi, b.stok, c.e_product_motifname 
						FROM tm_barang a, tm_stok_hasil_print b, tr_product_motif c WHERE a.kode_brg=b.kode_brg 
						AND b.kode_brg_jadi=c.i_product_motif AND a.status_aktif='t' ";
		}*/
		elseif($kel_brg=='4'){ // asesoris
			$sql	= " b.id, a.kode_brg, a.nama_brg, d.nama AS nama_satuan, a.id_gudang, b.kode_brg_jadi, b.ukuran, b.stok, c.e_product_motifname 
						FROM tm_barang a, tm_stok_hasil_asesoris b, tr_product_motif c, tm_satuan d WHERE a.kode_brg=b.kode_brg 
						AND b.kode_brg_jadi=c.i_product_motif AND a.satuan = d.id AND a.status_aktif='t' 
						AND b.kode_brg_jadi = '$kode_brg_jadi' AND b.stok > 0 ";	
		}elseif($kel_brg=='5'){ // asesoris murni
			$sql	= " b.id, a.kode_brg, a.nama_brg, a.id_jenis_bahan, d.nama AS nama_satuan, a.id_gudang, b.stok FROM tm_barang a, 
						tm_stok b, tm_satuan d, tm_jenis_bahan e, tm_jenis_barang f
						WHERE a.kode_brg=b.kode_brg AND a.id_jenis_bahan = e.id AND e.id_jenis_barang = f.id AND f.kode_kel_brg = 'P'
						AND a.satuan = d.id AND a.status_aktif='t' AND b.stok > 0 ";
		}else if($kel_brg=='6'){ // hasil cutting, 25-02-2012 bhnnya bisa dari tm_barang ataupun tm_brg_hasil_makloon
			/*$sql	= " b.id, a.kode_brg, a.nama_brg, a.id_jenis_bahan, a.id_gudang, b.kode_brg_jadi, b.stok, c.e_product_motifname,
						b.is_dacron, b.diprint
						FROM tm_barang a, tm_stok_hasil_cutting b, tr_product_motif c WHERE a.kode_brg=b.kode_brg 
						AND b.kode_brg_jadi=c.i_product_motif AND a.status_aktif='t' AND b.kode_brg_jadi = '$kode_brg_jadi' 
						AND b.stok > 0 "; */
			$sql	= " b.id, b.kode_brg, b.kode_brg_jadi, b.stok, c.e_product_motifname, 
						a.nama_brg as bhn_baku, d.nama_brg AS bhn_quilting, 
						b.is_dacron, b.diprint
						FROM tm_stok_hasil_cutting b
						LEFT JOIN tm_barang a ON a.kode_brg=b.kode_brg
						LEFT JOIN tm_brg_hasil_makloon d ON d.kode_brg=b.kode_brg
						LEFT JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
						WHERE a.status_aktif='t' AND b.kode_brg_jadi = '$kode_brg_jadi' 
						AND b.stok > 0 ";
		}
		
		if ($cari!="all") {	
			if ($kel_brg == '5')
				$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			else {
				/*if ($kel_brg == '1')
					$sql.=" AND (UPPER(a.kode_brg) LIKE UPPER('%$cari%') OR UPPER(a.nama_brg) LIKE UPPER('%$cari%') 
						OR UPPER(b.kode_brg) LIKE UPPER('%$cari%') )";
				else */
				$sql.=" AND (UPPER(b.kode_brg) LIKE UPPER('%$cari%') OR UPPER(a.nama_brg) LIKE UPPER('%$cari%') 
						OR UPPER(d.nama_brg) LIKE UPPER('%$cari%')
						OR UPPER(b.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(c.e_product_motifname) LIKE UPPER('%$cari%'))";
			}
				
		}
		
		$sql.=" ORDER BY a.kode_brg DESC ";
		//$this->db->select($sql, false)->limit($num,$offset);
		$this->db->select($sql, false);

    $query = $this->db->get();
    return $query->result();
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $kode_brg_jadi){
		if($kel_brg=='1'){
			$sql	= " SELECT b.id, a.kode_brg, a.nama_brg, d.nama AS nama_satuan, a.id_gudang, b.stok, b.kode_brg_jadi, c.e_product_motifname 
						FROM tm_brg_hasil_bisbisan a, tm_stok_hasil_bisbisan b, tr_product_motif c, tm_satuan d
						WHERE a.kode_brg=b.kode_brg_bisbisan AND b.kode_brg_jadi=c.i_product_motif 
						AND a.satuan = d.id AND a.status_aktif='t' AND b.kode_brg_jadi = '$kode_brg_jadi' AND b.stok > 0 ";
		}/*elseif($kel_brg=='2'){
			$sql	= " SELECT a.kode_brg, a.nama_brg, a.id_jenis_bahan, a.id_gudang, b.kode_brg_jadi, b.stok, c.e_product_motifname FROM tm_barang a, tm_stok_hasil_bordir b, tr_product_motif c WHERE a.kode_brg=b.kode_brg AND b.kode_brg_jadi=c.i_product_motif AND a.status_aktif='t' AND b.stok > 0 ";	
		}elseif($kel_brg=='3'){
			$sql	= " SELECT a.kode_brg, a.nama_brg, a.id_jenis_bahan, a.id_gudang, b.kode_brg_jadi, b.stok, c.e_product_motifname FROM tm_barang a, tm_stok_hasil_print b, tr_product_motif c WHERE a.kode_brg=b.kode_brg AND b.kode_brg_jadi=c.i_product_motif AND a.status_aktif='t' AND b.stok > 0 ";
		}*/
		elseif($kel_brg=='4'){
			$sql	= " SELECT b.id, a.kode_brg, a.nama_brg, d.nama AS nama_satuan, a.id_gudang, b.kode_brg_jadi, b.ukuran, b.stok, c.e_product_motifname 
						FROM tm_barang a, tm_stok_hasil_asesoris b, tr_product_motif c, tm_satuan d WHERE a.kode_brg=b.kode_brg 
						AND b.kode_brg_jadi=c.i_product_motif AND a.satuan = d.id AND a.status_aktif='t' 
						AND b.kode_brg_jadi = '$kode_brg_jadi' AND b.stok > 0 ";	
		}elseif($kel_brg=='5'){
			$sql	= " SELECT b.id, a.kode_brg, a.nama_brg, a.id_jenis_bahan, d.nama AS nama_satuan, a.id_gudang, b.stok FROM tm_barang a, 
						tm_stok b, tm_satuan d
						WHERE a.kode_brg=b.kode_brg AND a.satuan = d.id AND a.status_aktif='t' AND b.stok > 0 ";
		}elseif($kel_brg=='6'){
			/*$sql	= " SELECT b.id, a.kode_brg, a.nama_brg, a.id_jenis_bahan, a.id_gudang, b.kode_brg_jadi, b.stok, c.e_product_motifname,
						b.is_dacron, b.diprint
						FROM tm_barang a, tm_stok_hasil_cutting b, tr_product_motif c WHERE a.kode_brg=b.kode_brg 
						AND b.kode_brg_jadi=c.i_product_motif AND a.status_aktif='t' AND b.kode_brg_jadi = '$kode_brg_jadi' 
						AND b.stok > 0 "; */
			$sql = " SELECT b.id, b.kode_brg, b.kode_brg_jadi, b.stok, c.e_product_motifname, 
						a.nama_brg as bhn_baku, d.nama_brg AS bhn_quilting, 
						b.is_dacron, b.diprint
						FROM tm_stok_hasil_cutting b
						LEFT JOIN tm_barang a ON a.kode_brg=b.kode_brg
						LEFT JOIN tm_brg_hasil_makloon d ON d.kode_brg=b.kode_brg
						LEFT JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
						WHERE a.status_aktif='t' AND b.kode_brg_jadi = '$kode_brg_jadi' 
						AND b.stok > 0 ";
		}
		
		if ($cari!="all") {	
			if ($kel_brg == '5')
				$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
			else {
				/*if ($kel_brg == '1')
					$sql.=" AND (UPPER(a.kode_brg) LIKE UPPER('%$cari%') OR UPPER(a.nama_brg) LIKE UPPER('%$cari%') 
						OR UPPER(b.kode_brg) LIKE UPPER('%$cari%') )";
				else */
				$sql.=" AND (UPPER(b.kode_brg) LIKE UPPER('%$cari%') OR UPPER(a.nama_brg) LIKE UPPER('%$cari%') 
						OR UPPER(d.nama_brg) LIKE UPPER('%$cari%')
						OR UPPER(b.kode_brg_jadi) LIKE UPPER('%$cari%') OR UPPER(c.e_product_motifname) LIKE UPPER('%$cari%'))";
			}
				
		}
		
		$query	= $this->db->query($sql);
		return $query->result();
  }
  
  function getbarangjaditanpalimit($kel_brg,$cari){
  	if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(i_product_motif) LIKE UPPER('%$cari%') OR UPPER(i_product) LIKE UPPER('%$cari%') OR UPPER(e_product_motifname) LIKE UPPER('%$cari%') )";
	}
	
	if ($kel_brg == 5)
		$sql = "SELECT * FROM tr_product_motif WHERE n_active='1' ".$pencarian." ORDER BY i_product_motif ASC";
	else if ($kel_brg == 1)
		$sql = " SELECT distinct a.kode_brg_jadi, b.i_product_motif, b.e_product_motifname FROM tm_stok_hasil_bisbisan a, tr_product_motif b 
				WHERE a.kode_brg_jadi = b.i_product_motif ORDER BY a.kode_brg_jadi ASC ";
	else if ($kel_brg == 4)
		$sql = " SELECT distinct a.kode_brg_jadi, b.i_product_motif, b.e_product_motifname FROM tm_stok_hasil_asesoris a, tr_product_motif b 
				WHERE a.kode_brg_jadi = b.i_product_motif ORDER BY a.kode_brg_jadi ASC ";
	else if ($kel_brg == 6)
		$sql = " SELECT distinct a.kode_brg_jadi, b.i_product_motif, b.e_product_motifname FROM tm_stok_hasil_cutting a, tr_product_motif b 
				WHERE a.kode_brg_jadi = b.i_product_motif ORDER BY a.kode_brg_jadi ASC ";
		
  	$query	= $this->db->query($sql);
	return $query->result();
  }
  
  function getbarangjadi($num,$offset,$kel_brg,$cari){
  	if($cari=="all"){
		$pencarian	= "";
	}else{
		$pencarian	= " AND (UPPER(i_product_motif) LIKE UPPER('%$cari%') OR UPPER(i_product) LIKE UPPER('%$cari%') 
						OR UPPER(e_product_motifname) LIKE UPPER('%$cari%') )";
	}
	
	if ($kel_brg == 5)
		$sql = " * FROM tr_product_motif WHERE n_active='1' ".$pencarian." ORDER BY i_product_motif ASC";
	else if ($kel_brg == 1)
		$sql = " distinct a.kode_brg_jadi, b.i_product_motif, b.e_product_motifname FROM tm_stok_hasil_bisbisan a, tr_product_motif b 
				WHERE a.kode_brg_jadi = b.i_product_motif ORDER BY a.kode_brg_jadi ASC ";
	else if ($kel_brg == 4)
		$sql = " distinct a.kode_brg_jadi, b.i_product_motif, b.e_product_motifname FROM tm_stok_hasil_asesoris a, tr_product_motif b 
				WHERE a.kode_brg_jadi = b.i_product_motif ORDER BY a.kode_brg_jadi ASC ";
	else if ($kel_brg == 6)
		$sql = " distinct a.kode_brg_jadi, b.i_product_motif, b.e_product_motifname FROM tm_stok_hasil_cutting a, tr_product_motif b 
				WHERE a.kode_brg_jadi = b.i_product_motif ORDER BY a.kode_brg_jadi ASC ";
	  
	$this->db->select($sql, false)->limit($num,$offset);
    $query = $this->db->get();
    return $query->result(); 
  }
  
  function getsj($nomorsj) {
  	$q	= " SELECT * FROM tm_sj_proses_jahit WHERE no_sj=trim('$nomorsj') ";
  	return $this->db->query($q);
  }
  
  function kode_unit(){
    $this->db->select("* from tm_unit_jahit order by kode_unit ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  	
}
?>
