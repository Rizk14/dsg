<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function getAll($num, $offset, $supplier, $cari) {	  
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_retur_beli ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_retur_beli WHERE id_supplier = '$supplier' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_retur_beli WHERE id_supplier = '$supplier' 
					AND (UPPER(no_dn_retur) like UPPER('%$cari%') OR UPPER(no_dn_retur_manual) like UPPER('%$cari%')) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_retur_beli WHERE (UPPER(no_dn_retur) like UPPER('%$cari%') OR UPPER(no_dn_retur_manual) like UPPER('%$cari%')) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_detail WHERE id_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a
										INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan	= '';
						}
				
						$detail_fb[] = array(	'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_fb = '';
				}
				
				// ambil data detail fakturnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_faktur WHERE id_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) { 
						//ambil tgl faktur
						$query3	= $this->db->query(" SELECT no_faktur, tgl_faktur FROM tm_pembelian_nofaktur
						WHERE id = '$row2->id_pembelian_nofaktur' ");
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$no_faktur	= $hasilrow->no_faktur;
							$tgl_faktur	= $hasilrow->tgl_faktur;
							
							$pisah1 = explode("-", $tgl_faktur);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							if ($bln1 == '01')
								$nama_bln = "Januari";
							else if ($bln1 == '02')
								$nama_bln = "Februari";
							else if ($bln1 == '03')
								$nama_bln = "Maret";
							else if ($bln1 == '04')
								$nama_bln = "April";
							else if ($bln1 == '05')
								$nama_bln = "Mei";
							else if ($bln1 == '06')
								$nama_bln = "Juni";
							else if ($bln1 == '07')
								$nama_bln = "Juli";
							else if ($bln1 == '08')
								$nama_bln = "Agustus";
							else if ($bln1 == '09')
								$nama_bln = "September";
							else if ($bln1 == '10')
								$nama_bln = "Oktober";
							else if ($bln1 == '11')
								$nama_bln = "November";
							else if ($bln1 == '12')
								$nama_bln = "Desember";
							$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						}
						else {
							$tgl_faktur	= '';
						}
				
						$detail_faktur[] = array('id_pembelian_nofaktur'=> $row2->id_pembelian_nofaktur,
												'no_faktur'=> $no_faktur,
												'tgl_faktur'=> $tgl_faktur
											);
					}
				}
				else {
					$detail_faktur = '';
				}
				
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				
				//ambil nomor faktur
			/*	$query3	= $this->db->query(" SELECT no_faktur FROM tm_pembelian_nofaktur WHERE id = '$row1->id_pembelian_nofaktur' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$no_faktur	= $hasilrow->no_faktur;
				 }
				 else
					$no_sj = ''; */
				
				if ($row1->faktur_date_from != '' && $row1->faktur_date_to != '') {
					$pisah1 = explode("-", $row1->faktur_date_from);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					$faktur_date_from = $tgl1."-".$bln1."-".$thn1;
					
					$pisah2 = explode("-", $row1->faktur_date_to);
					$tgl2= $pisah2[2];
					$bln2= $pisah2[1];
					$thn2= $pisah2[0];
					$faktur_date_to = $tgl2."-".$bln2."-".$thn2;
				}
				else {
					$faktur_date_from = '';
					$faktur_date_to = '';
				}
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_dn_retur'=> $row1->no_dn_retur,	
											'no_dn_retur_manual'=> $row1->no_dn_retur_manual,	
											'tgl_retur'=> $row1->tgl_retur,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb,
											'detail_faktur'=> $detail_faktur,
											'faktur_date_from'=> $faktur_date_from,
											'faktur_date_to'=> $faktur_date_to,
											'status_nota'=> $row1->status_nota,
											'jumlah'=> $row1->jumlah	
											);
				//var_dump($row1->jumlah);

				$detail_fb = array();
				$detail_faktur = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			/*$query	= $this->db->query(" SELECT a.* FROM tm_retur_beli a, tm_pembelian_nofaktur b WHERE 
			a.id_pembelian_nofaktur = b.id AND b.id <> '0' "); */
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE id_supplier = '$supplier' ");
	}
	else {
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE id_supplier = '$supplier' 
					AND (UPPER(no_dn_retur) like UPPER('%$cari%') OR UPPER(no_dn_retur_manual) like UPPER('%$cari%')) ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE (UPPER(no_dn_retur) like UPPER('%$cari%') OR UPPER(no_dn_retur_manual) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_dn){
    $this->db->select("id from tm_retur_beli WHERE no_dn_retur = '$no_dn' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_notaretur($no_nota){
    $this->db->select("id from tm_nota_retur_beli WHERE no_nota = '$no_nota' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_dn_retur, $tgl_retur, $id_supplier, $ket, $id_brg, $nama, $qty, $harga, $id_faktur, $no_sj){  
    // 11-08-2015, tgl ga dipake
    //$tgl = date("Y-m-d");
			
			// ambil data terakhir di tabel tm_retur_beli
			$query2	= $this->db->query(" SELECT id FROM tm_retur_beli ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_retur	= $hasilrow->id; //echo $idnya; die();
			
			//save ke tabel retur_detail,
			if ($id_brg!='' && $qty!='') {
				if ($no_sj != '') {
				// hitung jumlah qty dari tabel SJ/tm_pembelian (29 okt 2011, ini udh ga dipake)
				/*	$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
					tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.no_faktur = '$faktur' AND c.id=d.id_pembelian_nofaktur 
					AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier' 
					AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$qty_faktur = $hasilrow->jum;
							
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b,
									tm_retur_beli_faktur c WHERE c.id_retur_beli = b.id 
									AND a.id_retur_beli = b.id AND a.id_retur_beli_faktur = c.id AND c.no_faktur = '$faktur' 
									AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$jum_retur = $hasilrow->jum; // ini sum qty di retur beli berdasarkan kode brg tsb						
					//$qty = $qty-$jum_retur;
					if ($jum_retur == '')
						$jum_retur = 0;
					//echo "qty=".$qty." qty_faktur=".$qty_faktur." jum retur=".$jum_retur; die();
					$jumtot = $jum_retur+$qty;
					if ($jumtot > $qty_faktur)
						$qty = $qty_faktur-$jum_retur; */
				
				// ambil id di tabel tm_retur_beli_faktur
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli_faktur WHERE id_pembelian_nofaktur = '$id_faktur' 
					AND id_retur_beli = '$id_retur' ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_retur_faktur = $hasilrow->id;
				}
				else {
					$id_retur_faktur = 0;
				}
				$is_luar_range = 'f';
				
				if ($id_retur_faktur == 0)
					$is_luar_range = 't';

				// jika semua data tdk kosong, insert ke tm_retur_beli_detail
				$data_detail = array(
					'id_brg'=>$id_brg,
					'qty'=>$qty,
					'harga'=>$harga,
					'id_retur_beli'=>$id_retur,
					'id_retur_beli_faktur'=>$id_retur_faktur,
					'is_luar_range'=>$is_luar_range
				);
				$this->db->insert('tm_retur_beli_detail',$data_detail);
			 
			//insert data brg keluar ke tabel history stok, dan kurangi stok di tabel tm_stok
				//12-01-2013. WARNING! pengurangan stok yg disini ga dipake. skrg udh ada di bon M keluar
				//cek stok terakhir tm_stok, dan update stoknya
			/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = abs($stok_lama-$qty);
				
				//NEW 140611, cek stok terakhir tm_stok_harga, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode' AND harga='$harga' ");
				if ($query3->num_rows() == 0){
					$stok_lama2 = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama2	= $hasilrow->stok;
				}
				$new_stok2 = abs($stok_lama2-$qty);
				// 29 okt 2011, tgl input isi datanya tgl_retur
				$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar_lain, saldo, tgl_input, harga) 
										VALUES ('$kode','$no_dn_retur', '$qty', '$new_stok2', '$tgl_retur', '$harga' ) ");
				
				$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' where kode_brg= '$kode' ");
				
				$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok2', tgl_update_stok = '$tgl' 
									where kode_brg= '$kode' AND harga = '$harga' ");
				*/
			} // end if kode != '' dan $qty != ''
		  
		//}
		//else {
			// ambil data terakhir di tabel tm_retur_beli
		/*	$query2	= $this->db->query(" SELECT id FROM tm_retur_beli ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_retur	= $hasilrow->id; //echo $idnya; die();
			
			//save ke tabel retur_detail,
			if ($kode!='' && $qty!='') {
				// hitung jumlah qty dari tabel SJ/tm_pembelian
					$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
					tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.no_faktur = '$faktur' AND c.id=d.id_pembelian_nofaktur 
					AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier' 
					AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$qty_faktur = $hasilrow->jum;
							
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b,
									tm_retur_beli_faktur c WHERE c.id_retur_beli = b.id 
									AND a.id_retur_beli = b.id AND a.id_retur_beli_faktur = c.id AND c.no_faktur = '$faktur' 
									AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$jum_retur = $hasilrow->jum; // ini sum qty di retur beli berdasarkan kode brg tsb						
					//$qty = $qty-$jum_retur;
					if ($jum_retur == '')
						$jum_retur = 0;
					//echo "qty=".$qty." qty_faktur=".$qty_faktur." jum retur=".$jum_retur; die();
					$jumtot = $jum_retur+$qty;
					if ($jumtot > $qty_faktur)
						$qty = $qty_faktur-$jum_retur;
				
				// ambil id di tabel tm_retur_beli_faktur
					$query2	= $this->db->query(" SELECT id FROM tm_retur_beli_faktur WHERE no_faktur = '$faktur' 
					AND id_retur_beli = '$id_retur' ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_retur_faktur = $hasilrow->id;
				
				// jika semua data tdk kosong, insert ke tm_retur_beli_detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty,
					'id_retur_beli'=>$id_retur,
					'id_retur_beli_faktur'=>$id_retur_faktur
				);
				$this->db->insert('tm_retur_beli_detail',$data_detail);
				
			 
			//insert data brg keluar ke tabel history stok, dan kurangi stok di tabel tm_stok
				
				//cek stok terakhir tm_stok, dan update stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				if ($query3->num_rows() == 0){
					$stok_lama = 0;
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
				}
				$new_stok = abs($stok_lama-$qty);
				$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, keluar, saldo, tgl_input, harga) 
										VALUES ('$kode','$no_sj_keluar', '$qty', '$new_stok', '$tgl', '$harga' ) ");
				
				$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' where kode_brg= '$kode' ");
			} */
		//} // end else
  }
    
  function delete($id){    
	$tgl = date("Y-m-d H:i:s");
	// reset stoknya
			$query3	= $this->db->query(" SELECT no_dn_retur, tgl_retur FROM tm_retur_beli WHERE id = '$id' ");
			$hasilrow = $query3->row();
			$no_dn_retur = $hasilrow->no_dn_retur;
			$tgl_retur = $hasilrow->tgl_retur;
	
				// 12-01-2013, WARNING! update stok bukan disini lagi, tapi di bon M keluar
			/*	$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_detail WHERE id_retur_beli = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$kode_brg = $row2->kode_brg;
						$qty = $row2->qty;
						$harga = $row2->harga;
						
						//1. ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok 
											WHERE kode_brg='$kode_brg' ");
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$stokreset = $stok_lama+$qty;
							
							// new 140611
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga 
											WHERE kode_brg='$kode_brg' AND harga = '$harga' ");
							$hasilrow = $query3->row();
							$stok_lama2	= $hasilrow->stok;
							$stokreset2 = $stok_lama2+$qty;
							
						//2. insert ke tabel tt_stok dgn tipe masuk, utk membatalkan data retur (ga dipake lg)
							//$this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk_lain, saldo, tgl_input, harga)
							//VALUES ('$kode_brg', '$no_dn_retur', '$qty', '$stokreset2', '$tgl_retur', '$harga')  ");
														
						//3. update stok di tm_stok
							$this->db->query(" UPDATE tm_stok SET stok = '$stokreset', tgl_update_stok = '$tgl'
												where kode_brg= '$kode_brg'  ");
							
							$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset2', tgl_update_stok = '$tgl'
												where kode_brg= '$kode_brg' AND harga = '$harga'  ");
					}
				}	*/
	
    $this->db->delete('tm_retur_beli_detail', array('id_retur_beli' => $id));
    $this->db->delete('tm_retur_beli_faktur', array('id_retur_beli' => $id));
    $this->db->delete('tm_retur_beli', array('id' => $id));
  }
  
  //function get_faktur($num, $offset, $csupplier, $cari) {
	function get_faktur($csupplier, $cari, $pkp) {
	if ($cari == "all") {
		/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
		$query = $this->db->get(); */
		
		$sql = " SELECT * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$csupplier' "; 
		/*if ($pkp == 't')
			$sql.= " AND status_faktur_pajak = 't' "; */
		
		$sql.= " ORDER BY tgl_faktur DESC ";
							
		$query	= $this->db->query($sql);

	}
	else {
		/*$this->db->select(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ", false)->limit($num,$offset);
		$query = $this->db->get(); */
		
		$sql = " SELECT * FROM tm_pembelian_nofaktur WHERE kode_supplier = '$csupplier' "; 
		/*if ($pkp == 't')
			$sql.= " AND status_faktur_pajak = 't' "; */
		$sql.= " AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ";
		
		$query	= $this->db->query($sql);
	}
		$data_faktur = array();
		$detail_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail faktur-nya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur_sj WHERE id_pembelian_nofaktur = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$no_sj	= $row2->no_sj;
						
						$sql = "SELECT tgl_sj from tm_pembelian where no_sj = '$no_sj' 
								AND kode_supplier= '$csupplier' AND status_aktif = 't'";
						$query3	= $this->db->query($sql);
						$hasilrow3 = $query3->row();
						$tgl_sj	= $hasilrow3->tgl_sj;
						
					/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b 
									WHERE a.id_retur_beli = b.id AND b.id_pembelian = '$row1->id' 
									AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$jum_op = $hasilrow->jum; // ini sum qty di returnya berdasarkan kode brg tsb
						
						$qty = $qty-$jum_op; */
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1; 
										
						$detail_faktur[] = array('id'=> $row2->id,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj
											);
					}
				}
				else {
					$detail_faktur = '';
				}
				
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $row1->tgl_faktur,
											'tgl_update'=> $row1->tgl_update,
											'kode_supplier'=> $row1->kode_supplier,
											'jumlah'=> $row1->jumlah,
											'detail_faktur'=> $detail_faktur
											);
				$detail_faktur = array();
			} // endforeach header
		}
		else {
			$data_faktur = '';
		}
		return $data_faktur;
  }
  
  function get_fakturtanpalimit($csupplier, $cari){
	if ($cari == "all") { 
		$query	= $this->db->query(" SELECT * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							ORDER BY tgl_faktur DESC ");
	}
	else {
		$query	= $this->db->query(" * FROM tm_pembelian_nofaktur WHERE status_faktur_pajak = 't' AND kode_supplier = '$csupplier' 
							AND UPPER(no_faktur) LIKE UPPER('%$cari%') ORDER BY tgl_faktur DESC ");
	}
    
    return $query->result();  
  }
  
  function get_detail_brg($date_from, $date_to, $id_supplier, $jenis_pembelian){
    $detail_brg = array();
    /*$sql = "SELECT a.kode_brg, a.harga FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
	tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.id = '$id_faktur' AND c.id=d.id_pembelian_nofaktur 
	AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier'";
	die($sql); */
    
    //=============================================
   // $list_faktur = explode(",", $no_faktur); 
  //  foreach($list_faktur as $rowutama) {
	//	$rowutama = trim($rowutama);
	//	if ($rowutama != '') { 
    //=============================================    
			$query	= $this->db->query(" SELECT a.id_brg, e.kode_brg, a.qty, a.harga, c.id as id_faktur, c.no_faktur, c.tgl_faktur, b.no_sj, b.tgl_sj 
			FROM tm_pembelian_detail a INNER JOIN tm_pembelian b ON a.id_pembelian = b.id 
			INNER JOIN tm_pembelian_nofaktur_sj d ON d.id_sj_pembelian = b.id
			INNER JOIN tm_pembelian_nofaktur c ON (c.id=d.id_pembelian_nofaktur AND b.id_supplier = c.id_supplier) 
			INNER JOIN tm_barang e ON e.id = a.id_brg
			WHERE c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
			AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
			AND c.id_supplier = '$id_supplier' AND c.jenis_pembelian = '$jenis_pembelian'
			AND a.status_stok = 't' AND c.status_lunas = 'f' AND b.status_aktif = 't' ORDER BY c.no_faktur, b.no_sj, e.kode_brg ");
						
			$hasil = $query->result();
			foreach($hasil as $row1) {
				
					$query2	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
										INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row1->id_brg' ");
					if ($query2->num_rows() > 0){
						$hasilrow = $query2->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg	= '';
						$satuan	= '';
					}
							
					// hitung jumlah qty dari tabel SJ/tm_pembelian
					// koreksi: 28 okt 2011, sementara ga usah dimunculin. jadi lieur
				/*	$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
					tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id 
					AND c.tgl_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND c.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND c.id=d.id_pembelian_nofaktur 
					AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$kode_supplier' 
					AND a.kode_brg = '$row1->kode_brg' ");
					$hasilrow = $query3->row();
					$qty_faktur = $hasilrow->jum;
							
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_retur_beli_detail a, tm_retur_beli b,
									tm_retur_beli_faktur c, tm_pembelian_nofaktur d 
									WHERE c.id_retur_beli = b.id 
									AND c.no_faktur = d.no_faktur
									AND b.kode_supplier = d.kode_supplier
									AND a.id_retur_beli = b.id AND a.id_retur_beli_faktur = c.id 
									AND d.tgl_faktur >= to_date('$date_from','dd-mm-yyyy')
									AND d.tgl_faktur <= to_date('$date_to','dd-mm-yyyy')
									AND a.kode_brg = '$row1->kode_brg' ");
					$hasilrow = $query3->row();
					$jum_retur = $hasilrow->jum; // ini sum qty di retur beli berdasarkan kode brg tsb						
					//$qty = $qty-$jum_retur;
					if ($jum_retur == '')
						$jum_retur = 0; */
							
					$pisah1 = explode("-", $row1->tgl_faktur);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
					
					$pisah1 = explode("-", $row1->tgl_sj);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					$tgl_sj = $tgl1."-".$bln1."-".$thn1;
					
					$detail_brg[] = array(		'id_brg'=> $row1->id_brg,
												'kode_brg'=> $row1->kode_brg,
												'harga'=> $row1->harga,
												'id_faktur'=> $row1->id_faktur,
												'no_faktur'=> $row1->no_faktur,
												'tgl_faktur'=> $tgl_faktur,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty_faktur'=> $row1->qty,
												//'qty_faktur'=> $qty_faktur,
												//'jum_retur'=> $jum_retur,
												'no_sj'=> $row1->no_sj,
												'tgl_sj'=> $tgl_sj
										);
				
			}
	//	}
//	} //end foreach
	return $detail_brg;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
	//kategori= '1'
    return $query->result();  
  }  
    
  function get_retur($id_retur){
	$query	= $this->db->query(" SELECT * FROM tm_retur_beli WHERE id = '$id_retur' ");    
    $hasil = $query->result();
    
    $data_retur = array();
	$detail_retur = array();
	
	foreach ($hasil as $row1) {
		// ambil data no faktur
	/*	$query1	= $this->db->query(" SELECT * FROM tm_retur_beli_faktur WHERE id_retur_beli = '$row1->id' ");
		if ($query1->num_rows() > 0) {
			$hasil1=$query1->result();
			$list_no_faktur = ''; $k=0;
			$hitung = count($hasil1);
			foreach ($hasil1 as $rownya) {
				$list_no_faktur .= $rownya->no_faktur;
				if ($k<$hitung-1)
					 echo ", ";
				$k++;
			}
		} */
		
				// ambil data detail barangnya, order by no fakturnya (koreksi 4 nov 2011: order by kode_brg)
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_detail WHERE id_retur_beli = '$row1->id'
				ORDER BY id_retur_beli_faktur, id "); 
				/*echo " SELECT * FROM tm_retur_beli_detail WHERE id_retur_beli = '$row1->id'
				ORDER BY id_retur_beli_faktur, id "; die(); */
				if ($query2->num_rows() > 0) {
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
										INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							$nama_brg	= '';
							$satuan	= '';
						}
						
						$query3	= $this->db->query(" SELECT id_pembelian_nofaktur FROM tm_retur_beli_faktur WHERE 
									id = '$row2->id_retur_beli_faktur' AND id_retur_beli = '$row1->id' ");
						/*echo "SELECT id_pembelian_nofaktur FROM tm_retur_beli_faktur WHERE 
									id = '$row2->id_retur_beli_faktur' AND id_retur_beli = '$row1->id'"; die(); */
						if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$id_pembelian_nofaktur	= $hasilrow->id_pembelian_nofaktur;
						}
						else
							$id_pembelian_nofaktur = '0';
						
						if ($id_pembelian_nofaktur != '0') {
							// 24 okt 2011, ambil data tgl faktur
							$query3	= $this->db->query(" SELECT no_faktur, tgl_faktur FROM tm_pembelian_nofaktur WHERE 
										id = '$id_pembelian_nofaktur' ");
							$hasilrow = $query3->row();
							$no_faktur	= $hasilrow->no_faktur;
							$tgl_faktur	= $hasilrow->tgl_faktur;
							
							$pisah1 = explode("-", $tgl_faktur);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$tgl_faktur = $tgl1."-".$bln1."-".$thn1;
						
							// ================================================
							
							// ambil data harga sesuai kode brg dari tabel tm_pembelian_detail, dan juga no sj & tgl_sj
							$query3	= $this->db->query(" SELECT a.qty, a.harga, b.no_sj, b.tgl_sj FROM tm_pembelian_detail a
							INNER JOIN tm_pembelian b ON a.id_pembelian=b.id 
							INNER JOIN tm_pembelian_nofaktur_sj d ON d.id_sj_pembelian = b.id
							INNER JOIN tm_pembelian_nofaktur c ON (c.id=d.id_pembelian_nofaktur AND b.id_supplier = c.id_supplier)
							WHERE c.id = '$id_pembelian_nofaktur'
							AND c.id_supplier = '$row1->id_supplier' AND a.id_brg = '$row2->id_brg' ");
							$hasilrow = $query3->row();
							$qty_faktur = $hasilrow->qty;
							$harganya = $hasilrow->harga;
							$no_sj = $hasilrow->no_sj;
							$tgl_sj = $hasilrow->tgl_sj;
							
							$pisah1 = explode("-", $tgl_sj);
							$tgl1= $pisah1[2];
							$bln1= $pisah1[1];
							$thn1= $pisah1[0];
							$tgl_sj = $tgl1."-".$bln1."-".$thn1;
							//=================================================
												
						// hitung jumlah qty dari tabel SJ/tm_pembelian
						/*$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
						tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id AND c.no_faktur = '$no_faktur' AND c.id=d.id_pembelian_nofaktur 
						AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$row1->kode_supplier' 
						AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$qty_faktur = $hasilrow->jum;  */
						
						// hitung jumlah qty dari tabel SJ/tm_pembelian (revisi 1)
						/*$query3	= $this->db->query(" SELECT SUM(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
						tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id 
						AND c.tgl_faktur >= '$row1->faktur_date_from'
						AND c.tgl_faktur <= '$row1->faktur_date_to'
						AND c.id=d.id_pembelian_nofaktur 
						AND d.no_sj = b.no_sj AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$row1->kode_supplier' 
						AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$qty_faktur = $hasilrow->jum; */
						
						// revisi 2. 26-06-2015 INI PINDAH KEATAS, DIGABUNG DGN NO SJ DAN TGL SJ
					/*	$query3	= $this->db->query(" SELECT a.qty FROM tm_pembelian_detail a, tm_pembelian b, tm_pembelian_nofaktur c, 
						tm_pembelian_nofaktur_sj d WHERE a.id_pembelian = b.id 
						AND c.id=d.id_pembelian_nofaktur AND c.no_faktur = '$no_faktur'
						AND d.id_sj_pembelian = b.id AND b.kode_supplier = c.kode_supplier AND c.kode_supplier = '$row1->kode_supplier' 
						AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$qty_faktur = $hasilrow->qty; */
																			
					} // end if id_pembelian_nofaktur != ''
					else {
						$no_faktur = '';
						$tgl_faktur = '';
						$no_sj = '';
						$tgl_sj = '';
						$qty_faktur = 0;
						
					}
				
						$detail_retur[] = array(		
										'id'=> $row2->id,
										'id_brg'=> $row2->id_brg,
										'kode_brg'=> $kode_brg,
									//	'harga'=> $harganya,
										'harga'=> $row2->harga,
										'is_luar_range'=>$row2->is_luar_range,
										'nama'=> $nama_brg,
										'satuan'=> $satuan,
										'qty_faktur'=> $qty_faktur,
										//'jum_retur'=> $jum_retur,
										'qty'=> $row2->qty,
										'id_pembelian_nofaktur'=> $id_pembelian_nofaktur,
										'no_faktur'=> $no_faktur,
										'tgl_faktur'=> $tgl_faktur,
										'no_sj'=> $no_sj,
										'tgl_sj'=> $tgl_sj
										);
					}
				}
				else {
					$detail_retur = '';
				}
				// ambil data nama supplier
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				
				if ($row1->faktur_date_from != '' && $row1->faktur_date_to != '') {
					$pisah1 = explode("-", $row1->faktur_date_from);
					$tgl1= $pisah1[2];
					$bln1= $pisah1[1];
					$thn1= $pisah1[0];
					$faktur_date_from = $tgl1."-".$bln1."-".$thn1;
					
					$pisah2 = explode("-", $row1->faktur_date_to);
					$tgl2= $pisah2[2];
					$bln2= $pisah2[1];
					$thn2= $pisah2[0];
					$faktur_date_to = $tgl2."-".$bln2."-".$thn2;
				}
				else {
					$faktur_date_from = '';
					$faktur_date_to = '';
				}
				
				$data_retur[] = array(		'id'=> $row1->id,	
											//'list_no_faktur'=> $list_no_faktur,	
											'tgl_retur'=> $row1->tgl_retur,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'no_dn_retur'=> $row1->no_dn_retur,
											'no_dn_retur_manual'=> $row1->no_dn_retur_manual,
											'faktur_date_from'=> $faktur_date_from,
											'faktur_date_to'=> $faktur_date_to,
											'jumlah'=> $row1->jumlah,
											'detail_retur'=> $detail_retur
											);
				$detail_retur = array();
				//$list_no_faktur = '';
	}
	return $data_retur;
  }
  
  function get_sj_retur($jnsaction, $supplier, $no_notanya, $cari, $pkp) {
	  // ambil data pembelian 
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE a.status_nota = 'f' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else { // utk proses edit
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' OR no_faktur <> '' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE a.status_nota = 'f' OR no_nota = '$no_notanya' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE a.status_nota = 'f' AND a.id_supplier = '$supplier' ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE a.status_nota = 'f' AND a.id_supplier = '$supplier'
						AND (a.status_nota = 'f' OR a.no_nota = '$no_notanya') ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql); //
			}
		}
	}
	else {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE a.status_nota = 'f'
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE (a.status_nota = 'f' OR a.no_nota = '$no_notanya' )
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE a.status_nota = 'f' AND a.id_supplier = '$supplier'
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				$sql = " SELECT distinct a.* FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
						INNER JOIN tm_pembelian_nofaktur c ON b.id_pembelian_nofaktur = c.id
						WHERE (a.status_nota = 'f' OR a.no_nota = '$no_notanya' ) 
						AND a.id_supplier = '$supplier'
						AND (UPPER(a.no_nota) like UPPER('%$cari%') OR UPPER(a.no_dn_retur) like UPPER('%$cari%')) ";
				if ($pkp == 't')
					$sql.= " AND c.status_faktur_pajak = 't' ";
				$sql.= " order by a.id_supplier, a.tgl_retur DESC ";
				
				$query	= $this->db->query($sql);
			}
		}
	}
		$data_fb = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$tot_sj = 0;
				// ambil sum total per faktur dari SJ yg dipilih (blm beres 280411)
				
				// 1. query perulangan dari tabel tm_retur_beli_faktur
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_retur_beli_faktur 
											WHERE id_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT SUM(qty*harga) as jumlahnya FROM tm_retur_beli_detail 
						WHERE id_retur_beli = '$row1->id' AND id_retur_beli_faktur = '$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jumlahnya	= $hasilrow->jumlahnya;
							$tot_sj+= $jumlahnya;
						}
						else {
							$jumlahnya	= '0';
						}
					}
				}
				
				$query3	= $this->db->query(" SELECT SUM(qty*harga) as jumlahnya FROM tm_retur_beli_detail 
						WHERE id_retur_beli = '$row1->id' AND id_retur_beli_faktur = '0' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jumlahnya	= $hasilrow->jumlahnya;
					$tot_sj+= $jumlahnya;
				}
				else {
					$jumlahnya	= '0';
				}
				
				$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
				$hasilrow = $query3->row();
				$kode_supplier	= $hasilrow->kode_supplier;
				$nama_supplier	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'no_dn_retur'=> $row1->no_dn_retur,
											'tgl_retur'=> $row1->tgl_retur,
											'tgl_update'=> $row1->tgl_update,
											'tot_sj'=> $tot_sj
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function savenota($no_fp, $tgl_fp, $supplier, $jum, $no_sj){  
    $tgl = date("Y-m-d H:i:s");
	$list_sj = explode(",", $no_sj); 

	$data_header = array(
			  'no_nota'=>$no_fp,
			  'tgl_nota'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'id_supplier'=>$supplier,
			  'jumlah'=>$jum
			);
	$this->db->insert('tm_nota_retur_beli',$data_header);
	
	// ambil data terakhir di tabel tm_nota_retur_beli
	$query2	= $this->db->query(" SELECT id FROM tm_nota_retur_beli ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_nota_retur_beli'=>$id_pf,
			  'no_dn_retur'=>$row1
			);
			$this->db->insert('tm_nota_retur_beli_sj',$data_detail);
			
			// update status_nota di tabel tm_retur_beli
			$this->db->query(" UPDATE tm_retur_beli SET no_nota = '$no_fp', status_nota = 't' 
								WHERE no_dn_retur = '$row1' AND id_supplier = '$supplier' ");
		}
	}
   
  }
  
  function getAllnota($num, $offset, $supplier, $cari) {
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_nota_retur_beli ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_nota_retur_beli
							WHERE id_supplier = '$supplier' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_nota_retur_beli where id_supplier = '$supplier' AND 
							(UPPER(no_nota) like UPPER('%$cari%') ) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_nota_retur_beli where 
							(UPPER(no_nota) like UPPER('%$cari%')) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				//echo $totalnya; die();
				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b INNER JOIN tm_nota_retur_beli a 
				ON b.id = a.id_supplier WHERE a.id = '$row1->id' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				// ambil detail data no & tgl SJ
				$query2	= $this->db->query(" SELECT a.no_dn_retur, b.tgl_retur FROM tm_nota_retur_beli_sj a 
							INNER JOIN tm_retur_beli b ON a.no_dn_retur = b.no_dn_retur WHERE a.id_nota_retur_beli = '$row1->id' ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_dn_retur	= $row2->no_dn_retur;
						$tgl_sj	= $row2->tgl_retur;
						
					/*	$sql = "SELECT tgl_retur from tm_retur_beli where no_dn_retur = '$no_dn_retur' 
								AND kode_supplier= '$kode_supplier'";
						$query3	= $this->db->query($sql);
					//	if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$tgl_sj	= $hasilrow3->tgl_retur; */
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_dn_retur'=> $no_dn_retur,
											'tgl_sj'=> $tgl_sj
											);		
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_nota);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_nota = $tgl1." ".$nama_bln." ".$thn1;
												
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						//$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
						$exptgl1 = explode(" ", $tgl1);
						$tgl1nya= $exptgl1[0];
						$jam1nya= $exptgl1[1];
						$tgl_update = $tgl1nya." ".$nama_bln." ".$thn1." ".$jam1nya;
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,	
											'tgl_nota'=> $tgl_nota,	
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,	
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAllnotatanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli
							WHERE id_supplier = '$supplier' ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where id_supplier = '$supplier' AND 
							(UPPER(no_nota) like UPPER('%$cari%') )  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where (UPPER(no_nota) like UPPER('%$cari%'))  ");
	} //
    
    return $query->result();  
  }
  
  function get_nota($id_nota){
	$query	= $this->db->query(" SELECT * FROM tm_nota_retur_beli where id='$id_nota' ");    
	
	$data_nota = array();
	$detail_sj = array();
	$no_faktur_pajak = '';
	$tgl_faktur_pajak = '';
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT no_dn_retur FROM tm_nota_retur_beli_sj 
									WHERE id_nota_retur_beli = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sj = '';
				foreach ($hasil2 as $row2) {
					$no_sj .= $row2->no_dn_retur.", ";
					
					// 24 mei 2011
					// ambil salah satu no faktur (1 faktur pajak = banyak faktur beli, 
					// berarti faktur2 yg dipilih di retur itu faktur pajaknya sama)
					$query3	= $this->db->query(" SELECT b.id_pembelian_nofaktur, c.no_faktur
							FROM tm_retur_beli a INNER JOIN tm_retur_beli_faktur b ON a.id = b.id_retur_beli
							INNER JOIN tm_pembelian_nofaktur c ON c.id = b.id_pembelian_nofaktur
							WHERE a.no_dn_retur = '$row2->no_dn_retur' LIMIT 1 ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$id_pembelian_nofaktur	= $hasilrow->id_pembelian_nofaktur;
						$no_faktur	= $hasilrow->no_faktur;
					}
					else {
						$id_pembelian_nofaktur = '';
						$no_faktur = '';
					}
					
					// ambil no faktur pajak
					$query3	= $this->db->query(" SELECT a.no_faktur_pajak, a.tgl_faktur_pajak FROM 
								tm_pembelian_pajak a INNER JOIN tm_pembelian_pajak_detail b ON a.id = b.id_pembelian_pajak
								WHERE b.id_faktur_pembelian = '$id_pembelian_nofaktur' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$no_faktur_pajak	= $hasilrow->no_faktur_pajak;
						$tgl_faktur_pajak	= $hasilrow->tgl_faktur_pajak;
					}
					else {
						$no_faktur_pajak = '';
						$tgl_faktur_pajak= '';
					}
										
					$query3	= $this->db->query(" SELECT b.id_brg, b.qty, b.harga
							FROM tm_retur_beli a INNER JOIN tm_retur_beli_detail b ON a.id = b.id_retur_beli
							WHERE a.no_dn_retur = '$row2->no_dn_retur' ");
					if ($query3->num_rows() > 0){
						$hasil3 = $query3->result();
						foreach ($hasil3 as $row3) {
								$query4	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
										INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row3->id_brg' ");
								if ($query4->num_rows() > 0){
									$hasilrow = $query4->row();
									$kode_brg	= $hasilrow->kode_brg;
									$nama_brg	= $hasilrow->nama_brg;
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$kode_brg	= '';
									$nama_brg	= '';
									$satuan	= '';
								}
								
								$detail_brg[] = array(		
											'id_brg'=> $row3->id_brg,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row3->qty,
											'harga'=> $row3->harga );
						}
					}
					else {
							$detail_brg = '';
					}
				}
				
				// ambil data supplier
				$query4	= $this->db->query(" SELECT kode_supplier, nama_npwp, alamat, npwp FROM tm_supplier 
				WHERE id = '$row1->id_supplier' ");
					if ($query4->num_rows() > 0){
						$hasilrow = $query4->row();
						$kode_supplier	= $hasilrow->kode_supplier;
						$nama_npwp	= $hasilrow->nama_npwp;
						$alamat	= $hasilrow->alamat;
						$npwp	= $hasilrow->npwp;
					}
					else {
						$kode_supplier = '';
						$nama_npwp	= '';
						$alamat	= '';
						$npwp	= '';
					}
				
				$pisah1 = explode("-", $row1->tgl_nota);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_nota = $tgl1."-".$bln1."-".$thn1;						
			
				$data_nota[] = array(		'id'=> $row1->id,	
											'no_nota'=> $row1->no_nota,
											'tgl_nota'=> $tgl_nota,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_npwp'=> $nama_npwp,
											'alamat'=> $alamat,
											'npwp'=> $npwp,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_faktur_pajak'=> $no_faktur_pajak,
											'tgl_faktur_pajak'=> $tgl_faktur_pajak,
											'no_sj'=> $no_sj,
											'detail_brg'=> $detail_brg
											);
				$detail_brg = array();
			} // endforeach header
	}
    return $data_nota; 
  }
  
  function deletenotaretur($id){    	
	//semua no_sj di tabel tm_retur_beli yg bersesuaian dgn tm_nota_retur_beli dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	$query	= $this->db->query(" SELECT a.id_supplier, b.no_dn_retur FROM tm_nota_retur_beli a INNER JOIN tm_nota_retur_beli_sj b
							ON a.id = b.id_nota_retur_beli WHERE a.id = '$id' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$this->db->query(" UPDATE tm_retur_beli SET status_nota = 'f', no_nota = '' 
							WHERE id_supplier = '$row1->id_supplier' AND no_dn_retur = '$row1->no_dn_retur' ");
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di nota_retur_beli dan nota_retur_beli_sj
	$this->db->query(" DELETE FROM tm_nota_retur_beli_sj where id_nota_retur_beli = '$id' ");
	$this->db->query(" DELETE FROM tm_nota_retur_beli where id = '$id' ");

  }
  
  function konversi_angka2romawi($bln_now) {
	if ($bln_now == "01")
		$romawi_bln = "I";
	else if ($bln_now == "02")
		$romawi_bln = "II";
	else if ($bln_now == "03")
		$romawi_bln = "III";
	else if ($bln_now == "04")
		$romawi_bln = "IV";
	else if ($bln_now == "05")
		$romawi_bln = "V";
	else if ($bln_now == "06")
		$romawi_bln = "VI";
	else if ($bln_now == "07")
		$romawi_bln = "VII";
	else if ($bln_now == "08")
		$romawi_bln = "VIII";
	else if ($bln_now == "09")
		$romawi_bln = "IX";
	else if ($bln_now == "10")
		$romawi_bln = "X";
	else if ($bln_now == "11")
		$romawi_bln = "XI";
	else if ($bln_now == "12")
		$romawi_bln = "XII";
		
	return $romawi_bln;
  }
  
  function konversi_romawi2angka($blnrom) {
	if ($blnrom == "I")
		$bln_now = "01";
	else if ($blnrom == "II")
		$bln_now = "02";
	else if ($blnrom == "III")
		$bln_now = "03";
	else if ($blnrom == "IV")
		$bln_now = "04";
	else if ($blnrom == "V")
		$bln_now = "05";
	else if ($blnrom == "VI")
		$bln_now = "06";
	else if ($blnrom == "VII")
		$bln_now = "07";
	else if ($blnrom == "VIII")
		$bln_now = "08";
	else if ($blnrom == "IX")
		$bln_now = "09";
	else if ($blnrom == "X")
		$bln_now = "10";
	else if ($blnrom == "XI")
		$bln_now = "11";
	else if ($blnrom == "XII")
		$bln_now = "12";
		
	return $bln_now;
  }
  
  function konversi_bln2deskripsi($bln1) {
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";  
		return $nama_bln;
  }
  
  function get_bahan($num, $offset, $cari, $id_supplier)
  {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
				WHERE a.status_aktif = 't' ORDER BY a.nama_brg ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
				WHERE a.status_aktif = 't' AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
				ORDER BY a.nama_brg ";
		$this->db->select($sql, false)->limit($num,$offset);	
    } 

    $query = $this->db->get();
    return $query->result();
  }
  
  function get_bahantanpalimit($cari, $kode_supplier){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
				WHERE a.status_aktif = 't' ";
		$query	= $this->db->query($sql);
		return $query->result();
	}
	else {
		$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id
				WHERE a.status_aktif = 't' AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";		
		$query	= $this->db->query($sql);
		return $query->result();
	}
  }

}
