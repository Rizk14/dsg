<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
	function get_stok($bulan, $tahun, $kel_brg) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		//$arrDay                =    array("$firstDay","$lastDay"); // return the result in an array format.
		
		// new rekap, dari tabel tm_stok_harga
		if ($kel_brg == "B") {
			$sql = " select distinct a.id, a.kode_brg, b.nama_brg, a.stok, a.harga, a.quilting FROM tm_stok_harga a, tm_barang b, tm_kelompok_barang c, 
					tm_jenis_barang d, tm_jenis_bahan e 
					WHERE a.kode_brg = b.kode_brg
					AND b.status_aktif = 't'
					AND b.id_jenis_bahan = e.id AND e.id_jenis_barang = d.id
					AND d.kode_kel_brg = c.kode
					AND c.kode = '$kel_brg' AND a.stok > 0 
					UNION select distinct a.id, a.kode_brg_quilting, b.nama_brg, a.stok, a.harga, a.quilting FROM tm_stok_harga a, tm_brg_hasil_makloon b
					WHERE a.kode_brg_quilting = b.kode_brg
					AND b.status_aktif = 't'
					AND a.stok > 0
					ORDER BY nama_brg ASC, id ASC ";
		}
		else {
			$sql = " select distinct a.id, a.kode_brg, b.nama_brg, a.stok, a.harga, a.quilting FROM tm_stok_harga a, tm_barang b, tm_kelompok_barang c, 
					tm_jenis_barang d, tm_jenis_bahan e 
					WHERE a.kode_brg = b.kode_brg
					AND b.status_aktif = 't'
					AND b.id_jenis_bahan = e.id AND e.id_jenis_barang = d.id
					AND d.kode_kel_brg = c.kode
					AND c.kode = '$kel_brg' AND a.stok > 0
					ORDER BY b.nama_brg ASC, a.id ASC ";
		}
		$query = $this->db->query($sql);
		
		// alur proses:
	/*	1. query distinct semua barang sesuai range tgl
		2. di perulangan brg, ambil data stok awal (diluar range), jumlah masuk, jum keluar, dan juga stok opnamenya berdasarkan tanggal periode
	*/ 
		
		$data_harga = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row1) {
				if ($row1->quilting == 'f')
					$query3	= $this->db->query(" SELECT a.nama_brg, a.satuan, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				else
					$query3	= $this->db->query(" SELECT a.nama_brg,	a.satuan, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$id_satuan	= $hasilrow->satuan;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$id_satuan = '';
					$satuan = '';
				} //
				
				// cek data supplier berdasarkan kode brg dan harganya, kemudian cek apakah pkp atau ga. kalo pkp, maka dibagi 1.1
				$query3	= $this->db->query(" SELECT a.kode_supplier, b.pkp, b.tipe_pajak FROM tm_harga_brg_supplier a, tm_supplier b 
						WHERE a.kode_supplier = b.kode_supplier AND a.kode_brg = '$row1->kode_brg' AND a.harga = '$row1->harga' LIMIT 1 ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$pkp	= $hasilrow->pkp;
					$tipe_pajak	= $hasilrow->tipe_pajak;
				}
				else {
					$kode_supplier = '';
					$pkp = '';
					$tipe_pajak = '';
				}
				
				$jum_masuk_awal = 0;
				$jum_masuk_awal_lain = 0;
				$jum_masuk = 0;
				$jum_masuk_lain = 0;
				$jum_keluar_awal = 0;
				$jum_keluar_awal_lain = 0;
				$jum_keluar = 0;
				$jum_keluar_lain = 0;
				
				if ($bulan == 1) {
					$bln_query = 12;
					$thn_query = $tahun-1;
				}
				else {
					$bln_query = $bulan-1;
					$thn_query = $tahun;
				}
				
				// 20-02-2012, cek apakah SO bln sebelumnya ada atau ga, kalo ga ada, maka pake perhitungan di tiap2 tabel transaksi
				$queryx	= $this->db->query(" SELECT c.stok FROM tt_stok_opname_bahan_baku a, 
								tt_stok_opname_bahan_baku_detail b, tt_so_bahan_baku_detail_harga c
								WHERE a.id = b.id_stok_opname_bahan_baku AND b.id = c.id_stok_opname_bahan_baku_detail
								AND b.kode_brg = '$row1->kode_brg' AND c.harga = '$row1->harga'
								AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->stok;
					
					$hargakonversi = $row1->harga;
					if ($pkp =='t') {
						$hargakonversi = $hargakonversi/1.1;
						$hargakonversi = round($hargakonversi, 2);
					}
					
					if ($id_satuan == '2' && $row1->quilting == 'f') {
						$hargakonversi = $hargakonversi * 0.91;
						$hargakonversi = round($hargakonversi, 2);
						$satuan = "Meter";
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$hargakonversi = $hargakonversi / 12;
						$hargakonversi = round($hargakonversi, 2);
						$satuan = "Pieces";
					}
					
					$harga_saldo_awal = $saldo_awal * $hargakonversi;
					$harga_saldo_awal = round($harga_saldo_awal, 2);
				}
				// &&&&&&&&&&& end 20-02-2012
				else {
					// 30 mei 2011
					// 1. ambil data saldo awal yg range tanggalnya sebelum tanggal yg dipilih
					if ($row1->quilting == 'f') {
						// 16-02-2012
						//1. ambil query total masuk dari tm_pembelian_detail yg tanggalnya < bulan ini
						/*echo "SELECT sum(b.qty) as jum_masuk FROM tm_pembelian a, tm_pembelian_detail b
							WHERE a.id = b.id_pembelian AND a.tgl_sj < '".$tahun."-".$bulan."-01' AND b.kode_brg = '$row1->kode_brg' 
							AND b.harga = '$row1->harga'"; */
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_pembelian a, tm_pembelian_detail b
							WHERE a.id = b.id_pembelian AND a.tgl_sj < '".$tahun."-".$bulan."-01' AND b.kode_brg = '$row1->kode_brg' 
							AND b.harga = '$row1->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_awal += $hasilrow->jum_masuk;
						}					
					}
					else { // quilting = 't'							
						//1. ambil query total masuk dari tm_sj_hasil_makloon_detail yg tanggalnya < bulan ini
						$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b
							WHERE a.id = b.id_sj_hasil_makloon AND a.tgl_sj < '".$tahun."-".$bulan."-01' AND b.kode_brg_makloon = '$row1->kode_brg' 
							AND b.harga = '$row1->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_awal += $hasilrow->jum_masuk;
						}
						
						//5. ambil query total keluar dari tt_schedule_cutting_detail khusus bhn quilting (17-02-2012)
						$query3	= $this->db->query(" SELECT sum(b.qty_quilting_m) as jum_keluar FROM tt_schedule_cutting a, 
							tt_schedule_cutting_detail b
							WHERE a.id = b.id_schedule_cutting AND a.tgl_cutting < '".$tahun."-".$bulan."-01' 
							AND b.kode_brg_quilting = '$row1->kode_brg'
							AND b.harga_quilting = '$row1->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_awal += $hasilrow->jum_keluar;
						}
					}
					
					// 16-02-2012					
					//2. ambil query total keluar_lain dari nota debet yg tanggalnya < bulan ini
					/*echo "SELECT sum(b.qty) as jum_keluar_lain FROM tm_retur_beli a, tm_retur_beli_detail b
						WHERE a.id = b.id_retur_beli AND a.tgl_retur < '".$tahun."-".$bulan."-01' AND b.kode_brg = '$row1->kode_brg' 
						AND b.harga = '$row1->harga'"; */
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar_lain FROM tm_retur_beli a, tm_retur_beli_detail b
						WHERE a.id = b.id_retur_beli AND a.tgl_retur < '".$tahun."-".$bulan."-01' AND b.kode_brg = '$row1->kode_brg' 
						AND b.harga = '$row1->harga' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_keluar_awal_lain += $hasilrow->jum_keluar_lain;
					}
					
				/*	echo "SELECT sum(b.qty_pjg_kain) as jum_keluar FROM tm_apply_stok_proses_cutting a, 
						tm_apply_stok_proses_cutting_detail b
						WHERE a.id = b.id_apply_stok AND a.tgl_bonm < '".$tahun."-".$bulan."-01' AND b.kode_brg = '$row1->kode_brg' 
						AND b.harga = '$row1->harga' AND b.status_stok = 't'";	*/
					//3. ambil query total keluar dari pemenuhan bhn baku (stok berdasarkan harganya gimana?? 16-02-2012: harga udh ada )
					$query3	= $this->db->query(" SELECT sum(b.qty_pjg_kain) as jum_keluar FROM tm_apply_stok_proses_cutting a, 
						tm_apply_stok_proses_cutting_detail b
						WHERE a.id = b.id_apply_stok AND a.tgl_bonm < '".$tahun."-".$bulan."-01' AND b.kode_brg = '$row1->kode_brg' 
						AND b.harga = '$row1->harga' AND b.status_stok = 't' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_keluar_awal += $hasilrow->jum_keluar;
					}
						
					//4. ambil query total keluar dari sj keluar asesoris (16-02-2012)
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sj_proses_asesoris a, 
						tm_sj_proses_asesoris_detail b
						WHERE a.id = b.id_sj_proses_asesoris AND a.tgl_sj < '".$tahun."-".$bulan."-01' AND b.kode_brg = '$row1->kode_brg' 
						AND b.harga = '$row1->harga' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_keluar_awal += $hasilrow->jum_keluar;
					}
							
					$hargakonversi = $row1->harga;
					if ($pkp =='t') {
						$hargakonversi = $hargakonversi/1.1;
						$hargakonversi = round($hargakonversi, 2);
					}
					
					$jum_masuk_awal = round($jum_masuk_awal, 2);
					$jum_masuk_awal_lain = round($jum_masuk_awal_lain, 2);
					$jum_keluar_awal = round($jum_keluar_awal, 2);
					$jum_keluar_awal_lain = round($jum_keluar_awal_lain, 2);
					
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					if ($id_satuan == '2' && $row1->quilting == 'f') {
						$hargakonversi = $hargakonversi * 0.91;
						$hargakonversi = round($hargakonversi, 2);
						$satuan = "Meter";
						$jum_masuk_awal = $jum_masuk_awal * 0.91;
						$jum_masuk_awal = round($jum_masuk_awal, 2);
						$jum_masuk_awal_lain = $jum_masuk_awal_lain * 0.91;
						$jum_masuk_awal_lain = round($jum_masuk_awal_lain, 2);
						$jum_keluar_awal = $jum_keluar_awal * 0.91;
						$jum_keluar_awal = round($jum_keluar_awal, 2);
						$jum_keluar_awal_lain = $jum_keluar_awal_lain * 0.91;
						$jum_keluar_awal_lain = round($jum_keluar_awal_lain, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$hargakonversi = $hargakonversi / 12;
						$hargakonversi = round($hargakonversi, 2);
						$satuan = "Pieces";
						$jum_masuk_awal = $jum_masuk_awal*12;
						$jum_masuk_awal = round($jum_masuk_awal, 2);
						$jum_masuk_awal_lain = $jum_masuk_awal_lain*12;
						$jum_masuk_awal_lain = round($jum_masuk_awal_lain, 2);
						$jum_keluar_awal = $jum_keluar_awal*12;
						$jum_keluar_awal = round($jum_keluar_awal, 2);
						$jum_keluar_awal_lain = $jum_keluar_awal_lain*12;
						$jum_keluar_awal_lain = round($jum_keluar_awal_lain, 2);
					}
					
					// &&&&&&&&&&&&&&&&&&& end 9 des ^^^^^^^^^^^^^^^^^^^^^^^^^
					// tes
						//if ($row1->kode_brg == 'BK020004020007')
						//	echo $hargakonversi."<br>";
					//
					
					$saldo_awal = $jum_masuk_awal+$jum_masuk_awal_lain - ($jum_keluar_awal+$jum_keluar_awal_lain);
					$saldo_awal = round($saldo_awal, 2);
					//$harga_saldo_awal = $saldo_awal * $row1->harga;
					$harga_saldo_awal = $saldo_awal * $hargakonversi;
					$harga_saldo_awal = round($harga_saldo_awal, 2);
			}	// end if dari SO bln sebelumnya

				// end saldo awal
		// ==========================================================================================================================
		//17-02-2012
		// 2. ambil data masuk dan keluar pada range tanggal yg dipilih
				if ($row1->quilting == 'f') {
					// 16-02-2012
					//1. ambil query total masuk dari tm_pembelian_detail yg tanggalnya berdasarkan range
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_pembelian a, tm_pembelian_detail b
						WHERE a.id = b.id_pembelian AND a.tgl_sj >= '".$tahun."-".$bulan."-01' 
						AND a.tgl_sj <= '".$tahun."-".$bulan."-".$lastDay."'
						AND b.kode_brg = '$row1->kode_brg' 
						AND b.harga = '$row1->harga' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk += $hasilrow->jum_masuk;
					}
				}
				else { // quilting = 't'
					//1. ambil query total masuk dari tm_sj_hasil_makloon_detail yg tanggalnya berdasarkan range
					$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b
						WHERE a.id = b.id_sj_hasil_makloon AND a.tgl_sj >= '".$tahun."-".$bulan."-01' 
						AND a.tgl_sj <= '".$tahun."-".$bulan."-".$lastDay."'
						AND b.kode_brg_makloon = '$row1->kode_brg' 
						AND b.harga = '$row1->harga' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk += $hasilrow->jum_masuk;
					}
					
					//5. ambil query total keluar dari tt_schedule_cutting_detail khusus bhn quilting (17-02-2012)
					$query3	= $this->db->query(" SELECT sum(b.qty_quilting_m) as jum_keluar FROM tt_schedule_cutting a, 
						tt_schedule_cutting_detail b
						WHERE a.id = b.id_schedule_cutting AND a.tgl_cutting >= '".$tahun."-".$bulan."-01' 
						AND a.tgl_cutting <= '".$tahun."-".$bulan."-".$lastDay."'
						AND b.kode_brg_quilting = '$row1->kode_brg'
						AND b.harga_quilting = '$row1->harga' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_keluar += $hasilrow->jum_keluar;
					}
				}
				
				// 16-02-2012					
				//2. ambil query total keluar_lain dari nota debet yg tanggalnya berdasarkan range
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar_lain FROM tm_retur_beli a, tm_retur_beli_detail b
					WHERE a.id = b.id_retur_beli AND a.tgl_retur >= '".$tahun."-".$bulan."-01' 
					AND a.tgl_retur <= '".$tahun."-".$bulan."-".$lastDay."'
					AND b.kode_brg = '$row1->kode_brg' 
					AND b.harga = '$row1->harga' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain += $hasilrow->jum_keluar_lain;
				}
					
				//3. ambil query total keluar dari pemenuhan bhn baku (stok berdasarkan harga )
				$query3	= $this->db->query(" SELECT sum(b.qty_pjg_kain) as jum_keluar FROM tm_apply_stok_proses_cutting a, 
					tm_apply_stok_proses_cutting_detail b
					WHERE a.id = b.id_apply_stok AND a.tgl_bonm >= '".$tahun."-".$bulan."-01' 
					AND a.tgl_bonm <= '".$tahun."-".$bulan."-".$lastDay."'
					AND b.kode_brg = '$row1->kode_brg' 
					AND b.harga = '$row1->harga' AND b.status_stok = 't' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar += $hasilrow->jum_keluar;
				}
					
				//4. ambil query total keluar dari sj keluar asesoris (16-02-2012)
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sj_proses_asesoris a, 
					tm_sj_proses_asesoris_detail b
					WHERE a.id = b.id_sj_proses_asesoris AND a.tgl_sj >= '".$tahun."-".$bulan."-01' 
					AND a.tgl_sj <= '".$tahun."-".$bulan."-".$lastDay."'
					AND b.kode_brg = '$row1->kode_brg' 
					AND b.harga = '$row1->harga' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar += $hasilrow->jum_keluar;
				}
				
				$jum_masuk = round($jum_masuk, 2);
				$jum_masuk_lain = round($jum_masuk_lain, 2);
				$jum_keluar = round($jum_keluar, 2);
				$jum_keluar_lain = round($jum_keluar_lain, 2);
			
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					if ($id_satuan == '2' && $row1->quilting == 'f') {
						$jum_masuk = $jum_masuk * 0.91;
						$jum_masuk = round($jum_masuk, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$jum_masuk = $jum_masuk*12;
						$jum_masuk = round($jum_masuk, 2);
					}
					
					//$harga_jum_masuk = $jum_masuk * $row1->harga;
					$harga_jum_masuk = $jum_masuk * $hargakonversi;
					$harga_jum_masuk = round($harga_jum_masuk, 2);
					//if ($pkp == 't')
					//	$harga_jum_masuk = $harga_jum_masuk / 1.1;
					
					//$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					if ($id_satuan == '2' && $row1->quilting == 'f') {
						$jum_masuk_lain = $jum_masuk_lain * 0.91;
						$jum_masuk_lain = round($jum_masuk_lain, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$jum_masuk_lain = $jum_masuk_lain*12;
						$jum_masuk_lain = round($jum_masuk_lain, 2);
					} 
					
					//$harga_jum_masuk_lain = $jum_masuk_lain * $row1->harga;
					$harga_jum_masuk_lain = $jum_masuk_lain * $hargakonversi;
					$harga_jum_masuk_lain = round($harga_jum_masuk_lain, 2);
					//if ($pkp == 't')
					//	$harga_jum_masuk_lain = $harga_jum_masuk_lain / 1.1;
					
					//$jum_keluar = $hasilrow->jum_keluar;
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					if ($id_satuan == '2' && $row1->quilting == 'f') {
						$jum_keluar = $jum_keluar * 0.91;
						$jum_keluar = round($jum_keluar, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$jum_keluar = $jum_keluar*12;
						$jum_keluar = round($jum_keluar, 2);
					}
					
					//$harga_jum_keluar = $jum_keluar * $row1->harga;
					$harga_jum_keluar = $jum_keluar * $hargakonversi;
					$harga_jum_keluar = round($harga_jum_keluar, 2);
					//if ($pkp == 't')
					//	$harga_jum_keluar = $harga_jum_keluar / 1.1;
					
					//$jum_keluar_lain = $hasilrow->jum_keluar_lain;
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					if ($id_satuan == '2' && $row1->quilting == 'f') {
						$jum_keluar_lain = $jum_keluar_lain * 0.91;
						$jum_keluar_lain = round($jum_keluar_lain, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$jum_keluar_lain = $jum_keluar_lain*12;
						$jum_keluar_lain = round($jum_keluar_lain, 2);
					}
					
					//$harga_jum_keluar_lain = $jum_keluar_lain * $row1->harga;
					$harga_jum_keluar_lain = $jum_keluar_lain * $hargakonversi;
					$harga_jum_keluar_lain = round($harga_jum_keluar_lain, 2);
		// end
		// ==========================================================================================================================				
				$tot_masuk = $jum_masuk+$jum_masuk_lain;
				$tot_masuk = round($tot_masuk, 2);
				$tot_keluar = $jum_keluar+$jum_keluar_lain;
				$tot_keluar = round($tot_keluar, 2);
				
				$saldo_akhir = $saldo_awal+($jum_masuk+$jum_masuk_lain-$jum_keluar-$jum_keluar_lain);
				$saldo_akhir = round($saldo_akhir, 2);
				//$harga_saldo_akhir = $saldo_akhir * $row1->harga;
				$harga_saldo_akhir = $saldo_akhir * $hargakonversi;
				$harga_saldo_akhir = round($harga_saldo_akhir, 2);
									
				// cek JUM STOK OPNAME BERDASARKAN HARGA di tabel tt_so_bahan_baku_detail_harga dan tt_so_hasil_quilting_detail_harga
				if ($row1->quilting == 'f') {
					$sql = "SELECT c.stok FROM tt_stok_opname_bahan_baku a, 
								tt_stok_opname_bahan_baku_detail b, tt_so_bahan_baku_detail_harga c
								WHERE a.id = b.id_stok_opname_bahan_baku AND b.id = c.id_stok_opname_bahan_baku_detail
								AND b.kode_brg = '$row1->kode_brg' AND c.harga = '$row1->harga'
								AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND b.status_approve = 't' ";
				}
				else
					$sql = "SELECT c.stok FROM tt_stok_opname_hasil_quilting a, 
								tt_stok_opname_hasil_quilting_detail b, tt_so_hasil_quilting_detail_harga c
								WHERE a.id = b.id_stok_opname_hasil_quilting AND b.id = c.id_stok_opname_hasil_quilting_detail
								AND b.kode_brg = '$row1->kode_brg' AND c.harga = '$row1->harga'
								AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND b.status_approve = 't' ";
								
				/*	$query3	= $this->db->query(" SELECT b.jum_stok_opname, b.stok_awal FROM tt_stok_opname_hasil_quilting a, 
						tt_stok_opname_hasil_quilting_detail b WHERE a.id = b.id_stok_opname_hasil_quilting
						AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND b.status_approve = 't' 
						 AND b.kode_brg = '$row1->kode_brg' "); */
				
				$query3	= $this->db->query($sql);
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->stok;
					$jum_stok_opname = round($jum_stok_opname, 2);
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					if ($id_satuan == '2' && $row1->quilting == 'f') {
						$jum_stok_opname = $jum_stok_opname * 0.91;
						$jum_stok_opname = round($jum_stok_opname, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$jum_stok_opname = $jum_stok_opname*12;
						$jum_stok_opname = round($jum_stok_opname, 2);
					}

				}
				else {
					$jum_stok_opname = 0;
					$jum_stok_opname = round($jum_stok_opname, 2);
				}
				
				//$harga_jum_stok_opname = $jum_stok_opname * $row1->harga;
				$harga_jum_stok_opname = $jum_stok_opname * $hargakonversi;
				$harga_jum_stok_opname = round($harga_jum_stok_opname, 2);
				
				$selisih = $jum_stok_opname - $saldo_akhir;
				$selisih = round($selisih, 2);
				//$harga_selisih = $selisih * $row1->harga;
				$harga_selisih = $selisih * $hargakonversi;
				$harga_selisih = round($harga_selisih, 2);
				// ##########################
								
				$rekap_stok[] = array(		
											'kode_brg'=> $row1->kode_brg,
											'stok_db'=> $row1->stok,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'harga'=> $hargakonversi,
											'saldo_awal'=> $saldo_awal,
											'harga_saldo_awal'=> $harga_saldo_awal,
											'jum_masuk'=> $jum_masuk,
											'harga_jum_masuk'=> $harga_jum_masuk,
											'jum_masuk_lain'=> $jum_masuk_lain,
											'harga_jum_masuk_lain'=> $harga_jum_masuk_lain,
											'jum_keluar'=> $jum_keluar,
											'harga_jum_keluar'=> $harga_jum_keluar,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'harga_jum_keluar_lain'=> $harga_jum_keluar_lain,
											'tot_masuk'=> $tot_masuk,
											'tot_keluar'=> $tot_keluar,
											'saldo_akhir'=> $saldo_akhir,
											'harga_saldo_akhir'=> $harga_saldo_akhir,
											'jum_stok_opname'=> $jum_stok_opname,
											'harga_jum_stok_opname'=> $harga_jum_stok_opname,
											'selisih'=> $selisih,
											'harga_selisih'=> $harga_selisih,
											'pkp'=> $pkp
											);
			} // endforeach header
		}
		else {
			$rekap_stok = '';
		}
		return $rekap_stok;
  }
  
  function get_stoktanpalimit($bulan, $tahun, $kel_brg){
	// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month  
	  
	if ($kel_brg == "B") {
			$sql = " select distinct a.id, a.kode_brg, b.nama_brg, a.stok, a.harga, a.quilting FROM tm_stok_harga a, tm_barang b, tm_kelompok_barang c, 
					tm_jenis_barang d, tm_jenis_bahan e 
					WHERE a.kode_brg = b.kode_brg
					AND b.status_aktif = 't'
					AND b.id_jenis_bahan = e.id AND e.id_jenis_barang = d.id
					AND d.kode_kel_brg = c.kode
					AND c.kode = '$kel_brg' AND a.stok > 0 
					UNION select distinct a.id, a.kode_brg_quilting, b.nama_brg, a.stok, a.harga, a.quilting FROM tm_stok_harga a, tm_brg_hasil_makloon b
					WHERE a.kode_brg_quilting = b.kode_brg
					AND a.stok>0
					AND b.status_aktif = 't'
					ORDER BY nama_brg ASC, id ASC ";
	}
	else {
		$sql = " select distinct a.id, a.kode_brg, b.nama_brg, a.stok, a.harga, a.quilting FROM tm_stok_harga a, tm_barang b, tm_kelompok_barang c, 
					tm_jenis_barang d, tm_jenis_bahan e 
					WHERE a.kode_brg = b.kode_brg
					AND b.status_aktif = 't'
					AND b.id_jenis_bahan = e.id AND e.id_jenis_barang = d.id
					AND d.kode_kel_brg = c.kode
					AND c.kode = '$kel_brg' AND a.stok > 0
					ORDER BY b.nama_brg ASC, a.id ASC ";
	}
	
	/*$query	= $this->db->query(" SELECT distinct a.kode_brg, b.nama_brg, a.harga FROM tm_stok_harga a, tm_barang b, tm_kelompok_barang c, 
					tm_jenis_barang d, tm_jenis_bahan e 
					WHERE a.kode_brg = b.kode_brg
					AND b.id_jenis_bahan = e.id AND e.id_jenis_barang = d.id
					AND d.kode_kel_brg = c.kode
					AND c.kode = '$kel_brg' AND a.stok <> 0 "); */
	
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function cek_rekap_stok($bulan, $tahun){
    $this->db->select(" * from tm_rekap_stok_bb WHERE bulan = '$bulan' AND tahun= '$tahun' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  /*function get_rekap_stok($bulan, $tahun){ // blm
    $this->db->select(" * from tm_rekap_perubahan_harga WHERE bulan = '$bulan' AND tahun= '$tahun' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		$hasil= $query->result();
		
		foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
						WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg = '';
					$satuan = '';
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier
						WHERE kode_supplier = '$row1->kode_supplier' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_supplier	= $hasilrow->nama;
				}
				else
					$nama_supplier = '';
				
				if ($row1->harga_lama != '' && $row1->harga_baru != '')
					$selisih = $row1->harga_baru - $row1->harga_lama;
				else
					$selisih = '';
								
				$data_harga[] = array(		'id'=> $row1->id,	
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,						
											'tgl_berubah'=> $row1->tgl_berubah,
											'harga_lama'=> $row1->harga_lama,
											'harga_baru'=> $row1->harga_baru,
											'keterangan'=> $row1->keterangan,
											'selisih'=> $selisih
											);
		} // endforeach header
	}
	else {
		$data_harga = '';
	}
	return $data_harga;
  } */
    
  function get_kel_brg(){
	$query	= $this->db->query(" SELECT * FROM tm_kelompok_barang WHERE kode <> 'Z' ORDER BY kode ");    
    return $query->result();  
  }

}
