<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_jns_setor_pajak ORDER BY i_jsetor_pajak DESC ");
	}

	function viewperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_jns_setor_pajak  ORDER BY i_jsetor_pajak DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function listpquery($tabel,$order,$filter) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		if ($query->num_rows() > 0) {
			return $result	= $query->result(); 
		}
	}
		
	function msimpan($kodeakunpajak1,$kodejenissetor1,$uraianpembayaran1,$e_note1) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		/*
		$arrjnssetorpajak	= array( 
			'i_akun_pajak' => $kodeakunpajak1,
			'i_jsetor_pajak' => $kodejenissetor1,
			'e_jsetor_pajak' => $uraianpembayaran1,
			'keterangan' => $e_note1,
			'd_entry' => $dentry);		
		print_r($arrjnssetorpajak);
		*/
			
		$db2->set( array( 
			'i_akun_pajak' =>$kodeakunpajak1,
			'i_jsetor_pajak' =>$kodejenissetor1,
			'e_jsetor_pajak' =>$uraianpembayaran1,
			'keterangan' =>$e_note1,
			'd_entry'=>$dentry
		));

		if($db2->insert('tr_jns_setor_pajak')){
		  redirect('jnssetorpajak/cform/');
		} else {
		  print "<script>alert(\"Maaf, Jenis Setor Pajak gagal disimpan. Jenis Setor Pajak sudah ada!\");show(\"jnssetorpajak/cform\",\"#content\");</script>";	
		}
	}
	
	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_jns_setor_pajak WHERE i_setoran='$id' ORDER BY i_setoran DESC LIMIT 1 " );
	}
	
	function katbarang($icategory) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_class WHERE i_class='$icategory' ORDER BY i_class DESC LIMIT 1 " );
	}
	
	function mupdate($isetoran1,$kodeakunpajak1,$kodejenissetor1,$uraianpembayaran1,$e_note1) {
		$db2=$this->load->database('db_external', TRUE);
		$jnssetorpajak_item	= array(
			'i_akun_pajak'=>$kodeakunpajak1,
			'i_jsetor_pajak'=>$kodejenissetor1,
			'e_jsetor_pajak'=>$uraianpembayaran1,
			'keterangan'=>$e_note1
		);
		$db2->update('tr_jns_setor_pajak',$jnssetorpajak_item,array('i_setoran'=>$isetoran1));
		redirect('jnssetorpajak/cform');
	}
	
	function viewcari($txtcar) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_jns_setor_pajak WHERE i_akun_pajak LIKE '$txtcar%' OR i_jsetor_pajak LIKE '$txtcar%' OR e_jsetor_pajak LIKE '%$txtcar%' ORDER BY i_setoran DESC ");
	}
	
	function mcari($txtcar,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_jns_setor_pajak WHERE i_akun_pajak LIKE '$txtcar%' OR i_jsetor_pajak LIKE '$txtcar%' OR e_jsetor_pajak LIKE '%$txtcar%' ORDER BY i_setoran DESC LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_jns_setor_pajak',array('i_setoran'=>$id));
		redirect('jnssetorpajak/cform/');	 
  }		
}
?>
