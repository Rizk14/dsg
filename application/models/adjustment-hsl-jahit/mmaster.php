<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE jenis='2' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_adj($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, tgl_adj FROM tt_adjustment_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			
			$idnya = $hasilrow->id;
			$tgl_adj = $hasilrow->tgl_adj;
			
		}
		else {
			
			$idnya = '';
			$tgl_adj = '';
			
		}
		
		$so_bahan = array(	   'idnya'=> $idnya,
							   'tgl_adj'=> $tgl_adj
							);
							
		return $so_bahan;
  }
  
  function cek_adj_bulan_sebelumnya($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, tgl_adj FROM tt_adjustment_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
			$tgl_adj = $hasilrow->tgl_adj;
			
		}
		else {
			$idnya = '';
			$tgl_adj = '';
		}
		
		$so_bahan = array(	   'idnya'=> $idnya,
							   'tgl_adj'=> $tgl_adj
							);
							
		return $so_bahan;
  }

  
  function get_all_adjustment($bulan, $tahun, $gudang) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT b.*, d.kode_brg, d.nama_brg,a.id as id_tt_adjustment_hasil_jahit FROM tt_adjustment_hasil_jahit_detail b 
					INNER JOIN tt_adjustment_hasil_jahit a ON b.id_adjustment_hasil_jahit = a.id
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang = '$gudang'
					
					ORDER BY d.kode_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				 
				
				
				$sqlxx = " SELECT a.*, c.nama FROM tt_adjustment_hasil_jahit_detail_warna a 
							INNER JOIN tm_warna c ON c.id = a.id_warna  
							WHERE a.id_adjustment_hasil_jahit_detail = '$row->id'
							ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a INNER JOIN tm_stok_hasil_jahit_warna b ON a.id = b.id_stok_hasil_jahit
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_gudang = '$gudang'
							AND b.id_warna = '$rowxx->id_warna' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						
						
						$detail_warna[] = array(
									'id_warna'=> $rowxx->id_warna,
									'nama_warna'=> $rowxx->nama,
									
									'adjustment'=> $rowxx->jum_adjustment
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array( 
										'id_tt_adjustment_hasil_jahit'=>$row->id_tt_adjustment_hasil_jahit,
										'kode_brg_wip'=> $row->kode_brg,
										'id_brg_wip'=> $row->id_brg_wip,
										'nama_brg_wip'=> $row->nama_brg,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function save($is_new, $id_stok, $is_pertamakali, $id_brg_wip, $id_warna, $stok, $stok_fisik, 
			 $gudang, $hapusitem){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  if ($is_pertamakali != '1') {
		// 06-02-2014
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		//$qtytotalsaldoakhir = 0;
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			
			
		
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
			
		}
		// ---------------------------------------------------------------------
	  
		  if ($is_new == '1') {
			  // 26-01-2016 tambahkan skrip utk hapus ke tm_stok_hasil_jahit jika diceklis
			
				   $data_detail = array(
								'id_adjustment_hasil_jahit'=>$id_stok,
								'id_brg_wip'=>$id_brg_wip, 
								'stok_awal'=>$qtytotalstokawal,
								'jum_adjustment'=>$qtytotalstokfisik

							);
				   $this->db->insert('tt_adjustment_hasil_jahit_detail',$data_detail);
				   
					// ambil id detail id_adjustment_hasil_jahit_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tt_adjustment_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
					
					// ----------------------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok[$xx] = trim($stok[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);
						
						
						$seq_warna	= $this->db->query(" SELECT id FROM tt_adjustment_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}
						
						$tt_adjustment_hasil_jahit_detail_warna	= array(
								 'id'=>$idbaru,
								 'id_adjustment_hasil_jahit_detail'=>$iddetail,
								 'id_warna'=>$id_warna[$xx],
								 'jum_adjustment'=>$stok_fisik[$xx],

							);
							$this->db->insert('tt_adjustment_hasil_jahit_detail_warna',$tt_adjustment_hasil_jahit_detail_warna);
					} // end for
		
		  }
		  else {
							   $data_detail = array(
								'id_adjustment_hasil_jahit'=>$id_stok,
								'id_brg_wip'=>$id_brg_wip, 
								'stok_awal'=>$qtytotalstokawal,
								'jum_adjustment'=>$qtytotalstokfisik

							);
				   $this->db->insert('tt_adjustment_hasil_jahit_detail',$data_detail);
				   
					// ambil id detail id_adjustment_hasil_jahit_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tt_adjustment_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;

			
					
					// ----------------------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok[$xx] = trim($stok[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);
						
						
						$seq_warna	= $this->db->query(" SELECT id FROM tt_adjustment_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}
						
						$tt_adjustment_hasil_jahit_detail_warna	= array(
								 'id'=>$idbaru,
								 'id_adjustment_hasil_jahit_detail'=>$iddetail,
								 'id_warna'=>$id_warna[$xx],
								 'jum_adjustment'=>$stok_fisik[$xx],

							);
							$this->db->insert('tt_adjustment_hasil_jahit_detail_warna',$tt_adjustment_hasil_jahit_detail_warna);
					} // end for
					
					
			 } // end pengecekan
		  
	  }
	  else {
		 
		$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
	
		for ($xx=0; $xx<count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			
		
			
						
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
			
		} // end for
		
	  
		  if ($is_new == '1') {
				$data_detail = array(
								'id_adjustment_hasil_jahit'=>$id_stok,
								'id_brg_wip'=>$id_brg_wip, 
								
								'jum_adjustment'=>$qtytotalstokfisik
								
							);
				$this->db->insert('tt_adjustment_hasil_jahit_detail',$data_detail);
				   
					// ambil id detail id_adjustment_hasil_jahit_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tt_adjustment_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
					
					// ----------------------------------------------
					for ($xx=0; $xx<count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok[$xx] = trim($stok[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);
						

						
						$seq_warna	= $this->db->query(" SELECT id FROM tt_adjustment_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}
						
						$tt_adjustment_hasil_jahit_detail_warna	= array(
								 'id'=>$idbaru,
								 'id_adjustment_hasil_jahit_detail'=>$iddetail,
								 'id_warna'=>$id_warna[$xx],
								 'jum_adjustment'=>$stok_fisik[$xx],
								
							);
							$this->db->insert('tt_adjustment_hasil_jahit_detail_warna',$tt_adjustment_hasil_jahit_detail_warna);
					} // end for
		  }
		  else {
			  // ambil id detail id_adjustment_hasil_jahit_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tt_adjustment_hasil_jahit_detail 
							where id_brg_wip = '$id_brg_wip' 
							AND id_adjustment_hasil_jahit = '$id_stok' ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
			  
			  $this->db->query(" UPDATE tt_adjustment_hasil_jahit_detail SET jum_adjustment = '$qtytotalstokfisik', 
							
								where id = '$iddetail' ");
				
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$this->db->query(" UPDATE tt_adjustment_hasil_jahit_detail_warna SET jum_adjustment = '".$stok_fisik[$xx]."',
								
								WHERE id_adjustment_hasil_jahit_detail='$iddetail' AND id_warna = '".$id_warna[$xx]."' ");
				}
				// ====================
		  }
	  } // END IF
  }
  function get_listbrgtanpalimit($cari) {
	$sql = " * FROM tm_barang_wip WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) ";
			
	$this->db->select($sql, false);
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get_listbrg($num, $offset, $cari)
  {
	  $sql = " * FROM tm_barang_wip WHERE TRUE ";
	  if ($cari != "all")
		$sql.= " AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) ";	  
	  $sql.= " ORDER BY kode_brg ";
	  
	  $this->db->select($sql, false)->limit($num,$offset);
	  
    $query = $this->db->get();
    return $query->result();
  }
 
  }
  
 


