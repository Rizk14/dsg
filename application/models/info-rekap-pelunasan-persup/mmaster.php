<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}

function get_all_pelunasan($jenis_beli, $date_from, $date_to) {
	 
	  $pencarian='';
	  if ($jenis_beli !=0)
	  $pencarian.="AND a.jenis_pembelian=$jenis_beli";
			
	 $sql = " SELECT distinct a.id_supplier, b.kode_supplier, b.nama, b.pkp FROM tm_pembelian a INNER JOIN tm_supplier b ON a.id_supplier = b.id
	 WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
	AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
	AND a.status_aktif = 't' ".$pencarian." ORDER BY nama ASC ";
   
    $query	= $this->db->query($sql);
    $data_lunas = array();
    
    if ($query->num_rows() > 0){
		$hasil = $query->result();
			
			foreach ($hasil as $row1) {
					$query3	= $this->db->query(" SELECT sum(b.subtotal) as jum_total FROM tm_payment_pembelian a 
					INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
					INNER JOIN tm_barang g ON b.id_payment = g.id
					INNER JOIN tm_jenis_barang f ON f.id = g.id_jenis_barang 
					WHERE f.kode_kel_brg = 'B'
					AND b.id_supplier = '$row1->id_supplier'
					AND a.tgl >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl <= to_date('$date_to','dd-mm-yyyy')
					".$pencarian);
					
	if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jum_total_baku	= $hasilrow->jum_total;
				}
	else {
			$jum_total_baku = '';
				}
				
				$query3	= $this->db->query(" SELECT sum(b.subtotal) as jum_total FROM tm_payment_pembelian a 
					INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
					INNER JOIN tm_barang g ON b.id_payment = g.id
					INNER JOIN tm_jenis_barang f ON f.id = g.id_jenis_barang 
					WHERE f.kode_kel_brg = 'P'
					AND b.id_supplier = '$row1->id_supplier'
					AND a.tgl >= to_date('$date_from','dd-mm-yyyy') 
					AND a.tgl <= to_date('$date_to','dd-mm-yyyy')
					".$pencarian);
					
					if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_total_pembantu	= $hasilrow->jum_total;
				}
				else {
					$jum_total_pembantu = '';
				}
				
				if ($row1->pkp == 't') {
					$dpp_baku = $jum_total_baku/1.1;
					$ppn_baku = $jum_total_baku/11;
					$dpp_pembantu = $jum_total_pembantu/1.1;
					$ppn_pembantu = $jum_total_pembantu/11;
				}
				else {
					$dpp_baku = 0;
					$ppn_baku = 0;
					$dpp_pembantu = 0;
					$ppn_pembantu = 0;
				}	
															
				$data_lunas[] = array(		'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $row1->nama,
											'pkp'=> $row1->pkp,
											'jum_total_baku'=> $jum_total_baku,
											'jum_total_pembantu'=> $jum_total_pembantu,
											'dpp_baku'=> $dpp_baku,
											'dpp_pembantu'=> $dpp_pembantu,
											'ppn_baku'=> $ppn_baku,
											'ppn_pembantu'=> $ppn_pembantu
											);
			} // endforeach header
		}
		else {
			$data_lunas = '';
		}
		return $data_lunas;
}
}
?>

