<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	// =========================== 09-06-2015 ==============================
  function get_fakturdo($faktur_from, $faktur_to, $fpengganti, $jenis_faktur) {
	  $db2 = $this->load->database('db_external', TRUE);
	  
	  // ambil nama perusahaan (dlm hal ini duta) dari tr_initial_company
		$sqlxx = " SELECT e_initial_name, e_initial_address, e_initial_npwp, e_initial_phone
					FROM tr_initial_company ";
		$queryxx = $db2->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$e_initial_name	= $hasilxx->e_initial_name;
			$e_initial_address	= $hasilxx->e_initial_address;
			$e_initial_npwp	= $hasilxx->e_initial_npwp;
			$e_initial_phone	= $hasilxx->e_initial_phone;
		}
		else {
			$e_initial_name = '';
			$e_initial_address = '';
			$e_initial_npwp = '';
			$e_initial_phone = '';
		}
		
		if ($jenis_faktur == '1') {
			$nama_tabel = "tm_faktur_do_t";
			$nama_tabel_detail = "tm_faktur_do_item_t";
		}
		else if ($jenis_faktur == '2'){
			$nama_tabel = "tm_faktur";
			$nama_tabel_detail = "tm_faktur_item";
		}
		else if ($jenis_faktur == '3'){
			$nama_tabel = "tm_faktur_bhnbaku";
			$nama_tabel_detail = "tm_faktur_bhnbaku_item";
		}
		
		// 12-06-2015
		$sql1 = " SELECT a.i_faktur, a.i_faktur_code, a.e_branch_name, a.d_faktur, a.v_discount,
					a.v_total_faktur, a.v_total_fppn 

					FROM ".$nama_tabel." a 

					WHERE a.i_faktur_code BETWEEN '$faktur_from' AND '$faktur_to'
					AND a.f_faktur_cancel='f'
					 ";
		$sql1.= " ORDER BY a.d_faktur ASC, a.i_faktur_code ASC "; //echo $sql1."<br>"; die();
		$query1	= $db2->query($sql1);
		
		// c.e_customer_name, c.e_customer_address, c.f_customer_pkp, c.e_customer_npwp, c.e_customer_phone
		// INNER JOIN tr_customer c ON c.i_customer_code = a.e_branch_name
		
		$data_faktur = array();
		$detail_faktur = array();
		//echo $query1->num_rows(); die();
		if ($query1->num_rows() > 0){
			$hasil1 = $query1->result();
			
			$tahunpajak = date("y");
			// 09-04-2013 ------------------
			$query=$db2->query(" SELECT * FROM tr_format_pajak ");
			if ($query->num_rows() > 0) {
				$hasilrow = $query->row();
				$segmen1 = $hasilrow->segmen1;
				$noawal = $hasilrow->nourut_awal;
				$noakhir = $hasilrow->nourut_akhir;
			}
#			$e_customer_npwp = str_replace(' ', '_', $e_customer_npwp);
			$nofakturset = "";		
			foreach ($hasil1 as $row1) {
				// 07-07-2015, AMBIL NAMA CUSTOMER. relasi ke tabel tr_customer dan tr_branch
				$sqlxx = " SELECT c.e_customer_name, c.e_customer_address, c.f_customer_pkp, c.e_customer_npwp, c.e_customer_phone 
							FROM tr_customer c INNER JOIN tr_branch d ON c.i_customer = d.i_customer
							WHERE d.e_initial = '$row1->e_branch_name' ";
				$queryxx=$db2->query($sqlxx);
				if ($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->row();
					$e_customer_name = $hasilxx->e_customer_name;
					$e_customer_address = $hasilxx->e_customer_address;
					$f_customer_pkp = $hasilxx->f_customer_pkp;
    				$hasilxx->e_customer_npwp = str_replace(' ', '', $hasilxx->e_customer_npwp);
					$e_customer_npwp = $hasilxx->e_customer_npwp;
					$e_customer_phone = $hasilxx->e_customer_phone;
											//'e_customer_address'=> $row1->e_customer_address,
											//'f_customer_pkp'=> $row1->f_customer_pkp,
											//'e_customer_npwp'=> $e_customer_npwp,
											//'e_customer_phone'=> $row1->e_customer_phone,
				}
				
				// ambil bulan dan tahun dari tgl faktur utk masa pajak dan tahun pelaporan
				$pisah1 = explode("-", $row1->d_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$d_faktur = $tgl1."/".$bln1."/".$thn1;
				if ($bln1 < 10)
					$bln1new = substr($bln1, 1, 1);
				else
					$bln1new = $bln1;
		
				// 06-07-2015 PROSES PENENTUAN NOMER FAKTUR PAJAK, COPY DARI PRINT FAKTUR PAJAK
					
					// 1. cek no fakturnya di tabel tm_nomor_pajak_faktur, apakah udh ada atau blm. Jika blm ada, maka generate nomor urut baru, kemudian insert baru
					// 2. Ambil data terakhir dari tm_nomor_pajak_faktur, cek nomor faktur pajak terakhir

					$query=$db2->query(" SELECT * FROM tm_nomor_pajak_faktur WHERE i_faktur_code like '%".trim($row1->i_faktur_code)."%' ");
					if ($query->num_rows() == 0) {
						//$status = "Faktur pajak belum pernah dicetak";
						
						// ambil no urut faktur pajak terbaru berdasarkan id
						$query3	= $db2->query(" SELECT * FROM tm_nomor_pajak_faktur ORDER BY id DESC LIMIT 1 ");
						if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$last_id	= $hasilrow3->id;
							$last_nomor_pajak	= $hasilrow3->nomor_pajak;
							$last_nomor_faktur	= $hasilrow3->i_faktur_code;
							//var_dump($last_nomor_pajak);
							//break;
						}
						else {
							$last_id = 0;
							$last_nomor_pajak = '';
						}

						if ($last_nomor_pajak != '') {
							$nourutpajak = substr($last_nomor_pajak, 11, 8);


							// 09-07-2013, besok hari ke-1 puasa. 
							//cek noawal. jika noawal > last nomor pajak, maka ambil acuan tebarunya dari noawal. else, last nomor pajak
							// koreksi 30-06-2014, potong 3 digit awal
							$potong4noawal = substr($noawal, 0, 3);
							$potong4nourutpajak = substr($nourutpajak, 0, 3);
							//var_dump($nourutpajak);
							
							//if ($noawal > $nourutpajak) {
							if ($potong4noawal != $potong4nourutpajak) {
								$nourutpajak = .$nourutpajak+1;
								$new_nourutpajak = $nourutpajak;
							}
							else
								$new_nourutpajak = $nourutpajak+1;
						}
						else {
							$nourutpajak = .$noawal;
							$new_nourutpajak = $nourutpajak;	
						}


						//echo $nourutpajak." ".$noakhir; die();
						
						if ($nourutpajak == $noakhir) {
							//print "<script>alert(\"Nomor urut faktur pajak sudah mencapai jatah maksimum. Nomor faktur penjualan terakhir yang sudah dibikin nomor pajaknya = ".$last_nomor_faktur." . Silahkan hubungi kantor pajak.\");</script>";
							//exit;
							// 07-07-2015 TAMBAHIN SKRIP UTK STOP DISINI, return variabel $data_faktur
							return $data_faktur;
							
						} // end if jika nomor faktur pajaknya habis
						else {
							/*if ($last_nomor_pajak != '') {
								$new_nourutpajak = $nourutpajak+1;
							}
							else
								$new_nourutpajak = $noawal; */
								
							$new_id = $last_id+1;
							
							$exp_segmen1 = explode(".",$segmen1);
							$kodeawal1= $exp_segmen1[0];
							$kodeawal2= $exp_segmen1[1];
							
							if ($fpengganti == 'y') {
								$kodeawal = '011';
							}
							else {
								$kodeawal = '010';
							}
							//$kodeawal = '010';
							$new_segmen1 = $kodeawal.".".$kodeawal2;
							
							$new_nomorpajak = $new_segmen1."-".$tahunpajak.".".$new_nourutpajak;
							//var_dump($new_nourutpajak);



							$str = array(
								'id'=>$new_id,
								'i_faktur_code'=>trim($row1->i_faktur_code),
								'nomor_pajak'=>$new_nomorpajak
							);
							$db2->insert('tm_nomor_pajak_faktur',$str);
						}
						
					}
				
				// ==========================END PENENTUAN NO FAKTUR PAJAK=====================
				
				// ambil no faktur pajak dari tabel nomor_pajak_faktur
				$sql2 = " SELECT nomor_pajak FROM tm_nomor_pajak_faktur WHERE i_faktur_code = '".trim($row1->i_faktur_code)."' ";
				//echo $sql2; die();
				$query2	= $db2->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->row();
					$nomor_pajak	= $hasil2->nomor_pajak;
					
					if ($fpengganti == 'y') {
						$kodeawal = '011';
						$pisahkan = substr($nomor_pajak, 3);
						$nomor_pajak = $kodeawal.$pisahkan;
					}
					
					$nomor_pajak_baru = str_replace(".","",$nomor_pajak);
					$nomor_pajak_baru2 = str_replace("-","",$nomor_pajak_baru);
					
					$nomor_pajak_baru3 = substr($nomor_pajak_baru2, 3);
				}
				else {
					$nomor_pajak_baru3 = '';
				}
				
				// npwp hilangin titiknya
				$e_customer_npwp = str_replace(".","",$e_customer_npwp);
				$e_customer_npwp = str_replace(" ","",$e_customer_npwp);
				//$ppn = $row1->v_total_fppn-$row1->v_total_faktur;
				
				//24-07-2015 diganti
				$ppn = $row1->v_total_faktur/10;
				
				$e_initial_npwp = str_replace(".","",$e_initial_npwp);
				
				// =======================================================
				//tm_faktur_do_item_t ambil jumlah
				$sql12 = " SELECT i_product, e_product_name
						FROM ".$nama_tabel_detail." WHERE i_faktur = '$row1->i_faktur'
						GROUP BY i_product, e_product_name ORDER BY i_product ASC ";
				
				$jumdetail = 0;
				$query12	= $db2->query($sql12);
				if ($query12->num_rows() > 0){
					$hasil12 = $query12->result();
					foreach ($hasil12 as $row12) {
						$jumdetail++;
					}
				} // end detail
				
				//$diskonrata = $row1->v_discount/$jumdetail;
				// =======================================================
				
				//tm_faktur_do_item_t
				$sql12 = " SELECT i_product, e_product_name
						FROM ".$nama_tabel_detail." WHERE i_faktur = '$row1->i_faktur'
						GROUP BY i_product, e_product_name ORDER BY i_product ASC ";
				// , v_unit_price, n_quantity
				
				$totppn = 0; //$jumdetail = 0;
				
				// 01-08-2015
				$diskonnya = $row1->v_discount;
				$keluar = 'f';
				
				$query12	= $db2->query($sql12);
				if ($query12->num_rows() > 0){
					$hasil12 = $query12->result();
					foreach ($hasil12 as $row12) {
						// ambil harga dan qty
						// tm_faktur_do_item_t
						$sqlxx = " SELECT sum(n_quantity) AS qty, 
									v_unit_price AS unitprice,
									sum(v_unit_price) AS jmlunitprice,
									sum((v_unit_price - v_discount) * n_quantity) AS amount,
									sum(v_unit_price  * n_quantity) AS amount2,
									sum(v_discount * n_quantity) AS vdisc


								FROM ".$nama_tabel_detail." 
								WHERE i_faktur='$row1->i_faktur' AND i_product='$row12->i_product'				
								GROUP BY i_product, i_faktur, v_unit_price ";
						$queryxx	= $db2->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$n_quantity	= $hasilxx->qty;
							$v_unit_price	= $hasilxx->unitprice;
							$amount	= $hasilxx->amount;
							$amount2	= $hasilxx->amount2;
							$vdisc = $hasilxx->vdisc;
						}
						else {
							$n_quantity	= 0;
							$v_unit_price	= 0;
							$amount	= 0;
							$amount2	= 0;
							$vdisc=0;
						}
						
						// 01-08-2015, cek diskon
						if (($diskonnya < $amount) && $keluar == 'f') {
							$subtotal = $amount-$diskon;
							$diskon = $row1->v_discount;
							$keluar = 't';
						}
						else {
							$subtotal = $amount;
							$diskon = 0;
						}
						
						//$subtotal = $row12->n_quantity*$row12->v_unit_price;
						//$subtotal = $amount-$diskonrata;
						$ppnitem = $subtotal*0.1;
						$totppn+=$ppnitem;
						//$jumdetail++;
						
						$detail_faktur[] = array(		
											'i_product'=> $row12->i_product,
											'e_product_name'=> $row12->e_product_name,
											'v_unit_price'=> $v_unit_price,
											'n_quantity'=> $n_quantity,
											'subtotal'=> $subtotal,
											'subtotalnondiskon'=> $amount,
											'subtotalnondiskon2'=> $amount2,
											'ppnitem'=> $ppnitem,
											//'diskonrata'=> $diskonrata
											'diskon'=> $diskon,
											'disc'=>$vdisc
											);
					}
				} // end detail
				
				// 25-07-2015 hasil penjumlahan dibulatkan kebawah
				$ppn = floor($totppn);
				
				$data_faktur[] = array(		'i_faktur'=> $row1->i_faktur,
											'i_faktur_code'=> $row1->i_faktur_code,
											'd_faktur'=> $d_faktur,
											'tahun_pajak'=> $thn1,
											'masa_pajak'=> $bln1new,
											'nomor_pajak'=> $nomor_pajak_baru3,
											'v_discount'=> $row1->v_discount,
											'v_total_faktur'=> $row1->v_total_faktur,
											'ppn'=> $ppn,
											'v_total_fppn'=> $row1->v_total_fppn,
											
											/*'e_customer_name'=> $row1->e_customer_name,
											'e_customer_address'=> $row1->e_customer_address,
											'f_customer_pkp'=> $row1->f_customer_pkp,
											'e_customer_npwp'=> $e_customer_npwp,
											'e_customer_phone'=> $row1->e_customer_phone, */
											'e_customer_name'=> $e_customer_name,
											'e_customer_address'=> $e_customer_address,
											'f_customer_pkp'=> $f_customer_pkp,
											'e_customer_npwp'=> $e_customer_npwp,
											'e_customer_phone'=> $e_customer_phone,
																						
											'e_initial_name'=> $e_initial_name,
											'e_initial_address'=> $e_initial_address,
											'e_initial_npwp'=> $e_initial_npwp,
											'e_initial_phone'=> $e_initial_phone,
											
											'detail_faktur'=> $detail_faktur
											);
				$detail_faktur = array();
			} // end foreach header
			
		} // end header
		else {
			$data_faktur = '';
		}
		
	  // ==============================================================================================
	  
	 /* $sql = " SELECT a.i_faktur, a.i_faktur_code, a.e_branch_name, a.d_faktur, a.v_discount,
					a.v_total_faktur, a.v_total_fppn, 
					b.i_product, b.e_product_name, b.v_unit_price, b.n_quantity,
					c.e_customer_name, c.e_customer_address, c.f_customer_pkp, c.e_customer_npwp,
					c.e_customer_phone
					FROM tm_faktur_do_t a 
					INNER JOIN tm_faktur_do_item_t b ON a.i_faktur = b.i_faktur
					INNER JOIN tr_customer c ON c.i_customer_code = a.e_branch_name
					
					WHERE a.d_faktur >= to_date('$date_from','dd-mm-yyyy') 
					AND a.d_faktur <= to_date('$date_to','dd-mm-yyyy')
					AND a.f_faktur_cancel='f'
					 ";
		$sql.= " ORDER BY a.d_faktur ASC, a.i_faktur_code ASC, b.i_product ASC ";
		
		$query	= $db2->query($sql);
				
		$data_faktur = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// 10-06-2015
				// ambil bulan dan tahun dari tgl faktur utk masa pajak dan tahun pelaporan
				$pisah1 = explode("-", $row1->d_faktur);
				$tgl1= $pisah1[2];
				$bln1= $pisah1[1];
				$thn1= $pisah1[0];
				
				$d_faktur = $tgl1."/".$bln1."/".$thn1;
				if ($bln1 < 10)
					$bln1new = substr($bln1, 1, 1);
				else
					$bln1new = $bln1;
		
				// ambil no faktur pajak dari tabel nomor_pajak_faktur
				$sql2 = " SELECT nomor_pajak FROM tm_nomor_pajak_faktur WHERE i_faktur_code = '$row1->i_faktur_code' ";
				$query2	= $db2->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->row();
					$nomor_pajak	= $hasil2->nomor_pajak;
					
					$nomor_pajak_baru = str_replace(".","",$nomor_pajak);
					$nomor_pajak_baru2 = str_replace("-","",$nomor_pajak_baru);
					
					$nomor_pajak_baru3 = substr($nomor_pajak_baru2, 3);
				}
				else {
					$nomor_pajak_baru3 = '';
				}
				
				// npwp hilangin titiknya
				$e_customer_npwp = str_replace(".","",$row1->e_customer_npwp);
				$ppn = $row1->v_total_fppn-$row1->v_total_faktur;
				$e_initial_npwp = str_replace(".","",$e_initial_npwp);
				
				$subtotal = $row1->n_quantity*$row1->v_unit_price;
				$ppnitem = $subtotal*0.1;
								
				$data_beli[] = array(		'i_faktur'=> $row1->i_faktur,
											'i_faktur_code'=> $row1->i_faktur_code,
											'd_faktur'=> $d_faktur,
											'tahun_pajak'=> $thn1,
											'masa_pajak'=> $bln1new,
											'nomor_pajak'=> $nomor_pajak_baru3,
											'v_discount'=> $row1->v_discount,
											'v_total_faktur'=> $row1->v_total_faktur,
											'ppn'=> $ppn,
											'v_total_fppn'=> $row1->v_total_fppn,
											'i_product'=> $row1->i_product,
											'e_product_name'=> $row1->e_product_name,
											'v_unit_price'=> $row1->v_unit_price,
											'n_quantity'=> $row1->n_quantity,
											'subtotal'=> $subtotal,
											'ppnitem'=> $ppnitem,
											
											'e_customer_name'=> $row1->e_customer_name,
											'e_customer_address'=> $row1->e_customer_address,
											'f_customer_pkp'=> $row1->f_customer_pkp,
											'e_customer_npwp'=> $e_customer_npwp,
											'e_customer_phone'=> $row1->e_customer_phone,
																						
											'e_initial_name'=> $e_initial_name,
											'e_initial_address'=> $e_initial_address,
											'e_initial_npwp'=> $e_initial_npwp,
											'e_initial_phone'=> $e_initial_phone
											);
			
			} // endforeach header
		}
		else {
			$data_beli = '';
		} */
		return $data_faktur;
  }
  // =====================================================================
}
?>
