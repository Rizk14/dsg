<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_categories ORDER BY e_category_name ASC, i_category DESC ");
	}

	function viewperpages($limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_categories ORDER BY e_category_name ASC, i_category DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function listpquery($tabel,$order,$filter) {
		 $db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM ".$tabel." ".$filter." ".$order, false);
		if ($query->num_rows() > 0) {
			return $result	= $query->result(); 
		}
	}
		
	function msimpan($i_category,$e_category_name,$i_class) {
		 $db2=$this->load->database('db_external', TRUE);
		// Cek Apakah sudah ada i_category_code atau tidak !! 
		$qry_kategori_code = $db2->query(" SELECT * FROM tr_categories WHERE i_category_code='$i_category' ORDER BY i_category DESC LIMIT 1 ");
		if($qry_kategori_code->num_rows()>0) {
			$ada=1;  	
		} else {
			$ada=0;
		}
		$qry_kategori	= $db2->query(" SELECT cast(i_category AS integer)+1 AS ikategori FROM tr_categories ORDER BY cast(i_category AS integer) DESC LIMIT 1 ");
		
		if($ada==0) {
		  if($qry_kategori->num_rows()>0) {
			$row_kategori	= $qry_kategori->row();
			$ikategori	= $row_kategori->ikategori;
		  } else {
			$ikategori	= 1;
		  }
			
		  $db2->set( array( 
			'i_category' =>$ikategori,
			'e_category_name' =>$e_category_name,
			'i_category_code' =>$i_category,
			'i_class' =>$i_class
		  ));
		
		  $db2->insert('tr_categories');
		  redirect('kategoribrg/cform/');
		} else {
		  print "<script>alert(\"Maaf, Kategori Brg gagal disimpan. Kode Kategori sudah ada!\");show(\"kategoribrg/cform\",\"#content\");</script>";	
		} 
	}
	
	function medit($id) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_categories WHERE i_category='$id' ORDER BY i_category DESC LIMIT 1 " );
	}
	
	function katbarang($icategory) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_class WHERE i_class='$icategory' ORDER BY i_class DESC LIMIT 1 " );
	}
	
	function mupdate($i_category,$e_category_name,$icategorycode,$i_class,$iclass) {
		 $db2=$this->load->database('db_external', TRUE);
		$kategori_item	= array(
			'e_category_name'=>$e_category_name,
			'i_category_code'=>$icategorycode,
			'i_class'=>$i_class
		);
		$db2->update('tr_categories',$kategori_item,array('i_category'=>$i_category));
		redirect('kategoribrg/cform');
	}
	
	function viewcari($txt_i_category,$txt_e_class_name) {
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_i_category!="") {
			return $db2->query(" SELECT * FROM tr_categories WHERE i_category_code='$txt_i_category' OR e_category_name LIKE '%$txt_e_class_name%' ORDER BY e_category_name ASC, i_category DESC ");
		} else {
			return $db2->query(" SELECT * FROM tr_categories WHERE e_category_name LIKE '%$txt_e_class_name%' ORDER BY e_category_name ASC, i_category DESC ");
		
		}
	}
	
	function mcari($txt_i_category,$txt_e_class_name,$limit,$offset) {
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_i_category!="") {
			$filter	= "WHERE i_category_code='$txt_i_category' OR e_category_name LIKE '%$txt_e_class_name%'";
		} else {
			$filter	= "WHERE e_category_name LIKE '%$txt_e_class_name%'";
		}
		$query	= $db2->query(" SELECT * FROM tr_categories ".$filter." ORDER BY e_category_name ASC, i_category DESC LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

  function delete($id) {
	   $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_categories',array('i_category'=>$id));
		redirect('kategoribrg/cform/');	 
  }		
}
?>
