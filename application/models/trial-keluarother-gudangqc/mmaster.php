<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
 function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE a.jenis = '2' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
  
  
  function getAllsjkeluartanpalimit($cari, $date_from, $date_to, $gudang,  $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_sjkeluarother_gudangqc a
				INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip  
				WHERE TRUE ".$pencarian);

    return $query->result();  
  }
  
  
   function getAllsjkeluar($num, $offset, $cari, $date_from, $date_to, $gudang, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($gudang != '0')
			$pencarian.= " AND a.id_gudang = '$gudang' ";
		
		
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$this->db->select(" distinct a.* FROM tm_sjkeluarother_gudangqc a INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip  
				WHERE TRUE ".$pencarian, false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%$caribrg%') OR UPPER(a.nama_brg_wip) like UPPER('%$caribrg%')) ";
					
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_sjkeluarother_gudangqc_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id  
								WHERE a.id_sjkeluarother_gudangqc = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						// 30-01-2014, detail qty warna -------------
						$strwarna = "";
						$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjkeluarother_gudangqc_detail_warna a
									INNER JOIN tm_warna c ON a.id_warna = c.id
									WHERE a.id_sjkeluarother_gudangqc_detail = '$row2->id' ORDER BY c.nama ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						}
						//if ($strwarna == "")
						//	$strwarna = $row2->ket_qty_warna;
						//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				
				
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'no_bp'=> $row1->no_bp,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'keterangan'=> $row1->keterangan,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  function cek_data_sjkeluarwip($no_sj, $thn1){
		
    $this->db->select("id from tm_sjkeluarother_gudangqc WHERE no_sj = '$no_sj' 
						 AND extract(year from tgl_sj) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function savesjkeluar($id_sj, $id_gudang, $id_brg_wip, $nama_brg_wip, $temp_qty, 
						$id_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
	
			// 30-01-2014, insert ke tabel detail ----------------------------------
			//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
													
					$qtytotal+= $qty_warna[$xx];
				} // end for
			// ---------------------------------------------------------------------
			
				// ======== update stoknya! =============
				//$nama_tabel_stok = "tm_stok_hasil_jahit";
				
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE id_brg_wip = '$id_brg_wip'
							AND id_gudang='$id_gudang' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
						$data_stok = array(
							'id_brg_wip'=>$id_brg_wip,
							'id_gudang'=>$id_gudang,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit', $data_stok);
						
						// ambil id_stok utk dipake di stok warna
						$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
						if($sqlxx->num_rows() > 0) {
							$hasilxx	= $sqlxx->row();
							$id_stok	= $hasilxx->id;
						}else{
							$id_stok	= 1;
						}
						
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_gudang='$id_gudang' ");
					}
					
				
				//-------------------------------------------------------------------------------------				
				
				// jika semua data tdk kosong, insert ke tm_sjkeluarother_gudangqc_detail
				$data_detail = array(
					'id_sjkeluarother_gudangqc'=>$id_sj,
					'id_brg_wip'=>$id_brg_wip,
					'nama_brg_wip'=>$nama_brg_wip,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				//	'ket_qty_warna'=>$ket_warna
				);
				$this->db->insert('tm_sjkeluarother_gudangqc_detail',$data_detail);
				
				// ambil id detail sjkeluarother_gudangqc_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_sjkeluarother_gudangqc_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_sjkeluarother_gudangqc_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_sjkeluarother_gudangqc_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_sjkeluarother_gudangqc_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_sjkeluarother_gudangqc_detail_warna',$tm_sjkeluarother_gudangqc_detail_warna);
					
					// ========================= 03-02-2014, stok per warna ===============================================
				
					//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE id_warna = '".$id_warna[$xx]."'
							AND id_stok_hasil_jahit='$id_stok' ");
					if ($query3->num_rows() == 0){
						$stok_warna_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_warna_lama	= $hasilrow->stok;
					}
					$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit_warna, insert
						$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						}
						
						$data_stok = array(
							'id'=>$id_stok_warna,
							'id_stok_hasil_jahit'=>$id_stok,
							'id_warna'=>$id_warna[$xx],
							'stok'=>$new_stok_warna,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
						where id_warna= '".$id_warna[$xx]."' AND id_stok_hasil_jahit='$id_stok' ");
					}
					
					
				} // end for
				// ----------------------------------------------
  }
  function deletesjkeluar($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_gudang,  b.* 
								 FROM tm_sjkeluarother_gudangqc_detail b INNER JOIN tm_sjkeluarother_gudangqc a ON a.id = b.id_sjkeluarother_gudangqc
								 WHERE b.id_sjkeluarother_gudangqc = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok pabrik =====================
						//$nama_tabel_stok = "tm_stok_hasil_jahit";
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit
									 WHERE id_brg_wip = '$row2->id_brg_wip' AND id_gudang='$row2->id_gudang' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
							$id_stok	= $hasilrow->id;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
						
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', 
									tgl_update_stok = '$tgl' WHERE id_brg_wip= '$row2->id_brg_wip'
									AND id_gudang = '$row2->id_gudang' ");
									
						
						
						// ---------------------------------------------------------------------------------------------
						// 2. reset stok per warna dari tabel tm_sjkeluarother_gudangqc_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_sjkeluarother_gudangqc_detail_warna 
												WHERE id_sjkeluarother_gudangqc_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna
											 WHERE id_stok_hasil_jahit = '$id_stok' 
											 AND id_warna='$rowwarna->id_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari SJ keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_jahit= '$id_stok'
											AND id_warna = '$rowwarna->id_warna' ");
								
								
							}
						} // end if detail warna
						
						// --------------------------------------------------------------------------------------------
						// hapus data di tm_sjkeluarother_gudangqc_detail_warna 03-02-2014
						$this->db->query(" DELETE FROM tm_sjkeluarother_gudangqc_detail_warna WHERE id_sjkeluarother_gudangqc_detail='".$row2->id."' ");
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_sjkeluarother_gudangqc_detail dan tm_sjkeluarother_gudangqc
	// 2. hapus data tm_sjkeluarother_gudangqc_detail dan tm_sjkeluarother_gudangqc
    $this->db->delete('tm_sjkeluarother_gudangqc_detail', array('id_sjkeluarother_gudangqc' => $id));
    $this->db->delete('tm_sjkeluarother_gudangqc', array('id' => $id));
  }
  function get_sjkeluar($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sjkeluarother_gudangqc where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarother_gudangqc_detail WHERE id_sjkeluarother_gudangqc = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_brg FROM tm_barang_wip 
											WHERE id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg_wip	= $hasilrow->kode_brg;
						
						$tabelstok = "tm_stok_hasil_jahit";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." 
									 WHERE id_brg_wip = '$row2->id_brg_wip' AND id_gudang='$row1->id_gudang' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_sjkeluarother_gudangqc_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.id_warna, b.nama, c.qty FROM tm_warna b
												INNER JOIN tm_sjkeluarother_gudangqc_detail_warna c ON b.id = c.id_warna
												 WHERE c.id_sjkeluarother_gudangqc_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								//$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$id_warna = $row3->id_warna;
								
								$detail_warna[] = array(	'id_warna'=> $id_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else {
							$detail_warna = '';
						}
						//-------------------------------------------------------------------------------------
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												//'ket_qty_warna'=> $row2->ket_qty_warna,
												'keterangan'=> $row2->keterangan,
												'jum_stok'=> $jum_stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
}
