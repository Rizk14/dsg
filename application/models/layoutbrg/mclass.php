<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view(){
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_layout ORDER BY e_layout_name ASC, i_layout DESC ", false);
	}

	function viewperpages($limit,$offset){
		 $db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_layout ORDER BY e_layout_name ASC, i_layout DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function msimpan($ilayout,$elayoutname){
		 $db2=$this->load->database('db_external', TRUE);
		$db2->set( array(
			'i_layout'=>$ilayout,
			'e_layout_name'=>$elayoutname
		));
		$db2->insert('tr_layout');
		redirect('layoutbrg/cform/');
	}

	function layoutcode(){
		 $db2=$this->load->database('db_external', TRUE);
		$laycode	= $db2->query( " SELECT cast(i_layout AS integer)+1 AS ilayout FROM tr_layout ORDER BY cast(i_layout AS integer) DESC, e_layout_name DESC " );
		return $laycode;
	}
	
	function medit($id){
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_layout WHERE i_layout='$id' ORDER BY i_layout DESC LIMIT 1 " );
	}
	
	function mupdate($ilayout,$e_layout_name){
		 $db2=$this->load->database('db_external', TRUE);
		$layout_item	= array(
			'e_layout_name'=>$e_layout_name
		);
		
		$db2->update('tr_layout',$layout_item,array('i_layout'=>$ilayout));
		redirect('layoutbrg/cform');
	}
	
	function viewcari($txt_i_layout,$txt_e_layout_name){
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_i_layout!="") {
			$filter	= " WHERE i_layout='$txt_i_layout' OR e_layout_name='$txt_e_layout_name' ";
		} else {
			$filter	= " WHERE e_layout_name='$txt_e_layout_name' ";
		}
		return $db2->query(" SELECT * FROM tr_layout ".$filter." ORDER BY e_layout_name ASC ");
	}
	
	function mcari($txt_i_layout,$txt_e_layout_name,$limit,$offset){
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_i_layout!="") {
			$filter	= " WHERE i_layout='$txt_i_layout' OR e_layout_name='$txt_e_layout_name' ";
		} else {
			$filter	= " WHERE e_layout_name='$txt_e_layout_name' ";
		}
		$query	= $db2->query(" SELECT * FROM tr_layout ".$filter." ORDER BY e_layout_name ASC LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

  function delete($id){
	   $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_layout',array('i_layout'=>$id));
		redirect('layoutbrg/cform/');
  }
}

?>
