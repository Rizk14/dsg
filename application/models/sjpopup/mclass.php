<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function get_nomorsj() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_sj WHERE f_sj_cancel='f' ORDER BY cast(i_sj_code AS integer) DESC, d_entry DESC LIMIT 1 " );
	}

	function get_nomor(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sj_code AS character varying),5,(LENGTH(cast(i_sj_code AS character varying))-4)) AS isjcode FROM tm_sj WHERE f_sj_cancel='f' ORDER BY cast(i_sj_code AS integer) DESC LIMIT 1 " );
	}

	function get_thn(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_sj_code AS character varying),1,4) AS thn FROM tm_sj WHERE f_sj_cancel='f' ORDER BY cast(i_sj_code AS integer) DESC LIMIT 1 " );
	}
		
	function msimpan($i_sj,$dsj,$v_sj_total,$e_note,$i_product,$e_product_name,$v_price,$n_count_product,
					$v_sj_gross,$iterasi,$i_customer,$i_branch, $i_outbonm) {
						$db2=$this->load->database('db_external', TRUE);
		$iteration	= $iterasi;
		
		$i_sj_item	= array();
			
		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$seq_tm_sj	= $db2->query(" SELECT cast(i_sj AS integer) AS i_sj FROM tm_sj ORDER BY cast(i_sj AS integer) DESC LIMIT 1 ");
		if($seq_tm_sj->num_rows() > 0 ) {
			$seqrow	= $seq_tm_sj->row();
			$isj	= $seqrow->i_sj+1;
		} else {
			$isj	= 1;
		}
						
		$totalnbarang	= 0;
		for($item=0;$item<=$iteration;$item++) {
			$totalnbarang += $v_sj_gross[$item];
		}
		if($v_sj_total==$totalnbarang) {
			$v_sj_total2 = $v_sj_total;
		} else {
			$v_sj_total2 = $totalnbarang;
		}
		
		$tm_sj	= array(
			 'i_sj'=>$isj,
			 'i_sj_code'=>$i_sj,
			 'i_customer'=>$i_customer,
			 'i_branch'=>$i_branch,
			 'd_sj'=>$dsj,
			 'e_note'=>$e_note,
			 'f_sj_cancel'=>'f',
			 'f_faktur_created'=>'f',
			 'd_entry'=>$dentry
		);

		if(isset($iteration)){
			
			$db2->insert('tm_sj', $tm_sj);
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				$seq_tm_sj_item	= $db2->query(" SELECT cast(i_sj_item AS integer) AS i_sj_item FROM tm_sj_item ORDER BY cast(i_sj_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_sj_item->num_rows() > 0 ) {
					$seqrow	= $seq_tm_sj_item->row();
					$i_sj_item[$jumlah]	= $seqrow->i_sj_item+1;
				} else {
					$i_sj_item[$jumlah]	= 1;				
				}
								
				$isjitem[$jumlah]	=
					array(
					 'i_sj_item'=>$i_sj_item[$jumlah],
					 'i_sj'=>$isj,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'v_product_price'=>$v_price[$jumlah],
					 'n_unit'=>$n_count_product[$jumlah],
					 'n_unit_akhir'=>$n_count_product[$jumlah],
					 'v_unit_price'=>$v_sj_gross[$jumlah],
					 'f_faktur_created'=>'f',
					 'd_entry'=>$dentry,
					 'i_so'=>'0',
					 'i_outbonm_gradea'=>$i_outbonm[$jumlah] );
				
				$db2->insert('tm_sj_item',$isjitem[$jumlah]);
				
				// 02-01-2013, update f_sj_created di tabel tm_outbonm_item
				$query4	= $db2->query(" SELECT i_outbonm_item FROM tm_outbonm_item where i_product = '$i_product[$jumlah]'
						AND i_outbonm='$i_outbonm[$jumlah]' ");
				$hasilrow4 = $query4->row();
				$i_outbonm_item	= $hasilrow4->i_outbonm_item;
				$db2->query(" UPDATE tm_outbonm_item SET f_sj_created = 't' WHERE i_outbonm_item='$i_outbonm_item' ");
				
				// 27-12-2012, update stok di SJ ini ga dipake lagi
			/*	$qstokopname	= $db2->query(" SELECT * FROM tm_stokopname WHERE f_stop_produksi='$stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");				
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();

						$back_qty_akhir	= ($row_stok_item->n_quantity_akhir)-$n_unit[$jumlah];
						
						if($back_qty_akhir=="")
							$back_qty_akhir	= 0;
						
						$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir' WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
						
					}					
				}	*/
			}
			print "<script>alert(\"Nomor SJ : '\"+$i_sj+\"' telah disimpan. Terimakasih.\");show(\"sjpopup/cform\",\"#content\")</script>";
		} else {
			print "<script>alert(\"Maaf, Data SJ gagal disimpan. Terimakasih.\");show(\"sjpopup/cform\",\"#content\");</script>";
		}

	}
	
	function carisj($nsj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_sj WHERE i_sj_code=trim('$nsj') AND f_sj_cancel='f' ");
	}

	function getcabang($icusto) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_customer',$icusto);
		$db2->order_by('e_branch_name');
		return $db2->get('tr_branch');
	}

	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}	
	
	function itemiproduct() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT cast(i_product AS integer)+1 AS i_product FROM tm_sj_item ORDER BY i_sj_item DESC LIMIT 1 ");
	}

	function lbarangjadiperpages($limit,$offset,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		/* 22082011
		$qstr	= " SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price FROM tr_product_motif a INNER JOIN tr_product_base b ON b.i_product_base=a.i_product WHERE a.n_active='1' ORDER BY a.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset." ";
		*/
		
		/* 09092011
		$qstr	= " SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' ORDER BY a.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset." ";
		*/
		$qstr	= " SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' ORDER BY a.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset." ";
					
		$query = $db2->query($qstr);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi($bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		/* 22082011
		return $db2->query( " SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price FROM tr_product_motif a INNER JOIN tr_product_base b ON b.i_product_base=a.i_product WHERE a.n_active='1' ORDER BY a.i_product_motif ASC ");
		*/
		/* 09092011
		return $db2->query(" SELECT a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' ORDER BY a.i_product_motif ASC ");
		*/ 
		return $db2->query(" SELECT a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' ORDER BY a.i_product_motif ASC ");		
	}

	function flbarangjadi($key,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		/* 22082011
		return $db2->query(" SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price FROM tr_product_motif a INNER JOIN tr_product_base b ON b.i_product_base=a.i_product WHERE a.n_active='1' AND a.i_product_motif='$key' ORDER BY a.i_product_motif ASC LIMIT 1000 ");
		*/
		/* 09092011
		return $db2->query(" SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' AND a.i_product_motif='$key' ORDER BY a.i_product_motif ASC "); 
		*/ 
		return $db2->query(" SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' AND a.i_product_motif='$key' ORDER BY a.i_product_motif ASC "); 		
	}

	function lbarangjadiperpagesopsi($limit,$offset,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' ORDER BY a.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset." ";
					
		$query = $db2->query($qstr);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadiopsi($bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' ORDER BY a.i_product_motif ASC ");
	}

	function flbarangjadiopsi($key,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);

		return $db2->query(" SELECT  a.i_product_motif AS iproduct, a.e_product_motifname AS productname, b.v_unitprice AS price, d.f_stop_produksi AS stp, d.i_so AS iso FROM tr_product_motif a 

			INNER JOIN tr_product_base b ON b.i_product_base=a.i_product
			INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product_motif
			INNER JOIN tm_stokopname d ON d.i_so=c.i_so

			WHERE a.n_active='1' AND d.i_status_so='0' AND a.i_product_motif='$key' ORDER BY a.i_product_motif ASC "); 
	}
	
	// 28-12-2012
	function lcabang() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$db2->select(" a.i_branch_code AS codebranch, a.e_branch_name AS branch, a.e_initial AS einitial FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
				    
		$query	= $db2->get();
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}else{
			return false;
		}
	}	
	
	function lbonmdetail() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query("
			SELECT	a.i_outbonm, a.i_outbonm_code, a.d_outbonm,
				cast(trim(b.i_product) AS character varying) AS iproduct,
				b.e_product_name AS productname,
				b.n_count_product AS qtynya
				
				FROM tm_outbonm a
				
				INNER JOIN tm_outbonm_item b ON a.i_outbonm=b.i_outbonm

				WHERE a.e_to_outbonm in ('Penjualan Grade A', 'Lain-lain') AND a.f_outbonm_cancel='f'
				AND b.f_sj_created = 'f'
							
				ORDER BY a.i_outbonm_code ASC ");
		
		if($query->num_rows()>0) {
			return $query->result();
		}		
	}
	
	function detailsimpan($i_outbonm,$iproduct) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_outbonm, a.i_outbonm_code,
				cast(trim(b.i_product) AS character varying) AS iproduct,
				d.e_product_motifname AS productname,
				b.n_count_product,
				e.f_stop_produksi AS stp
				
				FROM tm_outbonm a
				
				INNER JOIN tm_outbonm_item b ON a.i_outbonm=b.i_outbonm
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				WHERE a.i_outbonm='$i_outbonm' AND b.i_product='$iproduct' AND a.f_outbonm_cancel='f' 
							
				ORDER BY a.i_outbonm_code ASC, b.i_product ASC ");
	}
	
	function hargaperpelanggan($imotif,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}
}
?>
