<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_status_bbk ORDER BY i_status_bbk DESC", false);
	}

	function viewperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_status_bbk ORDER BY i_status_bbk DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($istatusbbk,$estatusname){
		$db2=$this->load->database('db_external', TRUE);
		$str = array(
			'i_status_bbk'=>$istatusbbk,
			'e_statusname'=>$estatusname
		);
		
		$db2->insert('tr_status_bbk',$str);
		
		redirect('trans_mst_bbk/cform/');
    }
	
	function mupdate($istatusbbk,$estatusname){
		$db2=$this->load->database('db_external', TRUE);
		$class_item	= array(
			'e_statusname'=>$estatusname
		);
		
		$db2->update('tr_status_bbk',$class_item,array('i_status_bbk'=>$istatusbbk));
		redirect('trans_mst_bbk/cform');
	}

	function code(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT cast(i_status_bbk AS integer)+1 AS code FROM tr_status_bbk ORDER BY i_status_bbk DESC LIMIT 1");
	}

	function medit($id){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_status_bbk WHERE i_status_bbk='$id' ");
	}
	
	function viewcari($txt_i_status_bbk,$txt_e_statusname){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_status_bbk WHERE i_status_bbk='$txt_i_status_bbk' OR e_statusname LIKE '%$txt_e_statusname%' ");
	}
	
	function mcari($txt_i_status_bbk,$txt_e_statusname,$limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_status_bbk WHERE i_status_bbk='$txt_i_status_bbk' OR e_statusname LIKE '%$txt_e_statusname%' LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

  function delete($id){
	  $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_status_bbk',array('i_status_bbk'=>$id));
		redirect('trans_mst_bbk/cform/');
  }
}
?>
