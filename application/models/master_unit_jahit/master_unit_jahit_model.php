<?php
class Master_unit_jahit_model extends MY_Model
{
public function __construct()
	{
			parent::__construct();
			 $this->db4 = $this->load->database('db_additional',TRUE);
	}
	protected $_per_page = 10;
    protected $_tabel = 'tb_master_unit_jahit';
    protected $form_rules = array(
        array(
            'field' => 'nama_unit_jahit',
            'label' => 'Nama Unit Jahit',
            'rules' => 'trim|required|max_length[64]'
        ),
         array(
            'field' => 'lokasi',
            'label' => 'Lokasi',
            'rules' => 'trim|required|max_length[16]'
        ),
         array(
            'field' => 'nama_perusahaan',
            'label' => 'Nama Perusahaan',
            'rules' => 'trim|required|max_length[16]'
        ),
          array(
            'field' => 'nama_penanggung_jawab',
            'label' => 'Nama Penanggung Jawab',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'nama_administrator',
            'label' => 'Nama Administrator',
            'rules' => 'trim|required|max_length[16]'
        ),
        
        
    );

     public $default_values = array(
		'id'	=>	'',
		'num'		=>1,
		'nama_unit_jahit' => '',
        'lokasi' => '',
		'nama_perusahaan' => '',
        'nama_penanggung_jawab' => '',
		'nama_administrator'=>'',
    );
    
        public function input($nama_unit_jahit,$lokasi,$nama_perusahaan,$nama_penanggung_jawab,$nama_administrator)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 
		$data= array(
		'nama_unit_jahit' => $nama_unit_jahit,
		'lokasi' => $lokasi,
		'nama_perusahaan' => $nama_perusahaan,
		'nama_penanggung_jawab' => $nama_penanggung_jawab,
		'nama_administrator' => $nama_administrator,
		
		'created_at'=> $created_at,
		'updated_at'=> $updated_at
		);
		
        $id= $this->db4->insert('tb_master_unit_jahit',$data);
	
        if ($id) {
		 return true;
		}
		 return false;

    }
     public function cari($offset)
    {
        $this->get_real_offset($offset);
        $kata_kunci = $this->input->get('kata_kunci', true);
       
        return $this->db4->where("( nama_unit_jahit LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->result();
    }
    
     public function cari_num_rows()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
          return $this->db4->where("( nama_unit_jahit LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->num_rows();
    }
    
    public function edit($id, $nama_unit_jahit,$lokasi,$nama_perusahaan,$nama_penanggung_jawab,$nama_administrator)
    {
     $updated_at = date('Y-m-d H:i:s');
     $this->db4->where('id', $id);
     $data= array(
		'nama_unit_jahit' => $nama_unit_jahit,
		'lokasi' => $lokasi,
		'nama_perusahaan' => $nama_perusahaan,
		'nama_penanggung_jawab' => $nama_penanggung_jawab,
		'nama_administrator' => $nama_administrator,
		
		'updated_at'=> $updated_at,
		);
    $this->db4->update('tb_master_unit_jahit', $data);
    return true;
    }
    
}
