<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_source_address ORDER BY iip ASC ", false);
		
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function CekIP($destination_ip) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_source_address WHERE e_ip='$destination_ip' ");
	}
	
	function msimpan($e_ip_name,$e_note,$cstatus) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 
							
		$tr_ip	= array(
			'e_ip'=>$e_ip_name,
			'e_note'=>$e_note,
			'f_active'=>$cstatus,
			'd_entry'=>$dentry
			);

		$db2->insert('tr_source_address',$tr_ip);
		
		redirect('ipaddress/cform/');
    }

  
  function get($ip) {
	  $db2=$this->load->database('db_external', TRUE);
    $query = $db2->get_where('tr_source_address',array('iip'=>$ip));
    return $query->row_array();		
  }
  
  function update($e_ip_name,$e_note,$cstatus,$ip) {
	  $db2=$this->load->database('db_external', TRUE);
  	$printer	= array(
		'e_ip'=>$e_ip_name,
		'e_note	'=>$e_note,
		'f_active'=>$cstatus
	);
	$db2->update('tr_source_address',$printer,array('iip'=>$ip));
	redirect('ipaddress/cform');
  }
  
  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
  	$db2->delete('tr_source_address',array('iip'=>$id));
	redirect('ipaddress/cform');
  }
}
?>
