<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $unit_makloon, $cari) {	  
	if ($cari == "all") {
		if ($unit_makloon == '0') {
			$this->db->select(" * FROM tm_sj_proses_quilting order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_proses_quilting WHERE kode_unit = '$unit_makloon' order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($unit_makloon != '0') {
			$this->db->select(" * FROM tm_sj_proses_quilting WHERE kode_unit = '$unit_makloon' 
			AND UPPER(no_sj) like UPPER('%$cari%') order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_proses_quilting WHERE UPPER(no_sj) like UPPER('%$cari%') order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting_detail 
											WHERE id_sj_proses_quilting = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
	//				$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						//----------------
						// bhn baku
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
						
						// bhn makloon
					/*	$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
										tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg_makloon' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_makloon	= $hasilrow->nama_brg;
							$satuan_makloon	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg_makloon	= '';
							$satuan_makloon	= '';
						}
						
						// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						} */
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
											'nama'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row2->qty,
											'qty_meter'=> $row2->qty_meter,
											'detail_pjg'=> $row2->detail_pjg_kain
									);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
				
				// bhn makloon
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
								tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg_makloon' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_makloon	= $hasilrow->nama_brg;
					$satuan_makloon	= $hasilrow->nama_satuan;
				}
				else {
					$nama_brg_makloon	= '';
					$satuan_makloon	= '';
				}
						
				// brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$row1->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
				}
				else {
					$nama_brg_jadi	= '';
				}
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'detail_fb'=> $detail_fb,
											//'id_detailnya'=> $id_detailnya
											'status_edit'=> $row1->status_edit,
											'makloon_internal'=> $row1->makloon_internal,
											'kode_brg_makloon'=> $row1->kode_brg_makloon,
											'nama_brg_makloon'=> $nama_brg_makloon,
											'satuan_makloon'=> $satuan_makloon,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi
											);
				$detail_fb = array();
			//	$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		
		return $data_fb;
  }
  
  function getAlltanpalimit($unit_makloon, $cari){
	if ($cari == "all") {
		if ($unit_makloon == '0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting WHERE kode_unit = '$unit_makloon' ");
	}
	else {
		if ($unit_makloon != '0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting WHERE kode_unit = '$unit_makloon' 
			AND UPPER(no_sj) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting WHERE UPPER(no_sj) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_sj, $unit_makloon){
    $this->db->select("id from tm_sj_proses_quilting WHERE no_sj = '$no_sj' AND kode_unit = '$unit_makloon' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_sj,$tgl_sj, $unit_makloon, $jenis_makloon, $makloon_internal, $kode_brg_jadi, $kode_brg_makloon, $ket, 
			$kode, $nama, $qty, $qtym, $detail_pjg){  
    
    $tgl = date("Y-m-d"); 
    // cek apa udah ada datanya blm dgn no sj tadi
    $this->db->select("id from tm_sj_proses_quilting WHERE no_sj = '$no_sj' AND kode_unit = '$unit_makloon' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_sj_proses_quilting
			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_makloon,
			  'keterangan'=>$ket,
			  'id_jenis_makloon'=>$jenis_makloon,
			  'makloon_internal'=>$makloon_internal,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			$this->db->insert('tm_sj_proses_quilting',$data_header);
			
			// ambil data terakhir di tabel tm_sj_proses_quilting
			$query2	= $this->db->query(" SELECT id FROM tm_sj_proses_quilting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				//cek qty, jika lebih besar dari stok maka otomatis disamakan dgn sisa stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$stok = $hasilrow->stok; // ini stok terkini di tm_stok
								
					// cek dulu satuan brgnya. jika satuannya yard, maka qty (m) yg tadi diambil itu konversikan lagi ke yard
				/*	$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$kode' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$satuan	= '';
						}
						// 12 agust 2011: skrg sudah ada field qtym, jadi qty tidak perlu dikonversi lagi
						if ($satuan == "Yard") {
							$qty_sat_awal = $qty / 0.91;
						}
						else
							$qty_sat_awal = $qty;
				
				if ($qty_sat_awal > $stok) {
					$qty_sat_awal = $stok;
					$qty = $qty_sat_awal * 0.91;
				}
				if ($stok == '') {
					$qty_sat_awal = '0';
					$qty = '0';
				} */
				
				if ($qty > $stok) {
					$qty = $stok;
				}
				
				// jika semua data tdk kosong, insert ke tabel detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty, // ini satuan awal (yard)
					'qty_meter'=>$qtym, // ini satuannya meter (m)
					'detail_pjg_kain'=>$detail_pjg,
					'id_sj_proses_quilting'=>$id_sj,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'kode_brg_makloon'=>$kode_brg_makloon
				);
				$this->db->insert('tm_sj_proses_quilting_detail',$data_detail);
			
			// ============ update stok =====================
			//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
						
					$new_stok = $stok_lama-$qty; // berkurang stok karena SJ keluar utk makloon
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
						//	'id_gudang'=>$id_gudang, //
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' ");
					}
					
					// ############################ new 140611, tm_stok_harga. cek stok di tiap2 harga, ambil stok yg msh ada
					// diurutkan dari harga lama
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg = '$kode' ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; // data ke-1: 500		data ke-2: 10
							$harganya = $row2->harga; // data ke-1: 20000	data ke-2: 370000
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) // temp_selisih = -6
									$selisih = $stoknya-$qty; // selisih1 = 500-506 = -6
								else
									$selisih = $stoknya+$temp_selisih; // selisih2 = 10-6 = 4
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; // temp_selisih = -6
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode' AND harga = '$harganya' "); //
									
									$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, keluar, saldo, tgl_input, harga) 
											VALUES ('$kode','$no_sj', '$unit_makloon', '$stoknya', '0', '$tgl', '$harganya') ");
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $qty; // temp_selisih = -6
										
									break;
								}
							}
						} // end for
					}
					// kurangi stoknya dari tm_stok
				/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode' AND harga = '$harganya' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
						
					$new_stok = $stok_lama-$qty_sat_awal; // berkurang stok karena SJ keluar utk makloon
				*/
				if ($selisih != 0) {
					$new_stok = $selisih; // new_stok = 4
					$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' AND harga = '$harganya' ");
					
					// #############################
					
					$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, keluar, saldo, tgl_input, harga) 
											VALUES ('$kode','$no_sj', '$unit_makloon', '".abs($temp_selisih)."', '$new_stok', '$tgl', '$harganya') ");
				}
					 // temp_selisih = -6
			// ==============================================
			} // end if kode
		}
		else {
			// ambil data terakhir di tabel tm_sj_proses_quilting
			$query2	= $this->db->query(" SELECT id FROM tm_sj_proses_quilting ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				//cek qty, jika lebih besar dari stok maka otomatis disamakan dgn sisa stoknya
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$stok = $hasilrow->stok; // ini stok terkini di tm_stok
								
					// cek dulu satuan brgnya. jika satuannya yard, maka qty (m) yg tadi diambil itu konversikan lagi ke yard
				/*	$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$kode' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$satuan	= '';
						}
						// 12 agust 2011: skrg sudah ada field qtym, jadi qty tidak perlu dikonversi lagi
						if ($satuan == "Yard") {
							$qty_sat_awal = $qty / 0.91;
						}
						else
							$qty_sat_awal = $qty;
				
				if ($qty_sat_awal > $stok) {
					$qty_sat_awal = $stok;
					$qty = $qty_sat_awal * 0.91;
				}
				if ($stok == '') {
					$qty_sat_awal = '0';
					$qty = '0';
				} */
				
				if ($qty > $stok) {
					$qty = $stok;
				}
				
				// jika semua data tdk kosong, insert ke tabel detail
				$data_detail = array(
					'kode_brg'=>$kode,
					'qty'=>$qty, // ini satuan awal (yard)
					'qty_meter'=>$qtym, // ini satuannya meter (m)
					'detail_pjg_kain'=>$detail_pjg,
					'id_sj_proses_quilting'=>$id_sj,
					'kode_brg_jadi'=>$kode_brg_jadi,
					'kode_brg_makloon'=>$kode_brg_makloon
				);
				$this->db->insert('tm_sj_proses_quilting_detail',$data_detail);
			
			// ============ update stok =====================
			//cek stok terakhir tm_stok, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
						
					$new_stok = $stok_lama-$qty; // berkurang stok karena SJ keluar utk makloon
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
						$data_stok = array(
							'kode_brg'=>$kode,
							'stok'=>$new_stok,
						//	'id_gudang'=>$id_gudang, //
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok',$data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' ");
					}
					
					// ############################ new 140611, tm_stok_harga. cek stok di tiap2 harga, ambil stok yg msh ada
					// diurutkan dari harga lama
					$selisih = 0;
					$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg = '$kode' ORDER BY id ASC ");
					if ($query2->num_rows() > 0){
						$hasil2=$query2->result();
							
						$temp_selisih = 0;				
						foreach ($hasil2 as $row2) {
							$stoknya = $row2->stok; // data ke-1: 500		data ke-2: 10
							$harganya = $row2->harga; // data ke-1: 20000	data ke-2: 370000
							
							if ($stoknya > 0) { 
								if ($temp_selisih == 0) // temp_selisih = -6
									$selisih = $stoknya-$qty; // selisih1 = 500-506 = -6
								else
									$selisih = $stoknya+$temp_selisih; // selisih2 = 10-6 = 4
								
								if ($selisih < 0) {
									$temp_selisih = $selisih; // temp_selisih = -6
									$this->db->query(" UPDATE tm_stok_harga SET stok = '0', tgl_update_stok = '$tgl' 
												where kode_brg= '$kode' AND harga = '$harganya' "); //
									
									$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, keluar, saldo, tgl_input, harga) 
											VALUES ('$kode','$no_sj', '$unit_makloon', '$stoknya', '0', '$tgl', '$harganya') ");
								}
									
								if ($selisih > 0) { // ke-2 masuk sini
									if ($temp_selisih == 0)
										$temp_selisih = $qty; // temp_selisih = -6
										
									break;
								}
							}
						} // end for
					}
					// kurangi stoknya dari tm_stok
				/*	$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$kode' AND harga = '$harganya' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
						
					$new_stok = $stok_lama-$qty_sat_awal; // berkurang stok karena SJ keluar utk makloon
				*/
				if ($selisih != 0) {
					$new_stok = $selisih; // new_stok = 4
					$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode' AND harga = '$harganya' ");
					
					// #############################
					
					$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, keluar, saldo, tgl_input, harga) 
											VALUES ('$kode','$no_sj', '$unit_makloon', '".abs($temp_selisih)."', '$new_stok', '$tgl', '$harganya') ");
				}
					 // temp_selisih = -6
			// ==============================================
			} // end if kode
		}
  }
    
  function delete($kode){    
	  $tgl = date("Y-m-d");
	  
		$query3	= $this->db->query(" SELECT no_sj, kode_unit FROM tm_sj_proses_quilting WHERE id = '$kode' ");
		$hasilrow = $query3->row();
		$no_sj	= $hasilrow->no_sj; 
		$unit_makloon	= $hasilrow->kode_unit; 
		// reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting_detail 
											WHERE id_sj_proses_quilting = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// cek dulu satuan brgnya. jika satuannya yard, maka qty di detail ini konversikan lagi ke yard
						/*	$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
												WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$satuan	= $hasilrow->nama_satuan;
								}
								else {
									$satuan	= '';
								}
								
								if ($satuan == "Yard") {
									$qty_sat_awal = $row2->qty / 0.91;
								}
								else {
									$qty_sat_awal = $row2->qty;
								} */
						
						// ============ update stok =====================
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$row2->kode_brg' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset dari SJ keluar utk makloon
								
								if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
									$data_stok = array(
										'kode_brg'=>$row2->kode_brg,
										'stok'=>$new_stok,
									//	'id_gudang'=>$id_gudang, //
										'tgl_update_stok'=>$tgl
										);
									$this->db->insert('tm_stok',$data_stok);
								}
								else {
									$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg= '$row2->kode_brg' ");
								}
								
								// ############################ new 140611, tm_stok_harga. cek stok di tiap2 harga, ambil stok yg msh ada
									// diurutkan dari harga lama
								/*	$query2	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga 
																WHERE kode_brg = '$row2->kode_brg' ORDER BY id ASC ");
									if ($query2->num_rows() > 0){
										$hasil2=$query2->result();
															
										foreach ($hasil2 as $row2) {
											$stoknya = $row2->stok;
											$harganya = $row2->harga;
											
											if ($stoknya != 0) {
												break;
											}
										}
									}
									// kurangi stoknya dari tm_stok
									$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$row2->kode_brg' 
																AND harga = '$harganya' ");
									if ($query3->num_rows() == 0){
										$stok_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_lama	= $hasilrow->stok;
									}
										
									$new_stok = $stok_lama+$qty_sat_awal; // 
									
									if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
										$data_stok = array(
											'kode_brg'=>$row2->kode_brg,
											'stok'=>$new_stok,
											'harga'=>$harganya,
											'tgl_update_stok'=>$tgl
											);
										$this->db->insert('tm_stok_harga',$data_stok);
									}
									else {
										$this->db->query(" UPDATE tm_stok_harga SET stok = '$new_stok', tgl_update_stok = '$tgl' 
										where kode_brg= '$row2->kode_brg' AND harga = '$harganya' "); // sampe siniii
									}
									*/
									// #############################
									
								// @@@@@@@ modifikasi 15 juni 2011
									// query dari tabel tt_stok
									$query4	= $this->db->query(" SELECT id, kode_brg, masuk, keluar, harga FROM tt_stok 
													WHERE kode_brg = '$row2->kode_brg' AND no_bukti = '$no_sj' 
													AND kode_unit = '$unit_makloon'
													ORDER BY id DESC ");
									if ($query4->num_rows() > 0){
										$hasil4=$query4->result();															
										foreach ($hasil4 as $row4) {
											$ttmasuk = $row4->masuk;
											$ttkeluar = $row4->keluar;
											$ttharga = $row4->harga;
											
											if ($ttmasuk != '')
												break;
											
											$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE kode_brg = '$row4->kode_brg' 
																AND harga = '$ttharga' ");
											if ($query3->num_rows() == 0){
												$stok_lama = 0;
											}
											else {
												$hasilrow = $query3->row();
												$stok_lama	= $hasilrow->stok;
											}
											$stokreset = $stok_lama+$ttkeluar;
											
											$this->db->query(" UPDATE tm_stok_harga SET stok = '$stokreset', tgl_update_stok = '$tgl' 
												where kode_brg= '$row4->kode_brg' AND harga = '$ttharga' "); //
									
											$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_unit, masuk, saldo, tgl_input, harga) 
													VALUES ('$row4->kode_brg','$no_sj', '$unit_makloon', '$ttkeluar', '$stokreset', '$tgl', '$ttharga') ");
										}
									}
									// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
								
							/*	$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, masuk, saldo, tgl_input, harga) 
														VALUES ('$row2->kode_brg','$no_sj', '$qty_sat_awal', '$new_stok', '$tgl', '$harganya') "); */
						
						// ==============================================
					}
				}
		
		$this->db->delete('tm_sj_proses_quilting_detail', array('id_sj_proses_quilting' => $kode));
		$this->db->delete('tm_sj_proses_quilting', array('id' => $kode));

  } 
  
  function get_sj($id_sj){
	$query	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting WHERE id = '$id_sj' ");    
    $hasil = $query->result();
    
    $data_sj = array();
	$detail_sj = array();
	
	foreach ($hasil as $row1) {
		// bhn makloon
			$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, 
							tm_satuan b WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg_makloon' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$nama_brg_makloon	= $hasilrow->nama_brg;
				$satuan_makloon	= $hasilrow->nama_satuan;
			}
			else {
				$nama_brg_makloon	= '';
				$satuan_makloon	= '';
			}
			
			// brg jadi
			$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
							WHERE i_product_motif = '$row1->kode_brg_jadi' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$nama_brg_jadi	= $hasilrow->e_product_motifname;
			}
			else {
				$nama_brg_jadi	= '';
			} 
		
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_quilting_detail WHERE id_sj_proses_quilting = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {						
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
												
						$query3	= $this->db->query(" SELECT c.kode_kel_brg FROM tm_barang a, tm_jenis_bahan b, tm_jenis_barang c 
									WHERE a.id_jenis_bahan = b.id AND b.id_jenis_barang = c.id AND kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
						
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$qty_stok = $hasilrow->stok;
				
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'kode_kel_brg'=> $kode_kel_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'qty_stok'=> $qty_stok,
												'qty_meter'=> $row2->qty_meter,
												'detail_pjg'=> $row2->detail_pjg_kain
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				// ambil data nama unit
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
				
				// ambil nama jenis makloon
				$query3	= $this->db->query(" SELECT nama FROM tm_jenis_makloon 
										WHERE id = '$row1->id_jenis_makloon' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_jenis_makloon	= $hasilrow->nama;
				}
				else
					$nama_jenis_makloon = '';
								 
				$data_sj[] = array(			'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'detail_sj'=> $detail_sj,
											//'id_detailnya'=> $id_detailnya
											'id_jenis_makloon'=> $row1->id_jenis_makloon,
											'nama_jenis_makloon'=> $nama_jenis_makloon,
											'makloon_internal'=> $row1->makloon_internal,
											'kode_brg_makloon'=> $row1->kode_brg_makloon,
											'nama_brg_makloon'=> $nama_brg_makloon,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $nama_brg_jadi
											);
				$detail_sj = array();
				
	}
	return $data_sj;
  }
  
  function get_unit_quilting(){
    $this->db->select(" * from tm_unit_quilting order by kode_unit ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function generate_nomor(){
    // generate no SJ Keluar Makloon
			$th_now	= date("Y");
			
			// generate no SJ
			$query3	= $this->db->query(" SELECT no_sj FROM tm_sj_proses_quilting ORDER BY no_sj DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
			if(strlen($no_sj)==14) {
				$nosj = substr($no_sj, 5, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}
			$nomorsj = "SJKQ-".$nomorsj;

			return $nomorsj;  
  }
  
  function get_jenis_makloon(){
    $this->db->select("* from tm_jenis_makloon order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_bhn)
  {
	if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) order by b.kode, c.kode, a.tgl_update DESC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_bhn){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function get_jenis_bhn($kel_brg) {
	$sql = " SELECT d.*, c.nama as nj_brg FROM tm_kelompok_barang b, tm_jenis_barang c, tm_jenis_bahan d 
				WHERE b.kode = c.kode_kel_brg AND c.id = d.id_jenis_barang 
				AND b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode DESC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  function get_bahan_makloon($num, $offset, $cari, $jenis_makloon)
  {
	if ($cari == "all") {
		$sql = " * FROM tm_brg_hasil_makloon ";
		if ($jenis_makloon != '')				
			$sql.= " where id_jenis_makloon = '$jenis_makloon' ";
		
		$sql.=" order by kode_brg ASC";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tm_brg_hasil_makloon ";
		if ($jenis_makloon != '')				
			$sql.= " WHERE id_jenis_makloon = '$jenis_makloon' ";
		
		$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) order by kode_brg ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM tm_stok_hasil_makloon WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama 
							FROM tm_satuan WHERE id = '$row1->satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama;
						}

			$data_bhn[] = array(		'kode_brg'=> $row1->kode_brg,	
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahan_makloontanpalimit($cari, $jenis_makloon){
	if ($cari == "all") {
		$sql = " SELECT * FROM tm_brg_hasil_makloon ";
		if ($jenis_makloon != '')
			$sql.= " where id_jenis_makloon = '$jenis_makloon' ";
		
		$sql.=" order by kode_brg ASC";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
	else {
		$sql = " SELECT * FROM tm_brg_hasil_makloon ";
		if ($jenis_makloon != '')				
			$sql.= " WHERE id_jenis_makloon = '$jenis_makloon' ";
		
		$sql.=" AND (UPPER(kode_brg) like UPPER('%$cari%') OR UPPER(nama_brg) like UPPER('%$cari%')) order by kode_brg ASC";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_brgjadi($num, $offset, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') 
				order by i_product_motif ASC"; 
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			

			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%$cari%') OR UPPER(e_product_motifname) like UPPER('%$cari%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }

}
