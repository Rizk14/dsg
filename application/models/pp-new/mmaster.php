<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function statusop($id) {
  	return $this->db->query(" SELECT b.id FROM tm_op a, tm_op_detail b WHERE a.id = b.id_op 
				AND b.status_op= 't' AND a.id_pp= '$id' ");
  }
  
  function getAll($num, $offset, $cari) {
	
		if ($cari=="all") {	
			$query	= $this->db->query(" SELECT * FROM tm_pp ORDER BY tgl_pp DESC, no_pp DESC LIMIT ".$num." OFFSET ".$offset);
		} else {
			$query	= $this->db->query(" SELECT * FROM tm_pp WHERE UPPER(no_pp) like UPPER('%$cari%') 
						ORDER BY tgl_pp DESC, no_pp DESC LIMIT ".$num." OFFSET ".$offset);
		}
	
		$data_pp = array();
		$detail_pp = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT kode, nama FROM tm_bagian WHERE id = '$row1->id_bagian' " );
				$hasilrow = $query3->row();
				$kode_bagian	= $hasilrow->kode;
				$nama_bagian	= $hasilrow->nama;
				
				// ambil data detail barangnya
				$query2	= $this->db->query( " SELECT * FROM tm_pp_detail WHERE id_pp = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$row2->id_brg' " );
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						//$nama_brg	= $hasilrow->nama_brg;
						//$satuan	= $hasilrow->nama_satuan;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' " );
						$hasilrow = $query3->row();
						$satuan	= $hasilrow->nama;
						
						// 11-07-2012
						$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$row2->id' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$sql = " SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
									WHERE a.id_pp_detail = '$row2->id' ";
							$query3	= $this->db->query($sql);
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum;
						}
						else {
							// 16-06-2015 GA DIPAKE
							/*$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE b.id_pp = '$row1->id' AND a.id_brg = '$row2->id_brg' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
							*/
							$jum_op = 0;
						}
						// ======= end 11-07-2012 =====================
						
						$sisanya = $row2->qty-$jum_op;
						if ($sisanya < 0)
							$sisanya = 0;
						
						// 16-06-2015 DIKOMEN, MSH NYARI DIPAKE DIMANA. GANTI PAKE YG BARU AJA
						/*$query3	= $this->db->query(" SELECT count(b.id) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$row1->id' AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$count_op = $hasilrow->jum; */
						
						$count_op = 0;
						$query3	= $this->db->query(" SELECT distinct a.id_op FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE a.id_pp_detail = '$row2->id' ");
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
							foreach ($hasil3 as $row3) {
								$count_op++;
							}
						}
						else
							$count_op = 0;
						
						if ($row2->is_satuan_lain == 't') {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
							$hasilrow = $query3->row();
							$sat_lain = $hasilrow->nama;
						}
						else
							$sat_lain = '';
				
						$detail_pp[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $row2->nama_brg,
												'satuan'=> $satuan,
												'id_satuan'=> $row2->id_satuan,
												'qty'=> $row2->qty,
												'jum_op'=> $jum_op,
												'sisanya'=> $sisanya,
												'count_op'=> $count_op,
												'keterangan'=> $row2->keterangan,
												'status_faktur'=> $row2->status_faktur,
												'is_satuan_lain'=> $row2->is_satuan_lain,
												'sat_lain'=> $sat_lain
											);
					}
				}
				else {
					$detail_pp = '';
				}
				
				$data_pp[] = array('id'=> $row1->id,	
							'no_pp'=> $row1->no_pp,
							'tgl_pp'=> $row1->tgl_pp,
							'kode_bagian'=> $kode_bagian,
							'nama_bagian'=> $nama_bagian,
							'tgl_update'=> $row1->tgl_update,
							'status_faktur'=> $row1->status_faktur,
							'status_edit'=> $row1->status_edit,
							'status_aktif'=> $row1->status_aktif,
							'detail_pp'=> $detail_pp
						);
				
				$detail_pp = array();
			} // endforeach header
		}
		else {
			$data_pp = '';
		}
		return $data_pp;
  }
  
  function getAlltanpalimit($cari){
	if ($cari == "all") {		
		$query	= $this->db->query(" SELECT * FROM tm_pp ");
	}  else {
		$query  = $this->db->query(" SELECT * FROM tm_pp WHERE UPPER(no_pp) like UPPER('%$cari%') ");
	}
	return $query->result();  
  }
  
  function get_header_pp($id_pp){
    //$query = $this->db->getwhere('tm_pp',array('id'=>$id_pp));
    $query	= $this->db->query(" SELECT * FROM tm_pp WHERE id='$id_pp' ");
    return $query->result();
  }
  
  function get_detail_pp($id_pp){
    //$query = $this->db->getwhere('tm_pp_detail',array('id_pp'=>$id_pp));
    $query	= $this->db->query(" SELECT * FROM tm_pp_detail WHERE id_pp = '$id_pp' AND status_faktur = 'f' ORDER BY id ASC ");
    $hasil = $query->result();
    $detail_pp = array();
    foreach ($hasil as $row1) {		
			$query2	= $this->db->query(" SELECT a.kode_brg, c.kode_kel_brg
						FROM tm_barang a INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id
						WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query2->row();
			$kode_brg	= $hasilrow->kode_brg;
			//$nama_brg	= $hasilrow->nama_brg;
			$kode_kel_brg	= $hasilrow->kode_kel_brg;
			//$nama_satuan	= $hasilrow->nama_satuan;
			
			$query2	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->id_satuan' ");
			$hasilrow = $query2->row();
			$nama_satuan	= $hasilrow->nama;
		
			$detail_pp[] = array('id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'nama'=> $row1->nama_brg,
											'nama_satuan'=> $nama_satuan,
											'id_satuan'=> $row1->id_satuan,
											'kode_kel_brg'=> $kode_kel_brg,
											'qty'=> $row1->qty,
											'keterangan'=> $row1->keterangan,
											//'is_satuan_lain'=> $row1->is_satuan_lain,
											//'satuan_lain'=> $row1->satuan_lain
										);
	}
	return $detail_pp;
  }  
    
  function cek_data($no_pp){
    $this->db->select("id from tm_pp WHERE no_pp = '$no_pp' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
		return $query->result();
	}
  }
  
  //
  function save($no_pp,$tgl_pp, $id_bagian, $id_brg,$nama, $id_satuan, $jumlah, $keterangan, $goedit){  
	  // $is_satuan_lain, $satuan_lain ga dipake lg
	  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
    
		$is_satuan_lain = 'f';
		$satuan_lain = 0;
	
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pp WHERE no_pp = '$no_pp' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
    if ($goedit == '') {
		// jika data header blm ada 
		if(count($hasil)== 0) {
			$data_header = array(
			  'no_pp'=>$no_pp,
			  'tgl_pp'=>$tgl_pp,
			  'id_bagian'=>$id_bagian,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'uid_update_by'=>$uid_update_by
			);
			$this->db->insert('tm_pp',$data_header);
			
			// ambil data terakhir di tabel tm_pp
			$query2	= $this->db->query(" SELECT id FROM tm_pp ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pp	= $hasilrow->id;
			
			if ($id_brg!='' && ($jumlah!='' || $jumlah!=0) ) {
				// jika semua data tdk kosong, insert ke tm_pp_detail
				$data_detail = array(
				  'id_brg'=>$id_brg,
				  'qty'=>$jumlah,
				  'keterangan'=>$keterangan,
				  'id_pp'=>$id_pp,
				  'is_satuan_lain'=>$is_satuan_lain,
				  'satuan_lain'=>$satuan_lain,
				  'nama_brg'=>$nama,
				  'id_satuan'=>$id_satuan
				);
				$this->db->insert('tm_pp_detail',$data_detail);
			}
		}
		else {
			// ambil id di tabel tm_pp sesuai no_pp
			$query2	= $this->db->query(" SELECT id from tm_pp WHERE no_pp = '$no_pp' ");
			$hasilrow = $query2->row();
			$id_pp	= $hasilrow->id;
			
			if ($id_brg!='' && ($jumlah!=''|| $jumlah!=0) ) {
				// jika semua data tdk kosong, insert ke tm_pp_detail
				$data_detail = array(
				  'id_brg'=>$id_brg,
				  'qty'=>$jumlah,
				  'keterangan'=>$keterangan,
				  'id_pp'=>$id_pp,
				  'is_satuan_lain'=>$is_satuan_lain,
				  'satuan_lain'=>$satuan_lain,
				  'nama_brg'=>$nama,
				  'id_satuan'=>$id_satuan
				);
				$this->db->insert('tm_pp_detail',$data_detail);
			}
		}
	} // end if goedit == ''
  }
  
  function update_pp($id_pp,$id_brg,$nama, $id_satuan, $jumlah, $keterangan, $goedit){
	  // $is_satuan_lain, $satuan_lain, GA DIPAKE
	  if ($id_brg !='' && $jumlah !='') {
		//if ($is_satuan_lain == '') {
			$is_satuan_lain = 'f';
			$satuan_lain = 0;
		//}
			// jika semua data tdk kosong, insert ke tm_pp_detail
			$data_detail = array(
				'id_brg'=>$id_brg,
				'qty'=>$jumlah,
				'keterangan'=>$keterangan,
				'id_pp'=>$id_pp,
				'is_satuan_lain'=>$is_satuan_lain,
				'satuan_lain'=>$satuan_lain,
				'nama_brg'=>$nama,
				'id_satuan'=>$id_satuan
				);
			
			$this->db->insert('tm_pp_detail',$data_detail);
	  }
  }  
  
  function delete($id) { 
  /*	$qop	= $this->db->query(" SELECT a.id AS idpp FROM tm_pp a WHERE a.id='$kode' ORDER BY a.no_pp DESC LIMIT 1 ");
	if($qop->num_rows()>0) {
		$row_op	= $qop->row();
		$idpp	= $row_op->id_pp;
		$qtop	= $this->db->query(" SELECT * FROM tm_op WHERE id_pp ='$idpp' AND status_op = 'f' ORDER BY id DESC");
		if($qtop->num_rows()>0) {
			$row_top	= $qtop->row();
			$idop	= $row_top->id;
			
			$this->db->delete('tm_pp_detail', array('id_pp' => $kode));
			$this->db->delete('tm_pp', array('id' => $kode));
			$this->db->delete('tm_op', array('id_pp' => $idpp));
			$this->db->delete('tm_op_detail', array('id_op' => $idop));
		}
	} */
	
		$this->db->delete('tm_pp_detail', array('id_pp' => $id));
		
		/*$query2	= $this->db->query( " SELECT id FROM tm_op WHERE id_pp = '$kode' ");
				if ($query2->num_rows() > 0) {
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$this->db->query(" DELETE FROM tm_op_detail WHERE id_op = '$row2->id' ");
					}
					$this->db->delete('tm_op', array('id_pp' => $kode));
				} */
		$this->db->delete('tm_pp', array('id' => $id));
  }
  
  function deleteitem($iddetail) { 
		$this->db->delete('tm_pp_detail', array('id' => $iddetail));
  }
  
  function get_item_pp($iddetail){
	$query = $this->db->query(" SELECT a.no_pp, a.tgl_pp, a.id_bagian, b.* FROM tm_pp a INNER JOIN tm_pp_detail b 
					ON a.id = b.id_pp WHERE b.id = '$iddetail' ");
	$datadetail = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil data nama barang
			$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id
							WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			$nama_satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama department
			$query3	= $this->db->query(" SELECT kode, nama FROM tm_bagian WHERE id = '$row1->id_bagian' ");
			$hasilrow = $query3->row();
			$kode_bagian	= $hasilrow->kode;
			$nama_bagian	= $hasilrow->nama;
			
			$query3	= $this->db->query(" SELECT c.kode_kel_brg FROM tm_barang a INNER JOIN tm_jenis_barang c ON a.id_jenis_barang = c.id 
						WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_kel_brg = $hasilrow->kode_kel_brg;
			
			$datadetail[] = array(			'id'=> $row1->id,	
											'id_pp'=> $row1->id_pp,	
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'no_pp'=> $row1->no_pp,
											'tgl_pp'=> $row1->tgl_pp,
											'nama_brg'=> $nama_brg,
											'kode_kel_brg'=> $kode_kel_brg,
											'nama_satuan'=> $nama_satuan,
											'kode_bagian'=> $kode_bagian,
											'nama_bagian'=> $nama_bagian,
											'qty'=> $row1->qty
											);
		}
	}
	return $datadetail;
  }
  
  function get_bahan($num, $offset, $cari, $kel_brg, $id_jenis_brg)
  {
		$sql = " a.*, b.nama as nj_brg, d.nama as nama_satuan 
						FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id
						INNER JOIN tm_kelompok_barang e ON b.kode_kel_brg = e.kode
						INNER JOIN tm_satuan d ON a.satuan = d.id 						
						WHERE a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_brg != '0')
			$sql.= " AND b.id = '$id_jenis_brg' ";
		if ($cari != "all")
			$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))  ";
		$sql.= " ORDER BY a.kode_brg ";
		$this->db->select($sql, false)->limit($num,$offset);
		
	/*if ($cari == "all") {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" order by a.kode_brg";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = "a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
				order by a.kode_brg";
		$this->db->select($sql, false)->limit($num,$offset);	
    } */

    $query = $this->db->get();
    return $query->result();
  }
  
  function get_bahantanpalimit($cari, $kel_brg, $id_jenis_brg){
	  $sql = " SELECT a.*, b.nama as nj_brg, d.nama as nama_satuan 
						FROM tm_barang a INNER JOIN tm_jenis_barang b ON a.id_jenis_barang = b.id
						INNER JOIN tm_kelompok_barang e ON b.kode_kel_brg = e.kode
						INNER JOIN tm_satuan d ON a.satuan = d.id 						
						WHERE a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_brg != '0')
			$sql.= " AND b.id = '$id_jenis_brg' ";
		if ($cari != "all")
			$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))  ";
		$sql.= " ORDER BY a.kode_brg ";
		$query	= $this->db->query($sql);
		return $query->result();
	  
	/*if ($cari == "all") {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		$query	= $this->db->query($sql);
		return $query->result();
	}
	else {
		$sql = " select a.*, b.nama as nj_brg, c.kode as kode_jenis, c.nama as nama_jenis, d.nama as nama_satuan 
			FROM tm_barang a, tm_jenis_barang b, tm_jenis_bahan c, tm_satuan d, tm_kelompok_barang e
						WHERE a.id_jenis_bahan = c.id AND b.id = c.id_jenis_barang AND a.satuan = d.id
						AND b.kode_kel_brg = e.kode AND a.status_aktif = 't' ";
		if ($kel_brg != '')				
			$sql.= " AND e.kode = '$kel_brg' ";
		if ($id_jenis_bhn != '0')
			$sql.= " AND c.id = '$id_jenis_bhn' ";
		
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))";
		
		$query	= $this->db->query($sql);
		return $query->result();
		
		//return $this->db->query($sql);
	} */
  }
  
  function getpp($nomorpp) {
  	$qgetpp	= " SELECT * FROM tm_pp WHERE no_pp=trim('$nomorpp') ";
  	return $this->db->query($qgetpp);
  }
  
 /* function crepp($jenis_brg) {
  	return $this->db->query(" SELECT cast(no_pp AS integer)+1 AS no_pp FROM tm_pp WHERE jenis_brg='$jenis_brg' ORDER BY cast(no_pp AS integer) DESC LIMIT 1 ");	
  } */
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_satuan(){
    $this->db->select("* from tm_satuan order by id", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bagian(){
    $this->db->select("* from tm_bagian order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_brg($kel_brg) {
	$sql = " SELECT c.*, b.kode as kode_kel_brg, b.nama as nama_kel_brg 
				FROM tm_kelompok_barang b INNER JOIN tm_jenis_barang c ON b.kode = c.kode_kel_brg
				WHERE b.kode = '$kel_brg' "; 	
	$sql.= " ORDER BY c.kode ASC ";
	$query	= $this->db->query($sql);    
	
    return $query->result();  
  }
  
  //02-02-2013
  function updatestatus($id, $aksi){    
	  if ($aksi == "on")
		$this->db->query(" UPDATE tm_pp SET status_aktif = 't' where id= '$id' ");
	  else
		$this->db->query(" UPDATE tm_pp SET status_aktif = 'f' where id= '$id' ");
  }

}

?>
