<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function hargaperpelanggan($imotif,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}

	function lcustomer() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_customer ORDER BY e_customer_name ASC ");
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}
		
	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
				SELECT 	a.i_product_base AS iproduct,
					b.i_product_motif AS imotif,	
					b.e_product_motifname AS motifname,
					b.n_quantity AS qty
					
				FROM tr_product_base a 
				
				RIGHT JOIN tr_product_motif b ON a.i_product_base=b.i_product ORDER BY b.i_product_motif DESC " );
	}	
	
	function fgetfakturpenjualan($key) {
		$db2=$this->load->database('db_external', TRUE);
		$thn_skrg = date("Y");
		if(!empty($key)) {
			return $db2->query("
			SELECT i_faktur_code, d_faktur FROM tm_faktur_do_t 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f' AND i_faktur_code like '%$key%'
				UNION SELECT i_faktur_code, d_faktur FROM tm_faktur 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f' AND i_faktur_code like '%$key%'
				UNION SELECT i_faktur_code, d_faktur FROM tm_faktur_bhnbaku 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f' AND i_faktur_code like '%$key%'
				ORDER BY d_faktur DESC, i_faktur_code DESC ");
		}
	}
	
	// 18-04-2013
	function getfakturpenjualan() {
		$db2=$this->load->database('db_external', TRUE);
		$thn_skrg = date("Y");
		return $db2->query(" SELECT i_faktur_code, d_faktur FROM tm_faktur_do_t 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f'
				UNION SELECT i_faktur_code, d_faktur FROM tm_faktur 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f'
				UNION SELECT i_faktur_code, d_faktur FROM tm_faktur_bhnbaku 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f' 
				ORDER BY d_faktur DESC, i_faktur_code DESC ");
	}	
	
	function getfakturpenjualanperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$thn_skrg = date("Y");
		$query = $db2->query( " SELECT i_faktur_code, d_faktur FROM tm_faktur_do_t 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f'
				UNION SELECT i_faktur_code, d_faktur FROM tm_faktur 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f'
				UNION SELECT i_faktur_code, d_faktur FROM tm_faktur_bhnbaku 
				WHERE d_faktur >='".$thn_skrg."-01-01' AND f_faktur_cancel = 'f' 
				ORDER BY d_faktur DESC, i_faktur_code DESC
				 LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		
		}
	}
	
	function getnomorfakturpajak($i_faktur_code, $dfirst, $dlast) {
		$db2=$this->load->database('db_external', TRUE);
		/*$sql = " SELECT distinct a.i_faktur_code, a.nomor_pajak FROM tm_nomor_pajak_faktur a, tm_faktur b, 
				tm_faktur_do_t c, tm_faktur_bhnbaku d WHERE 
				(a.i_faktur_code = b.i_faktur_code OR a.i_faktur_code = c.i_faktur_code
				OR a.i_faktur_code = d.i_faktur_code ) ";
		if ($i_faktur_code != '')
			$sql.= " AND a.i_faktur_code like '%$i_faktur_code%' ";
		if ($dfirst != '0' && $dlast != '0')
			$sql.= " AND (b.d_faktur BETWEEN '$dfirst' AND '$dlast' OR c.d_faktur BETWEEN '$dfirst' AND '$dlast'
					OR d.d_faktur BETWEEN '$dfirst' AND '$dlast' ) "; */
		
		$sql = " SELECT a.i_faktur_code, a.nomor_pajak, b.d_faktur FROM tm_nomor_pajak_faktur a, tm_faktur b
				WHERE a.i_faktur_code = b.i_faktur_code AND b.f_faktur_cancel = 'f' ";
		if ($i_faktur_code != '')
			$sql.= " AND a.i_faktur_code like '%$i_faktur_code%' ";
		if ($dfirst != '' && $dlast != '')
			$sql.= " AND (b.d_faktur BETWEEN '$dfirst' AND '$dlast') ";
			
		$sql.= " UNION SELECT a.i_faktur_code, a.nomor_pajak, b.d_faktur FROM tm_nomor_pajak_faktur a, tm_faktur_do_t b
				WHERE a.i_faktur_code = b.i_faktur_code AND b.f_faktur_cancel = 'f' ";
		if ($i_faktur_code != '')
			$sql.= " AND a.i_faktur_code like '%$i_faktur_code%' ";
		if ($dfirst != '' && $dlast != '')
			$sql.= " AND (b.d_faktur BETWEEN '$dfirst' AND '$dlast') ";	
		
		$sql.= " UNION SELECT a.i_faktur_code, a.nomor_pajak, b.d_faktur FROM tm_nomor_pajak_faktur a, tm_faktur_bhnbaku b
				WHERE a.i_faktur_code = b.i_faktur_code AND b.f_faktur_cancel = 'f' ";
		if ($i_faktur_code != '')
			$sql.= " AND a.i_faktur_code like '%$i_faktur_code%' ";
		if ($dfirst != '' && $dlast != '')
			$sql.= " AND (b.d_faktur BETWEEN '$dfirst' AND '$dlast') ";
		$sql.= " ORDER BY d_faktur DESC, i_faktur_code DESC ";
		//echo $sql; die();
		$query = $db2->query($sql);
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}	
}
?>
