<?php
class Mmaster extends CI_Model
{
	function __construct()
	{

		parent::__construct();
	}

	function get_gudang()
	{
		$query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE jenis='2' ORDER BY a.kode_lokasi, a.kode_gudang");

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cek_so($bulan, $tahun, $gudang)
	{
		$query3	= $this->db->query(" SELECT id, tgl_so, jenis_perhitungan_stok, status_approve FROM tt_stok_opname_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");

		if ($query3->num_rows() > 0) {
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
			$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
		} else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
			$jenis_perhitungan_stok = '';
		}

		$so_bahan = array(
			'status_approve' => $status_approve,
			'idnya' => $idnya,
			'tgl_so' => $tgl_so,
			'jenis_perhitungan_stok' => $jenis_perhitungan_stok
		);

		return $so_bahan;
	}

	function get_all_stok_hasil_jahit($bulan, $tahun, $gudang)
	{
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0, 0, 0, $bulan, 1, $tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d', $timeStamp);    //get first day of the given month
		list($y, $m, $t)        =    explode('-', date('Y-m-t', $timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0, 0, 0, $m, $t, $y); //create time stamp of the last date of the give month
		$lastDay            =    date('d', $lastDayTimeStamp); // Find last day of the month

		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun - 1;
		} else {
			$bln_query = $bulan - 1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0" . $bln_query;
		}

		$sql = " SELECT a.id, a.id_brg_wip, b.kode_brg, b.nama_brg FROM tm_stok_hasil_jahit a 
				INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id WHERE a.id_gudang = '$gudang' ORDER BY b.kode_brg ";

		$query	= $this->db->query($sql);

		if ($query->num_rows() > 0) {
			$hasil = $query->result();
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {
				// 20-10-2015 GA DIPAKE!
				// ---------- 04-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
				/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a, 
									tt_stok_opname_hasil_jahit_detail b
									WHERE a.id = b.id_stok_opname_hasil_jahit
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				// 3. hitung keluar bln ini
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar; */
				//-------------------------------------------------------------------------------------

				$sqlxx = " SELECT DISTINCT c.stok, c.id_warna, b.nama as nama_warna FROM 
						tm_warna b INNER JOIN tm_stok_hasil_jahit_warna c ON b.id = c.id_warna
						WHERE c.id_stok_hasil_jahit = '$row->id'
						ORDER BY b.nama ";

				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->result();

					foreach ($hasilxx as $rowxx) {
						// 20-10-2015 GA DIPAKE!
						// ---------- 04-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						/*	$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a 
											INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
											INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						// 3. hitung keluar bln ini
						// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna; */
						//-------------------------------------------------------------------------------------



						$detail_warna[] = array(
							'id_warna' => $rowxx->id_warna,
							'nama_warna' => $rowxx->nama_warna,

							//	'tahun_periode'=> $tahun_periode,
							//		'bulan_periode'=> $bulan_periode,
							//		'tanggal_periode'=>$tanggal_periode,
							//'saldo_akhir'=> $rowxx->stok,
							//'stok_opname'=> $rowxx->stok
							'saldo_akhir' => 0,
							'stok_opname' => 0
						);
					}
				} else {
					$sqlxx = " SELECT a.id_warna, b.nama as nama_warna FROM tm_warna_brg_wip a 
							INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip = '$row->id_brg_wip'
							ORDER BY b.nama ";

					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0) {
						$hasilxx = $queryxx->result();

						foreach ($hasilxx as $rowxx) {
							$detail_warna[] = array(
								'id_warna' => $rowxx->id_warna,
								'nama_warna' => $rowxx->nama_warna,
								'saldo_akhir' => 0,
								'stok_opname' => 0
							);
						}
					} else
						$detail_warna = '';
				}

				$detail_bahan[] = array(
					'id_brg_wip' => $row->id_brg_wip,
					'kode_brg_wip' => $row->kode_brg,
					'nama_brg_wip' => $row->nama_brg,
					'detail_warna' => $detail_warna
				);
				$detail_warna = array();
			}
		} else
			$detail_bahan = '';

		return $detail_bahan;
	}

	function get_all_stok_opname($bulan, $tahun, $gudang)
	{
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0, 0, 0, $bulan, 1, $tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d', $timeStamp);    //get first day of the given month
		list($y, $m, $t)        =    explode('-', date('Y-m-t', $timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0, 0, 0, $m, $t, $y); //create time stamp of the last date of the give month
		$lastDay            =    date('d', $lastDayTimeStamp); // Find last day of the month

		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun - 1;
		} else {
			$bln_query = $bulan - 1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0" . $bln_query;
		}

		$query	= $this->db->query(" SELECT b.*, d.kode_brg, d.nama_brg FROM tt_stok_opname_hasil_jahit_detail b 
					INNER JOIN tt_stok_opname_hasil_jahit a ON b.id_stok_opname_hasil_jahit = a.id
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang = '$gudang'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY d.kode_brg ");

		if ($query->num_rows() > 0) {
			$hasil = $query->result();

			$detail_bahan = array();
			foreach ($hasil as $row) {

				// ---------- 09-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 21-10-2015 GA DIPAKE!
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
				/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a, 
									tt_stok_opname_hasil_jahit_detail b
									WHERE a.id = b.id_stok_opname_hasil_jahit
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				// 3. hitung keluar bln ini
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
							AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar; */
				//-------------------------------------------------------------------------------------

				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_hasil_jahit_detail_warna a 
							INNER JOIN tm_warna c ON c.id = a.id_warna  
							WHERE a.id_stok_opname_hasil_jahit_detail = '$row->id'
							ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->result();

					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a INNER JOIN tm_stok_hasil_jahit_warna b ON a.id = b.id_stok_hasil_jahit
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_gudang = '$gudang'
							AND b.id_warna = '$rowxx->id_warna' ");

						if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						} else {
							$stok = '0';
						}

						// ---------- 04-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 21-10-2015 GA DIPAKE!
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						/*	$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a 
											INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
											INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						// 3. hitung keluar bln ini
						// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna; */
						//-------------------------------------------------------------------------------------
						// ===================================================

						$detail_warna[] = array(
							'id_warna' => $rowxx->id_warna,
							'nama_warna' => $rowxx->nama,
							//'saldo_akhir'=> $stok,
							'saldo_akhir' => $rowxx->saldo_akhir,
							'stok_opname' => $rowxx->jum_stok_opname
						);
					}
				} else
					$detail_warna = '';

				$detail_bahan[] = array(
					'kode_brg_wip' => $row->kode_brg,
					'id_brg_wip' => $row->id_brg_wip,
					'nama_brg_wip' => $row->nama_brg,
					'detail_warna' => $detail_warna
				);
				$detail_warna = array();
			}
		} else {
			$detail_bahan = '';
		}
		return $detail_bahan;
	}

	function save(
		$is_new,
		$id_stok,
		$is_pertamakali,
		$id_brg_wip,
		$id_warna,
		$stok,
		$stok_fisik,
		$id_warna2,
		$saldo_akhir,
		$gudang,
		$hapusitem
	) {
		$tgl = date("Y-m-d H:i:s");

		if ($is_pertamakali != '1') {
			// 06-02-2014
			//-------------- hitung total qty dari detail tiap2 warna -------------------
			$qtytotalstokawal = 0;
			$qtytotalstokfisik = 0;
			//$qtytotalsaldoakhir = 0;
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok[$xx] = trim($stok[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);

				//01-12-2015
				//$id_warna2[$xx] = trim($id_warna2[$xx]);
				//$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);

				$qtytotalstokawal += $stok[$xx];
				$qtytotalstokfisik += $stok_fisik[$xx];
				//$qtytotalsaldoakhir+= $saldo_akhir[$xx];
			} // end for
			// ---------------------------------------------------------------------

			if ($is_new == '1') {
				// 26-01-2016 tambahkan skrip utk hapus ke tm_stok_hasil_jahit jika diceklis
				if ($hapusitem == 'y') {
					$this->db->query(" DELETE FROM tm_stok_hasil_jahit_warna WHERE id_stok_hasil_jahit IN 
							(select id FROM tm_stok_hasil_jahit WHERE id_brg_wip = '$id_brg_wip' AND id_gudang='$gudang') ");
					$this->db->query(" DELETE FROM tm_stok_hasil_jahit WHERE id_brg_wip = '$id_brg_wip' AND id_gudang='$gudang' ");
				} else {
					$seq_id	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
					if ($seq_id->num_rows() > 0) {
						$id_sodetail	= $seq_id->row();
						$id_sodetail_n	= $id_sodetail->id + 1;
					} else {
						$id_sodetail_n	= 1;
					}
					$data_detail = array(
						'id' => $id_sodetail_n,
						'id_stok_opname_hasil_jahit' => $id_stok,
						'id_brg_wip' => $id_brg_wip,
						'stok_awal' => $qtytotalstokawal,
						'jum_stok_opname' => $qtytotalstokfisik,
						//'saldo_akhir'=>$qtytotalsaldoakhir
						'saldo_akhir' => 0
					);
					$this->db->insert('tt_stok_opname_hasil_jahit_detail', $data_detail);

					// ambil id detail id_stok_opname_hasil_jahit_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
					if ($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					} else
						$iddetail = 0;

					// ----------------------------------------------
					for ($xx = 0; $xx < count($id_warna); $xx++) {
						$id_warna[$xx] = trim($id_warna[$xx]);
						$stok[$xx] = trim($stok[$xx]);
						$stok_fisik[$xx] = trim($stok_fisik[$xx]);
						//01-12-2015
						//$id_warna2[$xx] = trim($id_warna2[$xx]);
						//$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);

						$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
						if ($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id + 1;
						} else {
							$idbaru	= 1;
						}

						$tt_stok_opname_hasil_jahit_detail_warna	= array(
							'id' => $idbaru,
							'id_stok_opname_hasil_jahit_detail' => $iddetail,
							'id_warna' => $id_warna[$xx],
							'jum_stok_opname' => $stok_fisik[$xx],
							//'saldo_akhir'=>$saldo_akhir[$xx]
							'saldo_akhir' => 0
						);
						$this->db->insert('tt_stok_opname_hasil_jahit_detail_warna', $tt_stok_opname_hasil_jahit_detail_warna);
					} // end for
				} // end pengecekan
			} else {
				// ambil id detail id_stok_opname_hasil_jahit_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail 
							where id_brg_wip = '$id_brg_wip' 
							AND id_stok_opname_hasil_jahit = '$id_stok' ");
				if ($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				} else
					$iddetail = 0;

				$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$qtytotalstokfisik', 
								saldo_akhir = '0'
								where id = '$iddetail' ");
				// saldo_akhir = '$qtytotalsaldoakhir'

				for ($xx = 0; $xx < count($id_warna); $xx++) {
					$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET jum_stok_opname = '" . $stok_fisik[$xx] . "',
								saldo_akhir = '0'
								WHERE id_stok_opname_hasil_jahit_detail='$iddetail' AND id_warna = '" . $id_warna[$xx] . "' ");
					// saldo_akhir = '".$saldo_akhir[$xx]."'
				}
				// ====================
			}
		} else {
			// 04-02-2016 diganti jadi perwarna utk yg pertama kali
			/*  if ($is_new == '1') {
		   $data_detail = array(
						'id_stok_opname_hasil_jahit'=>$id_stok,
						'id_brg_wip'=>$id_brg_wip, 
						'stok_awal'=>0,
						'jum_stok_opname'=>$stok_fisik,
						'saldo_akhir'=>$saldo_akhir
					);
		   $this->db->insert('tt_stok_opname_hasil_jahit_detail',$data_detail);
		  }
		  else {
			  // ambil id detail id_stok_opname_hasil_jahit_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail 
							where id_brg_wip = '$id_brg_wip' 
							AND id_stok_opname_hasil_jahit = '$id_stok' ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
			  
			  $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$stok_fisik', 
								saldo_akhir = '$saldo_akhir'
								where id = '$iddetail' ");
				// ====================
		  } */

			// 04-02-2016
			$qtytotalstokawal = 0;
			$qtytotalstokfisik = 0;
			$qtytotalsaldoakhir = 0;
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok[$xx] = trim($stok[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);

				//01-12-2015
				$id_warna2[$xx] = trim($id_warna2[$xx]);
				$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);

				$qtytotalstokawal += $stok[$xx];
				$qtytotalstokfisik += $stok_fisik[$xx];
				$qtytotalsaldoakhir += $saldo_akhir[$xx];
			} // end for
			//print_r($saldo_akhir); die();
			// ---------------------------------------------------------------------

			if ($is_new == '1') {
				$data_detail = array(
					'id_stok_opname_hasil_jahit' => $id_stok,
					'id_brg_wip' => $id_brg_wip,
					'stok_awal' => $qtytotalstokawal,
					'jum_stok_opname' => $qtytotalstokfisik,
					'saldo_akhir' => $qtytotalsaldoakhir
				);
				$this->db->insert('tt_stok_opname_hasil_jahit_detail', $data_detail);

				// ambil id detail id_stok_opname_hasil_jahit_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
				if ($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				} else
					$iddetail = 0;

				// ----------------------------------------------
				for ($xx = 0; $xx < count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$stok[$xx] = trim($stok[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					//01-12-2015
					$id_warna2[$xx] = trim($id_warna2[$xx]);
					$saldo_akhir[$xx] = trim($saldo_akhir[$xx]);

					$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
					if ($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id + 1;
					} else {
						$idbaru	= 1;
					}

					$tt_stok_opname_hasil_jahit_detail_warna	= array(
						'id' => $idbaru,
						'id_stok_opname_hasil_jahit_detail' => $iddetail,
						'id_warna' => $id_warna[$xx],
						'jum_stok_opname' => $stok_fisik[$xx],
						'saldo_akhir' => $saldo_akhir[$xx]
					);
					$this->db->insert('tt_stok_opname_hasil_jahit_detail_warna', $tt_stok_opname_hasil_jahit_detail_warna);
				} // end for
			} else {
				// ambil id detail id_stok_opname_hasil_jahit_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail 
							where id_brg_wip = '$id_brg_wip' 
							AND id_stok_opname_hasil_jahit = '$id_stok' ");
				if ($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				} else
					$iddetail = 0;

				$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$qtytotalstokfisik', 
								saldo_akhir = '$qtytotalsaldoakhir'
								where id = '$iddetail' ");

				for ($xx = 0; $xx < count($id_warna); $xx++) {
					$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET jum_stok_opname = '" . $stok_fisik[$xx] . "',
								saldo_akhir = '" . $saldo_akhir[$xx] . "'
								WHERE id_stok_opname_hasil_jahit_detail='$iddetail' AND id_warna = '" . $id_warna[$xx] . "' ");
				}
				// ====================
			}
		} // END IF
	}

	// 20-10-2015
	function cek_sokosong($id_gudang)
	{
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit WHERE id_gudang='$id_gudang' ");
		if ($query3->num_rows() > 0) {
			return 'f';
		} else {
			return 't';
		}
	}

	// 01-02-2016
	function cek_sopertamakali($gudang)
	{
		$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit WHERE id_gudang = '$gudang' ");
		if ($query3->num_rows() > 1) {
			return 'f';
		} else {
			return 't';
		}
	}
}
