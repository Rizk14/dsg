<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, status_approve FROM tt_stok_opname_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
		}
		else {
			$status_approve	= '';
			$idnya = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya
							);
							
		return $so_bahan;
  }

  function get_all_transaksiwip($bulan, $tahun, $gudang) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
	  
	  // 1. query semua kode brg yg ada transaksi di periode yg diinginkan
	  // 2. query ke tabel master stok yg qty-nya > 0. Dan bandingkan dgn hasil query no. 1, 
	  //	jika datanya ga ada di query no. 1 maka munculkan kode brg tsb
	  $sql = "SELECT a.kode_brg_jadi, sum(a.masuk) as masuk, sum(a.keluar) as keluar FROM (
				select b.kode_brg_jadi, sum(b.qty) as masuk, 0 as keluar FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi, 0 as masuk, sum(b.qty) as keluar FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
				a GROUP BY a.kode_brg_jadi"; //echo $sql; die();
		
		$query	= $this->db->query($sql);
			
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				$saldo_akhir = $row->masuk-$row->keluar;
				$detail_bahan[] = array('kode_brg_jadi'=> $row->kode_brg_jadi,
										'masuk'=> $row->masuk,
										'keluar'=> $row->keluar,
										'saldo_akhir'=> $saldo_akhir,
										'stok_opname'=> $saldo_akhir
									);
			}
		}
		else
			$detail_bahan = '';
		
		$query3	= $this->db->query(" SELECT a.kode_brg_jadi, a.stok
					FROM tm_stok_hasil_jahit a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi AND a.id_gudang = '$gudang' AND a.kode_brg_jadi NOT IN (					
					SELECT a.kode_brg_jadi FROM (
						select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
						AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
						
						UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
						AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
						a GROUP BY a.kode_brg_jadi
					) ");
					
		if ($query3->num_rows() > 0){
			$hasil3 = $query3->result();
			foreach ($hasil3 as $row3) {
				//---------------------------------------

				//$a=array("a"=>"Dog","b"=>"Cat","c"=>"Horse");
				//echo array_search("Dog",$a);

				//if (is_array($detail_bahan)) {
					//if(!in_array($row3->kode_brg_jadi, $detail_bahan)) {
					//	$list_kode_brg_jadi.= $row3->kode_brg_jadi.";";
					//}
					/*$a=array("a"=>"Dog","b"=>"Cat","c"=>"Horse");
					echo array_search("Dog",$a); */
					
					
					/*foreach ($detail_bahan as $key => $val) {
						//echo $row3->kode_brg_jadi." ".$val['kode_brg_jadi']."<br>";
						
						//echo $val['kode_brg_jadi']."<br>";
				   // }
				//} */
			    //---------------------------------------
			    //echo $row3->kode_brg_jadi."<br>";
			    $detail_bahan[] = array('kode_brg_jadi'=> $row3->kode_brg_jadi,
										'masuk'=> 0,
										'keluar'=> 0,
										'saldo_akhir'=> $row3->stok,
										'stok_opname'=> $row3->stok
									);
			}
		}
		
		return $detail_bahan;
  }
  
  function get_all_stok_opname($bulan, $tahun, $gudang) {

		$query	= $this->db->query(" SELECT b.* FROM tt_stok_opname_hasil_jahit_detail b, tt_stok_opname_hasil_jahit a, 
					tr_product_motif d
					WHERE b.id_stok_opname_hasil_jahit = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang = '$gudang'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil nama brg dan stok terkini
				$query3	= $this->db->query(" SELECT a.kode_brg_jadi, a.stok, c.e_product_motifname
					FROM tm_stok_hasil_jahit a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi
					AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang' ");
					
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg_jadi	= '';
					$stok = '';
				}
				
				$detail_bahan[] = array( 'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $nama_brg_jadi,
										'saldo_akhir'=> $stok,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function save($is_new, $id_stok, $kode_brg_jadi, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d"); 
	  
	  if ($is_new == '1') {
		   $data_detail = array(
						'id_stok_opname_hasil_jahit'=>$id_stok,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_hasil_jahit_detail',$data_detail);
	  }
	  else {
		  $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$stok_fisik' 
						where kode_brg_jadi = '$kode_brg_jadi' 
						AND id_stok_opname_hasil_jahit = '$id_stok' ");
	  }
  } 

}
