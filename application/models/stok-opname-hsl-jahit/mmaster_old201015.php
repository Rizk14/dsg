<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_so($bulan, $tahun, $gudang) {
	$query3	= $this->db->query(" SELECT id, tgl_so, status_approve FROM tt_stok_opname_hasil_jahit
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND id_gudang = '$gudang' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$status_approve	= $hasilrow->status_approve;
			$idnya = $hasilrow->id;
			$tgl_so = $hasilrow->tgl_so;
		}
		else {
			$status_approve	= '';
			$idnya = '';
			$tgl_so = '';
		}
		
		$so_bahan = array('status_approve'=> $status_approve,
							   'idnya'=> $idnya,
							   'tgl_so'=> $tgl_so
							);
							
		return $so_bahan;
  }

  function get_all_transaksiwip($bulan, $tahun, $gudang) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
	  
	  // 1. query semua kode brg yg ada transaksi di periode yg diinginkan
	  // 2. query ke tabel master stok yg qty-nya > 0. Dan bandingkan dgn hasil query no. 1, 
	  //	jika datanya ga ada di query no. 1 maka munculkan kode brg tsb
	 
	 // 06-02-2014, dikomen utk backup, karena skrg semuanya per warna
	/*  $sql = "SELECT a.kode_brg_jadi, sum(a.masuk) as masuk, sum(a.keluar) as keluar FROM (
				select b.kode_brg_jadi, sum(b.qty) as masuk, 0 as keluar FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi, 0 as masuk, sum(b.qty) as keluar FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
				a GROUP BY a.kode_brg_jadi"; */
	
	// ini per warna. 
	// 08-02-2014 koreksi: ternyata ga perlu pake query ini utk munculin stok di gudang. langsung aja pake tm_stok_hasil_jahit
	/*	$sql = "SELECT a.kode_brg_jadi FROM (
				select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
				a GROUP BY a.kode_brg_jadi"; */
		
		$sql = " SELECT id, kode_brg_jadi FROM tm_stok_hasil_jahit WHERE id_gudang = '$gudang' ORDER BY kode_brg_jadi ";
		
		$query	= $this->db->query($sql);
			
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			$detail_bahan = array();
			$detail_warna = array();
			foreach ($hasil as $row) {
				//$saldo_akhir = $row->masuk-$row->keluar;
				
				// 06-02-2014, query utk ambil detail warna. jika ga ada maka ambil default warna dari warna_brg_jadi
			/*	$sqlxx = "SELECT xx.kode_brg_jadi, xx.id_warna_brg_jadi, sum(xx.masuk) as masuk, sum(xx.keluar) as keluar FROM (
				select b.kode_brg_jadi, c.id_warna_brg_jadi, sum(c.qty) as masuk, 0 as keluar FROM tm_sjmasukwip a, tm_sjmasukwip_detail b,
				tm_sjmasukwip_detail_warna c  
				WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' 
				GROUP BY b.kode_brg_jadi, c.id_warna_brg_jadi
				
				UNION SELECT b.kode_brg_jadi, c.id_warna_brg_jadi, 0 as masuk, sum(c.qty) as keluar FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b,
				tm_sjkeluarwip_detail_warna c 
				WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' 
				GROUP BY b.kode_brg_jadi, c.id_warna_brg_jadi)
				xx WHERE xx.kode_brg_jadi = '$row->kode_brg_jadi' GROUP BY xx.kode_brg_jadi, xx.id_warna_brg_jadi"; */
				
				$sqlxx = " SELECT c.stok, b.kode, b.nama as nama_warna FROM 
						tm_warna b, tm_stok_hasil_jahit_warna c
						WHERE c.kode_warna = b.kode AND c.id_stok_hasil_jahit = '$row->id' ";
				
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					
					foreach ($hasilxx as $rowxx) {
						//$saldo_akhir_warna = $rowxx->masuk-$rowxx->keluar;
						
						// ambil nama warna
						/*$queryxx1	= $this->db->query(" SELECT a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
												WHERE a.kode_warna = b.kode AND a.id = '$rowxx->id_warna_brg_jadi' ");
						if($queryxx1->num_rows() > 0) {
							$hasilxx1 = $queryxx1->row();
							$kode_warna	= $hasilxx1->kode_warna;
							$nama_warna	= $hasilxx1->nama;
						}
						$detail_warna[] = array('id_warna_brg_jadi'=> $rowxx->id_warna_brg_jadi,
										'kode_warna'=> $kode_warna,
										'nama_warna'=> $nama_warna,
										'masuk'=> $rowxx->masuk,
										'keluar'=> $rowxx->keluar,
										'saldo_akhir'=> $saldo_akhir_warna,
										'stok_opname'=> $saldo_akhir_warna
									); */
						$detail_warna[] = array(
										'kode_warna'=> $rowxx->kode,
										'nama_warna'=> $rowxx->nama_warna,
										'saldo_akhir'=> $rowxx->stok,
										'stok_opname'=> $rowxx->stok);
					}
				}
				else {
					$sqlxx = " SELECT b.kode, b.nama FROM tm_warna_brg_jadi a, tm_warna b 
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$row->kode_brg_jadi' ";
					
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						
						foreach ($hasilxx as $rowxx) {
							$detail_warna[] = array(
										'kode_warna'=> $rowxx->kode,
										'nama_warna'=> $rowxx->nama,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0
									);
						}
					}
					else
						$detail_warna = '';
				}
				
				$detail_bahan[] = array('kode_brg_jadi'=> $row->kode_brg_jadi,
									//	'masuk'=> $row->masuk,
									//	'keluar'=> $row->keluar,
									//	'saldo_akhir'=> $saldo_akhir,
									//	'stok_opname'=> $saldo_akhir,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		}
		else
			$detail_bahan = '';
		
		// 08-02-2013, ini ga dipake. nanti dipakenya di laporan mutasi stok
	/*	$query3	= $this->db->query(" SELECT a.id, a.kode_brg_jadi
					FROM tm_stok_hasil_jahit a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi AND a.id_gudang = '$gudang' AND a.kode_brg_jadi NOT IN (					
					SELECT a.kode_brg_jadi FROM (
						select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
						AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
						
						UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
						AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
						a GROUP BY a.kode_brg_jadi
					) "); 
					
		if ($query3->num_rows() > 0){
			$hasil3 = $query3->result();
			foreach ($hasil3 as $row3) { */
				//---------------------------------------

				//$a=array("a"=>"Dog","b"=>"Cat","c"=>"Horse");
				//echo array_search("Dog",$a);

				//if (is_array($detail_bahan)) {
					//if(!in_array($row3->kode_brg_jadi, $detail_bahan)) {
					//	$list_kode_brg_jadi.= $row3->kode_brg_jadi.";";
					//}
					/*$a=array("a"=>"Dog","b"=>"Cat","c"=>"Horse");
					echo array_search("Dog",$a); */
					
					
					/*foreach ($detail_bahan as $key => $val) {
						//echo $row3->kode_brg_jadi." ".$val['kode_brg_jadi']."<br>";
						
						//echo $val['kode_brg_jadi']."<br>";
				   // }
				//} */
			    //---------------------------------------
			    //echo $row3->kode_brg_jadi."<br>";
			    
			    // 06-02-2014, ambil data stok terkini per warna
			/*	$queryxx1	= $this->db->query(" SELECT a.id, a.kode_warna, b.nama, c.stok FROM tm_warna_brg_jadi a, tm_warna b, 
										tm_stok_hasil_jahit_warna c
										WHERE c.id_warna_brg_jadi = a.id AND a.kode_warna = b.kode 
										AND c.id_stok_hasil_jahit = '$row3->id' ");
				if($queryxx1->num_rows() > 0) {
					$hasilxx1 = $queryxx1->result();
					foreach ($hasilxx1 as $rowxx1) {
						$detail_warna[] = array('id_warna_brg_jadi'=> $rowxx1->id,
										'kode_warna'=> $rowxx1->kode_warna,
										'nama_warna'=> $rowxx1->nama_warna,
										'masuk'=> 0,
										'keluar'=> 0,
										'saldo_akhir'=> $rowxx1->stok,
										'stok_opname'=> $rowxx1->stok
									);
					}
				}
				else {
					$sqlxx1 = " SELECT a.id, b.kode, b.nama FROM tm_warna_brg_jadi a, tm_warna b 
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$row3->kode_brg_jadi' ";
					
					$queryxx1	= $this->db->query($sqlxx1);
					if ($queryxx1->num_rows() > 0){
						$hasilxx1 = $queryxx1->result();
						
						foreach ($hasilxx1 as $rowxx1) {
							$detail_warna[] = array('id_warna_brg_jadi'=> $rowxx1->id,
										'kode_warna'=> $rowxx1->kode,
										'nama_warna'=> $rowxx1->nama,
										'masuk'=> 0,
										'keluar'=> 0,
										'saldo_akhir'=> 0,
										'stok_opname'=> 0
									);
						}
					}
					else
						$detail_warna = '';
				}
			    
			    $detail_bahan[] = array('kode_brg_jadi'=> $row3->kode_brg_jadi,
										//'masuk'=> 0,
										//'keluar'=> 0,
										//'saldo_akhir'=> $row3->stok,
										//'stok_opname'=> $row3->stok,
										'detail_warna'=> $detail_warna
									);
				$detail_warna = array();
			}
		} */
		
		return $detail_bahan;
  }
  
  function get_all_stok_opname($bulan, $tahun, $gudang) {

		$query	= $this->db->query(" SELECT b.*, d.e_product_motifname as nama_brg_jadi FROM tt_stok_opname_hasil_jahit_detail b, 
					tt_stok_opname_hasil_jahit a, tr_product_motif d
					WHERE b.id_stok_opname_hasil_jahit = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.bulan = '$bulan' AND a.tahun = '$tahun' AND a.id_gudang = '$gudang'
					AND a.status_approve = 'f' AND b.status_approve = 'f' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil nama brg dan stok terkini
				/*$query3	= $this->db->query(" SELECT a.kode_brg_jadi, a.stok, c.e_product_motifname
					FROM tm_stok_hasil_jahit a, tr_product_motif c
					WHERE c.i_product_motif = a.kode_brg_jadi
					AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang' ");
					
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
					$stok	= $hasilrow->stok;
				}
				else {
					$nama_brg_jadi	= '';
					$stok = '';
				} */
				
				/*
				 * $sqlxx1 = " SELECT a.id, b.kode, b.nama FROM tm_warna_brg_jadi a, tm_warna b 
							WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '$row3->kode_brg_jadi' ";
				 */
				
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_hasil_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_hasil_jahit_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a, tm_stok_hasil_jahit_warna b
							WHERE a.id = b.id_stok_hasil_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$gudang'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									//'masuk'=> 0,
									//'keluar'=> 0,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname
								);
					}
				}
				else
					$detail_warna = '';
				
				$detail_bahan[] = array( 'kode_brg_jadi'=> $row->kode_brg_jadi,
										//'nama_brg_jadi'=> $row->nama_brg_jadi,
										'detail_warna'=> $detail_warna
										//'saldo_akhir'=> $stok,
										//'stok_opname'=> $row->jum_stok_opname
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function save($is_new, $id_stok, $kode_brg_jadi, $kode_warna, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  // 06-02-2014
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
	// ---------------------------------------------------------------------
	  
	  if ($is_new == '1') {
		   $data_detail = array(
						'id_stok_opname_hasil_jahit'=>$id_stok,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'stok_awal'=>$qtytotalstokawal,
						'jum_stok_opname'=>$qtytotalstokfisik
					);
		   $this->db->insert('tt_stok_opname_hasil_jahit_detail',$data_detail);
		   
			// ambil id detail id_stok_opname_hasil_jahit_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
			
			// ----------------------------------------------
			for ($xx=0; $xx<count($kode_warna); $xx++) {
				$kode_warna[$xx] = trim($kode_warna[$xx]);
				$stok[$xx] = trim($stok[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);
				
				$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
				if($seq_warna->num_rows() > 0) {
					$seqrow	= $seq_warna->row();
					$idbaru	= $seqrow->id+1;
				}else{
					$idbaru	= 1;
				}
				
				$tt_stok_opname_hasil_jahit_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_stok_opname_hasil_jahit_detail'=>$iddetail,
						 'id_warna_brg_jadi'=>'0',
						 'kode_warna'=>$kode_warna[$xx],
						 'jum_stok_opname'=>$stok_fisik[$xx]
					);
					$this->db->insert('tt_stok_opname_hasil_jahit_detail_warna',$tt_stok_opname_hasil_jahit_detail_warna);
			} // end for
	  }
	  else {
		  // ambil id detail id_stok_opname_hasil_jahit_detail
			$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail 
						where kode_brg_jadi = '$kode_brg_jadi' 
						AND id_stok_opname_hasil_jahit = '$id_stok' ");
			if($seq_detail->num_rows() > 0) {
				$seqrow	= $seq_detail->row();
				$iddetail = $seqrow->id;
			}
			else
				$iddetail = 0;
		  
		  $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$qtytotalstokfisik' 
						where id = '$iddetail' ");
			
			for ($xx=0; $xx<count($kode_warna); $xx++) {
				$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."'
							WHERE id_stok_opname_hasil_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ");
			}
			// ====================
	  }
  } 

}
