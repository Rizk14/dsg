<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $supplier, $cari) { 
	if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" * FROM tm_faktur_makloon ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_faktur_makloon
							WHERE kode_unit = '$supplier' ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" * FROM tm_faktur_makloon where kode_unit = '$supplier' AND 
							(UPPER(no_faktur) like UPPER('%$cari%') ) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_faktur_makloon where 
							(UPPER(no_faktur) like UPPER('%$cari%')) ORDER BY id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {

				$query3	= $this->db->query(" SELECT b.kode_supplier, b.nama FROM tm_supplier b, tm_faktur_makloon a 
				WHERE a.kode_unit = b.kode_supplier AND a.id = '$row1->id' ");
				$hasilrow3 = $query3->row();
				$kode_supplier	= $hasilrow3->kode_supplier;
				$nama_supplier	= $hasilrow3->nama;
				
				// ambil detail data no & tgl SJ
				$query2	= $this->db->query(" SELECT a.no_sj_masuk FROM tm_faktur_makloon_sj a, tm_faktur_makloon b 
							WHERE a.id_faktur_makloon = b.id AND
							b.no_faktur = '$row1->no_faktur' AND b.kode_unit = '$kode_supplier' ");
				if ($query2->num_rows() > 0) { //
					$hasil2 = $query2->result();
					foreach ($hasil2 as $row2) {
						$no_sj	= $row2->no_sj_masuk;
						
						if ($row1->jenis_makloon == '1') {
							$sql = "SELECT tgl_sj from tm_sj_hasil_makloon where no_sj = '$no_sj' 
									AND kode_unit= '$kode_supplier'";
							$query3	= $this->db->query($sql);
						//	if ($query3->num_rows() > 0) {
								$hasilrow3 = $query3->row();
								$tgl_sj	= $hasilrow3->tgl_sj;
						}
						else if ($row1->jenis_makloon == '2') { // bisbisan
							$sql = "SELECT tgl_sj from tm_sj_hasil_bisbisan where no_sj = '$no_sj' 
									AND kode_unit= '$kode_supplier'";
							$query3	= $this->db->query($sql);
						//	if ($query3->num_rows() > 0) {
								$hasilrow3 = $query3->row();
								$tgl_sj	= $hasilrow3->tgl_sj;
						}
						else if ($row1->jenis_makloon == '3') { // bordir
							$sql = "SELECT tgl_sj from tm_sj_hasil_bordir where no_sj = '$no_sj' 
									AND kode_unit= '$kode_supplier'";
							$query3	= $this->db->query($sql);
						//	if ($query3->num_rows() > 0) {
								$hasilrow3 = $query3->row();
								$tgl_sj	= $hasilrow3->tgl_sj;
						}
						else if ($row1->jenis_makloon == '4') { // print
							$sql = "SELECT tgl_sj from tm_sj_hasil_print where no_sj = '$no_sj' 
									AND kode_unit= '$kode_supplier'";
							$query3	= $this->db->query($sql);
						//	if ($query3->num_rows() > 0) {
								$hasilrow3 = $query3->row();
								$tgl_sj	= $hasilrow3->tgl_sj;
						}
						else if ($row1->jenis_makloon == '5') {
							$sql = "SELECT tgl_sj from tm_sj_hasil_asesoris where no_sj = '$no_sj' 
									AND kode_unit= '$kode_supplier'";
							$query3	= $this->db->query($sql);
						//	if ($query3->num_rows() > 0) {
								$hasilrow3 = $query3->row();
								$tgl_sj	= $hasilrow3->tgl_sj;
						}
							
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_sj = $tgl1." ".$nama_bln." ".$thn1;
					//	} 

						$detail_fb[] = array('no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj
											);		
					}

				}
				else {
					$detail_fb = '';
				}

				$pisah1 = explode("-", $row1->tgl_faktur);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_faktur = $tgl1." ".$nama_bln." ".$thn1;
						
					$pisah1 = explode("-", $row1->tgl_input);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_input = $tgl1." ".$nama_bln." ".$thn1;
						
						$pisah1 = explode("-", $row1->tgl_update);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						if ($bln1 == '01')
							$nama_bln = "Januari";
						else if ($bln1 == '02')
							$nama_bln = "Februari";
						else if ($bln1 == '03')
							$nama_bln = "Maret";
						else if ($bln1 == '04')
							$nama_bln = "April";
						else if ($bln1 == '05')
							$nama_bln = "Mei";
						else if ($bln1 == '06')
							$nama_bln = "Juni";
						else if ($bln1 == '07')
							$nama_bln = "Juli";
						else if ($bln1 == '08')
							$nama_bln = "Agustus";
						else if ($bln1 == '09')
							$nama_bln = "September";
						else if ($bln1 == '10')
							$nama_bln = "Oktober";
						else if ($bln1 == '11')
							$nama_bln = "November";
						else if ($bln1 == '12')
							$nama_bln = "Desember";
						$tgl_update = $tgl1." ".$nama_bln." ".$thn1;
						
						$sql = "SELECT nama FROM tm_jenis_makloon where id = '$row1->jenis_makloon' ";
							$query3	= $this->db->query($sql);
								$hasilrow3 = $query3->row();
								$nama_makloon	= $hasilrow3->nama;
						
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,	
											'tgl_faktur'=> $tgl_faktur,	
											'kode_supplier'=> $kode_supplier,	
											'nama_supplier'=> $nama_supplier,	
											'tgl_input'=> $tgl_input,
											'tgl_update'=> $tgl_update,
											'totalnya'=> $row1->jumlah,
											'detail_fb'=> $detail_fb,
											'status_faktur_pajak'=> $row1->status_faktur_pajak,
											'nama_makloon'=> $nama_makloon
											); 
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($supplier, $cari){
	if ($cari == "all") {
		if ($supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon
							WHERE kode_unit = '$supplier' ");
	}
	else { 
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon where kode_unit = '$supplier' AND 
							(UPPER(no_faktur) like UPPER('%$cari%') )  ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon where (UPPER(no_faktur) like UPPER('%$cari%'))  ");
	}
    
    return $query->result();  
  }
      
  function cek_data($no_fp, $unit_makloon, $jenis_makloon){
    $this->db->select("id from tm_faktur_makloon WHERE no_faktur = '$no_fp' AND kode_unit = '$unit_makloon' 
						AND jenis_makloon = '$jenis_makloon' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_fp, $tgl_fp, $unit_makloon, $jenis_makloon, $jum, $no_sj){  
    $tgl = date("Y-m-d");
	$list_sj = explode(",", $no_sj); 

	$data_header = array(
			  'no_faktur'=>$no_fp,
			  'tgl_faktur'=>$tgl_fp,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'kode_unit'=>$unit_makloon,
			  'jenis_makloon'=>$jenis_makloon,
			  'jumlah'=>$jum
			);
	$this->db->insert('tm_faktur_makloon',$data_header);
	
	// ambil data terakhir di tabel tm_faktur_makloon
	$query2	= $this->db->query(" SELECT id FROM tm_faktur_makloon ORDER BY id DESC LIMIT 1 ");
	$hasilrow = $query2->row();
	$id_pf	= $hasilrow->id;
	
	// insert tabel detail sj-nya
	
	foreach($list_sj as $row1) {
		$row1 = trim($row1);
		if ($row1 != '') {
			$data_detail = array(
			  'id_faktur_makloon'=>$id_pf,
			  'no_sj_masuk'=>$row1
			);
			$this->db->insert('tm_faktur_makloon_sj',$data_detail);
			
			// update status_faktur sesuai jenis makloon
			if ($jenis_makloon == 1)
				$nama_tabel = "tm_sj_hasil_makloon";
			else if ($jenis_makloon == 2)
				$nama_tabel = "tm_sj_hasil_bisbisan";
			else if ($jenis_makloon == 3)
				$nama_tabel = "tm_sj_hasil_bordir";
			else if ($jenis_makloon == 4)
				$nama_tabel = "tm_sj_hasil_print";
			else if ($jenis_makloon == 5)
				$nama_tabel = "tm_sj_hasil_asesoris";
			
			$this->db->query(" UPDATE ".$nama_tabel." SET no_faktur = '$no_fp', status_faktur = 't' 
								WHERE no_sj = '$row1' AND kode_unit = '$unit_makloon' ");
		}
	}
   
  }
    
  function delete($kode){    	
	//semua no_sj di semua tabel sj hasil makloon yg bersesuaian dgn tm_faktur_makloon dikembalikan lagi statusnya menjadi FALSE
	//---------------------------------------------
	
	/*$query	= $this->db->query(" SELECT a.kode_supplier, b.no_sj FROM tm_pembelian_nofaktur a, tm_pembelian_nofaktur_sj b
							WHERE a.id = b.id_pembelian_nofaktur AND a.id = '$kode' "); */
	
	$query	= $this->db->query(" SELECT a.kode_unit, b.no_sj_masuk, a.jenis_makloon FROM tm_faktur_makloon a, tm_faktur_makloon_sj b
							WHERE a.id = b.id_faktur_makloon AND a.id = '$kode' ");
	
	if ($query->num_rows() > 0) {
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			if ($row1->jenis_makloon == 1)
				$nama_tabel = "tm_sj_hasil_makloon";
			else if ($row1->jenis_makloon == 2)
				$nama_tabel = "tm_sj_hasil_bisbisan";
			else if ($row1->jenis_makloon == 3)
				$nama_tabel = "tm_sj_hasil_bordir";
			else if ($row1->jenis_makloon == 4)
				$nama_tabel = "tm_sj_hasil_print";
			else if ($row1->jenis_makloon == 5)
				$nama_tabel = "tm_sj_hasil_asesoris";
				
			$this->db->query(" UPDATE ".$nama_tabel." SET status_faktur = 'f', no_faktur = '' 
							WHERE kode_unit = '$row1->kode_unit' AND no_sj = '$row1->no_sj_masuk' ");
		}
	}
	//---------------------------------------------
	
	// dan hapus datanya di pembelian_nofaktur dan pembelian_nofaktur_sj
	$this->db->query(" DELETE FROM tm_faktur_makloon_sj where id_faktur_makloon = '$kode' ");
	$this->db->query(" DELETE FROM tm_faktur_makloon where id = '$kode' ");

  }
    
  //function get_pembelian($num, $offset, $jnsaction, $supplier, $cari) {
	function get_sjmasukmakloon($jnsaction, $jnsmakloon, $supplier, $no_fakturnya, $cari) {
	  // ambil data sj masuk makloon 
	if ($cari == "all") {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' 
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' 
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' 
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' 
							order by a.id DESC ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' 
							order by a.id DESC ");
			}
			else { // utk proses edit
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' OR no_faktur <> '' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
			/*	$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' 
				OR no_faktur = '$no_fakturnya' order by kode_supplier, tgl_sj DESC "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya'
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya'
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya'
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya'
							order by a.id DESC ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya'
							order by a.id DESC ");
			}
		} // end if supplier == 0
		else { // ini utk supplier !=0
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				/*$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' 
						AND kode_supplier = '$supplier' 
						order by kode_supplier, tgl_sj DESC "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier'
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier'
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier'
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier'
							order by a.id DESC ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier'
							order by a.id DESC ");
				
			}
			else {
				//$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
			/*	$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE ( status_faktur = 'f' OR no_faktur = '$no_fakturnya' ) 
							AND kode_supplier = '$supplier' 
							order by kode_supplier, tgl_sj DESC "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier'
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier'
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier'
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier'
							order by a.id DESC ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier'
							order by a.id DESC "); //
				
			}
		}
	} // end if cari == 'all'
	else {
		if ($supplier == '0') {		
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' AND 
				//(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				//$query = $this->db->get();
				
				/*$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' 
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				/*$query	= $this->db->query(" select * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by kode_supplier, tgl_sj DESC "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
			}
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$this->db->select(" * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
				/*$query	= $this->db->query(" select * FROM tm_pembelian WHERE status_faktur = 'f' 
				AND kode_supplier = '$supplier' 
				AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by kode_supplier, tgl_sj DESC "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
			}
			else {
				/*$this->db->select(" * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur <> '') AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) order by kode_supplier, tgl_sj DESC ", false)->limit($num,$offset);
				$query = $this->db->get(); */
				
			/*	$query	= $this->db->query(" select * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
				AND kode_supplier = '$supplier' AND 
				(UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) 
				order by kode_supplier, tgl_sj DESC "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
			}
		}
	}
		$data_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				
				$query2	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_unit' ");
				$hasilrow = $query2->row();
				$nama_unit	= $hasilrow->nama;
				
				$data_fb[] = array(			'id'=> $row1->id,	
											'kode_unit'=> $row1->kode_unit,
											'nama_unit'=> $nama_unit,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'total'=> $row1->total
											);
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function get_sjmasukmakloontanpalimit($jnsaction, $jnsmakloon, $supplier, $no_fakturnya, $cari){
	if ($cari == "all") {
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				//$query = $this->db->getwhere('tm_pembelian',array('status_faktur'=>'f' ));
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' ");
				
			}
			else {
				/*$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f'
									OR no_faktur = '$no_fakturnya' "); */
									
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya' ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya' ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya' ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya' ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya' ");
				
			}			
		}
		else {
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$query = $this->db->getwhere('tm_pembelian',array('status_faktur'=>'f',
																'kode_supplier'=>$supplier)); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier' ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier' ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier' ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f'  AND a.kode_unit = '$supplier' ");
			}
			else {
				/*$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE (status_faktur = 'f' 
									OR no_faktur = '$no_fakturnya') AND kode_supplier = '$supplier' "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier' ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier' ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier' ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier' ");
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') AND a.kode_unit = '$supplier' "); //
			}
		}
	}
	else { 
		if ($supplier == '0') {
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' 
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f' AND
							a.status_faktur = 'f' AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
			}
			else {
			/*	$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya')
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR a.no_faktur = '$no_fakturnya') 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%'))
							order by a.id DESC ");
			}
		}
		else { 
			if ($jnsaction == 'A') { // utk view data pada proses add
				/*$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE status_faktur = 'f' AND kode_supplier = '$supplier' 
								AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) "); */
				
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND a.status_faktur = 'f' AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
			}
			else {
				/*$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE (status_faktur = 'f' OR no_faktur = '$no_fakturnya') 
									AND kode_supplier = '$supplier' 
									AND (UPPER(no_faktur) like UPPER('%$cari%') OR UPPER(no_sj) like UPPER('%$cari%')) "); */
									
				if ($jnsmakloon == 1)
					$query	= $this->db->query(" SELECT a.* FROM tm_sj_hasil_makloon a, tm_sj_proses_quilting b 
							WHERE a.id_sj_proses_quilting = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 2)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bisbisan a, tm_sj_proses_bisbisan b 
							WHERE a.id_sj_proses_bisbisan = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 3)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_bordir a, tm_sj_proses_bordir b 
							WHERE a.id_sj_proses_bordir = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
				
				else if ($jnsmakloon == 4)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_print a, tm_sj_proses_print b 
							WHERE a.id_sj_proses_print = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
							
				else if ($jnsmakloon == 5)
					$query	= $this->db->query(" SELECT * FROM tm_sj_hasil_asesoris a, tm_sj_proses_asesoris b 
							WHERE a.id_sj_proses_asesoris = b.id AND b.makloon_internal = 'f'
							AND (a.status_faktur = 'f' OR no_faktur = '$no_fakturnya')
							AND a.kode_unit = '$supplier' 
							AND (UPPER(a.no_faktur) like UPPER('%$cari%') OR UPPER(a.no_sj) like UPPER('%$cari%')) ");
			}
		}
	}
    
    return $query->result();  
  }
  
  function get_detail_op($id_op, $jenis_brg, $list_brg){
    //$query = $this->db->getwhere('tm_pp_detail',array('id_pp'=>$id_pp));
    //$hasil = $query->result();
    $detail_pp = array();
        
    foreach($list_brg as $row1) {
			//$row1 = intval($row1);
			if ($row1 != '') {
			$query2	= $this->db->query(" SELECT kode_brg, qty FROM tm_op_detail WHERE id = '$row1' ");
			$hasilrow = $query2->row();
			$kode_brg	= $hasilrow->kode_brg;
			$qty	= $hasilrow->qty;
    
    //foreach ($hasil as $row1) {
		/*	$query2	= $this->db->query(" SELECT nama_brg, harga FROM tm_bahan_baku WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query2->row();
			$nama_brg	= $hasilrow->nama_brg;
			$harga	= $hasilrow->harga; */
			
			if ($jenis_brg == '1')
				$nama_tabel = "tm_bahan_baku";
			else if ($jenis_brg == '2')
				$nama_tabel = "tm_asesoris";
			else if ($jenis_brg == '3')
				$nama_tabel = "tm_bahan_pendukung";
			else if ($jenis_brg == '4')
				$nama_tabel = "tm_perlengkapan";
			
			$query2	= $this->db->query(" SELECT nama_brg, harga FROM ".$nama_tabel." WHERE kode_brg = '$kode_brg' ");
			$hasilrow = $query2->row();
			$nama_brg	= $hasilrow->nama_brg;
			$harga	= $hasilrow->harga;
		
		/*	$detail_pp[] = array('kode_brg'=> $row1->kode_brg,
											'nama'=> $nama_brg,
											'harga'=> $harga,
											'qty'=> $row1->qty,
											'keterangan'=> $row1->keterangan
										); */
			$detail_op[] = array(		'id'=> $row1,
										'kode_brg'=> $kode_brg,
											'nama'=> $nama_brg,
											'harga'=> $harga,
											'qty'=> $qty
								);
		}
	}
	return $detail_op;
  }
      
  function get_faktur($id_faktur){
	$query	= $this->db->query(" SELECT * FROM tm_faktur_makloon where id='$id_faktur' ");    
	
	$data_faktur = array();
	$detail_sj = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $this->db->query(" SELECT no_sj_masuk FROM tm_faktur_makloon_sj 
									WHERE id_faktur_makloon = '$row1->id' ");
				$hasil2 = $query2->result();
				$no_sj = '';
				foreach ($hasil2 as $row2) {
					$no_sj .= $row2->no_sj_masuk.", ";
					
				}
				
				$pisah1 = explode("-", $row1->tgl_faktur);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_faktur = $tgl1."-".$bln1."-".$thn1;						
			
				$data_faktur[] = array(		'id'=> $row1->id,	
											'no_faktur'=> $row1->no_faktur,
											'tgl_faktur'=> $tgl_faktur,
											'kode_unit'=> $row1->kode_unit,
											'jenis_makloon'=> $row1->jenis_makloon,
											'tgl_update'=> $row1->tgl_update,
											'jumlah'=> $row1->jumlah,
											'no_sj'=> $no_sj
											);
			} // endforeach header
	}
    return $data_faktur; // sampe sini 10 maret 2011  
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_jenis_makloon(){
	$query	= $this->db->query(" SELECT * FROM tm_jenis_makloon ORDER BY id ");    
    return $query->result();  
  }  

}
