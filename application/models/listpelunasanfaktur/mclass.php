<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function totalvoucher($ivoucher) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.v_total_voucher AS pelunasan FROM tm_voucher_faktur a
				INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher
						
				WHERE a.f_voucher_cancel='f' AND a.i_voucher='$ivoucher'
						
				GROUP BY a.v_total_voucher ");
	}
	
	function clistvoucher1($limit,$offset,$ivoucher,$ndvoucherfirst,$ndvoucherlast) {
	$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_recieved, a.e_approved, a.v_total_voucher AS pelunasan, a.e_description 
		
		FROM tm_voucher_faktur a 
		
			INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher 
			
			INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
			
			WHERE a.f_voucher_cancel='f' AND (a.d_voucher BETWEEN '$ndvoucherfirst' AND '$ndvoucherlast') 
		
			GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_recieved, a.e_approved, a.v_total_voucher, a.e_description ORDER BY a.i_voucher ASC LIMIT ".$limit." OFFSET ".$offset." ");
							
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}			
	}

	function clistvoucher2($limit,$offset,$ivoucher,$ndvoucherfirst,$ndvoucherlast) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_voucher, d.i_voucher_code, a.i_voucher_no, a.d_voucher, b.v_voucher, c.i_dt, c.i_dt_code, c.d_dt, b.f_nota_sederhana
		
			FROM tm_voucher_faktur a
			
				INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher
			
				INNER JOIN tm_dt_faktur c ON c.i_dt=b.i_faktur
				
				INNER JOIN tr_kode_voucher d ON d.i_voucher=a.i_voucher_code
					
				WHERE a.f_voucher_cancel='f' AND a.i_voucher='$ivoucher'
						
				GROUP BY a.i_voucher, d.i_voucher_code, a.i_voucher_no, a.d_voucher, b.v_voucher, c.i_dt, c.i_dt_code, c.d_dt, b.f_nota_sederhana
						
				ORDER BY a.i_voucher ASC LIMIT ".$limit." OFFSET ".$offset." ");
													
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}			
	}
				
	function clistvoucherallpage1($ivoucher,$ndvoucherfirst,$ndvoucherlast) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_recieved, a.e_approved, a.v_total_voucher AS pelunasan, a.e_description 
			
			FROM tm_voucher_faktur a 
			
				INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher 
				
				INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
				
				WHERE a.f_voucher_cancel='f' AND (a.d_voucher BETWEEN '$ndvoucherfirst' AND '$ndvoucherlast') 
			
				GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher, a.e_recieved, a.e_approved, a.v_total_voucher, a.e_description ORDER BY a.i_voucher ASC ");
	}

	function clistvoucherallpage2($ivoucher,$ndvoucherfirst,$ndvoucherlast) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, d.i_voucher_code, a.i_voucher_no, a.d_voucher, b.v_voucher, c.i_dt, c.i_dt_code, c.d_dt, b.f_nota_sederhana
			
			FROM tm_voucher_faktur a
			
				INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher
			
				INNER JOIN tm_dt_faktur c ON c.i_dt=b.i_faktur
				
				INNER JOIN tr_kode_voucher d ON d.i_voucher=a.i_voucher_code
						
				WHERE a.f_voucher_cancel='f' AND a.i_voucher='$ivoucher'
						
				GROUP BY a.i_voucher, d.i_voucher_code, a.i_voucher_no, a.d_voucher, b.v_voucher, c.i_dt, c.i_dt_code, c.d_dt, b.f_nota_sederhana
						
				ORDER BY a.i_voucher ASC ");
	}
	
	function lvoucher() {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher 
		
			FROM tm_voucher_faktur a
			
			INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher
			INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
			
			WHERE a.f_voucher_cancel='f'
			
			GROUP BY  a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher ORDER BY a.i_voucher ASC ");
	}

	function lvoucherperpages($limit,$offset) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher 
		
			FROM tm_voucher_faktur a
		
			INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher
			INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
			
			WHERE a.f_voucher_cancel='f'
		
			GROUP BY  a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher ORDER BY a.i_voucher ASC LIMIT ".$limit." OFFSET ".$offset." ");
			
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flvoucher($key) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher 
		
			FROM tm_voucher_faktur a 
			
			INNER JOIN tm_voucher_faktur_item b ON b.i_voucher=a.i_voucher 
			INNER JOIN tr_kode_voucher c ON c.i_voucher=a.i_voucher_code
			
			WHERE a.f_voucher_cancel='f' AND a.i_voucher_code='$key'
			
			GROUP BY a.i_voucher, c.i_voucher_code, a.i_voucher_no, a.d_voucher ORDER BY a.i_voucher ASC ");
	}
	 
}

?>
