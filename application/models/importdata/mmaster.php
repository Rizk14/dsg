<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}

	// =========================== 09-06-2015 ==============================
  function proses_excel_bhnbaku($uploadfile_excel) {
	$this->load->library("PHPExcel");
	$this->load->library('PHPExcel/IOFactory');
	//$objReader = PHPExcel_IOFactory::createReader('Excel5'); // ini ga jalan kalo pake PHPExcel didepannya. Begitu juga "Cell" yg ada di skrip dibawah ini
	$objReader = IOFactory::createReader('Excel5');
	$objReader->setReadDataOnly(true);					//just read cell data
	$objPHPExcel = $objReader->load($uploadfile_excel);	//load filenya
	$objWorksheet = $objPHPExcel->getActiveSheet();		//objectworksheet dari sheet yang active
 
    $num_row = $objWorksheet->getHighestRow();			// e.g. 10
	$num_col = $objWorksheet->getHighestColumn(); 		// e.g 'F'
	
	$highestColumnIndex = Cell::columnIndexFromString($num_col); // e.g. 5
	
	//echo "error teu"; die();
	
	//conversion column excel ynag ini indexnya mulai dari 1 BEDAKAN BUNG..!!
	$columns = array(1=>"A",2=>"B",3=>"C",4=>"D",5=>"E",6=>"F",7=>"G",8=>"H",9=>"I",10=>"J",11=>"K",12=>"L",13=>"M",14=>"N",15=>"O",16=>"P",17=>"Q");
	
	$komponen = array();
		//ambil data mhs mulai dari row kelima - nama komponen.
		for($x=2; $x<=$num_row; $x++)
		{
			for($y=1; $y<=$highestColumnIndex; $y++) // $highestColumnIndex
			
			{
				$excelColumn = $columns[$y]; // convert jdi huruf
				$excelCell	 = $excelColumn.$x; // cell excel ex. B1
				$cell = $objWorksheet->getCell("$excelCell")->getValue();
				
				// koreksi 03-09-2015, bukan x>2 tapi x>=2 karena dimulai dari baris ke-2
				if (($x>=2) && ($y==1))
					$cell_namabrg = trim($cell);
				if (($x>=2) && ($y==2))
					$cell_idsatuan = trim($cell);
				if (($x>=2) && ($y==3))
					$cell_kodekelbrg = trim($cell);
				if (($x>=2) && ($y==4))
					$cell_idjenisbrg = trim($cell);
				if (($x>=2) && ($y==5))
					$cell_idgudang = trim($cell);
				if (($x>=2) && ($y==6))
					$cell_idsatuankonversi = trim($cell);
				if (($x>=2) && ($y==7))
					$cell_rumuskonversi = trim($cell);
				
				if (($x>=2) && ($y==8)) {
					$cell_angkafaktorkonversi = trim($cell);
					if ($cell_angkafaktorkonversi == '')
						$cell_angkafaktorkonversi = 0;
					
					if ($cell_rumuskonversi == '')
						$cell_rumuskonversi = 0;
					if ($cell_idsatuankonversi == '')
						$cell_idsatuankonversi = 0;
					
					// ambil kode jenis barang berdasarkan ID jenis barang, dan cek no urut tertingginya brp
					$sql = " SELECT kode FROM tm_jenis_barang WHERE id='$cell_idjenisbrg' ";
					$query = $this->db->query($sql);
							
					if ($query->num_rows() > 0){
						$hasil = $query->row();
						$kode_jenis = $hasil->kode;
					
						$queryxx = $this->db->query(" SELECT kode_brg FROM tm_barang
													WHERE kode_brg LIKE '".$kode_jenis."%' ORDER BY kode_brg DESC LIMIT 1 ");
								
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$kode_brg_last = $hasilxx->kode_brg;
							$digitbaru = (substr($kode_brg_last, 3, 4))+1; // 4 digit no urut
							
							switch(strlen($digitbaru)) {
								case "1": $newdigit	= "000".$digitbaru;
								break;
								case "2": $newdigit	= "00".$digitbaru;
								break;	
								case "3": $newdigit	= "0".$digitbaru;
								break;
								case "4": $newdigit	= $digitbaru;
								break;	
							}
							$kode_brg_baru = $kode_jenis.$newdigit;
						}
						else
							$kode_brg_baru = $kode_jenis.'0001';
						
						// insert ke master brg dgn jenis brg baru
						
						// ambil id tertinggi
						$sql2 = " SELECT id FROM tm_barang ORDER BY id DESC LIMIT 1 ";
						$query2	= $this->db->query($sql2);
						if ($query2->num_rows() > 0){
							$hasil2 = $query2->row();
							$idlama	= $hasil2->id;
							$idbaru = $idlama+1;
						}
						else
							$idbaru = 1;
							
						$tgl = date("Y-m-d H:i:s");
						$data = array(
							  'id'=>$idbaru,
							  'kode_brg'=>$kode_brg_baru,
							  'nama_brg'=>$cell_namabrg,
							  //'nama_brg_supplier'=>$nama,
							  'satuan'=>$cell_idsatuan,
							  'id_jenis_barang'=>$cell_idjenisbrg,
							  //'deskripsi'=>$deskripsi,
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl,
							  'id_gudang'=>$cell_idgudang,
							  'status_aktif'=>'t',
							  'id_satuan_konversi'=>$cell_idsatuankonversi,
							  'rumus_konversi'=>$cell_rumuskonversi,
							  'angka_faktor_konversi'=>$cell_angkafaktorkonversi
							);
						$this->db->insert('tm_barang',$data); 
					}
				} // end if y==8
				
			}
		}
	return true;
  }
  // =====================================================================
  
  // 29-06-2015
  function proses_excel_supplier($uploadfile_excel) {
	$this->load->library("PHPExcel");
	$this->load->library('PHPExcel/IOFactory');
	//$objReader = PHPExcel_IOFactory::createReader('Excel5'); // ini ga jalan kalo pake PHPExcel didepannya. Begitu juga "Cell" yg ada di skrip dibawah ini
	$objReader = IOFactory::createReader('Excel5');
	$objReader->setReadDataOnly(true);					//just read cell data
	$objPHPExcel = $objReader->load($uploadfile_excel);	//load filenya
	$objWorksheet = $objPHPExcel->getActiveSheet();		//objectworksheet dari sheet yang active
 
    $num_row = $objWorksheet->getHighestRow();			// e.g. 10
	$num_col = $objWorksheet->getHighestColumn(); 		// e.g 'F'
	
	$highestColumnIndex = Cell::columnIndexFromString($num_col); // e.g. 5
	
	//conversion column excel ynag ini indexnya mulai dari 1 BEDAKAN BUNG..!!
	$columns = array(1=>"A",2=>"B",3=>"C",4=>"D",5=>"E",6=>"F",7=>"G",8=>"H",9=>"I",10=>"J",11=>"K",12=>"L",13=>"M",14=>"N",15=>"O",16=>"P",17=>"Q");
	
	$komponen = array();
		//ambil data mhs mulai dari row kelima - nama komponen.
		for($x=2; $x<=$num_row; $x++)
		{
			for($y=1; $y<=$highestColumnIndex; $y++) // $highestColumnIndex
			
			{
				$excelColumn = $columns[$y]; // convert jdi huruf
				$excelCell	 = $excelColumn.$x; // cell excel ex. B1
				$cell = $objWorksheet->getCell("$excelCell")->getValue();
				
				if (($x>=2) && ($y==1))
					$cell_hurufawal = trim($cell);
				if (($x>=2) && ($y==2))
					$cell_namasupplier = trim($cell);
				if (($x>=2) && ($y==3))
					$cell_alamat = trim($cell);
				if (($x>=2) && ($y==4))
					$cell_kota = trim($cell);
				if (($x>=2) && ($y==5))
					$cell_kontakperson = trim($cell);
				if (($x>=2) && ($y==6))
					$cell_telepon = trim($cell);
				if (($x>=2) && ($y==7))
					$cell_faksimile = trim($cell);
				if (($x>=2) && ($y==8))
					$cell_top = trim($cell);
				if (($x>=2) && ($y==9))
					$cell_pkp = trim($cell);
				if (($x>=2) && ($y==10))
					$cell_npwp = trim($cell);
				if (($x>=2) && ($y==11))
					$cell_namanpwp = trim($cell);
				if (($x>=2) && ($y==12))
					$cell_tipepajak = trim($cell);
				if (($x>=2) && ($y==13)) {
					$cell_katsupplier = trim($cell);
					if ($cell_tipepajak == '')
						$cell_tipepajak = 'I';
						
					$queryxx = $this->db->query(" SELECT kode_supplier FROM tm_supplier
													WHERE kode_supplier LIKE 'S".$cell_hurufawal."%' ORDER BY kode_supplier DESC LIMIT 1 ");
								
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$kode_supplier_last = $hasilxx->kode_supplier;
							$digitbaru = (substr($kode_supplier_last, 2, 3))+1; // 3 digit no urut
							
							switch(strlen($digitbaru)) {
								case "1": $newdigit	= "00".$digitbaru;
								break;
								case "2": $newdigit	= "0".$digitbaru;
								break;	
								case "3": $newdigit	= $digitbaru;
								break;	
							}
							$kode_supplier_baru = 'S'.$cell_hurufawal.$newdigit;
						}
						else
							$kode_supplier_baru = 'S'.$cell_hurufawal.'001';
						
						// insert ke master supplier
						
						// ambil id tertinggi
						$sql2 = " SELECT id FROM tm_supplier ORDER BY id DESC LIMIT 1 ";
						$query2	= $this->db->query($sql2);
						if ($query2->num_rows() > 0){
							$hasil2 = $query2->row();
							$idlama	= $hasil2->id;
							$idbaru = $idlama+1;
						}
						else
							$idbaru = 1;
							
						$tgl = date("Y-m-d H:i:s");
						$data = array(
								  'id'=>$idbaru,
								  'kode_supplier'=>$kode_supplier_baru,
								  'nama'=>$cell_namasupplier,
								  'alamat'=>$cell_alamat,
								  'kota'=>$cell_kota,
								  'kontak_person'=>$cell_kontakperson,
								  'telp'=>$cell_telepon,
								  'fax'=>$cell_faksimile,
								  'top'=>$cell_top,
								  'pkp'=>$cell_pkp,
								  'npwp'=>$cell_npwp,
								  'nama_npwp'=>$cell_namanpwp,
								  'tipe_pajak'=>$cell_tipepajak,
								  'kategori'=>$cell_katsupplier,
								  'tgl_input'=>$tgl,
								  'tgl_update'=>$tgl
								);
						$this->db->insert('tm_supplier',$data); 
					
				} // end if y==13
				
			}
		}
	return true;
  }
  
  function proses_excel_hargasupplier($uploadfile_excel) {
	$this->load->library("PHPExcel");
	$this->load->library('PHPExcel/IOFactory');
	//$objReader = PHPExcel_IOFactory::createReader('Excel5'); // ini ga jalan kalo pake PHPExcel didepannya. Begitu juga "Cell" yg ada di skrip dibawah ini
	$objReader = IOFactory::createReader('Excel5');
	$objReader->setReadDataOnly(true);					//just read cell data
	$objPHPExcel = $objReader->load($uploadfile_excel);	//load filenya
	$objWorksheet = $objPHPExcel->getActiveSheet();		//objectworksheet dari sheet yang active
 
    $num_row = $objWorksheet->getHighestRow();			// e.g. 10
	$num_col = $objWorksheet->getHighestColumn(); 		// e.g 'F'
	
	$highestColumnIndex = Cell::columnIndexFromString($num_col); // e.g. 5
	
	//conversion column excel ynag ini indexnya mulai dari 1 BEDAKAN BUNG..!!
	$columns = array(1=>"A",2=>"B",3=>"C",4=>"D",5=>"E",6=>"F",7=>"G",8=>"H",9=>"I",10=>"J",11=>"K",12=>"L",13=>"M",14=>"N",15=>"O",16=>"P",17=>"Q");
	
	$komponen = array();
		//ambil data mhs mulai dari row kelima - nama komponen.
		for($x=2; $x<=$num_row; $x++)
		{
			for($y=1; $y<=$highestColumnIndex; $y++) // $highestColumnIndex
			
			{
				$excelColumn = $columns[$y]; // convert jdi huruf
				$excelCell	 = $excelColumn.$x; // cell excel ex. B1
				$cell = $objWorksheet->getCell("$excelCell")->getValue();
				
				// koreksi 03-09-2015, bukan x>2 tapi x>=2 karena dimulai dari baris ke-2
				if (($x>=2) && ($y==1))
					$cell_supplier = trim($cell);
				if (($x>=2) && ($y==2))
					$cell_barang = trim($cell);
				if (($x>=2) && ($y==3)) {
					$cell_harga = trim($cell);
						
					$queryxx = $this->db->query(" SELECT id FROM tm_supplier WHERE nama = '$cell_supplier' ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$id_supplier = $hasilxx->id;
					}
					else
						$id_supplier = 0;
										
					$pos = strpos($cell_barang, "\"");
					if ($pos > 0)
						$cell_barang = str_replace("\"", "&quot;", $cell_barang);
					else
						$cell_barang = str_replace("'", "\'", $cell_barang);
					
					$queryxx = $this->db->query(" SELECT id FROM tm_barang WHERE nama_brg = '$cell_barang' ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$id_brg = $hasilxx->id;
					}
					else
						$id_brg = 0;
						
						// insert ke master harga
						if ($id_brg != 0 && $id_supplier != 0 && ($cell_harga != '' || $cell_harga != '0')) {
							$tgl = date("Y-m-d H:i:s");
							$data = array(
							  'id_brg'=>$id_brg,
							  'id_supplier'=>$id_supplier,
							  'harga'=>$cell_harga,
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl
							);
							$this->db->insert('tm_harga_brg_supplier',$data); 
							
							$this->db->query(" INSERT INTO tt_harga (id_brg, id_supplier, harga, tgl_input) 
												VALUES ('$id_brg','$id_supplier', '$cell_harga', '$tgl') ");
						}
					
				} // end if y==3
				
			}
		}
	return true;
  }
  
  // 28-09-2015
  function proses_excel_trxpembelian($uploadfile_excel) {
	  $this->db->query(" truncate tm_temp_transaksi_pembelian restart identity cascade ");
	  
	$this->load->library("PHPExcel");
	$this->load->library('PHPExcel/IOFactory');
	//$objReader = PHPExcel_IOFactory::createReader('Excel5'); // ini ga jalan kalo pake PHPExcel didepannya. Begitu juga "Cell" yg ada di skrip dibawah ini
	$objReader = IOFactory::createReader('Excel5');
	$objReader->setReadDataOnly(true);					//just read cell data
	$objPHPExcel = $objReader->load($uploadfile_excel);	//load filenya
	$objWorksheet = $objPHPExcel->getActiveSheet();		//objectworksheet dari sheet yang active
 
    $num_row = $objWorksheet->getHighestRow();			// e.g. 10
	$num_col = $objWorksheet->getHighestColumn(); 		// e.g 'F'
	
	$highestColumnIndex = Cell::columnIndexFromString($num_col); // e.g. 5
	
	//conversion column excel ynag ini indexnya mulai dari 1 BEDAKAN BUNG..!!
	$columns = array(1=>"A",2=>"B",3=>"C",4=>"D",5=>"E",6=>"F",7=>"G",8=>"H",9=>"I",10=>"J",11=>"K",12=>"L",13=>"M",14=>"N",15=>"O",16=>"P",17=>"Q",18=>"R",19=>"S",20=>"T",21=>"U",22=>"V",23=>"W",24=>"X",25=>"Y",26=>"Z");
	
	$komponen = array();
	
	// 1. BACA DATA EXCEL DARI ARI
		for($x=2; $x<=$num_row; $x++)
		{
			for($y=1; $y<=$highestColumnIndex; $y++) // $highestColumnIndex
			
			{
				$excelColumn = $columns[$y]; // convert jdi huruf
				$excelCell	 = $excelColumn.$x; // cell excel ex. B1
				$cell = $objWorksheet->getCell("$excelCell")->getValue();
				
				if (($x>=2) && ($y==1))
					$cell_tgl_sj = trim($cell);
				if (($x>=2) && ($y==2))
					$cell_no_sj = trim($cell);
				if (($x>=2) && ($y==3))
					$cell_nama_supplier = trim($cell);
				if (($x>=2) && ($y==4))
					$cell_nama_brg = trim($cell);
				if (($x>=2) && ($y==5))
					$cell_no_perk = trim($cell);
				if (($x>=2) && ($y==6))
					$cell_harga = trim($cell);
				if (($x>=2) && ($y==7))
					$cell_qty = trim($cell);
				if (($x>=2) && ($y==8))
					$cell_satuan = trim($cell);
				if (($x>=2) && ($y==9))
					$cell_nilai = trim($cell);
				if (($x>=2) && ($y==10)) {
					//$cell_tot_hutang_dagang = trim($cell);
					$cell_tot_hutang_dagang = $objWorksheet->getCell("$excelCell")->getCalculatedValue();
				}
				if (($x>=2) && ($y==11)) {
					//$cell_ppn = trim($cell);
					$cell_ppn = $objWorksheet->getCell("$excelCell")->getCalculatedValue();
				}
				if (($x>=2) && ($y==12))
					$cell_bhn_baku = trim($cell);
				if (($x>=2) && ($y==13))
					$cell_bhn_pembantu = trim($cell);
				if (($x>=2) && ($y==14))
					$cell_no_faktur_pajak = trim($cell);
				if (($x>=2) && ($y==15))
					$cell_tgl_faktur_pajak = trim($cell);
				if (($x>=2) && ($y==22)) {
					$cell_tgl_faktur_pajak2 = $objWorksheet->getCell("$excelCell")->getCalculatedValue();
					
					if (trim($cell_ppn) == '')
						$cell_ppn = 0;
					
					if ($cell_ppn != 0)	{
						$dpp = $cell_tot_hutang_dagang/1.1;
						$dpp = round($dpp, 0);
					}
					else
						$dpp = $cell_tot_hutang_dagang;
						
						//$tgl = date("Y-m-d H:i:s");
					// insert ke tm_temp_transaksi_pembelian
					if ($cell_tot_hutang_dagang != '') {
						$tgl_sj = date('Y-m-d',strtotime('1899-12-31+'.($cell_tgl_sj-1).' days'));
						
						// 29-09-2015, cek kode supplier dari tabel tm_temp_supplier. trus dapatkan TOP utk hitung due_date
						$sqlxx = " SELECT a.top, b.kode_supplier FROM tm_supplier a INNER JOIN tm_temp_supplier b ON a.kode_supplier = b.kode_supplier
								WHERE b.nama_supplier = '".$cell_nama_supplier."' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$top	= $hasilxx->top;
							$kode_supplier = $hasilxx->kode_supplier;
							
							$date = $tgl_sj;
							$date1 = str_replace('-', '/', $date);
							$due_date = date('Y-m-d',strtotime($date1 . "+".$top." days"));
							//$due_date = date('Y-m-d', strtotime("+".$top." days"));
						}
						else {
							$kode_supplier = '';
							$due_date = $tgl_sj;
						}
						
						if (trim($cell_tgl_faktur_pajak) == '')
							$tgl_faktur_pajak = NULL;
						else {
							if ($tgl_sj >= '2015-04-01') {
								//echo $cell_tgl_faktur_pajak2."<br>";
								//$tgl_faktur_pajak = date('Y-m-d',strtotime('1899-12-31+'.($cell_tgl_faktur_pajak2-1).' days'));
								$tgl_faktur_pajak = $cell_tgl_faktur_pajak2;
							}
							else {
								$tgl_faktur_pajak = date('Y-m-d',strtotime('1899-12-31+'.($cell_tgl_faktur_pajak-1).' days'));
							}
						}
						
						$data = array(
								  'company'=>'DUTA',
								  'ou_code'=>'00',
								  'doc_no'=>$cell_no_sj,
								  'doc_date'=>$tgl_sj,
								  'supplier_doc_no'=>$cell_no_sj,
								  'supplier_doc_date'=>$tgl_sj,
								  'main_acc'=>'111',
								  'sub_acc'=>'500',
								  'gov_tax_amount'=>$cell_ppn,
								  'curr_code'=>'IDR',
								  'nett_amount'=>$dpp,
								  'supplier_code'=>$kode_supplier,
								  'due_date'=>$due_date,
								  'tax_no'=>$cell_no_faktur_pajak,
								  'tax_date'=>$tgl_faktur_pajak,
								  'nama_supplier'=>$cell_nama_supplier
								);
						$this->db->insert('tm_temp_transaksi_pembelian',$data); 
					}
					
				} // end if y==22
				
			}
		}
		//die();
	// 2. BACA DATA DARI tm_pembelian utk data 1 juli 2015 s/d 31 agustus 2015
	$sql2 = " SELECT a.id, a.id_supplier, a.no_sj, a.tgl_sj, a.total, a.dpp, a.total_pajak, 
				b.kode_supplier, b.nama, b.top FROM tm_pembelian a
				INNER JOIN tm_supplier b ON a.id_supplier = b.id WHERE a.tgl_sj >='2015-07-01' AND a.tgl_sj <= '2015-08-31'
				AND a.status_aktif = 't' ORDER BY a.tgl_sj ";
	$query2	= $this->db->query($sql2);
	
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->result();
		
		foreach ($hasil2 as $row2) {
			// ambil no faktur pajak dan tgl faktur pajak
			$sql3 = " SELECT a.no_faktur_pajak, a.tgl_faktur_pajak FROM tm_pembelian_pajak a 
					INNER JOIN tm_pembelian_pajak_detail b ON a.id = b.id_pembelian_pajak
					INNER JOIN tm_pembelian_nofaktur c ON c.id = b.id_faktur_pembelian
					INNER JOIN tm_pembelian_nofaktur_sj d ON c.id = d.id_pembelian_nofaktur
					WHERE d.id_sj_pembelian = '$row2->id' ";
			$query3	= $this->db->query($sql3);
	
			if ($query3->num_rows() > 0){
				$hasil3 = $query3->row();
				$no_faktur_pajak	= $hasil3->no_faktur_pajak;
				$tgl_faktur_pajak	= $hasil3->tgl_faktur_pajak;
			}
			else {
				$no_faktur_pajak = '';
				$tgl_faktur_pajak = NULL;
			}
			
			// ambil due date dari top
			$date = $row2->tgl_sj;
			$date1 = str_replace('-', '/', $date);
			$due_date = date('Y-m-d',strtotime($date1 . "+".$top." days"));
			
			// cek apakah datanya udah ada di tabel tm_temp_transaksi_pembelian
			$sqlxx = " SELECT id FROM tm_temp_transaksi_pembelian WHERE doc_no = '$row2->no_sj' 
					AND supplier_code = '$row2->kode_supplier' ";
			$queryxx	= $this->db->query($sqlxx);
			if ($queryxx->num_rows() == 0){
				$data = array(
								  'company'=>'DUTA',
								  'ou_code'=>'00',
								  'doc_no'=>$row2->no_sj,
								  'doc_date'=>$row2->tgl_sj,
								  'supplier_doc_no'=>$row2->no_sj,
								  'supplier_doc_date'=>$row2->tgl_sj,
								  'main_acc'=>'111',
								  'sub_acc'=>'500',
								  'gov_tax_amount'=>$row2->total_pajak,
								  'curr_code'=>'IDR',
								  'nett_amount'=>$row2->dpp,
								  'supplier_code'=>$row2->kode_supplier,
								  'due_date'=>$due_date,
								  'tax_no'=>$no_faktur_pajak,
								  'tax_date'=>$tgl_faktur_pajak,
								  'nama_supplier'=>$row2->nama
								);
						$this->db->insert('tm_temp_transaksi_pembelian',$data); 
			}
			
		}
	}
		
	
	return true;
  }
  
  // 29-09-2015
  function proses_excel_trxsupplier($uploadfile_excel) {
	  $this->db->query(" truncate tm_temp_supplier restart identity cascade ");
	  
	$this->load->library("PHPExcel");
	$this->load->library('PHPExcel/IOFactory');
	//$objReader = PHPExcel_IOFactory::createReader('Excel5'); // ini ga jalan kalo pake PHPExcel didepannya. Begitu juga "Cell" yg ada di skrip dibawah ini
	$objReader = IOFactory::createReader('Excel5');
	$objReader->setReadDataOnly(true);					//just read cell data
	$objPHPExcel = $objReader->load($uploadfile_excel);	//load filenya
	$objWorksheet = $objPHPExcel->getActiveSheet();		//objectworksheet dari sheet yang active
 
    $num_row = $objWorksheet->getHighestRow();			// e.g. 10
	$num_col = $objWorksheet->getHighestColumn(); 		// e.g 'F'
	
	$highestColumnIndex = Cell::columnIndexFromString($num_col); // e.g. 5
	
	//conversion column excel ynag ini indexnya mulai dari 1 BEDAKAN BUNG..!!
	$columns = array(1=>"A",2=>"B",3=>"C",4=>"D",5=>"E",6=>"F",7=>"G",8=>"H",9=>"I",10=>"J",11=>"K",12=>"L",13=>"M",14=>"N",15=>"O",16=>"P",17=>"Q",18=>"R",19=>"S",20=>"T",21=>"U",22=>"V",23=>"W",24=>"X",25=>"Y",26=>"Z");
	
	$komponen = array();
		for($x=2; $x<=$num_row; $x++)
		{
			for($y=1; $y<=$highestColumnIndex; $y++) // $highestColumnIndex
			
			{
				$excelColumn = $columns[$y]; // convert jdi huruf
				$excelCell	 = $excelColumn.$x; // cell excel ex. B1
				$cell = $objWorksheet->getCell("$excelCell")->getValue();
				
				if (($x>=2) && ($y==1))
					$cell_nama_supplier = trim($cell);
				if (($x>=2) && ($y==2)) {
					$cell_kode_supplier = trim($cell);
					
					// insert ke tm_temp_supplier
					// 29-09-2015, cek kode supplier dari tabel tm_temp_supplier. trus dapatkan TOP utk hitung due_date
						
					$data = array(
								  'kode_supplier'=>$cell_kode_supplier,
								  'nama_supplier'=>$cell_nama_supplier
								);
					$this->db->insert('tm_temp_supplier',$data); 
					
				} // end if y==2
				
			}
		}
	return true;
  }
  // -----------------------------
  
  function proses_import_trxpenjualan() {
	  $this->db->query(" truncate tm_temp_transaksi_penjualan restart identity cascade ");
	  
	$sql = " SELECT i_faktur_code, d_faktur, e_branch_name, d_due_date, v_discount, v_total_faktur, v_total_fppn, '0' as e_note_faktur 
			FROM tm_faktur_do_t WHERE d_faktur >= '2015-01-01' AND d_faktur <= '2015-08-31' AND f_faktur_cancel = 'f'
			UNION SELECT i_faktur_code, d_faktur, e_branch_name, d_due_date, v_discount, v_total_faktur, v_total_fppn, e_note_faktur 
			FROM tm_faktur WHERE d_faktur >= '2015-01-01' AND d_faktur <= '2015-08-31' AND f_faktur_cancel = 'f'
			UNION SELECT i_faktur_code, d_faktur, e_branch_name, d_due_date, v_discount, v_total_faktur, v_total_fppn, e_note_faktur 
			FROM tm_faktur_bhnbaku WHERE d_faktur >= '2015-01-01' AND d_faktur <= '2015-08-31' AND f_faktur_cancel = 'f'
			ORDER BY i_faktur_code ";
	
	$query	= $this->db->query($sql);
		
	if ($query->num_rows() > 0){
		$hasil = $query->result();
			
		foreach ($hasil as $row1) {
			$sql2 = " SELECT a.i_customer_code FROM tr_customer a INNER JOIN tr_branch b ON a.i_customer = b.i_customer
					 WHERE b.e_initial = '$row1->e_branch_name' ";
			$query2	= $this->db->query($sql2);
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->row();
				$i_customer_code = $hasil2->i_customer_code;
			}
			else
				$i_customer_code = '';
			
			// ambil no faktur pajak dari tm_nomor_pajak_faktur
			$sql2 = " SELECT nomor_pajak FROM tm_nomor_pajak_faktur WHERE i_faktur_code like '%".trim($row1->i_faktur_code)."%' ";
			$query2	= $this->db->query($sql2);
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->row();
				$nomor_pajak = $hasil2->nomor_pajak;
			}
			else
				$nomor_pajak = '';
			
			$ppn = $row1->v_total_fppn-$row1->v_total_faktur;
			$dpp = $row1->v_total_faktur;
			
			$data = array(
								  'company'=>'DUTA',
								  'ou_code'=>'00',
								  'doc_no'=>$row1->i_faktur_code,
								  'customer_code'=>$i_customer_code,
								  'doc_date'=>$row1->d_faktur,
								  'due_date'=>$row1->d_due_date,
								  'tax_no'=>$nomor_pajak,
								  'tax_date'=>$row1->d_faktur,
								  'main_acc'=>'112',
								  'sub_acc'=>'100',
								  'gov_tax_amount'=>$ppn,
								  'curr_code'=>'IDR',
								  'nett_amount'=>$dpp
								);
						$this->db->insert('tm_temp_transaksi_penjualan',$data); 
		}
	}
	
	// ========================================================================================================
	
	return true;
  }
  
  // 29-09-2015
  function get_temp_transaksi_penjualan(){
    $query = $this->db->query(" SELECT id, company, ou_code, doc_no, doc_date, customer_code, due_date, tax_no, tax_date, 
					main_acc, sub_acc, gov_tax_amount, curr_code, nett_amount, remark FROM tm_temp_transaksi_penjualan
					ORDER BY id ");
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_temp_transaksi_pembelian(){
    $query = $this->db->query(" SELECT id, company, ou_code, doc_no, doc_date, supplier_code, due_date, 
					supplier_doc_no, supplier_doc_date, tax_no, tax_date, 
					main_acc, sub_acc, gov_tax_amount, curr_code, nett_amount, remark FROM tm_temp_transaksi_pembelian
					ORDER BY id ");
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
    
}

