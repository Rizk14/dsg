<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang_jd(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
						WHERE a.jenis = '3' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_mutasi_stok_wip($date_from, $date_to, $gudang) {		
	  
	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	// explode date_from utk keperluan saldo awal 
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
			
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}
	  
	$sql =  "
			SELECT DISTINCT a.id_brg_wip, c.kode_brg from tm_sjmasukgudangjadi_detail a 
			inner join tm_sjmasukgudangjadi b on a.id_sjmasukgudangjadi=b.id
			inner join tm_barang_wip c on a.id_brg_wip=c.id
			WHERE c.status_aktif='t' AND b.tgl_sj >='".$tgldari."' 
			AND b.tgl_sj <='".$tglke."'
			
			UNION SELECT b.id_brg_wip, c.kode_brg FROM tm_sjkeluarwip a 
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				WHERE c.status_aktif='t' AND a.jenis_keluar='2'
				AND a.tgl_sj >='".$tgldari."' AND a.tgl_sj <='".$tglke."'
				
					ORDER BY kode_brg
				"; 
	//echo $sql; die();
	
		//AND a.status_approve='t'
		
		$query	= $this->db->query($sql);
		$jum_masuk = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}

				
				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
				$query3	= $this->db->query(" select sum(b.qty) as jum_masuk from tm_sjmasukgudangjadi a 
					inner join tm_sjmasukgudangjadi_detail b on a.id=b.id_sjmasukgudangjadi
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				// hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar2 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar2 = 0;
					
					$jum_saldo_masuk = $jum_masuk+ $jum_keluar2; 
				//=========================================
				
												
				$data_stok[] = array(		'kode_brg'=> $kode_brg_wip,
											'nama_brg'=> $nama_brg_wip,
											// per 08-04-2015 pake auto saldo akhir
											// 21-04-2015, waktu hari sabtu kata ginanjar pake SO lagi
											// 12-11-2015 duta utk gudang QC acuannya dari SO
											//'saldo_awal'=> $auto_saldo_awal,
											'jum_masuk'=> $jum_masuk,
											'jum_keluar2'=>$jum_keluar2
									
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  }

