<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($igiro,$ipv)
    {
		$this->db->select(" * from tm_giro_keluar a
							inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
							where a.i_giro='$igiro' and a.i_pv='$ipv'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($igiro,$isupplier,$ipv,$dgiro,$dpv,$dgiroduedate,$egirodescription,$egirobank,
				    $vjumlah,$vsisa)
    {
		
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
    			'i_giro' 			=> $igiro,
				'i_supplier' 		=> $isupplier,
				'i_pv' 				=> $ipv,
				'd_giro' 			=> $dgiro,
				'd_pv' 				=> $dpv,
				'd_giro_duedate' 	=> $dgiroduedate,
				'd_entry' 			=> $dentry,
				'e_giro_description'=> $egirodescription,
				'e_giro_bank' 		=> $egirobank,
				'v_jumlah' 			=> $vjumlah,
				'v_sisa' 			=> $vsisa
    		)
    	);
    	
    	$this->db->insert('tm_giro_keluar');
    }
    function update($igiro,$isupplier,$ipv,$dgiro,$dpv,$dgiroduedate,$dgirocair,$egirodescription,$egirobank,
				    $fgirotolak,$fgirocair,$vjumlah,$vsisa,$dgirotolak,$fgirobatal)
    {
		
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
    	$this->db->set(
    		array(
    			'i_giro' 			=> $igiro,
				'i_supplier' 		=> $isupplier,
				'i_pv' 				=> $ipv,
				'd_giro' 			=> $dgiro,
				'd_pv' 				=> $dpv,
				'd_giro_duedate' 	=> $dgiroduedate,
				'd_giro_cair' 		=> $dgirocair,
				'd_giro_tolak' 		=> $dgirotolak,
				'd_update' 			=> $dupdate,
				'e_giro_description'=> $egirodescription,
				'e_giro_bank' 		=> $egirobank,
				'f_giro_tolak' 		=> $fgirotolak,
				'f_giro_cair' 		=> $fgirocair,
				'f_giro_batal' 		=> $fgirobatal,
				'v_jumlah' 			=> $vjumlah,
				'v_sisa' 			=> $vsisa
    		)
    	);
    	$this->db->where('i_giro',$igiro);
    	$this->db->where('i_pv',$ipv);
    	$this->db->update('tm_giro_keluar');
    }	
    function bacasemua()
    {
		$this->db->select(' * from tm_giro_keluar a order by a.i_giro', false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select(" * from tm_supplier order by kode_supplier",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
							order by i_supplier",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function runningnumberpv($pvth)
	{
		$this->db->select(" trim(to_char(count(i_pv)+1,'000000')) as no from tm_giro_keluar where to_char(d_pv,'yyyy')='$pvth'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
			  $pv=$row->no;
			}
			return $pv;
		}else{
			$pv='000001';
			return $pv;
		}
	}
}
?>
