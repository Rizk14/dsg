<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function clistsj($isj,$d_sj_first,$d_sj_last) {
	$db2=$this->load->database('db_external', TRUE);
		if(($d_sj_first!='' && $d_sj_last!='') && $isj!='') {
			$dsj	= " WHERE ( b.d_sj BETWEEN '$d_sj_first' AND '$d_sj_last' ) AND b.f_sj_cancel='f' ";
			$idsj	= " AND b.i_sj_code='$isj' ";
		} elseif(($d_sj_first!='' && $d_sj_last!='') && $isj=='') {
			$dsj	= " WHERE ( b.d_sj BETWEEN '$d_sj_first' AND '$d_sj_last' ) AND b.f_sj_cancel='f' ";
			$idsj	= " ";		
		} elseif(($d_sj_first=='' || $d_sj_last=='') && $isj!='') {
			$dsj	= " WHERE b.f_sj_cancel='f' ";
			$idsj	= " AND b.i_sj_code='$isj' ";
		} elseif(($d_sj_first!='' || $d_sj_last!='') && $isj!='') {
			$dsj	= " WHERE b.f_sj_cancel='f' ";
			$idsj	= " AND b.i_sj_code='$isj' ";		
		} else {
			$dsj	= " WHERE b.f_sj_cancel='f' ";
			$idsj	= " ";
		}
		
		$strq	= "
			SELECT  b.i_sj AS isj, b.i_sj_code AS isjcode, 
				b.d_sj AS dsj, 
				a.i_product AS iproduct, 
				a.e_product_name AS productname, 
				a.n_unit AS unit, 
				a.n_unit_akhir AS belumfaktur,
				a.v_unit_price AS hjp, 
				(a.n_unit * a.v_unit_price) AS harga,
				a.f_faktur_created AS itemfaktur,
				b.f_faktur_created AS sjfaktur 
			
			FROM tm_sj_item a 
			
			RIGHT JOIN tm_sj b ON trim(b.i_sj)=trim(a.i_sj) 
			
			".$dsj." ".$idsj." ORDER BY b.d_sj DESC	";
		
		//print $strq;
				
		$query	= $db2->query($strq);
		
		if($query->num_rows() > 0 )	{
			return $result	= $query->result();
		}
	}

	function lsj() {		
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  b.i_sj_code AS isjcode,
					b.d_sj AS dsj
					
				FROM tm_sj_item a
				
				INNER JOIN tm_sj b ON b.i_sj=a.i_sj
				
				WHERE b.f_sj_cancel='f' GROUP BY b.i_sj_code, b.d_sj ORDER BY b.d_sj DESC ");
	}

	function lsjperpages($limit,$offset){	
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT  b.i_sj_code AS isjcode,
					b.d_sj AS dsj
					
				FROM tm_sj_item a
				
				INNER JOIN tm_sj b ON b.i_sj=a.i_sj
				
				WHERE b.f_sj_cancel='f' GROUP BY b.i_sj_code, b.d_sj ORDER BY b.d_sj DESC LIMIT ".$limit." OFFSET ".$offset);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function flsj($key) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($key)) {
			return $db2->query("
			SELECT  b.i_sj_code AS isjcode,
					b.d_sj AS dsj
					
				FROM tm_sj_item a
				
				INNER JOIN tm_sj b ON b.i_sj=a.i_sj
				
				WHERE b.f_sj_cancel='f' AND b.i_sj_code='$key' GROUP BY b.i_sj_code, b.d_sj ORDER BY b.d_sj DESC ");
		}
	}		
	
	function getsj($isj) {
		$db2=$this->load->database('db_external', TRUE);
		$strisj	= " SELECT * FROM tm_sj WHERE i_sj='$isj' AND f_faktur_created='f' AND f_sj_cancel='f' ORDER BY i_sj DESC LIMIT 1 ";
		
		return $db2->query($strisj);		
	}
	
	function getsjitem($isj,$tahun,$bulan) {
		$db2=$this->load->database('db_external', TRUE);
		/* 22082011
		$qstr	= "
		SELECT  a.i_sj AS isj,
				b.d_sj AS dsj,
				a.i_product AS iproduct,
				a.e_product_name AS productname,
				a.n_unit AS unit,
				a.n_unit_akhir AS qtyakhir,
				a.v_product_price AS hjp,
				(a.n_unit * a.v_unit_price) AS harga
			
			FROM tm_sj_item a
			
			INNER JOIN tm_sj b ON trim(b.i_sj_code)=trim(a.i_sj)

			WHERE a.i_sj='$isj' AND b.f_sj_cancel='f' AND b.f_faktur_created=false ORDER BY a.i_product ASC ";
		*/
		
		$qstr	= " SELECT a.i_sj AS isj, 
				b.d_sj AS dsj,
				a.i_product AS iproduct,
				a.e_product_name AS productname,
				a.n_unit AS unit,
				a.n_unit_akhir AS qtyakhir,
				a.v_product_price AS hjp,
				a.v_unit_price AS harga,
				a.i_so AS iso,
				d.f_stop_produksi AS stp

				FROM tm_sj_item a
				
				INNER JOIN tm_sj b ON trim(b.i_sj)=trim(a.i_sj)
				INNER JOIN tm_stokopname_item c ON c.i_product=a.i_product
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so

				WHERE a.i_sj='$isj' AND b.f_sj_cancel='f' AND b.f_faktur_created='f' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' ORDER BY a.i_product ASC ";
				
		$query	= $db2->query($qstr);
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}	

	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function lcabang($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.i_branch AS codebranch, 
					a.i_branch_code AS ibranchcode,
				    a.i_customer AS codecustomer, 
					a.e_branch_name AS ebranchname,
				    a.e_branch_name AS branch 
					
					FROM tr_branch a 
					
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$filter." ".$order;
					
		$query	= $db2->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function mupdate($i_sjcode,$isj,$d_sj,$i_customer,$i_branch,$v_sj_total,$e_note,$i_product,$e_product_name,$v_product_price,$n_unit,$v_unit_price,$iterasi,$stp,$iso){		
		/*
		$db2->trans_begin();
		*/
		$db2=$this->load->database('db_external', TRUE);
		$i_bbk_item	= array();
		$qty_product_update	= array();
		$iso2	= array();
		$ins_temp_tmso	= array();
		$tmsj_item	= array();
		
		$qtyakhir	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
				
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$isoz	= $row_stokopname->i_so;
		} else {
			$isoz	= "";
		}

		if($isoz!="") {
			$j=0;
			$iproductmotif	= array();
			$jml		= array();
			$totalharga	= array();
			$qtyakhir	= array();
			
			//kembalikan ke saldo
			$qsj_item	= $db2->query(" SELECT * FROM tm_sj_item WHERE i_sj='$isj' ");
			if($qsj_item->num_rows()>0) {
				$result_sjitem	= $qsj_item->result();
				foreach($result_sjitem as $row_sjitem) {
					$iproductmotif[$j]	= $row_sjitem->i_product;
					$jml[$j]		= $row_sjitem->n_unit_akhir;
					$price	= $row_sjitem->v_unit_price / $jml[$j];
					
					$qstok_item	= $db2->query("SELECT * FROM tm_stokopname_item WHERE i_so='$row_sjitem->i_so' AND i_product='$iproductmotif[$j]' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;
					} else {
						$qtyakhir[$j]	= 0;						
					}
						
					$back_qty_akhir[$j]	= $qtyakhir[$j]+$jml[$j];
					
					$update_stokop_item[$j]	= array(
						'n_quantity_akhir'=>$back_qty_akhir[$j]
					);

					$db2->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$row_sjitem->i_so,'i_product'=>$iproductmotif[$j]));

					$j+=1;
				}
			}

			$db2->delete('tm_sj',array('i_sj'=>$isj));
			$db2->delete('tm_sj_item',array('i_sj'=>$isj));
		}

		$db2->set(
			array(
			 'i_sj'=>$isj,
			 'i_sj_code'=>$i_sjcode,
			 'i_customer'=>$i_customer,
			 'i_branch'=>$i_branch,
			 'd_sj'=>$d_sj,
			 'e_note'=>$e_note,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry ));
			 
		if($db2->insert('tm_sj')){
		
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++) {
				
				$seq_tm_sj_item	= $db2->query(" SELECT i_sj_item FROM tm_sj_item ORDER BY i_sj_item DESC LIMIT 1 ");
				
				if($seq_tm_sj_item->num_rows() > 0) {
					$seqrow	= $seq_tm_sj_item->row();
					$i_sj_item[$jumlah]	= $seqrow->i_sj_item+1;
				} else {
					$i_sj_item[$jumlah]	= 1;
				}
				
				$tmsj_item[$jumlah] = array(
					 'i_sj_item'=>$i_sj_item[$jumlah],
					 'i_sj'=>$isj,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'v_product_price'=>$v_product_price[$jumlah],
					 'n_unit'=>$n_unit[$jumlah],
					 'n_unit_akhir'=>$n_unit[$jumlah],
					 'v_unit_price'=>$v_unit_price[$jumlah],
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah] );

				if($db2->insert('tm_sj_item',$tmsj_item[$jumlah])) {

					$qstok_item	= $db2->query("SELECT * FROM tm_stokopname_item WHERE i_so='$iso[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;

						$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]-$n_unit[$jumlah];
						
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);

						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$iso[$jumlah],'i_product'=>$i_product[$jumlah]));

					}					
									
					/* 22082011
					$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.i_status_do='1' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
					
					if($get_tmso->num_rows() > 0 ) {
					
						$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						if($get_tmso2->num_rows() > 0 ) {
							$row_tmso2	= $get_tmso2->row();
							$iso2[$jumlah]	= $row_tmso2->iso+1;
						} else {
							$iso2[$jumlah]	= 1;
						}
						
						$row_tmso	= $get_tmso->row_array();
						
						$temp_iso[$jumlah]	= $row_tmso['i_so'];
						$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
						$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
						$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
						$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
						$temp_ddo[$jumlah]	= $row_tmso['d_do'];
						$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal']==""?0:$row_tmso['n_saldo_awal'];
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir']==""?0:$row_tmso['n_saldo_akhir'];
						$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm']==""?0:$row_tmso['n_inbonm'];
						$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm']==""?0:$row_tmso['n_outbonm'];
						$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm']==""?0:$row_tmso['n_bbm'];
						$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk']==""?0:$row_tmso['n_bbk'];
						$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_unit[$jumlah];
						$tambah_bbk[$jumlah]	= $temp_n_bbk[$jumlah] - $n_unit[$jumlah];
						 
						$ins_temp_tmso[$jumlah]	= array(
							'i_so'=>$iso2[$jumlah],
							'i_product'=>$temp_iproduct[$jumlah],
							'i_product_motif'=>$temp_imotif[$jumlah],
							'e_product_motifname'=>$temp_productname[$jumlah],
							'i_status_do'=>$temp_istatusdo[$jumlah],
							'd_do'=>$temp_ddo[$jumlah],
							'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
							'n_saldo_akhir'=>$saldo_akhir[$jumlah],
							'n_inbonm'=>$temp_n_inbonm[$jumlah],
							'n_outbonm'=>$temp_n_outbonm[$jumlah],
							'n_bbm'=>$temp_n_bbm[$jumlah],
							'n_bbk'=>$tambah_bbk[$jumlah],
							'd_entry'=>$dentry
						);

						$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						*/
						
					//} else {
						/* 22082011
						$get_tmso	= $db2->query( " SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						
						if($get_tmso->num_rows()>0) {
						
							$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
							if($get_tmso2->num_rows() > 0 ) {
								$row_tmso2	= $get_tmso2->row();
								$iso2[$jumlah]	= $row_tmso2->iso+1;
							} else {
								$iso2[$jumlah]	= 1;
							}
							
							$row_tmso	= $get_tmso->row_array();
							
							$temp_iso[$jumlah]	= $row_tmso['i_so'];
							$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
							$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
							$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
							$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
							$temp_ddo[$jumlah]	= $row_tmso['d_do'];
							$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal']==""?0:$row_tmso['n_saldo_awal'];
							$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir']==""?0:$row_tmso['n_saldo_akhir'];
							$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm']==""?0:$row_tmso['n_inbonm'];
							$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm']==""?0:$row_tmso['n_outbonm'];
							$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm']==""?0:$row_tmso['n_bbm'];
							$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk']==""?0:$row_tmso['n_bbk'];
							$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_unit[$jumlah];
							$tambah_bbk[$jumlah]	= $temp_n_bbk[$jumlah] - $n_unit[$jumlah];
							 
							$ins_temp_tmso[$jumlah]	= array(
								'i_so'=>$iso2[$jumlah],
								'i_product'=>$temp_iproduct[$jumlah],
								'i_product_motif'=>$temp_imotif[$jumlah],
								'e_product_motifname'=>$temp_productname[$jumlah],
								'i_status_do'=>'1',
								'd_do'=>$temp_ddo[$jumlah],
								'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
								'n_saldo_akhir'=>$saldo_akhir[$jumlah],
								'n_inbonm'=>$temp_n_inbonm[$jumlah],
								'n_outbonm'=>$temp_n_outbonm[$jumlah],
								'n_bbm'=>$temp_n_bbm[$jumlah],
								'n_bbk'=>$tambah_bbk[$jumlah],
								'd_entry'=>$dentry
							);
							$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						} else {					
							echo "tm_bbk & tm_bbk_item gagal diupdate!";
						} */
					//}

					/*
					if ($db2->trans_status()===FALSE) {
						$db2->trans_rollback();
					} else {
						$db2->trans_commit();
					}
					*/
				} else {
					echo "sj gagal diupdate!";
				}
			}
		} else {
			print "<script>alert(\"Maaf, Data SJ gagal diupdate!\");show(\"listsj/cform\",\"#content\");</script>";
		}

		$qsj	= $db2->query("SELECT * FROM tm_sj WHERE i_sj='$isj' ");
		if($qsj->num_rows()==0) {
			print "<script>alert(\"Maaf, Data SJ gagal diupdate!\");show(\"listsj/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Nomor SJ : '\"+$i_sjcode+\"' telah diupdate, terimakasih.\");show(\"listsj/cform\",\"#content\");</script>";
		}
	}
	
	function cari_sj($nsj) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_sj WHERE i_sj_code=trim('$nsj') ");
	}
	
	function mbatal($isj) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($isj)) {
			$qcariisj	= $db2->query(" SELECT * FROM tm_sj WHERE i_sj='$isj' AND f_sj_cancel='f' AND f_faktur_created='f' ORDER BY i_sj DESC LIMIT 1 ");
			if($qcariisj->num_rows()>0) {
				$row_sj	= $qcariisj->row();
				$isj	= $row_sj->i_sj;
				$isjcode= $row_sj->i_sj_code;
				
				$qsjitem	= $db2->query(" SELECT * FROM tm_sj_item WHERE i_sj='$isj' ");
				if($qsjitem->num_rows()>0){
					foreach($qsjitem->result() as $row1){
						
						$i_product	= $row1->i_product;
						$i_so = $row1->i_so;
						$n_unit = $row1->n_unit_akhir;
						if($n_unit=='')
							$n_unit = 0;
							
						$qstokopname = $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$i_so' AND i_product='$i_product' ");
						if($qstokopname->num_rows()>0){
							$rstokopname = $qstokopname->row();
							$qtyakhir = $rstokopname->n_quantity_akhir;
							if($qtyakhir=='')
								$qtyakhir = 0;
								
							$qtyawal = $qtyakhir+$n_unit;
							if($qtyawal=='')
								$qtyawal = 0;
								
							$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$qtyawal' WHERE i_so='$i_so' AND i_product='$i_product' ");
						}
					}
				}
				
				$tbl_sj	= array(
					'f_sj_cancel'=>'TRUE'
				);
				$db2->update('tm_sj',$tbl_sj,array('i_sj'=>$isj));
				
				print "<script>alert(\"Nomor SJ: '\"+$isjcode+\"' telah dibatalkan, terimakasih.\");show(\"listsj/cform\",\"#content\");</script>";
			} else {
				print "<script>alert(\"Maaf, SJ tdk dpt dibatalkan.\");show(\"listsj/cform\",\"#content\");</script>";
			}
		} else {
			print "<script>alert(\"Maaf, SJ tdk dpt dibatalkan.\");show(\"listsj/cform\",\"#content\");</script>";	
		}
	}

}
?>
