<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $id_jenis)
  {
	if ($cari == "all") {
		if ($id_jenis == '0') {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
						WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id
						order by a.id_jenis_makloon ASC, a.kode_brg ASC, a.tgl_update DESC ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
						WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id
						AND a.id_jenis_makloon = '$id_jenis'
						order by a.id_jenis_makloon ASC, a.kode_brg ASC, a.tgl_update DESC ", false)->limit($num,$offset);
		}
	}
	else {
		if ($id_jenis == '0') {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
						WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id
						AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))
						order by a.id_jenis_makloon ASC, a.kode_brg ASC, a.tgl_update DESC ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
						WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id
						AND a.id_jenis_makloon = '$id_jenis'
						AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%'))
						order by a.id_jenis_makloon ASC, a.kode_brg ASC, a.tgl_update DESC ", false)->limit($num,$offset);
		}
    }
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari, $id_jenis){
	if ($cari == "all") {
		if ($id_jenis == '0') {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
			WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
						WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id
						AND a.id_jenis_makloon = '$id_jenis' ", false);
		}
	}
	else {
		if ($id_jenis == '0') {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
						WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id
						AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_makloon, d.nama as nama_satuan 
			FROM tm_brg_hasil_makloon a, tm_jenis_makloon b, tm_satuan d 
						WHERE a.id_jenis_makloon = b.id AND a.satuan = d.id
						AND a.id_jenis_makloon = '$id_jenis'
						AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ", false);
		}
	}
    $query = $this->db->get();
    return $query->result();  
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_brg_hasil_makloon',array('kode_brg'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_brg_hasil_makloon WHERE id = '$id' ");
    return $query->result();
  }
  
  function get_jenis_makloon(){
    $this->db->select("* from tm_jenis_makloon order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_satuan(){
    $this->db->select("* from tm_satuan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
    
  //function cek_data($kode, $id_jenis_bhn){
	  function cek_data($kode){
    $this->db->select("kode_brg from tm_brg_hasil_makloon WHERE kode_brg = '$kode' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode,$nama, $id_jenis, $satuan, $deskripsi, $status_aktif, $gudang, $id_barang, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_brg_hasil_makloon ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
		
    $data = array(
      'id'=>$idbaru,
      'kode_brg'=>$kode,
      'nama_brg'=>$nama,
      'satuan'=>$satuan,
      'id_jenis_makloon'=>$id_jenis,
      'deskripsi'=>$deskripsi,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'id_gudang'=>$gudang,
      'status_aktif'=>'t'
    );

    if ($goedit == '') {
		$this->db->insert('tm_brg_hasil_makloon',$data); 
	}
	else {
		if ($status_aktif == '')
			$status_aktif = 'f';
			
		$data = array(
				  'kode_brg'=>$kode,
				  'nama_brg'=>$nama,
				  'satuan'=>$satuan,
				  'id_jenis_makloon'=>$id_jenis,
				  'deskripsi'=>$deskripsi,
				  'tgl_update'=>$tgl,
				  'id_gudang'=>$gudang,
				  'status_aktif'=>$status_aktif
				);
		
		$this->db->where('id',$id_barang);
		$this->db->update('tm_brg_hasil_makloon',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_brg_hasil_makloon', array('id' => $id));
  }
  
  function get_jenis_barang($num, $offset, $kel_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_barangtanpalimit($kel_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_jns_bhn(){
    $this->db->select("a.kode as kj_brg, a.nama as nj_brg, b.* from tm_jenis_barang a, tm_jenis_bahan b 
    WHERE a.id = b.id_jenis_barang order by a.kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bahan($num, $offset, $jns_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_bahantanpalimit($jns_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }

}
