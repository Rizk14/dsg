<?php
class Item_makloon_model extends MY_Model
{
	protected $_per_page = 10;
    protected $_tabel = 'tb_master_barang_wip';

    
     public function get_all_barang_wip($offset)
    {
		
    $this->db->select("* FROM  tb_master_barang_wip")->limit($this->_per_page,$offset);
    $select = $this->db->get();
          
     if($select->num_rows() > 0){
		 return $select->result();
		 }
    }
    
     public function get_all_barang_bb($offset)
    {
		
    $this->db->select(" a.*,b.kode_barang_wip,nama_barang_wip FROM tb_master_barang_bb a inner join tb_master_barang_wip b ON b.id=a.id_barang_wip  ")->limit($this->_per_page,$offset);
    $select = $this->db->get();
          
     if($select->num_rows() > 0){
		 return $select->result();
		 }
    }
    
     function paging_barang($tipe, $base_url, $uri_segment)
    {
        // Memanggil library pagination.
        $this->load->library('pagination');

        // Konfigurasi.
        $config = array(
            'base_url' => $base_url,
            'uri_segment' => $uri_segment,
            'per_page' => $this->_per_page,
            'use_page_numbers' => true,
            'num_links' => 4,
            'first_link' => '&#124;&lt; First',
            'last_link' => 'Last &gt;&#124;',
            'next_link' => 'Next &gt;',
            'prev_link' => '&lt; Prev',

            // Menyesuaikan untuk Twitter Bootstrap 3.2.0.
            //~ 'full_tag_open' => '<ul class="pagination pagination-sm">',
            //~ 'full_tag_close' => '</ul>',
            //~ 'num_tag_open' => '<li>',
            //~ 'num_tag_close' => '</li>',
            //~ 'cur_tag_open' => '<li class="disabled"><li class="active"><a href="">',
            //~ 'cur_tag_close' => '<span class="sr-only"></span></a></li>',
            //~ 'next_tag_open' => '<li>',
            //~ 'next_tagl_close' => '</li>',
            //~ 'prev_tag_open' => '<li>',
            //~ 'prev_tagl_close' => '</li>',
            //~ 'first_tag_open' => '<li>',
            //~ 'first_tagl_close' => '</li>',
            //~ 'last_tag_open' => '<li>',
            //~ 'last_tagl_close' => '</li>',
        );

        // Jika paging digunakan untuk "pencarian", tambahkan / tampilkan $_GET di URL.
        // Caranya dengan memanipulasi $config['suffix'].
        if ($tipe == 'pencarian') {
            if (count($_GET) > 0) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['total_rows'] = $this->cari_num_rows_barang();
        } else {
            $config['first_url'] = '1';
            $config['total_rows'] = $this->get_all_num_rows_barang();
        }

        // Set konfigurasi.
        $this->pagination->initialize($config);

        // Buat link dan kembalikan link paging yang sudah jadi.
        return $this->pagination->create_links();
    }
    
    
     function paging_barang_bb($tipe, $base_url, $uri_segment)
    {
        // Memanggil library pagination.
        $this->load->library('pagination');

        // Konfigurasi.
        $config = array(
            'base_url' => $base_url,
            'uri_segment' => $uri_segment,
            'per_page' => $this->_per_page,
            'use_page_numbers' => true,
            'num_links' => 4,
            'first_link' => '&#124;&lt; First',
            'last_link' => 'Last &gt;&#124;',
            'next_link' => 'Next &gt;',
            'prev_link' => '&lt; Prev',

            //~ // Menyesuaikan untuk Twitter Bootstrap 3.2.0.
            //~ 'full_tag_open' => '<ul class="pagination pagination-sm">',
            //~ 'full_tag_close' => '</ul>',
            //~ 'num_tag_open' => '<li>',
            //~ 'num_tag_close' => '</li>',
            //~ 'cur_tag_open' => '<li class="disabled"><li class="active"><a href="#">',
            //~ 'cur_tag_close' => '<span class="sr-only"></span></a></li>',
            //~ 'next_tag_open' => '<li>',
            //~ 'next_tagl_close' => '</li>',
            //~ 'prev_tag_open' => '<li>',
            //~ 'prev_tagl_close' => '</li>',
            //~ 'first_tag_open' => '<li>',
            //~ 'first_tagl_close' => '</li>',
            //~ 'last_tag_open' => '<li>',
            //~ 'last_tagl_close' => '</li>',
        );

        // Jika paging digunakan untuk "pencarian", tambahkan / tampilkan $_GET di URL.
        // Caranya dengan memanipulasi $config['suffix'].
        if ($tipe == 'pencarian') {
            if (count($_GET) > 0) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['total_rows'] = $this->cari_num_rows_barang_bb();
        } else {
            $config['first_url'] = '1';
            $config['total_rows'] = $this->get_all_num_rows_barang_bb();
        }

        // Set konfigurasi.
        $this->pagination->initialize($config);

        // Buat link dan kembalikan link paging yang sudah jadi.
        return $this->pagination->create_links();
    }
    
    public function cari_barang($offset,$kata_kunci)
    {
       // $this->get_real_offset($offset);
             
        return $this->db->where("(nama_barang_wip LIKE '%$kata_kunci%') OR (kode_barang_wip LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get('tb_master_barang_wip')
                        ->result();
    }
    
     
    public function cari_barang_bb($offset,$kata_kunci)
    {
       // $this->get_real_offset($offset);
             
        return $this->db->select('tb_master_barang_bb.*,tb_master_barang_wip.nama_barang_wip,tb_master_barang_wip.kode_barang_wip')
						->join('tb_master_barang_wip', 'tb_master_barang_bb.id_barang_wip = tb_master_barang_wip.id','inner')
						->where("(nama_barang_wip LIKE '%$kata_kunci%') OR (kode_barang_wip LIKE '%$kata_kunci%')")
						->order_by('tb_master_barang_bb.id', 'ASC')
                        ->limit($this->_per_page, $this->_offset)
                        ->get('tb_master_barang_bb')
                        ->result();
    }
     public function cari_barang_bb_ck($offset,$kata_kunci)
    {
       // $this->get_real_offset($offset);
             
        return $this->db->select('tb_master_barang_bb.*,tb_master_barang_wip.nama_barang_wip,tb_master_barang_wip.kode_barang_wip')
						->join('tb_master_barang_wip', 'tb_master_barang_bb.id_barang_wip = tb_master_barang_wip.id','inner')
						->where("(nama_barang_wip LIKE '%$kata_kunci%') OR (kode_barang_wip LIKE '%$kata_kunci%')")
						->order_by('tb_master_barang_bb.id', 'ASC')
                        ->limit($this->_per_page, $this->_offset)
                        ->get('tb_master_barang_bb')
                        ->result();
    }
     public function cari_num_rows_barang()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
           return $this->db->where("(nama_barang_wip LIKE '%$kata_kunci%') OR (kode_barang_wip LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get('tb_master_barang_wip')
                        ->num_rows();
    }
    
     public function cari_num_rows_barang_bb()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
          return $this->db->where("(nama_barang_wip LIKE '%$kata_kunci%') OR (kode_barang_wip LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('tb_master_barang_bb.id', 'ASC')
                        ->join('tb_master_barang_wip', 'tb_master_barang_bb.id_barang_wip = tb_master_barang_wip.id','inner')
                        ->get('tb_master_barang_bb')
                        ->num_rows();
    }
    
     public function get_all_num_rows_barang()
    {
        return $this->db->get($this->_tabel)->num_rows();
    }
    
     public function get_all_num_rows_barang_bb()
    {
        return $this->db->get('tb_master_barang_bb')->num_rows();
    }
}
