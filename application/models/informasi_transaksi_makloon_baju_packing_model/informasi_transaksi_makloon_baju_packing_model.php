<?php
class Informasi_transaksi_makloon_baju_packing_model extends MY_Model
{
	protected $_per_page = 10;
   
    protected $form_rules = array(
        
        
         array(
            'field' => 'jenis_masuk',
            'label' => 'jenis_masuk',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'tanggal_sj_dari',
            'label' => 'Tanggal SJ Dari',
            'rules' => 'trim|required|max_length[16]'
        ),
          array(
            'field' => 'tanggal_sj_ke',
            'label' => 'Tanggal SJ Ke',
            'rules' => 'trim|required|max_length[16]'
        ),
        array(
            'field' => 'packing',
            'label' => 'Unit packing',
            'rules' => 'trim|required|max_length[16]'
        ),
           array(
            'field' => 'unit_packing',
            'label' => 'Unit Packing',
            'rules' => 'trim|required|max_length[16]'
        ),
          
         array(
            'field' => 'keterangan_header',
            'label' => 'Keterangan Header',
            'rules' => 'trim|required|max_length[16]'
        )
        
        
    );

     public $default_values = array(
		'id'	=>	'',
		'num'		=>1,

        'tanggal_sj_dari' => '',
        'tanggal_sj_ke' => '',
		'jenis_masuk' => '',
	
		'keterangan_header'=>'',
    );
    
     public function get_unit_gudang()
    {
     $sql=$this->db->query("SELECT id,nama_unit_packing FROM tb_master_gudang order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }    
    public function get_unit_packing()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_unit_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }  
    public function get_packing()
    {
     $sql=$this->db->query("SELECT * FROM tb_master_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }
    
    
     public function get_all_inner_paged($tanggal_sj_dari,$tanggal_sj_ke,$packing,$id_barang_bb)
    {
		
		$exid_barang_bb=explode(';',$id_barang_bb);
		foreach ($exid_barang_bb as $row555)
		{
			if($row555 != ''){
	
				$query11	= $this->db->query(" SELECT kode_barang_bb, nama_barang_bb FROM tb_master_barang_bb WHERE id = '$row555' ");
				if ($query11->num_rows() > 0){
					$hasilrow11 = $query11->row();
					
					$nama_barang_bb = $hasilrow11->nama_barang_bb;
					$kode_barang_bb = $hasilrow11->kode_barang_bb;
				}
				else {
				
					$kode_barang_bb = '';
					$nama_barang_bb = '';
					
				}
		
     $sql=$this->db->query("
	select id,tanggal_sj,no_sj,id_unit_packing ,1 as masuk_bagus_packing,0 as masuk_lain_packing,0 as keluar_bagus_packing,0 as keluar_lain_packing
	from tb_makloon_qc_baju_wip where jenis_masuk='1' AND id_unit_packing = '$packing' AND tanggal_sj >= '$tanggal_sj_dari' AND tanggal_sj <= '$tanggal_sj_ke'
	AND status_aktif='t'
	union 
	select id,tanggal_sj,no_sj,id_unit_packing ,0 as masuk_bagus_packing,1 as masuk_lain_packing,0 as keluar_bagus_packing,0 as keluar_lain_packing
	from tb_makloon_qc_baju_wip where jenis_masuk='2' AND id_unit_packing = '$packing' AND tanggal_sj >= '$tanggal_sj_dari' AND tanggal_sj <= '$tanggal_sj_ke'
	AND status_aktif='t'
	union 
	select id,tanggal_sj,no_sj,id_unit_packing  ,0 as masuk_bagus_packing,0 as masuk_lain_packing,1 as keluar_bagus_packing,0 as keluar_lain_packing
	from tb_makloon_baju_gudang_jadi_wip where jenis_masuk='1' AND id_unit_packing = '$packing' AND tanggal_sj >= '$tanggal_sj_dari' AND tanggal_sj <= '$tanggal_sj_ke'
	AND status_aktif='t'
	union 
	select id,tanggal_sj,no_sj,id_unit_packing  ,0 as masuk_bagus_packing,0 as masuk_lain_packing,0 as keluar_bagus_packing,1 as keluar_lain_packing
	from tb_makloon_baju_gudang_jadi_wip where jenis_masuk='2' AND id_unit_packing = '$packing' AND tanggal_sj >= '$tanggal_sj_dari' AND tanggal_sj <= '$tanggal_sj_ke'
	AND status_aktif='t'
	");
          
     if($sql->num_rows() > 0){
		 $query= $sql->result();
		 foreach($query as $row){
			 if($row->masuk_bagus_packing==1 || $row->masuk_lain_packing==1){
				 
				 $sql3 = " SELECT id,qty FROM tb_makloon_qc_baju_wip_detail
									WHERE id_makloon_qc_baju_wip= '$row->id' AND id_barang_bb = '$row555'
									 ";
				 
				 }
			 
			  if($row->keluar_bagus_packing==1 || $row->keluar_lain_packing==2){
				 
				 $sql3 = " SELECT id,qty FROM tb_makloon_baju_gudang_jadi_wip_detail
									WHERE id_makloon_baju_gudang_jadi_wip= '$row->id' AND id_barang_bb = '$row555'
									 ";
				 
				 }
			 $query3	= $this->db->query($sql3);
			 if ($query3->num_rows() > 0){
				$hasil3 = $query3->result();
				foreach($hasil3 as $row3){
						
				$data_detail[]=array(
				'id_header'=>$row->id ,
				'tanggal_sj'=>$row->tanggal_sj,
				'no_sj'=>$row->no_sj,
				'id_unit_packing'=>$row->id_unit_packing,
				'masuk_bagus_packing'=>$row->masuk_bagus_packing,
				'masuk_lain_packing'=>$row->masuk_lain_packing,
				'keluar_bagus_packing'=>$row->keluar_bagus_packing,
				'keluar_lain_packing'=>$row->keluar_lain_packing,
				'id_detail'=>$row3->id,
				'qty'=>$row3->qty
				);		
				
							}
				
						}
						
					}
			 
				}
				return $data_detail;
			}
		}
	}
}
