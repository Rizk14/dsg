<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	/* 20092011
	function jmlBonM($ibonmcode) {
		return	$db2->query(" SELECT * FROM tm_inbonm_item a INNER JOIN tm_inbonm b ON b.i_inbonm_code=cast(a.i_inbonm AS character varying) WHERE cast(a.i_inbonm AS character varying)=trim('$ibonmcode') ");
	}
	*/

	function jmlBonM($ibonmcode) {
		$db2=$this->load->database('db_external', TRUE);
		return	$db2->query(" SELECT * FROM tm_inbonm_item a INNER JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm WHERE b.i_inbonm_code=trim('$ibonmcode') ");
	}
	
	function clistbonmmasuk($bonmmasuk,$d_bonmmasuk_first,$d_bonmmasuk_last,$imotif) {
$db2=$this->load->database('db_external', TRUE);
		if(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk!='' && $imotif!='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm	= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk=='' && $imotif!='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm	= "";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk!='' && $imotif=='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm	= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= "";
		} elseif(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk=='' && $imotif=='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm	= "";
			$iproductmotif	= "";
		} elseif(($d_bonmmasuk_first!='' || $d_bonmmasuk_last!='') && $bonmmasuk!='' && $imotif=='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm	= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= "";
		} elseif(($d_bonmmasuk_first!='' || $d_bonmmasuk_last!='') && $bonmmasuk=='' && $imotif!='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm	= "";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first=='' || $d_bonmmasuk_last=='') && $bonmmasuk!='' && $imotif!='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm	= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first=='' || $d_bonmmasuk_last=='') && $bonmmasuk=='' && $imotif!='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm	= "";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first=='' || $d_bonmmasuk_last=='') && $bonmmasuk!='' && $imotif=='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm	= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= "";
		} else {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm	= "";
			$iproductmotif	= "";
		}
		
		$query	= $db2->query(" SELECT b.i_inbonm AS ibonm,
						b.i_inbonm_code AS inbonmcode,
						b.d_inbonm AS dinbonm,
						a.i_product AS iproduct,
						c.e_product_motifname AS productname,
						a.n_count_product AS qty,
						
						a.i_inbonm_item
				
				FROM tm_inbonm_item a
				
				RIGHT JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) ".$dinbonm." ".$ibonm." ".$iproductmotif." ORDER BY b.d_inbonm DESC ");
		
		if($query->num_rows()>0){
			return $result	= $query->result();
		}
	}

	function clistbonmmasuk2($bonmmasuk,$d_bonmmasuk_first,$d_bonmmasuk_last,$imotif) {
		$db2=$this->load->database('db_external', TRUE);
		if(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk!='' && $imotif!='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm		= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk=='' && $imotif!='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm		= "";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk!='' && $imotif=='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm		= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= "";
		} elseif(($d_bonmmasuk_first!='' && $d_bonmmasuk_last!='') && $bonmmasuk=='' && $imotif=='') {
			$dinbonm	= " WHERE (b.d_inbonm BETWEEN '$d_bonmmasuk_first' AND '$d_bonmmasuk_last') AND b.f_inbonm_cancel='f' ";
			$ibonm		= "";
			$iproductmotif	= "";						
		} elseif(($d_bonmmasuk_first!='' || $d_bonmmasuk_last!='') && $bonmmasuk!='' && $imotif=='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm		= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= "";
		} elseif(($d_bonmmasuk_first!='' || $d_bonmmasuk_last!='') && $bonmmasuk=='' && $imotif!='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm		= "";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first=='' || $d_bonmmasuk_last=='') && $bonmmasuk!='' && $imotif!='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm		= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first=='' || $d_bonmmasuk_last=='') && $bonmmasuk=='' && $imotif!='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm		= "";
			$iproductmotif	= " AND a.i_product='$imotif' ";
		} elseif(($d_bonmmasuk_first=='' || $d_bonmmasuk_last=='') && $bonmmasuk!='' && $imotif=='') {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm		= " AND b.i_inbonm_code='$bonmmasuk' ";
			$iproductmotif	= "";
		} else {
			$dinbonm	= " WHERE b.f_inbonm_cancel='f' ";
			$ibonm		= "";
			$iproductmotif	= "";
		}
		
		$query	= $db2->query(" SELECT b.i_inbonm AS ibonm, 
				b.i_inbonm_code AS inbonmcode, 
				b.d_inbonm AS dinbonm, b.i_sj_manual,
				sum(a.n_count_product) AS qty FROM tm_inbonm_item a 
			RIGHT JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			".$dinbonm." ".$ibonm." ".$iproductmotif." GROUP BY b.i_inbonm, b.i_inbonm_code, b.d_inbonm ORDER BY b.d_inbonm DESC ");
		
		if($query->num_rows() > 0 )	{
			return $result	= $query->result();
		}
	}
		
	/* Edit Session */
	function getbonmmasuk($ibonm) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_inbonm WHERE i_inbonm='$ibonm' AND f_inbonm_cancel='f' ORDER BY i_inbonm DESC LIMIT 1 ");
	}
	
	function mupdate($i_inbonm_code,$i_inbonm_hidden,$i_inbonm_code_hidden,$d_inbonm,$i_product,$e_product_name,
					$qty_warna, $i_product_color, $i_color, $qty_product,$iterasi,$f_stp,$iso,$i_sj) {
						$db2=$this->load->database('db_external', TRUE);
		// $n_count_product
		$db2->trans_begin();
		
		$tm_bonmmasuk_item	= array();
		$i_inbonm_item	= array();
		$qty_product_update	= array();
		$is2	= array();
		$ins_temp_tmso	= array();
		//$ncountproduct	= array();

		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$isoz	= $row_stokopname->i_so;
		}else{
			$isoz	= "";
		}

		if($isoz!="") {
			
			$j=0;
			$iproductmotif	= array();
			$jml		= array();
			$totalharga	= array();
			$qtyawal	= array();
			$qtyakhir	= array();
			
			$qinbonm1	= $db2->query(" SELECT * FROM tm_inbonm WHERE i_inbonm='$i_inbonm_hidden' AND f_inbonm_cancel='f' ");
			$rinbonm1	= $qinbonm1->row();

			$qinbonm_item	= $db2->query(" SELECT * FROM tm_inbonm_item WHERE i_inbonm='$rinbonm1->i_inbonm' ");
			
			if($qinbonm_item->num_rows()>0) {
				
				$result_inbonmitem	= $qinbonm_item->result();
				
				foreach($result_inbonmitem as $row_inbonmitem) {
					// ambil data2 per warna, reset stoknya -------------------------------
					$qinbonm_item_color	= $db2->query(" SELECT * FROM tm_inbonm_item_color 
											WHERE i_inbonm_item='$row_inbonmitem->i_inbonm_item' ");
			
					if($qinbonm_item_color->num_rows()>0) {
						$result_inbonmitem_color	= $qinbonm_item_color->result();
						foreach($result_inbonmitem_color as $rowxx) {
							if ($rowxx->i_color != '0') {
								$datacolor = " AND a.i_color = '".$rowxx->i_color."' ";
								$datacolor2 = $datacolor;
								$datacolor3 = " AND i_color = '".$rowxx->i_color."' ";
							}
							else {
								//$datacolor = " AND a.i_product_color = '".$rowxx->i_product_color."' ";
								$qxx = $db2->query(" SELECT i_color FROM tr_product_color WHERE i_product_color='$rowxx->i_product_color' ");
								if($qxx->num_rows()>0) {
									$rxx	= $qxx->row();
									$datacolor2	= " AND a.i_color= '".$rxx->i_color."' ";
									$datacolor3 = " AND i_color= '".$rxx->i_color."' ";
								}
							}
							$qsowarna	= $db2->query(" SELECT a.*, b.i_so_item FROM tm_stokopname_item_color a
										INNER JOIN tm_stokopname_item b ON a.i_so_item = b.i_so_item
										WHERE b.i_so='$row_inbonmitem->i_so' AND b.i_product='".$row_inbonmitem->i_product."' ".$datacolor2);
							if($qsowarna->num_rows()>0) {
								$rowsowarna	= $qsowarna->row();
								$qtyawalwarna	= $rowsowarna->n_quantity_awal;
								$qtyakhirwarna	= $rowsowarna->n_quantity_akhir;
								
								$back_qty_awalwarna	= $qtyawalwarna;
								$back_qty_akhirwarna	= $qtyakhirwarna-$rowxx->qty;
								
								if($back_qty_akhirwarna=='')
									$back_qty_akhirwarna = 0;
								
								$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir='$back_qty_akhirwarna'
												WHERE i_so_item = '$rowsowarna->i_so_item' ".$datacolor3);
							}else{
								$qtyawalwarna	= 0;
								$qtyakhirwarna	= 0;
							}
						} // end foreach warna
					} // end if per warna
					//-----------------------------------------------------------------------
					
					// modif 06-10-2014 setelah reset stok, hapus inbonm_item_color 08-01-2014
					$db2->query(" DELETE FROM tm_inbonm_item_color WHERE i_inbonm_item='".$row_inbonmitem->i_inbonm_item."' ");	
					// ------------------------------------------
					
					$iproductmotif[$j]	= $row_inbonmitem->i_product;
					$jml[$j]		= $row_inbonmitem->n_count_product;

					$qstokopname2	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_so='$row_inbonmitem->i_so' ORDER BY i_so DESC LIMIT 1 ");
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product='$iproductmotif[$j]' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;
					}else{
						$qtyawal[$j]	= 0;
						$qtyakhir[$j]	= 0;
					}
						
					$back_qty_awal[$j]	= $qtyawal[$j];
					$back_qty_akhir[$j]	= $qtyakhir[$j]-$jml[$j];
					
					if($back_qty_akhir[$j]=='')
						$back_qty_akhir[$j] = 0;
					
					$update_stokop_item[$j]	= array(
						'n_quantity_akhir'=>$back_qty_akhir[$j]
					);
					
					$db2->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$isoz2,'i_product'=>$iproductmotif[$j]));
					
					$j+=1;
				}
			} 

			$db2->query(" DELETE FROM tm_inbonm WHERE i_inbonm='$i_inbonm_hidden' ");
			$db2->query(" DELETE FROM tm_inbonm_item WHERE i_inbonm='$i_inbonm_hidden' ");			
		}

		$db2->set(
			array(
			 'i_inbonm'=>$i_inbonm_hidden,
			 'i_inbonm_code'=>$i_inbonm_code,
			 'd_inbonm'=>$d_inbonm,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry,
			 'i_sj_manual'=>$i_sj));
			 
		if($db2->insert('tm_inbonm')) {
			
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++) {
				
				//$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];
				
				$seq_tm_inbonm_item	= $db2->query(" SELECT i_inbonm_item FROM tm_inbonm_item ORDER BY i_inbonm_item DESC LIMIT 1 ");
				
				if($seq_tm_inbonm_item->num_rows() > 0) {
					$seqrow	= $seq_tm_inbonm_item->row();
					$i_inbonm_item[$jumlah]	= $seqrow->i_inbonm_item+1;
				}else{
					$i_inbonm_item[$jumlah]	= 1;
				}
				
				//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($i_product_color[$jumlah]); $xx++) {

					$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
					$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
													
					$qtytotal+= $qty_warna[$jumlah][$xx];
				} // end for
				// ---------------------------------- END -----------------------------------
				$qty_product_update[$jumlah]	= $qty_product[$jumlah]+$qtytotal;

				$tm_bonmmasuk_item[$jumlah]	= array(
					 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
					 'i_inbonm'=>$i_inbonm_hidden,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$qtytotal,
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah] );
				
				if($db2->insert('tm_inbonm_item',$tm_bonmmasuk_item[$jumlah])) {

					$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}

					$qstok_item	= $db2->query("SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product='$i_product[$jumlah]' ");

					if($qstok_item->num_rows()>0) {
						
						$row_stok_item	= $qstok_item->row();
						
						$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;
						
						$i_so_item = $row_stok_item->i_so_item;

						$back_qty_awal[$jumlah]	= $qtyawal[$jumlah];
						//$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]+$ncountproduct[$jumlah];
						$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]+$qtytotal;
						
						if($back_qty_akhir[$jumlah]=='')
							$back_qty_akhir[$jumlah] = 0;
						
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz2,'i_product'=>$i_product[$jumlah]));
					}
					
					// 06-10-2014, dipindah kesini yg color
					// ------------------------------------------
					for ($xx=0; $xx<count($i_product_color[$jumlah]); $xx++) {
						
							$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
							$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
							$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
							
							/*if ($rowxx->i_color != '0') {
								$datacolor = " AND a.i_color = '".$i_color[$jumlah][$xx]."' ";
								$datacolor2 = $datacolor;
								$datacolor3 = " AND i_color = '".$i_color[$jumlah][$xx]."' ";
							}
							else {
								//$datacolor = " AND a.i_product_color = '".$rowxx->i_product_color."' ";
								$qxx = $db2->query(" SELECT i_color FROM tr_product_color 
												WHERE i_product_color='".$i_product_color[$jumlah][$xx]."' ");
								if($qxx->num_rows()>0) {
									$rxx	= $qxx->row();
									$datacolor2	= " AND a.i_color= '".$rxx->i_color."' ";
									$datacolor3 = " AND i_color= '".$rxx->i_color."' ";
								}
							} */
							
							$seq_tm_inbonm_item_color	= $db2->query(" SELECT i_inbonm_item_color FROM tm_inbonm_item_color 
														ORDER BY i_inbonm_item_color DESC LIMIT 1 ");
						
							if($seq_tm_inbonm_item_color->num_rows() > 0) {
								$seqrow	= $seq_tm_inbonm_item_color->row();
								$i_inbonm_item_color[$jumlah]	= $seqrow->i_inbonm_item_color+1;
							}else{
								$i_inbonm_item_color[$jumlah]	= 1;
							}

							$tm_bonmmasuk_item_color[$jumlah]	= array(
								 'i_inbonm_item_color'=>$i_inbonm_item_color[$jumlah],
								 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
								 'i_product_color'=>$i_product_color[$jumlah][$xx],
								 'qty'=>$qty_warna[$jumlah][$xx],
								 'i_color'=>$i_color[$jumlah][$xx],
							);
							$db2->insert('tm_inbonm_item_color',$tm_bonmmasuk_item_color[$jumlah]);
							
							// 06-10-2014, update stok per warna
							$sqlwarna = " SELECT * FROM tm_stokopname_item_color WHERE i_so_item = '$i_so_item'
											AND i_color = '".$i_color[$jumlah][$xx]."' ";
							$querywarna = $db2->query($sqlwarna);
								
							if($querywarna->num_rows() > 0) {
								$hasilwarna = $querywarna->row();
								
								$qtyawalwarna	= $hasilwarna->n_quantity_awal;
								$qtyakhirwarna	= $hasilwarna->n_quantity_akhir;

								$back_qty_awalwarna	= $qtyawalwarna;
								$back_qty_akhirwarna	= $qtyakhirwarna+$qty_warna[$jumlah][$xx];
									
								if($back_qty_akhirwarna=="")
									$back_qty_akhirwarna	= 0;
																		
								$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir = '$back_qty_akhirwarna'
													WHERE i_so_item = '$i_so_item' AND i_color = '".$i_color[$jumlah][$xx]."' ");
									
							} // end if
					} // end for
					// ------------------------------------------
					
					if($db2->trans_status()===FALSE || $db2->trans_status()==FALSE) {
						$db2->trans_rollback();
					}else{
						$db2->trans_commit();
					}
					
					//reset kembali
					$qtytotal = 0;
				}else{
					echo "";
				}
			}
		}else{
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal diupdate!\");window.open(\"index\", \"_self\");</script>";
		}
		
		$qbonmmasuk	= $db2->query(" SELECT * FROM tm_inbonm WHERE i_inbonm='$i_inbonm_hidden' AND f_inbonm_cancel='f' ");
		if($qbonmmasuk->num_rows()==0) {
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal diupdate!\");window.open(\"index\", \"_self\");</script>";
		}else{
			print "<script>alert(\"Nomor Bon M Masuk : '\"+$i_inbonm_code_hidden+\"' berhasil diupdate, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}

	}

	function mupdate_old($i_inbonm_code,$i_inbonm_hidden,$i_inbonm_code_hidden,$d_inbonm,$i_product,$e_product_name,$n_count_product,$qty_product,$iterasi,$f_stp,$iso,$i_sj) {
		$db2=$this->load->database('db_external', TRUE);
		/*
		echo $i_inbonm_hidden." ".$i_inbonm_code_hidden." ".$d_inbonm." ".$i_product." ".$e_product_name." ".$n_count_product." ".$qty_product." ".$iterasi;
		*/
		//$db2->trans_begin();
		
		$tm_bonmmasuk_item	= array();
		$i_inbonm_item	= array();
		$qty_product_update	= array();
		$is2	= array();
		$ins_temp_tmso	= array();
		$ncountproduct	= array();

		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$isoz	= $row_stokopname->i_so;
		} else {
			$isoz	= "";
		}

		if($isoz!="") {
			$j=0;
			$iproductmotif	= array();
			$jml		= array();
			$totalharga	= array();
			$qtyawal	= array();
			$qtyakhir	= array();
			
			$qinbonm1	= $db2->query(" SELECT * FROM tm_inbonm WHERE i_inbonm='$i_inbonm_hidden' AND f_inbonm_cancel='f' ");
			$rinbonm1	= $qinbonm1->row();
			//$qinbonm_item	= $db2->query(" SELECT * FROM tm_inbonm_item WHERE i_inbonm='$i_inbonm_code_hidden' ");
			/* 20092011
			$qinbonm_item	= $db2->query(" SELECT * FROM tm_inbonm_item WHERE i_inbonm='$rinbonm1->i_inbonm_code' ");
			*/
			$qinbonm_item	= $db2->query(" SELECT * FROM tm_inbonm_item WHERE i_inbonm='$rinbonm1->i_inbonm' ");
			
			if($qinbonm_item->num_rows()>0) {
				
				$result_inbonmitem	= $qinbonm_item->result();
				
				foreach($result_inbonmitem as $row_inbonmitem) {
					
					$iproductmotif[$j]	= $row_inbonmitem->i_product;
					$jml[$j]		= $row_inbonmitem->n_count_product;

					//$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' AND  f_stop_produksi='$f_stp[$j]' AND i_so='$iso[$j]' ORDER BY i_so DESC LIMIT 1");
					//$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$j]' AND i_so='$iso[$j]' ORDER BY i_so DESC LIMIT 1");
					$qstokopname2	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_so='$row_inbonmitem->i_so' ORDER BY i_so DESC LIMIT 1 ");
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product='$iproductmotif[$j]' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;
					} else {
						$qtyawal[$j]	= 0;
						$qtyakhir[$j]	= 0;
					}
					
					//Disabled 25012011
					//$back_qty_awal[$j]	= $qtyakhir[$j]+$delivery[$j];
					//$back_qty_akhir[$j]	= $qtyawal[$j];
						
					$back_qty_awal[$j]	= $qtyawal[$j];
					$back_qty_akhir[$j]	= $qtyakhir[$j]-$jml[$j];
					
					if($back_qty_akhir[$j]=='')
						$back_qty_akhir[$j] = 0;
						
					/* 14072011
					$update_stokop_item[$j]	= array(
						'n_quantity_awal'=>$back_qty_awal[$j],
						'n_quantity_akhir'=>$back_qty_akhir[$j]
					);					
					*/
					
					$update_stokop_item[$j]	= array(
						'n_quantity_akhir'=>$back_qty_akhir[$j]
					);
					
					$db2->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$isoz2,'i_product'=>$iproductmotif[$j]));
					
					$j+=1;
				}
			}

			/* 19092011
			$db2->delete('tm_inbonm',array('i_inbonm_code'=>$i_inbonm_code_hidden));
			$db2->delete('tm_inbonm_item',array('i_inbonm'=>$i_inbonm_code_hidden));
			*/
			
			/* 20092011
			$db2->query(" DELETE FROM tm_inbonm WHERE i_inbonm_code='$i_inbonm_code_hidden' ");
			$db2->query(" DELETE FROM tm_inbonm_item WHERE i_inbonm='$i_inbonm_code_hidden' ");
			*/ 

			$db2->query(" DELETE FROM tm_inbonm WHERE i_inbonm='$i_inbonm_hidden' ");
			$db2->query(" DELETE FROM tm_inbonm_item WHERE i_inbonm='$i_inbonm_hidden' ");			
		}
		
		for($arr_iso=0; $arr_iso<count($iso); $arr_iso++){
			$stkopname	= $iso[$arr_iso];
			if($stkopname!="")
				break;
		}
		/*
		$db2->set(
			array(
			 'i_inbonm'=>$i_inbonm_hidden,
			 'i_inbonm_code'=>$i_inbonm_code_hidden,
			 'd_inbonm'=>$d_inbonm,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry,
			 'i_so'=>$stkopname ));
		*/
		
		/* 20092011		
		$db2->set(
			array(
			 'i_inbonm'=>$i_inbonm_hidden,
			 'i_inbonm_code'=>$i_inbonm_code_hidden,
			 'd_inbonm'=>$d_inbonm,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry ));
		*/

		$db2->set(
			array(
			 'i_inbonm'=>$i_inbonm_hidden,
			 'i_inbonm_code'=>$i_inbonm_code,
			 'd_inbonm'=>$d_inbonm,
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry,
			 'i_sj_manual'=>$i_sj));
			 
		if($db2->insert('tm_inbonm')) {
			
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++) {
				
				$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];
				
				/*
				$seq_tm_inbonm_item	= $db2->query(" SELECT cast(i_inbonm_item AS integer) AS i_inbonm_item FROM tm_inbonm_item ORDER BY cast(i_inbonm_item AS integer) DESC LIMIT 1 ");
				*/ 
				$seq_tm_inbonm_item	= $db2->query(" SELECT i_inbonm_item FROM tm_inbonm_item ORDER BY i_inbonm_item DESC LIMIT 1 ");
				
				if($seq_tm_inbonm_item->num_rows() > 0){
					$seqrow	= $seq_tm_inbonm_item->row();
					$i_inbonm_item[$jumlah]	= $seqrow->i_inbonm_item+1;
				} else {
					$i_inbonm_item[$jumlah]	= 1;
				}
				
				$qty_product_update[$jumlah]	= $qty_product[$jumlah]+$ncountproduct[$jumlah];
				
				/* 20092011
				$tm_bonmmasuk_item[$jumlah]	= array(
					 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
					 'i_inbonm'=>$i_inbonm_code_hidden,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$ncountproduct[$jumlah],
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah] );
				*/

				$tm_bonmmasuk_item[$jumlah]	= array(
					 'i_inbonm_item'=>$i_inbonm_item[$jumlah],
					 'i_inbonm'=>$i_inbonm_hidden,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$ncountproduct[$jumlah],
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah] );
				
				if($db2->insert('tm_inbonm_item',$tm_bonmmasuk_item[$jumlah])) {

					//$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' AND  f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
					$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product='$i_product[$jumlah]' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;

						$back_qty_awal[$jumlah]	= $qtyawal[$jumlah];
						$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]+$ncountproduct[$jumlah];
						
						/* 14072011
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_awal'=>$back_qty_awal[$jumlah],
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						*/
						
						if($back_qty_akhir[$jumlah]=='')
							$back_qty_akhir[$jumlah] = 0;
						
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz2,'i_product'=>$i_product[$jumlah]));
					}
					
					$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.i_status_do='1' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
					
					if($get_tmso->num_rows() > 0 ){
						
						$get_tmso2	= $db2->query(" SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						if($get_tmso2->num_rows() > 0 ){
							$row_tmso2	= $get_tmso2->row();
							$is2[$jumlah]	= $row_tmso2->iso+1;
						} else {
							$is2[$jumlah]	= 1;
						}
						
						$row_tmso	= $get_tmso->row_array();
						
						/* 06082011
						$temp_iso[$jumlah]	= $row_tmso['i_so'];
						$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
						$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
						$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
						$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
						$temp_ddo[$jumlah]	= $row_tmso['d_do'];
						$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
						$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
						$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
						$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
						$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
						$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
						$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
						 
						$ins_temp_tmso[$jumlah]	= array(
							'i_so'=>$iso[$jumlah],
							'i_product'=>$temp_iproduct[$jumlah],
							'i_product_motif'=>$temp_imotif[$jumlah],
							'e_product_motifname'=>$temp_productname[$jumlah],
							'i_status_do'=>$temp_istatusdo[$jumlah],
							'd_do'=>$temp_ddo[$jumlah],
							'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
							'n_saldo_akhir'=>$saldo_akhir[$jumlah],
							'n_inbonm'=>$tambah_inbonm[$jumlah],
							'n_outbonm'=>$temp_n_outbonm[$jumlah],
							'n_bbm'=>$temp_n_bbm[$jumlah],
							'n_bbk'=>$temp_n_bbk[$jumlah],
							'd_entry'=>$dentry
						);
						*/

						$temp_iso[$jumlah]	= $row_tmso['i_so'];

						$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
						if($temp_saldo_awal[$jumlah]=="")
							$temp_saldo_awal[$jumlah] = 0;
							
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
						if($temp_saldo_akhir[$jumlah]=="")
							$temp_saldo_akhir[$jumlah] = 0;
							
						$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
						if($temp_n_inbonm[$jumlah]=="")
							$temp_n_inbonm[$jumlah] = 0;
							
						$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
						if($temp_n_outbonm[$jumlah]=="")
							$temp_n_outbonm[$jumlah] = 0;
							
						$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
						if($temp_n_bbm[$jumlah]=="")
							$temp_n_bbm[$jumlah] = 0;
							
						$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
						if($temp_n_bbk[$jumlah]=="")
							$temp_n_bbk[$jumlah] = 0;
							
						$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
						if($saldo_akhir[$jumlah]=="")
							$saldo_akhir[$jumlah] = 0;
							
						$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
						if($tambah_inbonm[$jumlah]=="")
							$tambah_inbonm[$jumlah] = 0;
							 
						$ins_temp_tmso[$jumlah]	= array(
							'i_so'=>$is2[$jumlah],
							'i_product'=>$row_tmso['i_product'],
							'i_product_motif'=>$row_tmso['i_product_motif'],
							'e_product_motifname'=>$row_tmso['e_product_motifname'],
							'i_status_do'=>$row_tmso['i_status_do'],
							'd_do'=>$row_tmso['d_do'],
							'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
							'n_saldo_akhir'=>$saldo_akhir[$jumlah],
							'n_inbonm'=>$tambah_inbonm[$jumlah],
							'n_outbonm'=>$temp_n_outbonm[$jumlah],
							'n_bbm'=>$temp_n_bbm[$jumlah],
							'n_bbk'=>$temp_n_bbk[$jumlah],
							'd_entry'=>$dentry
						);
												
						$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						
					} else {
						$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
						
						if($get_tmso->num_rows()>0) {
							$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
							if($get_tmso2->num_rows() > 0 ){
								$row_tmso2	= $get_tmso2->row();
								$is2[$jumlah]	= $row_tmso2->iso+1;
							} else {
								$is2[$jumlah]	= 1;
							}
							
							$row_tmso	= $get_tmso->row_array();
							
							/* 06082011
							$temp_iso[$jumlah]	= $row_tmso['i_so'];
							$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
							$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
							$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
							$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
							$temp_ddo[$jumlah]	= $row_tmso['d_do'];
							$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
							$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
							$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
							$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
							$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
							$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
							$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
							$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
							 
							$ins_temp_tmso[$jumlah]	= array(
								'i_so'=>$iso[$jumlah],
								'i_product'=>$temp_iproduct[$jumlah],
								'i_product_motif'=>$temp_imotif[$jumlah],
								'e_product_motifname'=>$temp_productname[$jumlah],
								'i_status_do'=>'1',
								'd_do'=>$temp_ddo[$jumlah],
								'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
								'n_saldo_akhir'=>$saldo_akhir[$jumlah],
								'n_inbonm'=>$tambah_inbonm[$jumlah],
								'n_outbonm'=>$temp_n_outbonm[$jumlah],
								'n_bbm'=>$temp_n_bbm[$jumlah],
								'n_bbk'=>$temp_n_bbk[$jumlah],
								'd_entry'=>$dentry
							);
							*/

							$temp_iso[$jumlah]	= $row_tmso['i_so'];
							$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];

							$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal'];
							if($temp_saldo_awal[$jumlah]=="")
								$temp_saldo_awal[$jumlah] = 0;
								
							$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
							if($temp_saldo_akhir[$jumlah]=="")
								$temp_saldo_akhir[$jumlah] = 0;
								
							$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm'];
							if($temp_n_inbonm[$jumlah]=="")
								$temp_n_inbonm[$jumlah] = 0;
								
							$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm'];
							if($temp_n_outbonm[$jumlah]=="")
								$temp_n_outbonm[$jumlah] = 0;
								
							$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm'];
							if($temp_n_bbm[$jumlah]=="")
								$temp_n_bbm[$jumlah] = 0;
								
							$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk'];
							if($temp_n_bbk[$jumlah]=="")
								$temp_n_bbk[$jumlah] = 0;
								
							$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] + $ncountproduct[$jumlah];
							if($saldo_akhir[$jumlah]=="")
								$saldo_akhir[$jumlah] = 0;
								
							$tambah_inbonm[$jumlah]	= $temp_n_inbonm[$jumlah] + $ncountproduct[$jumlah];
							if($tambah_inbonm[$jumlah]=="")
								$tambah_inbonm[$jumlah] = 0;
								
							$ins_temp_tmso[$jumlah]	= array(
								'i_so'=>$is2[$jumlah],
								'i_product'=>$row_tmso['i_product'],
								'i_product_motif'=>$row_tmso['i_product_motif'],
								'e_product_motifname'=>$row_tmso['e_product_motifname'],
								'i_status_do'=>'1',
								'd_do'=>$row_tmso['d_do'],
								'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
								'n_saldo_akhir'=>$saldo_akhir[$jumlah],
								'n_inbonm'=>$tambah_inbonm[$jumlah],
								'n_outbonm'=>$temp_n_outbonm[$jumlah],
								'n_bbm'=>$temp_n_bbm[$jumlah],
								'n_bbk'=>$temp_n_bbk[$jumlah],
								'd_entry'=>$dentry
							);
														
							$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						} else {
							echo "gagal update tabel tm_inbonm & tm_inbonm_item!";
						}
					}
					
					//if ($db2->trans_status()===FALSE) {
					//	$db2->trans_rollback();
					//} else {
					//	$db2->trans_commit();
					//}
					
				} else {
					echo "gagal update tabel tm_inbonm!";
				}
			}
		} else {
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal diupdate!\");window.open(\"index\", \"_self\");</script>";
		}
		
		$qbonmmasuk	= $db2->query("SELECT * FROM tm_inbonm WHERE i_inbonm='$i_inbonm_hidden' AND f_inbonm_cancel='f' ");
			
		if($qbonmmasuk->num_rows()<0 || $qbonmmasuk->num_rows()==0) {
			print "<script>alert(\"Maaf, Data Bon M Masuk gagal diupdate!\");window.open(\"index\", \"_self\");</script>";
		} else {
			print "<script>alert(\"Nomor Bon M Masuk : '\"+$i_inbonm_code_hidden+\"' berhasil diupdate, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}

	}
		
	//function getbonmmasukitem($ibonmmasukcode) {
	function getbonmmasukitem($ibonmmasuk) {
		$db2=$this->load->database('db_external', TRUE);
		/* 11082011
		$qstr	= "
			SELECT a.i_product, b.e_product_motifname, a.n_count_product FROM tm_inbonm_item a
			
			INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
			INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
			
			WHERE i_inbonm='$ibonmmasukcode'
			
			GROUP BY a.i_product, b.e_product_motifname, a.n_count_product,a.i_inbonm_item
			
			ORDER BY a.i_inbonm_item ASC ";
		*/
		
		/* 20092011
		$qstr	= "
			SELECT a.i_product, b.e_product_motifname, a.n_count_product, a.i_so FROM tm_inbonm_item a
			
			INNER JOIN tm_inbonm d ON d.i_inbonm_code=cast(a.i_inbonm AS character varying)
			INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
			INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
			
			WHERE a.i_inbonm='$ibonmmasukcode' AND d.f_inbonm_cancel='f'
			
			GROUP BY a.i_product, b.e_product_motifname, a.n_count_product,a.i_inbonm_item,a.i_so
			
			ORDER BY a.i_inbonm_item ASC ";
		*/

		$qstr	= " SELECT a.i_inbonm_item, a.i_product, b.e_product_motifname, a.n_count_product, a.i_so FROM tm_inbonm_item a
			
			INNER JOIN tm_inbonm d ON d.i_inbonm=a.i_inbonm
			INNER JOIN tr_product_motif b ON b.i_product_motif=a.i_product
			INNER JOIN tr_product_base c ON c.i_product_base=b.i_product
			
			WHERE a.i_inbonm='$ibonmmasuk' AND d.f_inbonm_cancel='f'
			
			GROUP BY a.i_product, b.e_product_motifname, a.n_count_product,a.i_inbonm_item,a.i_so ORDER BY a.i_inbonm_item ASC ";
								
		$query	= $db2->query($qstr);
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function cari_bonmmasuk($nobonmmasuk) {
		return $db2->query(" SELECT * FROM tm_inbonm WHERE i_inbonm_code=trim('$nobonmmasuk') AND f_inbonm_cancel='f' ");
	}	
	/* End 0f Edit session */
	
	/* Cancel Session */
	function mbatal($ibonm) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($ibonm)) {
			$qcariibonm	= $db2->query(" SELECT * FROM tm_inbonm WHERE i_inbonm='$ibonm' AND f_inbonm_cancel='f' ORDER BY i_inbonm DESC LIMIT 1 ");
			if($qcariibonm->num_rows()>0) {
				$row_bonm	= $qcariibonm->row();
				$inbonmcode	= $row_bonm->i_inbonm_code;
				$inbonm		= $row_bonm->i_inbonm;
				//$iso		= $row_bonm->i_so;
				
				$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				} else {
					$isoz	= "";
				}
				
				if($isoz!="") {
					$j=0;
					$iproductmotif	= array();
					$jml		= array();
					$totalharga	= array();
					$qtyawal	= array();
					$qtyakhir	= array();
					
					$qinbonm_item	= $db2->query(" SELECT * FROM tm_inbonm_item WHERE i_inbonm='$inbonm' ");
					if($qinbonm_item->num_rows()>0) {
						
						$result_inbonmitem	= $qinbonm_item->result();
						
						foreach($result_inbonmitem as $row_inbonmitem) {
							// 07-10-2014, ambil data2 per warna, reset stoknya -------------------------------
							$qinbonm_item_color	= $db2->query(" SELECT * FROM tm_inbonm_item_color 
													WHERE i_inbonm_item='$row_inbonmitem->i_inbonm_item' ");
					
							if($qinbonm_item_color->num_rows()>0) {
								$result_inbonmitem_color	= $qinbonm_item_color->result();
								foreach($result_inbonmitem_color as $rowxx) {
									if ($rowxx->i_color != '0') {
										$datacolor = " AND a.i_color = '".$rowxx->i_color."' ";
										$datacolor2 = $datacolor;
										$datacolor3 = " AND i_color = '".$rowxx->i_color."' ";
									}
									else {
										//$datacolor = " AND a.i_product_color = '".$rowxx->i_product_color."' ";
										$qxx = $db2->query(" SELECT i_color FROM tr_product_color WHERE i_product_color='$rowxx->i_product_color' ");
										if($qxx->num_rows()>0) {
											$rxx	= $qxx->row();
											$datacolor2	= " AND a.i_color= '".$rxx->i_color."' ";
											$datacolor3 = " AND i_color= '".$rxx->i_color."' ";
										}
									}
									$qsowarna	= $db2->query(" SELECT a.*, b.i_so_item FROM tm_stokopname_item_color a
												INNER JOIN tm_stokopname_item b ON a.i_so_item = b.i_so_item
												WHERE b.i_so='$row_inbonmitem->i_so' AND b.i_product='".$row_inbonmitem->i_product."' ".$datacolor2);
									if($qsowarna->num_rows()>0) {
										$rowsowarna	= $qsowarna->row();
										$qtyawalwarna	= $rowsowarna->n_quantity_awal;
										$qtyakhirwarna	= $rowsowarna->n_quantity_akhir;
										
										$back_qty_awalwarna	= $qtyawalwarna;
										$back_qty_akhirwarna	= $qtyakhirwarna-$rowxx->qty;
										
										if($back_qty_akhirwarna=='')
											$back_qty_akhirwarna = 0;
										
										$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir='$back_qty_akhirwarna'
														WHERE i_so_item = '$rowsowarna->i_so_item' ".$datacolor3);
									}else{
										$qtyawalwarna	= 0;
										$qtyakhirwarna	= 0;
									}
								} // end foreach warna
							} // end if per warna
							//-----------------------------------------------------------------------
							
							// after reset stok warna, hapus inbonm_item_color 08-01-2014
							$db2->query(" DELETE FROM tm_inbonm_item_color WHERE i_inbonm_item='".$row_inbonmitem->i_inbonm_item."' ");	
							// ------------------------------------------
					
							$iproductmotif[$j]	= $row_inbonmitem->i_product;
							$jml[$j]		= $row_inbonmitem->n_count_product;
							$iso			= $row_inbonmitem->i_so;
							/* 08082011
							$qstok_item	= $db2->query("SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$iproductmotif[$j]' ");
							*/
							
							//$qstok_item	= $db2->query(" SELECT a.n_quantity_awal, a.n_quantity_akhir, b.i_so, b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b ON b.i_so=a.i_so WHERE b.i_status_so='0' AND a.i_product='$iproductmotif[$j]' AND b.i_so='$iso' ORDER BY b.i_so DESC LIMIT 1 ");
							$qstok_item	= $db2->query(" SELECT a.n_quantity_awal, a.n_quantity_akhir, b.i_so, b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b ON b.i_so=a.i_so WHERE a.i_product='$iproductmotif[$j]' AND b.i_so='$iso' ORDER BY b.i_so DESC LIMIT 1 ");
							
							if($qstok_item->num_rows()>0) {
								
								$row_stok_item	= $qstok_item->row();
								
								$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
								$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;
								$isoz2			= $row_stok_item->i_so;
								
								
								//Disabled 25012011
								//$back_qty_awal[$j]	= $qtyakhir[$j]+$delivery[$j];
								//$back_qty_akhir[$j]	= $qtyawal[$j];
								
								//$back_qty_awal[$j]	= $qtyawal[$j];
								//$back_qty_akhir[$j]	= $qtyakhir[$j]-$jml[$j];
								$back_qty_akhir[$j]	= ($row_stok_item->n_quantity_akhir)-$jml[$j];
								
								/* 14072011
								$update_stokop_item[$j]	= array(
									'n_quantity_awal'=>$back_qty_awal[$j],
									'n_quantity_akhir'=>$back_qty_akhir[$j]
								);
								*/
								
								$update_stokop_item[$j]	= array(
									'n_quantity_akhir'=>$back_qty_akhir[$j]
								);
								
								$db2->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$isoz2,'i_product'=>$iproductmotif[$j]));
							}
							$j+=1;
						}
					}
				}
				
				$tbl_bonm	= array(
					'f_inbonm_cancel'=>'TRUE'
				);
				
				$db2->update('tm_inbonm',$tbl_bonm,array('i_inbonm'=>$ibonm,'f_inbonm_cancel'=>'f'));
				$base_url=base_url();
				print "<script>alert(\"Nomor Bon M Masuk : '\"+$inbonmcode+\"' telah dibatalkan, terimakasih.\");window.open(\" ../index \", \"_self\");</script>";
			} else {
				print "<script>alert(\"Maaf, Bon M Masuk tdk dpt dibatalkan.\");window.open(\"index\", \"_self\");</script>";
			}
		} else {
			print "<script>alert(\"Maaf, Bon M Masuk tdk dpt dibatalkan.\");window.open(\"index\", \"_self\");</script>";	
		}
	}
	/* End 0f Cancel Session */
	
	/* Pop Up Bon M Masuk */
	function lbonmmasuk() {	
		$db2=$this->load->database('db_external', TRUE);	
		/* 20092011
		return $db2->query("
			SELECT  a.i_inbonm AS iinbonm,
					b.i_inbonm_code AS inbonmcode,
					b.d_inbonm AS dinbonm
					
			FROM tm_inbonm_item a 
						
			RIGHT JOIN tm_inbonm b ON trim(cast(b.i_inbonm_code AS character varying))=trim(cast(a.i_inbonm AS character varying))
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)

			WHERE b.f_inbonm_cancel='f' AND b.f_manual='t'

			GROUP BY a.i_inbonm,b.i_inbonm_code,b.d_inbonm
								
			ORDER BY b.d_inbonm DESC ");
		*/ 
		return $db2->query("
			SELECT  b.i_inbonm_code AS inbonmcode,
					b.d_inbonm AS dinbonm,
					b.i_sj_manual AS isjmanual
					
			FROM tm_inbonm_item a 
						
			RIGHT JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)

			WHERE b.f_inbonm_cancel='f' AND b.f_manual='t'

			GROUP BY b.i_inbonm_code,b.d_inbonm,b.i_sj_manual
								
			ORDER BY b.d_inbonm DESC ");		
	}

	function lbonmmasukperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		/* 20092011
		$query	= $db2->query("
			
			SELECT  a.i_inbonm AS iinbonm,
					b.i_inbonm_code AS inbonmcode,
					b.d_inbonm AS dinbonm
					
			FROM tm_inbonm_item a 
						
			RIGHT JOIN tm_inbonm b ON trim(cast(b.i_inbonm_code AS character varying))=trim(cast(a.i_inbonm AS character varying))
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)

			WHERE b.f_inbonm_cancel='f' AND b.f_manual='t'

			GROUP BY a.i_inbonm,b.i_inbonm_code,b.d_inbonm
			
			ORDER BY b.d_inbonm DESC LIMIT ".$limit." OFFSET ".$offset);
		*/

		$query	= $db2->query(" SELECT b.i_inbonm_code AS inbonmcode,
				b.d_inbonm AS dinbonm,
				b.i_sj_manual AS isjmanual
					
			FROM tm_inbonm_item a
			
			RIGHT JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
			
			WHERE b.f_inbonm_cancel='f' AND b.f_manual='t'
			
			GROUP BY b.i_inbonm_code,b.d_inbonm,b.i_sj_manual
								
			ORDER BY b.d_inbonm DESC LIMIT ".$limit." OFFSET ".$offset);
					
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function flbonmmasuk($key) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($key)) {
			/* 20092011
			return $db2->query("
				SELECT  a.i_inbonm AS iinbonm,
						b.i_inbonm_code AS inbonmcode,
						b.d_inbonm AS dinbonm
						
				FROM tm_inbonm_item a 
				
				RIGHT JOIN tm_inbonm b ON trim(cast(b.i_inbonm_code AS character varying))=trim(cast(a.i_inbonm AS character varying))
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
	
				WHERE b.f_inbonm_cancel='f' AND b.i_inbonm_code='$key' AND b.f_manual='t'
	
				GROUP BY a.i_inbonm,b.i_inbonm_code,b.d_inbonm
									
				ORDER BY b.d_inbonm DESC ");
			*/ 
			return $db2->query("
				SELECT  b.i_inbonm_code AS inbonmcode,
						b.d_inbonm AS dinbonm,
						b.i_sj_manual AS isjmanual
						
				FROM tm_inbonm_item a 
				
				RIGHT JOIN tm_inbonm b ON b.i_inbonm=a.i_inbonm
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				
				WHERE b.f_inbonm_cancel='f' AND (b.i_inbonm_code='$key' OR b.i_sj_manual LIKE '$key%') AND b.f_manual='t'
				
				GROUP BY b.i_inbonm_code,b.d_inbonm,b.i_sj_manual
				
				ORDER BY b.d_inbonm DESC ");			
		}
	}		
	/* End 0f Pop Up Bon M Masuk */
	
	function lbarangjadiperpages2($limit,$offset,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset );
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi2($bulan,$tahun) {	
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
								
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan'
				
				ORDER BY b.i_product_motif ASC ");
	}

	function flbarangjadi2($key,$bulan,$tahun) {		
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		
		$qrystring	= " 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') 
				ORDER BY b.i_product_motif ASC ";
								
		return $db2->query($qrystring);
	}	

	function lbarangjadiperpages2opsi($limit,$offset,$bulan,$tahun) {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query("
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC LIMIT ".$limit." OFFSET ".$offset );
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi2opsi($bulan,$tahun) {	
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
								
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif ASC " );
	}

	function flbarangjadi2opsi($key,$bulan,$tahun) {		
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		
		$qrystring	= " 
				SELECT a.i_product_base AS iproduct, 
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b 
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product 
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') 
				ORDER BY b.i_product_motif ASC ";
								
		return $db2->query($qrystring);
	}

}
?>
