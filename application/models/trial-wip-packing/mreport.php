<?php
class Mreport extends CI_Model
{
	function Mreport()
	{

		parent::__construct();
	}

	function get_sowipunittanpalimit($id_unit, $bulan, $tahun)
	{
		$sql = " select * FROM tt_stok_opname_unit_packing WHERE TRUE ";

		if ($bulan != "00")
			$sql .= " AND bulan='$bulan' ";
		if ($tahun != "0")
			$sql .= " AND tahun='$tahun' ";
		if ($id_unit != '0')
			$sql .= " AND id_unit = '$id_unit' ";

		$query	= $this->db->query($sql);
		return $query->result();
	}

	function get_sowipunit($num, $offset, $id_unit, $bulan, $tahun)
	{
		$sql = " * FROM tt_stok_opname_unit_packing WHERE TRUE ";

		if ($bulan != "00")
			$sql .= " AND bulan='$bulan' ";
		if ($tahun != "0")
			$sql .= " AND tahun='$tahun' ";
		if ($id_unit != '0')
			$sql .= " AND id_unit = '$id_unit' ";
		$sql .= " ORDER BY tahun DESC, bulan DESC, id_unit ";

		$this->db->select($sql, false)->limit($num, $offset);
		$query = $this->db->get();

		$data_so = array();
		if ($query->num_rows() > 0) {
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$bln1 = $row1->bulan;
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";

				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;

				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di unit packing ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di unit packing ini ?";

				$query41 = $this->db->query("SELECT * FROM tm_periode_wip order by id desc limit 1");
				if ($query41->num_rows() > 0) {
					$hasilrow41 = $query41->row();
					$tahun_periode = $hasilrow41->i_year;
					$bulan_periode = $hasilrow41->i_month;
					$tanggal_periode = $hasilrow41->i_periode;
				} else {
					$tahun_periode = '';
					$bulan_periode = '';
					$tanggal_periode = '';
				}

				$data_so[] = array(
					'id' => $row1->id,
					'tahun_periode' => $tahun_periode,
					'bulan_periode' => $bulan_periode,
					'tanggal_periode' => $tanggal_periode,
					'bulan' => $row1->bulan,
					'nama_bln' => $nama_bln,
					'tahun' => $row1->tahun,
					'status_approve' => $row1->status_approve,
					'pesan' => $pesan,
					'tgl_update' => $row1->tgl_update,
					'id_unit' => $row1->id_unit,
					'kode_unit' => $kode_unit,
					'nama_unit' => $nama_unit
				);
			} // endforeach header
		} else {
			$data_so = '';
		}
		return $data_so;
	}

	function get_unit_packing()
	{
		$query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function get_sounitpacking($id_so)
	{
		$query	= $this->db->query(" SELECT a.id as id_header, a.id_unit, a.bulan, a.tahun, a.tgl_so, a.jenis_perhitungan_stok, 
					b.*, d.kode_brg, d.nama_brg
					FROM tt_stok_opname_unit_packing_detail b INNER JOIN tt_stok_opname_unit_packing a ON b.id_stok_opname_unit_packing = a.id 
					INNER JOIN tm_barang_wip d ON d.id = b.id_brg_wip
					WHERE a.id = '$id_so'
					ORDER BY d.kode_brg ");

		if ($query->num_rows() > 0) {
			$hasil = $query->result();

			$detail_bahan = array();
			foreach ($hasil as $row) {

				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0, 0, 0, $row->bulan, 1, $row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d', $timeStamp);    //get first day of the given month
				list($y, $m, $t)        =    explode('-', date('Y-m-t', $timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0, 0, 0, $m, $t, $y); //create time stamp of the last date of the give month
				$lastDay            =    date('d', $lastDayTimeStamp); // Find last day of the month

				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun - 1;
				} else {
					$bln_query = $row->bulan - 1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0" . $bln_query;
				}

				if ($row->bulan == '01')
					$nama_bln = "Januari";
				else if ($row->bulan == '02')
					$nama_bln = "Februari";
				else if ($row->bulan == '03')
					$nama_bln = "Maret";
				else if ($row->bulan == '04')
					$nama_bln = "April";
				else if ($row->bulan == '05')
					$nama_bln = "Mei";
				else if ($row->bulan == '06')
					$nama_bln = "Juni";
				else if ($row->bulan == '07')
					$nama_bln = "Juli";
				else if ($row->bulan == '08')
					$nama_bln = "Agustus";
				else if ($row->bulan == '09')
					$nama_bln = "September";
				else if ($row->bulan == '10')
					$nama_bln = "Oktober";
				else if ($row->bulan == '11')
					$nama_bln = "November";
				else if ($row->bulan == '12')
					$nama_bln = "Desember";

				$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_packing WHERE id = '$row->id_unit' ");
				$hasilrow = $query3->row();
				$kode_unit	= $hasilrow->kode_unit;
				$nama_unit	= $hasilrow->nama;

				$sqlxx = " SELECT a.*, c.nama FROM tt_stok_opname_unit_packing_detail_warna a 
							INNER JOIN tm_warna c ON a.id_warna = c.id
							WHERE a.id_stok_opname_unit_packing_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0) {
					$hasilxx = $queryxx->result();

					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_unit_packing a INNER JOIN tm_stok_unit_packing_warna b ON a.id = b.id_stok_unit_packing
							WHERE a.id_brg_wip = '$row->id_brg_wip' AND a.id_unit = '$row->id_unit'
							AND b.id_warna = '$rowxx->id_warna' ");

						if ($query3->num_rows() > 0) {
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						} else {
							$stok = '0';
						}

						// ===================================================

						$detail_warna[] = array(
							'id_warna' => $rowxx->id_warna,
							'nama_warna' => $rowxx->nama,
							'saldo_akhir' => $stok,
							'stok_opname' => $rowxx->jum_stok_opname
						);
					}
				} else
					$detail_warna = '';


				if ($row->tgl_so != '') {
					$pisah1 = explode("-", $row->tgl_so);
					$thn1 = $pisah1[0];
					$bln1 = $pisah1[1];
					$tgl1 = $pisah1[2];
					$tgl_so = $tgl1 . "-" . $bln1 . "-" . $thn1;
				} else
					$tgl_so = '';

				$detail_bahan[] = array(
					'id_header' => $row->id_header,
					'id_unit' => $row->id_unit,
					'kode_unit' => $kode_unit,
					'nama_unit' => $nama_unit,
					'bulan' => $row->bulan,
					'tahun' => $row->tahun,
					'tgl_so' => $tgl_so,
					'nama_bln' => $nama_bln,
					'jenis_perhitungan_stok' => $row->jenis_perhitungan_stok,
					'id' => $row->id,
					'id_brg_wip' => $row->id_brg_wip,
					'kode_brg_wip' => $row->kode_brg,
					'nama_brg_wip' => $row->nama_brg,
					'detail_warna' => $detail_warna
				);
				$detail_warna = array();
			}
		} else {
			$detail_bahan = '';
		}
		return $detail_bahan;
	}
	function get_detail_stokbrgbaru_unitpacking($id_so)
	{
		// ambil id_gudang dari acuan id_so
		$sqlxx = " SELECT id_unit FROM tt_stok_opname_unit_packing WHERE id='$id_so' ";
		$queryxx = $this->db->query($sqlxx);

		if ($queryxx->num_rows() > 0) {
			$hasilxx = $queryxx->row();
			$id_unit = $hasilxx->id_unit;
		} else
			$id_unit = 0;

		$sql = " SELECT b.id as id_brg_wip, b.kode_brg, b.nama_brg, a.id as id_stok
							FROM tm_barang_wip b 
							LEFT JOIN tm_stok_unit_packing a ON a.id_brg_wip = b.id
							WHERE a.id_unit = '$id_unit' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select b.id_brg_wip FROM tt_stok_opname_unit_packing a
							INNER JOIN tt_stok_opname_unit_packing_detail b ON a.id = b.id_stok_opname_unit_packing
							WHERE a.id = '$id_so')
							ORDER BY b.kode_brg  ";

		$query	= $this->db->query($sql);

		if ($query->num_rows() > 0) {
			$hasil = $query->result();

			$detail_bahan = array();
			foreach ($hasil as $row) {
				// stok terkini
				/*$sql2	= $this->db->query(" SELECT stok FROM tm_stok_unit_jahit WHERE id_brg_wip = '$row->id_brg_wip' ");
				if($sql2->num_rows() > 0) {
					$hasil2	= $sql2->row();
					$stok	= $hasil2->stok;
				}else{
					$stok	= 0;
				} */

				// stok warna
				$sql2 = " SELECT a.stok, a.id_warna, b.nama as nama_warna FROM tm_stok_unit_packing_warna a
						INNER JOIN tm_warna b ON a.id_warna = b.id WHERE a.id_stok_unit_packing = '$row->id_stok'
						ORDER BY b.nama";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0) {
					$hasil2 = $query2->result();

					$detail_warna = array();
					foreach ($hasil2 as $row2) {
						$detail_warna[] = array(
							'id_warna' => $row2->id_warna,
							'nama_warna' => $row2->nama_warna,
							'saldo_akhir' => $row2->stok,
							'stok_opname' => $row2->stok
						);
					}
				}

				$detail_bahan[] = array(
					'id' => 0,
					'id_brg_wip' => $row->id_brg_wip,
					'kode_brg_wip' => $row->kode_brg,
					'nama_brg_wip' => $row->nama_brg,
					'detail_warna' => $detail_warna
				);
			}
		} else {
			$detail_bahan = '';
		}
		return $detail_bahan;
	}

	// 09-11-2015

	function savesounitpacking(
		$id_so,
		$tgl_so,
		$unit_packing,
		$jenis_perhitungan_stok,
		$id_itemnya,
		$id_brg_wip,
		$id_warna,
		$stok_fisik
	) {
		$tgl = date("Y-m-d H:i:s");

		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		for ($xx = 0; $xx < count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			// 11-03-2015
			//$auto_saldo_akhir_warna[$xx] = trim($auto_saldo_akhir_warna[$xx]);

			$qtytotalstokfisik += $stok_fisik[$xx];
		} // end for
		// ---------------------------------------------------------------------
		$totalxx = $qtytotalstokfisik;

		//$totalxx = $stok_fisik;

		// ambil id detail id_stok_opname_unit_jahit_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail 
					where id_brg_wip = '$id_brg_wip' 
					AND id_stok_opname_unit_packing = '$id_so' ");
		if ($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		} else
			$iddetail = 0;

		//echo $id_itemnya."<br>";

		// 04-02-2016 SKRIP DIBAWAH INI GA DIPAKE LG

		// ======== update stoknya! =============
		// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar WIP di tanggal setelahnya
		// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya

		// ambil tgl terakhir di bln tsb
		/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/

		//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
		/*	$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '1' ";
			$query3	= $this->db->query($sql3);
						// AND a.tgl_bonm >= '$tglawalplus'
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_bgs = $hasilrow->jum_masuk;
							
				if ($masuk_bgs == '')
					$masuk_bgs = 0;
			}
			else
				$masuk_bgs = 0;
						
			//2. hitung brg masuk retur brg wip dari QC, dari tm_sjkeluarwip
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.=" AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_keluar = '3' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_returbrgwip = $hasilrow->jum_masuk;
							
				if ($masuk_returbrgwip == '')
					$masuk_returbrgwip = 0;
			}
			else
				$masuk_returbrgwip = 0;
						
			//3. hitung brg masuk pengembalian dari QC, dari tm_bonmkeluarcutting
			$sql3 = " SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
				$sql3.=" a.tgl_bonm > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_bonm >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' AND a.jenis_keluar = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$masuk_pengembalian = $hasilrow->jum_masuk;
							
				if ($masuk_pengembalian == '')
					$masuk_pengembalian = 0;
			}
			else
				$masuk_pengembalian = 0;
						
			$jum_masuk = $masuk_bgs+$masuk_returbrgwip+$masuk_pengembalian;
			// ------------------------------------------ END MASUK --------------------------------
						
			// 4. hitung brg keluar bagus, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs = $hasilrow->jum_keluar;
							
				if ($keluar_bgs == '')
					$keluar_bgs = 0;
			}
			else
				$keluar_bgs = 0;
						
			// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '2' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_perbaikan = $hasilrow->jum_keluar;
							
				if ($keluar_perbaikan == '')
					$keluar_perbaikan = 0;
			}
			else
				$keluar_perbaikan = 0;
						
			// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit = '$unit_jahit' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
				if ($keluar_retur_bhnbaku == '')
					$keluar_retur_bhnbaku = 0;
			}
			else
				$keluar_retur_bhnbaku = 0;
			
			// 30-10-2015
			// 7. hitung brg keluar bgs ke gudang jadi, dari tm_sjmasukgudangjadi
			$sql3 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukgudangjadi a 
									INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE ";
			if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '3')
				$sql3.=" a.tgl_sj > '$tgl_so' ";
			else if ($jenis_perhitungan_stok == '2' || $jenis_perhitungan_stok == '4')
				$sql3.=" a.tgl_sj >= '$tgl_so' ";
				
			$sql3.= " AND b.id_brg_wip = '$id_brg_wip'
					AND a.id_unit_jahit = '$unit_jahit' AND a.jenis_masuk = '1' ";
			$query3	= $this->db->query($sql3);
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$keluar_bgs_gudangjadi = $hasilrow->jum_keluar;
							
				if ($keluar_bgs_gudangjadi == '')
					$keluar_bgs_gudangjadi = 0;
			}
			else
				$keluar_bgs_gudangjadi = 0;
						
			$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku+$keluar_bgs_gudangjadi;
			// -------------------------------- END BARANG KELUAR -----------------------------------------------
			
			$jum_stok_akhir = $jum_masuk-$jum_keluar;
			$totalxx = $totalxx + $jum_stok_akhir; */

		//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
		$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_packing
							WHERE id_brg_wip = '$id_brg_wip' AND id_unit='$unit_packing' ");
		if ($query3->num_rows() == 0) {
			$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_packing ORDER BY id DESC LIMIT 1 ");
			if ($seqxx->num_rows() > 0) {
				$seqrow	= $seqxx->row();
				$id_stok	= $seqrow->id + 1;
			} else {
				$id_stok	= 1;
			}

			// 30-10-2015:
			// pada waktu input SO, field stok_bagus dan stok_perbaikan di-nol-kan lagi (utk duta). nanti diterapkan juga di perusahaan lain

			$data_stok = array(
				'id' => $id_stok,
				'id_brg_wip' => $id_brg_wip,
				'id_unit' => $unit_packing,
				'stok' => $totalxx,
				'tgl_update_stok' => $tgl,

			);
			$this->db->insert('tm_stok_unit_packing', $data_stok);
		} else {
			$hasilrow = $query3->row();
			$id_stok	= $hasilrow->id;

			$this->db->query(" UPDATE tm_stok_unit_packing SET stok = '$totalxx', 
						 tgl_update_stok = '$tgl' where id_brg_wip= '$id_brg_wip' AND id_unit='$unit_packing' ");
		}

		// 04-02-2016
		// stok_unit_jahit_warna
		// ----------------------------------------------
		for ($xx = 0; $xx < count($id_warna); $xx++) {
			$id_warna[$xx] = trim($id_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);

			$totalxx = $stok_fisik[$xx];

			//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
			$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_packing_warna WHERE id_warna = '" . $id_warna[$xx] . "'
							AND id_stok_unit_packing='$id_stok' ");
			if ($query3->num_rows() == 0) {
				// jika blm ada data di tm_stok_unit_jahit_warna, insert
				$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_packing_warna ORDER BY id DESC LIMIT 1 ");
				if ($seq_stokwarna->num_rows() > 0) {
					$seq_stokwarnarow	= $seq_stokwarna->row();
					$id_stok_warna	= $seq_stokwarnarow->id + 1;
				} else {
					$id_stok_warna	= 1;
				}
				//26-06-2014, pake $totalxx dari hasil perhitungan transaksi keluar/masuk
				$data_stok = array(
					'id' => $id_stok_warna,
					'id_stok_unit_packing' => $id_stok,
					'id_warna' => $id_warna[$xx],
					'stok' => $totalxx,
					'tgl_update_stok' => $tgl
				);
				$this->db->insert('tm_stok_unit_packing_warna', $data_stok);
			} else {
				$this->db->query(" UPDATE tm_stok_unit_packing_warna SET stok = '" . $totalxx . "', tgl_update_stok = '$tgl' 
						where id_warna= '" . $id_warna[$xx] . "' AND id_stok_unit_packing='$id_stok' ");
			}

			if ($id_itemnya != '0') {
				$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail_warna SET jum_stok_opname = '" . $stok_fisik[$xx] . "'
							WHERE id_stok_opname_unit_packing_detail='$iddetail' AND id_warna = '" . $id_warna[$xx] . "' ");
			}
		} // end for

		// 09-11-2015
		//$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$totalxx', 
		//			status_approve='t' where id = '$iddetail' ");

		if ($id_itemnya != '0') {
			$this->db->query(" UPDATE tt_stok_opname_unit_packing_detail SET jum_stok_opname = '$qtytotalstokfisik',
					status_approve='t' where id = '$iddetail' ");
		}

		if ($id_itemnya == '0') {
			$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail ORDER BY id DESC LIMIT 1 ");
			if ($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailnew	= $seqrow->id + 1;
			} else {
				$iddetailnew	= 1;
			}

			$databaru = array(
				'id' => $iddetailnew,
				'id_stok_opname_unit_packing' => $id_so,
				'id_brg_wip' => $id_brg_wip,
				'stok_awal' => 0,
				'jum_stok_opname' => $qtytotalstokfisik,
				'status_approve' => 't'
			);
			$this->db->insert('tt_stok_opname_unit_packing_detail', $databaru);

			// insert ke tabel SO warna
			for ($xx = 0; $xx < count($id_warna); $xx++) {
				$id_warna[$xx] = trim($id_warna[$xx]);
				$stok_fisik[$xx] = trim($stok_fisik[$xx]);

				$seq_warna	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_packing_detail_warna ORDER BY id DESC LIMIT 1 ");
				if ($seq_warna->num_rows() > 0) {
					$seqrow	= $seq_warna->row();
					$idbaru	= $seqrow->id + 1;
				} else {
					$idbaru	= 1;
				}

				$tt_stok_opname_unit_packing_detail_warna	= array(
					'id' => $idbaru,
					'id_stok_opname_unit_packing_detail' => $iddetailnew,
					'id_warna' => $id_warna[$xx],
					'jum_stok_opname' => $stok_fisik[$xx],
					'saldo_akhir' => 0
				);
				$this->db->insert('tt_stok_opname_unit_packing_detail_warna', $tt_stok_opname_unit_packing_detail_warna);
			} // end for
		} // end if item baru
		// ====================================================================================
	}
}
