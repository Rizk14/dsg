<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function statusfakturop($id) {
  	return $this->db->query(" SELECT b.id FROM tm_op a, tm_op_detail b WHERE a.id = b.id_op 
				AND b.status_op= 't' AND a.id= '$id' ");
  }
  
  function getAll($num, $offset, $supplier, $cari) {
	  $sql = " a.* FROM tm_op a WHERE TRUE ";
	  if ($supplier != 'xx')
		$sql.= " AND a.id_supplier = '$supplier' ";
	  if ($cari != "all")
		$sql.= " AND UPPER(a.no_op) like UPPER('%$cari%') ";
	  $sql.= " ORDER BY a.tgl_op DESC, no_op DESC ";
	  $this->db->select($sql, false)->limit($num,$offset);
	  $query = $this->db->get();
	  
	/*if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" a.* FROM tm_op a ORDER BY a.tgl_op DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" a.* FROM tm_op a
			WHERE a.kode_supplier = '$supplier' ORDER BY a.tgl_op DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($supplier != '0') {
			$this->db->select(" a.* FROM tm_op a WHERE
			a.kode_supplier = '$supplier' AND UPPER(a.no_op) like UPPER('%$cari%') ORDER BY a.tgl_op DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" a.* FROM tm_op a WHERE 
			UPPER(a.no_op) like UPPER('%$cari%') ORDER BY a.tgl_op DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	} */
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$no_ppnya = ""; $list_no_ppnya = "";
				$id_detailnya = ""; 
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_op_detail WHERE id_op = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						//----------------
						// 12-07-2012, modifikasi pake pengecekan id_pp_detail, apakah ada atau ga
						// 17-06-2015 DIKOMEN
						/*if ($row1->id_pp != 0) {
							$query3	= $this->db->query(" SELECT id, id_brg FROM tm_pp_detail WHERE id_pp = '$row1->id_pp' ");
							$hasil3= $query3->result();
							foreach ($hasil3 as $row3) {
								if ($row2->kode_brg == $row3->kode_brg)
									$id_detailnya.= $row3->id."-";
							}
						} */
						//else {
							$id_detailnya.= $row2->id_pp_detail."-";
						//}
						//----------------
						
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg_supplier
										FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							$nama_brg_sup	= $hasilrow->nama_brg_supplier;
							//$satuan	= $hasilrow->nama_satuan;
							
							if ($nama_brg_sup != '')
								$nama_brg = $nama_brg_sup;
						}
						else {
							$kode_brg	= '';
							$nama_brg_sup = '';
							//$satuan	= '';
						}
						
						// 01-10-2015 nama satuan dari tabel op_detail
						$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$satuan	= '';
						}
						
						/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, 
											tm_pembelian b, tm_op c, tm_op_detail d 
											WHERE a.id_pembelian = b.id AND b.id_op like '%$row1->id%'
											AND b.status_aktif = 't'
											AND c.id = '%$row1->id%' AND c.id = d.id_op AND d.kode_brg = '".$this->input->post('kode_'.$i, TRUE)."' ");
						 * 
						 * */
						
						// 19-12-2011, cek
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_op_detail = '".$row2->id."' ");
						if ($query4->num_rows() > 0){
							//if ($row2->is_satuan_lain == 'f')
								$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a 
											INNER JOIN tm_pembelian b ON a.id_pembelian = b.id
											WHERE a.id_op_detail = '$row2->id' 
											AND b.status_aktif = 't'
											AND a.id_brg = '$row2->id_brg' ");
								// 04-07-2015 dikeluarin dari query: AND b.id_supplier = '$row1->id_supplier'
							/*else
								$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a 
											INNER JOIN tm_pembelian b ON a.id_pembelian = b.id
											WHERE a.id_op_detail = '$row2->id' 
											AND b.status_aktif = 't'
											AND a.id_brg = '$row2->id_brg'
											AND b.id_supplier = '$row1->id_supplier' "); */
							
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
						}//cek didieu 240212
						else {
							$jum_beli = 0;
							// 17-06-2015 DIKOMEN
						/*	if ($row2->is_satuan_lain == 'f')
								$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, 
											tm_pembelian b, tm_op c, tm_op_detail d
											WHERE a.id_pembelian = b.id AND b.id_op like '%$row1->id%' 
											AND b.status_aktif = 't' AND c.id = d.id_op
											AND c.id = '$row1->id'
											AND d.kode_brg = '$row2->kode_brg' AND d.kode_brg = a.kode_brg
											AND b.kode_supplier = '$row1->kode_supplier' ");
							else
								$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, 
											tm_pembelian b, tm_op c, tm_op_detail d
											WHERE a.id_pembelian = b.id AND b.id_op like '%$row1->id%' 
											AND b.status_aktif = 't' AND c.id = d.id_op
											AND c.id = '$row1->id'
											AND d.kode_brg = '$row2->kode_brg' AND d.kode_brg = a.kode_brg
											AND b.kode_supplier = '$row1->kode_supplier' "); */
						}
						
						$sisanya = $row2->qty-$jum_beli;
						if ($sisanya < 0)
							$sisanya = 0;
						
						// 19-12-2011, cek
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_op_detail = '".$row2->id."' ");
						if ($query4->num_rows() > 0){
							/*echo "SELECT count(b.id) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b
									ON a.id_pembelian = b.id WHERE b.status_aktif = 't'
									AND a.id_op_detail = '$row2->id' AND b.status_aktif = 't'
									AND a.id_brg = '$row2->id_brg' <br>"; */
							$query3	= $this->db->query(" SELECT count(b.id) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b
									ON a.id_pembelian = b.id WHERE b.status_aktif = 't'
									AND a.id_op_detail = '$row2->id' 
									AND a.id_brg = '$row2->id_brg' ");
							// 04-07-2015 dikeluarin dari query: AND b.id_supplier = '$row1->id_supplier'
							$hasilrow = $query3->row();
							$count_beli = $hasilrow->jum;
						}
						else {
							/*$query3	= $this->db->query(" SELECT count(b.id) as jum FROM tm_pembelian_detail a, tm_pembelian b,
									tm_op c, tm_op_detail d
									WHERE b.status_aktif = 't' AND a.id_pembelian = b.id AND c.id = d.id_op 
									AND b.id_op LIKE '%$row1->id%' AND c.id = '$row1->id'
									AND a.kode_brg = '$row2->kode_brg' AND a.kode_brg = d.kode_brg
									AND b.kode_supplier = '$row1->kode_supplier' "); */
							$count_beli = 0;
						}
						
						if ($row2->is_satuan_lain == 't') {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
							$hasilrow = $query3->row();
							$sat_lain = $hasilrow->nama;
						}
						else
							$sat_lain = '';
						
						// 11-07-2012
						$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$row2->id_pp_detail' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							//ambil nomor PP
							$query3	= $this->db->query(" SELECT a.no_pp FROM tm_pp_detail b INNER JOIN tm_pp a ON a.id=b.id_pp 
												WHERE b.id = '$row2->id_pp_detail' ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$no_pp	= $hasilrow->no_pp;
							 }
							 else
								$no_pp = '';
							if ($no_ppnya != $no_pp) {
								$list_no_ppnya.= $no_pp.";";
							}
							$no_ppnya = $no_pp;
						}
						
						// 22-09-2014
						$subtotal = $row2->qty*$row2->harga;
						
						$detail_fb[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $row2->nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'diskon'=> $row2->diskon,
												'subtotal'=> $subtotal,
												'keterangan'=> $row2->keterangan,
												'status_op'=> $row2->status_op,
												'jum_beli'=> $jum_beli,
												'count_beli'=> $count_beli,
												'sisanya'=> $sisanya,
												'sat_lain'=> $sat_lain
											);
					}
				}
				else {
					$detail_fb = '';
				}
				// ambil data nama supplier
				if ($row1->id_supplier != '0') {
					$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
				}
				else {
					$kode_supplier	= '';
					$nama_supplier	= 'Lain-lain';
				}
				//ambil nomor PP, 11-07-2012
				if ($list_no_ppnya == '') {
					$query3	= $this->db->query(" SELECT no_pp FROM tm_pp WHERE id = '$row1->id_pp' ");
					 if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$list_no_ppnya	= $hasilrow->no_pp;
					 }
					 else
						$list_no_ppnya = '';
				}
				 
				 // berdasarkan id_op, cek jumlah item brg yg status_op = 'f'. jika > 0, maka munculkan link edit per OP
				  
				 $query3	= $this->db->query(" SELECT count(id) as jum_item FROM tm_op_detail 
								WHERE id_op = '$row1->id' AND status_op = 'f' ");
				 if ($query3->num_rows() > 0) {
					$hasilrow = $query3->row();
					$jum_item_f	= $hasilrow->jum_item;
				 }
				 else
					$jum_item_f = 0;
			
				// 17-06-2015 WARNING, INI UTK APA YA? bikin ulang aja pake inner join
				/*$sql = "SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b,
												tm_op c, tm_op_detail d
								WHERE a.id_pembelian = b.id AND b.id_op like '%$row1->id%' AND b.status_aktif = 't'
								AND c.id = '$row1->id' AND c.id = d.id_op AND d.kode_brg = a.kode_brg
								AND b.kode_supplier = '$row1->kode_supplier'"; */
				$sql = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b ON a.id_pembelian = b.id
						INNER JOIN tm_op_detail d ON a.id_op_detail = d.id 
						INNER JOIN tm_op c ON c.id = d.id_op 
						WHERE b.status_aktif = 't'
						AND b.id_supplier = '$row1->id_supplier' AND c.id = '$row1->id' ";
								
				$query3	= $this->db->query($sql);
				$hasilrow = $query3->row();
				$jum_beli2 = $hasilrow->jum;
				
				// 22-09-2014
				$query3	= $this->db->query(" SELECT sum((qty*harga)-diskon) as grandtotal FROM tm_op_detail 
								WHERE id_op = '$row1->id' ");
				 if ($query3->num_rows() > 0) {
					$hasilrow = $query3->row();
					$grandtotal	= $hasilrow->grandtotal;
				 }
				 else
					$grandtotal = 0;
												 
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_pp'=> $list_no_ppnya,	
											'no_op'=> $row1->no_op,
											'tgl_op'=> $row1->tgl_op,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_op'=> $row1->status_op,
											'status_edit'=> $row1->status_edit,
											'status_aktif'=> $row1->status_aktif,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											'jum_item_f'=> $jum_item_f,
											'jum_beli2'=> $jum_beli2,
											'grandtotal'=> $grandtotal,
											// 02-07-2015
											'jenis_pembelian'=> $row1->jenis_pembelian
											);
				$detail_fb = array();
				$id_detailnya = "";
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  //modif 22-09-2014
  function getAlltanpalimit($supplier, $cari){
	//if ($cari == "all") {
	//	if ($supplier == '0')
			/*$query	= $this->db->query(" SELECT a.* FROM tm_op a INNER JOIN tm_pp b ON a.id_pp = b.id 
										WHERE b.id <> '0' "); */
	//		$query	= $this->db->query(" SELECT a.* FROM tm_op a ");
	//	else
			/*$query	= $this->db->query(" SELECT a.* FROM tm_op a INNER JOIN tm_pp b ON a.id_pp = b.id 
									WHERE b.id <> '0' 
									AND a.kode_supplier = '$supplier' "); */
	//		$query	= $this->db->query(" SELECT a.* FROM tm_op a 
	//								WHERE a.kode_supplier = '$supplier' ");
	/*}
	else {
		if ($supplier != '0')
			$query	= $this->db->query(" SELECT a.* FROM tm_op a 
							WHERE a.kode_supplier = '$supplier' AND UPPER(a.no_op) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT a.* FROM tm_op a 
						WHERE UPPER(a.no_op) like UPPER('%$cari%') ");
	} */
	
	$sql = " SELECT a.* FROM tm_op a WHERE TRUE ";
	  if ($supplier != 'xx')
		$sql.= " AND a.id_supplier = '$supplier' ";
	  if ($cari != "all")
		$sql.= " AND UPPER(a.no_op) like UPPER('%$cari%') ";
    $query	= $this->db->query($sql);
    return $query->result();  
  }
      
  function cek_data($no_faktur){
    $this->db->select("id from tm_op WHERE no_op = '$no_faktur' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($no_fb,$tgl_fb, $id_supplier,$jenis_pembelian, $ket,
				$hide_pkp, $hide_tipe_pajak, $id_pp_detail, $id_brg, $nama, $id_satuan,
				$qty, $harga, $diskon, $harga_lama, 
				$keterangan, $id_pp){  
	// 25-06-2015 $satuan_lain DIBUANG
	$uid_update_by = $this->session->userdata('uid');
    $tgl = date("Y-m-d H:i:s");
    
    $satuan_lain = 0;
    //if ($satuan_lain == 0)
		$is_satuan_lain = 'f';
	//else
	//	$is_satuan_lain = 't';
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_op WHERE no_op = '$no_fb' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_op
			$data_header = array(
			  'no_op'=>$no_fb,
			  'tgl_op'=>$tgl_fb,
			  //'id_pp'=>$id_pp,
			  'id_supplier'=>$id_supplier,
			  'jenis_pembelian'=>$jenis_pembelian,
			  'keterangan'=>$ket,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'uid_update_by'=>$uid_update_by
			);
			$this->db->insert('tm_op',$data_header);
			
			$this->db->query(" UPDATE tm_pp SET status_edit = 't' where id= '$id_pp' ");
		}
			
			// ambil data terakhir di tabel tm_op
			$query2	= $this->db->query(" SELECT id FROM tm_op ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_op	= $hasilrow->id; //echo $idnya; die();
			
			if ($id_brg!='' && $qty!='') {
				// 09-07-2012, cek apakah di tm_op_detail itu ada data relasi ke tm_pp_detail ?
				
				//cek qty, jika lebih besar maka otomatis disamakan dgn sisa qty di PP
				$query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a INNER JOIN tm_pp b ON a.id_pp = b.id
							WHERE a.id = '$id_pp_detail' ");
				$hasilrow = $query3->row();
				$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya/id_pp_detail
				
				// 10-07-2012
				$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$id_pp_detail' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
							WHERE a.id_pp_detail = '$id_pp_detail' ");
					$hasilrow = $query3->row();
					$jum_op = $hasilrow->jum;
				}
				else {
					// 17-06-2015 DIKOMEN, GA DIPAKE LG
					/*$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
							WHERE a.id_op = b.id AND b.id_pp = '$id_pp' AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$jum_op = $hasilrow->jum; */
					$jum_op = 0;
				}
				
				// 19-12-2011, cek apakah ada id_pp_detail
				$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_pp_detail = '".$id_pp_detail."' ");
				if ($query4->num_rows() > 0){
					// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
					// 25-06-2015 SATUAN LAIN GA DIPAKE!
					//if ($is_satuan_lain == 'f')
						$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b ON a.id_pembelian = b.id
									WHERE a.id_pp_detail = '$id_pp_detail'
									AND b.status_aktif = 't' AND a.id_brg = '$id_brg' ";
					/*else
						$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b ON a.id_pembelian = b.id
									WHERE a.id_pp_detail = '$id_pp_detail'
									AND b.status_aktif = 't' AND a.id_brg = '$id_brg' "; */
					
					$query3	= $this->db->query($sql3);
					$hasilrow = $query3->row();
					$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
				}
				else
					$jum_ppsj = 0;
				// 17-06-2015 DIKOMEN GA DIPAKE
				/*
				else {
					// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
					if ($is_satuan_lain == 'f')
						$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
									WHERE a.id_pembelian = b.id AND c.id = d.id_pp ";
					else
						$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
									WHERE a.id_pembelian = b.id AND c.id = d.id_pp ";
									
					$sql3.= " AND b.id_pp like '%$id_pp%' AND d.id = '$id_pp_detail' AND b.status_aktif = 't' ";
					$sql3.= " AND a.kode_brg = '$kode' AND d.kode_brg = a.kode_brg ";
				} */
		
				
				// #######################################################
				
				$jumtot = $jum_op+$qty +$jum_ppsj;
				if ($jumtot > $qty_pp)
					$qty = $qty_pp-$jum_op-$jum_ppsj;
				
				// jika semua data tdk kosong, insert ke tm_op_detail
				$data_detail = array(
					'id_brg'=>$id_brg,
					'qty'=>$qty,
					'harga'=>$harga,
					// 02-07-2015
					'diskon'=>$diskon,
					'keterangan'=>$keterangan,
					'id_op'=>$id_op,
					'is_satuan_lain'=>$is_satuan_lain,
					'satuan_lain'=>$satuan_lain,
					'id_pp_detail'=>$id_pp_detail,
					// 01-10-2015
					'id_satuan'=>$id_satuan,
					// 29-10-2015 ga pake db escape lagi
					//'nama_brg'=>$this->db->escape_str($nama)
					'nama_brg'=>$nama
				);
				$this->db->insert('tm_op_detail',$data_detail);
				
				if ($id_supplier != '0') {
					$query2	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '$id_brg'
									AND id_supplier = '$id_supplier' AND id_satuan = '$id_satuan' ");
					if ($query2->num_rows() > 0){
						if ($harga != $harga_lama)
							$this->db->query(" UPDATE tm_harga_brg_supplier SET harga = '$harga',
									tgl_update = '$tgl' where id_brg= '$id_brg' AND id_supplier= '$id_supplier'
									AND id_satuan = '$id_satuan' ");
					}
					else
						$this->db->query(" INSERT INTO tm_harga_brg_supplier (id_brg, id_supplier, id_satuan, 
										harga, tgl_input, tgl_update) 
										VALUES ('$id_brg','$id_supplier', '$id_satuan', '$harga', '$tgl', '$tgl') ");
					
					// 02-10-2015 dikomen			
					//if ($harga != $harga_lama)
					//	$this->db->query(" INSERT INTO tt_harga (id_brg, id_supplier, harga, tgl_input) 
					//					VALUES ('$id_brg','$id_supplier', '$harga', '$tgl') ");
				}
			}
				// cek sum qty di OP. jika sum di OP = qty PP, maka update status PPnya menjadi true
				$query3	= $this->db->query(" SELECT qty FROM tm_pp_detail WHERE id= '$id_pp_detail' ");
				$hasilrow = $query3->row();
				$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
				
				// 10-07-2012
				$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$id_pp_detail' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b ON a.id_op = b.id
							WHERE a.id_pp_detail = '$id_pp_detail' ");
					$hasilrow = $query3->row();
					$jum_op = $hasilrow->jum;
				}
				else {
					/*$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
							WHERE a.id_op = b.id AND b.id_pp = '$id_pp' AND a.kode_brg = '$kode' ");
					$hasilrow = $query3->row();
					$jum_op = $hasilrow->jum; */
					$jum_op = 0;
				} // ini sum qty di OP berdasarkan kode brg tsb
				
				// 19-12-2011, cek apakah ada id_pp_detail
				$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_pp_detail = '".$id_pp_detail."' ");
				if ($query4->num_rows() > 0){
					// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
					// 25-06-2015 SATUAN LAIN GA DIPAKE!
					//if ($is_satuan_lain == 'f')
						$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b ON a.id_pembelian = b.id
								WHERE b.status_aktif = 't'
								AND a.id_pp_detail = '$id_pp_detail' AND a.id_brg = '$id_brg' ";
					/*else
						$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b ON a.id_pembelian = b.id
								WHERE b.status_aktif = 't' 
								AND a.id_pp_detail = '$id_pp_detail' AND a.id_brg = '$id_brg' "; */
					
					$query3	= $this->db->query($sql3);
					$hasilrow = $query3->row();
					$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
				}
				else {
					// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
				/*	if ($is_satuan_lain == 'f')
						$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
								WHERE a.id_pembelian = b.id AND c.id = d.id_pp AND b.status_aktif = 't' ";
					else
						$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
								WHERE a.id_pembelian = b.id AND c.id = d.id_pp AND b.status_aktif = 't' ";
					
					$sql3.= " AND b.id_pp like '%$id_pp%' AND c.id = '$id_pp' ";
					$sql3.= " AND a.kode_brg = '$kode' AND a.kode_brg = d.kode_brg ";
					
					$query3	= $this->db->query($sql3);
					$hasilrow = $query3->row();
					$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
					*/
					$jum_ppsj = 0;
				}
				
				// #######################################################
				
				if ($qty_pp <= $jum_op+$jum_ppsj) {
					$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 't' where id= '$id_pp_detail' ");
					//cek di tabel tm_pp_detail, apakah status_faktur sudah 't' semua?
					$this->db->select("id from tm_pp_detail WHERE status_faktur = 'f' AND id_pp = '$id_pp' ", false);
					$query = $this->db->get();
					//jika sudah t semua, maka update tabel tm_pp di field status_faktur menjadi t
					if ($query->num_rows() == 0){
						$this->db->query(" UPDATE tm_pp SET status_faktur = 't' where id= '$id_pp' ");
					}
				}
  }
    
  function delete($id, $list_brg){    
	if ($list_brg != '') {
		$id_brg = explode("-", $list_brg);
		
		// cek di tm_op, apakah msh ada op lain dgn id_pp tsb
		/*$query2	= $this->db->query(" SELECT * FROM tm_op WHERE id_pp = '$id_pp' AND id <> '$kode' ");
		if ($query2->num_rows() == 0){ // jika tidak ada lagi, maka update status_faktur = 'f'
			//update status_faktur di tm_pp menjadi FALSE
			$this->db->query(" UPDATE tm_pp SET status_faktur = 'f', status_edit = 'f' where id= '$id_pp' "); 
		} */
			
		//update status_faktur = 'f' di tm_pp_detail, ini peritem sesuai di OP tsb. dan juga update status_faktur = 'f' di tm_pp
		foreach($id_brg as $row1) {
			if ($row1 != '') {
				$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 'f' where id= '$row1' ");
				
				$query2	= $this->db->query(" SELECT id_pp FROM tm_pp_detail WHERE id = '$row1' ");
				$hasilrow = $query2->row();
				$id_pp	= $hasilrow->id_pp;
		
				$this->db->query(" UPDATE tm_pp SET status_faktur = 'f' where id= '$id_pp' ");
			}
		}
	} // end if id_pp_detail
	
	$this->db->delete('tm_op_detail', array('id_op' => $id));
	$this->db->delete('tm_op', array('id' => $id));
  }
  
  // 18 jan 2012
  function updatestatus($id, $aksi){    
	  if ($aksi == "on")
		$this->db->query(" UPDATE tm_op SET status_aktif = 't' where id= '$id' ");
	  else
		$this->db->query(" UPDATE tm_op SET status_aktif = 'f' where id= '$id' ");
  }
  
  function get_pp($cari) {
	  // ambil data PP yg status_faktur = FALSE
	if ($cari == "all") {		
		//$this->db->select(" * FROM tm_pp WHERE status_faktur = 'f' order by tgl_pp DESC, no_pp DESC ", false)->limit($num,$offset);
		$this->db->select(" * FROM tm_pp WHERE status_faktur = 'f' AND status_aktif = 't' order by tgl_pp DESC, no_pp DESC ", false);
		$query = $this->db->get();
	}
	else {
		$this->db->select(" * FROM tm_pp WHERE status_faktur = 'f' AND status_aktif = 't' AND UPPER(no_pp) like UPPER('%$cari%') order by tgl_pp DESC, no_pp DESC ", false);
		$query = $this->db->get();
	}
		$data_pp = array();
		$detail_pp = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
							
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pp_detail WHERE id_pp = '$row1->id' AND status_faktur = 'f' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a
										INNER JOIN tm_satuan b ON a.satuan = b.id
										WHERE a.id = '$row2->id_brg' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						// 12 nov 2011, jika ada satuan lain, maka yg jadi patokan utk OP adalah satuan lain
						if ($row2->is_satuan_lain == 't') {
							//$sql = "SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain'"; echo $sql;
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$satuan = $hasilrow->nama;
							}
						}
												
						// -------------------
						
						/* $query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a, tm_pp b 
									WHERE b.id = '$id_pp' AND a.id_pp = b.id AND a.kode_brg = '$kode' ");
						$hasilrow = $query3->row();
						$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
						*/
						
						//09-07-2012, cek apakah di tm_op udh pake id_pp_detail atau blm. kalo blm, maka pake query lama
						$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$row2->id' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE a.id_pp_detail = '$row2->id' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan id_pp_detail
						}
						else {
							 // 16-06-2015 DIKOMEN, GA DIPAKE
							/*$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$row1->id' AND a.kode_brg = '$row2->kode_brg' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
							*/
							$jum_op = 0;
						}
						
						// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_pp_detail = '".$row2->id."' ");
						if ($query4->num_rows() == 0){
							// 16-06-2015 DIKOMEN, SOALNYA GA PERLU
							// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
						/*	if ($row2->is_satuan_lain == 'f') {
								$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
											WHERE a.id_pembelian = b.id AND c.id = d.id_pp AND b.status_aktif = 't' ";
							}
							else {
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
											WHERE a.id_pembelian = b.id AND c.id = d.id_pp AND b.status_aktif = 't' ";
							}

							$sql3.= " AND b.id_pp like '%$row1->id%' AND c.id = '$row1->id' ";
							$sql3.= " AND a.kode_brg = '$row2->kode_brg' AND a.kode_brg = d.kode_brg ";
								
							$query3	= $this->db->query($sql3);
							$hasilrow = $query3->row();
							$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
							*/
							$jum_ppsj = 0;
						}
						else {
							if ($row2->is_satuan_lain == 'f') {
								$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b
											ON a.id_pembelian = b.id WHERE a.id_pp_detail = '$row2->id' AND b.status_aktif = 't' ";
							}
							else {
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b
											ON a.id_pembelian = b.id WHERE a.id_pp_detail = '$row2->id' AND b.status_aktif = 't' ";
							}								
							$query3	= $this->db->query($sql3);
							$hasilrow = $query3->row();
							$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan id_pp_detail tsb
						}
						// =========================== end 09-07-2012 ====================================================
						// #######################################################
						
						$qty = $row2->qty-$jum_op-$jum_ppsj;
						// 13-08-2015
						if ($qty <= 0) {
							$this->db->query(" UPDATE tm_pp_detail SET status_faktur = 't' WHERE id='".$row2->id."' ");
							
							// 13-08-2015, cek apakah sudah t semua. jika sudah, maka update headernya juga							
							$queryxx	= $this->db->query(" SELECT id FROM tm_pp_detail WHERE id_pp = '".$row1->id."' AND status_faktur = 'f' ");
							if ($queryxx->num_rows() == 0){
								$this->db->query(" UPDATE tm_pp SET status_faktur = 't' WHERE id='".$row1->id."' ");
							}
						}
						//--------------------
						// 14-08-2015
						if ($qty > 0) {
							$detail_pp[] = array(	'id'=> $row2->id,
												'id_brg'=> $row2->id_brg,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan,
												'id_pp'=> $row1->id,
												'no_pp'=> $row1->no_pp
											);
						}
					}
				}
				else {
					$detail_pp = '';
				}
				
				$query3	= $this->db->query(" SELECT kode, nama FROM tm_bagian WHERE id = '$row1->id_bagian' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_bagian = $hasilrow->kode; 
					$nama_bagian = $hasilrow->nama; 
				}
				
				$data_pp[] = array(			'id'=> $row1->id,	
											'nama_bagian'=> $nama_bagian,
											'no_pp'=> $row1->no_pp,
											'tgl_pp'=> $row1->tgl_pp,
											'tgl_update'=> $row1->tgl_update,
											'detail_pp'=> $detail_pp
											);
				$detail_pp = array();
			} // endforeach header
		}
		else {
			$data_pp = '';
		}
		return $data_pp;
  }
  
  function get_pptanpalimit($cari){
	if ($cari == "all") {
		//$query = $this->db->getwhere('tm_pp',array('status_faktur'=>"f", 'status_aktif'=>"t" ));
		$query	= $this->db->query(" SELECT * FROM tm_pp WHERE status_faktur = 'f' AND status_aktif = 't' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_pp WHERE status_faktur = 'f' AND status_aktif = 't' AND UPPER(no_pp) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  //function get_detail_pp($id_pp, $list_brg, $kode_supplier){
  function get_detail_pp($list_brg, $id_supplier){
    $detail_pp = array();
        
    foreach($list_brg as $row1) {
		if ($row1 != '') {
			$query2	= $this->db->query(" SELECT a.id, a.no_pp, b.id_brg, b.nama_brg, b.id_satuan, b.qty, b.keterangan, 
								b.is_satuan_lain, b.satuan_lain 
								FROM tm_pp_detail b INNER JOIN tm_pp a ON a.id=b.id_pp WHERE b.id = '$row1' ");
			if ($query2->num_rows() > 0) {
				$hasilrow = $query2->row();
				$id_pp	= $hasilrow->id;
				$no_pp	= $hasilrow->no_pp;
				$id_brg	= $hasilrow->id_brg;
				$nama_brg	= $hasilrow->nama_brg;
				$id_satuan	= $hasilrow->id_satuan;
				$qty	= $hasilrow->qty;
				$ket	= $hasilrow->keterangan;
				$is_satuan_lain	= $hasilrow->is_satuan_lain;
				$satuan_lain	= $hasilrow->satuan_lain;
			
				$query2	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE id_brg = '$id_brg'
									AND id_supplier = '$id_supplier' AND id_satuan = '$id_satuan' ");
				if ($query2->num_rows() > 0){
					$hasilrow = $query2->row();
					$harga	= $hasilrow->harga;
				}
				else
					$harga = 0;
						
						//09-07-2012, cek apakah di tm_op udh pake id_pp_detail atau blm. kalo blm, maka pake query lama
						$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$row1' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a INNER JOIN tm_op b 
									ON a.id_op = b.id WHERE a.id_pp_detail = '$row1' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan id_pp_detail
						}
						else {
							/*$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$id_pp' AND a.kode_brg = '$kode_brg' ");
							$hasilrow = $query3->row();
							$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
							*/
							$jum_op = 0;
						}
						// =========================== end 09-07-2012 ====================================================
						
						// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_pp_detail = '".$row1."' ");
						if ($query4->num_rows() > 0){
							// 04-07-2015 DIKOMEN SOALNYA UDH GA DIPAKE
							//if ($satuan_lain == 'f') {
								$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b
										ON a.id_pembelian = b.id WHERE b.status_aktif = 't' ";
							/*}
							else {
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a INNER JOIN tm_pembelian b
										ON a.id_pembelian = b.id WHERE b.status_aktif = 't' ";
							} */
							$sql3.= " AND a.id_pp_detail = '$row1' ";
							
							$query3	= $this->db->query($sql3);
							$hasilrow = $query3->row();
							$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan pp detail tsb pake id_pp_detail
						}
						else
							$jum_ppsj = 0;
						// 16-06-2015 DIKOMEN SOALNYA UDH GA PERLU LAGI
						/*else {
							if ($satuan_lain == 'f') {
							$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
									WHERE a.id_pembelian = b.id AND c.id = d.id_pp AND b.status_aktif = 't' ";
							}
							else {
								$sql3 = " SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b, tm_pp c, tm_pp_detail d 
										WHERE a.id_pembelian = b.id AND c.id = d.id_pp AND b.status_aktif = 't' ";
							}
							$sql3.= " AND b.id_pp like '%$id_pp%' AND c.id = '$id_pp' ";
							$sql3.= " AND a.kode_brg = '$kode_brg' AND a.kode_brg = d.kode_brg ";
							
							$query3	= $this->db->query($sql3);
							$hasilrow = $query3->row();
							$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
						} */
						
						// #######################################################
						
						$qty = $qty-$jum_op-$jum_ppsj;
			
				$query2	= $this->db->query(" SELECT a.kode_brg FROM tm_barang a WHERE a.id = '$id_brg' ");
				$hasilrow = $query2->row();
				$kode_brg	= $hasilrow->kode_brg;
				//$nama_brg	= $hasilrow->nama_brg;
				//$id_satuan	= $hasilrow->satuan;
				//$nama_satuan	= $hasilrow->nama_satuan;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$id_satuan' ");
				$hasilrow = $query3->row();
				$nama_satuan = $hasilrow->nama;
				
				/*if ($is_satuan_lain == 't') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$satuan_lain' ");
					$hasilrow = $query3->row();
					$nama_sat_lain = $hasilrow->nama;
				}
				else */
					$nama_sat_lain = '';
		
				$detail_pp[] = array(	'id'=> $row1,
										'id_brg'=> $id_brg,
										'kode_brg'=> $kode_brg,
										'nama'=> $nama_brg,
										'id_satuan'=> $id_satuan,
										'nama_satuan'=> $nama_satuan,
										'sat_lain'=> $satuan_lain,
										'nama_sat_lain'=> $nama_sat_lain,
										'harga'=> $harga,
										'qty'=> $qty,
										'keterangan'=> $ket,
										'no_pp'=> $no_pp,
										'id_pp'=> $id_pp
								);
			} // end if num_row > 0
		}
	}
	return $detail_pp;
  }
  
  function get_supplier(){
	//$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori= '1' ORDER BY kode_supplier ");    
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  
  
  function get_pkp_tipe_pajak_bykodesup($id_sup){
	$query	= $this->db->query(" SELECT pkp, tipe_pajak, top FROM tm_supplier where id = '$id_sup' ");    
    return $query->result();  
  }
  
  function get_op($id_op, $is_cetak){
	$query	= $this->db->query(" SELECT * FROM tm_op WHERE id = '$id_op' ");    
    $hasil = $query->result();
    
    $data_op = array();
	$detail_op = array();
	// 29-07-2015
	$status_op = "";
	if ($is_cetak == '0')
		$status_op = " AND status_op = 'f' ";
					
	foreach ($hasil as $row1) {
		$no_ppnya = ""; $list_no_ppnya = ""; 
		
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_op_detail WHERE id_op = '$row1->id' ".$status_op." ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {						
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg_supplier
										FROM tm_barang a WHERE a.id = '$row2->id_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							//$nama_brg	= $hasilrow->nama_brg;
							$nama_brg_sup	= $hasilrow->nama_brg_supplier;
							//$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$kode_brg	= '';
							//$nama_brg	= '';
							$nama_brg_sup = '';
							//$satuan	= '';
						}
						
						// 01-10-2015 nama satuan dari tabel op_detail
						$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$satuan	= '';
						}
						
						/*
						$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_supplier WHERE kode_brg = '$row2->kode_brg'
								AND kode_supplier = '$row1->kode_supplier' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$harga	= $hasilrow->harga;
						}
						else
							$harga = 0; */
						
						// cek apakah status_op = 'f' dan jum_beli == 0 ?
						$query3	= $this->db->query(" SELECT status_op FROM tm_op_detail WHERE id = '$row2->id' ");
						 if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$status_op	= $hasilrow->status_op;
						 }
						 else
							$status_op = 0;
						
						// 19-12-2011, cek apakah ada id_pp_detail
						$query4	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE id_op_detail = '".$row2->id."' ");
						if ($query4->num_rows() > 0){
							// 25-06-2015 SATUAN LAIN GA DIPAKE
							//if ($row2->is_satuan_lain == 'f')
								$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b
											WHERE a.id_pembelian = b.id AND a.id_op_detail = '$row2->id' AND b.status_aktif = 't'
											AND a.kode_brg = '$row2->kode_brg'
											AND b.kode_supplier = '$row1->kode_supplier' "); //
							/*else
								$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b
											WHERE a.id_pembelian = b.id AND a.id_op_detail = '$row2->id' AND b.status_aktif = 't'
											AND a.kode_brg = '$row2->kode_brg'
											AND b.kode_supplier = '$row1->kode_supplier' "); */
							$hasilrow = $query3->row();
							$jum_beli2 = $hasilrow->jum; // ini sum qty di pembelian_detail
						}
						else {
							// 17-06-2015 DIKOMEN
							/*if ($row2->is_satuan_lain == 'f')
								$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b,
												tm_op c, tm_op_detail d
											WHERE a.id_pembelian = b.id AND b.id_op like '%$row1->id%' AND b.status_aktif = 't'
											AND c.id = '$row1->id' AND c.id = d.id_op
											AND a.kode_brg = '$row2->kode_brg' AND d.kode_brg = a.kode_brg 
											AND b.kode_supplier = '$row1->kode_supplier' "); //
							else
								$query3	= $this->db->query(" SELECT sum(a.qty_satuan_lain) as jum FROM tm_pembelian_detail a, tm_pembelian b,
												tm_op c, tm_op_detail d
											WHERE a.id_pembelian = b.id AND b.id_op like '%$row1->id%' AND b.status_aktif = 't'
											AND c.id = '$row1->id' AND c.id = d.id_op
											AND a.kode_brg = '$row2->kode_brg' AND d.kode_brg = a.kode_brg
											AND b.kode_supplier = '$row1->kode_supplier' "); //
							$hasilrow = $query3->row();
							$jum_beli2 = $hasilrow->jum; // ini sum qty di pembelian_detail
							*/
							$jum_beli2 = 0;
						}
						
						if ($row2->satuan_lain != 0) {
							$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
							 if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$nama_sat_lain	= $hasilrow->nama;
							 }
						}
						else
							$nama_sat_lain = '';
						
						// 11-07-2012
						$sqlxx = " SELECT * FROM tm_op_detail where id_pp_detail = '$row2->id_pp_detail' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							//ambil nomor PP
							$query3	= $this->db->query(" SELECT a.no_pp FROM tm_pp_detail b INNER JOIN tm_pp a ON a.id=b.id_pp 
												WHERE b.id = '$row2->id_pp_detail' ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$no_pp	= $hasilrow->no_pp;
							 }
							 else
								$no_pp = '';
							if ($no_ppnya != $no_pp) {
								$list_no_ppnya.= $no_pp."; ";
							}
							$no_ppnya = $no_pp;
						}
						
						// 22-09-2014
						$subtotal = $row2->qty*$row2->harga;
						if ($is_cetak == '0') {
							if ($status_op == 'f' && $jum_beli2 == 0) {
								$detail_op[] = array(	'id'=> $row2->id,
													'id_brg'=> $row2->id_brg,
													'kode_brg'=> $kode_brg,
													'nama'=> $row2->nama_brg,
													'nama_brg_sup'=> $nama_brg_sup,
													'satuan'=> $satuan,
													'id_satuan'=> $row2->id_satuan,
													'qty'=> $row2->qty,
													'harga'=> $row2->harga,
													'diskon'=> $row2->diskon,
													'subtotal'=> $subtotal,
													'keterangan'=> $row2->keterangan,
													'is_satuan_lain'=> $row2->is_satuan_lain,
													'satuan_lain'=> $row2->satuan_lain,
													'nama_sat_lain'=> $nama_sat_lain,
													'id_pp_detail'=> $row2->id_pp_detail
												);
							}
							else
								$detail_op = '';
						}
						else {
							$detail_op[] = array(	'id'=> $row2->id,
													'id_brg'=> $row2->id_brg,
													'kode_brg'=> $kode_brg,
													'nama'=> $row2->nama_brg,
													'nama_brg_sup'=> $nama_brg_sup,
													'satuan'=> $satuan,
													'id_satuan'=> $row2->id_satuan,
													'qty'=> $row2->qty,
													'harga'=> $row2->harga,
													'diskon'=> $row2->diskon,
													'subtotal'=> $subtotal,
													'keterangan'=> $row2->keterangan,
													'is_satuan_lain'=> $row2->is_satuan_lain,
													'satuan_lain'=> $row2->satuan_lain,
													'nama_sat_lain'=> $nama_sat_lain,
													'id_pp_detail'=> $row2->id_pp_detail
												);
						}
					}
				}
				else {
					$detail_op = '';
				}
				// ambil data nama supplier
				if ($row1->id_supplier != '0') {
					$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp, kontak_person FROM tm_supplier WHERE id = '$row1->id_supplier' ");
					$hasilrow = $query3->row();
					$kode_supplier	= $hasilrow->kode_supplier;
					$nama_supplier	= $hasilrow->nama;
					$pkp	= $hasilrow->pkp;
					$kontak_person	= $hasilrow->kontak_person;
				}
				else {
					$kode_supplier	= '';
					$pkp	= 'f';
					$nama_supplier	= 'Lain-lain';
					$kontak_person	= '-';
				}
						
				//ambil nomor PP, 11-07-2012
				// 17-06-2015 GA DIPAKE LG
				/*if ($list_no_ppnya == '') {
					$query3	= $this->db->query(" SELECT no_pp FROM tm_pp WHERE id = '$row1->id_pp' ");
					 if ($query3->num_rows() > 0) {
							$hasilrow = $query3->row();
							$list_no_ppnya	= $hasilrow->no_pp;
					 }
					 else
						$list_no_ppnya = '';
				} */
				
				// 22-09-2014
				$query3	= $this->db->query(" SELECT sum(qty*harga) as grandtotal FROM tm_op_detail 
								WHERE id_op = '$row1->id' ");
				 if ($query3->num_rows() > 0) {
					$hasilrow = $query3->row();
					$grandtotal	= $hasilrow->grandtotal;
				 }
				 else
					$grandtotal = 0;
				
				$data_op[] = array(			'id'=> $row1->id,	
											'no_pp'=> $list_no_ppnya,	
											'no_op'=> $row1->no_op,
											'tgl_op'=> $row1->tgl_op,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'pkp'=> $pkp,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_op'=> $row1->status_op,
											'grandtotal'=> $grandtotal,
											'detail_op'=> $detail_op,
											'kontak_person'=> $kontak_person,
											'jenis_pembelian'=> $row1->jenis_pembelian
											);
				$detail_op = array();
	}
	return $data_op;
  }
  
  function get_item_op($iddetail){
	$query = $this->db->query(" SELECT a.no_op, a.tgl_op, a.id_supplier, b.* FROM tm_op a INNER JOIN tm_op_detail b 
					ON a.id = b.id_op WHERE b.id = '$iddetail' ");
	$datadetail = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// 17-06-2015, ambil id_pp dari id_pp_detail
			$query3	= $this->db->query(" SELECT distinct a.id FROM tm_pp a INNER JOIN tm_pp_detail b ON a.id = b.id_pp
							WHERE b.id = '$row1->id_pp_detail' ");
			$hasilrow = $query3->row();
			$id_pp	= $hasilrow->id;
			
			// ambil data nama barang
			$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg, b.nama as nama_satuan FROM tm_barang a
							INNER JOIN tm_satuan b ON a.satuan = b.id
							WHERE a.id = '$row1->id_brg' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			$nama_satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_supplier' ");
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
			
			$datadetail[] = array(			'id'=> $row1->id,	
											'id_op'=> $row1->id_op,	
											'id_brg'=> $row1->id_brg,
											'kode_brg'=> $kode_brg,
											'id_supplier'=> $row1->id_supplier,
											'kode_supplier'=> $kode_supplier,
											'no_op'=> $row1->no_op,
											'tgl_op'=> $row1->tgl_op,
											'nama_brg'=> $nama_brg,
											'nama_satuan'=> $nama_satuan,
											'nama_supplier'=> $nama_supplier,
											'qty'=> $row1->qty,
											'id_pp'=> $id_pp,	
											'id_pp_detail'=> $row1->id_pp_detail	
											);
		}
	}
	return $datadetail;
  }
  
  function deleteitem($iddetail) { 
	  // 12-07-2012, tambahkan update status_faktur utk id_pp_detail tsb (jika ada id_pp_detail) ataupun berdasarkan kode_brg
	  $query2	= $this->db->query(" SELECT b.id_op, b.id_pp_detail, b.id_brg FROM tm_op_detail b INNER JOIN tm_op a 
							 ON a.id = b.id_op WHERE b.id = '$iddetail' ");
	  $hasilrow = $query2->row();
	  $id_op	= $hasilrow->id_op;
	  $id_pp_detail	= $hasilrow->id_pp_detail;
	  $id_brg	= $hasilrow->id_brg;
	  
	  // update status_faktur di tm_pp dan tm_pp_detail
	  // 17-06-2015 DIKOMEN
	  /*if ($id_pp_detail == 0) {
		  $query2	= $this->db->query(" SELECT id_pp FROM tm_op WHERE id = '$id_op' ");
		  $hasilrow = $query2->row();
		  $id_pp	= $hasilrow->id_pp;
		  
		  $query2	= $this->db->query(" SELECT id FROM tm_pp_detail WHERE id_pp = '$id_pp' AND kode_brg = '$kode_brg' ");
		  $hasilrow = $query2->row();
		  $id_pp_detail	= $hasilrow->id;
		  $this->db->query(" UPDATE tm_pp SET status_faktur = 'f' where id= '$id_pp' ");
		  $this->db->query(" UPDATE tm_pp_detail SET status_faktur = 'f' where id= '$id_pp_detail' ");
	  } */
	  //else {
		  $query2	= $this->db->query(" SELECT id_pp FROM tm_pp_detail WHERE id = '$id_pp_detail' ");
		  $hasilrow = $query2->row();
		  $id_pp	= $hasilrow->id_pp;
		  $this->db->query(" UPDATE tm_pp SET status_faktur = 'f' where id= '$id_pp' ");
		  $this->db->query(" UPDATE tm_pp_detail SET status_faktur = 'f' where id= '$id_pp_detail' ");
	  //}
			
	  $this->db->delete('tm_op_detail', array('id' => $iddetail));
	  
	  // 18-08-2015 cek apakah item brgnya kosong. kalo kosong, hapus headernya
	  $query2	= $this->db->query(" SELECT count(id) as jumitem FROM tm_op_detail WHERE id_op='$id_op' ");
	  $hasilrow = $query2->row();
	  $jumitem	= $hasilrow->jumitem;
	  
	  if ($jumitem == 0)
		$this->db->query(" DELETE FROM tm_op WHERE id='$id_op' ");
  }
  
  // 09-07-2015
  function get_perusahaan() {
	$query3	= $this->db->query(" SELECT id, nama, alamat, kota, bag_pembelian, bag_keuangan, kepala_bagian, bag_admstok 
						FROM tm_perusahaan ");
				
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
		$bag_pembelian = $hasilrow->bag_pembelian;
		$bag_keuangan = $hasilrow->bag_keuangan;
		$kepala_bagian = $hasilrow->kepala_bagian;
		$bag_admstok = $hasilrow->bag_admstok;
	}
	else {
		$id	= '';
		$nama = '';
		$kota = '';
		$alamat = '';
		$bag_pembelian = '';
		$bag_keuangan = '';
		$kepala_bagian = '';
		$bag_admstok = '';
	}
		
	$datasetting = array('id'=> $id,
						  'nama'=> $nama,
						  'alamat'=> $alamat,
						  'kota'=> $kota,
						  'bag_pembelian'=> $bag_pembelian,
						  'bag_keuangan'=> $bag_keuangan,
						  'kepala_bagian'=> $kepala_bagian,
						  'bag_admstok'=> $bag_admstok
					);
							
	return $datasetting;
  }
  
  function konversi_angka2romawi($bln_now) {
	if ($bln_now == "01")
		$romawi_bln = "I";
	else if ($bln_now == "02")
		$romawi_bln = "II";
	else if ($bln_now == "03")
		$romawi_bln = "III";
	else if ($bln_now == "04")
		$romawi_bln = "IV";
	else if ($bln_now == "05")
		$romawi_bln = "V";
	else if ($bln_now == "06")
		$romawi_bln = "VI";
	else if ($bln_now == "07")
		$romawi_bln = "VII";
	else if ($bln_now == "08")
		$romawi_bln = "VIII";
	else if ($bln_now == "09")
		$romawi_bln = "IX";
	else if ($bln_now == "10")
		$romawi_bln = "X";
	else if ($bln_now == "11")
		$romawi_bln = "XI";
	else if ($bln_now == "12")
		$romawi_bln = "XII";
		
	return $romawi_bln;
  }
  
  function konversi_romawi2angka($blnrom) {
	if ($blnrom == "I")
		$bln_now = "01";
	else if ($blnrom == "II")
		$bln_now = "02";
	else if ($blnrom == "III")
		$bln_now = "03";
	else if ($blnrom == "IV")
		$bln_now = "04";
	else if ($blnrom == "V")
		$bln_now = "05";
	else if ($blnrom == "VI")
		$bln_now = "06";
	else if ($blnrom == "VII")
		$bln_now = "07";
	else if ($blnrom == "VIII")
		$bln_now = "08";
	else if ($blnrom == "IX")
		$bln_now = "09";
	else if ($blnrom == "X")
		$bln_now = "10";
	else if ($blnrom == "XI")
		$bln_now = "11";
	else if ($blnrom == "XII")
		$bln_now = "12";
		
	return $bln_now;
  }
  
  function konversi_bln2deskripsi($bln1) {
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";  
		return $nama_bln;
  }

}

