<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view(){
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_status_product ORDER BY e_statusname ASC, cast(i_status_product AS integer) DESC ", false);
	}
	
	function viewperpages($limit,$offset){
		 $db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_status_product ORDER BY e_statusname ASC, cast(i_status_product AS integer) DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($vistatusproduct,$vestatusname){
		 $db2=$this->load->database('db_external', TRUE);

		$str = array(
			'i_status_product'=>$vistatusproduct,
			'e_statusname'=>$vestatusname
		);
		$db2->insert('tr_status_product',$str);
		redirect('statusbrg/cform/');
	}
	
	function codestatus(){
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT cast(i_status_product AS integer)+1 AS istatusproduct FROM tr_status_product ORDER BY cast(i_status_product AS integer) DESC LIMIT 1");
	}

	function medit($id) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_status_product WHERE i_status_product='$id' ORDER BY i_status_product DESC LIMIT 1 " );
	}
		
	function mupdate($istatusproduct,$estatusname){
		 $db2=$this->load->database('db_external', TRUE);
		$statusproduct_item	= array(
			'e_statusname'=>$estatusname
		);
		$db2->update('tr_status_product',$statusproduct_item,array('i_status_product'=>$istatusproduct));
		redirect('statusbrg/cform');
	}
	
	function viewcari($txt_i_status_product,$txt_e_statusname){
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_i_status_product!="") {
			$filter	= " WHERE i_status_product='$txt_i_status_product' OR e_statusname LIKE '%$txt_e_statusname%' ";
		} else {
			$filter	= " WHERE e_statusname LIKE '%$txt_e_statusname%' ";
		}
		return $db2->query(" SELECT * FROM tr_status_product ".$filter." ORDER BY e_statusname ASC ");
	}
	
	function mcari($txt_i_status_product,$txt_e_statusname,$limit,$offset){
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_i_status_product!="") {
			$filter	= " WHERE i_status_product='$txt_i_status_product' OR e_statusname 	LIKE '%$txt_e_statusname%' ";
		} else {
			$filter	= " WHERE e_statusname 	LIKE '%$txt_e_statusname%' ";
		}
		$query	= $db2->query(" SELECT * FROM tr_status_product ".$filter." ORDER BY e_statusname ASC LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

  function delete($id){
	   $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_status_product',array('i_status_product'=>$id));
		redirect('statusbrg/cform/');
  }
}
?>
