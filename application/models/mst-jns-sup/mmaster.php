<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
 function getAll(){
    $this->db->select("* from tm_jns_kat_sup 
					order by kode_jenis ASC", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  
  
   
  
  function get($id){
    //$query = $this->db->getwhere('tm_kategory_sup',array('id'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_jns_kat_sup WHERE id = '$id' ");
    return $query->result();
  }
  
  function cek_data( $kode_jenis){
    $this->db->select("id from tm_jns_kat_sup WHERE kode_jenis = '$kode_jenis'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode_jenis, $id_jenis, $nama, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
     
      'kode_jenis'=>$kode_jenis,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_jns_kat_sup',$data); }
	else {
		$data = array(

		   'kode_jenis'=>$kode_jenis,
		  'nama'=>$nama,
		
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_jenis);
		$this->db->update('tm_jns_kat_sup',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_jns_kat_sup', array('id' => $kode));
  }

}
