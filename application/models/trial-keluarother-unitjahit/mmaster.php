<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
   function getAllsjotherunitjahittanpalimit($cari, $date_from, $date_to, $id_unit_asal, $caribrg, $filterbrg){
	  $pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($id_unit_asal != "0")
			$pencarian.= " AND a.id_unit_asal = '$id_unit_asal' ";
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$query	= $this->db->query(" SELECT distinct a.* FROM tm_sjkeluarother_unit_jahit a
				INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip  
				WHERE TRUE ".$pencarian);

    return $query->result();  
  }
  
   function getAllsjotherunitjahit($num, $offset, $cari, $date_from, $date_to, $id_unit_asal, $caribrg, $filterbrg) {	  
		$pencarian = "";
		if($cari!="all")
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			
		if ($date_from != "00-00-0000")
			$pencarian.= " AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
		if ($date_to != "00-00-0000")
			$pencarian.= " AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
		if ($id_unit_asal != "0")
			$pencarian.= " AND a.id_unit_asal = '$id_unit_asal' ";
		if ($filterbrg == "y" && $caribrg !="all")
			$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%$caribrg%') OR UPPER(b.nama_brg_wip) like UPPER('%$caribrg%')) ";
		$pencarian.= " ORDER BY a.tgl_sj DESC, a.no_sj DESC ";
		
		$this->db->select(" distinct a.*, d.periode FROM tm_sjkeluarother_unit_jahit a 
							INNER JOIN tm_sjkeluarother_unit_jahit_detail b ON a.id = b.id_sjkeluarother_unit_jahit 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							LEFT JOIN tm_closeperiode_unitjahit d ON to_char(a.tgl_sj,'yyyymm') = d.periode  
							WHERE TRUE ".$pencarian, false)->limit($num,$offset);
		$query = $this->db->get();
		
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%$caribrg%') OR UPPER(a.nama_brg_wip) like UPPER('%$caribrg%')) ";
					
				$query2	= $this->db->query(" SELECT a.*, b.kode_brg FROM tm_sjkeluarother_unit_jahit_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id  
								WHERE a.id_sjkeluarother_unit_jahit = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {						
						// 30-01-2014, detail qty warna -------------
						$strwarna = "";
						$queryxx = $this->db->query(" SELECT a.qty, c.nama FROM tm_sjkeluarother_unit_jahit_detail_warna a
									INNER JOIN tm_warna c ON a.id_warna = c.id
									WHERE a.id_sjkeluarother_unit_jahit_detail = '$row2->id' ORDER BY c.nama ");
						if ($queryxx->num_rows() > 0){
							$hasilxx=$queryxx->result();
							$strwarna.= $row2->kode_brg.": <br>";
							foreach ($hasilxx as $rowxx) {
								$strwarna.= $rowxx->nama."=".$rowxx->qty."<br>";
							}
						}
						//if ($strwarna == "")
						//	$strwarna = $row2->ket_qty_warna;
						//-------------------------------------------
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $row2->kode_brg,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $strwarna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				// unit jahit asal
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_asal' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_unit_asal	= $hasilrow->kode_unit;
					$nama_unit_asal	= $hasilrow->nama;
				}
				else {
					$nama_unit_asal = '';
					$kode_unit_asal = '';
				}
				
				
								
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'no_bp'=> $row1->no_bp,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'id_unit_asal'=> $row1->id_unit_asal,
											'kode_unit_asal'=> $kode_unit_asal,
											'nama_unit_asal'=> $nama_unit_asal,
											'keterangan'=> $row1->keterangan,
											'detail_sj'=> $detail_sj,
											'periode'=> $row1->periode
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
   function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function cek_data_sjotherunitjahit($no_sj, $thn1, $id_unit_asal){
	  $filterjahit = ""; $filterjahit = "";
		
    $this->db->select("id from tm_sjkeluarother_unit_jahit WHERE no_sj = '$no_sj' AND id_unit_asal = '$id_unit_asal'
							AND extract(year from tgl_sj) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function savesjotherunitjahit($id_sj, $id_unit_asal, 
			$id_brg_wip, $nama_brg_wip, $temp_qty, $id_warna, $qty_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
				
			// 30-01-2014, insert ke tabel detail ----------------------------------
			//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
													
					$qtytotal+= $qty_warna[$xx];
				} // end for
			// ---------------------------------------------------------------------
			
				// ======== update stoknya! =============
				
				//cek stok terakhir tm_stok_unit_jahit pada id_unit_asal, dan update stoknya
				// 07-12-2015 STOKNYA GLOBAL AJA GA USAH DIPISAH BAGUS/PERBAIKAN DAN GA USAH DIPISAH PER WARNA
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_jahit WHERE id_brg_wip = '$id_brg_wip'
							AND id_unit='$id_unit_asal' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qtytotal;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
						$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrowxx	= $seqxx->row();
							$id_stok_unit	= $seqrowxx->id+1;
						}else{
							$id_stok_unit	= 1;
						}
							
						$data_stok = array(
							'id'=>$id_stok_unit,
							'id_brg_wip'=>$id_brg_wip,
							'id_unit'=>$id_unit_asal,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_unit_jahit', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where id_brg_wip= '$id_brg_wip' AND id_unit='$id_unit_asal' ");
					}
					
					
					//-------------------------------------------------------------------------------------		
				
				// jika semua data tdk kosong, insert ke tm_sjotherunit_detail
				$data_detail = array(
					'id_sjkeluarother_unit_jahit'=>$id_sj,
					'id_brg_wip'=>$id_brg_wip,
					'nama_brg_wip'=>$nama_brg_wip,
					'qty'=>$qtytotal,
					'keterangan'=>$ket_detail
				);
				$this->db->insert('tm_sjkeluarother_unit_jahit_detail',$data_detail);
				
				// ambil id detail tm_sjotherunit_detail
				$seq_detail	= $this->db->query(" SELECT id FROM tm_sjkeluarother_unit_jahit_detail ORDER BY id DESC LIMIT 1 ");
				if($seq_detail->num_rows() > 0) {
					$seqrow	= $seq_detail->row();
					$iddetail = $seqrow->id;
				}
				else
					$iddetail = 0;
				
				// ----------------------------------------------
				for ($xx=0; $xx<count($id_warna); $xx++) {
					$id_warna[$xx] = trim($id_warna[$xx]);
					$qty_warna[$xx] = trim($qty_warna[$xx]);
							
					$seq_warna	= $this->db->query(" SELECT id FROM tm_sjkeluarother_unit_jahit_detail_warna ORDER BY id DESC LIMIT 1 ");
					if($seq_warna->num_rows() > 0) {
						$seqrow	= $seq_warna->row();
						$idbaru	= $seqrow->id+1;
					}else{
						$idbaru	= 1;
					}

					$tm_sjkeluarwip_detail_warna	= array(
						 'id'=>$idbaru,
						 'id_sjkeluarother_unit_jahit_detail'=>$iddetail,
						 'id_warna'=>$id_warna[$xx],
						 'qty'=>$qty_warna[$xx]
					);
					$this->db->insert('tm_sjkeluarother_unit_jahit_detail_warna',$tm_sjkeluarwip_detail_warna);
				} // end for
				// ----------------------------------------------
  }
  function deletesjotherunitjahit($id){
	$tgl = date("Y-m-d H:i:s");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_unit_asal,  b.* 
								 FROM tm_sjkeluarother_unit_jahit_detail b INNER JOIN tm_sjkeluarother_unit_jahit a ON a.id = b.id_sjkeluarother_unit_jahit
								 WHERE b.id_sjkeluarother_unit_jahit = '$id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ================= update stok unit jahit asal ==========================
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_unit_jahit
										 WHERE id_brg_wip = '$row2->id_brg_wip' AND id_unit='$row2->id_unit_asal' ");
							if ($query3->num_rows() == 0){
								$stok_unit_lama = 0;
								$id_stok_unit = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
							}
							$new_stok_unit = $stok_unit_lama+$row2->qty; // bertambah stok karena reset
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										tgl_update_stok = '$tgl' WHERE id_brg_wip= '$row2->id_brg_wip'
										AND id_unit = '$row2->id_unit_asal' ");
						
						

						// hapus data di tm_sjotherunit_detail_warna
						$this->db->query(" DELETE FROM tm_sjkeluarother_unit_jahit_detail_warna WHERE id_sjkeluarother_unit_jahit_detail='".$row2->id."' ");
					} // end foreach detail
				} // end if
	
	// 2. hapus data tm_sjotherunit_detail dan tm_sjotherunit
    $this->db->delete('tm_sjkeluarother_unit_jahit_detail', array('id_sjkeluarother_unit_jahit' => $id));
    $this->db->delete('tm_sjkeluarother_unit_jahit', array('id' => $id));
  }
  
  function get_sjkeluar_other_unit_jahit($id_sj) {
  $query	= $this->db->query("SELECT * FROM tm_sjkeluarother_unit_jahit where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		$detail_warna = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarother_unit_jahit_detail where id_sjkeluarother_unit_jahit = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_brg FROM tm_barang_wip 
											WHERE id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg_wip	= $hasilrow->kode_brg;
						
						$tabelstok = "tm_stok_unit_jahit";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." 
									 WHERE id_brg_wip = '$row2->id_brg_wip' AND id_unit='$row1->id_unit_asal' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
						
						//-------------------------------------------------------------------------------------
						// ambil data qty warna dari tm_sjkeluarwip_detail_warna
						$sqlxx	= $this->db->query(" SELECT c.id_warna, b.nama, c.qty FROM tm_warna b
												INNER JOIN tm_sjkeluarother_unit_jahit_detail_warna c ON b.id = c.id_warna 
												WHERE c.id_sjkeluarother_unit_jahit_detail = '$row2->id' ");
						if ($sqlxx->num_rows() > 0){
							$hasilxx = $sqlxx->result();
							
							foreach ($hasilxx as $row3) {
								//$listwarna.= $rownya->e_color_name." : ".$rownya->qty."<br>";
								$nama_warna = $row3->nama;
								$qty_warna = $row3->qty;
								$id_warna = $row3->id_warna;
								
								$detail_warna[] = array(	'id_warna'=> $id_warna,
												'nama_warna'=> $nama_warna,
												'qty_warna'=> $qty_warna
											);
							}
						}
						else {
							$detail_warna = '';
						}
						//-------------------------------------------------------------------------------------
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $row2->nama_brg_wip,
												'qty'=> $row2->qty,
												//'ket_qty_warna'=> $row2->ket_qty_warna,
												'keterangan'=> $row2->keterangan,
												'jum_stok'=> $jum_stok,
												'detail_warna'=> $detail_warna
											);
						$detail_warna = array();
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
			$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_asal' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_unit_asal	= $hasilrow->nama;
					$kode_unit= $hasilrow->kode_unit;
				}
				else {
					$nama_unit_asal = '';
					$kode_unit='';
				}
				

				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'id_unit_asal'=> $row1->id_unit_asal,
											'kode_unit'=> $kode_unit,
											'nama_unit_asal'=> $nama_unit_asal,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
}
