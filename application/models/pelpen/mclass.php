<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}	
	
	function lkodesumber() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_kode_voucher ORDER BY i_voucher_code ASC ");
	}
	
	
	function lapprove($id) {
		$db2=$this->load->database('db_external', TRUE);
	$query=	 $db2->query(" SELECT * FROM tm_user where i_user_id='$id' ");
		 if($query->num_rows() > 0 ) {
			return $result = $query->result_array();
		}
		 
	}
	
	function lpelanggan() {
		$db2=$this->load->database('db_external', TRUE);
	return	$query = $db2->query(" SELECT * FROM tr_customer ORDER BY i_customer ASC ");
		
			 
		
	}
	
	function lkodesumberperpages() {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_kode_voucher ORDER BY i_voucher_code ASC ");
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	
	function lpelangganperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_customer ORDER BY i_customer ASC LIMIT $limit OFFSET $offset");
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	
	function flkodesumber($key) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_kode_voucher WHERE (i_voucher_code='$key' OR e_voucher_name='$key'  OR e_description LIKE '$key%') ORDER BY i_voucher_code ASC ");
	}
	
	function flpelanggan($key) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT *  FROM tr_customer WHERE (i_customer_code='$key' OR e_customer_name='$key') ORDER BY i_customer_code ASC ");
	}
	
	function lfaktur() {
	$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana
			
			FROM tm_dt_faktur a INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
			
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_sisa > 0) 
			
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana ORDER BY a.i_dt_code ASC ");
				
	}
	
	function lfakturperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana
			FROM tm_dt_faktur a INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
			
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_sisa > 0) 
			
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana ORDER BY a.i_dt_code ASC  LIMIT $limit OFFSET $offset ");
		
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function flfaktur($key) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" 	SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana
			
			FROM tm_dt_faktur a INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
			
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_sisa > 0) 
			AND a.i_dt_code like '%$key%'
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana ORDER BY a.i_dt_code ASC
			");
	}

	function clistfaktur($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana,a.i_faktur
			
			FROM tm_dt_faktur a INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
			
			WHERE a.f_dt_cancel='f' AND (a.f_pelunasan='f' OR a.v_sisa > 0) 
			AND a.i_dt = '$ifaktur'
			GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa, a.f_nota_sederhana ORDER BY a.i_dt_code ASC ");
	}
	

	function msimpan($kode_sumber,$i_kode_sumber,$no_voucher,$tgl_voucher,$i_nama_pelanggan,$nama_pelanggan,$deskripsi_voucher,$total_nilai_voucher,$approve_voucher,$app_dept,$f_nilai_manual,$iteration,$i_faktur_code,$i_dt,$tglfaktur,$total_fakturhidden,$piutanghidden,$nilai_voucher,$fnotasederhana,$i_faktur) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$nilaivoucher = array();
		$totalvoucher = 0;
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$date_dt = date("Y-m-d");
		
		$qtm_voucher_faktur	= $db2->query(" SELECT i_voucher FROM tm_voucher_faktur ORDER BY i_voucher DESC LIMIT 1 ");
		if($qtm_voucher_faktur->num_rows()>0) {
			$rtm_voucher_faktur = $qtm_voucher_faktur->row();
			$ivoucher = $rtm_voucher_faktur->i_voucher+1;
		}else{
			$ivoucher = 1;
		}
		
		if(isset($iteration)) {
			
			$qinsert_voucher = $db2->query(" INSERT INTO tm_voucher_faktur (i_voucher,i_voucher_code,i_voucher_no,d_voucher,i_customer,e_recieved,e_approved,e_description,v_total_voucher,f_nilai_manual,f_approve_dept,d_entry,d_update) 
					VALUES('$ivoucher','$i_kode_sumber','$no_voucher','$tgl_voucher','$i_nama_pelanggan','$nama_pelanggan','$approve_voucher','$deskripsi_voucher','$total_nilai_voucher','$f_nilai_manual','$app_dept','$dentry','$dentry') ");
			
			$sisanilaivoucher = '';

			if($sisanilaivoucher=='') {
				$sisanilaivoucher = $total_nilai_voucher;
			}
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

				if(($sisanilaivoucher >= $piutanghidden[$jumlah])) {
					$sisanilaivoucher = ($sisanilaivoucher - $piutanghidden[$jumlah]);
					$nilaivoucher[$jumlah] = $piutanghidden[$jumlah];
				}elseif(($sisanilaivoucher <= $piutanghidden[$jumlah]) && ($sisanilaivoucher!=0 || $sisanilaivoucher!='0')) {
					$nilaivoucher[$jumlah] = $sisanilaivoucher;
					$sisanilaivoucher = 0;
				}elseif($sisanilaivoucher==0 || $sisanilaivoucher<0) {
					$nilaivoucher[$jumlah] = 0;
				}else{
					$nilaivoucher[$jumlah] = 0;
				}
									
				$qtm_voucher_faktur_item = $db2->query(" SELECT i_voucher_item FROM tm_voucher_faktur_item ORDER BY i_voucher_item DESC LIMIT 1 ");
				if($qtm_voucher_faktur_item->num_rows()>0){
					$rtm_voucher_faktur_item = $qtm_voucher_faktur_item->row();
					$ivoucheritem = $rtm_voucher_faktur_item->i_voucher_item+1;
				}else{
					$ivoucheritem = 1;
				}
				
				if($f_nilai_manual=='t') {
							
					$nilaivoucher[$jumlah] = $nilai_voucher[$jumlah];
					
					if($nilaivoucher[$jumlah]>0) {
						$qinsert_voucher_item = $db2->query(" INSERT INTO tm_voucher_faktur_item (i_voucher_item,i_voucher,i_faktur,d_faktur,v_voucher,d_entry,d_update,f_nota_sederhana,i_dt) 
							VALUES('$ivoucheritem','$ivoucher','$i_faktur[$jumlah]','$tglfaktur[$jumlah]','$nilaivoucher[$jumlah]','$dentry','$dentry','$fnotasederhana[$jumlah]','$i_dt[$jumlah]') ");
					}		
							
				}else{
					
					if($nilaivoucher[$jumlah]>0) {
						$qinsert_voucher_item = $db2->query(" INSERT INTO tm_voucher_faktur_item (i_voucher_item,i_voucher,d_faktur,v_voucher,d_entry,d_update,f_nota_sederhana,i_dt) 
							VALUES('$ivoucheritem','$ivoucher','$tglfaktur[$jumlah]','$nilaivoucher[$jumlah]','$dentry','$dentry','$fnotasederhana[$jumlah]','$i_dt[$jumlah]') ");
					}		
							
				}
				
				$qdt = $db2->query(" SELECT i_dt, v_sisa FROM tm_dt_faktur WHERE i_dt='$i_dt[$jumlah]' AND d_dt='$tglfaktur[$jumlah]' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana[$jumlah]' ");
				
				if($qdt->num_rows()>0) {
					
					$rdt = $qdt->row();
					
					if($nilaivoucher[$jumlah]>0) {
												
						$sisadt = ($rdt->v_sisa) - $nilaivoucher[$jumlah];
						
						if($sisadt=='')
							$sisadt = 0;
						
						$db2->query(" UPDATE tm_dt_faktur SET v_sisa='$sisadt', f_pelunasan='t' WHERE i_dt='$rdt->i_dt' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana[$jumlah]' ");
					}
				}
				
				$qdti = $db2->query(" SELECT * FROM (
				SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,kategory FROM tm_faktur WHERE f_faktur_cancel='f'
				UNION
				SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
				UNION
				SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
				)x
				WHERE i_faktur='$i_faktur[$jumlah]' AND d_faktur='$tglfaktur[$jumlah]'  ");
				
				if($qdti->num_rows()>0) {
					
					$rdti = $qdti->row();
					
					if($nilaivoucher[$jumlah]>0) {
												
						$sisadti = ($rdti->v_grand_sisa) - $nilaivoucher[$jumlah];
						
						if($sisadti=='')
							$sisadti = 0;
						
			if($rdti->kategory == 1){
						$db2->query("UPDATE tm_faktur  SET v_grand_sisa='$sisadti',f_pelunasan='t' 
							WHERE CAST(i_faktur_code as integer)= $rdti->i_faktur_code ")	;
						}
			elseif($rdti->kategory == 3){
						$db2->query("UPDATE tm_faktur_bhnbaku  SET v_grand_sisa='$sisadti',f_pelunasan='t' 
							WHERE CAST(i_faktur_code as integer)= $rdti->i_faktur_code ")	;
						}
			elseif($rdti->kategory == 2){	
						$db2->query("UPDATE tm_faktur_do_t  SET v_grand_sisa='$sisadti',f_pelunasan='t' 
							WHERE CAST(i_faktur_code as integer)= $rdti->i_faktur_code ")	;	
						}	
							
					}
				}
		
			}
		}
	
		if($db2->trans_status()===FALSE) {
			$ok	= 0;
			$db2->trans_rollback();
		}else{
			$ok	= 1;
			$db2->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Voucher : '\"+$no_voucher+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}else{
			print "<script>alert(\"Maaf, Voucher gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
	}
	
}

?>
