<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  function get_detail_sjmasukgudangjadi($list_brg, $unit_packingnya){
    $detail_pp = array();

    foreach($list_brg as $row1) {
		
			if ($row1 != '') {
				
					$query2	= $this->db->query(" SELECT a.no_sj, b.id_sjmasukgudangjadi, b.id_brg_wip, b.qty,  b.diskon
											FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
											WHERE b.id = '$row1' ");
					$hasilrow = $query2->row();
					$id_sjmasukgudangjadi	= $hasilrow->id_sjmasukgudangjadi;
					$id_brg_wip	= $hasilrow->id_brg_wip;
					$qty	= $hasilrow->qty;
					//$harga	= $hasilrow->harga;
					$diskon	= $hasilrow->diskon;
					$no_sjmasukgudangjadi	= $hasilrow->no_sj;
									
				$query2	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$id_brg_wip' ");
				$hasilrow = $query2->row();
				$kode_brg_wip	= $hasilrow->kode_brg;
				$nama_brg_wip	= $hasilrow->nama_brg;
					
				$qty_sjmasukgudangjadi = $qty;
				
					$query4	= $this->db->query(" SELECT id FROM tm_pembelianpack_wip_detail WHERE id_sjmasukgudangjadi_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelianpack_wip_detail a INNER JOIN tm_pembelianpack_wip b ON a.id_pembelianpack_wip = b.id
										WHERE a.id_sjmasukgudangjadi_detail = '$row1' AND b.status_aktif = 't' 
										AND a.id_brg_wip = '".$id_brg_wip."' ");
						
						if($query3->num_rows()>0) {
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum;
						}
						else
							$jum_beli = 0;
					}
					else {
						$jum_beli = 0;
					}
					
					
					
			
				$jum_sjmasukgudangjadi = $jum_beli;
					
				$qty = $qty-$jum_sjmasukgudangjadi;
			
				$qty = number_format($qty, 4, '.','');
				
				$query5 = $this->db->query("SELECT harga from tm_harga_brg_unit_packing where id_brg='".$id_brg_wip."' and id_unit_packing='".$unit_packingnya."' ");
				if($query5->num_rows()>0){
					$hasilrow = $query5->row();
					$harga = $hasilrow->harga;
					}
					else
					{
					$harga= 0 ;
				}
					
					$detail_sjmasukgudangjadi[] = array(	'id'=> $row1,
											'id_brg_wip'=> $id_brg_wip,
											'kode_brg_wip'=> $kode_brg_wip,
											'nama_brg_wip'=> $nama_brg_wip,
											'qty'=> $qty,
											'harga'=> $harga,
											'diskon'=> $diskon,
											'qty_sjmasukgudangjadi'=> $qty_sjmasukgudangjadi,
											'jum_beli'=> $jum_beli,
											'no_sjmasukgudangjadi'=> $no_sjmasukgudangjadi,
											'id_sjmasukgudangjadi'=> $id_sjmasukgudangjadi
									);
				
		}
	}
	return $detail_sjmasukgudangjadi;
}
 function get_unit_packing(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit ");    
    return $query->result();  
  }  
  
    function get_sjmasukgudangjaditanpalimit($id_pck, $keywordcari, $tgldari, $tglke){
		$pencarian1='';
		$pencarian2='';
		$pencarian12='';
		if ($tgldari!= '0000-00-00'){
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		if ($tglke!= '0000-00-00'){
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}
		if ($id_pck!= '0'){
		$pencarian12.=	"AND id_unit_packing = '$id_pck'";
		}
		
	if ($keywordcari == "all") {
	
				$query	= $this->db->query(" SELECT * FROM tm_sjmasukgudangjadi WHERE status_sjmasukgudangjadi = 'f' AND status_aktif = 't'
											AND jenis_masuk='2' ".$pencarian12." ".$pencarian1." ".$pencarian2);
	}
	else {
		
				$query	= $this->db->query(" SELECT * FROM tm_sjmasukgudangjadi WHERE status_sjmasukgudangjadi = 'f' AND status_aktif = 't' 
											AND jenis_masuk='2' ".$pencarian12." "
											.$pencarian1." ".$pencarian2.
							  "AND UPPER(no_sj) like UPPER('%$keywordcari%') ");
	}
    
    return $query->result();  
  }
  function get_sjmasukgudangjadi($id_pck, $keywordcari, $tgldari, $tglke){
	$pencarian1='';
		$pencarian2='';
		$pencarian12='';
		if ($tgldari!= '0000-00-00'){
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		if ($tglke!= '0000-00-00'){
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}	
		if ($id_pck!= '0'){
		$pencarian2.=	"AND id_unit_packing = '$id_pck'";
		}	
	if ($keywordcari == "all") {
				$sql = " distinct a.* FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
						WHERE a.status_sjmasukgudangjadi = 'f' AND a.status_aktif = 't'".$pencarian12." ".$pencarian1." ".$pencarian2.
					"AND b.status_sjmasukgudangjadi_detail='f' AND a.jenis_masuk='2' order by a.tgl_sj DESC, a.no_sj DESC ";
			$this->db->select($sql, false);
			$query = $this->db->get();
		
	}
	else {
				$sql = " distinct a.* FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
							INNER JOIN tm_barang_wip c ON b.id_brg_wip = c.id
							WHERE a.status_sjmasukgudangjadi = 'f' AND a.status_aktif = 't' 
							 AND b.status_sjmasukgudangjadi_detail='f' AND a.jenis_masuk='2'
								".$pencarian12." ".$pencarian1." ".$pencarian2."
							 AND UPPER(no_sj) like UPPER('%$keywordcari%') 
							order by a.tgl_sj DESC, a.no_sj DESC ";
			$this->db->select($sql, false);
			$query = $this->db->get();
	
	}
	
		$data_sjmasukgudangjadi = array();
		$detail_sjmasukgudangjadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
					$filterbrg = "";
					if ($keywordcari != "all")
						$filterbrg = " AND UPPER(no_sj) like UPPER('%$keywordcari%') ";
					
					$sql2 = "SELECT b.* FROM tm_sjmasukgudangjadi_detail b INNER JOIN tm_sjmasukgudangjadi a ON b.id_sjmasukgudangjadi = a.id 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							WHERE b.id_sjmasukgudangjadi = '$row1->id' AND b.status_sjmasukgudangjadi_detail = 'f' ".$filterbrg." ORDER BY b.id ASC ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip 
										WHERE id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
											
							$query4	= $this->db->query(" SELECT id FROM tm_pembelianpack_wip_detail WHERE id_sjmasukgudangjadi_detail = '".$row2->id."' ");
							if ($query4->num_rows() > 0){
						
									$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelianpack_wip_detail a 
											INNER JOIN tm_pembelianpack_wip b ON a.id_pembelianpack_wip = b.id WHERE b.status_aktif = 't' ";
									$sql3.= " AND a.id_sjmasukgudangjadi_detail = '$row2->id' AND a.id_brg_wip = '$row2->id_brg_wip'
												 ";
									
									$query3	= $this->db->query($sql3);
									$hasilrow = $query3->row();
									$jum_sjmasukgudangjadi = $hasilrow->jum; 
			
							}
							else {
								$jum_sjmasukgudangjadi = 0;
							}
						
						$qty = $row2->qty-$jum_sjmasukgudangjadi;
						
						
						if ($qty <= 0) {
							$this->db->query(" UPDATE tm_sjmasukgudangjadi_detail SET status_sjmasukgudangjadi_detail = 't' WHERE id='".$row2->id."' ");
							
							$queryxx	= $this->db->query(" SELECT id FROM tm_sjmasukgudangjadi_detail WHERE id_sjmasukgudangjadi = '".$row1->id."' AND status_sjmasukgudangjadi_detail = 'f' ");
							if ($queryxx->num_rows() == 0){
								$this->db->query(" UPDATE tm_sjmasukgudangjadi SET status_sjmasukgudangjadi = 't' WHERE id='".$row1->id."' ");
							}
						}
						//--------------------
						
						
							$nama_satuan_lain = '';
						
						if ($qty > 0) {
							$detail_sjmasukgudangjadi[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan
												
											);
						}
					}
				}
				else {
					$detail_sjmasukgudangjadi = '';
				}
				
				
					if ($row1->id_unit_packing != '0') {
						$query3	= $this->db->query(" SELECT kode_unit, nama from tm_unit_packing where id = '$row1->id_unit_packing' ");
						$hasilrow = $query3->row();
						$kode_unit_packing	= $hasilrow->kode_unit;
						$nama_unit_packing	= $hasilrow->nama;
					}
					else{
						$kode_unit_packing	= '';
						$nama_unit_packing	= '';
						}
				
				
				
					//$no_sjmasukgudangjadi = $row1->no_sjmasukgudangjadi;
					$no_sj = $row1->no_sj;
					//$tgl_sjmasukgudangjadi = $row1->tgl_sjmasukgudangjadi;
					$tgl_sj = $row1->tgl_sj;
				
				$data_sjmasukgudangjadi[] = array(	'id'=> $row1->id,	
											'no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'tgl_update'=> $row1->tgl_update,
											
											'detail_sjmasukgudangjadi'=> $detail_sjmasukgudangjadi
											);
				$detail_sjmasukgudangjadi = array();
			} // endforeach header
		}
		else {
			$data_sjmasukgudangjadi = '';
		}
		return $data_sjmasukgudangjadi;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function cek_data($no_sjmasukpembelian, $id_unit_packing){
    $this->db->select("id from tm_pembelianpack_wip WHERE no_sjmasukpembelianpack = '$no_sjmasukpembelian' AND id_unit_packing = '$id_unit_packing' AND status_aktif = 't' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function save($no_sjmasukpembelian,$tgl_sjpembelianpack,$id_unit_packing,$id_unit_packingbaru, $gtotal, $asligtotal, $total_pajak, $dpp, 
						$uang_muka, $sisa_hutang,$ket,$hide_pkp, $hide_tipe_pajak, $lain_cash, $lain_kredit,
				$id_sjmasukgudangjadi_detail, $id_sjmasukgudangjadi2,
				$id_brg_wip, $nama, $id_satuan, $id_satuan_konversi, $qty, $harga, $harga_lama, $pajak, $diskon, $total, $aslitotal){  
    
    // 25-06-2015 $satuan_lain, $qty_sat_lain dihilangkan
    
    $tgl = date("Y-m-d H:i:s");
    
    if ($id_unit_packing == '0')
		$id_pck_forsave = $id_unit_packingbaru;
	else
		$id_pck_forsave = $id_unit_packing;
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pembelianpack_wip WHERE no_sjmasukpembelianpack = '$no_sjmasukpembelian' AND id_unit_packing = '$id_pck_forsave' 
					AND status_aktif = 't' ", false);
    $query = $this->db->get();
    
	if ($query->num_rows() == 0){
			
				$no_faktur = NULL;
				$tgl_faktur = NULL;
				$status_faktur = 'f';
				$faktur_sj = 'f';
						
			
			// 27-08-2015
			$uid_update_by = $this->session->userdata('uid');
			
				$data_header = array(
				 
				  'no_sjmasukpembelianpack'=>$no_sjmasukpembelian,
				  'tgl_sjpembelianpack'=>$tgl_sjpembelianpack,
				  'no_faktur'=>$no_faktur,
				  'id_unit_packing'=>$id_pck_forsave,
				  'total'=>$asligtotal,
				  'uang_muka'=>$uang_muka,
				  'sisa_hutang'=>$sisa_hutang,
				  'keterangan'=>$ket,
				  'pkp'=>$hide_pkp,
				  'tipe_pajak'=>$hide_tipe_pajak,
				  'total_pajak'=>$total_pajak,
				  'dpp'=>$dpp,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'faktur_sj'=>$faktur_sj,
				  'status_faktur'=>$status_faktur,
				  'stok_masuk_lain_cash'=>$lain_cash,
				  'stok_masuk_lain_kredit'=>$lain_kredit,
				  //'jenis_pembelian'=>$jenis_pembelian,
				  'uid_update_by'=>$uid_update_by );
			//}

		$this->db->insert('tm_pembelianpack_wip',$data_header);
	}
$this->db->query(" UPDATE tm_sjmasukgudangjadi SET status_sjmasukgudangjadi = 't' where id= '$id_sjmasukgudangjadi2' ");
			//		$this->db->query(" UPDATE tm_sjmasukgudangjadi SET status_edit = 't' where id= '$id_sjmasukgudangjadi2' ");

			// ambil data terakhir di tabel tm_pembelian
			$query2	= $this->db->query(" SELECT id FROM tm_pembelianpack_wip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pembelianpack_wip	= $hasilrow->id; //echo $idnya; die();
			
			if ($id_brg_wip!='' && $qty!='0' && $harga!='') {
				

					$data_detail = array(
						'id_brg_wip'=>$id_brg_wip,
						// 29-10-2015 diganti jadi ga pake escape
						//'nama_brg'=>$this->db->escape_str($nama),
						'nama_brg'=>$nama,
						'qty'=>$qty,
						'id_satuan'=>$id_satuan,
						'id_satuan_konversi'=>$id_satuan_konversi,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						//'total'=>$total,
						'total'=>$aslitotal,
						'id_pembelianpack_wip'=>$id_pembelianpack_wip,
						'id_sjmasukgudangjadi_detail'=>$id_sjmasukgudangjadi_detail
					);
					
				$this->db->insert('tm_pembelianpack_wip_detail',$data_detail);
				
				
				$queryxx	= $this->db->query(" SELECT id FROM tm_pembelianpack_wip_detail ORDER BY id DESC LIMIT 1 ");
				$hasilxx = $queryxx->row();
				$id_pembelianpack_wip_detail	= $hasilxx->id;

				// sementara id gudang blm diisi dulu, harus nanyain ke org Duta
					$id_gudang = 0;
					$lokasi = "01"; // duta

				//4. update harga di tabel harga_brg_supplier
				if (($harga != $harga_lama) && $id_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_packing WHERE id_brg = '$id_brg_wip'
									AND id_unit_packing = '$id_unit_packing'  ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_unit_packing (id_brg, id_unit_packing, harga, 
						tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_packing', '$harga', '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_unit_packing SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg_wip' AND id_unit_packing = '$id_unit_packing'  ");
					}
				}
				
				if ($id_unit_packing == '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_packing WHERE id_brg = '$id_brg_wip'
									AND id_unit_packing = '$id_unit_packingbaru'  ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_unit_packing (id_brg, id_unit_packing, harga, 
						tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_packingbaru', '$harga',  '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_unit_packing SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg_wip' AND id_unit_packing = '$id_unit_packingbaru'  ");
					}
				}
			}
			
			// ---------------------------
		
					
					$query3	= $this->db->query(" SELECT a.qty FROM tm_sjmasukgudangjadi_detail a INNER JOIN tm_sjmasukgudangjadi b ON a.id_sjmasukgudangjadi = b.id
								WHERE a.id = '$id_sjmasukgudangjadi_detail' ");
								
					$hasilrow = $query3->row();
					$qty_sjmasukgudangjadi = $hasilrow->qty; 
					
					// 19-12-2011, cek
					$query4	= $this->db->query(" SELECT id FROM tm_pembelianpack_wip_detail WHERE id_sjmasukgudangjadi_detail = '".$id_sjmasukgudangjadi_detail."' ");
					if ($query4->num_rows() > 0){
						
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelianpack_wip_detail a INNER JOIN tm_pembelianpack_wip b ON a.id_pembelianpack_wip = b.id
									WHERE a.id_sjmasukgudangjadi_detail = '$id_sjmasukgudangjadi_detail' AND b.status_aktif = 't' ");
						
						$hasilrow = $query3->row();
						$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
					}
					else {
						$jum_beli = 0;
					}
					
					
					if ($jum_beli >= $qty_sjmasukgudangjadi) {
						$this->db->query(" UPDATE tm_sjmasukgudangjadi_detail SET status_sjmasukgudangjadi_detail = 't' where id= '$id_sjmasukgudangjadi_detail' ");
						
						$this->db->select("id from tm_sjmasukgudangjadi_detail WHERE status_sjmasukgudangjadi_detail = 'f' AND id_sjmasukgudangjadi = '$id_sjmasukgudangjadi2' ", false);
						$query = $this->db->get();
						/*
						if ($query->num_rows() == 0){
							$this->db->query(" UPDATE tm_sjmasukgudangjadi SET status_sjmasukgudangjadi = 't' where id= '$id_sjmasukgudangjadi2' ");
						}
						*/
					}

			//----------------------------
			
			//update pkp dan tipe_pajak di tabel supplier
			$this->db->query(" UPDATE tm_unit_packing SET pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak' where id= '$id_pck_forsave' ");
			
			// 18-06-2015, save ke apply_stok digabung kesini
			$th_now	= date("Y");
	
		// ====================================================
		
  }

  function getAlltanpalimit($cunit_packing, $cari, $date_from, $date_to, $caribrg, $filterbrg){

    $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelianpack) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianpack >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianpack <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_packing != '0')
		$pencarian.= " AND a.id_unit_packing = '$cunit_packing' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelianpack DESC, a.id DESC ";
	
	$query = $this->db->query(" SELECT distinct a.* FROM tm_pembelianpack_wip a LEFT JOIN tm_pembelianpack_wip_detail b ON a.id=b.id_pembelianpack_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian);
	      
    return $query->result();  
  }
  function getAll($num, $offset, $cunit_packing, $cari, $date_from, $date_to, $caribrg, $filterbrg) {	
	
	
	 $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjmasukpembelianpack) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianpack >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianpack <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_packing != '0')
		$pencarian.= " AND a.id_unit_packing = '$cunit_packing' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelianpack DESC, a.id DESC ";
	
	$this->db->select(" distinct a.* FROM tm_pembelianpack_wip a LEFT JOIN tm_pembelianpack_wip_detail b ON a.id=b.id_pembelianpack_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	  
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				//print_r($row1);
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
								OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelianpack_wip_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id
							WHERE a.id_pembelianpack_wip = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						//----------------

					  
				
					  $id_detailnya = "";
					  
						//----------------
						
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							
						}
						else {
							$kode_brg = '';
						
						}
						

						$detail_fb[] = array(	'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg,
												'nama_brg_wip'=> $row2->nama_brg,
											
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$id_detailnya = "";
					$detail_fb = '';
				}
		
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit_packing' ");
				$hasilrow = $query3->row();
				$kode_unit_packing	= $hasilrow->kode_unit;
				$nama_unit_packing	= $hasilrow->nama;
		
				$no_sjmasukgudangjadinya = "";
				
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj FROM tm_pembelianpack_wip_detail a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id_sjmasukgudangjadi_detail = b.id 
						INNER JOIN tm_sjmasukgudangjadi c ON c.id = b.id_sjmasukgudangjadi
						WHERE a.id_pembelianpack_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$no_sjmasukgudangjadinya.= $rowxx->no_sj."<br>";
					}// end for
				}
				else {
					$no_sjmasukgudangjadinya = '';
				
				}
				/*
				$sqlxx = " SELECT status_stok FROM tm_apply_stok_pembelian WHERE no_sj = '$row1->no_sj' 
							AND id_supplier = '$row1->id_supplier' AND status_aktif = 't' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$status_stok= $hasilxx->status_stok;
					if ($status_stok == 't')
						$cetakbtb = '1';
					else
						$cetakbtb = '0';
				}
				else
					$cetakbtb = '0';
					*/
						
				$data_fb[] = array(			'id'=> $row1->id,	
											
											'no_sjmasukgudangjadi'=> $no_sjmasukgudangjadinya,	
											
											'no_sjmasukpembelianpack'=> $row1->no_sjmasukpembelianpack,
											'tgl_sjpembelianpack'=> $row1->tgl_sjpembelianpack,
											'total'=> $row1->total,
											'id_unit_packing'=> $row1->id_unit_packing,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'status_stok'=> $row1->status_stok,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											//'cetakbtb'=> $cetakbtb
											//'ambil_pp'=> $ambil_pp
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function delete($id){    
	  $tgl = date("Y-m-d H:i:s");
	  
	  
	  
	    $sqlxx = " SELECT DISTINCT id_sjmasukgudangjadi_detail FROM tm_pembelianpack_wip_detail WHERE id_pembelianpack_wip='$id' ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			//$hasilxx = $queryxx->row();
			$hasilxx=$queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$id_sjmasukgudangjadi_detail	= $rowxx->id_sjmasukgudangjadi_detail;
				
				
				if ($id_sjmasukgudangjadi_detail != '0') {
					$sqlxx2= " SELECT distinct a.id FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi 
							 WHERE b.id = '$id_sjmasukgudangjadi_detail' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_sjmasukgudangjadi	= $hasilxx2->id;
						$this->db->query(" UPDATE tm_sjmasukgudangjadi SET status_edit = 'f', status_sjmasukgudangjadi = 'f' where id= '$id_sjmasukgudangjadi' ");
					}
					// reset status di detailnya dari sjmasukgudangjadi pake perulangan
					$this->db->query(" UPDATE tm_sjmasukgudangjadi_detail SET status_sjmasukgudangjadi_detail = 'f' where id= '$id_sjmasukgudangjadi_detail' ");
				}
						
				
				
			} // end for
		}
	 
	$this->db->query(" UPDATE tm_pembelianpack_wip SET status_aktif = 'f', status_stok = 'f' WHERE id = '$id' ");
		$this->db->query(" UPDATE tm_pembelianpack_wip_detail SET status_stok = 'f' WHERE id_pembelianpack_wip = '$id' ");
  }
  
   function get_pembelian($id_pembelianpack_wip) {
			$query	= $this->db->query(" SELECT * FROM tm_pembelianpack_wip where id = '$id_pembelianpack_wip' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			
				
				
				// 10-07-2015 DIMODIF
				$no_sjmasukgudangjadinya = ""; 
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj,c.tgl_sj FROM tm_pembelianpack_wip_detail a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id_sjmasukgudangjadi_detail = b.id 
						INNER JOIN tm_sjmasukgudangjadi c ON c.id = b.id_sjmasukgudangjadi
						WHERE a.id_pembelianpack_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$pisah1 = explode("-", $rowxx->tgl_sj);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						$no_sjmasukgudangjadinya.= $rowxx->no_sj." (".$tgl_sj.")"."<br>";
					}// end for
				}
				else {
					$no_sjmasukgudangjadinya = '-';
				
				}
				
				
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelianpack_wip_detail WHERE id_pembelianpack_wip = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						if($query3->num_rows > 0 ){
						$hasilrow = $query3->row();
						$nama_satuan = $hasilrow->nama;
					}
					else{
						$nama_satuan = 'pieces';
						}
						
						
						
						$sum_qty = 0;
						$sum_beli = 0;
					  
					  if ($row2->satuan_lain != 0) {
						  $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
						  $hasilrow = $query3->row();
						  $nama_satuan_lain = $hasilrow->nama;
					  }
					  else
						$nama_satuan_lain = '';
					  
					    $query3	= $this->db->query(" SELECT d.kode as kode_kel_brg FROM tm_barang_wip a 
					    INNER JOIN tm_jenis_brg_wip c ON a.id_jenis_brg_wip = c.id 
						INNER JOIN tm_kel_brg_wip d ON a.id_kel_brg_wip=d.id
									WHERE a.id = '$row2->id_brg_wip' ");
						if($query3->num_rows > 0){			
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
					}
					else
						$kode_kel_brg = 0;
						
						$sqlop = " SELECT b.qty FROM tm_sjmasukgudangjadi a INNER JOIN tm_sjmasukgudangjadi_detail b ON a.id = b.id_sjmasukgudangjadi
									WHERE b.id = '$row2->id_sjmasukgudangjadi_detail' ";
						$queryop	= $this->db->query($sqlop);
						if($queryop->num_rows()>0) {
							$hasilop = $queryop->row();
							$qty_op_detail = $hasilop->qty;
						}
						else
							$qty_op_detail = 0;
						
						if ($row2->id_sjmasukgudangjadi_detail != '0') {
							$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelianpack_wip_detail a 
										INNER JOIN tm_pembelianpack_wip b ON b.id=a.id_pembelianpack_wip
										WHERE a.id_sjmasukgudangjadi_detail='$row2->id_sjmasukgudangjadi_detail' AND a.id_brg_wip='$row2->id_brg_wip' 
										AND b.status_aktif='t'
										AND b.tgl_sjpembelianpack <= '$row1->tgl_sjpembelianpack' ");
							
							if($query3->num_rows()>0) {
								$hasilrow4 = $query3->row();
								$pemenuhan = $hasilrow4->pemenuhan;
								$sisa = $qty_op_detail-$pemenuhan;
							}else{
								$pemenuhan = 0;
								$sisa = $qty_op_detail-$pemenuhan;
								
								if($pemenuhan=='')
									$pemenuhan = 0;
								
								if($sisa=='')
									$sisa = 0;
							}
						}
						else {
							$sisa = '-';
							$pemenuhan = 0;
						}
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_kel_brg'=> $kode_kel_brg,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $this->db->escape_str($nama_brg),
												'nama_satuan'=> $nama_satuan,
												'harga'=> $row2->harga,
												'qty'=> $row2->qty,
												'id_satuan'=> $row2->id_satuan,
												'id_satuan_konversi'=> $row2->id_satuan_konversi,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total,
												'qty_sjmasukgudangjadi'=> $sum_qty,
												'jum_beli'=> $sum_beli,
												'satuan_lain'=> $row2->satuan_lain,
												'nama_satuan_lain'=> $nama_satuan_lain,
												'qty_sat_lain'=> $row2->qty_satuan_lain,
												'id_sjmasukgudangjadi_detail'=> $row2->id_sjmasukgudangjadi_detail,
												
												
												'pemenuhan'=> $pemenuhan,
												'sisa'=> $sisa
											);
					}
				}
				else {
					$detail_fb = '';
				}


				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$row1->id_unit_packing' ");
				$hasilrow = $query3->row();
				$kode_unit_packing	= $hasilrow->kode_unit;
				$nama_unit_packing	= $hasilrow->nama;
				$top	= '';
				
				$pisah1 = explode("-", $row1->tgl_sjpembelianpack);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
				
				/*
				// 31-07-2015, ambil id gudang pake distinct utk keperluan nama staf adm stok di cetak BTB
				$sqlxx = " SELECT DISTINCT d.id_gudang FROM tm_apply_stok_pembelian_detail a 
							INNER JOIN tm_pembelianpack_wip_detail b ON a.id_pembelianpack_wip_detail = b.id
							INNER JOIN tm_pembelianpack_wip c ON b.id_pembelianpack_wip = c.id
							INNER JOIN tm_barang d ON d.id = a.id_brg
							WHERE c.id='$row1->id' AND a.status_stok = 't'  ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$list_id_gudang='';
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$list_id_gudang.= $rowxx->id_gudang.";";
					}// end for
				}
				else {
					$list_id_gudang='';
				}
				
				
				
				// 27-08-2015 ambil uid_update_by dari tabel tm_apply_stok_pembelian
				$sqlxx = " SELECT uid_update_by FROM tm_apply_stok_pembelian WHERE status_aktif = 't'
						AND no_sj = '$row1->no_sj' AND id_supplier = '$row1->id_supplier' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$admgudang_uid_update_by = $hasilxx->uid_update_by;
				}
				else
					$admgudang_uid_update_by = 0;
				*/
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_sjmasukgudangjadi'=> $no_sjmasukgudangjadinya,
											//'no_bonm'=> $no_bonmnya,
											//'no_pp'=> $no_ppnya,	
											'no_sjmasukpembelianpack'=> $row1->no_sjmasukpembelianpack,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'total'=> $row1->total,
											'id_unit_packing'=> $row1->id_unit_packing,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'pkp'=> $row1->pkp,
											'tipe_pajak'=> $row1->tipe_pajak,
											'dpp'=> $row1->dpp,
											'stok_masuk_lain_cash'=> $row1->stok_masuk_lain_cash,
											'stok_masuk_lain_kredit'=> $row1->stok_masuk_lain_kredit,
											'detail_fb'=> $detail_fb,
											//'list_id_gudang'=> $list_id_gudang,
										//	'admgudang_uid_update_by'=> $admgudang_uid_update_by
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }  
  
    function updatedata() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
			$id_pembelian_wip 	= $this->input->post('id_pembelian_wip', TRUE);
			$jenis_pembelian = $this->input->post('jenis_pembelian', TRUE);  
			$no_sjmasukpembelian 	= $this->input->post('no_sjmasukpembelian', TRUE);
			$no_sjmasukpembelian_lama 	= $this->input->post('no_sjmasukpembelian_lama', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
						
			$no 	= $this->input->post('no', TRUE);
			$id_unit_packing_lama = $this->input->post('hide_unit_packing', TRUE);  
			$id_unit_packing = $this->input->post('id_unit_packing', TRUE);  
			
			
			$asligtotal = $this->input->post('asligtotal', TRUE);  
			
			$total_pajak = $this->input->post('tot_pajak', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$uang_muka = $this->input->post('uang_muka', TRUE);
			$sisa_hutang = $this->input->post('sisa_hutang', TRUE);
			$ket = $this->input->post('ket', TRUE);  
			
			$hide_pkp = $this->input->post('hide_pkp', TRUE);
			$hide_tipe_pajak = $this->input->post('hide_tipe_pajak', TRUE);
			$ambil_pp = $this->input->post('ambil_pp', TRUE);
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$cunit_packing = $this->input->post('cunit_packing', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
			$caribrg = $this->input->post('caribrg', TRUE);
			$filterbrg = $this->input->post('filterbrg', TRUE);
			
			
			$tgl = date("Y-m-d H:i:s");

			$id_gudang = 0;
			$lokasi = "01"; // duta
			
			
			$lain_cash 	= $this->input->post('lain_cash', TRUE);
			$lain_kredit 	= $this->input->post('lain_kredit', TRUE);
			
			
			if ($lain_cash == '')
				$lain_cash = 'f';
			if ($lain_kredit == '')
				$lain_kredit = 'f';
				
			
			
			$submit2 = $this->input->post('submit2', TRUE);
				
			// 20-07-2012
			if ($submit2 != '') { 
				$query4	= $this->db->query(" SELECT no_sjmasukpembelian, id_unit_packing from tm_pembelian_wip where id = '$id_pembelian_wip' ");
				$hasilrow = $query4->row();
				$no_sjmasukpembelian	= $hasilrow->no_sjmasukpembelian;
				$id_unit_packing	= $hasilrow->id_unit_packing;
				

				$jumlah_input=$no-1;
				$hitungulangtotal = 0; $hitungulangppn = 0;
				for ($i=1;$i<=$jumlah_input;$i++) {
					if ($this->input->post('cek_'.$i, TRUE) == '') {
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							// ambil data total dan pajak dari tm_pembelian_wip_detail
							$query4	= $this->db->query(" SELECT pajak, total from tm_pembelian_wip_detail where id = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
							$hasilrow = $query4->row();
							$pajaknya	= $hasilrow->pajak;
							$totalnya	= $hasilrow->total;
						
							$hitungulangtotal+= $totalnya;
							$hitungulangppn+= $pajaknya;
						}
					}
					
					if ($this->input->post('cek_'.$i, TRUE) == 'y') {
						
						if ($this->input->post('id_detail_'.$i, TRUE) != 'n') {
							
							//========= start here 20-07-2012. 19-06-2015 ==========
							$id_brg_wipnya = $this->input->post('id_brg_wip_'.$i, TRUE);
						    $id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE); 
						    $id_sjmasukwip_detail = $this->input->post('id_sjmasukwip_detail_'.$i, TRUE); 
						    $id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE); 
							
							if ($id_sjmasukwip_detail != '0') {
								$query4	= $this->db->query(" SELECT id_sjmasukwip FROM tm_sjmasukwip_detail where id = '$id_sjmasukwip_detail' ");
								$hasilrow = $query4->row();
								$id_sjmasukwipupdatestatus	= $hasilrow->id_sjmasukwip;
								
								$this->db->query("UPDATE tm_sjmasukwip set status_sjmasukwip = 'f' where id= '$id_sjmasukwipupdatestatus' ");
								$this->db->query("UPDATE tm_sjmasukwip_detail set status_sjmasukwip = 'f' where id= '$id_sjmasukwip_detail' ");
							}
							
							
													 
							
							 $qty_lama = $this->input->post('qty_lama_'.$i, TRUE); 
							 $harga_lama = $this->input->post('harga_lama_'.$i, TRUE); 
							 $id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE); 
							 
							 //ambil stok terkini di tm_stok
							$query3	= $this->db->query(" SELECT stok FROM tm_stok_unit_packing WHERE id_brg_wip='$id_brg_wip_lama' ");
							if ($query3->num_rows() > 0){
								$hasilrow = $query3->row();
								$stok_lama	= $hasilrow->stok;
							}
							else
								$stok_lama = 0;
										
							$stokreset1 = $stok_lama-$qty_lama;
										
							
														
							$this->db->query(" UPDATE tm_stok_unit_packing SET stok = '$stokreset1', tgl_update_stok = '$tgl'
												where id_brg_wip= '$id_brg_wip_lama' ");
											
							$this->db->query("DELETE FROM tm_pembelian_wip_detail WHERE id='".$this->input->post('id_detail_'.$i, TRUE)."' ");
						} 
					} 
				} 
				if ($hide_pkp == 't')
					$hitungulangdpp = $hitungulangtotal/1.1;
				else
					$hitungulangdpp = 0;
				$hitungulangdpp = round($hitungulangdpp, 2);
				$hitungulangppn = round($hitungulangppn, 2);
				$hitungulangsisahutang = $hitungulangtotal-$uang_muka;
				
				$uid_update_by = $this->session->userdata('uid');
				
				// uang_muka = '$uang_muka', sisa_hutang = '$hitungulangsisahutang', 
				$this->db->query(" UPDATE tm_pembelian_wip SET no_sjmasukpembelian = '$no_sjmasukpembelian', id_unit_packing = '$id_unit_packing', 
								tgl_sjpembelian = '$tgl_sj', jenis_pembelian='$jenis_pembelian', tgl_update = '$tgl', 
								total_pajak = '$hitungulangppn', dpp = '$hitungulangdpp', total = '$hitungulangtotal',
								keterangan='$ket', uid_update_by='$uid_update_by'
								where id= '$id_pembelian_wip' ");
				
			} 
			else {
				
				$queryxx2	= $this->db->query(" SELECT pkp FROM tm_unit_packing WHERE id = '".$id_unit_packing."' ");
				if ($queryxx2->num_rows() > 0){
					$hasilxx2 = $queryxx2->row();
					$pkp	= $hasilxx2->pkp;
				}
				else
					$pkp = 'f';
										
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$id_brg_wip = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE);
						$qty = $this->input->post('qty_'.$i, TRUE);
						$qty_lama = $this->input->post('qty_lama_'.$i, TRUE);
						$harga = $this->input->post('harga_'.$i, TRUE);
						$harga_lama = $this->input->post('harga_lama_'.$i, TRUE);
						//21-09-2015
						$id_satuan_lama = $this->input->post('id_satuan_lama_'.$i, TRUE);
						$id_satuan = $this->input->post('id_satuan_'.$i, TRUE);
						$id_satuan_konversi = $this->input->post('id_satuan_konversi_'.$i, TRUE);
						$nama_brg = $this->input->post('nama_'.$i, TRUE);
						
						// 06-01-2015
						$id_brg_wipnya = $this->input->post('id_brg_wip_'.$i, TRUE);
						$id_brg_wip_lama = $this->input->post('id_brg_wip_lama_'.$i, TRUE); 
						$id_sjmasukwip_detail = $this->input->post('id_sjmasukwip_detail_'.$i, TRUE); 
						$id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE); 
							
						// 9 sept 2011, bisa insert item brg yg baru
						if ($this->input->post('id_detail_'.$i, TRUE) == 'n') {
							// a. insert item detail
							$data_detail = array(
								'id_brg_wip'=>$this->input->post('id_brg_wip_'.$i, TRUE),
								'nama_brg'=>$this->input->post('nama_'.$i, TRUE),
								'qty'=>$this->input->post('qty_'.$i, TRUE),
								'id_satuan'=>$this->input->post('id_satuan_'.$i, TRUE),
								'id_satuan_konversi'=>$this->input->post('id_satuan_konversi_'.$i, TRUE),
								'harga'=>$this->input->post('harga_'.$i, TRUE),
								'pajak'=>$this->input->post('pajak_'.$i, TRUE),
								'diskon'=>$this->input->post('diskon_'.$i, TRUE),
								'total'=>$this->input->post('aslitotal_'.$i, TRUE),
								'id_pembelian_wip'=>$id_pembelian_wip,
								'id_sjmasukwip_detail'=>$this->input->post('id_sjmasukwip_detail_'.$i, TRUE),
								'id_pp_detail'=>$this->input->post('id_pp_detail_'.$i, TRUE)
							);
							$this->db->insert('tm_pembelian_wip_detail',$data_detail);
							
							$query3	= $this->db->query(" SELECT id FROM tm_pembelian_wip_detail ORDER BY id DESC LIMIT 1 ");
							if ($query3->num_rows() > 0) {
								$hasilrow = $query3->row();
								$id_pembelian_wip_detail = $hasilrow->id;
							}
							else
								$id_pembelian_wip_detail = 0;
							
							// 18-06-2015, save ke apply_stok digabung kesini
							$th_now	= date("Y");
	
							
							if (($harga != $harga_lama) && ($id_unit_packing != $id_unit_packing_lama)) {
								
								$this->db->query(" DELETE FROM tm_harga_brg_unit_packing WHERE id_brg_wip = '$id_brg_wip' AND id_unit_packing = '$id_unit_packing_lama'
											AND id_satuan = '$id_satuan_lama' AND harga = '$harga_lama' ");
								
							
								
								$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_packing WHERE id_brg_wip = '$id_brg_wip'
												AND id_satuan = '$id_satuan' AND id_unit_packing = '$id_unit_packing' ");
								if ($query3->num_rows() == 0){
									$this->db->query(" INSERT INTO tm_harga_brg_unit_packing (id_brg_wip, id_unit_packing, id_satuan, harga, 
									tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_packing', '$id_satuan', '$harga', '$tgl', '$tgl') ");
								}
								else {
									$this->db->query(" UPDATE tm_harga_brg_unit_packing SET harga = '$harga', tgl_update='$tgl'
												where id_brg_wip= '$id_brg_wip' AND id_unit_packing = '$id_unit_packing' AND id_satuan = '$id_satuan' ");
								}
								
								
								
							}
							else if (($harga != $harga_lama) && ($id_unit_packing == $id_unit_packing_lama)) {
								
								
								$this->db->query(" UPDATE tm_harga_brg_unit_packing SET harga = '$harga', tgl_update='$tgl'
												where id_brg_wip= '$id_brg_wip' AND id_unit_packing = '$id_unit_packing' AND id_satuan = '$id_satuan' ");
								
								// 30-09-2015. cek ke tabel history harga (tm_stok_harga), jika harga blm ada maka insert
								$query3	= $this->db->query(" SELECT id FROM tm_stok_harga WHERE id_brg_wip = '$id_brg_wip' 
															AND harga = '$harga' AND id_satuan = '$id_satuan'
															AND is_harga_pkp = '$pkp' ");
								
							}
							
						
							
							$id_sjmasukwip_detail = $this->input->post('id_sjmasukwip_detail_'.$i, TRUE);
							$id_pp_detail = $this->input->post('id_pp_detail_'.$i, TRUE);
							
							if ($id_sjmasukwip_detail != '0') {
								// ambil qty di sjmasukwip_detail
								$query3	= $this->db->query(" SELECT id_sjmasukwip, qty FROM tm_sjmasukwip_detail WHERE id = '$id_sjmasukwip_detail' ");
								if ($query3->num_rows() > 0) {
									$hasilrow = $query3->row();
									$id_sjmasukwip = $hasilrow->id_sjmasukwip;
									$qty_sjmasukwip = $hasilrow->qty;
									
									//cek jumlah pembelian. jika > qty sjmasukwip maka jika sudah t semua di tabel detail, maka update tabel tm_sjmasukwip di field status_sjmasukwip menjadi t
									$sqlxx = " SELECT sum(b.qty) as jumbeli FROM tm_pembelian_wip a INNER JOIN tm_pembelian_wip_detail b 
												ON a.id = b.id_pembelian_wip WHERE a.status_aktif='t' AND b.id_sjmasukwip_detail = '$id_sjmasukwip_detail' ";
									$queryxx	= $this->db->query($sqlxx);
									if ($queryxx->num_rows() > 0) {
										$hasilxx = $queryxx->row();
										$jumbeli = $hasilxx->jumbeli;
										
										$selisih = $jumbeli-$qty_lama+$qty;
										
										if ($selisih >= $qty_sjmasukwip) {
											$this->db->query(" UPDATE tm_sjmasukwip_detail SET status_sjmasukwip = 't' WHERE id = '$id_sjmasukwip_detail' ");
											
											// cek udh t semua blm, kalo udh, maka ganti jadi t headernya
											$sqlxx2 = " SELECT id FROM tm_sjmasukwip_detail WHERE status_sjmasukwip = 'f' ";
											$queryxx2	= $this->db->query($sqlxx2);
											if ($queryxx2->num_rows() == 0) {
												$this->db->query(" UPDATE tm_sjmasukwip SET status_sjmasukwip = 't' WHERE id='$id_sjmasukwip' ");
											}
										}
									}
									
								}
							}
							
							
					
							
							// 06-01-2016 PINDAH KESINI YG UPDATE tm_pembelian_wip_detail
								//20-06-2015
							   $sql = " UPDATE tm_pembelian_wip_detail SET qty = '".$this->input->post('qty_'.$i, TRUE)."', ";
							   if ($id_brg_wip_lama != $id_brg_wip)
									$sql.= " id_brg_wip = '$id_brg_wip', ";
									
								$sql.= " nama_brg= '".$this->db->escape_str($this->input->post('nama_'.$i, TRUE))."', 
									id_satuan = '".$this->input->post('id_satuan_'.$i, TRUE)."',
									id_satuan_konversi = '".$this->input->post('id_satuan_konversi_'.$i, TRUE)."',
									harga = '".$this->input->post('harga_'.$i, TRUE)."', diskon = '".$this->input->post('diskon_'.$i, TRUE)."',
									pajak = '".$this->input->post('pajak_'.$i, TRUE)."', total = '".$this->input->post('aslitotal_'.$i, TRUE)."'
									where id= '".$this->input->post('id_detail_'.$i, TRUE)."' ";
								//echo $sql; die();
								$this->db->query($sql);
								
							
						
						   
						   // ======================= 10-12-2015 =========================================================
						   //cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
							
																		
								//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
								$queryxx2	= $this->db->query(" SELECT id_pembelian_wip
														FROM tm_pembelian_wip_detail WHERE id = '".$this->input->post('id_detail_'.$i, TRUE)."' ");
								if ($queryxx2->num_rows() > 0){
									$hasilxx2 = $queryxx2->row();
									$cid_pembelian_wip = $hasilxx2->id_pembelian_wip;
									
									$query3	= $this->db->query(" SELECT id FROM tm_pembelian_wip_detail WHERE status_stok = 'f' 
														AND id_pembelian_wip = '$cid_pembelian_wip' ");
									if ($query3->num_rows() == 0){
										$this->db->query(" UPDATE tm_pembelian_wip SET status_stok = 't' WHERE id = '$cid_pembelian_wip' ");
									}
								}

							
							

						} 
						
					   
					} 
					
			
					$uid_update_by = $this->session->userdata('uid');
					$this->db->query(" UPDATE tm_pembelian_wip SET no_sjmasukpembelian= '$no_sjmasukpembelian', id_unit_packing = '$id_unit_packing', 
										jenis_pembelian = '$jenis_pembelian',
										tgl_sjpembelian = '$tgl_sj', total = '$asligtotal',
										keterangan = '$ket', tgl_update = '$tgl', 
										pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak',
										total_pajak = '$total_pajak', dpp = '$dpp', 
										stok_masuk_lain_cash = '$lain_cash', 
										stok_masuk_lain_kredit = '$lain_kredit',
										uid_update_by='$uid_update_by'
										where id= '$id_pembelian_wip' ");
					
				}
				
				if ($carinya == '') $carinya = "all";
				if ($is_cari == 0)
					$url_redirectnya = "faktur-btb-wip/cform/view/index/".$cur_page;
				else
					$url_redirectnya = "faktur-btb-wip/cform/cari/".$cunit_packing."/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$caribrg."/".$filterbrg."/".$cur_page;
					
				redirect($url_redirectnya);
				
				
  }
  
}
