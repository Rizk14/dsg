<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.jenis = '2'
						ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_transaksi_hasil_jahit($id_gudang, $bulan, $tahun, $id_brg_wip) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
				
		$data_brg_wip = array();
		$data_warna = array();
		$data_so_warna = array();
		$jum_stok_opname = 0;
				
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$id_brg_wip' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk_bgs1'=> 0,
												'tot_masuk_hasil_perb1'=> 0,
												'tot_masuk_retur_packing1'=> 0,
												'tot_masuk_retur_gdgjadi1'=> 0,
												'tot_masuk_lain1'=> 0,
												
												'tot_keluar_bgs_packing1'=> 0,
												'tot_keluar_bgs_gdgjadi1'=> 0,
												'tot_keluar_retur_perb1'=> 0,
												'tot_keluar_lain1'=> 0
											);
					}
				}
				
				// ambil SO dari tt_stok_opname_hasil_jahit_detail
				$sqlwarna = " SELECT c.id_warna, d.nama, c.jum_stok_opname 
							FROM tt_stok_opname_hasil_jahit a INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
							INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail 
							INNER JOIN tm_warna d ON d.id = c.id_warna
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang='$id_gudang' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['id_warna'] == $rowwarna->id_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_stok_opname;
							}
						} // end for
						
						$data_so_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_stok_opname'=> $rowwarna->jum_stok_opname
											);
					}
				}
				else
					$data_so_warna = '';
				
				// 19-01-2016, tambahan. SO-nya ambil dari global juga
				// ambil SO dari tt_stok_opname_unit_jahit_detail
				$sqlso = " SELECT b.jum_stok_opname
							FROM tt_stok_opname_hasil_jahit a INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$id_brg_wip' AND a.id_gudang='$id_gudang' ";
				$queryso	= $this->db->query($sqlso);
				if ($queryso->num_rows() > 0){
					$hasilso = $queryso->row();
					$jum_stok_opname = $hasilso->jum_stok_opname;
				}
				
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
								
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				//$sql2 edit by yefta 15-02-2016
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 1 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain , 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '1' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 1 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '2' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 1 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '3' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 1 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '4' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 1 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_masuk = '5' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 1 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjmasukother_gudangqc a INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc 
						WHERE a.id_gudang = '$id_gudang' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 1 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '1' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 1 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '2' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 1 as keluar_retur_perb, 0 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '3' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 1 as keluar_lain, 0 as keluar_other
						FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
						WHERE a.id_gudang = '$id_gudang' AND a.jenis_keluar = '4' AND b.id_brg_wip='$id_brg_wip' AND a.status_app_qc='t' AND b.status_app_qc='t'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, a.id_gudang, 0 as masuk_bgs, 0 as masuk_hasil_perb, 0 as masuk_retur_packing, 0 as masuk_retur_gdgjadi, 0 as masuk_lain, 0 as masuk_other, 0 as keluar_bgs_packing, 0 as keluar_bgs_gdgjadi, 0 as keluar_retur_perb, 0 as keluar_lain, 1 as keluar_other
						FROM tm_sjkeluarother_gudangqc a INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc  
						WHERE a.id_gudang = '$id_gudang' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
												
						ORDER BY tgl_sj ASC "; //echo $sql2; die();
								
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				$tot_saldo=$jum_stok_opname;
				
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$id_gudang = $row2->id_gudang;
						$masuk_bgs = $row2->masuk_bgs;
						$masuk_hasil_perb = $row2->masuk_hasil_perb;
						$masuk_retur_packing = $row2->masuk_retur_packing;
						$masuk_retur_gdgjadi = $row2->masuk_retur_gdgjadi;
						$masuk_lain = $row2->masuk_lain;
						//$masuk_other edit 24-02-2016
						$masuk_other = $row2->masuk_other;
						$keluar_bgs_packing = $row2->keluar_bgs_packing;
						$keluar_bgs_gdgjadi = $row2->keluar_bgs_gdgjadi;
						$keluar_retur_perb = $row2->keluar_retur_perb;
						$keluar_lain = $row2->keluar_lain;
						//$keluar_other edit 15-02-2016
						$keluar_other = $row2->keluar_other;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($masuk_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '1' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_hasil_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '2' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_retur_packing == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '3' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_retur_gdgjadi == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '4' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						if ($masuk_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukwip a INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '5' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						// edit 24-02-2016
						if ($masuk_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukother_gudangqc a INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
									INNER JOIN tm_sjmasukother_gudangqc_detail_warna c ON b.id = c.id_sjmasukother_gudangqc_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' 
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
												
						// keluar
						if ($keluar_bgs_packing == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '1' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_bgs_gdgjadi == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '2' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_retur_perb == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '3' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						if ($keluar_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '4' AND a.status_app_qc='t' AND b.status_app_qc='t'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						// edit 15-02-2016
						if ($keluar_other == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjkeluarother_gudangqc a INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
									INNER JOIN tm_sjkeluarother_gudangqc_detail_warna c ON b.id = c.id_sjkeluarother_gudangqc_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' 
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						// --------------------
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											// 11-12-2015
											$tot_saldo+= $row3->qty;
											
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											// 11-12-2015
											$tot_saldo-= $row3->qty;
											
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_bgs == '1') {
											$data_warna[$xx]['tot_masuk_bgs1']+= $row3->qty;
										}
										else if ($masuk_hasil_perb == '1') {
											$data_warna[$xx]['tot_masuk_hasil_perb1']+= $row3->qty;
										}
										else if ($masuk_retur_packing == '1') {
											$data_warna[$xx]['tot_masuk_retur_packing1']+= $row3->qty;
										}
										else if ($masuk_retur_gdgjadi == '1') {
											$data_warna[$xx]['tot_masuk_retur_gdgjadi1']+= $row3->qty;
										}
										else if ($masuk_other == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_lain == '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($keluar_bgs_packing == '1') {
											$data_warna[$xx]['tot_keluar_bgs_packing1']+= $row3->qty;
										}
										else if ($keluar_bgs_gdgjadi == '1') {
											$data_warna[$xx]['tot_keluar_bgs_gdgjadi1']+= $row3->qty;
										}
										else if ($keluar_retur_perb == '1') {
											$data_warna[$xx]['tot_keluar_retur_perb1']+= $row3->qty;
										}
										else if ($keluar_lain == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										else if ($keluar_other == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										
									}
								}
								
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						if ($id_gudang != '0') {
							$query3	= $this->db->query(" SELECT a.nama as nama_lokasi, b.kode_gudang, b.nama as nama_gudang FROM tm_lokasi_gudang a 
									INNER JOIN tm_gudang b ON a.id = b.id_lokasi 
									WHERE b.id = '$id_gudang' ");
							$hasilrow = $query3->row();
							$nama_lokasi	= $hasilrow->nama_lokasi;
							$nama_gudang	= $hasilrow->nama_gudang;
							$kode_gudang	= $hasilrow->kode_gudang;
							$gudangnya = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
						}
						else {
							//$gudangnya = "Semua";
							$kode_gudang = "";
							$nama_gudang = "";
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'id_gudang'=> $id_gudang,
												'nama_lokasi'=> $nama_lokasi,
												'kode_gudang'=> $kode_gudang,
												'nama_gudang'=> $nama_gudang,
												'masuk_bgs'=> $masuk_bgs,
												'masuk_hasil_perb'=> $masuk_hasil_perb,
												'masuk_retur_packing'=> $masuk_retur_packing,
												'masuk_retur_gdgjadi'=> $masuk_retur_gdgjadi,
												'masuk_lain'=> $masuk_lain,
													//$keluar_other edit by 24-02-2016
												'masuk_other'=> $masuk_other,
												'keluar_bgs_packing'=> $keluar_bgs_packing,
												'keluar_bgs_gdgjadi'=> $keluar_bgs_gdgjadi,
												'keluar_retur_perb'=> $keluar_retur_perb,
												'keluar_lain'=> $keluar_lain,
												//$keluar_other edit by 15-02-2016
												'keluar_other'=> $keluar_other,
												
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna,
												// 19-01-2016
												'tot_saldo'=> $tot_saldo
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
		// =================================== END NEW =======================================================
				
		// array brg wip
		$data_brg_wip[] = array(		'id_brg_wip'=> $id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna,
												'jum_stok_opname'=> $jum_stok_opname,
												'gtot_saldo'=> $tot_saldo
									);
		$data_tabel1 = array();
		$data_warna = array();
		$data_so_warna = array();
		return $data_brg_wip;
  }
  
 function get_gudang_qc(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
						WHERE a.jenis = '2' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
       function get_mutasi_stok_wip($date_from, $date_to, $gudang) {		
	  
	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
	// explode date_from utk keperluan saldo awal 
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
			
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}
	  
	$sql = "SELECT a.id_brg_wip, a.id_kel_brg_wip FROM (
				select b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukwip a 
				INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.status_app_qc='t' AND b.status_app_qc='t' AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarwip a 
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.status_app_qc='t' AND b.status_app_qc='t' AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukother_gudangqc a 
				INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarother_gudangqc a 
				INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a 
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit 
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id
				
				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a 
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit 
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1' 
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id
					 
					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a 
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit 
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1' 
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id
					 
					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a 
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit 
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn_query' 
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id
				
				)
				a GROUP BY a.id_brg_wip, a.id_kel_brg_wip ORDER BY a.id_kel_brg_wip, a.id_brg_wip"; //echo $sql; die();
		//AND a.status_approve='t'
		
		$query	= $this->db->query($sql);
		
		$jum_keluar_pack = 0;
		$jum_keluar_gdjadi = 0;
		$jum_keluar_perb = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$jum_perb_unit = 0;
		$jum_ret_unit_pack = 0;
		$jum_ret_gd_jadi = 0;
		$jum_masuk_lain = 0;
		$jum_masuk_other = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			/*
			$tglfromback= $tgl1."-".$bln_query."-".$thn_query;
			

			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			$tgltoback= $tgl1."-".$bln_query."-".$thn_query;
			*/
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}

				// 13-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
				// 04-03-2015, saldo awal bukan dari jum_stok_opname, tapi ambil dari auto_saldo_akhir
				// 21-04-2015, skrg saldo awal ambil dari SO LAGI.
				// 12-11-2015, khusus gudang QC, ambil dari SO
				
					$queryx	= $this->db->query(" SELECT b.tot_jum_stok_opname FROM tt_stok_opname_hasil_jahit a 
									INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query'  ");
						// AND b.status_approve = 't' AND a.status_approve = 't'
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->tot_jum_stok_opname;
					//$auto_saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else {
					$saldo_awal = 0;
					//$auto_saldo_awal = 0;
				}
				
				$queryxyzb	= $this->db->query(" SELECT b.jum_adjustment FROM tt_adjustment_hasil_jahit a 
									INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id = b.id_adjustment_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query'  ");
						// AND b.status_approve = 't' AND a.status_approve = 't'
				
				if ($queryxyzb->num_rows() > 0){
					$hasilrowxyzb = $queryxyzb->row();
					$saldo_awal_jum_adjustmentb = $hasilrowxyzb->jum_adjustment;
					
				}
				else {
					$saldo_awal_jum_adjustmentb = 0;
					
				}

				$queryxyz	= $this->db->query(" SELECT b.jum_adjustment FROM tt_adjustment_hasil_jahit a 
									INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id = b.id_adjustment_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query'  ");
					
				
				if ($queryxyz->num_rows() > 0){
					$hasilrowxyz = $queryxyz->row();
					$jum_adjustment = $hasilrowxyz->jum_adjustment;
					
				}
				else {
					$jum_adjustment = 0;
					
				}
				$saldo_awal+=$saldo_awal_jum_adjustmentb;
				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
				
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '1' AND a.status_app_qc='t' AND b.status_app_qc='t'");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '2' AND a.status_app_qc='t' AND b.status_app_qc='t'");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_perb_unit = $hasilrow->jum_masuk;
				}
				else
					$jum_perb_unit = 0;
				
				//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '3' AND a.status_app_qc='t' AND b.status_app_qc='t'");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_ret_unit_pack = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_unit_pack = 0;
				
				//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '4' AND a.status_app_qc='t' AND b.status_app_qc='t'");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_ret_gd_jadi = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_gd_jadi = 0;
				
				//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '5' AND a.status_app_qc='t' AND b.status_app_qc='t'");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain = 0;
					
				//6. hitung brg masuk OTHER tm_masukother_gudangqc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a 
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_other = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_other = 0;	
					
					$jum_masuk_total = $jum_masuk+$jum_perb_unit+$jum_ret_unit_pack+$jum_ret_gd_jadi+$jum_masuk_lain+$jum_masuk_other;
					$jum_masuk_lain+=$jum_masuk_other;
			//--------------------------------------------------------------------------------------------------	
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '1' AND a.status_app_qc='t' AND b.status_app_qc='t'");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_pack = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_pack = 0;
				
				// hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '2' AND a.status_app_qc='t' AND b.status_app_qc='t'");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_gdjadi = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_gdjadi = 0;
				
				// hitung brg keluar lain jenis=3 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '3' AND a.status_app_qc='t' AND b.status_app_qc='t' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perb = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perb = 0;
				
				// hitung brg keluar lain jenis=4 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '4' AND a.status_app_qc='t' AND b.status_app_qc='t' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
					
					//. hitung brg keluar OTHER tm_sjkeluarother_gudangqc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a 
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang'  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_other = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_other = 0;	
				
				$jum_keluar_total = $jum_keluar_pack+$jum_keluar_gdjadi+$jum_keluar_perb+$jum_keluar_lain+$jum_keluar_other;
				// 18-01-2016, saldo akhir dihitung dari selisih masuk dan keluar
				$jum_saldo_akhir = $saldo_awal + $jum_masuk_total -$jum_keluar_total; 
				
	/////===============================================================================================================/////////			
			/*	
					
				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '1' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_masukback = $hasilrow->jum_masuk;
				}
				else
					$jum_masukback = 0;
				
				//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '2' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_perb_unitback = $hasilrow->jum_masuk;
				}
				else
					$jum_perb_unitback = 0;
				
				//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '3' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_ret_unit_packback = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_unit_packback = 0;
				
				//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '4' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_ret_gd_jadiback = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_gd_jadiback = 0;
				
				//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '5' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_masuk_lainback = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lainback = 0;
					
				//6. hitung brg masuk OTHER tm_masukother_gudangqc
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a 
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang'  ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_masuk_otherback = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_otherback = 0;	
					
				/////////////////////////////==============================/////////////////////////////////////////
				
				$jum_masuk_totalback = $jum_masukback+$jum_perb_unitback+$jum_ret_unit_packback+$jum_ret_gd_jadiback+$jum_masuk_lainback+$jum_masuk_otherback;
			//--------------------------------------------------------------------------------------------------	
				// 1.hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '1' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_keluar_packback = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_packback = 0;
				
				// 2.hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '2' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_keluar_gdjadiback = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_gdjadiback = 0;
				
				// 3.hitung brg keluar lain jenis=3 dari tm_sjkeluarwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '3' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_keluar_perbback = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perb = 0;
				
				// 4.hitung brg keluar lain jenis=4 dari tm_sjkeluarwip
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '4' ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_keluar_lainback = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lainback = 0;
					
					//5. hitung brg keluar OTHER tm_sjkeluarother_gudangqc
				$query5	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a 
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
							WHERE a.tgl_sj >= to_date('$tglfromback','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$tgltoback','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang'  ");
				if ($query5->num_rows() > 0){
					$hasilrow = $query5->row();
					$jum_keluar_otherback= $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_otherback = 0;	
				//=========================================+=========================================================
				$jum_keluar_totalback = $jum_keluar_packback+$jum_keluar_gdjadiback+$jum_keluar_perbback+$jum_keluar_lainback+$jum_keluar_otherback;
				
				*/
				// 13-09-2013 ================================
				$sql = "SELECT a.tgl_so,a.jenis_perhitungan_stok, b.id, b.jum_stok_opname FROM tt_stok_opname_hasil_jahit a 
							INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit 
							WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1'  ";
				// AND b.status_approve = 't'
												
				$query3	= $this->db->query($sql);
				
				//$jum_so='';
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_so_detail = $hasilrow->id;
					$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
					$tgl_so = $hasilrow->tgl_so;
					
					// 15-12-2015, update saldo akhir di tabel stok opname nya
					$sqlupdate = " UPDATE tt_stok_opname_hasil_jahit_detail SET saldo_akhir = '$jum_saldo_akhir'
									WHERE id = '$id_so_detail' ";
					$this->db->query($sqlupdate);
					
						//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masukx = $hasilrow77->jum_masuk;
							
							if ($jum_masukx == '')
								$jum_masukx = 0;
						}
						else
							$jum_masukx = 0;
							
							
							
						//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_perb_unitx = $hasilrow77->jum_masuk;
							
							if ($jum_perb_unitx == '')
								$jum_perb_unitx = 0;
						}
						else
							$jum_perb_unitx = 0;
							
							
							
						//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_unit_packx = $hasilrow77->jum_masuk;
							
							if ($jum_ret_unit_packx == '')
								$jum_ret_unit_packx = 0;
						}
						else
							$jum_ret_unit_packx = 0;
							
							//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_gd_jadix = $hasilrow77->jum_masuk;
							
							if ($jum_ret_gd_jadix == '')
								$jum_ret_gd_jadix = 0;
						}
						else
							$jum_ret_gd_jadix = 0;
							
							
								//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_lainx = $hasilrow77->jum_masuk;
							
							if ($jum_masuk_lainx == '')
								$jum_masuk_lainx = 0;
						}
						else
							$jum_masuk_lainx = 0;
							
								
								//6. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a 
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_otherx = $hasilrow77->jum_masuk;
							
							if ($jum_masuk_otherx == '')
								$jum_masuk_otherx = 0;
						}
						else
							$jum_masuk_otherx = 0;
								
					$jum_masuk_totalx = $jum_masukx+$jum_perb_unitx+$jum_ret_unit_packx+
					$jum_ret_gd_jadix+$jum_masuk_lainx+$jum_masuk_otherx;
					
							// 1. hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_packx = $hasilrow77->jum_keluar;
							
							if ($jum_keluar_packx == '')
								$jum_keluar_packx = 0;
						}
						else
							$jum_keluar_packx = 0;
							
							// 2. hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_gdjadix = $hasilrow77->jum_keluar;
							
							if ($jum_keluar_gdjadix == '')
								$jum_keluar_gdjadix = 0;
						}
						else
							$jum_keluar_gdjadix = 0;
							
							// 3. hitung brg keluar bagus jenis=3 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_perbx = $hasilrow77->jum_keluar;
							
							if ($jum_keluar_perbx == '')
								$jum_keluar_perbx = 0;
						}
						else
							$jum_keluar_perbx = 0;
							
							
								
							// 4. hitung brg keluar bagus jenis=4 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a 
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_lainx = $hasilrow77->jum_keluar;
							
							if ($jum_keluar_lainx == '')
								$jum_keluar_lainx = 0;
						}
						else
							$jum_keluar_lainx = 0;
							
							
							// 5. hitung brg keluar  dari tm_sjkeluarother_gudangqc
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a 
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_otherx = $hasilrow77->jum_keluar;
							
							if ($jum_keluar_otherx == '')
								$jum_keluar_otherx = 0;
						}
						else
							$jum_keluar_otherx = 0;
							
								$jum_keluar_totalx = $jum_keluar_packx+$jum_keluar_gdjadix+
								$jum_keluar_perbx+$jum_keluar_lainx+$jum_keluar_otherx;
								
									$tambahkurang = $jum_masuk_totalx - $jum_keluar_totalx;
								
						$jum_stok_opname = $jum_stok_opname+ $tambahkurang;	
						
					$sqlupdate="Update tt_stok_opname_hasil_jahit_detail set tot_jum_stok_opname='$jum_stok_opname'		
					WHERE id='$id_so_detail'";	
					$this->db->query($sqlupdate);
						
					$sqlupdatetm = " UPDATE tm_stok_hasil_jahit SET stok = '$jum_stok_opname'
									WHERE id_brg_wip = '$row1->id_brg_wip'
									AND id_gudang='$gudang' ";
					$this->db->query($sqlupdatetm);
					
						
				}
				else {
					$id_so_detail = 0;
					$jum_stok_opname = 0;
				}
				//=========================================
				
			
				//17-09-2014, ambil nama kel brg jadi
				if ($row1->id_kel_brg_wip != '') {
					$sqlxx = " SELECT kode, nama FROM tm_kel_brg_wip WHERE id='$row1->id_kel_brg_wip' ";
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$kode_kel = $hasilxx->kode;
						$nama_kel = $hasilxx->nama;
					}
					else {
						$kode_kel = '';
						$nama_kel = '';
					}
				}
				else {
					$kode_kel = '';
					$nama_kel = '';
				}
																
				$data_stok[] = array(		'kode_brg'=> $kode_brg_wip,
											'kode_kel'=> $kode_kel,
											'nama_kel'=> $nama_kel,
											'nama_brg'=> $nama_brg_wip,
											// per 08-04-2015 pake auto saldo akhir
											// 21-04-2015, waktu hari sabtu kata ginanjar pake SO lagi
											// 12-11-2015 duta utk gudang QC acuannya dari SO
											//'saldo_awal'=> $auto_saldo_awal,
											'saldo_awal'=> $saldo_awal,
											'jum_keluar_pack'=>$jum_keluar_pack ,
											'jum_keluar_gdjadi'=>$jum_keluar_gdjadi, 
											'jum_keluar_perb'=>$jum_keluar_perb, 
											'jum_keluar_lain'=>$jum_keluar_lain, 
											'jum_keluar_total'=>$jum_keluar_total, 
										
											'jum_masuk'=>$jum_masuk ,
											'jum_perb_unit'=>$jum_perb_unit, 
											'jum_ret_unit_pack'=>$jum_ret_unit_pack, 
											'jum_ret_gd_jadi'=>$jum_ret_gd_jadi ,
											'jum_masuk_lain'=>$jum_masuk_lain ,
											
								
											'jum_masuk_total'=>$jum_masuk_total,
											'jum_saldo_akhir'=>$jum_saldo_akhir,
											'jum_adjustment'=> $jum_adjustment,
											'jum_stok_opname'=> $jum_stok_opname
											/*
											,
											
											
											'jum_masukback'=> $jum_masukback,
											'jum_perb_unitback'=> $jum_perb_unitback,
											'jum_ret_unit_packback'=> $jum_ret_unit_packback,
											'jum_ret_gd_jadiback'=> $jum_ret_gd_jadiback,
											'jum_masuk_lainback'=> $jum_masuk_lainback,
											'jum_masuk_otherback'=> $jum_masuk_otherback,
											
											'jum_keluar_packback'=> $jum_keluar_packback,
											'jum_keluar_gdjadiback'=> $jum_keluar_gdjadiback,
											'jum_keluar_perbback'=> $jum_keluar_perbback,
											'jum_keluar_lainback'=> $jum_keluar_lainback,
											'jum_keluar_otherback'=> $jum_keluar_otherback,
											
											'jum_keluar_totalback'=> $jum_keluar_totalback,
											'jum_masuk_totalback'=>$jum_masuk_totalback
											
											*/
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
}

