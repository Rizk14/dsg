<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function ldodetail($ibranchcode) {
		
		$query = $this->db->query(" 
		
		SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS motifname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do

			WHERE a.i_branch='$ibranchcode' AND a.f_do_cancel='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND b.n_residual>0
			
			ORDER BY a.i_do_code ASC ");
		
		if($query->num_rows()>0) {
			return $query->result();
		}
	}
	
	function fldodetail($iproduct,$ibranch) {
		
		return $this->db->query(" SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS motifname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir
				
			FROM tm_do a
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			
			WHERE a.i_branch='$ibranch' AND b.i_product='$iproduct' AND a.f_do_cancel='f' AND b.f_faktur_created='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND b.n_residual > 0
			
			ORDER BY a.i_do_code ASC ");
	}
	
	function cari_fpenjualan($fpenj){
		return $this->db->query(" SELECT * FROM tm_faktur_do_t WHERE i_faktur_code=trim('$fpenj') AND f_faktur_cancel='f' ");
	}

	function cari_fpajak($fpajak,$tahun){
		return $this->db->query(" SELECT * FROM tm_faktur_do_t WHERE substring(i_faktur_code,1,4)='$tahun' AND i_faktur_pajak='$fpajak' AND f_faktur_cancel='f' ");
	}
		
	function tahunnow() {
		$thn	= date("Y");
		return $this->db->query("
			SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thnfaktur 
			
			FROM tm_faktur_do_t 
			
			WHERE SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' AND f_faktur_cancel='f'
			
			ORDER BY i_faktur DESC LIMIT 1 ");	
	}
	
	function getnomorfaktur(){
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 " );
	}

	function getthnfaktur(){
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak(){
		$thn = date("Y");
		return $this->db->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_do_t WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	function cari_fpenjualan_non_do($fpenj){
		return $this->db->query(" SELECT * FROM tm_faktur WHERE i_faktur_code=trim('$fpenj') AND f_faktur_cancel='f' ");
	}
	
	function cari_fpajak_non_do($fpajak,$tahun){
		return $this->db->query(" SELECT * FROM tm_faktur WHERE substring(i_faktur_code,1,4)='$tahun' and i_faktur_pajak='$fpajak' AND f_faktur_cancel='f' ");
	}
	
	function getnomorfaktur_non_do() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}
	
	function getthnfaktur_non_do() { 
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_non_do() {
		$thn = date("Y");
		return $this->db->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}	
	
	function getnomorfaktur_bhnbaku() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),5,(LENGTH(cast(i_faktur_code AS character varying))-4)) AS ifaktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}
	
	function getthnfaktur_bhnbaku() {
		$thn = date("Y");
		return $this->db->query(" SELECT SUBSTRING(cast(i_faktur_code AS character varying),1,4) AS thn FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY i_faktur DESC LIMIT 1 ");
	}

	function nofakturpajak_bhnbaku() {
		$thn = date("Y");
		return $this->db->query(" SELECT cast(i_faktur_pajak AS integer)+1 AS nofakturpajak FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND SUBSTRING(cast(i_faktur_code AS character varying),1,4)='$thn' ORDER BY cast(i_faktur_pajak AS integer) DESC LIMIT 1 ");
	}
	
	function lcabang() {
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$this->db->select(" a.i_branch_code AS codebranch, a.e_branch_name AS branch, a.e_initial AS einitial FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
				    
		$query	= $this->db->get();
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}else{
			return false;
		}
	}
	
	function lbarangjadiperpages($ibranch,$limit,$offset) {

		$qstr	= "		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,

				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_unitprice) AS price, 
				(b.n_residual * (b.v_unitprice)) AS nilai, 
				f.e_product_motifname AS motifname
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tr_product_motif f ON f.i_product_motif=b.i_product

			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			

			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch

			WHERE g.e_initial='$ibranch' AND a.f_do_cancel='f' AND b.f_faktur_created='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND b.n_residual > 0
			
			ORDER BY a.d_do DESC, a.i_do_code DESC LIMIT ".$limit." OFFSET ".$offset." ";
						
		$query = $this->db->query($qstr);
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi($ibranch) {

		return $this->db->query( "
		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_unitprice) AS price, 
				(b.n_residual * (b.v_unitprice)) AS nilai, 
				f.e_product_motifname AS motifname
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tr_product_motif f ON f.i_product_motif=b.i_product
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch
			
			WHERE g.e_initial='$ibranch' AND a.f_do_cancel='f' AND b.f_faktur_created='f' AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND b.n_residual > 0
			
			ORDER BY a.d_do DESC, a.i_do_code DESC LIMIT 1000 ");
					
	}

	function flbarangjadi($key,$cab) {
		
		$ky_upper	= strtoupper($key); 

		return $this->db->query( "
		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_unitprice) AS price, 
				(b.n_residual * (b.v_unitprice)) AS nilai,
				f.e_product_motifname AS motifname
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			INNER JOIN tr_product_motif f ON f.i_product_motif=b.i_product
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch
			
			WHERE g.e_initial='$cab' AND (a.i_do_code='$ky_upper' OR b.i_product='$ky_upper' OR b.e_product_name LIKE '$key' OR f.e_product_motifname LIKE '$key') AND a.f_do_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t') AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND b.n_residual > 0
			
			ORDER BY b.i_product ASC LIMIT 1000 ");
					
	}
			
	function lbarangjadi2(){
		$query = $this->db->query( "

			SELECT 	a.i_do AS ido,
				b.i_do_code AS idocode,
				b.d_do AS ddo,
				c.i_product_motif AS imotif,
				c.e_product_motifname AS productname,
				d.v_unitprice AS hjp,
				a.n_deliver AS qty,
				a.v_do_gross AS nilai
											
			FROM tm_do_item a
						
			INNER JOIN tm_do b ON b.i_do=a.i_do 
			INNER JOIN tr_product_motif c ON c.i_product_motif=a.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product LIMIT 1000
 		" );
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	
	function detailsimpan($ido,$ibranch,$iproduct) {
		
		return $this->db->query(" SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo,
				b.i_product AS iproduct,
				b.e_product_name AS productname,
				b.n_deliver AS qty,
				b.n_residual AS qtyakhir,
				(b.v_do_gross / b.n_deliver) AS price, 
				(b.n_residual * (b.v_do_gross/b.n_deliver)) AS nilai
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			
			INNER JOIN tr_branch g ON g.i_branch_code=a.i_branch
			
			WHERE a.i_do='$ido' AND a.i_branch='$ibranch' AND b.i_product='$iproduct' AND a.f_do_cancel='f' AND (b.f_faktur_created='f' OR b.f_faktur_created='t')  AND (a.f_faktur_created='f' OR a.f_faktur_created='t') AND b.n_residual > 0
			
			ORDER BY a.i_do_code ASC, b.i_product ASC 
		");
	}
	
	function ebranchname($ibranch) { 
		return $this->db->query(" SELECT TRIM(e_initial) AS e_initial FROM tr_branch WHERE i_branch_code='$ibranch' ORDER BY i_branch DESC LIMIT 1 ");
	}
	
	function msimpan($i_faktur,$nw_d_faktur,$i_branch,$v_total_nilai,$n_discount,$v_discount,$nw_d_due_date,$v_total_faktur,$i_faktur_pajak,$nw_d_pajak,$n_ppn,$f_cetak,$v_total_fppn,$i_do,$i_product,$e_product_name,$v_hjp,$n_quantity,$v_unit_price,$iteration) {
		
		$i_faktur_item		= array();
		$tm_fakturdo_item	= array();
		$ido	= array();
		
		$jml_item_br		= array();
		$qty_akhir			= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		$qty_akhir2			= array();
		
		$this->db->trans_begin();
		
		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	

		$seq_tm_faktur_do	= $this->db->query(" SELECT cast(i_faktur AS integer) AS i_faktur FROM tm_faktur_do_t ORDER BY cast(i_faktur AS integer) DESC LIMIT 1 ");
		if($seq_tm_faktur_do->num_rows() >0 ) {
			$seqrow	= $seq_tm_faktur_do->row();
			$ifaktur	= $seqrow->i_faktur+1;
		} else {
			$ifaktur	= 1;
		}
		
		$berhasil=0;
		
		if(isset($iteration)){

			$this->db->query(" INSERT INTO tm_faktur_do_t (i_faktur, i_faktur_code, d_faktur, e_branch_name, d_due_date, i_faktur_pajak, d_pajak, n_discount, v_discount, v_total_faktur, v_total_fppn, f_printed, f_do_or_nota, d_entry, d_update) VALUES('$ifaktur', '$i_faktur', '$nw_d_faktur', '$i_branch', '$nw_d_due_date', '$i_faktur_pajak', '$nw_d_pajak', '$n_discount', '$v_discount', '$v_total_faktur', '$v_total_fppn', 'FALSE', 'FALSE', '$dentry', '$dentry') ");
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
				$seq_tm_fakturdo_item	= $this->db->query(" SELECT i_faktur_item AS ifakturitem FROM tm_faktur_do_item_t ORDER BY i_faktur_item DESC LIMIT 1 ");
				if($seq_tm_fakturdo_item->num_rows() > 0) {
					$seqrow	= $seq_tm_fakturdo_item->row();
					$i_faktur_item[$jumlah]	= $seqrow->ifakturitem+1;
				} else {
					$i_faktur_item[$jumlah]	= 1;
				}

				$qget_tm_do	= $this->db->query(" SELECT i_do FROM tm_do WHERE i_do_code='$i_do[$jumlah]' AND f_do_cancel='f' ORDER BY i_do DESC LIMIT 1 ");		
				$row_tm_do	= $qget_tm_do->row();
				$ido[$jumlah]	= $row_tm_do->i_do;
				
				$this->db->query(" INSERT INTO tm_faktur_do_item_t (i_faktur_item,i_faktur,i_do,n_quantity,v_unit_price,d_entry,i_product,e_product_name) VALUES('$i_faktur_item[$jumlah]','$ifaktur','$ido[$jumlah]','$n_quantity[$jumlah]','$v_hjp[$jumlah]','$dentry','$i_product[$jumlah]','$e_product_name[$jumlah]') ");
				
				$q_qty_do_item	= $this->db->query(" SELECT n_residual, i_do_item FROM tm_do_item WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND (f_faktur_created='f' OR n_residual > 0) ");

				if($q_qty_do_item->num_rows()>0) {

					$row_item_br	= $q_qty_do_item->row();
					
					$jml_item_br[$jumlah]	= $row_item_br->n_residual;
					if($n_quantity[$jumlah]==$jml_item_br[$jumlah]) {
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah]-$n_quantity[$jumlah];
						
						if($qty_akhir[$jumlah]==0) {
							$qty_akhir2[$jumlah]	= 0;
						}else{
							$qty_akhir2[$jumlah]	= $qty_akhir[$jumlah];
						}

						$ffakturcreated1='t';

						$nresidual2=$qty_akhir2[$jumlah];
						$ffakturcreated2='t';
						
					}elseif($n_quantity[$jumlah] < $jml_item_br[$jumlah]) {
						$qty_akhir2[$jumlah]	= $jml_item_br[$jumlah]-$n_quantity[$jumlah];

						$ffakturcreated1='t';

						$nresidual2=$qty_akhir2[$jumlah];
						$ffakturcreated2='f';
												
					}else{
						$qty_akhir[$jumlah]	= $jml_item_br[$jumlah]-$n_quantity[$jumlah];
						
						if($qty_akhir[$jumlah]==0) {
							$qty_akhir2[$jumlah]	= 0;
						}else{
							$qty_akhir2[$jumlah]	= $qty_akhir[$jumlah];
						}

						$ffakturcreated1='t';

						$nresidual2=$qty_akhir2[$jumlah];
						$ffakturcreated2='t';										
					}
					
					if($berhasil==0){
						$this->db->query(" UPDATE tm_do SET f_faktur_created='$ffakturcreated1' WHERE i_do='$ido[$jumlah]' AND f_faktur_created='f' ");
						$berhasil=1;
					}

					$this->db->query(" UPDATE tm_do_item SET n_residual='$nresidual2', f_faktur_created='$ffakturcreated2' WHERE i_do='$ido[$jumlah]' AND i_product='$i_product[$jumlah]' AND i_do_item='$row_item_br->i_do_item' ");
				}

				if ($this->db->trans_status()===FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}		
			}
			$ok	= 1;
		}else{
			$ok	= 0;
		}

		if($ok==1) {
			print "<script>alert(\"Nomor Faktur : '\"+$i_faktur+\"' telah disimpan, terimakasih.\");show(\"fakpenjualanpopup/cform\",\"#content\");</script>";
		}else{
			print "<script>alert(\"Maaf, Faktur Penjualan gagal disimpan. Terimakasih.\");show(\"fakpenjualanpopup/cform\",\"#content\");</script>";
		}
	}
}
?>
