<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function lopdetail($ibranch) {
		$db2=$this->load->database('db_external', TRUE);
		$tahunskrg = date("Y");
		$blnskrg = date("m");
		if ($blnskrg == 1) {
			$blnmin2 = 11;
			$tahunskrg = $tahunskrg-1;
		}
		else if ($blnskrg == 2) {
			$blnmin2 = 12;
			$tahunskrg = $tahunskrg-1;
		}
		else
			$blnmin2 = $blnskrg-2;
		
		$query = $db2->query("
			SELECT	a.i_op AS iop, a.i_op_code AS iopcode, a.d_op AS dop,
				cast(trim(b.i_product) AS character varying) AS iproduct,
				b.e_product_name AS productname,
				b.n_residual AS qtyakhir
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op

				WHERE b.n_residual>0 AND a.i_branch='$ibranch' AND a.f_op_cancel='f'
				AND a.d_op > '".$tahunskrg."-".$blnmin2."-01'
				ORDER BY a.i_op_code ASC ");
		
		if($query->num_rows()>0) {
			return $query->result();
		}		
	}

	function lcabang() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$db2->select(" a.i_branch_code AS codebranch, a.e_branch_name AS branch, a.e_initial AS einitial FROM tr_branch a 
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$order." ",false);
				    
		$query	= $db2->get();
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}else{
			return false;
		}
	}
	
	function detailsimpan($iop,$ibranch,$iproduct) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_op AS iop, a.i_op_code AS iopcode,
				cast(trim(b.i_product) AS character varying) AS iproduct,
				d.e_product_motifname AS productname,
				b.n_residual AS qtyakhir,
				f.n_quantity_akhir AS qtyproduk,
				e.v_unitprice AS unitprice,
				e.harga_grosir,
				(b.n_residual * e.v_unitprice) AS harga,
				g.f_stop_produksi AS stp
				
				FROM tm_op a
				
				INNER JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname_item f ON trim(f.i_product)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
				WHERE b.n_residual>0 AND a.i_op='$iop' AND a.i_branch='$ibranch' AND b.i_product='$iproduct' AND a.f_op_cancel='f' AND b.f_do_created='f' AND (a.f_do_created='f' OR a.f_do_created='t') AND g.i_status_so='0'
							
				ORDER BY a.i_op_code ASC, b.i_product ASC ");
	}
		
	function hargaperpelanggan($imotif,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}
		
	function cari_do($ndo) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_do WHERE TRIM(i_do_code)=TRIM('$ndo') AND f_do_cancel='f' and i_do_code not like 'SAB%' ");
	}

	function cari_do_sab($ndo) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT * FROM tm_do WHERE TRIM(i_do_code)=TRIM('$ndo') AND f_do_cancel='f' and i_do_code like 'SAB%' ");
	}
	
	function cari_tm_so($iproduct){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_so WHERE i_product_motif='$iproduct' ");
	}
	
	function getnomordo(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_do_code AS character varying),5,(LENGTH(cast(i_do_code AS character varying))-4)) AS idocode FROM tm_do WHERE f_do_cancel='f' and i_do_code not like 'SAB%' ORDER BY i_do DESC LIMIT 1 " );
	}

	function getnomordosab(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_do_code AS character varying),8,(LENGTH(cast(i_do_code AS character varying))-4)) AS idocode FROM tm_do WHERE f_do_cancel='f' and i_do_code like 'SAB%' ORDER BY i_do DESC LIMIT 1 " );
	}

	function getthndo(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_do_code AS character varying),1,4) AS thn FROM tm_do WHERE f_do_cancel='f' ORDER BY i_do DESC LIMIT 1 " );
	}
	
	function getcabang($icusto){
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_customer',$icusto);
		$db2->order_by('e_branch_name');
		return $db2->get('tr_branch');
	}
		
	function lpelanggan(){	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function lop($ibranch,$icust){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT	a.i_op AS iop, a.i_op_code AS iopcode,
				cast(trim(b.i_product) AS character varying) AS iproduct,
				d.e_product_motifname AS productname,
				b.n_residual AS qtyakhir,
				f.n_quantity_akhir AS qtyproduk,
				e.v_unitprice AS unitprice,
				(b.n_residual * e.v_unitprice) AS harga,
				g.f_stop_produksi AS stp
				
				FROM tm_op a
				
				LEFT JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname_item f ON trim(f.i_product)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
				WHERE (b.n_residual!='0' OR b.f_delivered='f') AND a.i_branch='$ibranch' AND a.i_customer='$icust' AND a.f_op_cancel='f' AND b.f_do_created='f' AND (a.f_do_created='f' OR a.f_do_created='t') AND g.i_status_so='0'
							
				ORDER BY a.i_op_code ASC, b.i_product ASC ");
	}

	function lopperpages($ibranch,$icust,$limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT	a.i_op AS iop, a.i_op_code AS iopcode,
							cast(trim(b.i_product) AS character varying) AS iproduct,
							d.e_product_motifname AS productname,
							b.n_residual AS qtyakhir,
							f.n_quantity_akhir AS qtyproduk,
							e.v_unitprice AS unitprice,
							(b.n_residual * e.v_unitprice) AS harga,
							g.f_stop_produksi AS stp
										
							FROM tm_op a
										
							LEFT JOIN tm_op_item b ON a.i_op=b.i_op
							INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
							INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
							INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
							INNER JOIN tm_stokopname_item f ON trim(f.i_product)=cast(trim(b.i_product) AS character varying)
							INNER JOIN tm_stokopname g ON g.i_so=f.i_so
							WHERE (b.n_residual!='0' OR b.f_delivered='f') AND a.i_branch='$ibranch' AND a.i_customer='$icust' AND a.f_op_cancel='f' AND b.f_do_created='f' AND (a.f_do_created='f' OR a.f_do_created='t') AND g.i_status_so='0'
										
							ORDER BY a.d_op DESC, a.i_op_code DESC "." LIMIT ".$limit." OFFSET ".$offset);
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function flop($key,$icust){
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		return $db2->query( "
				SELECT	a.i_op AS iop, a.i_op_code AS iopcode,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_residual AS qtyakhir,
						f.n_quantity_akhir AS qtyproduk,
						e.v_unitprice AS unitprice,
						(b.n_residual * e.v_unitprice) AS harga,
						g.f_stop_produksi AS stp
				
				FROM tm_op a
				
				LEFT JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname_item f ON trim(f.i_product)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
											
				WHERE (b.n_residual!='0' OR b.f_delivered='f') AND (a.i_op_code='$ky_upper' OR d.e_product_motifname LIKE '$key' OR b.i_product LIKE '$ky_upper') AND a.f_op_cancel='f' AND b.f_do_created='f' AND (a.f_do_created='f' OR a.f_do_created='t') AND g.i_status_so='0'
				
				ORDER BY a.d_op DESC, a.i_op_code DESC " );			
	}
	
	/*** 22022012
	function msimpan($i_do,$d_do,$i_customer,$i_branch,$i_op,$i_op_sebunyi,$i_product,$e_product_name,$n_deliver,$vprice,$v_do_gross,$e_note,$iteration,$qty_product,$qty_op,$f_stp) {
		
		$qtyawal 	= array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$idoitem	= array();
		
		$qty_akhir		= array();
		$jml_item_br		= array();
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		
		$arrfakturupdate	= array();
		$arrfakturupdate2	= array();
		$jml_item_br	= array();
		$qty_akhir	= array();
				
		$iopXXX	= array();
		
		$db2->trans_begin();
		
		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$seq_tm_do	= $db2->query(" SELECT cast(i_do AS integer) AS i_do FROM tm_do ORDER BY cast(i_do AS integer) DESC LIMIT 1 ");
		if($seq_tm_do->num_rows() > 0) {
			$seqrow	= $seq_tm_do->row();
			$ido	= $seqrow->i_do+1;
		}else{
			$ido	= 1;
		}
		
		if(isset($iteration)) {
			
			$db2->query(" INSERT INTO tm_do(i_do,i_do_code,i_customer,i_branch,d_do,d_entry,d_update) VALUES('$ido','$i_do','$i_customer','$i_branch','$d_do','$dentry','$dentry') ");
				
			$update_order	= 0;
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

				$qstokopname	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				}else{
					$isoz	= 0;
				}
				
				$seq_tm_do_item	= $db2->query(" SELECT cast(i_do_item AS integer) AS i_do_item FROM tm_do_item ORDER BY cast(i_do_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_do_item->num_rows() > 0) {
					$seqrow	= $seq_tm_do_item->row();
					$idoitem[$jumlah]	= $seqrow->i_do_item+1;
				}else{
					$idoitem[$jumlah]	= 1;
				}

				$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
				if($qstok_item->num_rows()>0) {
					$row_stok_item	= $qstok_item->row();
					$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
					$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;
					
					$back_qty_awal[$jumlah]		= $qtyawal[$jumlah];
					$back_qty_akhir[$jumlah]	= (($qtyakhir[$jumlah])-($n_deliver[$jumlah]));
				}else{
					$qtyawal[$jumlah]		= 0;
					$qtyakhir[$jumlah]		= 0;
					$back_qty_awal[$jumlah]	= 0;
					$back_qty_akhir[$jumlah]= 0;
				}
				
				if($qtyawal[$jumlah]=='')
					$qtyawal[$jumlah] = 0;
				
				if($qtyakhir[$jumlah]=='')
					$qtyakhir[$jumlah] = 0;
				
				if($back_qty_awal[$jumlah]=='')
					$back_qty_awal[$jumlah] = 0;
				
				if($back_qty_akhir[$jumlah]=='')
					$back_qty_akhir[$jumlah] = 0;
				
				$tm_do_item[$jumlah]	= 
					array(
					 'i_do_item'=>$idoitem[$jumlah],
					 'i_do'=>$ido,
					 'i_op'=>$i_op_sebunyi[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_deliver'=>$n_deliver[$jumlah],
					 'n_residual'=>$n_deliver[$jumlah], 
					 'v_do_gross'=>$v_do_gross[$jumlah],
					 'e_note'=>$e_note[$jumlah],
					 'd_entry'=>$dentry );
					 
				if($db2->insert('tm_do_item',$tm_do_item[$jumlah])) {

					$q_qty_op_item	= $db2->query(" SELECT * FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					
					if($q_qty_op_item->num_rows()>0) {
							
						$row_item_br	= $q_qty_op_item->row();
						$jml_item_br[$jumlah]	= $row_item_br->n_residual;
						
						if($n_deliver[$jumlah]==$jml_item_br[$jumlah]) {
						
							$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_deliver[$jumlah];
								
							if($qty_akhir[$jumlah]=='')
								$qty_akhir[$jumlah]	= 0;

							if($update_order==0) {
								$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order = 1;
							}

							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir[$jumlah]', f_do_created='TRUE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
							
						}elseif($n_deliver[$jumlah] < $jml_item_br[$jumlah]) { 

							$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_deliver[$jumlah];
								
							if($qty_akhir[$jumlah]=='')
								$qty_akhir[$jumlah] = 0;
									
							if($update_order==0){
								$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order = 1;
							}
							
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir[$jumlah]', f_do_created='FALSE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}else{
							$qty_akhir[$jumlah]	= $jml_item_br[$jumlah] - $n_deliver[$jumlah];
								
							if($qty_akhir[$jumlah]=='')
								$qty_akhir[$jumlah]	= 0;
								
							if($update_order==0){
								$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order = 1;
							}

							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir[$jumlah]', f_do_created='TRUE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}
						
						$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$jumlah]' WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
					}
					
					$get_tmsox	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.d_do='$d_do' ");					
					
					if($get_tmsox->num_rows()>0 ) {
						$row_tmso	= $get_tmsox->row_array();
						
						$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						if($get_tmso2->num_rows()>0) {
							$row_tmso2	= $get_tmso2->row();
							$iso[$jumlah]	= $row_tmso2->iso+1;
						}else{
							$iso[$jumlah]	= 1;
						}
						
						$db2->query(" INSERT INTO tm_so(i_so, i_product, i_product_motif, e_product_motifname, i_status_do, d_do, n_saldo_awal, n_saldo_akhir, d_entry) VALUES('$iso[$jumlah]', '$row_tmso[i_product]', '$row_tmso[i_product_motif]', '$row_tmso[e_product_motifname]', '$row_tmso[i_status_do]', '$row_tmso[d_do]', '$back_qty_awal[$jumlah]', '$back_qty_akhir[$jumlah]', '$dentry') ");
						
						$berhasil	= 1;
						
					}else{
						$get_tmso2	= $db2->query(" SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						if($get_tmso2->num_rows()>0) {
							$row_tmso2	= $get_tmso2->row();
							$iso[$jumlah]	= $row_tmso2->iso+1;
						}else{
							$iso[$jumlah]	= 1;
						}
						 
						$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
						if($get_tmso->num_rows()>0) {
						
							$row_tmso	= $get_tmso->row_array();

							$db2->query(" INSERT INTO tm_so(i_so, i_product, i_product_motif, e_product_motifname, i_status_do, d_do, n_saldo_awal, n_saldo_akhir, d_entry) VALUES('$iso[$jumlah]', '$row_tmso[i_product]', '$row_tmso[i_product_motif]', '$row_tmso[e_product_motifname]', '1', '$d_do', '$back_qty_awal[$jumlah]', '$back_qty_akhir[$jumlah]', '$dentry') ");
							$berhasil	= 1;
						}
					}
				}
				
				if($db2->trans_status()===FALSE) {
					$db2->trans_rollback();
				}else{
					$db2->trans_commit();
				}
			}	
		}else{
			print "<script>alert(\"Maaf, DO gagal disimpan. Terimakasih.\");show(\"dopopup/cform\",\"#content\");</script>";
		}

		$qdo	= $db2->query("SELECT * FROM tm_do WHERE i_do='$ido' ");
		
		if($qdo->num_rows()>0) {
			
			$doitem	= $db2->query(" SELECT * FROM tm_do_item WHERE i_do='$ido' ");
			
			if($doitem->num_rows()>0) {
				print "<script>alert(\"Nomor DO : '\"+$i_do+\"' telah disimpan, terimakasih.\");show(\"dopopup/cform\",\"#content\");</script>";
			}else{
				print "<script>alert(\"Maaf, DO gagal disimpan, terimakasih.\");show(\"dopopup/cform\",\"#content\");</script>";
			}
		}else{
			print "<script>alert(\"Maaf, DO gagal disimpan, kesalahan pd saat input Master Motif Brg.\");show(\"dopopup/cform\",\"#content\");</script>";
		}
				
	}
	***/
 
	function msimpan($i_do,$d_do,$i_customer,$i_branch,$i_op,$i_op_sebunyi,$i_product,$e_product_name,$n_deliver,$vprice,
					$v_do_gross,$e_note,$iteration,$qty_product,$qty_op,$f_stp, $is_grosir, $harga_grosir, $adaboneka,
					$qty_warna, $i_color) {
		$db2=$this->load->database('db_external', TRUE);
		$qtyawal 	= array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$idoitem	= array();
		$iopXXX	= array();		
		$db2->trans_begin();

	$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$seq_tm_do	= $db2->query(" SELECT cast(i_do AS integer) AS i_do FROM tm_do ORDER BY cast(i_do AS integer) DESC LIMIT 1 ");
		if($seq_tm_do->num_rows() > 0) {
			$seqrow	= $seq_tm_do->row();
			$ido	= $seqrow->i_do+1;
		}else{
			$ido	= 1;
		}
		
		if(isset($iteration)) {
			
			$db2->query(" INSERT INTO tm_do(i_do,i_do_code,i_customer,i_branch,d_do,d_entry,d_update) VALUES('$ido','$i_do','$i_customer','$i_branch','$d_do','$dentry','$dentry') ");
			
			for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

				$qstokopname	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				}else{
					$isoz	= 0;
				}
				
				$seq_tm_do_item	= $db2->query(" SELECT cast(i_do_item AS integer) AS i_do_item FROM tm_do_item ORDER BY cast(i_do_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_do_item->num_rows()>0) {
					$seqrow	= $seq_tm_do_item->row();
					$idoitem[$jumlah]	= $seqrow->i_do_item+1;
				}else{
					$idoitem[$jumlah]	= 1;
				}

				$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
				
				if($qstok_item->num_rows()>0) {
					
					$row_stok_item	= $qstok_item->row();
					
					$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
					$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;
					
					$back_qty_awal[$jumlah]		= $qtyawal[$jumlah];
					$back_qty_akhir[$jumlah]	= (($qtyakhir[$jumlah])-($n_deliver[$jumlah]));
					
					$i_so_item = $row_stok_item->i_so_item;
				}else{
					$qtyawal[$jumlah]		= 0;
					$qtyakhir[$jumlah]		= 0;
					$back_qty_awal[$jumlah]	= 0;
					$back_qty_akhir[$jumlah]= 0;
					$i_so_item = 0;
				}
				
				if($qtyawal[$jumlah]=='')
					$qtyawal[$jumlah] = 0;
				
				if($qtyakhir[$jumlah]=='')
					$qtyakhir[$jumlah] = 0;
				
				if($back_qty_awal[$jumlah]=='')
					$back_qty_awal[$jumlah] = 0;
				
				if($back_qty_akhir[$jumlah]=='')
					$back_qty_akhir[$jumlah] = 0;
				
				if ($is_grosir[$jumlah]=='')
					$is_grosir[$jumlah] = 'f';
				$tm_do_item[$jumlah]	= array(
					 'i_do_item'=>$idoitem[$jumlah],
					 'i_do'=>$ido,
					 'i_op'=>$i_op_sebunyi[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_deliver'=>$n_deliver[$jumlah],
					 'n_residual'=>$n_deliver[$jumlah], 
					 'v_do_gross'=>$v_do_gross[$jumlah],
					 'e_note'=>$e_note[$jumlah],
					 'd_entry'=>$dentry,
					 'is_grosir'=>$is_grosir[$jumlah],
					 'harga_grosir'=>$harga_grosir[$jumlah],
					 'is_adaboneka'=>$adaboneka[$jumlah]);
					 
				if($db2->insert('tm_do_item',$tm_do_item[$jumlah])) {
					
					// 05-09-2014
					// ------------------------------------------
					for ($xx=0; $xx<count($i_color[$jumlah]); $xx++) {
					//foreach ($i_product_color[$jumlah] as $row1) {
						
						//if (trim($row1) != '') {
							$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
							$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
							
							$seq_tm_do_item_color	= $db2->query(" SELECT i_do_item_color FROM tm_do_item_color 
														ORDER BY i_do_item_color DESC LIMIT 1 ");
						
							if($seq_tm_do_item_color->num_rows() > 0) {
								$seqrow	= $seq_tm_do_item_color->row();
								$i_do_item_color[$jumlah]	= $seqrow->i_do_item_color+1;
							}else{
								$i_do_item_color[$jumlah]	= 1;
							}

							$tm_do_item_color[$jumlah]	= array(
								 'i_do_item_color'=>$i_do_item_color[$jumlah],
								 'i_do_item'=>$idoitem[$jumlah],
								 'i_color'=>$i_color[$jumlah][$xx],
								 'qty'=>$qty_warna[$jumlah][$xx]
							);
							$db2->insert('tm_do_item_color',$tm_do_item_color[$jumlah]);
							
							// 09-10-2014 update stok per warna
								$sqlwarna = " SELECT * FROM tm_stokopname_item_color WHERE i_so_item = '$i_so_item'
											AND i_color = '".$i_color[$jumlah][$xx]."' ";
								$querywarna = $db2->query($sqlwarna);
								
								if($querywarna->num_rows() > 0) {
									$hasilwarna = $querywarna->row();
									
									$qtyawalwarna	= $hasilwarna->n_quantity_awal;
									$qtyakhirwarna	= $hasilwarna->n_quantity_akhir;

									$back_qty_awalwarna	= $qtyawalwarna;
									$back_qty_akhirwarna	= $qtyakhirwarna-$qty_warna[$jumlah][$xx];
									
									if($back_qty_akhirwarna=="")
										$back_qty_akhirwarna	= 0;
																			
									$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir = '$back_qty_akhirwarna'
														WHERE i_so_item = '$i_so_item' AND i_color = '".$i_color[$jumlah][$xx]."' ");
									
								} // end if
							
						//} // end if
					} // end for
					// ------------------------------------------


					$q_qty_op_item	= $db2->query(" SELECT * FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					
					if($q_qty_op_item->num_rows()>0) {
							
						$row_item_br	= $q_qty_op_item->row();
						
						if($n_deliver[$jumlah]==($row_item_br->n_residual)) {			
							$qty_akhir	= (($row_item_br->n_residual)-$n_deliver[$jumlah]);
								
							if($qty_akhir=='')
								$qty_akhir	= 0;

							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir', f_do_created='TRUE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
							
						}elseif($n_deliver[$jumlah] < ($row_item_br->n_residual)) { 
							$qty_akhir	= (($row_item_br->n_residual)-$n_deliver[$jumlah]);
								
							if($qty_akhir=='')
								$qty_akhir = 0;
									
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");		
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir', f_do_created='FALSE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}else{
							$qty_akhir	= (($row_item_br->n_residual)-$n_deliver[$jumlah]);
							
							if($qty_akhir=='')
								$qty_akhir	= 0;
								
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir', f_do_created='TRUE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}
						
						$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$jumlah]' WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
					}
					 
				}
			}	
			
			if($db2->trans_status()===FALSE || $db2->trans_status()==false) {
				$db2->trans_rollback();
				$ok = 0;
			}else{
				$db2->trans_commit();
				$ok = 1;
				var_dump($ok);
				print "<script>alert(\"Nomor DO : '\"+'$i_do'+\"' Berhasil Di Simpan .\");window.open(\"index\", \"_self\");</script>";
			}
							
		}else{

			print "<script>alert(\"Maaf, DO gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
				
	


	} 

	// function msimpan_old($i_do,$d_do,$i_customer,$i_branch,$i_op,$i_op_sebunyi,$i_product,$e_product_name,
	// 		$n_deliver,$vprice,$v_do_gross,$e_note,$iteration,$qty_product,$qty_op,$f_stp) {
	// 	$db2=$this->load->database('db_external', TRUE);
	// 	$qtyawal 	= array();
	// 	$qtyakhir	= array();
	// 	$back_qty_awal	= array();
	// 	$back_qty_akhir	= array();
	// 	$idoitem	= array();
	// 	$iopXXX	= array();
		
	// 	$db2->trans_begin();
		
	// 	$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
	// 	$drow	= $qdate->row();
	// 	$dentry	= $drow->date;

	// 	$seq_tm_do	= $db2->query(" SELECT cast(i_do AS integer) AS i_do FROM tm_do ORDER BY cast(i_do AS integer) DESC LIMIT 1 ");
	// 	if($seq_tm_do->num_rows() > 0) {
	// 		$seqrow	= $seq_tm_do->row();
	// 		$ido	= $seqrow->i_do+1;
	// 	}else{
	// 		$ido	= 1;
	// 	}
		
	// 	if(isset($iteration)) {
			
	// 		$db2->query(" INSERT INTO tm_do(i_do,i_do_code,i_customer,i_branch,d_do,d_entry,d_update) VALUES('$ido','$i_do','$i_customer','$i_branch','$d_do','$dentry','$dentry') ");
			
	// 		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {

	// 			$qstokopname	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				
	// 			if($qstokopname->num_rows()>0) {
	// 				$row_stokopname	= $qstokopname->row();
	// 				$isoz	= $row_stokopname->i_so;
	// 			}else{
	// 				$isoz	= 0;
	// 			}
				
	// 			$seq_tm_do_item	= $db2->query(" SELECT cast(i_do_item AS integer) AS i_do_item FROM tm_do_item ORDER BY cast(i_do_item AS integer) DESC LIMIT 1 ");
	// 			if($seq_tm_do_item->num_rows()>0) {
	// 				$seqrow	= $seq_tm_do_item->row();
	// 				$idoitem[$jumlah]	= $seqrow->i_do_item+1;
	// 			}else{
	// 				$idoitem[$jumlah]	= 1;
	// 			}

	// 			$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
				
	// 			if($qstok_item->num_rows()>0) {
					
	// 				$row_stok_item	= $qstok_item->row();
					
	// 				$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
	// 				$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;
					
	// 				$back_qty_awal[$jumlah]		= $qtyawal[$jumlah];
	// 				$back_qty_akhir[$jumlah]	= (($qtyakhir[$jumlah])-($n_deliver[$jumlah]));
	// 			}else{
	// 				$qtyawal[$jumlah]		= 0;
	// 				$qtyakhir[$jumlah]		= 0;
	// 				$back_qty_awal[$jumlah]	= 0;
	// 				$back_qty_akhir[$jumlah]= 0;
	// 			}
				
	// 			if($qtyawal[$jumlah]=='')
	// 				$qtyawal[$jumlah] = 0;
				
	// 			if($qtyakhir[$jumlah]=='')
	// 				$qtyakhir[$jumlah] = 0;
				
	// 			if($back_qty_awal[$jumlah]=='')
	// 				$back_qty_awal[$jumlah] = 0;
				
	// 			if($back_qty_akhir[$jumlah]=='')
	// 				$back_qty_akhir[$jumlah] = 0;
				
	// 			$tm_do_item[$jumlah]	= 
	// 				array(
	// 				 'i_do_item'=>$idoitem[$jumlah],
	// 				 'i_do'=>$ido,
	// 				 'i_op'=>$i_op_sebunyi[$jumlah],
	// 				 'i_product'=>$i_product[$jumlah],
	// 				 'e_product_name'=>$e_product_name[$jumlah],
	// 				 'n_deliver'=>$n_deliver[$jumlah],
	// 				 'n_residual'=>$n_deliver[$jumlah], 
	// 				 'v_do_gross'=>$v_do_gross[$jumlah],
	// 				 'e_note'=>$e_note[$jumlah],
	// 				 'd_entry'=>$dentry );
					 
	// 			if($db2->insert('tm_do_item',$tm_do_item[$jumlah])) {

	// 				$q_qty_op_item	= $db2->query(" SELECT * FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					
	// 				if($q_qty_op_item->num_rows()>0) {
							
	// 					$row_item_br	= $q_qty_op_item->row();
						
	// 					if($n_deliver[$jumlah]==($row_item_br->n_residual)) {			
	// 						$qty_akhir	= (($row_item_br->n_residual)-$n_deliver[$jumlah]);
								
	// 						if($qty_akhir=='')
	// 							$qty_akhir	= 0;

	// 						$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
	// 						$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir', f_do_created='TRUE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
							
	// 					}elseif($n_deliver[$jumlah] < ($row_item_br->n_residual)) { 
	// 						$qty_akhir	= (($row_item_br->n_residual)-$n_deliver[$jumlah]);
								
	// 						if($qty_akhir=='')
	// 							$qty_akhir = 0;
									
	// 						$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");		
	// 						$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir', f_do_created='FALSE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
	// 					}else{
	// 						$qty_akhir	= (($row_item_br->n_residual)-$n_deliver[$jumlah]);
							
	// 						if($qty_akhir=='')
	// 							$qty_akhir	= 0;
								
	// 						$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
	// 						$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhir', f_do_created='TRUE' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
	// 					}
						
	// 					$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$jumlah]' WHERE i_so='$isoz' AND i_product='$i_product[$jumlah]' ");
	// 				}
					 
	// 			}
	// 		}	
			
	// 		if($db2->trans_status()===FALSE || $db2->trans_status()==false) {
	// 			$db2->trans_rollback();
	// 			$ok = 0;
	// 		}else{
	// 			$db2->trans_commit();
	// 			$ok = 1;
	// 		}
							
	// 	}else{
	// 		print "<script>alert(\"Maaf, DO gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
	// 	}		
			
	// 	if($ok==1) {
	// 		print "<script>alert(\"Nomor DO : '\"+$i_do+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
	// 	}else{
	// 		print "<script>alert(\"Maaf, DO gagal disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
	// 	}
				
	// }	
}
?>
