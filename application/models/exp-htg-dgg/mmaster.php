<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
	
	// =========================== 09-06-2015 ==============================
  // =====================================================================
  function get_all_pembelian($jenis_beli, $kategori, $date_from, $date_to, $supplier) {
		$duta = 1;
		
		// modif 27-08-2015
	if ($kategori == 1) {
			$sql =   " SELECT c.no_faktur ,f.kode_supplier, b.no_faktur_pajak, c.tgl_faktur, b.tgl_faktur_pajak, f.pkp,
			d.dpp,d.total_pajak,d.total, d.sisa_hutang ,c.tgl_input,c.tgl_update,  d.tgl_sj, d.no_sj ,g.kode_brg,g.nama_brg,
			e.qty,e.diskon
			from tm_pembelian_pajak_detail a 
					INNER JOIN tm_pembelian_pajak b ON a.id_pembelian_pajak=b.id 
					RIGHT OUTER JOIN tm_pembelian_nofaktur c ON a.id_faktur_pembelian=c.id 
					INNER JOIN tm_pembelian d ON d.no_faktur=c.no_faktur 
					INNER JOIN tm_pembelian_detail e ON e.id_pembelian=d.id 
					INNER JOIN tm_supplier f ON f.id=c.id_supplier 
					INNER JOIN tm_barang g ON e.id_brg=g.id
						WHERE d.status_aktif = 't'
						AND d.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND d.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND d.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND d.id_supplier = '$supplier' ";
		}
		else {
			$sql = " SELECT  c.no_faktur ,e.kode_supplier, b.no_faktur_pajak, b.tgl_faktur, e.pkp,
			c.dpp,d.total ,c.tgl_input,c.tgl_update,  c.tgl_sj, c.no_sj ,f.kode_brg,f.nama_brg,
			d.qty_konversi as qty ,d.diskon from tm_pembelian_makloon_faktur_detail a 
					INNER JOIN tm_pembelian_makloon_faktur b ON a.id_pembelian_makloon_faktur=b.id
					INNER JOIN tm_pembelian_makloon c ON a.id_pembelian_makloon_faktur=c.id
					INNER JOIN tm_pembelian_makloon_detail d ON d.id_pembelian_makloon=d.id
					INNER JOIN tm_supplier e ON b.id_supplier=e.id
					INNER JOIN tm_barang f ON d.id_brg=f.id
						WHERE c.status_aktif = 't'
						AND c.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
						AND c.tgl_sj <= to_date('$date_to','dd-mm-yyyy')
						AND c.jenis_pembelian = '$jenis_beli'";
			if ($supplier != '0')
				$sql.= " AND b.id_supplier = '$supplier' ";
		}
			
		$sql.= " ORDER BY tgl_sj ASC, kode_supplier ASC, no_sj ASC ";
		// ==================================================================================================
		$query	= $this->db->query($sql);
				
	
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$data_beli[] = array(		
									'no_faktur'=>$row1->no_faktur,
									'kode_supplier'=> $row1->kode_supplier,
									'no_faktur_pajak'=> $row1->no_faktur_pajak,
									'tgl_faktur'=>$row1->tgl_faktur,
									'tgl_faktur_pajak'=> $row1->tgl_faktur_pajak,
									'pkp'=>$row1->pkp,	
									'dpp'=>$row1->dpp,
									'total_pajak'=>$row1->total_pajak,	
									'total'=>$row1->total,
									'sisa_hutang'=>$row1->sisa_hutang,
									'tgl_input'=>$row1->tgl_input,
									'tgl_update'=>$row1->tgl_update	,
								
									'tgl_sj'=> $row1->tgl_sj,
									'no_sj'=> $row1->no_sj,
									'kode_brg'=> $row1->kode_brg,
									'nama_brg'=>$row1->nama_brg,
									'qty'=>$row1->qty,
									'diskon'=>$row1->diskon
									
									);
			} // endforeach header
		}
		else {
			$data_beli[] = array(		
									'no_faktur'=>"",
									'kode_supplier'=> "",
									'no_faktur_pajak'=> "",
									'tgl_faktur'=>'',
									'tgl_faktur_pajak'=> "",
									'pkp'=>"",
									'dpp'=>"",
									'total_pajak'=>"",
									'total'=>"",
									'sisa_hutang'=>"",
									'tgl_input'=>"",
									'tgl_update'=>"",
								
									'tgl_sj'=>"",
									'no_sj'=>"",
									'kode_brg'=> "",
									'nama_brg'=>"",
									'qty'=>"",
									'diskon'=>""
		);
		
	}
	return $data_beli;
}
 }
?>
