<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}
		
	function explistpiutang($i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT a.i_dt,
										a.i_dt_code,
										a.d_dt,
										b.i_nota,
										c.i_faktur_code AS i_nota_code,
										b.d_nota,
										trim(d.e_customer_name) AS e_customer_name,
										a.f_nota_sederhana
			
			FROM tm_dt a

				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
				INNER JOIN tr_customer d ON d.i_customer=b.i_customer

				WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$dfakturfirst' AND '$dfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana'

				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, b.i_nota, c.i_faktur_code, b.d_nota, trim(d.e_customer_name), a.f_nota_sederhana

				ORDER BY a.i_dt_code ASC ");
		}else{
			return $db2->query(" SELECT a.i_dt, 
										a.i_dt_code, 
										a.d_dt, 
										b.i_nota, 
										c.i_faktur_code AS i_nota_code, 
										b.d_nota, 
										trim(d.e_customer_name) AS e_customer_name, 
										a.f_nota_sederhana 
			
			FROM tm_dt a

				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
				INNER JOIN tr_customer d ON d.i_customer=b.i_customer

				WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$dfakturfirst' AND '$dfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana'

				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, b.i_nota, c.i_faktur_code, b.d_nota, trim(d.e_customer_name), a.f_nota_sederhana

				ORDER BY a.i_dt_code ASC ");			
		}
	}
	
	function totalfaktur($nofaktur,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.v_total_grand, a.v_grand_sisa AS piutang FROM tm_dt a
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
						
						WHERE a.f_dt_cancel='f' AND a.i_dt_code='$nofaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY a.v_total_grand, a.v_grand_sisa ");		
	}
	
	function clistfaktur1($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("  SELECT i_faktur_code,b.i_dt_code,d_voucher,f.i_voucher_no, x.v_grand_sisa,v_total_fppn FROM (
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur WHERE f_faktur_cancel='f' AND (d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND (d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f' AND (d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
			)x
			LEFT JOIN tm_dt_item a ON a.i_nota=x.i_faktur
			LEFT JOIN tm_dt b ON b.i_dt=a.i_dt
			LEFT JOIN tm_dt_faktur_item c ON c.i_faktur=x.i_faktur
			LEFT JOIN tm_dt_faktur d ON d.i_dt=c.i_dt
			LEFT JOIN tm_voucher_faktur_item e ON e.i_dt=d.i_dt
			LEFT JOIN tm_voucher_faktur f ON f.i_voucher=e.i_voucher
			
			GROUP BY i_faktur_code,b.i_dt_code,d_voucher,f.i_voucher_no,x.v_grand_sisa,v_total_fppn  LIMIT ".$limit." OFFSET ".$offset." ");
							
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function clistfaktur2($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
		$query	= $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
						
						INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
						
						INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
						
						WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
						
						ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
		$query	= $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
						
						INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
						
						INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
						
						WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
						
						ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");			
		}	
					
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}	
	}

	function vtotalfaktur($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		/*
		if($f_nota_sederhana=='f') {
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota						
							WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
			
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}else{
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
							WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
							
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");			
		}
		*/

		if($ndfakturfirst!='' && $ndfakturlast!='') {
			if($ndfakturfirst!='0' && $ndfakturlast!='0') {
				$filter = " WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndfakturfirst' AND '$ndfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana' ";
			}else{
				$filter = " WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana' ";
			}
		}else{
			$filter = " WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}
		
		if($f_nota_sederhana=='f') {
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
							
							".$filter."
			
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}else{
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt a
							INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
							
							".$filter."
							
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}

		if($query->num_rows()>0) {
			return $query->result();
		}
	}
				
	function clistfakturallpage1($ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT i_faktur_code,b.i_dt_code,d_voucher,f.i_voucher_no, x.v_grand_sisa,v_total_fppn FROM (
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur WHERE f_faktur_cancel='f' AND (d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f' AND (d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
			UNION
			SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa,v_total_fppn,kategory FROM tm_faktur_do_t  WHERE f_faktur_cancel='f' AND (d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
			)x
			LEFT JOIN tm_dt_item a ON a.i_nota=x.i_faktur
			LEFT JOIN tm_dt b ON b.i_dt=a.i_dt
			LEFT JOIN tm_dt_faktur_item c ON c.i_faktur=x.i_faktur
			LEFT JOIN tm_dt_faktur d ON d.i_dt=c.i_dt
			LEFT JOIN tm_voucher_faktur_item e ON e.i_dt=d.i_dt
			LEFT JOIN tm_voucher_faktur f ON f.i_voucher=e.i_voucher
			AND (x.d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
			GROUP BY i_faktur_code,b.i_dt_code,d_voucher,f.i_voucher_no,x.v_grand_sisa,v_total_fppn 
 ");
	}

	function clistfakturallpage2($ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {	
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
			
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							
				INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota
							
				INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
							
				WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
							
				GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
							
				ORDER BY a.i_dt ASC ");
		}else{
			return $db2->query(" SELECT b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt a
			
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
							
				INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
							
				INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
							
				WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
							
				GROUP BY b.i_nota, c.i_faktur_code, b.d_nota, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt

				ORDER BY a.i_dt ASC ");			
		}		
	}
	
	function lfaktur($fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana FROM tm_dt a 
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana ORDER BY a.i_dt ASC ");
	}	

	function lfakturperpages($limit,$offset,$fnotasederhana) {	
		$db2=$this->load->database('db_external', TRUE);	
		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana FROM tm_dt a 
		
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");

		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function flfaktur($key,$fnotasederhana) {	
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana FROM tm_dt a 
						INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
						
						WHERE a.f_dt_cancel='f' AND a.i_dt_code='$key' AND a.f_nota_sederhana='$fnotasederhana'
						
						GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana ORDER BY a.i_dt ASC ");
	}
	 
}

?>
