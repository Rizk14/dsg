<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_unit_jahit');
    $this->db->order_by('kode_unit','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    //$query = $this->db->getwhere('tm_unit_jahit',array('kode_unit'=>$id));
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit WHERE id='$id' ");
    return $query->result();
  }
  
  //
  function save($kode, $id_unit_jahit, $lokasi, $nama,$admin,$perusahaan,$penanggungjawab, $goedit)
  {  
    $tgl = date("Y-m-d H:i:s");
    
    // ambil id tertinggi
	$sql2 = " SELECT id FROM tm_unit_jahit ORDER BY id DESC LIMIT 1 ";
	$query2	= $this->db->query($sql2);
	if ($query2->num_rows() > 0){
		$hasil2 = $query2->row();
		$idlama	= $hasil2->id;
		$idbaru = $idlama+1;
	}
	else
		$idbaru = 1;
		
    $data = array(
	  'id'=>$idbaru,
      'kode_unit'=>$kode,
      'lokasi'=>$lokasi,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'administrator'=>$admin,
      'perusahaan'=>$perusahaan,
		  'penanggungjawab'=>$penanggungjawab
    );

    if ($goedit == '') {
		$this->db->insert('tm_unit_jahit',$data); }
	else {
		
		$data = array(
		  'kode_unit'=>$kode,
		  'lokasi'=>$lokasi,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl,
		  'administrator'=>$admin,
		  'perusahaan'=>$perusahaan,
		  'penanggungjawab'=>$penanggungjawab

		  
		  
		);
		
		$this->db->where('id',$id_unit_jahit);
		$this->db->update('tm_unit_jahit',$data);  
	}
		
  }
  
  function delete($id){    
    $this->db->delete('tm_unit_jahit', array('id' => $id));
  }
  
  // 05-04-2014
  function getAllgrupjahittanpalimit($cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.kode_unit) LIKE UPPER('%$cari%') OR UPPER(b.nama) LIKE UPPER('%$cari%')) ";
			
		$sqlnya = $this->db->query(" SELECT distinct a.kode_unit, b.nama 
							FROM tm_grup_jahit a, tm_unit_jahit b
							WHERE a.kode_unit = b.kode_unit ".$filter."
							ORDER BY a.kode_unit ");
		return $sqlnya->result();  
	}
  
  function getAllgrupjahit($limit,$offset, $cari) {
	  $filter = "";
	  if ($cari != "all")
			$filter = " AND (UPPER(b.kode_unit) LIKE UPPER('%$cari%') OR UPPER(b.nama) LIKE UPPER('%$cari%')) ";
			
	  $sqlnya	= $this->db->query(" SELECT distinct a.kode_unit, b.nama 
							FROM tm_grup_jahit a, tm_unit_jahit b
							WHERE a.kode_unit = b.kode_unit ".$filter."
							ORDER BY a.kode_unit LIMIT ".$limit." OFFSET ".$offset);
		
		if ($sqlnya->num_rows() > 0){
			$hasilnya=$sqlnya->result();
			$outputdata = array();
			$detailgrup = array();
			
			foreach ($hasilnya as $rownya) {
				$queryxx = $this->db->query(" SELECT id, nama_grup FROM tm_grup_jahit WHERE kode_unit = '".$rownya->kode_unit."' ");
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$detailgrup[] = array(	'id'=> $rowxx->id,
												'nama_grup'=> $rowxx->nama_grup
												);
					}
				}
				else
					$detailgrup = '';
					
				$outputdata[] = array(	'kode_unit'=> $rownya->kode_unit,
										'nama_unit'=> $rownya->nama,
										'detailgrup'=> $detailgrup
								);
				
				$detailgrup = array();
			} // end for
		}
		else
			$outputdata = '';
		return $outputdata;
	}
	
	function get_unit_jahit(){
		$query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	
	function savegrupjahit($unit_jahit,$namagrup) {
		$tgl = date("Y-m-d H:i:s");
		
		$listgrup = explode(";", $namagrup);
		foreach ($listgrup as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_grup_jahit 
									 WHERE kode_unit = '$unit_jahit' AND nama_grup = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$query2	= $this->db->query(" SELECT id FROM tm_grup_jahit ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_grup	= $hasilrow->id;
						$new_id_grup = $id_grup+1;
					}
					else
						$new_id_grup = 1;
					
					$datanya	= array(
							 'id'=>$new_id_grup,
							 'kode_unit'=>$unit_jahit,
							 'nama_grup'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_grup_jahit',$datanya);
				} // end if
			}
		} // end foreach
	}
	
	function deletegrupjahit($kode){
		$this->db->query(" DELETE FROM tm_grup_jahit WHERE kode_unit = '$kode' ");
	}
	
	function updategrupjahit($unit_jahit, $namagrup) {
		$tgl = date("Y-m-d H:i:s");
		
		$this->db->query(" DELETE FROM tm_grup_jahit WHERE kode_unit = '$unit_jahit' ");
		
		$listgrup = explode(";", $namagrup);
		foreach ($listgrup as $row1) {
			if (trim($row1!= '')) {
				//--------- cek apakah data udh pernah diinput atau blm. kalo udah, ga usah disave
				$sqlcek	= $this->db->query(" SELECT id FROM tm_grup_jahit 
									 WHERE kode_unit = '$unit_jahit' AND nama_grup = '$row1' ");
				if($sqlcek->num_rows() == 0) {
					$hasilsqlcek = $sqlcek->row();
					$query2	= $this->db->query(" SELECT id FROM tm_grup_jahit ORDER BY id DESC LIMIT 1 ");
					if($query2->num_rows() > 0) {
						$hasilrow = $query2->row();
						$id_grup	= $hasilrow->id;
						$new_id_grup = $id_grup+1;
					}
					else
						$new_id_grup = 1;
					
					$datanya	= array(
							 'id'=>$new_id_grup,
							 'kode_unit'=>$unit_jahit,
							 'nama_grup'=>$row1,
							 'tgl_input'=>$tgl,
							 'tgl_update'=>$tgl);
					$this->db->insert('tm_grup_jahit',$datanya);
				} // end if
				else { // jika udh ada, maka update aja (sepertinya ga akan masuk kesini, soalnya jika datanya udh pernah dipake di SJ masuk/keluar maka ga bisa diedit sama sekali
					$this->db->query(" UPDATE tm_grup_jahit SET nama_grup = '$row1' WHERE id='$hasilsqlcek->id' ");
				}
			}
		} // end foreach
	}

}

