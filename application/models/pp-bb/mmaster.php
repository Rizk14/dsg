<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function statusop($id) {
  	return $this->db->query(" SELECT * FROM tm_op WHERE status_op=true AND id_pp='$id' ");
  }
  
  function getAll($num, $offset, $jenis_brg, $cari) {
	
		if ($cari=='') {		// LUPA BLM cantumin limit dan offsetnya
			$query	= $this->db->query(" SELECT * FROM tm_pp WHERE jenis_brg = '$jenis_brg' LIMIT ".$num." OFFSET ".$offset);
		} else {
			$query	= $this->db->query(" SELECT * FROM tm_pp WHERE jenis_brg = '$jenis_brg' AND no_pp like '%$cari%' LIMIT ".$num." OFFSET ".$offset);
		}
	
		$data_pp = array();
		$detail_pp = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query( " SELECT * FROM tm_pp_detail WHERE id_pp = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT nama_brg, satuan FROM tm_bahan_baku WHERE kode_brg = '$row2->kode_brg' " );
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->satuan;
				
						$detail_pp[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan
											);
					}
				}
				else {
					$detail_pp = '';
				}
				
				$data_pp[] = array('id'=> $row1->id,	
							'no_pp'=> $row1->no_pp,
							'tgl_pp'=> $row1->tgl_pp,
							'jenis_brg'=> $row1->jenis_brg,
							'kode_bagian'=> $row1->kode_bagian,
							'tgl_update'=> $row1->tgl_update,
							'detail_pp'=> $detail_pp
						);
				
				$detail_pp = array();
			} // endforeach header
		}
		else {
			$data_pp = '';
		}
		return $data_pp;
  }
  
  function getAlltanpalimit($jenis_brg, $cari){
	if ($cari == '') {		
		return $this->db->query(" SELECT * FROM tm_pp where jenis_brg = '$jenis_brg' ");
	}  else {
		return $this->db->query(" SELECT * FROM tm_pp WHERE jenis_brg = '$jenis_brg' AND no_pp like '%$cari%' ");
	}
  }
  
  function get_header_pp($id_pp){
    $query = $this->db->getwhere('tm_pp',array('id'=>$id_pp));
    return $query->result();
  }
  
  function get_detail_pp($id_pp){
    $query = $this->db->getwhere('tm_pp_detail',array('id_pp'=>$id_pp));
    $hasil = $query->result();
    $detail_pp = array();
    foreach ($hasil as $row1) {
			$query2	= $this->db->query(" SELECT nama_brg FROM tm_bahan_baku WHERE kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query2->row();
			$nama_brg	= $hasilrow->nama_brg;
		
			$detail_pp[] = array('kode_brg'=> $row1->kode_brg,
											'nama'=> $nama_brg,
											'qty'=> $row1->qty,
											'keterangan'=> $row1->keterangan
										);
	}
	return $detail_pp;
  }  
    
  function cek_data($no_pp, $jenis_brg){
    $this->db->select("id from tm_pp WHERE no_pp = '$no_pp' AND jenis_brg = '$jenis_brg' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
		return $query->result();
	}
  }
  
  //
  function save($no_pp,$tgl_pp,$kode,$nama,$jumlah, $keterangan, $jenis_brg, $kodeedit, $goedit){  
    $tgl = date("Y-m-d");
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pp WHERE no_pp = '$no_pp' AND jenis_brg = '$jenis_brg'", false);
    $query = $this->db->get();
    $hasil = $query->result();
    if ($goedit == '') {
		// jika data header blm ada 
		if(count($hasil)== 0) {
			$data_header = array(
			  'no_pp'=>$no_pp,
			  'tgl_pp'=>$tgl_pp,
			  'kode_bagian'=>'0',
			  'jenis_brg'=>$jenis_brg,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			$this->db->insert('tm_pp',$data_header);
			
			// ambil data terakhir di tabel tm_pp
			$query2	= $this->db->query(" SELECT id FROM tm_pp ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pp	= $hasilrow->id; //echo $idnya; die();
			
			if ($kode!='' && $jumlah!='') {
				// jika semua data tdk kosong, insert ke tm_pp_detail
				$data_detail = array(
				  'kode_brg'=>$kode,
				  'qty'=>$jumlah,
				  'keterangan'=>$keterangan,
				  'id_pp'=>$id_pp
				);
				$this->db->insert('tm_pp_detail',$data_detail);
			}
		}
		else {
			// ambil id di tabel tm_pp sesuai no_pp dan kode_bagian
			$query2	= $this->db->query(" SELECT id from tm_pp WHERE no_pp = '$no_pp' AND jenis_brg = '$jenis_brg' ");
			$hasilrow = $query2->row();
			$id_pp	= $hasilrow->id;
			
			if ($kode!='' && $jumlah!='') {
				// jika semua data tdk kosong, insert ke tm_pp_detail
				$data_detail = array(
				  'kode_brg'=>$kode,
				  'qty'=>$jumlah,
				  'keterangan'=>$keterangan,
				  'id_pp'=>$id_pp
				);
				$this->db->insert('tm_pp_detail',$data_detail);
			}
		}
	} // end if goedit == ''
  }
  
  function update_pp($id_pp,$kode,$nama,$jumlah, $keterangan, $jenis_brg, $goedit){
	  if ($kode !='' && $jumlah !='') {
	  	$qop	= $this->db->query(" SELECT * FROM tm_op WHERE cast(id_pp AS character varying)='$id_pp' ORDER BY id DESC LIMIT 1 ");
	 	//if($qop->num_rows()>0) {
			$row_op	= $qop->row();
			$idop	= $row_op->id;

			// jika semua data tdk kosong, insert ke tm_pp_detail
			$data_detail = array(
				'kode_brg'=>$kode,
				'qty'=>$jumlah,
				'keterangan'=>$keterangan,
				'id_pp'=>$id_pp
				);
			//print $id_pp;
			
			$this->db->insert('tm_pp_detail',$data_detail);
			$this->db->delete('tm_op', array('id_pp' => $id_pp));
			$this->db->delete('tm_op_detail', array('id_op' => $idop));
		//}
	  }
  }  
  
  function delete($kode) { 
  	$qop	= $this->db->query(" SELECT a.id AS idpp FROM tm_pp a WHERE a.id='$kode' ORDER BY a.no_pp DESC LIMIT 1 ");
	if($qop->num_rows()>0) {
		$row_op	= $qop->row();
		$idpp	= $row_op->id_pp;
		$qtop	= $this->db->query(" SELECT * FROM tm_op WHERE cast(id_pp AS character varying)='$idpp' ORDER BY id DESC LIMIT 1 ");
		if($qtop->num_rows()>0) {
			$row_top	= $qtop->row();
			$idop	= $row_top->id;
			
			$this->db->delete('tm_pp_detail', array('id_pp' => $kode));
			$this->db->delete('tm_pp', array('id' => $kode));
			$this->db->delete('tm_op', array('id_pp' => $idpp));
			$this->db->delete('tm_op_detail', array('id_op' => $idop));
		}
	}
  }
  
  function get_bahan_baku($num, $offset, $cari)
  {
	if ($cari == '')
		$query	= $this->db->query(" SELECT a.*, b.nama as nama_kel, c.nama as nama_jenis FROM tm_bahan_baku a, tm_kelompok_barang b, tm_jenis_bahan_baku c 
					WHERE a.kode_kel_barang = b.kode AND a.kode_jenis_bahan_baku = c.kode 
					order by a.tgl_update DESC LIMIT ".$num. " OFFSET ".$offset);
	else
		$query	= $this->db->query(" SELECT a.*, b.nama as nama_kel, c.nama as nama_jenis FROM tm_bahan_baku a, tm_kelompok_barang b, tm_jenis_bahan_baku c 
					WHERE a.kode_kel_barang = b.kode AND a.kode_jenis_bahan_baku = c.kode AND (a.kode_brg like '%$cari%' OR a.nama_brg like '%$cari%')
		order by a.tgl_update DESC LIMIT ".$num. " OFFSET ".$offset);
    
    return $query->result();
  }
  
  function get_bahan_bakutanpalimit($cari){
	if ($cari == '') {
		return $this->db->query(" SELECT * FROM tm_bahan_baku ");
	}
	else {
		return $this->db->query(" SELECT * FROM tm_bahan_baku WHERE (kode_brg like '%$cari%' OR nama_brg like '%$cari%') ");
	}
  }
  
  function getpp($nomorpp,$jenis_brg) {
  	$qgetpp	= " SELECT * FROM tm_pp WHERE no_pp=trim('$nomorpp') AND jenis_brg='$jenis_brg' ";
  	return $this->db->query($qgetpp);
  }
  
  function crepp($jenis_brg) {
  	return $this->db->query(" SELECT cast(no_pp AS integer)+1 AS no_pp FROM tm_pp WHERE jenis_brg='$jenis_brg' ORDER BY cast(no_pp AS integer) DESC LIMIT 1 ");	
  }

}

?>
