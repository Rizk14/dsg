<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
          
  function get_perusahaan() {
	$query3	= $this->db->query(" SELECT id, nama, alamat, kota, bag_pembelian, bag_pembelian2, bag_keuangan, 
						kepala_bagian, bag_admstok, id_gudang_admstok, bag_admstok2, id_gudang_admstok2, 
						spv_bag_admstok FROM tm_perusahaan ");
				
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$id	= $hasilrow->id;
		$nama = $hasilrow->nama;
		$alamat = $hasilrow->alamat;
		$kota = $hasilrow->kota;
		$bag_pembelian = $hasilrow->bag_pembelian;
		$bag_pembelian2 = $hasilrow->bag_pembelian2;
		$bag_keuangan = $hasilrow->bag_keuangan;
		$kepala_bagian = $hasilrow->kepala_bagian;
		$bag_admstok = $hasilrow->bag_admstok;
		$bag_admstok2 = $hasilrow->bag_admstok2;
		$id_gudang_admstok = $hasilrow->id_gudang_admstok;
		$id_gudang_admstok2 = $hasilrow->id_gudang_admstok2;
		$spv_bag_admstok = $hasilrow->spv_bag_admstok;
	}
	else {
		$id	= '';
		$nama = '';
		$alamat = '';
		$kota = '';
		$bag_pembelian = '';
		$bag_pembelian2 = '';
		$bag_keuangan = '';
		$kepala_bagian = '';
		$bag_admstok = '';
		$bag_admstok2 = '';
		$id_gudang_admstok = '';
		$id_gudang_admstok2 = '';
		$spv_bag_admstok = '';
	}
		
	$datasetting = array('id'=> $id,
						  'nama'=> $nama,
						  'alamat'=> $alamat,
						  'kota'=> $kota,
						  'bag_pembelian'=> $bag_pembelian,
						  'bag_pembelian2'=> $bag_pembelian2,
						  'bag_keuangan'=> $bag_keuangan,
						  'kepala_bagian'=> $kepala_bagian,
						  'bag_admstok'=> $bag_admstok,
						  'bag_admstok2'=> $bag_admstok2,
						  'id_gudang_admstok'=> $id_gudang_admstok,
						  'id_gudang_admstok2'=> $id_gudang_admstok2,
						  'spv_bag_admstok'=> $spv_bag_admstok
					);
							
	return $datasetting;
  }
  
  //
  function save($id, $nama, $alamat, $kota, $bag_pembelian, $bag_pembelian2, $bag_keuangan, $kepala_bagian, $bag_admstok, $bag_admstok2, $id_gudang_admstok,$id_gudang_admstok2, $spv_bag_admstok){  
    $tgl = date("Y-m-d H:i:s");
    
    $list_id_gudang1 = '';
    for ($xx=0; $xx<count($id_gudang_admstok); $xx++) {
		$id_gudang_admstok[$xx] = trim($id_gudang_admstok[$xx]);
		$list_id_gudang1.= $id_gudang_admstok[$xx].";";
	} // end for
	
	$list_id_gudang2 = '';
    for ($xx=0; $xx<count($id_gudang_admstok2); $xx++) {
		$id_gudang_admstok2[$xx] = trim($id_gudang_admstok2[$xx]);
		$list_id_gudang2.= $id_gudang_admstok2[$xx].";";
	} // end for
    
    if ($id != '') {
		$data = array(
				  'nama'=>$nama,
				  'alamat'=>$alamat,
				  'kota'=>$kota,
				  'bag_pembelian'=>$bag_pembelian,
				  'bag_pembelian2'=>$bag_pembelian2,
				  'bag_keuangan'=>$bag_keuangan,
				  'kepala_bagian'=>$kepala_bagian,
				  'bag_admstok'=>$bag_admstok,
				  'bag_admstok2'=>$bag_admstok2,
				  'id_gudang_admstok'=>$list_id_gudang1,
				  'id_gudang_admstok2'=>$list_id_gudang2,
				  'spv_bag_admstok'=>$spv_bag_admstok,
				  'tgl_update'=>$tgl
				);
			
		$this->db->where('id',$id);
		$this->db->update('tm_perusahaan',$data);  
	}
	else {
		$data = array(
				  'id'=>1,
				  'nama'=>$nama,
				  'alamat'=>$alamat,
				  'kota'=>$kota,
				  'bag_pembelian'=>$bag_pembelian,
				  'bag_pembelian2'=>$bag_pembelian2,
				  'bag_keuangan'=>$bag_keuangan,
				  'kepala_bagian'=>$kepala_bagian,
				  'bag_admstok'=>$bag_admstok,
				  'bag_admstok2'=>$bag_admstok2,
				  'id_gudang_admstok'=>$list_id_gudang1,
				  'id_gudang_admstok2'=>$list_id_gudang2,
				  'spv_bag_admstok'=>$spv_bag_admstok,
				  'tgl_update'=>$tgl
				);
		$this->db->insert('tm_perusahaan',$data);
	}
  }
  
  // 30-07-2015
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 22-09-2015
  function get_satuan(){
    $this->db->select("* from tm_satuan order by id ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getkonversisatuan($id){
    $query = $this->db->query(" SELECT * FROM tm_konversi_satuan WHERE id='$id' ");
    return $query->result();
  }
  
  function getAllkonversisatuan(){
    $this->db->select('*');
    $this->db->from('tm_konversi_satuan');
    $this->db->order_by('id','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function savekonversisatuan($id_konversi, $id_satuan_awal, $id_satuan_konversi, $rumus_konversi, $angka_faktor_konversi, $konversi_harga_mutasi_stok, $goedit){  
    $tgl = date("Y-m-d H:i:s");
    $uid_update_by = $this->session->userdata('uid');
		
    $data = array(
      'id_satuan_awal'=>$id_satuan_awal,
      'id_satuan_konversi'=>$id_satuan_konversi,
      'rumus_konversi'=>$rumus_konversi,
      'angka_faktor_konversi'=>$angka_faktor_konversi,
      'konversi_harga_mutasi_stok'=>$konversi_harga_mutasi_stok,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl,
      'uid_update_by'=>$uid_update_by
    );

    if ($goedit == '') {
		$this->db->insert('tm_konversi_satuan',$data); }
	else {
		
		$data = array(
		  'id_satuan_awal'=>$id_satuan_awal,
		  'id_satuan_konversi'=>$id_satuan_konversi,
		  'rumus_konversi'=>$rumus_konversi,
		  'angka_faktor_konversi'=>$angka_faktor_konversi,
		  'konversi_harga_mutasi_stok'=>$konversi_harga_mutasi_stok,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$id_konversi);
		$this->db->update('tm_konversi_satuan',$data);  
	}
		
  }
  
  function deletekonversisatuan($id){    
    $this->db->delete('tm_konversi_satuan', array('id' => $id));
  }

}

