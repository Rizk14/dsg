<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
 function getAll(){
    $this->db->select("a.*, b.kode_lokasi, b.nama as nama_lokasi from tm_gudang a, tm_lokasi_gudang b , tm_jenis_gudang c WHERE a.id_lokasi = b.id  AND a.jenis = c.id
					order by a.kode_lokasi, a.kode_gudang ASC", false);
    $query = $this->db->get();
    return $query->result();
  }
  
  function get_lokasi(){
    $this->db->select("* from tm_lokasi_gudang order by kode_lokasi ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
   function get_jenis(){
    $this->db->select("* from tm_jenis_gudang order by kode_jenis ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
  function get($id){
    //$query = $this->db->getwhere('tm_gudang',array('id'=>$id));
    $query	= $this->db->query(" SELECT * FROM tm_gudang WHERE id = '$id' ");
    return $query->result();
  }
  
  function cek_data($id_lokasi, $kode_gudang){
    $this->db->select("id from tm_gudang WHERE id_lokasi = '$id_lokasi' AND kode_gudang = '$kode_gudang'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($id_lokasi,$kode_gudang, $kodeedit,$nama, $jenis, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'id_lokasi'=>$id_lokasi,
       'kode_gudang'=>$kode_gudang,
      'nama'=>$nama,
      'jenis'=>$jenis,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_gudang',$data); }
	else {
		$data = array(
		  'id_lokasi'=>$id_lokasi,
		   'kode_gudang'=>$kode_gudang,
		  'nama'=>$nama,
		  'jenis'=>$jenis,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('id',$kodeedit);
		$this->db->update('tm_gudang',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_gudang', array('id' => $kode));
  }

}
