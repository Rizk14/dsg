<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $cari, $supplier)
  {
	$sql = " a.*, b.kode_supplier, b.nama as nama_sup, c.kode_brg, c.nama_brg FROM tm_harga_quilting a INNER JOIN tm_supplier b ON a.id_unit = b.id
			INNER JOIN tm_brg_hasil_makloon c ON a.id_brg_quilting = c.id
			WHERE TRUE ";
	
	if ($supplier != '0')
		$sql.= " AND a.id_unit = '$supplier' ";
	if ($cari != "all")
		$sql.= " AND (UPPER(b.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(c.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	$sql.= " ORDER BY a.kode_unit ";
	$this->db->select($sql, false)->limit($num,$offset);
			
	/*if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg ORDER BY a.kode_unit ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg AND a.kode_unit = '$supplier' ORDER BY a.kode_unit ", false)->limit($num,$offset);
		}
	}
	else {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg AND (UPPER(a.kode_unit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg_quilting) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ORDER BY a.kode_unit ", false)->limit($num,$offset);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg 
			AND a.kode_unit = '$supplier' AND (UPPER(a.kode_unit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg_quilting) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%')) ORDER BY a.kode_unit ", false)->limit($num,$offset);
		}
    } */
    $query = $this->db->get();
    return $query->result();
  }
  
  function getAlltanpalimit($cari, $supplier){
	/*if ($cari == "all") {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg AND a.kode_unit = '$supplier' ", false);
		}
	}
	else {
		if ($supplier == '0') {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg AND (UPPER(a.kode_unit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg_quilting) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ", false);
		}
		else {
			$this->db->select(" a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a, tm_supplier b, tm_brg_hasil_makloon c 
			WHERE a.kode_unit = b.kode_supplier AND a.kode_brg_quilting = c.kode_brg 
			AND a.kode_unit = '$supplier' AND (UPPER(a.kode_unit) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(a.kode_brg_quilting) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%')) ", false);
		}
	} */
	$sql = " a.*, b.nama as nama_sup, c.nama_brg FROM tm_harga_quilting a INNER JOIN tm_supplier b ON a.id_unit = b.id
			INNER JOIN tm_brg_hasil_makloon c ON a.id_brg_quilting = c.id
			WHERE TRUE ";
	
	if ($supplier != '0')
		$sql.= " AND a.id_unit = '$supplier' ";
	if ($cari != "all")
		$sql.= " AND (UPPER(b.kode_supplier) like UPPER('%$cari%') 
			OR UPPER(b.nama) like UPPER('%$cari%') OR UPPER(c.kode_brg) like UPPER('%$cari%') OR 
			UPPER(c.nama_brg) like UPPER('%$cari%') ) ";
	$this->db->select($sql, false);
	
    $query = $this->db->get();
    return $query->result();  
  }
          
  //function cek_data($kode, $id_jenis_bhn){
  function cek_data($kode_supplier, $kode_brg){
    $this->db->select("id from tm_harga_quilting WHERE kode_brg_quilting = '$kode_brg' AND kode_unit = '$kode_supplier' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($id_supplier, $id_brg, $harga){  
    $tgl = date("Y-m-d H:i:s");
    if ($harga == '')
		$harga = 0;
    
    if ($harga != 0) {
		$data = array(
		  'id_brg_quilting'=>$id_brg,
		  'id_unit'=>$id_supplier,
		  'harga'=>$harga,
		  'tgl_input'=>$tgl,
		  'tgl_update'=>$tgl
		);
		$this->db->insert('tm_harga_quilting',$data); 
		
		/*$this->db->query(" INSERT INTO tt_harga (kode_brg, kode_supplier, harga, tgl_input) 
							VALUES ('$kode_brg','$kode_supplier', '$harga', '$tgl') "); */
	}
  }
  
  function delete($id){    
    $this->db->delete('tm_harga_quilting', array('id' => $id));
  }
  
  function get_jenis_barang($num, $offset, $kel_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_barangtanpalimit($kel_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_barang WHERE kode_kel_brg = '$kel_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_jns_bhn(){
    $this->db->select("a.kode as kj_brg, a.nama as nj_brg, b.* from tm_jenis_barang a, tm_jenis_bahan b 
    WHERE a.id = b.id_jenis_barang order by a.kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_jenis_bahan($num, $offset, $jns_brg, $cari) {
	if ($cari == "all") {		
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	else { 
		$this->db->select(" * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) order by tgl_update DESC ", false)->limit($num,$offset);
		$query = $this->db->get();
	}
	return $query->result();

  }
  
  function get_jenis_bahantanpalimit($jns_brg, $cari){
	if ($cari == "all") {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' ");
	}
	else {
		$query	= $this->db->query(" SELECT * FROM tm_jenis_bahan WHERE id_jenis_barang = '$jns_brg' AND (UPPER(kode) like UPPER('%$cari%') OR UPPER(nama) like UPPER('%$cari%')) ");
	}
    
    return $query->result();  
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori = '2' ORDER BY kode_supplier ");    
    return $query->result();  
  }
  
  function get_barang($id_supplier) {
	$sql = " SELECT * FROM tm_brg_hasil_makloon ORDER BY kode_brg ";
	$query	= $this->db->query($sql);    
	
	$data_harga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			$query3	= $this->db->query(" SELECT harga FROM tm_harga_quilting WHERE id_brg_quilting = '$row1->id'
									AND id_unit = '$id_supplier' ");
			if ($query3->num_rows() == 0){
				$harga	= '';
			}
			else {
				$hasilrow = $query3->row();
				$harga	= $hasilrow->harga;
			}
				$data_harga[] = array(			
								'id_brg'=> $row1->id,
								'kode_brg'=> $row1->kode_brg,
								'nama_brg'=> $row1->nama_brg,
								'harga'=> $harga
							);
		} // end foreach
	}
	else
		$data_harga = '';
    return $data_harga;  
  }
  
  function get_harga($idharga){
	$query = $this->db->query(" SELECT * FROM tm_harga_quilting WHERE id = '$idharga' ");
	$dataharga = array();
	if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil data nama supplier
			$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$row1->id_unit' ");
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
			
			// ambil data nama barang
			$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_brg_hasil_makloon WHERE id = '$row1->id_brg_quilting' ");
			$hasilrow = $query3->row();
			$kode_brg	= $hasilrow->kode_brg;
			$nama_brg	= $hasilrow->nama_brg;
			
			$dataharga[] = array(			'id'=> $row1->id,	
											'id_brg'=> $row1->id_brg_quilting,
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'id_supplier'=> $row1->id_unit,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'harga'=> $row1->harga,
											'tgl_update'=> $row1->tgl_update
											);
		}
	}
	return $dataharga;
  }

}

