<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_stok($num, $offset, $gudang, $cstatus, $cari) {
		/*$sql = " kode_brg, nama_brg from
				(select kode_brg, nama_brg from tm_barang UNION
				select kode_brg, nama_brg from tm_brg_hasil_makloon
				) as foo "; */
	
	$sql = " a.id, d.kode_brg, d.nama_brg, a.stok, a.tgl_update_stok, a.id_gudang FROM tm_stok_hasil_jahit a
			LEFT JOIN tm_barang_wip d ON d.id = a.id_brg_wip WHERE d.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.kode_brg) like UPPER('%$cari%') OR UPPER(d.nama_brg) like UPPER('%$cari%')) ";
	if ($gudang != '0')
		$sql.= " AND a.id_gudang = '$gudang' ";
	$sql.= " ORDER BY d.kode_brg ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	$detailwarna = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
					// 13-02-2014, stok per warna
					$queryxx	= $this->db->query(" SELECT a.id_warna, c.nama, a.stok FROM tm_stok_hasil_jahit_warna a 
									INNER JOIN tm_warna c ON c.id = a.id_warna
									WHERE a.id_stok_hasil_jahit = '$row1->id'
									ORDER BY c.nama ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
													'nama_warna'=> $rowxx->nama,
													'stok'=> $rowxx->stok
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
					
					$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON b.id = a.id_lokasi 
								WHERE a.id = '$row1->id_gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
								
				$data_stok[] = array(		'kode_brg_wip'=> $row1->kode_brg,	
											'nama_brg_wip'=> $row1->nama_brg,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'kode_gudang'=> $kode_gudang,	
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stoktanpalimit($gudang, $cstatus, $cari){
	$sql = " SELECT a.id, d.kode_brg, d.nama_brg, a.stok, a.tgl_update_stok, a.id_gudang FROM tm_stok_hasil_jahit a
			LEFT JOIN tm_barang_wip d ON d.id = a.id_brg_wip 
			WHERE d.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.kode_brg) like UPPER('%$cari%') OR UPPER(d.nama_brg) like UPPER('%$cari%')) ";
	if ($gudang != '0')
		$sql.= " AND a.id_gudang = '$gudang' ";
	  
		$query	= $this->db->query($sql);
    
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE a.jenis = '2' ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 13-02-2014
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 06-11-2015
  function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_stokunit($num, $offset, $unit_jahit, $cstatus, $cari) {	
	$sql = " a.id, d.kode_brg, d.nama_brg, a.stok, a.tgl_update_stok, a.id_unit FROM tm_stok_unit_jahit a
			LEFT JOIN tm_barang_wip d ON d.id = a.id_brg_wip WHERE d.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.kode_brg) like UPPER('%$cari%') OR UPPER(d.nama_brg) like UPPER('%$cari%')) ";
	if ($unit_jahit != '0')
		$sql.= " AND a.id_unit = '$unit_jahit' ";
	$sql.= " ORDER BY d.kode_brg, a.id_unit "; 

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	$detailwarna = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
									
					// 13-02-2014, stok per warna
					$queryxx	= $this->db->query(" SELECT a.id_warna, c.nama as nama_warna, a.stok, a.stok_bagus, a.stok_perbaikan 
									FROM tm_stok_unit_jahit_warna a INNER JOIN tm_warna c ON a.id_warna = c.id
									WHERE a.id_stok_unit_jahit = '$row1->id' ORDER BY c.nama ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
													'nama_warna'=> $rowxx->nama_warna,
													'stok'=> $rowxx->stok,
													'stok_bagus'=> $rowxx->stok_bagus,
													'stok_perbaikan'=> $rowxx->stok_perbaikan
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
					
					$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE id = '$row1->id_unit' ");
					$hasilrow = $query3->row();
					$kode_unit	= $hasilrow->kode_unit;
					$nama_unit	= $hasilrow->nama;
								
				$data_stok[] = array(		'kode_brg_wip'=> $row1->kode_brg,	
											'nama_brg_wip'=> $row1->nama_brg,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'kode_unit'=> $kode_unit,	
											'nama_unit'=> $nama_unit,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stokunittanpalimit($unit_jahit, $cstatus, $cari){
		$sql = " SELECT a.id, d.kode_brg, d.nama_brg, a.stok, a.tgl_update_stok, a.id_unit FROM tm_stok_unit_jahit a
			LEFT JOIN tm_barang_wip d ON d.id = a.id_brg_wip WHERE d.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.kode_brg) like UPPER('%$cari%') OR UPPER(d.nama_brg) like UPPER('%$cari%')) ";
	if ($unit_jahit != '0')
		$sql.= " AND a.id_unit = '$unit_jahit' ";
	  
		$query	= $this->db->query($sql);
    
    return $query->result();  
  }
  
  // 27-08-2014
  function get_stok_gudang_setengah_jadi($gudang, $cari) {	
	  // contoh
			
	/*$db2 = $this->load->database('db_external', TRUE);
	$sqlx = " SELECT * FROM tr_product_motif ORDER BY i_product_motif ";
	$queryx	= $db2->query($sqlx);
	echo $queryx->num_rows(); die();
	if ($queryx->num_rows() > 0){
		$hasilx = $queryx->result();
		foreach ($hasilx as $row1) {
			$data_stok[] = array(		'kode_brg_jadi'=> $row1->i_product_motif	
											'nama_brg_jadi'=> $nama_brg_jadi,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'kode_gudang'=> $kode_gudang,	
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detailwarna'=> $detailwarna
											);
		}
	} */
	
	$sql = " a.id, a.kode_brg_jadi, a.stok, a.tgl_update_stok, a.id_gudang, e.kode, e.nama FROM tm_stok_hasil_jahit a
			LEFT JOIN tr_product_motif d ON d.i_product_motif = a.kode_brg_jadi 
			LEFT JOIN tm_kel_brg_jadi e ON e.id = d.id_kel_brg_jadi
			WHERE TRUE ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.i_product_motif) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%')) ";
	if ($gudang != '0')
		$sql.= " AND a.id_gudang = '$gudang' ";
	$sql.= " ORDER BY e.kode, a.kode_brg_jadi ";
		
	$this->db->select($sql, false);
	$query = $this->db->get();
		
	$data_stok = array();
	$detailwarna = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				
					// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row1->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
						
					// 27-08-2014, ambil stok di gudang jadi
				/*	$db2 = $this->load->database('db_external', TRUE);
					$sqlx = " SELECT n_quantity_akhir FROM tm_stokopname_item WHERE i_product='$row1->kode_brg_jadi'
							ORDER BY d_entry DESC LIMIT 1 ";
					$queryx	= $db2->query($sqlx);
					
					if ($queryx->num_rows() > 0){
						$hasilx = $queryx->row();
						$stok_gudang_jadi = $hasilx->n_quantity_akhir;
					}
					else */
						$stok_gudang_jadi = 0;
					
					// 13-02-2014, stok per warna
					$queryxx	= $this->db->query(" SELECT c.kode, c.nama, a.stok FROM tm_stok_hasil_jahit_warna a, 
									tm_warna c
									WHERE a.kode_warna = c.kode AND a.id_stok_hasil_jahit = '$row1->id' ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'kode_warna'=> $rowxx->kode,
													'nama_warna'=> $rowxx->nama,
													'stok'=> $rowxx->stok
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
					
					$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi;
								
				$data_stok[] = array(		'kode_brg_jadi'=> $row1->kode_brg_jadi,	
											'nama_brg_jadi'=> $nama_brg_jadi,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'kode_gudang'=> $kode_gudang,	
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'stok_gudang_jadi'=> $stok_gudang_jadi,
											'detailwarna'=> $detailwarna,
											// 24-11-2014
											'kode_kel'=> $row1->kode,
											'nama_kel'=> $row1->nama
											);
				$detailwarna = array();
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  // 06-11-2015
  function get_stokunitpacking($num, $offset, $unit_packing, $cstatus, $cari) {	
	$sql = " a.id, d.kode_brg, d.nama_brg, a.stok, a.tgl_update_stok, a.id_unit FROM tm_stok_unit_packing a
			LEFT JOIN tm_barang_wip d ON d.id = a.id_brg_wip WHERE d.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.kode_brg) like UPPER('%$cari%') OR UPPER(d.nama_brg) like UPPER('%$cari%')) ";
	if ($unit_packing != '0')
		$sql.= " AND a.id_unit = '$unit_packing' ";
	$sql.= " ORDER BY d.kode_brg, a.id_unit "; 

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	$detailwarna = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
									
					// 13-02-2014, stok per warna
					$queryxx	= $this->db->query(" SELECT a.id_warna, c.nama as nama_warna, a.stok
									FROM tm_stok_unit_packing_warna a INNER JOIN tm_warna c ON a.id_warna = c.id
									WHERE a.id_stok_unit_packing = '$row1->id' ORDER BY c.nama ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'id_warna'=> $rowxx->id_warna,
													'nama_warna'=> $rowxx->nama_warna,
													'stok'=> $rowxx->stok
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
					
					$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_packing WHERE id = '$row1->id_unit' ");
					$hasilrow = $query3->row();
					$kode_unit	= $hasilrow->kode_unit;
					$nama_unit	= $hasilrow->nama;
								
				$data_stok[] = array(		'kode_brg_wip'=> $row1->kode_brg,	
											'nama_brg_wip'=> $row1->nama_brg,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'kode_unit'=> $kode_unit,	
											'nama_unit'=> $nama_unit,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stokunitpackingtanpalimit($unit_packing, $cstatus, $cari){
		$sql = " SELECT a.id, d.kode_brg, d.nama_brg, a.stok, a.tgl_update_stok, a.id_unit FROM tm_stok_unit_packing a
			LEFT JOIN tm_barang_wip d ON d.id = a.id_brg_wip WHERE d.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(d.kode_brg) like UPPER('%$cari%') OR UPPER(d.nama_brg) like UPPER('%$cari%')) ";
	if ($unit_packing != '0')
		$sql.= " AND a.id_unit = '$unit_packing' ";
	  
		$query	= $this->db->query($sql);
    
    return $query->result();  
  }

}
