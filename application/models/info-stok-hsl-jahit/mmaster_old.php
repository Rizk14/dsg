<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_stok($num, $offset, $cari) {
		/*$sql = " kode_brg, nama_brg from
				(select kode_brg, nama_brg from tm_barang UNION
				select kode_brg, nama_brg from tm_brg_hasil_makloon
				) as foo "; */
	
	$sql = " a.kode_brg_jadi, a.stok, a.tgl_update_stok FROM tm_stok_hasil_jahit a
			LEFT JOIN tr_product_motif d ON d.i_product_motif = a.kode_brg_jadi ";
	
	if ($cari != "all")
		$sql.= " WHERE UPPER(d.i_product_motif) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%') ";
	$sql.= " ORDER BY a.tgl_update_stok "; 

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				
				// brg jadi
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
										WHERE i_product_motif = '$row1->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg_jadi	= $hasilrow->e_product_motifname;
						}
						else {
							$nama_brg_jadi	= '';
						}
								
				$data_stok[] = array(		'kode_brg_jadi'=> $row1->kode_brg_jadi,	
											'nama_brg_jadi'=> $nama_brg_jadi,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok
											);
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stoktanpalimit($cari){
		$sql = " select a.kode_brg_jadi, a.stok, a.tgl_update_stok FROM tm_stok_hasil_jahit a
			LEFT JOIN tr_product_motif d ON d.i_product_motif = a.kode_brg_jadi ";
	
	if ($cari != "all")
		$sql.= " WHERE UPPER(d.i_product_motif) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%') "; 
	  
		$query	= $this->db->query($sql);
    
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
