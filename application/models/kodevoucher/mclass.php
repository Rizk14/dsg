<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function jnsvoucher(){
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query(" SELECT * FROM tr_jenis_voucher ORDER BY e_jenis_voucher ASC ");
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}		
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_kode_voucher ORDER BY i_voucher_code DESC ");
	}

	function viewperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_kode_voucher  ORDER BY i_voucher_code DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($ijnsvoucherhidden,$kodevoucher,$nmvoucher,$ketvoucher) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qijnsvoucher = $db2->query(" SELECT i_voucher FROM tr_kode_voucher ORDER BY i_voucher DESC LIMIT 1 ");
		
		if($qijnsvoucher->num_rows()>0){
			$rijnsvoucher	= $qijnsvoucher->row();
			$ivoucher		= $rijnsvoucher->i_voucher+1;
		}else{
			$ivoucher		= 1;
		}
		
		$db2->set(array( 
			'i_voucher' => $ivoucher,
			'i_jenis_voucher' => $ijnsvoucherhidden,
			'i_voucher_code' => $kodevoucher,
			'e_voucher_name' => $nmvoucher,
			'e_description' => $ketvoucher,
			'd_entry'=> $dentry
		));

		if($db2->insert('tr_kode_voucher')){
		  redirect('kodevoucher/cform/');
		} else {
		  print "<script>alert(\"Maaf, Data Kode Voucher gagal disimpan\");show(\"kodevoucher/cform\",\"#content\");</script>";	
		}
	}
	
	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_kode_voucher WHERE i_voucher='$id' ORDER BY i_voucher DESC LIMIT 1 " );
	}
	
	function jnsvoucher2($ijnsvoucher) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_jenis_voucher WHERE i_jenis='$ijnsvoucher' ORDER BY i_jenis DESC LIMIT 1 " );
	}
	
	function mupdate($ivoucher,$ijnsvoucherhidden,$kodevoucher,$nmvoucher,$ketvoucher) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
				
		$kodevoucher_item	= array(
			'i_jenis_voucher' => $ijnsvoucherhidden,
			'i_voucher_code' => $kodevoucher,
			'e_voucher_name' => $nmvoucher,
			'e_description' => $ketvoucher,
			'd_update'=> $dentry
		);
		
		$db2->update('tr_kode_voucher',$kodevoucher_item,array('i_voucher'=>$ivoucher));
		redirect('kodevoucher/cform');
	}
	
	function viewcari($txtcari) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_kode_voucher WHERE i_voucher_code LIKE '$txtcari%' OR e_voucher_name LIKE '$txtcari%' OR e_description LIKE '%$txtcari%' ORDER BY i_voucher_code ASC ");
	}
	
	function mcari($txtcari,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_kode_voucher WHERE i_voucher_code LIKE '$txtcari%' OR e_voucher_name LIKE '$txtcari%' OR e_description LIKE '%$txtcari%' ORDER BY i_voucher_code ASC LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_kode_voucher',array('i_voucher'=>$id));
		redirect('kodevoucher/cform/');	 
  }		
}
?>
