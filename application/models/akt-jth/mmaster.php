<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto)#,$iarea)
    {
		$this->db->select("	a.*, b.d_mutasi, b.e_description, c.nama from tm_jurnal_transharianitem a, tm_jurnal_transharian b, tm_area c
							where b.d_mutasi >= to_date('$dfrom','dd-mm-yyyy') 
							and b.d_mutasi <= to_date('$dto','dd-mm-yyyy') 
							and b.i_area=c.id
							and a.i_refference=b.i_refference and a.d_refference=b.d_refference and 
							((a.i_area=b.i_area and not b.i_refference like 'BK%') or (a.i_coa_bank=b.i_coa_bank and b.i_refference like 'BK%'))
							order by b.d_mutasi, a.i_refference, a.f_debet desc ",false);
#							and a.i_area like '%$iarea%'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						   order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
