<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}
	
	function lbarangjadiperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
						
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi() {
	$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}

	function flbarangjadi($key) {
	$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT count(cast(a.i_faktur_code AS integer)) AS jmlifaktur,
						a.i_faktur_code AS ifakturcode,
						a.d_faktur AS dfaktur
				
				FROM tm_faktur_item b
				
				INNER JOIN tm_faktur a ON a.i_faktur=b.i_faktur
				INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(b.i_product)
				INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
				
				WHERE a.i_faktur_code='$key' AND d.f_sj_cancel='f' AND d.f_faktur_created='t'
				
				GROUP BY a.d_faktur, a.i_faktur_code
				
				ORDER BY a.d_faktur DESC, a.i_faktur_code DESC ");
	}
			
	function lklsbrg() {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT a.* FROM tr_class a ORDER BY e_class_name, i_class ASC " );
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	
	function clistpenjualanndoperpages($nofaktur,$tfirst,$tlast,$limit,$offset) {
$db2=$this->load->database('db_external', TRUE);
		if( (!empty($nofaktur) && $nofaktur!='0') && ( (!empty($tfirst) && !empty($tlast) ) && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= " AND (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			
		} else if( (!empty($nofaktur) && $nofaktur!='0') && ($tfirst=='0' || $tlast=='0') ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur	= "";		
		} else if( $nofaktur=='0' && ( (!empty($tfirst) && !empty($tlast))  && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= "";
			$dfaktur	= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";			
		} else {
			$nfaktur	= "";
			$dfaktur	= "";
			
		}
			
		if($nofaktur!="") {
			$qstr	= " SELECT  c.i_faktur_code AS ifakturcode,	
						a.e_customer_name AS customername,
						a.e_customer_npwp AS npwp,
						c.d_faktur AS dfaktur,
						c.d_due_date AS dduedate,
						c.d_pajak AS dpajak,
						c.n_discount AS discountpersen,
						c.v_discount AS discountnilai,
						c.v_total_faktur AS totalfaktur,
						c.v_total_fppn AS tambahppndiscount		
					
					
					FROM tr_customer a
								
						INNER JOIN tr_branch b ON b.i_customer=a.i_customer
						INNER JOIN tm_faktur c ON c.e_branch_name=b.e_initial
						INNER JOIN tm_faktur_item d ON d.i_faktur=cast(c.i_faktur AS integer)

						INNER JOIN tm_sj_item e ON trim(e.i_product)=trim(d.i_product)
						INNER JOIN tm_sj f ON trim(f.i_sj)=trim(e.i_sj)
										
						".$nfaktur." ".$dfaktur."
						
						GROUP BY a.e_customer_name, 
							a.e_customer_npwp, 
							c.i_faktur_code, 
							c.d_faktur,
							c.d_due_date, 
							c.d_pajak,
							c.v_total_faktur, 
							c.n_discount,
							c.v_discount,
							c.v_total_fppn 
							
							ORDER BY c.i_faktur_code ASC, c.d_faktur DESC LIMIT ".$limit." OFFSET ".$offset;
		} else {
			$qstr	= " SELECT  c.i_faktur_code AS ifakturcode,	
						a.e_customer_name AS customername,
						a.e_customer_npwp AS npwp,
						c.d_faktur AS dfaktur,
						c.d_due_date AS dduedate,
						c.d_pajak AS dpajak,
						c.n_discount AS discountpersen,
						c.v_discount AS discountnilai,
						c.v_total_faktur AS totalfaktur,
						c.v_total_fppn AS tambahppndiscount		
					
					
					FROM tr_customer a
								
						INNER JOIN tr_branch b ON b.i_customer=a.i_customer
						INNER JOIN tm_faktur c ON c.e_branch_name=b.e_initial
						INNER JOIN tm_faktur_item d ON d.i_faktur=cast(c.i_faktur AS integer)

						INNER JOIN tm_sj_item e ON trim(e.i_product)=trim(d.i_product)
						INNER JOIN tm_sj f ON trim(f.i_sj)=trim(e.i_sj)
												
						GROUP BY a.e_customer_name, 
							a.e_customer_npwp, 
							c.i_faktur_code, 
							c.d_faktur,
							c.d_due_date, 
							c.d_pajak,
							c.v_total_faktur, 
							c.n_discount,
							c.v_discount,
							c.v_total_fppn 
							
							ORDER BY c.i_faktur_code ASC, c.d_faktur DESC LIMIT ".$limit." OFFSET ".$offset;		
		}
		
		$query	= $db2->query($qstr);
						
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}

	function clistpenjualanndo($nofaktur,$tfirst,$tlast) {
		$db2=$this->load->database('db_external', TRUE);
		if( (!empty($nofaktur) && $nofaktur!='0') && ( (!empty($tfirst) && !empty($tlast) ) && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur		= " AND (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			
		} else if( (!empty($nofaktur) && $nofaktur!='0') && ($tfirst=='0' || $tlast=='0') ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur		= "";		
		} else if( $nofaktur=='0' && ( (!empty($tfirst) && !empty($tlast))  && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= "";
			$dfaktur		= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";			
		} else {
			$nfaktur	= "";
			$dfaktur		= "";
			
		}	
		
		if($nofaktur!="") {
			$qstr	= " SELECT  c.i_faktur_code AS ifakturcode,	
						a.e_customer_name AS customername,
						a.e_customer_npwp AS npwp,
						c.d_faktur AS dfaktur,
						c.d_due_date AS dduedate,
						c.d_pajak AS dpajak,
						c.n_discount AS discountpersen,
						c.v_discount AS discountnilai,
						c.v_total_faktur AS totalfaktur,
						c.v_total_fppn AS tambahppndiscount				
					
					FROM tr_customer a
								
						INNER JOIN tr_branch b ON b.i_customer=a.i_customer
						INNER JOIN tm_faktur c ON c.e_branch_name=b.e_initial
						INNER JOIN tm_faktur_item d ON d.i_faktur=cast(c.i_faktur AS integer)

						INNER JOIN tm_sj_item e ON trim(e.i_product)=trim(d.i_product)
						INNER JOIN tm_sj f ON trim(f.i_sj)=trim(e.i_sj)
												
						".$nfaktur." ".$dfaktur."
						
						GROUP BY a.e_customer_name, 
							a.e_customer_npwp, 
							c.i_faktur_code, 
							c.d_faktur,
							c.d_due_date, 
							c.d_pajak,
							c.v_total_faktur, 
							c.n_discount,
							c.v_discount,
							c.v_total_fppn 
							
						ORDER BY c.i_faktur_code ASC, c.d_faktur DESC ";
		} else {
			$qstr	= " SELECT  c.i_faktur_code AS ifakturcode,	
						a.e_customer_name AS customername,
						a.e_customer_npwp AS npwp,
						c.d_faktur AS dfaktur,
						c.d_due_date AS dduedate,
						c.d_pajak AS dpajak,
						c.n_discount AS discountpersen,
						c.v_discount AS discountnilai,
						c.v_total_faktur AS totalfaktur,
						c.v_total_fppn AS tambahppndiscount		
					
					
					FROM tr_customer a
								
						INNER JOIN tr_branch b ON b.i_customer=a.i_customer
						INNER JOIN tm_faktur c ON c.e_branch_name=b.e_initial
						INNER JOIN tm_faktur_item d ON d.i_faktur=cast(c.i_faktur AS integer)

						INNER JOIN tm_sj_item e ON trim(e.i_product)=trim(d.i_product)
						INNER JOIN tm_sj f ON trim(f.i_sj)=trim(e.i_sj)
												
						GROUP BY a.e_customer_name, 
							a.e_customer_npwp, 
							c.i_faktur_code, 
							c.d_faktur,
							c.d_due_date, 
							c.d_pajak,
							c.v_total_faktur, 
							c.n_discount,
							c.v_discount,
							c.v_total_fppn 
							
						ORDER BY c.i_faktur_code ASC, c.d_faktur DESC ";		
		}

		return $db2->query($qstr);
	}

	function explistpenjualanndo($nofaktur,$tfirst,$tlast) {
		$db2=$this->load->database('db_external', TRUE);
		if( (!empty($nofaktur) && $nofaktur!='0') && ( (!empty($tfirst) && !empty($tlast) ) && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur		= " AND (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";
			
		} else if( (!empty($nofaktur) && $nofaktur!='0') && ($tfirst=='0' || $tlast=='0') ) {
			$nfaktur	= " WHERE c.i_faktur_code='$nofaktur' ";
			$dfaktur		= "";		
		} else if( $nofaktur=='0' && ( (!empty($tfirst) && !empty($tlast))  && ($tfirst!='0' && $tlast!='0') ) ) {
			$nfaktur	= "";
			$dfaktur		= " WHERE (c.d_faktur BETWEEN '$tfirst' AND '$tlast') ";			
		} else {
			$nfaktur	= "";
			$dfaktur		= "";
			
		}
		
			$strq	= " SELECT c.i_faktur_code AS ifakturcode,	
					a.e_customer_name AS customername,
					a.e_customer_npwp AS npwp,
					c.d_faktur AS dfaktur,
					c.d_pajak AS dpajak,
					c.d_due_date AS dduedate,
					c.n_discount AS discountpersen,
					c.v_discount AS nilaidiscon,
					c.v_total_faktur AS totalfaktur,
					(c.v_total_faktur-c.v_discount) AS dpp,
					c.v_total_fppn AS totalsetelahppndiscount
				
				
				FROM tr_customer a
							
					INNER JOIN tr_branch b ON b.i_customer=a.i_customer
					INNER JOIN tm_faktur c ON c.e_branch_name=b.e_initial
					INNER JOIN tm_faktur_item d ON d.i_faktur=cast(c.i_faktur AS integer)

					INNER JOIN tm_sj_item e ON trim(e.i_product)=trim(d.i_product)
					INNER JOIN tm_sj f ON trim(f.i_sj)=trim(e.i_sj)
											
					".$nfaktur." ".$dfaktur."
					
					GROUP BY a.e_customer_name, 
						a.e_customer_npwp, 
						c.i_faktur_code, 
						c.d_faktur,
						c.d_pajak,
						c.d_due_date, 
						c.v_total_faktur, 
						c.n_discount,
						c.v_discount,
						c.v_total_fppn ORDER BY c.i_faktur_code ASC, c.d_faktur DESC ";
		
		return $db2->query($strq);
	}
		
	function pajak($nofaktur) {
		$db2=$this->load->database('db_external', TRUE);
		$qstr	= " SELECT 	b.i_faktur_pajak AS ifakturpajak,
					b.d_pajak
			
			FROM tm_faktur b
			
			INNER JOIN tm_faktur_item a ON cast(b.i_faktur AS integer)=a.i_faktur
			INNER JOIN tm_sj_item c ON trim(c.i_product)=trim(a.i_product)
			INNER JOIN tm_sj d ON trim(d.i_sj)=trim(c.i_sj)
			
			WHERE b.i_faktur_code='$nofaktur'
			
			GROUP BY b.i_faktur_pajak, b.d_pajak ";
			
		return $db2->query($qstr);
	}	
}
?>
