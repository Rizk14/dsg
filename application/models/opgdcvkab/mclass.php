<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function get_op_kab($icustomer){
		$db2	= $this->load->database('db_external', TRUE);

		$data_op = array(); 
		$i=1;
		$d_opyes=date("Y-m-d",strtotime(""));

		//* PERBAIKAN ITEM DOUBEL KARENA TIDAK DI SUM (6 Agst 2021)
		$query = $db2->query ("select i_po_no, d_po, i_customer, i_branch_code, e_initial, 
							i_product, i_product_code, e_product_name, 
							sum(n_quantity) AS n_quantity, f_po_transfer  
							FROM vw_import_op_group_detail where f_po_transfer = 't' 
							and i_customer = '$icustomer'
							AND to_char(d_po, 'yyyy') >'2021'
							/*AND NOT i_po_no IN (SELECT no_op_kab FROM tm_op) */
							group by i_po_no, d_po, i_customer, i_branch_code, e_initial, 
							i_product, i_product_code, e_product_name, f_po_transfer  
							order by i_po_no desc ", false);
		//$query = $this->db->get();
//var_dump($query);
		if ($query->num_rows() > 0 ){
			$result = $query->result();
			foreach ($result as $row) {
				$data_op[] = array(
						'no' => $i,
						'i_po_no' => $row->i_po_no,
						'd_po' => $row->d_po,
						'i_branch_code' => $row->i_branch_code,
						'e_initial' => $row->e_initial,
						'i_product' => $row->i_product,
						'i_product_code' => $row->i_product_code,
						'e_product_name' => $row->e_product_name,
						'n_quantity' => $row->n_quantity

					);
				$i++;
			}	
		}
		return $data_op;
	}

	function simpanopkab(	$no_op,$tgl_op,$i_product, $i_product_motif,$e_product_name, $qty, $fck,$iterasi, 
							$i_customer, $i_branch_code, $d_delivery_limit){
		$tahun	= date("Y");
		$db2	= $this->load->database('db_external', TRUE);
		
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date");
		$drow	= $qdate->row();
		$dentry = $drow->date;
					
		$this->db->trans_begin();

	for($xx=1; $xx<=$iterasi-1; $xx++) {
		if ($fck[$xx] == 'y') {

		$noop2 = substr($no_op[$xx],-5);
		$noop2 = "KAB-".$tahun.$noop2; 

			$qopsab = $db2->query("SELECT i_op FROM tm_op WHERE i_op_code = '".$noop2."' AND to_char(d_op,'yyyy') = '".date('Y',strtotime($tgl_op[$xx]))."' AND f_op_cancel = 'f' ");
			
			if($qopsab->num_rows()==0){
		
				$seq_tm_op	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_op 
								ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
					if($seq_tm_op->num_rows() > 0 ) {
						$seqrow	= $seq_tm_op->row();
						$iop	= $seqrow->i_op+1;
					}else{
						$iop	= 1;
					}
					
					// *) sequence i_sop
				$seq_tm_sop	= $db2->query(" SELECT i_sop FROM tm_sop ORDER BY i_sop DESC LIMIT 1 ");
					if($seq_tm_sop->num_rows() > 0 ) {
						$seqrow2= $seq_tm_sop->row();
						$isop	= $seqrow2->i_sop+1;
					}else{
						$isop	= 1;
					}

				$qrytsop = $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),1,4) AS thisop FROM tm_op ORDER BY i_op DESC LIMIT 1 " );
				$qrysop	 = $db2->query(" SELECT SUBSTRING(cast(i_sop AS character varying),5,(LENGTH(cast(i_sop AS character varying))-4)) AS isop FROM tm_op ORDER BY i_op DESC LIMIT 1 ");
				if($qrytsop->num_rows() > 0) {
					$th		= $qrytsop->row_array();
					$thn2	= $th['thisop'];
				} else {
					$thn2	= $tahun;
				}
					
					if($thn2==$tahun) {
						if($qrysop->num_rows() > 0)  {
							$row2	= $qrysop->row_array();
							$sop		= $row2['isop']+1;		
							switch(strlen($sop)) {
								case "1": $nomorsop	= "0000".$sop;
								break;
								case "2": $nomorsop	= "000".$sop;
								break;	
								case "3": $nomorsop	= "00".$sop;
								break;
								case "4": $nomorsop	= "0".$sop;
								break;
								case "5": $nomorsop	= $sop;
								break;	
							}
						} else {
							$nomorsop		= "00001";
						}
						$nomor2	= $tahun.$nomorsop."B";
					} else {
						$nomor2	= $tahun."00001";
					}


				//insert ke tm_op dan tm_sop
				$tm_op = array(
						'i_op' 				=> $iop,
						'i_op_code' 		=> $noop2,
						'i_customer'		=> $i_customer,
						'i_branch'			=> $i_branch_code,
						'd_op'				=> $tgl_op[$xx],
						'd_delivery_limit'	=> $d_delivery_limit,
						'i_sop'				=> $nomor2,
						'd_entry'			=> $dentry,
						'd_update'			=> $dentry,
						'f_transfer_op'		=> 't',
						'no_op_kab'			=> $no_op[$xx]
						);
				$db2->insert('tm_op',$tm_op);

				$tm_sop = array(
						'i_sop'				=> $isop,
						'i_sop_code'		=> $nomor2,
						'i_customer'		=> $i_customer,
						'i_branch'			=> $i_branch_code,
						'd_sop'				=> $tgl_op[$xx],
						'i_op'				=> $iop,
						'd_entry'			=> $dentry,
						'd_update'			=> $dentry,
						'f_transfer_op'		=> 't'
						);
				$db2->insert('tm_sop',$tm_sop);



				//tabel 2 item barang
				$qbrgjadi = $db2->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '".$i_product_motif[$xx]."' ");

					if ($qbrgjadi->num_rows() > 0) {
						
						$rbrgjadi		= $qbrgjadi->row();
						$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
						$kode_brg_jadi	= $i_product_motif[$xx];
					}else {
						$qbrgjadi	= $db2->query(" SELECT e_product_motifname FROM tr_product_motif
											WHERE i_product_motif = '".$i_product[$xx]."00' ");
						if($qbrgjadi->num_rows() > 0 ) {
							$rbrgjadi	= $qbrgjadi->row();
							$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
							$kode_brg_jadi = $i_product[$xx]."00";
						}
						else {
							$nama_brg_jadi = "";
							$kode_brg_jadi = "";
						}
					}


				$seq_tm_op_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_op_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
					if($seq_tm_op_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_op_item->row();
						$i_op_item = $seqrow->i_op_item+1;
					}else{
						$i_op_item	= 1;
					}

					$seq_tm_sop_item	= $db2->query(" SELECT i_sop_item FROM tm_sop_item ORDER BY i_sop_item DESC LIMIT 1 ");
						if($seq_tm_sop_item->num_rows() > 0 ) {
							$seqrow2	= $seq_tm_sop_item->row();
							$i_sop_item	= $seqrow2->i_sop_item+1;
						}else{
							$i_sop_item	= 1;
						}

				$tm_op_item = array(
							'i_op_item'				=> $i_op_item,
							'i_op'					=> $iop,
							'i_product'				=> $kode_brg_jadi,
							'n_count'				=> $qty[$xx],
							'e_product_name'		=> $nama_brg_jadi,
							'f_delivered'			=> 'f',
							'n_residual'			=> $qty[$xx],
							'e_note'				=> '',
							'd_entry'				=> $dentry
							);

				$tm_sop_item = array(
							'i_sop_item'			=> $i_sop_item,
							'i_sop'					=> $isop,
							'i_product'				=> $kode_brg_jadi,
							'e_product_name'		=> $nama_brg_jadi,
							'd_entry'				=> $dentry
							);

				$db2->insert('tm_op_item',$tm_op_item);
				$db2->insert('tm_sop_item',$tm_sop_item);
							
			}else{
				//save tbl item saja
				// ambil id dari tabel tm_op dna tm_sop

				// tm_op
				$seq_tm_op	= $db2->query(" SELECT cast(i_op AS integer) AS i_op FROM tm_op 
								ORDER BY cast(i_op AS integer) DESC LIMIT 1 ");
				if($seq_tm_op->num_rows() > 0 ) {
						$seqrow	= $seq_tm_op->row();
						$iop	= $seqrow->i_op;
					}else{
						$iop	= 1;
					}


				// tm_sop
				$seq_tm_sop	= $db2->query(" SELECT i_sop FROM tm_sop ORDER BY i_sop DESC LIMIT 1 ");
					if($seq_tm_sop->num_rows() > 0 ) {
						$seqrow2= $seq_tm_sop->row();
						$isop	= $seqrow2->i_sop;
					}else{
						$isop	= 1;
					}

					$qbrgjadi = $db2->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '".$i_product_motif[$xx]."' ");

				if ($qbrgjadi->num_rows() > 0) {
						
						$rbrgjadi		= $qbrgjadi->row();
						$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
						$kode_brg_jadi	= $i_product_motif[$xx];
				}else {
						$qbrgjadi	= $db2->query(" SELECT e_product_motifname FROM tr_product_motif
											WHERE i_product_motif = '".$i_product[$xx]."00' ");
					if($qbrgjadi->num_rows() > 0 ) {
							$rbrgjadi	= $qbrgjadi->row();
							$nama_brg_jadi	= $rbrgjadi->e_product_motifname;
							$kode_brg_jadi = $i_product[$xx]."00";
						}
					else {
							$nama_brg_jadi = "";
							$kode_brg_jadi = "";
					}
				}
				
				//tm_op_item
				$seq_tm_op_item	= $db2->query(" SELECT cast(i_op_item AS integer) AS i_op_item FROM tm_op_item ORDER BY cast(i_op_item AS integer) DESC LIMIT 1 ");
					if($seq_tm_op_item->num_rows() > 0 ) {
						$seqrow	= $seq_tm_op_item->row();
						$i_op_item = $seqrow->i_op_item+1;
					}else{
						$i_op_item	= 1;
					}

				//tm_sop_item
				$seq_tm_sop_item	= $db2->query(" SELECT i_sop_item FROM tm_sop_item ORDER BY i_sop_item DESC LIMIT 1 ");
					if($seq_tm_sop_item->num_rows() > 0 ) {
						$seqrow2	= $seq_tm_sop_item->row();
						$i_sop_item	= $seqrow2->i_sop_item+1;
					}else{
						$i_sop_item	= 1;
					}
				
				$tm_op_item	= array(
							'i_op_item'				=> $i_op_item,
							'i_op'					=> $iop,
							'i_product'				=> $kode_brg_jadi,
							'n_count'				=> $qty[$xx],
							'e_product_name'		=> $nama_brg_jadi,
							'f_delivered'			=> 'f',
							'n_residual'			=> $qty[$xx],
							'e_note'				=> '',
							'd_entry'				=> $dentry
									);

				$tm_sop_item	= array(
							'i_sop_item'			=> $i_sop_item,
							'i_sop'					=> $isop,
							'i_product'				=> $kode_brg_jadi,
							'e_product_name'		=> $nama_brg_jadi,
							'd_entry'				=> $dentry
							);
					
				$db2->insert('tm_op_item',$tm_op_item);
				$db2->insert('tm_sop_item',$tm_sop_item);	
			}


		}//IF FCK
				
	}// TUtup FOR 

		if($db2->trans_status()===FALSE || $db2->trans_status()==false) {
			$db2->trans_rollback();
			$ok = 0;
		}else{
			$db2->trans_commit();
			$ok = 1;
				
			// for($xx=1; $xx<=$iterasi-1; $xx++) {

			// 	if ($fck[$xx] == 'y') {

			// 		$db3	= $this->load->database('db_sab',TRUE);
							
			// 		$data = array(

			// 				'f_po_spb'		=> 't'

			// 					);
			// 		$db3->where	('i_po_no',$no_op[$xx]);			
			// 		$db3->update('tm_po',$data);
					
			// 	}

			// }
			
			echo $ok;
		}

		if($ok==1) {
			/*** redirect('op/cform/'); ***/

			print "<script>alert(\"Transfer OP CV KAB berhasil disimpan. Terimakasih.\");window.open(\"opcvkab\", \"_self\");</script>";
			}
			else{
			/*** redirect('op/cform/'); ***/
			print "<script>alert(\"Maaf, transfer Order Pembelian (OP) CV KAB gagal disimpan. Terimakasih.\");window.open(\"opcvkab\", \"_self\");</script>";
			}

	} //Function
			
}
?>
