<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$iarea,$ikb) 
    {
		$this->db->query("update tm_kb set f_kb_cancel='t' WHERE i_kb='$ikb' and i_periode='$iperiode' and i_area='$iarea' and f_posting='f' and f_close='f'");
#####UnPosting      
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$ikb' and i_area='$iarea'");

      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ikb' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }

      $this->db->query("delete from tm_jurnal_transharian where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference='$ikb' and i_area='$iarea'");
      $quer 	= $this->db->query("SELECT i_pv, i_area, v_pv, i_pv_type from tm_pv_item
                                  where i_kk='$ikb' and i_area='$iarea' and i_pv_type='01'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_pv set v_pv=v_pv-$xx->v_pv
                            where i_pv='$xx->i_pv' and i_area='$xx->i_area' and i_pv_type='$xx->i_pv_type'");
          $this->db->query("delete from tm_pv_item where i_kk='$ikb' and i_pv='$xx->i_pv' and i_area='$xx->i_area' and i_pv_type='$xx->i_pv_type'");
        }
      }
      $quer 	= $this->db->query("SELECT i_rv, i_area, v_rv, i_rv_type from tm_rv_item
                                  where i_kk='$ikb' and i_area='$iarea' and i_rv_type='01'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_rv set v_rv=v_rv-$xx->v_rv
                            where i_rv='$xx->i_rv' and i_area='$xx->i_area' and i_rv_type='$xx->i_rv_type'");
          $this->db->query("delete from tm_rv_item where i_kk='$ikb' and i_rv='$xx->i_rv' and i_area='$xx->i_area' and i_rv_type='$xx->i_rv_type'");
        }
      }
#####
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	* from (
    		                    select c.i_rv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek,
							              b.e_area_name from tm_kb a, tr_area b, tm_rv_item c
							              where a.i_kb=c.i_kk and c.i_rv_type='01'
							              and (upper(a.i_kb) like '%$cari%' or upper(c.i_rv) like '%$cari%') 
							              and a.i_area=b.i_area and a.f_kb_cancel='f' and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              union all
							              select c.i_pv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek,
							              b.e_area_name from tm_kb a, tr_area b, tm_pv_item c
							              where a.i_kb=c.i_kk and c.i_pv_type='01'
							              and (upper(a.i_kb) like '%$cari%' or upper(c.i_pv) like '%$cari%') and a.i_area=b.i_area and a.f_kb_cancel='f' and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              ) as a ORDER BY a.i_kb ",false)->limit($num,$offset);
      }else{
		    $this->db->select("	* from (
    		                    select c.i_rv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek,
							              b.e_area_name from tm_kb a, tr_area b, tm_rv_item c
							              where a.i_kb=c.i_kk and c.i_rv_type='01'
							              and (upper(a.i_kb) like '%$cari%' or upper(c.i_rv) like '%$cari%')
							              and a.i_area=b.i_area and a.f_kb_cancel='f'
							              and a.i_area='$iarea' and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							              a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              union all
							              select c.i_pv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek,
							              b.e_area_name from tm_kb a, tr_area b, tm_pv_item c
							              where a.i_kb=c.i_kk and c.i_pv_type='01'
							              and (upper(a.i_kb) like '%$cari%' or upper(c.i_pv) like '%$cari%')
							              and a.i_area=b.i_area and a.f_kb_cancel='f'
							              and a.i_area='$iarea' and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							              a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              ) as a ORDER BY a.i_kb ",false)->limit($num,$offset);
							              #and a.i_area=c.i_area
      }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							    a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							    where (upper(a.i_kb) like '%$cari%')
							    and a.i_area=b.i_area and a.f_kb_cancel='f' and
							    a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_kb <= to_date('$dto','dd-mm-yyyy')
							    ORDER BY a.i_kb ",false)->limit($num,$offset);
		  }else{
		    $this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							    a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							    where (upper(a.i_kb) like '%$cari%')
							    and a.i_area=b.i_area and a.f_kb_cancel='f'
							    and a.i_area='$iarea' and
							    a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_kb <= to_date('$dto','dd-mm-yyyy')
							    ORDER BY a.i_kb ",false)->limit($num,$offset);
		  }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
