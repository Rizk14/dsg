<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function lbarangjadiperpages($limit,$offset) {
		/* Disabled 07-02-2011
		$query = $db2->query( "
				SELECT i_product_base AS iproduct,	
				       e_product_basename AS product
				FROM tr_product_base 
				
				WHERE f_stop_produksi=false
				
				ORDER BY i_product_base DESC LIMIT ".$limit." OFFSET ".$offset);
		*/
		 $db2 = $this->load->database('db_external', TRUE);
		$query = $db2->query( "
				SELECT i_product_base AS iproduct,	
				       e_product_basename AS product
				FROM tr_product_base 
				
				ORDER BY i_product_base DESC LIMIT ".$limit." OFFSET ".$offset);
								
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi() {
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT i_product_base AS iproduct,	
				       e_product_basename AS product
				FROM tr_product_base 
				
				WHERE f_stop_produksi=false
				
				ORDER BY i_product_base DESC ");
		*/		
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query( "
				SELECT i_product_base AS iproduct,	
				       e_product_basename AS product
				FROM tr_product_base 
				
				ORDER BY i_product_base DESC ");		
	}
	
	function flbarangjadi($key) {
		 $db2 = $this->load->database('db_external', TRUE);
		/* Disabled 07-02-2011
		return $db2->query( "
				SELECT i_product_base AS iproduct,	
				       e_product_basename AS product
				FROM tr_product_base 
				
				WHERE f_stop_produksi=false AND (i_product_base='$key' OR e_product_basename='$key')
				
				ORDER BY i_product_base DESC ");
		*/
		$key_upper	= strtoupper($key);		
		
		return $db2->query( "
				SELECT i_product_base AS iproduct,	
				       e_product_basename AS product
				FROM tr_product_base 
				
				WHERE (i_product_base='$key_upper' OR e_product_basename='$key_upper')
				
				ORDER BY i_product_base DESC ");		
	}
	
	function clistbrgjadi($iproduct){
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT  a.i_product_base AS iproduct,
				a.e_product_basename AS product,
				b.e_category_name AS kategori,
				c.e_brand_name AS merek,
				d.e_layout_name AS tampilan,
				a.e_quality AS kualitas,
				a.f_stop_produksi AS stop,
				a.e_surat_penawaran AS penawaran,
				a.d_surat_penawaran AS tpenawaran,
				a.v_unitprice AS hjp,
				a.n_status AS status
				
			FROM tr_product_base a 
			
			LEFT JOIN tr_categories b ON a.i_category=b.i_category
			LEFT JOIN tr_brand c ON a.i_brand=c.i_brand
			LEFT JOIN tr_layout d ON a.i_layout=d.i_layout WHERE a.i_product_base='$iproduct' " );
	}
	
	function cdetailbrgjadi($iproduct){
		 $db2 = $this->load->database('db_external', TRUE);
		$query	= $db2->query( "
				SELECT b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       b.n_quantity AS qty
				FROM tr_product_base a RIGHT JOIN tr_product_motif b 
				
				ON a.i_product_base=b.i_product WHERE b.i_product='$iproduct' " );
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	
	function statusproduct($istatus) {
		 $db2 = $this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_status_product WHERE i_status_product='$istatus' ");
	}	
}
?>
