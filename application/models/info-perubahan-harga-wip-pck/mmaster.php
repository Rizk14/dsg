<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
function get_unit_packing (){
	 $this->db->select(" * from tm_unit_packing order by kode_unit");
	$query=$this->db->get();
	if($query->num_rows() > 0){
		return $query->result();
		}
	}
	
	function get_nama_unit_packing ($id_unit_packing){
	 $this->db->select(" nama,kode_unit from tm_unit_packing where id='$id_unit_packing' order by kode_unit");
	$query=$this->db->get();
	if($query->num_rows() > 0){
		return $query->row();
		}
	}

  function cek_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing){
     $this->db->select("* from tm_perb_harga_hasil_packing WHERE SUBSTRING(cast(tgl_input as  varchar), 1,7 ) >= '".$tahun_dari."-".$bulan_dari."' AND SUBSTRING(cast(tgl_input as  varchar), 1,7 ) <= '".$tahun_ke."-".$bulan_ke."' AND id_unit_packing='$id_unit_packing'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  
	  function get_rekap_harga($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing){
   
    $this->db->select("* from tm_perb_harga_hasil_packing WHERE SUBSTRING(cast(tgl_input as  varchar), 1,7 ) >= '".$tahun_dari."-".$bulan_dari."' AND SUBSTRING(cast(tgl_input as  varchar), 1,7 ) <= '".$tahun_ke."-".$bulan_ke."' AND id_unit_packing='$id_unit_packing'
    order by id_brg_wip asc,tgl_input asc", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		$hasil= $query->result();
		foreach ($hasil as $row1) {
				$query3	= $this->db->query(" SELECT * FROM tm_barang_wip 
						WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$kode_brg	= $hasilrow->kode_brg;
					
				}
				else {
					$nama_brg = '';
					
					$kode_brg='';
				}
				
				$satuan= 'pieces';
				
				$query3	= $this->db->query(" SELECT nama,kode_unit FROM tm_unit_packing WHERE id = '$row1->id_unit_packing' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_unit_packing	= $hasilrow->kode_unit;
					$nama_unit_packing	= $hasilrow->nama;
				}
				else
					$nama_unit_packing = '';
					
					
				
								
				$data_harga[] = array(		'id'=> $row1->id,	
											'kode_brg'=> $kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'kode_unit_packing'=> $kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,						
											'harga'=> $row1->harga,
											'tgl_input'=>$row1->tgl_input,
																	
											);
		} // endforeach header
	}
	else {
		$data_harga = '';
	}
	return $data_harga;
  }
  
  function get_rekap_hargatanpalimit($bulan_dari,$bulan_ke, $tahun_dari,$tahun_ke,$id_unit_packing){
	$query	=  $this->db->query("SELECT * from tm_perb_harga_hasil_packing WHERE SUBSTRING(cast(tgl_input as  varchar), 1,7 ) >= '".$tahun_dari."-".$bulan_dari."' AND SUBSTRING(cast(tgl_input as  varchar), 1,7 ) <= '".$tahun_ke."-".$bulan_ke."' AND id_unit_packing='$id_unit_packing'", false);
    
    return $query->result();  
  }
  

}
