<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	public function getcustomer(){
		$db2=$this->load->database('db_external', TRUE);
		
		$order	= " ORDER BY e_branch_name ASC, i_branch_code DESC ";
		$db2->select(" * FROM tr_branch ".$order." ",false);
				    
		$query	= $db2->get();
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}else{
			return false;
		}

	}
	
	// 15-06-2012
	function getkontrabon() {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				FROM tm_dt a
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt 
				WHERE a.f_dt_cancel='f' 
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				ORDER BY a.i_dt_code DESC, a.d_dt DESC ");
	}
	
	function getkontrabonperpages($limit,$offset) {
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				FROM tm_dt a
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				WHERE a.f_dt_cancel='f' 
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				ORDER BY a.i_dt_code DESC, a.d_dt DESC LIMIT ".$limit." OFFSET ".$offset." ");
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function fgetkontrabon($key) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				FROM tm_dt a
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				WHERE a.f_dt_cancel='f' AND (a.i_dt_code LIKE '$key%')
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				ORDER BY a.i_dt_code DESC, a.d_dt DESC ");
	}
	
	// 16-06-2012
	function getkontrabonbynomor($ikontrabon) {
			$db2=$this->load->database('db_external', TRUE);
		// query ini nyontek dari function ldtitem2($idt,$fnotasederhana) di modul listkontrabon
		$sql = " SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, c.e_branch_city, b.d_faktur, b.d_due_date, c.i_customer, c.i_code, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn,b.v_materai_sisa as v_materai
					FROM tm_faktur_bhnbaku b
					
					INNER JOIN tm_faktur_bhnbaku_item a ON b.i_faktur=a.i_faktur
					INNER JOIN tm_dt_item d ON d.i_nota=b.i_faktur
					INNER JOIN tm_dt e ON e.i_dt=d.i_dt
					INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='t' AND e.i_dt='$ikontrabon' AND e.f_dt_cancel='f'
					GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, c.e_branch_city, b.d_faktur, b.d_due_date, c.i_customer, c.i_code, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
					ORDER BY b.i_faktur_code ASC "; 
		//$query = $db2->query($sql);
		return $db2->query($sql);						
		//return $result = $query->result();
	}
	
	function getkontrabonbynomorperpages($limit, $offset, $ikontrabon) {
			$db2=$this->load->database('db_external', TRUE);
		// query ini nyontek dari function ldtitem2($idt,$fnotasederhana) di modul listkontrabon
		$sql = " SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, c.e_branch_city, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn, b.v_materai_sisa as v_materai
					FROM tm_faktur_bhnbaku b
					
					INNER JOIN tm_faktur_bhnbaku_item a ON b.i_faktur=a.i_faktur
					INNER JOIN tm_dt_item d ON d.i_nota=b.i_faktur
					INNER JOIN tm_dt e ON e.i_dt=d.i_dt
					INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='t' AND e.i_dt='$ikontrabon' AND e.f_dt_cancel='f'
					GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, c.e_branch_city, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
					ORDER BY b.i_faktur_code ASC LIMIT ".$limit." OFFSET ".$offset." ";
		//return $db2->query($sql);
		//return $result = $sql->result();
		$query = $db2->query($sql);					
		return $result = $query->result();
	}
	// end 16-06-2012
	
	// 18-06-2012
	function totalfaktur($ifaktur) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(a.n_quantity*a.v_unit_price) AS totalfaktur, b.v_materai_sisa  
		FROM tm_faktur_bhnbaku_item a  
		INNER JOIN tm_faktur_bhnbaku b on a.i_faktur = b.i_faktur 
		WHERE a.i_faktur='$ifaktur'
		group by b.v_materai_sisa");
	} //

	function getmaterai($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
	return $db2->query(" SELECT v_materai_sisa AS totalfaktur FROM tm_faktur_do_t WHERE i_faktur='$ifaktur' ");
	} //

	function clistfpenjbrgjadiperdo($nofaktur,$ddo_first,$ddo_last) {
			$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
		
		if( strlen($nfaktur)!=0 || strlen($ddo)!=0 ) {
			/* Disabled 09082011
			$query	= $db2->query( "
			
				SELECT 	e.d_do AS ddo,
						e.i_do_code AS idocode,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname,
						a.n_quantity  AS qty,
						a.v_unit_price AS unitprice,
						sum((a.n_quantity * a.v_unit_price)) AS amount
						
				FROM tm_faktur_do_item a
					
				RIGHT JOIN tm_faktur_do b ON cast(b.i_faktur_code AS integer)=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo."
				
				GROUP BY e.i_do_code, c.i_product_motif, c.e_product_motifname, a.n_quantity, a.v_unit_price, e.d_do
				
				" );
				*/
			/*
			a.v_unit_price AS unitprice,
			*/	
			/*
			(a.v_unit_price / a.n_quantity) AS unitprice, 
			sum(a.v_unit_price) AS amount 
			*/
			
			//09-06-2012
			$sqlnya = "SELECT  b.i_faktur_code, c.i_do_code AS idocode, c.d_do AS ddo, a.i_product AS imotif, 
					a.e_product_name AS motifname, 
					a.n_quantity AS qty, 
					a.v_unit_price AS unitprice,
					sum((a.n_quantity * a.v_unit_price)) AS amount
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				 
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur_code, c.i_do_code, c.d_do, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ORDER BY a.i_product ASC";
			//echo $sqlnya;
			
			$query	= $db2->query("
				SELECT  b.i_faktur_code, c.i_do_code AS idocode, c.d_do AS ddo, a.i_product AS imotif, 
					a.e_product_name AS motifname, 
					a.n_quantity AS qty, 
					a.v_unit_price AS unitprice,
					sum((a.n_quantity * a.v_unit_price)) AS amount
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do
				 
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur_code, c.i_do_code, c.d_do, a.i_product, a.e_product_name, a.n_quantity, a.v_unit_price ORDER BY a.i_product ASC	");
								
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}
		}
	}

	function clistfpenjbrgjadiperdo2($nofaktur,$ddo_first,$ddo_last) {
			$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (c.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";		
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
		
		if( strlen($nfaktur)!=0 || strlen($ddo)!=0 ) {
			/*
			$query	= $db2->query("
			
				SELECT 	b.i_faktur_code,
						c.i_product_motif AS imotif,
						c.e_product_motifname AS motifname
					
				FROM tm_faktur_do_item a
					
				RIGHT JOIN tm_faktur_do b ON cast(b.i_faktur_code AS integer)=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do ".$nfaktur." ".$ddo."
				
				GROUP BY b.i_faktur_code, c.i_product_motif, c.e_product_motifname ");
			*/
			$sqlnya = "SELECT 	b.i_faktur, b.i_faktur_code, 

						a.i_product AS imotif,
						a.e_product_name AS motifname
					
				FROM tm_faktur_do_item_t a
					
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do ".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur, b.i_faktur_code, a.i_product, a.e_product_name ORDER BY a.i_product ASC";
				//echo $sqlnya; die();
			$query	= $db2->query("
				SELECT 	b.i_faktur, b.i_faktur_code, 

						a.i_product AS imotif,
						a.e_product_name AS motifname
					
				FROM tm_faktur_do_item_t a
					
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do ".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur, b.i_faktur_code, a.i_product, a.e_product_name ORDER BY a.i_product ASC ");

			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}
		}
	}
	
	function clistfpenjperdo($nofaktur,$ddo_first,$ddo_last) {
	$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code=trim('$nofaktur') ";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code=trim('$nofaktur') ";
			$ddo		= "";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
		
		return $db2->query( "
		
				SELECT 	count(cast(b.i_faktur_code AS integer)) AS jmlfaktur,
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.d_due_date AS ddue,
						b.e_branch_name AS branchname
					
				FROM tm_faktur_do_item_t a
					
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				INNER JOIN tm_do e ON e.i_do=a.i_do 
				
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY b.i_faktur_code, b.d_faktur, b.d_due_date, b.e_branch_name " );
			
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	
	function clistfpenjperdo_detail_opdo($nofaktur,$ddo_first,$ddo_last) {
			$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='".trim($nofaktur)."'";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";
			$fbatal		= " AND b.f_faktur_cancel='f' ";
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
			
		/*
		$strqry	= "
			SELECT 	count(cast(e.i_do_code AS integer)) AS jmldo,
					count(cast(f.i_op AS integer)) AS jmlop,
					e.i_do_code AS ido,
					f.i_op AS iopcode
			
			FROM tm_faktur_do_item a
				
			RIGHT JOIN tm_faktur_do b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			INNER JOIN tm_do e ON e.i_do=a.i_do 
			
			".$nfaktur." ".$ddo."
			
			GROUP BY e.i_do_code, f.i_op ";
		*/
		
		/* 26072011
		$strqry	= "
			SELECT  e.i_do_code AS idocode, 
				f.i_op AS iopcode 
			
			FROM tm_faktur_do_item_t a 
			
			RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do 
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product 
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product 
			INNER JOIN tm_do e ON e.i_do=a.i_do 
			
			".$nfaktur." ".$ddo."
			
			GROUP BY e.i_do_code, f.i_op
		";
		*/
		
		$strqry	= " SELECT  e.i_do_code AS idocode,
				g.i_op_code AS iopcode
				
				FROM tm_faktur_do_item_t a
				
				RIGHT JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do_item f ON f.i_do=a.i_do
				INNER JOIN tm_do e ON e.i_do=a.i_do
				INNER JOIN tm_op g ON g.i_op=f.i_op
				
				".$nfaktur." ".$ddo." ".$fbatal."
				
				GROUP BY e.i_do_code, g.i_op_code ";	//echo $strqry; die();	
		
		$query	= $db2->query($strqry);
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	
	function clistfpenjperdo_jml($nofaktur,$ddo_first,$ddo_last) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";			
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";					
		} else {
			$nfaktur	= "";
			$ddo		= "";
			
		}
		
		/*
		sum((a.n_quantity * a.v_unit_price)) AS total
		*/
		/*
		a.v_unit_price AS total	
		*/

		/*
		return $db2->query( "		
			SELECT 	b.n_discount AS n_disc,
					b.v_discount AS v_disc,
					(a.n_quantity * a.v_unit_price) AS total
			
			FROM tm_faktur_do_item_t a
			
			RIGHT JOIN tm_faktur_do_t b ON cast(b.i_faktur_code AS integer)=a.i_faktur
			INNER JOIN tm_do_item f ON f.i_do=a.i_do
			INNER JOIN tr_product_motif c ON c.i_product_motif=f.i_product
			INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
			INNER JOIN tm_do e ON e.i_do=a.i_do
				
			".$nfaktur." ".$ddo."
			
			GROUP BY b.n_discount, b.v_discount, a.v_unit_price " );
		*/
		
		return $db2->query(" SELECT n_discount AS n_disc, v_discount AS v_disc, v_total_faktur AS total FROM tm_faktur_do_t WHERE i_faktur_code='$nofaktur' AND f_faktur_cancel='f' ");
	}

	function clistfpenjperdo_totalnyaini($nofaktur,$ddo_first,$ddo_last) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($nofaktur) && (!empty($ddo_first) && !empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= " AND (e.d_do BETWEEN '$ddo_first' AND '$ddo_last') ";
			$fbatal		= " AND b.f_faktur_cancel='f' ";			
		} else if(!empty($nofaktur) && (empty($ddo_first) || empty($ddo_last)) ) {
			$nfaktur	= " WHERE b.i_faktur_code='$nofaktur' ";
			$ddo		= "";
			$fbatal		= " AND b.f_faktur_cancel='f' ";			
		} else {
			$nfaktur	= "";
			$ddo		= "";
			$fbatal		= " WHERE b.f_faktur_cancel='f' ";
		}
			
		return $db2->query(" SELECT sum(a.n_quantity * a.v_unit_price) totalnyaini
				FROM tm_faktur_do_item_t a INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur 
				INNER JOIN tm_do c ON c.i_do=a.i_do WHERE b.i_faktur_code='$nofaktur' AND b.f_faktur_cancel='f' ");
	}
	
	function getcabang($nofaktur){
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
			SELECT c.e_branch_name AS cabangname, c.e_branch_address AS address
							
			FROM tm_faktur_do_t a
				
			INNER JOIN tm_faktur_do_item_t b ON a.i_faktur=b.i_faktur
			INNER JOIN tr_branch c ON c.e_initial=a.e_branch_name
			INNER JOIN tr_customer d ON d.i_customer=c.i_customer
			
			WHERE a.i_faktur_code='$nofaktur' AND a.f_faktur_cancel='f'
			
			GROUP BY c.e_branch_name, c.e_branch_address
		" );
	}
	
	function getinitial() {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_initial_company WHERE i_initial_status='1' ORDER BY i_initial_code DESC LIMIT 1");
	}
	

	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
				SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
					b.i_faktur_code AS ifakturcode,
					b.d_faktur AS dfaktur
				
				FROM tm_faktur_do_item_t a 
				
				INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
				INNER JOIN tm_do c ON c.i_do=a.i_do 
				INNER JOIN tm_do_item d ON d.i_do=c.i_do 
				INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
				INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
				
				WHERE b.i_faktur_code='$key' AND b.f_faktur_cancel='f'
				
				GROUP BY b.d_faktur, b.i_faktur_code
				
				ORDER BY b.d_faktur DESC, b.i_faktur_code DESC " );
	}

	/*
	function remote($destination_ip) {
		return $db2->query(" SELECT * FROM tr_printer WHERE ip='$destination_ip' ORDER BY i_printer DESC LIMIT 1 ");
	}	
	*/

	function remote($id) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_printer WHERE i_user_id='$id' ORDER BY i_printer DESC LIMIT 1 ");
	}

	/*
					sum(v_unit_price) AS jmlunitprice,
					sum(n_quantity * v_unit_price) AS amount	
	*/
	/*
					(v_unit_price / n_quantity) AS unitprice,
					sum(v_unit_price / n_quantity) AS jmlunitprice,
					sum(v_unit_price) AS amount
	*/
	
	function jmlitemharga($ifaktur,$iproduct) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT sum(n_quantity) AS qty, 
					v_unit_price AS unitprice,
					sum(v_unit_price) AS jmlunitprice,
					sum(v_unit_price *n_quantity) AS amount
				FROM tm_faktur_do_item_t 
				WHERE i_faktur='$ifaktur' AND i_product='$iproduct'				
				GROUP BY i_product, i_faktur, v_unit_price ");			
	}
}
?>
