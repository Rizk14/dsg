<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll(){
    $this->db->select('*');
    $this->db->from('tm_unit_quilting');
    $this->db->order_by('kode_unit','ASC');
    $query = $this->db->get();
    
    return $query->result();
  }
  
  function get($id){
    $query = $this->db->getwhere('tm_unit_quilting',array('kode_unit'=>$id));
    return $query->result();
  }
  
  function cek_data($kode){
    $this->db->select("kode_unit from tm_unit_quilting WHERE kode_unit = '$kode'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  //
  function save($kode,$kodeedit,$lokasi, $nama, $goedit){  
    $tgl = date("Y-m-d");
    $data = array(
      'kode_unit'=>$kode,
      'lokasi'=>$lokasi,
      'nama'=>$nama,
      'tgl_input'=>$tgl,
      'tgl_update'=>$tgl
    );

    if ($goedit == '') {
		$this->db->insert('tm_unit_quilting',$data); }
	else {
		$data = array(
		  'kode_unit'=>$kode,
		  'lokasi'=>$lokasi,
		  'nama'=>$nama,
		  'tgl_update'=>$tgl
		);
		
		$this->db->where('kode_unit',$kodeedit);
		$this->db->update('tm_unit_quilting',$data);  
	}
		
  }
  
  function delete($kode){    
    $this->db->delete('tm_unit_quilting', array('kode_unit' => $kode));
  }

}

