<?php
class Master_barang_wip_model extends MY_Model
{
		


	protected $_per_page = 5;
    protected $_tabel = 'tb_master_barang_wip';
    protected $form_rules = array(
        array(
            'field' => 'nama_barang_wip',
            'label' => 'Nama Barang',
            'rules' => 'trim|required|max_length[64]'
        ),
        array(
            'field' => 'kode_barang_wip',
            'label' => 'Kode Barang',
            'rules' => 'trim|required|max_length[10]'
        ),
        
    );

     public $default_values = array(
		'nama_barang_wip' => '',
        'kode_barang_wip' => '',
        'id'	=>	'',
		'num'		=>1,
    );

     public function __construct()
	{
			parent::__construct();
			 $this->db4 = $this->load->database('db_additional',TRUE);
	}
        public function input($kode_barang_wip,$nama_barang_wip)
    {
		 $created_at = $updated_at = date('Y-m-d H:i:s');
		 
       $db4=$this->load->database('db_additional',TRUE);
        $query=$this->db4->query(" select id from tm_barang_wip order by id DESC limit 1");
        if($query->num_rows() > 0){
			$hasilrow=$query->row();
			$id_tm_barang=$hasilrow->id;
			$idbaru = $id_tm_barang+1;
			}
        $data= array(
        'id' => $idbaru,
		'kode_brg' => $kode_barang_wip,
		'nama_brg' => $nama_barang_wip,
		'tgl_input'=> $created_at,
		'tgl_update'=> $updated_at
		);
		
        $id= $this->db4->insert('tm_barang_wip',$data);
	
	
		$data= array(
		'id' => $idbaru,
		'kode_barang_wip' => $kode_barang_wip,
		'nama_barang_wip' => $nama_barang_wip,
		'created_at'=> $created_at,
		'updated_at'=> $updated_at
		);
		
        $id= $this->_db4->insert('tb_master_barang_wip',$data);
        if ($id) {
		 return true;
		}
		 return false;

    }
     public function cari($offset)
    {
		$db4=$this->load->database('db_additional',TRUE);
        $this->get_real_offset($offset);
        $kata_kunci = $this->input->get('kata_kunci', true);
       
        return $this->_db4->where("( kode_barang_wip LIKE '%$kata_kunci%' OR nama_barang_wip LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->result();
    }
    
     public function delete_aktif($id)
    {
		$db4=$this->load->database('db_additional',TRUE);
		$this->db4->where('id', $id);
		$this->db4->delete('tm_barang_wip');
        $db4->where('id', $id);
		$db4->delete('tb_master_barang_wip');
		return TRUE;
    }
    
    public function get_id($id)
    {
		$db4=$this->load->database('db_additional',TRUE);
        $db4->where('id', $id);
		$db4->get('tb_master_barang_wip');
		return TRUE;
    }
     public function cari_num_rows()
    {
		$db4=$this->load->database('db_additional',TRUE);
        $kata_kunci = $this->input->get('kata_kunci', true);
          return $db4->where("( kode_barang_wip LIKE '%$kata_kunci%' OR nama_barang_wip LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->num_rows();
    }
    
    public function edit($id, $nama_barang_wip,$kode_barang_wip)
    {
		$db4=$this->load->database('db_additional',TRUE);
        $updated_at = date('Y-m-d H:i:s');
     $this->db4->where('id', $id);
     
     $data = array(
        'nama_barang_wip' => $nama_barang_wip,
       'kode_barang_wip' => $kode_barang_wip,
       'updated_at' => $updated_at,
);
    $this->db4->update('tb_master_barang_wip', $data);
     $db4->where('id', $id);
    $data= array(
		'kode_brg' => $kode_barang_wip,
		'nama_brg' => $nama_barang_wip,
		'tgl_update'=> $updated_at
		);
    $this->db4->update('tm_barang_wip', $data);
    return true;
    }
    
}
