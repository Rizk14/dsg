<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}
		
	/*
	function lforcastperpages($limit,$offset){
		$query = $db2->query(" SELECT a.i_forcast_code, a.d_forcast FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast
				WHERE a.f_forcast_cancel='f' GROUP BY a.i_forcast_code,a.d_forcast LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lforcast(){
		return $db2->query(" SELECT a.i_forcast_code, a.d_forcast FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast
				WHERE a.f_forcast_cancel='f' GROUP BY a.i_forcast_code,a.d_forcast ");
	}	
	
	function flforcast($key) {
		if(!empty($key)) {
			return $db2->query(" SELECT a.i_forcast_code, a.d_forcast FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast
				WHERE a.f_forcast_cancel='f' AND a.i_forcast_code LIKE '$key%' GROUP BY a.i_forcast_code,a.d_forcast ");
		}
	}

	function ldropforcastperpages($limit,$offset){
		$query = $db2->query(" SELECT a.i_drop_forcast FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast
				WHERE a.f_forcast_cancel='f' GROUP BY a.i_drop_forcast LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function ldropforcast(){
		return $db2->query(" SELECT a.i_drop_forcast FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast
				WHERE a.f_forcast_cancel='f' GROUP BY a.i_drop_forcast ");
	}
	
	function fldropforcast($key) {
		if(!empty($key)) {
			return $db2->query(" SELECT a.i_drop_forcast FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast
				WHERE a.f_forcast_cancel='f' AND a.i_drop_forcast LIKE '$key%' GROUP BY a.i_drop_forcast ");
		}
	}
	*/
	
	/*
	function clistforcast($iforcast,$idropforcast,$n_d_forcast_first,$n_d_forcast_last){
		if(($iforcast!='kosong' && $idropforcast!='kosong') && ($n_d_forcast_first!="0" && $n_d_forcast_last!="0")) {
			$ddate	= " WHERE (a.d_forcast BETWEEN '$n_d_forcast_first' AND '$n_d_forcast_last') ";
			$iforcst= " AND a.i_forcast_code='$iforcast' ";
			$idropforcst= " AND a.i_drop_forcast='$idropforcast' ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast=='kosong' && $idropforcast!='kosong') && ($n_d_forcast_first!="0" && $n_d_forcast_last!="0")) {
			$ddate	= " WHERE (a.d_forcast BETWEEN '$n_d_forcast_first' AND '$n_d_forcast_last') ";
			$iforcst= " ";
			$idropforcst= " AND a.i_drop_forcast='$idropforcast' ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast!='kosong' && $idropforcast=='kosong') && ($n_d_forcast_first!="0" && $n_d_forcast_last!="0")) {
			$ddate	= " WHERE (a.d_forcast BETWEEN '$n_d_forcast_first' AND '$n_d_forcast_last') ";
			$iforcst= " AND a.i_forcast_code='$iforcast' ";
			$idropforcst= " ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast=='kosong' || $idropforcast=='kosong') && ($n_d_forcast_first!="0" && $n_d_forcast_last!="0")) {
			$ddate	= " WHERE (a.d_forcast BETWEEN '$n_d_forcast_first' AND '$n_d_forcast_last' ) ";
			$iforcst= " ";
			$idropforcst= " ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast!='kosong' && $idropforcast=='kosong') && ($n_d_forcast_first!="0" || $n_d_forcast_last!="0")) {
			$ddate	= " ";
			$iforcst= " WHERE a.i_forcast_code='$iforcast' ";
			$idropforcst= " ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast=='kosong' && $idropforcast!='kosong') && ($n_d_forcast_first!="0" || $n_d_forcast_last!="0")) {
			$ddate	= " ";
			$iforcst= " ";
			$idropforcst= " WHERE a.i_drop_forcast='$idropforcast' ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast=='kosong' && $idropforcast!='kosong') && ($n_d_forcast_first=="0" || $n_d_forcast_last=="0")) {
			$ddate	= " ";
			$iforcst= " ";
			$idropforcst= " WHERE a.i_drop_forcast='$idropforcast' ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast!='kosong' && $idropforcast=='kosong') && ($n_d_forcast_first=="0" || $n_d_forcast_last=="0")) {
			$ddate	= " ";
			$iforcst= " WHERE a.i_forcast_code='$iforcast' ";
			$idropforcst= " ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast!='kosong' && $idropforcast!='kosong') && ($n_d_forcast_first=="0" || $n_d_forcast_last=="0")) {
			$ddate	= " ";
			$iforcst= " WHERE a.i_forcast_code='$iforcast' ";
			$idropforcst= " AND a.i_drop_forcast='$idropforcast' ";
			$batal	= " AND a.f_forcast_cancel='f' ";
		} else if(($iforcast!='kosong' && $idropforcast!='kosong') && ($n_d_forcast_first!="0" || $n_d_forcast_last!="0")) {
			$ddate	= " ";
			$iforcst= " WHERE a.i_forcast_code='$iforcast' ";
			$idropforcst= " AND a.i_drop_forcast='$idropforcast' ";
			$batal	= " AND a.f_forcast_cancel='f' ";			
		}else{
			$ddate	= " ";
			$iforcst= " ";
			$idropforcst= " ";
			$batal	= " WHERE a.f_forcast_cancel='f' ";													
		}

		$query	= $db2->query(" SELECT a.i_forcast, a.i_forcast_code, a.i_drop_forcast, a.d_forcast, b.i_product,b.e_product_name, b.n_forcast, b.n_residual FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast ".$ddate." ".$iforcst." ".$idropforcst." ".$batal." ");
								
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	*/
	
	function clistforcast2($n_d_forcast_first,$n_d_forcast_last){
		$db2=$this->load->database('db_external', TRUE);
		if(($n_d_forcast_first!="0" && $n_d_forcast_last!="0")) {
			$ddate	= " WHERE (a.d_sj BETWEEN '$n_d_forcast_first' AND '$n_d_forcast_last') ";
			$batal	= " AND a.f_sj_cancel='f' AND c.f_sj='t' ";
		}else{
			$ddate	= " ";
			$batal	= " WHERE a.f_sj_cancel='f' AND c.f_sj='t' ";													
		}

		$query	= $db2->query(" SELECT a.i_sj,
				a.i_sj_code,
				a.d_sj,
				b.i_forcast,
				b.i_product,
				b.e_product_name,
				b.n_deliver AS sj,
				c.i_forcast_code,
				c.i_drop_forcast,
				c.d_forcast AS ddropforcast,
				c.d_entry AS dforcast FROM tm_sj_drpforcast a

				INNER JOIN tm_sj_drpforcastitem b ON a.i_sj=b.i_sj
				INNER JOIN tm_forcast c ON c.i_forcast=b.i_forcast

				".$ddate." ".$batal." ORDER BY a.i_sj_code ASC, b.i_product ASC ");
								
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	
	/*
	function clistforcast_excell1($iforcast,$idropforcast,$n_d_forcast_first,$n_d_forcast_last){

		$ddate	= " WHERE (a.d_forcast BETWEEN '$n_d_forcast_first' AND '$n_d_forcast_last') ";
		
		$query	= $db2->query(" SELECT b.i_product, b.e_product_name FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast ".$ddate." GROUP BY b.i_product, b.e_product_name ORDER BY b.i_product ASC  ");
								
		if($query->num_rows() > 0 ) {
			return $result	= $query->result();
		}
	}
	*/
	
	function clistforcast_excell($n_d_forcast_first,$n_d_forcast_last){
$db2=$this->load->database('db_external', TRUE);
		if(($n_d_forcast_first!="0" && $n_d_forcast_last!="0")) {
			$ddate	= " WHERE (a.d_sj BETWEEN '$n_d_forcast_first' AND '$n_d_forcast_last') ";
			$batal	= " AND a.f_sj_cancel='f' AND c.f_sj='t' ";
		}else{
			$ddate	= " ";
			$batal	= " WHERE a.f_sj_cancel='f' AND c.f_sj='t' ";													
		}

		return $db2->query(" SELECT a.i_sj,
				a.i_sj_code,
				a.d_sj,
				b.i_forcast,
				b.i_product,
				b.e_product_name,
				b.n_deliver AS sj,
				c.i_forcast_code,
				c.i_drop_forcast,
				c.d_forcast AS ddropforcast,
				c.d_entry AS dforcast FROM tm_sj_drpforcast a

				INNER JOIN tm_sj_drpforcastitem b ON a.i_sj=b.i_sj
				INNER JOIN tm_forcast c ON c.i_forcast=b.i_forcast

				".$ddate." ".$batal." ORDER BY a.i_sj_code ASC, b.i_product ASC ");
	}
	
	/*		
	function lsjdropforcast($iforcast,$iproduct,$tforcastfirst,$tforcastlast){
		if($tforcastfirst!=0 && $tforcastlast!=0){
			$ddate = "";
		}
		
		return $db2->query(" SELECT a.i_sj, a.i_sj_code, a.d_sj, b.n_deliver FROM tm_sj_drpforcast a 
			INNER JOIN tm_sj_drpforcastitem b ON b.i_sj=a.i_sj WHERE b.i_forcast='$iforcast' AND b.i_product='$iproduct' AND a.f_sj_cancel='f' ");
	}
	*/
	
	/*
	function totalforcast($iproduct,$tforcastfirst,$tforcastlast){
		return $db2->query(" SELECT sum(b.n_forcast) AS totalforcast, sum(b.n_residual) AS totalsisaforcast FROM tm_forcast a
				INNER JOIN tm_forcast_item b ON b.i_forcast=a.i_forcast WHERE (a.d_forcast BETWEEN '$tforcastfirst' AND '$tforcastlast' ) AND a.f_forcast_cancel='f' AND b.i_product='$iproduct' ");
	}
	*/ 	
}

?>
