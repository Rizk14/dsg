<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  function get_detail_sjkeluarwip($list_brg, $unit_jahitnya){
    $detail_pp = array();

    foreach($list_brg as $row1) {
		
			if ($row1 != '') {
				
					$query2	= $this->db->query(" SELECT a.no_sj, b.id_sjkeluarwip, b.id_brg_wip, b.qty, b.harga, b.diskon
											FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
											WHERE b.id = '$row1' ");
					$hasilrow = $query2->row();
					$id_sjkeluarwip	= $hasilrow->id_sjkeluarwip;
					$id_brg_wip	= $hasilrow->id_brg_wip;
					$qty	= $hasilrow->qty;
					$harga	= $hasilrow->harga;
					$diskon	= $hasilrow->diskon;
					$no_sjkeluarwip	= $hasilrow->no_sj;
									
				$query2	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$id_brg_wip' ");
				$hasilrow = $query2->row();
				$kode_brg_wip	= $hasilrow->kode_brg;
				$nama_brg_wip	= $hasilrow->nama_brg;
					
				$qty_sjkeluarwip = $qty;
				
					$query4	= $this->db->query(" SELECT id FROM tm_pembelianretur_wip_detail WHERE id_sjkeluarwip_detail = '".$row1."' ");
					if ($query4->num_rows() > 0){
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelianretur_wip_detail a INNER JOIN tm_pembelianretur_wip b ON a.id_pembelianretur_wip = b.id
										WHERE a.id_sjkeluarwip_detail = '$row1' AND b.status_aktif = 't' 
										AND a.id_brg_wip = '".$id_brg_wip."' ");
						
						if($query3->num_rows()>0) {
							$hasilrow = $query3->row();
							$jum_beli = $hasilrow->jum;
						}
						else
							$jum_beli = 0;
					}
					else {
						$jum_beli = 0;
					}
					$query5=$this->db->query("SELECT harga from tm_harga_brg_unit_jahit where id_brg='".$id_brg_wip."' 
					AND id_unit_jahit='".$unit_jahitnya."'");
					
					if ($query5->num_rows()>0){
						$hasilrow = $query5->row();
						$harga=$hasilrow->harga ;
						}
					else
					{
						$harga= 0;
						}
			
				$jum_sjkeluarwip = $jum_beli;
					
				$qty = $qty-$jum_sjkeluarwip;
			
				$qty = number_format($qty, 4, '.','');

					$detail_sjkeluarwip[] = array(	'id'=> $row1,
											'id_brg_wip'=> $id_brg_wip,
											'kode_brg_wip'=> $kode_brg_wip,
											'nama_brg_wip'=> $nama_brg_wip,
											'qty'=> $qty,
											'harga'=> $harga,
											'diskon'=> $diskon,
											'qty_sjkeluarwip'=> $qty_sjkeluarwip,
											'jum_beli'=> $jum_beli,
											'no_sjkeluarwip'=> $no_sjkeluarwip,
											'id_sjkeluarwip'=> $id_sjkeluarwip
									);
				
		}
	}
	return $detail_sjkeluarwip;
}
 function get_unit_jahit(){
	$query	= $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit ");    
    return $query->result();  
  }  
  
    function get_sjkeluarwiptanpalimit($id_ujh, $keywordcari, $tgldari, $tglke){
		$pencarian1='';
		$pencarian2='';
		if ($tgldari!= '0000-00-00'){
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		if ($tglke!= '0000-00-00'){
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}
		
		
	if ($keywordcari == "all") {
	
				$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip WHERE status_sjkeluarwip = 'f' AND status_aktif = 't'
											AND jenis_keluar='3' AND id_unit_jahit = '$id_ujh'".$pencarian1." ".$pencarian2);
	}
	else {
		
				$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip WHERE status_sjkeluarwip = 'f' AND status_aktif = 't' 
											AND jenis_keluar='3' AND id_unit_jahit = '$id_ujh' "
											.$pencarian1." ".$pencarian2.
							  "AND UPPER(no_sjkeluarwip) like UPPER('%$keywordcari%') ");
	}
    
    return $query->result();  
  }
  function get_sjkeluarwip($id_ujh, $keywordcari, $tgldari, $tglke){
	$pencarian1='';
		$pencarian2='';
		if ($tgldari!= '0000-00-00'){
		$pencarian1.="AND tgl_sj >= '$tgldari'";
		
		}
		if ($tglke!= '0000-00-00'){
		$pencarian2.=	" AND tgl_sj<= '$tglke'";
		}	
	
	if ($keywordcari == "all") {
				$sql = " distinct a.* FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
						WHERE a.status_nota_retur = 'f' AND a.status_aktif = 't' AND a.id_unit_jahit = '$id_ujh' ".$pencarian1." ".$pencarian2.
					"AND b.status_sjkeluarwip='f' AND a.jenis_keluar='3' order by a.tgl_sj DESC, a.no_sj DESC ";
			$this->db->select($sql, false);
			$query = $this->db->get();
		
	}
	else {
				$sql = " distinct a.* FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_barang_wip c ON b.id_brg_wip = c.id
							WHERE a.status_sjkeluarwip = 'f' AND a.status_aktif = 't' 
							 AND a.id_unit_jahit = '$id_ujh' AND b.status_sjkeluarwip='f' AND a.jenis_keluar='3'
								".$pencarian1." ".$pencarian2."
							 AND (UPPER(c.kode_brg_wip) like UPPER('%$keywordcari%') 
							 OR UPPER(c.nama_brg_wip) like UPPER('%$keywordcari%')) 
							order by a.tgl_sjkeluarwip DESC, a.no_sjkeluarwip DESC ";
			$this->db->select($sql, false);
			$query = $this->db->get();
	
	}
	
		$data_sjkeluarwip = array();
		$detail_sjkeluarwip = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
					$filterbrg = "";
					if ($keywordcari != "all")
						$filterbrg = " AND (UPPER(c.kode_brg_wip) like UPPER('%$keywordcari%') OR UPPER(c.nama_brg_wip) like UPPER('%$keywordcari%')) ";
					
					$sql2 = "SELECT b.* FROM tm_sjkeluarwip_detail b INNER JOIN tm_sjkeluarwip a ON b.id_sjkeluarwip = a.id 
							INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
							WHERE b.id_sjkeluarwip = '$row1->id' AND b.status_sjkeluarwip = 'f' ".$filterbrg." ORDER BY b.id ASC ";
				$query2	= $this->db->query($sql2);
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip 
										WHERE id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
											
							$query4	= $this->db->query(" SELECT id FROM tm_pembelianretur_wip_detail WHERE id_sjkeluarwip_detail = '".$row2->id."' ");
							if ($query4->num_rows() > 0){
						
									$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelianretur_wip_detail a 
											INNER JOIN tm_pembelianretur_wip b ON a.id_pembelianretur_wip = b.id WHERE b.status_aktif = 't' ";
									$sql3.= " AND a.id_sjkeluarwip_detail = '$row2->id' AND a.id_brg_wip = '$row2->id_brg_wip'
												 ";
									
									$query3	= $this->db->query($sql3);
									$hasilrow = $query3->row();
									$jum_sjkeluarwip = $hasilrow->jum; 
			
							}
							else {
								$jum_sjkeluarwip = 0;
							}
						
						$qty = $row2->qty-$jum_sjkeluarwip;
						
						
						if ($qty <= 0) {
							$this->db->query(" UPDATE tm_sjkeluarwip_detail SET status_sjkeluarwip = 't' WHERE id='".$row2->id."' ");
							
							$queryxx	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '".$row1->id."' AND status_sjkeluarwip = 'f' ");
							if ($queryxx->num_rows() == 0){
								$this->db->query(" UPDATE tm_sjkeluarwip SET status_sjkeluarwip = 't' WHERE id='".$row1->id."' ");
							}
						}
						//--------------------
						
						
							$nama_satuan_lain = '';
						
						if ($qty > 0) {
							$detail_sjkeluarwip[] = array(	'id'=> $row2->id,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $nama_brg,
												'qty'=> $qty,
												'keterangan'=> $row2->keterangan
												
											);
						}
					}
				}
				else {
					$detail_sjkeluarwip = '';
				}
				
				
					if ($row1->id_unit_jahit != '0') {
						$query3	= $this->db->query(" SELECT kode_unit, nama from tm_unit_jahit where id = '$row1->id_unit_jahit' ");
						$hasilrow = $query3->row();
						$kode_unit_jahit	= $hasilrow->kode_unit;
						$nama_unit_jahit	= $hasilrow->nama;
					}
					
				
				
				
					//$no_sjkeluarwip = $row1->no_sjkeluarwip;
					$no_sj = $row1->no_sj;
					//$tgl_sjkeluarwip = $row1->tgl_sjkeluarwip;
					$tgl_sj = $row1->tgl_sj;
				
				$data_sjkeluarwip[] = array(	'id'=> $row1->id,	
											'no_sj'=> $no_sj,
											'tgl_sj'=> $tgl_sj,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'tgl_update'=> $row1->tgl_update,
											
											'detail_sjkeluarwip'=> $detail_sjkeluarwip
											);
				$detail_sjkeluarwip = array();
			} // endforeach header
		}
		else {
			$data_sjkeluarwip = '';
		}
		return $data_sjkeluarwip;
  }
  
  function get_kel_brg(){
    $this->db->select("* from tm_kelompok_barang where nama <> 'Bahan Pembantu Produksi' order by kode ASC", false);
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function cek_data($no_sjkeluarpembelian, $id_unit_jahit){
    $this->db->select("id from tm_pembelianretur_wip WHERE no_sjkeluarpembelian = '$no_sjkeluarpembelian' AND id_unit_jahit = '$id_unit_jahit' AND status_aktif = 't' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  function save($no_sjkeluarpembelian,$tgl_sjpembelianretur,$id_unit_jahit,$id_unit_jahitbaru, $gtotal, $asligtotal, $total_pajak, $dpp, 
						$uang_muka, $sisa_hutang,$ket,$hide_pkp, $hide_tipe_pajak, $lain_cash, $lain_kredit,
				$id_sjkeluarwip_detail, $id_sjkeluarwip2,
				$id_brg_wip, $nama, $id_satuan, $id_satuan_konversi, $qty, $harga, $harga_lama, $pajak, $diskon, $total, $aslitotal){  
    
    // 25-06-2015 $satuan_lain, $qty_sat_lain dihilangkan
    
    $tgl = date("Y-m-d H:i:s");
    
    if ($id_unit_jahit == '0')
		$id_ujh_forsave = $id_unit_jahitbaru;
	else
		$id_ujh_forsave = $id_unit_jahit;
    
    // cek apa udah ada datanya blm
    $this->db->select("id from tm_pembelianretur_wip WHERE no_sjkeluarpembelian = '$no_sjkeluarpembelian' AND id_unit_jahit = '$id_ujh_forsave' 
					AND status_aktif = 't' ", false);
    $query = $this->db->get();
    
	if ($query->num_rows() == 0){
			
				$no_faktur = NULL;
				$tgl_faktur = NULL;
				$status_faktur = 'f';
				$faktur_sj = 'f';
						
			
			// 27-08-2015
			$uid_update_by = $this->session->userdata('uid');
			
				$data_header = array(
				 
				  'no_sjkeluarpembelian'=>$no_sjkeluarpembelian,
				  'tgl_sjpembelianretur'=>$tgl_sjpembelianretur,
				  'no_faktur'=>$no_faktur,
				  'id_unit_jahit'=>$id_ujh_forsave,
				  'total'=>$asligtotal,
				  'uang_muka'=>$uang_muka,
				  'sisa_hutang'=>$sisa_hutang,
				  'keterangan'=>$ket,
				  'pkp'=>$hide_pkp,
				  'tipe_pajak'=>$hide_tipe_pajak,
				  'total_pajak'=>$total_pajak,
				  'dpp'=>$dpp,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'faktur_sj'=>$faktur_sj,
				  'status_faktur'=>$status_faktur,
				  'stok_keluar_lain_cash'=>$lain_cash,
				  'stok_keluar_lain_kredit'=>$lain_kredit,
				  //'jenis_pembelian'=>$jenis_pembelian,
				  'uid_update_by'=>$uid_update_by );
			//}

		$this->db->insert('tm_pembelianretur_wip',$data_header);
	}

			//		$this->db->query(" UPDATE tm_sjkeluarwip SET status_edit = 't' where id= '$id_sjkeluarwip2' ");

			$this->db->query(" UPDATE tm_sjkeluarwip SET status_sjkeluarwip = 't' where id= '$id_sjkeluarwip2' ");
					
			// ambil data terakhir di tabel tm_pembelian
			$query2	= $this->db->query(" SELECT id FROM tm_pembelianretur_wip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_pembelianretur_wip	= $hasilrow->id; //echo $idnya; die();
			
			if ($id_brg_wip!='' && $qty!='0' && $harga!='') {
				$qty=str_replace(".0000","",$qty);

					$data_detail = array(
						'id_brg_wip'=>$id_brg_wip,
						// 29-10-2015 diganti jadi ga pake escape
						//'nama_brg'=>$this->db->escape_str($nama),
						'nama_brg'=>$nama,
						'qty'=>$qty,
						'id_satuan'=>$id_satuan,
						'id_satuan_konversi'=>$id_satuan_konversi,
						'harga'=>$harga,
						'pajak'=>$pajak,
						'diskon'=>$diskon,
						//'total'=>$total,
						'total'=>$aslitotal,
						'id_pembelianretur_wip'=>$id_pembelianretur_wip,
						'id_sjkeluarwip_detail'=>$id_sjkeluarwip_detail
					);
					
				$this->db->insert('tm_pembelianretur_wip_detail',$data_detail);
				
				
				$queryxx	= $this->db->query(" SELECT id FROM tm_pembelianretur_wip_detail ORDER BY id DESC LIMIT 1 ");
				$hasilxx = $queryxx->row();
				$id_pembelian_detail	= $hasilxx->id;

				// sementara id gudang blm diisi dulu, harus nanyain ke org Duta
					$id_gudang = 0;
					$lokasi = "01"; // duta

				//4. update harga di tabel harga_brg_supplier
				if (($harga != $harga_lama) && $id_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_jahit WHERE id_brg = '$id_brg_wip'
									AND id_unit_jahit = '$id_unit_jahit'  ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_unit_jahit (id_brg, id_unit_jahit, harga, 
						tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_jahit', '$harga', '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_unit_jahit SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg_wip' AND id_unit_jahit = '$id_unit_jahit'  ");
					}
				}
				
				if ($id_unit_jahit == '0') {
					$query3	= $this->db->query(" SELECT harga FROM tm_harga_brg_unit_jahit WHERE id_brg = '$id_brg_wip'
									AND id_unit_jahit = '$id_unit_jahitbaru'  ");
					if ($query3->num_rows() == 0){
						$this->db->query(" INSERT INTO tm_harga_brg_unit_jahit (id_brg, id_unit_jahit, harga, 
						tgl_input, tgl_update) VALUES ('$id_brg_wip', '$id_unit_jahitbaru', '$harga',  '$tgl', '$tgl') ");
					}
					else {
						$this->db->query(" UPDATE tm_harga_brg_unit_jahit SET harga = '$harga', tgl_update='$tgl'
									where id_brg= '$id_brg_wip' AND id_unit_jahit = '$id_unit_jahitbaru'  ");
					}
				}
			}
			
			// ---------------------------
		
					
					$query3	= $this->db->query(" SELECT a.qty FROM tm_sjkeluarwip_detail a INNER JOIN tm_sjkeluarwip b ON a.id_sjkeluarwip = b.id
								WHERE a.id = '$id_sjkeluarwip_detail' ");
								
					$hasilrow = $query3->row();
					$qty_sjkeluarwip = $hasilrow->qty; 
					
					// 19-12-2011, cek
					$query4	= $this->db->query(" SELECT id FROM tm_pembelianretur_wip_detail WHERE id_sjkeluarwip_detail = '".$id_sjkeluarwip_detail."' ");
					if ($query4->num_rows() > 0){
						
							$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_pembelianretur_wip_detail a INNER JOIN tm_pembelianretur_wip b ON a.id_pembelianretur_wip = b.id
									WHERE a.id_sjkeluarwip_detail = '$id_sjkeluarwip_detail' AND b.status_aktif = 't' ");
						
						$hasilrow = $query3->row();
						$jum_beli = $hasilrow->jum; // ini sum qty di pembelian_detail berdasarkan kode brg tsb
					}
					else {
						$jum_beli = 0;
					}
					
					
					if ($jum_beli >= $qty_sjkeluarwip) {
						$this->db->query(" UPDATE tm_sjkeluarwip_detail SET status_sjkeluarwip = 't' where id= '$id_sjkeluarwip_detail' ");
						
						$this->db->select("id from tm_sjkeluarwip_detail WHERE status_sjkeluarwip = 'f' AND id_sjkeluarwip = '$id_sjkeluarwip2' ", false);
						$query = $this->db->get();
						/*
						if ($query->num_rows() == 0){
							$this->db->query(" UPDATE tm_sjkeluarwip SET status_sjkeluarwip = 't' where id= '$id_sjkeluarwip2' ");
						}
						*/
					}

			//----------------------------
			
			//update pkp dan tipe_pajak di tabel supplier
			$this->db->query(" UPDATE tm_unit_jahit SET pkp = '$hide_pkp', tipe_pajak = '$hide_tipe_pajak' where id= '$id_ujh_forsave' ");
			
			// 18-06-2015, save ke apply_stok digabung kesini
			$th_now	= date("Y");
	
		// ====================================================
		
  }

  function getAlltanpalimit($cunit_jahit, $cari, $date_from, $date_to, $caribrg, $filterbrg){

    $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjkeluarpembelian) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianretur >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianretur <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_jahit != '0')
		$pencarian.= " AND a.id_unit_jahit = '$cunit_jahit' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelianretur DESC, a.id DESC ";
	
	$query = $this->db->query(" SELECT distinct a.* FROM tm_pembelianretur_wip a LEFT JOIN tm_pembelianretur_wip_detail b ON a.id=b.id_pembelianretur_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian);
	      
    return $query->result();  
  }
  function getAll($num, $offset, $cunit_jahit, $cari, $date_from, $date_to, $caribrg, $filterbrg) {	
	
	
	 $pencarian = "";
	if($cari!="all")
		$pencarian.= " AND UPPER(a.no_sjkeluarpembelian) like UPPER('%".$this->db->escape_str($cari)."%') ";
	
	if ($date_from != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianretur >= to_date('$date_from','dd-mm-yyyy') ";
	if ($date_to != "00-00-0000")
		$pencarian.= " AND a.tgl_sjpembelianretur <= to_date('$date_to','dd-mm-yyyy') ";
	if ($cunit_jahit != '0')
		$pencarian.= " AND a.id_unit_jahit = '$cunit_jahit' ";
	
	if ($filterbrg == "y" && $caribrg !="all")
		$pencarian.= " AND (UPPER(c.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
					OR UPPER(b.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
	$pencarian.= " ORDER BY a.tgl_sjpembelianretur DESC, a.id DESC ";
	
	$this->db->select(" distinct a.* FROM tm_pembelianretur_wip a LEFT JOIN tm_pembelianretur_wip_detail b ON a.id=b.id_pembelianretur_wip
						LEFT JOIN tm_barang_wip c ON c.id = b.id_brg_wip  WHERE  a.status_aktif = 't' ".$pencarian." ", false)->limit($num,$offset);
	$query = $this->db->get();
	  
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				//print_r($row1);
				$pencarian2 = "";
				if ($filterbrg == "y" && $caribrg !="all")
					$pencarian2.= " AND (UPPER(b.kode_brg) like UPPER('%".$this->db->escape_str($caribrg)."%') 
								OR UPPER(a.nama_brg) like UPPER('%".$this->db->escape_str($caribrg)."%')) ";
				
				$query2	= $this->db->query(" SELECT a.* FROM tm_pembelianretur_wip_detail a INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id
							WHERE a.id_pembelianretur_wip = '$row1->id' ".$pencarian2." ORDER BY a.id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					
					$id_detailnya = "";
					foreach ($hasil2 as $row2) {
						//----------------

					  
				
					  $id_detailnya = "";
					  
						//----------------
						
						$query3	= $this->db->query(" SELECT a.kode_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$kode_brg	= $hasilrow->kode_brg;
							
						}
						else {
							$kode_brg = '';
						
						}
						

						$detail_fb[] = array(	'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg_wip'=> $kode_brg,
												'nama_brg_wip'=> $row2->nama_brg,
											
												'qty'=> $row2->qty,
												'harga'=> $row2->harga,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total
											);
					}
				}
				else {
					$id_detailnya = "";
					$detail_fb = '';
				}
		
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$kode_unit_jahit	= $hasilrow->kode_unit;
				$nama_unit_jahit	= $hasilrow->nama;
		
				$no_sjkeluarwipnya = "";
				
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj FROM tm_pembelianretur_wip_detail a INNER JOIN tm_sjkeluarwip_detail b ON a.id_sjkeluarwip_detail = b.id 
						INNER JOIN tm_sjkeluarwip c ON c.id = b.id_sjkeluarwip
						WHERE a.id_pembelianretur_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$no_sjkeluarwipnya.= $rowxx->no_sj."<br>";
					}// end for
				}
				else {
					$no_sjkeluarwipnya = '';
				
				}
				/*
				$sqlxx = " SELECT status_stok FROM tm_apply_stok_pembelian WHERE no_sj = '$row1->no_sj' 
							AND id_supplier = '$row1->id_supplier' AND status_aktif = 't' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$status_stok= $hasilxx->status_stok;
					if ($status_stok == 't')
						$cetakbtb = '1';
					else
						$cetakbtb = '0';
				}
				else
					$cetakbtb = '0';
					*/
						
				$data_fb[] = array(			'id'=> $row1->id,	
											
											'no_sjkeluarwip'=> $no_sjkeluarwipnya,	
											
											'no_sjkeluarpembelian'=> $row1->no_sjkeluarpembelian,
											'tgl_sjpembelianretur'=> $row1->tgl_sjpembelianretur,
											'total'=> $row1->total,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'status_faktur'=> $row1->status_faktur,
											'status_stok'=> $row1->status_stok,
											'detail_fb'=> $detail_fb,
											'id_detailnya'=> $id_detailnya,
											//'cetakbtb'=> $cetakbtb
											//'ambil_pp'=> $ambil_pp
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function delete($id){    
	  $tgl = date("Y-m-d H:i:s");
	  
	  
	  
	    $sqlxx = " SELECT DISTINCT id_sjkeluarwip_detail FROM tm_pembelianretur_wip_detail WHERE id_pembelianretur_wip='$id' ";
		$queryxx	= $this->db->query($sqlxx);
		if ($queryxx->num_rows() > 0){
			//$hasilxx = $queryxx->row();
			$hasilxx=$queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$id_sjkeluarwip_detail	= $rowxx->id_sjkeluarwip_detail;
				
				
				if ($id_sjkeluarwip_detail != '0') {
					$sqlxx2= " SELECT distinct a.id FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip 
							 WHERE b.id = '$id_sjkeluarwip_detail' ";
					$queryxx2	= $this->db->query($sqlxx2);
					if ($queryxx2->num_rows() > 0){
						$hasilxx2 = $queryxx2->row();
						$id_sjkeluarwip	= $hasilxx2->id;
						$this->db->query(" UPDATE tm_sjkeluarwip SET status_edit = 'f', status_sjkeluarwip = 'f' where id= '$id_sjkeluarwip' ");
					}
					// reset status di detailnya dari sjkeluarwip pake perulangan
					$this->db->query(" UPDATE tm_sjkeluarwip_detail SET status_sjkeluarwip = 'f' where id= '$id_sjkeluarwip_detail' ");
				}
						
				
				
			} // end for
		}
	
	$this->db->query(" UPDATE tm_pembelianretur_wip SET status_aktif = 'f', status_stok = 'f' WHERE id = '$id' ");
		$this->db->query(" UPDATE tm_pembelianretur_wip_detail SET status_stok = 'f' WHERE id_pembelianretur_wip = '$id' ");
  }
  
  function get_retur($id_retur_wip) {
			$query	= $this->db->query(" SELECT * FROM tm_pembelianretur_wip where id = '$id_retur_wip' ");
	
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			
				
				
				// 10-07-2015 DIMODIF
				$no_sjkeluarwipnya = ""; 
				// 10-07-2015
				$sqlxx = " SELECT DISTINCT c.no_sj,c.tgl_sj FROM tm_pembelianretur_wip_detail a 
				INNER JOIN tm_sjkeluarwip_detail b ON a.id_sjkeluarwip_detail = b.id 
				INNER JOIN tm_sjkeluarwip c ON c.id = b.id_sjkeluarwip WHERE a.id_pembelianretur_wip='$row1->id' ORDER BY c.no_sj ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					//$hasilxx = $queryxx->row();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$pisah1 = explode("-", $rowxx->tgl_sj);
						$thn1= $pisah1[0];
						$bln1= $pisah1[1];
						$tgl1= $pisah1[2];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						$no_sjkeluarwipnya.= $rowxx->no_sj." (".$tgl_sj.")"."<br>";
					}// end for
				}
				else {
					$no_sjkeluarwipnya = '-';
				
				}
				
				
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelianretur_wip_detail WHERE id_pembelianretur_wip = '$row1->id' ORDER BY id ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.kode_brg, a.nama_brg FROM tm_barang_wip a WHERE a.id = '$row2->id_brg_wip' ");
						$hasilrow = $query3->row();
						$kode_brg	= $hasilrow->kode_brg;
						$nama_brg	= $hasilrow->nama_brg;
						
						$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->id_satuan' ");
						if($query3->num_rows > 0 ){
						$hasilrow = $query3->row();
						$nama_satuan = $hasilrow->nama;
					}
					else{
						$nama_satuan = 'pieces';
						}
						
						
						
						$sum_qty = 0;
						$sum_beli = 0;
					  
					  if ($row2->satuan_lain != 0) {
						  $query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row2->satuan_lain' ");
						  $hasilrow = $query3->row();
						  $nama_satuan_lain = $hasilrow->nama;
					  }
					  else
						$nama_satuan_lain = '';
					  
					    $query3	= $this->db->query(" SELECT d.kode as kode_kel_brg FROM tm_barang_wip a 
					    INNER JOIN tm_jenis_brg_wip c ON a.id_jenis_brg_wip = c.id 
						INNER JOIN tm_kel_brg_wip d ON a.id_kel_brg_wip=d.id
									WHERE a.id = '$row2->id_brg_wip' ");
						if($query3->num_rows > 0){			
						$hasilrow = $query3->row();
						$kode_kel_brg = $hasilrow->kode_kel_brg;
					}
					else
						$kode_kel_brg = 0;
						
						$sqlop = " SELECT b.qty FROM tm_sjkeluarwip a INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE b.id = '$row2->id_sjkeluarwip_detail' ";
						$queryop	= $this->db->query($sqlop);
						if($queryop->num_rows()>0) {
							$hasilop = $queryop->row();
							$qty_op_detail = $hasilop->qty;
						}
						else
							$qty_op_detail = 0;
						
						if ($row2->id_sjkeluarwip_detail != '0') {
							$query3	= $this->db->query(" SELECT SUM(a.qty) AS pemenuhan FROM tm_pembelianretur_wip_detail a 
										INNER JOIN tm_pembelianretur_wip b ON b.id=a.id_pembelianretur_wip
										WHERE a.id_sjkeluarwip_detail='$row2->id_sjkeluarwip_detail' AND a.id_brg_wip='$row2->id_brg_wip' 
										AND b.status_aktif='t'
										AND b.tgl_sjpembelianretur <= '$row1->tgl_sjpembelianretur' ");
							
							if($query3->num_rows()>0) {
								$hasilrow4 = $query3->row();
								$pemenuhan = $hasilrow4->pemenuhan;
								$sisa = $qty_op_detail-$pemenuhan;
							}else{
								$pemenuhan = 0;
								$sisa = $qty_op_detail-$pemenuhan;
								
								if($pemenuhan=='')
									$pemenuhan = 0;
								
								if($sisa=='')
									$sisa = 0;
							}
						}
						else {
							$sisa = '-';
							$pemenuhan = 0;
						}
										
						$detail_fb[] = array(	'id'=> $row2->id,
												'kode_kel_brg'=> $kode_kel_brg,
												'id_brg_wip'=> $row2->id_brg_wip,
												'kode_brg'=> $kode_brg,
												'nama'=> $this->db->escape_str($nama_brg),
												'nama_satuan'=> $nama_satuan,
												'harga'=> $row2->harga,
												'qty'=> $row2->qty,
												'id_satuan'=> $row2->id_satuan,
												'id_satuan_konversi'=> $row2->id_satuan_konversi,
												'pajak'=> $row2->pajak,
												'diskon'=> $row2->diskon,
												'total'=> $row2->total,
												'qty_sjmasukwip'=> $sum_qty,
												'jum_beli'=> $sum_beli,
												'satuan_lain'=> $row2->satuan_lain,
												'nama_satuan_lain'=> $nama_satuan_lain,
												'qty_sat_lain'=> $row2->qty_satuan_lain,
												'id_sjkeluarwip_detail'=> $row2->id_sjkeluarwip_detail,
												
												
												'pemenuhan'=> $pemenuhan,
												'sisa'=> $sisa
											);
					}
				}
				else {
					$detail_fb = '';
				}


				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$row1->id_unit_jahit' ");
				$hasilrow = $query3->row();
				$kode_unit_jahit	= $hasilrow->kode_unit;
				$nama_unit_jahit	= $hasilrow->nama;
				$top	= '';
				
				$pisah1 = explode("-", $row1->tgl_sjpembelianretur);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_sj = $tgl1."-".$bln1."-".$thn1;
				
				/*
				// 31-07-2015, ambil id gudang pake distinct utk keperluan nama staf adm stok di cetak BTB
				$sqlxx = " SELECT DISTINCT d.id_gudang FROM tm_apply_stok_pembelian_detail a 
							INNER JOIN tm_pembelian_wip_detail b ON a.id_pembelian_detail = b.id
							INNER JOIN tm_pembelian_wip c ON b.id_pembelian = c.id
							INNER JOIN tm_barang d ON d.id = a.id_brg
							WHERE c.id='$row1->id' AND a.status_stok = 't'  ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$list_id_gudang='';
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$list_id_gudang.= $rowxx->id_gudang.";";
					}// end for
				}
				else {
					$list_id_gudang='';
				}
				
				
				
				// 27-08-2015 ambil uid_update_by dari tabel tm_apply_stok_pembelian
				$sqlxx = " SELECT uid_update_by FROM tm_apply_stok_pembelian WHERE status_aktif = 't'
						AND no_sj = '$row1->no_sj' AND id_supplier = '$row1->id_supplier' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$admgudang_uid_update_by = $hasilxx->uid_update_by;
				}
				else
					$admgudang_uid_update_by = 0;
				*/
								
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_sjkeluarwip'=> $no_sjkeluarwipnya,
											//'no_bonm'=> $no_bonmnya,
											//'no_pp'=> $no_ppnya,	
											'no_sjkeluarpembelian'=> $row1->no_sjkeluarpembelian,
											'tgl_sj'=> $tgl_sj,
											'jenis_pembelian'=> $row1->jenis_pembelian,
											'total'=> $row1->total,
											'id_unit_jahit'=> $row1->id_unit_jahit,
											'kode_unit_jahit'=> $kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'top'=> $top,
											'total_pajak'=> $row1->total_pajak,
											'uang_muka'=> $row1->uang_muka,
											'sisa_hutang'=> $row1->sisa_hutang,
											'keterangan'=> $row1->keterangan,
											'tgl_update'=> $row1->tgl_update,
											'pkp'=> $row1->pkp,
											'tipe_pajak'=> $row1->tipe_pajak,
											'dpp'=> $row1->dpp,
											'stok_keluar_lain_cash'=> $row1->stok_keluar_lain_cash,
											'stok_keluar_lain_kredit'=> $row1->stok_keluar_lain_kredit,
											'detail_fb'=> $detail_fb,
											//'list_id_gudang'=> $list_id_gudang,
										//	'admgudang_uid_update_by'=> $admgudang_uid_update_by
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }  
}
