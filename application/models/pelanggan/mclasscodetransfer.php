<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclasscodetransfer extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" 
		SELECT  a.i_reff_transfer,
				a.i_customer_from,
				a.i_customer_transfer
		
		FROM tr_customer_transfer a ORDER BY a.i_customer_transfer ASC ", false);	
	}

	function viewperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" 
		SELECT  a.i_reff_transfer,
				a.i_customer_from,
				a.i_customer_transfer
				
		FROM tr_customer_transfer a ORDER BY a.i_customer_transfer ASC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function ckpelanggan($itransfer) {
		$db2=$this->load->database('db_external', TRUE);
		$q_str	= " SELECT * FROM tr_customer_transfer WHERE i_customer_transfer='$itransfer' ";
		return $db2->query($q_str);
	}

	function ecustomer($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		$q_str	= " SELECT * FROM tr_customer WHERE i_customer='$icustomer' ";
		return $db2->query($q_str);
	}
			
	function listkonstumer() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_customer ORDER BY e_customer_name ASC, i_customer_code DESC " );
	}
	
	function listkonstumerperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT * FROM tr_customer ORDER BY e_customer_name ASC, i_customer_code DESC LIMIT ".$limit." OFFSET ".$offset );
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}
	
	function msimpan($icustomer,$itransfer) {
		$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;		
		$qry	= $db2->query("SELECT CAST(i_reff_transfer AS integer)+1 AS irefftransfer FROM tr_customer_transfer ORDER BY i_reff_transfer DESC LIMIT 1");
		
		if($qry->num_rows()>0) {
			$row = $qry->row();
			$irefftransfer  = $row->irefftransfer;
		} else {
			$irefftransfer  = 1;
		}
		
		$tr_customer_transfer	= array(
			 'i_reff_transfer'=>$irefftransfer,
			 'i_customer_from'=>$icustomer,
			 'i_customer_transfer'=>$itransfer,
			 'd_entry'=>$dentry
		);
		$db2->set($tr_customer_transfer);
		$db2->insert('tr_customer_transfer');
		redirect('pelanggan/cformcodetransfer/');
	}
	
	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_customer_transfer WHERE i_reff_transfer='$id' ");
	}
	
	function icustomer($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_customer WHERE i_customer='$icustomer' ");
	}
	
	function mupdate($irefftransfer,$icustomer,$itransfer) {
		$db2=$this->load->database('db_external', TRUE);
		
		$tr_customer_transfer	= array(
			'i_customer_from'=>$icustomer,
			'i_customer_transfer'=>$itransfer
		);
		
		$db2->update('tr_customer_transfer',$tr_customer_transfer,array('i_reff_transfer'=>$irefftransfer));
		redirect('pelanggan/cformcodetransfer/');
	}
	
	function viewcari($txtcari) {
		$db2=$this->load->database('db_external', TRUE);
		if($txtcari!="") {
			$filter	= " WHERE (i_customer_from LIKE '%$txtcari%' OR i_customer_transfer LIKE '%$txtcari%') ";
		} else {
			$filter	= " ";
		}
		return $db2->query( "SELECT * FROM tr_customer_transfer ".$filter." ORDER BY i_customer_transfer DESC " );
	}
	
	function mcari($txtcari,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		if($txtcari!="") {
			$filter	= " WHERE (i_customer_from LIKE '%$txtcari%' OR i_customer_transfer LIKE '%$txtcari%') ";
		} else {
			$filter	= " ";
		}
		$query	= $db2->query(" SELECT * FROM tr_customer_transfer ".$filter." ORDER BY i_customer_transfer DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_customer_transfer',array('i_reff_transfer'=>$id));
		redirect('pelanggan/cformcodetransfer/');	 
  }
}
?>
