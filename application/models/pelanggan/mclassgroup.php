<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclassgroup extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	function delete($id){
		$db2=$this->load->database('db_external', TRUE);
		 $db2->delete('tr_group',array('i_group'=>$id));
		 redirect('pelanggan/cformgroup');
	}
  	
	function view(){
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_group_name ASC, cast(i_group_code AS integer) DESC ";
		return $db2->query(" SELECT * FROM tr_group ".$order." ", false);
	}

	function viewperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_group_name ASC, cast(i_group_code AS integer) DESC ";
		$query	= $db2->query(" SELECT * FROM tr_group ".$order." LIMIT ".$limit." OFFSET ".$offset, false);

		if ($query->num_rows() > 0){
			return $result = $query->result();
		}
	}
	
	function codegrouppelanggan(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT cast(i_group AS integer)+1 AS igroup, cast(i_group_code AS integer)+1 AS igroupcode FROM tr_group ORDER BY cast(i_group AS integer) DESC LIMIT 1 ");
	}
	
	function msimpan($i_group_code,$e_group_name,$e_note){
	$db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$qry_cust_code	= $db2->query( " SELECT cast(i_group AS integer)+1 AS igroup, cast(i_group_code AS integer)+1 AS igroupcode FROM tr_group ORDER BY cast(i_group AS integer) DESC LIMIT 1 ");
		
		if($qry_cust_code->num_rows() > 0){
			$row_cust_code	=  $qry_cust_code->row();
			$igroup	= $row_cust_code->igroup;
			$igroupcode	= $row_cust_code->igroupcode;

			switch(strlen($igroupcode)) {
				case 1:
					$groupcode	= '0000'.$igroupcode;
				break;
				case 2:
					$groupcode	= '000'.$igroupcode;
				break;
				case 3:
					$groupcode	= '00'.$igroupcode;
				break;
				case 4:
					$groupcode	= '0'.$igroupcode;
				break;
				case 5:
					$groupcode	= $igroupcode;
				break;
			}
			
		} else {
			$igroup	= 1;

			$groupcode	= '00001';
		}
		
		$db2->set(array(
			'i_group'=>$igroup,
			'i_group_code'=>$groupcode,
			'e_group_name'=>$e_group_name,
			'e_note'=>$e_note,
			'd_entry'=>$dentry	
		));

		$db2->insert('tr_group');
		redirect('pelanggan/cformgroup/');
	}
	
	function mupdate($i_group,$i_group_code,$e_group_name,$e_note) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$customer_item	= array(
			'i_group_code'=>$i_group_code,
			'e_group_name'=>$e_group_name,
			'e_note'=>$e_note
		);
		
		$db2->update('tr_group',$customer_item,array('i_group'=>$i_group));

		if ($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	
		redirect('pelanggan/cformgroup/');
	}
	
	function medit($igroup) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_group WHERE i_group='$igroup' ORDER BY i_group DESC, i_group_code DESC LIMIT 1 " );
	}
	
	function viewcari($txt_i_group_code,$txt_group_name) {
		$db2=$this->load->database('db_external', TRUE);
		if($txt_i_group_code!="") {
			return $db2->query(" SELECT * FROM tr_group WHERE i_group_code='$txt_i_group_code' AND e_group_name LIKE '$txt_group_name%' ORDER BY e_group_name ASC ");
		} else {
			return $db2->query(" SELECT * FROM tr_group WHERE e_group_name LIKE '$txt_group_name%' ORDER BY e_group_name ASC ");
		}
	}
	
	function mcari($txt_i_group_code,$txt_group_name,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		//mcari($txt_i_group_code,$txt_group_name,$pagination['per_page'],$pagination['cur_page'])
		if($txt_i_group_code!="") {
			$query	= $db2->query(" SELECT * FROM tr_group WHERE i_group_code='$txt_i_group_code' AND e_group_name LIKE '$txt_group_name%' ORDER BY e_group_name ASC LIMIT ".$limit." OFFSET ".$offset,false);
			if($query->num_rows()>0) {
				return $result	= $query->result();
			}
		} else {
			$query	= $db2->query(" SELECT * FROM tr_group WHERE e_group_name LIKE '$txt_group_name%' ORDER BY e_group_name ASC LIMIT ".$limit." OFFSET ".$offset,false);
			if($query->num_rows()>0) {
				return $result	= $query->result();
			}		
		}
	}
}
?>
