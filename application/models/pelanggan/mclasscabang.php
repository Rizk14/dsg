<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclasscabang extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" 
		SELECT  a.i_branch,
				a.i_branch_code,
				a.i_code,
				a.e_branch_name,
				b.i_customer_code,
				a.e_branch_city,
				a.e_initial
		FROM tr_branch a 
		
		INNER JOIN tr_customer b ON a.i_customer=b.i_customer ORDER BY a.e_branch_name ASC, cast(a.i_branch AS integer) DESC ", false);	
	}

	function viewperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" 
		SELECT  a.i_branch,
				a.i_branch_code,
				a.i_code,
				a.e_branch_name,
				b.i_customer_code,
				a.e_branch_city,
				a.e_initial
		FROM tr_branch a 
		INNER JOIN tr_customer b ON a.i_customer=b.i_customer ORDER BY a.e_branch_name ASC, cast(a.i_branch AS integer) DESC	LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function buatkode(){
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT cast(i_branch_code AS integer)+1 AS ibranchcode FROM tr_branch ORDER BY cast(i_branch_code AS integer) DESC LIMIT 1 ");
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function ckpelanggan($ibranch) {
		$db2=$this->load->database('db_external', TRUE);
		$q_str	= " SELECT * FROM tr_branch WHERE i_branch_code='$ibranch' ";
		return $db2->query($q_str);
	}

	function ckpelangganinisial($einisial) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT * FROM tr_branch WHERE e_initial='$einisial' ");
	}
			
	function listkonstumer() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_customer ORDER BY e_customer_name ASC, i_customer_code DESC " );
	}
	
	function listkonstumerperpages($limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT * FROM tr_customer ORDER BY e_customer_name ASC, i_customer_code DESC LIMIT ".$limit." OFFSET ".$offset );
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}
	
	function msimpan($i_branch,$i_area,$i_customer,$e_branch_name,$e_branch_address,$e_branch_city,$e_initial,$icustomer) {
	  $db2=$this->load->database('db_external', TRUE);
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		$qry_cabang	= $db2->query("SELECT cast(i_branch AS integer)+1 AS icabang FROM tr_branch ORDER BY cast(i_branch AS integer) DESC LIMIT 1");
		if($qry_cabang->num_rows()>0) {
			$row_icab = $qry_cabang->row();
			$icabang  = $row_icab->icabang;
		} else {
			$icabang  = 1;
		}
		/* 20052011
		*/
		$tr_pelanggan	= array(
			 'i_branch'=>$icabang,
			 'i_branch_code'=>$i_branch,
			 'i_code'=>$i_area,
			 'i_customer'=>$icustomer,
			 'e_branch_name'=>$e_branch_name,
			 'e_branch_address'=>$e_branch_address,
			 'e_branch_city'=>$e_branch_city,
			 'e_initial'=>$e_initial,
			 'd_entry'=>$dentry );
		
		$db2->set($tr_pelanggan);

		$db2->insert('tr_branch');

		redirect('pelanggan/cformcabang/');
	}
	
	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_branch WHERE i_branch='$id' ORDER BY i_branch DESC, e_branch_name ASC LIMIT 1 ");
	}
	
	function icustomer($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_customer WHERE i_customer='$icustomer' ORDER BY i_customer DESC LIMIT 1 ");
	}
	
	function mupdate($ibranch,$iarea,$icustomer,$ebranchname,$ebranchaddress,$ebranchcity,$einitial,$ibranch2,$icustomer2) {
		$db2=$this->load->database('db_external', TRUE);
		$cabang_item	= array(
			'i_customer'=>$icustomer2,
			'i_branch_code'=>$ibranch,
			'i_code'=>$iarea,
			'e_branch_name'=>$ebranchname,
			'e_branch_address'=>$ebranchaddress,
			'e_branch_city'=>$ebranchcity,
			'e_initial'=>$einitial
		);
		
		$db2->update('tr_branch',$cabang_item,array('i_branch'=>$ibranch2));
		redirect('pelanggan/cformcabang/');
	}
	
	function viewcari($txt_i_branch,$txt_i_customer_code,$txt_e_branch_name) {
		$db2=$this->load->database('db_external', TRUE);
		if($txt_i_branch!="") {
			$filter	= " WHERE a.i_code='$txt_i_branch' AND (b.i_customer_code LIKE '$txt_i_customer_code%' OR a.e_branch_name LIKE '$txt_e_branch_name%') ";
		} else {
			$filter	= " WHERE b.i_customer_code LIKE '$txt_i_customer_code%' OR a.e_branch_name LIKE '$txt_e_branch_name%' ";		
		}	
		return $db2->query( " 
		SELECT  a.i_branch,
				a.i_branch_code,
				a.i_code,
				a.e_branch_name,
				b.i_customer_code,
				a.e_branch_city,
				a.e_initial
				
		FROM tr_branch a 
		INNER JOIN tr_customer b ON a.i_customer=b.i_customer "
		.$filter.
		" ORDER BY a.i_branch DESC, a.e_branch_name ASC	" );
	}
	
	function mcari($txt_i_branch,$txt_i_customer_code,$txt_e_branch_name,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		//~ if($txt_i_branch!="") {
			//~ $filter	= " WHERE a.i_code='$txt_i_branch' AND (b.i_customer_code LIKE '$txt_i_customer_code%' OR a.e_branch_name LIKE '$txt_e_branch_name%') ";
		//~ } else {
			//~ $filter	= " WHERE b.i_customer_code LIKE '$txt_i_customer_code%' OR a.e_branch_name LIKE '$txt_e_branch_name%' ";		
		//~ }
		$filter	="";
		if($txt_i_branch!="") {
			$filter	.= " AND a.i_code='$txt_i_branch' ";
		} 		
		if($txt_i_customer_code!="") {
			$filter	.= " AND b.i_customer_code LIKE '$txt_i_customer_code%' ";		
		}
		if($txt_e_branch_name!="") {
			$filter	.= " AND a.e_branch_name LIKE '%$txt_e_branch_name%' ";		
		}
		$query	= $db2->query(" 
		SELECT  a.i_branch,
				a.i_branch_code,
				a.i_code,
				a.e_branch_name,
				b.i_customer_code,
				a.e_branch_city,
				a.e_initial
								
		FROM tr_branch a 
		INNER JOIN tr_customer b ON a.i_customer=b.i_customer WHERE TRUE "
		.$filter.
		" ORDER BY a.i_branch DESC, a.e_branch_name ASC	LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}		
	}

  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_branch',array('i_branch'=>$id));
		redirect('pelanggan/cformcabang/');	 
  }
}
