<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_customer_name ASC, i_customer_code DESC ";
		return $db2->query(" SELECT * FROM tr_customer ".$order." ", false);
	}

	function viewperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_customer_name ASC, i_customer_code DESC ";
		$query	= $db2->query(" SELECT * FROM tr_customer ".$order." LIMIT ".$limit." OFFSET ".$offset, false);

		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function ckpelanggan($i_customer_code) {
		$db2=$this->load->database('db_external', TRUE);
		$icustomercode	= trim($i_customer_code);
		return $db2->query(" SELECT * FROM tr_customer WHERE i_customer_code='$icustomercode' ");
	}
	
	function msimpan($i_customer_code,$e_customer_name,$e_customer_address,$f_customer_pkp,$e_customer_npwp,$n_customer_top,$f_customer_konsinyasi,$e_customer_phone,$e_customer_fax,$e_customer_contact,$igroup) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		if(is_numeric($n_customer_top)){
			$ncustomertop	= $n_customer_top;
		}else{
			$ncustomertop	= 0;
		}
		$icustomercode	= strtoupper($i_customer_code);
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$f_customer_pkp 	= ($f_customer_pkp=='1' || $f_customer_pkp=="1")?'TRUE':'FALSE';
		$f_customer_konsinyasi	= ($f_customer_konsinyasi=='1' || $f_customer_konsinyasi=="1")?'TRUE':'FALSE';
		$qry_cust_code	= $db2->query( " SELECT cast(i_customer AS integer)+1 AS icustomer FROM tr_customer ORDER BY cast(i_customer AS integer) DESC LIMIT 1 ");
		
		if($qry_cust_code->num_rows>0) {
			$row_cust_code	=  $qry_cust_code->row();
			$icustomer	= $row_cust_code->icustomer;
		} else {
			$icustomer	= 1;
		}
		
		$tr_cust	= array(
			'i_customer'=>$icustomer,
			'i_customer_code'=>$icustomercode,
			'e_customer_name'=>$e_customer_name,
			'e_customer_address'=>$e_customer_address,
			'f_customer_pkp'=>$f_customer_pkp,
			'e_customer_npwp'=>$e_customer_npwp,
			'n_customer_top'=>$ncustomertop,
			'f_customer_konsinyasi'=>$f_customer_konsinyasi,
			'e_customer_phone'=>$e_customer_phone,
			'e_customer_fax'=>$e_customer_fax,
			'e_customer_contact'=>$e_customer_contact,
			'i_group_code'=>$igroup,
			'd_entry'=>$dentry	
		);
		
		$db2->set($tr_cust);
		$db2->insert('tr_customer');

		if ($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
				
		redirect('pelanggan/cform/');
	}
	
	function mupdate($i_customer_code,$e_customer_name,$e_customer_address,$f_customer_pkp,$e_customer_npwp,$n_customer_top,$f_customer_konsinyasi,$e_customer_phone,$e_customer_fax,$e_customer_contact,$i_customer2,$i_customer_code2,$igroup) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$fcustomerpkp	= $f_customer_pkp==1?'TRUE':'FALSE';
		$fcustomerkonsinyasi	= $f_customer_konsinyasi==1?'TRUE':'FALSE';

		if(is_numeric($n_customer_top)){
			$ncustomertop	= $n_customer_top;
		}elseif(!is_numeric($n_customer_top) || $n_customer_top==''){
			$ncustomertop	= 0;
		}else{
			$ncustomertop	= 0;
		}
		
		$customer_item	= array(
			'i_customer_code'=>$i_customer_code,
			'e_customer_name'=>$e_customer_name,
			'e_customer_address'=>$e_customer_address,
			'f_customer_pkp'=>$fcustomerpkp,
			'e_customer_npwp'=>$e_customer_npwp,
			'n_customer_top'=>$ncustomertop,
			'f_customer_konsinyasi'=>$fcustomerkonsinyasi,
			'e_customer_phone'=>$e_customer_phone,
			'e_customer_fax'=>$e_customer_fax,
			'e_customer_contact'=>$e_customer_contact,
			'i_group_code'=>$igroup
		);
		
		$db2->update('tr_customer',$customer_item,array('i_customer'=>$i_customer2));

		if ($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
		redirect('pelanggan/cform/');
	}
	
	function medit($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_customer WHERE i_customer='$icustomer' ORDER BY i_customer DESC, i_customer_code DESC LIMIT 1 " );
	}
	
	function viewcari($txt_i_customer_code,$txt_e_customer_name,$txt_e_cust_phonefax) {
		$db2=$this->load->database('db_external', TRUE);
		if($txt_i_customer_code!="") {
			return $db2->query(" SELECT * FROM tr_customer WHERE i_customer_code LIKE '$txt_i_customer_code%' AND (e_customer_name LIKE '$txt_e_customer_name%' OR e_customer_phone LIKE '$txt_e_cust_phonefax%' OR e_customer_fax LIKE '$txt_e_cust_phonefax%') ORDER BY e_customer_name ASC ");
		} else {
			return $db2->query(" SELECT * FROM tr_customer WHERE (e_customer_name LIKE '$txt_e_customer_name%' OR e_customer_phone LIKE '$txt_e_cust_phonefax%' OR e_customer_fax LIKE '$txt_e_cust_phonefax%') ORDER BY e_customer_name ASC ");
		}
	}
	
	function mcari($txt_i_customer_code,$txt_e_customer_name,$txt_e_cust_phonefax,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		if($txt_i_customer_code!="") {
			$query	= $db2->query(" SELECT * FROM tr_customer WHERE i_customer_code LIKE '$txt_i_customer_code%' AND (e_customer_name LIKE '$txt_e_customer_name%' OR e_customer_phone LIKE '$txt_e_cust_phonefax%' OR e_customer_fax LIKE '$txt_e_cust_phonefax%') ORDER BY e_customer_name ASC LIMIT ".$limit." OFFSET ".$offset,false);
			if($query->num_rows()>0) {
				return $result	= $query->result();
			}
		} else {
			$query	= $db2->query(" SELECT * FROM tr_customer WHERE (e_customer_name LIKE '$txt_e_customer_name%' OR e_customer_phone LIKE '$txt_e_cust_phonefax%' OR e_customer_fax LIKE '$txt_e_cust_phonefax%') ORDER BY e_customer_name ASC LIMIT ".$limit." OFFSET ".$offset,false);
			if($query->num_rows()>0) {
				return $result	= $query->result();
			}		
		}
	}

	function listgrouppelanggan() {
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY e_group_name ASC, i_group_code DESC ";
		$query	= $db2->query(" SELECT * FROM tr_group ".$order." ", false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}	

  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_customer',array('i_customer'=>$id));
		redirect('pelanggan/cform/');	 
  }	

	function caricustomer($kodecustomer) {
		$db2=$this->load->database('db_external', TRUE);
		$kodecustomer	= strtoupper($kodecustomer);
		return $db2->query(" SELECT * FROM tr_customer WHERE i_customer_code=trim('$kodecustomer') ");
	}  
}
?>
