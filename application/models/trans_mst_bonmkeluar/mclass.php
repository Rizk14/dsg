<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_to_outbomm ORDER BY i_to_outbonm DESC", false);
	}

	function viewperpages($limit,$offset) {	
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_to_outbomm ORDER BY i_to_outbonm DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
		
	function msimpan($istatusbonmkeluar,$estatusname) {
		$db2=$this->load->database('db_external', TRUE);
		$str = array(
			'i_to_outbonm'=>$istatusbonmkeluar,
			'e_to'=>$estatusname
		);
		
		$db2->insert('tr_to_outbomm',$str);
		
		redirect('trans_mst_bonmkeluar/cform/');
    }
	
	function mupdate($istatusbonmkeluar,$estatusname) {
		$db2=$this->load->database('db_external', TRUE);
		$class_item	= array(
			'e_to'=>$estatusname
		);
		
		$db2->update('tr_to_outbomm',$class_item,array('i_to_outbonm'=>$istatusbonmkeluar));
		redirect('trans_mst_bonmkeluar/cform');
	}

	function code() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("SELECT cast(i_to_outbonm AS integer)+1 AS code FROM tr_to_outbomm ORDER BY i_to_outbonm DESC LIMIT 1");
	}

	function medit($id) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_to_outbomm WHERE i_to_outbonm='$id' ");
	}
	
	function viewcari($txt_i_status_bonmkeluar,$txt_e_statusname) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_to_outbomm WHERE i_to_outbonm='$txt_i_status_bonmkeluar' OR e_to LIKE '%$txt_e_statusname%' ");
	}
	
	function mcari($txt_i_status_bonmkeluar,$txt_e_statusname,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM tr_to_outbomm WHERE i_to_outbonm='$txt_i_status_bonmkeluar' OR e_to LIKE '%$txt_e_statusname%' LIMIT ".$limit." OFFSET ".$offset,false);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

  function delete($id) {
	  $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_to_outbomm',array('i_to_outbonm'=>$id));
		redirect('trans_mst_bonmkeluar/cform/');	 
  }		
}
?>
