<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}


	function hargaperpelanggan($imotif,$icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='$icustomer' AND f_active='t' ");
	}
		
	function hargadefault($imotif) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT v_price FROM tr_product_price WHERE i_product_motif='$imotif' AND i_customer='0' AND f_active='t' ");
	}
		
	function jmlDO($idocode){
		$db2=$this->load->database('db_external', TRUE);
		return	$db2->query(" SELECT * FROM tm_do_item a INNER JOIN tm_do b ON a.i_do=b.i_do WHERE b.i_do_code='$idocode' AND b.f_do_cancel='f' ");
	}
		
	function cekstatusfaktur($idocode) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT  a.d_do,
				a.i_do_code
					
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch 

				WHERE a.f_faktur_created='t' AND a.i_do_code='$idocode' AND a.f_do_cancel='f'
				GROUP BY a.d_do, a.i_do_code 
				
				ORDER BY a.i_do_code DESC ");	
	}
	
	function clistdobrg($d_do_first,$d_do_last,$kddo,$ftransfer,$ffaktur,$fcetak) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($kddo)) {
			$do	= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		}else{
			$do	= "";
			$ibatal	= " WHERE a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		}
		
		/* 27072011		
		$str_qry	= "
				SELECT  a.d_do AS dop,
					a.i_do_code AS idocode,
					a.i_do AS ido,
					a.i_branch AS ibranch,
					e.e_branch_name AS ebranchname,
					b.i_op AS iop,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_deliver AS qty,
					b.n_residual AS sisadelivery
				
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$do." ".$ibatal." 				
				ORDER BY b.i_product ASC ";
		*/
		
		$str_qry	= "
		SELECT a.d_do AS dop,
			a.i_do_code AS idocode,
			a.i_do AS ido,
			a.i_branch AS ibranch, 
			e.e_branch_name AS ebranchname,
			b.i_op AS iop,
			f.i_op_code AS iopcode,
			b.i_product AS iproduct, 
			c.e_product_motifname AS motifname,
			b.n_deliver AS qty,
			b.n_residual AS sisadelivery

			FROM tm_do_item b

			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
			INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch
			INNER JOIN tm_op f ON f.i_op=b.i_op

			".$do." ".$ibatal." ".$ftrans." ".$ff." ".$fc."

			ORDER BY f.i_op_code ASC, b.i_product ASC ";
		
		$query	= $db2->query($str_qry);
		
		if($query->num_rows()>0)	{
			return $result	= $query->result();
		}
	}

	function clistdobrg_t($d_do_first,$d_do_last,$kddo,$ftransfer,$ffaktur,$fcetak) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($d_do_first) && !empty($d_do_last) && !empty($kddo) ) {
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= " AND a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else if((!empty($d_do_first) && !empty($d_do_last)) && empty($kddo) ) {	
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= "";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else if((empty($d_do_first) || empty($d_do_last)) && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else if((strlen($d_do_first)=="" || strlen($d_do_last)=="") && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else {
			$ddo	= "";
			$do		= "";
			$ibatal	= " WHERE a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		}
		
		/*
		$str_qry	= "
				SELECT  a.d_do AS dop,
					a.i_do_code AS idocode,
					a.i_do AS ido,
					a.i_branch AS ibranch,
					e.e_branch_name AS ebranchname,
					b.i_op AS iop,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_deliver AS qty,
					b.n_residual AS sisadelivery
				
				FROM tm_do_item b
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$do." ".$ibatal." 
				ORDER BY a.i_do_code DESC ";
		*/
		
		$str_qry	= "
			SELECT  a.d_do AS ddo,
				a.i_do_code AS idocode,
				a.i_branch AS ibranch,
				e.e_branch_name AS ebranchname,
				a.f_do_cancel,
				a.f_faktur_created

			FROM tm_do_item b 

				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$ddo." ".$ibatal." ".$ftrans." ".$ff." ".$fc."
			
			GROUP BY a.d_do, a.i_do_code, a.i_branch, e.e_branch_name, a.f_do_cancel, a.f_faktur_created 
			
			ORDER BY a.i_do_code ASC ";
				
		return $db2->query($str_qry);
	}

	function viewperpages_t($limit,$offset,$d_do_first,$d_do_last,$kddo,$ftransfer,$ffaktur,$fcetak) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($d_do_first) && !empty($d_do_last) && !empty($kddo) ) {
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= " AND a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else if((!empty($d_do_first) && !empty($d_do_last)) && empty($kddo) ) {	
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= "";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else if((empty($d_do_first) || empty($d_do_last)) && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else if((strlen($d_do_first)=="" || strlen($d_do_last)=="") && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		} else {
			$ddo	= "";
			$do		= "";
			$ibatal	= " WHERE a.f_do_cancel='f' ";
			$ftrans	= " AND a.f_transfer='$ftransfer' ";
			$ff		= " AND a.f_faktur_created='$ffaktur' ";
			$fc		= " AND a.f_printed='$fcetak' ";
		}
		$str_qry	= " SELECT  a.d_do AS ddo,
				a.i_do_code AS idocode,
				a.i_branch AS ibranch,
				e.e_branch_name AS ebranchname,
				a.f_do_cancel,
				a.f_faktur_created
			
			FROM tm_do_item b
			
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$ddo." ".$ibatal." ".$ftrans." ".$ff." ".$fc."
			
			GROUP BY a.d_do, a.i_do_code, a.i_branch, e.e_branch_name, a.f_do_cancel, a.f_faktur_created 
			
			ORDER BY a.i_do_code ASC LIMIT ".$limit." OFFSET ".$offset." ";
			
		$query	= $db2->query($str_qry);
		
		if($query->num_rows()>0){
			return $result	= $query->result();
		}
	}
			
	function lbarangjadiperpages($limit,$offset){
		$db2=$this->load->database('db_external', TRUE);
		$query = $db2->query( "
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			WHERE a.f_do_cancel='f' 

			GROUP BY a.i_do, a.i_do_code, a.d_do

			ORDER BY a.d_do DESC LIMIT ".$limit." OFFSET ".$offset );

		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi(){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query( "
		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) WHERE a.f_do_cancel='f' 
			
			GROUP BY a.i_do, a.i_do_code, a.d_do
			
			ORDER BY a.d_do DESC LIMIT 1000 " );
	}	
	
	function flbarangjadi($key) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($key)) {
			return $db2->query( "			
				SELECT  a.i_do AS ido,
					a.i_do_code AS ido_code,
					a.d_do AS ddo
					
				FROM tm_do a 
				
				INNER JOIN tm_do_item b ON a.i_do=b.i_do
				
				INNER JOIN (SELECT d.i_product_motif AS pbase 
					FROM tr_product_base c 
					RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
				
				AS pbase ON trim(pbase)=trim(b.i_product) 
				
				WHERE (a.i_do_code LIKE '$key%' OR b.i_product LIKE '$key%') AND a.f_do_cancel=false
				
				GROUP BY a.i_do, a.i_do_code, a.d_do
				
				ORDER BY a.d_do DESC LIMIT 100 " );
		}	
	}
	
	/* Edit Session */
	function getdoheader($idocode) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_do WHERE i_do_code='$idocode' AND f_do_cancel='f' ");
	}

	function lpelanggan() {	
		$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$db2->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function getpelanggan($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_customer',$icustomer);
		$db2->order_by('i_do');
		return $db2->get('tm_do');
	}
	
	function lcabang($icustomer) {
		$db2=$this->load->database('db_external', TRUE);
		if(!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.i_branch AS codebranch, 
					a.i_branch_code AS ibranchcode,
				    a.i_customer AS codecustomer, 
					a.e_branch_name AS ebranchname,
				    a.e_branch_name AS branch 
					
					FROM tr_branch a 
					
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$filter." ".$order;
					
		$query	= $db2->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function getcabang($ibranch) {
		$db2=$this->load->database('db_external', TRUE);
		$db2->where('i_branch',$ibranch);
		$db2->order_by('i_do');
		return $db2->get('tm_do');
	}
		
	function ldoitem($ido) {
	$db2=$this->load->database('db_external', TRUE);
				/*
				SELECT	a.i_op_code AS op,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						b.n_residual AS qtyakhir,
						d.n_quantity AS qtyproduk,
						e.v_unitprice AS unitprice,
						(b.n_residual * e.v_unitprice) AS harga	
						*/
						
		/* Remark 20062011				
		$qstr	= "
			revisi : SELECT  a.d_do AS dop,
					a.i_do_code AS idocode,
					a.i_do AS ido,
					a.i_branch AS ibranch,
					b.i_op AS iopcode,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_deliver AS qty,
					f.n_quantity_akhir AS qtyproduk,
					d.v_unitprice AS unitprice,
					b.v_do_gross as harga 
					
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch 
				INNER JOIN tm_stokopname_item f ON trim(f.i_product)=trim(b.i_product)
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
				WHERE a.i_do='$ido' AND g.i_status_so='0'
					
				ORDER BY b.i_do_item DESC";
		*/
				
		/* Baru 20062011 */
		
		/* 26072011
		$qstr	= "
			SELECT a.d_do AS dop, 
				a.i_do_code AS idocode, 
				a.i_do AS ido, 
				a.i_branch AS ibranch, 
				h.i_op AS iop,
				b.i_op AS iopcode, 
				b.i_product AS iproduct, 
				b.e_product_name AS motifname, 
				b.n_deliver AS qty, 
				f.n_quantity_akhir AS qtyproduk, 
				(b.v_do_gross/b.n_deliver) AS unitprice, 
				b.v_do_gross as harga 
			
			FROM tm_do_item b 
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tm_stokopname_item f ON trim(f.i_product)=trim(b.i_product) 
			INNER JOIN tm_stokopname g ON g.i_so=f.i_so 
			INNER JOIN tm_op h ON h.i_op_code=cast(b.i_op AS character varying)
			WHERE a.i_do='$ido' AND g.i_status_so='0' AND a.f_do_cancel='f' ORDER BY b.i_do_item DESC	";
		*/

		$qstr	= "
			SELECT a.d_do AS dop,
				a.i_do_code AS idocode,
				a.i_do AS ido,
				a.i_branch AS ibranch,
				h.i_op_code AS iopcode,
				b.i_op AS iop,
				b.i_product AS iproduct,
				b.e_product_name AS motifname,
				b.n_deliver AS qty,
				f.n_quantity_akhir AS qtyproduk,
				(b.v_do_gross/b.n_deliver) AS unitprice,
				b.v_do_gross as harga,
				b.is_grosir,
				b.harga_grosir,
				b.is_adaboneka,
				g.f_stop_produksi AS stp,
				b.i_do_item
				
			FROM tm_do_item b 
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tm_stokopname_item f ON trim(f.i_product)=trim(b.i_product) 
			INNER JOIN tm_stokopname g ON g.i_so=f.i_so 
			INNER JOIN tm_op h ON h.i_op=b.i_op
			WHERE a.i_do='$ido' AND g.i_status_so='0' AND a.f_do_cancel='f' ORDER BY h.i_op_code ASC, b.i_product ASC ";
		
		$query	= $db2->query($qstr);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function cari_do($ndo,$codelama) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_do WHERE (i_do_code=trim('$ndo') AND i_do_code!=trim('$codelama')) AND f_do_cancel='f' ");
	}

	function mupdateXX($i_product_item,$n_deliver_item,$i_do,$i_do_code_hidden,$i_do_hidden,$nw_d_do,$i_customer_hidden,$i_branch_hidden,$i_op,$i_op_sebunyi,$i_product,$e_product_name,$n_deliver,$v_do_gross,$e_note,$iterasi,$qty_product,$qty_op,$f_stp,$fstp_arr) {
$db2=$this->load->database('db_external', TRUE);
		$back_qty_akhir	= array();
		
		//$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$iso	= $row_stokopname->i_so;
		}else{
			$iso	= "";
		}

		if($iso!="") {
		
			$j = 0;
			
			$qdo_item	= $db2->query(" SELECT i_product, n_deliver, i_op FROM tm_do_item WHERE i_do='$i_do_hidden' ");
			
			if($qdo_item->num_rows()>0) {
				
				$result_doitem	= $qdo_item->result();
				
				foreach($result_doitem as $row_doitem) {
					
					$qstokopname2	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$fstp_arr[$j]' ");
					
					if($qstokopname2->num_rows()>0) {
						
						$row_stokopname2= $qstokopname2->row();
						
						$iso2			= $row_stokopname2->i_so;
						
						$qstok_item	= $db2->query(" SELECT n_quantity_akhir FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
						
						if($qstok_item->num_rows()>0) {
							
							$row_stok_item	= $qstok_item->row();
							
							$back_qty_akhir[$j]	= (($row_stok_item->n_quantity_akhir)+($row_doitem->n_deliver));
							
							if($back_qty_akhir[$j]=='') {
								$back_qty_akhir[$j] = 0;
							}
							
							$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$j]' WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
						}
					}
				
					$qorder	= $db2->query(" SELECT i_op_item, n_residual, n_count FROM tm_op_item WHERE i_op='$row_doitem->i_op' AND i_product='$row_doitem->i_product' ");
					
					if($qorder->num_rows()>0) {
						
						$row_order	= $qorder->row();
						
						$nsisaawal	= (($row_order->n_residual)+($row_doitem->n_deliver));
						
						if($nsisaawal==($row_order->n_count)) {
							$fdo	= ", f_do_created='f' ";;
						}else{
							$fdo	= "";
						}
						
						$db2->query(" UPDATE tm_op_item SET n_residual='$nsisaawal' ".$fdo." WHERE i_op_item='$row_order->i_op_item' ");
					}
					$j+=1;
				}

				$db2->query(" DELETE FROM tm_do WHERE i_do='$i_do_hidden' AND f_do_cancel='f' ");
				$db2->query(" DELETE FROM tm_do_item WHERE i_do='$i_do_hidden' ");
							
			}
		
		}
		
		if(isset($iterasi)) {	
		
			$idoitem	= array();
			$tm_do_item	= array();
			$isox	= array();
			$tm_so	= array();
			$iopXXX	= array();
			$sisaorderawal	= array();
			
			$update_order	= 0;
			
			$db2->query(" INSERT INTO tm_do(i_do,i_do_code,i_customer,i_branch,d_do,d_entry,d_update) VALUES('$i_do_hidden','$i_do','$i_customer_hidden','$i_branch_hidden','$nw_d_do','$dentry','$dentry') ");
			
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++) {

				$seq_tm_do_item	= $db2->query(" SELECT cast(i_do_item AS integer)+1 AS i_do_item FROM tm_do_item ORDER BY cast(i_do_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_do_item->num_rows()>0){
					$seqrow	= $seq_tm_do_item->row();
					$idoitem[$jumlah]	= $seqrow->i_do_item;
				}else{
					$idoitem[$jumlah]	= 1;
				}
				
				$qstokopname2	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				
				if($qstokopname2->num_rows()>0) {
					
					$row_stokopname2= $qstokopname2->row();
					
					$iso2	= $row_stokopname2->i_so;
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
					
					if($qstok_item->num_rows()>0) {
						
						$row_stok_item	= $qstok_item->row();
						
						$kurangi	= (($row_stok_item->n_quantity_akhir)-($n_deliver[$jumlah]));
						
						if($kurangi=='') {
							$kurangi = 0;
						}
							
						$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$kurangi' WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
					}
				}
				
				$tm_do_item[$jumlah]	= array(
					 'i_do_item'=>$idoitem[$jumlah],
					 'i_do'=>$i_do_hidden,
					 'i_op'=>$i_op_sebunyi[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_deliver'=>$n_deliver[$jumlah],
					 'n_residual'=>$n_deliver[$jumlah], 
					 'v_do_gross'=>$v_do_gross[$jumlah],
					 'e_note'=>$e_note[$jumlah],
					 'd_entry'=>$dentry);		
	 			
				if($db2->insert('tm_do_item',$tm_do_item[$jumlah])) {	
					
					$q_qty_op_item	= $db2->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					
					if($q_qty_op_item->num_rows()>0) {
						
						$row_item_br	= $q_qty_op_item->row();
						
						if($n_deliver[$jumlah]==($row_item_br->n_residual)) {
							
							$qty_akhirX	= ($row_item_br->n_residual) - $n_deliver[$jumlah];
							
							if($qty_akhirX=='') {
								$qty_akhirX = 0;
							}
							
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");

							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
													
						}elseif($n_deliver[$jumlah] < ($row_item_br->n_residual)) {
						
							$qty_akhirX	= ($row_item_br->n_residual) - $n_deliver[$jumlah];
							
							if($qty_akhirX=='') {
								$qty_akhirX = 0;
							}
							
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");

							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX', f_do_created='f' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
								
						}else{
							
							$qty_akhirX	= ($row_item_br->n_residual) - $n_deliver[$jumlah];
							
							if($qty_akhirX=='') {
								$qty_akhirX = 0;
							}
							
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
							
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}
					
					}
					
					/*** 15022012					
					$get_tmsox	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.d_do='$nw_d_do' ");					
					
					if($get_tmsox->num_rows() > 0 ) {
						
						$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						
						if($get_tmso2->num_rows() > 0 ) {
							$row_tmso2	= $get_tmso2->row();
							$isox[$jumlah]	= $row_tmso2->iso+1;
						}else{
							$isox[$jumlah]	= 1;
						}

						$row_tmso	= $get_tmsox->row_array();

						if($row_tmso['n_saldo_akhir']<0 || $row_tmso['n_saldo_akhir']==''){
							$n_saldo_akhir	= 0;
						}else{
							$n_saldo_akhir	= $row_tmso['n_saldo_akhir'];
						}
													
						$saldo_sisa[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_deliver[$jumlah];
						
						$db2->query(" INSERT INTO tm_so (i_so,i_product,i_product_motif,e_product_motifname,i_status_do,d_do,n_saldo_awal,n_saldo_akhir,d_entry) VALUES('$isox[$jumlah]','$row_tmso[i_product]','$row_tmso[i_product_motif]','$row_tmso[e_product_motifname]','$row_tmso[i_status_do]','$row_tmso[d_do]','$n_saldo_akhir','$saldo_sisa[$jumlah]','$dentry') ");
						
					} else {
						$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						
						if($get_tmso2->num_rows() > 0 ) {
							$row_tmso2	= $get_tmso2->row();
							$isox[$jumlah]	= $row_tmso2->iso+1;
						}else{
							$isox[$jumlah]	= 1;
						}
						
						$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
						
						if($get_tmso->num_rows()>0) {
							
							$row_tmso	= $get_tmso->row_array();
							
							if($row_tmso['n_saldo_akhir']<0 || $row_tmso['n_saldo_akhir']==''){
								$n_saldo_akhir	= 0;
							}else{
								$n_saldo_akhir	= $row_tmso['n_saldo_akhir'];
							}
							
							$saldo_sisa[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_deliver[$jumlah];
							
							$db2->query(" INSERT INTO tm_so(i_so,i_product,i_product_motif,e_product_motifname,i_status_do,d_do,n_saldo_awal,n_saldo_akhir,d_entry) VALUES('$isox[$jumlah]','$row_tmso[i_product]','$row_tmso[i_product_motif]','$row_tmso[e_product_motifname]','1','$nw_d_do','$n_saldo_akhir','$saldo_sisa[$jumlah]','$dentry') ");
						}else{
							echo "tm_do & tm_do_item gagal diupdate!";
						}
					}
					***/
				}else{
					echo "";
				}
				/*
				if ($db2->trans_status()===FALSE) {
					$db2->trans_rollback();
				} else {
					$db2->trans_commit();
				}
				*/ 
			}
		}else{
			print "<script>alert(\"Maaf, DO gagal diupdate. Terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
		
			
 		}
		
		$qdo	= $db2->query(" SELECT * FROM tm_do WHERE i_do='$i_do_hidden' AND f_do_cancel='f' ");
		if($qdo->num_rows()==0) {
			print "<script>alert(\"Maaf, DO gagal diupdate, kesalahan pd saat input Master Motif Brg.\");
			window.open(\"index\", \"_self\");</script>";
		
			
		} else {
			print "<script>alert(\"Nomor DO : '\"+$i_do_code_hidden+\"' telah diupdate, terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
		
		}
	}

	function mupdate($i_product_item,$n_deliver_item,$i_do,$i_do_code_hidden,$i_do_hidden,$nw_d_do,
			$i_customer_hidden,$i_branch_hidden,$i_op,$i_op_sebunyi,$i_product,$e_product_name,$n_deliver,$v_do_gross,
			$e_note,$iterasi,$qty_product,$qty_op,$f_stp,$fstp_arr, $is_grosir, $harga_grosir, $adaboneka,
			$qty_warna, $i_color) {
$db2=$this->load->database('db_external', TRUE);
		$back_qty_akhir	= array();
		
		$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$iso	= $row_stokopname->i_so;
		}else{
			$iso	= "";
		}

		if($iso!="") {
		
			$j = 0;
			
			$qdo_item	= $db2->query(" SELECT * FROM tm_do_item WHERE i_do='$i_do_hidden' ");
			if($qdo_item->num_rows()>0) {
				
				$result_doitem	= $qdo_item->result();
				
				foreach($result_doitem as $row_doitem) {
					// 09-10-2014
					
					$qstokopname2	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$fstp_arr[$j]' ORDER BY i_so DESC LIMIT 1");
					
					$row_stokopname2= $qstokopname2->row();
					$iso2			= $row_stokopname2->i_so;
					
					// ambil data2 per warna, reset stoknya -------------------------------
					$qdo_item_color	= $db2->query(" SELECT * FROM tm_do_item_color 
											WHERE i_do_item='$row_doitem->i_do_item' ");
					
					if($qdo_item_color->num_rows()>0) {
						$result_doitem_color	= $qdo_item_color->result();
						foreach($result_doitem_color as $rowxx) {
							/*if ($rowxx->i_color != '0') {
								$datacolor = " AND a.i_color = '".$rowxx->i_color."' ";
								$datacolor2 = $datacolor;
								$datacolor3 = " AND i_color = '".$rowxx->i_color."' ";
							}
							else {
								$qxx = $db2->query(" SELECT i_color FROM tr_product_color WHERE i_product_color='$rowxx->i_product_color' ");
								if($qxx->num_rows()>0) {
									$rxx	= $qxx->row();
									$datacolor2	= " AND a.i_color= '".$rxx->i_color."' ";
									$datacolor3 = " AND i_color= '".$rxx->i_color."' ";
								}
							} */
							/*echo "SELECT a.*, b.i_so_item FROM tm_stokopname_item_color a
										INNER JOIN tm_stokopname_item b ON a.i_so_item = b.i_so_item
										WHERE b.i_so='$iso2' AND b.i_product='".$row_doitem->i_product."' 
										AND a.i_color='".$rowxx->i_color."'"; die(); */
							$qsowarna	= $db2->query(" SELECT a.*, b.i_so_item FROM tm_stokopname_item_color a
										INNER JOIN tm_stokopname_item b ON a.i_so_item = b.i_so_item
										WHERE b.i_so='$iso2' AND b.i_product='".$row_doitem->i_product."' 
										AND a.i_color='".$rowxx->i_color."' ");
							if($qsowarna->num_rows()>0) {
								$rowsowarna	= $qsowarna->row();
								$qtyawalwarna	= $rowsowarna->n_quantity_awal;
								$qtyakhirwarna	= $rowsowarna->n_quantity_akhir;
								
								$back_qty_awalwarna	= $qtyawalwarna;
								$back_qty_akhirwarna	= $qtyakhirwarna+$rowxx->qty;
								
								if($back_qty_akhirwarna=='')
									$back_qty_akhirwarna = 0;
								
								$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir='$back_qty_akhirwarna'
												WHERE i_so_item = '$rowsowarna->i_so_item' AND i_color = '".$rowxx->i_color."' ");
							}else{
								$qtyawalwarna	= 0;
								$qtyakhirwarna	= 0;
							}
						} // end foreach warna
					} // end if per warna
					//-----------------------------------------------------------------------
					
					// hapus do_item_color after reset stok 06-09-2014
					$db2->query(" DELETE FROM tm_do_item_color WHERE i_do_item='".$row_doitem->i_do_item."' ");	
					// ------------------------------------------
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$back_qty_akhir[$j]	= (($row_stok_item->n_quantity_akhir)+($row_doitem->n_deliver));
						if($back_qty_akhir[$j]=='') {
							$back_qty_akhir[$j] = 0;
						}			
						$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$j]' WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
					}
					
					$qorder	= $db2->query(" SELECT * FROM tm_op_item WHERE i_op='$row_doitem->i_op' AND i_product='$row_doitem->i_product' ORDER BY i_op_item ASC ");
					
					if($qorder->num_rows()>0) {
						$row_order	= $qorder->row();
						$nsisaawal	= (($row_order->n_residual)+($row_doitem->n_deliver));
						if($nsisaawal==($row_order->n_count)){
							$fdo	= ", f_do_created='f' ";;
						}else{
							$fdo	= "";
						}	
						$db2->query(" UPDATE tm_op_item SET n_residual='$nsisaawal' ".$fdo." WHERE i_op_item='$row_order->i_op_item' ");
					}
					$j+=1;
				}
			}
			
			$db2->query(" DELETE FROM tm_do WHERE i_do='$i_do_hidden' AND f_do_cancel='f' ");
			$db2->query(" DELETE FROM tm_do_item WHERE i_do='$i_do_hidden' ");
		}
		
		if(isset($iterasi)) {	
		
			$idoitem	= array();
			$tm_do_item	= array();
			$isox	= array();
			$tm_so	= array();
			
			$iopXXX	= array();
			$qty_akhirX		= array();
			$sisaorderawal	= array();
			
			$db2->query(" INSERT INTO tm_do(i_do,i_do_code,i_customer,i_branch,d_do,d_entry,d_update) VALUES('$i_do_hidden','$i_do','$i_customer_hidden','$i_branch_hidden','$nw_d_do','$dentry','$dentry') ");
			
			for($jumlah=1;$jumlah<=$iterasi;$jumlah++) {

				$qstokopname2	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				$row_stokopname2= $qstokopname2->row();
				$iso2	= $row_stokopname2->i_so;
				
				$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
				if($qstok_item->num_rows()>0) {
					$row_stok_item	= $qstok_item->row();
					// 09-10-2014
					$i_so_item = $row_stok_item->i_so_item;
					
					$kurangi	= (($row_stok_item->n_quantity_akhir)-($n_deliver[$jumlah]));
					
					if($kurangi=='')
						$kurangi = 0;
						
					$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$kurangi' WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
				}
				
				$seq_tm_do_item	= $db2->query(" SELECT cast(i_do_item AS integer)+1 AS i_do_item FROM tm_do_item ORDER BY cast(i_do_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_do_item->num_rows()>0){
					$seqrow	= $seq_tm_do_item->row();
					$idoitem[$jumlah]	= $seqrow->i_do_item;
				} else {
					$idoitem[$jumlah]	= 1;
				}
				
				// 27-06-2012
				if ($is_grosir[$jumlah] == '')
					$is_grosir[$jumlah] = 'f';
					
				$tm_do_item[$jumlah]	= array(
					 'i_do_item'=>$idoitem[$jumlah],
					 'i_do'=>$i_do_hidden,
					 'i_op'=>$i_op_sebunyi[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_deliver'=>$n_deliver[$jumlah],
					 'n_residual'=>$n_deliver[$jumlah], 
					 'v_do_gross'=>$v_do_gross[$jumlah],
					 'e_note'=>$e_note[$jumlah],
					 'd_entry'=>$dentry,
					 'is_grosir'=>$is_grosir[$jumlah],
					 'harga_grosir'=>$harga_grosir[$jumlah],
					 'is_adaboneka'=>$adaboneka[$jumlah]);		
	 			
				if($db2->insert('tm_do_item',$tm_do_item[$jumlah])) {
					
					// 06-09-2014
					// ------------------------------------------
					for ($xx=0; $xx<count($i_color[$jumlah]); $xx++) {
					//foreach ($i_product_color[$jumlah] as $row1) {
						
						//if (trim($row1) != '') {
							$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
							$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
							
							$seq_tm_do_item_color	= $db2->query(" SELECT i_do_item_color FROM tm_do_item_color 
														ORDER BY i_do_item_color DESC LIMIT 1 ");
						
							if($seq_tm_do_item_color->num_rows() > 0) {
								$seqrow	= $seq_tm_do_item_color->row();
								$i_do_item_color[$jumlah]	= $seqrow->i_do_item_color+1;
							}else{
								$i_do_item_color[$jumlah]	= 1;
							}

							$tm_do_item_color[$jumlah]	= array(
								 'i_do_item_color'=>$i_do_item_color[$jumlah],
								 'i_do_item'=>$idoitem[$jumlah],
								 'i_color'=>$i_color[$jumlah][$xx],
								 'qty'=>$qty_warna[$jumlah][$xx]
							);
							$db2->insert('tm_do_item_color',$tm_do_item_color[$jumlah]);
							
							// 09-10-2014, update stok per warna
							$sqlwarna = " SELECT * FROM tm_stokopname_item_color WHERE i_so_item = '$i_so_item'
											AND i_color = '".$i_color[$jumlah][$xx]."' ";
							$querywarna = $db2->query($sqlwarna);
								
							if($querywarna->num_rows() > 0) {
								$hasilwarna = $querywarna->row();
								
								$qtyawalwarna	= $hasilwarna->n_quantity_awal;
								$qtyakhirwarna	= $hasilwarna->n_quantity_akhir;

								$back_qty_awalwarna	= $qtyawalwarna;
								$back_qty_akhirwarna	= $qtyakhirwarna-$qty_warna[$jumlah][$xx];
									
								if($back_qty_akhirwarna=="")
									$back_qty_akhirwarna	= 0;
																		
								$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir = '$back_qty_akhirwarna'
													WHERE i_so_item = '$i_so_item' AND i_color = '".$i_color[$jumlah][$xx]."' ");
									
							} // end if
							
						//} // end if
					} // end for
					// ------------------------------------------
					
					$q_qty_op_item	= $db2->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					
					if($q_qty_op_item->num_rows()>0) {
						
						$row_item_br	= $q_qty_op_item->row();
						
						if($n_deliver[$jumlah]==($row_item_br->n_residual)) {
							
							$qty_akhirX[$jumlah]	= (($row_item_br->n_residual) - $n_deliver[$jumlah]);
							
							if($qty_akhirX[$jumlah]=='')
								$qty_akhirX[$jumlah] = 0;
							
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
							
						}elseif($n_deliver[$jumlah] < ($row_item_br->n_residual)) {
							$qty_akhirX[$jumlah]	= (($row_item_br->n_residual) - $n_deliver[$jumlah]);
							
							if($qty_akhirX[$jumlah]=='')
								$qty_akhirX[$jumlah] = 0;
								
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='f' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
									
						}else{
							$qty_akhirX[$jumlah]	= (($row_item_br->n_residual) - $n_deliver[$jumlah]);
							
							if($qty_akhirX[$jumlah]=='')
								$qty_akhirX[$jumlah] = 0;

							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}												
					}				
				}else{
				}
			}

			if ($db2->trans_status()===FALSE || $db2->trans_status()==false) {
				$ok = 0;
				$db2->trans_rollback();
			}else{
				$ok = 1;
				$db2->trans_commit();
			}
							
		}else{
			print "<script>alert(\"Maaf, DO gagal diupdate. Terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
		
			
 		}
		
		if($ok==0) {
			print "<script>alert(\"Maaf, DO gagal diupdate. Terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
		
		}else{
			print "<script>alert(\"Nomor DO : '\"+$i_do_code_hidden+\"' telah diupdate, terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
		
		}
	}


	function mupdate_old($i_product_item,$n_deliver_item,$i_do,$i_do_code_hidden,$i_do_hidden,$nw_d_do,$i_customer_hidden,$i_branch_hidden,$i_op,$i_op_sebunyi,$i_product,$e_product_name,$n_deliver,$v_do_gross,$e_note,$iterasi,$qty_product,$qty_op,$f_stp,$fstp_arr) {
$db2=$this->load->database('db_external', TRUE);
		$back_qty_akhir	= array();
		
		$db2->trans_begin();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$iso	= $row_stokopname->i_so;
		} else {
			$iso	= "";
		}

		if($iso!="") {
		
			$j = 0;
			
			$qdo_item	= $db2->query(" SELECT * FROM tm_do_item WHERE i_do='$i_do_hidden' ");
			if($qdo_item->num_rows()>0) {
				
				$result_doitem	= $qdo_item->result();
				
				foreach($result_doitem as $row_doitem) {
					
					$qstokopname2	= $db2->query(" SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$fstp_arr[$j]' ORDER BY i_so DESC LIMIT 1");
					
					$row_stokopname2= $qstokopname2->row();
					$iso2			= $row_stokopname2->i_so;
					
					$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$back_qty_akhir[$j]	= (($row_stok_item->n_quantity_akhir)+($row_doitem->n_deliver));
						if($back_qty_akhir[$j]=='') {
							$back_qty_akhir[$j] = 0;
						}			
						$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$j]' WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
					}
					
					$qorder	= $db2->query(" SELECT * FROM tm_op_item WHERE i_op='$row_doitem->i_op' AND i_product='$row_doitem->i_product' ORDER BY i_op_item ASC ");
					
					if($qorder->num_rows()>0) {
						$row_order	= $qorder->row();
						$nsisaawal	= (($row_order->n_residual)+($row_doitem->n_deliver));
						if($nsisaawal==($row_order->n_count)){
							$fdo	= ", f_do_created='f' ";;
						}else{
							$fdo	= "";
						}	
						$db2->query(" UPDATE tm_op_item SET n_residual='$nsisaawal' ".$fdo." WHERE i_op_item='$row_order->i_op_item' ");
					}
					$j+=1;
				}
			}
			
			$db2->query(" DELETE FROM tm_do WHERE i_do='$i_do_hidden' AND f_do_cancel='f' ");
			$db2->query(" DELETE FROM tm_do_item WHERE i_do='$i_do_hidden' ");
		}
		
		if(isset($iterasi)) {	
		
			$idoitem	= array();
			$tm_do_item	= array();
			$isox	= array();
			$tm_so	= array();
			
			$iopXXX	= array();
			
			$jml_item_brX	= array();
			$qty_akhirX		= array();
			
			$update_order	= 0;
			
			$sisaorderawal	= array();
			
			$db2->query(" INSERT INTO tm_do(i_do,i_do_code,i_customer,i_branch,d_do,d_entry,d_update) VALUES('$i_do_hidden','$i_do','$i_customer_hidden','$i_branch_hidden','$nw_d_do','$dentry','$dentry') ");
			
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++){

				$qstokopname2	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				$row_stokopname2= $qstokopname2->row();
				$iso2	= $row_stokopname2->i_so;
				
				$qstok_item	= $db2->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
				if($qstok_item->num_rows()>0) {
					$row_stok_item	= $qstok_item->row();
					$kurangi	= (($row_stok_item->n_quantity_akhir)-($n_deliver[$jumlah]));
					if($kurangi=='')
						$kurangi = 0;
						
					$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$kurangi' WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
				}
												
				$seq_tm_do_item	= $db2->query(" SELECT cast(i_do_item AS integer)+1 AS i_do_item FROM tm_do_item ORDER BY cast(i_do_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_do_item->num_rows()>0){
					$seqrow	= $seq_tm_do_item->row();
					$idoitem[$jumlah]	= $seqrow->i_do_item;
				} else {
					$idoitem[$jumlah]	= 1;
				}
				
				$tm_do_item[$jumlah]	= array(
					 'i_do_item'=>$idoitem[$jumlah],
					 'i_do'=>$i_do_hidden,
					 'i_op'=>$i_op_sebunyi[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_deliver'=>$n_deliver[$jumlah],
					 'n_residual'=>$n_deliver[$jumlah], 
					 'v_do_gross'=>$v_do_gross[$jumlah],
					 'e_note'=>$e_note[$jumlah],
					 'd_entry'=>$dentry);		
	 			
				if($db2->insert('tm_do_item',$tm_do_item[$jumlah])) {	
					
					$q_qty_op_item	= $db2->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					
					if($q_qty_op_item->num_rows()>0) {
						
						$row_item_br	= $q_qty_op_item->row();
						$jml_item_brX[$jumlah]	= $row_item_br->n_residual;
						
						if($n_deliver[$jumlah]==$jml_item_brX[$jumlah]) {
							
							$qty_akhirX[$jumlah]	= $jml_item_brX[$jumlah] - $n_deliver[$jumlah];
							
							if($qty_akhirX[$jumlah]=='')
								$qty_akhirX[$jumlah] = 0;
							
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");

							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
													
						} else if($n_deliver[$jumlah] < $jml_item_brX[$jumlah]) { // jika jmlitem kurang dari qty brg yg ada
							$qty_akhirX[$jumlah]	= $jml_item_brX[$jumlah] - $n_deliver[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
							
							if($qty_akhirX[$jumlah]=='')
								$qty_akhirX[$jumlah] = 0;
								
							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");

							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='f' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
									
						} else {
							$qty_akhirX[$jumlah]	= $jml_item_brX[$jumlah] - $n_deliver[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
							
							if($qty_akhirX[$jumlah]=='')
								$qty_akhirX[$jumlah] = 0;

							$db2->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND f_do_created='f' ");
							
							$db2->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}
															
					}
										
					$get_tmsox	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.d_do='$nw_d_do' ");					

					$get_tmso2	= $db2->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						
					if($get_tmso2->num_rows()>0) {
						$row_tmso2	= $get_tmso2->row();
						$isox[$jumlah]	= $row_tmso2->iso+1;
					} else {
						$isox[$jumlah]	= 1;
					}
											
					if($get_tmsox->num_rows()>0) {						
						$row_tmso	= $get_tmsox->row_array();

						if($row_tmso['n_saldo_akhir']<0 || $row_tmso['n_saldo_akhir']==''){
							$n_saldo_akhir	= 0;
						}else{
							$n_saldo_akhir	= $row_tmso['n_saldo_akhir'];
						}
													
						$saldo_sisa[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_deliver[$jumlah];
						
						$db2->query(" INSERT INTO tm_so (i_so,i_product,i_product_motif,e_product_motifname,i_status_do,d_do,n_saldo_awal,n_saldo_akhir,d_entry) VALUES('$isox[$jumlah]','$row_tmso[i_product]','$row_tmso[i_product_motif]','$row_tmso[e_product_motifname]','$row_tmso[i_status_do]','$row_tmso[d_do]','$n_saldo_akhir','$saldo_sisa[$jumlah]','$dentry') ");
						
					}else{
						$get_tmso	= $db2->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");					
						if($get_tmso->num_rows()>0) {							
							$row_tmso	= $get_tmso->row_array();
							
							if($row_tmso['n_saldo_akhir']<0 || $row_tmso['n_saldo_akhir']==''){
								$n_saldo_akhir	= 0;
							}else{
								$n_saldo_akhir	= $row_tmso['n_saldo_akhir'];
							}
							
							$saldo_sisa[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_deliver[$jumlah];
							
							$db2->query(" INSERT INTO tm_so(i_so,i_product,i_product_motif,e_product_motifname,i_status_do,d_do,n_saldo_awal,n_saldo_akhir,d_entry) VALUES('$isox[$jumlah]','$row_tmso[i_product]','$row_tmso[i_product_motif]','$row_tmso[e_product_motifname]','1','$nw_d_do','$n_saldo_akhir','$saldo_sisa[$jumlah]','$dentry') ");
						} else {
							//echo "tm_do & tm_do_item gagal diupdate!";
						}
					}
					
				} else {
					//echo "tm_do gagal diupdate!";
				}
			}

			if ($db2->trans_status()===FALSE) {
				$db2->trans_rollback();
			} else {
				$db2->trans_commit();
			}
							
		} else {
			print "<script>alert(\"Maaf, DO gagal diupdate. Terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
 		}
		
		$qdo	= $db2->query(" SELECT * FROM tm_do WHERE i_do='$i_do_hidden' AND f_do_cancel='f' ");
		if($qdo->num_rows()<0 || $qdo->num_rows()==0) {
			print "<script>alert(\"Maaf, DO gagal diupdate. Terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Nomor DO : '\"+$i_do_code_hidden+\"' telah diupdate, terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
		}
	}
	
			
	function mbatal($idocode) {
	$db2=$this->load->database('db_external', TRUE);
		if(!empty($idocode)) {
		
			$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
			if($qstokopname->num_rows()>0) {
				$row_stokopname	= $qstokopname->row();
				$iso	= $row_stokopname->i_so;
			}else{
				$iso	= "";
			}
			
			$qcariidocode	= $db2->query(" SELECT * FROM tm_do WHERE i_do_code='$idocode' AND f_do_cancel='f' ORDER BY i_do DESC LIMIT 1 ");
			if($qcariidocode->num_rows()>0) {
				
				$row_idocode	= $qcariidocode->row();
				
				$idox	= $row_idocode->i_do;
				
				$tbl_do	= array(
					'f_do_cancel'=>'TRUE'
				);
				
				if($db2->update('tm_do',$tbl_do,array('i_do_code'=>$idocode,'f_do_cancel'=>'f'))) {
				
					$j=0;
					
					$iproductmotif	= array();
					$qtyakhir	= array();
					$back_qty_order	= array();
					
					$qdo_item	= $db2->query(" SELECT * FROM tm_do_item WHERE i_do='$idox'  ");
					
					if($qdo_item->num_rows()>0) {

						$result_doitem	= $qdo_item->result();
						
						foreach($result_doitem as $row_doitem) {
							// ------------------------------------------
							$iproductmotif[$j]	= $row_doitem->i_product;
							
							$qstok_item	= $db2->query(" SELECT a.n_quantity_awal, a.n_quantity_akhir, b.i_so, 
										b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b 
										ON b.i_so=a.i_so WHERE b.i_status_so='0' AND a.i_product='$iproductmotif[$j]' 
										ORDER BY b.i_so DESC LIMIT 1 ");
							
							if($qstok_item->num_rows()>0) {
								$row_stok_item	= $qstok_item->row();
								$isonya = $row_stok_item->i_so;
							}
							else
								$isonya = 0;
							
							// ------------------------------ START WARNA -------------------------------------------
							$qdo_item_color	= $db2->query(" SELECT * FROM tm_do_item_color 
											WHERE i_do_item='$row_doitem->i_do_item' ");
					
							if($qdo_item_color->num_rows()>0) {
								$result_doitem_color	= $qdo_item_color->result();
								foreach($result_doitem_color as $rowxx) {
									$qsowarna	= $db2->query(" SELECT a.*, b.i_so_item FROM tm_stokopname_item_color a
												INNER JOIN tm_stokopname_item b ON a.i_so_item = b.i_so_item
												WHERE b.i_so='$isonya' AND b.i_product='".$row_doitem->i_product."' 
												AND a.i_color='".$rowxx->i_color."' ");
									if($qsowarna->num_rows()>0) {
										$rowsowarna	= $qsowarna->row();
										$qtyawalwarna	= $rowsowarna->n_quantity_awal;
										$qtyakhirwarna	= $rowsowarna->n_quantity_akhir;
										
										$back_qty_awalwarna	= $qtyawalwarna;
										$back_qty_akhirwarna	= $qtyakhirwarna+$rowxx->qty;
										
										if($back_qty_akhirwarna=='')
											$back_qty_akhirwarna = 0;
										
										$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir='$back_qty_akhirwarna'
														WHERE i_so_item = '$rowsowarna->i_so_item' AND i_color = '".$rowxx->i_color."' ");
									}else{
										$qtyawalwarna	= 0;
										$qtyakhirwarna	= 0;
									}
								} // end foreach warna
							} // end if per warna
							
							// hapus do_item_color 06-09-2014
							$db2->query(" DELETE FROM tm_do_item_color WHERE i_do_item='".$row_doitem->i_do_item."' ");	
							// ------------------------------------------
							
							// -------------------------------------- END WARNA -------------------------------------
							
							$back_qty_akhir[$j]	= (($row_stok_item->n_quantity_akhir)+($row_doitem->n_deliver));
							
							if($back_qty_akhir[$j]=='') {
								$back_qty_akhir[$j] = 0;
							}
						
							$qsisa	= $db2->query(" SELECT * FROM tm_op_item WHERE i_product='$iproductmotif[$j]' AND i_op='$row_doitem->i_op' ");
							$rowsisa	= $qsisa->row();
							
							$nresidual	= $rowsisa->n_residual;
							if($nresidual==''){
								$nresidual = 0;
							}
							
							$back_qty_order[$j]	= $nresidual+($row_doitem->n_deliver);
							
							if($back_qty_order[$j]==''){
								$back_qty_order[$j] = 0;
							}
							
							$db2->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$j]' WHERE i_so='$row_stok_item->i_so' AND i_product='$iproductmotif[$j]' ");
							
							$db2->query(" UPDATE tm_op SET f_do_created='f' WHERE i_op='$row_doitem->i_op' AND f_do_created='t' ");
							$db2->query(" UPDATE tm_op_item SET f_do_created='f', n_residual='$back_qty_order[$j]' WHERE i_product='$iproductmotif[$j]' AND i_op='$row_doitem->i_op' ");
							
							$j+=1;
						}
					}
				
				}
				print "<script>alert(\"Nomor DO : '\"+$idocode+\"' telah dibatalkan, terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
			} else {
				print "<script>alert(\"Maaf, DO tdk dpt dibatalkan.\");show(\"listdo/cform\",\"#content\");</script>";
			}
		} else {
			print "<script>alert(\"Maaf, DO tdk dpt dibatalkan.\");show(\"listdo/cform\",\"#content\");</script>";		
		}
	}

	function lop($ibranch,$icust) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT	a.i_op AS iop, a.i_op_code AS iopcode,
				cast(trim(b.i_product) AS character varying) AS iproduct,
				d.e_product_motifname AS productname,
				b.n_count AS qtyakhir,
				f.n_quantity_akhir AS qtyproduk,
				e.v_unitprice AS unitprice,
				e.harga_grosir AS harga_grosir,
				(b.n_count * e.v_unitprice) AS harga,
				g.f_stop_produksi AS stp
				
				FROM tm_op a
				
				LEFT JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname_item f ON trim(f.i_product)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
				WHERE  a.i_branch='$ibranch' AND a.i_customer='$icust' AND a.f_op_cancel='f' AND g.i_status_so='0'
				
				ORDER BY a.d_op DESC, a.i_op_code DESC ");
	}

	function lopperpages($ibranch,$icust,$limit,$offset) {
		$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT	a.i_op AS iop, a.i_op_code AS iopcode,
							cast(trim(b.i_product) AS character varying) AS iproduct,
							d.e_product_motifname AS productname,
							b.n_count AS qtyakhir,
							f.n_quantity_akhir AS qtyproduk,
							e.v_unitprice AS unitprice,
							e.harga_grosir AS harga_grosir,
							(b.n_count * e.v_unitprice) AS harga,
							g.f_stop_produksi AS stp
										
							FROM tm_op a
										
							LEFT JOIN tm_op_item b ON a.i_op=b.i_op
							INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
							INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
							INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
							INNER JOIN tm_stokopname_item f ON trim(f.i_product)=cast(trim(b.i_product) AS character varying)
							INNER JOIN tm_stokopname g ON g.i_so=f.i_so
							WHERE a.i_branch='$ibranch' AND a.i_customer='$icust' AND a.f_op_cancel='f' AND g.i_status_so='0'
										
							ORDER BY a.d_op DESC, a.i_op_code DESC "." LIMIT ".$limit." OFFSET ".$offset);
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function flop($key) {
		$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		
		return $db2->query("
				SELECT	a.i_op AS iop, a.i_op_code AS iopcode,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qtyakhir,
						f.n_quantity_akhir AS qtyproduk,
						e.v_unitprice AS unitprice,
						(b.n_count * e.v_unitprice) AS harga,
						g.f_stop_produksi AS stp
				
				FROM tm_op a
				
				LEFT JOIN tm_op_item b ON a.i_op=b.i_op
				INNER JOIN tr_product_motif d ON trim(d.i_product_motif)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tr_product_base e ON trim(e.i_product_base)=trim(d.i_product) 
				INNER JOIN (SELECT d.i_product_motif AS pbase FROM tr_product_base c RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product)) as pbase ON trim(pbase)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname_item f ON trim(f.i_product)=cast(trim(b.i_product) AS character varying)
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
											
				WHERE (a.i_op_code='$ky_upper' OR b.i_product='$ky_upper') AND a.f_op_cancel='f' AND g.i_status_so='0'
				
				ORDER BY a.d_op DESC, a.i_op_code DESC ");			
	}	

	function cari_tm_so($iproduct){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_so WHERE i_product_motif='$iproduct' ");
	}	
	
	function cari_qtyorder($iop,$iproduct){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_op_item WHERE i_op='$iop' AND i_product='$iproduct' ");	
	}
}
?>
