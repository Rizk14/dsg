<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends Model {

	function __construct() { 
		parent::Model();
	}
	
	function jmlDO($idocode){
		return	$this->db->query(" SELECT * FROM tm_do_item a INNER JOIN tm_do b ON a.i_do=b.i_do WHERE b.i_do_code='$idocode' AND b.f_do_cancel='f' ");
	}
		
	function cekstatusfaktur($idocode) {
		return $this->db->query("
			SELECT  a.d_do,
				a.i_do_code
					
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch 

				WHERE a.f_faktur_created='t' AND a.i_do_code='$idocode'
				GROUP BY a.d_do, a.i_do_code 
				
				ORDER BY a.i_do_code DESC ");	
	}
	
	function clistdobrg($d_do_first,$d_do_last,$kddo) {
		if(!empty($kddo)) {
			$do	= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
		}else{
			$do	= "";
			$ibatal	= " WHERE a.f_do_cancel='f' ";
		}
		
		/* 27072011		
		$str_qry	= "
				SELECT  a.d_do AS dop,
					a.i_do_code AS idocode,
					a.i_do AS ido,
					a.i_branch AS ibranch,
					e.e_branch_name AS ebranchname,
					b.i_op AS iop,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_deliver AS qty,
					b.n_residual AS sisadelivery
				
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$do." ".$ibatal." 				
				ORDER BY b.i_product ASC ";
		*/
		
		$str_qry	= "
		SELECT a.d_do AS dop,
			a.i_do_code AS idocode,
			a.i_do AS ido,
			a.i_branch AS ibranch, 
			e.e_branch_name AS ebranchname,
			b.i_op AS iop,
			f.i_op_code AS iopcode,
			b.i_product AS iproduct, 
			c.e_product_motifname AS motifname,
			b.n_deliver AS qty,
			b.n_residual AS sisadelivery

			FROM tm_do_item b

			RIGHT JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
			INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch
			INNER JOIN tm_op f ON f.i_op=b.i_op

			".$do." ".$ibatal."

			ORDER BY b.i_product ASC ";
		
		$query	= $this->db->query($str_qry);
		
		if($query->num_rows()>0)	{
			return $result	= $query->result();
		}
	}

	function clistdobrg_t($d_do_first,$d_do_last,$kddo) {
		if(!empty($d_do_first) && !empty($d_do_last) && !empty($kddo) ) {
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= " AND a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			
		} else if((!empty($d_do_first) && !empty($d_do_last)) && empty($kddo) ) {	
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= "";
			$ibatal	= " AND a.f_do_cancel='f' ";
			
		} else if((empty($d_do_first) || empty($d_do_last)) && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
		} else if((strlen($d_do_first)=="" || strlen($d_do_last)=="") && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
		} else {
			$ddo	= "";
			$do		= "";
			$ibatal	= " WHERE a.f_do_cancel='f' ";
		}
		/*
		$str_qry	= "
				SELECT  a.d_do AS dop,
					a.i_do_code AS idocode,
					a.i_do AS ido,
					a.i_branch AS ibranch,
					e.e_branch_name AS ebranchname,
					b.i_op AS iop,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_deliver AS qty,
					b.n_residual AS sisadelivery
				
				FROM tm_do_item b
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$do." ".$ibatal." 
				ORDER BY a.i_do_code DESC ";
		*/
		$str_qry	= "
			SELECT  a.d_do AS ddo,
				a.i_do_code AS idocode,
				a.i_branch AS ibranch,
				e.e_branch_name AS ebranchname,
				a.f_do_cancel,
				a.f_faktur_created

			FROM tm_do_item b 

				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$ddo." ".$ibatal."
			
			GROUP BY a.d_do, a.i_do_code, a.i_branch, e.e_branch_name, a.f_do_cancel, a.f_faktur_created 
			
			ORDER BY a.i_do_code DESC ";
				
		return $this->db->query($str_qry);
	}

	function viewperpages_t($limit,$offset,$d_do_first,$d_do_last,$kddo) {
		if(!empty($d_do_first) && !empty($d_do_last) && !empty($kddo) ) {
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= " AND a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
			
		} else if((!empty($d_do_first) && !empty($d_do_last)) && empty($kddo) ) {	
			$ddo	= " WHERE ( a.d_do BETWEEN '$d_do_first' AND '$d_do_last' )";
			$do		= "";
			$ibatal	= " AND a.f_do_cancel='f' ";
			
		} else if((empty($d_do_first) || empty($d_do_last)) && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
		} else if((strlen($d_do_first)=="" || strlen($d_do_last)=="") && !empty($kddo) ) {
			$ddo	= "";
			$do		= " WHERE a.i_do_code='$kddo' ";
			$ibatal	= " AND a.f_do_cancel='f' ";
		} else {
			$ddo	= "";
			$do		= "";
			$ibatal	= " WHERE a.f_do_cancel='f' ";
		}
		$str_qry	= " SELECT  a.d_do AS ddo,
				a.i_do_code AS idocode,
				a.i_branch AS ibranch,
				e.e_branch_name AS ebranchname,
				a.f_do_cancel,
				a.f_faktur_created
			
			FROM tm_do_item b
			
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch ".$ddo." ".$ibatal."
			
			GROUP BY a.d_do, a.i_do_code, a.i_branch, e.e_branch_name, a.f_do_cancel, a.f_faktur_created 
			
			ORDER BY a.i_do_code DESC LIMIT ".$limit." OFFSET ".$offset." ";
				
		$query	= $this->db->query($str_qry);
		
		if($query->num_rows()>0){
			return $result	= $query->result();
		}
	}
			
	function lbarangjadiperpages($limit,$offset){
		$query = $this->db->query( "
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) 
			
			WHERE a.f_do_cancel='f' 

			GROUP BY a.i_do, a.i_do_code, a.d_do

			ORDER BY a.d_do DESC LIMIT ".$limit." OFFSET ".$offset );

		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lbarangjadi(){
		return $this->db->query( "
		
			SELECT  a.i_do AS ido,
				a.i_do_code AS ido_code,
				a.d_do AS ddo
				
			FROM tm_do a 
			
			INNER JOIN tm_do_item b ON a.i_do=b.i_do
			
			INNER JOIN (SELECT d.i_product_motif AS pbase 
				FROM tr_product_base c 
				RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
			
			AS pbase ON trim(pbase)=trim(b.i_product) WHERE a.f_do_cancel='f' 
			
			GROUP BY a.i_do, a.i_do_code, a.d_do
			
			ORDER BY a.d_do DESC LIMIT 1000 " );
	}	
	
	function flbarangjadi($key) {
		if(!empty($key)) {
			return $this->db->query( "			
				SELECT  a.i_do AS ido,
					a.i_do_code AS ido_code,
					a.d_do AS ddo
					
				FROM tm_do a 
				
				INNER JOIN tm_do_item b ON a.i_do=b.i_do
				
				INNER JOIN (SELECT d.i_product_motif AS pbase 
					FROM tr_product_base c 
					RIGHT JOIN tr_product_motif d ON trim(c.i_product_base)=trim(d.i_product))
				
				AS pbase ON trim(pbase)=trim(b.i_product) 
				
				WHERE (a.i_do_code LIKE '$key%' OR b.i_product LIKE '$key%') AND a.f_do_cancel=false
				
				GROUP BY a.i_do, a.i_do_code, a.d_do
				
				ORDER BY a.d_do DESC LIMIT 100 " );
		}	
	}
	
	/* Edit Session */
	function getdoheader($idocode) {
		return $this->db->query(" SELECT * FROM tm_do WHERE i_do_code='$idocode' ");
	}

	function lpelanggan() {	
		$order	= " ORDER BY a.e_customer_name ASC, a.i_customer_code DESC ";
		$this->db->select(" a.i_customer AS code, a.e_customer_name AS customer FROM tr_customer a ".$order." ",false);
		$query	= $this->db->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
	function getpelanggan($icustomer) {
		$this->db->where('i_customer',$icustomer);
		$this->db->order_by('i_do');
		return $this->db->get('tm_do');
	}
	
	function lcabang($icustomer) {
		if(!empty($icustomer)) {
			$filter	= " WHERE a.i_customer='$icustomer' ";
		} else {
			$filter	= "";
		}
		
		$order	= " ORDER BY a.e_branch_name ASC, a.i_branch_code DESC ";
		$strq	= " SELECT a.i_branch AS codebranch, 
					a.i_branch_code AS ibranchcode,
				    a.i_customer AS codecustomer, 
					a.e_branch_name AS ebranchname,
				    a.e_branch_name AS branch 
					
					FROM tr_branch a 
					
				    INNER JOIN tr_customer b ON a.i_customer=b.i_customer ".$filter." ".$order;
					
		$query	= $this->db->query($strq);
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}

	function getcabang($ibranch) {
		$this->db->where('i_branch',$ibranch);
		$this->db->order_by('i_do');
		return $this->db->get('tm_do');
	}
		
	function ldoitem($ido) {
				/*
				SELECT	a.i_op_code AS op,
						cast(trim(b.i_product) AS character varying) AS iproduct,
						d.e_product_motifname AS productname,
						b.n_count AS qty,
						b.n_residual AS qtyakhir,
						d.n_quantity AS qtyproduk,
						e.v_unitprice AS unitprice,
						(b.n_residual * e.v_unitprice) AS harga	
						*/
		/* Remark 20062011				
		$qstr	= "
			revisi : SELECT  a.d_do AS dop,
					a.i_do_code AS idocode,
					a.i_do AS ido,
					a.i_branch AS ibranch,
					b.i_op AS iopcode,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					b.n_deliver AS qty,
					f.n_quantity_akhir AS qtyproduk,
					d.v_unitprice AS unitprice,
					b.v_do_gross as harga 
					
				FROM tm_do_item b 
				
				RIGHT JOIN tm_do a ON a.i_do=b.i_do
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
				INNER JOIN tr_branch e ON e.i_branch_code=a.i_branch 
				INNER JOIN tm_stokopname_item f ON trim(f.i_product)=trim(b.i_product)
				INNER JOIN tm_stokopname g ON g.i_so=f.i_so
				WHERE a.i_do='$ido' AND g.i_status_so='0'
					
				ORDER BY b.i_do_item DESC";
		*/		
		/* Baru 20062011 */
		
		/* 26072011
		$qstr	= "
			SELECT a.d_do AS dop, 
				a.i_do_code AS idocode, 
				a.i_do AS ido, 
				a.i_branch AS ibranch, 
				h.i_op AS iop,
				b.i_op AS iopcode, 
				b.i_product AS iproduct, 
				b.e_product_name AS motifname, 
				b.n_deliver AS qty, 
				f.n_quantity_akhir AS qtyproduk, 
				(b.v_do_gross/b.n_deliver) AS unitprice, 
				b.v_do_gross as harga 
			
			FROM tm_do_item b 
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tm_stokopname_item f ON trim(f.i_product)=trim(b.i_product) 
			INNER JOIN tm_stokopname g ON g.i_so=f.i_so 
			INNER JOIN tm_op h ON h.i_op_code=cast(b.i_op AS character varying)
			WHERE a.i_do='$ido' AND g.i_status_so='0' AND a.f_do_cancel='f' ORDER BY b.i_do_item DESC	";
		*/

		$qstr	= "
			SELECT a.d_do AS dop,
				a.i_do_code AS idocode,
				a.i_do AS ido,
				a.i_branch AS ibranch,
				h.i_op_code AS iopcode,
				b.i_op AS iop,
				b.i_product AS iproduct,
				b.e_product_name AS motifname,
				b.n_deliver AS qty,
				f.n_quantity_akhir AS qtyproduk,
				(b.v_do_gross/b.n_deliver) AS unitprice,
				b.v_do_gross as harga,
				g.f_stop_produksi AS stp
				
			FROM tm_do_item b 
			INNER JOIN tm_do a ON a.i_do=b.i_do
			INNER JOIN tm_stokopname_item f ON trim(f.i_product)=trim(b.i_product) 
			INNER JOIN tm_stokopname g ON g.i_so=f.i_so 
			INNER JOIN tm_op h ON h.i_op=b.i_op
			WHERE a.i_do='$ido' AND g.i_status_so='0' AND a.f_do_cancel='f' ORDER BY b.i_do_item DESC	";
		
		$query	= $this->db->query($qstr);
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}

	function cari_do($ndo,$codelama) {
		return $this->db->query(" SELECT * FROM tm_do WHERE (i_do_code=trim('$ndo') AND i_do_code!=trim('$codelama')) AND f_do_cancel='f' ");
	}

	//function mupdateXX($i_op_item,$i_product_item,$n_deliver_item,$i_do,$i_do_code_hidden,$i_do_hidden,$nw_d_do,$i_customer_hidden,$i_branch_hidden,$i_op,$i_op_sebunyi,$i_product,$e_product_name,$n_deliver,$v_do_gross,$e_note,$iterasi,$qty_product,$qty_op) {
	function mupdateXX($i_product_item,$n_deliver_item,$i_do,$i_do_code_hidden,$i_do_hidden,$nw_d_do,$i_customer_hidden,$i_branch_hidden,$i_op,$i_op_sebunyi,$i_product,$e_product_name,$n_deliver,$v_do_gross,$e_note,$iterasi,$qty_product,$qty_op,$i_order,$f_stp) {
			
		//Hrs ada query yg mengurangi ke tabel tm_stokopname,
		//Seluruh transaksi DO, BBK, BBM, Bon M Masuk, Bon M Keluar. Pengurangan atau Penambahan Stok
		//mengambil dari tabel tm_stokopname, sedangkan tm_so adalah temp/ History berjalan

		//$qtyawal = array();
		//$qtyakhir	= array();
		//$back_qty_awal	= array();
		$back_qty_akhir	= array();
		//$update_stokop_item	= array();
		
		$this->db->trans_begin();
		
		$qdate	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		//Kembalikan ke saldo awal
		//Delete DO yg sudah di DO -kan
		//Insert Brg ke Table DO Kembali
		//Insert Transaksi ke Table tm_so
		
		$qstokopname	= $this->db->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$iso	= $row_stokopname->i_so;
		} else {
			$iso	= "";
		}

		if($iso!="") {
			$j=0;
			//$iproductmotif	= array();
			//$delivery	= array();
			//$totalharga	= array();
			//$qtyawal	= array();
			//$qtyakhir	= array();
			
			//kembalikan ke saldo sebelum di Do - kan
			$qdo_item	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$i_do_hidden' ");
			if($qdo_item->num_rows()>0) {

				$result_doitem	= $qdo_item->result();
				foreach($result_doitem as $row_doitem) {
					//$iproductmotif[$j]	= $row_doitem->i_product;
					//$delivery[$j]		= $row_doitem->n_deliver;
					//$totalharga[$j]		= $row_doitem->v_do_gross;

					$qstokopname2	= $this->db->query("SELECT i_so FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$j]' ORDER BY i_so DESC LIMIT 1");
					$row_stokopname2= $qstokopname2->row();
					$iso2	= $row_stokopname2->i_so;
					
					$qstok_item	= $this->db->query("SELECT * FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						//$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
						//$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;

						//$back_qty_awal[$j]	= $qtyawal[$j];
						//$back_qty_akhir[$j]	= $qtyakhir[$j]+$row_doitem->n_deliver;
						$back_qty_akhir[$j]	= (($row_stok_item->n_quantity_akhir)+($row_doitem->n_deliver));

						/*** 24062011					
						$update_stokop_item[$j]	= array(
							'n_quantity_awal'=>$back_qty_awal[$j],
							'n_quantity_akhir'=>$back_qty_akhir[$j]
						);
						***/
						
						/***
						print_r($update_stokop_item[$j]);
						print "<br>";
						***/
						/*** 24062011
						$this->db->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$iso,'i_product'=>$row_doitem->i_product));
						***/
						//$this->db->query(" UPDATE tm_stokopname_item SET n_quantity_awal='$back_qty_awal[$j]', n_quantity_akhir='$back_qty_akhir[$j]' WHERE i_so='$iso' AND i_product='$row_doitem->i_product' ");
						$this->db->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$j]' WHERE i_so='$iso2' AND i_product='$row_doitem->i_product' ");
					}
					$j+=1;
				}
				
				//Mengembalikan Nilai Awal
				$back_order	= 0;
				foreach($i_order as $iorder){
					if($back_order==0 || $back_order=='0'){
						$this->db->query(" UPDATE tm_op SET f_do_created='f' WHERE f_op_cancel='f' AND i_op='$iorder' ");
						$back_order = 1;
					}
					
					if($iorder!=''){
						$qorder	= $this->db->query(" SELECT * FROM tm_op_item WHERE i_op='$iorder' ORDER BY i_op_item ASC ");
						if($qorder->num_rows()>0){
							$row_order	= $qorder->row();
							$this->db->query(" UPDATE tm_op_item SET n_residual='$row_order->n_count', f_do_created='f' WHERE i_op_item='$row_order->i_op_item' AND i_product='$row_order->i_product' AND i_op='$iorder' ");
						}
					}
				}
				//End 0f back
			}
			//End 0f kembali ke Saldo sebelum di Do-kan
			
			//Hapus Do
			/*** 24062011
			$this->db->delete('tm_do',array('i_do'=>$i_do_hidden));
			$this->db->delete('tm_do_item',array('i_do'=>$i_do_hidden));
			***/
			$this->db->query(" DELETE FROM tm_do WHERE i_do='$i_do_hidden' AND f_do_cancel='f' ");
			$this->db->query(" DELETE FROM tm_do_item WHERE i_do='$i_do_hidden' ");
			//End 0f Hps DO
		}
		
		$this->db->query(" INSERT INTO tm_do(i_do,i_do_code,i_customer,i_branch,d_do,d_entry) VALUES('$i_do_hidden','$i_do','$i_customer_hidden','$i_branch_hidden','$nw_d_do','$dentry') ");
		
		/*** 16072011		
		$tm_do	=
			array(
			 'i_do'=>$i_do_hidden,
			 'i_do_code'=>$i_do_code_hidden,
			 'i_customer'=>$i_customer_hidden,
			 'i_branch'=>$i_branch_hidden,
			 'd_do'=>$nw_d_do,
			 'd_entry'=>$dentry );
		***/
		
		/***
		print_r($tm_do);
		print "<br>";
		print "------------------------------------------------------------------------------";
		print "<br>";
		***/
		
		/*** 16072011
		if($this->db->insert('tm_do',$tm_do)) {
		***/
		if(isset($iterasi)) {	
		
			$qty_sisa_saldo_product	= array();
			$qty_sisa_op_blm_do	= array();
			$idoitem	= array();
			$tm_do_item	= array();
			$isox	= array();
			$tm_so	= array();
			
			//$arrfakturupdate	= array();
			//$arrfakturupdate2	= array();
			
			$iopXXX	= array();
			
			$jml_item_brX	= array();
			$qty_akhirX		= array();
			
			//$arrfakturupdateX	= array();
			//$arrfakturupdate2X	= array();
			
			$update_order	= 0;
			$update_order2	= 0;
			
			$sisaorderawal	= array();
			
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++){

				$qstokopname2	= $this->db->query(" SELECT * FROM tm_stokopname WHERE i_status_so='0' AND f_stop_produksi='$f_stp[$jumlah]' ORDER BY i_so DESC LIMIT 1");
				$row_stokopname2= $qstokopname2->row();
				$iso2	= $row_stokopname2->i_so;
				
				$qstok_item	= $this->db->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
				if($qstok_item->num_rows()>0) {
					$row_stok_item	= $qstok_item->row();
					$kurangi	= (($row_stok_item->n_quantity_akhir)-($n_deliver[$jumlah]));
					$this->db->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$kurangi' WHERE i_so='$iso2' AND i_product='$i_product[$jumlah]' ");
				}
												
				$seq_tm_do_item	= $this->db->query(" SELECT cast(i_do_item AS integer) AS i_do_item FROM tm_do_item ORDER BY cast(i_do_item AS integer) DESC LIMIT 1 ");
				if($seq_tm_do_item->num_rows() > 0 ){
					$seqrow	= $seq_tm_do_item->row();
					$idoitem[$jumlah]	= $seqrow->i_do_item+1;
				} else {
					$idoitem[$jumlah]	= 1;
				}

				//Kurangi Qty yg ada di Produk Motif dgn Qty/Jumlah DO
				$qty_sisa_saldo_product[$jumlah]= $qty_product[$jumlah]-$n_deliver[$jumlah];
				$qty_sisa_op_blm_do[$jumlah]	= $qty_op[$jumlah]-$n_deliver[$jumlah];
				
				/* 26072011
				$tm_do_item[$jumlah]	= 
					array(
					 'i_do_item'=>$idoitem[$jumlah],
					 'i_do'=>$i_do_hidden,
					 'i_op'=>$i_op[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_deliver'=>$n_deliver[$jumlah],
					 'n_residual'=>$n_deliver[$jumlah], 
					 'v_do_gross'=>$v_do_gross[$jumlah],
					 'e_note'=>$e_note[$jumlah],
					 'd_entry'=>$dentry );
				*/
				$tm_do_item[$jumlah]	= 
					array(
					 'i_do_item'=>$idoitem[$jumlah],
					 'i_do'=>$i_do_hidden,
					 'i_op'=>$i_op_sebunyi[$jumlah],
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_deliver'=>$n_deliver[$jumlah],
					 'n_residual'=>$n_deliver[$jumlah], 
					 'v_do_gross'=>$v_do_gross[$jumlah],
					 'e_note'=>$e_note[$jumlah],
					 'd_entry'=>$dentry );				
				
				/***
				print_r($tm_do_item[$jumlah]);
				print "<br>";
				print "------------------------------------------------------------------------------";
				print "<br>";				
				***/
	 			
				if($this->db->insert('tm_do_item',$tm_do_item[$jumlah])) {	
				
					// Kembalikan OP ke order sebelum di DO-kan
					//if($i_product_item[$jumlah]!='' && $i_op_item[$jumlah]!=''){
					if($i_product_item[$jumlah]!='' && $i_op_sebunyi[$jumlah]!=''){	
						/* 26072011
						$qtm_op_item	= $this->db->query(" SELECT n_count, n_residual FROM tm_op_item WHERE i_op='$i_op_item[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
						*/
						$qtm_op_item	= $this->db->query(" SELECT n_count, n_residual FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
						
						$rtm_op_item	= $qtm_op_item->row();

						$qtyorder	= $rtm_op_item->n_count;
						$sisaorderawal[$jumlah]	= $rtm_op_item->n_residual+$n_deliver_item[$jumlah];
						
						if($qtyorder==$sisaorderawal[$jumlah]){
							
							if($update_order==0){
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_item[$jumlah]' ");
								*/ 
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order=1;	
							}
							/* 26072011
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='f' WHERE i_op='$i_op_item[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
							*/ 
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='f' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
								
						}elseif($sisaorderawal[$jumlah]<$qtyorder && $sisaorderawal[$jumlah]!=0){
							/*** 24062011
							$arrfakturupdate[$jumlah]	= array(
								'f_do_created'=>'TRUE'
							);
							
							$arrfakturupdate2[$jumlah]	= array(
								'n_residual'=>$sisaorderawal[$jumlah],
								'f_do_created'=>'FALSE'
							);
							***/
							
							if($update_order==0){
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_item[$jumlah]' ");
								*/ 
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order=1;	
							}
							/* 26072011
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='f' WHERE i_op='$i_op_item[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
							*/ 
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='f' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
							
						}elseif($sisaorderawal[$jumlah]==0){
							/*** 24062011
							$arrfakturupdate[$jumlah]	= array(
								'f_do_created'=>'TRUE'
							);
	
							$arrfakturupdate2[$jumlah]	= array(
								'n_residual'=>$sisaorderawal[$jumlah],
								'f_do_created'=>'TRUE'
							);		
							***/
							
							if($update_order==0){
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_item[$jumlah]' ");
								*/
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								
								$update_order=1;	
							}
							/* 26072011
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='t' WHERE i_op='$i_op_item[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
							*/ 
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
																				
						}else{
							/*** 24062011
							$arrfakturupdate[$jumlah]	= array(
								'f_do_created'=>'TRUE'
							);
	
							$arrfakturupdate2[$jumlah]	= array(
								'n_residual'=>$sisaorderawal[$jumlah],
								'f_do_created'=>'TRUE'
							);	
							***/
							
							if($update_order==0){
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_item[$jumlah]' ");
								*/ 
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order=1;	
							}
							/* 26072011
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='t' WHERE i_op='$i_op_item[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
							*/ 
							$this->db->query(" UPDATE tm_op_item SET n_residual='$sisaorderawal[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product_item[$jumlah]' ");
																			
						}
						
						/*** 24062011
						if($update_order==0){
							$this->db->update('tm_op',$arrfakturupdate[$jumlah],array('i_op'=>$i_op_item[$jumlah]));
							$update_order=1;	
						}
						$this->db->update('tm_op_item',$arrfakturupdate2[$jumlah],array('i_op'=>$i_op_item[$jumlah],'i_product'=>$i_product_item[$jumlah]));						
						***/
						
						/***
						print_r($arrfakturupdate[$jumlah]);
						print "<br>";
						print "------------------------------------------------------------------------------";
						print "<br>";
						print_r($arrfakturupdate2[$jumlah]);
						print "<br>";
						print "------------------------------------------------------------------------------";
						print "<br>";
						***/
					}
					// End 0f Kembalikan
					
					// Update Order dgn data yg baru
					
					/* 26072011
					$qiop	= $this->db->query(" SELECT i_op FROM tm_op WHERE i_op_code='$i_op[$jumlah]' ");
					$row_iop= $qiop->row();
					$iopXXX[$jumlah]	= $row_iop->i_op;
					*/
					
					/* 26072011
					$q_qty_op_item	= $this->db->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$iopXXX[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					*/
					$q_qty_op_item	= $this->db->query(" SELECT n_residual FROM tm_op_item WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
					
					if($q_qty_op_item->num_rows()>0) {
						
						$row_item_br	= $q_qty_op_item->row();
						$jml_item_brX[$jumlah]	= $row_item_br->n_residual;
						
						if($n_deliver[$jumlah]==$jml_item_brX[$jumlah]) {
							
							$qty_akhirX[$jumlah]	= $jml_item_brX[$jumlah] - $n_deliver[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
							
							if($qty_akhirX[$jumlah]==0) {
								$qty_akhirX[$jumlah] = 0;
							}
							/*** 24062011
							$arrfakturupdateX[$jumlah]	= array(
								'f_do_created'=>'TRUE'
							);
				
							$arrfakturupdate2X[$jumlah]	= array(
								'n_residual'=>$qty_akhirX[$jumlah],
								'f_do_created'=>'TRUE'
							);
							***/
							
							if($update_order2==0){
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$iopXXX[$jumlah]' ");
								*/
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								
								$update_order2 = 1;
							}	
							/* 26072011
							$this->db->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$iopXXX[$jumlah]' AND i_product='$i_product[$jumlah]' ");
							*/ 
							$this->db->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
													
						} else if($n_deliver[$jumlah] < $jml_item_brX[$jumlah]) { // jika jmlitem kurang dari qty brg yg ada
							$qty_akhirX[$jumlah]	= $jml_item_brX[$jumlah] - $n_deliver[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
							
							/*** 24062011	
							$arrfakturupdateX[$jumlah]	= array(
								'f_do_created'=>'TRUE'
							);
	
							$arrfakturupdate2X[$jumlah]	= array(
								'n_residual'=>$qty_akhirX[$jumlah],
								'f_do_created'=>'FALSE'
							);
							***/
							
							if($update_order2==0){
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$iopXXX[$jumlah]' ");
								*/ 
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order2 = 1;
							}
							/* 26072011
							$this->db->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='f' WHERE i_op='$iopXXX[$jumlah]' AND i_product='$i_product[$jumlah]' ");
							*/ 
							$this->db->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='f' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
									
						} else {
							$qty_akhirX[$jumlah]	= $jml_item_brX[$jumlah] - $n_deliver[$jumlah]; // utk kebutuhan update ke qty_akhir di tabel sj detail
							
							if($qty_akhirX[$jumlah]==0) {
								$qty_akhirX[$jumlah] = 0;
							}
							/*** 24062011								
							$arrfakturupdateX[$jumlah]	= array(
								'f_do_created'=>'TRUE'
							);
				
							$arrfakturupdate2X[$jumlah]	= array(
								'n_residual'=>$qty_akhirX[$jumlah],
								'f_do_created'=>'TRUE'
							);
							***/

							if($update_order2==0){
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$iopXXX[$jumlah]' ");
								*/
								$this->db->query(" UPDATE tm_op SET f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' ");
								$update_order2 = 1;
							}
							
							/* 26072011
							$this->db->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$iopXXX[$jumlah]' AND i_product='$i_product[$jumlah]' ");
							*/
							
							$this->db->query(" UPDATE tm_op_item SET n_residual='$qty_akhirX[$jumlah]', f_do_created='t' WHERE i_op='$i_op_sebunyi[$jumlah]' AND i_product='$i_product[$jumlah]' ");
						}
						
						/*** 24062011
						if($update_order2==0){
							$this->db->update('tm_op',$arrfakturupdateX[$jumlah],array('i_op'=>$iopXXX[$jumlah]));
							$update_order2 = 1;
						}		
						$this->db->update('tm_op_item',$arrfakturupdate2X[$jumlah],array('i_op'=>$iopXXX[$jumlah],'i_product'=>$i_product[$jumlah]));
						***/
						
						/***
						print_r($arrfakturupdateX[$jumlah]);
						print "<br>";
						print "------------------------------------------------------------------------------";
						print "<br>";
						print_r($arrfakturupdate2X[$jumlah]);
						print "<br>";	
						print "------------------------------------------------------------------------------";
						print "<br>";	
						***/										
					}
					// End 0f Update Order
										
					//UPDATE TRANSAKSI DO KE TABEL tm_so
					//Jika sudah ada DO
					$get_tmsox	= $this->db->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' AND a.d_do='$nw_d_do' ");					
					
					if($get_tmsox->num_rows() > 0 ) {
						
						$get_tmso2	= $this->db->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						
						if($get_tmso2->num_rows() > 0 ) {
							$row_tmso2	= $get_tmso2->row();
							$isox[$jumlah]	= $row_tmso2->iso+1;
						} else {
							$isox[$jumlah]	= 1;
						}

						$row_tmso	= $get_tmsox->row_array();
						
						/*** 24062011
						$temp_iso[$jumlah]	= $row_tmso['i_so'];
						$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
						$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
						$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
						$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
						$temp_ddo[$jumlah]	= $row_tmso['d_do'];
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
						***/

						if($row_tmso['n_saldo_akhir']<0 || $row_tmso['n_saldo_akhir']==''){
							$n_saldo_akhir	= 0;
						}else{
							$n_saldo_akhir	= $row_tmso['n_saldo_akhir'];
						}
													
						$saldo_sisa[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_deliver[$jumlah];
						
						/*** 24062011
						$this->db->set(array(
							'i_so'=>$isox[$jumlah],
							'i_product'=>$temp_iproduct[$jumlah],
							'i_product_motif'=>$temp_imotif[$jumlah],
							'e_product_motifname'=>$temp_productname[$jumlah],
							'i_status_do'=>$temp_istatusdo[$jumlah],
							'd_do'=>$temp_ddo[$jumlah],
							'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
							'n_saldo_akhir'=>$saldo_sisa[$jumlah],
							'd_entry'=>$dentry	));
						***/
							
						/*
						$tm_so[$jumlah]	= array(
							'i_so'=>$isox[$jumlah],
							'i_product'=>$temp_iproduct[$jumlah],
							'i_product_motif'=>$temp_imotif[$jumlah],
							'e_product_motifname'=>$temp_productname[$jumlah],
							'i_status_do'=>$temp_istatusdo[$jumlah],
							'd_do'=>$temp_ddo[$jumlah],
							'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
							'n_saldo_akhir'=>$saldo_sisa[$jumlah],
							'd_entry'=>$dentry	);
						*/
						
						/*** 24062011
						$this->db->insert('tm_so');
						***/
						
						$this->db->query(" INSERT INTO tm_so (i_so,i_product,i_product_motif,e_product_motifname,i_status_do,d_do,n_saldo_awal,n_saldo_akhir,d_entry) VALUES('$isox[$jumlah]','$row_tmso[i_product]','$row_tmso[i_product_motif]','$row_tmso[e_product_motifname]','$row_tmso[i_status_do]','$row_tmso[d_do]','$n_saldo_akhir','$saldo_sisa[$jumlah]','$dentry') ");
						
					} else {
						$get_tmso2	= $this->db->query( " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 " );
						
						if($get_tmso2->num_rows() > 0 ) {
							$row_tmso2	= $get_tmso2->row();
							$isox[$jumlah]	= $row_tmso2->iso+1;
						} else {
							$isox[$jumlah]	= 1;
						}
						
						//Jika belum ada tgl DO "input awal DO"
						$get_tmso	= $this->db->query(" SELECT a.* FROM tm_so a WHERE a.i_product_motif='$i_product[$jumlah]' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ");
						
						if($get_tmso->num_rows()>0) {
							
							$row_tmso	= $get_tmso->row_array();
							
							/*** 24062011
							$temp_iso[$jumlah]	= $row_tmso['i_so'];
							$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
							$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
							$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
							$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
							$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'];
							***/
							if($row_tmso['n_saldo_akhir']<0 || $row_tmso['n_saldo_akhir']==''){
								$n_saldo_akhir	= 0;
							}else{
								$n_saldo_akhir	= $row_tmso['n_saldo_akhir'];
							}
							
							$saldo_sisa[$jumlah]	= $row_tmso['n_saldo_akhir'] - $n_deliver[$jumlah];
							
							/*** 24062011
							$this->db->set(array(
								'i_so'=>$isox[$jumlah],
								'i_product'=>$temp_iproduct[$jumlah],
								'i_product_motif'=>$temp_imotif[$jumlah],
								'e_product_motifname'=>$temp_productname[$jumlah],
								'i_status_do'=>'1',
								'd_do'=>$nw_d_do,
								'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
								'n_saldo_akhir'=>$saldo_sisa[$jumlah],
								'd_entry'=>$dentry ));
							***/
							
							/*							
							$tm_so[$jumlah]	= array(
								'i_so'=>$isox[$jumlah],

								'i_product'=>$temp_iproduct[$jumlah],
								'i_product_motif'=>$temp_imotif[$jumlah],
								'e_product_motifname'=>$temp_productname[$jumlah],
								'i_status_do'=>'1',
								'd_do'=>$nw_d_do,
								'n_saldo_awal'=>$temp_saldo_akhir[$jumlah],
								'n_saldo_akhir'=>$saldo_sisa[$jumlah],
								'd_entry'=>$dentry );
							*/
							
							/*** 24062011
							$this->db->insert('tm_so');
							***/
							
							$this->db->query(" INSERT INTO tm_so(i_so,i_product,i_product_motif,e_product_motifname,i_status_do,d_do,n_saldo_awal,n_saldo_akhir,d_entry) VALUES('$isox[$jumlah]','$row_tmso[i_product]','$row_tmso[i_product_motif]','$row_tmso[e_product_motifname]','1','$nw_d_do','$n_saldo_akhir','$saldo_sisa[$jumlah]','$dentry') ");
						} else {
							//Hapus kembali tm_do yg telah disimpan dgn ID :
							//i_do = $ido,
							//Hapus kembali tm_do_item yg telah disimpan dgn ID :
							//i_do_item = $idoitem[$jumlah] (gunakan pengulangan),
							//atau
							//i_do = $ido (menghapus semua data pd tabel tm_do_item berdasarkan i_do)				
							echo "tm_do & tm_do_item gagal diupdate!";
						}
					}
					//END 0F TRANSAKSI
				} else {
					//Hapus kembali tm_do yg telah disimpan, dgn ID :
					//i_do = $ido
					echo "tm_do gagal diupdate!";
				}
				
				if ($this->db->trans_status()===FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
			}
		} else {
			print "<script>alert(\"Maaf, DO gagal diupdate. Terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
 		}
		
		$qdo	= $this->db->query(" SELECT * FROM tm_do WHERE i_do='$i_do_hidden' ");
		if($qdo->num_rows()<0 || $qdo->num_rows()==0) {
			print "<script>alert(\"Maaf, DO gagal diupdate, kesalahan pd saat input Master Motif Brg.\");show(\"listdo/cform\",\"#content\");</script>";
		} else {
			print "<script>alert(\"Nomor DO : '\"+$i_do_code_hidden+\"' telah diupdate, terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
		}
	}
	//End 0f Edit Session
	
	/* Cancel Session */
	function mbatal($idocode) {
	
		if(!empty($idocode)) {
		
			$qstokopname	= $this->db->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
			if($qstokopname->num_rows()>0) {
				$row_stokopname	= $qstokopname->row();
				$iso	= $row_stokopname->i_so;
			} else {
				$iso	= "";
			}
			
			$qcariidocode	= $this->db->query(" SELECT * FROM tm_do WHERE i_do_code='$idocode' AND f_do_cancel='f' ORDER BY i_do DESC LIMIT 1 ");
			if($qcariidocode->num_rows()>0) {
				$row_idocode	= $qcariidocode->row();
				$idox	= $row_idocode->i_do;
				
				$tbl_do	= array(
					'f_do_cancel'=>'TRUE'
				);
				
				if($this->db->update('tm_do',$tbl_do,array('i_do_code'=>$idocode))) {
				
					$j=0;
					$iproductmotif	= array();
					$delivery	= array();
					$totalharga	= array();
					//$qtyawal	= array();
					$qtyakhir	= array();
					$iopcode	= array();
					$iopcode[0]	= '';
					
					//kembalikan ke saldo sebelum di Do - kan
					/* 26072011
					$qdo_item	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$idox' AND char_length(cast(i_op as character varying))>5 ");
					*/
					
					$qdo_item	= $this->db->query(" SELECT * FROM tm_do_item WHERE i_do='$idox'  ");
					
					if($qdo_item->num_rows()>0) {

						$result_doitem	= $qdo_item->result();
						
						foreach($result_doitem as $row_doitem) {
							
							$iproductmotif[$j]	= $row_doitem->i_product;
							$delivery[$j]		= $row_doitem->n_deliver;
							$totalharga[$j]		= $row_doitem->v_do_gross;

							if($iopcode[0]==''){
								$iopcode[0]	= $row_doitem->i_op;
							}
							
							/* 08082011
							$qstok_item	= $this->db->query(" SELECT * FROM tm_stokopname_item WHERE i_so='$iso' AND i_product='$iproductmotif[$j]' ");
							*/
							
							$qstok_item	= $this->db->query(" SELECT a.n_quantity_awal, a.n_quantity_akhir, b.i_so, b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b ON b.i_so=a.i_so WHERE b.i_status_so='0' AND a.i_product='$iproductmotif[$j]' ORDER BY b.i_so DESC LIMIT 1 ");
							
							if($qstok_item->num_rows()>0) {
								$row_stok_item	= $qstok_item->row();
								/*** 27062011								
								$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
								$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;
								***/
							} else {
								/*** 27062011
								$qtyawal[$j]	= 0;
								$qtyakhir[$j]	= 0;						
								***/
							}
							
							$back_qty_akhir[$j]	= (($row_stok_item->n_quantity_akhir)+($row_doitem->n_deliver));

							/*** 27062011
							$back_qty_awal[$j]	= $qtyakhir[$j]+$delivery[$j];
							$back_qty_akhir[$j]	= $qtyawal[$j];
							***/

							/*** Disabled 27062011
							$update_stokop_item	= array(
								'n_quantity_awal'=>$back_qty_awal[$j],
								'n_quantity_akhir'=>$back_qty_akhir[$j]
							);
							***/
							
							/***
							$update_stokop_item	= array(
								'n_quantity_akhir'=>$back_qty_akhir[$j]
							);
							
							$this->db->update('tm_stokopname_item',$update_stokop_item,array('i_so'=>$iso,'i_product'=>$iproductmotif[$j]));
							***/

							$this->db->query(" UPDATE tm_stokopname_item SET n_quantity_akhir='$back_qty_akhir[$j]' WHERE i_so='$row_stok_item->i_so' AND i_product='$iproductmotif[$j]' ");
							$j+=1;
						}
					}
					//End 0f kembali ke Saldo sebelum di Do-kan
					
					/* Ubah tabel OP & OP item ke status awal */
					/* 26072011
					$qop	= $this->db->query(" SELECT i_op, i_op_code FROM tm_op WHERE i_op_code='$iopcode[0]' AND f_do_created='t'  AND f_op_cancel='f' ");
					*/ 
					$qop	= $this->db->query(" SELECT i_op, i_op_code FROM tm_op WHERE i_op='$iopcode[0]' AND f_do_created='t'  AND f_op_cancel='f' ");
					
					if($qop->num_rows()>0){
						
						$back_qty_order	= array();
						$st_update_op	= 0;
						$iter	= 0;
						
						/* 26072011
						$op_row	= $qop->row();
						$row_iopcode	= $op_row->i_op_code;
						$row_iop	= $op_row->i_op;
						*/
						
						if($st_update_op==0 || $st_update_op=='0'){
							
							if(sizeof($iproductmotif)>0){
								
								$nilainya = 1;
								/* 26072011
								$this->db->query(" UPDATE tm_op SET f_do_created='f' WHERE i_op_code='$row_iopcode' ");
								*/
								$this->db->query(" UPDATE tm_op SET f_do_created='f' WHERE i_op='$iopcode[0]' ");
								
								foreach($iproductmotif as $imotif){
									/* 26072011
									$qsisa	= $this->db->query(" SELECT * FROM tm_op_item WHERE i_product='$iproductmotif[$iter]' AND i_op='$row_iop' ");
									*/
									$qsisa	= $this->db->query(" SELECT * FROM tm_op_item WHERE i_product='$iproductmotif[$iter]' AND i_op='$iopcode[0]' ");
									$rowsisa	= $qsisa->row();
									
									$nresidual	= $rowsisa->n_residual;
									$back_qty_order[$iter]	= $nresidual+$delivery[$iter];
									
									/* 26072011
									$this->db->query(" UPDATE tm_op_item SET f_do_created='f', n_residual='$back_qty_order[$iter]' WHERE i_product='$iproductmotif[$iter]' AND i_op='$row_iop' ");
									*/ 
									$this->db->query(" UPDATE tm_op_item SET f_do_created='f', n_residual='$back_qty_order[$iter]' WHERE i_product='$iproductmotif[$iter]' AND i_op='$iopcode[0]' ");
									$iter+=1;
								}
							}else{
								$nilainya = 0;
							}
						}
					}
					/* End 0f Status awal */
				}
				print "<script>alert(\"Nomor DO : '\"+$idocode+\"' telah dibatalkan, terimakasih.\");show(\"listdo/cform\",\"#content\");</script>";
			} else {
				print "<script>alert(\"Maaf, DO tdk dpt dibatalkan.\");show(\"listdo/cform\",\"#content\");</script>";
			}
		} else {
			print "<script>alert(\"Maaf, DO tdk dpt dibatalkan.\");show(\"listdo/cform\",\"#content\");</script>";		
		}
	}
	/* End 0f Cancel Session */
}
?>
