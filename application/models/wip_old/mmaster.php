<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_unit_jahit ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_unit_packing(){
    $query = $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAllsjmasuk($num, $offset, $cari, $date_from, $date_to) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		$this->db->select(" * FROM tm_sjmasukwip WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail WHERE id_sjmasukwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname as nama_brg FROM tr_product_motif 
										 WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$nama_brg = '';
						}
						
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $row2->ket_qty_warna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Masuk bagus hasil jahit";
				else if ($row1->jenis_masuk == '2')
					$nama_jenis = "Lain-lain (Perbaikan dari unit jahit)";
				else if ($row1->jenis_masuk == '3')
					$nama_jenis = "Lain-lain (Retur dari unit packing)";
				else if ($row1->jenis_masuk == '4')
					$nama_jenis = "Lain-lain (Retur dari gudang jadi)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_masuk'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'keterangan'=> $row1->keterangan,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function getAllsjmasuktanpalimit($cari, $date_from, $date_to){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_sjmasukwip WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  //
  function savesjmasuk($no_sj,$tgl_sj, $id_gudang, $jenis, $kode_unit_jahit, $kode_unit_packing, $ket, 
			$kode, $nama, $qty, $ket_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
    // cek apa udah ada datanya blm dgn no bon M tadi
    $this->db->select("id from tm_sjmasukwip WHERE no_sj = '".$this->db->escape_str($no_sj)."' AND tgl_sj = '$tgl_sj' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_sjmasukwip
			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'jenis_masuk'=>$jenis,
			  'id_gudang'=>$id_gudang,
			  'kode_unit_jahit'=>$kode_unit_jahit,
			  'kode_unit_packing'=>$kode_unit_packing,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_sjmasukwip',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_sjmasukwip
			$query2	= $this->db->query(" SELECT id FROM tm_sjmasukwip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			
				// ======== update stoknya! =============
				$nama_tabel_stok = "tm_stok_hasil_jahit";
				
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg_jadi = '$kode'
							AND id_gudang='$id_gudang' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama+$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode,
							'id_gudang'=>$id_gudang,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode' AND id_gudang='$id_gudang' ");
					}
										
				
				// jika semua data tdk kosong, insert ke tm_sjmasukwip_detail
				$data_detail = array(
					'id_sjmasukwip'=>$id_sj,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					'ket_qty_warna'=>$ket_warna
				);
				$this->db->insert('tm_sjmasukwip_detail',$data_detail);
			
		
  }
    
  function deletesjmasuk($kode){
	$tgl = date("Y-m-d");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_gudang, b.* FROM tm_sjmasukwip_detail b, tm_sjmasukwip a 
								 WHERE a.id = b.id_sjmasukwip AND b.id_sjmasukwip = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ============ update stok =====================
						$nama_tabel_stok = "tm_stok_hasil_jahit";
										
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." 
											 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row2->id_gudang' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama-$row2->qty; // berkurang stok karena reset dari SJ masuk
								
								$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', 
											tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
											AND id_gudang = '$row2->id_gudang' ");
								
					} // end foreach
				} // end if
	
	// 2. hapus data tm_sjmasukwip_detail dan tm_sjmasukwip
    $this->db->delete('tm_sjmasukwip_detail', array('id_sjmasukwip' => $kode));
    $this->db->delete('tm_sjmasukwip', array('id' => $kode));
  }
    
  function get_sjmasuk($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sjmasukwip where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail WHERE id_sjmasukwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						
						$tabelstok = "tm_stok_hasil_jahit";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." 
									 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row1->id_gudang' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'ket_qty_warna'=> $row2->ket_qty_warna,
												'keterangan'=> $row2->keterangan,
												'jum_stok'=> $jum_stok
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_masuk == '1')
					$nama_jenis = "Masuk bagus hasil jahit";
				else if ($row1->jenis_masuk == '2')
					$nama_jenis = "Lain-lain (Perbaikan dari unit jahit)";
				else if ($row1->jenis_masuk == '3')
					$nama_jenis = "Lain-lain (Retur dari unit packing)";
				else if ($row1->jenis_masuk == '4')
					$nama_jenis = "Lain-lain (Retur dari gudang jadi)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_masuk,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
      
  function get_brgjadi($num, $offset, $gudang, $cari)
  {
	if ($cari == "all") {
		$sql = " * FROM tr_product_motif ORDER BY i_product_motif ASC ";
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
		$sql = " * FROM tr_product_motif  ";		
		$sql.=" WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
				OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') 
				order by i_product_motif ASC";
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		$data_bhn= array();
		foreach ($hasil as $row1) {			
			$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$row1->i_product_motif'
										AND id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$stok	= $hasilrow->stok;
			}
			else
				$stok = 0;
						
			$data_bhn[] = array(		'kode_brg'=> $row1->i_product_motif,	
										'nama_brg'=> $row1->e_product_motifname,
										'stok'=> $stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_brgjaditanpalimit($gudang, $cari){
	if ($cari == "all") {
		$sql = " select * FROM tr_product_motif ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " SELECT * FROM tr_product_motif WHERE UPPER(i_product_motif) like UPPER('%".$this->db->escape_str($cari)."%') 
					OR UPPER(e_product_motifname) like UPPER('%".$this->db->escape_str($cari)."%') ";
		$query = $this->db->query($sql);
		return $query->result();  
	}
  }
  
  function cek_data_sjmasukwip($no_sj, $thn1){
    $this->db->select("id from tm_sjmasukwip WHERE no_sj = '$no_sj' 
				AND extract(year from tgl_sj) = '$thn1' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 14-03-2013, SJ keluar WIP
  
  function cek_data_sjkeluarwip($no_sj){
    $this->db->select("id from tm_sjkeluarwip WHERE no_sj = '$no_sj' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function getAllsjkeluar($num, $offset, $cari, $date_from, $date_to) {	  
		$pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		$this->db->select(" * FROM tm_sjkeluarwip WHERE TRUE ".$pencarian." ", false)->limit($num,$offset);
		$query = $this->db->get();

		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();

					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname as nama_brg FROM tr_product_motif 
										 WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
						}
						else {
							$nama_brg = '';
						}
						
										
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg,
												'qty'=> $row2->qty,
												'keterangan'=> $row2->keterangan,
												'ket_qty_warna'=> $row2->ket_qty_warna
											);
					}
				}
				else {
					$detail_sj = '';
				}
												
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar bagus ke unit packing";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Keluar bagus ke gudang jadi";
				else if ($row1->jenis_keluar == '3')
					$nama_jenis = "Lain-lain (Retur ke unit jahit)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'tgl_update'=> $row1->tgl_update,
											'jenis_keluar'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'keterangan'=> $row1->keterangan,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function getAllsjkeluartanpalimit($cari, $date_from, $date_to){
	  $pencarian = "";
		if($cari!="all"){
			$pencarian.= " AND UPPER(no_sj) like UPPER('%".$this->db->escape_str($cari)."%')";
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}else{
			if ($date_from != "00-00-0000")
				$pencarian.= " AND tgl_sj >= to_date('$date_from','dd-mm-yyyy') ";
			if ($date_to != "00-00-0000")
				$pencarian.= " AND tgl_sj <= to_date('$date_to','dd-mm-yyyy') ";
			$pencarian.= " ORDER BY tgl_sj DESC, no_sj DESC ";
		}
		
		$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip WHERE TRUE ".$pencarian." ");

    return $query->result();  
  }
  
  //
  function savesjkeluar($no_sj,$tgl_sj, $id_gudang, $jenis, $kode_unit_jahit, $kode_unit_packing, $ket, 
			$kode, $nama, $qty, $ket_warna, $ket_detail){  
    $tgl = date("Y-m-d H:i:s");
	
    // cek apa udah ada datanya blm dgn no SJ tadi
    $this->db->select("id from tm_sjkeluarwip WHERE no_sj = '".$this->db->escape_str($no_sj)."' ", false);
    $query = $this->db->get();
    $hasil = $query->result();
				
		// jika data header blm ada 
		if(count($hasil)== 0) {
			// insert di tm_sjkeluarwip
			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'jenis_keluar'=>$jenis,
			  'id_gudang'=>$id_gudang,
			  'kode_unit_jahit'=>$kode_unit_jahit,
			  'kode_unit_packing'=>$kode_unit_packing,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl,
			  'keterangan'=>$ket
			);
			$this->db->insert('tm_sjkeluarwip',$data_header);
		} // end if
		
			// ambil data terakhir di tabel tm_sjkeluarwip
			$query2	= $this->db->query(" SELECT id FROM tm_sjkeluarwip ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			
				// ======== update stoknya! =============
				$nama_tabel_stok = "tm_stok_hasil_jahit";
				
				//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
					$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." WHERE kode_brg_jadi = '$kode'
							AND id_gudang='$id_gudang' ");
					if ($query3->num_rows() == 0){
						$stok_lama = 0;
					}
					else {
						$hasilrow = $query3->row();
						$stok_lama	= $hasilrow->stok;
					}
					$new_stok = $stok_lama-$qty;
					
					if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
						$data_stok = array(
							'kode_brg_jadi'=>$kode,
							'id_gudang'=>$id_gudang,
							'stok'=>$new_stok,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert($nama_tabel_stok, $data_stok);
					}
					else {
						$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', tgl_update_stok = '$tgl' 
						where kode_brg_jadi= '$kode' AND id_gudang='$id_gudang' ");
					}
										
				
				// jika semua data tdk kosong, insert ke tm_sjkeluarwip_detail
				$data_detail = array(
					'id_sjkeluarwip'=>$id_sj,
					'kode_brg_jadi'=>$kode,
					'qty'=>$qty,
					'keterangan'=>$ket_detail,
					'ket_qty_warna'=>$ket_warna
				);
				$this->db->insert('tm_sjkeluarwip_detail',$data_detail);
			
		
  }
    
  function deletesjkeluar($kode){
	$tgl = date("Y-m-d");	
	
		// 1. reset stoknya
		// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT a.id_gudang, b.* FROM tm_sjkeluarwip_detail b, tm_sjkeluarwip a 
								 WHERE a.id = b.id_sjkeluarwip AND b.id_sjkeluarwip = '$kode' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						// ============ update stok =====================
						$nama_tabel_stok = "tm_stok_hasil_jahit";
										
						//cek stok terakhir tm_stok, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM ".$nama_tabel_stok." 
											 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row2->id_gudang' ");
								if ($query3->num_rows() == 0){
									$stok_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_lama	= $hasilrow->stok;
								}
								$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
								
								$this->db->query(" UPDATE ".$nama_tabel_stok." SET stok = '$new_stok', 
											tgl_update_stok = '$tgl' WHERE kode_brg_jadi= '$row2->kode_brg_jadi'
											AND id_gudang = '$row2->id_gudang' ");
								
					} // end foreach
				} // end if
	
	// 2. hapus data tm_sjkeluarwip_detail dan tm_sjkeluarwip
    $this->db->delete('tm_sjkeluarwip_detail', array('id_sjkeluarwip' => $kode));
    $this->db->delete('tm_sjkeluarwip', array('id' => $kode));
  }
    
  function get_sjkeluar($id_sj) {
		$query	= $this->db->query(" SELECT * FROM tm_sjkeluarwip where id = '$id_sj' ");
	
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {								
				// ambil data detailnya
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
											WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						$hasilrow = $query3->row();
						$nama_brg_jadi	= $hasilrow->e_product_motifname;
						
						$tabelstok = "tm_stok_hasil_jahit";
						$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." 
									 WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$row1->id_gudang' ");
						$hasilrow = $query3->row();
						if ($query3->num_rows() != 0) 
							$jum_stok	= $hasilrow->jstok;
						else
							$jum_stok = 0;
																						
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg_jadi'=> $row2->kode_brg_jadi,
												'nama'=> $nama_brg_jadi,
												'qty'=> $row2->qty,
												'ket_qty_warna'=> $row2->ket_qty_warna,
												'keterangan'=> $row2->keterangan,
												'jum_stok'=> $jum_stok
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$pisah1 = explode("-", $row1->tgl_sj);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl = $tgl1."-".$bln1."-".$thn1;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else {
					$nama_gudang = '';
				}
				
				// unit jahit
				if ($row1->kode_unit_jahit != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit_jahit' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_jahit	= $hasilrow->nama;
					}
					else
						$nama_unit_jahit = "";
				}
				else
					$nama_unit_jahit = "";
				
				// unit packing
				if ($row1->kode_unit_packing != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit = '$row1->kode_unit_packing' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_unit_packing	= $hasilrow->nama;
					}
					else
						$nama_unit_packing = "";
				}
				else
					$nama_unit_packing = "";
				
				if ($row1->jenis_keluar == '1')
					$nama_jenis = "Keluar bagus ke unit packing";
				else if ($row1->jenis_keluar == '2')
					$nama_jenis = "Keluar bagus ke gudang jadi";
				else if ($row1->jenis_keluar == '3')
					$nama_jenis = "Lain-lain (Retur ke unit jahit)";
				else
					$nama_jenis = "Lain-lain (Lainnya)";
				
				$data_sj[] = array(		'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl,
											'keterangan'=> $row1->keterangan,
											'jenis'=> $row1->jenis_keluar,
											'nama_jenis'=> $nama_jenis,
											'id_gudang'=> $row1->id_gudang,
											'nama_gudang'=> $nama_gudang,
											'kode_unit_jahit'=> $row1->kode_unit_jahit,
											'nama_unit_jahit'=> $nama_unit_jahit,
											'kode_unit_packing'=> $row1->kode_unit_packing,
											'nama_unit_packing'=> $nama_unit_packing,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
}
