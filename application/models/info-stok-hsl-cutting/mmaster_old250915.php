<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_stok($num, $offset, $cari) {
		/*$sql = " kode_brg, nama_brg from
				(select kode_brg, nama_brg from tm_barang UNION
				select kode_brg, nama_brg from tm_brg_hasil_makloon
				) as foo "; */
	
	$sql = " a.kode_brg, a.kode_brg_jadi, a.diprint, a.is_dacron, a.stok, a.tgl_update_stok FROM tm_stok_hasil_cutting a
			LEFT JOIN tm_barang b ON b.kode_brg=a.kode_brg
			LEFT JOIN tm_brg_hasil_makloon c ON c.kode_brg=a.kode_brg
			LEFT JOIN tr_product_motif d ON d.i_product_motif = a.kode_brg_jadi ";
	
	if ($cari != "all")
		$sql.= " WHERE UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')
				OR UPPER(c.nama_brg) like UPPER('%$cari%') OR UPPER(c.kode_brg) like UPPER('%$cari%')
				OR UPPER(d.i_product_motif) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%') ";
	$sql.= " ORDER BY a.tgl_update_stok, a.kode_brg_jadi, a.kode_brg "; 

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
			/*	$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$ekode_gudang	= $hasilrow->kode_gudang;
				$eid_gudang	= $hasilrow->id;
				$enama_gudang	= $hasilrow->nama;
				$enama_lokasi	= $hasilrow->nama_lokasi; */
				
				$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg	= $hasilrow->nama_brg;
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					//$satuan	= '';
					$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
					}
					else {
						$nama_brg = '';
						$satuan = '';
					}
				}
				
				// brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif 
								WHERE i_product_motif = '$row1->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_jadi	= $hasilrow->e_product_motifname;
				}
				else {
					$nama_brg_jadi	= '';
				}
								
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg,	
											'nama_brg'=> $nama_brg,
											/*'ekode_gudang'=> $ekode_gudang,
											'eid_gudang'=> $eid_gudang,
											'enama_gudang'=> $enama_gudang,
											'enama_lokasi'=> $enama_lokasi, */
											'satuan'=> $satuan,
											'kode_brg_jadi'=> $row1->kode_brg_jadi,	
											'nama_brg_jadi'=> $nama_brg_jadi,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'diprint'=> $row1->diprint,
											'is_dacron'=> $row1->is_dacron
											);
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stoktanpalimit($cari){
		$sql = " select a.kode_brg, a.kode_brg_jadi, a.stok, a.diprint, a.is_dacron, a.tgl_update_stok FROM tm_stok_hasil_cutting a
			LEFT JOIN tm_barang b ON b.kode_brg=a.kode_brg
			LEFT JOIN tm_brg_hasil_makloon c ON c.kode_brg=a.kode_brg
			LEFT JOIN tr_product_motif d ON d.i_product_motif = a.kode_brg_jadi ";
	
		if ($cari != "all")
			$sql.= " WHERE UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')
					OR UPPER(c.nama_brg) like UPPER('%$cari%') OR UPPER(a.kode_brg_jadi) like UPPER('%$cari%')
					OR UPPER(d.i_product_motif) like UPPER('%$cari%') OR UPPER(d.e_product_motifname) like UPPER('%$cari%') "; 
	  
		$query	= $this->db->query($sql);
    
    return $query->result();  
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
