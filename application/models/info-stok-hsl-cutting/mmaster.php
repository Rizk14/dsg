<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  // 15-03-2014, modif 25-09-2015
  function get_stok($num, $offset, $cstatus, $cari) {	
	$sql = " a.id, b.kode_brg, b.nama_brg, a.stok, a.tgl_update_stok FROM tm_stok_hasil_cutting a
			INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id WHERE b.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(b.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')) ";

	$sql.= " ORDER BY b.kode_brg ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	$detailwarna = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
					// stok per warna
					$queryxx	= $this->db->query(" SELECT c.nama, a.stok FROM tm_stok_hasil_cutting_warna a 
									INNER JOIN tm_warna c ON a.id_warna = c.id
									WHERE a.id_stok_hasil_cutting = '$row1->id' ");
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->result();
						foreach ($hasilxx as $rowxx) {
							$detailwarna[] = array(	'nama_warna'=> $rowxx->nama,
													'stok'=> $rowxx->stok
												);
						}
					}
					else
						$detailwarna = '';
					// --------------------------
													
				$data_stok[] = array(		'kode_brg_wip'=> $row1->kode_brg,	
											'nama_brg_wip'=> $row1->nama_brg,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'detailwarna'=> $detailwarna
											);
				$detailwarna = array();
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stoktanpalimit($cstatus, $cari){
	$sql = " SELECT a.id, b.kode_brg, b.nama_brg, a.stok, a.tgl_update_stok FROM tm_stok_hasil_cutting a
			INNER JOIN tm_barang_wip b ON a.id_brg_wip = b.id WHERE b.status_aktif = '$cstatus' ";
	
	if ($cari != "all")
		$sql.= " AND (UPPER(b.kode_brg) like UPPER('%$cari%') OR UPPER(b.nama_brg) like UPPER('%$cari%')) ";
	
	$query	= $this->db->query($sql);
    return $query->result();  
  }

}
