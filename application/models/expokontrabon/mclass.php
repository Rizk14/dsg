<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function logfiles($efilename,$iuserid) {
$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query(" SELECT to_char(current_timestamp,'yyyy-mm-dd HH:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$fields = array(
			'e_filename' => $efilename,
			'i_user_id' => $iuserid,
			'd_created' => $dentry
		);
		
		$db2->insert('tm_files_log',$fields);

		if($db2->trans_status()===FALSE) {
			$db2->trans_rollback();
		} else {
			$db2->trans_commit();
		}
	}

	function expokontrabon($ikontrabon,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($ikontrabon!=''){
			$idtcode	= " WHERE a.i_dt='$ikontrabon' ";	
			$fbatal		= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		} else {
			$idtcode	= "";
			$fbatal		= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}

		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				
				FROM tm_dt a 
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				 
				".$idtcode." ".$fbatal." ".$fnotasederhana."
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				ORDER BY a.i_dt_code ASC, a.d_dt ASC ");			
	}
	
	function expokontrabon_detail($idt,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT a.i_nota, c.i_faktur_code, a.d_nota, c.d_due_date, b.e_branch_name FROM tm_dt_item a 
			
			INNER JOIN tr_branch b ON b.i_branch_code=a.i_branch
			INNER JOIN tm_faktur_do_t c ON c.i_faktur=a.i_nota
			
			WHERE a.i_dt='$idt' GROUP BY a.i_nota, c.i_faktur_code, a.d_nota, c.d_due_date, b.e_branch_name ORDER BY a.i_nota ASC ");
		}elseif($f_nota_sederhana=='t'){
			return $db2->query(" SELECT a.i_nota, c.i_faktur_code, a.d_nota, c.d_due_date, b.e_branch_name FROM tm_dt_item a 
			
			INNER JOIN tr_branch b ON b.i_branch_code=a.i_branch
			INNER JOIN tm_faktur c ON c.i_faktur=a.i_nota
			
			WHERE a.i_dt='$idt' GROUP BY a.i_nota, c.i_faktur_code, a.d_nota, c.d_due_date, b.e_branch_name ORDER BY a.i_nota ASC ");			
		}
	}

	function vdiskon($inota,$fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($fnotasederhana=='f') {
			return $db2->query(" SELECT b.n_discount, b.v_discount
					
				FROM tm_faktur_do_t b
													
				WHERE b.f_faktur_cancel='f' AND b.i_faktur='$inota' ");
		}else{
			return $db2->query(" SELECT c.n_discount, c.v_discount 
					
				INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
					
				WHERE b.f_faktur_cancel='f' AND b.i_faktur='$inota' ");
		}	
	}
	
	function totalkontrabon($dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(a.v_total_grand) AS totalkontrabon, sum(a.v_grand_sisa) AS pelunasan 
		FROM tm_dt a 

		WHERE (a.d_dt BETWEEN '$dkontrabon_first' AND '$dkontrabon_last') AND a.f_dt_cancel='f' AND a.f_nota_sederhana='$f_nota_sederhana' ");
	}
	
	function clistkontrabonperpages($limit,$offset,$ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if(($dkontrabon_first!='' || $dkontrabon_first!='0') && ($dkontrabon_last!='' || $dkontrabon_last!='0')){
			$ddt	= " WHERE (a.d_dt BETWEEN '$dkontrabon_first' AND '$dkontrabon_last') ";	
			$fbatal	= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}else{
			$ddt	= "";
			$fbatal	= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}

		if($f_nota_sederhana=='f') {
			$query	= $db2->query(" SELECT c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota	
					INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
			$query	= $db2->query(" SELECT c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota
					INNER JOIN tm_faktur_item e ON e.i_faktur=c.i_faktur
					INNER JOIN tm_sj f ON f.i_sj=e.i_sj
					INNER JOIN tr_branch d ON d.i_branch_code=f.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset." ");		
		}
										
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}			
	}
			
	function clistkontrabon($ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
$db2=$this->load->database('db_external', TRUE);
		if(($dkontrabon_first!='' || $dkontrabon_first!='0') && ($dkontrabon_last!='' || $dkontrabon_last!='0')) {
			$ddt	= " WHERE (a.d_dt BETWEEN '$dkontrabon_first' AND '$dkontrabon_last') ";
			$fbatal	= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}else{
			$ddt	= "";
			$fbatal	= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}
		
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT  c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_nota	
					INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC ");
		}else{
			return $db2->query(" SELECT c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, 
					b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota	
					INNER JOIN tm_faktur_item e ON e.i_faktur=c.i_faktur
					INNER JOIN tm_sj f ON f.i_sj=e.i_sj
					INNER JOIN tr_branch d ON d.i_branch_code=f.i_branch
					
					".$ddt." ".$fbatal." ".$fnotasederhana."
					
					GROUP BY c.i_faktur_code, b.i_nota, b.d_nota, b.i_customer, b.i_branch, d.e_branch_name, b.i_product, b.e_product_name, a.i_dt, a.i_dt_code, a.d_dt, a.v_grand_sisa, a.f_pelunasan
					ORDER BY a.i_dt_code ASC, c.i_faktur_code ASC, b.i_product ASC ");
		}
	}

	function clistkontrabonperpages2($limit,$offset,$ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($ikontrabon!=''){
			$idtcode	= " WHERE a.i_dt='$ikontrabon' ";	
			$fbatal		= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		} else {
			$idtcode	= "";
			$fbatal		= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}

		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				
				FROM tm_dt a 
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				 
				".$idtcode." ".$fbatal." ".$fnotasederhana."
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				ORDER BY a.i_dt_code ASC, a.d_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
							
			if($query->num_rows() > 0) {
				return $result	= $query->result();
			}			
	}
			
	function clistkontrabon2($ikontrabon,$dkontrabon_first,$dkontrabon_last,$f_nota_sederhana) {
$db2=$this->load->database('db_external', TRUE);
		if($ikontrabon!=''){
			$idtcode	= " WHERE a.i_dt='$ikontrabon' ";	
			$fbatal		= " AND a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		} else {
			$idtcode	= "";
			$fbatal		= " WHERE a.f_dt_cancel='f' ";
			$fnotasederhana = "AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}

		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				
				FROM tm_dt a 
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				 
				".$idtcode." ".$fbatal." ".$fnotasederhana."
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_discount, a.v_ppn, a.v_total_grand, a.v_grand_sisa, a.f_pelunasan 
				ORDER BY a.i_dt_code ASC, a.d_dt ASC ");						
	}

	function lbarangjadi($fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt AND a.f_nota_sederhana='$fnotasederhana'
				
				WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana'
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				
				ORDER BY a.i_dt_code ASC, a.d_dt ASC ");		
	}	

	function lbarangjadiperpages($limit,$offset,$fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		/*	
		$query	= $db2->query("
				SELECT a.i_dt_code AS idtcode, a.d_dt AS ddt
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND a.f_pelunasan='f' GROUP BY a.i_dt_code, a.d_dt
				
				ORDER BY a.i_dt_code ASC, a.d_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
		*/

		$query	= $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$fnotasederhana' 
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				
				ORDER BY a.i_dt_code ASC, a.d_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function flbarangjadi($key,$fnotasederhana) {
$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT a.i_dt AS idt, a.i_dt_code AS idtcode, a.d_dt AS ddt, a.f_nota_sederhana
				
				FROM tm_dt a
				
				INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
				
				WHERE a.f_dt_cancel='f' AND (a.i_dt_code LIKE '$key%') AND a.f_nota_sederhana='$fnotasederhana'
				
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.f_nota_sederhana
				
				ORDER BY a.i_dt_code ASC, a.d_dt ASC ");
	}	
	
	function lfaktur($f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
		return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn
						
			FROM tm_faktur_do_t b
			
			INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
			INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
			
			WHERE b.f_faktur_cancel='f'
				
			GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn ORDER BY b.i_faktur_code ASC ");						
		}else{
			return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
							
				FROM tm_faktur b
				
				INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj d ON d.i_sj=a.i_sj
				INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
				
				WHERE b.f_faktur_cancel='f'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC ");		
		}	
	}
	
	function lfakturperpages($limit,$offset,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			$query	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn
							
				FROM tm_faktur_do_t b
				
				INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
				INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
				
				WHERE b.f_faktur_cancel='f'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn ORDER BY b.i_faktur_code ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
			$query	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
							
				FROM tm_faktur b
				
				INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj d ON d.i_sj=a.i_sj
				INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
				
				WHERE b.f_faktur_cancel='f'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC LIMIT ".$limit." OFFSET ".$offset." ");		
		}
		
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}		
	}
	
	function flfaktur($key,$f_nota_sederhana){	
		$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {	
			return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn
							
				FROM tm_faktur_do_t b
				
				INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
				INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
				
				WHERE b.f_faktur_cancel='f' AND b.i_faktur_code LIKE '$key%'
				
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, n_discount, v_discount, v_total_faktur, v_total_fppn ORDER BY b.i_faktur_code ASC ");
		}else{
			return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
							
				FROM tm_faktur b
				
				INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
				INNER JOIN tm_sj d ON d.i_sj=a.i_sj
				INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
				
				WHERE b.f_faktur_cancel='f' AND b.i_faktur_code LIKE '$key%'
					
				GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC ");			
		}		
	}
	
	function getdtheader2($idt,$fnotasederhana){
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_dt WHERE i_dt='$idt' AND f_dt_cancel='f' AND f_nota_sederhana='$fnotasederhana' ");
	}
	
	function totalfaktur($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_do_item_t WHERE i_faktur='$ifaktur' ");
	}

	function totalfakturNONDO($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_item WHERE i_faktur='$ifaktur' ");
	}
	
	function ldtitem2($idt,$fnotasederhana){
		$db2=$this->load->database('db_external', TRUE);
		if($fnotasederhana=='f') {
			$query	= $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn
					
					FROM tm_faktur_do_t b
					
					INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
					INNER JOIN tm_dt_item d ON d.i_nota=b.i_faktur
					INNER JOIN tm_dt e ON e.i_dt=d.i_dt
					INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
								
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='t' AND e.i_dt='$idt'
							
					GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn 
	
					ORDER BY b.i_faktur_code ASC ");
		}else{
		$query	= $db2->query(" SELECT c.i_faktur, c.i_faktur_code, d.e_branch_name, c.d_faktur, c.d_due_date, d.i_customer, d.i_branch_code, d.e_branch_name, c.n_discount, c.v_discount, c.v_total_faktur, c.v_total_fppn 
					
					FROM tm_dt a 
					INNER JOIN tm_dt_item b ON b.i_dt=a.i_dt
					INNER JOIN tm_faktur c ON c.i_faktur=b.i_nota	
					INNER JOIN tm_faktur_item e ON e.i_faktur=c.i_faktur
					INNER JOIN tm_sj f ON f.i_sj=e.i_sj
					INNER JOIN tr_branch d ON d.i_branch_code=f.i_branch
					
					WHERE c.f_faktur_cancel='f' AND c.f_kontrabon='t' AND a.i_dt='$idt'
					
					GROUP BY c.i_faktur, c.i_faktur_code, d.e_branch_name, c.d_faktur, c.d_due_date, d.i_customer, d.i_branch_code, d.e_branch_name, c.n_discount, c.v_discount, c.v_total_faktur, c.v_total_fppn 
					
					ORDER BY c.i_faktur_code ASC ");		
		}
		
		if($query->num_rows()>0){
			return $result	= $query->result();
		}			
	}

	function cari_dt($idtcode,$idtcodehidden) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_dt WHERE i_dt_code='$idtcode' AND i_dt_code!='$idtcodehidden' AND f_dt_cancel='f' ");
	}
}
?>
