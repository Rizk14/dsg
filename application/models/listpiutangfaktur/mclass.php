<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function totalfaktur($ifaktur,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.v_total, a.v_sisa AS piutang FROM tm_dt_faktur a
						INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
						
						WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
						
						GROUP BY a.v_total, a.v_sisa ");		
	}
	
	function clistfaktur1_old($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa AS piutang FROM tm_dt_faktur a 
					INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt 
					WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndfakturfirst' AND '$ndfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana'
					GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
			$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa AS piutang FROM tm_dt_faktur a 
					INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt 
					WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndfakturfirst' AND '$ndfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana'
					GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");			
		}
				
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}			
	}
function clistfaktur1($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
		
			$db2=$this->load->database('db_external', TRUE);
			$query	= $db2->query("  SELECT x.i_faktur, x.i_faktur_code, x.d_faktur, x.v_total_faktur,
			b.i_dt,b.i_dt_code,b.d_dt,b.v_total,x.v_grand_sisa as piutang
			
			 FROM (
		SELECT i_faktur, i_faktur_code,d_faktur, v_total_faktur,v_grand_sisa FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur, v_total_faktur,v_grand_sisa FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur, v_total_faktur,v_grand_sisa FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		)x LEFT OUTER JOIN tm_dt_faktur b ON x.i_faktur=b.i_faktur where 
		  (x.d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast') 
		ORDER BY b.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
		
	
				
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}			
	}
	function clistfaktur2_old($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
	$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			$query	= $db2->query(" SELECT b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt_faktur a
							INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_faktur
							INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
							
							WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
							
							GROUP BY b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
							
							ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
			$query	= $db2->query(" SELECT b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt_faktur a
							INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur c ON c.i_faktur=b.i_faktur
							INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
							
							WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
							
							GROUP BY b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
							
							ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");			
		}
		
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
	
function clistfaktur2($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
			return $db2->query("  SELECT x.i_faktur, x.i_faktur_code, x.d_faktur, x.e_branch, x.v_total_faktur,
			b.i_dt,b.i_dt_code,b.d_dt,b.v_total,x.v_grand_sisa as piutang (
		SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur,v_grand_sisa FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		)x LEFT OUTER JOIN tm_dt_faktur b ON x.i_faktur=b.i_faktur where x.i_faktur='$ifaktur' AND f_dt_cancel='f'
		ORDER BY b.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
			
	}
	
	function vtotalfaktur($limit,$offset,$ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		/*
		if($f_nota_sederhana=='f') {
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt_faktur a
							INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt

							INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_faktur
							
							WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
			
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}else{
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt_faktur a

							INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur c ON c.i_faktur=b.i_faktur

							WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
							
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");			
		}
		*/

		if($ndfakturfirst!='' && $ndfakturlast!='') {
			if($ndfakturfirst!='0' && $ndfakturlast!='0') {
				$filter = " WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndfakturfirst' AND '$ndfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana' ";
			}else{
				$filter = " WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana' ";
			}
		}else{
			$filter = " WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana' ";
		}
		
		if($f_nota_sederhana=='f') {
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt_faktur a
							INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_faktur
							
							".$filter."
			
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");
		}else{
			$query = $db2->query(" SELECT c.v_total_fppn AS v_total_fppn FROM tm_dt_faktur a
							INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
							INNER JOIN tm_faktur c ON c.i_faktur=b.i_faktur
							
							".$filter."
							
							GROUP BY c.v_total_fppn, a.i_dt ORDER BY a.i_dt ASC ");			
		}

		if($query->num_rows()>0) {
			return $query->result();
		}
	}
			
	function clistfakturallpage1_old($ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
			$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa AS piutang FROM tm_dt_faktur a 
				INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt 
				WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndfakturfirst' AND '$ndfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana'
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa ORDER BY a.i_dt ASC ");
		}else{
			return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa AS piutang FROM tm_dt_faktur a 
				INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt 
				WHERE a.f_dt_cancel='f' AND (a.d_dt BETWEEN '$ndfakturfirst' AND '$ndfakturlast') AND a.f_nota_sederhana='$f_nota_sederhana'
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt, a.v_total, a.v_sisa ORDER BY a.i_dt ASC ");			
		}
	}

function clistfakturallpage1($ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {
		$db2=$this->load->database('db_external', TRUE);
			return $db2->query("  SELECT * FROM (
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		)x LEFT OUTER JOIN tm_dt_faktur b ON x.i_faktur=b.i_faktur where (x.d_faktur BETWEEN '$ndfakturfirst' AND '$ndfakturlast')");			
		}
	

	function clistfakturallpage2_old($ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {	
			$db2=$this->load->database('db_external', TRUE);
		if($f_nota_sederhana=='f') {
			return $db2->query(" SELECT b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt_faktur a
				INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
				INNER JOIN tm_faktur_do_t c ON c.i_faktur=b.i_faktur
				INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
						
				WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
						
				GROUP BY b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
						
				ORDER BY a.i_dt ASC ");
		}else{
			return $db2->query(" SELECT b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt_faktur a
				INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt
				INNER JOIN tm_faktur c ON c.i_faktur=b.i_faktur
				INNER JOIN tr_branch d ON d.i_branch_code=b.i_branch
						
				WHERE a.f_dt_cancel='f' AND a.i_dt='$ifaktur' AND a.f_nota_sederhana='$f_nota_sederhana'
						
				GROUP BY b.i_faktur, c.i_faktur_code, b.d_faktur, c.d_due_date, b.i_branch, d.e_branch_name, c.v_total_fppn, a.i_dt, a.i_dt_code, a.d_dt
						
				ORDER BY a.i_dt ASC ");
		}		
	}
	function clistfakturallpage2($ifaktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana) {	
			$db2=$this->load->database('db_external', TRUE);
			return $db2->query(" SELECT * FROM (
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		)x LEFT OUTER JOIN tm_dt_faktur b ON x.i_faktur=b.i_faktur where x.i_faktur='$ifaktur' ");
			
	}
	function lfaktur_old($ffaktursederhana) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt_faktur a 
		
				INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt 
						
				WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$ffaktursederhana'
						
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt ORDER BY a.i_dt ASC ");
	}	
	function lfaktur($ffaktursederhana) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" 
		SELECT * FROM (
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		)x ORDER BY d_faktur DESC
		");
	}	
	function lfakturperpages($limit,$offset,$ffaktursederhana) {	
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query(" SELECT * FROM (
		SELECT i_faktur , i_faktur_code,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		)x ORDER BY d_faktur DESC LIMIT ".$limit." OFFSET ".$offset." ");
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}

	function lfakturperpages_old($limit,$offset,$ffaktursederhana) {	
			$db2=$this->load->database('db_external', TRUE);	
		$query	= $db2->query(" SELECT a.i_dt, a.i_dt_code, a.d_dt FROM tm_dt_faktur a 
		
				INNER JOIN tm_dt_faktur_item b ON b.i_dt=a.i_dt 
						
				WHERE a.f_dt_cancel='f' AND a.f_nota_sederhana='$ffaktursederhana'
						
				GROUP BY a.i_dt, a.i_dt_code, a.d_dt ORDER BY a.i_dt ASC LIMIT ".$limit." OFFSET ".$offset." ");
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function flfaktur($key,$ffaktursederhana) {	
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM (
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_bhnbaku WHERE f_faktur_cancel='f'
		UNION
		SELECT i_faktur, i_faktur_code,d_faktur FROM tm_faktur_do_t  WHERE f_faktur_cancel='f'
		)x WHERE i_faktur_code='$key'  ORDER BY d_faktur DESC ");
				
	}
	 
}
?>
