<?php
class Approve_makloon_baju_pengadaan_model extends MY_Model
{
	protected $_per_page = 10;
    protected $_tabel = 'tb_makloon_baju_gudang_jadi_wip';
    public function __construct()
	{
			parent::__construct();
			 $this->db4 = $this->load->database('db_additional',TRUE);
	}
     public function get_all_paged_custom()
    {
       $query= $this->db4->query("SELECT * from tb_makloon_baju_gudang_jadi_wip");
        if($query->num_rows() > 0){
			$hasil=$query->result();
			foreach ($hasil as $row){
				
				        $query2= $this->db4->query("SELECT nama_gudang from tb_master_gudang where id='$row->id_gudang'");
						if($query2->num_rows() > 0){
						$hasil2=$query2->row();
					$nama_gudang_masuk=$hasil2->nama_gudang;
					}
					
					$query3= $this->db4->query("SELECT nama_unit_packing from tb_master_unit_packing where id='$row->id_unit_packing'");
						if($query3->num_rows() > 0){
						$hasil3=$query3->row();
					$nama_unit_packing=$hasil3->nama_unit_packing;
					}
					else{
							$nama_unit_packing='';
						}
					
					$query4= $this->db4->query("SELECT nama_unit_jahit from tb_master_unit_jahit where id='$row->id_unit_jahit'");
						if($query4->num_rows() > 0){
						$hasil4=$query4->row();
					$nama_unit_jahit=$hasil4->nama_unit_jahit;
					}
					else{
							$nama_unit_jahit='';
						}
					
					$data[]=array(
	'id'=>$row->id,
	'nama_gudang_masuk'=>$nama_gudang_masuk,
	'nama_unit_packing'=>$nama_unit_packing,
	'nama_unit_jahit'=>$nama_unit_jahit,
	'no_sj'=>$row->no_sj,
	'tanggal_sj'=>$row->tanggal_sj,
	'jenis_masuk'=>$row->jenis_masuk,
	'status_approve'=>$row->status_approve
	
	);
				}
			return $data;
			
			}
         
	
                  
    }

    public function cari($offset)
    {
        $this->get_real_offset($offset);
        $kata_kunci = $this->input->get('kata_kunci', true);
       

        return $this->db4->where("( username LIKE '%$kata_kunci%')")
                        ->limit($this->_per_page, $this->_offset)
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->result();
    }

    public function cari_num_rows()
    {
        $kata_kunci = $this->input->get('kata_kunci', true);
       

        return $this->db4->where("( username LIKE '%$kata_kunci%')")
                        ->order_by('id', 'ASC')
                        ->get($this->_tabel)
                        ->num_rows();
    }

    public function ubah_status_approve($id, $status)
    {
        if ($status == 'f') {
            $this->db4->set('status_approve', 't');
        } else {
            $this->db4->set('status_approve', 'f');
        }
        $this->db4->where('id', $id);
        return $this->db4->update($this->_tabel);
    }

  public function getAllDetail($id)
    {
      $query =$this->db4->query("SELECT * FROM tb_makloon_baju_gudang_jadi_wip");
	
	if($query->num_rows > 0){
		$hasil = $query->result();
		foreach($hasil as $row){
			
			$query2 =$this->db4->query("SELECT id_barang_wip,qty,keterangan 
			from tb_makloon_baju_gudang_jadi_wip_detail where id_makloon_baju_gudang_jadi_wip ='$row->id'");
			
			if($query2->num_rows > 0){
				$hasil2 =$query2->row();
				$id_barang_wip =$hasil2->id_barang_wip;
				$qty =$hasil2->qty;
				$keterangan_detail=$hasil2->keterangan;
				}
				
				$query3=$this->db4->query("SELECT nama_barang_wip,kode_barang_wip FROM tb_master_barang_wip 
				where id='$id_barang_wip'");
				if($query3->num_rows > 0){
					$hasil3 =$query3->row();
					$nama_barang_wip=$hasil3->nama_barang_wip;
					$kode_barang_wip=$hasil3->kode_barang_wip;
					}

		
			}
			$detail_data[]=array(
					
					'nama_barang_wip'=>$nama_barang_wip,
					'kode_barang_wip'=>$kode_barang_wip,
					'id_barang_wip'=>$id_barang_wip,
					'qty'=>$qty,
					'keterangan_detail'=>$keterangan_detail
				);
			
			$header_data[]=array(
			'id'=>$row->id,
			'no_sj'=>$row->no_sj,
			'tanggal_sj'=>$row->tanggal_sj,
			'jenis_masuk'=>$row->jenis_masuk,
			'id_unit_jahit'=>$row->id_unit_jahit,
			'id_unit_packing'=>$row->id_unit_packing,
			'id_gudang'=>$row->id_gudang,
			'keterangan_header'=>$row->keterangan,
			'detail_data'=>$detail_data
		);

		
	    
    }
    else{
			$header_data[]=array();
			}
		return $header_data;    
    
}

 public function get_unit_jahit()
    {
     $sql=$this->db4->query("SELECT id,nama_unit_jahit FROM tb_master_unit_jahit order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }    
    public function get_unit_packing()
    {
     $sql=$this->db4->query("SELECT * FROM tb_master_unit_packing order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }  
    public function get_gudang()
    {
     $sql=$this->db4->query("SELECT * FROM tb_master_gudang order by id");
          
     if($sql->num_rows() > 0){
		 return $sql->result();
		 }
    }
    
}
