<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function namacabang($ibranch) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tr_branch WHERE i_branch_code='$ibranch' ");
	}
	
	function clistbrgpendding($fdate,$d_op_first,$d_op_last,$stp,$iorder,$ordermotif,$limit,$offset) {
			$db2=$this->load->database('db_external', TRUE);
		/***
		$query	= $db2->query( "
				SELECT  a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount,
					a.d_op
					
				FROM tm_op_item b 
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
				WHERE (a.d_op BETWEEN '$d_op_first' AND '$d_op_last') AND a.f_op_cancel='f' AND (a.f_do_created='f' OR a.f_do_created='t') AND b.f_do_created='f' AND d.f_stop_produksi='$stp' "." ORDER BY a.i_op_code DESC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset );
		***/
		
		/* Disabled 10112011
		$query	= $db2->query( "
				SELECT  a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount,
					a.d_op
					
				FROM tm_op_item b 
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
				WHERE (a.d_op BETWEEN '$d_op_first' AND '$d_op_last') AND a.f_op_cancel='f' AND b.f_do_created='f' AND d.f_stop_produksi='$stp' ".$iorder." ".$ordermotif." ORDER BY a.i_op_code DESC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset );		
		*/ 

		$query	= $db2->query(" SELECT  a.d_op AS dop,
					a.i_op_code AS iopcode,
					a.i_branch AS ibranch,
					b.i_product AS iproduct,
					c.e_product_motifname AS motifname,
					(b.n_count-b.n_residual) AS dobarang,
					b.n_residual AS sisa,
					d.v_unitprice AS hjp,
					(b.n_residual*d.v_unitprice) AS amount
					
				FROM tm_op_item b 
				
				LEFT JOIN tm_op a ON a.i_op=b.i_op
				INNER JOIN tr_product_motif c ON c.i_product_motif=b.i_product
				INNER JOIN tr_product_base d ON d.i_product_base=c.i_product
				
				WHERE ".$fdate." a.f_op_cancel='f' AND b.f_do_created='f' AND d.f_stop_produksi='$stp' ".$iorder." ".$ordermotif." ORDER BY a.i_op_code ASC, b.i_product ASC LIMIT ".$limit." OFFSET ".$offset );
		
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
}
?>
