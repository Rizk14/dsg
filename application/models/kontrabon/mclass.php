<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function carinokontrabon($nokontrabon) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_dt WHERE i_dt_code=trim('$nokontrabon') AND f_dt_cancel='f' ");
	}
	
	function get_nokontrabon() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_dt_code AS character varying),5,(LENGTH(cast(i_dt_code AS character varying))-4)) AS icode FROM tm_dt WHERE f_dt_cancel='f' ORDER BY i_dt DESC LIMIT 1 ");
	}

	function get_thnkontrabon() {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT SUBSTRING(cast(i_dt_code AS character varying),1,4) AS thn FROM tm_dt WHERE f_dt_cancel='f' ORDER BY i_dt DESC LIMIT 1 ");
	}
	
	function lfaktur($fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($fnotasederhana=='f') {
			return $db2->query(" SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.i_faktur
					
					FROM tm_faktur_do_item_t a 
					
					INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
					INNER JOIN tm_do c ON c.i_do=a.i_do
					INNER JOIN tm_do_item d ON d.i_do=c.i_do
					INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product
					INNER JOIN tr_product_base f ON f.i_product_base=e.i_product
					
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='f' AND LENGTH(CAST(b.i_faktur_code AS character varying)) > 7
					AND b.d_faktur >='2012-01-01'
					GROUP BY b.d_faktur, b.i_faktur_code, b.i_faktur
					
					ORDER BY b.i_faktur_code DESC ");
		}else{
			return $db2->query(" SELECT b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.i_faktur
					
					FROM tm_faktur_item a 
					
					INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur
					
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='f' AND LENGTH(CAST(b.i_faktur_code AS character varying)) > 7
					
					GROUP BY b.d_faktur, b.i_faktur_code, b.i_faktur
					
					ORDER BY b.i_faktur_code ASC ");	
		}		
	}
		
	function lfakturperpages($limit,$offset,$fnotasederhana) {	
		$db2=$this->load->database('db_external', TRUE);
		if($fnotasederhana=='f') {
			$query	= $db2->query("
					SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.i_faktur
										
					FROM tm_faktur_do_item_t a 
					
					INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
					INNER JOIN tm_do c ON c.i_do=a.i_do 
					INNER JOIN tm_do_item d ON d.i_do=c.i_do 
					INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
					INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
					
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='f' AND LENGTH(CAST(b.i_faktur_code AS character varying)) > 7
					AND b.d_faktur >='2012-01-01'
					GROUP BY b.d_faktur, b.i_faktur_code, b.i_faktur
					
					ORDER BY b.i_faktur_code DESC LIMIT ".$limit." OFFSET ".$offset." ");
		}else{
			$query	= $db2->query("
					SELECT b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.i_faktur
										
					FROM tm_faktur_item a 
					
					INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur
										
					WHERE b.f_faktur_cancel='f' AND b.f_kontrabon='f' AND LENGTH(CAST(b.i_faktur_code AS character varying)) > 7
					
					GROUP BY b.d_faktur, b.i_faktur_code, b.i_faktur
					
					ORDER BY b.i_faktur_code ASC LIMIT ".$limit." OFFSET ".$offset." ");		
		}
		if($query->num_rows() > 0 ) {
			return $result = $query->result();
		}
	}

	function flfaktur($key,$fnotasederhana) {
		$db2=$this->load->database('db_external', TRUE);
		if($fnotasederhana=='f') {
			return $db2->query("
					SELECT count(cast(b.i_faktur_code AS integer)) AS jmlifaktur, 
						b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.i_faktur
					
					FROM tm_faktur_do_item_t a 
					
					INNER JOIN tm_faktur_do_t b ON b.i_faktur=a.i_faktur
					INNER JOIN tm_do c ON c.i_do=a.i_do 
					INNER JOIN tm_do_item d ON d.i_do=c.i_do 
					INNER JOIN tr_product_motif e ON e.i_product_motif=d.i_product 
					INNER JOIN tr_product_base f ON f.i_product_base=e.i_product 
					
					WHERE b.i_faktur_code='$key' AND b.f_faktur_cancel='f' AND b.f_kontrabon='f' AND LENGTH(CAST(b.i_faktur_code AS character varying)) > 7
					
					GROUP BY b.d_faktur, b.i_faktur_code, b.i_faktur
					
					ORDER BY b.i_faktur_code ASC LIMIT 2000 ");
		}else{
			return $db2->query("
					SELECT b.i_faktur_code AS ifakturcode,
						b.d_faktur AS dfaktur,
						b.i_faktur
					
					FROM tm_faktur_item a 
					
					INNER JOIN tm_faktur b ON b.i_faktur=a.i_faktur
										
					WHERE b.i_faktur_code='$key' AND b.f_faktur_cancel='f' AND b.f_kontrabon='f' AND LENGTH(CAST(b.i_faktur_code AS character varying)) > 7
					
					GROUP BY b.d_faktur, b.i_faktur_code, b.i_faktur
					
					ORDER BY b.i_faktur_code ASC LIMIT 2000 ");
		
		}		
	}

	function clistfaktur($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn, b.v_materai_sisa as v_materai
						
			FROM tm_faktur_do_t b
			
			INNER JOIN tm_faktur_do_item_t a ON b.i_faktur=a.i_faktur
			INNER JOIN tr_branch c ON c.e_initial=b.e_branch_name
			
			WHERE b.i_faktur='$ifaktur' AND b.f_faktur_cancel='f' AND b.f_kontrabon='f'
			
			GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, c.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC ");
	}

	function clistfakturNONDO($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn, b.v_materai_sisa as v_materai
					
			FROM tm_faktur b
			
			INNER JOIN tm_faktur_item a ON b.i_faktur=a.i_faktur
			INNER JOIN tm_sj d ON d.i_sj=a.i_sj
			INNER JOIN tr_branch c ON c.i_branch_code=d.i_branch
			
			WHERE b.i_faktur='$ifaktur' AND b.f_faktur_cancel='f' AND b.f_kontrabon='f'
				
			GROUP BY b.i_faktur, b.i_faktur_code, b.e_branch_name, b.d_faktur, b.d_due_date, d.i_customer, c.i_branch_code, c.e_branch_name, 
			b.n_discount, b.v_discount, b.v_total_faktur, b.v_total_fppn ORDER BY b.i_faktur_code ASC ");
	}
	
	function totalfaktur($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_do_item_t WHERE i_faktur='$ifaktur' ");
	}

	function totalfakturNONDO($ifaktur) {
		$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT sum(n_quantity*v_unit_price) AS totalfaktur FROM tm_faktur_item WHERE i_faktur='$ifaktur' ");
	}
			
	function msimpan($i_faktur_code,$i_faktur,$tgl_faktur,$due_date,$e_branch_name,$v_total_plusppn,$i_customer,$i_branch_code,$totalgrandhidden,$totalhidden,$discounthidden,$nilai_ppnhidden,$tglkontrabon,$iteration,$f_nota_sederhana,$no_kontrabon){
		$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();

		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date; 	
		
		$date_dt = date("Y-m-d");
		
		$qtm_dt	= $db2->query(" SELECT i_dt FROM tm_dt ORDER BY i_dt DESC LIMIT 1 ");
		if($qtm_dt->num_rows()>0){
			$rtm_dt = $qtm_dt->row();
			$idt	= $rtm_dt->i_dt+1;
		}else{
			$idt	= 1;
		}
		
		
		
		$db2->query(" INSERT INTO tm_dt (i_dt,i_dt_code,d_dt,v_total,v_discount,v_ppn,v_total_grand,v_grand_sisa,d_entry,d_update,f_nota_sederhana) 
			
			VALUES('$idt','$no_kontrabon','$tglkontrabon','$totalhidden','$discounthidden','$nilai_ppnhidden','$totalgrandhidden','$totalgrandhidden','$dentry','$dentry','$f_nota_sederhana') ");
		
		
		for($jumlah=0;$jumlah<=$iteration;$jumlah++) {
			
			$qtm_dt_faktur	= $db2->query(" SELECT i_dt FROM tm_dt_faktur ORDER BY i_dt DESC LIMIT 1 ");
		if($qtm_dt_faktur->num_rows()>0){
			$rtm_dt_faktur = $qtm_dt_faktur->row();
			$idt_faktur	= $rtm_dt_faktur->i_dt+1;
		}else{
			$idt_faktur	= 1;
		}
			
			$rep_v_total_plusppn=str_replace(",","",$v_total_plusppn[$jumlah]);
			$db2->query(" INSERT INTO tm_dt_faktur (i_dt,i_dt_code,d_dt,i_faktur,v_total,i_customer,i_branch,v_discount,v_ppn,v_total_grand,v_grand_sisa,d_entry,d_update,f_nota_sederhana,v_sisa) 
			VALUES('$idt_faktur','$i_faktur_code[$jumlah]','$tgl_faktur[$jumlah]','$i_faktur[$jumlah]','$rep_v_total_plusppn','$i_customer[$jumlah]','$i_branch_code[$jumlah]','$discounthidden',
			'$nilai_ppnhidden','$totalgrandhidden','$totalgrandhidden','$dentry','$dentry','$f_nota_sederhana','$rep_v_total_plusppn') ");
		
			
			if($f_nota_sederhana=='f') {
				$qfakturitem = $db2->query(" SELECT * FROM tm_faktur_do_item_t WHERE i_faktur='$i_faktur[$jumlah]' ");
			}else{
				$qfakturitem = $db2->query(" SELECT * FROM tm_faktur_item WHERE i_faktur='$i_faktur[$jumlah]' ");
			}
			
			if($qfakturitem->num_rows()>0) {
				
				$result = $qfakturitem->result();
				
				foreach($result as $row) {

					$qi_dt_item	= $db2->query(" SELECT cast(i_dt_item AS integer)+1 AS i_dt_item FROM tm_dt_item ORDER BY i_dt_item DESC LIMIT 1 ");
					
					if($qi_dt_item->num_rows()>0) {
						$ri_dt_item = $qi_dt_item->row();
						$i_dt_item 	= $ri_dt_item->i_dt_item;
					}else{
						$i_dt_item 	= 1;
					}
			
				
					
					$db2->query(" INSERT INTO tm_dt_item(i_dt_item,i_dt,i_nota,d_nota,i_customer,i_branch,i_product,e_product_name,d_entry,f_nota_sederhana) 
					
								VALUES('$i_dt_item','$idt','$i_faktur[$jumlah]','$tgl_faktur[$jumlah]','$i_customer[$jumlah]','$i_branch_code[$jumlah]','$row->i_product','$row->e_product_name','$dentry','$f_nota_sederhana') ");
				
				$qi_dt_item	= $db2->query(" SELECT cast(i_dt_item AS integer)+1 AS i_dt_item FROM tm_dt_faktur_item ORDER BY i_dt_item DESC LIMIT 1 ");
					
					if($qi_dt_item->num_rows()>0) {
						$ri_dt_faktur_item = $qi_dt_item->row();
						$i_dt_faktur_item 	= $ri_dt_faktur_item->i_dt_item;
					}else{
						$i_dt_faktur_item 	= 1;
					}
				
				$db2->query(" INSERT INTO tm_dt_faktur_item(i_dt_item,i_dt,i_faktur,d_faktur,i_customer,i_branch,i_product,e_product_name,d_entry,f_nota_sederhana) 
					
								VALUES('$i_dt_faktur_item','$idt_faktur','$i_faktur[$jumlah]','$tgl_faktur[$jumlah]','$i_customer[$jumlah]','$i_branch_code[$jumlah]','$row->i_product','$row->e_product_name','$dentry','$f_nota_sederhana') ");
				}
				
				if($f_nota_sederhana=='f') {
					$db2->query(" UPDATE tm_faktur_do_t SET f_kontrabon='t' WHERE i_faktur='$i_faktur[$jumlah]' AND f_faktur_cancel='f' ");
				}else{
					$db2->query(" UPDATE tm_faktur SET f_kontrabon='t' WHERE i_faktur='$i_faktur[$jumlah]' AND f_faktur_cancel='f' ");
					$db2->query(" UPDATE tm_dt SET f_nota_sederhana='t' WHERE i_dt_code='$no_kontrabon'");

				}
			}
		}
		
		if($db2->trans_status()===FALSE) {
			$ok	= 0;
			$db2->trans_rollback();
		} else {
			$ok	= 1;
			$db2->trans_commit();
		}

		if($ok==1) {
			print "<script>alert(\"Kontra Bon: '\"+$no_kontrabon+\"' telah disimpan, terimakasih.\");window.open(\"index\", \"_self\");</script>";
		} else {
			print "<script>alert(\"Maaf, Kontra Bon gagal disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		}
	}
	
}
?>
