<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
    
  function cek_saldoawal($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hutang_dagang_wip
						WHERE bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$saldoawal = array('idnya'=> $idnya
							);
							
		return $saldoawal;
  }

  function get_all_transaksi_pembelian_wip_kredit($bulan, $tahun) {
	  // ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		$sql = " SELECT id, nama_kelompok FROM tm_kelompok_unit ORDER BY id ";
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detailsup = array();			
			foreach ($hasil as $row) {
				/*$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg= '$row->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				} */
				
				$detailsup[] = array('id_kelompok_unit'=> $row->id,
									
									 'nama_kelompok_unit'=> $row->nama_kelompok,
									 'stok_opname'=> '0'
									);
			}
		}
		else {
			$detailsup = '';
		}
		return $detailsup;
  }
  
  function get_all_saldoawal($bulan, $tahun) {

		$query	= $this->db->query(" SELECT b.*, c.kode_kelompok_unit, c.nama FROM tt_stok_opname_hutang_dagang_wip_detail b 
					INNER JOIN tt_stok_opname_hutang_dagang_wip a ON b.id_stok_opname_hutang_dagang_wip = a.id 
					INNER JOIN tm_kelompok_unit c ON c.id = b.id_kelompok_unit
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY c.kode_kelompok_unit ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detailsup = array();			
			foreach ($hasil as $row) {
				
				$detailsup[] = array('id_kelompok_unit'=> $row->id_kelompok_unit,
										'kode_kelompok_unit'=> $row->kode_kelompok_unit,
										'nama_kelompok_unit'=> $row->nama,
										'stok_opname'=> $row->jum_stok_opname
									);
			}
			

		}
		else {
			$detailsup = '';
		}
		return $detailsup;
  }
  
  function savesaldoawalhutang($is_new, $id_stok, $id_kelompok_unit, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  if ($is_new == '1') {
			$seq	= $this->db->query(" SELECT id FROM tt_stok_opname_hutang_dagang_wip_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$idbaru	= $seqrow->id+1;
			}else{
				$idbaru	= 1;
			}
		
		   $data_detail = array(
						'id'=>$idbaru,
						'id_stok_opname_hutang_dagang_wip'=>$id_stok,
						'id_kelompok_unit'=>$id_kelompok_unit, 
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_hutang_dagang_wip_detail',$data_detail);
	  }
	  else {
		  // 19 nov 2011, cek di tt_stok_opname_hutang_dagang_wip_detail. apakah kode kelompok_unitnya ada yg blm ada
		 $query3	= $this->db->query(" SELECT id FROM tt_stok_opname_hutang_dagang_wip_detail WHERE id_kelompok_unit = '$id_kelompok_unit' ");
		 if ($query3->num_rows() == 0){
			$data_detail = array(
						'id_stok_opname_hutang_dagang_wip'=>$id_stok,
						'id_kelompok_unit'=>$id_kelompok_unit, 
						'jum_stok_opname'=>$stok_fisik
					);
		   $this->db->insert('tt_stok_opname_hutang_dagang_wip_detail',$data_detail);
		 }
		 else {
		  $this->db->query(" UPDATE tt_stok_opname_hutang_dagang_wip_detail SET jum_stok_opname = '$stok_fisik' 
						where id_kelompok_unit= '$id_kelompok_unit' AND id_stok_opname_hutang_dagang_wip = '$id_stok' ");
		 }
	  }
  }
  
  // 05-08-2015
  function get_kelompok_unit(){
	$query	= $this->db->query(" SELECT * FROM tm_kelompok_unit ORDER BY kode_kelompok_unit ");    
    return $query->result();  
  }
  
  //~ function saveopnamehutang($id_kelompok_unit, $isretur, $pkp, $tipe_pajak, $tgl_bukti, $no_bukti, $jumlah){  
    //~ $tgl = date("Y-m-d H:i:s");
    //~ 
    //~ $pisah1 = explode("-", $tgl_bukti);
	//~ $tgl1= $pisah1[0];
	//~ $bln1= $pisah1[1];
	//~ $thn1= $pisah1[2];
	//~ $tgl_bukti = $thn1."-".$bln1."-".$tgl1;
	//~ 
	//~ if ($pkp == 't') {
		//~ //if ($tipe_pajak == 'I')
		//~ $dpp = $jumlah/1.1;
		//~ $total_pajak = $jumlah/11;
	//~ }
	//~ else {
		//~ $dpp = 0;
		//~ $total_pajak = 0;
	//~ }
		//~ 
		//~ if ($isretur == '0') {
			//~ $queryxx	= $this->db->query(" SELECT id FROM tm_pembelian WHERE no_sj = '$no_bukti' 
										//~ AND id_kelompok_unit = '$id_kelompok_unit' AND status_aktif = 't' ");
			//~ if ($queryxx->num_rows() == 0){
				//~ $data_header = array(
					  //~ 'no_sj'=>$no_bukti,
					  //~ 'tgl_sj'=>$tgl_bukti,
					  //~ 'no_faktur'=>'',
					  //~ 'id_kelompok_unit'=>$id_kelompok_unit,
					  //~ 'total'=>$jumlah,
					  //~ 'uang_muka'=>0,
					  //~ 'sisa_hutang'=>0,
					  //~ 'keterangan'=>'',
					  //~ 'pkp'=>$pkp,
					  //~ 'tipe_pajak'=>$tipe_pajak,
					  //~ 'total_pajak'=>$total_pajak,
					  //~ 'dpp'=>$dpp,
					  //~ 'tgl_input'=>$tgl,
					  //~ 'tgl_update'=>$tgl,
					  //~ 'jenis_pembelian'=>'2',
					  //~ 'status_faktur'=>'t' );
				//~ 
				//~ $this->db->insert('tm_pembelian',$data_header);
				//~ 
				//~ // 06-08-2015
				//~ // ambil id sj pembelian
					//~ $query3	= $this->db->query(" SELECT id FROM tm_pembelian WHERE no_sj = '$no_bukti' 
										//~ AND id_kelompok_unit = '$id_kelompok_unit' AND status_aktif = 't' ");
					//~ $hasilrow = $query3->row();
					//~ $id_sj_pembelian	= $hasilrow->id;
				//~ 
				//~ $data_header = array(
				  //~ 'no_faktur'=>$no_bukti,
				  //~ 'tgl_faktur'=>$tgl_bukti,
				  //~ 'tgl_input'=>$tgl,
				  //~ 'tgl_update'=>$tgl,
				  //~ 'id_kelompok_unit'=>$id_kelompok_unit,
				  //~ 'jenis_pembelian'=>'2',
				  //~ 'status_faktur_pajak'=>'t',
				  //~ 'jumlah'=>$jumlah
				//~ );
				//~ $this->db->insert('tm_pembelian_nofaktur',$data_header);
				//~ 
				//~ // ambil id faktur
					//~ $query3	= $this->db->query(" SELECT id FROM tm_pembelian_nofaktur WHERE no_faktur = '$no_bukti' 
										//~ AND id_kelompok_unit = '$id_kelompok_unit' ");
					//~ $hasilrow = $query3->row();
					//~ $id_faktur	= $hasilrow->id;
				//~ 
				//~ $data_detail = array(
				  //~ 'id_pembelian_nofaktur'=>$id_faktur,
				  //~ 'no_sj'=>'',
				  //~ 'id_sj_pembelian'=>$id_sj_pembelian
				//~ );
				//~ $this->db->insert('tm_pembelian_nofaktur_sj',$data_detail);
			//~ }
		//~ } //end if isretur
		//~ else {
			//~ $queryxx	= $this->db->query(" SELECT id FROM tm_retur_beli WHERE no_dn_retur_manual = '$no_bukti' 
										//~ AND id_kelompok_unit = '$id_kelompok_unit' ");
			//~ if ($queryxx->num_rows() == 0){
				//~ $data_header = array(
							  //~ 'no_dn_retur'=>$no_bukti,
							  //~ 'tgl_retur'=>$tgl_bukti,
							  //~ 'id_kelompok_unit'=>$id_kelompok_unit,
							  //~ 'keterangan'=>'',
							  //~ 'tgl_input'=>$tgl,
							  //~ 'tgl_update'=>$tgl,
							  //~ 'jenis_pembelian'=>'2',
							  //~ 'no_dn_retur_manual'=>$no_bukti,
							  //~ 'jumlah'=>$jumlah
							//~ );
				//~ $this->db->insert('tm_retur_beli',$data_header);
			//~ }
		//~ }
	//~ 
		//~ // #########################################################################################
		//~ 
  //~ }

}

