<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $unit_packing, $cari) {	  
	if ($cari=="all") {
		if ($unit_packing=='0') {
			$this->db->select(" * FROM tm_sj_proses_packing order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_proses_packing WHERE kode_unit='$unit_packing' order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($unit_packing!='0') {
			$this->db->select(" * FROM tm_sj_proses_packing WHERE kode_unit='$unit_packing' 
			AND UPPER(no_sj) like UPPER('%$cari%') order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$this->db->select(" * FROM tm_sj_proses_packing WHERE UPPER(no_sj) like UPPER('%$cari%') order by id DESC ", false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	
	$data_fb = array();
	$detail_fb = array();

	if ($query->num_rows() > 0) {
		$hasil = $query->result();

		foreach ($hasil as $row1) {
			
			$proses_packing = 'f';
			$dalam_proses	= 'f';
			
			$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_packing_detail WHERE id_sj_proses_packing='$row1->id' ");
			if ($query2->num_rows() > 0){
				$hasil2=$query2->result();
				
				foreach ($hasil2 as $row2){
					$query3	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->e_product_motifname;
						$kode_brg	= $hasilrow->i_product_motif;
					} else {
						$nama_brg	= '';
					}
					
					$qhslpacking	= $this->db->query(" SELECT sum(qty) AS qtyhslpacking FROM tm_sj_hasil_packing_detail WHERE id_sj_proses_packing_detail='$row2->id' ");
					
					if($qhslpacking->num_rows()>0){					
						$rhslpacking	= $qhslpacking->row();
						$qtynya	= (($row2->qty)-($rhslpacking->qtyhslpacking));
					}else{
						$qtynya	= $row2->qty;
					}
					
					if($qtynya==0)
						$proses_packing	= 't';
					
					if($rhslpacking->qtyhslpacking>0)
						$dalam_proses = 't';
									
					$detail_fb[] = array('kode_brg' => $kode_brg,
								'nama' => $nama_brg,
								'qty' => $qtynya,
								'keterangan' => $row2->keterangan,
								'status_packing' => $row2->status_packing,
								'id_hsljahit' => $row2->id_hsljahit,
								'from_gudang' => $row2->from_gudang );
				}
			}
			else {
				$detail_fb = '';
			}
			
			$query4			= $this->db->query(" SELECT nama FROM tm_unit_packing WHERE kode_unit='$row1->kode_unit' ");
			$row_nama_unit	= $query4->row();
			$nama_unit		= $row_nama_unit->nama;
			
			$data_fb[] = array('id'=> $row1->id,
								'no_sj'=> $row1->no_sj,
								'tgl_sj'=> $row1->tgl_sj,
								'kode_unit'=> $row1->kode_unit,
								'nama_unit'=> $nama_unit,
								'keterangan'=> $row1->keterangan,
								'tgl_update'=> $row1->tgl_update,
								'status_packing'=> $row1->status_packing,
								'status_edit'=> $row1->status_edit,
								//'makloon_internal'=> $row1->makloon_internal,
								'proses_packing'=> $proses_packing,
								'dalam_proses'=>$dalam_proses,
								'detail_fb'=> $detail_fb);
			$detail_fb = array();
			}
		}
	else {
		$data_fb = '';
	}
		
	return $data_fb;
  }
  
  function getAlltanpalimit($unit_packing, $cari){
	if ($cari=="all") {
		if ($unit_packing=='0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_packing ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_packing WHERE kode_unit = '$unit_packing' ");
	}
	else {
		if ($unit_packing!='0')
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_packing WHERE kode_unit = '$unit_packing' AND UPPER(no_sj) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_sj_proses_packing WHERE UPPER(no_sj) like UPPER('%$cari%') ");
	}
    
    return $query->result();
  }
  
  function lunitpacking(){
  	$query	= $this->db->query(" SELECT * FROM tm_unit_packing ORDER BY kode_unit ASC ");
	return $query->result();
  }
  
  function cek_data($no_sj){
    $this->db->select(" * from tm_sj_proses_packing WHERE no_sj='$no_sj' ", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function save($no_sj,$tgl_sj,$unit_packing, $ket,$kode,$nama,$qty,$keterangan,$qty_nyumput,$gudang, $idhsljahit){  
    
    $tgl = date("Y-m-d"); 

    $this->db->select(" * FROM tm_sj_proses_packing WHERE no_sj='$no_sj' ", false);
    $query = $this->db->get();
    $hasil = $query->result();

		if(count($hasil)==0) {

			$data_header = array(
			  'no_sj'=>$no_sj,
			  'tgl_sj'=>$tgl_sj,
			  'kode_unit'=>$unit_packing,
			  'keterangan'=>$ket,
			  'status_packing'=>'f',
			  'status_edit'=>'f',
			 // 'makloon_internal'=>$packing_internal,
			  'tgl_input'=>$tgl,
			  'tgl_update'=>$tgl
			);
			
			$this->db->insert('tm_sj_proses_packing',$data_header);
		
			$query2	= $this->db->query(" SELECT id FROM tm_sj_proses_packing ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {

				$qinsert	= $this->db->query("INSERT INTO tm_sj_proses_packing_detail(id_sj_proses_packing,
								kode_brg_jadi,qty,keterangan,status_packing,id_hsljahit,from_gudang) 
								VALUES('$id_sj','$kode','$qty','$keterangan','f','$idhsljahit','$gudang') ");
				if($qinsert) {
					
					//$query	= $this->db->query(" SELECT * FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kode' AND id_gudang='$gudang' ");
					//if($query->num_rows()>0) {
						//$rstokhsljahit	= $query->row();
						//$idhsljahit		= $rstokhsljahit->id;
						//$stokhsljahit	= $rstokhsljahit->stok;

						$sisastokhsljahit	= $qty_nyumput-$qty;
						$fpacking	= ($sisastokhsljahit==0 || $sisastokhsljahit<0)?'t':'f';
						
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$sisastokhsljahit', tgl_update_stok='$tgl', 
										f_packing='$fpacking' WHERE kode_brg_jadi='$kode' AND id='$idhsljahit' ");
					//	$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi, no_bukti, keluar, saldo, tgl_input, id_gudang) VALUES('$kode','$no_sj','$qty','$sisastokhsljahit','$tgl','$gudang') ");
					//}
				}

			} 
		}
		else {
			$query2	= $this->db->query(" SELECT id FROM tm_sj_proses_packing ORDER BY id DESC LIMIT 1 ");
			$hasilrow = $query2->row();
			$id_sj	= $hasilrow->id; 
			
			if ($kode!='' && ($qty!='' || $qty!=0)) {
				$qinsert	= $this->db->query("INSERT INTO tm_sj_proses_packing_detail(id_sj_proses_packing,kode_brg_jadi,
							qty,keterangan,status_packing,from_gudang) VALUES('$id_sj','$kode','$qty','$keterangan','f','$gudang') ");
				if($qinsert){	
					
					//$query	= $this->db->query(" SELECT * FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$kode' ");
					//if($query->num_rows()>0){
						//$rstokhsljahit	= $query->row();
						//$idhsljahit		= $rstokhsljahit->id;
						//$stokhsljahit	= $rstokhsljahit->stok;
						//$sisastokhsljahit	= $stokhsljahit-$qty;

						$sisastokhsljahit	= $qty_nyumput-$qty;
						$fpacking	= ($sisastokhsljahit==0 || $sisastokhsljahit<0)?'t':'f';
						
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$sisastokhsljahit', tgl_update_stok='$tgl', 
									f_packing='$fpacking' WHERE kode_brg_jadi='$kode' AND id='$idhsljahit' ");						
						//$this->db->query(" INSERT INTO tt_stok_hasil_jahit(kode_brg_jadi, no_bukti, keluar, saldo, tgl_input, id_gudang) VALUES('$kode','$no_sj','$qty','$sisastokhsljahit','$tgl','$gudang') ");											
					//}
				}
			}
		}
  }
    
  function delete($kode){    
	  $tgl = date("Y-m-d");
	  
		$query3	= $this->db->query(" SELECT no_sj FROM tm_sj_proses_packing WHERE id='$kode' ");
		$hasilrow = $query3->row();
		$no_sj	= $hasilrow->no_sj; 

		$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_packing_detail WHERE id_sj_proses_packing='$kode' ");
		if ($query2->num_rows() > 0){
			$hasil2=$query2->result();
										
			foreach ($hasil2 as $row2) {
				
				$query4	= $this->db->query(" SELECT qty FROM tm_sj_proses_packing_detail WHERE id_sj_proses_packing='$kode' AND kode_brg_jadi='$row2->kode_brg_jadi' ");
				if($query4->num_rows()>0){
					$rproses_pack_detail= $query4->row();
					$qty_pack_detail	= $rproses_pack_detail->qty;
				}else{
					$qty_pack_detail	= 0;
				}
				
				$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi='$row2->kode_brg_jadi' AND id='$row2->id_hsljahit' ");
				if ($query3->num_rows()==0){
					$stok_lama = 0;
					//$fpacking	= ($stok_lama==0 || $stok_lama<0)?'f':'t';
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
					
					$new_stok = $stok_lama+$qty_pack_detail;
					//$fpacking	= ($new_stok==0 || $new_stok<0)?'f':'t';
				}
				
				//$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$new_stok', tgl_update_stok='$tgl', f_packing='$fpacking' WHERE kode_brg_jadi='$row2->kode_brg_jadi' AND id='$row2->id_hsljahit' ");
				$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok='$new_stok', tgl_update_stok='$tgl', f_packing='f' WHERE kode_brg_jadi='$row2->kode_brg_jadi' AND id='$row2->id_hsljahit' ");
									
				/*$query4	= $this->db->query(" SELECT * FROM tt_stok_hasil_jahit WHERE kode_brg_jadi='$row2->kode_brg_jadi' AND no_bukti='$no_sj' ORDER BY id DESC ");
				if ($query4->num_rows() > 0){
					$hasil4=$query4->result();															
					foreach ($hasil4 as $row4) {
									
						$query3	= $this->db->query(" INSERT INTO tt_stok_hasil_jahit (kode_brg, no_bukti, masuk, saldo, tgl_input) VALUES ('$row2->kode_brg_jadi','$no_sj', '$stok_lama', '$new_stok', '$tgl') ");
					}
				} */
			}
		}
		
		$this->db->delete('tm_sj_proses_packing_detail', array('id_sj_proses_packing' => $kode));
		$this->db->delete('tm_sj_proses_packing', array('id' => $kode));
  }
  
  function get_sj($id_sj){
	$query	= $this->db->query(" SELECT * FROM tm_sj_proses_packing WHERE id='$id_sj' ");    
    $hasil = $query->result();
    
    $data_sj = array();
	$detail_sj = array();
	
	foreach ($hasil as $row1) {

				$query3	= $this->db->query(" SELECT * FROM tm_unit_packing WHERE kode_unit='$row1->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
				}
						
				$query2	= $this->db->query(" SELECT * FROM tm_sj_proses_packing_detail WHERE id_sj_proses_packing='$row1->id' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
										
					foreach ($hasil2 as $row2) {
						$query4	= $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product_motif='$row2->kode_brg_jadi' ");
						if ($query4->num_rows() > 0){
							$hasilrow2 = $query4->row();
						}
						else {
						}

						$qhslpacking	= $this->db->query(" SELECT sum(qty) AS qtyhslpacking FROM tm_sj_hasil_packing_detail WHERE id_sj_proses_packing_detail='$row2->id' ");
						$rhslpacking	= $qhslpacking->row();
						
						$qtynya	= ($row2->qty)-($rhslpacking->qtyhslpacking);
										
							$detail_sj[] = array('id'=> $row2->id,
										'id_sj_proses_packing'=> $row2->id_sj_proses_packing,
										'kode_brg_jadi'=> $row2->kode_brg_jadi,
										'nama_brg'=>$hasilrow2->e_product_motifname,
										'qty'=> $qtynya,
										'keterangan'=> $row2->keterangan,
										'status_packing'=> $row2->status_packing,
										'id_hsljahit'=> $row2->id_hsljahit,
										'id_gudang' => $row2->from_gudang);
					}
				}
				else {
					$detail_sj = '';
				}
								 
				$data_sj[] = array('id'=> $row1->id,	
								'no_sj'=> $row1->no_sj,
								'tgl_sj'=> $row1->tgl_sj,
								'kode_unit'=> $row1->kode_unit,
								'nama_unit'=> $hasilrow->nama,
								'keterangan'=> $row1->keterangan,
								'status_packing'=> $row1->status_packing,
								'status_edit'=> $row1->status_edit,
								//'makloon_internal' => $row1->makloon_internal,
								'tgl_input' => $row1->tgl_input,
								'tgl_update' => $row1->tgl_update,
								'detail_sj'=> $detail_sj );
				$detail_sj = array();
				
	}
	return $data_sj;
  }
  
  function generate_nomor(){
    // generate no SJ Keluar Makloon
			$th_now	= date("Y");
			
			// generate no SJ
			$query3	= $this->db->query(" SELECT no_sj FROM tm_sj_proses_packing ORDER BY id DESC, no_sj DESC LIMIT 1 ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
			if(strlen($no_sj)==13) {
				$nosj = substr($no_sj, 4, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}
			$nomorsj = "SJP-".$nomorsj;

			return $nomorsj;  
  }
  
  function get_bahan($num, $offset, $cari)
  {
		$query	= $this->db->query(" SELECT a.i_product_motif AS iproductmotif, a.e_product_motifname AS eproductmotif, c.id AS idhsljahit, c.stok AS qtyhsljahit, c.id_gudang AS igudang FROM tr_product_motif a 
				INNER JOIN tr_product_base b ON b.i_product_base=a.i_product 
				INNER JOIN tm_stok_hasil_jahit c ON c.kode_brg_jadi=a.i_product_motif 
				WHERE c.f_packing='f'
				ORDER BY a.i_product_motif ASC LIMIT ".$num." OFFSET ".$offset);
		if($query->num_rows()>0){
			return $query->result();
		}
  }
  
  function get_bahantanpalimit($cari)
  {
		return $this->db->query(" SELECT a.i_product_motif AS iproductmotif, a.e_product_motifname AS eproductmotif, c.id AS idhsljahit, c.stok AS qtyhsljahit, c.id_gudang AS igudang FROM tr_product_motif a 
				INNER JOIN tr_product_base b ON b.i_product_base=a.i_product 
				INNER JOIN tm_stok_hasil_jahit c ON c.kode_brg_jadi=a.i_product_motif 
				WHERE c.f_packing='f'
				ORDER BY a.i_product_motif ASC ");
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier ORDER BY kode_supplier ");    
    return $query->result();  
  }  

}
