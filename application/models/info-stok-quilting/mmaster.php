<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function get_stok($num, $offset, $cari) {
	//function get_opname_hutang($date_from, $date_to) {
	$sql = " a.kode_brg, a.nama_brg, a.id_gudang, b.stok, b.tgl_update_stok 
			FROM tm_brg_hasil_makloon a, tm_stok_hasil_makloon b WHERE a.kode_brg = b.kode_brg ";

	if ($cari != "all")
		$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
	$sql.= " ORDER BY b.tgl_update_stok ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_stok = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {				
				$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_brg_hasil_makloon a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$satuan	= $hasilrow->nama_satuan;
				}
				else {
					$satuan	= '';
				}
				
				$query3	= $this->db->query(" SELECT a.id, a.bulan, a.tahun, a.tgl_update, b.status_approve 
									FROM tt_stok_opname_hasil_quilting a, 
									tt_stok_opname_hasil_quilting_detail b WHERE a.id = b.id_stok_opname_hasil_quilting
									AND b.kode_brg = '$row1->kode_brg' ORDER BY a.id DESC ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$bulan	= $hasilrow->bulan;
					$tahun	= $hasilrow->tahun;
					$tgl_update_so	= $hasilrow->tgl_update;
					$status_approve	= $hasilrow->status_approve;
				}
				else {
					$bulan	= '';
					$tahun	= '';
					$tgl_update_so	= '';
					$status_approve = 'f';
				}
								
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg,	
											'nama_brg'=> $row1->nama_brg,
											'satuan'=> $satuan,
											'stok'=> $row1->stok,
											'tgl_update_stok'=> $row1->tgl_update_stok,
											'bulan'=> $bulan,
											'tahun'=> $tahun,
											'tgl_update_so'=> $tgl_update_so,
											'status_approve'=> $status_approve
											);
			} // endforeach header
	}
	else {
			$data_stok = '';
	}
		return $data_stok;
  }
  
  function get_stoktanpalimit($cari){
	$sql = " SELECT a.kode_brg, a.nama_brg, a.id_gudang, b.stok, b.tgl_update_stok 
			FROM tm_brg_hasil_makloon a, tm_stok_hasil_makloon b WHERE a.kode_brg = b.kode_brg ";
	if ($cari != "all")
		$sql.= " AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
    
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

}
