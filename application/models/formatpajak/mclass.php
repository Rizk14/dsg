<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function viewformatpajak() {	
		$db2=$this->load->database('db_external', TRUE);
		$query=$db2->query(" SELECT * FROM tr_format_pajak ");
		/*if ($query->num_rows() > 0) {
			return $result = $query->row();
		}
		else
			return $result = ''; */
		return $query;
	}
		
	function msimpan($segmen1, $nourut_awal, $nourut_akhir) {
		$db2=$this->load->database('db_external', TRUE);
		$tgl = date("Y-m-d H:i:s");
		
		$sql = $db2->query(" SELECT id FROM tr_format_pajak ");
		if ($sql->num_rows() > 0) {
			$hasilrow = $sql->row();
			$idnya= $hasilrow->id;
			$idskrg = $idnya;
		}
		else {
			$idnya = '';
			$idskrg = 1;
		}	
			
			$str = array(
				'id'=>$idskrg,
				'segmen1'=>$segmen1,
				// 'nourut_awal'=>"0".$nourut_awal,
				// 'nourut_akhir'=>"0".$nourut_akhir,
				'nourut_awal'=>$nourut_awal,
				'nourut_akhir'=>$nourut_akhir,
				'tgl_update'=>$tgl
			);
		if ($idnya == '')
			$db2->insert('tr_format_pajak',$str);
		else {
			$db2->where('id',$idskrg);
			$db2->update('tr_format_pajak',$str);  
		}
		
		print "<script>alert(\"Format nomor pajak berhasil disimpan. Terimakasih.\");window.open(\"index\", \"_self\");</script>";
		//redirect('formatpajak/cform/');
    }
}

?>
