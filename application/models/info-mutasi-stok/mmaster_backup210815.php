<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }
  
  function get_mutasi_stok($date_from, $date_to, $gudang, $format_harga) {				
		// 1. ambil data2 gudang dari rentang tanggal yg dipilih
		$filter = "";
		if ($gudang != '0') {
			$filter = " AND d.id = '$gudang' ";
		}
		
		// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
		
		$sql = "SELECT a.id_brg, a.kode_brg, a.nama_brg, a.kode_gudang, a.id_gudang, a.harga FROM (
				SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang, b.harga FROM tm_apply_stok_pembelian a 
				INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')   
				AND b.tgl_bonm <=to_date('$date_to','dd-mm-yyyy') AND b.status_stok='t' ".$filter." 
				AND e.status_aktif = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg, b.harga
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang, c.harga FROM tm_bonmmasuklain a 
				INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
				INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')  
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg, c.harga
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang, c.harga FROM tm_bonmkeluar a 
				INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
				INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE a.tgl_bonm >=to_date('$date_from','dd-mm-yyyy')  
				AND a.tgl_bonm <=to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg, c.harga
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang, 0 as harga FROM tt_stok_opname_bahan_baku a 
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku 
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
					".$filter." AND e.status_aktif = 't' 
					AND a.status_approve='t' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg, harga
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang, 0 as harga FROM tt_stok_opname_bahan_baku a 
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku 
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					WHERE a.bulan='$bln1' AND a.tahun='$thn1' 
					".$filter." AND e.status_aktif = 't'
					GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg, harga
				)
				a GROUP BY a.id_gudang, a.kode_gudang, a.id_brg, a.kode_brg, a.nama_brg, a.harga 
				ORDER BY a.kode_gudang, a.nama_brg, a.harga";
		//AND b.jum_stok_opname <> 0
	  //echo $sql; die();
		$query	= $this->db->query($sql);
		
		$jum_keluar = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {	
				if ($row1->harga == '')
					$row1->harga = 0;
				// 12-07-2013, ambil data saldo awal dari SO bulan sebelumnya (ambil bulan dan tahun dari date_from)
				// cek apakah termasuk quilting atau bukan (utk menentukan query di tabel SO)
				// 29-10-2014, dikomen aja. langsung ambil query SO di tabel tt_stok_opname_bahan_baku
				
				$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a 
									INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
									INNER JOIN tm_barang e ON e.id = b.id_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.id_brg = '$row1->id_brg' AND d.id = '$row1->id_gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
									
				/*$queryx	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg = '$row1->kode_brg'
										AND status_aktif = 't' ");
				if ($queryx->num_rows() > 0){
					$is_quilting = 'f';
				}
				else
					$is_quilting = 't';
				
				if ($is_quilting == 'f') {
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a 
									INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
									INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
									WHERE b.kode_brg = '$row1->kode_brg' AND a.id_gudang = '$row1->id_gudang'
									AND c.harga = '$harga'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				}
				else {
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_quilting a
									INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id = b.id_stok_opname_hasil_quilting
									INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id = c.id_stok_opname_hasil_quilting_detail
									WHERE b.kode_brg = '$row1->kode_brg' AND c.harga = '$harga'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				} */
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
					if ($saldo_awal == '')
						$saldo_awal = 0;
				}
				else {
					$saldo_awal = 0;
				}
				
				// 16-03-2015 dikomen, ganti pake auto.
				// 21-08-2015 GA PAKE AUTO
				//$saldo_awal_rp = $saldo_awal*$row1->harga;
				//$saldo_awal_rp = $auto_saldo_awal*$row1->harga;
				$saldo_awal_rp = 0;
				
				// 29-10-2014: ambil brg masuk hasil pembelian
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND b.status_stok = 't' AND e.id_gudang = '$row1->id_gudang'
							AND b.harga = '$row1->harga' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;
				
				// 05-11-2014, ambil ket suppliernya PKP atau bukan (koreksi: ditunda)
				/*$queryxx	= $this->db->query(" SELECT distinct a.kode_supplier FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$row1->id_gudang'
							AND b.harga = '$row1->harga' ");
				if ($queryxx->num_rows() > 0){					
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$kode_sup = $rowxx->kode_supplier;
						
						// cek apakah kode supplier ini PKP atau bukan
						$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$kode_sup' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_supplier	= $hasilrow->nama;
							$pkp	= $hasilrow->pkp;
						}
						else
							$pkp = 'f';
					}
				}
				else
					$pkp = 'f';
				*/
				
				// konversi ke satuan baru (dari yard/lusin) ke meter atau pcs
				// 30-10-2014, ini ga dipake lagi. soalnya udh ada settingan konversi stok di master bhn baku
				/* if ($id_satuan == 2) { // yard
					$jum_masuk = $jum_masuk*0.91; // ini konversi dari yard ke meter
					// catatan 29-10-2014: di bon M keluar dan masuk lain2, 1 yard = 0.90 m. udh ditanyain ke rohmat via gmail,
					// kata rohmat, 1 yard = 0.91 m.
				 }
				 else if ($id_satuan == 7) { // lusin
					$jum_masuk = $jum_masuk*12; // ini konversi dari lusin ke pcs
				 } */
				 
				 $query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							b.nama as nama_satuan FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$id_satuan = $hasilnya->satuan;
					$satuan = $hasilnya->nama_satuan;
					
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
						}
						else
							$jum_masuk_konv = $jum_masuk;
					}
					else
						$jum_masuk_konv = $jum_masuk;
				}
				else {
					$id_satuan_konversi = 0;
					$jum_masuk_konv = $jum_masuk;
					$id_satuan = 0;
					$satuan = '';
				}
				
				$jum_masuk_konv_rp = $jum_masuk_konv*$row1->harga;
				
				// ambil nama satuan konv
				$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.id_satuan_konversi = b.id WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$nama_satuan_konv = $hasilnya->nama_satuan;
				}
				else {
					$nama_satuan_konv = 'Tidak Ada';
				}
				// ---------------------------------------------------------------------------------------------
				
				// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND d.id = '$row1->id_gudang' AND c.harga = '$row1->harga'
							AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan = '') ");
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
					
					// 27-02-2015
					/*if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_keluar = $jum_keluar*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_keluar = $jum_keluar/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_keluar = $jum_keluar+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_keluar = $jum_keluar-$angka_faktor_konversi;
						}
					} */
				}
				else
					$jum_keluar = 0;
				
				$jum_keluar_rp = $jum_keluar*$row1->harga;
				
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND d.id = '$row1->id_gudang' AND c.harga = '$row1->harga'
							AND ((a.tujuan >= '3' AND a.tujuan <='5') OR a.keterangan='DIBEBANKAN') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
					
					// 27-02-2015
					/*if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_keluar_lain = $jum_keluar_lain*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_keluar_lain = $jum_keluar_lain/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_keluar_lain = $jum_keluar_lain+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_keluar_lain = $jum_keluar_lain-$angka_faktor_konversi;
						}
					} */
				}
				else
					$jum_keluar_lain = 0;
				
				$jum_keluar_lain_rp = $jum_keluar_lain*$row1->harga;
				
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND d.id = '$row1->id_gudang' AND c.harga = '$row1->harga' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;
				
				$jum_masuk_lain_rp = $jum_masuk_lain*$row1->harga;
				
				//==================================== 30-10-2014 STOK OPNAME ==========================================
				$query3	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a 
									INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
									INNER JOIN tm_barang e ON e.id = b.id_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.id_brg = '$row1->id_brg' AND d.id = '$row1->id_gudang'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
								
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					if ($jum_stok_opname == '')
						$jum_stok_opname = 0;
				}
				else {
					$jum_stok_opname = 0;
				}
				$jum_stok_opname_rp = $jum_stok_opname*$row1->harga;
				//--------------------------------------
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama FROM tm_gudang a 
											WHERE a.kode_gudang = '$row1->kode_gudang' ");
												
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else
					$nama_gudang = '';
				
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg,
											'harga'=> $row1->harga,
											'kode_gudang'=> $row1->kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_brg'=> $row1->nama_brg,
											'id_satuan'=> $id_satuan,
											'satuan'=> $satuan,
											'nama_satuan_konv'=> $nama_satuan_konv,
											'saldo_awal'=> $saldo_awal,
											//'saldo_awal'=> $auto_saldo_awal,
											//'saldo_awal_rp'=> $saldo_awal_rp,
											'jum_masuk'=> $jum_masuk_konv,
											'jum_masuk_rp'=> $jum_masuk_konv_rp,
											'jum_masuk_lain'=> $jum_masuk_lain,
											'jum_masuk_lain_rp'=> $jum_masuk_lain_rp,
											'jum_keluar'=> $jum_keluar,
											'jum_keluar_rp'=> $jum_keluar_rp,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'jum_keluar_lain_rp'=> $jum_keluar_lain_rp,
											'jum_stok_opname'=> $jum_stok_opname,
											//'jum_stok_opname_rp'=> $jum_stok_opname_rp
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  // 12-05-2015
  function get_mutasi_stok_gudang($date_from, $date_to, $gudang) {				
		// 1. ambil data2 gudang dari rentang tanggal yg dipilih
		$filter = "";
		if ($gudang != '0') {
			$filter = " AND d.id = '$gudang' ";
		}
		
		// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
		
		$sql = "SELECT a.id_brg, a.kode_brg, a.nama_brg, a.kode_gudang, a.id_gudang FROM (
				SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_apply_stok_pembelian a 
				INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')   
				AND b.tgl_bonm <=to_date('$date_to','dd-mm-yyyy') AND b.status_stok='t' ".$filter." 
				AND e.status_aktif = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmmasukmanual a 
				INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')  
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') 
				".$filter." AND e.status_aktif = 't' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmmasuklain a 
				INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')  
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.is_quilting='f'
				".$filter." AND e.status_aktif = 't' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmkeluar a 
				INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE a.tgl_bonm >=to_date('$date_from','dd-mm-yyyy')  
				AND a.tgl_bonm <=to_date('$date_to','dd-mm-yyyy') AND b.is_quilting='f'
				".$filter." AND e.status_aktif = 't' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tt_stok_opname_bahan_baku a 
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku 
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
					".$filter." AND e.status_aktif = 't'
					AND a.status_approve='t' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				
				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tt_stok_opname_bahan_baku a 
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku 
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					WHERE a.bulan='$bln1' AND a.tahun='$thn1' 
					".$filter." AND e.status_aktif = 't'
					GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				)
				a GROUP BY a.id_gudang, a.kode_gudang, a.id_brg, a.kode_brg, a.nama_brg ORDER BY a.kode_gudang, a.nama_brg";
			// 12-05-2015 dikeluarin dari query tt_stok_opname
			//  AND b.jum_stok_opname <> 0
	  //echo $sql; die();
		$query	= $this->db->query($sql);
		
		$jum_keluar = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {	
				//if ($row1->harga == '')
				//	$row1->harga = 0;
				
				// 12-07-2013, ambil data saldo awal dari SO bulan sebelumnya (ambil bulan dan tahun dari date_from)
				// cek apakah termasuk quilting atau bukan (utk menentukan query di tabel SO)
				// 29-10-2014, dikomen aja. langsung ambil query SO di tabel tt_stok_opname_bahan_baku
				// CATATAN 06-08-2015, INI UDAH PAKE QTY DGN SAT KONVERSI
				$queryx	= $this->db->query(" SELECT b.jum_stok_opname, b.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a 
									INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
									INNER JOIN tm_barang e ON e.id = b.id_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.id_brg = '$row1->id_brg' AND d.id = '$row1->id_gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
									
				/*$queryx	= $this->db->query(" SELECT * FROM tm_barang WHERE kode_brg = '$row1->kode_brg'
										AND status_aktif = 't' ");
				if ($queryx->num_rows() > 0){
					$is_quilting = 'f';
				}
				else
					$is_quilting = 't';
				
				if ($is_quilting == 'f') {
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a 
									INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
									INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
									WHERE b.kode_brg = '$row1->kode_brg' AND a.id_gudang = '$row1->id_gudang'
									AND c.harga = '$harga'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				}
				else {
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_quilting a
									INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id = b.id_stok_opname_hasil_quilting
									INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id = c.id_stok_opname_hasil_quilting_detail
									WHERE b.kode_brg = '$row1->kode_brg' AND c.harga = '$harga'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				} */
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
					$auto_saldo_awal = $hasilrow->auto_saldo_akhir;
					if ($saldo_awal == '')
						$saldo_awal = 0;
					if ($auto_saldo_awal == '')
						$auto_saldo_awal = 0;
				}
				else {
					$saldo_awal = 0;
					$auto_saldo_awal = 0;
				}
				
				// 16-03-2015 dikomen, ganti pake auto
				// 12-05-2015 balik lagi pake SO
				//$saldo_awal_rp = $saldo_awal*$row1->harga;
				//$saldo_awal_rp = $auto_saldo_awal*$row1->harga;
				
				// 29-10-2014: ambil brg masuk hasil pembelian
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND b.status_stok = 't' AND e.id_gudang = '$row1->id_gudang' ");
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;
				
				// 06-08-2015, hitung barang masuk pembelian MANUAL
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukmanual a 
							INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND d.id = '$row1->id_gudang' ");
				// sum(b.qty_satawal)
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masukmanual = $hasilrow->jum_masuk;
					if ($jum_masukmanual == '')
						$jum_masukmanual = 0;
				}
				else
					$jum_masukmanual = 0;
				
				// 05-11-2014, ambil ket suppliernya PKP atau bukan (koreksi: ditunda)
				/*$queryxx	= $this->db->query(" SELECT distinct a.kode_supplier FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$row1->id_gudang'
							AND b.harga = '$row1->harga' ");
				if ($queryxx->num_rows() > 0){					
					$hasilxx = $queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$kode_sup = $rowxx->kode_supplier;
						
						// cek apakah kode supplier ini PKP atau bukan
						$query3	= $this->db->query(" SELECT nama, pkp FROM tm_supplier WHERE kode_supplier = '$kode_sup' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_supplier	= $hasilrow->nama;
							$pkp	= $hasilrow->pkp;
						}
						else
							$pkp = 'f';
					}
				}
				else
					$pkp = 'f';
				*/
				
				// konversi ke satuan baru (dari yard/lusin) ke meter atau pcs
				// 30-10-2014, ini ga dipake lagi. soalnya udh ada settingan konversi stok di master bhn baku
				/* if ($id_satuan == 2) { // yard
					$jum_masuk = $jum_masuk*0.91; // ini konversi dari yard ke meter
					// catatan 29-10-2014: di bon M keluar dan masuk lain2, 1 yard = 0.90 m. udh ditanyain ke rohmat via gmail,
					// kata rohmat, 1 yard = 0.91 m.
				 }
				 else if ($id_satuan == 7) { // lusin
					$jum_masuk = $jum_masuk*12; // ini konversi dari lusin ke pcs
				 } */
				 
				 $query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.nama_brg, b.nama as nama_satuan FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan = $hasilnya->satuan;
					$satuan = $hasilnya->nama_satuan;
					
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
							$jum_masukmanual_konv = $jum_masukmanual*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
							$jum_masukmanual_konv = $jum_masukmanual/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
							$jum_masukmanual_konv = $jum_masukmanual+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
							$jum_masukmanual_konv = $jum_masukmanual-$angka_faktor_konversi;
						}
						else {
							$jum_masuk_konv = $jum_masuk;
							$jum_masukmanual_konv = $jum_masukmanual;
						}
					}
					else {
						$jum_masuk_konv = $jum_masuk;
						$jum_masukmanual_konv = $jum_masukmanual;
					}
				}
				else {
					$id_satuan_konversi = 0;
					$jum_masuk_konv = $jum_masuk;
					$jum_masukmanual_konv = $jum_masukmanual;
					$id_satuan = 0;
					$satuan = '';
				}
				
				//$jum_masuk_konv_rp = $jum_masuk_konv*$row1->harga;
				
				// ambil nama satuan konv
				$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.id_satuan_konversi = b.id WHERE a.id = '$row1->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$nama_satuan_konv = $hasilnya->nama_satuan;
				}
				else {
					$nama_satuan_konv = 'Tidak Ada';
				}
				// ---------------------------------------------------------------------------------------------
				
				// cek apakah brg quilting atau bukan
			/*	$query3	= $this->db->query(" SELECT a.nama_brg, a.satuan, b.nama as nama_satuan FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$quilting = 'f';
					$hasilnya = $query3->row();
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan = $hasilnya->satuan;
					$satuan = $hasilnya->nama_satuan;
					
					//1. hitung brg masuk dari tm_apply_stok_pembelian
					$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$row1->id_gudang' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk = $hasilrow->jum_masuk;
					}
					else
						$jum_masuk = 0;
					
					// --------- 25-09-2013 ----------------------
					// konversi ke satuan baru (dari yard/lusin) ke meter atau pcs
					 if ($id_satuan == 2) { // yard
						$jum_masuk = $jum_masuk*0.91; // ini konversi dari yard ke meter
						// catatan 29-10-2014: di bon M keluar dan masuk lain2, 1 yard = 0.90 m. udh ditanyain ke rohmat via gmail,
						// tinggal nunggu jawaban
					 }
					 else if ($id_satuan == 7) { // lusin
						$jum_masuk = $jum_masuk*12; // ini konversi dari lusin ke pcs
					 }
					 
					// -------------------------------------------
					
				}
				else {
					$quilting = 't';
					
					//1. hitung brg masuk dari tm_sj_hasil_makloon
					$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a, tm_sj_hasil_makloon_detail b
							WHERE a.id = b.id_sj_hasil_makloon AND b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_makloon = '$row1->kode_brg'
							AND b.status_stok = 't' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$jum_masuk = $hasilrow->jum_masuk;
					}
					else
						$jum_masuk = 0;
					$nama_brg = '';
					$id_satuan = 0;
					$satuan = '';
				} */
				
				// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				// 26-02-2015, ambilnya dari qty_satawal trus dikonversi
				// 06-08-2015, LANGSUNG QTY KONVERSI AJA
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND d.id = '$row1->id_gudang'
							AND b.is_quilting = 'f' AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan = '') ");
				// sum(b.qty_satawal)
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
					
					// 27-02-2015. 06-08-2015 DIKOMEN
					/*if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_keluar = $jum_keluar*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_keluar = $jum_keluar/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_keluar = $jum_keluar+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_keluar = $jum_keluar-$angka_faktor_konversi;
						}
					} */
				}
				else
					$jum_keluar = 0;
				
				//$jum_keluar_rp = $jum_keluar*$row1->harga;
				
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				// 26-02-2015, ambilnya bukan qty, tapi qty_satawal
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND d.id = '$row1->id_gudang' 
							AND b.is_quilting = 'f' AND ((a.tujuan >= '3' AND a.tujuan <='5') OR a.keterangan='DIBEBANKAN') ");
				// sum(b.qty_satawal)
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
					
					// 27-02-2015. 06-08-2015 DIKOMEN
				/*	if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_keluar_lain = $jum_keluar_lain*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_keluar_lain = $jum_keluar_lain/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_keluar_lain = $jum_keluar_lain+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_keluar_lain = $jum_keluar_lain-$angka_faktor_konversi;
						}
					} */
				}
				else
					$jum_keluar_lain = 0;
				
				//$jum_keluar_lain_rp = $jum_keluar_lain*$row1->harga;
				
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				// 13-05-2015, SUDAH DICEK DI FITUR INPUT BONMMASUKLAIN DETAIL, pake field qty sudah betul
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1->id_brg'
							AND d.id = '$row1->id_gudang' 
							AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;
				
				//$jum_masuk_lain_rp = $jum_masuk_lain*$row1->harga;
				
				//==================================== 30-10-2014 STOK OPNAME ==========================================
				$query3	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a 
									INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
									INNER JOIN tm_barang e ON e.id = b.id_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.id_brg = '$row1->id_brg' AND d.id = '$row1->id_gudang'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				
				/*if ($is_quilting == 'f') {
					$sql = "SELECT b.jum_stok_opname FROM tt_stok_opname_bahan_baku a, 
							tt_stok_opname_bahan_baku_detail b
							WHERE a.id = b.id_stok_opname_bahan_baku 
							AND b.kode_brg = '$row1->kode_brg' 
							AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
					//if ($row1->kode_brg == 'PKS010000370001') { echo $sql; die(); }
					
				}
				else
					$sql = "SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_quilting a, 
								tt_stok_opname_hasil_quilting_detail b
								WHERE a.id = b.id_stok_opname_hasil_quilting 
								AND b.kode_brg = '$row1->kode_brg' 
								AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' "; */
				//$query3	= $this->db->query($sql);
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					if ($jum_stok_opname == '')
						$jum_stok_opname = 0;
					//$jum_stok_opname = round($jum_stok_opname, 2);
					
					// 9 desember 2011, utk satuan yard konversi ke meter. lusin ke pcs
					/*if ($id_satuan == '2' && $row1->quilting == 'f') {
						$jum_stok_opname = $jum_stok_opname * 0.91;
						$jum_stok_opname = round($jum_stok_opname, 2);
					}
					if ($id_satuan == '7' && $row1->quilting == 'f') {
						$jum_stok_opname = $jum_stok_opname*12;
						$jum_stok_opname = round($jum_stok_opname, 2);
					} */

				}
				else {
					$jum_stok_opname = 0;
					//$jum_stok_opname = round($jum_stok_opname, 2);
				}
				//$jum_stok_opname_rp = $jum_stok_opname*$row1->harga;
				//--------------------------------------
				
				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1->id_gudang' ");
												
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else
					$nama_gudang = '';
				
				// 06-08-2015
				$totmasuk = $jum_masuk_konv+$jum_masukmanual_konv;
				
				$data_stok[] = array(		'id_brg'=> $row1->id_brg,
											'kode_brg'=> $row1->kode_brg,
											//'harga'=> $row1->harga,
											'id_gudang'=> $row1->id_gudang,
											'kode_gudang'=> $row1->kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_brg'=> $row1->nama_brg,
											'id_satuan'=> $id_satuan,
											'satuan'=> $satuan,
											'nama_satuan_konv'=> $nama_satuan_konv,
											'saldo_awal'=> $saldo_awal,
											//'saldo_awal'=> $auto_saldo_awal,
											//'saldo_awal_rp'=> $saldo_awal_rp,
											'jum_masuk'=> $totmasuk,
											//'jum_masuk_rp'=> $jum_masuk_konv_rp,
											'jum_masuk_lain'=> $jum_masuk_lain,
											//'jum_masuk_lain_rp'=> $jum_masuk_lain_rp,
											'jum_keluar'=> $jum_keluar,
											//'jum_keluar_rp'=> $jum_keluar_rp,
											'jum_keluar_lain'=> $jum_keluar_lain,
											//'jum_keluar_lain_rp'=> $jum_keluar_lain_rp,
											'jum_stok_opname'=> $jum_stok_opname,
											//'jum_stok_opname_rp'=> $jum_stok_opname_rp
											
											//05-11-2014 tunda
											//'pkp'=> $pkp
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  // =====================================================================
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 14-03-2013, mutasi stok WIP. update 17-09-2014, ada pengelompokan berdasarkan kel brg jadi
  function get_mutasi_stok_wip($date_from, $date_to, $gudang) {		
	  
	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
	  
		/*$sql = "SELECT b.i_product_motif, b.e_product_motifname FROM tr_product_motif b
				INNER JOIN tr_product_base a ON a.i_product_base = b.i_product
				WHERE b.n_active = '1'
				ORDER BY a.f_stop_produksi ASC, b.i_product_motif "; */
		$sql = "SELECT a.kode_brg_jadi, a.kode FROM (
				select b.kode_brg_jadi, d.kode FROM tm_sjmasukwip a 
				INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
				INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
				LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
				WHERE c.n_active='1' AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi, d.kode
				
				UNION SELECT b.kode_brg_jadi, d.kode FROM tm_sjkeluarwip a 
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
				LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
				WHERE c.n_active='1' AND a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi, d.kode
				
				UNION SELECT b.kode_brg_jadi, d.kode FROM tt_stok_opname_hasil_jahit a 
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit 
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE c.n_active='1' AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					AND a.status_approve='t' GROUP BY b.kode_brg_jadi, d.kode
				)
				a GROUP BY a.kode_brg_jadi, a.kode ORDER BY a.kode, a.kode_brg_jadi"; //echo $sql; die();
		
		$query	= $this->db->query($sql);
		
		$jum_keluar1 = 0;
		$jum_keluar2 = 0;
		$jum_keluar_lain1 = 0;
		$jum_keluar_lain2 = 0;
		$jum_masuk = 0;
		$jum_masuk_lain1 = 0;
		$jum_masuk_lain2 = 0;
		$jum_masuk_lain3 = 0;
		$jum_masuk_lain4 = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row1->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
					
				// 13-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
				// 04-03-2015, saldo awal bukan dari jum_stok_opname, tapi ambil dari auto_saldo_akhir
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname, b.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a, 
									tt_stok_opname_hasil_jahit_detail b
									WHERE a.id = b.id_stok_opname_hasil_jahit
									AND b.kode_brg_jadi = '$row1->kode_brg_jadi' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
					$auto_saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else {
					$saldo_awal = 0;
					$auto_saldo_awal = 0;
				}
				
				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain1 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain1 = 0;
				
				//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain2 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain2 = 0;
				
				//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain3 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain3 = 0;
				
				//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '5' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain4 = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain4 = 0;
				
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar1 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar1 = 0;
				
				// hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar2 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar2 = 0;
				
				// hitung brg keluar lain jenis=3 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain1 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain1 = 0;
				
				// hitung brg keluar lain jenis=4 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain2 = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain2 = 0;
				
				// 13-09-2013 ================================
				$sql = "SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_jahit a, 
							tt_stok_opname_hasil_jahit_detail b
							WHERE a.id = b.id_stok_opname_hasil_jahit 
							AND b.kode_brg_jadi = '$row1->kode_brg_jadi' AND a.id_gudang = '$gudang'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
												
				$query3	= $this->db->query($sql);
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
				}
				else {
					$jum_stok_opname = 0;
				}
				//=========================================
				
				//17-09-2014, ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row1->kode' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
																
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg_jadi,
											'kode_kel'=> $row1->kode,
											'nama_kel'=> $nama_kel,
											'nama_brg'=> $e_product_motifname,
											'saldo_awal'=> $auto_saldo_awal,
											'jum_masuk'=> $jum_masuk,
											'jum_masuk_lain1'=> $jum_masuk_lain1,
											'jum_masuk_lain2'=> $jum_masuk_lain2,
											'jum_masuk_lain3'=> $jum_masuk_lain3,
											'jum_masuk_lain4'=> $jum_masuk_lain4,
											'jum_keluar1'=> $jum_keluar1,
											'jum_keluar2'=> $jum_keluar2,
											'jum_keluar_lain1'=> $jum_keluar_lain1,
											'jum_keluar_lain2'=> $jum_keluar_lain2,
											'jum_stok_opname'=> $jum_stok_opname
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  
  // ======================= 12-12-2014 =======================================
  function get_mutasi_stok_hasilcutting($date_from, $date_to) {		
	  // 12-12-2014 =============================================
	  // explode date_from utk keperluan query
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgldari = $thn1."-".$bln1."-".$tgl1;
							
		 // explode date_to utk keperluan query
		$pisah1 = explode("-", $date_to);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tglke = $thn1."-".$bln1."-".$tgl1;
	
		// explode date_from utk keperluan saldo awal 
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
			
		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
	  
		$sql = "SELECT a.kode_brg_jadi FROM (
					select b.kode_brg_jadi FROM tm_bonmmasukcutting a 
					INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE a.tgl_bonm >='".$tgldari."' 
					AND a.tgl_bonm <='".$tglke."' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a 
					INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE a.tgl_sj >='".$tgldari."'  
					AND a.tgl_sj <='".$tglke."' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_bonmkeluarcutting a 
					INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE a.tgl_bonm >='".$tgldari."'  
					AND a.tgl_bonm <='".$tglke."' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tt_stok_opname_hasil_cutting a 
					INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id=b.id_stok_opname_hasil_cutting 
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND b.jum_stok_opname <> 0
					AND a.status_approve='t' GROUP BY b.kode_brg_jadi
				)
				a GROUP BY a.kode_brg_jadi ORDER BY a.kode_brg_jadi";
	  //=========================================================
	  		
		//$sql = "SELECT * FROM tm_stok_hasil_cutting ORDER BY kode_brg_jadi, kode_brg ";	  
		$query	= $this->db->query($sql);
		
		$jum_masuk_bgs = 0;
		$jum_masuk_lain = 0;
		$jum_masuk_retur = 0;
		
		$jum_keluar_bgs = 0;
		$jum_keluar_lain = 0;
		$jum_keluar_retur = 0;

		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {	
				//12-12-2014
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row1->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				// 26-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
				// revisi 12-12-2014
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_cutting a, 
									tt_stok_opname_hasil_cutting_detail b
									WHERE a.id = b.id_stok_opname_hasil_cutting
									AND b.kode_brg_jadi = '$row1->kode_brg_jadi' 
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
					
					if ($saldo_awal == '')
						$saldo_awal = 0;
				}
				else
					$saldo_awal = 0;
				
			/*	masuk: 
				gd perminggu = bon M masuk hasil cutting, jenis masuk bagus
				lain2 =  bon M masuk hasil cutting, jenis masuk lain2
				retur = SJ masuk bhn baku dari jahitan */
				
				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.jenis_masuk = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bgs = $hasilrow->jum_masuk;
					
					if ($jum_masuk_bgs == '')
						$jum_masuk_bgs = 0;
				}
				else
					$jum_masuk_bgs = 0;
				
				//2. hitung brg masuk lain2 (jenis = 2) dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a, 
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.jenis_masuk = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk;
					
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;
				
				//3. hitung brg masuk retur dari tm_sjmasukbhnbakupic
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_retur = $hasilrow->jum_masuk;
					
					if ($jum_masuk_retur == '')
						$jum_masuk_retur = 0;
				}
				else
					$jum_masuk_retur = 0;
												
				/*	
		keluar:
		kirim unit = SJ keluar bhn baku hasil cutting, jenis keluar bagus
		lain2 = SJ keluar bhn baku hasil cutting, jenis keluar lain2
		retur = kata ginanjar ga ada. Tapi di form input SJ keluar hasil cutting sediakan jenis keluar Pengembalian Retur */
				
				// 4. hitung brg keluar bagus jenis=1 dari tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.jenis_keluar = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bgs = $hasilrow->jum_keluar;
					
					if ($jum_keluar_bgs == '')
						$jum_keluar_bgs = 0;
				}
				else
					$jum_keluar_bgs = 0;
								
				// 5. hitung brg keluar lain jenis=3 dari bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.jenis_keluar = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
					
					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
				}
				else
					$jum_keluar_lain = 0;
								
				// 5. hitung brg keluar retur jenis=2 dari bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_retur = $hasilrow->jum_keluar;
					
					if ($jum_keluar_retur == '')
						$jum_keluar_retur = 0;
				}
				else
					$jum_keluar_retur = 0;
				
				//----------- STOK OPNAME -----------------------------
				$sql3 = "SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_cutting a, 
							tt_stok_opname_hasil_cutting_detail b
							WHERE a.id = b.id_stok_opname_hasil_cutting 
							AND b.kode_brg_jadi = '$row1->kode_brg_jadi' 
							AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
												
				$query3	= $this->db->query($sql3);
				
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
					
					if ($jum_stok_opname == '')
						$jum_stok_opname = 0;
				}
				else {
					$jum_stok_opname = 0;
				}
				//=========================================
																				
				$data_stok[] = array(		
											'kode_brg_jadi'=> $row1->kode_brg_jadi,
											'nama_brg_jadi'=> $e_product_motifname,
											'saldo_awal'=> $saldo_awal,
											'jum_masuk_bgs'=> $jum_masuk_bgs,
											'jum_masuk_lain'=> $jum_masuk_lain,
											'jum_masuk_retur'=> $jum_masuk_retur,
											'jum_keluar_bgs'=> $jum_keluar_bgs,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'jum_keluar_retur'=> $jum_keluar_retur,
											'jum_stok_opname'=> $jum_stok_opname
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  //===========================================================================
  
  // 04-06-2014
  function get_sobhnbaku($num, $offset, $id_gudang, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_bahan_baku WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_gudang != '0')
		$sql.= " AND id_gudang = '$id_gudang' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di gudang ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di gudang ini ?";
				$data_so[] = array(			'id'=> $row1->id,	
											'bulan'=> $row1->bulan,	
											'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'id_gudang'=> $id_gudang,	
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sobhnbakutanpalimit($id_gudang, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_bahan_baku WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_gudang != '0')
		$sql.= " AND id_gudang = '$id_gudang' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function get_detail_sobhnbaku($id_so) {
		$query	= $this->db->query(" SELECT a.id as id_header, a.id_gudang, a.bulan, a.tahun, a.tgl_so, b.*, 
					d.kode_brg, d.nama_brg, d.satuan, d.id_satuan_konversi, d.rumus_konversi, d.angka_faktor_konversi
					FROM tt_stok_opname_bahan_baku_detail b 
					INNER JOIN tt_stok_opname_bahan_baku a ON b.id_stok_opname_bahan_baku = a.id 
					INNER JOIN tm_barang d ON b.id_brg = d.id
					WHERE a.id = '$id_so' ORDER BY d.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// 17-03-2015
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 04-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
				
				// 25-11-2014 CONTEK DARI MODUL stok-opname mmaster.php
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
					
				// 27-05-2015 HITUNG SALDO AKHIR UTK SEMENTARA GA DIPAKE DULU
				// 22-06-2015 DIKOMEN AJAA
				// ---------- 12-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a, 
									tt_stok_opname_bahan_baku_detail b
									WHERE a.id = b.id_stok_opname_bahan_baku
									AND b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$row->id_gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$saldo_awal = $hasilx->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE b.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
							AND b.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND b.status_stok = 't' AND e.id_gudang = '$row->id_gudang' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_masuk = $hasilx->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$jum_masuk = $jum_masuk * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$jum_masuk = $jum_masuk / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$jum_masuk = $jum_masuk + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$jum_masuk = $jum_masuk - $row->angka_faktor_konversi;
				} 
				$jum_masuk = round($jum_masuk, 2);
				
				// 3. masuk lain2
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$row->id_gudang' AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;
				
				// 3. hitung keluar bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty_satawal) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$row->id_gudang' AND b.is_quilting = 'f' ");
				
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_keluar = $hasilx->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
					
					// 27-02-2015
					if ($row->id_satuan_konversi != 0) {
						if ($row->rumus_konversi == '1') {
							$jum_keluar = $jum_keluar*$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '2') {
							$jum_keluar = $jum_keluar/$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '3') {
							$jum_keluar = $jum_keluar+$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '4') {
							$jum_keluar = $jum_keluar-$row->angka_faktor_konversi;
						}
					}
				}
				else
					$jum_keluar = 0;
								
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk+$jum_masuk_lain-$jum_keluar; */
				$auto_saldo_akhir = 0;
				//-------------------------------------------------------------------------------------
				
				// 31-10-2014. 27-05-2015 DIKOMEN KARENA GA DIPAKE LG
			/*	$sqlxx = " SELECT * FROM tt_stok_opname_bahan_baku_detail_harga
							WHERE id_stok_opname_bahan_baku_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_harga
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga
							WHERE kode_brg = '$row->kode_brg' AND
							harga = '$rowxx->harga' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ---------- 12-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a 
											INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											WHERE b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$row->id_gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.harga = '".$rowxx->harga."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_harga = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_harga = 0;
						
						// 2. jum masuk bln ini
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
									INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
									AND b.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND b.status_stok = 't' AND e.id_gudang = '$row->id_gudang'
									AND b.harga = '$rowxx->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_harga = $hasilrow->jum_masuk;
							if ($jum_masuk_harga == '')
								$jum_masuk_harga = 0;
						}
						else
							$jum_masuk_harga = 0;
						
						if ($row->id_satuan_konversi != '0') {
							if ($row->rumus_konversi == '1')
								$jum_masuk_harga = $jum_masuk_harga * $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '2')
								$jum_masuk_harga = $jum_masuk_harga / $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '3')
								$jum_masuk_harga = $jum_masuk_harga + $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '4')
								$jum_masuk_harga = $jum_masuk_harga - $row->angka_faktor_konversi;
						} 
						$jum_masuk_harga = round($jum_masuk_harga, 2);
						
						// 3. jum masuk lain2 bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
									INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
									INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$row->id_gudang' AND c.harga = '$rowxx->harga' AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_lain_harga = $hasilrow->jum_masuk_lain;
							if ($jum_masuk_lain_harga == '')
								$jum_masuk_lain_harga = 0;
						}
						else
							$jum_masuk_lain_harga = 0;
						
						// 4. jum keluar bln ini
						// ambilnya bukan qty, tapi qty_satawal
						$query3	= $this->db->query(" SELECT sum(c.qty_satawal) as jum_keluar FROM tm_bonmkeluar a
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$row->id_gudang' AND c.harga = '$rowxx->harga'
									AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_harga = $hasilrow->jum_keluar;
							if ($jum_keluar_harga == '')
								$jum_keluar_harga = 0;
							
							// 27-02-2015
							if ($row->id_satuan_konversi != 0) {
								if ($row->rumus_konversi == '1') {
									$jum_keluar_harga = $jum_keluar_harga*$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '2') {
									$jum_keluar_harga = $jum_keluar_harga/$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '3') {
									$jum_keluar_harga = $jum_keluar_harga+$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '4') {
									$jum_keluar_harga = $jum_keluar_harga-$row->angka_faktor_konversi;
								}
							}
							else
								$jum_keluar_harga = 0;
						}
						// 5. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_harga = $saldo_awal_harga+$jum_masuk_harga-$jum_keluar_harga;
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_harga[] = array(
									'harga'=> $rowxx->harga,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'auto_saldo_akhir_harga'=> $auto_saldo_akhir_harga
								);
					}
				}
				else
					$detail_harga = ''; */
				// --------------------------------
				
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}
				
				// 27-05-2015
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg= '$row->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				}
				
				$tgl_so = $row->tgl_so;
				$pisah1 = explode("-", $tgl_so);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_so = $tgl1."-".$bln1."-".$thn1;
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'kode_gudang'=> $kode_gudang,
										'nama_gudang'=> $nama_gudang,
										'nama_lokasi'=> $nama_lokasi,
										'id_gudang'=> $row->id_gudang,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'nama_bln'=> $nama_bln,
										'tgl_so'=> $tgl_so,
										'id'=> $row->id,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $nama_satuan,
										// 27-05-2015
										'saldo_akhir'=> $stok,
										'stok_opname'=> $row->jum_stok_opname,
										'nama_satuan_konv'=> $nama_satuan_konv,
										//'detail_harga'=> $detail_harga,
										'auto_saldo_akhir'=> $auto_saldo_akhir
									);
				//$detail_harga = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  // 13-06-2015
  function get_detail_stokbrgbaru($id_so) {
	  // ambil id_gudang dari acuan id_so
	  $sqlxx = " SELECT id_gudang FROM tt_stok_opname_bahan_baku WHERE id='$id_so' ";
	  $queryxx = $this->db->query($sqlxx);
	
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_gudang = $hasilxx->id_gudang;
		}
		else
			$id_gudang = 0;
	  
		$query	= $this->db->query(" SELECT b.id as id_brg, a.stok, b.kode_brg, b.nama_brg, c.nama as nama_satuan, 
							b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi
							FROM tm_barang b 
							LEFT JOIN tm_stok a ON a.id_brg = b.id
							LEFT JOIN tm_satuan c ON c.id = b.satuan
							WHERE b.id_gudang = '$id_gudang' AND b.status_aktif='t' 
							AND b.id NOT IN 
							(select b.id_brg FROM tt_stok_opname_bahan_baku a
							INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
							WHERE a.id = '$id_so')
							ORDER BY b.nama_brg ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";
				
				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;
					
					$stokinduk = round($stokinduk, 4);
				} 
				else {
					$stokinduk = $row->stok;
					
					if ($stokinduk == '')
						$stokinduk = 0;
				}
				
				$detail_bahan[] = array('id'=> 0,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $stokinduk
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  // ------------------- end 13-06-2015 -------------------------------------------
  
  // 27-05-2015 dimodif, harga dan auto_saldo_akhir ga dipake
  //function savesobhnbaku($id_so, $kode_brg, $stok, $stok_fisik, $harga, $auto_saldo_akhir, $auto_saldo_akhir_harga){ 
  function savesobhnbaku($id_so, $tgl_so, $gudang, $id_brg, $id_itemnya, $stok, $stok_fisik){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  //$tgl = date("Y-m-d"); 
	  			
		// 27-05-2015 dikomen
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		/*$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($harga); $xx++) {
			$harga[$xx] = trim($harga[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			
			// 17-03-2015
			$auto_saldo_akhir_harga[$xx] = trim($auto_saldo_akhir_harga[$xx]);
			
			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
		*/
		
		// ambil id detail id_stok_opname_bahan_baku_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail 
					where id_brg = '$id_brg' 
					AND id_stok_opname_bahan_baku = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
		
		//$totalxx = $qtytotalstokfisik;
		$totalxx = $stok_fisik;
		
		// =============================== 13-06-2015 SEKALIGUS UPDATE STOKNYA =======================================================
		
		// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
			
			// 1.ambil brg masuk hasil pembelian
			$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a 
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							WHERE b.tgl_bonm >= '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jum_masuk = $hasilrow->jum_masuk;
				if ($jum_masuk == '')
					$jum_masuk = 0;
				
				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.kode_brg, a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$kode_brg = $hasilnya->kode_brg;
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
						}
						else
							$jum_masuk_konv = $jum_masuk;
					}
					else
						$jum_masuk_konv = $jum_masuk;
				}
				else
					$jum_masuk_konv = $jum_masuk;
			}
			else
				$jum_masuk_konv = 0;
			
			// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE  a.tgl_bonm >= '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
								
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE a.tgl_bonm >= '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND (a.tujuan > '2' OR a.keterangan = 'DIBEBANKAN') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
					
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							WHERE a.tgl_bonm  >= '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;
				
				$totmasuk = $jum_masuk_konv + $jum_masuk_lain;
				$totkeluar = $jum_keluar+$jum_keluar_lain;
												
				$jum_stok_akhir = $totmasuk-$totkeluar;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------

				// tambahkan stok_akhir ini ke qty SO
				$totalxx = $totalxx+$jum_stok_akhir;
		
		//cek stok terakhir tm_stok, dan update stoknya stoknya konversikan balik ke sat awal
				
				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.kode_brg, a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$kode_brg = $hasilnya->kode_brg;
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
					
					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$totalxx_konvawal = $totalxx/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$totalxx_konvawal = $totalxx*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$totalxx_konvawal = $totalxx-$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$totalxx_konvawal = $totalxx+$angka_faktor_konversi;
						}
						else
							$totalxx_konvawal = $totalxx;
					}
					else
						$totalxx_konvawal = $totalxx;
				}
				else
					$totalxx_konvawal = $totalxx;
					
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok WHERE id_brg = '$id_brg' ");
				if ($query3->num_rows() == 0){						
					$data_stok = array(
							'id_brg'=>$id_brg,
							'stok'=>$totalxx_konvawal,
							'tgl_update_stok'=>$tgl
							);
					$this->db->insert('tm_stok', $data_stok);
				}
				else {
					$hasilrow = $query3->row();
					$id_stok	= $hasilrow->id;
					
					$this->db->query(" UPDATE tm_stok SET stok = '$totalxx_konvawal', tgl_update_stok = '$tgl' 
					where id_brg= '$id_brg' ");
				}
		// ==================================================================================================
		
		if ($id_itemnya == '0') {
			$databaru = array(
						'id_stok_opname_bahan_baku'=>$id_so,
						'id_brg'=>$id_brg, 
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik,
						'status_approve'=> 't'
					);
		   $this->db->insert('tt_stok_opname_bahan_baku_detail',$databaru);
		}
		else {
			$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET jum_stok_opname = '$stok_fisik',
						auto_saldo_akhir='0', status_approve='t' WHERE id = '$iddetail' ");	
		}
		// 25-11-2014, 27-05-2015 dikomen
		/*for ($xx=0; $xx<count($harga); $xx++) {
			$totalxx = $stok_fisik[$xx];
				
			$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail_harga SET jum_stok_opname = '".$totalxx."',
							auto_saldo_akhir='".$auto_saldo_akhir_harga[$xx]."' 
							WHERE id_stok_opname_bahan_baku_detail='$iddetail' AND harga = '".$harga[$xx]."' ");
		} */
		// ====================	
  }
  
  // 06-11-2014
  function get_mutasi_stok_quilting($date_from, $date_to) {				
		// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
		
		$sql = "SELECT a.kode_brg, a.harga FROM (
				SELECT b.kode_brg_makloon as kode_brg, b.harga FROM tm_sj_hasil_makloon a 
				INNER JOIN tm_sj_hasil_makloon_detail b ON a.id = b.id_sj_hasil_makloon
				WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')   
				AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.status_stok='t' GROUP BY b.kode_brg_makloon, b.harga
				
				UNION SELECT b.kode_brg, c.harga FROM tm_bonmmasuklain a 
				INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
				INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')  
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.is_quilting='t'
				GROUP BY b.kode_brg, c.harga
				
				UNION SELECT b.kode_brg, c.harga FROM tm_bonmkeluar a 
				INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
				INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
				WHERE a.tgl_bonm >=to_date('$date_from','dd-mm-yyyy')  
				AND a.tgl_bonm <=to_date('$date_to','dd-mm-yyyy') AND b.is_quilting='t'
				GROUP BY b.kode_brg, c.harga
				
				UNION SELECT b.kode_brg, c.harga FROM tt_stok_opname_hasil_quilting a 
					INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id=b.id_stok_opname_hasil_quilting 
					INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id=c.id_stok_opname_hasil_quilting_detail 
					WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND b.jum_stok_opname <> 0
					AND a.status_approve='t' GROUP BY b.kode_brg, c.harga
				)
				a GROUP BY a.kode_brg, a.harga ORDER BY a.kode_brg, a.harga";
	  //echo $sql; die();
		$query	= $this->db->query($sql);
		
		$jum_keluar = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$data_stok = array();
		
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			foreach ($hasil as $row1) {	
				if ($row1->harga == '')
					$row1->harga = 0;
				// 12-07-2013, ambil data saldo awal dari SO bulan sebelumnya (ambil bulan dan tahun dari date_from)
				
				$queryx	= $this->db->query(" SELECT c.jum_stok_opname FROM tt_stok_opname_hasil_quilting a 
									INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id = b.id_stok_opname_hasil_quilting
									INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id = c.id_stok_opname_hasil_quilting_detail
									WHERE b.kode_brg = '$row1->kode_brg'
									AND c.harga = '$row1->harga'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
													
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
					if ($saldo_awal == '')
						$saldo_awal = 0;
				}
				else
					$saldo_awal = 0;
				
				$saldo_awal_rp = $saldo_awal*$row1->harga;
				
				// 29-10-2014: ambil brg masuk hasil pembelian quilting
				$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a 
							INNER JOIN tm_sj_hasil_makloon_detail b ON a.id = b.id_sj_hasil_makloon
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_makloon = '$row1->kode_brg'
							AND b.status_stok = 't'
							AND b.harga = '$row1->harga' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;
				
				$jum_masuk_konv = $jum_masuk;
				$jum_masuk_konv_rp = $jum_masuk_konv*$row1->harga;
				
				// ambil nama satuan
				$query3	= $this->db->query(" SELECT a.satuan, a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$satuan = $hasilnya->nama_satuan;
					$id_satuan = $hasilnya->satuan;
					$nama_brg = $hasilnya->nama_brg;
				}
				else {
					$satuan = '';
					$id_satuan = 0;
					$nama_brg = '';
				}
				// ---------------------------------------------------------------------------------------------
				
				// 2. hitung brg keluar bagus dari tm_bonmkeluar
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE  a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND c.harga = '$row1->harga'
							AND b.is_quilting = 't' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
				}
				else
					$jum_keluar = 0;
				
				$jum_keluar_rp = $jum_keluar*$row1->harga;
				
				// 3. hitung brg keluar lain dari tm_bonmkeluar
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND c.harga = '$row1->harga'
							AND b.is_quilting = 't' AND a.tujuan > '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
				}
				else
					$jum_keluar_lain = 0;
				
				$jum_keluar_lain_rp = $jum_keluar_lain*$row1->harga;
				
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND c.harga = '$row1->harga'
							AND b.is_quilting = 't' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;
				
				$jum_masuk_lain_rp = $jum_masuk_lain*$row1->harga;
				
				//==================================== 30-10-2014 STOK OPNAME ==========================================
				$query3	= $this->db->query(" SELECT c.jum_stok_opname FROM tt_stok_opname_hasil_quilting a 
									INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id = b.id_stok_opname_hasil_quilting
									INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id = c.id_stok_opname_hasil_quilting_detail
									WHERE b.kode_brg = '$row1->kode_brg' 
									AND c.harga = '$row1->harga'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
								
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					if ($jum_stok_opname == '')
						$jum_stok_opname = 0;

				}
				else {
					$jum_stok_opname = 0;
				}
				$jum_stok_opname_rp = $jum_stok_opname*$row1->harga;
				//--------------------------------------
				
				/*$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama
												FROM tm_gudang a WHERE a.kode_gudang = '$row1->kode_gudang' ");
												
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else
					$nama_gudang = ''; */
				
				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg,
											'harga'=> $row1->harga,
											//'kode_gudang'=> $row1->kode_gudang,
											//'nama_gudang'=> $nama_gudang,
											'nama_brg'=> $nama_brg,
											'id_satuan'=> $id_satuan,
											'satuan'=> $satuan,
											'saldo_awal'=> $saldo_awal,
											'saldo_awal_rp'=> $saldo_awal_rp,
											'jum_masuk'=> $jum_masuk_konv,
											'jum_masuk_rp'=> $jum_masuk_konv_rp,
											'jum_masuk_lain'=> $jum_masuk_lain,
											'jum_masuk_lain_rp'=> $jum_masuk_lain_rp,
											'jum_keluar'=> $jum_keluar,
											'jum_keluar_rp'=> $jum_keluar_rp,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'jum_keluar_lain_rp'=> $jum_keluar_lain_rp,
											'jum_stok_opname'=> $jum_stok_opname,
											'jum_stok_opname_rp'=> $jum_stok_opname_rp
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
      
}

