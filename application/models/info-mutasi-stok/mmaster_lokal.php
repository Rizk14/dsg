<?php
class Mmaster extends CI_Model{
  function __construct() { 
	  parent::__construct();
  }

    function get_jenis_barang(){
	$query	= $this->db->query("select * from tm_jenis_barang ORDER BY kode ");
    return $query->result();
  }
   function get_kelompok_barang(){
	$query	= $this->db->query("select * from tm_kelompok_barang ORDER BY kode ");
    return $query->result();
  }

 function get_mutasi_stok($date_from, $date_to, $gudang, $format_harga, $kalkulasi_ulang_so,$kel_brg,$jns_brg) {
		// 1. ambil data2 gudang dari rentang tanggal yg dipilih
		$filter = "";
		if ($gudang != '0') {
			$filter .= " AND d.id = '$gudang' ";
		}

		if ($kel_brg != '0') {
			$filter .= " AND g.kode = '$kel_brg' ";
		}

		if ($jns_brg != '0') {
			$filter .= " AND f.id = '$jns_brg' ";
		}
		// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$iperiode=$thn1.$bln1;
			// 03-10-2015
			$date_from_exp = $thn1."-".$bln1."-".$tgl1;

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}

			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

		// 21-08-2015
		// 1. QUERY DISTINCT BARANGNYA DARI SELURUH TABEL TRANSAKSI DI TANGGAL YG DIPILIH
		// 2. DIDALAM PERULANGAN QUERY TSB, QUERY KE-2 UTK MENGAMBIL DATA2 TRANSAKSI PER HARGA

		$sql1x = "SELECT a.id_brg, a.kode_brg, a.nama_brg, a.kode_gudang, a.id_gudang FROM (
			SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tt_stok_opname_bahan_baku a
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
					WHERE a.bulan='$bln_query' AND a.tahun='$thn_query'
					".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
					AND a.status_approve='t' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_apply_stok_pembelian a
				INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
				WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
				AND b.tgl_bonm <=to_date('$date_to','dd-mm-yyyy') AND a.status_aktif='t' AND b.status_stok='t' ".$filter."
				AND e.status_aktif = 't' AND a.status_aktif='t' AND e.is_mutasi_stok_harga = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmmasuklain a
				INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmkeluar a
				INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
				WHERE a.tgl_bonm >=to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <=to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tt_stok_opname_bahan_baku a
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
					WHERE a.bulan='$bln1' AND a.tahun='$thn1'
					".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
					GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				)
				a GROUP BY a.id_gudang, a.kode_gudang, a.id_brg, a.kode_brg, a.nama_brg ORDER BY a.kode_gudang, a.kode_brg";

		// BONMMASUKMANUAL GA DIPAKE DI MUTASI HARGA
		/*UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmmasukmanual a
				INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg */

		$query1x	= $this->db->query($sql1x);
		$data_header = array();

		if ($query1x->num_rows() > 0){
			$hasil1x = $query1x->result();

			foreach ($hasil1x as $row1x) {
				// 07-09-2015, ambil sum qty brg masuk lain2 dari sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f' ");
				//vardump($row1x->id_brg);
				/*if ($row1x->kode_brg == 'BNG0015')
					echo "SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f'"."<br>"; */
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;

				// 02-09-2015, ambil sum qty brg keluar. ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f' AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan <> 'DIBEBANKAN') ");
				// sum(b.qty_satawal)
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
					}
				else
					$jum_keluar = 0;

				// 3. hitung brg keluar lain dari tm_bonmkeluar. ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f' AND ((a.tujuan >= '3' AND a.tujuan <='5') OR a.keterangan='DIBEBANKAN') ");

				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
				}
				else
					$jum_keluar_lain = 0;

				// 25-08-2015
				// ambil data SO bulan ini
				/*
				$sqlxx = " SELECT b.id, b.jum_stok_opname FROM tt_stok_opname_bahan_baku a
						INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
						WHERE b.id_brg = '$row1x->id_brg' AND a.id_gudang = '$row1x->id_gudang' AND a.bulan='$bln1' AND a.tahun='$thn1'
						AND b.status_approve = 't' AND a.status_approve = 't' ";

				*/

				$sqlxx	= " SELECT id_so_detail::integer as id, saldo_so AS jum_stok_opname  FROM
							f_stockopname_gudangstoksaja('$bln1', '$thn1', to_date('$date_to','dd-mm-yyyy'))
							WHERE id_brg='$row1x->id_brg' AND id_gudang='$row1x->id_gudang' ";

				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$jum_so_total = $hasilxx->jum_stok_opname;
					$id_so_detail = $hasilxx->id;
					if ($jum_so_total == '')
						$jum_so_total = 0;
					// masuk pa eko
				}
				else {
					$id_so_detail = 0;
					$jum_so_total = 0;
				}
				//echo $row1x->id_brg." ".$jum_so_total."<br>";

				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1x->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else
					$nama_gudang = '';

				// 08-10-2015, skrg id_satuan dan id_satuan_konversi pake dari tm_stok_harga
			/*	$query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi,
									b.nama as nama_satuan FROM tm_barang a
									INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row1x->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$id_satuan = $hasilnya->satuan;
					$satuan = $hasilnya->nama_satuan;

					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;

					// ambil nama satuan konv
					$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilnya = $query3->row();
						$nama_satuan_konv = $hasilnya->nama_satuan;
					}
					else {
						$nama_satuan_konv = 'Tidak Ada';
					}
				}
				else {
					$id_satuan_konversi = 0;
					$id_satuan = 0;
					$satuan = '';
					$nama_satuan_konv = '';
				} */

				// ======================07-09-2015, AMBIL DATA URUTAN HARGANYA DARI DARI tm_stok_harga=====================
				// 3:27 DARI tm_stok_harga aja. dgn syarat, hrs dirapikan ulang data2 harga di tm_stok_harga berdasarkan id_brg dan urutannya
				// penjelasan detail ada di file history tugas 2015

				// insert ke tabel SO harga.
				// 08-10-2015 dikomen dulu utk ngetes2
				// 07-12-2015 kasih pengecekan

				if ($date_from_exp >= '2015-08-01') {
					if ($id_so_detail != 0 && $kalkulasi_ulang_so == 't') {
						$sqlxx = " DELETE FROM tt_stok_opname_bahan_baku_detail_harga WHERE id_stok_opname_bahan_baku_detail = '$id_so_detail' ";
						$this->db->query($sqlxx);
					}
				}

				$hitungsaldo = 0;
				$jum_keluar_harga = 0;
				$jum_keluar_lain_harga = 0;
				$jum_masuk_lain_harga = 0;

				$sql2x = " SELECT max(id) as id, harga, is_harga_pkp, id_satuan, id_satuan_konversi
					FROM tm_stok_harga WHERE id_brg='$row1x->id_brg'
				    GROUP BY harga , is_harga_pkp, id_satuan, id_satuan_konversi
				    ORDER BY id ";
				//echo $sql2x."<br>";die();
			 	/*
					if ($row1x->kode_brg == 'KAI0012') {
							echo " SELECT id, harga, is_harga_pkp, id_satuan, id_satuan_konversi
						FROM tm_stok_harga WHERE id_brg='$row1x->id_brg' ORDER BY id ";
				}
				*/
				$query2x	= $this->db->query($sql2x);

				$data_perharga = array();

				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->result();

					$nomor = 1;
					foreach ($hasil2x as $row2x) {
						$hargapkp = $row2x->harga;
						$is_harga_pkp = $row2x->is_harga_pkp;
						$id_satuan = $row2x->id_satuan;
						$id_satuan_konversi = $row2x->id_satuan_konversi;

						// ambil nama satuan
						$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$id_satuan' ");
						if ($query3->num_rows() > 0){
							$hasilnya = $query3->row();
							$satuan = $hasilnya->nama_satuan;
						}
						else {
							$satuan = '';
						}

						// ambil nama satuan konv
						$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$id_satuan_konversi' ");
						if ($query3->num_rows() > 0){
							$hasilnya = $query3->row();
							$nama_satuan_konv = $hasilnya->nama_satuan;
						}
						else {
							$nama_satuan_konv = 'Tidak Ada';
						}

						// ================================ 08-10-2015 satuan konversi =======================================
						$query3	= $this->db->query(" SELECT rumus_konversi, angka_faktor_konversi, konversi_harga_mutasi_stok
										FROM tm_konversi_satuan
										WHERE id_satuan_awal = '$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi' ");
						if ($query3->num_rows() > 0){
							$hasilnya = $query3->row();
							$rumus_konversi = $hasilnya->rumus_konversi;
							$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
							$konversi_harga_mutasi_stok = $hasilnya->konversi_harga_mutasi_stok;

								/*
						if ($row1x->kode_brg == 'KAI0074') {
							echo "SELECT rumus_konversi, angka_faktor_konversi, konversi_harga_mutasi_stok
										FROM tm_konversi_satuan
										WHERE id_satuan_awal = '$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi'<br>";
						}
						*/
						}
						//else {
						//	$satuan = '';
						//	$nama_satuan_konv = '';
						//}
						// ===================================================================================================

						// 07-09-2015
						if ($is_harga_pkp == 't') {
							$nama_pkp = "PKP";
							$hargapkp = $row2x->harga/1.1;

							if ($id_satuan_konversi != 0) {
								/*if ($id_satuan == 2 && $id_satuan_konversi == 3)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 6 && $id_satuan_konversi == 5)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 2 && $id_satuan_konversi == 1)
									$hargapkp = $hargapkp*$angka_faktor_konversi; */

								// 08-10-2015
								if ($konversi_harga_mutasi_stok == '1') {
									$hargapkp = $hargapkp*$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '2') {
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '3') {
									$hargapkp = $hargapkp+$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '4') {
									$hargapkp = $hargapkp-$angka_faktor_konversi;
								}
							}
						}
						else {
							$nama_pkp = "Non-PKP";
							if ($id_satuan_konversi != 0) {
								/*if ($id_satuan == 2 && $id_satuan_konversi == 3)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 6 && $id_satuan_konversi == 5)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 6 && $id_satuan_konversi == 8)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 2 && $id_satuan_konversi == 1)
									$hargapkp = $hargapkp*$angka_faktor_konversi; */

								// 08-10-2015
								if ($konversi_harga_mutasi_stok == '1') {
									$hargapkp = $hargapkp*$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '2') {
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '3') {
									$hargapkp = $hargapkp+$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '4') {
									$hargapkp = $hargapkp-$angka_faktor_konversi;
								}
							}
						}

						// 12-07-2013, ambil data saldo awal dari SO bulan sebelumnya (ambil bulan dan tahun dari date_from)
						// if ($iperiode<='202001') {
						$queryx	= $this->db->query(" SELECT c.jum_stok_opname FROM tt_stok_opname_bahan_baku a
											INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											INNER JOIN tm_barang e ON e.id = b.id_brg
											INNER JOIN tm_gudang d ON d.id = e.id_gudang
											WHERE b.id_brg = '$row1x->id_brg'
											AND d.id = '$row1x->id_gudang'
											AND c.harga = '$row2x->harga'
											AND c.is_harga_pkp = '$is_harga_pkp'
											AND a.bulan = '$bln_query'
											AND a.tahun = '$thn_query'
											AND b.status_approve = 't'
											AND a.status_approve = 't'
											AND c.id_satuan='$id_satuan'");
						/*}else{
							saldo awal ambil dari saldo akhir
							$queryx	= $this->db->query("SELECT c.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a 
											INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku 
											INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail 
											INNER JOIN tm_barang e ON e.id = b.id_brg 
											INNER JOIN tm_gudang d ON d.id = e.id_gudang 
											WHERE b.id_brg = '$row1x->id_brg' AND d.id = '$row1x->id_gudang' AND c.harga = '$row2x->harga' 
											AND c.is_harga_pkp = 't' AND a.bulan = '$bln1' AND a.tahun = '$thn1' 
											AND b.status_approve = 't' AND a.status_approve = 't' AND c.id_satuan='$id_satuan'");
						}*/
					//AND c.id_satuan='$id_satuan'

						//~ if ($row1x->kode_brg == 'KAI0121') {
							//~ echo "SELECT c.id, c.jum_stok_opname ,c.harga FROM tt_stok_opname_bahan_baku a
											//~ INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											//~ INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											//~ INNER JOIN tm_barang e ON e.id = b.id_brg
											//~ INNER JOIN tm_gudang d ON d.id = e.id_gudang
											//~ WHERE b.id_brg = '$row1x->id_brg' AND d.id = '$row1x->id_gudang'
											 //~ AND c.is_harga_pkp = '$is_harga_pkp'
											//~ AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											//~ AND a.status_approve = 't' AND c.id_satuan='$id_satuan'<br>";
						//~ }
						// if ($iperiode<='202001') {
							if ($queryx->num_rows() > 0){
								$hasilrow = $queryx->row();
								$saldo_awal = $hasilrow->jum_stok_opname;


								if ($saldo_awal == '')
									$saldo_awal = 0;
							}
							else {
								$saldo_awal = 0;
							}

							$saldo_awal_rp = $saldo_awal*$hargapkp;
							$hitungsaldo+= $saldo_awal;

						// }else{
						// 	if ($queryx->num_rows() > 0){
						// 		$hasilrow = $queryx->row();
						// 		$saldo_awal = $hasilrow->auto_saldo_akhir;
						// 		//$auto_saldo_awal = $hasilrow->auto_saldo_akhir;
						// 		if ($saldo_awal == '')
						// 			$saldo_awal = 0;
						// 		// if ($auto_saldo_awal == '')
						// 		// 	$auto_saldo_awal = 0;
						// 	}else {
						// 		$saldo_awal = 0;
						// 	}
						// 	$saldo_awal_rp = $saldo_awal*$hargapkp;
						// 	$hitungsaldo+= $saldo_awal;
						// }


						//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
						//	echo " hitungsaldo dari saldo awal = ".$hitungsaldo."<br>";

						// 29-10-2014: ambil brg masuk hasil pembelian
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk, sum(x1.diskon) as jum_diskon
									FROM tm_apply_stok_pembelian a
									INNER JOIN tm_supplier sup ON sup.id = a.id_supplier
									INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									INNER JOIN tm_pembelian_detail x1 ON b.id_pembelian_detail = x1.id
									INNER JOIN tm_barang e ON e.id = b.id_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
									AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')
									AND b.id_brg = '$row1x->id_brg'
									AND b.status_stok = 't'
									AND a.status_aktif = 't'
									AND e.id_gudang = '$row1x->id_gudang'
									AND b.harga = '$row2x->harga'
									AND sup.pkp = '$is_harga_pkp'
									AND b.id_satuan = '$id_satuan' ");

						//~ if ($row1x->kode_brg == 'MIC0169')
							//~ echo " SELECT sum(b.qty) as jum_masuk, sum(x1.diskon) as jum_diskon
									//~ FROM tm_apply_stok_pembelian a
									//~ INNER JOIN tm_supplier sup ON sup.id = a.id_supplier
									//~ INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									//~ INNER JOIN tm_pembelian_detail x1 ON b.id_pembelian_detail = x1.id
									//~ INNER JOIN tm_barang e ON e.id = b.id_brg
									//~ INNER JOIN tm_gudang d ON d.id = e.id_gudang
									//~ WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
									//~ AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
									//~ AND b.status_stok = 't' AND a.status_aktif = 't' AND e.id_gudang = '$row1x->id_gudang'
									//~ AND b.harga = '$row2x->harga' AND sup.pkp = '$is_harga_pkp'
									//~ AND b.id_satuan = '$id_satuan' <br>  ";


						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk = $hasilrow->jum_masuk;
							$jum_diskon = $hasilrow->jum_diskon;
							if ($jum_masuk == '')
								$jum_masuk = 0;
							if ($jum_diskon == '')
								$jum_diskon = 0;
						}
						else {
							$jum_masuk = 0;
							$jum_diskon = 0;
						}

						if ($id_satuan_konversi != 0) {
							if($id_satuan_konversi!=$id_satuan){
								if ($rumus_konversi == '1') {
									$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '2') {
									$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '3') {
									$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '4') {
									$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
								}
							}else
								$jum_masuk_konv = $jum_masuk;
						}
						else
							$jum_masuk_konv = $jum_masuk;

						$jum_masuk_konv_rp = ($jum_masuk_konv*$hargapkp)-$jum_diskon;
						$hitungsaldo+= $jum_masuk_konv;

						$jum_masuk_lain_rp = 0;
						//if ($row1x->kode_brg == 'KAI0008')
						//	echo "id sat konv = ".$id_satuan_konversi."<br>";
						//	echo $jum_masuk_konv."<br>";

						//if ($row1x->kode_brg == 'SPU0016')
						//	echo $jum_masuk_konv."<br>";

						//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
						//	echo " hitungsaldo dari jum masuk konv = ".$hitungsaldo."<br>";

						// 25-08-2015, masuk lain2 ini cek juga dari pembelian cash yg pelunasannya bukan di bulan ini, tapi bulan berikutnya
						// cek apakah ada pembelian cash. jika ada, cek pelunasannya ada di bulan berikutnya ga. kalo ada, masuk lain2. kalo di bulan yg sama dgn tgl SJ, masuk bagus
						// 14-09-2015: pisahkan yg sudah lunas dan yg blm lunas
						// 1.1. 28-09-2015: query cek data SJ yg statusnya blm lunas, UNION dgn yg udh ada data pelunasan di bulan berikutnya
						// a.id dikeluarkan

						$sqlxx = " SELECT sum(b.qty) as qty FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
						INNER JOIN tm_supplier c ON c.id = a.id_supplier
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj >=to_date('$date_from','dd-mm-yyyy')
								AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
								AND b.harga = '$row2x->harga' AND b.id_satuan = '$id_satuan'
								AND a.status_lunas='f' AND c.pkp='$row2x->is_harga_pkp' AND a.id IN
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur >=to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_faktur <=to_date('$date_to','dd-mm-yyyy') AND a.jenis_pembelian='1'
									AND a.status_lunas='f'
									)

								UNION SELECT sum(b.qty) as jum FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
								INNER JOIN tm_supplier c ON c.id = a.id_supplier
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj >=to_date('$date_from','dd-mm-yyyy')
								AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
								AND b.harga = '$row2x->harga' AND b.id_satuan = '$id_satuan' AND c.pkp='$row2x->is_harga_pkp'
								AND a.id IN (
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur > to_date('$date_from','dd-mm-yyyy')
									AND a.jenis_pembelian='1'
									AND a.status_lunas='t' AND a.id IN
										(SELECT c.id_pembelian_nofaktur FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
										INNER JOIN tm_payment_pembelian_nofaktur c ON b.id = c.id_payment_pembelian_detail
										WHERE a.tgl > to_date('$date_to','dd-mm-yyyy')
										AND a.jenis_pembelian='1')
								) )

								UNION SELECT sum(b.qty) as qty FROM tm_pembelian a
								INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
								INNER JOIN tm_supplier c ON c.id = a.id_supplier
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj >=to_date('$date_from','dd-mm-yyyy')
								AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
								AND b.harga = '$row2x->harga' AND b.id_satuan = '$id_satuan' AND c.pkp='$row2x->is_harga_pkp'
								AND a.status_lunas='f' AND a.id NOT IN
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur >=to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_faktur <=to_date('$date_to','dd-mm-yyyy') AND a.jenis_pembelian='1'
									AND a.status_lunas='f'
									)
								 ";



						//~ if ($row1x->kode_brg == 'KAI0121')
							//~ echo $sqlxx."<br><br>";
						//if ($row1x->kode_brg == 'TKR0002')
						//	echo $jum_masuk_konv;
						//if ($row1x->kode_brg == 'KAI0008')
						//	echo $sqlxx."<br>";

						// 02-10-2015 diganti, karena kalo select sum pasti ada row, jadi kita cek isi data di field yg diselect
						$queryxx	= $this->db->query($sqlxx);
						// 08-12-2015 PAKE PERULANGAN
						//$hasilxx = $queryxx->row();
						if ($queryxx->num_rows() > 0){
							$hasilxx1 = $queryxx->result();

							foreach ($hasilxx1 as $hasilxx) {
								if ($hasilxx->qty != '') {
								//if ($queryxx->num_rows() > 0){
									//$hasilxx = $queryxx->row();

									// 14-09-2015 jgn langsung diassign pake variabel jum_masuk_konv. tapi ambil qtynya
									//$jum_masuk_lain2 = $jum_masuk_konv;
									$jum_masuk_lain2 = $hasilxx->qty;

									// 07-10-2015-------------------------------------------------------------
									if ($id_satuan_konversi != 0) {
										if ($rumus_konversi == '1') {
											$jum_masuk_lain2 = $jum_masuk_lain2*$angka_faktor_konversi;
										}
										else if ($rumus_konversi == '2') {
											$jum_masuk_lain2 = $jum_masuk_lain2/$angka_faktor_konversi;
										}
										else if ($rumus_konversi == '3') {
											$jum_masuk_lain2 = $jum_masuk_lain2+$angka_faktor_konversi;
										}
										else if ($rumus_konversi == '4') {
											$jum_masuk_lain2 = $jum_masuk_lain2-$angka_faktor_konversi;
										}
									}
									// -------------------------------------------------------------

									$jum_masuk_konv = $jum_masuk_konv-$jum_masuk_lain2;
									$jum_masuk_konv_rp = ($jum_masuk_konv*$hargapkp)-$jum_diskon;

									$jum_masuk_lain += $jum_masuk_lain2;
									// masuk lain2, pake harga terbaru
									if ($nomor == $query2x->num_rows()) {
										$jum_masuk_lain_harga = $jum_masuk_lain;
										$jum_masuk_lain_rp = $jum_masuk_lain*$hargapkp;
										$hitungsaldo+= $jum_masuk_lain;
									}
									else {
										$jum_masuk_lain_harga = 0;
									}

									//if ($row1x->kode_brg == 'PLA0001')
									//	echo $jum_masuk_lain;
								}
								else {
									// masuk lain2, pake harga terbaru
									if ($nomor == $query2x->num_rows()) {
										$jum_masuk_lain_harga = $jum_masuk_lain;
										$jum_masuk_lain_rp = $jum_masuk_lain_harga*$hargapkp;
										$hitungsaldo+= $jum_masuk_lain_harga;
									}
									else {
										$jum_masuk_lain_harga = 0;
										$jum_masuk_lain_rp = 0;
									}
								}
							} // END FOREACH HASILXX1
						}
						// ++++++++++++++++++++++++++++++++++++++++++++++++++


						//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
						//	echo " hitungsaldo dari jum masuk lain = ".$hitungsaldo."<br>";

						// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
						// 02-09-2015 MODIF, ACUAN HARGANYA DARI SALDO AWAL DAN BARANG MASUK
						// 07-09-2015: jika barangnya dari gudang boneka atau pengadaan, jum keluar bagus = jum masuk

						// 25-09-2015 dikomen
						//$jum_keluar_lain_harga = $jum_keluar_lain;
						//$jum_keluar_lain = 0;

						//if ($row1x->kode_brg == 'LNN0003')
						//	echo "kode brg $row1x->kode_brg ".$jum_masuk_konv."<br>";

						if ($gudang == '6') {
							$jum_keluar_harga = $jum_masuk_konv;
							$jum_keluar_lain_harga = 0;
						}
						else {
							// 08-09-2015 PINDAH KESINI
							// 25-08-2015, keluar lain2 ini cek juga dari pembelian cash yg pelunasannya di bulan ini dan tgl SJnya di bulan sebelumnya
							// jika ada, keluar lain2. dan tambahkan qty masuk bagus juga.
							$sqlxx = " SELECT sum(b.qty) as jumkeluarlain, sum(b.diskon) as jumdiskon2 FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy')
								AND b.id_brg = '$row1x->id_brg' AND b.id_satuan = '$id_satuan'
								and a.pkp='$row2x->is_harga_pkp'
								AND b.harga = '$row2x->harga' AND a.status_lunas='t' AND a.id IN
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur < to_date('$date_to','dd-mm-yyyy')
									AND a.jenis_pembelian='1'
									AND a.status_lunas='t' AND a.id IN
										(SELECT c.id_pembelian_nofaktur FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
										INNER JOIN tm_payment_pembelian_nofaktur c ON b.id = c.id_payment_pembelian_detail
										WHERE a.tgl <= to_date('$date_to','dd-mm-yyyy')
										AND a.tgl >= to_date('$date_from','dd-mm-yyyy') AND a.jenis_pembelian='1')
									) ";
							//~ if ($row1x->kode_brg == 'KAI0121')
								//~ echo $sqlxx."<br>";

							//if ($row1x->kode_brg == 'LNN0003')
							//	echo "telusuri keluar lain2 ".$sqlxx."<br>";
							//if ($row1x->kode_brg == 'SLT0007')
							//	echo "telusuri keluar lain2 ".$sqlxx."<br>";
							//if ($row1x->kode_brg == 'RIN0007')
							//	echo $sqlxx;

							$queryxx	= $this->db->query($sqlxx);
							// 02-10-2015 diganti, karena kalo select sum pasti ada row, jadi kita cek isi data di field yg diselect
							$hasilxx = $queryxx->row();
							if ($hasilxx->jumkeluarlain != '') {
							//if ($queryxx->num_rows() > 0){
							//	$hasilxx = $queryxx->row();
								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "disini ".$hasilxx->jumkeluarlain."<br>";

								$jumkeluarlain = $hasilxx->jumkeluarlain;


								//if ($row1x->kode_brg == 'KAI0008')
								//	echo "id sat konv = ".$id_satuan_konversi."<br>";

								if ($id_satuan_konversi != 0) {
									if ($rumus_konversi == '1') {
										$jumkeluarlain_konv = $jumkeluarlain*$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '2') {
										$jumkeluarlain_konv = $jumkeluarlain/$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '3') {
										$jumkeluarlain_konv = $jumkeluarlain+$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '4') {
										$jumkeluarlain_konv = $jumkeluarlain-$angka_faktor_konversi;
									}
									else
										$jumkeluarlain_konv = $jumkeluarlain;
								}
								else
									$jumkeluarlain_konv = $jumkeluarlain;

								$jum_keluar_lain_harga = $jumkeluarlain_konv; // 30-09-2015, ini yg dirubah, ga pake +=, langsung = aja
								$jum_diskon2 = $hasilxx->jumdiskon2;

								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "kadieu euy0 ".$jum_keluar_lain_harga."<br>";

								$jum_masuk_konv+= $jumkeluarlain_konv;
								//echo $jum_masuk_konv." + ".$jumkeluarlain_konv."<br>";
								//break;
								//die;

								$jum_masuk_konv_rp = ($jum_masuk_konv*$hargapkp)-$jum_diskon2;
								$hitungsaldo+= $jumkeluarlain_konv;

								//if ($row1x->kode_brg == 'RIN0007')
								//	echo $jum_keluar_lain_harga."<br>";

								// 02-10-2015 MODIF. JIKA KELUAR LAIN2 DARI PEMBELIAN CASH ADA, MAKA DAHULUKAN PENGURANGANNYA KELUAR LAIN2, BARULAH YG KELUAR BAGUS
								$jum_keluar_lain += $jumkeluarlain_konv;
								$cekhitung= $hitungsaldo-$jum_keluar_lain;

								if ($cekhitung >=0 && $nomor < $query2x->num_rows()) {
									$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
									$jum_keluar_lain_harga = $jum_keluar_lain;
									$jum_keluar_lain = 0;
									//if ($row1x->kode_brg == 'RIN0007')
									//	echo $jum_keluar_lain_harga."<br>";

									// 08-09-2015, hitung apakah saldo setelah dikurangi keluar < 0 ?
									$cekhitung2 = $hitungsaldo-$jum_keluar;

									if ($cekhitung2 < 0 && $nomor < $query2x->num_rows()) {
										$jum_keluar_harga += $hitungsaldo;
										$hitungsaldo = 0;
										$jum_keluar = $jum_keluar-$jum_keluar_harga;
									}
									else if ($cekhitung2 < 0 && $nomor == $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar;
										$jum_keluar_harga += $jum_keluar;
										$jum_keluar = 0;
									}
									else if ($cekhitung2 >=0 && $nomor < $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar;
										$jum_keluar_harga += $jum_keluar;
										$jum_keluar = 0;
									}
								}
								else if ($cekhitung >=0 && $nomor == $query2x->num_rows()) {
									$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
									$jum_keluar_lain_harga = $jum_keluar_lain;
									$jum_keluar_lain = 0;

									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga += $jum_keluar;
									$jum_keluar = 0;
								}
								// -----------------------END 02-10-2015----------------------
								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "kadieu euy01 ".$jum_keluar_lain_harga."<br>";
							}



							//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
							//	echo " hitungsaldo dari jum keluar lain = ".$hitungsaldo."<br>";
							// ===========================----------------------------=================================
							else {
								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "ga ada keluar lain2 ".$jum_keluar_lain_harga."<br>";
								// 03-10-2015 diinisialisasikan lagi dari awal = 0 bisi ada bawaan variabel di proses sebelumnya
								$jum_keluar_lain_harga = 0;

								$cekhitung= $hitungsaldo-$jum_keluar;
								//if ($row1x->kode_brg == 'MIC0045')
								//	echo $hitungsaldo."<br>";

								if ($cekhitung < 0 && $nomor < $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'BNG0015')
									//	echo "didieu0 $jum_keluar_lain <br>";
									$jum_keluar_harga = $hitungsaldo;
									$hitungsaldo = 0;
									$jum_keluar = $jum_keluar-$jum_keluar_harga;
									$jum_keluar_lain_harga = 0;
									// jika minus, maka pengurangan keluar dan keluar lain di harga berikutnya
								}
								else if ($cekhitung < 0 && $nomor == $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'SLT0007')
									//	echo "didieu1 <br>";
									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga = $jum_keluar;
									$jum_keluar = 0;
									$jum_keluar_lain_harga += $jum_keluar_lain;
								}
								else if ($cekhitung >=0 && $nomor < $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'RIN0007')
									//	echo "didieu2 ".$jum_keluar_lain_harga."<br>";

									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga = $jum_keluar;
									$jum_keluar = 0;

									// 08-09-2015, hitung apakah saldo setelah dikurangi keluar lain < 0 ?
									$cekhitung2 = $hitungsaldo-$jum_keluar_lain;

									if ($cekhitung2 < 0 && $nomor < $query2x->num_rows()) {
										//$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										//$jum_keluar_lain_harga = $jum_keluar_lain;
										$jum_keluar_lain_harga += $hitungsaldo;
										$hitungsaldo = 0;
										$jum_keluar_lain = $jum_keluar_lain-$jum_keluar_lain_harga;

										//if ($row1x->kode_brg == 'SLT0007')
										//	echo "didieu21 ".$jum_keluar_lain_harga."<br>";
									}
									else if ($cekhitung2 < 0 && $nomor == $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										$jum_keluar_lain_harga += $jum_keluar_lain;
										$jum_keluar_lain = 0;
										//if ($row1x->kode_brg == 'SLT0007')
										//	echo "didieu22 ".$jum_keluar_lain_harga."<br>";
									}
									else if ($cekhitung2 >=0 && $nomor < $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										$jum_keluar_lain_harga += $jum_keluar_lain;
										$jum_keluar_lain = 0;
										//if ($row1x->kode_brg == 'SLT0007')
										//	echo "didieu23 ".$jum_keluar_lain_harga."<br>";
									}
								}
								else if ($cekhitung >=0 && $nomor == $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'RIN0007')
									//	echo "didieu3 ".$jum_keluar_lain_harga."<br>";
									//if ($row1x->kode_brg == 'BNG0015')
									//	echo "didieux $jum_keluar_lain <br>";
									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga = $jum_keluar;
									$jum_keluar = 0;

									// 02-10-2015
									//if ($jum_keluar_lain != 0) {
										$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										$jum_keluar_lain_harga += $jum_keluar_lain;
										// 30-09-2015
										$jum_keluar_lain = 0;
									//}
									/*else {
										$jum_keluar_lain_harga = 0;
										$jum_keluar_lain = 0;
									} */
								}
							} // end cek ada keluarlain dari pembelian cash
						} // end if
						//var_dump($jum_keluar_rp);
						$jum_keluar_rp = $jum_keluar_harga*$hargapkp;
						$jum_keluar_lain_rp = $jum_keluar_lain_harga*$hargapkp;

						//if ($row1x->kode_brg == 'RIN0007')
						//	echo "disini ".$jum_keluar_lain_harga."<br>";

						//==================================== STOK OPNAME ==========================================
						// 25-08-2015
						$saldo_akhir = $saldo_awal+$jum_masuk_konv+$jum_masuk_lain_harga-$jum_keluar_harga-$jum_keluar_lain_harga;

						//if ($row1x->kode_brg == 'BNG0015')
							//echo $saldo_awal." ".$jum_masuk_konv." ".$jum_masuk_lain_harga." ".$jum_keluar_harga." ".$jum_keluar_lain_harga." ".$hitungsaldo."<br>";

						$saldo_akhir_rp = ($saldo_akhir*$hargapkp)-$jum_diskon;

						if ($gudang == '6') {
							$jum_so_harga = 0;
						}
						else {
							if ($jum_so_total >= 0 && ($nomor == $query2x->num_rows()))
								$jum_so_harga = $jum_so_total;
							else {
								if ($jum_so_total < $saldo_akhir) {
									$jum_so_harga = $jum_so_total;
									$jum_so_total = 0;
								}
								else {
									$jum_so_harga = $saldo_akhir;
									$jum_so_total= $jum_so_total-$saldo_akhir;
								}
							}
						}

						// insert ke tabel SO harga.
						// 07-10-2015 dikomen dulu utk ngetes2
						if ($date_from_exp >= '2015-08-01') {
							if ($id_so_detail != 0 && $kalkulasi_ulang_so == 't') {
								$data_so_harga = array(
								  'id_stok_opname_bahan_baku_detail'=>$id_so_detail,
								  'harga'=>$row2x->harga,
								  'jum_stok_opname'=>$jum_so_harga,
								  'is_harga_pkp'=>$is_harga_pkp,
								  'id_satuan'=>$id_satuan
								  );
								$this->db->insert('tt_stok_opname_bahan_baku_detail_harga',$data_so_harga);
							}
						}
						//echo $row1x->kode_brg." : ".$jum_masuk_konv."<br>";
						//die;
						//break;
						//($jum_masuk_konv);
						$jum_so_harga_rp = $jum_so_harga*$hargapkp;

						$selisih = $jum_so_harga-$saldo_akhir;
						$selisih = round($selisih,4);
						$selisih_rp = $selisih*$hargapkp;
						//--------------------------------------
						//if ($row1x->kode_brg == 'SLT0003')
						//	echo "kode brg $row1x->kode_brg ".$jum_keluar_harga."<br>";
						$data_perharga[] = array(
													'harga'=> $hargapkp,
													'nama_pkp'=> $nama_pkp,
													'saldo_awal'=> $saldo_awal,
													'saldo_awal_rp'=> $saldo_awal_rp,
													'jum_masuk'=> $jum_masuk_konv,
													'jum_masuk_rp'=> $jum_masuk_konv_rp,
													'jum_masuk_lain'=> $jum_masuk_lain_harga,
													'jum_masuk_lain_rp'=> $jum_masuk_lain_rp,
													'jum_keluar'=> $jum_keluar_harga,
													'jum_keluar_rp'=> $jum_keluar_rp,
													'jum_keluar_lain'=> $jum_keluar_lain_harga,
													'jum_keluar_lain_rp'=> $jum_keluar_lain_rp,
													'saldo_akhir'=> $saldo_akhir,
													'saldo_akhir_rp'=> $saldo_akhir_rp,
													'jum_stok_opname'=> $jum_so_harga,
													'jum_stok_opname_rp'=> $jum_so_harga_rp,
													'selisih'=> $selisih,
													'selisih_rp'=> $selisih_rp,
													'id_satuan'=> $id_satuan,
													'satuan'=> $satuan,
													'nama_satuan_konv'=> $nama_satuan_konv,
													);
						$nomor++;
					} // endforeach2 perharga
				} // end if2
				// 08-10-2015

				else {
					$data_perharga[] = array(
													'harga'=> 0,
													'nama_pkp'=> '',
													'saldo_awal'=> 0,
													'saldo_awal_rp'=> 0,
													'jum_masuk'=> 0,
													'jum_masuk_rp'=> 0,
													'jum_masuk_lain'=> 0,
													'jum_masuk_lain_rp'=> 0,
													'jum_keluar'=> 0,
													'jum_keluar_rp'=> 0,
													'jum_keluar_lain'=> 0,
													'jum_keluar_lain_rp'=> 0,
													'saldo_akhir'=> 0,
													'saldo_akhir_rp'=> 0,
													'jum_stok_opname'=> $jum_so_total,
													'jum_stok_opname_rp'=> 0,
													'selisih'=> 0,
													'selisih_rp'=> 0,
													'id_satuan'=> 0,
													'satuan'=> '',
													'nama_satuan_konv'=> '',
													);
				}

				$data_header[] = array(		'kode_brg'=> $row1x->kode_brg,
											'kode_gudang'=> $row1x->kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_brg'=> $row1x->nama_brg,
											/*'id_satuan'=> $id_satuan,
											'satuan'=> $satuan,
											'nama_satuan_konv'=> $nama_satuan_konv, */
											'data_perharga'=> $data_perharga
											);
				$data_perharga = array();
			} // end foreach1
		} // end if1


		// ===========================================BATAS AKHIR 24-08-2015 dan 25======================================================
		else {
			$data_header = '';
		}
		return $data_header;
  }

  function get_mutasi_stok2($date_from, $date_to, $gudang, $format_harga, $kalkulasi_ulang_so,$kel_brg,$jns_brg) {
		// 1. ambil data2 gudang dari rentang tanggal yg dipilih
		$filter = "";
		if ($gudang != '0') {
			$filter .= " AND d.id = '$gudang' ";
		}

		if ($kel_brg != '0') {
			$filter .= " AND g.kode = '$kel_brg' ";
		}

		if ($jns_brg != '0') {
			$filter .= " AND f.id = '$jns_brg' ";
		}
		// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			// 03-10-2015
			$date_from_exp = $thn1."-".$bln1."-".$tgl1;

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}

			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

		// 21-08-2015
		// 1. QUERY DISTINCT BARANGNYA DARI SELURUH TABEL TRANSAKSI DI TANGGAL YG DIPILIH
		// 2. DIDALAM PERULANGAN QUERY TSB, QUERY KE-2 UTK MENGAMBIL DATA2 TRANSAKSI PER HARGA

		$sql1x = "SELECT a.id_brg, a.kode_brg, a.nama_brg, a.kode_gudang, a.id_gudang FROM (
			SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tt_stok_opname_bahan_baku a
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
					WHERE a.bulan='$bln_query' AND a.tahun='$thn_query'
					".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
					AND a.status_approve='t' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_apply_stok_pembelian a
				INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
				WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
				AND b.tgl_bonm <=to_date('$date_to','dd-mm-yyyy') AND a.status_aktif='t' AND b.status_stok='t' ".$filter."
				AND e.status_aktif = 't' AND a.status_aktif='t' AND e.is_mutasi_stok_harga = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmmasuklain a
				INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmkeluar a
				INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
				WHERE a.tgl_bonm >=to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <=to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
				GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg

				UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tt_stok_opname_bahan_baku a
					INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id=b.id_stok_opname_bahan_baku
					INNER JOIN tm_barang e ON e.id = b.id_brg
					INNER JOIN tm_gudang d ON d.id = e.id_gudang
					INNER JOIN tm_jenis_barang f ON f.id=e.id_jenis_barang
					INNER JOIN tm_kelompok_barang g ON g.kode=f.kode_kel_brg
					WHERE a.bulan='$bln1' AND a.tahun='$thn1'
					".$filter." AND e.status_aktif = 't' AND e.is_mutasi_stok_harga = 't'
					GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg
				)
				a GROUP BY a.id_gudang, a.kode_gudang, a.id_brg, a.kode_brg, a.nama_brg ORDER BY a.kode_gudang, a.kode_brg";

		// BONMMASUKMANUAL GA DIPAKE DI MUTASI HARGA
		/*UNION SELECT b.id_brg, e.kode_brg, e.nama_brg, d.kode_gudang, d.id as id_gudang FROM tm_bonmmasukmanual a
				INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
				INNER JOIN tm_barang e ON e.id = b.id_brg
				INNER JOIN tm_gudang d ON d.id = e.id_gudang
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')
				".$filter." AND e.status_aktif = 't' GROUP BY d.id, d.kode_gudang, b.id_brg, e.kode_brg, e.nama_brg */

		$query1x	= $this->db->query($sql1x);
		$data_header = array();

		if ($query1x->num_rows() > 0){
			$hasil1x = $query1x->result();

			foreach ($hasil1x as $row1x) {
				// 07-09-2015, ambil sum qty brg masuk lain2 dari sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f' ");
				/*if ($row1x->kode_brg == 'BNG0015')
					echo "SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f'"."<br>"; */
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;

				// 02-09-2015, ambil sum qty brg keluar. ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f' AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan <> 'DIBEBANKAN') ");
				// sum(b.qty_satawal)
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
					}
				else
					$jum_keluar = 0;

				// 3. hitung brg keluar lain dari tm_bonmkeluar. ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.id = b.id_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
							AND d.id = '$row1x->id_gudang'
							AND b.is_quilting = 'f' AND ((a.tujuan >= '3' AND a.tujuan <='5') OR a.keterangan='DIBEBANKAN') ");

				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
				}
				else
					$jum_keluar_lain = 0;

				// 25-08-2015
				// ambil data SO bulan ini
			
					$sqlxx = " SELECT id_so_detail::integer as id, saldo_so AS jum_stok_opname  FROM
								f_stockopname_gudangstoksaja('$bln1', '$thn1', to_date('$date_to','dd-mm-yyyy'))
								WHERE id_brg='$row1x->id_brg' AND id_gudang='$row1x->id_gudang' ";

				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$jum_so_total = $hasilxx->jum_stok_opname;
					$id_so_detail = $hasilxx->id;
					if ($jum_so_total == '')
						$jum_so_total = 0;
				}
				else {
					$id_so_detail = 0;
					$jum_so_total = 0;
				}
				//echo $row1x->id_brg." ".$jum_so_total."<br>";

				$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$row1x->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else
					$nama_gudang = '';

				// 08-10-2015, skrg id_satuan dan id_satuan_konversi pake dari tm_stok_harga
			/*	$query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi,
									b.nama as nama_satuan FROM tm_barang a
									INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$row1x->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$id_satuan = $hasilnya->satuan;
					$satuan = $hasilnya->nama_satuan;

					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;

					// ambil nama satuan konv
					$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilnya = $query3->row();
						$nama_satuan_konv = $hasilnya->nama_satuan;
					}
					else {
						$nama_satuan_konv = 'Tidak Ada';
					}
				}
				else {
					$id_satuan_konversi = 0;
					$id_satuan = 0;
					$satuan = '';
					$nama_satuan_konv = '';
				} */

				// ======================07-09-2015, AMBIL DATA URUTAN HARGANYA DARI DARI tm_stok_harga=====================
				// 3:27 DARI tm_stok_harga aja. dgn syarat, hrs dirapikan ulang data2 harga di tm_stok_harga berdasarkan id_brg dan urutannya
				// penjelasan detail ada di file history tugas 2015

				// insert ke tabel SO harga.
				// 08-10-2015 dikomen dulu utk ngetes2
				// 07-12-2015 kasih pengecekan
				if ($date_from_exp >= '2015-08-01') {
					if ($id_so_detail != 0 && $kalkulasi_ulang_so == 't') {
						$sqlxx = " DELETE FROM tt_stok_opname_bahan_baku_detail_harga WHERE id_stok_opname_bahan_baku_detail = '$id_so_detail' ";
						$this->db->query($sqlxx);
					}
				}

				$hitungsaldo = 0; $jum_keluar_harga = 0; $jum_keluar_lain_harga = 0; $jum_masuk_lain_harga = 0;
				$sql2x = " SELECT id, harga, is_harga_pkp, id_satuan, id_satuan_konversi
						FROM tm_stok_harga WHERE id_brg='$row1x->id_brg' ORDER BY id ";
				//echo $sql2x."<br>";die();
			 	/*
					if ($row1x->kode_brg == 'KAI0012') {
							echo " SELECT id, harga, is_harga_pkp, id_satuan, id_satuan_konversi
						FROM tm_stok_harga WHERE id_brg='$row1x->id_brg' ORDER BY id ";
				}
				*/
				$query2x	= $this->db->query($sql2x);

				$data_perharga = array();

				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->result();

					$nomor = 1;
					foreach ($hasil2x as $row2x) {
						$hargapkp = $row2x->harga;
						$is_harga_pkp = $row2x->is_harga_pkp;
						$id_satuan = $row2x->id_satuan;
						$id_satuan_konversi = $row2x->id_satuan_konversi;

						// ambil nama satuan
						$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$id_satuan' ");
						if ($query3->num_rows() > 0){
							$hasilnya = $query3->row();
							$satuan = $hasilnya->nama_satuan;
						}
						else {
							$satuan = '';
						}

						// ambil nama satuan konv
						$query3	= $this->db->query(" SELECT nama as nama_satuan FROM tm_satuan WHERE id = '$id_satuan_konversi' ");
						if ($query3->num_rows() > 0){
							$hasilnya = $query3->row();
							$nama_satuan_konv = $hasilnya->nama_satuan;
						}
						else {
							$nama_satuan_konv = 'Tidak Ada';
						}

						// ================================ 08-10-2015 satuan konversi =======================================
						$query3	= $this->db->query(" SELECT rumus_konversi, angka_faktor_konversi, konversi_harga_mutasi_stok
										FROM tm_konversi_satuan
										WHERE id_satuan_awal = '$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi' ");
						if ($query3->num_rows() > 0){
							$hasilnya = $query3->row();
							$rumus_konversi = $hasilnya->rumus_konversi;
							$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
							$konversi_harga_mutasi_stok = $hasilnya->konversi_harga_mutasi_stok;

								/*
						if ($row1x->kode_brg == 'KAI0074') {
							echo "SELECT rumus_konversi, angka_faktor_konversi, konversi_harga_mutasi_stok
										FROM tm_konversi_satuan
										WHERE id_satuan_awal = '$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi'<br>";
						}
						*/
						}
						//else {
						//	$satuan = '';
						//	$nama_satuan_konv = '';
						//}
						// ===================================================================================================

						// 07-09-2015
						if ($is_harga_pkp == 't') {
							$nama_pkp = "PKP";
							$hargapkp = $row2x->harga/1.1;

							if ($id_satuan_konversi != 0) {
								/*if ($id_satuan == 2 && $id_satuan_konversi == 3)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 6 && $id_satuan_konversi == 5)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 2 && $id_satuan_konversi == 1)
									$hargapkp = $hargapkp*$angka_faktor_konversi; */

								// 08-10-2015
								if ($konversi_harga_mutasi_stok == '1') {
									$hargapkp = $hargapkp*$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '2') {
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '3') {
									$hargapkp = $hargapkp+$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '4') {
									$hargapkp = $hargapkp-$angka_faktor_konversi;
								}
							}
						}
						else {
							$nama_pkp = "Non-PKP";
							if ($id_satuan_konversi != 0) {
								/*if ($id_satuan == 2 && $id_satuan_konversi == 3)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 6 && $id_satuan_konversi == 5)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 6 && $id_satuan_konversi == 8)
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								else if ($id_satuan == 2 && $id_satuan_konversi == 1)
									$hargapkp = $hargapkp*$angka_faktor_konversi; */

								// 08-10-2015
								if ($konversi_harga_mutasi_stok == '1') {
									$hargapkp = $hargapkp*$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '2') {
									$hargapkp = $hargapkp/$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '3') {
									$hargapkp = $hargapkp+$angka_faktor_konversi;
								}
								else if ($konversi_harga_mutasi_stok == '4') {
									$hargapkp = $hargapkp-$angka_faktor_konversi;
								}
							}
						}

						// 12-07-2013, ambil data saldo awal dari SO bulan sebelumnya (ambil bulan dan tahun dari date_from)

						$queryx	= $this->db->query(" sSELECT c.jum_stok_opname FROM tt_stok_opname_bahan_baku a
											INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											INNER JOIN tm_barang e ON e.id = b.id_brg
											INNER JOIN tm_gudang d ON d.id = e.id_gudang
											WHERE b.id_brg = '$row1x->id_brg' AND d.id = '$row1x->id_gudang'
											AND c.harga = '$row2x->harga' AND c.is_harga_pkp = '$is_harga_pkp'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.id_satuan='$id_satuan'");
					//AND c.id_satuan='$id_satuan'

						//~ if ($row1x->kode_brg == 'KAI0121') {
							//~ echo "SELECT c.id, c.jum_stok_opname ,c.harga FROM tt_stok_opname_bahan_baku a
											//~ INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											//~ INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											//~ INNER JOIN tm_barang e ON e.id = b.id_brg
											//~ INNER JOIN tm_gudang d ON d.id = e.id_gudang
											//~ WHERE b.id_brg = '$row1x->id_brg' AND d.id = '$row1x->id_gudang'
											 //~ AND c.is_harga_pkp = '$is_harga_pkp'
											//~ AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											//~ AND a.status_approve = 't' AND c.id_satuan='$id_satuan'<br>";
						//~ }

						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal = $hasilrow->jum_stok_opname;
							if ($saldo_awal == '')
								$saldo_awal = 0;
						}
						else {
							$saldo_awal = 0;
						}

						$saldo_awal_rp = $saldo_awal*$hargapkp;
						$hitungsaldo+= $saldo_awal;

						//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
						//	echo " hitungsaldo dari saldo awal = ".$hitungsaldo."<br>";

						// 29-10-2014: ambil brg masuk hasil pembelian
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk, sum(x1.diskon) as jum_diskon
									FROM tm_apply_stok_pembelian a
									INNER JOIN tm_supplier sup ON sup.id = a.id_supplier
									INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									INNER JOIN tm_pembelian_detail x1 ON b.id_pembelian_detail = x1.id
									INNER JOIN tm_barang e ON e.id = b.id_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
									AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')
									AND b.id_brg = '$row1x->id_brg'
									AND b.status_stok = 't'
									AND a.status_aktif = 't'
									AND e.id_gudang = '$row1x->id_gudang'
									AND b.harga = '$row2x->harga'
									AND sup.pkp = '$is_harga_pkp'
									AND b.id_satuan = '$id_satuan' ");

						//~ if ($row1x->kode_brg == 'MIC0169')
							//~ echo " SELECT sum(b.qty) as jum_masuk, sum(x1.diskon) as jum_diskon
									//~ FROM tm_apply_stok_pembelian a
									//~ INNER JOIN tm_supplier sup ON sup.id = a.id_supplier
									//~ INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									//~ INNER JOIN tm_pembelian_detail x1 ON b.id_pembelian_detail = x1.id
									//~ INNER JOIN tm_barang e ON e.id = b.id_brg
									//~ INNER JOIN tm_gudang d ON d.id = e.id_gudang
									//~ WHERE b.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
									//~ AND b.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
									//~ AND b.status_stok = 't' AND a.status_aktif = 't' AND e.id_gudang = '$row1x->id_gudang'
									//~ AND b.harga = '$row2x->harga' AND sup.pkp = '$is_harga_pkp'
									//~ AND b.id_satuan = '$id_satuan' <br>  ";


						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk = $hasilrow->jum_masuk;
							$jum_diskon = $hasilrow->jum_diskon;
							if ($jum_masuk == '')
								$jum_masuk = 0;
							if ($jum_diskon == '')
								$jum_diskon = 0;
						}
						else {
							$jum_masuk = 0;
							$jum_diskon = 0;
						}

						if ($id_satuan_konversi != 0) {
							if ($rumus_konversi == '1') {
								$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '2') {
								$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '3') {
								$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
							}
							else if ($rumus_konversi == '4') {
								$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
							}
							else
								$jum_masuk_konv = $jum_masuk;
						}
						else
							$jum_masuk_konv = $jum_masuk;

						$jum_masuk_konv_rp = ($jum_masuk_konv*$hargapkp)-$jum_diskon;
						$hitungsaldo+= $jum_masuk_konv;

						$jum_masuk_lain_rp = 0;
						//if ($row1x->kode_brg == 'KAI0008')
						//	echo "id sat konv = ".$id_satuan_konversi."<br>";
						//	echo $jum_masuk_konv."<br>";

						//if ($row1x->kode_brg == 'SPU0016')
						//	echo $jum_masuk_konv."<br>";

						//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
						//	echo " hitungsaldo dari jum masuk konv = ".$hitungsaldo."<br>";

						// 25-08-2015, masuk lain2 ini cek juga dari pembelian cash yg pelunasannya bukan di bulan ini, tapi bulan berikutnya
						// cek apakah ada pembelian cash. jika ada, cek pelunasannya ada di bulan berikutnya ga. kalo ada, masuk lain2. kalo di bulan yg sama dgn tgl SJ, masuk bagus
						// 14-09-2015: pisahkan yg sudah lunas dan yg blm lunas
						// 1.1. 28-09-2015: query cek data SJ yg statusnya blm lunas, UNION dgn yg udh ada data pelunasan di bulan berikutnya
						// a.id dikeluarkan

						$sqlxx = " SELECT sum(b.qty) as qty FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
						INNER JOIN tm_supplier c ON c.id = a.id_supplier
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj >=to_date('$date_from','dd-mm-yyyy')
								AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
								AND b.harga = '$row2x->harga' AND b.id_satuan = '$id_satuan'
								AND a.status_lunas='f' AND c.pkp='$row2x->is_harga_pkp' AND a.id IN
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur >=to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_faktur <=to_date('$date_to','dd-mm-yyyy') AND a.jenis_pembelian='1'
									AND a.status_lunas='f'
									)

								UNION SELECT sum(b.qty) as jum FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
								INNER JOIN tm_supplier c ON c.id = a.id_supplier
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj >=to_date('$date_from','dd-mm-yyyy')
								AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
								AND b.harga = '$row2x->harga' AND b.id_satuan = '$id_satuan' AND c.pkp='$row2x->is_harga_pkp'
								AND a.id IN (
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur > to_date('$date_from','dd-mm-yyyy')
									AND a.jenis_pembelian='1'
									AND a.status_lunas='t' AND a.id IN
										(SELECT c.id_pembelian_nofaktur FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
										INNER JOIN tm_payment_pembelian_nofaktur c ON b.id = c.id_payment_pembelian_detail
										WHERE a.tgl > to_date('$date_to','dd-mm-yyyy')
										AND a.jenis_pembelian='1')
								) )

								UNION SELECT sum(b.qty) as qty FROM tm_pembelian a
								INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
								INNER JOIN tm_supplier c ON c.id = a.id_supplier
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj >=to_date('$date_from','dd-mm-yyyy')
								AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.id_brg = '$row1x->id_brg'
								AND b.harga = '$row2x->harga' AND b.id_satuan = '$id_satuan' AND c.pkp='$row2x->is_harga_pkp'
								AND a.status_lunas='f' AND a.id NOT IN
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur >=to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_faktur <=to_date('$date_to','dd-mm-yyyy') AND a.jenis_pembelian='1'
									AND a.status_lunas='f'
									)
								 ";



						//~ if ($row1x->kode_brg == 'KAI0121')
							//~ echo $sqlxx."<br><br>";
						//if ($row1x->kode_brg == 'TKR0002')
						//	echo $jum_masuk_konv;
						//if ($row1x->kode_brg == 'KAI0008')
						//	echo $sqlxx."<br>";

						// 02-10-2015 diganti, karena kalo select sum pasti ada row, jadi kita cek isi data di field yg diselect
						$queryxx	= $this->db->query($sqlxx);
						// 08-12-2015 PAKE PERULANGAN
						//$hasilxx = $queryxx->row();
						if ($queryxx->num_rows() > 0){
							$hasilxx1 = $queryxx->result();

							foreach ($hasilxx1 as $hasilxx) {
								if ($hasilxx->qty != '') {
								//if ($queryxx->num_rows() > 0){
									//$hasilxx = $queryxx->row();

									// 14-09-2015 jgn langsung diassign pake variabel jum_masuk_konv. tapi ambil qtynya
									//$jum_masuk_lain2 = $jum_masuk_konv;
									$jum_masuk_lain2 = $hasilxx->qty;

									// 07-10-2015-------------------------------------------------------------
									if ($id_satuan_konversi != 0) {
										if ($rumus_konversi == '1') {
											$jum_masuk_lain2 = $jum_masuk_lain2*$angka_faktor_konversi;
										}
										else if ($rumus_konversi == '2') {
											$jum_masuk_lain2 = $jum_masuk_lain2/$angka_faktor_konversi;
										}
										else if ($rumus_konversi == '3') {
											$jum_masuk_lain2 = $jum_masuk_lain2+$angka_faktor_konversi;
										}
										else if ($rumus_konversi == '4') {
											$jum_masuk_lain2 = $jum_masuk_lain2-$angka_faktor_konversi;
										}
									}
									// -------------------------------------------------------------

									$jum_masuk_konv = $jum_masuk_konv-$jum_masuk_lain2;
									$jum_masuk_konv_rp = ($jum_masuk_konv*$hargapkp)-$jum_diskon;

									$jum_masuk_lain += $jum_masuk_lain2;
									// masuk lain2, pake harga terbaru
									if ($nomor == $query2x->num_rows()) {
										$jum_masuk_lain_harga = $jum_masuk_lain;
										$jum_masuk_lain_rp = $jum_masuk_lain*$hargapkp;
										$hitungsaldo+= $jum_masuk_lain;
									}
									else {
										$jum_masuk_lain_harga = 0;
									}

									//if ($row1x->kode_brg == 'PLA0001')
									//	echo $jum_masuk_lain;
								}
								else {
									// masuk lain2, pake harga terbaru
									if ($nomor == $query2x->num_rows()) {
										$jum_masuk_lain_harga = $jum_masuk_lain;
										$jum_masuk_lain_rp = $jum_masuk_lain_harga*$hargapkp;
										$hitungsaldo+= $jum_masuk_lain_harga;
									}
									else {
										$jum_masuk_lain_harga = 0;
										$jum_masuk_lain_rp = 0;
									}
								}
							} // END FOREACH HASILXX1
						}
						// ++++++++++++++++++++++++++++++++++++++++++++++++++


						//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
						//	echo " hitungsaldo dari jum masuk lain = ".$hitungsaldo."<br>";

						// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
						// 02-09-2015 MODIF, ACUAN HARGANYA DARI SALDO AWAL DAN BARANG MASUK
						// 07-09-2015: jika barangnya dari gudang boneka atau pengadaan, jum keluar bagus = jum masuk

						// 25-09-2015 dikomen
						//$jum_keluar_lain_harga = $jum_keluar_lain;
						//$jum_keluar_lain = 0;

						//if ($row1x->kode_brg == 'LNN0003')
						//	echo "kode brg $row1x->kode_brg ".$jum_masuk_konv."<br>";

						if ($gudang == '6') {
							$jum_keluar_harga = $jum_masuk_konv;
							$jum_keluar_lain_harga = 0;
						}
						else {
							// 08-09-2015 PINDAH KESINI
							// 25-08-2015, keluar lain2 ini cek juga dari pembelian cash yg pelunasannya di bulan ini dan tgl SJnya di bulan sebelumnya
							// jika ada, keluar lain2. dan tambahkan qty masuk bagus juga.
							$sqlxx = " SELECT sum(b.qty) as jumkeluarlain, sum(b.diskon) as jumdiskon2 FROM tm_pembelian a INNER JOIN tm_pembelian_detail b ON a.id = b.id_pembelian
								WHERE a.status_aktif='t' AND a.jenis_pembelian='1' AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy')
								AND b.id_brg = '$row1x->id_brg' AND b.id_satuan = '$id_satuan'
								AND b.harga = '$row2x->harga' AND a.status_lunas='t' AND a.id IN
									(select b.id_sj_pembelian FROM tm_pembelian_nofaktur a
									INNER JOIN tm_pembelian_nofaktur_sj b ON a.id = b.id_pembelian_nofaktur
									WHERE a.tgl_faktur < to_date('$date_to','dd-mm-yyyy')
									AND a.jenis_pembelian='1'
									AND a.status_lunas='t' AND a.id IN
										(SELECT c.id_pembelian_nofaktur FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
										INNER JOIN tm_payment_pembelian_nofaktur c ON b.id = c.id_payment_pembelian_detail
										WHERE a.tgl <= to_date('$date_to','dd-mm-yyyy')
										AND a.tgl >= to_date('$date_from','dd-mm-yyyy') AND a.jenis_pembelian='1')
									) ";
							//~ if ($row1x->kode_brg == 'KAI0121')
								//~ echo $sqlxx."<br>";

							//if ($row1x->kode_brg == 'LNN0003')
							//	echo "telusuri keluar lain2 ".$sqlxx."<br>";
							//if ($row1x->kode_brg == 'SLT0007')
							//	echo "telusuri keluar lain2 ".$sqlxx."<br>";
							//if ($row1x->kode_brg == 'RIN0007')
							//	echo $sqlxx;

							$queryxx	= $this->db->query($sqlxx);
							// 02-10-2015 diganti, karena kalo select sum pasti ada row, jadi kita cek isi data di field yg diselect
							$hasilxx = $queryxx->row();
							if ($hasilxx->jumkeluarlain != '') {
							//if ($queryxx->num_rows() > 0){
							//	$hasilxx = $queryxx->row();
								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "disini ".$hasilxx->jumkeluarlain."<br>";

								$jumkeluarlain = $hasilxx->jumkeluarlain;

								//if ($row1x->kode_brg == 'KAI0008')
								//	echo "id sat konv = ".$id_satuan_konversi."<br>";

								if ($id_satuan_konversi != 0) {
									if ($rumus_konversi == '1') {
										$jumkeluarlain_konv = $jumkeluarlain*$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '2') {
										$jumkeluarlain_konv = $jumkeluarlain/$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '3') {
										$jumkeluarlain_konv = $jumkeluarlain+$angka_faktor_konversi;
									}
									else if ($rumus_konversi == '4') {
										$jumkeluarlain_konv = $jumkeluarlain-$angka_faktor_konversi;
									}
									else
										$jumkeluarlain_konv = $jumkeluarlain;
								}
								else
									$jumkeluarlain_konv = $jumkeluarlain;

								$jum_keluar_lain_harga = $jumkeluarlain_konv; // 30-09-2015, ini yg dirubah, ga pake +=, langsung = aja
								$jum_diskon2 = $hasilxx->jumdiskon2;

								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "kadieu euy0 ".$jum_keluar_lain_harga."<br>";

								$jum_masuk_konv+= $jumkeluarlain_konv;
								$jum_masuk_konv_rp = ($jum_masuk_konv*$hargapkp)-$jum_diskon2;
								$hitungsaldo+= $jumkeluarlain_konv;

								//if ($row1x->kode_brg == 'RIN0007')
								//	echo $jum_keluar_lain_harga."<br>";

								// 02-10-2015 MODIF. JIKA KELUAR LAIN2 DARI PEMBELIAN CASH ADA, MAKA DAHULUKAN PENGURANGANNYA KELUAR LAIN2, BARULAH YG KELUAR BAGUS
								$jum_keluar_lain += $jumkeluarlain_konv;
								$cekhitung= $hitungsaldo-$jum_keluar_lain;

								if ($cekhitung >=0 && $nomor < $query2x->num_rows()) {
									$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
									$jum_keluar_lain_harga = $jum_keluar_lain;
									$jum_keluar_lain = 0;
									//if ($row1x->kode_brg == 'RIN0007')
									//	echo $jum_keluar_lain_harga."<br>";

									// 08-09-2015, hitung apakah saldo setelah dikurangi keluar < 0 ?
									$cekhitung2 = $hitungsaldo-$jum_keluar;

									if ($cekhitung2 < 0 && $nomor < $query2x->num_rows()) {
										$jum_keluar_harga += $hitungsaldo;
										$hitungsaldo = 0;
										$jum_keluar = $jum_keluar-$jum_keluar_harga;
									}
									else if ($cekhitung2 < 0 && $nomor == $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar;
										$jum_keluar_harga += $jum_keluar;
										$jum_keluar = 0;
									}
									else if ($cekhitung2 >=0 && $nomor < $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar;
										$jum_keluar_harga += $jum_keluar;
										$jum_keluar = 0;
									}
								}
								else if ($cekhitung >=0 && $nomor == $query2x->num_rows()) {
									$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
									$jum_keluar_lain_harga = $jum_keluar_lain;
									$jum_keluar_lain = 0;

									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga += $jum_keluar;
									$jum_keluar = 0;
								}
								// -----------------------END 02-10-2015----------------------
								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "kadieu euy01 ".$jum_keluar_lain_harga."<br>";
							}
							//if ($row1x->kode_brg == 'MIC0045' && $nomor ==1)
							//	echo " hitungsaldo dari jum keluar lain = ".$hitungsaldo."<br>";
							// ===========================----------------------------=================================
							else {
								//if ($row1x->kode_brg == 'RIN0007')
								//	echo "ga ada keluar lain2 ".$jum_keluar_lain_harga."<br>";
								// 03-10-2015 diinisialisasikan lagi dari awal = 0 bisi ada bawaan variabel di proses sebelumnya
								$jum_keluar_lain_harga = 0;

								$cekhitung= $hitungsaldo-$jum_keluar;
								//if ($row1x->kode_brg == 'MIC0045')
								//	echo $hitungsaldo."<br>";

								if ($cekhitung < 0 && $nomor < $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'BNG0015')
									//	echo "didieu0 $jum_keluar_lain <br>";
									$jum_keluar_harga = $hitungsaldo;
									$hitungsaldo = 0;
									$jum_keluar = $jum_keluar-$jum_keluar_harga;
									$jum_keluar_lain_harga = 0;
									// jika minus, maka pengurangan keluar dan keluar lain di harga berikutnya
								}
								else if ($cekhitung < 0 && $nomor == $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'SLT0007')
									//	echo "didieu1 <br>";
									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga = $jum_keluar;
									$jum_keluar = 0;
									$jum_keluar_lain_harga += $jum_keluar_lain;
								}
								else if ($cekhitung >=0 && $nomor < $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'RIN0007')
									//	echo "didieu2 ".$jum_keluar_lain_harga."<br>";

									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga = $jum_keluar;
									$jum_keluar = 0;

									// 08-09-2015, hitung apakah saldo setelah dikurangi keluar lain < 0 ?
									$cekhitung2 = $hitungsaldo-$jum_keluar_lain;

									if ($cekhitung2 < 0 && $nomor < $query2x->num_rows()) {
										//$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										//$jum_keluar_lain_harga = $jum_keluar_lain;
										$jum_keluar_lain_harga += $hitungsaldo;
										$hitungsaldo = 0;
										$jum_keluar_lain = $jum_keluar_lain-$jum_keluar_lain_harga;

										//if ($row1x->kode_brg == 'SLT0007')
										//	echo "didieu21 ".$jum_keluar_lain_harga."<br>";
									}
									else if ($cekhitung2 < 0 && $nomor == $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										$jum_keluar_lain_harga += $jum_keluar_lain;
										$jum_keluar_lain = 0;
										//if ($row1x->kode_brg == 'SLT0007')
										//	echo "didieu22 ".$jum_keluar_lain_harga."<br>";
									}
									else if ($cekhitung2 >=0 && $nomor < $query2x->num_rows()) {
										$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										$jum_keluar_lain_harga += $jum_keluar_lain;
										$jum_keluar_lain = 0;
										//if ($row1x->kode_brg == 'SLT0007')
										//	echo "didieu23 ".$jum_keluar_lain_harga."<br>";
									}
								}
								else if ($cekhitung >=0 && $nomor == $query2x->num_rows()) {
									//if ($row1x->kode_brg == 'RIN0007')
									//	echo "didieu3 ".$jum_keluar_lain_harga."<br>";
									//if ($row1x->kode_brg == 'BNG0015')
									//	echo "didieux $jum_keluar_lain <br>";
									$hitungsaldo = $hitungsaldo-$jum_keluar;
									$jum_keluar_harga = $jum_keluar;
									$jum_keluar = 0;

									// 02-10-2015
									//if ($jum_keluar_lain != 0) {
										$hitungsaldo = $hitungsaldo-$jum_keluar_lain;
										$jum_keluar_lain_harga += $jum_keluar_lain;
										// 30-09-2015
										$jum_keluar_lain = 0;
									//}
									/*else {
										$jum_keluar_lain_harga = 0;
										$jum_keluar_lain = 0;
									} */
								}
							} // end cek ada keluarlain dari pembelian cash
						} // end if

						$jum_keluar_rp = $jum_keluar_harga*$hargapkp;
						$jum_keluar_lain_rp = $jum_keluar_lain_harga*$hargapkp;

						//if ($row1x->kode_brg == 'RIN0007')
						//	echo "disini ".$jum_keluar_lain_harga."<br>";

						//==================================== STOK OPNAME ==========================================
						// 25-08-2015
						$saldo_akhir = $saldo_awal+$jum_masuk_konv+$jum_masuk_lain_harga-$jum_keluar_harga-$jum_keluar_lain_harga;

						//if ($row1x->kode_brg == 'BNG0015')
							//echo $saldo_awal." ".$jum_masuk_konv." ".$jum_masuk_lain_harga." ".$jum_keluar_harga." ".$jum_keluar_lain_harga." ".$hitungsaldo."<br>";

						$saldo_akhir_rp = ($saldo_akhir*$hargapkp)-$jum_diskon;

						if ($gudang == '6') {
							$jum_so_harga = 0;
						}
						else {
							if ($jum_so_total >= 0 && ($nomor == $query2x->num_rows()))
								$jum_so_harga = $jum_so_total;
							else {
								if ($jum_so_total < $saldo_akhir) {
									$jum_so_harga = $jum_so_total;
									$jum_so_total = 0;
								}
								else {
									$jum_so_harga = $saldo_akhir;
									$jum_so_total= $jum_so_total-$saldo_akhir;
								}
							}
						}

						// insert ke tabel SO harga.
						// 07-10-2015 dikomen dulu utk ngetes2
						if ($date_from_exp >= '2015-08-01') {
							if ($id_so_detail != 0 && $kalkulasi_ulang_so == 't') {
								$data_so_harga = array(
								  'id_stok_opname_bahan_baku_detail'=>$id_so_detail,
								  'harga'=>$row2x->harga,
								  'jum_stok_opname'=>$jum_so_harga,
								  'is_harga_pkp'=>$is_harga_pkp,
								  'id_satuan'=>$id_satuan
								  );
								$this->db->insert('tt_stok_opname_bahan_baku_detail_harga',$data_so_harga);
							}
						}

						$jum_so_harga_rp = $jum_so_harga*$hargapkp;

						$selisih = $jum_so_harga-$saldo_akhir;
						$selisih = round($selisih,4);
						$selisih_rp = $selisih*$hargapkp;
						//--------------------------------------
						//if ($row1x->kode_brg == 'SLT0003')
						//	echo "kode brg $row1x->kode_brg ".$jum_keluar_harga."<br>";
						$data_perharga[] = array(
													'harga'=> $hargapkp,
													'nama_pkp'=> $nama_pkp,
													'saldo_awal'=> $saldo_awal,
													'saldo_awal_rp'=> $saldo_awal_rp,
													'jum_masuk'=> $jum_masuk_konv,
													'jum_masuk_rp'=> $jum_masuk_konv_rp,
													'jum_masuk_lain'=> $jum_masuk_lain_harga,
													'jum_masuk_lain_rp'=> $jum_masuk_lain_rp,
													'jum_keluar'=> $jum_keluar_harga,
													'jum_keluar_rp'=> $jum_keluar_rp,
													'jum_keluar_lain'=> $jum_keluar_lain_harga,
													'jum_keluar_lain_rp'=> $jum_keluar_lain_rp,
													'saldo_akhir'=> $saldo_akhir,
													'saldo_akhir_rp'=> $saldo_akhir_rp,
													'jum_stok_opname'=> $jum_so_harga,
													'jum_stok_opname_rp'=> $jum_so_harga_rp,
													'selisih'=> $selisih,
													'selisih_rp'=> $selisih_rp,
													'id_satuan'=> $id_satuan,
													'satuan'=> $satuan,
													'nama_satuan_konv'=> $nama_satuan_konv,
													);
						$nomor++;
					} // endforeach2 perharga
				} // end if2
				// 08-10-2015
				else {
					$data_perharga[] = array(
													'harga'=> 0,
													'nama_pkp'=> '',
													'saldo_awal'=> 0,
													'saldo_awal_rp'=> 0,
													'jum_masuk'=> 0,
													'jum_masuk_rp'=> 0,
													'jum_masuk_lain'=> 0,
													'jum_masuk_lain_rp'=> 0,
													'jum_keluar'=> 0,
													'jum_keluar_rp'=> 0,
													'jum_keluar_lain'=> 0,
													'jum_keluar_lain_rp'=> 0,
													'saldo_akhir'=> 0,
													'saldo_akhir_rp'=> 0,
													'jum_stok_opname'=> $jum_so_total,
													'jum_stok_opname_rp'=> 0,
													'selisih'=> 0,
													'selisih_rp'=> 0,
													'id_satuan'=> 0,
													'satuan'=> '',
													'nama_satuan_konv'=> '',
													);
				}

				$data_header[] = array(		'kode_brg'=> $row1x->kode_brg,
											'kode_gudang'=> $row1x->kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_brg'=> $row1x->nama_brg,
											/*'id_satuan'=> $id_satuan,
											'satuan'=> $satuan,
											'nama_satuan_konv'=> $nama_satuan_konv, */
											'data_perharga'=> $data_perharga
											);
				$data_perharga = array();
			} // end foreach1
		} // end if1


		// ===========================================BATAS AKHIR 24-08-2015 dan 25======================================================
		else {
			$data_header = '';
		}
		return $data_header;
  }


  // 12-05-2015
  function get_mutasi_stok_gudang($date_from, $date_to, $gudang,$kel_brg,$jns_brg) {
		$filter = "";
		if ($gudang != '0') {
			$filter = " AND a.id_gudang = '$gudang' ";
		}

		if ($kel_brg != '0') {
			$filter .= " AND e.kode = '$kel_brg' ";
		}

		if ($jns_brg != '0') {
			$filter .= " AND d.id = '$jns_brg' ";
		}

		// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$iperiode=$thn1.$bln1;

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}

			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			$dtoback = date('Y-m-d',strtotime($date_from . "last day of previous month"));		

			$query = $this->db->query("	
										SELECT
										   a.id_brg,
										   a.kode_brg,
										   a.nama_brg,
										   a.id_gudang,
										   a.kode_gudang,
										   a.nama_gudang,
										   a.id_satuan,
										   a.nama_satuan,
										   a.id_satuan_konversi,
										   a.nama_satuan_konv,
										   a.saldo_awal,
										   a.jum_masuk,
										   a.jum_masuk_lain,
										   a.jum_keluar,
										   a.jum_keluar_lain,
										   a.stok_opname,
										   b.rumus_konversi,
										   b.angka_faktor_konversi 
										FROM
										   fmutasi_new('$iperiode', '$bln_query', '$thn_query', '$dtoback', '$bln1', '$thn1', '$date_from', '$date_to') a
										INNER JOIN 
											tm_barang b ON a.id_brg = b.id
										INNER JOIN 
											tm_gudang c ON b.id_gudang = c.id
										INNER JOIN 
											tm_jenis_barang d ON b.id_jenis_barang = d.id
										INNER JOIN 
											tm_kelompok_barang e ON d.kode_kel_brg= e.kode
										WHERE 
											b.status_aktif ='t' ".$filter." 
										ORDER BY 
											a.id_gudang ASC
									");

		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$id_brg 		= $row1->id_brg;
				$kode_brg 		= $row1->kode_brg;
				$nama_brg 		= $row1->nama_brg;
				$id_gudang 		= $row1->id_gudang;
				$kode_gudang 	= $row1->kode_gudang;
				$nama_gudang 	= $row1->nama_gudang;
				$id_satuan 		= $row1->id_satuan;
				$satuan 		= $row1->nama_satuan;
				$id_satuan_konversi = $row1->id_satuan_konversi;
				$nama_satuan_konv 	= $row1->nama_satuan_konv;
				$rumus_konversi = $row1->rumus_konversi;
				$angka_faktor_konversi = $row1->angka_faktor_konversi;

				$saldo_awal 	= $row1->saldo_awal;
				$jum_masuk 		= $row1->jum_masuk;

				if ($id_satuan_konversi != 0) {
					if ($rumus_konversi == '1') {
						$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '2') {
						$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '3') {
						$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '4') {
						$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
					}
					else {
						$jum_masuk_konv = $jum_masuk;
					}
				}else {
					$jum_masuk_konv = $jum_masuk;
				}

				$jum_masuk_lain = $row1->jum_masuk_lain;
				
				if($id_gudang != '6'){
					$jum_keluar = $row1->jum_keluar;
				}else{
					$jum_keluar = $jum_masuk_konv;
				}

				$jum_keluar_lain 	= $row1->jum_keluar_lain;
				$stok_opname 		= $row1->stok_opname;

				$data_stok[] = array(		
										'id_brg'			=> $id_brg,
										'kode_brg'			=> $kode_brg,
										'id_gudang'			=> $id_gudang,
										'kode_gudang'		=> $kode_gudang,
										'nama_gudang'		=> $nama_gudang,
										'nama_brg'			=> $nama_brg,
										'id_satuan'			=> $id_satuan,
										'satuan'			=> $satuan,
										'nama_satuan_konv'	=> $nama_satuan_konv,
										'saldo_awal'		=> $saldo_awal,
										'jum_masuk'			=> $jum_masuk,
										'jum_masuk_lain'	=> $jum_masuk_lain,
										'jum_keluar'		=> $jum_keluar,
										'jum_keluar_lain'	=> $jum_keluar_lain,
										'jum_stok_opname'	=> $stok_opname,
									);
			} // endforeach header
		}else {
			$data_stok = '';
		}
		return $data_stok;
  }
  // =====================================================================

  function get_gudang(){
if($this->session->userdata('gid') == 16){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
				                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
    }else{
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
				                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
				                        }
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

  // 12-11-2015
  function get_gudang_qc(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi
						FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id
						WHERE a.kel_gudang = '2' ORDER BY a.kode_lokasi, a.kode_gudang");

    if ($query->num_rows() > 0){
		return $query->result();
	}
  }

  // 14-03-2013, mutasi stok WIP. update 17-09-2014, ada pengelompokan berdasarkan kel brg jadi
  // PEMBAHARUAN 12-11-2015
  function get_mutasi_stok_wip_dg_w_old($date_from, $date_to, $gudang) {

	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;

	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;

	// explode date_from utk keperluan saldo awal
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];

	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}

	$sql = "SELECT a.id_brg_wip, a.id_kel_brg_wip FROM (
				select b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukwip a
				INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarwip a
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukother_gudangqc a
				INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarother_gudangqc a
				INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

				)
				a GROUP BY a.id_brg_wip, a.id_kel_brg_wip ORDER BY a.id_kel_brg_wip, a.id_brg_wip"; //echo $sql; die();
		//AND a.status_approve='t'

		$query	= $this->db->query($sql);

		$jum_keluar_pack = 0;
		$jum_keluar_gdjadi = 0;
		$jum_keluar_perb = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$jum_perb_unit = 0;
		$jum_ret_unit_pack = 0;
		$jum_ret_gd_jadi = 0;
		$jum_masuk_lain = 0;
		$jum_masuk_other = 0;
		$data_stok = array();
		$saldo_so_warna= array();
		$totselisih_warna= array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();

			// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}

			/*
			$tglfromback= $tgl1."-".$bln_query."-".$thn_query;


			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			$tgltoback= $tgl1."-".$bln_query."-".$thn_query;
			*/

			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}

				// 13-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
				// 04-03-2015, saldo awal bukan dari jum_stok_opname, tapi ambil dari auto_saldo_akhir
				// 21-04-2015, skrg saldo awal ambil dari SO LAGI.
				// 12-11-2015, khusus gudang QC, ambil dari SO

					$queryx	= $this->db->query(" SELECT b.tot_jum_stok_opname,b.id FROM tt_stok_opname_hasil_jahit a
									INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query'  ");
						// AND b.status_approve = 't' AND a.status_approve = 't'

				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->tot_jum_stok_opname;
					$id_so_detail = $hasilrow->id;
					$sqlx23 = " SELECT a.jum_stok_opname,a.tot_jum_stok_opname, b.nama,b.id as idwarna,a.id as id_so_warna FROM tt_stok_opname_hasil_jahit_detail_warna a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE id_stok_opname_hasil_jahit_detail = '$id_so_detail' ";
							$queryx23	= $this->db->query($sqlx23);
							if ($queryx23->num_rows() > 0){
								$hasilx23 = $queryx23->result();

								foreach ($hasilx23 as $rowx23) {
									//$saldo_awal2 = $rowx2->saldo_akhir;
									$saldo_sa2 = $rowx23->jum_stok_opname;
									$saldo_sa_tot2 = $rowx23->tot_jum_stok_opname;
									$nama_warna_sa2 = $rowx23->nama;
									$idwarna=$rowx23->idwarna;
									$idsawarna=$rowx23->id_so_warna;
									$saldo_sa_warna[] = array(		'nama_warna'=> $nama_warna_sa2,
																	'saldo_awal'=> $saldo_sa_tot2
																	);


		/////===============================================================================================================/////////

						// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
									AND a.id_gudang = '$gudang'  AND d.id='$idwarna'
									 AND a.jenis_masuk = '1' GROUP BY d.nama");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna = $hasilrow->jum_masuk;
							$nama_warna = $hasilrow->nama;

					}
						else{
							$jum_masuk_bagus_warna = 0;
							$nama_warna = $nama_warna_sa2;
						}
						$data_warna_sjmb[]=array(
							'jum_masuk_bagus_warna'=>$jum_masuk_bagus_warna ,
							'nama_warna'=>$nama_warna );

						// 2.1.2 dari tabel tm_sjmasukwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
									AND a.id_gudang = '$gudang'  AND d.id='$idwarna'
									 AND a.jenis_masuk = '2' GROUP BY d.nama");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();

							$jum_masuk_perbaikan_warna = $hasilrow->jum_masuk;
							$nama_warna2 = $hasilrow->nama;
						}

						else{
							$jum_masuk_perbaikan_warna = 0;
							$nama_warna2 = $nama_warna_sa2;
						}

							$data_warna_sjmp[]=array(
							'jum_masuk_perbaikan_warna'=>$jum_masuk_perbaikan_warna ,
							'nama_warna'=>$nama_warna2

							);
			// 2.1.3 dari tabel tm_sjmasukwip. jenis retpack
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
									AND a.id_gudang = '$gudang'  AND d.id='$idwarna'
									 AND a.jenis_masuk = '3' GROUP BY d.nama");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_retpack_warna = $hasilrow->jum_masuk;
							$nama_warna = $hasilrow->nama;

					}
						else{
							$jum_masuk_retpack_warna = 0;
							$nama_warna = $nama_warna_sa2;;
						}

						$data_warna_sjmr[]=array(
							'jum_masuk_retpack_warna'=>$jum_masuk_retpack_warna ,
							'nama_warna'=>$nama_warna
							);

						// 2.1.4 dari tabel tm_sjmasukwip. jenis retgdjd
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
									AND a.id_gudang = '$gudang'  AND d.id='$idwarna'
									 AND a.jenis_masuk = '4' GROUP BY d.nama");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_retgdjd_warna = $hasilrow->jum_masuk;
							$nama_warna = $hasilrow->nama;
					}
						else{
							$jum_masuk_retgdjd_warna = 0;
							$nama_warna = $nama_warna_sa2;
						}

						$data_warna_sjmrg[]=array(
							'jum_masuk_retgdjd_warna'=>$jum_masuk_retgdjd_warna ,
							'nama_warna'=>$nama_warna

							);
						// 2.1.5 dari tabel tm_sjmasukwip. jenis retdll
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
									AND a.id_gudang = '$gudang'  AND d.id='$idwarna'
									 AND a.jenis_masuk = '5' GROUP BY d.nama");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_retdll_warna = $hasilrow->jum_masuk;
							$nama_warna = $hasilrow->nama;
						}

						else{
							$jum_masuk_retdll_warna = 0;
							$nama_warna = $nama_warna_sa2;
						}
						$data_warna_sjmll[]=array(
							'jum_masuk_retdll_warna'=>$jum_masuk_retdll_warna ,
							'nama_warna'=>$nama_warna

							);
				$jum_masuk_warna_total = $jum_masuk_bagus_warna+$jum_masuk_perbaikan_warna+$jum_masuk_retpack_warna+$jum_masuk_retgdjd_warna+$jum_masuk_retdll_warna;
					$totjum_masuk_warna_total[]=array(
							'jum_masuk_warna_total'=>$jum_masuk_warna_total ,
							'nama_warna'=>$nama_warna

							);

		//000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000//
								// 2.2.1 dari tabel tm_sjkeluar packing
			$query31	= $this->db->query(" SELECT sum(c.qty) as jum_keluar ,d.nama FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							INNER JOIN tm_warna d ON c.id_warna = d.id
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND d.id='$idwarna'
							AND a.jenis_keluar = '1'  GROUP BY d.nama");

						if ($query31->num_rows() > 0){
							$hasilrow31 = $query31->row();
							$jum_keluar_bagus_warna = $hasilrow31->jum_keluar;
							$nama_warna = $hasilrow31->nama;

					}
						else{
							$jum_keluar_bagus_warna = 0;
							$nama_warna = $nama_warna_sa2;
						}
						$data_warna_sjkp[]=array(
							'jum_keluar_bagus_warna'=>$jum_keluar_bagus_warna ,
							'nama_warna'=>$nama_warna );


			// 2.2.1 dari tabel tm_sjkeluar gdjadi
			$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar ,d.nama FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							INNER JOIN tm_warna d ON c.id_warna = d.id
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND d.id='$idwarna'
							AND a.jenis_keluar = '2'  GROUP BY d.nama");

						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_gdjadi_warna = $hasilrow->jum_keluar;
							$nama_warna = $hasilrow->nama;

					}
						else{
							$jum_keluar_gdjadi_warna = 0;
							$nama_warna = $nama_warna_sa2;
						}
						$data_warna_sjkgj[]=array(
							'jum_keluar_gdjadi_warna'=>$jum_keluar_gdjadi_warna ,
							'nama_warna'=>$nama_warna );

				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar ,d.nama FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							INNER JOIN tm_warna d ON c.id_warna = d.id
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND d.id='$idwarna'
							AND a.jenis_keluar = '3'  GROUP BY d.nama");

						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_retper_warna = $hasilrow->jum_keluar;
							$nama_warna = $hasilrow->nama;

					}
						else{
							$jum_keluar_retper_warna = 0;
							$nama_warna = $nama_warna_sa2;
						}
						$data_warna_sjkr[]=array(
							'jum_keluar_retper_warna'=>$jum_keluar_retper_warna ,
							'nama_warna'=>$nama_warna );


							$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar ,d.nama FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							INNER JOIN tm_warna d ON c.id_warna = d.id
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND d.id='$idwarna'
							AND a.jenis_keluar = '4'  GROUP BY d.nama");

						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_dll_warna = $hasilrow->jum_keluar;
							$nama_warna = $hasilrow->nama;

					}
						else{
							$jum_keluar_dll_warna = 0;
							$nama_warna = $nama_warna_sa2;
						}
						$data_warna_sjkll[]=array(
							'jum_keluar_dll_warna'=>$jum_keluar_dll_warna ,
							'nama_warna'=>$nama_warna );

						$jum_keluar_warna_total = $jum_keluar_bagus_warna+$jum_keluar_gdjadi_warna+$jum_keluar_retper_warna+$jum_keluar_dll_warna;
					$totjum_keluar_warna_total[]=array(
							'jum_keluar_warna_total'=>$jum_keluar_warna_total ,
							'nama_warna'=>$nama_warna

							);

							$jum_warna_total = $saldo_sa2+$jum_masuk_warna_total-$jum_keluar_warna_total;
					$totjum_warna_total[]=array(
							'jum_warna_total'=>$jum_warna_total ,
							'nama_warna'=>$nama_warna

							);

								}
							}


					//$auto_saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else {
					$saldo_awal = 0;
					$saldo_sa_warna = '';
					//$auto_saldo_awal = 0;
				}


				//111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111//

				$queryxyzb	= $this->db->query(" SELECT b.jum_adjustment,b.id FROM tt_adjustment_hasil_jahit a
									INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id = b.id_adjustment_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1'  ");
						// AND b.status_approve = 't' AND a.status_approve = 't'

				if ($queryxyzb->num_rows() > 0){
					$hasilrowxyzb = $queryxyzb->row();
					$saldo_awal_jum_adjustmentb = $hasilrowxyzb->jum_adjustment;
					$id_jum_detail = $hasilrow->id;
					$sqlx23a = " SELECT a.jum_adjustment, b.nama FROM tt_adjustment_hasil_jahit_detail_warna a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE id_adjustment_hasil_jahit_detail = '$id_jum_detail' ";
							$queryx23a	= $this->db->query($sqlx23a);
							if ($queryx23a->num_rows() > 0){
								$hasilx23a = $queryx23a->result();

								foreach ($hasilx23a as $rowx23a) {
									//$saldo_awal2 = $rowx2->saldo_akhir;
									$saldo_jm2 = $rowx23a->jum_adjustment;
									$nama_warna_jm2 = $rowx23a->nama;
									$saldo_jum_awal_warna[] = array(		'nama_warna'=> $nama_warna_jm2,
																	'saldo_jum'=> $saldo_jm2
																	);
								}
							}

				}

				else {
					$saldo_awal_jum_adjustmentb = 0;
					$saldo_jum_warna='';
				}

				$queryxyz	= $this->db->query(" SELECT b.jum_adjustment,b.id FROM tt_adjustment_hasil_jahit a
									INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id = b.id_adjustment_hasil_jahit
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query'  ");


				if ($queryxyz->num_rows() > 0){
					$hasilrowxyz = $queryxyz->row();
					$jum_adjustment = $hasilrowxyz->jum_adjustment;
					$id_jum_detail = $hasilrow->id;
					$sqlx23a = " SELECT a.jum_adjustment, b.nama FROM tt_adjustment_hasil_jahit_detail_warna a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE id_adjustment_hasil_jahit_detail = '$id_jum_detail' ";
							$queryx23a	= $this->db->query($sqlx23a);
							if ($queryx23a->num_rows() > 0){
								$hasilx23a = $queryx23a->result();

								foreach ($hasilx23a as $rowx23a) {
									//$saldo_awal2 = $rowx2->saldo_akhir;
									$saldo_jm2 = $rowx23a->jum_adjustment;
									$nama_warna_jm2 = $rowx23a->nama;
									$saldo_jum_warna[] = array(		'nama_warna'=> $nama_warna_jm2,
																	'saldo_jum'=> $saldo_jm2
																	);
								}
							}

				}
				else {
					$jum_adjustment = 0;
					$saldo_jum_warna='';

				}
				$saldo_awal+=$saldo_awal_jum_adjustmentb;
				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip

				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;

				}
				else
					$jum_masuk = 0;

				//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_perb_unit = $hasilrow->jum_masuk;
				}
				else
					$jum_perb_unit = 0;

				//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_ret_unit_pack = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_unit_pack = 0;

				//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_ret_gd_jadi = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_gd_jadi = 0;

				//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '5' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain = 0;

				//6. hitung brg masuk OTHER tm_masukother_gudangqc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang'  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_other = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_other = 0;

					$jum_masuk_total = $jum_masuk+$jum_perb_unit+$jum_ret_unit_pack+$jum_ret_gd_jadi+$jum_masuk_lain+$jum_masuk_other;
					$jum_masuk_lain+=$jum_masuk_other;
			//--------------------------------------------------------------------------------------------------
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_pack = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_pack = 0;

				// hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_gdjadi = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_gdjadi = 0;

				// hitung brg keluar lain jenis=3 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perb = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perb = 0;

				// hitung brg keluar lain jenis=4 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;

					//. hitung brg keluar OTHER tm_sjkeluarother_gudangqc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang'  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_other = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_other = 0;

				$jum_keluar_total = $jum_keluar_pack+$jum_keluar_gdjadi+$jum_keluar_perb+$jum_keluar_lain+$jum_keluar_other;
				// 18-01-2016, saldo akhir dihitung dari selisih masuk dan keluar
				$jum_saldo_akhir = $saldo_awal + $jum_masuk_total -$jum_keluar_total;



				// 13-09-2013 ================================
				$sql = "SELECT a.tgl_so,a.jenis_perhitungan_stok, b.id, b.jum_stok_opname FROM tt_stok_opname_hasil_jahit a
							INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
							WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1'  ";
				// AND b.status_approve = 't'

				$query3	= $this->db->query($sql);

				//$jum_so='';
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_so_detail2 = $hasilrow->id;
					$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
					$tgl_so = $hasilrow->tgl_so;

					$sqlx233 = " SELECT a.jum_stok_opname,a.tot_jum_stok_opname, b.nama,b.id as idwarna,a.id as idsowarna  FROM tt_stok_opname_hasil_jahit_detail_warna a INNER JOIN tm_warna b
										ON a.id_warna = b.id
										WHERE id_stok_opname_hasil_jahit_detail = '$id_so_detail2' ";
							$queryx233	= $this->db->query($sqlx233);
							if ($queryx233->num_rows() > 0){
								$hasilx233 = $queryx233->result();
								//~ $saldo_so2=0;
								foreach ($hasilx233 as $rowx233) {
									$selisih_warna=0;
									//$saldo_awal2 = $rowx2->saldo_akhir;
									$saldo_so2 = $rowx233->jum_stok_opname;
									$saldo_so_tot2 = $rowx233->tot_jum_stok_opname;
									$nama_warna_so2 = $rowx233->nama;
									$idwarna=$rowx233->idwarna;
									$idsowarna=$rowx233->idsowarna;

							//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
						$sql88 = " SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '1' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_masukxw = $hasilrow88->jum_masuk;

							if ($jum_masukxw == '')
								$jum_masukxw = 0;
						}
						else
							$jum_masukxw = 0;

							//2. hitung brg masuk bagus (jenis=2) dari tm_sjmasukwip
						$sql88 = " SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '2' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_perb_unitxw = $hasilrow88->jum_masuk;

							if ($jum_perb_unitxw == '')
								$jum_perb_unitxw = 0;
						}
						else
							$jum_perb_unitxw = 0;


							//3. hitung brg masuk bagus (jenis=3) dari tm_sjmasukwip
						$sql88 = " SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '3' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_ret_unit_packxw = $hasilrow88->jum_masuk;

							if ($jum_ret_unit_packxw == '')
								$jum_ret_unit_packxw = 0;
						}
						else
							$jum_ret_unit_packxw = 0;


							//4. hitung brg masuk bagus (jenis=4) dari tm_sjmasukwip
						$sql88 = " SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '4' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_ret_gd_jadixw = $hasilrow88->jum_masuk;

							if ($jum_ret_gd_jadixw == '')
								$jum_ret_gd_jadixw = 0;
						}
						else
							$jum_ret_gd_jadixw = 0;


							//5. hitung brg masuk bagus (jenis=5) dari tm_sjmasukwip
						$sql88 = " SELECT sum(c.qty) as jum_masuk ,d.nama FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '5' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_masuk_lainxw = $hasilrow88->jum_masuk;

							if ($jum_masuk_lainxw == '')
								$jum_masuk_lainxw = 0;
						}
						else
							$jum_masuk_lainxw = 0;

							//6. hitung brg masuk lain  dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk,d.nama  FROM tm_sjmasukother_gudangqc a
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
							INNER JOIN tm_sjmasukother_gudangqc_detail_warna c ON b.id = c.id_sjmasukother_gudangqc_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND d.id='$idwarna'  GROUP BY d.nama ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_otherxw = $hasilrow77->jum_masuk;

							if ($jum_masuk_otherxw == '')
								$jum_masuk_otherxw = 0;
						}
						else
							$jum_masuk_otherxw = 0;

					$jum_masuk_totalxw = $jum_masukxw+$jum_perb_unitxw+$jum_ret_unit_packxw+$jum_ret_gd_jadixw+$jum_masuk_lainxw+$jum_masuk_otherxw;

						$jum_masuk_totalxwarna[]=array(
							'jum_masuk_totalxw'=>$jum_masuk_totalxw ,
							'nama_warna'=>$nama_warna_so2

							);
			//444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444
							$sql88 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '1' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_keluar_packxw = $hasilrow88->jum_keluar;

							if ($jum_keluar_packxw == '')
								$jum_keluar_packxw = 0;
						}
						else
							$jum_keluar_packxw = 0;

						$sql88 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '2' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_keluar_gdjadixw = $hasilrow88->jum_keluar;

							if ($jum_keluar_gdjadixw == '')
								$jum_keluar_gdjadixw = 0;
						}
						else
							$jum_keluar_gdjadixw = 0;


						$sql88 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '3' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_keluar_perbxw = $hasilrow88->jum_keluar;

							if ($jum_keluar_perbxw == '')
								$jum_keluar_perbxw = 0;
						}
						else
							$jum_keluar_perbxw = 0;

							$sql88 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '4' AND d.id='$idwarna'  GROUP BY d.nama";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_keluar_lainxw = $hasilrow88->jum_keluar;

							if ($jum_keluar_lainxw == '')
								$jum_keluar_lainxw = 0;
						}
						else
							$jum_keluar_lainxw = 0;


							// 5. hitung brg keluar  dari tm_sjkeluarother_gudangqc
						$sql88 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
							INNER JOIN tm_sjkeluarother_gudangqc_detail_warna c ON b.id = c.id_sjkeluarother_gudangqc_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql88.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql88.=" a.tgl_sj >= '$tgl_so' ";
						$sql88.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND d.id='$idwarna'  GROUP BY d.nama ";
						$query88	= $this->db->query($sql88);
						if ($query88->num_rows() > 0){
							$hasilrow88 = $query88->row();
							$jum_keluar_otherxw = $hasilrow88->jum_keluar;

							if ($jum_keluar_otherxw == '')
								$jum_keluar_otherxw= 0;
						}
						else
							$jum_keluar_otherxw = 0;

						$jum_keluar_totalxw = $jum_keluar_packxw+$jum_keluar_gdjadixw+$jum_keluar_perbxw+$jum_keluar_lainxw+$jum_keluar_otherxw;

						$tambahkurangw = $jum_masuk_totalxw - $jum_keluar_totalxw;
						$saldo_so2=$saldo_so2+ $tambahkurangw;
						$selisih_warna=$saldo_so2-$jum_warna_total;

						$totselisih_warna[]=array(
							'selisih_warna'=>$selisih_warna ,
							'nama_warna'=>$nama_warna_so2

							);
						$saldo_so_warna[] = array(		'nama_warna'=> $nama_warna_so2,
																	'saldo_so'=> $saldo_so2,
																	'saldo_so_tot'=> $saldo_so_tot2
																	);
							//~ $sqlupdatew="Update tt_stok_opname_hasil_jahit_detail_warna set tot_jum_stok_opname='$saldo_so2'
					//~ WHERE id='$id_so_detail'";
					//~ $this->db->query($sqlupdate);

						$sqlupdatew="Update tt_stok_opname_hasil_jahit_detail_warna set tot_jum_stok_opname='$saldo_so2'
					WHERE id='$idsowarna'";
					$this->db->query($sqlupdatew);



					//~ $sqlupdatetmw = " UPDATE tm_stok_hasil_jahit_warna SET stok = '$saldo_so2'
									//~ WHERE id_brg_wip = '$row1->id_brg_wip' AND id_warna='$id_warna'
									//~ AND id_gudang='$gudang' ";
					//~ $this->db->query($sqlupdatetmw);
								}
							}
		//~ // 15-12-2015, update saldo akhir di tabel stok opname nya
					$sqlupdate = " UPDATE tt_stok_opname_hasil_jahit_detail SET saldo_akhir = '$jum_saldo_akhir'
									WHERE id = '$id_so_detail2' ";
					$this->db->query($sqlupdate);
			//33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
						//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masukx = $hasilrow77->jum_masuk;

							if ($jum_masukx == '')
								$jum_masukx = 0;
						}
						else
							$jum_masukx = 0;



						//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_perb_unitx = $hasilrow77->jum_masuk;

							if ($jum_perb_unitx == '')
								$jum_perb_unitx = 0;
						}
						else
							$jum_perb_unitx = 0;



						//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_unit_packx = $hasilrow77->jum_masuk;

							if ($jum_ret_unit_packx == '')
								$jum_ret_unit_packx = 0;
						}
						else
							$jum_ret_unit_packx = 0;

							//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_gd_jadix = $hasilrow77->jum_masuk;

							if ($jum_ret_gd_jadix == '')
								$jum_ret_gd_jadix = 0;
						}
						else
							$jum_ret_gd_jadix = 0;


								//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_lainx = $hasilrow77->jum_masuk;

							if ($jum_masuk_lainx == '')
								$jum_masuk_lainx = 0;
						}
						else
							$jum_masuk_lainx = 0;


								//6. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_otherx = $hasilrow77->jum_masuk;

							if ($jum_masuk_otherx == '')
								$jum_masuk_otherx = 0;
						}
						else
							$jum_masuk_otherx = 0;

					$jum_masuk_totalx = $jum_masukx+$jum_perb_unitx+$jum_ret_unit_packx+
					$jum_ret_gd_jadix+$jum_masuk_lainx+$jum_masuk_otherx;

							// 1. hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_packx = $hasilrow77->jum_keluar;

							if ($jum_keluar_packx == '')
								$jum_keluar_packx = 0;
						}
						else
							$jum_keluar_packx = 0;

							// 2. hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_gdjadix = $hasilrow77->jum_keluar;

							if ($jum_keluar_gdjadix == '')
								$jum_keluar_gdjadix = 0;
						}
						else
							$jum_keluar_gdjadix = 0;

							// 3. hitung brg keluar bagus jenis=3 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_perbx = $hasilrow77->jum_keluar;

							if ($jum_keluar_perbx == '')
								$jum_keluar_perbx = 0;
						}
						else
							$jum_keluar_perbx = 0;



							// 4. hitung brg keluar bagus jenis=4 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_lainx = $hasilrow77->jum_keluar;

							if ($jum_keluar_lainx == '')
								$jum_keluar_lainx = 0;
						}
						else
							$jum_keluar_lainx = 0;


							// 5. hitung brg keluar  dari tm_sjkeluarother_gudangqc
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_otherx = $hasilrow77->jum_keluar;

							if ($jum_keluar_otherx == '')
								$jum_keluar_otherx = 0;
						}
						else
							$jum_keluar_otherx = 0;

								$jum_keluar_totalx = $jum_keluar_packx+$jum_keluar_gdjadix+
								$jum_keluar_perbx+$jum_keluar_lainx+$jum_keluar_otherx;

									$tambahkurang = $jum_masuk_totalx - $jum_keluar_totalx;

						$jum_stok_opname = $jum_stok_opname+ $tambahkurang;

					$sqlupdate="Update tt_stok_opname_hasil_jahit_detail set tot_jum_stok_opname='$jum_stok_opname'
					WHERE id='$id_so_detail2'";
					$this->db->query($sqlupdate);

					$sqlupdatetm = " UPDATE tm_stok_hasil_jahit SET stok = '$jum_stok_opname'
									WHERE id_brg_wip = '$row1->id_brg_wip'
									AND id_gudang='$gudang' ";
					$this->db->query($sqlupdatetm);


				}
				else {
					$id_so_detail = 0;
					$jum_stok_opname = 0;
				}
				//=========================================


				//17-09-2014, ambil nama kel brg jadi
				if ($row1->id_kel_brg_wip != '') {
					$sqlxx = " SELECT kode, nama FROM tm_kel_brg_wip WHERE id='$row1->id_kel_brg_wip' ";
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$kode_kel = $hasilxx->kode;
						$nama_kel = $hasilxx->nama;
					}
					else {
						$kode_kel = '';
						$nama_kel = '';
					}
				}
				else {
					$kode_kel = '';
					$nama_kel = '';
				}

				$data_stok[] = array(		'kode_brg'=> $kode_brg_wip,
											'kode_kel'=> $kode_kel,
											'nama_kel'=> $nama_kel,
											'nama_brg'=> $nama_brg_wip,
											// per 08-04-2015 pake auto saldo akhir
											// 21-04-2015, waktu hari sabtu kata ginanjar pake SO lagi
											// 12-11-2015 duta utk gudang QC acuannya dari SO
											//'saldo_awal'=> $auto_saldo_awal,
											'saldo_awal'=> $saldo_awal,
											'jum_keluar_pack'=>$jum_keluar_pack ,
											'jum_keluar_gdjadi'=>$jum_keluar_gdjadi,
											'jum_keluar_perb'=>$jum_keluar_perb,
											'jum_keluar_lain'=>$jum_keluar_lain,
											'jum_keluar_total'=>$jum_keluar_total,

											'jum_masuk'=>$jum_masuk ,
											'jum_perb_unit'=>$jum_perb_unit,
											'jum_ret_unit_pack'=>$jum_ret_unit_pack,
											'jum_ret_gd_jadi'=>$jum_ret_gd_jadi ,
											'jum_masuk_lain'=>$jum_masuk_lain ,


											'jum_masuk_total'=>$jum_masuk_total,
											'jum_saldo_akhir'=>$jum_saldo_akhir,
											'jum_adjustment'=> $jum_adjustment,
											'jum_stok_opname'=> $jum_stok_opname,
											'saldo_sa_warna'=> $saldo_sa_warna,


											'data_warna_sjmb'=>$data_warna_sjmb,
											'data_warna_sjmp'=>$data_warna_sjmp,
											'data_warna_sjmr'=>$data_warna_sjmr,
											'data_warna_sjmrg'=>$data_warna_sjmrg,
											'data_warna_sjmll'=>$data_warna_sjmll,
											'totjum_masuk_warna_total'=>$totjum_masuk_warna_total,

											'data_warna_sjkp'=>$data_warna_sjkp,
											'data_warna_sjkgj'=>$data_warna_sjkgj,
											'data_warna_sjkr'=>$data_warna_sjkr,
											'data_warna_sjkll'=>$data_warna_sjkll,
											'totjum_keluar_warna_total'=>$totjum_keluar_warna_total,

											'totjum_warna_total'=>$totjum_warna_total,
										'saldo_so_warna'=> $saldo_so_warna,
										'totselisih_warna'=> $totselisih_warna,

											);
											$saldo_sa_warna=array();
											$data_warna_sjmb=array();
											$data_warna_sjmp=array();
											$data_warna_sjmr=array();
											$data_warna_sjmrg=array();
											$data_warna_sjmll=array();
											$totjum_masuk_warna_total=array();
											$data_warna_sjkp=array();
											$data_warna_sjkgj=array();
											$data_warna_sjkr=array();
											$data_warna_sjkll=array();
											$totjum_keluar_warna_total=array();
											$totjum_warna_total=array();
											$saldo_so_warna=array();
											$totselisih_warna=array();

			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
   function get_mutasi_stok_wip_dg_w($date_from, $date_to, $gudang) {

	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;

	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;

	// explode date_from utk keperluan saldo awal
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];

	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}

	$sql = "SELECT a.id_brg_wip, a.id_kel_brg_wip FROM (
				select b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukwip a
				INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarwip a
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukother_gudangqc a
				INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarother_gudangqc a
				INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

				)
				a GROUP BY a.id_brg_wip, a.id_kel_brg_wip ORDER BY a.id_kel_brg_wip, a.id_brg_wip"; //echo $sql; die();
		//AND a.status_approve='t'

		$query	= $this->db->query($sql);

		$data_stok = array();

		if ($query->num_rows() > 0){
			$hasil = $query->result();

			// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}



			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];



			foreach ($hasil as $row1) {
				// 20-03-2014
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}

				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}


				// $queryx	= $this->db->query(" SELECT c.jum_stok_opname,c.tot_jum_stok_opname,
				// 					c.id,d.nama,d.id as id_warna FROM tt_stok_opname_hasil_jahit a
				// 					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
				// 					INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
				// 					INNER JOIN tm_warna d ON d.id = c.id_warna
				// 					WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
				// 					AND a.bulan = '$bln_query' AND a.tahun = '$thn_query'  ");



$queryx	= $this->db->query(" SELECT
sum(a.jum_stok_opname) as jum_stok_opname,
sum(a.tot_jum_stok_opname) as tot_jum_stok_opname,
sum(a.id)::integer as id,
a.nama,
a.id_warna
FROM (
SELECT c.jum_stok_opname,c.tot_jum_stok_opname, c.id,d.nama,d.id as id_warna
FROM tt_stok_opname_hasil_jahit a
INNER JOIN tt_stok_opname_hasil_jahit_detail b
ON a.id = b.id_stok_opname_hasil_jahit
INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
INNER JOIN tm_warna d ON d.id = c.id_warna
WHERE b.id_brg_wip = '$row1->id_brg_wip'
AND a.id_gudang = '$gudang'
AND a.bulan = '$bln_query'
AND a.tahun = '$thn_query'

UNION ALL

SELECT  0 as jum_stok_opname, 0 as tot_jum_stok_opname, 0 as id, b.nama, a.id_warna
FROM tm_warna_brg_wip a
INNER JOIN tm_warna b ON a.id_warna=b.id
WHERE a.id_brg_wip='$row1->id_brg_wip'
) a
GROUP BY a.nama, a.id_warna  ");











						// AND b.status_approve = 't' AND a.status_approve = 't'

				if ($queryx->num_rows() > 0){

					$hasilrow2 = $queryx->result();

					foreach($hasilrow2 as $row2){
					$saldo_awal_warna = $row2->tot_jum_stok_opname;
					$id_so_detail_warna = $row2->id;
					$nama_warna = $row2->nama;
					$id_warna = $row2->id_warna;


				$queryxyz	= $this->db->query(" SELECT sum(b.jum_adjustment) as jum_adjustment  FROM tt_adjustment_hasil_jahit a
									INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id = b.id_adjustment_hasil_jahit
									INNER JOIN tt_adjustment_hasil_jahit_detail_warna c ON b.id = c.id_adjustment_hasil_jahit_detail
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND c.id_warna = '$id_warna' ");

				if ($queryxyz->num_rows() > 0){
					$hasilrowxyz = $queryxyz->row();
					$jum_adjustment_a = $hasilrowxyz->jum_adjustment;
					//~ $id_jum_detail_warna = $hasilrow->id;

				}
				else {
					$jum_adjustment_a = 0;

				}

				$saldo_awal_warna +=$jum_adjustment_a;

					$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '1' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_masuk = $hasilrow3->jum_masuk;

				}
				else
					$jum_masuk = 0;

					$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '2' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_perb_unit = $hasilrow3->jum_masuk;

				}
				else
					$jum_perb_unit = 0;

					$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '3' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_ret_unit_pack = $hasilrow3->jum_masuk;

				}
				else
					$jum_ret_unit_pack = 0;

					$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '4' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_ret_gd_jadi = $hasilrow3->jum_masuk;

				}
				else
					$jum_ret_gd_jadi = 0;

					$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '5' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_masuk_lain = $hasilrow3->jum_masuk;

				}
				else
					$jum_masuk_lain = 0;

					$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
							INNER JOIN tm_sjmasukother_gudangqc_detail_warna c ON b.id = c.id_sjmasukother_gudangqc_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_masuk_other = $hasilrow3->jum_masuk;

				}
				else
					$jum_masuk_other = 0;

						$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang_terima = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '5' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_masuk_bgs_qc = $hasilrow3->jum_masuk;

				}
				else
					$jum_masuk_bgs_qc = 0;

						$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang_terima = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '6' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_masuk_rtr_qc = $hasilrow3->jum_masuk;

				}
				else
					$jum_masuk_rtr_qc = 0;

			$jum_masuk_lain+=$jum_masuk_other;
			$saldo_masuk_warna=$jum_masuk+$jum_perb_unit+$jum_ret_unit_pack+$jum_ret_gd_jadi+$jum_masuk_bgs_qc+$jum_masuk_rtr_qc+$jum_masuk_lain;

				$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '1' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_keluar_pack = $hasilrow3->jum_keluar;

				}
				else
					$jum_keluar_pack = 0;

			$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '2' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_keluar_gdjadi = $hasilrow3->jum_keluar;

				}
				else
					$jum_keluar_gdjadi = 0;

			$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '3' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_keluar_perb = $hasilrow3->jum_keluar;

				}
				else
					$jum_keluar_perb = 0;

			$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '4' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_keluar_lain = $hasilrow3->jum_keluar;

				}
				else
					$jum_keluar_lain = 0;

				$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarother_gudangqc a
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
							INNER JOIN tm_sjkeluarother_gudangqc_detail_warna c ON b.id = c.id_sjkeluarother_gudangqc_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_keluar_other = $hasilrow3->jum_masuk;

				}
				else
					$jum_keluar_other = 0;


						$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '5' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_keluar_bgs_qc = $hasilrow3->jum_keluar;

				}
				else
					$jum_keluar_bgs_qc = 0;

						$queryx3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'  AND a.jenis_keluar = '6' ");
				if ($queryx3->num_rows() > 0){
					$hasilrow3 = $queryx3->row();
					$jum_keluar_rtr_qc = $hasilrow3->jum_keluar;

				}
				else
					$jum_keluar_rtr_qc = 0;

				$jum_keluar_total = $jum_keluar_pack+$jum_keluar_gdjadi+$jum_keluar_perb+$jum_keluar_lain+$jum_keluar_other+$jum_keluar_bgs_qc+$jum_keluar_rtr_qc;
				$jum_saldo_akhir = $saldo_awal_warna + $saldo_masuk_warna - $jum_keluar_total;
				$jum_keluar_lain+=$jum_keluar_other;

				$queryxyz	= $this->db->query(" SELECT sum(b.jum_adjustment) as jum_adjustment  FROM tt_adjustment_hasil_jahit a
									INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id = b.id_adjustment_hasil_jahit
									INNER JOIN tt_adjustment_hasil_jahit_detail_warna c ON b.id = c.id_adjustment_hasil_jahit_detail
									WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND c.id_warna = '$id_warna' ");

				if ($queryxyz->num_rows() > 0){
					$hasilrowxyz = $queryxyz->row();
					$jum_adjustment_b = $hasilrowxyz->jum_adjustment;
					//~ $id_jum_detail_warna = $hasilrow->id;

				}
				else {
					$jum_adjustment_b = 0;

				}

				$sql = "SELECT a.tgl_so,a.jenis_perhitungan_stok,  c.jum_stok_opname as jum_stok_opname,c.id FROM tt_stok_opname_hasil_jahit a
							INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
							INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
							WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang' AND c.id_warna = '$id_warna'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1'  ";

				$query3	= $this->db->query($sql);
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_so_detail_warna = $hasilrow->id;
					$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					$tgl_so = $hasilrow->tgl_so;

					//~ $sqlupdate = " UPDATE tt_stok_opname_hasil_jahit_detail_warna SET saldo_akhir = '$jum_saldo_akhir'
									//~ WHERE id = '$id_so_detail_warna' ";
					//~ $this->db->query($sqlupdate);

					//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip

						$sql77 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masukx = $hasilrow77->jum_masuk;

							if ($jum_masukx == '')
								$jum_masukx = 0;
						}
						else
							$jum_masukx = 0;

							$sql77 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_perb_unitx = $hasilrow77->jum_masuk;

							if ($jum_perb_unitx == '')
								$jum_perb_unitx = 0;
						}
						else
							$jum_perb_unitx = 0;

								$sql77 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_unit_packx = $hasilrow77->jum_masuk;

							if ($jum_ret_unit_packx == '')
								$jum_ret_unit_packx = 0;
						}
						else
							$jum_ret_unit_packx = 0;

						$sql77 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_gd_jadix = $hasilrow77->jum_masuk;

							if ($jum_ret_gd_jadix == '')
								$jum_ret_gd_jadix = 0;
						}
						else
							$jum_ret_gd_jadix = 0;

						$sql77 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_masuk = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_lainx = $hasilrow77->jum_masuk;

							if ($jum_masuk_lainx == '')
								$jum_masuk_lainx = 0;
						}
						else
							$jum_masuk_lainx = 0;

								$sql77 = " SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
							INNER JOIN tm_sjmasukother_gudangqc_detail_warna c ON b.id = c.id_sjmasukother_gudangqc_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND c.id_warna = '$id_warna' AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_otherx = $hasilrow77->jum_masuk;

							if ($jum_masuk_otherx == '')
								$jum_masuk_otherx = 0;
						}
						else
							$jum_masuk_otherx = 0;

						$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang_terima ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_keluar = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_bgsqcx = $hasilrow77->jum_keluar;

							if ($jum_masuk_bgsqcx == '')
								$jum_masuk_bgsqcx = 0;
						}
						else
							$jum_masuk_bgsqcx = 0;

						$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang_terima ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_keluar = '6' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_rtrqcx = $hasilrow77->jum_keluar;

							if ($jum_masuk_rtrqcx == '')
								$jum_masuk_rtrqcx = 0;
						}
						else
							$jum_masuk_rtrqcx = 0;

						$jum_masuk_totalx = $jum_masukx+$jum_perb_unitx+$jum_ret_unit_packx+
					$jum_ret_gd_jadix+$jum_masuk_lainx+$jum_masuk_otherx+$jum_masuk_bgsqcx+$jum_masuk_rtrqcx;

					$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_keluar = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_packx = $hasilrow77->jum_keluar;

							if ($jum_keluar_packx == '')
								$jum_keluar_packx = 0;
						}
						else
							$jum_keluar_packx = 0;

						$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_keluar = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_gdjadix = $hasilrow77->jum_keluar;

							if ($jum_keluar_gdjadix == '')
								$jum_keluar_gdjadix = 0;
						}
						else
							$jum_keluar_gdjadix = 0;


							$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_keluar = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_perbx = $hasilrow77->jum_keluar;

							if ($jum_keluar_perbx == '')
								$jum_keluar_perbx = 0;
						}
						else
							$jum_keluar_perbx = 0;


							$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND c.id_warna = '$id_warna' AND a.jenis_keluar = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_lainx = $hasilrow77->jum_keluar;

							if ($jum_keluar_lainx == '')
								$jum_keluar_lainx = 0;
						}
						else
							$jum_keluar_lainx = 0;

									$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
							INNER JOIN tm_sjkeluarother_gudangqc_detail_warna c ON b.id = c.id_sjkeluarother_gudangqc_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND c.id_warna = '$id_warna' AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_otherx = $hasilrow77->jum_keluar;

							if ($jum_keluar_otherx == '')
								$jum_keluar_otherx = 0;
						}
						else
							$jum_keluar_otherx = 0;

						$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								 AND c.id_warna = '$id_warna' AND a.id_gudang ='$gudang' AND a.jenis_keluar = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_bgsqcx = $hasilrow77->jum_keluar;

							if ($jum_keluar_bgsqcx == '')
								$jum_keluar_bgsqcx = 0;
						}
						else
							$jum_keluar_bgsqcx = 0;

								$sql77 = " SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND c.id_warna = '$id_warna' AND a.id_gudang ='$gudang' AND a.jenis_keluar = '6' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_rtrqcx = $hasilrow77->jum_keluar;

							if ($jum_keluar_rtrqcx == '')
								$jum_keluar_rtrqcx = 0;
						}
						else
							$jum_keluar_rtrqcx = 0;

						$jum_keluar_totalx = $jum_keluar_packx+$jum_keluar_gdjadix+
						$jum_keluar_perbx+$jum_keluar_lainx+$jum_keluar_otherx+$jum_keluar_bgsqcx+$jum_keluar_rtrqcx;

						$tambahkurang = $jum_masuk_totalx - $jum_keluar_totalx;

#echo "jum_masuk_totalx  = ".$jum_masuk_totalx."<br>";
#echo "jum_keluar_totalx = ".$jum_keluar_totalx."<br>";
#echo "jum_stok_opname   = ".$jum_stok_opname."<br>";
#die;
						$jum_stok_opname = $jum_stok_opname+ $tambahkurang;
						//~ if($jum_stok_opname==''){
							//~ $jum_stok_opname=0;
							//~ }


					$sqlupdate=" UPDATE tt_stok_opname_hasil_jahit_detail_warna set saldo_akhir = '$jum_saldo_akhir', tot_jum_stok_opname='$jum_stok_opname'
					WHERE id='$id_so_detail_warna'";
					$this->db->query($sqlupdate);
					//~ $sqlupdatetm = " UPDATE tm_stok_hasil_jahit a inner join tm_stok_hasil_jahit_warna b ON a.id=b.id_stok_hasil_jahit
									//~ SET stok = '$jum_stok_opname'
									//~ WHERE id_brg_wip = '$row1->id_brg_wip' AND id_warna='$id_warna'
									//~ AND id_gudang='$gudang' ";
					//~ $this->db->query($sqlupdatetm);

				}
				else {
					$id_so_detail_warna = 0;
					$jum_stok_opname = 0;
				}
					$selisih = $jum_stok_opname - $jum_saldo_akhir;
					$data_warna[]=array(
					'saldo_awal_warna'=>$saldo_awal_warna,
					'id_so_detail_warna'=>$id_so_detail_warna,
					'jum_masuk'=>$jum_masuk,
					'jum_perb_unit'=>$jum_perb_unit,
					'jum_ret_unit_pack'=>$jum_ret_unit_pack,
					'jum_ret_gd_jadi'=>$jum_ret_gd_jadi,
					'jum_masuk_lain'=>$jum_masuk_lain,
					'jum_masuk_bgs_qc'=>$jum_masuk_bgs_qc,
					'jum_masuk_rtr_qc'=>$jum_masuk_rtr_qc,
					'saldo_masuk_warna'=>$saldo_masuk_warna,
					'jum_keluar_pack'=>$jum_keluar_pack,
					'jum_keluar_gdjadi'=>$jum_keluar_gdjadi,
					'jum_keluar_perb'=>$jum_keluar_perb,
					'jum_keluar_lain'=>$jum_keluar_lain,
					'jum_keluar_bgs_qc'=>$jum_keluar_bgs_qc,
					'jum_keluar_rtr_qc'=>$jum_keluar_rtr_qc,
					'jum_keluar_total'=>$jum_keluar_total,
					'jum_saldo_akhir'=>$jum_saldo_akhir,
					'jum_stok_opname'=> $jum_stok_opname,
					'jum_adjustment_a'=> $jum_adjustment_a,
					'jum_adjustment_b'=> $jum_adjustment_b,
					'selisih'=> $selisih,
					'nama_warna'=>$nama_warna
					);



				}
			}
			else{
				$data_warna_sa='';
				}

					if ($row1->id_kel_brg_wip != '') {
					$sqlxx = " SELECT kode, nama FROM tm_kel_brg_wip WHERE id='$row1->id_kel_brg_wip' ";
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$kode_kel = $hasilxx->kode;
						$nama_kel = $hasilxx->nama;
					}
					else {
						$kode_kel = '';
						$nama_kel = '';
					}
				}
				else {
					$kode_kel = '';
					$nama_kel = '';
				}

					$data_stok[]=array(
					'kode_brg'=>$kode_brg_wip,
					'nama_brg'=>$nama_brg_wip,
					'kode_kel'=>$kode_kel,
					'nama_kel'=>$nama_kel,
					'data_warna'=>$data_warna,

					);
					$data_warna=array();
			}


		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }


  function get_mutasi_stok_wip($date_from, $date_to, $gudang) {

	  // explode date_from utk keperluan query
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;

	 // explode date_to utk keperluan query
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;

	// explode date_from utk keperluan saldo awal
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];

	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}

	$sql = "SELECT a.id_brg_wip, a.id_kel_brg_wip FROM (
				select b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukwip a
				INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarwip a
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukother_gudangqc a
				INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarother_gudangqc a
				INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					INNER JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang'
					 GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

				)
				a GROUP BY a.id_brg_wip, a.id_kel_brg_wip ORDER BY a.id_kel_brg_wip, a.id_brg_wip"; //echo $sql; die();
		//AND a.status_approve='t'
/*
	$sql = "SELECT a.id_brg_wip, a.id_kel_brg_wip FROM (
				select b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukwip a
				INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarwip a
				INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjmasukother_gudangqc a
				INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tm_sjkeluarother_gudangqc a
				INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
				WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id

				UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_stok_opname_hasil_jahit a
					INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id=b.id_stok_opname_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_stok_opname <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn1'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

					 UNION SELECT b.id_brg_wip, d.id as id_kel_brg_wip FROM tt_adjustment_hasil_jahit a
					INNER JOIN tt_adjustment_hasil_jahit_detail b ON a.id=b.id_adjustment_hasil_jahit
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					LEFT JOIN tm_kel_brg_wip d ON d.id = c.id_kel_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln1' AND a.tahun='$thn_query'
					AND a.id_gudang = '$gudang' AND b.jum_adjustment <> 0
					 GROUP BY b.id_brg_wip, d.id

				)
				a GROUP BY a.id_brg_wip, a.id_kel_brg_wip ORDER BY a.id_kel_brg_wip, a.id_brg_wip"; //echo $sql; die();
*/
		$query	= $this->db->query($sql);

		$jum_keluar_pack = 0;
		$jum_keluar_gdjadi = 0;
		$jum_keluar_perb = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$jum_perb_unit = 0;
		$jum_ret_unit_pack = 0;
		$jum_ret_gd_jadi = 0;
		$jum_masuk_lain = 0;
		$jum_masuk_other = 0;
		$jum_masuk_bgs_qc = 0;
		$jum_masuk_rtr_qc = 0;
		$jum_keluar_bgs_qc = 0;
		$jum_keluar_rtr_qc = 0;
		$data_stok = array();

		if ($query->num_rows() > 0){
			$hasil = $query->result();

			// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}

			/*
			$tglfromback= $tgl1."-".$bln_query."-".$thn_query;


			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			$tgltoback= $tgl1."-".$bln_query."-".$thn_query;
			*/

			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			foreach ($hasil as $row1) {
				// 20-03-2014
				/*$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}*/

				// 13-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
				// 04-03-2015, saldo awal bukan dari jum_stok_opname, tapi ambil dari auto_saldo_akhir
				// 21-04-2015, skrg saldo awal ambil dari SO LAGI.
				// 12-11-2015, khusus gudang QC, ambil dari SO

				$queryx	= $this->db->query("
											SELECT 
												z.tot_sojahit,
												z.id_sojahit,
												z.tot_adjustment_awal,
												z.id_adjustment_awal,
												z.tot_adjustment,
												z.id_adjustment,
												z.id_brg_wip,
												b.kode_brg,
												b.nama_brg
											FROM(
												SELECT
												  	b.tot_jum_stok_opname as tot_sojahit,
												  	b.id as id_sojahit,
												  	0 as tot_adjustment_awal,
													0 as id_adjustment_awal,
													0 as tot_adjustment,
													0 as id_adjustment,
													b.id_brg_wip 
												FROM
												  	tt_stok_opname_hasil_jahit a 
												   	INNER JOIN
												    	tt_stok_opname_hasil_jahit_detail b 
												    	ON a.id = b.id_stok_opname_hasil_jahit 
												WHERE
												   b.id_brg_wip = '$row1->id_brg_wip' 
												   AND a.id_gudang = '$gudang' 
												   AND a.bulan = '$bln_query' 
												   AND a.tahun = '$thn_query'

												UNION ALL
												SELECT
														0 as tot_sojahit,
														0 as id_sojahit,
													   	b.jum_adjustment as tot_adjustment_awal,
													   	b.id as id_adjustment_awal,
													   	0 as tot_adjustment,
	   													0 as id_adjustment,
	   													b.id_brg_wip  
													FROM
														tt_adjustment_hasil_jahit a 
														INNER JOIN
													    	tt_adjustment_hasil_jahit_detail b 
													    	ON a.id = b.id_adjustment_hasil_jahit 
													WHERE
													   b.id_brg_wip = '$row1->id_brg_wip' 
													   AND a.id_gudang = '$gudang' 
													   AND a.bulan = '$bln_query' 
													   AND a.tahun = '$thn_query'

													UNION ALL
													SELECT
														0 as tot_sojahit,
														0 as id_sojahit,
														0 as tot_adjustment_awal,
														0 as id_adjustment_awal,
														b.jum_adjustment as tot_adjustment,
													  	b.id as id_adjustment,
													  	b.id_brg_wip 
													FROM
														tt_adjustment_hasil_jahit a 
														INNER JOIN
													    	tt_adjustment_hasil_jahit_detail b 
													    	ON a.id = b.id_adjustment_hasil_jahit                            
													WHERE
													   b.id_brg_wip = '$row1->id_brg_wip' 
													   AND a.id_gudang = '$gudang' 
													   AND a.bulan = '$bln_query' 
													   AND a.tahun = '$thn_query'
												) as z
													INNER JOIN  
										    		tm_barang_wip b
										    		ON z.id_brg_wip = b.id
											  ");
					// AND b.status_approve = 't' AND a.status_approve = 't'

				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();

					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;

					$saldo_awal = $hasilrow->tot_sojahit;
					$id_so_detail = $hasilrow->id_sojahit;

					$saldo_awal_jum_adjustmentb = $hasilrow->tot_adjustment_awal;
					$id_jum_detail = $hasilrow->id_adjustment_awal;

					$jum_adjustment = $hasilrow->tot_adjustment;
					$id_jum_detail = $hasilrow->id_adjustment;

				}
				else {
						$kode_brg_wip = '';
						$nama_brg_wip = '';
						$saldo_awal=0;
						$saldo_awal_jum_adjustmentb=0;
						$jum_adjustment = 0;

				}
				/*$queryxyzb	= $this->db->query(" 
												
												  ");*/
						// AND b.status_approve = 't' AND a.status_approve = 't'

				/*if ($queryxyzb->num_rows() > 0){
					$hasilrowxyzb = $queryxyzb->row();
					$saldo_awal_jum_adjustmentb = $hasilrowxyzb->jum_adjustment;
					$id_jum_detail = $hasilrow->id;

				}

				else {
					$saldo_awal_jum_adjustmentb=0;

				}*/

				/*$queryxyz	= $this->db->query(" 
												

												 ");*/

				/*if ($queryxyz->num_rows() > 0){
					$hasilrowxyz = $queryxyz->row();
					$jum_adjustment = $hasilrowxyz->jum_adjustment;
					$id_jum_detail = $hasilrow->id;
				}
				else {
					$jum_adjustment = 0;
				}*/

				$saldo_awal+=$saldo_awal_jum_adjustmentb;
//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
				//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip

				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;

				}
				else
					$jum_masuk = 0;

				//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_perb_unit = $hasilrow->jum_masuk;
				}
				else
					$jum_perb_unit = 0;

				//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_ret_unit_pack = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_unit_pack = 0;

				//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_ret_gd_jadi = $hasilrow->jum_masuk;
				}
				else
					$jum_ret_gd_jadi = 0;

				//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_masuk = '5' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_lain = 0;

				//6. hitung brg masuk OTHER tm_masukother_gudangqc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang'  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_other = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_other = 0;

							//. hitung brg masuk qc qc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang_terima = '$gudang' AND a.jenis_keluar = '5' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bgs_qc = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bgs_qc = 0;

					//. hitung brg masuk qc qc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang_terima = '$gudang' AND a.jenis_keluar = '6' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_rtr_qc = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_rtr_qc = 0;

					//~ $jum_masuk_total = $jum_masuk+$jum_perb_unit+$jum_ret_unit_pack+$jum_ret_gd_jadi+$jum_masuk_lain+$jum_masuk_other;
					$jum_masuk_total = $jum_masuk+$jum_perb_unit+$jum_ret_unit_pack+$jum_ret_gd_jadi+$jum_masuk_lain+$jum_masuk_other+$jum_masuk_bgs_qc+$jum_masuk_rtr_qc;

					$jum_masuk_lain+=$jum_masuk_other;

			//--------------------------------------------------------------------------------------------------
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_pack = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_pack = 0;

				// hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_gdjadi = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_gdjadi = 0;

				// hitung brg keluar lain jenis=3 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perb = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perb = 0;

				// hitung brg keluar lain jenis=4 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '4' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;

					//. hitung brg keluar OTHER tm_sjkeluarother_gudangqc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang'  ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_other = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_other = 0;

					//. hitung brg keluar qc qc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '5' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bgs_qc = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bgs_qc = 0;

					//. hitung brg keluar qc qc
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.id_gudang = '$gudang' AND a.jenis_keluar = '6' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_rtr_qc = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_rtr_qc = 0;

				$jum_keluar_total = $jum_keluar_pack+$jum_keluar_gdjadi+$jum_keluar_perb+$jum_keluar_lain+$jum_keluar_other+$jum_keluar_bgs_qc+$jum_keluar_rtr_qc;
				// 18-01-2016, saldo akhir dihitung dari selisih masuk dan keluar
				$jum_saldo_akhir = $saldo_awal + $jum_masuk_total - $jum_keluar_total;
				$jum_keluar_lain+=$jum_keluar_other;


				// 13-09-2013 ================================
				$sql = " SELECT a.tgl_so,a.jenis_perhitungan_stok, b.id, b.jum_stok_opname FROM tt_stok_opname_hasil_jahit a
							INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
							WHERE b.id_brg_wip = '$row1->id_brg_wip' AND a.id_gudang = '$gudang'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1'  ";
				// AND b.status_approve = 't'

				$query3	= $this->db->query($sql);

				//$jum_so='';
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$id_so_detail = $hasilrow->id;
					$jenis_perhitungan_stok = $hasilrow->jenis_perhitungan_stok;
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);
					$tgl_so = $hasilrow->tgl_so;

					// 15-12-2015, update saldo akhir di tabel stok opname nya
					$sqlupdate = " UPDATE tt_stok_opname_hasil_jahit_detail SET saldo_akhir = '$jum_saldo_akhir'
									WHERE id = '$id_so_detail' ";
					$this->db->query($sqlupdate);

						//1. hitung brg masuk bagus (jenis=1) dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masukx = $hasilrow77->jum_masuk;

							if ($jum_masukx == '')
								$jum_masukx = 0;
						}
						else
							$jum_masukx = 0;



						//2. hitung brg masuk lain jenis = 2 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_perb_unitx = $hasilrow77->jum_masuk;

							if ($jum_perb_unitx == '')
								$jum_perb_unitx = 0;
						}
						else
							$jum_perb_unitx = 0;



						//3. hitung brg masuk lain jenis = 3 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_unit_packx = $hasilrow77->jum_masuk;

							if ($jum_ret_unit_packx == '')
								$jum_ret_unit_packx = 0;
						}
						else
							$jum_ret_unit_packx = 0;

							//4. hitung brg masuk lain jenis = 4 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_ret_gd_jadix = $hasilrow77->jum_masuk;

							if ($jum_ret_gd_jadix == '')
								$jum_ret_gd_jadix = 0;
						}
						else
							$jum_ret_gd_jadix = 0;


								//5. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_masuk = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_lainx = $hasilrow77->jum_masuk;

							if ($jum_masuk_lainx == '')
								$jum_masuk_lainx = 0;
						}
						else
							$jum_masuk_lainx = 0;


								//6. hitung brg masuk lain jenis = 5 dari tm_sjmasukwip
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukother_gudangqc a
							INNER JOIN tm_sjmasukother_gudangqc_detail b ON a.id = b.id_sjmasukother_gudangqc
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_otherx = $hasilrow77->jum_masuk;

							if ($jum_masuk_otherx == '')
								$jum_masuk_otherx = 0;
						}
						else
							$jum_masuk_otherx = 0;


					// qcqc
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang_terima ='$gudang' AND a.jenis_keluar = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_bgsqcx = $hasilrow77->jum_keluar;

							if ($jum_keluar_bgsqcx == '')
								$jum_keluar_bgsqcx = 0;
						}
						else
							$jum_keluar_bgsqcx = 0;

						// qcqc
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang_terima ='$gudang' AND a.jenis_keluar = '6' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_rtrqcx = $hasilrow77->jum_keluar;

							if ($jum_keluar_rtrqcx == '')
								$jum_keluar_rtrqcx = 0;
						}
						else
							$jum_keluar_rtrqcx = 0;

/*
					$jum_masuk_totalx = $jum_masukx+$jum_perb_unitx+$jum_ret_unit_packx+
					$jum_ret_gd_jadix+$jum_masuk_lainx+$jum_masuk_otherx+$jum_keluar_bgsqcx+$jum_keluar_rtrqcx;
*/
							// 1. hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '1' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_packx = $hasilrow77->jum_keluar;

							if ($jum_keluar_packx == '')
								$jum_keluar_packx = 0;
						}
						else
							$jum_keluar_packx = 0;

							// 2. hitung brg keluar bagus jenis=2 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '2' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_gdjadix = $hasilrow77->jum_keluar;

							if ($jum_keluar_gdjadix == '')
								$jum_keluar_gdjadix = 0;
						}
						else
							$jum_keluar_gdjadix = 0;

							// 3. hitung brg keluar bagus jenis=3 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '3' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_perbx = $hasilrow77->jum_keluar;

							if ($jum_keluar_perbx == '')
								$jum_keluar_perbx = 0;
						}
						else
							$jum_keluar_perbx = 0;



							// 4. hitung brg keluar bagus jenis=4 dari tm_sjkeluarwip
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '4' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_lainx = $hasilrow77->jum_keluar;

							if ($jum_keluar_lainx == '')
								$jum_keluar_lainx = 0;
						}
						else
							$jum_keluar_lainx = 0;


							// 5. hitung brg keluar  dari tm_sjkeluarother_gudangqc
						$sql77 = " SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarother_gudangqc a
							INNER JOIN tm_sjkeluarother_gudangqc_detail b ON a.id = b.id_sjkeluarother_gudangqc
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang'  ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_keluar_otherx = $hasilrow77->jum_keluar;

							if ($jum_keluar_otherx == '')
								$jum_keluar_otherx = 0;
						}
						else
							$jum_keluar_otherx = 0;

								// qcqc
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '5' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_bgsqcx = $hasilrow77->jum_masuk;

							if ($jum_masuk_bgsqcx == '')
								$jum_masuk_bgsqcx = 0;
						}
						else
							$jum_masuk_bgsqcx = 0;

						// qcqc
						$sql77 = " SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a
							INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									WHERE ";
							if ($jenis_perhitungan_stok == '1' || $jenis_perhitungan_stok == '2')
								$sql77.=" a.tgl_sj > '$tgl_so' ";
							else if ($jenis_perhitungan_stok == '3' || $jenis_perhitungan_stok == '4')
								$sql77.=" a.tgl_sj >= '$tgl_so' ";
						$sql77.=" AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
								AND a.id_gudang ='$gudang' AND a.jenis_keluar = '6' ";
						$query77	= $this->db->query($sql77);
						if ($query77->num_rows() > 0){
							$hasilrow77 = $query77->row();
							$jum_masuk_rtrqcx = $hasilrow77->jum_masuk;

							if ($jum_masuk_rtrqcx == '')
								$jum_masuk_rtrqcx = 0;
						}
						else
							$jum_masuk_rtrqcx = 0;


					      $jum_masuk_totalx = $jum_masukx+$jum_perb_unitx+$jum_ret_unit_packx+
					      $jum_ret_gd_jadix+$jum_masuk_lainx+$jum_masuk_otherx+$jum_masuk_bgsqcx+$jum_masuk_rtrqcx;

								$jum_keluar_totalx = $jum_keluar_packx+$jum_keluar_gdjadix+
								$jum_keluar_perbx+$jum_keluar_lainx+$jum_keluar_otherx+$jum_keluar_bgsqcx+$jum_keluar_rtrqcx;
/*
								$jum_keluar_totalx = $jum_keluar_packx+$jum_keluar_gdjadix+
								$jum_keluar_perbx+$jum_keluar_lainx+$jum_keluar_otherx+$jum_masuk_bgsqcx+$jum_masuk_rtrqcx;
*/
								$tambahkurang = $jum_masuk_totalx - $jum_keluar_totalx;

						$jum_stok_opname = $jum_stok_opname+ $tambahkurang;

					$sqlupdate="Update tt_stok_opname_hasil_jahit_detail set tot_jum_stok_opname='$jum_stok_opname'
					WHERE id='$id_so_detail'";
					$this->db->query($sqlupdate);

					$sqlupdatetm = " UPDATE tm_stok_hasil_jahit SET stok = '$jum_stok_opname'
									WHERE id_brg_wip = '$row1->id_brg_wip'
									AND id_gudang='$gudang' ";
					$this->db->query($sqlupdatetm);


				}
				else {
					$id_so_detail = 0;
					$jum_stok_opname = 0;
				}
				//=========================================


				//17-09-2014, ambil nama kel brg jadi
				if ($row1->id_kel_brg_wip != '') {
					$sqlxx = " SELECT kode, nama FROM tm_kel_brg_wip WHERE id='$row1->id_kel_brg_wip' ";
					$queryxx	= $this->db->query($sqlxx);
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$kode_kel = $hasilxx->kode;
						$nama_kel = $hasilxx->nama;
					}
					else {
						$kode_kel = '';
						$nama_kel = '';
					}
				}
				else {
					$kode_kel = '';
					$nama_kel = '';
				}

				$data_stok[] = array(		'kode_brg'=> $kode_brg_wip,
											'kode_kel'=> $kode_kel,
											'nama_kel'=> $nama_kel,
											'nama_brg'=> $nama_brg_wip,
											// per 08-04-2015 pake auto saldo akhir
											// 21-04-2015, waktu hari sabtu kata ginanjar pake SO lagi
											// 12-11-2015 duta utk gudang QC acuannya dari SO
											//'saldo_awal'=> $auto_saldo_awal,
											'saldo_awal'=> $saldo_awal,
											'jum_keluar_pack'=>$jum_keluar_pack ,
											'jum_keluar_gdjadi'=>$jum_keluar_gdjadi,
											'jum_keluar_perb'=>$jum_keluar_perb,
											'jum_keluar_lain'=>$jum_keluar_lain,
											'jum_keluar_total'=>$jum_keluar_total,

											'jum_masuk'=>$jum_masuk ,
											'jum_perb_unit'=>$jum_perb_unit,
											'jum_ret_unit_pack'=>$jum_ret_unit_pack,
											'jum_ret_gd_jadi'=>$jum_ret_gd_jadi ,
											'jum_masuk_lain'=>$jum_masuk_lain ,

											'jum_keluar_bgs_qc'=>$jum_keluar_bgs_qc,
											'jum_keluar_rtr_qc'=>$jum_keluar_rtr_qc,
											'jum_masuk_bgs_qc'=>$jum_masuk_bgs_qc,
											'jum_masuk_rtr_qc'=>$jum_masuk_rtr_qc,

											'jum_masuk_total'=>$jum_masuk_total,
											'jum_saldo_akhir'=>$jum_saldo_akhir,
											'jum_adjustment'=> $jum_adjustment,
											'jum_stok_opname'=> $jum_stok_opname,

											);

			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }

  // ======================= 12-12-2014 PEMBAHARUAN 15-10-2015 =======================================
  function get_mutasi_stok_hasilcutting($date_from, $date_to) {
	  // 12-12-2014 =============================================
	  // explode date_from utk keperluan query
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgldari = $thn1."-".$bln1."-".$tgl1;

		 // explode date_to utk keperluan query
		$pisah1 = explode("-", $date_to);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tglke = $thn1."-".$bln1."-".$tgl1;

		// explode date_from utk keperluan saldo awal
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];

		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		/*
			if ($bln1 == 1) {
			$bln_back = 11;
			$thn_back = $thn1-1;
		}
		else {
			$bln_back = $bln1-2;
			$thn_back = $thn1;
			if ($bln_back < 10)
				$bln_back = "0".$bln_back;
		}
	  */
		$sql = "SELECT a.id_brg_wip, a.kode_brg_wip, a.nama_brg_wip FROM (
					select b.id_brg_wip, c.kode_brg as kode_brg_wip, c.nama_brg as nama_brg_wip FROM tm_bonmmasukcutting a
					INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_bonm >='".$tgldari."'
					AND a.tgl_bonm <='".$tglke."' GROUP BY b.id_brg_wip, c.kode_brg, c.nama_brg

					UNION SELECT b.id_brg_wip, c.kode_brg as kode_brg_wip, c.nama_brg as nama_brg_wip FROM tm_sjmasukbhnbakupic a
					INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_sj >='".$tgldari."'
					AND a.tgl_sj <='".$tglke."' GROUP BY b.id_brg_wip, c.kode_brg, c.nama_brg

					UNION SELECT b.id_brg_wip, c.kode_brg as kode_brg_wip, c.nama_brg as nama_brg_wip FROM tm_bonmkeluarcutting a
					INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.tgl_bonm >='".$tgldari."'
					AND a.tgl_bonm <='".$tglke."' GROUP BY b.id_brg_wip, c.kode_brg, c.nama_brg

					UNION SELECT b.id_brg_wip, c.kode_brg as kode_brg_wip, c.nama_brg as nama_brg_wip FROM tt_stok_opname_hasil_cutting a
					INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id=b.id_stok_opname_hasil_cutting
					INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
					WHERE c.status_aktif='t' AND a.bulan='$bln_query' AND a.tahun='$thn_query'
					AND b.jum_stok_opname <> 0
					AND a.status_approve='t' GROUP BY b.id_brg_wip, c.kode_brg, c.nama_brg
				)
				a GROUP BY a.id_brg_wip, a.kode_brg_wip, a.nama_brg_wip ORDER BY a.kode_brg_wip";
	  //=========================================================

		//$sql = "SELECT * FROM tm_stok_hasil_cutting ORDER BY kode_brg_jadi, kode_brg ";
		$query	= $this->db->query($sql);

		$jum_masuk_bgs = 0;
		$jum_masuk_lain = 0;
		$jum_masuk_retur = 0;

		$jum_keluar_bgs = 0;
		$jum_keluar_lain = 0;
		$jum_keluar_retur = 0;

		$data_stok = array();

		if ($query->num_rows() > 0){
			$hasil = $query->result();

			// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}


			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];




			foreach ($hasil as $row1) {
				//12-12-2014
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$kode_brg_wip = $hasilrow->kode_brg;
					$nama_brg_wip = $hasilrow->nama_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}

				// 26-09-2013, SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA
				// revisi 12-12-2014
					$queryx	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_cutting a
									INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
									WHERE b.id_brg_wip = '$row1->id_brg_wip'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");

				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;

					if ($saldo_awal == '') {
						$saldo_awal = 0;
					}
				}
				else {
					$saldo_awal = 0;
				}

			/*	masuk:
				gd perminggu = bon M masuk hasil cutting, jenis masuk bagus
				lain2 =  bon M masuk hasil cutting, jenis masuk lain2
				retur = SJ masuk bhn baku dari jahitan */

				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a
							INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.jenis_masuk = '1' ");
				/*echo "SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a,
							tm_bonmmasukcutting_detail b
							WHERE a.id = b.id_bonmmasukcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.jenis_masuk = '1' <br><br>"; */
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bgs = $hasilrow->jum_masuk;

					if ($jum_masuk_bgs == '')
						$jum_masuk_bgs = 0;
				}
				else
					$jum_masuk_bgs = 0;

				//2. hitung brg masuk lain2 (jenis = 2) dari tm_bonmmasukcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukcutting a
							INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.jenis_masuk = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk;

					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;

				//3. hitung brg masuk retur dari tm_sjmasukbhnbakupic
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a
							INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_retur = $hasilrow->jum_masuk;

					if ($jum_masuk_retur == '')
						$jum_masuk_retur = 0;
				}
				else
					$jum_masuk_retur = 0;

				/*
		keluar:
		kirim unit = SJ keluar bhn baku hasil cutting, jenis keluar bagus
		lain2 = SJ keluar bhn baku hasil cutting, jenis keluar lain2
		retur = kata ginanjar ga ada. Tapi di form input SJ keluar hasil cutting sediakan jenis keluar Pengembalian Retur */

				// 4. hitung brg keluar bagus jenis=1 dari tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a
							INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.jenis_keluar = '1' ");
				/*echo "SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a,
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row1->kode_brg_jadi'
							AND a.jenis_keluar = '1' <br><br>"; */
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bgs = $hasilrow->jum_keluar;

					if ($jum_keluar_bgs == '')
						$jum_keluar_bgs = 0;
				}
				else
					$jum_keluar_bgs = 0;

				// 5. hitung brg keluar lain jenis=3 dari bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a
							INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.jenis_keluar = '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;

					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
				}
				else
					$jum_keluar_lain = 0;

				// 5. hitung brg keluar retur jenis=2 dari bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluarcutting a
							INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.id_brg_wip = '$row1->id_brg_wip'
							AND a.jenis_keluar = '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_retur = $hasilrow->jum_keluar;

					if ($jum_keluar_retur == '')
						$jum_keluar_retur = 0;
				}
				else
					$jum_keluar_retur = 0;

				//----------- STOK OPNAME -----------------------------
				$sql3 = "SELECT b.jum_stok_opname FROM tt_stok_opname_hasil_cutting a
							INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
							WHERE b.id_brg_wip = '$row1->id_brg_wip'
							AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";

				$query3	= $this->db->query($sql3);

				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					//$jum_stok_opname = round($jum_stok_opname, 2);

					if ($jum_stok_opname == '')
						$jum_stok_opname = 0;
				}
				else {
					$jum_stok_opname = 0;
				}
				//=========================================

				$data_stok[] = array(
											'id_brg_wip'=> $row1->id_brg_wip,
											'kode_brg_wip'=> $kode_brg_wip,
											'nama_brg_wip'=> $nama_brg_wip,
											'saldo_awal'=> $saldo_awal,
											// 06-04-2015 ini diaktifin kalo saldo akhir udh akurat pake skrip recalculte
											// 15-10-2015 no longer used anymore
											//'saldo_awal'=> $auto_saldo_awal,
											'jum_masuk_bgs'=> $jum_masuk_bgs,
											'jum_masuk_lain'=> $jum_masuk_lain,
											'jum_masuk_retur'=> $jum_masuk_retur,
											'jum_keluar_bgs'=> $jum_keluar_bgs,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'jum_keluar_retur'=> $jum_keluar_retur,
											'jum_stok_opname'=> $jum_stok_opname,



											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }
  //===========================================================================

  // 04-06-2014
  function get_sobhnbaku($num, $offset, $id_gudang, $bulan, $tahun) {
	$sql = " * FROM tt_stok_opname_bahan_baku WHERE TRUE ";

	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_gudang != '0')
		$sql.= " AND id_gudang = '$id_gudang' ";
		$sql.= " ORDER BY tahun DESC, bulan DESC ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();

	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$bln1 = $row1->bulan;
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";

				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;

				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di gudang ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di gudang ini ?";

									$query41 = $this->db->query("SELECT * FROM tm_periode_bb order by id desc limit 1");
					if($query41->num_rows() > 0){
					$hasilrow41 = $query41->row();
					$tahun_periode = $hasilrow41->i_year;
					$bulan_periode = $hasilrow41->i_month;
					$tanggal_periode = $hasilrow41->i_periode;
}
else{
	$tahun_periode = '';
					$bulan_periode = '';
					$tanggal_periode = '';
	}

				$data_so[] = array(			'id'=> $row1->id,
											'bulan'=> $row1->bulan,
											'nama_bln'=> $nama_bln,
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'tahun_periode'=> $tahun_periode,
											'bulan_periode'=> $bulan_periode,
											'tanggal_periode'=>$tanggal_periode,
											'id_gudang'=> $id_gudang,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi

											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }

  function get_sobhnbakutanpalimit($id_gudang, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_bahan_baku WHERE TRUE ";

	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($id_gudang != '0')
		$sql.= " AND id_gudang = '$id_gudang' ";

	$query	= $this->db->query($sql);
    return $query->result();
  }

  function get_detail_sobhnbaku($id_so) {
		$query	= $this->db->query("SELECT a.id as id_header, a.id_gudang, a.bulan, a.tahun, a.tgl_so, a.jenis_perhitungan_stok, b.*,
					d.kode_brg, d.nama_brg, d.satuan, d.id_satuan_konversi, d.rumus_konversi, d.angka_faktor_konversi
					FROM tt_stok_opname_bahan_baku_detail b
					INNER JOIN tt_stok_opname_bahan_baku a ON b.id_stok_opname_bahan_baku = a.id
					INNER JOIN tm_barang d ON b.id_brg = d.id
					WHERE a.id = '$id_so' ORDER BY d.kode_brg ");

		if ($query->num_rows() > 0){
			$hasil = $query->result();

			$detail_bahan = array();
			foreach ($hasil as $row) {
				// 17-03-2015
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month

				// 04-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}

				// 25-11-2014 CONTEK DARI MODUL stok-opname mmaster.php
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";

				// 27-05-2015 HITUNG SALDO AKHIR UTK SEMENTARA GA DIPAKE DULU
				// 22-06-2015 DIKOMEN AJAA
				// ---------- 12-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
			/*	$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a,
									tt_stok_opname_bahan_baku_detail b
									WHERE a.id = b.id_stok_opname_bahan_baku
									AND b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$row->id_gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$saldo_awal = $hasilx->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;

				// 2. hitung masuk bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE b.tgl_bonm >='".$row->tahun."-".$row->bulan."-01'
							AND b.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND b.status_stok = 't' AND e.id_gudang = '$row->id_gudang' ");
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_masuk = $hasilx->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;

				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$jum_masuk = $jum_masuk * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$jum_masuk = $jum_masuk / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$jum_masuk = $jum_masuk + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$jum_masuk = $jum_masuk - $row->angka_faktor_konversi;
				}
				$jum_masuk = round($jum_masuk, 2);

				// 3. masuk lain2
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01'
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$row->id_gudang' AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;

				// 3. hitung keluar bln ini
				$queryx	= $this->db->query(" SELECT sum(b.qty_satawal) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
							INNER JOIN tm_gudang d ON d.id = e.id_gudang
							WHERE  a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01'
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
							AND d.id = '$row->id_gudang' AND b.is_quilting = 'f' ");

				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_keluar = $hasilx->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;

					// 27-02-2015
					if ($row->id_satuan_konversi != 0) {
						if ($row->rumus_konversi == '1') {
							$jum_keluar = $jum_keluar*$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '2') {
							$jum_keluar = $jum_keluar/$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '3') {
							$jum_keluar = $jum_keluar+$row->angka_faktor_konversi;
						}
						else if ($row->rumus_konversi == '4') {
							$jum_keluar = $jum_keluar-$row->angka_faktor_konversi;
						}
					}
				}
				else
					$jum_keluar = 0;

				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk+$jum_masuk_lain-$jum_keluar; */
				$auto_saldo_akhir = 0;
				//-------------------------------------------------------------------------------------

				// 31-10-2014. 27-05-2015 DIKOMEN KARENA GA DIPAKE LG
			/*	$sqlxx = " SELECT * FROM tt_stok_opname_bahan_baku_detail_harga
							WHERE id_stok_opname_bahan_baku_detail = '$row->id' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();

					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_harga
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga
							WHERE kode_brg = '$row->kode_brg' AND
							harga = '$rowxx->harga' ");

						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}

						// ---------- 12-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_bahan_baku a
											INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
											INNER JOIN tt_stok_opname_bahan_baku_detail_harga c ON b.id = c.id_stok_opname_bahan_baku_detail
											WHERE b.kode_brg = '$row->kode_brg' AND a.id_gudang = '$row->id_gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.harga = '".$rowxx->harga."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_harga = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_harga = 0;

						// 2. jum masuk bln ini
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a
									INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE b.tgl_bonm >='".$row->tahun."-".$row->bulan."-01'
									AND b.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND b.status_stok = 't' AND e.id_gudang = '$row->id_gudang'
									AND b.harga = '$rowxx->harga' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_harga = $hasilrow->jum_masuk;
							if ($jum_masuk_harga == '')
								$jum_masuk_harga = 0;
						}
						else
							$jum_masuk_harga = 0;

						if ($row->id_satuan_konversi != '0') {
							if ($row->rumus_konversi == '1')
								$jum_masuk_harga = $jum_masuk_harga * $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '2')
								$jum_masuk_harga = $jum_masuk_harga / $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '3')
								$jum_masuk_harga = $jum_masuk_harga + $row->angka_faktor_konversi;
							else if ($row->rumus_konversi == '4')
								$jum_masuk_harga = $jum_masuk_harga - $row->angka_faktor_konversi;
						}
						$jum_masuk_harga = round($jum_masuk_harga, 2);

						// 3. jum masuk lain2 bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
									INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
									INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01'
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$row->id_gudang' AND c.harga = '$rowxx->harga' AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_lain_harga = $hasilrow->jum_masuk_lain;
							if ($jum_masuk_lain_harga == '')
								$jum_masuk_lain_harga = 0;
						}
						else
							$jum_masuk_lain_harga = 0;

						// 4. jum keluar bln ini
						// ambilnya bukan qty, tapi qty_satawal
						$query3	= $this->db->query(" SELECT sum(c.qty_satawal) as jum_keluar FROM tm_bonmkeluar a
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
									INNER JOIN tm_barang e ON e.kode_brg = b.kode_brg
									INNER JOIN tm_gudang d ON d.id = e.id_gudang
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01'
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg = '$row->kode_brg'
									AND d.id = '$row->id_gudang' AND c.harga = '$rowxx->harga'
									AND b.is_quilting = 'f' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_harga = $hasilrow->jum_keluar;
							if ($jum_keluar_harga == '')
								$jum_keluar_harga = 0;

							// 27-02-2015
							if ($row->id_satuan_konversi != 0) {
								if ($row->rumus_konversi == '1') {
									$jum_keluar_harga = $jum_keluar_harga*$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '2') {
									$jum_keluar_harga = $jum_keluar_harga/$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '3') {
									$jum_keluar_harga = $jum_keluar_harga+$row->angka_faktor_konversi;
								}
								else if ($row->rumus_konversi == '4') {
									$jum_keluar_harga = $jum_keluar_harga-$row->angka_faktor_konversi;
								}
							}
							else
								$jum_keluar_harga = 0;
						}
						// 5. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_harga = $saldo_awal_harga+$jum_masuk_harga-$jum_keluar_harga;
						//-------------------------------------------------------------------------------------
						// ===================================================

						$detail_harga[] = array(
									'harga'=> $rowxx->harga,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'auto_saldo_akhir_harga'=> $auto_saldo_akhir_harga
								);
					}
				}
				else
					$detail_harga = ''; */
				// --------------------------------

				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";

				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi
								FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id WHERE a.id = '$row->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;

				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}

				// 27-05-2015
				$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE id_brg= '$row->id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$stok	= $hasilrow->stok;
				}
				else {
					$stok = 0;
				}

				$tgl_so = $row->tgl_so;
				$pisah1 = explode("-", $tgl_so);
				$thn1= $pisah1[0];
				$bln1= $pisah1[1];
				$tgl1= $pisah1[2];
				$tgl_so = $tgl1."-".$bln1."-".$thn1;

				$detail_bahan[] = array('id_header'=> $row->id_header,
										'kode_gudang'=> $kode_gudang,
										'nama_gudang'=> $nama_gudang,
										'nama_lokasi'=> $nama_lokasi,
										'id_gudang'=> $row->id_gudang,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'nama_bln'=> $nama_bln,
										'jenis_perhitungan_stok'=>$row->jenis_perhitungan_stok,
										'tgl_so'=> $tgl_so,
										'id'=> $row->id,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $nama_satuan,
										// 27-05-2015
										'saldo_akhir'=> $stok,
										'stok_opname'=> $row->jum_stok_opname,
										'nama_satuan_konv'=> $nama_satuan_konv,
										//'detail_harga'=> $detail_harga,
										'auto_saldo_akhir'=> $auto_saldo_akhir
									);
				//$detail_harga = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }

  // 13-06-2015
  function get_detail_stokbrgbaru($id_so) {
	  // ambil id_gudang dari acuan id_so
	  $sqlxx = " SELECT id_gudang FROM tt_stok_opname_bahan_baku WHERE id='$id_so' ";
	  $queryxx = $this->db->query($sqlxx);

		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_gudang = $hasilxx->id_gudang;
		}
		else
			$id_gudang = 0;

		$query	= $this->db->query(" SELECT distinct b.id as id_brg, a.stok, b.kode_brg, b.nama_brg, c.nama as nama_satuan,
							b.id_satuan_konversi, b.rumus_konversi, b.angka_faktor_konversi
							FROM tm_barang b
							LEFT JOIN tm_stok a ON a.id_brg = b.id
							LEFT JOIN tm_satuan c ON c.id = b.satuan
							WHERE b.id_gudang = '$id_gudang' AND b.status_aktif='t'
							AND b.id NOT IN
							(select b.id_brg FROM tt_stok_opname_bahan_baku a
							INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_stok_opname_bahan_baku
							WHERE a.id = '$id_so')
							ORDER BY b.kode_brg ");

		if ($query->num_rows() > 0){
			$hasil = $query->result();

			$detail_bahan = array();
			foreach ($hasil as $row) {
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";

				if ($row->id_satuan_konversi != '0') {
					if ($row->rumus_konversi == '1')
						$stokinduk = $row->stok * $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '2')
						$stokinduk = $row->stok / $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '3')
						$stokinduk = $row->stok + $row->angka_faktor_konversi;
					else if ($row->rumus_konversi == '4')
						$stokinduk = $row->stok - $row->angka_faktor_konversi;
					else
						$stokinduk = $row->stok;

					$stokinduk = round($stokinduk, 4);
				}
				else {
					$stokinduk = $row->stok;

					if ($stokinduk == '')
						$stokinduk = 0;
				}

				$detail_bahan[] = array('id'=> 0,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'satuan'=> $row->nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'saldo_akhir'=> $stokinduk,
										'stok_opname'=> $stokinduk
									);
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }

  // ------------------- end 13-06-2015 -------------------------------------------

  // 27-05-2015 dimodif, harga dan auto_saldo_akhir ga dipake
  //function savesobhnbaku($id_so, $kode_brg, $stok, $stok_fisik, $harga, $auto_saldo_akhir, $auto_saldo_akhir_harga){
  function savesobhnbaku($id_so, $tgl_so, $gudang, $id_brg, $id_itemnya, $stok, $stok_fisik){
	  $tgl = date("Y-m-d H:i:s");
	  //$tgl = date("Y-m-d");

		// 27-05-2015 dikomen
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		/*$qtytotalstokawal = 0;
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($harga); $xx++) {
			$harga[$xx] = trim($harga[$xx]);
			$stok[$xx] = trim($stok[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);

			// 17-03-2015
			$auto_saldo_akhir_harga[$xx] = trim($auto_saldo_akhir_harga[$xx]);

			$qtytotalstokawal+= $stok[$xx];
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
		*/

		// ambil id detail id_stok_opname_bahan_baku_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_bahan_baku_detail
					where id_brg = '$id_brg'
					AND id_stok_opname_bahan_baku = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;

		//$totalxx = $qtytotalstokfisik;
		$totalxx = $stok_fisik;

		// =============================== 13-06-2015 SEKALIGUS UPDATE STOKNYA =======================================================

		// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya

			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/

			// 1.ambil brg masuk hasil pembelian
			$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_apply_stok_pembelian a
							INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
							WHERE b.tgl_bonm > '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND b.status_stok = 't' AND a.id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jum_masuk = $hasilrow->jum_masuk;
				if ($jum_masuk == '')
					$jum_masuk = 0;

				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi,
							a.kode_brg, a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$kode_brg = $hasilnya->kode_brg;
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;

					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$jum_masuk_konv = $jum_masuk*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$jum_masuk_konv = $jum_masuk/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$jum_masuk_konv = $jum_masuk+$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$jum_masuk_konv = $jum_masuk-$angka_faktor_konversi;
						}
						else
							$jum_masuk_konv = $jum_masuk;
					}
					else
						$jum_masuk_konv = $jum_masuk;
				}
				else
					$jum_masuk_konv = $jum_masuk;
			}
			else
				$jum_masuk_konv = 0;

			// 1.2. ambil brg masuk hasil pembelian manual
			$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmmasukmanual a
									INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
									INNER JOIN tm_barang c ON c.id = b.id_brg
									WHERE a.tgl_bonm > '$tgl_so' AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jum_masukmanual = $hasilrow->jum_masuk;
				if ($jum_masukmanual == '')
					$jum_masukmanual = 0;

				if ($id_satuan_konversi != 0) {
					if ($rumus_konversi == '1') {
						$jum_masukmanual_konv = $jum_masukmanual*$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '2') {
						$jum_masukmanual_konv = $jum_masukmanual/$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '3') {
						$jum_masukmanual_konv = $jum_masukmanual+$angka_faktor_konversi;
					}
					else if ($rumus_konversi == '4') {
						$jum_masukmanual_konv = $jum_masukmanual-$angka_faktor_konversi;
					}
					else
						$jum_masukmanual_konv = $jum_masukmanual;
				}
				else
					$jum_masukmanual_konv = $jum_masukmanual;
			}
			else
				$jum_masukmanual_konv = 0;

			// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
			$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									WHERE  a.tgl_bonm > '$tgl_so'
									AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang'
									AND b.is_quilting = 'f' AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan <> 'DIBEBANKAN') ");

				// 05-09-2015 DIGANTI KARENA GA AKURAT. GANTINYA PAKE YG DARI KALKULASI ULANG STOK/APP-STOK-OPNAME (DIATAS)
				/*$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE  a.tgl_bonm > '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND a.tujuan < '3' "); */
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;

				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				// 05-09-2015 DIGANTI
				/*$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE a.tgl_bonm >= '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' AND (a.tujuan > '2' OR a.keterangan = 'DIBEBANKAN') "); */

				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
									INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
									WHERE a.tgl_bonm > '$tgl_so'
									AND b.id_brg = '$id_brg'
									AND a.id_gudang = '$gudang'
									AND b.is_quilting = 'f' AND ((a.tujuan >= '3' AND a.tujuan <= '5') OR a.keterangan = 'DIBEBANKAN' ) ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;

				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							WHERE a.tgl_bonm  > '$tgl_so'
							AND b.id_brg = '$id_brg'
							AND a.id_gudang = '$gudang'
							AND b.is_quilting = 'f' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;

				$totmasuk = $jum_masuk_konv + $jum_masukmanual_konv+ $jum_masuk_lain;
				$totkeluar = $jum_keluar+$jum_keluar_lain;

				$jum_stok_akhir = $totmasuk-$totkeluar;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------

				// tambahkan stok_akhir ini ke qty SO
				$totalxx = $totalxx+$jum_stok_akhir;

		//cek stok terakhir tm_stok, dan update stoknya stoknya konversikan balik ke sat awal

				$query3	= $this->db->query(" SELECT a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi,
							a.kode_brg, a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$kode_brg = $hasilnya->kode_brg;
					$nama_brg = $hasilnya->nama_brg;
					$id_satuan_konversi = $hasilnya->id_satuan_konversi;
					$rumus_konversi = $hasilnya->rumus_konversi;
					$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;

					if ($id_satuan_konversi != 0) {
						if ($rumus_konversi == '1') {
							$totalxx_konvawal = $totalxx/$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '2') {
							$totalxx_konvawal = $totalxx*$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '3') {
							$totalxx_konvawal = $totalxx-$angka_faktor_konversi;
						}
						else if ($rumus_konversi == '4') {
							$totalxx_konvawal = $totalxx+$angka_faktor_konversi;
						}
						else
							$totalxx_konvawal = $totalxx;
					}
					else
						$totalxx_konvawal = $totalxx;
				}
				else
					$totalxx_konvawal = $totalxx;

				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok WHERE id_brg = '$id_brg' ");
				if ($query3->num_rows() == 0){
					$data_stok = array(
							'id_brg'=>$id_brg,
							'stok'=>$totalxx_konvawal,
							'tgl_update_stok'=>$tgl
							);
					$this->db->insert('tm_stok', $data_stok);
				}
				else {
					$hasilrow = $query3->row();
					$id_stok	= $hasilrow->id;

					$this->db->query(" UPDATE tm_stok SET stok = '$totalxx_konvawal', tgl_update_stok = '$tgl'
					where id_brg= '$id_brg' ");
				}
		// ==================================================================================================

		if ($iddetail == '0') {
			$databaru = array(
						'id_stok_opname_bahan_baku'=>$id_so,
						'id_brg'=>$id_brg,
						'stok_awal'=>$stok,
						'jum_stok_opname'=>$stok_fisik,
						'status_approve'=> 't'
					);
		   $this->db->insert('tt_stok_opname_bahan_baku_detail',$databaru);
		}
		else {
			$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail SET jum_stok_opname = '$stok_fisik',
						auto_saldo_akhir='0', status_approve='t' WHERE id_brg = '$id_brg' AND id_stok_opname_bahan_baku='$id_so' ");
		}
		// 25-11-2014, 27-05-2015 dikomen
		/*for ($xx=0; $xx<count($harga); $xx++) {
			$totalxx = $stok_fisik[$xx];

			$this->db->query(" UPDATE tt_stok_opname_bahan_baku_detail_harga SET jum_stok_opname = '".$totalxx."',
							auto_saldo_akhir='".$auto_saldo_akhir_harga[$xx]."'
							WHERE id_stok_opname_bahan_baku_detail='$iddetail' AND harga = '".$harga[$xx]."' ");
		} */
		// ====================
  }

  // 06-11-2014
  function get_mutasi_stok_quilting($date_from, $date_to) {
		// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}

			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

		$sql = "SELECT a.kode_brg, a.harga FROM (
				SELECT b.kode_brg_makloon as kode_brg, b.harga FROM tm_sj_hasil_makloon a
				INNER JOIN tm_sj_hasil_makloon_detail b ON a.id = b.id_sj_hasil_makloon
				WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_sj <=to_date('$date_to','dd-mm-yyyy') AND b.status_stok='t' GROUP BY b.kode_brg_makloon, b.harga

				UNION SELECT b.kode_brg, c.harga FROM tm_bonmmasuklain a
				INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
				INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
				WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.is_quilting='t'
				GROUP BY b.kode_brg, c.harga

				UNION SELECT b.kode_brg, c.harga FROM tm_bonmkeluar a
				INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
				INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
				WHERE a.tgl_bonm >=to_date('$date_from','dd-mm-yyyy')
				AND a.tgl_bonm <=to_date('$date_to','dd-mm-yyyy') AND b.is_quilting='t'
				GROUP BY b.kode_brg, c.harga

				UNION SELECT b.kode_brg, c.harga FROM tt_stok_opname_hasil_quilting a
					INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id=b.id_stok_opname_hasil_quilting
					INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id=c.id_stok_opname_hasil_quilting_detail
					WHERE a.bulan='$bln_query' AND a.tahun='$thn_query'
					AND b.jum_stok_opname <> 0
					AND a.status_approve='t' GROUP BY b.kode_brg, c.harga
				)
				a GROUP BY a.kode_brg, a.harga ORDER BY a.kode_brg, a.harga";
	  //echo $sql; die();
		$query	= $this->db->query($sql);

		$jum_keluar = 0;
		$jum_keluar_lain = 0;
		$jum_masuk = 0;
		$data_stok = array();

		if ($query->num_rows() > 0){
			$hasil = $query->result();

			// explode date_from utk keperluan saldo awal
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}

			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

			foreach ($hasil as $row1) {
				if ($row1->harga == '')
					$row1->harga = 0;
				// 12-07-2013, ambil data saldo awal dari SO bulan sebelumnya (ambil bulan dan tahun dari date_from)

				$queryx	= $this->db->query(" SELECT c.jum_stok_opname FROM tt_stok_opname_hasil_quilting a
									INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id = b.id_stok_opname_hasil_quilting
									INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id = c.id_stok_opname_hasil_quilting_detail
									WHERE b.kode_brg = '$row1->kode_brg'
									AND c.harga = '$row1->harga'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");

				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->jum_stok_opname;
					if ($saldo_awal == '')
						$saldo_awal = 0;
				}
				else
					$saldo_awal = 0;

				$saldo_awal_rp = $saldo_awal*$row1->harga;

				// 29-10-2014: ambil brg masuk hasil pembelian quilting
				$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a
							INNER JOIN tm_sj_hasil_makloon_detail b ON a.id = b.id_sj_hasil_makloon
							WHERE a.tgl_sj >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_makloon = '$row1->kode_brg'
							AND b.status_stok = 't'
							AND b.harga = '$row1->harga' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
					if ($jum_masuk == '')
						$jum_masuk = 0;
				}
				else
					$jum_masuk = 0;

				$jum_masuk_konv = $jum_masuk;
				$jum_masuk_konv_rp = $jum_masuk_konv*$row1->harga;

				// ambil nama satuan
				$query3	= $this->db->query(" SELECT a.satuan, a.nama_brg, b.nama as nama_satuan FROM tm_brg_hasil_makloon a
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.kode_brg = '$row1->kode_brg' ");
				if ($query3->num_rows() > 0){
					$hasilnya = $query3->row();
					$satuan = $hasilnya->nama_satuan;
					$id_satuan = $hasilnya->satuan;
					$nama_brg = $hasilnya->nama_brg;
				}
				else {
					$satuan = '';
					$id_satuan = 0;
					$nama_brg = '';
				}
				// ---------------------------------------------------------------------------------------------

				// 2. hitung brg keluar bagus dari tm_bonmkeluar
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE  a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND c.harga = '$row1->harga'
							AND b.is_quilting = 't' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
					if ($jum_keluar == '')
						$jum_keluar = 0;
				}
				else
					$jum_keluar = 0;

				$jum_keluar_rp = $jum_keluar*$row1->harga;

				// 3. hitung brg keluar lain dari tm_bonmkeluar
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND c.harga = '$row1->harga'
							AND b.is_quilting = 't' AND a.tujuan > '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
					if ($jum_keluar_lain == '')
						$jum_keluar_lain = 0;
				}
				else
					$jum_keluar_lain = 0;

				$jum_keluar_lain_rp = $jum_keluar_lain*$row1->harga;

				// 4. hitung brg masuk lain dari tm_bonmmasuklain
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
							WHERE a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')
							AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg = '$row1->kode_brg'
							AND c.harga = '$row1->harga'
							AND b.is_quilting = 't' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
					if ($jum_masuk_lain == '')
						$jum_masuk_lain = 0;
				}
				else
					$jum_masuk_lain = 0;

				$jum_masuk_lain_rp = $jum_masuk_lain*$row1->harga;

				//==================================== 30-10-2014 STOK OPNAME ==========================================
				$query3	= $this->db->query(" SELECT c.jum_stok_opname FROM tt_stok_opname_hasil_quilting a
									INNER JOIN tt_stok_opname_hasil_quilting_detail b ON a.id = b.id_stok_opname_hasil_quilting
									INNER JOIN tt_stok_opname_hasil_quilting_detail_harga c ON b.id = c.id_stok_opname_hasil_quilting_detail
									WHERE b.kode_brg = '$row1->kode_brg'
									AND c.harga = '$row1->harga'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't'
									AND a.status_approve = 't' ");

				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_stok_opname = $hasilrow->jum_stok_opname;
					if ($jum_stok_opname == '')
						$jum_stok_opname = 0;

				}
				else {
					$jum_stok_opname = 0;
				}
				$jum_stok_opname_rp = $jum_stok_opname*$row1->harga;
				//--------------------------------------

				/*$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama
												FROM tm_gudang a WHERE a.kode_gudang = '$row1->kode_gudang' ");

				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_gudang	= $hasilrow->nama;
				}
				else
					$nama_gudang = ''; */

				$data_stok[] = array(		'kode_brg'=> $row1->kode_brg,
											'harga'=> $row1->harga,
											//'kode_gudang'=> $row1->kode_gudang,
											//'nama_gudang'=> $nama_gudang,
											'nama_brg'=> $nama_brg,
											'id_satuan'=> $id_satuan,
											'satuan'=> $satuan,
											'saldo_awal'=> $saldo_awal,
											'saldo_awal_rp'=> $saldo_awal_rp,
											'jum_masuk'=> $jum_masuk_konv,
											'jum_masuk_rp'=> $jum_masuk_konv_rp,
											'jum_masuk_lain'=> $jum_masuk_lain,
											'jum_masuk_lain_rp'=> $jum_masuk_lain_rp,
											'jum_keluar'=> $jum_keluar,
											'jum_keluar_rp'=> $jum_keluar_rp,
											'jum_keluar_lain'=> $jum_keluar_lain,
											'jum_keluar_lain_rp'=> $jum_keluar_lain_rp,
											'jum_stok_opname'=> $jum_stok_opname,
											'jum_stok_opname_rp'=> $jum_stok_opname_rp
											);
			} // endforeach header
		}
		else {
			$data_stok = '';
		}
		return $data_stok;
  }

  // 26-08-2015 untuk di form input SO berdasarkan harga
  function get_all_stok_opname_bahanbaku_forharga($bulan, $tahun, $gudang) {
		// ambil tgl terakhir di bln tsb
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month

		// 04-03-2015, ambil data bulan lalu
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}

		$query	= $this->db->query(" SELECT a.id as id_header, a.tgl_so, b.*, c.kode_brg, c.nama_brg, c.satuan,
					c.id_satuan_konversi, c.rumus_konversi, c.angka_faktor_konversi
					FROM tt_stok_opname_bahan_baku_detail b
					INNER JOIN tt_stok_opname_bahan_baku a ON b.id_stok_opname_bahan_baku = a.id
					INNER JOIN tm_barang c ON b.id_brg = c.id
					WHERE a.id_gudang = '$gudang' AND a.bulan = '$bulan'
					AND a.tahun = '$tahun' AND a.status_approve = 't' AND b.status_approve = 't'
					 ORDER BY c.nama_brg ");
		if ($query->num_rows() > 0){
			$hasil = $query->result();

			$detail_bahan = array();
			foreach ($hasil as $row) {
				// 01-11-2014
				if ($row->id_satuan_konversi != '0') {
					$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->id_satuan_konversi' ");
					if ($query3->num_rows() > 0){
						$hasilrow = $query3->row();
						$nama_satuan_konv	= $hasilrow->nama;
					}
					else {
						$nama_satuan_konv = "Tidak Ada";
					}
				}
				else
					$nama_satuan_konv = "Tidak Ada";

				$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id= '$row->satuan' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_satuan	= $hasilrow->nama;
				}
				else {
					$nama_satuan = "";
				}

				// ambil data detail harga
				$sqlxx = " SELECT id, harga, jum_stok_opname, is_harga_pkp FROM tt_stok_opname_bahan_baku_detail_harga
						WHERE id_stok_opname_bahan_baku_detail = '$row->id' ORDER BY id "; //echo $sqlxx; die();
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();

					$detail_harga = array();
					foreach ($hasilxx as $rowxx) {
						$detail_harga[] = array(
											'id'=> $rowxx->id,
											'harga'=> $rowxx->harga,
											'jum_stok_opname'=> $rowxx->jum_stok_opname,
											'is_harga_pkp'=> $rowxx->is_harga_pkp
										);
					} // end for2
				}
				else
					$detail_harga = '';

				$detail_bahan[] = array('id_header'=> $row->id_header,
										'tgl_so'=> $row->tgl_so,
										'id'=> $row->id,
										'id_brg'=> $row->id_brg,
										'kode_brg'=> $row->kode_brg,
										'nama_brg'=> $row->nama_brg,
										'nama_satuan'=> $nama_satuan,
										'nama_satuan_konv'=> $nama_satuan_konv,
										'stok_opname'=> $row->jum_stok_opname,
										'detail_harga'=> $detail_harga
									);
				$detail_harga = array();
			}
		}
		else {
			$detail_bahan = '';
		}

		return $detail_bahan;
  }

  function savesoharga($iddetail, $harga, $is_pkp, $stok_harga){
	  $tgl = date("Y-m-d H:i:s");
	  if ($is_pkp == '')
		$is_pkp = 'f';

	  $data_detail = array(
						'id_stok_opname_bahan_baku_detail'=>$iddetail,
						'harga'=>$harga,
						'jum_stok_opname'=>$stok_harga,
						'is_harga_pkp'=>$is_pkp
					);
	  $this->db->insert('tt_stok_opname_bahan_baku_detail_harga',$data_detail);

	  // ambil id_brg, id_satuan, id_satuan_konversi
		$query3	= $this->db->query(" SELECT a.id, a.satuan, a.id_satuan_konversi FROM tm_barang a
						INNER JOIN tt_stok_opname_bahan_baku_detail b ON a.id = b.id_brg
						WHERE b.id = '$iddetail' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_satuan	= $hasilrow->satuan;
			$id_satuan_konversi	= $hasilrow->id_satuan_konversi;
			$id_brg	= $hasilrow->id;

			// 10-10-2015, cek ke tm_stok_harga apakah udh ada id_brg tsb
				//~ $sqlxx = " SELECT id, harga, stok FROM tm_stok_harga WHERE id_brg = '$id_brg' AND harga = '$harga'
						//~ AND is_harga_pkp = '$is_pkp' AND id_satuan = '$id_satuan' AND id_satuan_konversi = '$id_satuan_konversi' ";
				//~ $queryxx	= $this->db->query($sqlxx);
					$queryxx	= $this->db->query(" SELECT id FROM tm_stok_harga WHERE id_brg = '$id_brg'
															AND harga = '$harga' AND id_satuan = '$id_satuan'
															AND is_harga_pkp = '$pkp' ");
				if ($queryxx->num_rows() == 0){
					$data_stok_harga = array(
									'id_brg'=>$id_brg,
									'stok'=>0,
									'harga'=>$harga,
									'id_satuan'=>$id_satuan,
									'id_satuan_konversi'=>$id_satuan_konversi,
									'tgl_update_stok'=>$tgl,
									'is_harga_pkp'=>$is_pkp
									);
					$this->db->insert('tm_stok_harga', $data_stok_harga);
				}
		}
  }

  public function getdata($date_from, $date_to, $gudang){
  	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$iperiode = $thn1.$bln1;
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10){
			$bln_query = "0".$bln_query;
		}
	}

	$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

	$dtoback3 = date('d-m-Y',strtotime('-1 month', strtotime($date_from)));
	$dtoback1 = date('d-m-Y',strtotime('-2 month', strtotime($date_from)));
	$dtoback4 = date('d-m-Y',strtotime('-2 month', strtotime($date_to)));
		$bln2= substr($dtoback1, 3,2);
		$thn2= substr($dtoback1, 6,4);//t
	$dto1 = date('d-m-Y',strtotime($date_to."last day of previous month"));
		$bln4= substr($dto1, 3,2);
		$thn4= substr($dto1, 6,4);
  	if ($iperiode<='202001') {
  	$this->db->select("
  		id_brg, kode, barang, gudang, satuan, rumus_konversi, angka_faktor_konversi, saldoawal, bonmasuk1, bonmasuklain, bonkeluar, bonkeluarlain, saldoakhir, so, selisih
  		FROM f_saldoso('$bln_query', $thn_query, '$date_from','$date_to', '$bln1', $thn1, '$dto1')
		WHERE id_gudang='$gudang' order by barang
  	",FALSE);
  	}else if ($iperiode<='202002'){	
  	$this->db->select("
  		id_brg,  kode, barang, gudang, satuan, rumus_konversi, angka_faktor_konversi,  saldoawal, bonmasuk1, bonmasuklain, bonkeluar, bonkeluarlain, saldoakhir, so, selisih
  		FROM f_saldo('$bln1', $thn1, '$date_from', '$date_to', '$bln1',$thn1,'$bln2',$thn2, '$dtoback3', '$dto1', '$bln4',$thn4, '$dtoback4')
		WHERE id_gudang='$gudang' order by barang
  	",FALSE);
  	}else if ($iperiode >='202102'){
  	$this->db->select("
  		id_brg,  kode, barang, gudang, satuan, rumus_konversi, angka_faktor_konversi,  saldoawal, bonmasuk1, bonmasuklain, bonkeluar, bonkeluarlain, saldoakhir, so, selisih
  		FROM f_saldobaru('$bln1', $thn1, '$date_from', '$date_to', '$bln1',$thn1,'$bln4',$thn1, '$dtoback3', '$dto1', '$bln4',$thn4, '$dtoback4')
		WHERE id_gudang='$gudang' order by barang
  	",FALSE);	
  	}else{
  	$this->db->select("
  		id_brg,  kode, barang, gudang, satuan, rumus_konversi, angka_faktor_konversi,  saldoawal, bonmasuk1, bonmasuklain, bonkeluar, bonkeluarlain, saldoakhir, so, selisih
  		FROM f_saldobaru('$bln1', $thn1, '$date_from', '$date_to', '$bln1',$thn1,'$bln4',$thn2, '$dtoback3', '$dto1', '$bln4',$thn4, '$dtoback4')
		WHERE id_gudang='$gudang' order by barang
  	",FALSE);	
  	}	
  	return $this->db->get();
  }

  public function updatesaldodetail($year, $month, $id_brg, $saldoawal, $id_gudang){
        $query	= $this->db->query(" 
        						select *
								from tt_stok_opname_bahan_baku 
							        where bulan='$month'
							        and tahun='$year'
							        and id_gudang = '$id_gudang'", false);
  
        if ($query->num_rows() > 0){
        	$hasil = $query->row();
        	$id = $hasil->id;

        }else{
        	$id = '';
        }
        $data = array(
        	'id_brg'     		=> $id_brg,
        	'auto_saldo_akhir'  => $saldoawal, 
        );
        $this->db->where('id_brg', $id_brg);
        $this->db->where('id_stok_opname_bahan_baku' , $id);
        $this->db->update('tt_stok_opname_bahan_baku_detail', $data);
    }

    public function getdataharga($date_from, $date_to, $gudang){
  	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$iperiode = $thn1.$bln1;
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10){
			$bln_query = "0".$bln_query;
		}
	}

	$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];

	$dtoback3 = date('d-m-Y',strtotime('-1 month', strtotime($date_from)));
	$dtoback1 = date('d-m-Y',strtotime('-2 month', strtotime($date_from)));
	$dtoback4 = date('d-m-Y',strtotime('-2 month', strtotime($date_to)));
		$bln2= substr($dtoback1, 3,2);
		$thn2= substr($dtoback1, 6,4);//t
	$dto1 = date('d-m-Y',strtotime($date_to."last day of previous month"));
		$bln4= substr($dto1, 3,2);
		$thn4= substr($dto1, 6,4);
  	if ($iperiode<='202001') {
  	$this->db->select("
  		id_brg, kode, barang, gudang, satuan, rumus_konversi, angka_faktor_konversi, saldoawal, bonmasuk1, bonmasuklain, bonkeluar, bonkeluarlain, saldoakhir, so, selisih
  		FROM f_saldoso('$bln_query', $thn_query, '$date_from','$date_to', '$bln1', $thn1, '$dto1')
		WHERE id_gudang='$gudang' order by barang
  	",FALSE);
  	}else if ($iperiode<='202002'){	
  	$this->db->select("
  		id_brg,  kode, barang, gudang, satuan, rumus_konversi, angka_faktor_konversi,  saldoawal, bonmasuk1, bonmasuklain, bonkeluar, bonkeluarlain, saldoakhir, so, selisih
  		FROM f_saldo('$bln1', $thn1, '$date_from', '$date_to', '$bln1',$thn1,'$bln2',$thn2, '$dtoback3', '$dto1', '$bln4',$thn4, '$dtoback4')
		WHERE id_gudang='$gudang' order by barang
  	",FALSE);
  	}else{
  	$this->db->select("
  		id_brg,  kode, barang, gudang, satuan, rumus_konversi, angka_faktor_konversi,  saldoawal, bonmasuk1, bonmasuklain, bonkeluar, bonkeluarlain, saldoakhir, so, selisih
  		FROM f_saldobaru('$bln1', $thn1, '$date_from', '$date_to', '$bln1',$thn1,'$bln4',$thn2, '$dtoback3', '$dto1', '$bln4',$thn4, '$dtoback4')
		WHERE id_gudang='$gudang' order by barang
  	",FALSE);	
  	}	
  	return $this->db->get();
  }
}

