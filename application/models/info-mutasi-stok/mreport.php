<?php
class Mreport extends CI_Model{
  public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
  
  function get_gudang(){
if($this->session->userdata('gid') == 16){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
				                        WHERE a.jenis = '1' and a.id=16 ORDER BY a.kode_lokasi, a.kode_gudang");
    }else{
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
				                        FROM tm_gudang a INNER JOIN tm_lokasi_gudang b ON a.id_lokasi = b.id 
				                        WHERE a.jenis = '1' ORDER BY a.kode_lokasi, a.kode_gudang");
				                        }
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bahan($num, $offset, $cari, $id_gudang)
  {
	if ($cari == "all") {
		$sql = " a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id 
					WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		
		$this->db->select($sql, false)->limit($num,$offset);
	}
	else {
			$sql = "a.*, b.nama as nama_satuan 
				FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id 
							WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";			
			$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
					order by a.nama_brg";
							
		$this->db->select($sql, false)->limit($num,$offset);	
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){ 
		$hasil = $query->result();
		foreach ($hasil as $row1) {			
			$tabelstok = "tm_stok";
				
			// 29-10-2014
			$id_satuan_konversi = $row1->id_satuan_konversi;
			$rumus_konversi = $row1->rumus_konversi;
			$angka_faktor_konversi = $row1->angka_faktor_konversi;

			$query3	= $this->db->query(" SELECT sum(stok) as jstok FROM ".$tabelstok." WHERE id_brg = '$row1->id' ");
			$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$jum_stok	= $hasilrow->jstok;
			else
				$jum_stok = 0;

			$query3	= $this->db->query(" SELECT nama FROM tm_satuan WHERE id = '$row1->satuan' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$satuan	= $hasilrow->nama;
			}
						
			$data_bhn[] = array(		'id_brg'=> $row1->id,	
										'kode_brg'=> $row1->kode_brg,
										'nama_brg'=> $row1->nama_brg,
										'satuan'=> $satuan,
										// 29-10-2014
										'id_satuan_konversi'=> $id_satuan_konversi,
										'rumus_konversi'=> $rumus_konversi,
										'angka_faktor_konversi'=> $angka_faktor_konversi,
										//------------
										'tgl_update'=> $row1->tgl_update,
										'jum_stok'=> $jum_stok
								);
		}
	}
	else
		$data_bhn = '';
	return $data_bhn;	
  }
  
  function get_bahantanpalimit($cari, $id_gudang){
	if ($cari == "all") {
		$sql = " select a.*, b.nama as nama_satuan FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.status_aktif = 't' 
					AND a.id_gudang = '$id_gudang' ORDER BY a.nama_brg ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
	}
	else {
		$sql = " select a.*, b.nama as nama_satuan 
			FROM tm_barang a INNER JOIN tm_satuan b ON a.satuan = b.id 
						WHERE a.status_aktif = 't' AND a.id_gudang = '$id_gudang' ";
		$sql.=" AND (UPPER(a.kode_brg) like UPPER('%$cari%') OR UPPER(a.nama_brg) like UPPER('%$cari%')) 
				order by a.nama_brg ";
		
		$query	= $this->db->query($sql);
		return $query->result();  
		//return $this->db->query($sql);
	}
  }
  
  // 19-05-2015
  function get_transaksi_bhnbaku($bulan, $tahun, $id_gudang, $id_brg) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// NEW
		$saldo = 0; $tot_masuk1 = 0; $tot_masuk2 = 0; $tot_keluar = 0;
								
		// ambil SO brg dari tt_stok_opname_bahan_baku (SO ini udah dalam satuan konversi)
		$sqlso = " SELECT b.jum_stok_opname 
							FROM tt_stok_opname_bahan_baku a INNER JOIN tt_stok_opname_bahan_baku_detail b
							ON a.id = b.id_stok_opname_bahan_baku
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg = '$id_brg' ";
		$queryso	= $this->db->query($sqlso);
		if ($queryso->num_rows() > 0){
			$hasilso = $queryso->row();
			$jum_stok_opname = $hasilso->jum_stok_opname;
			if ($jum_stok_opname == '')
				$jum_stok_opname = 0;
		}
		else
			$jum_stok_opname = 0;
		
		$saldo = $jum_stok_opname;
		
		// 01-08-2015
		$query3	= $this->db->query(" SELECT a.satuan, a.id_satuan_konversi, a.rumus_konversi, a.angka_faktor_konversi, 
							a.kode_brg, a.nama_brg, b.nama as nama_satuan_konv FROM tm_barang a 
							LEFT JOIN tm_satuan b ON a.id_satuan_konversi = b.id WHERE a.id = '$id_brg' ");
		if ($query3->num_rows() > 0){
			$hasilnya = $query3->row();
			$kode_brg = $hasilnya->kode_brg;
			$nama_brg = $hasilnya->nama_brg;
			$id_satuan = $hasilnya->satuan;
			$id_satuan_konversi = $hasilnya->id_satuan_konversi;
			$rumus_konversi = $hasilnya->rumus_konversi;
			$angka_faktor_konversi = $hasilnya->angka_faktor_konversi;
			$nama_satuan_konv = $hasilnya->nama_satuan_konv;
			
			if ($nama_satuan_konv == '')
				$nama_satuan_konv = "Tidak Ada";
		}
		
		// 04-08-2015, ambil nama satuan awal
		$query3	= $this->db->query(" SELECT b.nama as nama_satuan FROM tm_barang a 
							INNER JOIN tm_satuan b ON a.satuan = b.id WHERE a.id = '$id_brg' ");
		if ($query3->num_rows() > 0){
			$hasilnya = $query3->row();
			$nama_satuan = $hasilnya->nama_satuan;
		}
		
		
		// QUERY BARANG MASUK DAN KELUAR DI BULAN TERTENTU
		// 01-07-2015 QUILTING GA DIPAKE
		//if ($is_quilting == '') {
		$sql2 = " SELECT distinct a.no_sj, a.id_supplier, b.no_bonm, '0' as no_manual, b.tgl_bonm, sum(b.qty) as qty_satawal, 0 as qty_konversi, 1 as masuk, 0 as keluar, 0 as masuklain, 0 as keluarlain
					FROM tm_apply_stok_pembelian a INNER JOIN tm_apply_stok_pembelian_detail b ON a.id = b.id_apply_stok
					
					WHERE b.tgl_bonm >='".$tahun."-".$bulan."-01' AND b.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
				 AND b.id_brg='$id_brg' AND b.status_stok='t' AND a.status_aktif='t'
				 GROUP BY a.no_sj, a.id_supplier, b.no_bonm, b.tgl_bonm, no_manual 
				 
				UNION
					SELECT distinct '0' as no_sj, 0 as id_supplier, a.no_bonm, '0' as no_manual, a.tgl_bonm, sum(b.qty) as qty_satawal, 0 as qty_konversi, 1 as masuk, 0 as keluar, 0 as masuklain, 0 as keluarlain
					FROM tm_bonmmasukmanual a INNER JOIN tm_bonmmasukmanual_detail b ON a.id = b.id_bonmmasukmanual
					WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
					AND b.id_brg='$id_brg' GROUP BY no_sj, id_supplier, no_bonm, tgl_bonm, no_manual
		";
		//}
/*		else {
			$sql2 = " SELECT distinct a.no_sj as no_bonm, '0' as no_manual, a.tgl_sj as tgl_bonm, sum(b.qty_makloon) as qty_satawal, 1 as masuk, 0 as keluar, 0 as masuklain, 0 as keluarlain
						FROM tm_sj_hasil_makloon a INNER JOIN tm_sj_hasil_makloon_detail b ON a.id = b.id_sj_hasil_makloon
						WHERE 
						 a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_makloon='$kode_brg' AND b.status_stok='t' GROUP BY no_bonm, tgl_bonm, no_manual  ";
		} */
		
		$sql2.= " UNION
						SELECT distinct '0' as no_sj, 0 as id_supplier, a.no_bonm, a.no_manual, a.tgl_bonm, sum(b.qty_satawal) as qty_satawal, sum(b.qty) as qty_konversi, 0 as masuk, 1 as keluar, 0 as masuklain, 0 as keluarlain
						FROM tm_bonmkeluar a INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
						WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.id_brg='$id_brg' AND ((a.tujuan < '3' OR a.tujuan > '5') AND a.keterangan <> 'DIBEBANKAN') GROUP BY no_sj, id_supplier, no_bonm, tgl_bonm, no_manual
				
				UNION
						SELECT distinct '0' as no_sj, 0 as id_supplier, a.no_bonm, a.no_manual, a.tgl_bonm, sum(b.qty_satawal) as qty_satawal, sum(b.qty) as qty_konversi, 0 as masuk, 0 as keluar, 0 as masuklain, 1 as keluarlain
						FROM tm_bonmkeluar a INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
						WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.id_brg='$id_brg' AND ((a.tujuan >= '3' AND a.tujuan <= '5') OR a.keterangan = 'DIBEBANKAN') GROUP BY no_sj, id_supplier, no_bonm, tgl_bonm, no_manual
						
						UNION
						SELECT distinct '0' as no_sj, 0 as id_supplier, a.no_bonm, a.no_manual, a.tgl_bonm, sum(b.qty_satawal) as qty_satawal, sum(b.qty) as qty_konversi, 0 as masuk, 0 as keluar, 1 as masuklain, 0 as keluarlain
						FROM tm_bonmmasuklain a INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
						WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.id_brg='$id_brg' GROUP BY no_sj, id_supplier, no_bonm, tgl_bonm, no_manual
						ORDER BY tgl_bonm ASC "; //echo $sql2;
		
		$query2	= $this->db->query($sql2);
		$data_tabel1 = array();
		if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$no_sj = $row2->no_sj;
						$id_supplier = $row2->id_supplier;
						$no_bonm = $row2->no_bonm;
						$no_manual = $row2->no_manual;
						$tgl_bonm = $row2->tgl_bonm;
						$qty_satawal = $row2->qty_satawal;
						$qty_konversi = $row2->qty_konversi;
						$is_masuk = $row2->masuk;
						$is_keluar = $row2->keluar;
						$is_masuklain = $row2->masuklain;
						$is_keluarlain = $row2->keluarlain;
						
						// 04-08-2015
						if ($qty_konversi == '0') {
							if ($id_satuan_konversi != 0) {
								if ($rumus_konversi == '1') {
									$qty_konversi = $qty_satawal*$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '2') {
									$qty_konversi = $qty_satawal/$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '3') {
									$qty_konversi = $qty_satawal+$angka_faktor_konversi;
								}
								else if ($rumus_konversi == '4') {
									$qty_konversi = $qty_satawal-$angka_faktor_konversi;
								}
								else
									$qty_konversi = $qty_satawal;
							}
							else
								$qty_konversi = $qty_satawal;
						}
												
						$pisah1 = explode("-", $tgl_bonm);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_bonm = $tgl1."-".$bln1."-".$thn1;
						
						if ($is_masuk == '1' || $is_masuklain == '1') {
							$masuk = "ya";
							$keluar = "tidak";
							//$saldo+= $qty_satawal;
							$saldo+= $qty_konversi;
						}
						else if ($is_keluar == '1' || $is_keluarlain == '1') {
							$masuk = "tidak";
							$keluar = "ya";
							//$saldo-= $qty_satawal;
							$saldo-= $qty_konversi;
						}
						
						if ($id_supplier != 0) {
							$query3	= $this->db->query(" SELECT kode_supplier, nama FROM tm_supplier WHERE id = '$id_supplier' ");
							$hasilrow = $query3->row();
							if ($query3->num_rows() != 0) {
								$kode_supplier	= $hasilrow->kode_supplier;
								$nama_supplier	= $hasilrow->nama;
							}
							else {
								$kode_supplier = '';
								$nama_supplier = "";
							}
							
							$query3	= $this->db->query(" SELECT tgl_sj FROM tm_pembelian WHERE no_sj = '$no_sj' AND id_supplier = '$id_supplier' ");
							$hasilrow = $query3->row();
							if ($query3->num_rows() != 0) {
								$tgl_sj	= $hasilrow->tgl_sj;
								
								$pisah1 = explode("-", $tgl_sj);
								$tgl1= $pisah1[2];
								$bln1= $pisah1[1];
								$thn1= $pisah1[0];
								$tgl_sj = $tgl1."-".$bln1."-".$thn1;
							}
							else {
								$tgl_sj = '';
							}
						}
						else {
							$kode_supplier = '';
							$nama_supplier = "";
							$tgl_sj = '';
						}
						
						$data_tabel1[] = array(	'no_bonm'=> $no_bonm,
												'tgl_bonm'=> $tgl_bonm,
												'no_manual'=> $no_manual,
												'is_masuk'=> $is_masuk,
												'is_keluar'=> $is_keluar,
												'is_masuklain'=> $is_masuklain,
												'is_keluarlain'=> $is_keluarlain,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'qty'=> $qty_konversi,
												'saldo'=> $saldo,
												// 04-08-2015
												'nama_satuan'=> $nama_satuan,
												'nama_satuan_konv'=> $nama_satuan_konv,
												// 08-08-2015
												'kode_supplier'=> $kode_supplier,
												'nama_supplier'=> $nama_supplier,
												'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj
										);
					} // end for2
		}
		else
			$data_tabel1='';
		//}
		
		return $data_tabel1;
		// ------- ================================================================= ----------
  }
  
// =====================================================================================================
  
  function cek_forecast($bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tm_forecast_wip WHERE bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_brgjadi() {
		$sql = " SELECT distinct b.i_product_motif, b.e_product_motifname FROM tr_product_base a, tr_product_motif b 
				WHERE a.i_product_base = b.i_product ORDER BY b.i_product_motif ";
		$query	= $this->db->query($sql);
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$databrgjadi = array();			
			foreach ($hasil as $row) {				
				$databrgjadi[] = array( 'id'=> '0',
										'kode_brg_jadi'=> $row->i_product_motif,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> '0',
										'keterangan'=> ''
									);
			}
		}
		else {
			$databrgjadi = '';
		}
		return $databrgjadi;
  }
  
  function get_forecast($bulan, $tahun) {
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_detail b, 
					tm_forecast_wip a, 
					tr_product_motif c
					WHERE b.id_forecast_wip = a.id 
					AND b.kode_brg_jadi = c.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY b.id ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function saveforecast($is_new, $id_fc, $iddetail, $kode_brg_jadi, $fc, $ket){ 
	  $tgl = date("Y-m-d H:i:s"); 
	 
	 // 02-04-2014, ga pake is_new lagi. langsung save aja setelah delete detail sebelumnya
	//  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailx	= $seqrow->id+1;
			}else{
				$iddetailx	= 1;
			}
		  
		   $data_detail = array(
						'id'=>$iddetailx,
						'id_forecast_wip'=>$id_fc,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'qty'=>$fc,
						'keterangan'=>$ket
					);
		   $this->db->insert('tm_forecast_wip_detail',$data_detail);
	/*  }
	  else {
		  $iddetailx = $iddetail;
		  
		  $this->db->query(" UPDATE tm_forecast_wip_detail SET qty = '$fc', keterangan='$ket'
						where id = '$iddetailx' ");
			// ====================
	  } */
  }
  
  // 15-03-2014
  function get_sowip($num, $offset, $gudang, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_hasil_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($gudang != '0')
		$sql.= " AND id_gudang = '$gudang' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di perusahaan ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di perusahaan ?";
								
				$data_so[] = array(		'id'=> $row1->id,	
										'bulan'=> $row1->bulan,	
										'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,	
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sowiptanpalimit($gudang, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_hasil_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($gudang != '0')
		$sql.= " AND id_gudang = '$gudang' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  function get_sowipunit($num, $offset, $kode_unit, $bulan, $tahun) {	
	$sql = " * FROM tt_stok_opname_unit_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($kode_unit != '0')
		$sql.= " AND kode_unit = '$kode_unit' ";
	$sql.= " ORDER BY tahun DESC, bulan DESC, kode_unit ";

	$this->db->select($sql, false)->limit($num,$offset);
	$query = $this->db->get();
		
	$data_so = array();
	if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {	
				$bln1 = $row1->bulan;								
				if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row1->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				if ($row1->status_approve == 't')
					$pesan = "Data SO ini sudah diapprove. Pengubahan data SO akan otomatis mengupdate stok terkini, sehingga stok terkini akan menggunakan data SO ini. Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				else
					$pesan = "Apakah anda yakin ingin mengubah data SO di unit jahit ini ?";
				$data_so[] = array(			'id'=> $row1->id,	
											'bulan'=> $row1->bulan,	
											'nama_bln'=> $nama_bln,	
											'tahun'=> $row1->tahun,
											'status_approve'=> $row1->status_approve,
											'pesan'=> $pesan,
											'tgl_update'=> $row1->tgl_update,
											'kode_unit'=> $row1->kode_unit,	
											'nama_unit'=> $nama_unit
											);
			} // endforeach header
	}
	else {
			$data_so = '';
	}
	return $data_so;
  }
  
  function get_sowipunittanpalimit($kode_unit, $bulan, $tahun){
	$sql = " select * FROM tt_stok_opname_unit_jahit WHERE TRUE ";
	
	if ($bulan != "00")
		$sql.= " AND bulan='$bulan' ";
	if ($tahun != "0")
		$sql.= " AND tahun='$tahun' ";
	if ($kode_unit != '0')
		$sql.= " AND kode_unit = '$kode_unit' ";
	  
	$query	= $this->db->query($sql);
    return $query->result();  
  }
  
  // 17-03-2014
  function get_sounitjahit($id_so) {

		$query	= $this->db->query(" SELECT a.id as id_header, a.kode_unit, a.bulan, a.tahun, a.tgl_so, b.*, 
					d.e_product_motifname as nama_brg_jadi 
					FROM tt_stok_opname_unit_jahit_detail b, 
					tt_stok_opname_unit_jahit a, tr_product_motif d
					WHERE b.id_stok_opname_unit_jahit = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.id = '$id_so' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
				
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT kode_unit, nama
								FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
				
				// ---------- 11-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
				$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir, b.auto_saldo_akhir_bagus, b.auto_saldo_akhir_perbaikan
									FROM tt_stok_opname_unit_jahit a INNER JOIN tt_stok_opname_unit_jahit_detail b ON 
									a.id = b.id_stok_opname_unit_jahit
									WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
					$saldo_awal_bagus = $hasilrow->auto_saldo_akhir_bagus;
					$saldo_awal_perbaikan = $hasilrow->auto_saldo_akhir_perbaikan;
				}
				else {
					$saldo_awal = 0;
					$saldo_awal_bagus = 0;
					$saldo_awal_perbaikan = 0;
				}
				
				// 2. hitung keluar bln ini
				// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus = 0;
				
				// 2.1.2 tm_sjmasukwip, jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_perbaikan = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_perbaikan = 0;
				
				// 2.2. dari tabel tm_sjmasukbhnbakupic
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
							tm_sjmasukbhnbakupic_detail b
							WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$row->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_bagus+= $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_bagus+= 0;
				
				// 3. hitung masuk bln ini
				// 3.1.1 dari tabel tm_sjkeluarwip. jenis bagus
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND a.jenis_keluar<>'3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus = 0;
				
				// 3.1.2 dari tabel tm_sjkeluarwip. jenis perbaikan
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit_jahit = '$row->kode_unit' AND a.jenis_keluar='3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_perbaikan = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_perbaikan = 0;
				
				// 3.2. dari tabel tm_bonmkeluarcutting
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
							tm_bonmkeluarcutting_detail b
							WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.kode_unit = '$row->kode_unit' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_bagus+= $hasilrow->jum_masuk;
				}
				else
					$jum_masuk_bagus+= 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal_bagus+$saldo_awal_perbaikan+$jum_masuk_bagus+$jum_masuk_perbaikan-$jum_keluar_bagus-$jum_keluar_perbaikan;
				$auto_saldo_akhir_bagus = $saldo_awal_bagus+$jum_masuk_bagus-$jum_keluar_bagus;
				$auto_saldo_akhir_perbaikan = $saldo_awal_perbaikan+$jum_masuk_perbaikan-$jum_keluar_perbaikan;
				//-------------------------------------------------------------------------------------
				
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_unit_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_unit_jahit_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_unit_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok, b.stok_bagus, b.stok_perbaikan
							FROM tm_stok_unit_jahit a, tm_stok_unit_jahit_warna b
							WHERE a.id = b.id_stok_unit_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ---------- 11-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir, c.auto_saldo_akhir_bagus, c.auto_saldo_akhir_perbaikan
											FROM tt_stok_opname_unit_jahit a 
											INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
											INNER JOIN tt_stok_opname_unit_jahit_detail_warna c ON b.id = c.id_stok_opname_unit_jahit_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
							$saldo_awal_bagus_warna = $hasilrow->auto_saldo_akhir_bagus;
							$saldo_awal_perbaikan_warna = $hasilrow->auto_saldo_akhir_perbaikan;
						}
						else {
							$saldo_awal_warna = 0;
							$saldo_awal_bagus_warna = 0;
							$saldo_awal_perbaikan_warna = 0;
						}
						
						// 2. hitung keluar bln ini
						// 2.1.1 dari tabel tm_sjmasukwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND (a.jenis_masuk='1' OR a.jenis_masuk='5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna = 0;
						
						// 2.1.2 dari tabel tm_sjmasukwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND (a.jenis_masuk<>'1' AND a.jenis_masuk<>'5') ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_perbaikan_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_perbaikan_warna = 0;
						
						// 2.2. dari tabel tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a 
									INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_bagus_warna+= $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_bagus_warna+= 0;
						
						// 3. hitung masuk bln ini
						// 3.1.1 hitung dari tm_sjkeluarwip. jenis bagus
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND a.jenis_keluar <>'3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna= 0;
						
						// 3.1.2 hitung dari tm_sjkeluarwip. jenis perbaikan
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."'
									AND a.jenis_keluar ='3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_perbaikan_warna= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_perbaikan_warna= 0;
						
						// 3.2. dari tabel tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									WHERE a.tgl_bonm >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_bonm <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit' AND c.kode_warna = '".$rowxx->kode_warna."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_bagus_warna+= $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_bagus_warna+= 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_bagus_warna+$saldo_awal_perbaikan_warna+$jum_masuk_bagus_warna+$jum_masuk_perbaikan_warna-$jum_keluar_bagus_warna-$jum_keluar_perbaikan_warna;
						$auto_saldo_akhir_bagus_warna = $saldo_awal_bagus_warna+$jum_masuk_bagus_warna-$jum_keluar_bagus_warna;
						$auto_saldo_akhir_perbaikan_warna = $saldo_awal_perbaikan_warna+$jum_masuk_perbaikan_warna-$jum_keluar_perbaikan_warna;
						
						//$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna;
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'stok_opname_bgs'=> $rowxx->jum_bagus,
									'stok_opname_perbaikan'=> $rowxx->jum_perbaikan,
									
									// 11-03-2015
									'auto_saldo_akhir_warna'=> $auto_saldo_akhir_warna,
									'auto_saldo_akhir_bagus_warna'=> $auto_saldo_akhir_bagus_warna,
									'auto_saldo_akhir_perbaikan_warna'=> $auto_saldo_akhir_perbaikan_warna
								);
					}
				}
				else
					$detail_warna = '';
				
				// 22-12-2014
				if ($row->tgl_so != '') {
					$pisah1 = explode("-", $row->tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
				}
				else
					$tgl_so = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'kode_unit'=> $row->kode_unit,
										'nama_unit'=> $nama_unit,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'tgl_so'=> $tgl_so,
										'nama_bln'=> $nama_bln,
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->nama_brg_jadi,
										'detail_warna'=> $detail_warna,
										// 11-03-2015
										'auto_saldo_akhir'=> $auto_saldo_akhir,
										'auto_saldo_akhir_bagus'=> $auto_saldo_akhir_bagus,
										'auto_saldo_akhir_perbaikan'=> $auto_saldo_akhir_perbaikan
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesounitjahit($id_so, $kode_brg_jadi, $kode_warna, $stok_fisik_bgs, $stok_fisik_perbaikan,
					$auto_saldo_akhir, $auto_saldo_akhir_warna, $auto_saldo_akhir_bagus, $auto_saldo_akhir_perbaikan,
					$auto_saldo_akhir_bagus_warna, $auto_saldo_akhir_perbaikan_warna){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisikbgs = 0;
		$qtytotalstokfisikperbaikan = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$stok_fisik_bgs[$xx] = trim($stok_fisik_bgs[$xx]);
			$stok_fisik_perbaikan[$xx] = trim($stok_fisik_perbaikan[$xx]);
			
			// 11-03-2015
			$auto_saldo_akhir_warna[$xx] = trim($auto_saldo_akhir_warna[$xx]);
			$auto_saldo_akhir_bagus_warna[$xx] = trim($auto_saldo_akhir_bagus_warna[$xx]);
			$auto_saldo_akhir_perbaikan_warna[$xx] = trim($auto_saldo_akhir_perbaikan_warna[$xx]);
			
			$qtytotalstokfisikbgs+= $stok_fisik_bgs[$xx];
			$qtytotalstokfisikperbaikan+= $stok_fisik_perbaikan[$xx];
		} // end for
	// ---------------------------------------------------------------------
		// 24-03-2014
		$totalxx = $qtytotalstokfisikbgs+$qtytotalstokfisikperbaikan;
			
		// ambil id detail id_stok_opname_unit_jahit_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_unit_jahit_detail 
					where kode_brg_jadi = '$kode_brg_jadi' 
					AND id_stok_opname_unit_jahit = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
		  
		 //if ($kode_brg_jadi == 'TPB220100') {
		//	echo $totalxx." ".$qtytotalstokfisikbgs."<br>";
			/*echo "UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$totalxx', status_approve='f' 
					where id = '$iddetail'"; die(); */
		 //}
		 $this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail SET jum_stok_opname = '$totalxx', 
					jum_bagus = '".$qtytotalstokfisikbgs."', jum_perbaikan = '".$qtytotalstokfisikperbaikan."',
					auto_saldo_akhir='$auto_saldo_akhir', auto_saldo_akhir_bagus='$auto_saldo_akhir_bagus', 
					auto_saldo_akhir_perbaikan='$auto_saldo_akhir_perbaikan',
					status_approve='f' 
					where id = '$iddetail' ");
			
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			// 24-03-2014
			$totalxx = $stok_fisik_bgs[$xx]+$stok_fisik_perbaikan[$xx];
			
			/*if ($kode_brg_jadi == 'TPB220100' && $kode_warna[$xx] == '10') {
				echo "detail=".$stok_fisik_bgs[$xx]." ".$stok_fisik_perbaikan[$xx]."<br>";
				echo " UPDATE tt_stok_opname_unit_jahit_detail_warna SET jum_stok_opname = '".$totalxx."',
						jum_bagus = '".$stok_fisik_bgs[$xx]."', jum_perbaikan = '".$stok_fisik_perbaikan[$xx]."'
						WHERE id_stok_opname_unit_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ";
				die(); 
			} */
			$this->db->query(" UPDATE tt_stok_opname_unit_jahit_detail_warna SET jum_stok_opname = '".$totalxx."',
						jum_bagus = '".$stok_fisik_bgs[$xx]."', jum_perbaikan = '".$stok_fisik_perbaikan[$xx]."',
						auto_saldo_akhir='".$auto_saldo_akhir_warna[$xx]."', auto_saldo_akhir_bagus='".$auto_saldo_akhir_bagus_warna[$xx]."',
						auto_saldo_akhir_perbaikan='".$auto_saldo_akhir_perbaikan_warna[$xx]."'
						WHERE id_stok_opname_unit_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ");
		}
		// ====================  
  }
  
  // 18-03-2014
  function get_sohasiljahit($id_so) {
		$query	= $this->db->query(" SELECT a.id as id_header, a.id_gudang, a.bulan, a.tahun, a.tgl_so, b.*, 
					d.e_product_motifname as nama_brg_jadi 
					FROM tt_stok_opname_hasil_jahit_detail b, 
					tt_stok_opname_hasil_jahit a, tr_product_motif d
					WHERE b.id_stok_opname_hasil_jahit = a.id 
					AND b.kode_brg_jadi = d.i_product_motif
					AND a.id = '$id_so' 
					ORDER BY b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$detail_bahan = array();			
			foreach ($hasil as $row) {
				// ambil tgl terakhir di bln tsb
				$timeStamp            =    mktime(0,0,0,$row->bulan,1,$row->tahun);    //Create time stamp of the first day from the give date.
				$firstDay            =     date('d',$timeStamp);    //get first day of the given month
				list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
				$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
				$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
				// 11-03-2015, ambil data bulan lalu
				if ($row->bulan == 1) {
					$bln_query = 12;
					$thn_query = $row->tahun-1;
				}
				else {
					$bln_query = $row->bulan-1;
					$thn_query = $row->tahun;
					if ($bln_query < 10)
						$bln_query = "0".$bln_query;
				}
		
				if ($row->bulan == '01')
						$nama_bln = "Januari";
					else if ($row->bulan == '02')
						$nama_bln = "Februari";
					else if ($row->bulan == '03')
						$nama_bln = "Maret";
					else if ($row->bulan == '04')
						$nama_bln = "April";
					else if ($row->bulan == '05')
						$nama_bln = "Mei";
					else if ($row->bulan == '06')
						$nama_bln = "Juni";
					else if ($row->bulan == '07')
						$nama_bln = "Juli";
					else if ($row->bulan == '08')
						$nama_bln = "Agustus";
					else if ($row->bulan == '09')
						$nama_bln = "September";
					else if ($row->bulan == '10')
						$nama_bln = "Oktober";
					else if ($row->bulan == '11')
						$nama_bln = "November";
					else if ($row->bulan == '12')
						$nama_bln = "Desember";
				
				$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
				$hasilrow = $query3->row();
				$kode_gudang	= $hasilrow->kode_gudang;
				$nama_gudang	= $hasilrow->nama;
				$nama_lokasi	= $hasilrow->nama_lokasi;
				
				// ---------- 11-03-2015, hitung saldo akhir per kode brg. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
				// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
				$queryx	= $this->db->query(" SELECT b.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a, 
									tt_stok_opname_hasil_jahit_detail b
									WHERE a.id = b.id_stok_opname_hasil_jahit
									AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$row->id_gudang'
									AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
									AND a.status_approve = 't' ");
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal = $hasilrow->auto_saldo_akhir;
				}
				else
					$saldo_awal = 0;
				
				// 2. hitung masuk bln ini
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
							tm_sjmasukwip_detail b
							WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$row->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;
				}
				else
					$jum_masuk = 0;
				
				// 3. hitung keluar bln ini
				// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjkeluarwip a, 
							tm_sjkeluarwip_detail b
							WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
							AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.id_gudang = '$row->id_gudang' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
				
				// 4. hitung saldo awal+masuk-keluar
				$auto_saldo_akhir = $saldo_awal+$jum_masuk-$jum_keluar;
				//-------------------------------------------------------------------------------------
				
				$sqlxx = " SELECT a.*, c.kode, c.nama FROM tt_stok_opname_hasil_jahit_detail_warna a, 
							tm_warna c  
							WHERE a.kode_warna = c.kode
							AND a.id_stok_opname_hasil_jahit_detail = '$row->id' ORDER BY c.nama ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->result();
						
					foreach ($hasilxx as $rowxx) {
						// ambil stok terkini di tabel tm_stok_hasil_jahit_warna
						$query3	= $this->db->query(" SELECT b.stok
							FROM tm_stok_hasil_jahit a, tm_stok_hasil_jahit_warna b
							WHERE a.id = b.id_stok_hasil_jahit
							AND a.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$row->id_gudang'
							AND b.kode_warna = '$rowxx->kode' ");
							
						if ($query3->num_rows() > 0){
							$hasilrow3 = $query3->row();
							$stok	= $hasilrow3->stok;
						}
						else {
							$stok = '0';
						}
						
						// ---------- 11-03-2015, hitung saldo akhir per warna. Rumusnya: saldo akhir bln lalu + masuk bln ini - keluar bln ini
						// 1. ambil saldo awal bln lalu, dari tabel SO field auto_saldo_akhir
						$queryx	= $this->db->query(" SELECT c.auto_saldo_akhir FROM tt_stok_opname_hasil_jahit a 
											INNER JOIN tt_stok_opname_hasil_jahit_detail b ON a.id = b.id_stok_opname_hasil_jahit
											INNER JOIN tt_stok_opname_hasil_jahit_detail_warna c ON b.id = c.id_stok_opname_hasil_jahit_detail
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.id_gudang = '$row->id_gudang'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_warna = $hasilrow->auto_saldo_akhir;
						}
						else
							$saldo_awal_warna = 0;
						
						// 2. hitung masuk bln ini
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk FROM tm_sjmasukwip a 
									INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
									INNER JOIN tm_sjmasukwip_detail_warna c ON b.id = c.id_sjmasukwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$row->id_gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_masuk_warna = $hasilrow->jum_masuk;
						}
						else
							$jum_masuk_warna = 0;
						
						// 3. hitung keluar bln ini
						// hitung brg keluar bagus jenis=1 dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_sjkeluarwip a 
									INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
									INNER JOIN tm_sjkeluarwip_detail_warna c ON b.id = c.id_sjkeluarwip_detail
									WHERE a.tgl_sj >='".$row->tahun."-".$row->bulan."-01' 
									AND a.tgl_sj <='".$row->tahun."-".$row->bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.id_gudang = '$row->id_gudang' AND c.kode_warna = '".$rowxx->kode."' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_keluar_warna = $hasilrow->jum_keluar;
						}
						else
							$jum_keluar_warna = 0;
						
						// 4. hitung saldo awal+masuk-keluar
						$auto_saldo_akhir_warna = $saldo_awal_warna+$jum_masuk_warna-$jum_keluar_warna;
						//-------------------------------------------------------------------------------------
						// ===================================================
						
						$detail_warna[] = array(
									'kode_warna'=> $rowxx->kode,
									'nama_warna'=> $rowxx->nama,
									'saldo_akhir'=> $stok,
									'stok_opname'=> $rowxx->jum_stok_opname,
									'auto_saldo_akhir_warna'=> $auto_saldo_akhir_warna
								);
					}
				}
				else
					$detail_warna = '';
				
				// 22-12-2014
				if ($row->tgl_so != '') {
					$pisah1 = explode("-", $row->tgl_so);
					$thn1= $pisah1[0];
					$bln1= $pisah1[1];
					$tgl1= $pisah1[2];
					$tgl_so = $tgl1."-".$bln1."-".$thn1;
				}
				else
					$tgl_so = '';
				
				$detail_bahan[] = array('id_header'=> $row->id_header,
										'id_gudang'=> $row->id_gudang,
										'kode_gudang'=> $kode_gudang,
										'nama_gudang'=> $nama_gudang,
										'nama_lokasi'=> $nama_lokasi,
										'bulan'=> $row->bulan,
										'tahun'=> $row->tahun,
										'tgl_so'=> $tgl_so,
										'nama_bln'=> $nama_bln,
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->nama_brg_jadi,
										'detail_warna'=> $detail_warna,
										'auto_saldo_akhir'=> $auto_saldo_akhir
									);
				$detail_warna = array();
			}
		}
		else {
			$detail_bahan = '';
		}
		return $detail_bahan;
  }
  
  function savesohasiljahit($id_so, $kode_brg_jadi, $kode_warna, $stok_fisik, $auto_saldo_akhir, $auto_saldo_akhir_warna){ 
	  $tgl = date("Y-m-d H:i:s"); 
	  
		//-------------- hitung total qty dari detail tiap2 warna -------------------
		$qtytotalstokfisik = 0;
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$kode_warna[$xx] = trim($kode_warna[$xx]);
			$stok_fisik[$xx] = trim($stok_fisik[$xx]);
			// 11-03-2015
			$auto_saldo_akhir_warna[$xx] = trim($auto_saldo_akhir_warna[$xx]);
			
			$qtytotalstokfisik+= $stok_fisik[$xx];
		} // end for
	// ---------------------------------------------------------------------
	  
		// ambil id detail id_stok_opname_hasil_jahit_detail
		$seq_detail	= $this->db->query(" SELECT id FROM tt_stok_opname_hasil_jahit_detail 
					where kode_brg_jadi = '$kode_brg_jadi' 
					AND id_stok_opname_hasil_jahit = '$id_so' ");
		if($seq_detail->num_rows() > 0) {
			$seqrow	= $seq_detail->row();
			$iddetail = $seqrow->id;
		}
		else
			$iddetail = 0;
		  
		 $this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail SET jum_stok_opname = '$qtytotalstokfisik',
					auto_saldo_akhir='$auto_saldo_akhir', status_approve='f' where id = '$iddetail' ");
			
		for ($xx=0; $xx<count($kode_warna); $xx++) {
			$this->db->query(" UPDATE tt_stok_opname_hasil_jahit_detail_warna SET jum_stok_opname = '".$stok_fisik[$xx]."',
						auto_saldo_akhir='".$auto_saldo_akhir_warna[$xx]."'
						WHERE id_stok_opname_hasil_jahit_detail='$iddetail' AND kode_warna = '".$kode_warna[$xx]."' ");
		}
		// ====================  
  }
  
  // 22-03-2014
  function get_mutasi_unit($date_from, $date_to, $unit_jahit) {		
	$pisah1 = explode("-", $date_from);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tgldari = $thn1."-".$bln1."-".$tgl1;
						
	$pisah1 = explode("-", $date_to);
	$tgl1= $pisah1[0];
	$bln1= $pisah1[1];
	$thn1= $pisah1[2];
	$tglke = $thn1."-".$bln1."-".$tgl1;
	
			// explode date_from utk keperluan saldo awal 
			$pisah1 = explode("-", $date_from);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			
			if ($bln1 == 1) {
				$bln_query = 12;
				$thn_query = $thn1-1;
			}
			else {
				$bln_query = $bln1-1;
				$thn_query = $thn1;
				if ($bln_query < 10)
					$bln_query = "0".$bln_query;
			}
			
			// explode date_to utk keperluan SO
			$pisah1 = explode("-", $date_to);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
	  
		// 1. ambil data2 unit jahit dari rentang tanggal yg dipilih
		$filter = "";
		$filter2 = "";
		if ($unit_jahit != '0') {
			$filter = " AND a.kode_unit_jahit = '$unit_jahit' ";
			$filter2= " AND a.kode_unit = '$unit_jahit' ";
		}
		
		// 06-06-2014 DIMODIF. tambahin query ke tabel stok unit jahit
		$sql = "SELECT a.kode_unit_jahit FROM (
				select a.kode_unit_jahit FROM tm_sjmasukwip a
				WHERE a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.kode_unit_jahit <> '0' GROUP BY a.kode_unit_jahit
				
				UNION SELECT a.kode_unit_jahit FROM tm_sjkeluarwip a
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter." AND a.kode_unit_jahit <> '0' GROUP BY a.kode_unit_jahit
				
				UNION SELECT a.kode_unit as kode_unit_jahit FROM tm_bonmkeluarcutting a
				WHERE a.tgl_bonm >='".$tgldari."'  
				AND a.tgl_bonm <='".$tglke."' ".$filter2." AND a.kode_unit <> '0' GROUP BY a.kode_unit
				
				UNION SELECT a.kode_unit as kode_unit_jahit FROM tm_sjmasukbhnbakupic a
				WHERE a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' ".$filter2." AND a.kode_unit <> '0' GROUP BY a.kode_unit
				
				UNION SELECT a.kode_unit as kode_unit_jahit FROM tt_stok_opname_unit_jahit a
				WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' AND a.status_approve='t' ".$filter2." GROUP BY a.kode_unit
				)
				a GROUP BY a.kode_unit_jahit ORDER BY a.kode_unit_jahit";
		
		/*UNION SELECT kode_unit as kode_unit_jahit FROM tm_stok_unit_jahit
				WHERE TRUE ".$filter2." GROUP BY kode_unit */
		
		/*$sql = "SELECT a.kode_brg_jadi FROM (
				select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tgldari."' 
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tgldari."'  
				AND a.tgl_sj <='".$tglke."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
				a GROUP BY a.kode_brg_jadi"; */
		
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$total_so_rupiah=0; $total_sisa_stok_rupiah=0;
			foreach ($hasil as $row) {
				// 22-03-2014 -----------------------------------------------------
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit_jahit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = "SELECT a.kode_brg_jadi FROM (
					select b.kode_brg_jadi FROM tm_sjmasukwip a 
					INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE c.n_active='1' AND a.tgl_sj >='".$tgldari."' 
					AND a.tgl_sj <='".$tglke."' AND a.kode_unit_jahit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a 
					INNER JOIN tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE c.n_active='1' AND a.tgl_sj >='".$tgldari."'  
					AND a.tgl_sj <='".$tglke."' AND a.kode_unit_jahit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_bonmkeluarcutting a 
					INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE c.n_active='1' AND a.tgl_bonm >='".$tgldari."'  
					AND a.tgl_bonm <='".$tglke."' AND a.kode_unit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a 
					INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE c.n_active='1' AND a.tgl_sj >='".$tgldari."'  
					AND a.tgl_sj <='".$tglke."' AND a.kode_unit = '$row->kode_unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tt_stok_opname_unit_jahit a 
					INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id=b.id_stok_opname_unit_jahit
					INNER JOIN tr_product_motif c ON c.i_product_motif = b.kode_brg_jadi
					WHERE c.n_active='1' AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
					AND a.kode_unit = '$row->kode_unit_jahit' AND a.status_approve='t' AND b.jum_stok_opname <> 0 
					GROUP BY b.kode_brg_jadi
					)
					a GROUP BY a.kode_brg_jadi
					ORDER BY kode_brg_jadi";
				/*
				 * UNION SELECT kode_brg_jadi FROM tm_stok_unit_jahit WHERE kode_unit='$row->kode_unit_jahit'
					GROUP BY kode_brg_jadi
				 */ 
				$query2	= $this->db->query($sql2);
				
				/*$jum_keluar1 = 0;
				$jum_keluar2 = 0;
				$jum_keluar_lain1 = 0;
				$jum_keluar_lain2 = 0;
				$jum_masuk = 0;
				$jum_masuk_lain1 = 0;
				$jum_masuk_lain2 = 0;
				$jum_masuk_lain3 = 0;
				$jum_masuk_lain4 = 0; 
				*/
				$jum_saldo_awal = 0;
				$jum_masuk = 0;
				$jum_keluar = 0;
				$jum_stok_akhir = 0;
				$jum_so = 0;
				$selisih_bgs = 0;
				$selisih_perbaikan = 0;
				$sisa_stok = 0;
				
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
							
						// SALDO AWAL, AMBIL DARI SO BLN SEBELUMNYA 
						//(dikomen heula, PERLU MODIF DULU DI FITUR INPUT SO UNIT JAHIT KARENA BLM ADA PEMISAHAN BAGUS & PERBAIKAN)
						// udh tambahin field jum_bagus dan jum_perbaikan di tabel tt_stok_opname_unit_jahit_detail dan warna
						// jadi bikin querynya aja dulu
						
						// saldo awal bagus dan perbaikan
					/*	echo "SELECT b.jum_bagus, b.jum_perbaikan FROM tt_stok_opname_unit_jahit a, 
											tt_stok_opname_unit_jahit_detail b
											WHERE a.id = b.id_stok_opname_unit_jahit
											AND b.kode_brg_jadi = '$row2->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't'"; echo "<br>"; */
						
							$queryx	= $this->db->query(" SELECT b.jum_bagus, b.jum_perbaikan, b.auto_saldo_akhir_bagus, 
											auto_saldo_akhir_perbaikan
											FROM tt_stok_opname_unit_jahit a, 
											tt_stok_opname_unit_jahit_detail b
											WHERE a.id = b.id_stok_opname_unit_jahit
											AND b.kode_brg_jadi = '$row2->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit_jahit'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' ");
						
						if ($queryx->num_rows() > 0){
							$hasilrow = $queryx->row();
							$saldo_awal_bgs = $hasilrow->jum_bagus;
							$saldo_awal_perbaikan = $hasilrow->jum_perbaikan;
							//$auto_saldo_awal = $hasilrow->auto_saldo_akhir;
							$auto_saldo_awal_bagus = $hasilrow->auto_saldo_akhir_bagus;
							$auto_saldo_awal_perbaikan = $hasilrow->auto_saldo_akhir_perbaikan;
						}
						else {
							$saldo_awal_bgs = 0;
							$saldo_awal_perbaikan = 0;
							//$auto_saldo_awal = 0; 
							$auto_saldo_awal_bagus = 0; $auto_saldo_awal_perbaikan = 0;
						}
						
						// 09-03-2015, sementara pake SO dulu, nanti diganti jadi dari auto saldo akhir
						// 18-04-2015 PAKE ACUAN SO LAGI
						$jum_saldo_awal = $saldo_awal_bgs+$saldo_awal_perbaikan;
						
						// 08-04-2015, diganti pake auto saldo
						//$jum_saldo_awal = $auto_saldo_awal_bagus+$auto_saldo_awal_perbaikan;
						
						//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit_jahit' AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit_jahit' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0;
						
						//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit_jahit' AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0;
						
						$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row->kode_unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >= to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0;
						
						$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						// 09-03-2015, $saldo_awal_bgs dan perbaikan diganti $auto_saldo_awal_bagus dan $auto_saldo_awal_perbaikan
						// nanti akhir maret diganti lagi jadi pake auto_saldo_awal_bagus
						//$stok_akhir_bgs = $auto_saldo_awal_bagus + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						//$stok_akhir_perbaikan = $auto_saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						
						// 08-04-2015 pake auto saldo
						// 18-04-2015 PAKE ACUAN SO LAGI
						$stok_akhir_bgs = $saldo_awal_bgs + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						
						$jum_stok_akhir = $stok_akhir_bgs + $stok_akhir_perbaikan;
						
						// ============================= END STOK AKHIR ==============================================
						
						// STOK OPNAME BAGUS DAN PERBAIKAN
						$sql = "SELECT b.jum_bagus, b.jum_perbaikan FROM tt_stok_opname_unit_jahit a, 
									tt_stok_opname_unit_jahit_detail b
									WHERE a.id = b.id_stok_opname_unit_jahit 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi' AND a.kode_unit = '$row->kode_unit_jahit'
									AND a.bulan = '$bln1' AND a.tahun = '$thn1' AND b.status_approve = 't' ";
														
						$query3	= $this->db->query($sql);
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$so_bgs = $hasilrow->jum_bagus;
							$so_perbaikan = $hasilrow->jum_perbaikan;
						}
						else {
							$so_bgs = 0;
							$so_perbaikan = 0;
						}
						$jum_so = $so_bgs+$so_perbaikan;
						//=========================================
						
						$selisih_bgs = $so_bgs-$stok_akhir_bgs;
						$selisih_perbaikan = $so_perbaikan-$stok_akhir_perbaikan;
						
						//23-05-2014 ambil data stok schedule dari tabel tm_stok_schedule
						$sqlxx = "SELECT qty FROM tm_stok_schedule_wip WHERE kode_brg_jadi = '$row2->kode_brg_jadi' 
								AND kode_unit = '$row->kode_unit_jahit'
								AND bulan='$bln1' AND tahun='$thn1' ";
						$queryxx	= $this->db->query($sqlxx);
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$stok_schedule = $hasilxx->qty;
						}
						else
							$stok_schedule = 0;
						
						$sisa_stok = $jum_so-$stok_schedule;
						
						// 23-05-2014, harga HPP, pake dari tabel hpp_wip
						$sqlxx = "SELECT harga FROM tm_hpp_wip WHERE kode_brg_jadi = '$row2->kode_brg_jadi' ";
						$queryxx	= $this->db->query($sqlxx);
						
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$hpp = $hasilxx->harga;
						}
						else
							$hpp = 0;
						
						// jumlah nominal rupiah SO
						$jum_so_rupiah = $jum_so*$hpp;
						$total_so_rupiah+= $jum_so_rupiah;
						
						// jumlah nominal sisa stok
						$sisa_stok_rupiah = $sisa_stok*$hpp;
						$total_sisa_stok_rupiah+= $sisa_stok_rupiah;
																		
						$data_stok[] = array(		'kode_brg'=> $row2->kode_brg_jadi,
													'nama_brg'=> $e_product_motifname,
													'hpp'=> $hpp,
													//09-03-2015, dikomen
													'saldo_awal_bgs'=> $saldo_awal_bgs,
													'saldo_awal_perbaikan'=> $saldo_awal_perbaikan,
													
													// per 08-04-2015 pake auto saldo akhir
													// 18-04-2015 PAKE ACUAN SO LAGI
													//'saldo_awal_bgs'=> $auto_saldo_awal_bagus,
													//'saldo_awal_perbaikan'=> $auto_saldo_awal_perbaikan,
													'jum_saldo_awal'=> $jum_saldo_awal,
													
													'masuk_bgs'=> $masuk_bgs,
													'masuk_returbrgjadi'=> $masuk_returbrgjadi,
													'masuk_pengembalian'=> $masuk_pengembalian,
													'jum_masuk'=> $jum_masuk,
													
													'keluar_bgs'=> $keluar_bgs,
													'keluar_perbaikan'=> $keluar_perbaikan,
													'keluar_retur_bhnbaku'=> $keluar_retur_bhnbaku,
													'jum_keluar'=> $jum_keluar,
													
													'stok_akhir_bgs'=> $stok_akhir_bgs,
													'stok_akhir_perbaikan'=> $stok_akhir_perbaikan,
													'jum_stok_akhir'=> $jum_stok_akhir,
													
													'so_bgs'=> $so_bgs,
													'so_perbaikan'=> $so_perbaikan,
													'jum_so'=> $jum_so,
													'jum_so_rupiah'=> $jum_so_rupiah,
													
													'selisih_bgs'=> $selisih_bgs,
													'selisih_perbaikan'=> $selisih_perbaikan,
													'stok_schedule'=> $stok_schedule,
													'sisa_stok'=> $sisa_stok,
													'sisa_stok_rupiah'=> $sisa_stok_rupiah
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------

				// array unit jahit
				$data_unit_jahit[] = array(		'kode_unit'=> $row->kode_unit_jahit,
												'nama_unit'=> $nama_unit,
												'data_stok'=> $data_stok,
												'total_so_rupiah'=> $total_so_rupiah,
												'total_sisa_stok_rupiah'=> $total_sisa_stok_rupiah
									);
				$data_stok = array(); // done
				$total_so_rupiah = 0; $total_sisa_stok_rupiah = 0;
			} // endforeach header
		}
		else {
			$data_unit_jahit = '';
		}
		return $data_unit_jahit;
  }
  
  // 02-04-2014
  function get_stokmingguan_unit($unit_jahit) {		
	  	  
		// 1. ambil data2 unit jahit
		$filter = "";
		if ($unit_jahit != '0')
			$filter = " WHERE a.kode_unit = '$unit_jahit' ";
		$sql = "SELECT distinct a.kode_unit FROM tm_stok_unit_jahit a
				INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit
				".$filter." ORDER BY a.kode_unit";
		
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row) {
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				//echo "SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit'  <br>";
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = " SELECT kode_brg_jadi, stok, stok_bagus, stok_perbaikan FROM tm_stok_unit_jahit
						WHERE kode_unit = '$row->kode_unit' ";				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';

						$data_stok[] = array(		'kode_brg'=> $row2->kode_brg_jadi,
													'nama_brg'=> $e_product_motifname,
													'stok'=> $row2->stok,
													'stok_bagus'=> $row2->stok_bagus,
													'stok_perbaikan'=> $row2->stok_perbaikan
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------

				// array unit jahit
				$data_unit_jahit[] = array(		'kode_unit'=> $row->kode_unit,
												'nama_unit'=> $nama_unit,
												'data_stok'=> $data_stok
									);
				$data_stok = array();
			} // endforeach header
		}
		else {
			$data_unit_jahit = '';
		}
		return $data_unit_jahit;
  }
  
  // 03-04-2014
  function get_bhnbaku_unit($date_from, $date_to) {		
	  
	  // ambil data forecast bulan sebelumnya (bulan sebelum date_from)
		$pisah1 = explode("-", $date_from);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		//$tgldari = $thn1."-".$bln1."-".$tgl1;
		
		// koreksi 11-04-2014, skrg ga dipake. yg dipake adalah bulan yg ada di tgl awal
		/*if ($bln1 == '01') {
			$bln_sebelumnya = 12;
			$thn_sebelumnya = $thn1-1;
		}
		else {
			$bln_sebelumnya = $bln1-1;
			$thn_sebelumnya = $thn1;
			
			if ($bln_sebelumnya < 10)
				$bln_sebelumnya = "0".$bln_sebelumnya;
		} */
		
		if ($bln1 == '01')
						$nama_bln = "Januari";
					else if ($bln1 == '02')
						$nama_bln = "Februari";
					else if ($bln1 == '03')
						$nama_bln = "Maret";
					else if ($bln1 == '04')
						$nama_bln = "April";
					else if ($bln1 == '05')
						$nama_bln = "Mei";
					else if ($bln1 == '06')
						$nama_bln = "Juni";
					else if ($bln1 == '07')
						$nama_bln = "Juli";
					else if ($bln1 == '08')
						$nama_bln = "Agustus";
					else if ($bln1 == '09')
						$nama_bln = "September";
					else if ($bln1 == '10')
						$nama_bln = "Oktober";
					else if ($bln1 == '11')
						$nama_bln = "November";
					else if ($bln1 == '12')
						$nama_bln = "Desember";
				
		// 1. ambil data2 distinct kode brg jadi dari rentang tanggal yg dipilih		
		$sql = " SELECT distinct b.kode_brg_jadi FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
				WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND a.jenis_keluar = '1'
				ORDER BY b.kode_brg_jadi ";
		
		$query	= $this->db->query($sql);
		$data_brg_jadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			foreach ($hasil as $row) {
				// ambil nama brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				$query3	= $this->db->query(" SELECT b.qty FROM tm_forecast_wip a, tm_forecast_wip_detail b 
							WHERE a.id = b.id_forecast_wip AND b.kode_brg_jadi = '$row->kode_brg_jadi'
							AND a.bulan='$bln1' AND a.tahun='$thn1' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$qty_forecast = $hasilrow->qty;
				}
				else
					$qty_forecast = 0;
				// --------------------------------------------------------------
						
				// 2. Query utk ambil unit2 jahit berdasarkan perulangan brg jadi
				$sql2 = " SELECT distinct a.kode_unit FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
				WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
				AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND b.kode_brg_jadi='$row->kode_brg_jadi' 
				AND a.jenis_keluar = '1' ORDER BY a.kode_unit ";				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					$jmldipenuhi = 0;
					foreach ($hasil2 as $row2) { // perulangan unit2 jahitnya
						$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit' ");
						$hasilrow = $query3->row();
						$nama_unit	= $hasilrow->nama;
						
						// ambil jumlah qty barang berdasarkan unit
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
								WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy') 
								AND a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy') AND kode_unit = '$row2->kode_unit'
								AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.jenis_keluar = '1' ");
						$hasilrow = $query3->row();
						$jum	= $hasilrow->jum;
						
						$jmldipenuhi+=$jum;
						
						$data_stok[] = array(		'kode_unit'=> $row2->kode_unit,
													'nama_unit'=> $nama_unit,
													'jum'=> $jum
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				//-----------------------------------------------------------------
				$sisa_forecast = $qty_forecast-$jmldipenuhi;
				// array brg jadi
				$data_brg_jadi[] = array(		'kode_brg_jadi'=> $row->kode_brg_jadi,
												'nama_brg_jadi'=> $e_product_motifname,
												'bln_forecast'=> $nama_bln,
												'thn_forecast'=> $thn1,
												'qty_forecast'=> $qty_forecast,
												'sisa_forecast'=> $sisa_forecast,
												'data_stok'=> $data_stok
									);
				$data_stok = array();
			} // endforeach header
		}
		else {
			$data_brg_jadi = '';
		}
		return $data_brg_jadi;
  }
  
  // 05-04-2014
  function get_grup_jahit($unit_jahit){
    $query = $this->db->query(" SELECT * FROM tm_grup_jahit WHERE kode_unit='$unit_jahit' ORDER BY nama_grup ");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 19-04-2014
  function get_rekap_presentasi_kerja_unit($bulan, $tahun, $unit_jahit) {		
	  
		// 1. ambil data2 unit jahit dari periode yg dipilih
		$filter = "";
		if ($unit_jahit != '0')
			$filter = " AND kode_unit = '$unit_jahit' ";
		
		$sql = " SELECT id, total_rata, kode_unit FROM tm_presentasi_kerja_unit WHERE bulan='$bulan' AND tahun='$tahun' ".$filter."
				ORDER BY kode_unit ";
					
		$query	= $this->db->query($sql);
		$data_unit_jahit = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
						
			foreach ($hasil as $row) {
				// ambil nama unit
				$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
				$hasilrow = $query3->row();
				$nama_unit	= $hasilrow->nama;
		
				// 2. Query distinct nama grup jahit
				// modif 15-05-2014
				$sql2 = " SELECT DISTINCT c.nama_grup_jahit FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.bulan='$bulan' AND a.tahun='$tahun' AND a.kode_unit='$row->kode_unit' 
						AND c.persentase <> '0'
						ORDER BY c.nama_grup_jahit ";
				$query2	= $this->db->query($sql2);
				
				$data_grup_jahit = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						// 2. Query item2 brgnya berdasarkan nama grup
						// modif 15-05-2014
						$sql2x = " SELECT b.kode_brg_jadi, c.* FROM tm_presentasi_kerja_unit a, 
								tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
								WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
								AND a.bulan='$bulan' AND a.tahun='$tahun' AND a.kode_unit='$row->kode_unit'
								AND c.nama_grup_jahit='$row2->nama_grup_jahit' 
								AND c.persentase <> '0'
								ORDER BY b.kode_brg_jadi "; 
						$query2x	= $this->db->query($sql2x);
						
						$data_item_brg = array();
						if ($query2x->num_rows() > 0){
							$hasil2x = $query2x->result();
							foreach ($hasil2x as $row2x) {
								$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2x->kode_brg_jadi' ");
								if ($query3->num_rows() > 0){
									$hasilrow = $query3->row();
									$e_product_motifname = $hasilrow->e_product_motifname;
								}
								else
									$e_product_motifname = '';
									
								$data_item_brg[] = array('kode_brg_jadi'=> $row2x->kode_brg_jadi,
														'nama_brg_jadi'=> $e_product_motifname,
														'hari'=> $row2x->hari,
														'kapasitas'=> $row2x->kapasitas,
														'minggu1'=> $row2x->minggu1,
														'minggu2'=> $row2x->minggu2,
														'minggu3'=> $row2x->minggu3,
														'minggu4'=> $row2x->minggu4,
														'total'=> $row2x->total,
														'persentase'=> $row2x->persentase
													);
								
							} // end for2x
						} // end if2x
						else
							$data_item_brg = '';
									
						$data_grup_jahit[] = array('nama_grup_jahit'=> $row2->nama_grup_jahit,
												   'data_item_brg'=> $data_item_brg
													);
						$data_item_brg = array();
					//-----------------------------------------------------------------
				} // end for2
			} // end if2
			else
				$data_grup_jahit = '';
				
			$data_unit_jahit[] = array('kode_unit'=> $row->kode_unit,
									   'nama_unit'=> $nama_unit,
									   'total_rata'=> $row->total_rata,
									   'data_grup_jahit'=> $data_grup_jahit);
		} // end for
	} // end if
	else {
		$data_unit_jahit = '';
	}
	return $data_unit_jahit;
  }
  
  // 28-04-2014
  function get_rekap_presentasi_kerja_unit_tahunan($tahun) {		
	  // 16-10-2014
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautama = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql = " SELECT distinct a.kode_unit, c.nama_grup_jahit FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.tahun='$tahun' AND c.persentase<>'0' AND a.kode_unit = '".$rowutama->kode_unit."'
						ORDER BY a.kode_unit, c.nama_grup_jahit ";
			  
			   $query	= $this->db->query($sql);
			   $data_rekap = array();
				
				if ($query->num_rows() > 0){
				$hasil = $query->result();
								
				foreach ($hasil as $row) {
					// hitung masing2 total presentase utk tiap2 grup jahit
					for ($bulan=1; $bulan<=12; $bulan++) {
						if ($bulan < 10)
							$bln = "0".$bulan;
						else
							$bln = $bulan;
												
						$query3	= $this->db->query(" SELECT AVG(persentase) AS rata FROM tm_presentasi_kerja_unit a, 
						tm_presentasi_kerja_unit_detail b, tm_presentasi_kerja_unit_detail_grupjahit c 
						WHERE a.id = b.id_presentasi_kerja_unit AND b.id = c.id_presentasi_kerja_unit_detail
						AND a.tahun='$tahun' AND a.bulan='$bln' AND c.nama_grup_jahit='$row->nama_grup_jahit'
						AND a.kode_unit = '$row->kode_unit' AND c.persentase<>'0' ");
						
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$rata = $hasilrow->rata;
							if ($rata == '')
								$rata = 0;
						}
						else
							$rata= 0;
						
						$listratabulan[] = array($bulan => $rata);
					} // end for
					
					// ambil nama unit
					$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
					$hasilrow = $query3->row();
					$nama_unit	= $hasilrow->nama;
					
					$data_rekap[] = array('kode_unit'=> $row->kode_unit,
										  'nama_unit'=> $nama_unit,
										  'nama_grup_jahit'=> $row->nama_grup_jahit,
										  'listratabulan'=> $listratabulan);
					$listratabulan=array();
				} // end for
			  }
			  else
				$data_rekap = '';
				
			  $datautama[] = array(		'kode'=> $rowutama->kode,
										'nama'=> $rowutama->nama,
										'data_rekap'=> $data_rekap
													);
			} // end for
		} // end if	  
	  
		return $datautama;
	  //----------------------------------------------------------------------------------------------------------------------
  }
  
  // 30-04-2014
  function get_forecastvsschedule($bulan, $tahun) {
	  
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// modif 17-09-2014
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_detail b 
					INNER JOIN tm_forecast_wip a ON b.id_forecast_wip = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					ORDER BY d.kode, b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				// 17-09-2014, ambil SO di semua unit jahit di bulan sebelumnya
				$queryx	= $this->db->query(" SELECT sum(b.jum_bagus) as jumbagus FROM tt_stok_opname_unit_jahit a, 
											tt_stok_opname_unit_jahit_detail b
											WHERE a.id = b.id_stok_opname_unit_jahit
											AND b.kode_brg_jadi = '$row->kode_brg_jadi'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't' ");
						
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal_bgs = $hasilrow->jumbagus;
					if ($saldo_awal_bgs == '')
						$saldo_awal_bgs = 0;
				}
				else {
					$saldo_awal_bgs = 0;
				}
				
				// KOLOM SCHEDULE ambil data barang masuk ke unit jahit
				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.jenis_keluar = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						//2. hitung brg masuk retur brg jadi dari perusahaan, dari tm_sjkeluarwip
					/*	$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjkeluarwip a, 
									tm_sjkeluarwip_detail b
									WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit <> '0' AND a.jenis_keluar = '3' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_returbrgjadi = $hasilrow->jum_masuk;
							
							if ($masuk_returbrgjadi == '')
								$masuk_returbrgjadi = 0;
						}
						else
							$masuk_returbrgjadi = 0; 
						
						//3. hitung brg masuk pengembalian dari perusahaan, dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a, 
									tm_bonmkeluarcutting_detail b
									WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.jenis_keluar = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_pengembalian = $hasilrow->jum_masuk;
							
							if ($masuk_pengembalian == '')
								$masuk_pengembalian = 0;
						}
						else
							$masuk_pengembalian = 0; */
						
						//$jum_masuk = $masuk_bgs+$masuk_returbrgjadi+$masuk_pengembalian;
						$jum_masuk = $masuk_bgs;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit <> '0' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						// 5. hitung brg keluar perbaikan, dari tm_sjmasukwip
					/*	$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit <> '0' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_perbaikan = $hasilrow->jum_keluar;
							
							if ($keluar_perbaikan == '')
								$keluar_perbaikan = 0;
						}
						else
							$keluar_perbaikan = 0;
						
						// 6. hitung brg keluar retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_retur_bhnbaku = $hasilrow->jum_keluar;
							
							if ($keluar_retur_bhnbaku == '')
								$keluar_retur_bhnbaku = 0;
						}
						else
							$keluar_retur_bhnbaku = 0; */
						
						//$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						$jum_keluar = $keluar_bgs;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						// tambahan 27-08-2014, kolom schedule dimodif perhitungannya
						//kolom schedule = brg masuk - stock schedule bln trsbt + stock schedule 1 bln sblmnya
						
						// a. stok schedule bulan tsb
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bulan' AND tahun='$tahun' AND kode_brg_jadi = '$row->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule = $hasilrow->qty;
							
							if ($qty_schedule == '')
								$qty_schedule = 0;
						}
						else
							$qty_schedule = 0;
						
						// b. stok schedule 1 bulan sebelumnya
						if ($bulan == 1) {
							$bln_query = 12;
							$thn_query = $tahun-1;
						}
						else {
							$bln_query = $bulan-1;
							$thn_query = $tahun;
							if ($bln_query < 10)
								$bln_query = "0".$bln_query;
						}
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bln_query' AND tahun='$thn_query' AND kode_brg_jadi = '$row->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule_sebelumnya = $hasilrow->qty;
							
							if ($qty_schedule_sebelumnya == '')
								$qty_schedule_sebelumnya = 0;
						}
						else
							$qty_schedule_sebelumnya = 0;
						
						$totalqtyschedule = $jum_masuk-$qty_schedule+$qty_schedule_sebelumnya;
						//===========================================================
						if ($row->qty != 0) {
							//$persenschedule = ($jum_masuk/$row->qty)*100;
							$persenschedule = ($totalqtyschedule/$row->qty)*100;
							$persenforecast = ($jum_keluar/$row->qty)*100;
						}
						else {
						//echo $row->qty."<br>";
							$persenschedule = 0;
							$persenforecast = 0;
						}
						// 17-09-2014
						//echo $totalqtyschedule." ".$saldo_awal_bgs."<br>";
						$temptotal = $totalqtyschedule+$saldo_awal_bgs;
						if ($temptotal != 0)
							$persenforecast2 = ($jum_keluar/($totalqtyschedule+$saldo_awal_bgs))*100;
						else
							$persenforecast2 = 0;
						
				//17-09-2014, ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan,
										//'schedule'=> $jum_masuk,
										'schedule'=> $totalqtyschedule,
										'brgmasuk'=> $jum_keluar,
										'persenschedule'=> $persenschedule,
										//'persenforecast'=> $persenforecast,
										'persenforecast2'=> $persenforecast2,
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel,
										'saldo_awal_bgs'=> $saldo_awal_bgs
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  // 10-05-2014
  function get_transaksi_unit_jahit($bulan, $tahun, $unit_jahit) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// 1. ambil data2 stok di unit jahit
		$sql = " SELECT id, kode_brg_jadi FROM tm_stok_unit_jahit WHERE kode_unit = '$unit_jahit' ORDER BY kode_brg_jadi ";
		$query	= $this->db->query($sql);
		$data_brg_jadi = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row) {
				$data_warna = array();
				$data_so_warna = array();
				
				// 14-05-2014, ambil data2 warna dari tm_stok_unit_jahit_warna
				$sqlwarna = " SELECT a.kode_warna, b.nama FROM tm_stok_unit_jahit_warna a, tm_warna b 
							WHERE a.kode_warna=b.kode AND a.id_stok_unit_jahit='$row->id' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'saldo_retur'=> 0,
												'tot_masuk1'=> 0,
												'tot_keluar1'=> 0,
												'tot_masuk_retur1'=> 0,
												'tot_keluar_retur1'=> 0,
												'tot_masuk_retur2'=> 0,
												'tot_keluar_retur2'=> 0
											);
					}
				}
				
				// 17-05-2014, ambil SO brg jadi dari tt_stok_opname_unit_jahit_detail_warna
				$sqlwarna = " SELECT c.kode_warna, d.nama, c.jum_bagus, c.jum_perbaikan 
							FROM tt_stok_opname_unit_jahit a, tt_stok_opname_unit_jahit_detail b, 
							tt_stok_opname_unit_jahit_detail_warna c, tm_warna d 
							WHERE a.id = b.id_stok_opname_unit_jahit AND b.id = c.id_stok_opname_unit_jahit_detail 
							AND c.kode_warna=d.kode AND a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.kode_brg_jadi = '$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit' "; //echo $sqlwarna; die();
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['kode_warna'] == $rowwarna->kode_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_bagus;
								$data_warna[$xx]['saldo_retur']= $rowwarna->jum_perbaikan;
							}
						} // end for
						
						$data_so_warna[] = array(	'kode_warna'=> $rowwarna->kode_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_bagus'=> $rowwarna->jum_bagus,
												'jum_perbaikan'=> $rowwarna->jum_perbaikan
											);
					}
				}
				
				// ambil nama brg jadi
				$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row->kode_brg_jadi' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$e_product_motifname = $hasilrow->e_product_motifname;
				}
				else
					$e_product_motifname = '';
				
				/*  $sql = "SELECT a.kode_brg_jadi, sum(a.masuk) as masuk, sum(a.keluar) as keluar FROM (
				select b.kode_brg_jadi, sum(b.qty) as masuk, 0 as keluar FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
				WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi
				
				UNION SELECT b.kode_brg_jadi, 0 as masuk, sum(b.qty) as keluar FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
				WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
				AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.id_gudang = '$gudang' GROUP BY b.kode_brg_jadi)
				a GROUP BY a.kode_brg_jadi"; */
				
				// lanjutan 13-05-2014
				
				// 1. UNTUK TABEL 1
				// QUERY UTK KELUAR MASUK BRG JADI BAGUS DAN RETUR BHN BAKU
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, 1 as masukwip, 0 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip AND a.jenis_masuk = '1'
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit_jahit='$unit_jahit'
						UNION
						SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, 0 as masukwip, 1 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
						WHERE a.id = b.id_bonmkeluarcutting AND a.jenis_keluar = '1'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						
						UNION
						SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, 0 as masukwip, 0 as bonmkeluarcutting, 1 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
						WHERE a.id = b.id_bonmkeluarcutting AND a.jenis_keluar = '2'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwip, 0 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 1 as sjmasukbhnbakupic 
						FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b
						WHERE a.id = b.id_sjmasukbhnbakupic
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						ORDER BY tgl_sj ASC
						 "; // 10-05-2014 contoh union di modul info-pembelian mmaster.php.
				
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$is_masukwip = $row2->masukwip;
						$is_bonmkeluarcutting = $row2->bonmkeluarcutting;
						$is_bonmkeluarcutting2 = $row2->bonmkeluarcutting2;
						$is_sjmasukbhnbakupic = $row2->sjmasukbhnbakupic;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($is_masukwip == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c 
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						else if ($is_bonmkeluarcutting == '1' || $is_bonmkeluarcutting2 == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b, tm_bonmkeluarcutting_detail_warna c 
									WHERE a.id = b.id_bonmkeluarcutting AND b.id = c.id_bonmkeluarcutting_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "ya";
							$keluar= "tidak";
						}
						else if ($is_sjmasukbhnbakupic == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b, tm_sjmasukbhnbakupic_detail_warna c 
									WHERE a.id = b.id_sjmasukbhnbakupic AND b.id = c.id_sjmasukbhnbakupic_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "tidak";
							$keluar= "ya";
						}
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$sqlxx = " SELECT nama FROM tm_warna WHERE kode='$row3->kode_warna' ";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nama_warna = $hasilxx->nama;
								}
								else
									$nama_warna = '';
								
								// saldo per row 14-05-2014
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['kode_warna'] == $row3->kode_warna) {
										if ($masuk == "ya") {
											$data_warna[$xx]['saldo']+= $row3->qty;
											//$data_warna[$xx]['tot_masuk']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											$data_warna[$xx]['saldo']-= $row3->qty;
											//$data_warna[$xx]['tot_keluar']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($is_masukwip == '1') {
											$data_warna[$xx]['tot_keluar1']+= $row3->qty;
										}
										else if ($is_bonmkeluarcutting == '1') {
											$data_warna[$xx]['tot_masuk1']+= $row3->qty;
										}
										else if ($is_bonmkeluarcutting2 == '1') {
											$data_warna[$xx]['tot_masuk_retur1']+= $row3->qty;
										}
										else if ($is_sjmasukbhnbakupic == '1') {
											$data_warna[$xx]['tot_keluar_retur1']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'is_masukwip'=> $is_masukwip,
												'is_bonmkeluarcutting'=> $is_bonmkeluarcutting,
												'is_bonmkeluarcutting2'=> $is_bonmkeluarcutting2,
												'is_sjmasukbhnbakupic'=> $is_sjmasukbhnbakupic,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
					
				// 2. UNTUK TABEL 2
				// QUERY UTK KELUAR MASUK RETUR BRG JADI
				$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, 1 as masukwip, 0 as keluarwip
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip AND a.jenis_masuk = '2'
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit_jahit='$unit_jahit'
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwip, 1 as keluarwip
						FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
						WHERE a.id = b.id_sjkeluarwip AND a.jenis_keluar = '3'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit_jahit='$unit_jahit'
						ORDER BY tgl_sj ASC
						 ";
				
				$query2	= $this->db->query($sql2);
				$data_tabel2 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$is_masukwip = $row2->masukwip;
						$is_keluarwip = $row2->keluarwip;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						
						if ($is_masukwip == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjmasukwip a, tm_sjmasukwip_detail b, tm_sjmasukwip_detail_warna c 
									WHERE a.id = b.id_sjmasukwip AND b.id = c.id_sjmasukwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						else if ($is_keluarwip == '1') {
							$sql3 = " SELECT c.kode_warna, c.qty FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b, tm_sjkeluarwip_detail_warna c 
									WHERE a.id = b.id_sjkeluarwip AND b.id = c.id_sjkeluarwip_detail
									AND a.id = '$id_data' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									ORDER BY c.kode_warna ";
							$masuk = "ya";
							$keluar = "tidak";
						}
												
						$query3	= $this->db->query($sql3);
						$data_tabel2_perwarna = array();
						$data_tabel2_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$sqlxx = " SELECT nama FROM tm_warna WHERE kode='$row3->kode_warna' ";
								$queryxx	= $this->db->query($sqlxx);
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nama_warna = $hasilxx->nama;
								}
								else
									$nama_warna = '';
								
								// saldo per row 14-05-2014
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['kode_warna'] == $row3->kode_warna) {
										if ($masuk == "ya") {
											$data_warna[$xx]['saldo_retur']+= $row3->qty;
											//$data_warna[$xx]['tot_masuk_retur']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo_retur'];
											$data_tabel2_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											$data_warna[$xx]['saldo_retur']-= $row3->qty;
											//$data_warna[$xx]['tot_keluar_retur']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo_retur'];
											$data_tabel2_saldo_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($is_masukwip == '1') {
											$data_warna[$xx]['tot_keluar_retur2']+= $row3->qty;
										}
										else if ($is_keluarwip == '1') {
											$data_warna[$xx]['tot_masuk_retur2']+= $row3->qty;
										}
									}
								}
									
								$data_tabel2_perwarna[] = array('kode_warna'=> $row3->kode_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						$data_tabel2[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'is_masukwip'=> $is_masukwip,
												'is_keluarwip'=> $is_keluarwip,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel2_perwarna'=> $data_tabel2_perwarna,
												'data_tabel2_saldo_perwarna'=> $data_tabel2_saldo_perwarna
										);
						$data_tabel2_perwarna = array();
						$data_tabel2_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel2='';
				
				// =================================== END NEW =======================================================
				
				// array brg jadi
				$data_brg_jadi[] = array(		'kode_brg_jadi'=> $row->kode_brg_jadi,
												'nama_brg_jadi'=> $e_product_motifname,
												'data_tabel1'=> $data_tabel1,
												'data_tabel2'=> $data_tabel2,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna
									);
				$data_tabel1 = array();
				$data_tabel2 = array();
				$data_warna = array();
				$data_so_warna = array();
			} // endforeach header
		}
		else {
			$data_brg_jadi = '';
		}
		return $data_brg_jadi;
  }
  
  // 24-05-2014
  function get_mutasi_unit_stokschedule($unit_jahit, $bulan, $tahun) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
	  
				// Query utk ambil item2 barang berdasarkan perulangan unit jahit
				$sql2 = "SELECT a.kode_brg_jadi FROM (
					select b.kode_brg_jadi FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
					WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit_jahit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjkeluarwip a, tm_sjkeluarwip_detail b 
					WHERE a.id = b.id_sjkeluarwip AND a.tgl_sj >='".$tahun."-".$bulan."-01'   
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit_jahit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b 
					WHERE a.id = b.id_bonmkeluarcutting AND a.tgl_bonm >='".$tahun."-".$bulan."-01'   
					AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b 
					WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
					AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND a.kode_unit = '$unit_jahit' GROUP BY b.kode_brg_jadi
					
					UNION SELECT b.kode_brg_jadi FROM tt_stok_opname_unit_jahit a, tt_stok_opname_unit_jahit_detail b
					WHERE a.id=b.id_stok_opname_unit_jahit AND a.bulan='$bulan' AND a.tahun='$tahun' 
					AND a.kode_unit = '$unit_jahit' AND a.status_approve='t' AND b.jum_stok_opname <> 0 
					GROUP BY b.kode_brg_jadi
					)
					a GROUP BY a.kode_brg_jadi ORDER BY a.kode_brg_jadi";
				
				$query2	= $this->db->query($sql2);
								
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
																		
						$data_stok[] = array(		'id'=> 0,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'stok_schedule'=> 0
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				return $data_stok;
				//-----------------------------------------------------------------
  }
  
  function get_stok_schedule($unit_jahit, $bulan, $tahun) {
		$query	= $this->db->query(" SELECT a.id, a.kode_brg_jadi, a.qty, b.e_product_motifname 
					FROM tm_stok_schedule_wip a, tr_product_motif b
					WHERE a.kode_brg_jadi = b.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.kode_unit = '$unit_jahit'
					ORDER BY a.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datastok = array();
			foreach ($hasil as $row) {
				$datastok[] = array( 	'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'stok_schedule'=> $row->qty
									);
			}
		}
		else {
			$datastok = '';
		}
		return $datastok;
  }
  
  function cek_stokschedule($unit_jahit, $bulan, $tahun) {
	$query3	= $this->db->query(" SELECT id FROM tm_stok_schedule_wip
						WHERE kode_unit='$unit_jahit' AND bulan = '$bulan' AND tahun = '$tahun' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
    
  // 29-08-2014
  function get_mutasi_unit_qc($bulan, $tahun) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
				
		// 1. query brg bagus
		// 16-10-2014
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautamabagus = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
						INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
						WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
						AND a.jenis_masuk = '1' AND a.kode_unit_jahit = '".$rowutama->kode_unit."'
					  UNION SELECT DISTINCT a.kode_unit as kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a
					  INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
					  WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
					  AND a.kode_unit = '".$rowutama->kode_unit."'
					 ORDER BY kode_unit_jahit, kode_brg_jadi
					 ";
				$query2	= $this->db->query($sql2);
				
				$data_stok = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
						
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
																		
						// ambil jum masuk wip (SJ masuk WIP)=====================
						// 1. hitung brg masuk WIP bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row2->kode_unit_jahit' AND a.jenis_masuk = '1' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
												
						// 2. hitung brg masuk retur bhn baku, dari tm_sjmasukbhnbakupic
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukbhnbakupic a, 
									tm_sjmasukbhnbakupic_detail b
									WHERE a.id = b.id_sjmasukbhnbakupic AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_retur_bhnbaku = $hasilrow->jum_masuk;
							
							if ($masuk_retur_bhnbaku == '')
								$masuk_retur_bhnbaku = 0;
						}
						else
							$masuk_retur_bhnbaku = 0;
						
						$jum_masuk_bgs = $masuk_bgs+$masuk_retur_bhnbaku;
						//$jum_masuk_perbaikan = $masuk_perbaikan;
						//$jum_keluar = $keluar_bgs+$keluar_perbaikan+$keluar_retur_bhnbaku;
						
						/*
						 * $stok_akhir_bgs = $saldo_awal_bgs + $masuk_bgs + $masuk_pengembalian - $keluar_bgs - $keluar_retur_bhnbaku;
						$stok_akhir_perbaikan = $saldo_awal_perbaikan + $masuk_returbrgjadi - $keluar_perbaikan;
						 */
						// -------------------------------- END BARANG MASUK -----------------------------------------------
					
						//========================================================
						
						// ambil retur, dari tabel tm_retur_qc_wip
						$query3	= $this->db->query(" SELECT qty FROM tm_retur_qc_wip WHERE kode_unit = '$row2->kode_unit_jahit'
											AND bulan='$bulan' AND tahun='$tahun' AND jenis = 'BAGUS'
											AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_retur_bgs = $hasilrow->qty;
						}
						else {
							$jum_retur_bgs = 0;
						}
						
						$gradea_bgs = $jum_masuk_bgs-$jum_retur_bgs;
						//$gradea_perbaikan = $jum_masuk_perbaikan-$jum_retur_perbaikan;
						
						if ($jum_masuk_bgs != 0)
							$persena_bgs = ($gradea_bgs/$jum_masuk_bgs)*100;
						else
							$persena_bgs = 0;
						//$persena_perbaikan = $gradea_perbaikan/$jum_masuk_perbaikan;
						
						if ($jum_masuk_bgs != 0)
							$persen_retur_bgs = ($jum_retur_bgs/$jum_masuk_bgs)*100;
						else
							$persen_retur_bgs = 0;
						//$persen_retur_perbaikan = $jum_retur_perbaikan/$jum_masuk_perbaikan;
						
						$data_stok[] = array(		'id'=> 0,
													'kode_unit_jahit'=> $row2->kode_unit_jahit,
													'nama_unit_jahit'=> $nama_unit_jahit,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'qty'=> $jum_masuk_bgs,
													'retur'=> $jum_retur_bgs,
													'gradea'=> $gradea_bgs,
													'persena'=> $persena_bgs,
													'persen_retur'=> $persen_retur_bgs
													);
					} // end foreach2
				}
				else
					$data_stok = '';
				
				$datautamabagus[] = array(		'kode'=> $rowutama->kode,
												'nama'=> $rowutama->nama,
												'data_stok'=> $data_stok
													);
					
			} // end foreach
		} // end if queryutama
		
		/*$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
					INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
					WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
					AND a.jenis_masuk = '1'
				  UNION SELECT DISTINCT a.kode_unit as kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukbhnbakupic a
				  INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
				  WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
				 ORDER BY kode_unit_jahit, kode_brg_jadi
				 "; */
		//$query2	= $this->db->query($sql2);
								
				
		// 2. barang perbaikan
		$sqlutama = " SELECT a.kode, a.nama, b.kode_unit FROM tm_kel_forecast_wip_unit a 
					INNER JOIN tm_kel_forecast_wip_unit_listunit b ON a.id = b.id_kel_forecast_wip_unit
					ORDER BY a.kode, b.kode_unit ";
		$queryutama	= $this->db->query($sqlutama);
		
		$datautamaperbaikan = array();
		if ($queryutama->num_rows() > 0){
			$hasilutama = $queryutama->result();
					
			foreach ($hasilutama as $rowutama) {
				$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
							INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
							WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
							AND a.jenis_masuk = '2' AND a.kode_unit_jahit = '".$rowutama->kode_unit."' 
							ORDER BY a.kode_unit_jahit, b.kode_brg_jadi ";
				$query2	= $this->db->query($sql2);
				
				$data_stok2 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) { // perulangan item2 brgnya
						$query3	= $this->db->query(" SELECT e_product_motifname FROM tr_product_motif WHERE i_product_motif = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$e_product_motifname = $hasilrow->e_product_motifname;
						}
						else
							$e_product_motifname = '';
						
						$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row2->kode_unit_jahit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
																		
						// ambil jum masuk wip (SJ masuk WIP)=====================
						// 1. hitung brg masuk WIP PERBAIKAN, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
									AND b.kode_brg_jadi = '$row2->kode_brg_jadi'
									AND a.kode_unit_jahit = '$row2->kode_unit_jahit' AND a.jenis_masuk = '2' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_perbaikan = $hasilrow->jum_masuk;
							
							if ($masuk_perbaikan == '')
								$masuk_perbaikan = 0;
						}
						else
							$masuk_perbaikan = 0;
						
						$jum_masuk_perbaikan = $masuk_perbaikan;
						// -------------------------------- END BARANG MASUK -----------------------------------------------
					
						//========================================================
						
						// ambil retur, dari tabel tm_retur_qc_wip
						$query3	= $this->db->query(" SELECT qty FROM tm_retur_qc_wip WHERE kode_unit = '$row2->kode_unit_jahit'
											AND bulan='$bulan' AND tahun='$tahun' AND jenis = 'PERBAIKAN'
											AND kode_brg_jadi = '$row2->kode_brg_jadi' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$jum_retur_perbaikan = $hasilrow->qty;
						}
						else {
							$jum_retur_perbaikan = 0;
						}
						
						//$gradea_bgs = $jum_masuk_bgs-$jum_retur_bgs;
						$gradea_perbaikan = $jum_masuk_perbaikan-$jum_retur_perbaikan;
						
						//$persena_bgs = $gradea_bgs/$jum_masuk_bgs;
						$persena_perbaikan = ($gradea_perbaikan/$jum_masuk_perbaikan)*100;
						
						//$persen_retur_bgs = $jum_retur_bgs/$jum_masuk_bgs;
						$persen_retur_perbaikan = ($jum_retur_perbaikan/$jum_masuk_perbaikan)*100;
						
						$data_stok2[] = array(		'id'=> 0,
													'kode_unit_jahit'=> $row2->kode_unit_jahit,
													'nama_unit_jahit'=> $nama_unit_jahit,
													'kode_brg_jadi'=> $row2->kode_brg_jadi,
													'nama_brg_jadi'=> $e_product_motifname,
													'qty'=> $jum_masuk_perbaikan,
													'retur'=> $jum_retur_perbaikan,
													'gradea'=> $gradea_perbaikan,
													'persena'=> $persena_perbaikan,
													'persen_retur'=> $persen_retur_perbaikan
													);
					} // end foreach2
				}
				else
					$data_stok2 = '';
				
				$datautamaperbaikan[] = array(		'kode'=> $rowutama->kode,
													'nama'=> $rowutama->nama,
													'data_stok2'=> $data_stok2
													);
			} // end foreachutama
		} // end ifutama
		
		// 2. barang perbaikan
		/*$sql2 = " SELECT DISTINCT a.kode_unit_jahit, b.kode_brg_jadi FROM tm_sjmasukwip a 
					INNER JOIN tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip 
					WHERE a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."'
					AND a.jenis_masuk = '2' ORDER BY a.kode_unit_jahit, b.kode_brg_jadi
				 ";
		$query2	= $this->db->query($sql2); */
								
				
					
		$hasil= array('datautamabagus'=>$datautamabagus,
					'datautamaperbaikan'=>$datautamaperbaikan);
				
		return $hasil;
				//-----------------------------------------------------------------
  }
  
  // 30-08-2014
  function cek_returqc($bulan, $tahun, $jenis) {
	$query3	= $this->db->query(" SELECT id FROM tm_retur_qc_wip
						WHERE bulan = '$bulan' AND tahun = '$tahun' AND jenis='$jenis' ");
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_retur_qc($bulan, $tahun, $jenis) {
		$query	= $this->db->query(" SELECT a.id, a.kode_unit, a.kode_brg_jadi, a.qty, b.e_product_motifname 
					FROM tm_retur_qc_wip a, tr_product_motif b
					WHERE a.kode_brg_jadi = b.i_product_motif
					AND a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.jenis = '$jenis'
					ORDER BY a.kode_unit, a.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datastok = array();
			foreach ($hasil as $row) {
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$row->kode_unit' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_unit_jahit = $hasilrow->nama;
						}
						else
							$nama_unit_jahit = '';
				
				$datastok[] = array( 	'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'kode_unit_jahit'=> $row->kode_unit,
										'nama_unit_jahit'=> $nama_unit_jahit,
										'retur'=> $row->qty
									);
			}
		}
		else {
			$datastok = '';
		}
		return $datastok;
  }
  
  // 23-09-2014
  function cek_forecast_unit($bulan, $tahun, $id_kel_unit) {
	/*$jumstring = strlen($kodeunit);
	  
	$new_kodeunit = substr($kodeunit, 0, $jumstring-1);
	$sql = " SELECT distinct id FROM tm_forecast_wip_unit WHERE bulan = '$bulan' AND tahun = '$tahun'
					AND id IN (SELECT distinct id_forecast_wip_unit FROM tm_forecast_wip_unit_listunit WHERE 
							kode_unit IN (".$new_kodeunit.") ) "; */
	
	$sql = " SELECT id FROM tm_forecast_wip_unit WHERE bulan = '$bulan' AND tahun = '$tahun'
			 AND id_kel_forecast_wip_unit = '$id_kel_unit' ";
	
	$query3	= $this->db->query($sql);
		
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$idnya = $hasilrow->id;
		}
		else {
			$idnya = '';
		}
		
		$datanya = array('idnya'=> $idnya
							);
							
		return $datanya;
  }
  
  function get_forecast_unit($bulan, $tahun, $id_kel_unit) {
	  /*$jumstring = strlen($kodeunit);
	  $new_kodeunit = substr($kodeunit, 0, $jumstring-1); */
	  
		/*$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.id IN (SELECT distinct id_forecast_wip_unit FROM tm_forecast_wip_unit_listunit WHERE 
							kode_unit IN (".$new_kodeunit.") )
					ORDER BY b.id "); */
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.id_kel_forecast_wip_unit = '$id_kel_unit'
					ORDER BY b.id ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  function saveforecastunit($is_new, $id_fc, $iddetail, $kode_brg_jadi, $fc, $ket){ 
	  $tgl = date("Y-m-d H:i:s"); 
	 // 23-09-2014, adopsi dari forecast biasa
	 // 02-04-2014, ga pake is_new lagi. langsung save aja setelah delete detail sebelumnya
	//  if ($is_new == '1') {
		  $seq	= $this->db->query(" SELECT id FROM tm_forecast_wip_unit_detail ORDER BY id DESC LIMIT 1 ");
			if($seq->num_rows() > 0) {
				$seqrow	= $seq->row();
				$iddetailx	= $seqrow->id+1;
			}else{
				$iddetailx	= 1;
			}
		  
		   $data_detail = array(
						'id'=>$iddetailx,
						'id_forecast_wip_unit'=>$id_fc,
						'kode_brg_jadi'=>$kode_brg_jadi, 
						'qty'=>$fc,
						'keterangan'=>$ket
					);
		   $this->db->insert('tm_forecast_wip_unit_detail',$data_detail);
	/*  }
	  else {
		  $iddetailx = $iddetail;
		  
		  $this->db->query(" UPDATE tm_forecast_wip_detail SET qty = '$fc', keterangan='$ket'
						where id = '$iddetailx' ");
			// ====================
	  } */
  }
  
  function get_forecastvsscheduleunit($bulan, $tahun, $id_kel_unit) {
	  
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// modif 17-09-2014
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		/*$jumstring = strlen($kodeunit);
		$new_kodeunit = substr($kodeunit, 0, $jumstring-1);
		
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun'
					AND a.id IN (SELECT distinct id_forecast_wip_unit FROM tm_forecast_wip_unit_listunit WHERE 
							kode_unit IN (".$new_kodeunit.") )
					ORDER BY d.kode, b.kode_brg_jadi "); */
		
		// ambil list unit jahit di tabel kel_forecast_wip_unit_listunit
		$query3	= $this->db->query(" SELECT a.kode_unit, b.nama FROM tm_kel_forecast_wip_unit_listunit a 
							INNER JOIN tm_unit_jahit b ON a.kode_unit = b.kode_unit 
							WHERE a.id_kel_forecast_wip_unit = '".$id_kel_unit."' ORDER BY a.kode_unit ");
							
		$kodeunit=""; $new_kodeunit="";
		if ($query3->num_rows() > 0){
			$hasil3 = $query3->result();
				
			foreach ($hasil3 as $row3) {
				$kodeunit.= "'".$row3->kode_unit."',";
			}
		}
		$jumstring = strlen($kodeunit);
		$new_kodeunit = substr($kodeunit, 0, $jumstring-1);
		
		$query	= $this->db->query(" SELECT b.*, c.e_product_motifname, d.kode FROM tm_forecast_wip_unit_detail b 
					INNER JOIN tm_forecast_wip_unit a ON b.id_forecast_wip_unit = a.id 
					INNER JOIN tr_product_motif c ON b.kode_brg_jadi = c.i_product_motif
					LEFT JOIN tm_kel_brg_jadi d ON d.id = c.id_kel_brg_jadi
					WHERE a.bulan = '$bulan' 
					AND a.tahun = '$tahun' AND a.id_kel_forecast_wip_unit = '$id_kel_unit'
					ORDER BY d.kode, b.kode_brg_jadi ");
	
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				// ambil SO di unit2 jahit yg dipilih di bulan sebelumnya
				$queryx	= $this->db->query(" SELECT sum(b.jum_bagus) as jumbagus FROM tt_stok_opname_unit_jahit a 
											INNER JOIN tt_stok_opname_unit_jahit_detail b ON a.id = b.id_stok_opname_unit_jahit
											WHERE b.kode_brg_jadi = '$row->kode_brg_jadi'
											AND a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.status_approve = 't'
											AND a.status_approve = 't'
											AND a.kode_unit IN (".$new_kodeunit.") ");
						
				if ($queryx->num_rows() > 0){
					$hasilrow = $queryx->row();
					$saldo_awal_bgs = $hasilrow->jumbagus;
					if ($saldo_awal_bgs == '')
						$saldo_awal_bgs = 0;
				}
				else {
					$saldo_awal_bgs = 0;
				}
				
				// KOLOM SCHEDULE ambil data barang masuk ke unit jahit
				//1. hitung brg masuk bagus (jenis=1) dari tm_bonmkeluarcutting
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk FROM tm_bonmkeluarcutting a 
									INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									WHERE a.tgl_bonm >='".$tahun."-".$bulan."-01' 
									AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.jenis_keluar = '1'
									AND a.kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$masuk_bgs = $hasilrow->jum_masuk;
							
							if ($masuk_bgs == '')
								$masuk_bgs = 0;
						}
						else
							$masuk_bgs = 0;
						
						$jum_masuk = $masuk_bgs;
						// ------------------------------------------ END MASUK --------------------------------
						
						// 4. hitung brg keluar bagus, dari tm_sjmasukwip
						$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_sjmasukwip a, 
									tm_sjmasukwip_detail b
									WHERE a.id = b.id_sjmasukwip AND a.tgl_sj >='".$tahun."-".$bulan."-01' 
									AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' AND b.kode_brg_jadi = '$row->kode_brg_jadi'
									AND a.kode_unit_jahit <> '0' AND a.jenis_masuk = '1'
									AND a.kode_unit_jahit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$keluar_bgs = $hasilrow->jum_keluar;
							
							if ($keluar_bgs == '')
								$keluar_bgs = 0;
						}
						else
							$keluar_bgs = 0;
						
						$jum_keluar = $keluar_bgs;
						
						// -------------------------------- END BARANG KELUAR -----------------------------------------------
						
						// tambahan 27-08-2014, kolom schedule dimodif perhitungannya
						//kolom schedule = brg masuk - stock schedule bln trsbt + stock schedule 1 bln sblmnya
						
						// a. stok schedule bulan tsb
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bulan' AND tahun='$tahun' AND kode_brg_jadi = '$row->kode_brg_jadi'
									AND kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule = $hasilrow->qty;
							
							if ($qty_schedule == '')
								$qty_schedule = 0;
						}
						else
							$qty_schedule = 0;
						
						// b. stok schedule 1 bulan sebelumnya
						if ($bulan == 1) {
							$bln_query = 12;
							$thn_query = $tahun-1;
						}
						else {
							$bln_query = $bulan-1;
							$thn_query = $tahun;
							if ($bln_query < 10)
								$bln_query = "0".$bln_query;
						}
						$query3	= $this->db->query(" SELECT sum(qty) as qty FROM tm_stok_schedule_wip
									WHERE bulan='$bln_query' AND tahun='$thn_query' AND kode_brg_jadi = '$row->kode_brg_jadi'
									AND kode_unit IN (".$new_kodeunit.") ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$qty_schedule_sebelumnya = $hasilrow->qty;
							
							if ($qty_schedule_sebelumnya == '')
								$qty_schedule_sebelumnya = 0;
						}
						else
							$qty_schedule_sebelumnya = 0;
						
						$totalqtyschedule = $jum_masuk-$qty_schedule+$qty_schedule_sebelumnya;
						//===========================================================
						if ($row->qty != 0) {
							//$persenschedule = ($jum_masuk/$row->qty)*100;
							$persenschedule = ($totalqtyschedule/$row->qty)*100;
							$persenforecast = ($jum_keluar/$row->qty)*100;
						}
						else {
						//echo $row->qty."<br>";
							$persenschedule = 0;
							$persenforecast = 0;
						}
						// 17-09-2014
						//echo $totalqtyschedule." ".$saldo_awal_bgs."<br>";
						$temptotal = $totalqtyschedule+$saldo_awal_bgs;
						if ($temptotal != 0)
							$persenforecast2 = ($jum_keluar/($totalqtyschedule+$saldo_awal_bgs))*100;
						else
							$persenforecast2 = 0;
						
				//17-09-2014, ambil nama kel brg jadi
				$sqlxx = " SELECT nama FROM tm_kel_brg_jadi WHERE kode='$row->kode' ";
				$queryxx	= $this->db->query($sqlxx);
				if ($queryxx->num_rows() > 0){
					$hasilxx = $queryxx->row();
					$nama_kel = $hasilxx->nama;
				}
				else {
					$nama_kel = '';
				}
				
				$datadetail[] = array( 
										'id'=> $row->id,
										'kode_brg_jadi'=> $row->kode_brg_jadi,
										'nama_brg_jadi'=> $row->e_product_motifname,
										'fc'=> $row->qty,
										'keterangan'=> $row->keterangan,
										//'schedule'=> $jum_masuk,
										'schedule'=> $totalqtyschedule,
										'brgmasuk'=> $jum_keluar,
										'persenschedule'=> $persenschedule,
										//'persenforecast'=> $persenforecast,
										'persenforecast2'=> $persenforecast2,
										'kode_kel'=> $row->kode,
										'nama_kel'=> $nama_kel,
										'saldo_awal_bgs'=> $saldo_awal_bgs
									);
			}
		}
		else {
			$datadetail = '';
		}
		return $datadetail;
  }
  
  // 30-09-2014
  function get_kel_unit_jahit(){
    $query = $this->db->query(" SELECT * FROM tm_kel_forecast_wip_unit ORDER BY kode");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  // 20-10-2014
  function get_transaksiglobal($bulan, $tahun) {
	  
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		// 1. minggu 1: tgl 1 s/d 7
		/*
			1.1. produksi ke unit jahit
			1.2. unit jahit ke wip
			1.3. wip ke packing
			1.4. gudang jadi ke distributor
		 */ 
		 //  the 'w' modifier, to get to number of the day (0 to 6, 0 being sunday, and 6 being saturday) :
		 $minggu = 1;
		for ($tgl=1; $tgl<=$lastDay; $tgl++) {
			if ($tgl<10)
				$tglx = "0".$tgl;
			else
				$tglx = $tgl;
			
			if ($tgl == 1)
				$awal = $tahun."-".$bulan."-".$tglx;
				
			$tglexist = $tahun."-".$bulan."-".$tglx;
			$timeexist = strtotime($tglexist);
			$hari = date("w", $timeexist);
			
			//echo $hari." ";
			if ($hari == '1') {
				$awal = $tahun."-".$bulan."-".$tglx;
			}
			
			if (($hari == '0' && $tgl != $lastDay) || ($hari == '0' && $tgl == $lastDay) || $tgl == $lastDay) {
				if ($hari == '0')
					$tglxx = $tgl-1;
				else
					$tglxx = $tgl;
				if ($tglxx < 10)
					$tglxx = "0".$tglxx;

				$akhir = $tahun."-".$bulan."-".$tglxx;
				// 1.1. produksi ke unit jahit
				$query = $this->db->query(" SELECT sum(b.qty) as produksi2unit FROM tm_bonmkeluarcutting a INNER JOIN 
											tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
											WHERE a.tgl_bonm >= '".$awal."' AND a.tgl_bonm <= '".$akhir."' ");
				if ($query->num_rows() > 0){
					$hasilrow = $query->row();
					$produksi2unit1 = $hasilrow->produksi2unit;
					if ($produksi2unit1 == '')
						$produksi2unit1 = 0;
				}
				else
					$produksi2unit1=0;
				
				// 1.2. unit jahit ke wip
				$query = $this->db->query(" SELECT sum(b.qty) as jahit2wip FROM tm_sjmasukwip a INNER JOIN 
											tm_sjmasukwip_detail b ON a.id = b.id_sjmasukwip
											WHERE a.tgl_sj >= '".$awal."' AND a.tgl_sj <= '".$akhir."'
											AND a.kode_unit_jahit <> '0' ");
				if ($query->num_rows() > 0){
					$hasilrow = $query->row();
					$jahit2wip1 = $hasilrow->jahit2wip;
					if ($jahit2wip1 == '')
						$jahit2wip1 = 0;
				}
				else
					$jahit2wip1=0;
				
				// 1.3. wip ke packing	
				/*echo "SELECT sum(b.qty) as wip2packing FROM tm_sjkeluarwip a INNER JOIN 
											tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
											WHERE a.tgl_sj >= '".$awal."' AND a.tgl_sj <= '".$akhir."'
											AND a.kode_unit_packing <> '0' <br>"; */
				$query = $this->db->query(" SELECT sum(b.qty) as wip2packing FROM tm_sjkeluarwip a INNER JOIN 
											tm_sjkeluarwip_detail b ON a.id = b.id_sjkeluarwip
											WHERE a.tgl_sj >= '".$awal."' AND a.tgl_sj <= '".$akhir."'
											AND a.kode_unit_packing <> '0' ");
				if ($query->num_rows() > 0){
					$hasilrow = $query->row();
					$wip2packing1 = $hasilrow->wip2packing;
					if ($wip2packing1 == '')
						$wip2packing1 = 0;
				}
				else
					$wip2packing1=0;
				
				// 1.4. gudang jadi ke distributor --> DO			
				$db2 = $this->load->database('db_external', TRUE);
				$sqlx = " SELECT sum(b.n_deliver) as gdjadi2dist FROM tm_do a INNER JOIN 
											tm_do_item b ON a.i_do = b.i_do
											WHERE a.d_do >= '".$awal."' AND a.d_do <= '".$akhir."'
											AND a.f_do_cancel='f' ";
				$queryx	= $db2->query($sqlx);
						
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$gdjadi2dist1 = $hasilx->gdjadi2dist;
					if ($gdjadi2dist1 == '')
						$gdjadi2dist1 = 0;
				}
				else
					$gdjadi2dist1 = 0;
				
				$datadetail[] = array( 		'produksi2unit1'=> $produksi2unit1,
									'jahit2wip1'=> $jahit2wip1,
									'wip2packing1'=> $wip2packing1,
									'gdjadi2dist1'=> $gdjadi2dist1,
									'minggu'=> $minggu,
									'awal'=> $awal,
									'akhir'=> $akhir
									);
				$minggu++;
			} // end if hari
				
		} // end for
		return $datadetail;
  }
  
  // 16-10-2015
  function get_transaksi_hasil_cutting($bulan, $tahun, $id_brg_wip) {		
		$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
		$firstDay            =     date('d',$timeStamp);    //get first day of the given month
		list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
		$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
		$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
		
		if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
				
		if ($bulan == 1) {
			$bln_query = 12;
			$thn_query = $tahun-1;
		}
		else {
			$bln_query = $bulan-1;
			$thn_query = $tahun;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
		
		// NEW
	/*	$saldo = 0; $tot_masuk1 = 0; $tot_masuk2 = 0; $tot_keluar = 0;
								
		// ambil SO brg dari tt_stok_opname_bahan_baku (SO ini udah dalam satuan konversi)
		$sqlso = " SELECT b.jum_stok_opname 
							FROM tt_stok_opname_bahan_baku a INNER JOIN tt_stok_opname_bahan_baku_detail b
							ON a.id = b.id_stok_opname_bahan_baku
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg = '$id_brg' ";
		$queryso	= $this->db->query($sqlso);
		if ($queryso->num_rows() > 0){
			$hasilso = $queryso->row();
			$jum_stok_opname = $hasilso->jum_stok_opname;
			if ($jum_stok_opname == '')
				$jum_stok_opname = 0;
		}
		else
			$jum_stok_opname = 0;
		
		$saldo = $jum_stok_opname; */
		
		$data_brg_wip = array();
		// 1. ambil data2 stok di unit jahit
		//$sql = " SELECT id, kode_brg_jadi FROM tm_stok_unit_jahit WHERE kode_unit = '$unit_jahit' ORDER BY kode_brg_jadi ";
		//$query	= $this->db->query($sql);
		//$data_brg_jadi = array();
		//if ($query->num_rows() > 0){
			//$hasil = $query->result();
			//foreach ($hasil as $row) {
				$data_warna = array();
				$data_so_warna = array();
				
				// 16-10-2015 AMBIL DARI tm_warna
				$sqlwarna = " SELECT a.id_warna, b.nama FROM tm_warna_brg_wip a INNER JOIN tm_warna b ON a.id_warna = b.id
							WHERE a.id_brg_wip ='$id_brg_wip' ORDER BY b.nama ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						$data_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'saldo'=> 0,
												'tot_masuk1'=> 0,
												'tot_keluar1'=> 0,
												'tot_masuk_lain1'=> 0,
												'tot_keluar_lain1'=> 0,
												'tot_masuk_retur1'=> 0,
												'tot_keluar_retur1'=> 0
											);
					}
				}
				
				// ambil SO dari tt_stok_opname_hasil_cutting_detail_warna
				$sqlwarna = " SELECT c.id_warna, d.nama, c.jum_stok_opname
							FROM tt_stok_opname_hasil_cutting a INNER JOIN tt_stok_opname_hasil_cutting_detail b ON a.id = b.id_stok_opname_hasil_cutting
							INNER JOIN tt_stok_opname_hasil_cutting_detail_warna c ON b.id = c.id_stok_opname_hasil_cutting_detail
							INNER JOIN tm_warna d ON d.id = c.id_warna
							WHERE a.bulan='$bln_query' AND a.tahun='$thn_query' 
							AND b.id_brg_wip = '$id_brg_wip' ";
				$querywarna	= $this->db->query($sqlwarna);
				if ($querywarna->num_rows() > 0){
					$hasilwarna = $querywarna->result();
					foreach ($hasilwarna as $rowwarna) {
						
						for ($xx=0; $xx<count($data_warna); $xx++) {
							if ($data_warna[$xx]['id_warna'] == $rowwarna->id_warna) {
								$data_warna[$xx]['saldo']= $rowwarna->jum_stok_opname;
							}
						} // end for
						
						$data_so_warna[] = array(	'id_warna'=> $rowwarna->id_warna,
												'nama_warna'=> $rowwarna->nama,
												'jum_stok_opname'=> $rowwarna->jum_stok_opname
											);
					}
				}
				
				// ambil nama brg wip
				$query3	= $this->db->query(" SELECT kode_brg, nama_brg FROM tm_barang_wip WHERE id = '$id_brg_wip' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$nama_brg_wip = $hasilrow->nama_brg;
					$kode_brg_wip = $hasilrow->kode_brg;
				}
				else {
					$kode_brg_wip = '';
					$nama_brg_wip = '';
				}
								
				// 1. QUERY UTK KELUAR MASUK BRG BERDASARKAN TANGGAL YG DIPILIH
				$sql2 = " SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, b.keterangan, 0 as id_unit, 1 as masuk_bgs, 0 as masuk_lain, 0 as masuk_retur, 0 as keluar_bgs, 0 as keluar_lain, 0 as keluar_retur
						FROM tm_bonmmasukcutting a INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting 
						WHERE a.jenis_masuk = '1' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, b.keterangan, 0 as id_unit, 0 as masuk_bgs, 1 as masuk_lain, 0 as masuk_retur, 0 as keluar_bgs, 0 as keluar_lain, 0 as keluar_retur
						FROM tm_bonmmasukcutting a INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting 
						WHERE a.jenis_masuk = '2' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."'
						UNION SELECT distinct a.id, a.no_sj, a.tgl_sj, b.keterangan, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 1 as masuk_retur, 0 as keluar_bgs, 0 as keluar_lain, 0 as keluar_retur
						FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic 
						WHERE b.id_brg_wip='$id_brg_wip'
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, b.keterangan, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_retur, 1 as keluar_bgs, 0 as keluar_lain, 0 as keluar_retur
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.jenis_keluar = '1' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, b.keterangan, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_retur, 0 as keluar_bgs, 1 as keluar_lain, 0 as keluar_retur
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.jenis_keluar = '3' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						UNION SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, b.keterangan, a.id_unit, 0 as masuk_bgs, 0 as masuk_lain, 0 as masuk_retur, 0 as keluar_bgs, 0 as keluar_lain, 1 as keluar_retur
						FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting 
						WHERE a.jenis_keluar = '2' AND b.id_brg_wip='$id_brg_wip'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						ORDER BY tgl_sj ASC ";
				
			/*	$sql2 = " SELECT distinct a.id, a.no_sj, a.tgl_sj, 1 as masukwip, 0 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_sjmasukwip a, tm_sjmasukwip_detail b 
						WHERE a.id = b.id_sjmasukwip AND a.jenis_masuk = '1'
						 AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						 AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit_jahit='$unit_jahit'
						UNION
						SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, 0 as masukwip, 1 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
						WHERE a.id = b.id_bonmkeluarcutting AND a.jenis_keluar = '1'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						
						UNION
						SELECT distinct a.id, a.no_manual as no_sj, a.tgl_bonm as tgl_sj, 0 as masukwip, 0 as bonmkeluarcutting, 1 as bonmkeluarcutting2, 0 as sjmasukbhnbakupic  
						FROM tm_bonmkeluarcutting a, tm_bonmkeluarcutting_detail b
						WHERE a.id = b.id_bonmkeluarcutting AND a.jenis_keluar = '2'
						AND a.tgl_bonm >='".$tahun."-".$bulan."-01' AND a.tgl_bonm <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						
						UNION
						SELECT distinct a.id, a.no_sj, a.tgl_sj, 0 as masukwip, 0 as bonmkeluarcutting, 0 as bonmkeluarcutting2, 1 as sjmasukbhnbakupic 
						FROM tm_sjmasukbhnbakupic a, tm_sjmasukbhnbakupic_detail b
						WHERE a.id = b.id_sjmasukbhnbakupic
						AND a.tgl_sj >='".$tahun."-".$bulan."-01' AND a.tgl_sj <='".$tahun."-".$bulan."-".$lastDay."' 
						AND b.kode_brg_jadi='$row->kode_brg_jadi' AND a.kode_unit='$unit_jahit'
						ORDER BY tgl_sj ASC
						 "; // 10-05-2014 contoh union di modul info-pembelian mmaster.php.
						 */
				
				$query2	= $this->db->query($sql2);
				$data_tabel1 = array();
				if ($query2->num_rows() > 0){
					$hasil2 = $query2->result();
					
					foreach ($hasil2 as $row2) {
						$id_data = $row2->id;
						$no_sj = $row2->no_sj;
						$tgl_sj = $row2->tgl_sj;
						$keterangan = $row2->keterangan;
						$id_unit = $row2->id_unit;
						$masuk_bgs = $row2->masuk_bgs;
						$masuk_lain = $row2->masuk_lain;
						$masuk_retur = $row2->masuk_retur;
						$keluar_bgs = $row2->keluar_bgs;
						$keluar_lain = $row2->keluar_lain;
						$keluar_retur = $row2->keluar_retur;
						
						$pisah1 = explode("-", $tgl_sj);
						$tgl1= $pisah1[2];
						$bln1= $pisah1[1];
						$thn1= $pisah1[0];
						$tgl_sj = $tgl1."-".$bln1."-".$thn1;
						// sampe sini 4:07 16-10-2015 lanjut besok
						// lanjut 17-10-2015
						
						if ($masuk_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmmasukcutting a INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
									INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '1'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmmasukcutting a INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
									INNER JOIN tm_bonmmasukcutting_detail_warna c ON b.id = c.id_bonmmasukcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_masuk = '2'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						if ($masuk_retur == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_sjmasukbhnbakupic a INNER JOIN tm_sjmasukbhnbakupic_detail b ON a.id = b.id_sjmasukbhnbakupic
									INNER JOIN tm_sjmasukbhnbakupic_detail_warna c ON b.id = c.id_sjmasukbhnbakupic_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip'
									ORDER BY d.nama ";
							$masuk = "ya";
							$keluar = "tidak";
						}
						
						// keluar
						if ($keluar_bgs == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '1'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						if ($keluar_lain == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '3'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						
						if ($keluar_retur == '1') {
							$sql3 = " SELECT c.id_warna, d.nama, c.qty FROM tm_bonmkeluarcutting a INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
									INNER JOIN tm_bonmkeluarcutting_detail_warna c ON b.id = c.id_bonmkeluarcutting_detail
									INNER JOIN tm_warna d ON d.id = c.id_warna
									WHERE a.id = '$id_data' AND b.id_brg_wip = '$id_brg_wip' AND a.jenis_keluar = '2'
									ORDER BY d.nama ";
							$masuk = "tidak";
							$keluar = "ya";
						}
						// --------------------
						
						$query3	= $this->db->query($sql3);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
						if ($query3->num_rows() > 0){
							$hasil3 = $query3->result();
								
							foreach ($hasil3 as $row3) {
								$nama_warna = $row3->nama;
								
								// saldo per row
								for ($xx=0; $xx<count($data_warna); $xx++) {
									if ($data_warna[$xx]['id_warna'] == $row3->id_warna) {
										if ($masuk == "ya") {
											$data_warna[$xx]['saldo']+= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										else {
											$data_warna[$xx]['saldo']-= $row3->qty;
											$saldo = $data_warna[$xx]['saldo'];
											$data_tabel1_saldo_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'saldo'=> $saldo
															);
										}
										
										if ($masuk_bgs == '1') {
											$data_warna[$xx]['tot_masuk1']+= $row3->qty;
										}
										else if ($masuk_lain== '1') {
											$data_warna[$xx]['tot_masuk_lain1']+= $row3->qty;
										}
										else if ($masuk_retur == '1') {
											$data_warna[$xx]['tot_masuk_retur1']+= $row3->qty;
										}
										else if ($keluar_bgs == '1') {
											$data_warna[$xx]['tot_keluar1']+= $row3->qty;
										}
										else if ($keluar_lain == '1') {
											$data_warna[$xx]['tot_keluar_lain1']+= $row3->qty;
										}
										else if ($keluar_retur == '1') {
											$data_warna[$xx]['tot_keluar_retur1']+= $row3->qty;
										}
									}
								}
								
								$data_tabel1_perwarna[] = array('id_warna'=> $row3->id_warna,
																'nama_warna'=> $nama_warna,
																'qty'=> $row3->qty
															);
							} // end foreach
						}
						
						if ($id_unit != 0) {
							$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$id_unit' ");
							$hasilrow = $query3->row();
							$kode_unit	= $hasilrow->kode_unit;
							$nama_unit	= $hasilrow->nama;
						}
						else {
							$kode_unit = '';
							$nama_unit = '';
						}
						
						$data_tabel1[] = array(	'no_sj'=> $no_sj,
												'tgl_sj'=> $tgl_sj,
												'keterangan'=> $keterangan,
												'id_unit'=> $id_unit,
												'kode_unit'=> $kode_unit,
												'nama_unit'=> $nama_unit,
												'masuk_bgs'=> $masuk_bgs,
												'masuk_lain'=> $masuk_lain,
												'masuk_retur'=> $masuk_retur,
												'keluar_bgs'=> $keluar_bgs,
												'keluar_lain'=> $keluar_lain,
												'keluar_retur'=> $keluar_retur,
												'masuk'=> $masuk,
												'keluar'=> $keluar,
												'data_tabel1_perwarna'=> $data_tabel1_perwarna,
												'data_tabel1_saldo_perwarna'=> $data_tabel1_saldo_perwarna
										);
						$data_tabel1_perwarna = array();
						$data_tabel1_saldo_perwarna = array();
					} // end for2
				}
				else
					$data_tabel1='';
		
		// -----------------------------------------------------------------------------------------------------------------
				// =================================== END NEW =======================================================
				
				// array brg wip
				$data_brg_wip[] = array(		'id_brg_wip'=> $id_brg_wip,
												'kode_brg_wip'=> $kode_brg_wip,
												'nama_brg_wip'=> $nama_brg_wip,
												'data_tabel1'=> $data_tabel1,
												'data_warna'=> $data_warna,
												'data_so_warna'=> $data_so_warna
									);
				$data_tabel1 = array();
				$data_warna = array();
				$data_so_warna = array();
			//} // endforeach header
		//}
		//else {
			//$data_brg_jadi = '';
		//}
		return $data_brg_wip;
  }
  
  // 22-10-2015
  function get_all_cuttingvssj($date_from, $date_to) {
		$sql=" SELECT distinct b.id_brg_wip, c.kode_brg, c.nama_brg FROM tm_bonmmasukcutting a 
				INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				WHERE (a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')) 
				AND (a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')) 
				ORDER BY c.kode_brg ";
										
		$query	= $this->db->query($sql);
		
		$data = array();
		$detail = array();
		
		if ($query->num_rows() > 0) {
			$hasil = $query->result();
			
			$datadetail = array();			
			foreach ($hasil as $row) {
				$id_brg_wip = $row->id_brg_wip;
				$kode_brg_wip = $row->kode_brg;
				$nama_brg_wip = $row->nama_brg;
				
				// 1. ambil data jumlah hasil cutting di range tgl
				$sqlcutting=" SELECT sum(b.qty) as jumcutting FROM tm_bonmmasukcutting a 
						INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
						WHERE (a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')) 
						AND (a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')) AND b.id_brg_wip='".$id_brg_wip."' ";
					
				$queryx	= $this->db->query($sqlcutting);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_cutting = $hasilx->jumcutting;
					if ($jum_cutting == '')
						$jum_cutting = 0;
				}
				else
					$jum_cutting = 0;
				
				// 2. ambil data jumlah SJ keluar ke jahitan di range tgl
				$sqlcutting=" SELECT sum(b.qty) as jumkirim FROM tm_bonmkeluarcutting a 
						INNER JOIN tm_bonmkeluarcutting_detail b ON a.id = b.id_bonmkeluarcutting
						WHERE (a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')) 
						AND (a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')) AND b.id_brg_wip='".$id_brg_wip."' ";
					
				$queryx	= $this->db->query($sqlcutting);
				if ($queryx->num_rows() > 0){
					$hasilx = $queryx->row();
					$jum_kirim = $hasilx->jumkirim;
					if ($jum_kirim == '')
						$jum_kirim = 0;
				}
				else
					$jum_kirim = 0;	
				// ====================================================================================================
				
				$data[] = array('kode_brg_wip'=> $kode_brg_wip,
								'nama_brg_wip'=> $nama_brg_wip,
								'jum_cutting'=> $jum_cutting,
								'jum_kirim'=> $jum_kirim
				);
			}
			
		}else{
			$data = '';
		}
		
		return $data;
  }
  
  function get_all_cuttingvssjtanpalimit($date_from, $date_to) {
	$sql=" SELECT distinct b.id_brg_wip, c.kode_brg, c.nama_brg FROM tm_bonmmasukcutting a 
				INNER JOIN tm_bonmmasukcutting_detail b ON a.id = b.id_bonmmasukcutting
				INNER JOIN tm_barang_wip c ON c.id = b.id_brg_wip
				WHERE (a.tgl_bonm >= to_date('$date_from','dd-mm-yyyy')) 
				AND (a.tgl_bonm <= to_date('$date_to','dd-mm-yyyy')) 
				ORDER BY c.kode_brg ";
		
	$query	= $this->db->query($sql);
	    
    return $query->result();  
    
  }
}
