<?php
class Mmaster extends CI_Model{
  function __construct() { 

  parent::__construct();

}
  
  function getAll($num, $offset, $gudang, $cari) {
	if ($cari == "all") {
		if ($gudang == '0') {
			/*$this->db->select(" distinct a.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
			WHERE a.id = b.id_apply_stok AND b.status_stok = 't' ORDER BY a.id_gudang, a.kode_supplier ", false)->limit($num,$offset); */
			
			$sql = " distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' ORDER BY b.id DESC ";
						
			$this->db->select($sql, false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$sql = " distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' AND a.id_gudang = '$gudang' ORDER BY b.id DESC ";
			
			$this->db->select($sql, false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
	else {
		if ($gudang != '0') {
			$sql = " distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' AND a.id_gudang = '$gudang' AND UPPER(b.no_bonm) like UPPER('%$cari%') ORDER BY b.id DESC ";
			
			$this->db->select($sql, false)->limit($num,$offset);
			$query = $this->db->get();
		}
		else {
			$sql = " distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' AND UPPER(b.no_bonm) like UPPER('%$cari%') ORDER BY b.id DESC ";
			
			$this->db->select($sql, false)->limit($num,$offset);
			$query = $this->db->get();
		}
	}
		$data_fb = array();
		$detail_fb = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail WHERE no_bonm = '$row1->no_bonm' 
				AND status_stok = 't' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
									
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND a.kode_brg = '$row2->kode_brg' ");
						if ($query3->num_rows() > 0){
							$hasilrow = $query3->row();
							$nama_brg	= $hasilrow->nama_brg;
							$satuan	= $hasilrow->nama_satuan;
						}
						else {
							$nama_brg	= '';
							$satuan	= '';
						}
				
						$detail_fb[] = array('kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_fb = '';
				}
				 
				// ambil data nama gudang
					$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
							FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row1->id_gudang' ");
					$hasilrow = $query3->row();
					$kode_gudang	= $hasilrow->kode_gudang;
					$nama_gudang	= $hasilrow->nama;
					$nama_lokasi	= $hasilrow->nama_lokasi; 
				
				//ambil no sj dan kode supplier
				$query3	= $this->db->query(" SELECT no_sj, kode_supplier 
							FROM tm_apply_stok_pembelian WHERE id = '$row1->id_apply_stok' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$no_sj	= $hasilrow->no_sj;
						$kode_supplier	= $hasilrow->kode_supplier;
				 }
				 else {
					$no_sj = '';
					$kode_supplier	= '';
				 }
				
				//ambil tgl SJ
				$query3	= $this->db->query(" SELECT tgl_sj, stok_masuk_lain_cash, stok_masuk_lain_kredit 
							FROM tm_pembelian WHERE no_sj = '$no_sj' 
							AND kode_supplier = '$kode_supplier' ");
				 if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$tgl_sj	= $hasilrow->tgl_sj;
						$lain_cash	= $hasilrow->stok_masuk_lain_cash;
						$lain_kredit	= $hasilrow->stok_masuk_lain_kredit;
				 }
				 else {
					$tgl_sj = '';
					$lain_cash	= 'f';
					$lain_kredit	= 'f';
				 }
				 				 
				$data_fb[] = array(			'id'=> $row1->id,	
											'no_sj'=> $no_sj,	
											'tgl_sj'=> $tgl_sj,	
											'lain_cash'=> $lain_cash,	
											'lain_kredit'=> $lain_kredit,	
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											'kode_supplier'=> $kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'kode_gudang'=> $kode_gudang,
											'nama_gudang'=> $nama_gudang,
											'nama_lokasi'=> $nama_lokasi,
											'detail_fb'=> $detail_fb
											);
				$detail_fb = array();
			} // endforeach header
		}
		else {
			$data_fb = '';
		}
		return $data_fb;
  }
  
  function getAlltanpalimit($gudang, $cari){
	if ($cari == "all") {
		if ($gudang == '0') {
			$sql = " SELECT distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' ORDER BY b.id DESC ";
			$query	= $this->db->query($sql);
		}
		else {
			$sql = " SELECT distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' AND a.id_gudang = '$gudang' ORDER BY b.id DESC ";
			$query	= $this->db->query($sql);
		}
	}
	else {
		if ($gudang != '0') {
			$sql = " SELECT distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' AND a.id_gudang = '$gudang' AND UPPER(b.no_bonm) like UPPER('%$cari%') ORDER BY b.id DESC ";
			
			$query	= $this->db->query($sql);
		}
		else {
			$sql = " SELECT distinct b.id, b.no_bonm, b.tgl_bonm, b.id_apply_stok FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b
			WHERE a.id = b.id_apply_stok AND a.status_aktif = 't' AND a.no_bonm <> b.no_bonm AND
			b.status_stok = 't' AND a.stok_lain = 't' AND UPPER(b.no_bonm) like UPPER('%$cari%') ORDER BY b.id DESC ";
			
			$query	= $this->db->query($sql);
		}
	}
    
    return $query->result();  
  }
            
  //function get_sj($num, $offset, $kode_supplier, $cari) {
  function get_sj($kode_supplier, $cari) {
	  // ambil data SJ yg stok_masuk_lain_kredit = 'f'
	if ($cari == "all") {		
		if ($kode_supplier == '0')
			$this->db->select(" * FROM tm_pembelian WHERE stok_masuk_lain_kredit = 't' AND status_stok = 'f' 
								AND status_aktif = 't' order by id DESC ", false);
		else
			$this->db->select(" * FROM tm_pembelian WHERE stok_masuk_lain_kredit = 't' AND status_stok = 'f' 
								AND status_aktif = 't' AND kode_supplier = '$kode_supplier' order by id DESC ", false);
		
		$query = $this->db->get();
	}
	else {
		if ($kode_supplier == '0')
			$this->db->select(" * FROM tm_pembelian WHERE stok_masuk_lain_kredit = 't' AND status_stok = 'f' 
							AND status_aktif = 't' AND UPPER(no_sj) like UPPER('%$cari%') order by id DESC ", false);
		else
			$this->db->select(" * FROM tm_pembelian WHERE stok_masuk_lain_kredit = 't' AND status_stok = 'f' 
							AND status_aktif = 't' AND kode_supplier = '$kode_supplier' AND UPPER(no_sj) like UPPER('%$cari%') order by id DESC ", false);
		
		$query = $this->db->get();
	}
		$data_sj = array();
		$detail_sj = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
							
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_pembelian_detail WHERE id_pembelian = '$row1->id' 
								AND status_stok = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						
						// -------------------
						
						/* $query3	= $this->db->query(" SELECT a.qty FROM tm_pp_detail a, tm_pp b 
									WHERE b.id = '$id_pp' AND a.id_pp = b.id AND a.kode_brg = '$kode' ");
						$hasilrow = $query3->row();
						$qty_pp = $hasilrow->qty; // ini qty di PP detail berdasarkan kode brgnya
						*/
						
					/*	$query3	= $this->db->query(" SELECT sum(a.qty) as jum FROM tm_op_detail a, tm_op b 
									WHERE a.id_op = b.id AND b.id_pp = '$row1->id' AND a.kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$jum_op = $hasilrow->jum; // ini sum qty di OP berdasarkan kode brg tsb
						
						// ambil sum qty di SJ berdasarkan kode brgnya (140511) #######################################################
						$sql3 = " SELECT sum(a.qty) as jum FROM tm_pembelian_detail a, tm_pembelian b 
									WHERE a.id_pembelian = b.id ";
						//if ($cek_pp == 0)			
						//	$sql3.= " AND b.id_op = '$row1->id' ";
						//else
							$sql3.= " AND b.id_pp = '$row1->id' ";
							
						$sql3.= " AND a.kode_brg = '$row2->kode_brg' ";
						
						$query3	= $this->db->query($sql3);
						$hasilrow = $query3->row();
						$jum_ppsj = $hasilrow->jum; // ini sum qty di Pembelian berdasarkan kode brg tsb
						// #######################################################
						
						$qty = $row2->qty-$jum_op-$jum_ppsj; */
						
						//--------------------
						
						$detail_sj[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_sj = '';
				}
				
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
				
				$data_sj[] = array(			'id'=> $row1->id,	
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $row1->tgl_sj,
											'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'detail_sj'=> $detail_sj
											);
				$detail_sj = array();
			} // endforeach header
		}
		else {
			$data_sj = '';
		}
		return $data_sj;
  }
  
  function get_sjtanpalimit($kode_supplier, $cari){
	if ($cari == "all") {
		if ($kode_supplier == '0')
			$query = $this->db->getwhere('tm_pembelian',array('stok_masuk_lain_kredit'=>"t",
														  'status_stok'=>"f",
														  'status_aktif'=>"t" ));
		else
			$query = $this->db->getwhere('tm_pembelian',array('stok_masuk_lain_kredit'=>"t",
														  'status_stok'=>"f",
														  'status_aktif'=>"t",
														  'kode_supplier'=>$kode_supplier));
		
	}
	else {
		if ($kode_supplier == '0')
			$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE stok_masuk_lain_kredit = 't' AND status_stok = 'f' 
					AND status_aktif = 't' AND UPPER(no_sj) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT * FROM tm_pembelian WHERE stok_masuk_lain_kredit = 't' AND status_stok = 'f' 
					AND status_aktif = 't' AND kode_supplier = '$kode_supplier' AND UPPER(no_sj) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function get_detail_bonm($id_gudang, $list_bonm){
    $detail_bonm = array();

    foreach($list_bonm as $row1) {
			$row1 = trim($row1);
			if ($row1 != '') {
				$query2	= $this->db->query(" SELECT b.kode_brg, b.qty, b.no_bonm, b.tgl_bonm, b.id, b.id_apply_stok, b.harga,
							a.no_sj, a.kode_supplier
							FROM tm_apply_stok_pembelian_detail b, tm_apply_stok_pembelian a where a.id = b.id_apply_stok 
							AND a.no_bonm = '$row1' AND a.id_gudang = '$id_gudang' ");
				
				$hasil2 = $query2->result();
				foreach($hasil2 as $row2) {
					$query2	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
								WHERE b.id = a.satuan AND a.kode_brg = '$row2->kode_brg' ");
					$hasilrow = $query2->row();
					$nama_brg	= $hasilrow->nama_brg;
					$nama_satuan	= $hasilrow->nama_satuan;
					
					$pisah1 = explode("-", $row2->tgl_bonm);
					$tgl1= $pisah1[0];
					$bln1= $pisah1[1];
					$thn1= $pisah1[2];
					$tgl_bonm = $thn1."-".$bln1."-".$tgl1;
					
					$detail_bonm[] = array('no_bonm'=> $row2->no_bonm,
										'id'=> $row2->id,
										'id_apply_stok'=> $row2->id_apply_stok,
										'kode_brg'=> $row2->kode_brg,
										'nama'=> $nama_brg,
										'nama_satuan'=> $nama_satuan,
										'qty'=> $row2->qty,
										'harga'=> $row2->harga,
										'tgl_bonm'=> $tgl_bonm,
										'no_sj'=> $row2->no_sj,
										'kode_supplier'=> $row2->kode_supplier
								);
				}
			
		}
	}
	return $detail_bonm;
  }
  
  function get_detail_sj($kode_supplier, $list_sj){
    $detail_sj = array();

    foreach($list_sj as $row1) {
		$row1 = trim($row1);
			if ($row1 != '') {
				$query2	= $this->db->query(" SELECT b.kode_brg, b.qty, a.no_sj, a.tgl_sj, b.id, b.id_pembelian, b.harga 
							FROM tm_pembelian_detail b, tm_pembelian a where a.id = b.id_pembelian 
							AND b.status_stok = 'f' AND a.stok_masuk_lain_kredit = 't'
							AND a.no_sj = '$row1' AND a.kode_supplier = '$kode_supplier' ");
				
				$hasil2 = $query2->result();
				foreach($hasil2 as $row2) {
					$query2	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
								WHERE b.id = a.satuan AND a.kode_brg = '$row2->kode_brg' ");
					$hasilrow = $query2->row();
					$nama_brg	= $hasilrow->nama_brg;
					$nama_satuan	= $hasilrow->nama_satuan;
					
					$pisah1 = explode("-", $row2->tgl_sj);
					$tgl1= $pisah1[0];
					$bln1= $pisah1[1];
					$thn1= $pisah1[2];
					$tgl_sj = $thn1."-".$bln1."-".$tgl1;
					
					$detail_sj[] = array('no_sj'=> $row2->no_sj,
										'id'=> $row2->id,
										'id_pembelian'=> $row2->id_pembelian,
										'kode_brg'=> $row2->kode_brg,
										'nama'=> $nama_brg,
										'nama_satuan'=> $nama_satuan,
										'qty'=> $row2->qty,
										'harga'=> $row2->harga,
										'tgl_sj'=> $tgl_sj
								);
				}
			
		}
	}
	return $detail_sj;
  }
  
  function konversi_angka2romawi($bln_now) {
	if ($bln_now == "01")
		$romawi_bln = "I";
	else if ($bln_now == "02")
		$romawi_bln = "II";
	else if ($bln_now == "03")
		$romawi_bln = "III";
	else if ($bln_now == "04")
		$romawi_bln = "IV";
	else if ($bln_now == "05")
		$romawi_bln = "V";
	else if ($bln_now == "06")
		$romawi_bln = "VI";
	else if ($bln_now == "07")
		$romawi_bln = "VII";
	else if ($bln_now == "08")
		$romawi_bln = "VIII";
	else if ($bln_now == "09")
		$romawi_bln = "IX";
	else if ($bln_now == "10")
		$romawi_bln = "X";
	else if ($bln_now == "11")
		$romawi_bln = "XI";
	else if ($bln_now == "12")
		$romawi_bln = "XII";
		
	return $romawi_bln;
  }
  
  function konversi_romawi2angka($blnrom) {
	if ($blnrom == "I")
		$bln_now = "01";
	else if ($blnrom == "II")
		$bln_now = "02";
	else if ($blnrom == "III")
		$bln_now = "03";
	else if ($blnrom == "IV")
		$bln_now = "04";
	else if ($blnrom == "V")
		$bln_now = "05";
	else if ($blnrom == "VI")
		$bln_now = "06";
	else if ($blnrom == "VII")
		$bln_now = "07";
	else if ($blnrom == "VIII")
		$bln_now = "08";
	else if ($blnrom == "IX")
		$bln_now = "09";
	else if ($blnrom == "X")
		$bln_now = "10";
	else if ($blnrom == "XI")
		$bln_now = "11";
	else if ($blnrom == "XII")
		$bln_now = "12";
		
	return $bln_now;
  }
  
  function konversi_bln2deskripsi($bln1) {
				if ($bln1 == '01')
					$nama_bln = "Januari";
				else if ($bln1 == '02')
					$nama_bln = "Februari";
				else if ($bln1 == '03')
					$nama_bln = "Maret";
				else if ($bln1 == '04')
					$nama_bln = "April";
				else if ($bln1 == '05')
					$nama_bln = "Mei";
				else if ($bln1 == '06')
					$nama_bln = "Juni";
				else if ($bln1 == '07')
					$nama_bln = "Juli";
				else if ($bln1 == '08')
					$nama_bln = "Agustus";
				else if ($bln1 == '09')
					$nama_bln = "September";
				else if ($bln1 == '10')
					$nama_bln = "Oktober";
				else if ($bln1 == '11')
					$nama_bln = "November";
				else if ($bln1 == '12')
					$nama_bln = "Desember";  
		return $nama_bln;
  }
  
  function get_supplier(){
	$query	= $this->db->query(" SELECT * FROM tm_supplier WHERE kategori='1' ORDER BY kode_supplier ");    
    return $query->result();  
  }
  
  function create_bonm_update_stok($kode_supplier, $date_stok, $no_bonm, $tgl_bonm, 
						$kode, $no_sj, $id_detailnya, $id_pembelian, $qty, $harga){  
    $tgl = date("Y-m-d");
    $th_now	= date("Y");
	
		// ====================================================
			// 1. insert Bon M masuk secara otomatis di tm_apply_stok_pembelian, sekaligus update stok
			
				// ambil data gudang sesuai barangnya
				$query3	= $this->db->query(" SELECT id_gudang FROM tm_barang WHERE kode_brg = '$kode' ");
				$hasilrow = $query3->row();
				$id_gudang = $hasilrow->id_gudang;
				
				// cek apakah header bonm udh ada
				$query3	= $this->db->query(" SELECT id, no_bonm FROM tm_apply_stok_pembelian WHERE no_sj = '$no_sj' 
				AND kode_supplier = '$kode_supplier' AND id_gudang = '$id_gudang' ");
				if ($query3->num_rows() > 0) { // jika udh ada
					$hasilrow = $query3->row();
					$no_bonm1 = $hasilrow->no_bonm; // no_bonm yg ini ga dipake di tm_apply_stok_pembelian_detail
					$id_apply_stok = $hasilrow->id;
					
					//save ke tabel tm_apply_stok_pembelian_detail
					if ($kode!='' && $qty!='') {
						// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'harga'=>$harga,
							'id_apply_stok'=>$id_apply_stok,
							'no_bonm'=>$no_bonm,
							'tgl_bonm'=>$tgl_bonm,
							'status_stok'=> 't'
						);
						$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
						
						// ##############################################################################
						// update lagi di tm_pembelian_detail
						$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 't' WHERE id = '$id_detailnya' ");
									
						//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
						$query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE status_stok = 'f' 
											AND id_pembelian = '$id_pembelian' ");
						if ($query3->num_rows() == 0){
							$this->db->query(" UPDATE tm_pembelian SET status_stok = 't' WHERE id = '$id_pembelian' ");
						}
						
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$qty;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
							$data_stok = array(
								'kode_brg'=>$kode,
								'stok'=>$new_stok,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok',$data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode' ");
						}
						
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, kode_supplier, no_bukti, masuk_lain, saldo, tgl_input, harga) 
										VALUES ('$kode', '$kode_supplier', '$no_bonm', '$qty', '$new_stok', '$date_stok', '$harga' ) ");
						// ##############################################################################
						
					} // end if kode != '' dan $qty != ''
				}
				else {
					// generate no Bon M
					$query3	= $this->db->query(" SELECT no_bonm FROM tm_apply_stok_pembelian WHERE id_gudang = '$id_gudang' 
					ORDER BY no_bonm DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					if ($query3->num_rows() != 0) 
						$no_bonm	= $hasilrow->no_bonm;
					else
						$no_bonm = '';
					if(strlen($no_bonm)==12) {
						$nobonm = substr($no_bonm, 3, 9);
						$n_bonm	= (substr($nobonm,4,5))+1;
						$th_bonm	= substr($nobonm,0,4);
						if($th_now==$th_bonm) {
								$jml_n_bonm	= $n_bonm; //
								switch(strlen($jml_n_bonm)) {
									case "1": $kodebonm	= "0000".$jml_n_bonm;
									break;
									case "2": $kodebonm	= "000".$jml_n_bonm;
									break;	
									case "3": $kodebonm	= "00".$jml_n_bonm;
									break;
									case "4": $kodebonm	= "0".$jml_n_bonm;
									break;
									case "5": $kodebonm	= $jml_n_bonm;
									break;	
								}
								$nomorbonm = $th_now.$kodebonm;
						}
						else {
							$nomorbonm = $th_now."00001";
						}
					}
					else {
						$nomorbonm	= $th_now."00001";
					}
					$no_bonm1 = "BM-".$nomorbonm;
					
					// insert di tm_apply_stok_pembelian
					$data_header2 = array(
					  'no_bonm'=>$no_bonm1,
					  'tgl_bonm'=>$tgl,
					  'no_sj'=>$no_sj,
					  'kode_supplier'=>$kode_supplier,
					  'id_gudang'=>$id_gudang,
					  'tgl_input'=>$tgl,
					  'tgl_update'=>$tgl,
					  'stok_lain'=>'t',
					  'status_stok'=> 't'
					);
					$this->db->insert('tm_apply_stok_pembelian',$data_header2);
					
					// ambil data terakhir di tabel tm_apply_stok_pembelian
					$query2	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query2->row();
					$id_apply_stok	= $hasilrow->id;
					
					//save ke tabel tm_apply_stok_pembelian_detail
					if ($kode!='' && $qty!='') {				
						// jika semua data tdk kosong, insert ke tm_apply_stok_pembelian_detail
						$data_detail = array(
							'kode_brg'=>$kode,
							'qty'=>$qty,
							'harga'=>$harga,
							'id_apply_stok'=>$id_apply_stok,
							'status_stok'=> 't',
							'no_bonm'=>$no_bonm,
							'tgl_bonm'=>$tgl_bonm
						);
						$this->db->insert('tm_apply_stok_pembelian_detail',$data_detail);
						
						// ##############################################################################
						// update lagi di tm_pembelian_detail
						$this->db->query(" UPDATE tm_pembelian_detail SET status_stok = 't' WHERE id = '$id_detailnya' ");
									
						//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
						$query3	= $this->db->query(" SELECT id FROM tm_pembelian_detail WHERE status_stok = 'f' 
											AND id_pembelian = '$id_pembelian' ");
						if ($query3->num_rows() == 0){
							$this->db->query(" UPDATE tm_pembelian SET status_stok = 't' WHERE id = '$id_pembelian' ");
						}
						
						//cek stok terakhir tm_stok, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$qty;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
							$data_stok = array(
								'kode_brg'=>$kode,
								'stok'=>$new_stok,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok',$data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode' ");
						}
						
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, kode_supplier, no_bukti, masuk_lain, saldo, tgl_input, harga) 
										VALUES ('$kode', '$kode_supplier', '$no_bonm', '$qty', '$new_stok', '$date_stok', '$harga' ) ");
						// ##############################################################################
						
					} // end if kode != '' dan $qty != ''
				} // end else
				

			// ====================================================
  }
  
  function get_gudang(){
    $query = $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi ORDER BY a.kode_lokasi, a.kode_gudang");
    
    if ($query->num_rows() > 0){
		return $query->result();
	}
  }
  
  function get_bonm($num, $offset, $id_gudang, $cari) {
	  // ambil data bon M
	if ($cari == "all") {		
		if ($id_gudang == '0') {
		$sql= " distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' order by c.id DESC ";
			$this->db->select($sql, false)->limit($num,$offset);
		}
		else {
			$sql= " distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't' AND b.id_gudang = '$id_gudang'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' order by c.id DESC ";
			$this->db->select($sql, false)->limit($num,$offset);
		}
		
		$query = $this->db->get();
	}
	else {
		if ($id_gudang == '0') {
			$sql= " distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' AND UPPER(c.no_bonm) like UPPER('%$cari%') order by c.id DESC ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
		else {
			$sql= " distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't' AND b.id_gudang = '$id_gudang'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' AND UPPER(c.no_bonm) like UPPER('%$cari%') order by c.id DESC ";
			
			$this->db->select($sql, false)->limit($num,$offset);
		}
		
		$query = $this->db->get();
	}
		$data_bonm = array();
		$detail_bonm = array();
		if ($query->num_rows() > 0){
			$hasil = $query->result();
			foreach ($hasil as $row1) {
							
				// ambil data detail barangnya
				$query2	= $this->db->query(" SELECT * FROM tm_apply_stok_pembelian_detail 
								WHERE no_bonm = '$row1->no_bonm' 
								AND status_revisi_stok_lain = 'f' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
										WHERE a.satuan = b.id AND kode_brg = '$row2->kode_brg' ");
						$hasilrow = $query3->row();
						$nama_brg	= $hasilrow->nama_brg;
						$satuan	= $hasilrow->nama_satuan;
						//--------------------
						
						$detail_bonm[] = array(	'id'=> $row2->id,
												'kode_brg'=> $row2->kode_brg,
												'nama'=> $nama_brg,
												'satuan'=> $satuan,
												'qty'=> $row2->qty
											);
					}
				}
				else {
					$detail_bonm = '';
				}
				
				/*$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama; */
				
				$data_bonm[] = array(			'id'=> $row1->id,	
											'no_bonm'=> $row1->no_bonm,
											'tgl_bonm'=> $row1->tgl_bonm,
											//'nama_supplier'=> $nama_supplier,
											'tgl_update'=> $row1->tgl_update,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // endforeach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
  }
  
  function get_bonmtanpalimit($id_gudang, $cari){
	if ($cari == "all") {
		if ($id_gudang == '0')
			$query	= $this->db->query(" SELECT distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' ");
		else
			$query	= $this->db->query(" SELECT distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't' AND b.id_gudang = '$id_gudang'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' ");
		
	}
	else {
		if ($id_gudang == '0')
			$query	= $this->db->query(" SELECT distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' AND UPPER(c.no_bonm) like UPPER('%$cari%') ");
		else
			$query	= $this->db->query(" SELECT distinct c.id, c.no_bonm, c.tgl_bonm FROM tm_pembelian a, tm_apply_stok_pembelian b, 
				tm_apply_stok_pembelian_detail c WHERE b.id = c.id_apply_stok 
				AND a.no_sj = b.no_sj AND a.kode_supplier = b.kode_supplier 
				AND b.status_aktif = 't' AND b.id_gudang = '$id_gudang'
				AND c.status_revisi_stok_lain = 'f' AND a.stok_masuk_lain_cash = 't' 
				AND b.status_stok = 't' AND UPPER(c.no_bonm) like UPPER('%$cari%') ");
	}
    
    return $query->result();  
  }
  
  function update_stok_cash($id_gudang, $date_stok, $kode, $no_bonm, $qty, $harga, $id_detail, $id_apply_stok, 
							$no_sj, $kode_supplier){  
    $tgl = date("Y-m-d");
    $th_now	= date("Y");
    
    // 11 juni 2011 ###############################
    
    // update status_revisi_stok_lain menjadi 't'
    $this->db->query(" UPDATE tm_apply_stok_pembelian_detail SET status_revisi_stok_lain = 't'
							where id= '$id_detail' ");
	
	//cek apakah status_stok di tabel detailnya sudah t semua, jika sudah maka update statusnya
	$query3	= $this->db->query(" SELECT id FROM tm_apply_stok_pembelian_detail WHERE status_revisi_stok_lain = 'f' 
									AND id_apply_stok = '$id_apply_stok' ");
	if ($query3->num_rows() == 0){
		$this->db->query(" UPDATE tm_apply_stok_pembelian SET status_revisi_stok_lain = 't' WHERE id = '$id_apply_stok' ");
	}
    
				//1. reset stok
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama-$qty;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
							$data_stok = array(
								'kode_brg'=>$kode,
								'stok'=>$new_stok,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok',$data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode' ");
						}
						
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_supplier, keluar_lain, saldo, tgl_input, harga) 
										VALUES ('$kode','$no_bonm', '$kode_supplier', '$qty', '$new_stok', '$date_stok', '$harga' ) ");
				
				//2. insert stok lagi
						$query3	= $this->db->query(" SELECT stok FROM tm_stok WHERE kode_brg = '$kode' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$qty;
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok, insert
							$data_stok = array(
								'kode_brg'=>$kode,
								'stok'=>$new_stok,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok',$data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok SET stok = '$new_stok', tgl_update_stok = '$tgl' 
							where kode_brg= '$kode' ");
						}
						
						$query3	= $this->db->query(" INSERT INTO tt_stok (kode_brg, no_bukti, kode_supplier, masuk, saldo, tgl_input, harga) 
										VALUES ('$kode','$no_bonm', '$kode_supplier', '$qty', '$new_stok', '$date_stok', '$harga' ) ");
    // ############################################
	
  }
  
  function get_detail_bonm_cash($id_gudang){
    $headernya = array();    
    $detail_brg = array();    
    // =====================================================================================
    // NEW 15 AGUSTUS 2011
    $query	= $this->db->query(" SELECT a.no_sj, a.kode_supplier, b.* FROM tm_apply_stok_pembelian a, tm_apply_stok_pembelian_detail b 
					WHERE a.id = b.id_apply_stok AND a.status_stok = 'f' AND a.status_aktif = 't'
					AND b.status_stok = 'f' AND a.no_bonm = b.no_bonm AND a.stok_lain = 't'
					AND a.id_gudang = '$id_gudang' ORDER BY a.kode_supplier, b.id DESC ");
    if ($query->num_rows() > 0) {
		$hasil = $query->result();
		$temp_id_pembelian_detail = 0;
		
		foreach($hasil as $row1) {
			$query3	= $this->db->query(" SELECT a.nama_brg, b.nama as nama_satuan FROM tm_barang a, tm_satuan b 
								WHERE a.satuan = b.id AND
								a.kode_brg = '$row1->kode_brg' ");
			$hasilrow = $query3->row();
			$nama_brg	= $hasilrow->nama_brg;
			$satuan	= $hasilrow->nama_satuan;
			
			// ambil data nama supplier
				$query3	= $this->db->query(" SELECT nama FROM tm_supplier WHERE kode_supplier = '$row1->kode_supplier' ");
				$hasilrow = $query3->row();
				$nama_supplier	= $hasilrow->nama;
								
				//ambil tgl SJ dan id pembelian detail
				$query3	= $this->db->query(" SELECT a.id AS id_pembelian, a.tgl_sj, b.id AS id_detail FROM tm_pembelian a, tm_pembelian_detail b 
							WHERE a.id = b.id_pembelian AND a.no_sj = '$row1->no_sj' 
							AND a.kode_supplier = '$row1->kode_supplier' AND b.kode_brg = '$row1->kode_brg' 
							AND b.qty='$row1->qty' AND b.status_stok = 'f' AND a.status_aktif = 't' ORDER BY b.id DESC ");
				 if ($query3->num_rows() > 0) {
					 if ($query3->num_rows() == 1) {
						$hasilrow = $query3->row();
						$tgl_sj	= $hasilrow->tgl_sj;
						$id_pembelian	= $hasilrow->id_pembelian;
					 	$id_pembelian_detail	= $hasilrow->id_detail;
					 }
					 else if ($query3->num_rows() > 1) {
						 /* misalnya ada 3 data kode brg yg sama dan qty yg sama*/
						 $hasil3 = $query3->result();
						 foreach ($hasil3 as $row3) {
							 if ($row3->id_pembelian_detail != $temp_id_pembelian_detail) {
								$temp_id_pembelian_detail = $row3->id_pembelian_detail;
								$id_pembelian = $temp_id_pembelian_detail;
								$tgl_sj	= $row3->tgl_sj;
								$id_pembelian_detail = $row3->id_detail;
								break;
						     }
						 } // sampe sini 04:01 selasa
					 }
				 }
				 else {
					$tgl_sj = '';
					$id_pembelian = '0';
					$id_pembelian_detail = '0';
				}
				
				$headernya[] = array(		
											'id'=> $row1->id,
											'id_apply_stok'=> $row1->id_apply_stok,
											'no_sj'=> $row1->no_sj,
											'tgl_sj'=> $tgl_sj,
											'id_pembelian'=> $id_pembelian,
											'id_pembelian_detail'=> $id_pembelian_detail,
											'kode_supplier'=> $row1->kode_supplier,
											'nama_supplier'=> $nama_supplier,
											'kode_brg'=> $row1->kode_brg,
											'nama_brg'=> $nama_brg,
											'satuan'=> $satuan,
											'qty'=> $row1->qty,
											'harga'=> $row1->harga
									);
		}
	}
	else {
			$headernya = '';
	}
    // =====================================================================================
    
	return $headernya;
  }

}

