<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}
	
	function view(){
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_brand ORDER BY e_brand_name ASC, i_brand DESC ", false);
	}

	function viewperpages($limit,$offset){
		 $db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query( " SELECT * FROM tr_brand ORDER BY e_brand_name ASC, i_brand DESC LIMIT ".$limit." OFFSET ".$offset, false);
		if ($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function msimpan($i_brand,$e_brand_name){
		 $db2=$this->load->database('db_external', TRUE);
		$qry_ibrand	= $db2->query(" SELECT cast(i_brand AS integer)+1 AS imerek FROM tr_brand ORDER BY cast(i_brand AS integer) DESC LIMIT 1 ");
		if($qry_ibrand->num_rows()>0) {
			$row_ibrand	= $qry_ibrand->row();
			$imerek	= $row_ibrand->imerek;
		} else {
			$imerek	= 1;
		}
		
		$db2->set( array(
			'i_brand'=>$imerek,
			'c_brand_code' => $i_brand,
			'e_brand_name' => $e_brand_name
		));
		
		$db2->insert('tr_brand');
		redirect('merekbrg/cform/');
	}
	
	function medit($id) {
		 $db2=$this->load->database('db_external', TRUE);
		return $db2->query( " SELECT * FROM tr_brand WHERE i_brand='$id' ORDER BY i_brand DESC LIMIT 1 " );
	}
	
	function mupdate($ibrandcode,$ebrandname,$ibrand){
		 $db2=$this->load->database('db_external', TRUE);
		$merek_item	= array(
			'e_brand_name'=>$ebrandname,
			'c_brand_code'=>$ibrandcode
		);
		$db2->update('tr_brand',$merek_item,array('i_brand'=>$ibrand));
		redirect('merekbrg/cform');
	}
	
	function mcari($txt_c_brand_code,$txt_e_brand_name,$limit,$offset){
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_c_brand_code!=""){
			$filter	= " WHERE c_brand_code='$txt_c_brand_code' OR e_brand_name LIKE '%$txt_e_brand_name%' ";
		} else {
			$filter	= " WHERE e_brand_name LIKE '%$txt_e_brand_name%' ";
		}
		$query	= $db2->query(" SELECT * FROM tr_brand ".$filter." ORDER BY e_brand_name ASC, i_brand DESC LIMIT ".$limit." OFFSET ".$offset,false);
		if($query->num_rows()>0) {
			return $result	= $query->result();
		}
	}
	
	function viewcari($txt_c_brand_code,$txt_e_brand_name){
		 $db2=$this->load->database('db_external', TRUE);
		if($txt_c_brand_code!=""){
			$filter	= " WHERE c_brand_code='$txt_c_brand_code' OR e_brand_name LIKE '%$txt_e_brand_name%' ";
		} else {
			$filter	= " WHERE e_brand_name LIKE '%$txt_e_brand_name%' ";
		}
		return $db2->query(" SELECT * FROM tr_brand ".$filter." ORDER BY e_brand_name ASC, i_brand DESC ");
	}

  function delete($id){
	   $db2=$this->load->database('db_external', TRUE);
		$db2->delete('tr_brand',array('i_brand'=>$id));
		redirect('merekbrg/cform/');
  }		
}
?>
