<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mclass extends CI_Model {

	function __construct() { 
		parent::__construct();
	}

	function jmlBonKeluar($ibonkcode){
			$db2=$this->load->database('db_external', TRUE);
		return	$db2->query(" SELECT * FROM tm_outbonm_item a INNER JOIN tm_outbonm b ON b.i_outbonm_code=cast(a.i_outbonm AS character varying) WHERE cast(a.i_outbonm AS character varying)=trim('$ibonkcode') ");
	}
		
	function clistbonmkeluar($bonmkeluar,$d_bonmkeluar_first,$d_bonmkeluar_last) {
	$db2=$this->load->database('db_external', TRUE);
		if(($d_bonmkeluar_first!='' && $d_bonmkeluar_last!='') && $bonmkeluar!='' ) {
			$doutbonm	= " WHERE ( b.d_outbonm BETWEEN '$d_bonmkeluar_first' AND '$d_bonmkeluar_last' ) AND b.f_outbonm_cancel='f' ";
			$ibonm	= " AND b.i_outbonm_code='$bonmkeluar' ";
		} elseif(($d_bonmkeluar_first!='' && $d_bonmkeluar_last!='') && $bonmkeluar=='') {
			$doutbonm	= " WHERE ( b.d_outbonm BETWEEN '$d_bonmkeluar_first' AND '$d_bonmkeluar_last' ) AND b.f_outbonm_cancel='f' ";
			$ibonm	= " ";
		} elseif(($d_bonmkeluar_first!='' || $d_bonmkeluar_last!='') && $bonmkeluar!='') {
			$doutbonm	= " WHERE b.f_outbonm_cancel='f' ";
			$ibonm	= " AND b.i_outbonm_code='$bonmkeluar' ";		
		} elseif(($d_bonmkeluar_first=='' || $d_bonmkeluar_last=='') && $bonmkeluar!='') {
			$doutbonm	= " WHERE b.f_outbonm_cancel='f' ";
			$ibonm	= " AND b.i_outbonm_code='$bonmkeluar' ";		
		} else {
			$doutbonm	= " WHERE b.f_outbonm_cancel='f' ";
			$ibonm	= " ";		
		}
			
		$query	= $db2->query(" SELECT  b.i_outbonm_code AS ioutbonmcode,
					b.i_outbonm AS ioutbonm,
					b.d_outbonm AS doutbonm,
					a.i_product AS iproduct,
					c.e_product_motifname AS productname,
					a.n_count_product AS qty,
					a.i_outbonm_item
				
				FROM tm_outbonm_item a
				
				RIGHT JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)

				".$doutbonm." ".$ibonm." 
				
				ORDER BY b.d_outbonm DESC");
		
		if($query->num_rows() > 0 )	{
			return $result	= $query->result();
		}
	}

	function clistbonmkeluar2($bonmkeluar,$d_bonmkeluar_first,$d_bonmkeluar_last) {
	$db2=$this->load->database('db_external', TRUE);
		if(($d_bonmkeluar_first!='' && $d_bonmkeluar_last!='') && $bonmkeluar!='' ) {
			$doutbonm	= " WHERE ( b.d_outbonm BETWEEN '$d_bonmkeluar_first' AND '$d_bonmkeluar_last' ) AND b.f_outbonm_cancel='f' ";
			$ibonm	= " AND b.i_outbonm_code='$bonmkeluar' ";
		} elseif(($d_bonmkeluar_first!='' && $d_bonmkeluar_last!='') && $bonmkeluar=='') {
			$doutbonm	= " WHERE ( b.d_outbonm BETWEEN '$d_bonmkeluar_first' AND '$d_bonmkeluar_last' ) AND b.f_outbonm_cancel='f' ";
			$ibonm	= " ";
		} elseif(($d_bonmkeluar_first!='' || $d_bonmkeluar_last!='') && $bonmkeluar!='') {
			$doutbonm	= " WHERE b.f_outbonm_cancel='f' ";
			$ibonm	= " AND b.i_outbonm_code='$bonmkeluar' ";
		} elseif(($d_bonmkeluar_first=='' || $d_bonmkeluar_last=='') && $bonmkeluar!='') {
			$doutbonm	= " WHERE b.f_outbonm_cancel='f' ";
			$ibonm	= " AND b.i_outbonm_code='$bonmkeluar' ";
		} else {
			$doutbonm	= " WHERE b.f_outbonm_cancel='f' ";
			$ibonm	= " ";
		}
		
		$query	= $db2->query(" SELECT b.i_outbonm_code AS ioutbonmcode,
			b.i_outbonm AS ioutbonm,
			b.d_outbonm AS doutbonm,
			sum(a.n_count_product) AS qty
			
			FROM tm_outbonm_item a 
			RIGHT JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm 
			INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product) 
			INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product) 
			".$doutbonm." ".$ibonm." GROUP BY b.i_outbonm_code, b.i_outbonm, b.d_outbonm ORDER BY b.d_outbonm DESC ");
		
		$data_bonm = array();
		$detail_bonm = array();
		
		if($query->num_rows() > 0 )	{
			$hasil = $query->result();
			foreach ($hasil as $row1) {
				$query2	= $db2->query(" SELECT * FROM tm_outbonm_item WHERE i_outbonm = '$row1->ioutbonm'
										ORDER BY i_outbonm_item ASC ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						$detail_bonm[] = array(	
												'i_product'=> $row2->i_product,
												'e_product_name'=> $row2->e_product_name,
												'n_count_product'=> $row2->n_count_product
											);
					}
				}
				else {
					$detail_bonm = '';
				}
				$data_bonm[] = array(		'ioutbonmcode'=> $row1->ioutbonmcode,	
											'ioutbonm'=> $row1->ioutbonm,
											'doutbonm'=> $row1->doutbonm,
											'qty'=> $row1->qty,
											'detail_bonm'=> $detail_bonm
											);
				$detail_bonm = array();
			} // end foreach header
		}
		else {
			$data_bonm = '';
		}
		return $data_bonm;
		//return $result	= $query->result();
	}
		
	function lkepada() {
			$db2=$this->load->database('db_external', TRUE);
		$order	= " ORDER BY i_to_outbonm ASC, e_to DESC ";
		$db2->select(" a.i_to_outbonm AS i, a.e_to AS to FROM tr_to_outbomm a ".$order." ",false);
		$query	= $db2->get();
		if($query->num_rows() > 0) {
			return $result	= $query->result();
		}
	}
		
	function getbonmkeluar($ioutbonm) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_outbonm WHERE i_outbonm='$ioutbonm' AND f_outbonm_cancel='f' ORDER BY i_outbonm DESC LIMIT 1 ");
	}
	
	function getbonmkeluaritem($ibonmkeluar) {
			$db2=$this->load->database('db_external', TRUE);
		$qstr	= "
			SELECT a.i_outbonm_item, a.i_product, b.e_product_motifname, a.n_count_product, a.i_so FROM tm_outbonm_item a
			
			INNER JOIN tm_outbonm d ON d.i_outbonm=a.i_outbonm
			INNER JOIN tr_product_motif b ON trim(b.i_product_motif)=trim(a.i_product)
			INNER JOIN tr_product_base c ON trim(c.i_product_base)=trim(b.i_product)
			
			WHERE a.i_outbonm='$ibonmkeluar'
			
			GROUP BY a.i_product, b.e_product_motifname, a.n_count_product, a.i_outbonm_item, a.i_so ORDER BY a.i_outbonm_item ASC ";

		$query	= $db2->query($qstr);
		if($query->num_rows()>0) {
			return $result = $query->result();
		}
	}
	
	
	function mupdate($i_outbonm_code,$e_to_outbonm,$i_outbonm_hidden,$i_outbonm_code_hidden,$doutbonm,$i_product,$e_product_name,
		$qty_warna, $i_product_color, $i_color, $qty_product,$iterasi,$f_stp,$iso) {
	$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$tm_bonmkeluar_item	= array();
		$i_outbonm_item	= array();
		$qty_product_update	= array();
		$is2	= array();
		$ins_temp_tmso	= array();
		//$ncountproduct	= array();
		
		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$strstokopname	= "SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1";
		$qstokopname	= $db2->query($strstokopname);
		
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$isoz	= $row_stokopname->i_so;
		}else{
			$isoz	= 0;
		}
		
		if($isoz!="") {
			
			$j=0;
			
			$iproductmotif	= array();
			$jml		= array();
			$totalharga	= array();
			$qtyawal	= array();
			$qtyakhir	= array();
			
			$zz	= $db2->query(" SELECT * FROM tm_outbonm_item WHERE i_outbonm='$i_outbonm_hidden' ");
			
			if($zz->num_rows()>0) {
				
				foreach($zz->result() as $xx) {
					
					$iproductmotif[$j]	= $xx->i_product;
					$jml[$j]			= $xx->n_count_product;
					$iisoooo			= $xx->i_so;
					
					// 23-10-2014
					// ambil data2 per warna, reset stoknya -------------------------------
					$qoutbonm_item_color	= $db2->query(" SELECT * FROM tm_outbonm_item_color 
											WHERE i_outbonm_item='$xx->i_outbonm_item' ");
			
					if($qoutbonm_item_color->num_rows()>0) {
						$result_outbonmitem_color	= $qoutbonm_item_color->result();
						foreach($result_outbonmitem_color as $rowxx) {
							if ($rowxx->i_color != '0') {
								$datacolor = " AND a.i_color = '".$rowxx->i_color."' ";
								$datacolor2 = $datacolor;
								$datacolor3 = " AND i_color = '".$rowxx->i_color."' ";
							}
							else {
								//$datacolor = " AND a.i_product_color = '".$rowxx->i_product_color."' ";
								$qxx = $db2->query(" SELECT i_color FROM tr_product_color WHERE i_product_color='$rowxx->i_product_color' ");
								if($qxx->num_rows()>0) {
									$rxx	= $qxx->row();
									$datacolor2	= " AND a.i_color= '".$rxx->i_color."' ";
									$datacolor3 = " AND i_color= '".$rxx->i_color."' ";
								}
							}
							$qsowarna	= $db2->query(" SELECT a.*, b.i_so_item FROM tm_stokopname_item_color a
										INNER JOIN tm_stokopname_item b ON a.i_so_item = b.i_so_item
										WHERE b.i_so='$iisoooo' AND b.i_product='".$xx->i_product."' ".$datacolor2);
							if($qsowarna->num_rows()>0) {
								$rowsowarna	= $qsowarna->row();
								$qtyawalwarna	= $rowsowarna->n_quantity_awal;
								$qtyakhirwarna	= $rowsowarna->n_quantity_akhir;
								
								$back_qty_awalwarna	= $qtyawalwarna;
								$back_qty_akhirwarna	= $qtyakhirwarna+$rowxx->qty;
								
								if($back_qty_akhirwarna=='')
									$back_qty_akhirwarna = 0;
								
								$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir='$back_qty_akhirwarna'
												WHERE i_so_item = '$rowsowarna->i_so_item' ".$datacolor3);
							}else{
								$qtyawalwarna	= 0;
								$qtyakhirwarna	= 0;
							}
						} // end foreach warna
					} // end if per warna
					
					// modif 23-10-2014 setelah reset stok, hapus outbonm_item_color 08-01-2014
					$db2->query(" DELETE FROM tm_outbonm_item_color WHERE i_outbonm_item='".$xx->i_outbonm_item."' ");	
					//-----------------------------------------------------------------------
					
					$qstokopname2	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_so='$iisoooo' ORDER BY i_so DESC LIMIT 1");
					
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}
										
					$qstrstok_item	= " SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product=trim('$iproductmotif[$j]') ";
					$qstok_item	= $db2->query($qstrstok_item);
					
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;
					}else{
						$qtyawal[$j]	= 0;
						$qtyakhir[$j]	= 0;	
					}
						
					$back_qty_awal[$j]	= $qtyawal[$j];
					$back_qty_akhir[$j]	= $qtyakhir[$j]+$jml[$j];
					
					$update_stokop_item[$j]	= array(
						'n_quantity_akhir'=>$back_qty_akhir[$j]
					);
					
					$db2->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$isoz2,'i_product'=>$iproductmotif[$j]));
					$j+=1;
				}
			}
			
			$db2->delete('tm_outbonm',array('i_outbonm'=>$i_outbonm_hidden));
			$db2->delete('tm_outbonm_item',array('i_outbonm'=>$i_outbonm_hidden));
		}

		for($arr_iso=0; $arr_iso<count($iso); $arr_iso++){
			$stkopname	= $iso[$arr_iso];
			if($stkopname!="")
				break;
		}
			 
		$arrtmoutbonm	= 
			array(
			 'i_outbonm'=>$i_outbonm_hidden,
			 'i_outbonm_code'=>$i_outbonm_code,
			 'e_to_outbonm'=>$e_to_outbonm,
			 'd_outbonm'=>$doutbonm,
			 'f_outbonm_cancel'=>'FALSE',
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry );
			 		
		$tmoutbonm	= $db2->set($arrtmoutbonm);
	 	
		if($db2->insert('tm_outbonm')) {
			
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++) {
				
				//$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];
				$qstrseq_tm_outbonm_item	= " SELECT cast(i_outbonm_item AS integer) AS i_outbonm_item FROM tm_outbonm_item ORDER BY cast(i_outbonm_item AS integer) DESC LIMIT 1 ";
				$seq_tm_outbonm_item	= $db2->query($qstrseq_tm_outbonm_item);
					
				if($seq_tm_outbonm_item->num_rows() > 0){
					$seqrow	= $seq_tm_outbonm_item->row();
					$i_outbonm_item[$jumlah]	= $seqrow->i_outbonm_item+1;
				} else {
					$i_outbonm_item[$jumlah]	= 1;
				}
				
				//-------------- hitung total qty dari detail tiap2 warna -------------------
				$qtytotal = 0;
				for ($xx=0; $xx<count($i_product_color[$jumlah]); $xx++) {

					$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
					$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
													
					$qtytotal+= $qty_warna[$jumlah][$xx];
				} // end for
				// ---------------------------------- END -----------------------------------
				
				//$qty_product_update[$jumlah]	= $qty_product[$jumlah]+$ncountproduct[$jumlah];
				
				$tm_bonmkeluar_item[$jumlah]	= array(
					 'i_outbonm_item'=>$i_outbonm_item[$jumlah],
					 'i_outbonm'=>$i_outbonm_hidden,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$qtytotal,
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah]);
				
				if($db2->insert('tm_outbonm_item',$tm_bonmkeluar_item[$jumlah])) {

					$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
					
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}
					
					$strqstok_item	= "SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product=trim('$i_product[$jumlah]') ";
					$qstok_item	= $db2->query($strqstok_item);
					
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;
						
						$i_so_item = $row_stok_item->i_so_item;
						
						$back_qty_awal[$jumlah]	= $qtyawal[$jumlah];
						//$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]-$ncountproduct[$jumlah];
						$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]-$qtytotal;
						
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz2,'i_product'=>$i_product[$jumlah]));
					}
					
					// 23-10-2014, kesini yg color
					// ------------------------------------------
					for ($xx=0; $xx<count($i_product_color[$jumlah]); $xx++) {
						
							$i_product_color[$jumlah][$xx] = trim($i_product_color[$jumlah][$xx]);
							$i_color[$jumlah][$xx] = trim($i_color[$jumlah][$xx]);
							$qty_warna[$jumlah][$xx] = trim($qty_warna[$jumlah][$xx]);
														
							$seq_tm_outbonm_item_color	= $db2->query(" SELECT i_outbonm_item_color FROM tm_outbonm_item_color 
														ORDER BY i_outbonm_item_color DESC LIMIT 1 ");
						
							if($seq_tm_outbonm_item_color->num_rows() > 0) {
								$seqrow	= $seq_tm_outbonm_item_color->row();
								$i_outbonm_item_color[$jumlah]	= $seqrow->i_outbonm_item_color+1;
							}else{
								$i_outbonm_item_color[$jumlah]	= 1;
							}

							$tm_bonmkeluar_item_color[$jumlah]	= array(
								 'i_outbonm_item_color'=>$i_outbonm_item_color[$jumlah],
								 'i_outbonm_item'=>$i_outbonm_item[$jumlah],
								 'i_product_color'=>$i_product_color[$jumlah][$xx],
								 'qty'=>$qty_warna[$jumlah][$xx],
								 'i_color'=>$i_color[$jumlah][$xx],
							);
							$db2->insert('tm_outbonm_item_color',$tm_bonmkeluar_item_color[$jumlah]);
							
							// 23-10-2014, update stok per warna
							$sqlwarna = " SELECT * FROM tm_stokopname_item_color WHERE i_so_item = '$i_so_item'
											AND i_color = '".$i_color[$jumlah][$xx]."' ";
							$querywarna = $db2->query($sqlwarna);
								
							if($querywarna->num_rows() > 0) {
								$hasilwarna = $querywarna->row();
								
								$qtyawalwarna	= $hasilwarna->n_quantity_awal;
								$qtyakhirwarna	= $hasilwarna->n_quantity_akhir;

								$back_qty_awalwarna	= $qtyawalwarna;
								$back_qty_akhirwarna	= $qtyakhirwarna-$qty_warna[$jumlah][$xx];
									
								if($back_qty_akhirwarna=="")
									$back_qty_akhirwarna	= 0;
																		
								$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir = '$back_qty_akhirwarna'
													WHERE i_so_item = '$i_so_item' AND i_color = '".$i_color[$jumlah][$xx]."' ");
									
							} // end if
					} // end for
					// ------------------------------------------
					
					if ($db2->trans_status()===FALSE || $db2->trans_status()==FALSE) {
						$db2->trans_rollback();
					}else{
						$db2->trans_commit();
					}
				}
			}
		}
		
		$strqbonmkeluar	= " SELECT * FROM tm_outbonm WHERE i_outbonm='$i_outbonm_hidden' ";
		$qbonmkeluar	= $db2->query($strqbonmkeluar);
		
		if($qbonmkeluar->num_rows()>0) {
			print "<script>alert(\"Nomor Bon M Keluar : '\"+$i_outbonm_code_hidden+\"' berhasil diupdate, terimakasih.\");
			window.open(\"index\", \"_self\");</script>";
		}else{
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal diupdate!\");
			window.open(\"index\", \"_self\");</script>";
		}
		
	}

	function mupdate_old($i_outbonm_code,$e_to_outbonm,$i_outbonm_hidden,$i_outbonm_code_hidden,$doutbonm,$i_product,$e_product_name,$n_count_product,$qty_product,$iterasi,$f_stp,$iso) {
	$db2=$this->load->database('db_external', TRUE);
		$db2->trans_begin();
		
		$tm_bonmkeluar_item	= array();
		$i_outbonm_item	= array();
		$qty_product_update	= array();
		$is2	= array();
		$ins_temp_tmso	= array();
		$ncountproduct	= array();
		
		$qtyawal = array();
		$qtyakhir	= array();
		$back_qty_awal	= array();
		$back_qty_akhir	= array();
		$update_stokop_item	= array();
		
		$qdate	= $db2->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;
		
		$strstokopname	= "SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1";
		$qstokopname	= $db2->query($strstokopname);
		if($qstokopname->num_rows()>0) {
			$row_stokopname	= $qstokopname->row();
			$isoz	= $row_stokopname->i_so;
		} else {
			$isoz	= 0;
		}
		
		if($isoz!="") {
			
			$j=0;
			
			$iproductmotif	= array();
			$jml		= array();
			$totalharga	= array();
			$qtyawal	= array();
			$qtyakhir	= array();
			
			$zz	= $db2->query(" SELECT * FROM tm_outbonm_item WHERE i_outbonm='$i_outbonm_hidden' ");
			
			if($zz->num_rows()>0) {
				//$result_outbonmitem	= $qstroutbonm_item->result();
				foreach($zz->result() as $xx) {
					$iproductmotif[$j]	= $xx->i_product;
					$jml[$j]			= $xx->n_count_product;
					$iisoooo			= $xx->i_so;
					
					//$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' AND  f_stop_produksi='$f_stp[$j]' ORDER BY i_so DESC LIMIT 1");
					
					//$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$j]' AND i_so='$iso[$j]' ORDER BY i_so DESC LIMIT 1");
					$qstokopname2	= $db2->query(" SELECT * FROM tm_stokopname WHERE i_so='$iisoooo' ORDER BY i_so DESC LIMIT 1");
					
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}
										
					$qstrstok_item	= " SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product=trim('$iproductmotif[$j]') ";
					$qstok_item	= $db2->query($qstrstok_item);
					
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;
					}else{
						$qtyawal[$j]	= 0;
						$qtyakhir[$j]	= 0;	
					}
					
					//Disabled 25012011
					//$back_qty_awal[$j]	= $qtyakhir[$j]+$delivery[$j];
					//$back_qty_akhir[$j]	= $qtyawal[$j];
						
					$back_qty_awal[$j]	= $qtyawal[$j];
					$back_qty_akhir[$j]	= $qtyakhir[$j]+$jml[$j];
					
					/* 14072011
					$update_stokop_item[$j]	= array(
						'n_quantity_awal'=>$back_qty_awal[$j],
						'n_quantity_akhir'=>$back_qty_akhir[$j]
					);
					*/
					
					$update_stokop_item[$j]	= array(
						'n_quantity_akhir'=>$back_qty_akhir[$j]
					);
					
					$db2->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$isoz2,'i_product'=>$iproductmotif[$j]));
					$j+=1;
				}
			}
			
			$db2->delete('tm_outbonm',array('i_outbonm'=>$i_outbonm_hidden));
			$db2->delete('tm_outbonm_item',array('i_outbonm'=>$i_outbonm_hidden));
		}

		for($arr_iso=0; $arr_iso<count($iso); $arr_iso++){
			$stkopname	= $iso[$arr_iso];
			if($stkopname!="")
				break;
		}
				
		/*		
		$arrtmoutbonm	= 
			array(
			 'i_outbonm'=>$i_outbonm_hidden,
			 'i_outbonm_code'=>$i_outbonm_code_hidden,
			 'e_to_outbonm'=>$e_to_outbonm,
			 'd_outbonm'=>$doutbonm,
			 'f_outbonm_cancel'=>'FALSE',
			 'd_entry'=>$dentry,
			 'i_so'=>$stkopname,
			 'd_update'=>$dentry );
		*/	 
		$arrtmoutbonm	= 
			array(
			 'i_outbonm'=>$i_outbonm_hidden,
			 'i_outbonm_code'=>$i_outbonm_code,
			 'e_to_outbonm'=>$e_to_outbonm,
			 'd_outbonm'=>$doutbonm,
			 'f_outbonm_cancel'=>'FALSE',
			 'd_entry'=>$dentry,
			 'd_update'=>$dentry );
			 		
		$tmoutbonm	= $db2->set($arrtmoutbonm);
	 	
		if($db2->insert('tm_outbonm')) {
			
			for($jumlah=0;$jumlah<=$iterasi;$jumlah++) {
				
				$ncountproduct[$jumlah]	= $n_count_product[$jumlah]==""?0:$n_count_product[$jumlah];
				$qstrseq_tm_outbonm_item	= " SELECT cast(i_outbonm_item AS integer) AS i_outbonm_item FROM tm_outbonm_item ORDER BY cast(i_outbonm_item AS integer) DESC LIMIT 1 ";
				$seq_tm_outbonm_item	= $db2->query($qstrseq_tm_outbonm_item);
					
				if($seq_tm_outbonm_item->num_rows() > 0){
					$seqrow	= $seq_tm_outbonm_item->row();
					$i_outbonm_item[$jumlah]	= $seqrow->i_outbonm_item+1;
				} else {
					$i_outbonm_item[$jumlah]	= 1;
				}
				
				$qty_product_update[$jumlah]	= $qty_product[$jumlah]+$ncountproduct[$jumlah];
				
				$tm_bonmkeluar_item[$jumlah]	= array(
					 'i_outbonm_item'=>$i_outbonm_item[$jumlah],
					 'i_outbonm'=>$i_outbonm_hidden,
					 'i_product'=>$i_product[$jumlah],
					 'e_product_name'=>$e_product_name[$jumlah],
					 'n_count_product'=>$ncountproduct[$jumlah],
					 'd_entry'=>$dentry,
					 'i_so'=>$iso[$jumlah]);
				
				if($db2->insert('tm_outbonm_item',$tm_bonmkeluar_item[$jumlah])) {

					//$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' AND  f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
					$qstokopname2	= $db2->query("SELECT * FROM tm_stokopname WHERE f_stop_produksi='$f_stp[$jumlah]' AND i_so='$iso[$jumlah]' ORDER BY i_so DESC LIMIT 1");
					
					if($qstokopname2->num_rows()>0) {
						$row_stokopname2	= $qstokopname2->row();
						$isoz2	= $row_stokopname2->i_so;
					}
					
					$strqstok_item	= "SELECT * FROM tm_stokopname_item WHERE i_so='$isoz2' AND i_product=trim('$i_product[$jumlah]') ";
					$qstok_item	= $db2->query($strqstok_item);
					
					if($qstok_item->num_rows()>0) {
						$row_stok_item	= $qstok_item->row();
						$qtyawal[$jumlah]	= $row_stok_item->n_quantity_awal;
						$qtyakhir[$jumlah]	= $row_stok_item->n_quantity_akhir;
						
						$back_qty_awal[$jumlah]	= $qtyawal[$jumlah];
						$back_qty_akhir[$jumlah]	= $qtyakhir[$jumlah]-$ncountproduct[$jumlah];
						
						/* 14072011
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_awal'=>$back_qty_awal[$jumlah],
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);
						*/
						
						$update_stokop_item[$jumlah]	= array(
							'n_quantity_akhir'=>$back_qty_akhir[$jumlah]
						);						
						$db2->update('tm_stokopname_item',$update_stokop_item[$jumlah],array('i_so'=>$isoz2,'i_product'=>$i_product[$jumlah]));
					} else {
						print "<script>alert(\"Maaf, Data Bon M Keluar gagal diupdate!\");show(\"listbonmkeluar/cform\",\"#content\");</script>";
					}
					
					$strget_tmso	= " SELECT a.* FROM tm_so a WHERE a.i_product_motif=trim('$i_product[$jumlah]') AND a.i_status_do='1' ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ";
					$get_tmso	= $db2->query($strget_tmso);
							
					if($get_tmso->num_rows()>0){
						$strget_tmso2	= " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ";
						$get_tmso2	= $db2->query($strget_tmso2);
											
						if($get_tmso2->num_rows() > 0 ){
							$row_tmso2	= $get_tmso2->row();
							$is2[$jumlah]	= $row_tmso2->iso+1;
						} else {
							$is2[$jumlah]	= 1;
						}
						
						$row_tmso	= $get_tmso->row_array();
						
						/* 29072011
						$temp_iso[$jumlah]	= $row_tmso['i_so'];
						$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
						$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
						$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
						$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
						$temp_ddo[$jumlah]	= $row_tmso['d_do'];
						$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal']==""?0:$row_tmso['n_saldo_awal'];
						$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir']==""?0:$row_tmso['n_saldo_akhir'];
						$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm']==""?0:$row_tmso['n_inbonm'];
						$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm']==""?0:$row_tmso['n_outbonm'];
						$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm']==""?0:$row_tmso['n_bbm'];
						$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk']==""?0:$row_tmso['n_bbk'];
						$temp_n_do[$jumlah]	= $row_tmso['n_do']==""?0:$row_tmso['n_do'];
						*/
						
						if($row_tmso['n_outbonm']==''){
							$noutbonm	= 0;
						}else{
							$noutbonm	= $row_tmso['n_outbonm'];
						}
						
						if($row_tmso['n_saldo_akhir']==''){
							$nsaldoakhir2 = 0;
						}else{
							$nsaldoakhir2 = $row_tmso['n_saldo_akhir'];
						}
						
						$saldo_akhir[$jumlah]	= $nsaldoakhir2 - $ncountproduct[$jumlah];
						$tambah_outbonm[$jumlah]= $noutbonm + $ncountproduct[$jumlah];
						
						if($saldo_akhir[$jumlah]=='')
							$saldo_akhir[$jumlah] = 0;
						
						if($tambah_outbonm[$jumlah]=='')
							$tambah_outbonm[$jumlah] = 0;
						
						if($row_tmso['n_inbonm']==''){
							$ninbonm2	= 0;
						}else{
							$ninbonm2	= $row_tmso['n_inbonm'];
						}
						
						if($row_tmso['n_bbm']==''){
							$nbbm2	= 0;	 	
						}else{
							$nbbm2	= $row_tmso['n_bbm'];
						}	 	
						
						if($row_tmso['n_bbk']==''){
							$nbbk2	= 0;	
						}else{
							$nbbk2	= $row_tmso['n_bbk'];
						}
						
						if($row_tmso['n_do']==''){
							$ndo2	= 0;	
						}else{
							$ndo2	= $row_tmso['n_do'];
						}
						
						$ins_temp_tmso[$jumlah]	= array(
							'i_so' => $is2[$jumlah],
							'i_product' => $row_tmso['i_product'],
							'i_product_motif' => $row_tmso['i_product_motif'],
							'e_product_motifname' => $row_tmso['e_product_motifname'],
							'i_status_do' => $row_tmso['i_status_do'],
							'd_do' => $row_tmso['d_do'],
							'n_saldo_awal' => $nsaldoakhir2,
							'n_saldo_akhir' => $saldo_akhir[$jumlah],
							'n_inbonm' => $ninbonm2,
							'n_outbonm' => $tambah_outbonm[$jumlah],
							'n_bbm' => $nbbm2,
							'n_bbk' => $nbbk2,
							'n_do' => $ndo2,
							'd_entry'=>$dentry );
						
						$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						
					} else {
						$strget_tmso	= " SELECT a.* FROM tm_so a WHERE a.i_product_motif=trim('$i_product[$jumlah]') ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ";
						$get_tmso	= $db2->query($strget_tmso);
												
						if($get_tmso->num_rows()>0) {
							$strget_tmso2	= " SELECT cast(a.i_so AS integer) AS iso FROM tm_so a ORDER BY cast(a.i_so AS integer) DESC LIMIT 1 ";
							$get_tmso2	= $db2->query($strget_tmso2);
												
							if($get_tmso2->num_rows() > 0 ){
								$row_tmso2	= $get_tmso2->row();
								$is2[$jumlah]	= $row_tmso2->iso+1;
							} else {
								$is2[$jumlah]	= 1;
							}
							
							$row_tmso	= $get_tmso->row_array();
							
							/* 29072011
							$temp_iso[$jumlah]	= $row_tmso['i_so'];
							$temp_iproduct[$jumlah]	= $row_tmso['i_product'];
							$temp_imotif[$jumlah]	= $row_tmso['i_product_motif'];
							$temp_productname[$jumlah]	= $row_tmso['e_product_motifname'];
							$temp_istatusdo[$jumlah]	= $row_tmso['i_status_do'];
							$temp_ddo[$jumlah]	= $row_tmso['d_do'];
							$temp_saldo_awal[$jumlah]	= $row_tmso['n_saldo_awal']==""?0:$row_tmso['n_saldo_awal'];
							$temp_saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir']==""?0:$row_tmso['n_saldo_akhir'];
							$temp_n_inbonm[$jumlah]	= $row_tmso['n_inbonm']==""?0:$row_tmso['n_inbonm'];
							$temp_n_outbonm[$jumlah]	= $row_tmso['n_outbonm']==""?0:$row_tmso['n_outbonm'];
							$temp_n_bbm[$jumlah]	= $row_tmso['n_bbm']==""?0:$row_tmso['n_bbm'];
							$temp_n_bbk[$jumlah]	= $row_tmso['n_bbk']==""?0:$row_tmso['n_bbk'];
							$temp_n_do[$jumlah]	= $row_tmso['n_do']==""?0:$row_tmso['n_do'];
							$saldo_akhir[$jumlah]	= $row_tmso['n_saldo_akhir'] - $ncountproduct[$jumlah];
							$tambah_outbonm[$jumlah]	= $temp_n_outbonm[$jumlah] + $ncountproduct[$jumlah];
							*/

							if($row_tmso['n_outbonm']==''){
								$noutbonm	= 0;
							}else{
								$noutbonm	= $row_tmso['n_outbonm'];
							}
							
							if($row_tmso['n_saldo_akhir']==''){
								$nsaldoakhir2 = 0;
							}else{
								$nsaldoakhir2 = $row_tmso['n_saldo_akhir'];
							}
							
							$saldo_akhir[$jumlah]	= $nsaldoakhir2 - $ncountproduct[$jumlah];
							$tambah_outbonm[$jumlah]= $noutbonm + $ncountproduct[$jumlah];
							
							if($saldo_akhir[$jumlah]=='')
								$saldo_akhir[$jumlah] = 0;
							
							if($tambah_outbonm[$jumlah]=='')
								$tambah_outbonm[$jumlah] = 0;
							
							if($row_tmso['n_inbonm']==''){
								$ninbonm2	= 0;
							}else{
								$ninbonm2	= $row_tmso['n_inbonm'];
							}
							
							if($row_tmso['n_bbm']==''){
								$nbbm2	= 0;	 	
							}else{
								$nbbm2	= $row_tmso['n_bbm'];
							}	 	
							
							if($row_tmso['n_bbk']==''){
								$nbbk2	= 0;	
							}else{
								$nbbk2	= $row_tmso['n_bbk'];
							}
							
							if($row_tmso['n_do']==''){
								$ndo2	= 0;	
							}else{
								$ndo2	= $row_tmso['n_do'];
							}
												 
							$ins_temp_tmso[$jumlah]	= array(
								'i_so' => $is2[$jumlah],
								'i_product' => $row_tmso['i_product'],
								'i_product_motif' => $row_tmso['i_product_motif'],
								'e_product_motifname' => $row_tmso['e_product_motifname'],
								'i_status_do' => $row_tmso['i_status_do'],
								'd_do' => $row_tmso['d_do'],
								'n_saldo_awal' => $nsaldoakhir2,
								'n_saldo_akhir' => $saldo_akhir[$jumlah],
								'n_inbonm' => $ninbonm2,
								'n_outbonm' => $tambah_outbonm[$jumlah],
								'n_bbm' => $nbbm2,
								'n_bbk' => $nbbk2,
								'n_do' => $ndo2,
								'd_entry' => $dentry);
							
							$db2->insert('tm_so',$ins_temp_tmso[$jumlah]);
						} else {
							//echo "gagal update tabel tm_outbonm & tm_outbonm_item!";
						}
					}
					
					if ($db2->trans_status()===FALSE) {
						$db2->trans_rollback();
					} else {
						$db2->trans_commit();
						/*** print "<script>alert(\"Maaf, Data Bon M Keluar gagal diupdate!\");show(\"listbonmkeluar/cform\",\"#content\");</script>"; ***/
					}
				/* } else {
					echo "gagal update tabel tm_inbonm!"; */
				}
			}
		}
		
		$strqbonmkeluar	= " SELECT * FROM tm_outbonm WHERE i_outbonm='$i_outbonm_hidden' ";
		$qbonmkeluar	= $db2->query($strqbonmkeluar);
		
		if($qbonmkeluar->num_rows()>0) {
			print "<script>alert(\"Nomor Bon M Keluar : '\"+$i_outbonm_code_hidden+\"' berhasil diupdate, terimakasih.\");show(\"listbonmkeluar/cform\",\"#content\");</script>"; 
		} else {
			print "<script>alert(\"Maaf, Data Bon M Keluar gagal diupdate!\");show(\"listbonmkeluar/cform\",\"#content\");</script>";
		}
		
	}
	
		
	function cari_bonmkeluar($nobonmkeluar) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query(" SELECT * FROM tm_outbonm WHERE i_outbonm_code=trim('$nobonmkeluar') ");
	}
	
	function mbatal($ioutbonm) {
			$db2=$this->load->database('db_external', TRUE);
		if(!empty($ioutbonm)) {
			$qcariioutbonm	= $db2->query(" SELECT * FROM tm_outbonm WHERE i_outbonm='$ioutbonm' AND f_outbonm_cancel='f' ORDER BY i_outbonm DESC LIMIT 1 ");
			if($qcariioutbonm->num_rows()>0) {
				$row_bonm	= $qcariioutbonm->row();
				$ioutbonm_code	= $row_bonm->i_outbonm_code;
				//$iso		= $row_bonm->i_so; /// salah dodollllllllll
				
				$qstokopname	= $db2->query("SELECT * FROM tm_stokopname WHERE i_status_so='0' ORDER BY i_so DESC LIMIT 1");
				if($qstokopname->num_rows()>0) {
					$row_stokopname	= $qstokopname->row();
					$isoz	= $row_stokopname->i_so;
				} else {
					$isoz	= "";
				}		
	
				if($isoz!="") {
					$j=0;
					$iproductmotif	= array();
					$jml		= array();
					$totalharga	= array();
					$qtyawal	= array();
					$qtyakhir	= array();
					
					$qioutbonm_item	= $db2->query(" SELECT * FROM tm_outbonm_item WHERE i_outbonm='$ioutbonm' ");
					
					if($qioutbonm_item->num_rows()>0) {
					
						$result_outbonmitem	= $qioutbonm_item->result();
						
						foreach($result_outbonmitem as $row_outbonmitem) {
							
							// 23-10-2014, ambil data2 per warna, reset stoknya -------------------------------
							$qoutbonm_item_color	= $db2->query(" SELECT * FROM tm_outbonm_item_color 
													WHERE i_outbonm_item='$row_outbonmitem->i_outbonm_item' ");
					
							if($qoutbonm_item_color->num_rows()>0) {
								$result_outbonmitem_color	= $qoutbonm_item_color->result();
								foreach($result_outbonmitem_color as $rowxx) {
									if ($rowxx->i_color != '0') {
										$datacolor = " AND a.i_color = '".$rowxx->i_color."' ";
										$datacolor2 = $datacolor;
										$datacolor3 = " AND i_color = '".$rowxx->i_color."' ";
									}
									else {
										//$datacolor = " AND a.i_product_color = '".$rowxx->i_product_color."' ";
										$qxx = $db2->query(" SELECT i_color FROM tr_product_color WHERE i_product_color='$rowxx->i_product_color' ");
										if($qxx->num_rows()>0) {
											$rxx	= $qxx->row();
											$datacolor2	= " AND a.i_color= '".$rxx->i_color."' ";
											$datacolor3 = " AND i_color= '".$rxx->i_color."' ";
										}
									}
									$qsowarna	= $db2->query(" SELECT a.*, b.i_so_item FROM tm_stokopname_item_color a
												INNER JOIN tm_stokopname_item b ON a.i_so_item = b.i_so_item
												WHERE b.i_so='$row_outbonmitem->i_so' AND b.i_product='".$row_outbonmitem->i_product."' ".$datacolor2);
									if($qsowarna->num_rows()>0) {
										$rowsowarna	= $qsowarna->row();
										$qtyawalwarna	= $rowsowarna->n_quantity_awal;
										$qtyakhirwarna	= $rowsowarna->n_quantity_akhir;
										
										$back_qty_awalwarna	= $qtyawalwarna;
										$back_qty_akhirwarna	= $qtyakhirwarna+$rowxx->qty;
										
										if($back_qty_akhirwarna=='')
											$back_qty_akhirwarna = 0;
										
										$db2->query(" UPDATE tm_stokopname_item_color SET n_quantity_akhir='$back_qty_akhirwarna'
														WHERE i_so_item = '$rowsowarna->i_so_item' ".$datacolor3);
									}else{
										$qtyawalwarna	= 0;
										$qtyakhirwarna	= 0;
									}
								} // end foreach warna
							} // end if per warna
							//-----------------------------------------------------------------------
							
							// after reset stok warna, hapus outbonm_item_color
							$db2->query(" DELETE FROM tm_outbonm_item_color WHERE i_outbonm_item='".$row_outbonmitem->i_outbonm_item."' ");	
							// ------------------------------------------
						
							$iproductmotif[$j]	= $row_outbonmitem->i_product;
							$jml[$j]			= $row_outbonmitem->n_count_product;
							$iso	= $row_outbonmitem->i_so;
							
							/* 08082011
							$qstok_item	= $db2->query("SELECT * FROM tm_stokopname_item WHERE i_so='$isoz' AND i_product='$iproductmotif[$j]' ");
							*/
							
							//$qstok_item	= $db2->query(" SELECT a.n_quantity_awal, a.n_quantity_akhir, b.i_so, b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b ON b.i_so=a.i_so WHERE b.i_status_so='0' AND a.i_product='$iproductmotif[$j]' AND b.i_so='$iso' ORDER BY b.i_so DESC LIMIT 1 ");
							$qstok_item	= $db2->query(" SELECT a.n_quantity_awal, a.n_quantity_akhir, b.i_so, b.f_stop_produksi AS stp FROM tm_stokopname_item a INNER JOIN tm_stokopname b ON b.i_so=a.i_so WHERE a.i_product='$iproductmotif[$j]' AND b.i_so='$iso' ORDER BY b.i_so DESC LIMIT 1 ");
							
							if($qstok_item->num_rows()>0) {
								
								$row_stok_item	= $qstok_item->row();
								
								//$qtyawal[$j]	= $row_stok_item->n_quantity_awal;
								//$qtyakhir[$j]	= $row_stok_item->n_quantity_akhir;

								//Disabled 25012011
								//$back_qty_awal[$j]	= $qtyakhir[$j]+$delivery[$j];
								//$back_qty_akhir[$j]	= $qtyawal[$j];
									
								//$back_qty_awal[$j]	= $qtyawal[$j];
								//$back_qty_akhir[$j]	= $qtyakhir[$j]+$jml[$j];

								$back_qty_awal[$j]	= $row_stok_item->n_quantity_awal;
								$back_qty_akhir[$j]	= ($row_stok_item->n_quantity_akhir)+$jml[$j];
								
								/* 14072011
								$update_stokop_item[$j]	= array(
									'n_quantity_awal'=>$back_qty_awal[$j],
									'n_quantity_akhir'=>$back_qty_akhir[$j]
								);
								*/
								
								$update_stokop_item[$j]	= array(
									'n_quantity_akhir'=>$back_qty_akhir[$j]
								);							
								$db2->update('tm_stokopname_item',$update_stokop_item[$j],array('i_so'=>$row_stok_item->i_so,'i_product'=>$iproductmotif[$j]));
															
							} 
							
							$j+=1;
						}
					}
				}
				
				/* 08082011
				$tbl_bonm	= array(
					'f_outbonm_cancel'=>'TRUE'
				);				
				$db2->update('tm_outbonm',$tbl_bonm,array('i_outbonm'=>$ioutbonm));
				*/
				$db2->query(" UPDATE tm_outbonm SET f_outbonm_cancel='t' WHERE i_outbonm='$ioutbonm' ");
				
				print "<script>alert(\"Nomor Bon M Keluar : '\"+$ioutbonm_code+\"' telah dibatalkan, terimakasih.\");show(\"listbonmkeluar/cform\",\"#content\");</script>";
			} else {
				print "<script>alert(\"Maaf, Bon M Keluar tdk dpt dibatalkan.\");show(\"listbonmkeluar/cform\",\"#content\");</script>";
			}
		} else {
			print "<script>alert(\"Maaf, Bon M Keluar tdk dpt dibatalkan.\");show(\"listbonmkeluar/cform\",\"#content\");</script>";	
		}		
	}

	function lbonmkeluar() {		
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
			SELECT b.i_outbonm_code AS ioutbonmcode,
				   b.d_outbonm AS doutbonm
				
				FROM tm_outbonm_item a 
				
				RIGHT JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)

				WHERE b.f_outbonm_cancel='f'

				GROUP BY b.i_outbonm_code,b.d_outbonm
				
				ORDER BY b.d_outbonm DESC ");
	}

	function lbonmkeluarperpages($limit,$offset){	
			$db2=$this->load->database('db_external', TRUE);
		$query	= $db2->query("
			SELECT b.i_outbonm_code AS ioutbonmcode,
				   b.d_outbonm AS doutbonm
				
				FROM tm_outbonm_item a 
				
				RIGHT JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)

				WHERE b.f_outbonm_cancel='f'

				GROUP BY b.i_outbonm_code,b.d_outbonm
				
				ORDER BY b.d_outbonm DESC LIMIT ".$limit." OFFSET ".$offset);
						
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function flbonmkeluar($key) {
			$db2=$this->load->database('db_external', TRUE);
		if(!empty($key)) {
			return $db2->query("
			SELECT b.i_outbonm_code AS ioutbonmcode,
				   b.d_outbonm AS doutbonm
				
				FROM tm_outbonm_item a 
				
				RIGHT JOIN tm_outbonm b ON b.i_outbonm=a.i_outbonm
				INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(a.i_product)
				INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)

				WHERE b.f_outbonm_cancel='f' AND b.i_outbonm_code='$key'

				GROUP BY b.i_outbonm_code,b.d_outbonm
				
				ORDER BY b.d_outbonm DESC ");
		}
	}	

	function lbarangjadiperpages2($limit,$offset,$bulan,$tahun) {
			$db2=$this->load->database('db_external', TRUE);
		$strqry	= (" SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan'
				
				ORDER BY b.i_product_motif DESC "." LIMIT ".$limit." OFFSET ".$offset);
								
		$query = $db2->query($strqry);
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi2($bulan,$tahun) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' ORDER BY b.i_product_motif DESC ");
	}

	function flbarangjadi2($key,$bulan,$tahun) {
			$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		return $db2->query( "
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND substring(cast(d.d_so AS character varying),1,4)='$tahun' AND substring(cast(d.d_so AS character varying),6,2)='$bulan' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') ORDER BY b.i_product_motif DESC	
		");		
	}

	function lbarangjadiperpages2opsi($limit,$offset,$bulan,$tahun) {
			$db2=$this->load->database('db_external', TRUE);
		$strqry	= (" SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0'
				
				ORDER BY b.i_product_motif DESC "." LIMIT ".$limit." OFFSET ".$offset);
								
		$query = $db2->query($strqry);
				
		if($query->num_rows() > 0) {
			return $result = $query->result();
		}
	}
	
	function lbarangjadi2opsi($bulan,$tahun) {
			$db2=$this->load->database('db_external', TRUE);
		return $db2->query("
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
					   
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' ORDER BY b.i_product_motif DESC ");
	}

	function flbarangjadi2opsi($key,$bulan,$tahun) {
			$db2=$this->load->database('db_external', TRUE);
		$ky_upper	= strtoupper($key);
		return $db2->query( "
				SELECT a.i_product_base AS iproduct,
				       a.e_product_basename AS productname,
				       b.i_product_motif AS imotif,
				       b.e_product_motifname AS motifname,
				       c.n_quantity_akhir AS qty,
					   d.f_stop_produksi AS stp,
					   d.i_so AS iso
				
				FROM tr_product_motif b
				
				INNER JOIN tr_product_base a ON a.i_product_base=b.i_product
				INNER JOIN tm_stokopname_item c ON c.i_product=b.i_product_motif
				INNER JOIN tm_stokopname d ON d.i_so=c.i_so
				
				WHERE b.n_active='1' AND d.i_status_so='0' AND (b.i_product_motif='$ky_upper' OR b.e_product_motifname LIKE '$key') ORDER BY b.i_product_motif DESC	
		");		
	}			
}
?>
