<?php
class Cgudang extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('wip/mgudang');
  }
   
   function konversistok(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		// 17-02-2014
		$th_now	= date("Y");
		$query3	= $this->db->query(" SELECT no_bukti FROM tm_konversi_stok_wip ORDER BY no_bukti DESC LIMIT 1 ");
		$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_bukti	= $hasilrow->no_bukti;
			else
				$no_bukti = '';
			if(strlen($no_bukti)==8) {
				$nobukti = substr($no_bukti, 0, 8);
				$n_bukti	= (substr($nobukti,4,4))+1;
				$th_bukti	= substr($nobukti,0,4);
				if($th_now==$th_bukti) {
						$jml_n_bukti	= $n_bukti;
						switch(strlen($jml_n_bukti)) {
							case "1": $kodebukti	= "000".$jml_n_bukti;
							break;
							case "2": $kodebukti	= "00".$jml_n_bukti;
							break;	
							case "3": $kodebukti	= "0".$jml_n_bukti;
							break;
							case "4": $kodebukti	= $jml_n_bukti;
							break;	
						}
						$nomorbukti = $th_now.$kodebukti;
				}
				else {
					$nomorbukti = $th_now."0001";
				}
			}
			else {
				$nomorbukti	= $th_now."0001";
			}
			
		$data['no_bukti'] = $nomorbukti;
		// ----------------------------------
		
	$data['msg'] = '';
	$data['list_gudang'] = $this->mgudang->get_gudang();
    $data['isi'] = 'wip/vkonversistokwip';
    
	$this->load->view('template',$data);
  }
  
  function show_popup_brgjadi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	  
	$posisi	= $this->input->post('posisi', TRUE);  
	$gudang	= $this->input->post('gudang', TRUE);  
	$keywordcari	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari=='' && $gudang == '' && $posisi == '') {
		$posisi 	= $this->uri->segment(4);
		$gudang 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}	

	if ($keywordcari=='')
		$keywordcari = "all";
			
	$qjum_total = $this->mgudang->getbarangjadibygudangtanpalimit($gudang, $keywordcari);

	$config['base_url'] 	= base_url()."index.php/wip/cgudang/show_popup_brgjadi/".$posisi."/".$gudang."/".$keywordcari."/";
	$config['total_rows'] 	= count($qjum_total); 
	$config['per_page'] 	= 10;
	$config['first_link'] 	= 'Awal';
	$config['last_link'] 	= 'Akhir';
	$config['next_link'] 	= 'Selanjutnya';
	$config['prev_link'] 	= 'Sebelumnya';
	$config['cur_page']		= $this->uri->segment(7,0);
	$this->pagination->initialize($config);
	
	$data['query']		= $this->mgudang->getbarangjadibygudang($config['per_page'],$config['cur_page'],$gudang,$keywordcari);	
	$data['jum_total']	= count($qjum_total);

	if ($keywordcari=="all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	
	$query3	= $this->db->query(" SELECT a.kode_gudang, a.nama, b.nama as nama_lokasi 
								FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$gudang' ");
	$hasilrow = $query3->row();
	$kode_gudang	= $hasilrow->kode_gudang;
	$nama_gudang	= $hasilrow->nama;
	$nama_lokasi	= $hasilrow->nama_lokasi;
	
	$data['posisi'] = $posisi;
	$data['gudang'] = $gudang;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['startnya']	= $config['cur_page'];
	$this->load->view('wip/vpopupbrgjadikonversi',$data);
  }
  
  // 17-02-2014
  function submitkonversistok(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
			$no_bukti 	= $this->input->post('no_bukti', TRUE);
			$gudang1 	= $this->input->post('gudang1', TRUE);
			$gudang2 	= $this->input->post('gudang2', TRUE);
			$kode_brg_jadi1 	= $this->input->post('kode_brg_jadi1', TRUE);
			$kode_brg_jadi2 	= $this->input->post('kode_brg_jadi2', TRUE);
			$brg_jadi1 	= $this->input->post('brg_jadi1', TRUE);
			$brg_jadi2 	= $this->input->post('brg_jadi2', TRUE);
			$idwarna1 	= $this->input->post('idwarna1', TRUE);
			$idwarna2 	= $this->input->post('idwarna2', TRUE);
									
			$this->mgudang->savekonversistok($no_bukti, $gudang1, $gudang2, $kode_brg_jadi1, $kode_brg_jadi2, $idwarna1, $idwarna2);
			
			$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$gudang1' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$nama_gudang1	= $hasilrow->nama;
			}
			else {
				$nama_gudang1 = '';
			}
			
			$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$gudang2' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$nama_gudang2	= $hasilrow->nama;
			}
			else {
				$nama_gudang2 = '';
			}
			
			$data['isi'] = 'wip/vsukseskonversi';
			if ($kode_brg_jadi1 != $kode_brg_jadi2)
				$msg = "Stok barang jadi WIP dengan kode barang ".$kode_brg_jadi1." berhasil dikonversi ke kode barang ".$kode_brg_jadi2;
			else
				$msg = "Stok barang jadi WIP dengan kode barang ".$kode_brg_jadi1." berhasil dikonversi dari gudang ".$nama_gudang1." ke gudang ".$nama_gudang2;
			$data['msg'] = $msg;
			$this->load->view('template',$data);
  }
  
  // 18-02-2014
  function historykonversistok(){
    $data['isi'] = 'wip/vformhistorykonversi';
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$gudang 	= $this->input->post('gudang', TRUE);  
	
	if ($keywordcari == '' && $gudang == '') {
		$gudang 	= $this->uri->segment(5);
		$keywordcari 	= $this->uri->segment(6);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($gudang == '')
		$gudang = 0;

    $jum_total = $this->mgudang->get_konversistoktanpalimit($gudang, $keywordcari);

			$config['base_url'] = base_url().'index.php/info-stok-hsl-jahit/cform/historykonversistok/index/'.$gudang.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mgudang->get_konversistok($config['per_page'],$this->uri->segment(7), $gudang, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_gudang'] = $this->mgudang->get_gudang();
	$data['cgudang'] = $gudang;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
}
