<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-acc/mmaster');
  }

  function index(){
	$kode 	= $this->uri->segment(4);
	
	if ($kode != '') {
		$hasil = $this->mmaster->get($kode);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$ekode = $row->kode_brg;
			$enama_brg = $row->nama_brg;
			$esatuan = $row->satuan;
			$ekode_kel_brg = $row->kode_kel_barang;
			$edeskripsi = $row->deskripsi;
			$eharga = $row->harga;
		}
	}
	else {
			$ekode = '';
			$enama_brg = '';
			$esatuan = '';
			$ekode_kel_brg = '';
			$edeskripsi = '';
			$eharga = '';
			$edit = '';
	}
	$data['ekode'] = $ekode;
	$data['enama_brg'] = $enama_brg;
	$data['esatuan'] = $esatuan;
	$data['ekode_kel_brg'] = $ekode_kel_brg;
	$data['edeskripsi'] = $edeskripsi;
	$data['eharga'] = $eharga;
	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['kel_brg'] = $this->mmaster->get_kel_brg();
    $data['isi'] = 'mst-acc/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		//if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode Asesoris', 'required');
			$this->form_validation->set_rules('nama', 'Nama Asesoris', 'required');

		//}
		if ($this->form_validation->run() == FALSE)
		{
			$data['isi'] = 'mst-acc/vmainform';
			$data['msg'] = 'Field2 kode, nama tidak boleh kosong..!';
			$ekode = '';
			$enama_brg = '';
			$esatuan = '';
			$ekode_kel_brg = '';
			$edeskripsi = '';
			$eharga = '';
			$edit = '';
			
			$data['ekode'] = $ekode;
			$data['enama_brg'] = $enama_brg;
			$data['esatuan'] = $esatuan;
			$data['ekode_kel_brg'] = $ekode_kel_brg;
			$data['edeskripsi'] = $edeskripsi;			
			$data['eharga'] = $eharga;
			$data['edit'] = $edit;
			$data['kel_brg'] = $this->mmaster->get_kel_brg();

			$this->load->view('template',$data);
		}
		else
		{
			$kode 	= $this->input->post('kode', TRUE);
			$kodeedit 	= $this->input->post('kodeedit', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$kode_kel_brg 	= $this->input->post('kode_kel_brg', TRUE);
			$satuan 	= $this->input->post('satuan', TRUE);
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$harga 	= $this->input->post('harga', TRUE);
			
			if ($goedit == '') {
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-acc/vmainform';
					$data['msg'] = "Data kode ".$kode." sudah ada..!";
					$ekode = '';
					$enama_brg = '';
					$esatuan = '';
					$ekode_kel_brg = '';
					$edeskripsi = '';
					$eharga = '';
					$edit = '';
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;
					$data['esatuan'] = $esatuan;
					$data['ekode_kel_brg'] = $ekode_kel_brg;
					$data['edeskripsi'] = $edeskripsi;	
					$data['eharga'] = $eharga;	
					$data['edit'] = $edit;
					$data['kel_brg'] = $this->mmaster->get_kel_brg();
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kode,$nama, $kode_kel_brg,$satuan, $deskripsi, $harga, $kodeedit, $goedit);
					redirect('mst-acc/cform/view');
				}
			} // end if goedit == ''
			else {
				if ($kode != $kodeedit) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-acc/vmainform';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['ekode'] = $kode;
						$data['enama_brg'] = $nama;
						$data['esatuan'] = $satuan;
						$data['ekode_kel_brg'] = $kode_kel_brg;
						$data['edeskripsi'] = $deskripsi;	
						$data['eharga'] = $harga;	
						$data['edit'] = $edit;
						$data['kel_brg'] = $this->mmaster->get_kel_brg();
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode,$nama, $kode_kel_brg,$satuan, $deskripsi, $harga, $kodeedit, $goedit);
						redirect('mst-acc/cform/view');
					}
				}
				else {
					$this->mmaster->save($kode,$nama, $kode_kel_brg,$satuan, $deskripsi, $harga, $kodeedit, $goedit);
					redirect('mst-acc/cform/view');
				}
			}
			
		}
  }
  
  function view(){
    $data['isi'] = 'mst-acc/vformview';
    $keywordcari = '';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);	
							$config['base_url'] = base_url().'index.php/mst-acc/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari);
							$config['base_url'] = base_url().'index.php/mst-acc/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'mst-acc/vformview';
	$data['cari'] = $keywordcari;
	$this->load->view('template',$data);
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('mst-acc/cform/view');
  }
  
  
}
