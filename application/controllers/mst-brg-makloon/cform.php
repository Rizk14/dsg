<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('mst-brg-makloon/mmaster');
  }
  
  // 11-06-2015, skrg pake field id utk primary key. nanti di tabel2 lain ganti pake id_brg, bukan kode_brg
  function updateidbarang() {
	  $sql = " SELECT * FROM tm_brg_hasil_makloon ORDER BY tgl_input ";
	  $query	= $this->db->query($sql);
	  
	  if ($query->num_rows() > 0){
		$hasil = $query->result();
		foreach ($hasil as $row1) {
			// ambil id tertinggi
			$sql2 = " SELECT id FROM tm_brg_hasil_makloon ORDER BY id DESC LIMIT 1 ";
			$query2	= $this->db->query($sql2);
			if ($query2->num_rows() > 0){
				$hasil2 = $query2->row();
				$idlama	= $hasil2->id;
				$idbaru = $idlama+1;
				
				$this->db->query(" UPDATE tm_brg_hasil_makloon SET id='$idbaru' WHERE kode_brg='$row1->kode_brg' ");
			}
		}
	}
	echo "sukses";
  }

  function index(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$id_barang 	= $this->uri->segment(4);
	
	if ($id_barang != '') {
		$hasil = $this->mmaster->get($id_barang);
		$edit = 1;
		
		foreach ($hasil as $row) {
			$eid = $row->id;
			$ekode = $row->kode_brg;
			$enama_brg = $row->nama_brg;
			$esatuan = $row->satuan;
			$eid_jenis_makloon = $row->id_jenis_makloon;
			$edeskripsi = $row->deskripsi;
			$estatus_aktif = $row->status_aktif;
			
			$query3	= $this->db->query(" SELECT nama FROM tm_jenis_makloon
			WHERE id = '$eid_jenis_makloon' ");
			$hasilrow = $query3->row();
			$enama_jenis_makloon	= $hasilrow->nama;

			$eid_gudang	= $row->id_gudang;
			
			/*$query3	= $this->db->query(" SELECT a.id, a.kode_gudang, a.nama, b.nama as nama_lokasi 
						FROM tm_gudang a, tm_lokasi_gudang b WHERE a.kode_lokasi = b.kode_lokasi AND a.id = '$row->id_gudang' ");
			$hasilrow = $query3->row();
			$ekode_gudang	= $hasilrow->kode_gudang;
			$eid_gudang	= $hasilrow->id;
			$enama_gudang	= $hasilrow->nama;
			$enama_lokasi	= $hasilrow->nama_lokasi; */
		}
	}
	else {
			$eid = '';
			$ekode = '';
			$enama_brg = '';
			$esatuan = '';
			$eid_jenis_makloon = '';
			$edeskripsi = '';
			$enama_jenis_makloon = '';
			$eid_gudang = '';
			$estatus_aktif = '';

			$edit = '';
	}
	$data['eid'] = $eid;
	$data['ekode'] = $ekode;
	$data['enama_brg'] = $enama_brg;
	$data['esatuan'] = $esatuan;
	$data['eid_jenis_makloon'] = $eid_jenis_makloon;
	$data['edeskripsi'] = $edeskripsi;
	$data['estatus_aktif'] = $estatus_aktif;
	$data['enama_jenis_makloon'] = $enama_jenis_makloon;
	$data['eid_gudang'] = $eid_gudang;
	
	$data['edit'] = $edit;
	$data['msg'] = '';
	
	$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
	$data['satuan'] = $this->mmaster->get_satuan();
	$data['list_gudang'] = $this->mmaster->get_gudang();
    $data['isi'] = 'mst-brg-makloon/vmainform';
    
	$this->load->view('template',$data);

  }

  function submit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$this->load->library('form_validation');
		$goedit 	= $this->input->post('goedit', TRUE);
		/*if ($goedit == 1) {
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');

		} */
		
			$kode 	= $this->input->post('kode', TRUE);
			$kode_lama 	= $this->input->post('kode_lama', TRUE); 
			$id_barang 	= $this->input->post('id_barang', TRUE); 
			$nama 	= $this->input->post('nama', TRUE);
			$jenis_makloon 	= '1';
			$satuan 	= $this->input->post('satuan', TRUE);
			$deskripsi 	= $this->input->post('deskripsi', TRUE);
			$gudang 	= $this->input->post('gudang', TRUE);
			$status_aktif 	= $this->input->post('status_aktif', TRUE);
			
			if ($goedit == '') { // tambah data
				$cek_data = $this->mmaster->cek_data($kode);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'mst-brg-makloon/vmainform';
					$data['msg'] = "Data kode ".$kode." sudah ada..!";
					
					$eid = '';
					$ekode = '';
					$enama_brg = '';
					$esatuan = '';
					$eid_jenis_makloon = '';
					$edeskripsi = '';
					$eid_gudang = '';
					$edit = '';
					
					$data['eid'] = $eid;
					$data['ekode'] = $ekode;
					$data['enama_brg'] = $enama_brg;
					$data['esatuan'] = $esatuan;
					$data['eid_jenis_makloon'] = $eid_jenis_makloon;
					$data['edeskripsi'] = $edeskripsi;
					$data['eid_gudang'] = $eid_gudang;
					$data['edit'] = $edit;
					$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
					$data['satuan'] = $this->mmaster->get_satuan();
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($kode,$nama, $jenis_makloon, $satuan, $deskripsi, $status_aktif, $gudang, $id_barang, $goedit);
					redirect('mst-brg-makloon/cform');
				}
			} // end if goedit == ''
			else {
				if ($kode != $kode_lama) {
					$cek_data = $this->mmaster->cek_data($kode);
					if (count($cek_data) > 0) { 
						$data['isi'] = 'mst-brg-makloon/vmainform';
						$data['msg'] = "Data kode ".$kode." sudah ada..!";
						
						$edit = '1';
						$data['eid'] = $eid;
						$data['ekode'] = $kode;
						$data['enama_brg'] = $nama;
						$data['esatuan'] = $satuan;
						$data['eid_jenis_makloon'] = $jenis_makloon;
						$data['edeskripsi'] = $deskripsi;
						$data['edit'] = $edit;
						$data['estatus_aktif'] = $status_aktif;
						$data['eid_gudang'] = $eid_gudang;
						
						$data['list_jenis_makloon'] = $this->mmaster->get_jenis_makloon();
						$data['satuan'] = $this->mmaster->get_satuan();
						$this->load->view('template',$data);
					}
					else {
						$this->mmaster->save($kode,$nama, $jenis_makloon, $satuan, $deskripsi, $status_aktif, $gudang, $id_barang, $goedit);
						redirect('mst-brg-makloon/cform/view');
					}
				}
				else {
					$this->mmaster->save($kode,$nama, $jenis_makloon, $satuan, $deskripsi, $status_aktif, $gudang, $id_barang, $goedit);
					redirect('mst-brg-makloon/cform/view');
				}
			}
			
  }
  
  function view(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $data['isi'] = 'mst-brg-makloon/vformview';
    $keywordcari = "all";
    $jenis_makloon = '1';
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_makloon);	
							$config['base_url'] = base_url().'index.php/mst-brg-makloon/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $keywordcari, $jenis_makloon);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	//$data['jns_makloon'] = $this->mmaster->get_jenis_makloon();
	$this->load->view('template',$data);
  }
  
  function cari(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jenis_makloon 	= '1';  
	
	if ($keywordcari == '') {
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
			
    $jum_total = $this->mmaster->getAlltanpalimit($keywordcari, $jenis_makloon);
							$config['base_url'] = base_url().'index.php/mst-brg-makloon/cform/cari/index/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $keywordcari, $jenis_makloon);
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'mst-brg-makloon/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	//$data['jns_makloon'] = $this->mmaster->get_jenis_makloon();
	$this->load->view('template',$data);
  }
  
  function show_popup_jenis(){
	// =======================
	// disini coding utk pengecekan user login
	//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$kel_brg 	= $this->uri->segment(4);
	if ($kel_brg == '')
		$kel_brg = $this->input->post('kel_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $kel_brg == '') {
		$kel_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_barangtanpalimit($kel_brg, $keywordcari);
			$config['base_url'] = base_url()."index.php/mst-bb/cform/show_popup_jenis/".$kel_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_barang($config['per_page'],$this->uri->segment(6), $kel_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['kel_brg'] = $kel_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_kelompok_barang WHERE kode = '$kel_brg' ");
		$hasilrow = $query3->row();
		$nama_kel	= $hasilrow->nama;
	$data['nama_kel'] = $nama_kel;

	$this->load->view('mst-bb/vpopupjenis',$data);

  }
  
  function show_popup_jenis_bhn(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$jns_brg 	= $this->uri->segment(4);
	if ($jns_brg == '')
		$jns_brg = $this->input->post('jns_brg', TRUE);  

	$keywordcari 	= $this->input->post('cari', TRUE);  
	
	if ($keywordcari == '' || $jns_brg == '') {
		$jns_brg 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	
	$jum_total = $this->mmaster->get_jenis_bahantanpalimit($jns_brg, $keywordcari);
							$config['base_url'] = base_url()."index.php/mst-bb/cform/show_popup_jenis_bhn/".$jns_brg."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->get_jenis_bahan($config['per_page'],$this->uri->segment(6), $jns_brg, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
		
	$data['jns_brg'] = $jns_brg;
		$query3	= $this->db->query(" SELECT nama FROM tm_jenis_barang WHERE id = '$jns_brg' ");
		$hasilrow = $query3->row();
		$nama_jns_brg	= $hasilrow->nama;
	$data['nama_jns_brg'] = $nama_jns_brg;

	$this->load->view('mst-bb/vpopupjenisbhn',$data);

  }

  function delete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
    $id 	= $this->uri->segment(4);
    $this->mmaster->delete($id);
    redirect('mst-brg-makloon/cform/view');
  }
  
  
}
