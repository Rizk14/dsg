<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
			$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
			$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
			$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
			$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
			$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['detail']		= "";
			$data['list']		= "";
			$data['ljnsbrg']	= "";
			$data['limages']	= base_url();
			$data['isi']		= "";
			$this->load->model('prntstoksaatini/mclass');
			$data['opt_jns_brg']	= $this->mclass->lklsbrg();
			$data['isi']	='prntstoksaatini/vmainform';
		$this->load->view('template',$data);
			
		
	}
	
	function carilistbrgjadi() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();
			
		$ddofirst	= $this->input->post('d_do_first')?$this->input->post('d_do_first'):$this->input->get_post('d_do_first');
		$ddolast	= $this->input->post('d_do_last')?$this->input->post('d_do_last'):$this->input->get_post('d_do_last');
		/*
		$fstopproduksi	= $this->input->post('f_stop_produksi')?$this->input->post('f_stop_produksi'):$this->input->get_post('f_stop_produksi');
		*/
		$stop	= $this->input->post('stop')?$this->input->post('stop'):$this->input->get_post('stop');
		$iclass		= $this->input->post('i_class')?$this->input->post('i_class'):$this->input->get_post('i_class');
		
		//$stopproduk		= ($fstopproduksi==1 || $fstopproduksi!='1')?TRUE:FALSE;
		$stopproduk2	= $stop==1?'TRUE':'FALSE';
		
		$e_d_do_first	= explode("/",$ddofirst,strlen($ddofirst));
		$e_d_do_last	= explode("/",$ddolast,strlen($ddolast)); // dd/mm/YYYY
		$nddofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:date('Y-m')."-"."01";
		$nddolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:date('Y-m-d');
		
		$tahun	= substr($nddofirst,0,4); // YYYY-mm-dd
		$bulan	= substr($nddofirst,5,2);
		
		$bln_sekarang	= date("m");
		$thn_sekarang	= date("Y");
		
		if($bln_sekarang==$bulan && $thn_sekarang==$tahun){
			$filter_tgl_stokopname	= " AND d.i_status_so=0 ";
		}else{
			$filter_tgl_stokopname	= " AND SUBSTRING(CAST(d.d_so AS character varying),1,4)='$tahun' AND SUBSTRING(CAST(d.d_so AS character varying),6,2)='$bulan'";
		}
		
		$data['tgldomulai']	= $ddofirst;
		$data['tgldoakhir']	= $ddolast;
		$data['sproduksi']	= $stop==1?" checked ":"";
		$data['kproduksi']	= $iclass;
		$data['sproduksi2']	= $stop==1?1:0;
		$data['tgldomulai2']	= $nddofirst;
		$data['tgldoakhir2']	= $nddolast;
		
		$this->load->model('prntstoksaatini/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();
		
		/* Disabled 20-01-2011
		$qstoksaatini	= $this->mclass->cliststoksaatini($nddofirst,$nddolast,$stopproduk2,$iclass);
		$lstokopname	= "";
		$no	= 1;
		$iproduct	= array();
		$iproductmo	= array();
		
		if($qstoksaatini->num_rows()>0) {
			foreach($qstoksaatini->result() as $field) {
				$iproduct[$no]	= $field->i_product;
				$iproductmo[$no]	= $field->i_product_motif;
				$qstoksaatini_detail	= $this->mclass->cliststoksaatini2($iproduct[$no],$iproductmo[$no]);
				if($qstoksaatini_detail->num_rows()>0) {
					foreach($qstoksaatini_detail->result() as $row) {
						$lstokopname	.=	
						"<tr>
						  <td>".$no."</td>
						  <td>".$field->i_product_motif."</td>
						  <td>".$row->e_product_motifname."</td>
						  <td>".$row->n_saldo_awal."</td>
						  <td>".$row->n_saldo_awal."</td>
						  <td>".$row->n_inbonm."</td>
						  <td>".$row->n_bbm."</td>
						  <td>".$row->n_do."</td>
						  <td>".$row->n_outbonm."</td>
						  <td>".$row->n_bbk."</td>
						</tr>";	
					}
				}
				$no+=1;	
			}
			
		}
		$data['isi']	= $lstokopname;
		*/

		$qbrgjadi	= $this->mclass->lbrgjadi($stopproduk2,$filter_tgl_stokopname);
		$lstokopname	= "";
		
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();

		/* Deklarasi variabel : Mencari saldo akhir */
		$x11	= array();
		$x22	= array();
		$z11	= array();
		$saldoakhirnya	= array();
		
		/* End 0f saldo akhir */
				
		if($qbrgjadi->num_rows()>0) {
			
			$cc = 1;
			
			foreach($qbrgjadi->result() as $field) {
				$qbonmkeluar	= $this->mclass->lbonkeluar($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}

				$qbonmmasuk	= $this->mclass->lbonmasuk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk; 
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($nddofirst,$nddolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm; 
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($nddofirst,$nddolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}

				$qsj	= $this->mclass->lsj($nddofirst,$nddolast,$field->i_product_motif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj;
				} else {
					$sj[$j] = 0;
				}
				
				$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
				$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

				/* Mencari saldo akhir */
				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$z11[$j]	= $field->n_quantity_awal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
				/* End 0f saldo akhir */
				
				/*
				$field->n_quantity_akhir
				*/										
				$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td align=\"right\">".$field->n_quantity_awal."</td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align=\"right\">".$bonmmasuk[$j]."</td>
				  <td align=\"right\">".$bbm[$j]."</td>
				  <td align=\"right\">".$do[$j]."</td>
				  <td align=\"right\">".$bonmkeluar[$j]."</td>
				  <td align=\"right\">".$bbk[$j]."</td>
				</tr>";	
				$j++;
				$no++;
				$cc++;
			}
		}		
		$data['query']	= $lstokopname;		
		$data['isi']	='prntstoksaatini/vlistform';
		$this->load->view('template',$data);

	}
	
	function cstoksaatini() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$tglmulai	= $this->uri->segment(4);
		$tglakhir	= $this->uri->segment(5);
		$stopproduct= $this->uri->segment(6);
		$kdproduct	= $this->uri->segment(7);
		
		$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();
		
		$ddofirst	= $tglmulai;
		$ddolast	= $tglakhir;
		$stop		= $stopproduct;
		$iclass		= $kdproduct;
		
		$stopproduk2	= $stop==1?'TRUE':'FALSE';
		
		$e_d_do_first	= explode("-",$ddofirst,strlen($ddofirst)); // YYYY-mm-dd
		$e_d_do_last	= explode("-",$ddolast,strlen($ddolast));
		$nddofirst		= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:'01/'.date('m/Y');
		$nddolast		= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:date('d/m/Y');
		
		$tahun	= substr($ddofirst,0,4); // YYYY-mm-dd
		$bulan	= substr($ddofirst,5,2);
		
		$bln_sekarang	= date("m");
		$thn_sekarang	= date("Y");
		
		if($bln_sekarang==$bulan && $thn_sekarang==$tahun){
			$filter_tgl_stokopname	= " AND d.i_status_so=0 ";
		}else{
			$filter_tgl_stokopname	= " AND SUBSTRING(CAST(d.d_so AS character varying),1,4)='$tahun' AND SUBSTRING(CAST(d.d_so AS character varying),6,2)='$bulan'";
		}
		
		$data['tgldomulai']	= $nddofirst;
		$data['tgldoakhir']	= $nddolast;
		$data['tgldomulai2']	= $ddofirst;
		$data['tgldoakhir2']	= $ddolast;		
		$data['sproduksi']	= $stop==1?" checked ":"";
		$data['sproduksi2']	= $stop==1?1:0;
		$data['kproduksi']	= $iclass;
				
		$this->load->model('prntstoksaatini/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		$qbrgjadi	= $this->mclass->lbrgjadi($stopproduk2,$filter_tgl_stokopname);
		$lstokopname	= "";
		
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();

		/* Deklarasi variabel : Mencari saldo akhir */
		$x11	= array();
		$x22	= array();
		$z11	= array();
		$saldoakhirnya	= array();
		
		/* End 0f saldo akhir */
				
		if($qbrgjadi->num_rows()>0) {
			
			$cc = 1;
			
			foreach($qbrgjadi->result() as $field) {
				$qbonmkeluar	= $this->mclass->lbonkeluar($ddofirst,$ddolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}

				$qbonmmasuk	= $this->mclass->lbonmasuk($ddofirst,$ddolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($ddofirst,$ddolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk; 
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($ddofirst,$ddolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm; 
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($ddofirst,$ddolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}

				$qsj	= $this->mclass->lsj($ddofirst,$ddolast,$field->i_product_motif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj;
				} else {
					$sj[$j] = 0;
				}
																												
				$Classnya	= $cc % 2 == 0 ? "row1" :"row2";
				$bgcolor	= $cc % 2 == 0 ? "#E4E4E4" : "#E4E4E4";

				/* Mencari saldo akhir */
				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$z11[$j]	= $field->n_quantity_awal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
				/* End 0f saldo akhir */
				
				/*
				$field->n_quantity_akhir
				*/																										
				$lstokopname	.=	
				"<tr class=\"$Classnya\" onMouseOver=\"this.className='row2'\"
              	 onMouseOut=\"this.className='$Classnya'\">
				  <td height=\"20px;\" bgcolor=\"$bgcolor\">".$no."</td>
				  <td>".$field->i_product_motif."</td>
				  <td>".$field->e_product_motifname."</td>
				  <td align=\"right\">".$field->n_quantity_awal."</td>
				  <td align=\"right\">".$saldoakhirnya[$j]."</td>
				  <td align=\"right\">".$bonmmasuk[$j]."</td>
				  <td align=\"right\">".$bbm[$j]."</td>
				  <td align=\"right\">".$do[$j]."</td>
				  <td align=\"right\">".$bonmkeluar[$j]."</td>
				  <td align=\"right\">".$bbk[$j]."</td>
				</tr>";	
				$j++;
				$no++;
				$cc++;
			}
		}
		$data['query']	= $lstokopname;		
		$data['isi']	='prntstoksaatini/vprintform';
		$this->load->view('template',$data);
		
	}
	
	function cpopup() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
		$tglmulai	= $this->uri->segment(4);
		$tglakhir	= $this->uri->segment(5);
		$stopproduct= $this->uri->segment(6);
		$kdproduct	= $this->uri->segment(7);
		
		$data['page_title_stoksaatini']	= $this->lang->line('page_title_stoksaatini');
		$data['list_stoksaatini_tgl_do_mulai']	= $this->lang->line('list_stoksaatini_tgl_do_mulai');
		$data['list_stoksaatini_s_produk']	= $this->lang->line('list_stoksaatini_s_produk');
		$data['list_stoksaatini_j_produk']	= $this->lang->line('list_stoksaatini_j_produk');
		$data['list_stoksaatini_kd_brg']	= $this->lang->line('list_stoksaatini_kd_brg');
		$data['list_stoksaatini_nm_brg']	= $this->lang->line('list_stoksaatini_nm_brg');
		$data['list_stoksaatini_s_awal']	= $this->lang->line('list_stoksaatini_s_awal');
		$data['list_stoksaatini_s_akhir']	= $this->lang->line('list_stoksaatini_s_akhir');
		$data['list_stoksaatini_bmm']	= $this->lang->line('list_stoksaatini_bmm');
		$data['list_stoksaatini_bmk']	= $this->lang->line('list_stoksaatini_bmk');
		$data['list_stoksaatini_bbm']	= $this->lang->line('list_stoksaatini_bbm');
		$data['list_stoksaatini_bbk']	= $this->lang->line('list_stoksaatini_bbk');
		$data['list_stoksaatini_do']	= $this->lang->line('list_stoksaatini_do');
		$data['form_title_detail_stoksaatini']	= $this->lang->line('form_title_detail_stoksaatini');
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_cetak']	= $this->lang->line('button_cetak');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lstokopname']= "";
		$data['ljnsbrg']	= "";
		$data['limages']	= base_url();
		
		$ddofirst	= $tglmulai;
		$ddolast	= $tglakhir;
		$stop		= $stopproduct;
		$iclass		= $kdproduct;
		
		$stopproduk2	= $stop==1?'TRUE':'FALSE';
		
		$e_d_do_first	= explode("-",$ddofirst,strlen($ddofirst)); // YYYY-mm-dd
		$e_d_do_last	= explode("-",$ddolast,strlen($ddolast));
		$nddofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:'01/'.date('m/Y');
		$nddolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:date('d/m/Y');

		$tahun	= substr($ddofirst,0,4); // YYYY-mm-dd
		$bulan	= substr($ddofirst,5,2);
		
		$bln_sekarang	= date("m");
		$thn_sekarang	= date("Y");
		
		if($bln_sekarang==$bulan && $thn_sekarang==$tahun){
			$filter_tgl_stokopname	= " AND d.i_status_so=0 ";
		}else{
			$filter_tgl_stokopname	= " AND SUBSTRING(CAST(d.d_so AS character varying),1,4)='$tahun' AND SUBSTRING(CAST(d.d_so AS character varying),6,2)='$bulan'";
		}
		
		$data['tgldomulai']	= $nddofirst;
		$data['tgldoakhir']	= $nddolast;
		$data['tgldomulai2']= $ddofirst;
		$data['tgldoakhir2']= $ddolast;		
		$data['sproduksi']	= $stop==1?" checked ":"";
		$data['kproduksi']	= $iclass;
		
		$this->load->model('prntstoksaatini/mclass');
		$data['opt_jns_brg']	= $this->mclass->lklsbrg();

		$qbrgjadi		= $this->mclass->lbrgjadi($stopproduk2,$filter_tgl_stokopname);
		$lstokopname	= "";
		
		$no	= 1;
		$j	= 0;
		
		$bonmkeluar	= array();
		$bonmmasuk	= array();
		$bbk	= array();
		$bbm	= array();
		$do	= array();

		/* Deklarasi variabel : Mencari saldo akhir */
		$x11	= array();
		$x22	= array();
		$z11	= array();
		$saldoakhirnya	= array();
		
		/* End 0f saldo akhir */
				
		if($qbrgjadi->num_rows()>0) {
		
			$cc = 1;
			
			foreach($qbrgjadi->result() as $field) {
				$qbonmkeluar	= $this->mclass->lbonkeluar($ddofirst,$ddolast,$field->i_product_motif);
				if($qbonmkeluar->num_rows()>0) {
					$row_bmkeluar	= $qbonmkeluar->row();
					$bonmkeluar[$j]	= $row_bmkeluar->jbonkeluar;
				} else {
					$bonmkeluar[$j] = 0;
				}

				$qbonmmasuk	= $this->mclass->lbonmasuk($ddofirst,$ddolast,$field->i_product_motif);
				if($qbonmmasuk->num_rows()>0) {
					$row_bmmasuk	= $qbonmmasuk->row();
					$bonmmasuk[$j]	= $row_bmmasuk->jbonmasuk; 
				} else {
					$bonmmasuk[$j] = 0;
				}

				$qbbk	= $this->mclass->lbbk($ddofirst,$ddolast,$field->i_product_motif);
				if($qbbk->num_rows()>0) {
					$row_bbk	= $qbbk->row();
					$bbk[$j]	= $row_bbk->jbbk; 
				} else {
					$bbk[$j] = 0;
				}

				$qbbm	= $this->mclass->lbbm($ddofirst,$ddolast,$field->i_product_motif);
				if($qbbm->num_rows()>0) {
					$row_bbm	= $qbbm->row();
					$bbm[$j]	= $row_bbm->jbbm; 
				} else {
					$bbm[$j] = 0;
				}

				$qdo	= $this->mclass->ldo($ddofirst,$ddolast,$field->i_product_motif);
				if($qdo->num_rows()>0) {
					$row_do	= $qdo->row();
					$do[$j]	= $row_do->jdo; 
				} else {
					$do[$j] = 0;
				}

				$qsj	= $this->mclass->lsj($ddofirst,$ddolast,$field->i_product_motif);
				if($qsj->num_rows()>0) {
					$row_sj	= $qsj->row();
					$sj[$j]	= $row_sj->jsj;
				} else {
					$sj[$j] = 0;
				}
												
				//if($no!=$qbrgjadi->num_rows()) {	
					/* Disabled 08-02-2011															
					$lstokopname	.=	
					"<tr>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\">".$no.".</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\">".$field->i_product_motif."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\">".$field->e_product_motifname."</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\" align=\"right\">".$field->n_quantity_awal."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\" align=\"right\">".$field->n_quantity_akhir."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\" align=\"right\">".$bonmmasuk[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\" align=\"right\">".$bbm[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\" align=\"right\">".$do[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; border-right:thin #000000;\" align=\"right\">".$bonmkeluar[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000;\" align=\"right\">".$bbk[$j]."&nbsp;&nbsp;</td>
					</tr>";	
					*/

				/* Mencari saldo akhir */
				$x11[$j]	= $bonmmasuk[$j]+$bbm[$j];
				$x22[$j]	= $do[$j]+$bonmkeluar[$j]+$bbk[$j]+$sj[$j];
				$z11[$j]	= $field->n_quantity_awal+$x11[$j];
				$saldoakhirnya[$j]	= $z11[$j]-$x22[$j];
				/* End 0f saldo akhir */
				/*
				$field->n_quantity_akhir
				*/					
				$lstokopname .= 
				  "<tr>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \">".$no."</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \">".$field->i_product_motif."</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \">".$field->e_product_motifname."</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; text-align:right; \">".$field->n_quantity_awal."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000;  text-align:right; \">".$bonmmasuk[$j]."</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000;  text-align:right; \">".$bbm[$j]."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; text-align:right; \">".$do[$j]."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000;  text-align:right; \">".$bbk[$j]."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; text-align:right; \">".$bonmkeluar[$j]."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; text-align:right; \">".$saldoakhirnya[$j]."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; text-align:right; \">".number_format($field->v_unitprice,'2','.',',')."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; text-align:right; \">".number_format($field->n_quantity_awal * $field->v_unitprice,'2','.',',')."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; text-align:right; \">".number_format($do[$j] * $field->v_unitprice,'2','.',',')."&nbsp;</td>
					<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000;  text-align:right; \">".number_format($saldoakhirnya[$j] * $field->v_unitprice,'2','.',',')."&nbsp;</td>
				  </tr>";					
				//} else {
					/* Disabled 08-02-2011
					$lstokopname	.=	
					"<tr>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \">".$no.".</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \">".$field->i_product_motif."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \">".$field->e_product_motifname."</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \" align=\"right\">".$field->n_quantity_awal."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \" align=\"right\">".$field->n_quantity_akhir."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \" align=\"right\">".$bonmmasuk[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \" align=\"right\">".$bbm[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \" align=\"right\">".$do[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \" align=\"right\">".$bonmkeluar[$j]."&nbsp;&nbsp;</td>
					  <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; border-bottom:thin #000000; \" align=\"right\">".$bbk[$j]."&nbsp;&nbsp;</td>
					</tr>";
					*/
				//}
				$j++;
				$no++;
				$cc++;
			}
		}
		$data['isi']	= $lstokopname;
		
		/* Disabled 08-02-2011
		$this->load->view('prntstoksaatini/vpopupform',$data);
		*/
		
		$this->load->view('prntstoksaatini/vpopupform_test',$data);		
	}
}
?>	
