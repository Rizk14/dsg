<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$data['form_title_detail_brgjadi'] = $this->lang->line('form_title_detail_brgjadi');
			$data['page_title_brgjadi']	= $this->lang->line('page_title_brgjadi');
			$data['list_brgjadi_kd_brg'] = $this->lang->line('list_brgjadi_kd_brg');
			$data['list_brgjadi_nm_brg'] = $this->lang->line('list_brgjadi_nm_brg');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['isi']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi']	= 'listbrgjadi/vmainform';
			$this->load->view('template',$data);	
		
	}
	
	function listbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbrgjadi/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbrgjadi/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);		
		$this->load->view('listbrgjadi/vlistformbrgjadi',$data);		
	}

	function listbarangjadinext() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbrgjadi/mclass');

		$query	= $this->mclass->lbarangjadi();
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/listbrgjadi/cform/listbarangjadinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']	= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page']);
		$this->load->view('listbrgjadi/vlistformbrgjadi',$data);		
	}

	function flistbarangjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		
		$data['page_title']	= "BARANG JADI";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('listbrgjadi/mclass');
		
		if(!empty($key)) {
			$query	= $this->mclass->flbarangjadi($key);
			$jml	= $query->num_rows();
		} else {
			$jml=0;
		}
		
		$list	= "";
				
		if($jml>0) {
			$cc	= 1; 			
			foreach($query->result() as $row){
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->iproduct','$row->product')\">".$row->iproduct."</a></td> 
				  <td><a href=\"javascript:settextfield('$row->iproduct','$row->product')\">".$row->product."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}
		
	function carilistbrgjadi() {
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$iproduct	= $this->input->post('i_product')?$this->input->post('i_product'):$this->input->get_post('i_product');
			
		$data['form_title_detail_brgjadi'] = $this->lang->line('form_title_detail_brgjadi');
		$data['page_title_brgjadi']	= $this->lang->line('page_title_brgjadi');
		$data['list_brgjadi_kd_brg'] = $this->lang->line('list_brgjadi_kd_brg');
		$data['list_s_produksi'] = $this->lang->line('list_s_produksi');
		$data['list_brgjadi_nm_brg'] = $this->lang->line('list_brgjadi_nm_brg');
		$data['list_brgjadi_tgl_penawaran']	= $this->lang->line('list_brgjadi_tgl_penawaran');
		$data['list_brgjadi_kategori'] = $this->lang->line('list_brgjadi_kategori');
		$data['list_brgjadi_no_penawaran'] = $this->lang->line('list_brgjadi_no_penawaran');
		$data['list_brgjadi_merek'] = $this->lang->line('list_brgjadi_merek');
		$data['list_brgjadi_hjp'] = $this->lang->line('list_brgjadi_hjp');
		$data['list_brgjadi_layout'] = $this->lang->line('list_brgjadi_layout');
		$data['list_brgjadi_status'] = $this->lang->line('list_brgjadi_status');
		$data['list_brgjadi_qty'] = $this->lang->line('list_brgjadi_qty');
		$data['list_brgjadi_kd_motif'] = $this->lang->line('list_brgjadi_kd_motif');
		$data['list_brgjadi_nm_motif']	= $this->lang->line('list_brgjadi_nm_motif');
		$data['list_brgjadi_stok']	= $this->lang->line('list_brgjadi_stok');
		$data['button_batal']	= $this->lang->line('button_batal');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['lmotif']	= "";
		$data['list']		= "";
		$data['limages']	= base_url();	

		$this->load->model('listbrgjadi/mclass');
		$qry_list_brg_jd	= $this->mclass->clistbrgjadi($iproduct);
		if($qry_list_brg_jd->num_rows() > 0 ) {
			$row_list_brg_jd	= $qry_list_brg_jd->row();
			
			$data['idproduk']	= $row_list_brg_jd->iproduct;
			$data['nmproduk']	= $row_list_brg_jd->product;
			$data['nmkat']	= $row_list_brg_jd->kategori;
			$data['nmmerek']	= $row_list_brg_jd->merek;
			$data['nmtampilan']	= $row_list_brg_jd->tampilan;
			$data['kualitas']	= $row_list_brg_jd->kualitas;
			$data['stop']	= ($row_list_brg_jd->stop==true)?"checked":"";
			$data['penawaran']	= $row_list_brg_jd->penawaran;
			$data['tpenawaran']	= $row_list_brg_jd->tpenawaran;
			$data['hjp']	= $row_list_brg_jd->hjp;
			
			$qsproduct	= $this->mclass->statusproduct($row_list_brg_jd->status);
			if($qsproduct->num_rows()>0) {
				$row_sproduct	= $qsproduct->row();
				$data['status']	= $row_sproduct->e_statusname;
			} else {
				$data['status']	= "";
			}
			
		} else {
			$data['idproduk']	= "";
			$data['nmproduk']	= "";
			$data['nmkat']	= "";
			$data['nmmerek']	= "";
			$data['nmtampilan']	= "";
			$data['kualitas']	= "";
			$data['stop']	= "";
			$data['penawaran']	= "";
			$data['tpenawaran']	= "";
			$data['hjp']	= "";
			$data['status']	= "";		
		}
		$data['isi_detail']	= $this->mclass->cdetailbrgjadi($iproduct);
		$data['isi']	= 'listbrgjadi/vlistform';
			$this->load->view('template',$data);	
	
	}
}
?>
