<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('info-pembelian-jhtpack-faktur-gr/mmaster');
  }

  function lapfakturwip(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	$data['list_kel_unit'] = $this->mmaster->getkelunit();
	//~ $data['list_unit_packing'] = $this->mmaster->getlistunitpacking();
	//~ $data['list_unit_jahit'] = $this->mmaster->getlistunitjahit(); 
	$data['isi'] = 'info-pembelian-jhtpack-faktur-gr/vmainformlapfakturwip';
	$this->load->view('template',$data);
  }
  
  function lapfakturwipview(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
    $data['isi'] = 'info-pembelian-jhtpack-faktur-gr/vformviewlapfakturwip';
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE); 
	$jenis_masuk = $this->input->post('jenis_masuk', TRUE);
	$kelompok = $this->input->post('kelompok', TRUE);
	$querynya = $this->mmaster->get_all_fakturwip($date_from, $date_to, $kelompok, $jenis_masuk);
	$data['query'] = $querynya;
	if (is_array($querynya))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
		if($kelompok != 0){		
		//~ 
	//~ // ambil data nama unit jahit
	$query3	= $this->db->query(" SELECT a.nama,a.kode_unit FROM tm_unit_jahit a ,
	tm_link_unit b ,tm_kelompok_unit c,tm_kelompok_unit_detail d
				where a.id=b.id_unit_jahit AND d.id_unit_jahit=b.id_unit_jahit  AND d.id_kelompok_unit=c.id 
	AND c.id = '$kelompok' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_unit_jahit	= $hasilrow->nama;
		$kode_unit_jahit	= $hasilrow->kode_unit;
	}
	else{
		$nama_unit_jahit = '';
		$kode_unit_jahit	= '';
	}
}
	else{
		$nama_unit_jahit = "ALL";
		$kode_unit_jahit	= "ALL";
	}
	if($kelompok != 0){		
	//~ // ambil data nama unit packing
	$query3	= $this->db->query(" SELECT a.nama,a.kode_unit FROM tm_unit_packing a ,
	tm_link_unit b ,tm_kelompok_unit c,tm_kelompok_unit_detail d
				where a.id=b.id_unit_packing AND d.id_unit_packing=b.id_unit_packing  AND d.id_kelompok_unit=c.id 
	AND c.id = '$kelompok' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_unit_packing	= $hasilrow->nama;
		$kode_unit_packing	= $hasilrow->kode_unit;
	}
	else{
		$nama_unit_packing = '';
		$kode_unit_packing	= '';
	}
}
else{
		$nama_unit_packing = "ALL";
		$kode_unit_packing	= "ALL";
	}
	if($kelompok != 0){		
	$query4	= $this->db->query(" SELECT * FROM tm_kelompok_unit 
	WHERE id = '$kelompok' ");
	if ($query4->num_rows() > 0){
		$hasilrow4 = $query4->row();
		$nama_kelompok=$hasilrow4->nama_kelompok;
	}
	}
	else{
		$nama_kelompok="ALL";
		}
	$data['nama_kelompok'] = $nama_kelompok;
	$data['kelompok'] = $kelompok;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$data['nama_unit_jahit'] = $nama_unit_jahit;
	$data['nama_unit_packing'] = $nama_unit_packing;
	$data['jenis_masuk'] = $jenis_masuk;
	$this->load->view('template',$data);
  }
  
   function export_excel_lapfakturwip() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$kode_unit_jahit = $this->input->post('kode_unit_jahit', TRUE);  
		$kode_unit_packing = $this->input->post('kode_unit_packing', TRUE);  
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);
		$jenis_masuk = $this->input->post('jenis_masuk', TRUE);  		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_fakturwip_for_print($date_from, $date_to, $kode_unit_jahit,$kode_unit_packing, $jenis_masuk);
		
	$html_data = "
	<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
	<thead>
	<tr>
			<th colspan='14' align='center'>Laporan Faktur Pembelian Makloon Hasil Jahit + Packing(WIP)</th>
	 </tr>
		 
		 <tr>
			<th colspan='14' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
	</tr>
	 <tr class='judulnya'>
		 <th rowspan='2'>No</th>
		 <th rowspan='2'>Unit Jahit</th>
		 <th rowspan='2'>Unit Packing</th>
		 <th rowspan='2'>No Faktur</th>
		 <th rowspan='2'>Tgl Faktur</th>
		
		 <th colspan='8'>List Brg Jadi</th>		 
		 <th rowspan='2'>Grand Total </th>
	 </tr>
	 <tr class='judulnya'>
		<th>Kode Brg Jadi</th>
		<th>Nama Brg Jadi</th>
		<th>Satuan</th>
		<th>Quantity</th>
		<th>Harga Jahit (Rp.)</th>
		<th>Harga Packing (Rp.)</th>
		<th>Diskon</th>
		<th>Subtotal</th>
		
	 </tr>
	</thead>
	<tbody>";
	
			if (is_array($query)) {
				$tot_grandtotal = 0;
				$no = 1;
			
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.="<tr class=\"record\">";
				 $html_data.= "<td align='center'>".$no."</td>";
				 $html_data.="<td>".$query[$j]['nama_unit_jahit']."</td>";
				 $html_data.="<td>".$query[$j]['nama_unit_packing']."</td>";
				  $html_data.=  "<td>".$query[$j]['no_faktur']."</td>";

				 $html_data.=  "<td>".$query[$j]['tgl_faktur']."</td>";
				
				 
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['kode_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
			 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				
				 
				 $html_data.="<td align='center'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				$html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
				$html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['harga_j'];
				$html_data.=  "" ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['harga_p'];
				$html_data.=  "" ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['diskon'];
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";
				 
				  $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				$html_data.= $var_detail[$k]['subtotal'];
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";

				

			 $html_data.=    "<td align='right'>".$query[$j]['grandtotal']."</td>";
				 
				$tot_grandtotal+=$query[$j]['grandtotal'];
				 
				  $html_data.=  "</tr>";

				$no++;
		 	} // end for
		   }
			else {
				$tot_grandtotal = 0;
			}

		 $html_data.=" <tr>
			<td colspan='13' align='center'><b>TOTAL</b></td>
			<td align='center'><b> ".number_format($tot_grandtotal,2,',','.'). "</b></td>
		 </tr>
 	</tbody>
</table><br>";
		// ====================================================================

		$nama_file = "laporan_faktur_pembelian_wip_jahit_packing";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
    function export_excel_lapfakturwip_khusus() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$kode_unit_jahit = $this->input->post('kode_unit_jahit', TRUE);  
		$kode_unit_packing = $this->input->post('kode_unit_packing', TRUE);  
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);
		$jenis_masuk = $this->input->post('jenis_masuk', TRUE);  		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_fakturwip_for_print($date_from, $date_to, $kode_unit_jahit,$kode_unit_packing, $jenis_masuk);
		
	$html_data = "
	<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
	<thead>
	<tr>
			<th colspan='12' align='center'>Laporan Faktur Pembelian Makloon Hasil Jahit + Packing(WIP)</th>
	 </tr>
		 
		 <tr>
			<th colspan='12' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
	</tr>
	 <tr class='judulnya'>
		 <th rowspan='2'>No</th>
		 
		 <th rowspan='2'>No Faktur</th>
		 <th rowspan='2'>Tgl Faktur</th>
		
		 <th colspan='8'>List Brg Jadi</th>		 
		 <th rowspan='2'>Grand Total </th>
	 </tr>
	 <tr class='judulnya'>
		<th>Kode Brg Jadi</th>
		<th>Nama Brg Jadi</th>
		<th>Satuan</th>
		<th>Quantity</th>
		<th>Harga Jahit (Rp.)</th>
		<th>Harga Packing (Rp.)</th>
		<th>Diskon</th>
		<th>Subtotal</th>
		
	 </tr>
	</thead>
	<tbody>";
	
			if (is_array($query)) {
				$tot_grandtotal = 0;
				$no = 1;
			
			 for($j=0;$j<count($query);$j++){
				 				 
				 $html_data.="<tr class=\"record\">";
				 $html_data.= "<td align='center'>".$no."</td>";
				 ;
				 
				 $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['no_faktur'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.="</td>";
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['tgl_faktur'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.="</td>";
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['kode_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.="</td>";
				  $html_data.= "<td nowrap>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['nama_brg'];
						  //echo $var_detail[$k]['nama_brg'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.="</td>";
			 $html_data.= "<td>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				 $html_data.= $var_detail[$k]['satuan'];
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				
				 
				 $html_data.="<td align='center'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				$html_data.= $var_detail[$k]['qty'];
						  if ($k<$hitung-1)
				$html_data.= "<br> ";
					}
				 }
				 $html_data.="</td>";
				 
				 $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['harga_j'];
				$html_data.=  "" ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['harga_p'];
				$html_data.=  "" ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				 $html_data.= "</td>";
				 
				  $html_data.="<td align='right' style='white-space:nowrap;'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
						$html_data.= $var_detail[$k]['diskon'];
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
				 $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";
				 
				  $html_data.= "<td align='right'>";
				 if (is_array($query[$j]['detail_beli'])) {
					 $var_detail = array();
					 $var_detail = $query[$j]['detail_beli'];
					 $hitung = count($var_detail);
					for($k=0;$k<count($var_detail); $k++){
				$html_data.= $var_detail[$k]['subtotal'];
				 $html_data.= ""   ;
						  if ($k<$hitung-1)
						      $html_data.= "<br> ";
					}
				 }
				  $html_data.= "</td>";

				

			 $html_data.=    "<td align='right'>".$query[$j]['grandtotal']."</td>";
				 
				$tot_grandtotal+=$query[$j]['grandtotal'];
				 
				  $html_data.=  "</tr>";

				$no++;
		 	} // end for
		   }
			else {
				$tot_grandtotal = 0;
			}

		 $html_data.=" <tr>
			<td colspan='11' align='center'><b>TOTAL</b></td>
			<td align='center'><b> ".number_format($tot_grandtotal,2,',','.'). "</b></td>
		 </tr>
 	</tbody>
</table><br>";
		// ====================================================================

		$nama_file = "laporan_faktur_pembelian_wip_jahit_packing";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
}
