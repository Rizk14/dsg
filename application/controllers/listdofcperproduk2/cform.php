<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('listdofcperproduk/mclass');
	}
	
	function index() {
		
		 $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');
			$data['disabled']	= 'f';
			$data['detail']		= "";
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi']	= 'listdofcperproduk/vmainform';
			$this->load->view('template',$data);
			
		}	
	
	
	function viewdata() {
 	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['button_keluar']	= $this->lang->line('button_keluar');	
		$data['detail']		= "";
		$data['list']		= "";
		$data['limages']	= base_url();
		$data['lopvsdo']	= "";
		
		$d_first	= $this->input->post('d_op_first');
		$d_last	= $this->input->post('d_op_last');
		
		$data['tglmulai']	= $d_first;
		$data['tglakhir']	= $d_last;
		
		$e_d_do_first	= explode("/",$d_first,strlen($d_first));
		$e_d_do_last	= explode("/",$d_last,strlen($d_last));
		
		$n_d_do_first	= !empty($e_d_do_first[2])?$e_d_do_first[2].'-'.$e_d_do_first[1].'-'.$e_d_do_first[0]:"";
		$n_d_do_last	= !empty($e_d_do_last[2])?$e_d_do_last[2].'-'.$e_d_do_last[1].'-'.$e_d_do_last[0]:"";

		$data['var_ddofirst']	= $n_d_do_first;
		$data['var_ddolast']	= $n_d_do_last;
		
		$data['query']	= $this->mclass->getqtyperproduk($n_d_do_first,$n_d_do_last);
			$data['isi']	= 'listdofcperproduk/vlistform';
			$this->load->view('template',$data);
		
	}
}
?>
