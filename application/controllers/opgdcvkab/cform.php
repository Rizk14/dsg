<?php
class Cform extends CI_Controller {

	function __construct() {
		parent::__construct();		
		
		/* Disabled 
		$this->output->cache(2);
		*/ 
	}
	
	// 06-09-2021, ambil list OP dari cv kab
	function opcvkab() {
		
		$data['limages']		= base_url();
		
		$this->load->model('opgdcvkab/mclass');

		$icustomer = '159'; // i_customer nya KAB di sysdbduta

		$data['query']	= $this->mclass->get_op_kab($icustomer);
		$data['isi']='opgdcvkab/vformopcvkab';		
		$this->load->view('template',$data);
	}

	function simpanopkab() {
		$iterasi			= $this->input->post('iteration',true); 
		$d_delivery_limit	= $this->input->post('d_delivery_limit');
		$exp_ddl = explode("/", $d_delivery_limit);
		$tgl1 = trim($exp_ddl[0]); 
		$bln1 = trim($exp_ddl[1]);
		$thn1 = trim($exp_ddl[2]);
		$d_delivery_limit = $thn1."-".$bln1."-".$tgl1;
			
		$no_op	= array();
		$tgl_op	= array();
		$i_product_motif	= array();
		$i_product	= array();
		$e_product_name	= array();
		$qty	= array();
		$i_customer	= '159'; // khusus KAB
		$i_branch_code	= '6248'; // KAB
		
		$fck	= array();

		

		for($cacah=1;$cacah<=$iterasi;$cacah++) {
		//~ for($cacah=0;$cacah<=$iterasi;$cacah++) {
				$no_op[$cacah] = $this->input->post('no_op_'.$cacah); //echo $no_op[$cacah]."<br>";
				$tgl_op[$cacah] = $this->input->post('tgl_op_'.$cacah);
				
				
				
				$i_product[$cacah] = $this->input->post('i_product_'.$cacah);
				
				$i_product_motif[$cacah] = $this->input->post('i_product_motif_'.$cacah);
				$e_product_name[$cacah] = $this->input->post('e_product_name_'.$cacah);
				$qty[$cacah] = $this->input->post('qty_'.$cacah);
				$fck[$cacah] = $this->input->post('f_ck_tblItem'.$cacah);
				
		}

		$this->load->model('opgdcvkab/mclass');

		$this->mclass->simpanopkab(	$no_op,$tgl_op,$i_product, $i_product_motif,$e_product_name, $qty, $fck,$iterasi, 
									$i_customer, $i_branch_code, $d_delivery_limit);
		
	}

}
?>	

