<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('kartuhutang/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['isi'] = 'kartuhutang/vmainform';
	$this->load->view('template',$data);

  }
  
  function view(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			
    $supplier = $this->input->post('supplier', TRUE);
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);  
	
	$data['query'] = $this->mmaster->get_all_sjpembelian($date_from, $date_to, $supplier);
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	else
		$data['jum_total'] = 0;
	$data['isi'] = 'kartuhutang/vformview';
	
	// ambil data nama supplier
	$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp, kategori FROM tm_supplier WHERE id = '$supplier' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$kode_supplier	= $hasilrow->kode_supplier;
		$nama_supplier	= $hasilrow->nama;
		$kategori_sup	= $hasilrow->kategori;
	}
	else {
		$nama_supplier = '';
		$kode_supplier = '';
		$kategori_sup = '';
	}
	
	// ------- hitung saldo awal berdasarkan supplier dan range tanggalnya (copy dari rekap hutang dagang) --------------------------
	//$saldo_awal = 0;
	$pisah1 = explode("-", $date_from);
	$thn1= $pisah1[2];
	$bln1= $pisah1[1];
	$tgl1= $pisah1[0];
		
	if ($bln1 == 1) {
		$bln_query = 12;
		$thn_query = $thn1-1;
	}
	else {
		$bln_query = $bln1-1;
		$thn_query = $thn1;
		if ($bln_query < 10)
			$bln_query = "0".$bln_query;
	}
		
	if ($bln1 == '07' && $thn1 == '2015') {
		if ($kategori_sup == '1') {
			$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
								INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
								WHERE a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.id_supplier = '$supplier' ");
			if ($query2->num_rows() > 0){
				$hasilrow = $query2->row();
				$tot_hutang	= $hasilrow->jum_stok_opname;
				if ($tot_hutang == '')
					$tot_hutang = 0;
			}
			else
				$tot_hutang = 0;
		}
		else
			$tot_hutang = 0;
	}
	else {
		if ($kategori_sup == '1') {
			$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a 
								INNER JOIN tm_supplier b ON a.id_supplier = b.id
								WHERE a.status_aktif = 't'
								AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
								AND a.tgl_sj >= '2015-07-01'
								AND b.id = '$supplier' ");
												
			$hasilrow = $query2->row();
			$tot_hutang	= $hasilrow->tot_hutang;
			if ($tot_hutang == '')
				$tot_hutang = 0;
								
			// 09-09-2015, tambahkan hutang dari saldo awal hutang yg diinput pertama kali (= saldo akhir juni 2015)
			$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
								INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
								WHERE a.bulan = '06' AND a.tahun = '2015' AND b.id_supplier = '$supplier' ");
			if ($query2->num_rows() > 0){
				$hasilrow = $query2->row();
				$tot_hutang	+= $hasilrow->jum_stok_opname;
			}
			// ---------------------------------------------
		}
		else {
			$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian_makloon a 
								INNER JOIN tm_supplier b ON a.id_supplier = b.id
								WHERE a.status_aktif = 't'
								AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
								AND a.tgl_sj >= '2015-07-01'
								AND b.id = '$supplier' ");							
			$hasilrow = $query2->row();
			$tot_hutang	= $hasilrow->tot_hutang;
			if ($tot_hutang == '')
				$tot_hutang = 0;
		}
	}
	
	// hitung pelunasannya				
	$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat 
				FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
				WHERE a.tgl < to_date('$date_from','dd-mm-yyyy')
				AND a.tgl >= '2015-07-01'
				AND b.id_supplier = '$supplier' ");
				
	$hasilrow = $query2->row();
	$tot_bayar	= $hasilrow->tot_bayar;
	$tot_bulat	= $hasilrow->tot_bulat;
	if ($tot_bayar == '')
		$tot_bayar = 0;
	if ($tot_bulat == '')
		$tot_bulat = 0;
								
	// jumlah retur jika ada
	// koreksi 25 okt 2011: ga perlu ada acuan ke status_nota di nota retur
	// 27-08-2015 UNTUK SUPPLIER MAKLOON GA PAKE RETUR
	if ($kategori_sup == '1') {
		$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
					INNER JOIN tm_retur_beli_detail c ON a.id = c.id_retur_beli WHERE
					a.id_supplier = '$supplier'
					AND a.tgl_retur < to_date('$date_from','dd-mm-yyyy')
					AND a.tgl_retur >= '2015-07-01' ");
						
		$hasilrow = $query2->row();
		$tot_retur	= $hasilrow->tot_retur;	
		if ($tot_retur == '')
			$tot_retur = 0;
	}
	else
		$tot_retur = 0;
								
	//ini asli. 30-06-2015 DIMODIF TANPA UANG MUKA
	//$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar-$tot_retur+$tot_bulat;
	$tot_hutang = $tot_hutang-$tot_bayar-$tot_retur+$tot_bulat;
	$saldo_awal = $tot_hutang;
	// ----------------------------------------------------------------------------------------------
	
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['supplier'] = $supplier;
	$data['kode_supplier'] = $kode_supplier;
	$data['nama_supplier'] = $nama_supplier;
	$data['saldo_awal'] = $saldo_awal;
	$this->load->view('template',$data);
  }
  
  // 04-12-2015
  function export_excel() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$supplier = $this->input->post('id_supplier', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);  
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mmaster->get_all_sjpembelian($date_from, $date_to, $supplier);
		
		// ambil data nama supplier
		$query3	= $this->db->query(" SELECT kode_supplier, nama, pkp, kategori FROM tm_supplier WHERE id = '$supplier' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_supplier	= $hasilrow->kode_supplier;
			$nama_supplier	= $hasilrow->nama;
			$kategori_sup	= $hasilrow->kategori;
		}
		else {
			$nama_supplier = '';
			$kode_supplier = '';
			$kategori_sup = '';
		}
		
		// ------- hitung saldo awal berdasarkan supplier dan range tanggalnya (copy dari rekap hutang dagang) --------------------------
		//$saldo_awal = 0;
		$pisah1 = explode("-", $date_from);
		$thn1= $pisah1[2];
		$bln1= $pisah1[1];
		$tgl1= $pisah1[0];
			
		if ($bln1 == 1) {
			$bln_query = 12;
			$thn_query = $thn1-1;
		}
		else {
			$bln_query = $bln1-1;
			$thn_query = $thn1;
			if ($bln_query < 10)
				$bln_query = "0".$bln_query;
		}
			
		if ($bln1 == '07' && $thn1 == '2015') {
			if ($kategori_sup == '1') {
				$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
									INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
									WHERE a.bulan = '$bln_query' AND a.tahun = '$thn_query' AND b.id_supplier = '$supplier' ");
				if ($query2->num_rows() > 0){
					$hasilrow = $query2->row();
					$tot_hutang	= $hasilrow->jum_stok_opname;
					if ($tot_hutang == '')
						$tot_hutang = 0;
				}
				else
					$tot_hutang = 0;
			}
			else
				$tot_hutang = 0;
		}
		else {
			if ($kategori_sup == '1') {
				$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian a 
									INNER JOIN tm_supplier b ON a.id_supplier = b.id
									WHERE a.status_aktif = 't'
									AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj >= '2015-07-01'
									AND b.id = '$supplier' ");
													
				$hasilrow = $query2->row();
				$tot_hutang	= $hasilrow->tot_hutang;
				if ($tot_hutang == '')
					$tot_hutang = 0;
									
				// 09-09-2015, tambahkan hutang dari saldo awal hutang yg diinput pertama kali (= saldo akhir juni 2015)
				$query2	= $this->db->query(" SELECT b.jum_stok_opname FROM tt_stok_opname_hutang_dagang a
									INNER JOIN tt_stok_opname_hutang_dagang_detail b ON a.id = b.id_stok_opname_hutang_dagang
									WHERE a.bulan = '06' AND a.tahun = '2015' AND b.id_supplier = '$supplier' ");
				if ($query2->num_rows() > 0){
					$hasilrow = $query2->row();
					$tot_hutang	+= $hasilrow->jum_stok_opname;
				}
				// ---------------------------------------------
			}
			else {
				$query2	= $this->db->query(" SELECT SUM(a.total) as tot_hutang FROM tm_pembelian_makloon a 
									INNER JOIN tm_supplier b ON a.id_supplier = b.id
									WHERE a.status_aktif = 't'
									AND a.tgl_sj < to_date('$date_from','dd-mm-yyyy') 
									AND a.tgl_sj >= '2015-07-01'
									AND b.id = '$supplier' ");							
				$hasilrow = $query2->row();
				$tot_hutang	= $hasilrow->tot_hutang;
				if ($tot_hutang == '')
					$tot_hutang = 0;
			}
		}
		
		// hitung pelunasannya				
		$query2	= $this->db->query(" SELECT SUM(b.jumlah_bayar) as tot_bayar, SUM(b.pembulatan) as tot_bulat 
					FROM tm_payment_pembelian a INNER JOIN tm_payment_pembelian_detail b ON a.id = b.id_payment
					WHERE a.tgl < to_date('$date_from','dd-mm-yyyy')
					AND a.tgl >= '2015-07-01'
					AND b.id_supplier = '$supplier' ");
					
		$hasilrow = $query2->row();
		$tot_bayar	= $hasilrow->tot_bayar;
		$tot_bulat	= $hasilrow->tot_bulat;
		if ($tot_bayar == '')
			$tot_bayar = 0;
		if ($tot_bulat == '')
			$tot_bulat = 0;
									
		// jumlah retur jika ada
		// koreksi 25 okt 2011: ga perlu ada acuan ke status_nota di nota retur
		// 27-08-2015 UNTUK SUPPLIER MAKLOON GA PAKE RETUR
		if ($kategori_sup == '1') {
			$query2	= $this->db->query(" SELECT SUM(c.qty*c.harga) as tot_retur FROM tm_retur_beli a 
						INNER JOIN tm_retur_beli_detail c ON a.id = c.id_retur_beli WHERE
						a.id_supplier = '$supplier'
						AND a.tgl_retur < to_date('$date_from','dd-mm-yyyy')
						AND a.tgl_retur >= '2015-07-01' ");
							
			$hasilrow = $query2->row();
			$tot_retur	= $hasilrow->tot_retur;	
			if ($tot_retur == '')
				$tot_retur = 0;
		}
		else
			$tot_retur = 0;
									
		//ini asli. 30-06-2015 DIMODIF TANPA UANG MUKA
		//$tot_hutang = $tot_hutang-$uang_muka-$tot_bayar-$tot_retur+$tot_bulat;
		$tot_hutang = $tot_hutang-$tot_bayar-$tot_retur+$tot_bulat;
		$saldo_awal = $tot_hutang;
		// ----------------------------------------------------------------------------------------------
		
		// ===================================================================================================
		
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
		<thead>
		 <tr>
			<th colspan='9' align='center'>KARTU HUTANG</th>
		 </tr>
		 <tr>
			<th colspan='9' align='center'>Supplier: $kode_supplier - $nama_supplier </th>
		 </tr>
		 <tr>
			<th colspan='9' align='center'>Periode: $date_from s.d $date_to</th>
		 </tr>
		 <tr>
			 <th>Tanggal</th>
			 <th width='30%'>Uraian</th>
			 <th>Nomor Bukti</th>
			 <th>Saldo Awal</th>
			 <th>Pembelian</th>
			 <th>Pelunasan</th>
			 <th>C/n</th>
			 <th>Pembulatan</th>
			 <th>Saldo Akhir</th>";		
		 $html_data.="</tr>
		</thead>
		<tbody>";
		
		$html_data.="
		<tr>
			<td>".$date_from."</td>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td align='right'>".$saldo_awal."</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		";
		if (is_array($query)) {
			$tot_pembelian = 0;
			$tot_pelunasan = 0;
			$tot_pembulatan = 0;
				
			for($j=0;$j<count($query);$j++){
				// hitung jumlah total masing2 field
				$tot_pembelian+= $query[$j]['pembelian'];
				$tot_pelunasan+= $query[$j]['pelunasan'];
				$tot_pembulatan+= $query[$j]['pembulatan'];
					
			} // end header
		}
		else {
			$tot_saldoawal = 0;
			$tot_pembelian = 0;
			$tot_pelunasan = 0;
			$tot_pembulatan = 0;
		}
		
		if (is_array($query)) {
			 for($j=0;$j<count($query);$j++){
				 $html_data.= "<tr>
				 <td>".$query[$j]['tgl_bukti']."</td>";
				 if ($query[$j]['is_pembelian'] == 1) {
					 if ($query[$j]['status_lunas'] == 't')
						$html_data.= "<td class='uraian'>Pembelian (SUDAH LUNAS): <br>".$query[$j]['list_brg']."</td>";
					else
						$html_data.= "<td class='uraian'>Pembelian (BELUM LUNAS): <br>".$query[$j]['list_brg']."</td>";
				}
				 else
					$html_data.= "<td class='uraian'>Pelunasan: <br>".$query[$j]['deskripsi_pelunasan']."</td>";
				 $html_data.= "<td>'".$query[$j]['no_bukti']."</td>";
				 $html_data.= "<td>&nbsp;</td>";
				 
				 if ($query[$j]['pembelian'] != '')
					$html_data.=    "<td align='right'>".$query[$j]['pembelian']."</td>";
				 else
					$html_data.=    "<td>&nbsp;</td>";
					
				 if ($query[$j]['pelunasan'] != '')
					$html_data.=    "<td align='right'>".$query[$j]['pelunasan']."</td>";
				 else
					$html_data.=    "<td>&nbsp;</td>";
				 $html_data.=    "<td>&nbsp;</td>";
				 
				 if ($query[$j]['pelunasan'] != '')
					$html_data.=    "<td align='right'>".$query[$j]['pembulatan']."</td>";
				 else
					$html_data.=    "<td>&nbsp;</td>";
				 $html_data.=    "<td>&nbsp;</td>";
				 $html_data.=  "</tr>";
		 	}
		 }
		 
		 $saldo_akhir = $saldo_awal+$tot_pembelian-$tot_pelunasan+$tot_pembulatan;
		 
		 $html_data.= "<tr>
			<td>".$date_to."</td>
			<td>Saldo Akhir</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align='right'>".$saldo_akhir."</td>
		</tr>
		<tr>
			<td colspan='3' align='right'><b>TOTAL</b></td>
			<td align='right'><b>".$saldo_awal."</b></td>
			<td align='right'><b>".$tot_pembelian."</b></td>
			<td align='right'><b>".$tot_pelunasan."</b></td>
			<td align='right'>&nbsp;</td>
			<td align='right'><b>".$tot_pembulatan."</b></td>
			<td align='right'><b>".$saldo_akhir."</b></td>
		 </tr>
		";
		 $html_data.="</tbody>
		</table>";

		$nama_file = "kartu_hutang";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }

}
