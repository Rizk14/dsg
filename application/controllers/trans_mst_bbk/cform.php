<?php
/*
v = variables
*/
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();		
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bbk/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'trans_mst_bbk/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		$qry_class				= $this->mclass->code();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['istatusbbk']	= $row_class->code;
		} else {
			$data['istatusbbk']	= 1;
		}
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'trans_mst_bbk/vmainform';
		$this->load->view('template',$data);
	}
function tambah() {
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bbk/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'trans_mst_bbk/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		$qry_class				= $this->mclass->code();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['istatusbbk']	= $row_class->code;
		} else {
			$data['istatusbbk']	= 1;
		}
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		
		$data['isi']	= 'trans_mst_bbk/vmainformadd';
		$this->load->view('template',$data);
	}
	function pagesnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bbk/mclass');
		$query	= $this->mclass->view();
		$jml	= $query->num_rows();
		
		$pagination['base_url'] = 'trans_mst_bbk/cform/pagesnext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();
		$qry_class				= $this->mclass->code();
		if($qry_class->num_rows()>0) {
			$row_class	= $qry_class->row();
			$data['istatusbbk']	= $row_class->code;
		} else {
			$data['istatusbbk']	= 1;
		}	
		$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
		$data['isi']	= 'trans_mst_bbk/vmainform';
		$this->load->view('template',$data);
	}
	
	function detail() {	
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$data['isi']	= 'trans_mst_bbk/vmainform';
		$this->load->view('template',$data);
	}
	
	function simpan() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$istatusbbk	= @$this->input->post('i_status_bbk')?@$this->input->post('i_status_bbk'):@$this->input->get_post('i_status_bbk');
		$estatusname= @$this->input->post('e_statusname')?@$this->input->post('e_statusname'):@$this->input->get_post('e_statusname');
		
		if((isset($istatusbbk) || !empty($istatusbbk)) && 
		    (isset($estatusname) || !empty($estatusname)) &&
		    (($istatusbbk!=0 || $istatusbbk!="")) && ($estatusname!=0 || $estatusname!="")) {
			$this->load->model('trans_mst_bbk/mclass');
			$this->mclass->msimpan($istatusbbk,$estatusname);
		} else {
			$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('trans_mst_bbk/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'trans_mst_bbk/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			$qry_class				= $this->mclass->code();
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['istatusbbk']	= $row_class->code;
			} else {
				$data['istatusbbk']	= 1;
			}
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			
			$data['isi']	= 'trans_mst_bbk/vmainform';
		$this->load->view('template',$data);
		}
	}
	
	function edit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id	= $this->uri->segment(4,0);
		$data['id']	= $id;
		$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
		$limages			= base_url();
		$data['list']		= "";
		$this->load->model('trans_mst_bbk/mclass');
		$qry_klsbrg		= $this->mclass->medit($id);
		
		if( $qry_klsbrg->num_rows() > 0 ) {
			$row_klsbrg	= $qry_klsbrg->row();
			$data['i_status_bbk']	= $row_klsbrg->i_status_bbk;
			$data['e_statusname']	= $row_klsbrg->e_statusname;
		} else {
			$data['i_status_bbk']	= "";
			$data['e_statusname']	= "";
		}
		
		$data['isi']	= 'trans_mst_bbk/veditform';
		$this->load->view('template',$data);
		
	}
	
	
	function actedit() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$istatusbbk	= @$this->input->post('i_status_bbk')?@$this->input->post('i_status_bbk'):@$this->input->get_post('i_status_bbk');
		$estatusname= @$this->input->post('e_statusname')?@$this->input->post('e_statusname'):@$this->input->get_post('e_statusname');
		
		if((isset($istatusbbk) || !empty($istatusbbk)) && 
		    (isset($estatusname) || !empty($estatusname)) &&
		    (($istatusbbk!=0 || $istatusbbk!="")) && ($estatusname!=0 || $estatusname!="")) {
			$this->load->model('trans_mst_bbk/mclass');
			$this->mclass->mupdate($istatusbbk,$estatusname);
		} else {
			$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
			$data['detail']		= "";
			$data['list']		= "";
			$data['not_defined']= "Form pengisian hrs diisi lengkap, terimakasih";
			$data['limages']	= base_url();
			
			$this->load->model('trans_mst_bbk/mclass');
			$query	= $this->mclass->view();
			$jml	= $query->num_rows();
			
			$pagination['base_url'] = 'trans_mst_bbk/cform/pagesnext/';
			$pagination['total_rows']	= $jml;
			$pagination['per_page']		= 20;
			$pagination['first_link'] 	= 'Awal';
			$pagination['last_link'] 	= 'Akhir';
			$pagination['next_link'] 	= 'Selanjutnya';
			$pagination['prev_link'] 	= 'Sebelumnya';
			$pagination['cur_page'] 	= $this->uri->segment(4,0);
			$this->pagination->initialize($pagination);
			$data['create_link']	= $this->pagination->create_links();
			$qry_class				= $this->mclass->code();
			if($qry_class->num_rows()>0) {
				$row_class	= $qry_class->row();
				$data['istatusbbk']	= $row_class->code;
			} else {
				$data['istatusbbk']	= 1;
			}
			$data['query']	= $this->mclass->viewperpages($pagination['per_page'],$pagination['cur_page']);
			
			$data['isi']	= 'trans_mst_bbk/vmainform';
		$this->load->view('template',$data);
			
		}
	}
	
	/* 20052011
	function actdelete(){
		$id = $this->input->post('pid')?$this->input->post('pid'):$this->input->get_post('pid');
		$this->db->delete('tr_status_bbk',array('i_status_bbk'=>$id));
	}
	*/

	function actdelete(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$id = $this->input->post('id')?$this->input->post('id'):$this->uri->segment(4);
		$this->load->model('trans_mst_bbk/mclass');
		$this->mclass->delete($id);
	}
		
	function cari() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_status_bbk	= $this->input->post('txt_i_status_bbk')?$this->input->post('txt_i_status_bbk'):$this->input->get_post('txt_i_status_bbk');
		$txt_e_statusname	= $this->input->post('txt_e_statusname')?$this->input->post('txt_e_statusname'):$this->input->get_post('txt_e_statusname');

		$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bbk/mclass');
		$query	= $this->mclass->viewcari($txt_i_status_bbk,$txt_e_statusname);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'trans_mst_bbk/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();

		$data['isi']	= $this->mclass->mcari($txt_i_status_bbk,$txt_e_statusname,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('trans_mst_bbk/vcariform',$data);
	}

	function carinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		$txt_i_class	= $this->input->post('txt_i_class')?$this->input->post('txt_i_class'):$this->input->get_post('txt_i_class');
		$txt_e_class_name	= $this->input->post('txt_e_class_name')?$this->input->post('txt_e_class_name'):$this->input->get_post('txt_e_class_name');

		$data['page_title']	= "MASTER TRANSAKSI BBK (KETERANGAN)";
		$data['detail']		= "";
		$data['list']		= "";
		$data['not_defined']	= "";
		$data['limages']	= base_url();
		
		$this->load->model('trans_mst_bbk/mclass');
		$query	= $this->mclass->viewcari($txt_i_class,$txt_e_class_name);
		$jml	= $query->num_rows();
		
		$pagination['base_url'] 	= 'trans_mst_bbk/cform/carinext/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(4,0);
		$this->pagination->initialize($pagination);
		$data['create_link']	= $this->pagination->create_links();

		$data['isi']	= $this->mclass->mcari($txt_i_class,$txt_e_class_name,$pagination['per_page'],$pagination['cur_page']);
		$this->load->view('trans_mst_bbk/vcariform',$data);
	}
}
?>
