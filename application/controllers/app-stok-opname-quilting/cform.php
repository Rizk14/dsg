<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('app-stok-opname-quilting/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	$data['isi'] = 'app-stok-opname-quilting/vmainform';
	$data['msg'] = '';
	//$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);

  }
  
  function view(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
    
    $bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);

					if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
		$data['nama_bulan'] = $nama_bln;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

	$cek_data = $this->mmaster->cek_so($bulan, $tahun); 
	
	if ($cek_data['idnya'] == '' ) { 
		// jika data so blm ada, munculkan pesan
		$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." belum diinput..!";
		//$data['list_gudang'] = $this->mmaster->get_gudang();
		$data['isi'] = 'app-stok-opname-quilting/vmainform';
		$this->load->view('template',$data);
	}
	else { // jika sudah diapprove maka munculkan msg
		if ($cek_data['status_approve'] == 't') {
			$data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah di-approve..!";
			//$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname-quilting/vmainform';
			$this->load->view('template',$data);
		}
		else {
			// get data dari tabel tt_stok_opname_ yg statusnya 'f'
			$data['query'] = $this->mmaster->get_all_stok_opname($bulan, $tahun);
	
			$data['jum_total'] = count($data['query']);
			$data['isi'] = 'app-stok-opname-quilting/vformview';
			$this->load->view('template',$data);
			
		}
	} // end else

  }
  
  function submit() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
	  $bulan = $this->input->post('bulan', TRUE);
	  $tahun = $this->input->post('tahun', TRUE);
	  $id_header = $this->input->post('id_header', TRUE);  
	  $no = $this->input->post('no', TRUE);  
	  
	  // 01-11-2014
	  if ($bulan < 12) {
		$bulanplus1 = $bulan+1;
		$tahunplus1 = $tahun;
		if ($bulanplus1 < 10)
			$bulanplus1= '0'.$bulanplus1;
	  }
	  else {
		$bulanplus1 = '01';
		$tahunplus1 = $tahun+1;
	  }
	  
	  $tglawalplus = $tahunplus1."-".$bulanplus1."-01";
	  $tgl = date("Y-m-d H:i:s"); 
	  
	  if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
		// 04-11-2014, skrip baru ================================================================
	  for ($i=1;$i<=$no;$i++)
		  {
			 $kode_brg = $this->input->post('kode_'.$i, TRUE);
			 $harga = $this->input->post('harga_'.$i, TRUE);
			 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE);
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 
			 //-------------- hitung total qty dari detail tiap2 harga -------------------
				
				$qtytotalstokfisik = 0;
				for ($xx=0; $xx<count($harga); $xx++) {
					$harga[$xx] = trim($harga[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					$qtytotalstokfisik+= $stok_fisik[$xx];
				} // end for
			// ---------------------------------------------------------------------
				// 24-03-2014
				$totalxx = $qtytotalstokfisik;
			
			// ======== update stoknya! =============
			// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
			// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
			// ambil tgl terakhir di bln tsb
			/*$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			*/ 
			
			// 1.ambil brg masuk hasil pembelian quilting //
			$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a 
							INNER JOIN tm_sj_hasil_makloon_detail b ON a.id = b.id_sj_hasil_makloon
							WHERE a.tgl_sj >= '$tglawalplus'
							AND b.kode_brg_makloon = '$kode_brg'
							AND b.status_stok = 't' ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$jum_masuk = $hasilrow->jum_masuk;
				if ($jum_masuk == '')
					$jum_masuk = 0;
					
				$jum_masuk_konv = $jum_masuk;
			}
			else
				$jum_masuk_konv = 0;
			
			// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE  a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND b.is_quilting = 't' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
								
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							WHERE a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND b.is_quilting = 't' AND a.tujuan > '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
					
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(b.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							WHERE a.tgl_bonm  >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND b.is_quilting = 't' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;
				
				$totmasuk = $jum_masuk_konv + $jum_masuk_lain;
				$totkeluar = $jum_keluar+$jum_keluar_lain;
												
				$jum_stok_akhir = $totmasuk-$totkeluar;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------

				// tambahkan stok_akhir ini ke qty SO
				//$totalbagusxx = $qtytotalstokfisikbgs + $stok_akhir_bgs; //echo $stok_akhir_bgs." ";
				//$totalperbaikanxx = $qtytotalstokfisikperbaikan + $stok_akhir_perbaikan; //echo $stok_akhir_perbaikan." ";
				$totalxx = $totalxx+$jum_stok_akhir; //echo $totalxx." ";
			
				//cek stok terakhir tm_stok, dan update stoknya
				// 04-11-2014, stoknya ga ada konversi stok
				
				$totalxx_konvawal = $totalxx;
					
					$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode_brg' ");
					if ($query3->num_rows() == 0){
						/*$seqxx	= $this->db->query(" SELECT id FROM tm_stok ORDER BY id DESC LIMIT 1 ");
						if($seqxx->num_rows() > 0) {
							$seqrow	= $seqxx->row();
							$id_stok	= $seqrow->id+1;
						}else{
							$id_stok	= 1;
						} */
						
						$data_stok = array(
							//'id'=>$id_stok,
							'kode_brg'=>$kode_brg,
							'stok'=>$totalxx_konvawal,
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_hasil_makloon', $data_stok);
					}
					else {
						$hasilrow = $query3->row();
						$id_stok	= $hasilrow->id;
						
						$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$totalxx_konvawal', tgl_update_stok = '$tgl' 
						where kode_brg= '$kode_brg' ");
					} // done 04-11-2014
					
				// stok_harga
				// ----------------------------------------------
				for ($xx=0; $xx<count($harga); $xx++) {
					$harga[$xx] = trim($harga[$xx]);
					$stok_fisik[$xx] = trim($stok_fisik[$xx]);
					
					// 24-03-2014
					$totalxx = $stok_fisik[$xx];
						
					// ========================= stok per harga ===============================================
					
					// 11-04-2014, tambahkan query utk menghitung brg masuk/keluar dari SJ masuk/keluar di tanggal setelahnya
					// contoh: jika SO maret, maka hitung semua transaksi brg masuk/keluar bln setelahnya
			
				// 1. ambil brg masuk hasil pembelian quilting
				$query3	= $this->db->query(" SELECT sum(b.qty_makloon) as jum_masuk FROM tm_sj_hasil_makloon a 
							INNER JOIN tm_sj_hasil_makloon_detail b ON a.id = b.id_sj_hasil_makloon
							WHERE a.tgl_sj >= '$tglawalplus' AND b.kode_brg_makloon = '$kode_brg'
							AND b.status_stok = 't' 
							AND b.harga = '".$harga[$xx]."' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk = $hasilrow->jum_masuk;					

					$jum_masuk_konv = $jum_masuk;
				}
				else
					$jum_masuk_konv = 0;
			
				// 2. hitung brg keluar bagus dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a 
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE  a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND c.harga = '".$harga[$xx]."'
							AND b.is_quilting = 't' AND a.tujuan < '3' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar = 0;
			
				// 3. hitung brg keluar lain dari tm_bonmkeluar. 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_keluar FROM tm_bonmkeluar a
							INNER JOIN tm_bonmkeluar_detail b ON a.id = b.id_bonmkeluar
							INNER JOIN tm_bonmkeluar_detail_harga c ON b.id = c.id_bonmkeluar_detail
							WHERE a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND c.harga = '".$harga[$xx]."'
							AND b.is_quilting = 't' AND a.tujuan > '2' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_keluar_lain = $hasilrow->jum_keluar;
				}
				else
					$jum_keluar_lain = 0;
				
				// 4. hitung brg masuk lain dari tm_bonmmasuklain 30-10-2014, ini udh ambil qty sat konversi
				$query3	= $this->db->query(" SELECT sum(c.qty) as jum_masuk_lain FROM tm_bonmmasuklain a 
							INNER JOIN tm_bonmmasuklain_detail b ON a.id = b.id_bonmmasuklain
							INNER JOIN tm_bonmmasuklain_detail_harga c ON b.id = c.id_bonmmasuklain_detail
							WHERE a.tgl_bonm >= '$tglawalplus'
							AND b.kode_brg = '$kode_brg'
							AND c.harga = '".$harga[$xx]."'
							AND b.is_quilting = 't' ");
				if ($query3->num_rows() > 0){
					$hasilrow = $query3->row();
					$jum_masuk_lain = $hasilrow->jum_masuk_lain;
				}
				else
					$jum_masuk_lain = 0;
			
				$totmasuk = $jum_masuk_konv + $jum_masuk_lain;
				$totkeluar = $jum_keluar+$jum_keluar_lain;
												
				$jum_stok_akhir = $totmasuk-$totkeluar;
				$totalxx = $totalxx + $jum_stok_akhir;
			//------------------------------- END query cek brg masuk/keluar ---------------------------------------------
				// tambahkan stok_akhir_bgs dan perbaikan ini ke qty SO
				//$totalbagusxx = $stok_fisik_bgs[$xx] + $stok_akhir_bgs;
				//$totalperbaikanxx = $stok_fisik_perbaikan[$xx] + $stok_akhir_perbaikan;
				//$totalxx = $totalxx + $jum_stok_akhir;
					
					// 04-11-2014
					//cek stok terakhir tm_stok_harga, dan update stoknya.
					// stoknya ga ada konversi
				
					$totalxx_konvawal = $totalxx;
					
					$query3	= $this->db->query(" SELECT stok FROM tm_stok_harga WHERE harga = '".$harga[$xx]."'
							AND kode_brg_quilting='$kode_brg' ");
					if ($query3->num_rows() == 0){
						// jika blm ada data di tm_stok_unit_jahit_warna, insert
					/*	$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
						if($seq_stokwarna->num_rows() > 0) {
							$seq_stokwarnarow	= $seq_stokwarna->row();
							$id_stok_warna	= $seq_stokwarnarow->id+1;
						}else{
							$id_stok_warna	= 1;
						} */
						
						$data_stok = array(
							//'id'=>$id_stok_warna,
							'kode_brg_quilting'=>$kode_brg,
							'harga'=>$harga[$xx],
							'stok'=>$totalxx_konvawal,
							'quilting'=>'t',
							'tgl_update_stok'=>$tgl
							);
						$this->db->insert('tm_stok_harga', $data_stok);
					}
					else {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '".$totalxx_konvawal."', 
						tgl_update_stok = '$tgl' 
						where harga= '".$harga[$xx]."' AND kode_brg_quilting='$kode_brg' ");
					}
				} // end for
				// ----------------------------------------------
			 // ====================================================================================
			 
			 $this->db->query(" UPDATE tt_stok_opname_hasil_quilting_detail SET status_approve = 't'
								where id = '$idnya' ");

		  } // end for
		  $this->db->query(" UPDATE tt_stok_opname_hasil_quilting SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		    $data['msg'] = "Data stok opname untuk bulan ".$nama_bln." ".$tahun." sudah berhasil di-approve, dan otomatis mengupdate stok";
			//$data['list_gudang'] = $this->mmaster->get_gudang();
			$data['isi'] = 'app-stok-opname-quilting/vmainform';
			$this->load->view('template',$data); // done untested
	  // ============================================= END 04-11-2014 ==========================================
		
	  
	/*  $ada = 0;
	  // cek di tiap2 bahan bakunya, apakah ada data brg yg blm ada harganya. jika blm ada harga, maka munculkan pesan error
	  // revisi 140212: utk data bhn baku yg blm ada stok harga, harganya diset 0 aja dulu
	  for ($i=1;$i<=$no;$i++) {
		  $kode_brg = $this->input->post('kode_'.$i, TRUE);
		  $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
		  
		  $query3	= $this->db->query(" SELECT harga FROM tm_stok_harga WHERE kode_brg_quilting = '$kode_brg' ");
			if ($query3->num_rows() == 0){
				//$ada = 0;
				//break;
				//14-02-2012
				// save di tm_stok_hasil_makloon
				$data_detail = array(
						'kode_brg'=>$kode_brg, 
						'stok'=>$jum_stok,
						'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok_hasil_makloon',$data_detail);
				
				// save di tm_stok_harga
				$data_detail = array(
						'kode_brg_quilting'=>$kode_brg, 
						'stok'=>$jum_stok,
						'harga'=>'0',
						'quilting'=>'t',
						'tgl_update_stok'=>$tgl
					);
				$this->db->insert('tm_stok_harga',$data_detail);
			}
			else {
				$ada = 1;
			}
	  }
	  		  	      
	      for ($i=1;$i<=$no;$i++)
		  {
			 // cek ada selisih ga.
			 $kode_brg = $this->input->post('kode_'.$i, TRUE);
			 $stok_fisik = $this->input->post('stok_fisik_'.$i, TRUE);
			 $jum_stok = $this->input->post('jum_stok_'.$i, TRUE);
			 $idnya = $this->input->post('id_'.$i, TRUE);
			 
			 //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 18-02-2012 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			 // update ke tm_stok_hasil_makloon dan ke tm_stok_harga
			 //cek apakah data stoknya ada
				$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_makloon WHERE kode_brg = '$kode_brg' ");
				if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_makloon, insert
					$stok_lama = 0;
				
					$data_stok = array(
						'kode_brg'=>$kode_brg,
						'stok'=>$stok_fisik,
						'tgl_update_stok'=>$tgl
					);
					$this->db->insert('tm_stok_hasil_makloon',$data_stok);
				}
				else {
					$hasilrow = $query3->row();
					$stok_lama	= $hasilrow->stok;
					
					//update ke tm_stok_hasil_makloon dan history stoknya, 
					$this->db->query(" UPDATE tm_stok_hasil_makloon SET stok = '$stok_fisik', tgl_update_stok = '$tgl' 
					where kode_brg= '$kode_brg' ");
				}
				
				// selisih reverse di bhn quilting itu ga ada konversi
				$selisih_reverse = $stok_fisik-$stok_lama; // selisih = 11-12
				
				// stok harga
				// 08-12-2012, cek dulu ada brp data stok harga yg tidak 0. trus query perulangan data2 stok harga yg ada berdasarkan kode brg tsb. 
				
				$query2x	= $this->db->query(" SELECT count(id) as jum FROM tm_stok_harga WHERE kode_brg_quilting = '$kode_brg' 
									AND stok > 0 AND quilting='t' ");
				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->row();
					$jmldatastokharga = $hasil2x->jum;
				}
				else
					$jmldatastokharga = 0; // 1
								
				$query2x	= $this->db->query(" SELECT sum(stok) as jumstok FROM tm_stok_harga WHERE kode_brg_quilting = '$kode_brg' 
									AND stok > 0 AND quilting='t' ");
				if ($query2x->num_rows() > 0){
					$hasil2x = $query2x->row();
					$jmlstokharga = $hasil2x->jumstok;
				}
				else
					$jmlstokharga = 0; // 1997.65, fisik = 0. 0-1997.65
					
				$pengurangan = $stok_fisik-$jmlstokharga; 
				//if ($kode_brg == 'QST0013') echo "pengurangan=".$pengurangan."<br>";
				
				if ($jmldatastokharga != 0 && $pengurangan !=0) {
					$bagirata = $stok_fisik/$jmldatastokharga;
					$statupdate=1;
				}
				else {
					$bagirata = 0;
					$statupdate=0;
				}
								
				$query2x	= $this->db->query(" SELECT id, stok, harga FROM tm_stok_harga WHERE kode_brg_quilting = '$kode_brg' 
									AND stok > 0 AND quilting = 't' ORDER BY id ASC ");
				$hasil2x = $query2x->result();
				
				foreach ($hasil2x as $row2x) {
					$stoknya = $row2x->stok;
					$harganya = $row2x->harga;
					$id_stok_harga = $row2x->id;
					
					if ($statupdate == 0) {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$stoknya', tgl_update_stok = '$tgl' 
										where id= '$id_stok_harga' ");
						
						// save ke tt_so_bahan_baku_detail_harga
						// 06-12-12
						//cek apakah data stoknya ada
						$query3x	= $this->db->query(" SELECT id FROM tt_so_hasil_quilting_detail_harga WHERE 
										id_stok_opname_hasil_quilting_detail = '$idnya' AND harga = '$harganya' ");
						if ($query3x->num_rows() == 0){
							$data_stok = array(
								'id_stok_opname_hasil_quilting_detail'=>$idnya,
								'harga'=>$harganya,
								'stok'=>$stoknya
							);
							$this->db->insert('tt_so_hasil_quilting_detail_harga',$data_stok);
						}
						else {
							$hasil3x = $query3x->row();
							$idsodetailharga = $hasil3x->id; 
							$this->db->query(" UPDATE tt_so_hasil_quilting_detail_harga SET stok = '$stoknya'
											where id= '$idsodetailharga' ");
						}
					}
					else {
						$this->db->query(" UPDATE tm_stok_harga SET stok = '$bagirata', tgl_update_stok = '$tgl' 
										where id= '$id_stok_harga' ");
						
						// save ke tt_so_bahan_baku_detail_harga
						// 06-12-12
						//cek apakah data stoknya ada
						$query3x	= $this->db->query(" SELECT id FROM tt_so_hasil_quilting_detail_harga WHERE 
										id_stok_opname_hasil_quilting_detail = '$idnya' AND harga = '$harganya' ");
						if ($query3x->num_rows() == 0){
							$data_stok = array(
								'id_stok_opname_hasil_quilting_detail'=>$idnya,
								'harga'=>$harganya,
								'stok'=>$bagirata
							);
							$this->db->insert('tt_so_hasil_quilting_detail_harga',$data_stok);
						}
						else {
							$hasil3x = $query3x->row();
							$idsodetailharga = $hasil3x->id; 
							$this->db->query(" UPDATE tt_so_hasil_quilting_detail_harga SET stok = '$bagirata'
											where id= '$idsodetailharga' ");
						}
					}
				}
				
				//dan update statusnya di tt_stok_opname_hasil_quilting_detail
				$this->db->query(" UPDATE tt_stok_opname_hasil_quilting_detail SET status_approve = 't'
								where id = '$idnya' ");
			 //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end 18-02-2012 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
			
		  } // end for
		  
		  $this->db->query(" UPDATE tt_stok_opname_hasil_quilting SET tgl_update = '$tgl',
								status_approve = 't' where id = '$id_header' ");
		  
		  redirect('app-stok-opname-quilting/cform'); */
  }

}
