<?php
class master_jenis_bb extends CI_Controller
{
    public $data = array(
        'halaman' => 'master_jenis_bb',        
        'title' => 'Master jenis BB',
        'isi' => 'master_jenis_bb/master_jenis_bb_form'
    );

	// Perlu mendefisikan ulang, karena lokasi model tidak standar
	// yaitu di bawah folder "user" -> model/user
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_jenis_bb/master_jenis_bb_model', 'master_jenis_bb');
    }

   
    public function index()
    {
		$offset=$this->uri->segment(4,TRUE);
        $this->data['values'] = (object) $this->master_jenis_bb->default_values;
        $this->data['list_data_wip']=$this->master_jenis_bb->master_jenis_bb($offset);
		$this->load->view('template', $this->data);
			
    }
     public function sukses_input()
    {	
		
        $this->data['isi'] = 'master_jenis_bb/master_jenis_bb-sukses';
        $this->load->view('template', $this->data);
    }

    // Jika pendaftaran error, tampilkan informasi mengenai error.
    public function error_input()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Penginputan Master jenis BB Error';
        $this->load->view('template', $this->data);
    }
   public function submit()
    {
		
		$no=$this->input->post('no',TRUE);
        
       
		 $jumlah_input=$no-1;
		// print_r($jumlah_input);
		 for($i=1; $i<=$jumlah_input; $i++){
		$this->master_jenis_bb->input(	
		$this->input->post('kode_jenis_bb_m_'.$i,TRUE),
		$this->input->post('nama_jenis_bb_'.$i,TRUE)
		);
		
		}
		if(true)
		redirect('master_jenis_bb/master_jenis_bb/sukses_input');
		else 
		redirect('master_jenis_bb/master_jenis_bb/error_input');
    }
    
    
   public function view($offset= null)
    {	
		
		$page = $this->uri->segment(5);
		
		$per_page=10;
		
		if (empty($page)) {
		$offset = 0;
		} else {
		$offset = ($page * $per_page - $per_page);
		}
		
       $master_jenis_bb = $this->master_jenis_bb->master_jenis_bb($offset);
        if ($master_jenis_bb) {
            $this->data['master_jenis_bb'] = $master_jenis_bb;
            $this->data['paging'] = $this->master_jenis_bb->paging('biasa', site_url('master_jenis_bb/master_jenis_bb/halaman/'), 4);
        } else {
            $this->data['master_jenis_bb'] = 'Tidak ada data Master jenis BB, Silahkan Melakukan '.anchor('/master_jenis_bb/master_jenis_bb/', 'Proses penginputan.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('master_jenis_bb/master_jenis_bb/cari');
        $this->data['isi'] = 'master_jenis_bb/master_jenis_bb_list';
        $this->load->view('template', $this->data);
    }
    public function cari($offset = 0)
    {
        $master_jenis_bb = $this->master_jenis_bb->cari($offset);
        if ($master_jenis_bb) {
            $this->data['master_jenis_bb'] = $master_jenis_bb;
            $this->data['paging'] = $this->master_jenis_bb->paging('pencarian', site_url('/master_jenis_bb/master_jenis_bb/cari/'), 4);
        } else {
            $this->data['master_jenis_bb'] = 'Data tidak ditemukan.'. anchor('/master_jenis_bb/master_jenis_bb/view', ' Tampilkan semua Master jenis BB.', 'class="alert-link"');
        }
        $this->data['form_action'] = site_url('/master_jenis_bb/master_jenis_bb/cari');
        $this->data['isi'] = 'master_jenis_bb/master_jenis_bb_list';
        $this->load->view('template', $this->data);
    }
    
   
    
     public function hapus($id)
    {
        
        if ($this->session->userdata('user_bagian') != '2') {
            $this->session->set_flashdata('pesan_error', 'Anda tidak berhak menghapus data Master jenis BB. Kembali ke halaman ' . anchor('master_jenis_bb/master_jenis_bb', 'master_jenis_bb.', 'class="alert-link"'));
            redirect('master_jenis_bb/master_jenis_bb/error');
        }

      
        if (! $this->master_jenis_bb->get($id)) {
            $this->session->set_flashdata('pesan_error', 'Data Master jenis BB tidak ada. Kembali ke halaman ' . anchor('master_jenis_bb/master_jenis_bb', 'master_jenis_bb.', 'class="alert-link"'));
            redirect('master_jenis_bb/master_jenis_bb/error');
        }

        // Hapus
        if ($this->master_jenis_bb->delete($id)) {
            $this->session->set_flashdata('pesan', 'Data berhasil dihapus. Kembali ke halaman '. anchor('master_jenis_bb/master_jenis_bb/view', 'View Master jenis BB.', 'class="alert-link"'));
            redirect('master_jenis_bb/master_jenis_bb/sukses');
        } else {
            $this->session->set_flashdata('pesan_error', 'Data gagal dihapus. Kembali ke halaman '. anchor('master_jenis_bb/master_jenis_bb/view', 'View Master jenis BB.', 'class="alert-link"'));
            redirect('master_jenis_bb/master_jenis_bb/error');
        }
    }
     public function sukses()
    {
        $this->data['isi'] = 'sukses';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }

    public function error()
    {
        $this->data['isi'] = 'error';
        $this->data['title'] = 'Data pengguna';
        $this->load->view('template', $this->data);
    }
    
    public function edit()
    {
		$id = $this->input->post('id',TRUE);
		if ($id=='')
		$id = $this->uri->segment(4);
        $master_jenis_bb = $this->master_jenis_bb->get($id);
        if (! $master_jenis_bb) {
            $this->session->set_flashdata('pesan_error', 'Data Master jenis BB tidak ada. Kembali ke halaman ' . anchor('master_jenis_bb/master-jenis/view', 'Master jenis BB.', 'class="alert-link"'));
            redirect('master_jenis_bb/master-jenis/error');
        }

        // Data untuk form.
        if (!$_POST) {
            $data = (object) $master_jenis_bb;     
        } else {
            $data = (object) $this->input->post(null, true);
        }
        $this->data['values'] = $data;
        
		$this->data['isi'] = 'master_jenis_bb/master_jenis_bb_form_edit';
        $this->load->view('template', $this->data);
	 }
	    public function updatedata(){
			$id =  $this->input->post('id',TRUE);
	        $nama_jenis_bb =  $this->input->post('nama_jenis_bb',TRUE);
			$kode_jenis_bb =  $this->input->post('kode_jenis_bb',TRUE);
	    
	     if (! $this->master_jenis_bb->validate('form_rules')) {
            $this->data['isi'] = 'master_jenis_bb/master_jenis_bb_form_edit';
            $this->data['form_action'] = site_url('master_jenis_bb/master_jenis_bb/view');
            return;     
		}
    	 
		if ($this->master_jenis_bb->edit($id,$nama_jenis_bb,$kode_jenis_bb)) {
            $this->session->set_flashdata('pesan', 'Data berhasil diupdate. Kembali ke halaman ' . anchor('master_jenis_bb/master_jenis_bb/view', 'Master jenis BB.', 'class="alert-link"'));
            redirect('master_jenis_bb/master_jenis_bb/sukses');
        } else {
            $this->session->set_flashdata('pesan_error', 'Data tidak berhasil diupdate. Kembali ke halaman ' . anchor('master_jenis_bb/master_jenis_bb/view', 'Master jenis BB.', 'class="alert-link"'));
            redirect('/master_jenis_bb/master_jenis_bb/error');
	}
}
     
}

