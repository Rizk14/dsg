<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('expopiutangfaktur/mclass');
	}
	
	function index() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}	
			$data['page_title_piutang']			= $this->lang->line('page_title_piutang');
			$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
			$data['list_piutang_no_faktur']	= $this->lang->line('list_piutang_no_faktur');
			$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
			$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
			$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
			$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
			$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
			$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
			$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
			$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
			$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
			$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
			$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
			$data['list_piutang_nilai_faktur'] = $this->lang->line('list_piutang_nilai_faktur');
			$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
			$data['list_faktur_nota_sederhana'] = $this->lang->line('list_faktur_nota_sederhana');
			
			$data['button_laporan']	= $this->lang->line('button_laporan');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['list']		= "";
			$data['limages']	= base_url();
			$data['isi'] = 'expopiutangfaktur/vmainform';
			$this->load->view('template',$data);
			
		}	
	
	
	function carilistfaktur() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_piutang']			= $this->lang->line('page_title_piutang');
		$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
		$data['list_piutang_no_faktur']	= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
		$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
		$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
		$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_nilai_faktur'] = $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_faktur_nota_sederhana'] = $this->lang->line('list_faktur_nota_sederhana');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_pelunasan_no_voucher'] = $this->lang->line('list_pelunasan_no_voucher');
		$data['list_voucher_piutang'] = $this->lang->line('list_voucher_piutang');
		$data['list_piutang_total_faktur'] = $this->lang->line('list_piutang_total_faktur');
		$data['list_faktur_tgl_faktur'] = $this->lang->line('list_faktur_tgl_faktur');
		
		
		
		$data['button_laporan']	= $this->lang->line('button_laporan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpiutang']	= "";
		$data['limages']	= base_url();
		
		$no_faktur	= $this->input->post('no_faktur');
		$i_faktur	= $this->input->post('i_faktur');
		$dfakturfirst= $this->input->post('d_faktur_first');
		$dfakturlast	= $this->input->post('d_faktur_last');
		$f_nota_sederhana	= $this->input->post('tf_nota_sederhana');
		
		$data['tglfakturmulai']	= (!empty($dfakturfirst) && $dfakturfirst!='0')?$dfakturfirst:'';
		$data['tglfakturakhir']	= (!empty($dfakturlast) && $dfakturlast!='0')?$dfakturlast:'';
		$data['nofaktur']		= (!empty($no_faktur) && $no_faktur!='0')?$no_faktur:'';
		$data['ifaktur']			= (!empty($i_faktur) && $i_faktur!='0')?$i_faktur:'';
		$data['f_nota_sederhana']	= $f_nota_sederhana;
		
		$data['checked'] = $f_nota_sederhana=='t'?'checked':'';
		
		$e_d_faktur_first= explode("/",$dfakturfirst,strlen($dfakturfirst));
		$e_d_faktur_last	= explode("/",$dfakturlast,strlen($dfakturlast));
		
		$ndfakturfirst	= !empty($dfakturfirst)?$e_d_faktur_first[2].'-'.$e_d_faktur_first[1].'-'.$e_d_faktur_first[0]:'0';
		$ndfakturlast	= !empty($dfakturlast)?$e_d_faktur_last[2].'-'.$e_d_faktur_last[1].'-'.$e_d_faktur_last[0]:'0';
		
		$turi1	= ($no_faktur!='' && $no_faktur!='0')?$no_faktur:'0';
		$turi2	= ($i_faktur!='' && $i_faktur!='0')?$i_faktur:'0';
		$turi3	= $ndfakturfirst;
		$turi4	= $ndfakturlast;
		$turi5	= $f_nota_sederhana;
		
		$data['turi1'] = $turi1;
		$data['turi2'] = $turi2;
		$data['turi3'] = $turi3;
		$data['turi4'] = $turi4;
		$data['turi5'] = $turi5;
		
		/*** $this->load->model('expopiutangfaktur/mclass'); ***/
		
		$pagination['base_url'] 	= 'expopiutangfaktur/cform/carilistfakturnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if($dfakturfirst!='' && $dfakturlast!='') {
			$qlistfakturallpage	= $this->mclass->clistfakturallpage1($i_faktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana);
			$data['template'] = 1;
		}else{
			$qlistfakturallpage	= $this->mclass->clistfakturallpage2($i_faktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana);
			$data['template'] = 2;
		}
		
		$pagination['total_rows']	= $qlistfakturallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dfakturfirst!='' && $dfakturlast!='') {
			$data['query']	= $this->mclass->clistfaktur1($pagination['per_page'],$pagination['cur_page'],$i_faktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana);
		}else{
			$data['query']	= $this->mclass->clistfaktur2($pagination['per_page'],$pagination['cur_page'],$i_faktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana);
		}

		$jmltotalfaktur = 0;
		$qvtotalfaktur	= $this->mclass->vtotalfaktur($pagination['per_page'],$pagination['cur_page'],$i_faktur,$ndfakturfirst,$ndfakturlast,$f_nota_sederhana);

		if(sizeof($qvtotalfaktur)>0) {
			foreach($qvtotalfaktur as $vtotalfaktur) {
				$jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
			}
			$data['totalfaktur2'] = $jmltotalfaktur;
		}else{
			$data['totalfaktur2'] = array();
		}
		$data['isi'] = 'expopiutangfaktur/vlistform';
			$this->load->view('template',$data);
		
	}

	function carilistfakturnext() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_piutang']			= $this->lang->line('page_title_piutang');
		$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
		$data['list_piutang_no_faktur']	= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_piutang_total_faktur'] = $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
		$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
		$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
		$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_nilai_faktur'] = $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_faktur_nota_sederhana'] = $this->lang->line('list_faktur_nota_sederhana');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_pelunasan_no_voucher'] = $this->lang->line('list_pelunasan_no_voucher');
		$data['list_voucher_piutang'] = $this->lang->line('list_voucher_piutang');
		$data['list_piutang_total_faktur'] = $this->lang->line('list_piutang_total_faktur');
		$data['list_faktur_tgl_faktur'] = $this->lang->line('list_faktur_tgl_faktur');
		
		$data['button_laporan']	= $this->lang->line('button_laporan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpiutang']	= "";		
		$data['limages']	= base_url();

		$no_faktur	= $this->uri->segment(4);
		$i_faktur	= $this->uri->segment(5);
		$dfakturfirst	= $this->uri->segment(6);
		$dfakturlast	= $this->uri->segment(7);
		$f_nota_sederhana	= $this->uri->segment(8);
		
		$data['turi1'] = $no_faktur;
		$data['turi2'] = $i_faktur;
		$data['turi3'] = $dfakturfirst;
		$data['turi4'] = $dfakturlast;
		$data['turi5'] = $f_nota_sederhana;
		
		$e_d_faktur_first = ($dfakturfirst!='0')?explode("-",$dfakturfirst,strlen($dfakturfirst)):'';
		$e_d_faktur_last	 = ($dfakturlast!='0')?explode("-",$dfakturlast,strlen($dfakturlast)):'';

		$ndfakturfirst	= $dfakturfirst!='0'?$e_d_faktur_first[2].'/'.$e_d_faktur_first[1].'/'.$e_d_faktur_first[0]:'0';
		$ndfakturlast	= $dfakturlast!='0'?$e_d_faktur_last[2].'/'.$e_d_faktur_last[1].'/'.$e_d_faktur_last[0]:'0';
						
		$data['tglfakturmulai']	= (!empty($ndfakturfirst) && $ndfakturfirst!='0')?$ndfakturfirst:'';
		$data['tglfakturakhir']	= (!empty($ndfakturlast) && $ndfakturlast!='0')?$ndfakturlast:'';
		$data['nofaktur']	= (!empty($no_faktur) && $no_faktur!='0')?$no_faktur:'';
		$data['ifaktur']	= (!empty($i_faktur) && $i_faktur!='0')?$i_faktur:'';
		$data['f_nota_sederhana']	= $f_nota_sederhana;
		
		$data['checked'] = $f_nota_sederhana=='t'?'checked':'';
		
		$turi1	= ($no_faktur!='' && $no_faktur!='0')?$no_faktur:'0';
		$turi2	= ($i_faktur!='' && $i_faktur!='0')?$i_faktur:'0';
		$turi3	= $dfakturfirst;
		$turi4	= $dfakturlast;
		$turi5	= $f_nota_sederhana;
		
		/*** $this->load->model('expopiutangfaktur/mclass'); ***/
		
		$pagination['base_url'] 	= 'expopiutangfaktur/cform/carilistfakturnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if($dfakturfirst!='0' && $dfakturlast!='0') {
			$qlistfakturallpage	= $this->mclass->clistfakturallpage1($i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
		}else{
			$qlistfakturallpage	= $this->mclass->clistfakturallpage2($i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
		}
		
		$pagination['total_rows']	= $qlistfakturallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dfakturfirst!='0' && $dfakturlast!='0') {
			$data['isi']	= $this->mclass->clistfaktur1($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
			$data['template'] = 1;
		}else{
			$data['isi']	= $this->mclass->clistfaktur2($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
			$data['template'] = 2;
		}

		$jmltotalfaktur = 0;
		$qvtotalfaktur	= $this->mclass->vtotalfaktur($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
		foreach($qvtotalfaktur as $vtotalfaktur) {
			$jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
		}
		$data['totalfaktur2'] = $jmltotalfaktur;
		
		$this->load->view('expopiutangfaktur/vlistform',$data);
	}

	function gexportpiutang() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	 $db2 = $this->load->database('db_external', TRUE);
		$data['page_title_piutang']			= $this->lang->line('page_title_piutang');
		$data['form_title_detail_piutang']	= $this->lang->line('form_title_detail_piutang');
		$data['list_piutang_no_faktur']	= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_piutang_total_faktur'] = $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_piutang']		= $this->lang->line('list_piutang_piutang');
		$data['list_piutang_total_piutang']	= $this->lang->line('list_piutang_total_piutang');
		$data['list_piutang_no_faktur']		= $this->lang->line('list_piutang_no_faktur');
		$data['list_piutang_tgl_faktur']	= $this->lang->line('list_piutang_tgl_faktur');
		$data['list_piutang_due_date']		= $this->lang->line('list_piutang_due_date');
		$data['list_piutang_pelanggan']		= $this->lang->line('list_piutang_pelanggan');
		$data['list_piutang_nilai_faktur']	= $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_total_faktur']	= $this->lang->line('list_piutang_total_faktur');
		$data['list_piutang_nilai_faktur'] = $this->lang->line('list_piutang_nilai_faktur');
		$data['list_piutang_nilai_piutang']	= $this->lang->line('list_piutang_nilai_piutang');
		$data['list_faktur_nota_sederhana'] = $this->lang->line('list_faktur_nota_sederhana');
		
		$data['button_laporan']	= $this->lang->line('button_laporan');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lpiutang']	= "";		
		$data['limages']	= base_url();
		
		$no_faktur	= $this->uri->segment(4);
		$i_faktur	= $this->uri->segment(5);
		$dfakturfirst	= $this->uri->segment(6);
		$dfakturlast	= $this->uri->segment(7);
		$f_nota_sederhana	= $this->uri->segment(8);
		
		$e_d_faktur_first = ($dfakturfirst!='0')?explode("-",$dfakturfirst,strlen($dfakturfirst)):'';
		$e_d_faktur_last	 = ($dfakturlast!='0')?explode("-",$dfakturlast,strlen($dfakturlast)):'';
		
		$ndfakturfirst	= $dfakturfirst!='0'?$e_d_faktur_first[2].'/'.$e_d_faktur_first[1].'/'.$e_d_faktur_first[0]:'0';
		$ndfakturlast	= $dfakturlast!='0'?$e_d_faktur_last[2].'/'.$e_d_faktur_last[1].'/'.$e_d_faktur_last[0]:'0';
		
		$periode	= $ndfakturfirst." s.d ".$ndfakturlast;
		
		$data['tglfakturmulai']	= (!empty($ndfakturfirst) && $ndfakturfirst!='0')?$ndfakturfirst:'';
		$data['tglfakturakhir']	= (!empty($ndfakturlast) && $ndfakturlast!='0')?$ndfakturlast:'';
		$data['nofaktur']	= (!empty($no_faktur) && $no_faktur!='0')?$no_faktur:'';
		$data['ifaktur']	= (!empty($i_faktur) && $i_faktur!='0')?$i_faktur:'';
		$data['f_nota_sederhana']	= $f_nota_sederhana;
		
		$turi1	= ($no_faktur!='' && $no_faktur!='0')?$no_faktur:'0';
		$turi2	= ($i_faktur!='' && $i_faktur!='0')?$i_faktur:'0';
		$turi3	= $dfakturfirst;
		$turi4	= $dfakturlast;
		$turi5	= $f_nota_sederhana;
		
		/*** $this->load->model('expopiutangfaktur/mclass'); ***/
		
		$pagination['base_url'] 	= '/expopiutangfaktur/cform/carilistfakturnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if($dfakturfirst!='0' && $dfakturlast!='0') {
			$qlistfakturallpage	= $this->mclass->clistfakturallpage1($i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
		}else{
			$qlistfakturallpage	= $this->mclass->clistfakturallpage2($i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
		}
		
		$pagination['total_rows']	= $qlistfakturallpage->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if($dfakturfirst!='0' && $dfakturlast!='0') {
			$data['isi']	= $this->mclass->clistfaktur1($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
			$data['template'] = 1;
		}else{
			$data['isi']	= $this->mclass->clistfaktur2($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
			$data['template'] = 2;
		}

		$jmltotalfaktur = 0;
		$qvtotalfaktur	= $this->mclass->vtotalfaktur($pagination['per_page'],$pagination['cur_page'],$i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
		foreach($qvtotalfaktur as $vtotalfaktur) {
			$jmltotalfaktur	= $jmltotalfaktur+($vtotalfaktur->v_total_fppn);
		}
		$data['totalfaktur2'] = $jmltotalfaktur;
		
		$ObjPHPExcel = new PHPExcel();
		
		$qexppiutang	= $this->mclass->explistpiutang($i_faktur,$dfakturfirst,$dfakturlast,$f_nota_sederhana);
		
		if($qexppiutang->num_rows()>0) {
			
			$jnspenjualan = $f_nota_sederhana=='f'?'PENJUALAN BERDASAR DO':'PENJUALAN NON DO';
			
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Piutang")
				->setSubject("Laporan Piutang")
				->setDescription("Laporan Piutang")
				->setKeywords("Laporan Per-Tanggal Pencarian")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);			
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(14);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:M2');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN PIUTANG DAGANG');
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:M3');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:M4');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A4'
			);		
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', $jnspenjualan);
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A5:M5');
			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A5'
			);		
			$ObjPHPExcel->getActiveSheet()->setCellValue('A5', 'CV. DUTA SETIA GARMEN');
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A7', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),													
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('B7', 'No Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('C7', 'Tgl. Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('D7', 'Nilai Kontra Bon');
			$ObjPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('E7', 'No Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F7', 'Tgl. Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('G7', 'Pelanggan');
			$ObjPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('H7', 'Bruto');
			$ObjPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I7', 'Diskon');
			$ObjPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
								
			$ObjPHPExcel->getActiveSheet()->setCellValue('J7', 'DPP');
			$ObjPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('K7', 'PPN');
			$ObjPHPExcel->getActiveSheet()->getStyle('K7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('L7', 'Nilai Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('L7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

			$ObjPHPExcel->getActiveSheet()->setCellValue('M7', 'Piutang');
			$ObjPHPExcel->getActiveSheet()->getStyle('M7')->applyFromArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic'=> false,
							'size'  => 10
						),					
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
												
			if($qexppiutang->num_rows()>0) {
				
				$j	= 8;
				$nomor	= 1;
				$jml=1;
				
				$bruto	= array();
				$dpp	= array();
				$ppn	= array();
				$total	= array();

				$idtcode = '';
				$sisapiutang = '';
				
				$totalfaktur = 0;
				$totaldpp = 0;
				$totalppn = 0;
				$totalfaktur = 0;
				$totalpiutang = 0;
				$totalbruto = 0;
				$totaldiskon = 0;
									
				foreach($qexppiutang->result() as $row) {
					
					if($f_nota_sederhana=='f') {
						$qnilaifaktur	= $db2->query(" SELECT sum(n_quantity*v_unit_price) AS nilaifaktur FROM tm_faktur_do_item_t WHERE i_faktur='$row->i_nota' ");
					}else{
						$qnilaifaktur	= $db2->query(" SELECT sum(n_quantity*v_unit_price) AS nilaifaktur FROM tm_faktur_item WHERE i_faktur='$row->i_nota' ");
					}
					
					if($qnilaifaktur->num_rows()>0) {
						$rnilaifaktur	= $qnilaifaktur->row();
						
						if($f_nota_sederhana=='f') {
							$qnilaidiskon	= $db2->query(" SELECT v_discount AS nilaidiscon FROM tm_faktur_do_t WHERE i_faktur='$row->i_nota' ");
						}else{
							$qnilaidiskon	= $db2->query(" SELECT v_discount AS nilaidiscon FROM tm_faktur WHERE i_faktur='$row->i_nota' ");
						}
						
						$rnilaidiskon	= $qnilaidiskon->row();
						
						$qpiutang	= $db2->query(" SELECT i_dt, i_dt_code, v_total_grand, v_grand_sisa FROM tm_dt WHERE i_dt='$row->i_dt' AND f_nota_sederhana='$f_nota_sederhana' ");
						$rpiutang	= $qpiutang->row();
						
						$bruto[$jml]	= $rnilaifaktur->nilaifaktur;
						$dpp[$jml]		= ($bruto[$jml]-($rnilaidiskon->nilaidiscon));
						$ppn[$jml]		= round(($dpp[$jml]*10)/100);
						$total[$jml]	= round($dpp[$jml]+$ppn[$jml]);
						
						if($idtcode=='') {
							$idtcode = $row->i_dt_code;
						}
						
						//if($idtcode!=$row->i_dt_code) {
							//$sisapiutang=='';
						//}
						
						if($idtcode==$row->i_dt_code) {
							
							if($sisapiutang=='')
								$sisapiutang = $rpiutang->v_grand_sisa;

							/* pengurangnya nilai bruto
							if($sisapiutang > $bruto[$jml]) {
								$piutang = $bruto[$jml];
								$sisapiutang = $sisapiutang - $bruto[$jml];
							}elseif($sisapiutang==$bruto[$jml]) {
								$piutang = $bruto[$jml];
								$sisapiutang = 0;
							}elseif(($sisapiutang < $bruto[$jml]) && ($sisapiutang >0) ) {
								$piutang = $sisapiutang;
								$sisapiutang = 0;
							}else{
								$piutang = 0;
								$sisapiutang = 0;
							}
							*/
							if($sisapiutang > $total[$jml]) {
								$piutang = $total[$jml];
								$sisapiutang = $sisapiutang - $total[$jml];
							}elseif($sisapiutang==$total[$jml]) {
								$piutang = $total[$jml];
								$sisapiutang = 0;
							}elseif(($sisapiutang < $total[$jml]) && ($sisapiutang >0) ) {
								$piutang = $sisapiutang;
								$sisapiutang = 0;
							}else{
								$piutang = 0;
								$sisapiutang = 0;
							}							
							$idtcode = $row->i_dt_code;
							
						} else if($idtcode!=$row->i_dt_code){
							
							$sisapiutang = '';
							
							if($sisapiutang=='')
								$sisapiutang = $rpiutang->v_grand_sisa;
							/*
							if($sisapiutang > $bruto[$jml]) {
								$piutang = $bruto[$jml];
								$sisapiutang = $sisapiutang - $bruto[$jml];
							}elseif($sisapiutang==$bruto[$jml]) {
								$piutang = $bruto[$jml];
								$sisapiutang = 0;
							}elseif(($sisapiutang < $bruto[$jml]) && ($sisapiutang >0) ) {
								$piutang = $sisapiutang;
								$sisapiutang = 0;
							}else{
								$piutang = 0;
								$sisapiutang = 0;
							}
							*/
							if($sisapiutang > $total[$jml]) {
								$piutang = $total[$jml];
								$sisapiutang = $sisapiutang - $total[$jml];
							}elseif($sisapiutang==$total[$jml]) {
								$piutang = $total[$jml];
								$sisapiutang = 0;
							}elseif(($sisapiutang < $total[$jml]) && ($sisapiutang >0) ) {
								$piutang = $sisapiutang;
								$sisapiutang = 0;
							}else{
								$piutang = 0;
								$sisapiutang = 0;
							}
														
							$idtcode = $row->i_dt_code;							
						}
								
					}
					
					$row->d_dt	= explode("-",$row->d_dt,strlen($row->d_dt)); // Y-m-d
					$row->d_nota= explode("-",$row->d_nota,strlen($row->d_nota)); // Y-m-d
					
					$tgldt		= $row->d_dt[2]."/".$row->d_dt[1]."/".$row->d_dt[0];
					$tgld_nota	= $row->d_nota[2]."/".$row->d_nota[1]."/".$row->d_nota[0];

					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $nomor);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_dt_code);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $tgldt);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					//$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $bruto[$jml]);
					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $total[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->i_nota_code);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $tgld_nota);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->e_customer_name);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $bruto[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $rnilaidiskon->nilaidiscon);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
												
					$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $dpp[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, $ppn[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					//$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $bruto[$jml]);
					$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$j, $total[$jml]);
					$ObjPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

					//$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, " Piutang ");
					$ObjPHPExcel->getActiveSheet()->setCellValue('M'.$j, $piutang);
					$ObjPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 9
								),							
								'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					
					$totalbruto = $totalbruto+$bruto[$jml];
					$totaldiskon = $totaldiskon+$rnilaidiskon->nilaidiscon;
					//$totalfaktur = $totalfaktur+$bruto[$jml];
					$totalfaktur = $totalfaktur+$total[$jml];
					$totaldpp = $totaldpp+$dpp[$jml];
					$totalppn = $totalppn+$ppn[$jml];
					//$totalfaktur = $totalfaktur+$bruto[$jml];
					$totalfaktur = $totalfaktur+$total[$jml];
					$totalpiutang = $totalpiutang+$piutang;
					
					$jml++;													
					$j++;																																																							
					$nomor++;
				}
				
				$jj=$j+2;
				
				$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$jj, $totalbruto);
				$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$jj, $totaldiskon);
				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$jj, $totalfaktur);
				$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$jj, $totaldpp);
				$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$jj, $totalppn);
				$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$jj, $totalfaktur);
				$ObjPHPExcel->getActiveSheet()->setCellValue('M'.$jj, $totalpiutang);
			}
				
			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			
			$nondo = $f_nota_sederhana=='f'?'':'nondo_';		
			$files	= $this->session->userdata('gid')."laporan_piutang_".$nondo.$dfakturfirst."_".$dfakturlast.".xls";
			$ObjWriter->save("files/".$files);	

			$efilename = @substr($files,1,strlen($files));
	
			$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
																						
		}
		
		$this->load->view('expopiutangfaktur/vexpform',$data);		
	}
		
	function listfaktur() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana = $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		/*** $this->load->model('expopiutangfaktur/mclass'); ***/

		$query	= $this->mclass->lfaktur($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expopiutangfaktur/cform/listfakturnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);
				
		$this->load->view('expopiutangfaktur/vlistfaktur',$data);			
	}

	function listfakturnext() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$fnotasederhana = $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		/*** $this->load->model('expopiutangfaktur/mclass'); ***/

		$query	= $this->mclass->lfaktur($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expopiutangfaktur/cform/listfakturnext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);
		
		$this->load->view('expopiutangfaktur/vlistfaktur',$data);
	}

	function flistfaktur() {
			$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$fnotasederhana = $this->input->post('fnotasederhana')?$this->input->post('fnotasederhana'):$this->input->get_post('fnotasederhana');
		
		$data['page_title']	= "KONTRA BON";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		/*** $this->load->model('expopiutangfaktur/mclass'); ***/

		$query	= $this->mclass->flfaktur($key,$fnotasederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$list .= "
				 <tr>
				  <td width=\"2px;\">".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt')\">".$row->i_dt_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_dt_code','$row->i_dt')\">".$row->d_dt."</a></td>
				 </tr>";
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}

}
?>
