<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('faktur-pajak-makloon/mmaster');
  }

  function index(){
// =======================
	// disini coding utk pengecekan user login
	// $kode_bagian = dari tabel user utk ambil data dari tabel tm_bagian
//========================

	$data['isi'] = 'faktur-pajak-makloon/vmainform';
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon(); 
	$data['msg'] = '';
	$this->load->view('template',$data);

  }
  
  function edit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$id_pajak 	= $this->uri->segment(4);
	$data['query'] = $this->mmaster->get_pajak($id_pajak);
	$data['list_supplier'] = $this->mmaster->get_supplier();
	$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon(); 
	$data['msg'] = '';
	$data['isi'] = 'faktur-pajak-makloon/veditform';
	$this->load->view('template',$data);

  }
  
  // updatedata
  function updatedata() {
			$id_pajak 	= $this->input->post('id_pajak', TRUE);
			$faktur_pajak_lama = $this->input->post('no_faktur_pajak_lama', TRUE);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$jum = $this->input->post('jum', TRUE);  
			$dpp = $this->input->post('dpp', TRUE);  
			$no_faktur = $this->input->post('no_faktur', TRUE);
			$no_faktur_lama = $this->input->post('no_faktur_lama', TRUE);
			$supplier = $this->input->post('supplier', TRUE);
			$jnsmakloon = $this->input->post('jnsmakloon', TRUE);
			
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;
									
			$tgl = date("Y-m-d");			
			
			if ($no_fp != $faktur_pajak_lama) {
				$cek_data = $this->mmaster->cek_data($no_fp, $jnsmakloon, $supplier);
				if (count($cek_data) == 0) {
					$this->db->query(" UPDATE tm_faktur_pajak_makloon SET no_faktur_pajak = '$no_fp', 
									tgl_faktur_pajak = '$tgl_fp', jumlah= '$jum', dpp = '$dpp' where id= '$id_pajak' ");
					
					// reset dulu status_faktur_pajak menjadi FALSE --------------------------------
					$list_faktur_lama = explode(",", $no_faktur_lama); //
					foreach($list_faktur_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
							WHERE kode_unit = '$supplier' AND no_faktur = '$row2' AND jenis_makloon = '$jnsmakloon' "); 
						}
					}
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_faktur_pajak_makloon_detail where id_faktur_pajak_makloon= '$id_pajak' ");
					
					// insert tabel detail faktur-nya
					$list_faktur = explode(",", $no_faktur);
					foreach($list_faktur as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { 
							$data_detail = array(
							  'id_faktur_pajak_makloon'=>$id_pajak,
							  'no_faktur'=>$row1
							);
							$this->db->insert('tm_faktur_pajak_makloon_detail',$data_detail);
							
							// update status_faktur_pajak di tabel tm_faktur_makloon
							$this->db->query(" UPDATE tm_faktur_makloon SET no_faktur_pajak = '$no_fp', 
							status_faktur_pajak = 't' WHERE no_faktur = '$row1' AND kode_unit = '$supplier' 
							AND jenis_makloon = '$jnsmakloon' ");
						}
					} //die();
				
					redirect('faktur-pajak-makloon/cform/view');
				}
				else {
					$data['query'] = $this->mmaster->get_pajak($id_pajak);
					$data['isi'] = 'faktur-pajak-makloon/veditform';
					$data['list_supplier'] = $this->mmaster->get_supplier(); 
					$data['jenis_makloon'] = $this->mmaster->get_jenis_makloon(); 
					$data['msg'] = "Update gagal. Data no faktur pajak ".$no_fp." untuk makloon dgn faktur ".$no_faktur." sudah ada..!";
					$this->load->view('template',$data);
				}	
			}
			else {
					$this->db->query(" UPDATE tm_faktur_pajak_makloon SET no_faktur_pajak = '$no_fp', 
									tgl_faktur_pajak = '$tgl_fp', jumlah= '$jum' where id= '$id_pajak' ");
					
					// reset dulu status_faktur_pajak menjadi FALSE --------------------------------
					$list_faktur_lama = explode(",", $no_faktur_lama); //
					foreach($list_faktur_lama as $row2) {
						$row2 = trim($row2);
						if ($row2 != '') {
							$this->db->query(" UPDATE tm_faktur_makloon SET status_faktur_pajak = 'f', no_faktur_pajak = '' 
							WHERE kode_unit = '$supplier' AND no_faktur = '$row2' AND jenis_makloon = '$jnsmakloon' "); 
						}
					}
					//-------------------------------------------------------------------------
					
					$this->db->query(" DELETE FROM tm_faktur_pajak_makloon_detail where id_faktur_pajak_makloon= '$id_pajak' ");
					
					// insert tabel detail faktur-nya
					$list_faktur = explode(",", $no_faktur);
					foreach($list_faktur as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') { 
							$data_detail = array(
							  'id_faktur_pajak_makloon'=>$id_pajak,
							  'no_faktur'=>$row1
							);
							$this->db->insert('tm_faktur_pajak_makloon_detail',$data_detail);
							
							// update status_faktur_pajak di tabel tm_faktur_makloon
							$this->db->query(" UPDATE tm_faktur_makloon SET no_faktur_pajak = '$no_fp', 
							status_faktur_pajak = 't' WHERE no_faktur = '$row1' AND kode_unit = '$supplier'
							AND jenis_makloon = '$jnsmakloon' ");
						}
					}
				
				redirect('faktur-pajak-makloon/cform/view');
			}
  }
    
  function show_popup_faktur_jasa(){
	// =======================
	// disini coding utk pengecekan user login
//========================
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$jnsaction = $this->uri->segment(4); 
	$csupplier = $this->uri->segment(5); 
	$jenis_makloon = $this->uri->segment(6); 
	
	if ($jnsaction == '')
		$jnsaction = $this->input->post('jnsaction', TRUE); 
	if ($csupplier == '')
		$csupplier = $this->input->post('supplier', TRUE); 
	if ($jenis_makloon == '')
		$jenis_makloon = $this->input->post('jenis_makloon', TRUE); 
	
	if ($keywordcari == '' && ($csupplier == '' || $jnsaction == '' || $jenis_makloon == '') ) {
		$jnsaction 	= $this->uri->segment(4);
		$csupplier 	= $this->uri->segment(5);
		$jenis_makloon 	= $this->uri->segment(6);
		$keywordcari 	= $this->uri->segment(7);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';	
		
	$jum_total = $this->mmaster->get_faktur_jasatanpalimit($jnsaction, $csupplier, $jenis_makloon, $keywordcari);
						/*	$config['base_url'] = base_url()."index.php/faktur-pajak/cform/show_popup_faktur_pembelian/".$jnsaction."/".$csupplier."/".$keywordcari."/";
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		*/
	//$data['query'] = $this->mmaster->get_faktur_pembelian($config['per_page'],$this->uri->segment(7), $jnsaction, $csupplier, $keywordcari);						
	$data['query'] = $this->mmaster->get_faktur_jasa($jnsaction, $csupplier, $jenis_makloon, $keywordcari);
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$data['jnsaction'] = $jnsaction;
	$data['jenis_makloon'] = $jenis_makloon;

	$this->load->view('faktur-pajak-makloon/vpopuppemb',$data);

  }

  function submit(){
	//$this->load->library('form_validation');

			$no_faktur 	= $this->input->post('no_faktur', TRUE);
			$no_faktur = trim($no_faktur);
			$supplier = $this->input->post('supplier', TRUE);
			$no_fp 	= $this->input->post('no_fp', TRUE);
			$tgl_fp = $this->input->post('tgl_fp', TRUE);  
			$pisah1 = explode("-", $tgl_fp);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_fp = $thn1."-".$bln1."-".$tgl1;						
			$jum 	= $this->input->post('jum', TRUE);
			$dpp 	= $this->input->post('dpp', TRUE);
			$jenis_makloon = $this->input->post('jenis_makloon', TRUE);
			
				$cek_data = $this->mmaster->cek_data($no_fp, $jenis_makloon, $supplier);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'faktur-pajak-makloon/vmainform';
					$data['msg'] = "Data no faktur ".$no_fp." dari unit makloon ".$supplier." sudah ada..!";
					$this->load->view('template',$data);
				}
				else {
					$this->mmaster->save($no_fp, $tgl_fp, $jenis_makloon, $supplier, $jum, $dpp, $no_faktur);
					redirect('faktur-pajak-makloon/cform/view');
				}
  }
  
  function view(){
    $data['isi'] = 'faktur-pajak-makloon/vformview';
    $keywordcari = "all";
    $csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-pajak-makloon/cform/view/index/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(5);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(5), $csupplier, $keywordcari);		
				
	$data['jum_total'] = count($jum_total);
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }
  
  function cari(){
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$csupplier = $this->input->post('supplier', TRUE);  
	
	if ($keywordcari == '' && $csupplier == '') {
		$csupplier 	= $this->uri->segment(4);
		$keywordcari 	= $this->uri->segment(5);
	}
	
	if ($keywordcari == '')
		$keywordcari 	= "all";

	if ($csupplier == '')
		$csupplier = '0';
	
    $jum_total = $this->mmaster->getAlltanpalimit($csupplier, $keywordcari);
							$config['base_url'] = base_url().'index.php/faktur-pajak-makloon/cform/cari/'.$csupplier.'/'.$keywordcari.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(6);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAll($config['per_page'],$this->uri->segment(6), $csupplier, $keywordcari);						
	$data['jum_total'] = count($jum_total);
	$data['isi'] = 'faktur-pajak-makloon/vformview';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['list_supplier'] = $this->mmaster->get_supplier(); 
	$data['csupplier'] = $csupplier;
	$this->load->view('template',$data);
  }

  function delete(){
    $kode 	= $this->uri->segment(4);
    $this->mmaster->delete($kode);
    redirect('faktur-pajak-makloon/cform/view');
  }
  
  function get_detail_supplier() {
		$kode_sup 	= $this->uri->segment(4);
		$rows = array();
		if(isset($kode_sup)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			//$rows = $this->mmaster->get_pkp_tipe_pajak_bykodesup($kode_sup);
			$rows = $this->mmaster->get_detail_supplier($kode_sup);
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);
  }
  
}
