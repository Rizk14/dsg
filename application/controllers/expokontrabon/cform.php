<?php
class Cform extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}
	
	function index() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			
			$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
			$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
			$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
			$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
			$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
			$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
			$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
			$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
			$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
			$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
			$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
			$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
			$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
			$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
			$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
			$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
			
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['button_detail']	= $this->lang->line('button_detail');	
			$data['detail']		= "";
			$data['list']		= "";
			$data['disabled']	= 'f';
			$data['limages']	= base_url();
			
			$this->load->model('expokontrabon/mclass');
			$data['isi'] = 'expokontrabon/vmainform';
			$this->load->view('template',$data);
				
	}
	
	function carilistkontrabon() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
		$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
		$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
		$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
		$data['disabled']	= "";
		
		$nokontrabon		= $this->input->post('no_kontrabon');
		$ikontrabon			= $this->input->post('i_kontrabon');
		
		$dkontrabonfirst= $this->input->post('d_kontrabon_first');
		$dkontrabonlast	= $this->input->post('d_kontrabon_last');
		
		$tf_nota_sederhana	= $this->input->post('tf_nota_sederhana');
		
		$data['tglkontrabonmulai']	= (!empty($dkontrabonfirst) || $dkontrabonfirst!='0')?$dkontrabonfirst:'';
		$data['tglkontrabonakhir']	= (!empty($dkontrabonlast) || $dkontrabonlast!='0')?$dkontrabonlast:'';
		$data['nokotrabon']	= (!empty($nokontrabon))?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$e_d_kontrabon_first= explode("/",$dkontrabonfirst,strlen($dkontrabonfirst));
		$e_d_kontrabon_last	= explode("/",$dkontrabonlast,strlen($dkontrabonlast));
		
		$ndkontrabonfirst	= !empty($e_d_kontrabon_first[2])?$e_d_kontrabon_first[2].'-'.$e_d_kontrabon_first[1].'-'.$e_d_kontrabon_first[0]:'0';
		$ndkontrabonlast	= !empty($e_d_kontrabon_last[2])?$e_d_kontrabon_last[2].'-'.$e_d_kontrabon_last[1].'-'.$e_d_kontrabon_last[0]:'0';
		
		$turi1	= ($nokontrabon!='' && $nokontrabon!='0')?$nokontrabon:'0';
		$turi2	= ($ikontrabon!='' && $ikontrabon!='0')?$ikontrabon:'0';
		$turi3	= $ndkontrabonfirst;
		$turi4	= $ndkontrabonlast;
		$turi5	= $tf_nota_sederhana;
		
		$data['turi1']	= $nokontrabon;
		$data['turi2']	= $ikontrabon;
		$data['turi3']	= $ndkontrabonfirst;
		$data['turi4']	= $ndkontrabonlast;
		$data['turi5']	= $tf_nota_sederhana;
		
		$this->load->model('expokontrabon/mclass');
		
		$pagination['base_url'] 	= '/expokontrabon/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if(($dkontrabonfirst!='') && ($dkontrabonlast!='')) {
			$qlistkontrabon	= $this->mclass->clistkontrabon($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 1;
		}else{
			$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 2;
		}
		
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if(($dkontrabonfirst!='') && ($dkontrabonlast!='')) {
			$data['query']	= $this->mclass->clistkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}else{
			$data['query']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}
		$data['isi'] = 'expokontrabon/vlistform';
			$this->load->view('template',$data);
		
	}

	function carilistkontrabonnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
		$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
		$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
		$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
		$data['disabled']	= "";
		
		$nokontrabon	= $this->uri->segment(4);
		$ikontrabon		= $this->uri->segment(5);
		$ndkontrabonfirst	= $this->uri->segment(6);
		$ndkontrabonlast	= $this->uri->segment(7);
		$tf_nota_sederhana = $this->uri->segment(8); 

		$data['turi1']	= $nokontrabon;
		$data['turi2']	= $ikontrabon;
		$data['turi3']	= $ndkontrabonfirst;
		$data['turi4']	= $ndkontrabonlast;
		$data['turi5']	= $tf_nota_sederhana;
		
		$e_d_do_first	= ($ndkontrabonfirst!='0')?explode("-",$ndkontrabonfirst,strlen($ndkontrabonfirst)):'';
		$e_d_do_last	= ($ndkontrabonlast!='0')?explode("-",$ndkontrabonlast,strlen($ndkontrabonlast)):'';

		$nddofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:'0';
		$nddolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:'0';
						
		$data['tglkontrabonmulai']	= (!empty($nddofirst) && $nddofirst!='0')?$nddofirst:'';
		$data['tglkontrabonakhir']	= (!empty($nddolast) && $nddolast!='0')?$nddolast:'';
		$data['nokotrabon']	= (!empty($nokontrabon) || $nokontrabon!='0')?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$turi1	= ($nokontrabon!='' || $nokontrabon!='0')?$nokontrabon:'0';
		$turi2	= ($ikontrabon!='' || $ikontrabon!='0')?$ikontrabon:'0';
		$turi3	= $ndkontrabonfirst;
		$turi4	= $ndkontrabonlast;
		$turi5	= $tf_nota_sederhana;
		
		$this->load->model('expokontrabon/mclass');
		
		$pagination['base_url'] 	= '/expokontrabon/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if(($ndkontrabonfirst!='0') && ($ndkontrabonlast!='0')) {
			$qlistkontrabon	= $this->mclass->clistkontrabon($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 1;
		} else {
			$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 2;
		}
		
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if(($ndkontrabonfirst!='0') && ($ndkontrabonlast!='0')) {
			$data['isi']	= $this->mclass->clistkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}else{
			$data['isi']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}
		
		$this->load->view('expokontrabon/vlistform',$data);	
	}
	
	function listbarangjadi() {
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana = $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('expokontrabon/mclass');

		$query	= $this->mclass->lbarangjadi($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expokontrabon/cform/listbarangjadinext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('expokontrabon/vlistformbrgjadi',$data);			
	}

	function listbarangjadinext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title']	= "KONTRA BON";
		$data['isi']		= "";
		$data['list']		= "";
		$data['lurl']		= base_url();
		
		$fnotasederhana = $this->uri->segment(4);
		$data['fnotasederhana'] = $fnotasederhana;
		
		$this->load->model('expokontrabon/mclass');

		$query	= $this->mclass->lbarangjadi($fnotasederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] 	= base_url().'index.php/expokontrabon/cform/listbarangjadinext/'.$fnotasederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(5,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lbarangjadiperpages($pagination['per_page'],$pagination['cur_page'],$fnotasederhana);		
				
		$this->load->view('expokontrabon/vlistformbrgjadi',$data);			
	}		

	function flistbarangjadi() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$key	= $this->input->post('key')?$this->input->post('key'):$this->input->get_post('key');
		$fnotasederhana = $this->input->post('fnotasederhana')?$this->input->post('fnotasederhana'):$this->input->get_post('fnotasederhana');
		
		$data['page_title']	= "KONTRA BON";
		$data['isi']	= "";
		$data['lurl']	= base_url();
		$data['key']	= $key;

		$this->load->model('expokontrabon/mclass');

		$query	= $this->mclass->flbarangjadi($key,$fnotasederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		if($jml>0) {
			$cc	= 1; 
			foreach($query->result() as $row){
				
				$f_nota_sederhana = ($row->f_nota_sederhana=='t')?'Faktur Non DO':'';
				
				$ikontrabon	= trim($row->idtcode);
					
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$row->idtcode."</a></td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$row->ddt."</a></td>
				  <td><a href=\"javascript:settextfield('$row->idt','$ikontrabon')\">".$f_nota_sederhana."</a></td>
				 </tr>";
					 
				 $cc+=1;
			}
		}
		
		$item	= 
		"<table class=\"listtable2\">
		<tbody>".
		$list
		."</tbody>
		</table>";
				
		echo $item;
	}	
	
	function listfaktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iterasi	= $this->uri->segment(4);
		$f_nota_sederhana = $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		
		$data['f_nota_sederhana']	= $f_nota_sederhana;
		
		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('expokontrabon/mclass');

		$query	= $this->mclass->lfaktur($f_nota_sederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/expokontrabon/cform/listfakturnext/'.$iterasi.'/'.$f_nota_sederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$f_nota_sederhana);		

		$this->load->view('expokontrabon/vlistfaktur',$data);
	}
	
	function listfakturnext() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$iterasi	= $this->uri->segment(4);
		$f_nota_sederhana = $this->uri->segment(5);
		
		$data['iterasi']	= $iterasi;
		$data['f_nota_sederhana']	= $f_nota_sederhana;

		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['list']		= "";
		$data['lurl']		= base_url();

		$this->load->model('expokontrabon/mclass');

		$query	= $this->mclass->lfaktur($f_nota_sederhana);
		$jml	= $query->num_rows();
		$result	= $query->result();
		
		$pagination['base_url'] = base_url().'index.php/expokontrabon/cform/listfakturnext/'.$iterasi.'/'.$f_nota_sederhana.'/';
		$pagination['total_rows']	= $jml;
		$pagination['per_page']		= 20;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(6,0);
		$this->pagination_ori->initialize($pagination);
		$data['create_links']	= $this->pagination_ori->create_links();
		$data['isi']	= $this->mclass->lfakturperpages($pagination['per_page'],$pagination['cur_page'],$f_nota_sederhana);		

		$this->load->view('expokontrabon/vlistfaktur',$data);		
	}
	
	function flistfaktur() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$key2	= $this->input->post('key');
		$f_nota_sederhana = $this->input->post('f_nota_sederhana');
		
		$data['f_nota_sederhana']	= $f_nota_sederhana;
		
		$data['page_title']	= "FAKTUR PENJUALAN";
		$data['lurl']		= base_url();

		$this->load->model('expokontrabon/mclass');

		$query	= $this->mclass->flfaktur($key2,$f_nota_sederhana);
		$jml	= $query->num_rows();
		
		$list	= "";
		
		$bln	= array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'Nopember', '12'=>'Desember');
		
		if($jml>0) {
			
			$cc	= 1;
			
			foreach($query->result() as $row){
				
				$tgl	= explode("-",$row->d_faktur,strlen($row->d_faktur)); // YYYY-mm-dd
				$tgl_zero_null	= substr($tgl[2],0,1)=='0'?substr($tgl[2],1,1):$tgl[2];
				$tanggal		= !empty($tgl[0])?$tgl_zero_null." ".$bln[$tgl[1]]." ".$tgl[0]:"";
				
				$list .= "
				 <tr>
				  <td>".$cc."</td>
				  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur','$row->d_faktur','$row->d_faktur','$row->d_due_date','$row->d_due_date','$row->e_branch_name','$row->v_total_fppn','$row->i_customer','$row->i_branch_code')\">".$row->i_faktur_code."</a></td>
				  <td><a href=\"javascript:settextfield('$row->i_faktur_code','$row->i_faktur','$row->d_faktur','$row->d_faktur','$row->d_due_date','$row->d_due_date','$row->e_branch_name','$row->v_total_fppn','$row->i_customer','$row->i_branch_code')\">".$tanggal."</a></td>
				 </tr>";
				 
				 $cc+=1;
			}
		}
		
		$item	= "<table class=\"listtable2\"><tbody>".$list."</tbody></table>";
		
		echo $item;		
	}
	
	function detail() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
			$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
			$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
			$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
			$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
			$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
			$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
			$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
			$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
			$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
			$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
			$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
			$data['list_kontrabon_no_kontrabon']= $this->lang->line('list_kontrabon_no_kontrabon');
			$data['list_piutang_nilai_kontrabon'] = $this->lang->line('list_piutang_nilai_kontrabon');
			
			$data['button_update']	= $this->lang->line('button_update');
			$data['button_batal']	= $this->lang->line('button_batal');
			$data['disabled']	= 'f';
			$data['isi']		= "";
			$data['list']		= "";
			$data['lcabang']	= "";
			$data['limages']	= base_url();
			$tahun	= date("Y");
			$data['tjthtempo']	= "";
			
			$idtcode	= $this->uri->segment(4);			 			
			$idt		= $this->uri->segment(5);
			$ndkontrabonfirst	= $this->uri->segment(6);
			$ndkontrabonlast	= $this->uri->segment(7);
			$fnotasederhana 	= $this->uri->segment(8);
			
			$data['idt']	= $idt;
			$data['idtcode']= $idtcode;
			$data['fnotasederhana'] = $fnotasederhana;

			$data['turi1']	= $idtcode;
			$data['turi2']	= $idt;
			$data['turi3']	= $ndkontrabonfirst;
			$data['turi4']	= $ndkontrabonlast;
			$data['turi5']	= $fnotasederhana;
									 
			$this->load->model('expokontrabon/mclass');
			
			$qdt	= $this->mclass->getdtheader2($idt,$fnotasederhana);
			
			if($qdt->num_rows()>0) {
				
				$rdt	= $qdt->row();
				
				$exp_ddt= explode("-",$rdt->d_dt,strlen($rdt->d_dt)); // YYYY-mm-dd
				
				$data['idtcode']	= $rdt->i_dt_code;
				$data['ddt']		= $exp_ddt[2]."/".$exp_ddt[1]."/".$exp_ddt[0];
				$data['idt']		= $rdt->i_dt;
			}else{
				$data['idtcode']	= "";
				$data['ddt']		= "";
				$data['idt']		= "";
			}
			
			$data['idtitem']	= $this->mclass->ldtitem2($idt,$fnotasederhana);
			
			$this->load->view('expokontrabon/vformdetail',$data);
				
	}

	function backlistkontrabon() {
$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
		$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
		$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
		$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
		$data['disabled']	= "";
		
		$nokontrabon		= $this->uri->segment(4);
		$ikontrabon			= $this->uri->segment(5);
		$ndkontrabonfirst	= $this->uri->segment(6);
		$ndkontrabonlast	= $this->uri->segment(7);
		$tf_nota_sederhana	= $this->uri->segment(8);
		
		$data['tglkontrabonmulai']	= '';
		$data['tglkontrabonakhir']	= '';
		$data['nokotrabon']	= (!empty($nokontrabon))?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$turi1	= $nokontrabon;
		$turi2	= $ikontrabon;
		$turi3	= '0';
		$turi4	= '0';
		$turi5	= $tf_nota_sederhana;
			
		$data['turi1']	= $turi1;
		$data['turi2']	= $turi2;
		$data['turi3']	= $ndkontrabonfirst;
		$data['turi4']	= $ndkontrabonlast;
		$data['turi5']	= $tf_nota_sederhana;
		
		$this->load->model('expokontrabon/mclass');
		
		$pagination['base_url'] 	= '/expokontrabon/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		$data['templates']	= 2;
		
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		$data['isi']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		
		$this->load->view('expokontrabon/vlistform',$data);	
	}
	
	function cari_fpenjualan(){
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
		$fpenj	= $this->input->post('fpenj')?$this->input->post('fpenj'):$this->input->get_post('fpenj');
		$fpenjhiden	= $this->input->post('fpenjhiden')?$this->input->post('fpenjhiden'):$this->input->get_post('fpenjhiden');
		$this->load->model('listpenjualanperdo/mclass');
		$qnsop	= $this->mclass->cari_fpenjualan($fpenj,$fpenjhiden);
		if($qnsop->num_rows()>0) {
			echo "Maaf, No. faktur sudah ada!";		
		}
	}

	function expokontrabon() {
		$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
///
		$data['page_title_kontrabon']	= $this->lang->line('page_title_kontrabon');
		$data['form_title_detail_kontrabon']= $this->lang->line('form_title_detail_kontrabon');
		$data['list_kontrabon_no_faktur']	= $this->lang->line('list_kontrabon_no_faktur');
		$data['list_kontrabon_tgl_faktur']	= $this->lang->line('list_kontrabon_tgl_faktur');
		$data['list_kontrabon_tgl_kontrabon']	= $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_tgl_tempo']	= $this->lang->line('list_kontrabon_tgl_tempo');
		$data['list_kontrabon_kd_brg']	= $this->lang->line('list_kontrabon_kd_brg');
		$data['list_kontrabon_nm_brg']	= $this->lang->line('list_kontrabon_nm_brg');
		$data['list_kontrabon_pelanggan']	= $this->lang->line('list_kontrabon_pelanggan');
		$data['list_kontrabon_jml_tagih']	= $this->lang->line('list_kontrabon_jml_tagih');
		$data['list_kontrabon_total_tagih']	= $this->lang->line('list_kontrabon_total_tagih');
		$data['list_kontrabon_tgl_kontrabon'] = $this->lang->line('list_kontrabon_tgl_kontrabon');
		$data['list_kontrabon_nilai'] = $this->lang->line('list_kontrabon_nilai');
		$data['list_kontrabon_discount'] = $this->lang->line('list_kontrabon_discount');
		$data['list_kontrabon_ppn'] = $this->lang->line('list_kontrabon_ppn');
		$data['list_kontrabon_status_pelunasan'] = $this->lang->line('list_kontrabon_status_pelunasan');
		$data['list_kontrabon_no_kontrabon'] = $this->lang->line('list_kontrabon_no_kontrabon');
		$data['list_kontrabon_nota_sederhana'] = $this->lang->line('list_kontrabon_nota_sederhana');
		
		$data['button_excel']	= $this->lang->line('button_excel');
		$data['button_keluar']	= $this->lang->line('button_keluar');
		$data['link_aksi']	= $this->lang->line('link_aksi');
		$data['detail']		= "";
		$data['list']		= "";
		$data['lkontrabon']	= "";		
		$data['limages']	= base_url();
		$data['disabled']	= "t";
		
		$nokontrabon	= $this->uri->segment(4);
		$ikontrabon		= $this->uri->segment(5);
		$ndkontrabonfirst	= $this->uri->segment(6);
		$ndkontrabonlast	= $this->uri->segment(7);
		$tf_nota_sederhana = $this->uri->segment(8); 

		$data['turi1']	= $nokontrabon;
		$data['turi2']	= $ikontrabon;
		$data['turi3']	= $ndkontrabonfirst;
		$data['turi4']	= $ndkontrabonlast;
		$data['turi5']	= $tf_nota_sederhana;
		
		$e_d_do_first	= ($ndkontrabonfirst!='0')?explode("-",$ndkontrabonfirst,strlen($ndkontrabonfirst)):'';
		$e_d_do_last	= ($ndkontrabonlast!='0')?explode("-",$ndkontrabonlast,strlen($ndkontrabonlast)):'';

		$nddofirst	= !empty($e_d_do_first[2])?$e_d_do_first[2].'/'.$e_d_do_first[1].'/'.$e_d_do_first[0]:'0';
		$nddolast	= !empty($e_d_do_last[2])?$e_d_do_last[2].'/'.$e_d_do_last[1].'/'.$e_d_do_last[0]:'0';
						
		$data['tglkontrabonmulai']	= (!empty($nddofirst) && $nddofirst!='0')?$nddofirst:'';
		$data['tglkontrabonakhir']	= (!empty($nddolast) && $nddolast!='0')?$nddolast:'';
		$data['nokotrabon']	= (!empty($nokontrabon) || $nokontrabon!='0')?$nokontrabon:'';
		$data['ikontrabon']	= (!empty($ikontrabon))?$ikontrabon:'';
		$data['tf_nota_sederhana'] = $tf_nota_sederhana;
		
		$data['checked'] = $tf_nota_sederhana=='t'?'checked':'';
		
		$turi1	= ($nokontrabon!='' || $nokontrabon!='0')?$nokontrabon:'0';
		$turi2	= ($ikontrabon!='' || $ikontrabon!='0')?$ikontrabon:'0';
		$turi3	= $ndkontrabonfirst;
		$turi4	= $ndkontrabonlast;
		$turi5	= $tf_nota_sederhana;
		
		$this->load->model('expokontrabon/mclass');
		
		$pagination['base_url'] 	= '/expokontrabon/cform/carilistkontrabonnext/'.$turi1.'/'.$turi2.'/'.$turi3.'/'.$turi4.'/'.$turi5.'/';
		
		if(($ndkontrabonfirst!='0') && ($ndkontrabonlast!='0')) {
			$qlistkontrabon	= $this->mclass->clistkontrabon($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 1;
		} else {
			$qlistkontrabon	= $this->mclass->clistkontrabon2($ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
			$data['templates']	= 2;
		}
		
		$pagination['total_rows']	= $qlistkontrabon->num_rows();
		$pagination['per_page']		= 10;
		$pagination['first_link'] 	= 'Awal';
		$pagination['last_link'] 	= 'Akhir';
		$pagination['next_link'] 	= 'Selanjutnya';
		$pagination['prev_link'] 	= 'Sebelumnya';
		$pagination['cur_page'] 	= $this->uri->segment(9,0);
		$this->pagination->initialize($pagination);
		$data['create_link']		= $this->pagination->create_links();
		
		if(($ndkontrabonfirst!='0') && ($ndkontrabonlast!='0')) {
			$data['isi']	= $this->mclass->clistkontrabonperpages($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}else{
			$data['isi']	= $this->mclass->clistkontrabonperpages2($pagination['per_page'],$pagination['cur_page'],$ikontrabon,$ndkontrabonfirst,$ndkontrabonlast,$tf_nota_sederhana);
		}
///
		
		$qheader_dt	= $this->mclass->expokontrabon($ikontrabon,$tf_nota_sederhana);
		
		$nomor2	= 1;
		$cel	= 8;
		$total	= 0;
		
		if($qheader_dt->num_rows()>0) {
			
			$rheader_dt	= $qheader_dt->row();
			
			$ddt	= explode("-",$rheader_dt->d_dt,strlen($rheader_dt->d_dt));
			$tgl	= substr($ddt[2],0,1)=='0'?substr($ddt[2],1,1):$ddt[2];
			$bl	= $ddt[1];
			$thn	= $ddt[0];

			$arrbln	= array(
				'01'=>'Januari',
				'02'=>'Februari',
				'03'=>'Maret',
				'04'=>'April',
				'05'=>'Mei',
				'06'=>'Juni',
				'07'=>'Juli',
				'08'=>'Agustus',
				'09'=>'September',
				'10'=>'Oktober',
				'11'=>'Nopember',
				'12'=>'Desember'
			);
			$bln = $arrbln[$bl];

			$qdetail_dt	= $this->mclass->expokontrabon_detail($rheader_dt->i_dt,$tf_nota_sederhana);
			
			$j	= 0;
								
			$ObjPHPExcel = new PHPExcel();
			
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Data Stok Barang")
				->setSubject("Stok Barang")
				->setDescription("Laporan Data Stok Barang")
				->setKeywords("Laporan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);

			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:F2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN KONTRA BON');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				)
			),
			'A3'
			);			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:F3');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', "NOMOR KONTRA BON : ".$rheader_dt->i_dt_code);

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				)
			),
			'A4'
			);			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:F4');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', "TGL KONTRA BON : ".$tgl." ".$bln." ".$thn);
		
			$ObjPHPExcel->getActiveSheet()->mergeCells('A6:A7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)
			));	
		
			$ObjPHPExcel->getActiveSheet()->mergeCells('B6:B7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'NO FAKTUR');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)
			));			
			$ObjPHPExcel->getActiveSheet()->mergeCells('C6:D6');
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'TANGGAL');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)
			));	
		
			$ObjPHPExcel->getActiveSheet()->setCellValue('C7', 'FAKTUR');
			$ObjPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)
			));

			$ObjPHPExcel->getActiveSheet()->setCellValue('D7', 'JATUH TEMPO');
			$ObjPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)
			));

			$ObjPHPExcel->getActiveSheet()->mergeCells('E6:E7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'PELANGGAN');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)
			));

			$ObjPHPExcel->getActiveSheet()->mergeCells('F6:F7');
			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'JUMLAH');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)
			));
						
			foreach($qdetail_dt->result() as $field2) {
				
				$dnota = explode("-",$field2->d_nota,strlen($field2->d_nota));
				$dtempo = explode("-",$field2->d_due_date,strlen($field2->d_due_date));

				if($tf_nota_sederhana=='f') {
					$query2 = $this->mclass->totalfaktur($field2->i_nota);
				}else{
					$query2 = $this->mclass->totalfakturNONDO($field2->i_nota);
				}

				$query3 = $this->mclass->vdiskon($field2->i_nota,$tf_nota_sederhana);

				$row2	= $query2->row();
				$row3	= $query3->row();
				
				$nilai_ppn_perfaktur = (($row2->totalfaktur*10) / 100);
				
				if($row3->v_discount>0){
					$total_sblm_ppn_perfaktur	= round($row2->totalfaktur - $row3->v_discount); // DPP
					$nilai_ppn2_perfaktur	= round((($total_sblm_ppn_perfaktur*10) / 100));
					$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
				}else{
					$total_sblm_ppn_perfaktur	= round($row2->totalfaktur); // DPP
					$nilai_ppn2_perfaktur	= round($nilai_ppn_perfaktur);
					$total_grand_perfaktur	= round($total_sblm_ppn_perfaktur + $nilai_ppn2_perfaktur);
				}

				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$cel, $nomor2);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'italic'=> false,
							'size'  => 9
							),									
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)

						)
					);
						
				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$cel, $field2->i_faktur_code);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'italic'=> false,
							'size'  => 9
							),									
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)

						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$cel, $dnota[2]."/".$dnota[1]."/".$dnota[1]);
				$ObjPHPExcel->getActiveSheet()->getStyle('C'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'italic'=> false,
							'size'  => 9
							),									
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)

						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$cel, $dtempo[2]."/".$dtempo[1]."/".$dtempo[1]);
				$ObjPHPExcel->getActiveSheet()->getStyle('D'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'italic'=> false,
							'size'  => 9
							),									
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)

						)
					);

				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$cel, $field2->e_branch_name);
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'italic'=> false,
							'size'  => 9
							),									
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)

						)
					);
							
				$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$cel, $total_grand_perfaktur);
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$cel)->applyFromArray(
						array(
							'font' => array(
							'name'	=> 'Arial',
							'italic'=> false,
							'size'  => 9
							),									
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
							)

						)
					);
					
				$j++;

				$total = $total_grand_perfaktur+$total;	

				$nomor2++;	
				$cel++;
			}
			
			$cel2 = $cel+1;

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				),									
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)

			),
			'A'.$cel2
			);	

			$ObjPHPExcel->getActiveSheet()->mergeCells('A'.$cel2.':E'.$cel2);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$cel2, "TOTAL KONTRA BON ");

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'italic'=> false,
				'size'  => 10
				),
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
				)

			),
			'F'.$cel2
			);	

			$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$cel2, $total);
			
			$ObjWriter	= IOFactory::createWriter($ObjPHPExcel, 'Excel5');	

			$files	= $this->session->userdata('gid')."laporan_kontrabon_".$rheader_dt->i_dt_code.".xls";
			$ObjWriter->save("files/".$files);
		}

		$efilename = @substr($files,1,strlen($files));
	
		$this->mclass->logfiles($efilename,$this->session->userdata('user_idx'));
		
		$this->load->view('expokontrabon/vlistform',$data);
	}
}
?>
