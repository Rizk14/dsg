<?php
class Creport extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('wip/mreport');
  }
  
  // 15-03-2014
  function viewsohasiljahit(){
     $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
    
    $data['isi'] = 'wip/vformviewsohasiljahit';
	$gudang 	= $this->input->post('gudang', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);  
	
	if ($gudang == '' && $bulan=='' && $tahun=='') {
		$gudang 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($gudang == '')
		$gudang = 0;

    $jum_total = $this->mreport->get_sowiptanpalimit($gudang, $bulan, $tahun);

			$config['base_url'] = base_url().'index.php/wip/creport/viewsohasiljahit/'.$gudang.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mreport->get_sowip($config['per_page'],$this->uri->segment(7), $gudang, $bulan, $tahun);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;

	$data['list_gudang'] = $this->mreport->get_gudang();
	$data['cgudang'] = $gudang;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  // 18-03-2014. 02-11-2015 cek2
  function editsohasiljahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cgudang 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cgudang'] = $cgudang;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mreport->get_sohasiljahit($id_so); 
	$data['querybrgbaru'] = $this->mreport->get_detail_stokbrgbaru_hasiljahit($id_so); 
	
	//$data['jum_total'] = count($data['query']);
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	if (is_array($data['querybrgbaru']))
		$data['jum_total'] += count($data['querybrgbaru']);
		
	$data['isi'] = 'wip/veditsohasiljahit';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  function updatesohasiljahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cgudang = $this->input->post('cgudang', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  $id_gudang = $this->input->post('id_gudang', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_hasil_jahit SET tgl_so = '$tgl_so', jenis_perhitungan_stok='$jenis_perhitungan_stok',
							tgl_update = '$tgl', status_approve='t' 
							where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mreport->savesohasiljahit($id_so, $tgl_so, $jenis_perhitungan_stok, $id_gudang,
			 $this->input->post('id_'.$i, TRUE),
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
			  
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "wip/creport/viewsohasiljahit/".$cgudang."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 06-11-2015
  function viewsounitjahit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
    $data['isi'] = 'wip/vformviewsounitjahit';
	$id_unit 	= $this->input->post('id_unit', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);
	$s_approve  = $this->input->post('s_approve', TRUE);
	
	if ($id_unit == '' && $bulan=='' && $tahun=='') {
		$id_unit 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($id_unit == '')
		$id_unit = '0';

    $jum_total = $this->mreport->get_sowipunittanpalimit($id_unit, $bulan, $tahun, $s_approve);

			$config['base_url'] = base_url().'index.php/wip/creport/viewsounitjahit/'.$id_unit.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mreport->get_sowipunit($config['per_page'],$this->uri->segment(7), $id_unit, $bulan, $tahun, $s_approve);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['cunit'] = $id_unit;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  // 17-03-2014
  function editsounitjahit(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cunit 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cunit'] = $cunit;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mreport->get_sounitjahit($id_so); 
	$data['querybrgbaru'] = $this->mreport->get_detail_stokbrgbaru_unitjahit($id_so); 
	
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	if (is_array($data['querybrgbaru']))
		$data['jum_total'] += count($data['querybrgbaru']);
	//$html_data .= $data['jum_total']; die();
	
	$data['isi'] = 'wip/veditsounitjahit';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  function updatesounitjahit() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $id_unit = $this->input->post('id_unit', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cunit = $this->input->post('cunit', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_unit_jahit SET tgl_so = '$tgl_so', 
						jenis_perhitungan_stok='$jenis_perhitungan_stok', tgl_update = '$tgl', status_approve='t' 
						where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mreport->savesounitjahit($id_so, $tgl_so, $id_unit, $jenis_perhitungan_stok,
			 $this->input->post('id_'.$i, TRUE),
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
		//die();
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "wip/creport/viewsounitjahit/".$cunit."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 09-11-2015
  function viewsounitpacking(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
    $data['isi'] = 'wip/vformviewsounitpacking';
	$id_unit 	= $this->input->post('id_unit', TRUE);  
	$bulan 	= $this->input->post('bulan', TRUE);  
	$tahun 	= $this->input->post('tahun', TRUE);  
	
	if ($id_unit == '' && $bulan=='' && $tahun=='') {
		$id_unit 	= $this->uri->segment(4);
		$bulan 	= $this->uri->segment(5);
		$tahun 	= $this->uri->segment(6);
	}
	
	if ($bulan == '')
		$bulan 	= "00";
	if ($tahun == '')
		$tahun 	= "0";
	if ($id_unit == '')
		$id_unit = '0';

    $jum_total = $this->mreport->get_sowipunitpackingtanpalimit($id_unit, $bulan, $tahun);

			$config['base_url'] = base_url().'index.php/wip/creport/viewsounitpacking/'.$id_unit.'/'.$bulan.'/'.$tahun.'/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
						
	$data['query'] = $this->mreport->get_sowipunitpacking($config['per_page'],$this->uri->segment(7), $id_unit, $bulan, $tahun);
	$data['jum_total'] = count($jum_total);
	
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	
	$data['list_unit'] = $this->mreport->get_unit_packing();
	$data['cunit'] = $id_unit;
	$data['cbulan'] = $bulan;
	
	if ($tahun == '0')
		$data['ctahun'] = '';
	else
		$data['ctahun'] = $tahun;
	$data['startnya'] = $config['cur_page'];

	$this->load->view('template',$data);
  }
  
  function editsounitpacking(){
    $is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	// ------------------------
	  
	$id_so 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$cunit 	= $this->uri->segment(6);
	$cbulan 	= $this->uri->segment(7);
	$ctahun 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['cunit'] = $cunit;
	$data['cbulan'] = $cbulan;
	$data['ctahun'] = $ctahun;
	$data['id_so'] = $id_so;
	
	$data['query'] = $this->mreport->get_sounitpacking($id_so); 
	$data['querybrgbaru'] = $this->mreport->get_detail_stokbrgbaru_unitpacking($id_so); 
	
	if (is_array($data['query']))
		$data['jum_total'] = count($data['query']);
	if (is_array($data['querybrgbaru']))
		$data['jum_total'] += count($data['querybrgbaru']);
	//$html_data .= $data['jum_total']; die();
	
	$data['isi'] = 'wip/veditsounitpacking';
	$this->load->view('template',$data);
	//-------------------------
  }
  
  // 13-02-2016 lanjutan desta
  // contek dari unit jahit
  function updatesounitpacking() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
	  if (!isset($is_logged_in) || $is_logged_in!= true) {
	  	  redirect('loginform');
	  }
	  
	  $id_so = $this->input->post('id_so', TRUE);
	  $id_unit = $this->input->post('id_unit', TRUE);
	  $jum_data = $this->input->post('jum_data', TRUE);  
	  
	    $cur_page = $this->input->post('cur_page', TRUE);
		$cunit = $this->input->post('cunit', TRUE);
		$cbulan = $this->input->post('cbulan', TRUE);
		$ctahun = $this->input->post('ctahun', TRUE);
	  
	  // 22-12-2014
	  $tgl_so 	= $this->input->post('tgl_so', TRUE);
	  $pisah1 = explode("-", $tgl_so);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$tgl_so = $thn1."-".$bln1."-".$tgl1;
	  
	  $jenis_perhitungan_stok 	= $this->input->post('jenis_hitung', TRUE);
	  $tgl = date("Y-m-d H:i:s"); 
	  
		  // update ke tabel tt_stok_opname_
		  // ambil data terakhir di tabel tt_stok_opname_				 
		 $this->db->query(" UPDATE tt_stok_opname_unit_packing SET tgl_so = '$tgl_so', 
						jenis_perhitungan_stok='$jenis_perhitungan_stok', tgl_update = '$tgl', status_approve='t' 
						where id = '$id_so' ");
				 
		 for ($i=1;$i<=$jum_data;$i++)
		 {
			 $this->mreport->savesounitpacking($id_so, $tgl_so, $id_unit, $jenis_perhitungan_stok,
			 $this->input->post('id_'.$i, TRUE),
			 $this->input->post('id_brg_wip_'.$i, TRUE), 
			 $this->input->post('id_warna_'.$i, TRUE),
			 $this->input->post('stok_fisik_'.$i, TRUE)
			 );
		 }
		//die();
		if ($ctahun == '') $ctahun = "0";
			$url_redirectnya = "wip/creport/viewsounitpacking/".$cunit."/".$cbulan."/".$ctahun."/".$cur_page;
		redirect($url_redirectnya);
  }
  
  // 12-11-2015
  // ++++++++++++++++++ mutasi brg unit WIP ++++++++++++++++++++++++++++
  function mutasiunit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformmutasiunit';
	$this->load->view('template',$data);
  }
  
  function viewmutasiunit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewmutasiunit';
    
	$date_from = $this->input->post('date_from', TRUE);
	$pisah1 = explode("-", $date_from);
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tgldari= $tahun."-".$bulan."-".$tanggal;
	
	$date_to = $this->input->post('date_to', TRUE); 
	$pisah1 = explode("-", $date_to); 
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tglke= $tahun."-".$bulan."-".$tanggal;
	
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	
	$data['query'] = $this->mreport->get_mutasi_unit($date_from, $date_to, $unit_jahit, $tahun, $bulan, $tgldari,$tglke);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['unit_jahit'] = $unit_jahit;
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_jahit'] = $unit_jahit;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	$this->load->view('template',$data);
  }
  
  // 14-11-2015
  // ++++++++++++++++++ mutasi brg unit packing ++++++++++++++++++++++++++++
  function mutasiunitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_packing();
	$data['isi'] = 'wip/vformmutasiunitpacking';
	$this->load->view('template',$data);
  }
  
  function viewmutasiunitpacking(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewmutasiunitpacking';
	$date_from = $this->input->post('date_from', TRUE);
	$pisah1 = explode("-", $date_from);
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tgldari= $tahun."-".$bulan."-".$tanggal;
	
	$date_to = $this->input->post('date_to', TRUE); 
	$pisah1 = explode("-", $date_to); 
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tglke= $tahun."-".$bulan."-".$tanggal;
	$unit_packing = $this->input->post('unit_packing', TRUE);  
	
	$data['query'] = $this->mreport->get_mutasi_unit_packing($date_from, $date_to, $unit_packing, $tahun, $bulan, $tgldari,$tglke);
	$data['jum_total'] = count($data['query']);
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$data['unit_packing'] = $unit_packing;
	
	if ($unit_packing != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_packing'] = $unit_packing;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	$this->load->view('template',$data);
  }
  
  // ++++++++++++++++++ laporan transaksi barang hasil jahit di gudang QC ++++++++++++++++++++++++++++
  function transaksihasiljahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_gudang'] = $this->mreport->get_gudang();
	$data['isi'] = 'wip/vformlaptransaksihasiljahit';
	$this->load->view('template',$data);
  }
  
  function viewtransaksihasiljahit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksihasiljahit';
	$id_gudang = $this->input->post('id_gudang', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	
	$data['query'] = $this->mreport->get_transaksi_hasil_jahit($id_gudang, $bulan, $tahun, $id_brg_wip);
	$data['jum_total'] = count($data['query']);
	
	if ($id_gudang != '0') {
		$query3	= $this->db->query(" SELECT a.nama as nama_lokasi, b.kode_gudang, b.nama as nama_gudang FROM tm_lokasi_gudang a 
				INNER JOIN tm_gudang b ON a.id = b.id_lokasi 
				WHERE b.id = '$id_gudang' ");
		$hasilrow = $query3->row();
		$nama_lokasi	= $hasilrow->nama_lokasi;
		$nama_gudang	= $hasilrow->nama_gudang;
		$kode_gudang	= $hasilrow->kode_gudang;
		$gudangnya = "[".$nama_lokasi."] ".$kode_gudang."-".$nama_gudang;
	}
	else {
		//$gudangnya = "Semua";
		$kode_gudang = "";
		$nama_gudang = "";
	}
	
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
	$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip;
	
	$data['id_gudang'] = $id_gudang;
	$data['nama_lokasi'] = $nama_lokasi;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  function caribrgwip(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil kode, nama
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}
		
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['id_brg_wip'] = $id_brg_wip;
		$this->load->view('wip/vinfobrgwip2', $data); 
		return true;
  }
  
  // 04-12-2015
  // ++++++++++++++++++ laporan transaksi barang unit jahit ++++++++++++++++++++++++++++
  function transaksiunitjahit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_jahit();
	$data['isi'] = 'wip/vformlaptransaksiunitjahit';
	$this->load->view('template',$data);
  }
  
  // 05-12-2015
  function viewtransaksiunitjahit(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
   
	$unit_jahit = $this->input->post('unit_jahit', TRUE);
	//$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	//$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	//$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	
	// 14-01-2016 diganti jadi tanggal
	//$bulan = $this->input->post('bulan', TRUE);
	//$tahun = $this->input->post('tahun', TRUE);
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);
	
	// 12-01-2016
	$jumbrg = $this->input->post('jumbrg', TRUE);  
	$list_id_brg_wip = "";
	for ($i=1; $i<=$jumbrg; $i++) {
		$list_id_brg_wip.= $this->input->post('id_brg_wip_'.$i, TRUE).";";
	}
	
	//$data['query'] = $this->mreport->get_transaksi_unit_jahit($unit_jahit, $bulan, $tahun, $list_id_brg_wip);
	$data['query'] = $this->mreport->get_transaksi_unit_jahit($unit_jahit, $date_from, $date_to, $list_id_brg_wip);
	//print_r($data['query']); die();
	$data['jum_total'] = count($data['query']);
	
	if ($unit_jahit != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_jahit WHERE id = '$unit_jahit' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_jahit'] = $unit_jahit;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
		
	/*if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember"; */
		
	/*$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip; */
	$data['list_id_brg_wip'] = $list_id_brg_wip;
	$data['jumbrg'] = $jumbrg;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	//$data['bulan'] = $bulan;
	//$data['tahun'] = $tahun;
	//$data['nama_bulan'] = $nama_bln;
	 $data['isi'] = 'wip/vviewlaptransaksiunitjahit';
	$this->load->view('template',$data);
  }
  
  // 08-12-2015
  // ++++++++++++++++++ laporan transaksi barang unit packing ++++++++++++++++++++++++++++
  function transaksiunitpacking(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['list_unit'] = $this->mreport->get_unit_packing();
	$data['isi'] = 'wip/vformlaptransaksiunitpacking';
	$this->load->view('template',$data);
  }
  
  function viewtransaksiunitpacking(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksiunitpacking';
	$unit_packing = $this->input->post('unit_packing', TRUE);
	$id_brg_wip = $this->input->post('id_brg_wip', TRUE);  
	$kode_brg_wip = $this->input->post('kode_brg_wip', TRUE);  
	$nama_brg_wip = $this->input->post('nama_brg_wip', TRUE);  
	$bulan = $this->input->post('bulan', TRUE);
	$tahun = $this->input->post('tahun', TRUE);
	
	$data['query'] = $this->mreport->get_transaksi_unit_packing($unit_packing, $bulan, $tahun, $id_brg_wip);
	//print_r($data['query']); die();
	$data['jum_total'] = count($data['query']);
	
	if ($unit_packing != '0') {
		$query3	= $this->db->query(" SELECT kode_unit, nama FROM tm_unit_packing WHERE id = '$unit_packing' ");
		$hasilrow = $query3->row();
		$kode_unit	= $hasilrow->kode_unit;
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$kode_unit = "";
		$nama_unit = "";
	}
		
	$data['unit_packing'] = $unit_packing;
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
		
	if ($bulan == '01')
						$nama_bln = "Januari";
					else if ($bulan == '02')
						$nama_bln = "Februari";
					else if ($bulan == '03')
						$nama_bln = "Maret";
					else if ($bulan == '04')
						$nama_bln = "April";
					else if ($bulan == '05')
						$nama_bln = "Mei";
					else if ($bulan == '06')
						$nama_bln = "Juni";
					else if ($bulan == '07')
						$nama_bln = "Juli";
					else if ($bulan == '08')
						$nama_bln = "Agustus";
					else if ($bulan == '09')
						$nama_bln = "September";
					else if ($bulan == '10')
						$nama_bln = "Oktober";
					else if ($bulan == '11')
						$nama_bln = "November";
					else if ($bulan == '12')
						$nama_bln = "Desember";
		
	$data['id_brg_wip'] = $id_brg_wip;
	$data['kode_brg_wip'] = $kode_brg_wip;
	$data['nama_brg_wip'] = $nama_brg_wip;
	$data['bulan'] = $bulan;
	$data['tahun'] = $tahun;
	$data['nama_bulan'] = $nama_bln;
	$this->load->view('template',$data);
  }
  
  // 12-12-2015
  function export_excel_mutasiunit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_jahit = $this->input->post('unit_jahit', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
	$pisah1 = explode("-", $date_from);
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tgldari= $tahun."-".$bulan."-".$tanggal;
	
	$date_to = $this->input->post('date_to', TRUE); 
	$pisah1 = explode("-", $date_to); 
	$tanggal= $pisah1[0];
	$bulan= $pisah1[1];
	$tahun= $pisah1[2];
	$tglke= $tahun."-".$bulan."-".$tanggal;
		$nama_unit = $this->input->post('nama_unit', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_mutasi_unit($date_from, $date_to, $unit_jahit, $tahun, $bulan, $tgldari,$tglke);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='19' align='center'>LAPORAN MUTASI UNIT JAHIT</th>
				 </tr>
				 <tr>
					<th colspan='19' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr></table><br>";
		if (is_array($query)) {
			for($a=0;$a<count($query);$a++){
				$html_data.= "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b><br>";
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th width='3%' rowspan='2'>No</th>
					 <th width='15%' rowspan='2'>Kode</th>
					 <th width='25%' rowspan='2'>Nama Brg WIP</th>
					 <th width='8%' rowspan='2'>HPP</th>
					<th rowspan='2'>Saldo Awal</th>
					<th colspan='4'>Masuk</th>
					<th colspan='7'>Keluar</th>
					<th rowspan='2'>Saldo Akhir</th>
					<th rowspan='2'>Stok Opname</th>
					<th rowspan='2'>Adjustment</th>
					<th width='3%' rowspan='2'>Selisih</th>
				 </tr>
				 <tr>
					 <th width='3%'>Bgs</th>
					 <th width='3%'>Retur Gdg QC</th>
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
					 <th width='3%'>Bgs Gdg QC</th>
					 <th width='3%'>Bgs Gdg Jadi</th>
					 <th width='3%'>Hsl Perbaikan</th>
					 <th width='3%'>Retur Bhn Baku</th>
					 <th width='3%'> Ke Unit Packing</th>
					
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
				 </tr>
				</thead>
				<tbody>";
				
				$detail_stok = $query[$a]['data_stok'];
				if (is_array($detail_stok)) {
					for($j=0;$j<count($detail_stok);$j++){
						$html_data.=" 
						<tr>
							<td align='center'>".($j+1)."</td>
							<td>".$detail_stok[$j]['kode_brg_wip']."</td>
							<td>".$detail_stok[$j]['nama_brg_wip']."</td>
							<td align='center'>".$detail_stok[$j]['hpp']."</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_awal']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_bgs']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_returbrgjadi']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_lain']."</td>
							<td align='right'>".$detail_stok[$j]['jum_masuk']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_bgs']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_gudangjadi']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_perbaikan']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_retur_bhnbaku']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_unit_packing']."</td>

							<td align='right'>".$detail_stok[$j]['keluar_lain']."</td>
							<td align='right'>".$detail_stok[$j]['jum_keluar']."</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_akhir']."</td>
							<td align='right'>".$detail_stok[$j]['jum_so']."</td>
							<td align='right'>".$detail_stok[$j]['jum_adjustment']."</td>
							
							<td align='right'>".$detail_stok[$j]['selisih']."</td>
						</tr>
						";
					} //endfor2
				} // endif2
/*				$html_data.= "<tr>
						<td colspan='21' align='center'>TOTAL</td>
						<td align='right'>".$query[$a]['total_so_rupiah']."</td>
						<td colspan='4'></td>
						<td>".$query[$a]['total_sisa_stok_rupiah']."</td>
				</tr>"; */
				$html_data.="</tbody></table><br><br>";
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_mutasi_unit_jahit";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  // 15-12-2015
  function export_excel_mutasiunitpacking() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$unit_packing = $this->input->post('unit_packing', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
  		$date_to = $this->input->post('date_to', TRUE);
		$nama_unit = $this->input->post('nama_unit', TRUE);
		$kode_unit = $this->input->post('kode_unit', TRUE);
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE); 
		
		$query = $this->mreport->get_mutasi_unit_packing($date_from, $date_to, $unit_packing);
		
		// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='15' align='center'>LAPORAN MUTASI UNIT PACKING</th>
				 </tr>
				 <tr>
					<th colspan='15' align='center'>Periode: $date_from s.d $date_to</th>
				 </tr></table><br>";
		if (is_array($query)) {
			for($a=0;$a<count($query);$a++){
				$html_data.= "<b>".$query[$a]['kode_unit']." - ".$query[$a]['nama_unit']."</b><br>";
				$html_data.= "
				<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<thead>
				 <tr>
					 <th width='3%' rowspan='2'>No</th>
					 <th width='15%' rowspan='2'>Kode</th>
					 <th width='25%' rowspan='2'>Nama Brg WIP</th>
					 <th width='8%' rowspan='2'>HPP</th>
					<th rowspan='2'>Saldo Awal</th>
					<th colspan='3'>Masuk</th>
					<th colspan='4'>Keluar</th>
					<th rowspan='2'>Saldo Akhir</th>
					<th rowspan='2'>Stok Opname</th>
					<th width='3%' rowspan='2'>Selisih</th>
				 </tr>
				 <tr>
					 <th width='3%'>Bgs</th>
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
					 <th width='3%'>Bgs</th>
					 <th width='3%'>Retur</th>
					 <th width='3%'>Lain2</th>
					 <th width='3%'>Total</th>
				 </tr>
				</thead>
				<tbody>";
				
				$detail_stok = $query[$a]['data_stok'];
				if (is_array($detail_stok)) {
					for($j=0;$j<count($detail_stok);$j++){
						$html_data.=" 
						<tr>
							<td align='center'>".($j+1)."</td>
							<td>".$detail_stok[$j]['kode_brg_wip']."</td>
							<td>".$detail_stok[$j]['nama_brg_wip']."</td>
							<td align='center'>".$detail_stok[$j]['hpp']."</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_awal']."</td>
							<td align='right'>".$detail_stok[$j]['masuk_bgs']."</td>";
							
							$masuk_lain = $detail_stok[$j]['masuk_lain']+$detail_stok[$j]['masuk_other'];
							$html_data.= "
							<td align='right'>".$masuk_lain."</td>
							
							<td align='right'>".$detail_stok[$j]['jum_masuk']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_bgs']."</td>
							<td align='right'>".$detail_stok[$j]['keluar_retur']."</td>";
							
							$keluar_lain = $detail_stok[$j]['keluar_lain']+$detail_stok[$j]['keluar_other'];
							$html_data.= "
							<td align='right'>".$keluar_lain."</td>
							
							<td align='right'>".$detail_stok[$j]['jum_keluar']."</td>
							<td align='right'>".$detail_stok[$j]['jum_saldo_akhir']."</td>
							<td align='right'>".$detail_stok[$j]['jum_so']."</td>
							<td align='right'>".$detail_stok[$j]['selisih']."</td>
						</tr>
						";
					} //endfor2
				} // endif2
/*				$html_data.= "<tr>
						<td colspan='21' align='center'>TOTAL</td>
						<td align='right'>".$query[$a]['total_so_rupiah']."</td>
						<td colspan='4'></td>
						<td>".$query[$a]['total_sisa_stok_rupiah']."</td>
				</tr>"; */
				$html_data.="</tbody></table><br><br>";
			} // end for1
		} // end if1
		
	//--------------------------------------------------------------------------------------------	

		$nama_file = "laporan_mutasi_unit_packing";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;

		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
  
  // 12-01-2016
  function caribrgwip_laptransaksi(){
		$kode_brg_wip 	= $this->input->post('kode_brg_wip', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel tm_barang_wip utk ambil kode, nama
		$queryxx = $this->db->query(" SELECT id, nama_brg FROM tm_barang_wip
									WHERE kode_brg = '".$kode_brg_wip."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$id_brg_wip = $hasilxx->id;
			$nama_brg_wip = $hasilxx->nama_brg;
		}
		else {
			$id_brg_wip = '';
			$nama_brg_wip = '';
		}
		
		$data['nama_brg_wip'] = $nama_brg_wip;
		$data['id_brg_wip'] = $id_brg_wip;
		$data['posisi'] = $posisi;
		$this->load->view('wip/vinfobrgwip2_laptransaksi', $data); 
		return true;
  }
  
  // 13-02-2016 lanjutan desta
  // ++++++++++++++++++ laporan transaksi barang di gudang jadi ++++++++++++++++++++++++++++
  // contek dari lap transaksi unit jahit
  function transaksigudangjadi(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	
	$data['isi'] = 'wip/vformlaptransaksigudangjadi';
	$this->load->view('template',$data);
  }
  
  function viewtransaksigudangjadi(){
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}
	
    $data['isi'] = 'wip/vviewlaptransaksigudangjadi';
	$unit_jahit = $this->input->post('unit_jahit', TRUE);
	
	$date_from = $this->input->post('date_from', TRUE);
	$date_to = $this->input->post('date_to', TRUE);
	
	$jumbrg = $this->input->post('jumbrg', TRUE);  
	$list_id_brg_wip = "";
	for ($i=1; $i<=$jumbrg; $i++) {
		$list_id_brg_wip.= $this->input->post('id_brg_wip_'.$i, TRUE).";";
	}
	
	$data['query'] = $this->mreport->get_transaksi_gudang_jadi($date_from, $date_to, $list_id_brg_wip);
	$data['jum_total'] = count($data['query']);
	
	$data['list_id_brg_wip'] = $list_id_brg_wip;
	$data['jumbrg'] = $jumbrg;
	$data['date_from'] = $date_from;
	$data['date_to'] = $date_to;
	$this->load->view('template',$data);
  }
  
 
  function export_excel_transaksiunitjahit() {
	    $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
		
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);
		$list_id_brg_wip = $this->input->post('list_id_brg_wip', TRUE);
		
		$unit_jahit = $this->input->post('unit_jahit', TRUE);
		$nama_unit = $this->input->post('nama_unit', TRUE);
		
		$export_excel1 = $this->input->post('export_excel', TRUE);  
		$export_ods1 = $this->input->post('export_ods', TRUE);  
		
		$query = $this->mreport->get_transaksi_unit_jahit($unit_jahit, $date_from, $date_to, $list_id_brg_wip);
		
	// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='14' align='center'>LAPORAN KELUAR MASUK BARANG DI UNIT JAHIT ".$nama_unit."</th>
				 </tr>
				 <tr>
					<th colspan='14' align='center'>PERIODE ".$date_from." - ".$date_to."</th>
				 </tr><br>";
				 
				$nomor = 1;
				if (is_array($query)) {
					for($a=0;$a<count($query);$a++){
					$data_warna = $query[$a]['data_warna'];	
					$hitung_warna = count($data_warna);
					$hitung_warna_plus = count($data_warna)+1;
					$hitung_warna_bag = $hitung_warna_plus*9;
$total_masuk_bagus_back=	0;
$data_tabel7 = $query[$a]['data_tabel7'];
					$html_data.= "

<tr>
<th colspan='14' align='left' bgcolor='#d3d3d3'>".$query[$a]['kode_brg_wip']."-".$query[$a]['nama_brg_wip']."</th>
<th></th>
</tr>
	



			 <tr>
				 <th>Tgl</th>
				 <th>Ket</th>
				 <th>No Bukti</th>
				
				 <th colspan=".$hitung_warna_plus.">Masuk Bagus</th>
				 <th colspan=".$hitung_warna_plus.">Masuk<br>Retur Perbaikan</th>
				 <th colspan=".$hitung_warna_plus.">Masuk Lain</th>
				 <th colspan=".$hitung_warna_plus.">Keluar Bagus<br>QC</th>
				 <th colspan=".$hitung_warna_plus.">Keluar Bagus<br>Gdg Jadi</th>
				 <th colspan=".$hitung_warna_plus.">Keluar<br>Hasil Perbaikan</th>
				 <th colspan=".$hitung_warna_plus.">Keluar Lain</th>
				 <th colspan=".$hitung_warna_plus.">Keluar<br>Retur Bhn Baku</th>
				 <th colspan=".$hitung_warna_plus.">Keluar<br>ke Unit Packing</th>
				 <th>Saldo<br>Per Warna</th>
				 <th>Saldo Global</th>
			 </tr>
			 <tr>
			 <th></th>
				 <th></th>
				  <th></th>";



if($total_masuk_bagus_back == 0   ){
echo "<tr class=\"record\"  bgcolor='#ff3333'>";
}
else
echo "<tr class=\"record\">";



	  
		 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}
		 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}
		 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}
			 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}
			 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}	
			 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}	
			 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}	
			 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}	
			 if (is_array($data_warna)) {
					for($z=0;$z<$hitung_warna;$z++){	
					$html_data .= "
					<th>".$data_warna[$z]['nama_warna']."</th>";	
					}
						$html_data .= "
					<th>Total</th>";	
				}										
		 $html_data.= "</tr>";
	 $html_data.= "
	</thead>
	<tbody>
		<tr>
			<td>Saldo Awal</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			";
			for($z=0;$z<$hitung_warna_bag;$z++){	
				$html_data.= "<td>&nbsp;</td>";
			}
			
			 $html_data.= "
			<td>"; 
			$data_so_warna = $query[$a]['data_so_warna'];
			if (is_array($data_so_warna)) {
				for($zz=0;$zz<count($data_so_warna);$zz++){ 
					$html_data.=" &nbsp;".$data_so_warna[$zz]['nama_warna'].": ".$data_so_warna[$zz]['saldo']."<br>";
				}
			}
		
		$html_data.="	</td>";
		
		$html_data.="<td align='right'>";
			
			
		$html_data .= $query[$a]['tot_jum_stok_opname'];
		$html_data.="
			</td>
		</tr>";
			

		$data_tabel1 = $query[$a]['data_tabel1'];
		if (is_array($data_tabel1)) {
			for($j=0;$j<count($data_tabel1);$j++){
$html_data.="
	<tr>
		<td style='white-space:nowrap;'>";
	 $html_data .= "&nbsp;".$data_tabel1[$j]['tgl_sj'] ."</td>
		<td style='white-space:nowrap;'>";
		 if ($data_tabel1[$j]['masuk'] == "ya") { 
			if ($data_tabel1[$j]['masuk_bgs'] == '1' || $data_tabel1[$j]['masuk_retur_perb'] == '1' || $data_tabel1[$j]['masuk_lain'] == '1' || $data_tabel1[$j]['masuk_lain2'] == '1' || $data_tabel1[$j]['masuk_lain3'] == '1' || $data_tabel1[$j]['masuk_pengembalian_retur'] == '1' || $data_tabel1[$j]['masuk_other'] == '1'){
				$html_data .= "Masuk"; 
			}
		}
		else {
			$html_data .= "Keluar"; 
		}	
		$html_data .="</td>";
		 $html_data .="<td>";
		 $html_data.=" ".$data_tabel1[$j]['no_sj']." </td>";
		
		  if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_bgs'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						$html_data.=" <td>";
						$html_data.= "".$data_tabel1_perwarna[$z]['qty']."<br>";
						$html_data.=" </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
				
			}
	else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
		   if ($data_tabel1[$j]['masuk'] == "ya" && $data_tabel1[$j]['masuk_retur_perb'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						$html_data.=" <td>";
						$html_data.= $data_tabel1_perwarna[$z]['qty'];
						$html_data.= " </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
				
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
		   if ($data_tabel1[$j]['masuk'] == "ya" && ($data_tabel1[$j]['masuk_lain'] == '1'|| $data_tabel1[$j]['masuk_lain2'] == '1' || $data_tabel1[$j]['masuk_lain3'] == '1'|| $data_tabel1[$j]['masuk_other'] == '1')) {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						 $html_data.=" <td>";
						$html_data.= $data_tabel1_perwarna[$z]['qty'];
						$html_data.= " </td>";
					$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";	
				}	
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
			
			 if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_bgs'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						 $html_data.=" <td>";
						$html_data.= $data_tabel1_perwarna[$z]['qty'];
						$html_data.= " </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
				
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
			
			 if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_bgs_gdgjadi'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						 $html_data.=" <td>";
						$html_data.= "&nbsp;".$data_tabel1_perwarna[$z]['nama_warna'].": ".$data_tabel1_perwarna[$z]['qty']."<br>";
						$html_data.= " </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
				
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
			 if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_hasil_perb'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						 $html_data.=" <td>";
						$html_data.= $data_tabel1_perwarna[$z]['qty'];
						$html_data.= " </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
				
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
			 if ($data_tabel1[$j]['keluar'] == "ya" && ($data_tabel1[$j]['keluar_lain'] == '1' || $data_tabel1[$j]['keluar_lain2'] == '1' || $data_tabel1[$j]['keluar_other'] == '1')) {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						 $html_data.=" <td>";
						$html_data.= $data_tabel1_perwarna[$z]['qty'];
						$html_data.= " </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
			 if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_retur_bhnbaku'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						 $html_data.=" <td>";
						$html_data.= $data_tabel1_perwarna[$z]['qty'];
						$html_data.= " </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
			 if ($data_tabel1[$j]['keluar'] == "ya" && $data_tabel1[$j]['keluar_unit_packing'] == '1') {
				$data_tabel1_perwarna = $data_tabel1[$j]['data_tabel1_perwarna'];
				$totglobal = 0;
				if (is_array($data_tabel1_perwarna)) {
					for($z=0;$z<count($data_tabel1_perwarna);$z++){
						 $html_data.=" <td>";
						$html_data.= $data_tabel1_perwarna[$z]['qty'];
						$html_data.= " </td>";
						$totglobal+=$data_tabel1_perwarna[$z]['qty'];
					}
					$html_data.=" <td>".$totglobal."</td>";
				}
				
			}
			else{
		$html_data.="
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>";
		}
		
		$html_data.="
		<td>";
				$data_tabel1_saldo_perwarna = $data_tabel1[$j]['data_tabel1_saldo_perwarna'];
				if (is_array($data_tabel1_saldo_perwarna)) {
					$totglobal = $query[$a]['tot_jum_stok_opname'];
					$data_so_warna = $query[$a]['data_so_warna'];
				
					for($z=0;$z<count($data_tabel1_saldo_perwarna);$z++){
						$data_so_warnasaldo=$data_so_warna[$z]['saldo'];
				$html_data .= "<br>".$data_tabel1_saldo_perwarna[$z]['nama_warna'].": ".$data_so_warnasaldo+=$data_tabel1_saldo_perwarna[$z]['saldo']."<br>";
					$totglobal+=$data_tabel1_saldo_perwarna[$z]['saldo'];
					}
					$html_data .= "<br>Total: ".$totglobal;
					$html_data.="
		</td>";
				}
				
		$html_data.= "
		 <td align='right'>";
				$html_data.="". $data_tabel1[$j]['tot_saldo']."		
		 </td>";
		}
	}
}
		$nama_file = "laporan_transaksiunitjahit";
		if ($export_excel1 != '')
			$nama_file.= ".xls";
		else
			$nama_file.= ".ods";
		$data = $html_data;
		$dir=getcwd();
		include($dir."/application/libraries/generateExcelFile.php");
		return true;
  }
}
}
