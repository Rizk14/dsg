<?php
class Cform extends CI_Controller{
  function __construct (){
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('wip/mmaster');
  }
	
	// 14-03-2013, SJ masuk WIP
  function addsjmasuk(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$th_now	= date("Y");

	$data['isi'] = 'wip/vmainformsjmasuk';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$this->load->view('template',$data);

  }
  
  function editsjmasuk(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_sj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$kode_unit_jahit 	= $this->uri->segment(9);
	$kode_unit_packing 	= $this->uri->segment(10);
	$gudang 	= $this->uri->segment(11);
	$carinya 	= $this->uri->segment(12);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$data['gudang'] = $gudang;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_sjmasuk($id_sj); 
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['msg'] = '';
	$data['isi'] = 'wip/veditformsjmasuk';
	$this->load->view('template',$data);

  }
  
  function updatedatasjmasuk() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_sj 	= $this->input->post('id_sj', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis_masuk', TRUE);  
			$jenis_lama 	= $this->input->post('jenis_masuk_lama', TRUE);  
			
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_jahit_lama 	= $this->input->post('kode_unit_jahit_lama', TRUE);  
			
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);
			$id_gudang_lama 	= $this->input->post('id_gudang_lama', TRUE);  
			
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$unit_jahit = $this->input->post('unit_jahit', TRUE);
			$unit_packing = $this->input->post('unit_packing', TRUE);
			$gudang = $this->input->post('gudang', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");
			if ($id_gudang != $id_gudang_lama)
				$update_gudang = " , id_gudang = '$id_gudang' ";
			else
				$update_gudang = " ";
			
			// 01-03-2014, perlu dipikirkan utk pengecekan no SJ, apakah udh ada di database dgn nomor dan thn yg sama
			// sementara ditunda dulu, ga usah pake pengecekan no SJ
			// =========================================================================
			//update headernya
			$this->db->query(" UPDATE tm_sjmasukwip SET tgl_sj = '$tgl_sj', jenis_masuk = '$jenis', tgl_update='$tgl',
							no_sj='".$this->db->escape_str($no_sj)."', kode_unit_jahit = '$kode_unit_jahit', 
							kode_unit_packing = '$kode_unit_packing', 
							keterangan = '$ket' ".$update_gudang." where id= '$id_sj' ");
							
				//reset stok, dan hapus dulu detailnya
				//============= 25-01-2013 ====================
				$query2	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail WHERE id_sjmasukwip = '$id_sj' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok PABRIK =====================
						//$nama_tabel_stok = "tm_stok_hasil_jahit";
				
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit
										WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$id_gudang_lama' ");
						if ($query3->num_rows() == 0){
							$id_stok = 0;
							$stok_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama-$row2->qty; // berkurang stok karena reset dari bon M masuk lain
								
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$row2->kode_brg_jadi' AND id_gudang = '$id_gudang_lama' ");
						
						//================ update stok UNIT JAHIT jika kode_unit_jahit != 0 ===============================
						if ($kode_unit_jahit_lama != '0') {
							//25-03-2014
							if ($jenis_lama == '1')
								$field_stok = "stok_bagus";
							else if ($jenis_lama == '2')
								$field_stok = "stok_perbaikan";
							
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, ".$field_stok." FROM tm_stok_unit_jahit
											WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$kode_unit_jahit_lama' ");
							if ($query3->num_rows() == 0){
								$id_stok_unit = 0;
								$stok_unit_lama = 0;
								$stok_unitxx_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								
								if ($jenis_lama == '1')
									$stok_unitxx_lama	= $hasilrow->stok_bagus;
								else if ($jenis_lama == '2')
									$stok_unitxx_lama	= $hasilrow->stok_perbaikan;
							}
							$new_stok_unit = $stok_unit_lama+$row2->qty; // bertambah stok unit karena reset dari SJ masuk
							$new_stok_unitxx = $stok_unitxx_lama+$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										".$field_stok." = '$new_stok_unitxx', tgl_update_stok = '$tgl' 
										WHERE kode_brg_jadi= '$row2->kode_brg_jadi' AND kode_unit = '$kode_unit_jahit_lama' ");
						} // end if
						
						// 2. reset stok per warna dari tabel tm_sjmasukwip_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_sjmasukwip_detail_warna 
												WHERE id_sjmasukwip_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna
											 WHERE id_stok_hasil_jahit = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama-$rowwarna->qty; // berkurang stok karena reset dari SJ masuk
										
								$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_jahit= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ============= update stok unit jahit jika kode_unit_jahit != 0
								if ($kode_unit_jahit_lama != '0') {
									//25-03-2014
									if ($jenis_lama == '1')
										$field_stok = "stok_bagus";
									else if ($jenis_lama == '2')
										$field_stok = "stok_perbaikan";
								
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, ".$field_stok." FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warnaxx_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										
										if ($jenis_lama == '1')
											$stok_unit_warnaxx_lama	= $hasilrow->stok_bagus;
										else if ($jenis_lama == '2')
											$stok_unit_warnaxx_lama	= $hasilrow->stok_perbaikan;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama+$rowwarna->qty; // bertambah stok ke unit karena reset dari SJ masuk
									$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama+$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												".$field_stok." = '$new_stok_unit_warnaxx', 
												tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
								} // end if kode_unit_jahit != 0
							}
						} // end if detail warna
						
						$this->db->query(" DELETE FROM tm_sjmasukwip_detail_warna WHERE id_sjmasukwip_detail = '$row2->id' ");
						
						// ==============================================
					} // end foreach detail
				} // end reset stok
				//=============================================
				$this->db->delete('tm_sjmasukwip_detail', array('id_sjmasukwip' => $id_sj));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$kode = $this->input->post('kode_'.$i, TRUE);
						//$qty = $this->input->post('qty_'.$i, TRUE);
						//$ket_warna = $this->input->post('ket_warna_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						$temp_qty = $this->input->post('temp_qty_'.$i, TRUE);
						$kode_warna = $this->input->post('kode_warna_'.$i, TRUE);
						$qty_warna = $this->input->post('qty_warna_'.$i, TRUE);
						
						// 30-01-2014, insert ke tabel detail ----------------------------------
						//-------------- hitung total qty dari detail tiap2 warna -------------------
							$qtytotal = 0;
							for ($xx=0; $xx<count($kode_warna); $xx++) {
								$kode_warna[$xx] = trim($kode_warna[$xx]);
								$qty_warna[$xx] = trim($qty_warna[$xx]);
																
								$qtytotal+= $qty_warna[$xx];
							} // end for
						// ---------------------------------------------------------------------
						
						// ======== update stoknya! =============
						//$nama_tabel_stok = "tm_stok_hasil_jahit";
						// 12-09-2013, KARENA LOKASI GUDANG BISA DIEDIT MAKA CEK STOK BERDASARKAN GUDANG BARU ($id_gudang, bukan $id_gudang_lama)
						// 1. stok total
						// ============================= update stok PABRIK ======================================
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$kode'
												AND id_gudang='$id_gudang' ");
							if ($query3->num_rows() == 0){
								$id_stok = 0;
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok	= $hasilrow->id;
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama+$qtytotal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode,
									'stok'=>$new_stok,
									'id_gudang'=>$id_gudang,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_jahit', $data_stok);
								
								// ambil id_stok utk dipake di stok warna
								$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
								if($sqlxx->num_rows() > 0) {
									$hasilxx	= $sqlxx->row();
									$id_stok	= $hasilxx->id;
								}else{
									$id_stok	= 1;
								}
						
							}
							else {
								$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$kode' AND id_gudang = '$id_gudang' ");
							}
					
					// ============= update stok UNIT JAHIT jika kode_unit_jahit != 0 ======================
					if ($kode_unit_jahit != '0') {
						//25-03-2014
						if ($jenis == '1')
							$field_stok = "stok_bagus";
						else if ($jenis == '2')
							$field_stok = "stok_perbaikan";
						
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, ".$field_stok." FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode'
											AND kode_unit='$kode_unit_jahit' ");
						if ($query3->num_rows() == 0){
							$id_stok_unit = 0;
							$stok_unit_lama = 0;
							$stok_unitxx_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							
							if ($jenis == '1')
								$stok_unitxx_lama	= $hasilrow->stok_bagus;
							else if ($jenis == '2')
								$stok_unitxx_lama	= $hasilrow->stok_perbaikan;
						}
						$new_stok_unit = $stok_unit_lama-$qtytotal;
						$new_stok_unitxx = $stok_unitxx_lama-$qtytotal;
							
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'kode_brg_jadi'=>$kode,
								'stok'=>$new_stok_unit,
								$field_stok=>$new_stok_unitxx,
								'kode_unit'=>$kode_unit_jahit,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
								".$field_stok." = '$new_stok_unitxx', tgl_update_stok = '$tgl' 
								where kode_brg_jadi= '$kode' AND kode_unit = '$kode_unit_jahit' ");
						}
					} // end if kode_unit_jahit != 0
					// =====================================================================================
					
					// jika semua data tdk kosong, insert ke tm_sjmasukwip_detail
					$data_detail = array(
						'id_sjmasukwip'=>$id_sj,
						'kode_brg_jadi'=>$kode,
						'qty'=>$qtytotal,
						'keterangan'=>$ket_detail
						//'ket_qty_warna'=>$ket_warna,
					);
					$this->db->insert('tm_sjmasukwip_detail',$data_detail);
							// ================ end insert item detail ===========
					
					// ambil id detail sjmasukwip_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
				
					// ----------------------------------------------
					for ($xx=0; $xx<count($kode_warna); $xx++) {
						$kode_warna[$xx] = trim($kode_warna[$xx]);
						$qty_warna[$xx] = trim($qty_warna[$xx]);
								
						$seq_warna	= $this->db->query(" SELECT id FROM tm_sjmasukwip_detail_warna ORDER BY id DESC LIMIT 1 ");
							
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}

						$tm_sjmasukwip_detail_warna	= array(
							 'id'=>$idbaru,
							 'id_sjmasukwip_detail'=>$iddetail,
							 'id_warna_brg_jadi'=>'0',
							 'kode_warna'=>$kode_warna[$xx],
							 'qty'=>$qty_warna[$xx]
						);
						$this->db->insert('tm_sjmasukwip_detail_warna',$tm_sjmasukwip_detail_warna);
						
						// ========================= 03-02-2014, stok per warna ===============================================
						//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
													AND id_stok_hasil_jahit='$id_stok' ");
						if ($query3->num_rows() == 0){
							$stok_warna_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_jahit'=>$id_stok,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_warna,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_jahit='$id_stok' ");
						}
						
						// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit jika kode_unit_jahit != 0
					if ($kode_unit_jahit != '0') {
						//25-03-2014
						if ($jenis == '1')
							$field_stok = "stok_bagus";
						else if ($jenis == '2')
							$field_stok = "stok_perbaikan";
						
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, ".$field_stok." FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warnaxx_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							
							if ($jenis == '1')
								$stok_unit_warnaxx_lama = $hasilrow->stok_bagus;
							else if ($jenis == '2')
								$stok_unit_warnaxx_lama = $hasilrow->stok_perbaikan;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama-$qty_warna[$xx]; // berkurang stok karena keluar dari unit
						$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama-$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								$field_stok=>$new_stok_unit_warnaxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							".$field_stok." = '$new_stok_unit_warnaxx', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
					} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
						
					} // end for
					// ----------------------------------------------
				
				} // end perulangan

			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "wip/cform/viewsjmasuk/".$cur_page;
			else
				$url_redirectnya = "wip/cform/carisjmasuk/".$tgl_awal."/".$tgl_akhir."/".$unit_jahit."/".$unit_packing."/".$gudang."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
				
  }

  function submitsjmasuk(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$jenis 	= $this->input->post('jenis_masuk', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			if ($kode_unit_jahit != 0) {
				$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$kode_unit_jahit' ");
				$hasilrow = $query3->row();
				if ($query3->num_rows() != 0) 
					$nama_unit	= $hasilrow->nama;
				else
					$nama_unit = '';
			}
			
			//$cek_data = $this->mmaster->cek_data_sjmasukwip($no_sj, $thn1, $kode_unit_jahit);
			$cek_data = $this->mmaster->cek_data_sjmasukwip($no_sj, $thn1, $kode_unit_jahit, $kode_unit_packing, $jenis);
			if (count($cek_data) > 0) {
				$data['isi'] = 'wip/vmainformsjmasuk';
				if ($nama_unit != '')
					$str = " di unit jahit ".$nama_unit;
				else
					$str = "";
				$data['msg'] = "Data no SJ masuk ".$no_sj.$str." utk tahun $thn1 sudah ada..!";
				$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
				$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$this->load->view('template',$data);
			}
			else {
				// 05-03-2014
				// insert di tm_sjmasukwip
				$tgl = date("Y-m-d H:i:s");
				$data_header = array(
				  'no_sj'=>$no_sj,
				  'tgl_sj'=>$tgl_sj,
				  'jenis_masuk'=>$jenis,
				  'id_gudang'=>$id_gudang,
				  'kode_unit_jahit'=>$kode_unit_jahit,
				  'kode_unit_packing'=>$kode_unit_packing,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'keterangan'=>$ket
				);
				$this->db->insert('tm_sjmasukwip',$data_header);
				
				// ambil data terakhir di tabel tm_sjmasukwip
				$query2	= $this->db->query(" SELECT id FROM tm_sjmasukwip ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_sj	= $hasilrow->id; 
				//-----------
				
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					// new 05-03-2014
					$this->mmaster->savesjmasuk($id_sj, $id_gudang, $kode_unit_jahit, $kode_unit_packing, $jenis,
								$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								//$this->input->post('ket_warna_'.$i, TRUE),  
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) );
					/*$this->mmaster->savesjmasuk($no_sj,$tgl_sj, $id_gudang, $jenis, $kode_unit_jahit, $kode_unit_packing, $ket, 
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								//$this->input->post('ket_warna_'.$i, TRUE),  
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) ); */
				}
				redirect('wip/cform/viewsjmasuk');
			}
  }
  
  function viewsjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'wip/vformviewsjmasuk';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	$kode_unit_jahit = '0';
	$kode_unit_packing = '0';
	$gudang = '0';
	$caribrg = "all";
	$filterbrg = "n";
	
    $jum_total = $this->mmaster->getAllsjmasuktanpalimit($keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/wip/cform/viewsjmasuk/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllsjmasuk($config['per_page'],$this->uri->segment(4), $keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['gudang'] = $gudang;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$this->load->view('template',$data);
  }
  
  function carisjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
	$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);
	$gudang 	= $this->input->post('gudang', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($kode_unit_jahit == '')
		$kode_unit_jahit = $this->uri->segment(6);
	if ($kode_unit_packing == '')
		$kode_unit_packing = $this->uri->segment(7);
	if ($gudang == '')
		$gudang = $this->uri->segment(8);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(9);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(10);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(11);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($caribrg == '')
		$caribrg = "all";
	if ($filterbrg == '')
		$filterbrg = "n";
	
	$jum_total = $this->mmaster->getAllsjmasuktanpalimit($keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/wip/cform/carisjmasuk/'.$date_from.'/'.$date_to.'/'.$kode_unit_jahit.'/'.$kode_unit_packing.'/'.$gudang.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(12);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsjmasuk($config['per_page'],$this->uri->segment(12), $keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'wip/vformviewsjmasuk';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['gudang'] = $gudang;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);
  }

  function show_popup_brgjadi(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$gudang 	= $this->input->post('id_gudang', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '' && $gudang == '') {
			$gudang 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
	// +++++++++++++++

		$qjum_total = $this->mmaster->get_brgjaditanpalimit($gudang, $keywordcari);
		
				$config['base_url'] = base_url()."index.php/wip/cform/show_popup_brgjadi/".$gudang."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi($config['per_page'],$config['cur_page'], $gudang, $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE id = '$gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$nama_gudang = '';
	}
	$data['gudang'] = $gudang;
	$data['nama_gudang'] = $nama_gudang;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('wip/vpopupbrgjadi',$data);
  }
  
  function deletesjmasuk(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$unit_jahit 	= $this->uri->segment(9);
	$unit_packing 	= $this->uri->segment(10);
	$gudang 	= $this->uri->segment(11);
	$carinya 	= $this->uri->segment(12);
    
    $this->mmaster->deletesjmasuk($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "wip/cform/viewsjmasuk/".$cur_page;
	else
		$url_redirectnya = "wip/cform/carisjmasuk/".$tgl_awal."/".$tgl_akhir."/".$unit_jahit."/".$unit_packing."/".$gudang."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  
  // +++++++++++++++++ 14-03-2013, SJ keluar WIP +++++++++++++++++
  function addsjkeluar(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}

	$th_now	= date("Y");
	$query3	= $this->db->query(" SELECT no_sj FROM tm_sjkeluarwip ORDER BY no_sj DESC LIMIT 1 ");
	$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_sj	= $hasilrow->no_sj;
			else
				$no_sj = '';
			if(strlen($no_sj)==9) {
				$nosj = substr($no_sj, 0, 9);
				$n_sj	= (substr($nosj,4,5))+1;
				$th_sj	= substr($nosj,0,4);
				if($th_now==$th_sj) {
						$jml_n_sj	= $n_sj;
						switch(strlen($jml_n_sj)) {
							case "1": $kodesj	= "0000".$jml_n_sj;
							break;
							case "2": $kodesj	= "000".$jml_n_sj;
							break;	
							case "3": $kodesj	= "00".$jml_n_sj;
							break;
							case "4": $kodesj	= "0".$jml_n_sj;
							break;
							case "5": $kodesj	= $jml_n_sj;
							break;	
						}
						$nomorsj = $th_now.$kodesj;
				}
				else {
					$nomorsj = $th_now."00001";
				}
			}
			else {
				$nomorsj	= $th_now."00001";
			}

	$data['isi'] = 'wip/vmainformsjkeluar';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['no_sj'] = $nomorsj;
	$this->load->view('template',$data);

  }
  
  function editsjkeluar(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_sj 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$kode_unit_jahit 	= $this->uri->segment(9);
	$kode_unit_packing 	= $this->uri->segment(10);
	$gudang 	= $this->uri->segment(11);
	$carinya 	= $this->uri->segment(12);
	
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = $is_cari;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$data['gudang'] = $gudang;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_sjkeluar($id_sj); 
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	$data['msg'] = '';
	$data['isi'] = 'wip/veditformsjkeluar';
	$this->load->view('template',$data);

  }
  
  function updatedatasjkeluar() {
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
			$id_sj 	= $this->input->post('id_sj', TRUE);
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$jenis 	= $this->input->post('jenis_keluar', TRUE);  
			
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_jahit_lama 	= $this->input->post('kode_unit_jahit_lama', TRUE);  
			
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$id_gudang_lama 	= $this->input->post('id_gudang_lama', TRUE);  
			
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
			$no 	= $this->input->post('no', TRUE);
			
			$cur_page = $this->input->post('cur_page', TRUE);
			$is_cari = $this->input->post('is_cari', TRUE);
			$tgl_awal = $this->input->post('tgl_awal', TRUE);
			$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
			$unit_jahit = $this->input->post('unit_jahit', TRUE);
			$unit_packing = $this->input->post('unit_packing', TRUE);
			$gudang = $this->input->post('gudang', TRUE);
			$carinya = $this->input->post('carinya', TRUE);
						
			$tgl = date("Y-m-d H:i:s");
			
			if ($id_gudang != $id_gudang_lama)
				$update_gudang = " , id_gudang = '$id_gudang' ";
			else
				$update_gudang = " ";

			//update headernya
			$this->db->query(" UPDATE tm_sjkeluarwip SET tgl_sj = '$tgl_sj', jenis_keluar = '$jenis', tgl_update='$tgl',
							no_sj='".$this->db->escape_str($no_sj)."', kode_unit_jahit = '$kode_unit_jahit', 
							kode_unit_packing = '$kode_unit_packing', 
							keterangan = '$ket' ".$update_gudang." where id= '$id_sj' ");
							
				//reset stok, dan hapus dulu detailnya
				//============= 25-01-2013 ====================
				$query2	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail WHERE id_sjkeluarwip = '$id_sj' ");
				if ($query2->num_rows() > 0){
					$hasil2=$query2->result();
					//$tgl = date("Y-m-d");
					foreach ($hasil2 as $row2) {
						// 1. stok total
						// ============ update stok PABRIK =====================
						//$nama_tabel_stok = "tm_stok_hasil_jahit";
				
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit
										WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND id_gudang='$id_gudang_lama' ");
						if ($query3->num_rows() == 0){
							$stok_lama = 0;
							$id_stok = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok	= $hasilrow->id;
							$stok_lama	= $hasilrow->stok;
						}
						$new_stok = $stok_lama+$row2->qty; // bertambah stok karena reset
								
						$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$row2->kode_brg_jadi' AND id_gudang = '$id_gudang_lama' ");
						
						//================ update stok UNIT JAHIT jika kode_unit_jahit != 0 ===============================
						if ($kode_unit_jahit_lama != '0') {
							// 25-03-2014, ambil stok_perbaikan
							//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok, stok_perbaikan FROM tm_stok_unit_jahit
											WHERE kode_brg_jadi = '$row2->kode_brg_jadi' AND kode_unit='$kode_unit_jahit_lama' ");
							if ($query3->num_rows() == 0){
								$id_stok_unit = 0;
								$stok_unit_lama = 0;
								$stok_unitxx_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok_unit	= $hasilrow->id;
								$stok_unit_lama	= $hasilrow->stok;
								$stok_unitxx_lama = $hasilrow->stok_perbaikan;
							}
							$new_stok_unit = $stok_unit_lama-$row2->qty; // berkurang stok unit karena reset dari SJ keluar
							$new_stok_unitxx = $stok_unitxx_lama-$row2->qty;
									
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
										stok_perbaikan = '$new_stok_unitxx', tgl_update_stok = '$tgl' 
										WHERE kode_brg_jadi= '$row2->kode_brg_jadi' AND kode_unit = '$kode_unit_jahit_lama' ");
						} // end if
						
						// 2. reset stok per warna dari tabel tm_sjmasukwip_detail_warna
						$querywarna	= $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail_warna 
												WHERE id_sjkeluarwip_detail = '$row2->id' ");
						if ($querywarna->num_rows() > 0){
							$hasilwarna=$querywarna->result();
												
							foreach ($hasilwarna as $rowwarna) {
								//============== update stok pabrik ===============================================
								//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
								$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna
											 WHERE id_stok_hasil_jahit = '$id_stok' 
											 AND kode_warna='$rowwarna->kode_warna' ");
								if ($query3->num_rows() == 0){
									$stok_warna_lama = 0;
								}
								else {
									$hasilrow = $query3->row();
									$stok_warna_lama	= $hasilrow->stok;
								}
								$new_stok_warna = $stok_warna_lama+$rowwarna->qty; // bertambah stok karena reset dari SJ keluar
										
								$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', 
											tgl_update_stok = '$tgl' WHERE id_stok_hasil_jahit= '$id_stok'
											AND kode_warna = '$rowwarna->kode_warna' ");
								
								// ============= update stok unit jahit jika kode_unit_jahit != 0
								if ($kode_unit_jahit_lama != '0') {
									// 25-03-2014, tambahin stok_perbaikan
									//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
									$query3	= $this->db->query(" SELECT stok, stok_perbaikan FROM tm_stok_unit_jahit_warna
												 WHERE id_stok_unit_jahit = '$id_stok_unit' 
												 AND kode_warna='$rowwarna->kode_warna' ");
									if ($query3->num_rows() == 0){
										$stok_unit_warna_lama = 0;
										$stok_unit_warnaxx_lama = 0;
									}
									else {
										$hasilrow = $query3->row();
										$stok_unit_warna_lama	= $hasilrow->stok;
										$stok_unit_warnaxx_lama = $hasilrow->stok_perbaikan;
									}
									$new_stok_unit_warna = $stok_unit_warna_lama-$rowwarna->qty; // berkurang stok di unit karena reset dari SJ keluar
									$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama-$rowwarna->qty;
											
									$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
												stok_perbaikan = '$new_stok_unit_warnaxx', tgl_update_stok = '$tgl' WHERE id_stok_unit_jahit= '$id_stok_unit'
												AND kode_warna = '$rowwarna->kode_warna' ");
								} // end if kode_unit_jahit != 0
							}
						} // end if detail warna
						
						$this->db->query(" DELETE FROM tm_sjkeluarwip_detail_warna WHERE id_sjkeluarwip_detail = '$row2->id' ");
						
						// ==============================================
					} // end foreach detail
				} // end reset stok
				//=============================================
				$this->db->delete('tm_sjkeluarwip_detail', array('id_sjkeluarwip' => $id_sj));
				
					$jumlah_input=$no-1; 
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						$kode = $this->input->post('kode_'.$i, TRUE);
						//$qty = $this->input->post('qty_'.$i, TRUE);
						//$ket_warna = $this->input->post('ket_warna_'.$i, TRUE);
						$ket_detail = $this->input->post('ket_detail_'.$i, TRUE);
						
						$temp_qty = $this->input->post('temp_qty_'.$i, TRUE);
						$kode_warna = $this->input->post('kode_warna_'.$i, TRUE);
						$qty_warna = $this->input->post('qty_warna_'.$i, TRUE);
						
						// 30-01-2014, insert ke tabel detail ----------------------------------
						//-------------- hitung total qty dari detail tiap2 warna -------------------
							$qtytotal = 0;
							for ($xx=0; $xx<count($kode_warna); $xx++) {
								$kode_warna[$xx] = trim($kode_warna[$xx]);
								$qty_warna[$xx] = trim($qty_warna[$xx]);
																
								$qtytotal+= $qty_warna[$xx];
							} // end for
						// ---------------------------------------------------------------------
						
						// ======== update stoknya! =============
						//$nama_tabel_stok = "tm_stok_hasil_jahit";
						
						// 12-09-2013, pake id_gudang yg baru
						// 1. stok total
						// ============================= update stok PABRIK ======================================
						//cek stok terakhir tm_stok_hasil_jahit, dan update stoknya
							$query3	= $this->db->query(" SELECT id, stok FROM tm_stok_hasil_jahit WHERE kode_brg_jadi = '$kode'
												AND id_gudang='$id_gudang' ");
							if ($query3->num_rows() == 0){
								$id_stok = 0;
								$stok_lama = 0;
							}
							else {
								$hasilrow = $query3->row();
								$id_stok	= $hasilrow->id;
								$stok_lama	= $hasilrow->stok;
							}
							$new_stok = $stok_lama-$qtytotal;
							
							if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit, insert
								$data_stok = array(
									'kode_brg_jadi'=>$kode,
									'stok'=>$new_stok,
									'id_gudang'=>$id_gudang,
									'tgl_update_stok'=>$tgl
									);
								$this->db->insert('tm_stok_hasil_jahit', $data_stok);
								
								// ambil id_stok utk dipake di stok warna
								$sqlxx	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit ORDER BY id DESC LIMIT 1 ");
								if($sqlxx->num_rows() > 0) {
									$hasilxx	= $sqlxx->row();
									$id_stok	= $hasilxx->id;
								}else{
									$id_stok	= 1;
								}
							}
							else {
								$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$new_stok', tgl_update_stok = '$tgl' 
									where kode_brg_jadi= '$kode' AND id_gudang = '$id_gudang' ");
							}
					
					// ============= update stok UNIT JAHIT jika kode_unit_jahit != 0 ======================
					if ($kode_unit_jahit != '0') {
						// 25-03-2014 tambahin stok_perbaikan
						//cek stok terakhir tm_stok_unit_jahit, dan update stoknya
						$query3	= $this->db->query(" SELECT id, stok, stok_perbaikan FROM tm_stok_unit_jahit WHERE kode_brg_jadi = '$kode'
											AND kode_unit='$kode_unit_jahit' ");
						if ($query3->num_rows() == 0){
							$id_stok_unit = 0;
							$stok_unit_lama = 0;
							$stok_unitxx_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$id_stok_unit	= $hasilrow->id;
							$stok_unit_lama	= $hasilrow->stok;
							$stok_unitxx_lama = $hasilrow->stok_perbaikan;
						}
						$new_stok_unit = $stok_unit_lama+$qtytotal;
						$new_stok_unitxx = $stok_unitxx_lama+$qtytotal;
							
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit, insert
							$seqxx	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit ORDER BY id DESC LIMIT 1 ");
							if($seqxx->num_rows() > 0) {
								$seqrowxx	= $seqxx->row();
								$id_stok_unit	= $seqrowxx->id+1;
							}else{
								$id_stok_unit	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit,
								'kode_brg_jadi'=>$kode,
								'stok'=>$new_stok_unit,
								'stok_perbaikan'=>$new_stok_unitxx,
								'kode_unit'=>$kode_unit_jahit,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$new_stok_unit', 
								stok_perbaikan = '$new_stok_unitxx', tgl_update_stok = '$tgl' 
								where kode_brg_jadi= '$kode' AND kode_unit = '$kode_unit_jahit' ");
						}
					} // end if kode_unit_jahit != 0
					// =====================================================================================
					
					// jika semua data tdk kosong, insert ke tm_sjkeluarwip_detail
					$data_detail = array(
						'id_sjkeluarwip'=>$id_sj,
						'kode_brg_jadi'=>$kode,
						'qty'=>$qtytotal,
						'keterangan'=>$ket_detail
						//'ket_qty_warna'=>$ket_warna,
					);
					$this->db->insert('tm_sjkeluarwip_detail',$data_detail);
							// ================ end insert item detail ===========
					
					// ambil id detail sjmasukwip_detail
					$seq_detail	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail ORDER BY id DESC LIMIT 1 ");
					if($seq_detail->num_rows() > 0) {
						$seqrow	= $seq_detail->row();
						$iddetail = $seqrow->id;
					}
					else
						$iddetail = 0;
					
					// ----------------------------------------------
					for ($xx=0; $xx<count($kode_warna); $xx++) {
						$kode_warna[$xx] = trim($kode_warna[$xx]);
						$qty_warna[$xx] = trim($qty_warna[$xx]);
								
						$seq_warna	= $this->db->query(" SELECT id FROM tm_sjkeluarwip_detail_warna ORDER BY id DESC LIMIT 1 ");
							
						if($seq_warna->num_rows() > 0) {
							$seqrow	= $seq_warna->row();
							$idbaru	= $seqrow->id+1;
						}else{
							$idbaru	= 1;
						}

						$tm_sjkeluarwip_detail_warna	= array(
							 'id'=>$idbaru,
							 'id_sjkeluarwip_detail'=>$iddetail,
							 'id_warna_brg_jadi'=>'0',
							 'kode_warna'=>$kode_warna[$xx],
							 'qty'=>$qty_warna[$xx]
						);
						$this->db->insert('tm_sjkeluarwip_detail_warna',$tm_sjkeluarwip_detail_warna);
						
						// ========================= 03-02-2014, stok per warna ===============================================
						//cek stok terakhir tm_stok_hasil_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok FROM tm_stok_hasil_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_hasil_jahit='$id_stok' ");
						if ($query3->num_rows() == 0){
							$stok_warna_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_warna_lama	= $hasilrow->stok;
						}
						$new_stok_warna = $stok_warna_lama-$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_hasil_jahit_warna, insert
							$seq_stokwarna	= $this->db->query(" SELECT id FROM tm_stok_hasil_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokwarna->num_rows() > 0) {
								$seq_stokwarnarow	= $seq_stokwarna->row();
								$id_stok_warna	= $seq_stokwarnarow->id+1;
							}else{
								$id_stok_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_warna,
								'id_stok_hasil_jahit'=>$id_stok,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_warna,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_hasil_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET stok = '$new_stok_warna', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_hasil_jahit='$id_stok' ");
						}
						
						// ----------------------- stok unit jahit -------------------------------------------
					//update stok unit jahit jika kode_unit_jahit != 0
					if ($kode_unit_jahit != '0') {
						// 25-03-2014 tambahin stok_perbaikan
						//cek stok terakhir tm_stok_unit_jahit_warna, dan update stoknya
						$query3	= $this->db->query(" SELECT stok, stok_perbaikan FROM tm_stok_unit_jahit_warna WHERE kode_warna = '".$kode_warna[$xx]."'
								AND id_stok_unit_jahit='$id_stok_unit' ");
						if ($query3->num_rows() == 0){
							$stok_unit_warna_lama = 0;
							$stok_unit_warnaxx_lama = 0;
						}
						else {
							$hasilrow = $query3->row();
							$stok_unit_warna_lama	= $hasilrow->stok;
							$stok_unit_warnaxx_lama = $hasilrow->stok_perbaikan;
						}
						$new_stok_unit_warna = $stok_unit_warna_lama+$qty_warna[$xx]; // bertambah stok karena masuk ke unit
						$new_stok_unit_warnaxx = $stok_unit_warnaxx_lama+$qty_warna[$xx];
						
						if ($query3->num_rows() == 0){ // jika blm ada data di tm_stok_unit_jahit_warna, insert
							$seq_stokunitwarna	= $this->db->query(" SELECT id FROM tm_stok_unit_jahit_warna ORDER BY id DESC LIMIT 1 ");
							if($seq_stokunitwarna->num_rows() > 0) {
								$seq_stokunitwarnarow	= $seq_stokunitwarna->row();
								$id_stok_unit_warna	= $seq_stokunitwarnarow->id+1;
							}else{
								$id_stok_unit_warna	= 1;
							}
							
							$data_stok = array(
								'id'=>$id_stok_unit_warna,
								'id_stok_unit_jahit'=>$id_stok_unit,
								'id_warna_brg_jadi'=>'0',
								'kode_warna'=>$kode_warna[$xx],
								'stok'=>$new_stok_unit_warna,
								'stok_perbaikan'=>$new_stok_unit_warnaxx,
								'tgl_update_stok'=>$tgl
								);
							$this->db->insert('tm_stok_unit_jahit_warna', $data_stok);
						}
						else {
							$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET stok = '$new_stok_unit_warna', 
							stok_perbaikan = '$new_stok_unit_warnaxx', tgl_update_stok = '$tgl' 
							where kode_warna= '".$kode_warna[$xx]."' AND id_stok_unit_jahit='$id_stok_unit' ");
						}
					} // end if stok unit jahit
					// ------------------------------------------------------------------------------------------
						
					} // end for
					// ----------------------------------------------
						
				} // end perulangan

			if ($carinya == '') $carinya = "all";
			if ($is_cari == 0)
				$url_redirectnya = "wip/cform/viewsjkeluar/".$cur_page;
			else
				$url_redirectnya = "wip/cform/carisjkeluar/".$tgl_awal."/".$tgl_akhir."/".$unit_jahit."/".$unit_packing."/".$gudang."/".$carinya."/".$cur_page;
			
			redirect($url_redirectnya);
				
  }

  function submitsjkeluar(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
			$no_sj 	= $this->input->post('no_sj', TRUE);
			$tgl_sj 	= $this->input->post('tgl_sj', TRUE);  
			$id_gudang 	= $this->input->post('gudang', TRUE);  
			$jenis 	= $this->input->post('jenis_keluar', TRUE);  
			$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);  
			$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);  
			$ket 	= $this->input->post('ket', TRUE);  
			$pisah1 = explode("-", $tgl_sj);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_sj = $thn1."-".$bln1."-".$tgl1;
									
			$no 	= $this->input->post('no', TRUE);
			$jumlah_input=$no-1;
			
			//$cek_data = $this->mmaster->cek_data_sjkeluarwip($no_sj, $kode_unit_jahit);
			$cek_data = $this->mmaster->cek_data_sjkeluarwip($no_sj, $thn1, $kode_unit_jahit, $kode_unit_packing, $jenis);
			if (count($cek_data) > 0) {
				$data['isi'] = 'wip/vmainformsjkeluar';
				$data['msg'] = "Data no SJ keluar ".$no_sj." sudah ada..!";
				$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
				$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
				$data['list_gudang'] = $this->mmaster->get_gudang();
				$this->load->view('template',$data);
			}
			else {
				// new 05-03-2014
				$tgl = date("Y-m-d H:i:s");
				// insert di tm_sjkeluarwip
				$data_header = array(
				  'no_sj'=>$no_sj,
				  'tgl_sj'=>$tgl_sj,
				  'jenis_keluar'=>$jenis,
				  'id_gudang'=>$id_gudang,
				  'kode_unit_jahit'=>$kode_unit_jahit,
				  'kode_unit_packing'=>$kode_unit_packing,
				  'tgl_input'=>$tgl,
				  'tgl_update'=>$tgl,
				  'keterangan'=>$ket
				);
				$this->db->insert('tm_sjkeluarwip',$data_header);
				
				// ambil data terakhir di tabel tm_sjkeluarwip
				$query2	= $this->db->query(" SELECT id FROM tm_sjkeluarwip ORDER BY id DESC LIMIT 1 ");
				$hasilrow = $query2->row();
				$id_sj	= $hasilrow->id; 
				
				for ($i=1;$i<=$jumlah_input;$i++)
				{
					$this->mmaster->savesjkeluar($id_sj, $id_gudang, $kode_unit_jahit, $kode_unit_packing, $jenis,
							$this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
								//$this->input->post('qty_'.$i, TRUE), 
								$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
								$this->input->post('qty_warna_'.$i, TRUE),
								$this->input->post('ket_detail_'.$i, TRUE) );						
				}
				redirect('wip/cform/viewsjkeluar');
			}
  }
  
  function viewsjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
    $data['isi'] = 'wip/vformviewsjkeluar';
    $keywordcari = "all";
    $date_from = "00-00-0000";
	$date_to = "00-00-0000";
	$kode_unit_jahit = '0';
	$kode_unit_packing = '0';
	$gudang = '0';
	$caribrg = "all";
	$filterbrg = "n";
	
    $jum_total = $this->mmaster->getAllsjkeluartanpalimit($keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/wip/cform/viewsjkeluar/';
							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(4);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->getAllsjkeluar($config['per_page'],$this->uri->segment(4), $keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 0;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	
	$data['gudang'] = $gudang;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$this->load->view('template',$data);
  }
  
  function carisjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
	$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
	$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);
	$gudang 	= $this->input->post('gudang', TRUE);
	$filterbrg 	= $this->input->post('filter_brg', TRUE);
	$caribrg 	= $this->input->post('cari_brg', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($kode_unit_jahit == '')
		$kode_unit_jahit = $this->uri->segment(6);
	if ($kode_unit_packing == '')
		$kode_unit_packing = $this->uri->segment(7);
	if ($gudang == '')
		$gudang = $this->uri->segment(8);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(9);
	if ($caribrg == '')
		$caribrg = $this->uri->segment(10);
	if ($filterbrg == '')
		$filterbrg = $this->uri->segment(11);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	if ($caribrg == '')
		$caribrg = "all";
	if ($filterbrg == '')
		$filterbrg = "n";
	
	$jum_total = $this->mmaster->getAllsjkeluartanpalimit($keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
							$config['base_url'] = base_url().'index.php/wip/cform/carisjkeluar/'.$date_from.'/'.$date_to.'/'.$kode_unit_jahit.'/'.$kode_unit_packing.'/'.$gudang.'/'.$keywordcari.'/'.$caribrg.'/'.$filterbrg;
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(12);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllsjkeluar($config['per_page'],$this->uri->segment(12), $keywordcari, $date_from, $date_to, $gudang, $kode_unit_jahit, $kode_unit_packing, $caribrg, $filterbrg);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'wip/vformviewsjkeluar';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	if ($caribrg == "all")
		$data['caribrg'] = '';
	else
		$data['caribrg'] = $caribrg;
	$data['filterbrg'] = $filterbrg;
	
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['list_unit_packing'] = $this->mmaster->get_unit_packing();
	
	$data['gudang'] = $gudang;
	$data['kode_unit_jahit'] = $kode_unit_jahit;
	$data['kode_unit_packing'] = $kode_unit_packing;
	$this->load->view('template',$data);
  }
  
  function deletesjkeluar(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$is_cari 	= $this->uri->segment(6);
	$tgl_awal 	= $this->uri->segment(7);
	$tgl_akhir 	= $this->uri->segment(8);
	$kode_unit_jahit 	= $this->uri->segment(9);
	$kode_unit_packing 	= $this->uri->segment(10);
	$gudang 	= $this->uri->segment(11);
	$carinya 	= $this->uri->segment(12);
    
    $this->mmaster->deletesjkeluar($kode);
    
    if ($carinya == '') $carinya = "all";
	if ($is_cari == 0)
		$url_redirectnya = "wip/cform/viewsjkeluar/".$cur_page;
	else
		$url_redirectnya = "wip/cform/carisjkeluar/".$tgl_awal."/".$tgl_akhir."/".$kode_unit_jahit."/".$kode_unit_packing."/".$gudang."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  // 04-12-2014
  // 09-07-2013, SET STOK AWAL PERTAMA KALI
  function addstokawal(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$th_now	= date("Y");

	$data['isi'] = 'wip/vform1stokawal';
	$data['msg'] = '';
	$data['list_gudang'] = $this->mmaster->get_gudang();
	$this->load->view('template',$data);
  }
  
  function submitstokawal(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
		$id_gudang 	= $this->input->post('gudang', TRUE);  
		$no 	= $this->input->post('no', TRUE);
		$jumlah_input=$no-1;
			
		for ($i=1;$i<=$jumlah_input;$i++)
		{
			$this->mmaster->savestokawal($id_gudang, $this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
						$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
						$this->input->post('qty_warna_'.$i, TRUE) );
		}
		redirect('wip/cform/viewstokawal');
  }
  
  function viewstokawal(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '')
			$keywordcari = $this->uri->segment(4);
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$jum_total = $this->mmaster->getAllstokawaltanpalimit($keywordcari);
								$config['base_url'] = base_url().'index.php/wip/cform/viewstokawal/'.$keywordcari.'/';
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '20';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(5);
								$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllstokawal($config['per_page'],$this->uri->segment(5), $keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['startnya'] = $config['cur_page'];
		
		$data['isi'] = 'wip/vformviewstokawal';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		
		$this->load->view('template',$data);
  }
  
  function show_popup_brgjadi_stokawal(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$gudang 	= $this->input->post('id_gudang', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '' && $gudang == '') {
			$gudang 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
	// +++++++++++++++

		$qjum_total = $this->mmaster->get_brgjadi_stokawaltanpalimit($gudang, $keywordcari);
		
				$config['base_url'] = base_url()."index.php/wip/cform/show_popup_brgjadi_stokawal/".$gudang."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi_stokawal($config['per_page'],$config['cur_page'], $gudang, $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	
	$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$gudang' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$kode_gudang	= $hasilrow->kode_gudang;
		$nama_gudang	= $hasilrow->nama;
	}
	else {
		$kode_gudang	= '';
		$nama_gudang = '';
	}
	$data['gudang'] = $gudang;
	$data['kode_gudang'] = $kode_gudang;
	$data['nama_gudang'] = $nama_gudang;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('wip/vpopupbrgjadistokawal',$data);
  }
  
  // 12-07-2013
  function editstokawal(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	$is_simpan 	= $this->input->post('is_simpan', TRUE);
	
	if ($is_simpan == '') {
		$id_stok 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$carinya 	= $this->uri->segment(6);
		
		$query3	= $this->db->query(" SELECT a.id_gudang, a.kode_brg_jadi, a.stok, b.e_product_motifname 
								FROM tm_stok_hasil_jahit a, tr_product_motif b 
								WHERE a.kode_brg_jadi = b.i_product_motif AND a.id = '$id_stok' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$id_gudang = $hasilrow->id_gudang;
			$kode_brg_jadi = $hasilrow->kode_brg_jadi;
			$nama_brg_jadi = $hasilrow->e_product_motifname;
			$stok = $hasilrow->stok;
						
			// ambil data nama gudang
			$query3	= $this->db->query(" SELECT kode_gudang, nama FROM tm_gudang WHERE id = '$id_gudang' ");
			$hasilrow = $query3->row();
			$kode_gudang	= $hasilrow->kode_gudang;
			$nama_gudang	= $hasilrow->nama;
		}
		
		$data['msg'] = '';
		$data['id_stok'] = $id_stok;
		$data['cur_page'] = $cur_page;
		if ($carinya == '')
			$data['carinya'] = "all";
		else
			$data['carinya'] = $carinya;
		$data['gudang'] = $kode_gudang." - ".$nama_gudang;
		$data['brg_jadi'] = $kode_brg_jadi." - ".$nama_brg_jadi;
		$data['stok'] = $stok;

		$data['isi'] = 'wip/veditstokawal';
		$this->load->view('template',$data);
	}
	else { // simpan
		$id_stok 	= $this->input->post('id_stok', TRUE);
		$cur_page 	= $this->input->post('cur_page', TRUE);  
		$carinya 	= $this->input->post('carinya', TRUE);  
		$stok = $this->input->post('stok', TRUE);
		$tgl = date("Y-m-d");
		
		// update stoknya
		$this->db->query(" UPDATE tm_stok_hasil_jahit SET stok = '$stok', tgl_update_stok = '$tgl'
							WHERE id= '$id_stok' ");
		
		if ($carinya == '') $carinya = "all";
		$url_redirectnya = "wip/cform/viewstokawal/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
	}

  }
  // end 04-12-2014
  
  // 25-07-2013
  function notareturwipadd(){
// =======================
	// disini coding utk pengecekan user login	
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}

	$no_sj_keluar = $this->input->post('sj_keluar', TRUE);  
	$id_sj_keluar = $this->input->post('id_sj', TRUE);  
	$proses_submit = $this->input->post('submit', TRUE); 
	$unit_jahit = $this->input->post('unit_jahit', TRUE);  
	
	$th_now	= date("Y");
	
	if ($proses_submit == "Proses") {
		$query3	= $this->db->query(" SELECT no_nota FROM tm_nota_retur_wip ORDER BY no_nota DESC LIMIT 1 ");
		$hasilrow = $query3->row();
			if ($query3->num_rows() != 0) 
				$no_nota	= $hasilrow->no_nota;
			else
				$no_nota = '';
			if(strlen($no_nota)==8) {
				$nonota = substr($no_nota, 0, 8);
				$n_nota	= (substr($nonota,4,4))+1;
				$th_nota	= substr($nonota,0,4);
				if($th_now==$th_nota) {
						$jml_n_nota	= $n_nota;
						switch(strlen($jml_n_nota)) {
							case "1": $kodenota	= "000".$jml_n_nota;
							break;
							case "2": $kodenota	= "00".$jml_n_nota;
							break;	
							case "3": $kodenota	= "0".$jml_n_nota;
							break;
							case "4": $kodenota	= $jml_n_nota;
							break;	
						}
						$nomornota = $th_now.$kodenota;
				}
				else {
					$nomornota = $th_now."0001";
				}
			}
			else {
				$nomornota	= $th_now."0001";
			}
			
		$data['no_nota'] = $nomornota;
		
		$data['sj_detail'] = $this->mmaster->get_detail_sj_keluar($no_sj_keluar, $id_sj_keluar, $unit_jahit);
		$data['msg'] = '';
		$data['no_sj_keluar'] = $no_sj_keluar;
		$data['go_proses'] = '1';
		
		$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
		$hasilrow = $query3->row();
		if ($query3->num_rows() != 0) 
			$nama_unit	= $hasilrow->nama;
				
			$data['nama_unit'] = $nama_unit;
			$data['unit_jahit'] = $unit_jahit;
	}
	else {
		$data['msg'] = '';
		$data['go_proses'] = '';
		$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	}

	$data['isi'] = 'wip/vformnotareturwip';
	$this->load->view('template',$data);

  }
  
  function show_popup_sj_keluar(){
	// =======================
	// disini coding utk pengecekan user login
	//========================

	$unit_jahit	= $this->uri->segment(4);

	if ($unit_jahit == '') {
		$unit_jahit 	= $this->input->post('unit_jahit', TRUE);  
	}
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $unit_jahit == '' ) {
			$unit_jahit 	= $this->uri->segment(4);
			$keywordcari 	= $this->uri->segment(5);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";

		$qjum_total = $this->mmaster->get_sj_keluar2tanpalimit($keywordcari, $unit_jahit);
		
	$data['query'] = $this->mmaster->get_sj_keluar2($keywordcari, $unit_jahit);

	$data['jum_total'] = count($qjum_total);
	$data['unit_jahit'] = $unit_jahit;
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;

	$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$unit_jahit' ");
	$hasilrow = $query3->row();
	$nama_unit	= $hasilrow->nama;
	$data['nama_unit'] = $nama_unit;

	$this->load->view('wip/vpopupsjkeluar',$data);
	
  }
  
  // 26-07-2013
  function notareturwipsubmit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
			$no_nota 	= $this->input->post('no_nota', TRUE);
			$tgl_nota 	= $this->input->post('tgl_nota', TRUE);
			$no_nota = trim($no_nota);
			$unit_jahit = $this->input->post('unit_jahit', TRUE);
			$gtotal = $this->input->post('gtotal', TRUE);  
			$ket = $this->input->post('ket', TRUE);
			
			$tgl_nota = $this->input->post('tgl_nota', TRUE);  
			$pisah1 = explode("-", $tgl_nota);
			$tgl1= $pisah1[0];
			$bln1= $pisah1[1];
			$thn1= $pisah1[2];
			$tgl_nota = $thn1."-".$bln1."-".$tgl1;
			
			$no 	= $this->input->post('no', TRUE);
						
			// jika edit, var ini ada isinya
			$id_nota 	= $this->input->post('id_nota', TRUE);
			
			if ($id_nota == '') {
				$cek_data = $this->mmaster->cek_data_notaretur($no_nota);
				if (count($cek_data) > 0) { 
					$data['isi'] = 'wip/vformnotareturwip';
					$data['msg'] = "Data no nota ".$no_nota." sudah ada..!";
					$data['go_proses'] = '';
					$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
					$this->load->view('template',$data);
				}
				else {
					$jumlah_input=$no-1;
					for ($i=1;$i<=$jumlah_input;$i++)
					{
						// 1. update data harga dan totalnya
						/*$this->mmaster->saveharga($unit_makloon, $this->input->post('id_sj_detail_'.$i, TRUE), 
								$this->input->post('kode_brg_makloon_'.$i, TRUE),
								$this->input->post('harga_'.$i, TRUE), $this->input->post('harga_lama_'.$i, TRUE),
								$this->input->post('total_'.$i, TRUE) ); */
						
						$this->mmaster->savenotaretur($no_nota, $tgl_nota, $ket, $gtotal, $unit_jahit, 
								$this->input->post('id_sj_keluar_detail_'.$i, TRUE), 
								$this->input->post('kode_brg_jadi_'.$i, TRUE),
								$this->input->post('qty_'.$i, TRUE), 
								$this->input->post('harga_'.$i, TRUE), 
								$this->input->post('harga_lama_'.$i, TRUE), 
								$this->input->post('diskon_'.$i, TRUE), 
								$this->input->post('total_'.$i, TRUE) );
					}
					redirect('wip/cform/notareturwipview'); 
				}
					// 2. insert data nota dan detail item brgnya
					/*$tgl = date("Y-m-d H:i:s");
					$data_header = array(
							  'no_nota'=>$no_nota,
							  'tgl_nota'=>$tgl_nota,
							  'tgl_input'=>$tgl,
							  'tgl_update'=>$tgl,
							  'kode_unit_jahit'=>$unit_jahit,
							  'keterangan'=>$ket,
							  'total'=>$gtotal
							);
					$this->db->insert('tm_nota_retur_wip',$data_header);
					
					$list_sj = explode(",", $no_sj_masuk); 
					// ambil data terakhir di tabel tm_faktur_makloon
					$query3	= $this->db->query(" SELECT id FROM tm_faktur_makloon ORDER BY id DESC LIMIT 1 ");
					$hasilrow = $query3->row();
					$id_fq	= $hasilrow->id;
					
					// insert tabel detail sj-nya
					foreach($list_sj as $row1) {
						$row1 = trim($row1);
						if ($row1 != '') {
							$data_detail = array(
							  'id_faktur_makloon'=>$id_fq,
							  'no_sj_masuk'=>$row1
							);
							$this->db->insert('tm_faktur_makloon_sj',$data_detail);
														
							// 10-04-2012 tambahkan update field jenis_pembelian
							$this->db->query(" UPDATE tm_sj_hasil_makloon SET no_faktur = '$no_faktur', status_faktur = 't',
												jenis_pembelian = '$jenis_pembelian'
												WHERE no_sj = '$row1' AND kode_unit = '$unit_makloon' ");
						}
					}
					
					redirect('faktur-quilting/cform/view');
				} */
			} // end if id_faktur == ''
			else { // update
				$cur_page = $this->input->post('cur_page', TRUE);
				$tgl_awal = $this->input->post('tgl_awal', TRUE);
				$tgl_akhir = $this->input->post('tgl_akhir', TRUE);
				$carinya = $this->input->post('carinya', TRUE);
							
				$tgl = date("Y-m-d H:i:s");
				
				// update di tabel tm_nota_retur_wip
					$this->db->query(" UPDATE tm_nota_retur_wip SET no_nota='$no_nota', tgl_nota = '$tgl_nota', 
									total= '$gtotal', keterangan='$ket', tgl_update = '$tgl'
									where id= '$id_nota' ");
				
				$jumlah_input=$no-1;
				for ($i=1;$i<=$jumlah_input;$i++)
				{ // detailnya

					$id_nota_retur_detail = $this->input->post('id_nota_retur_detail_'.$i, TRUE);
					$kode_brg_jadi = $this->input->post('kode_brg_jadi_'.$i, TRUE);
					$harga = $this->input->post('harga_'.$i, TRUE);
					$harga_lama = $this->input->post('harga_lama_'.$i, TRUE);
					$diskon = $this->input->post('diskon_'.$i, TRUE);
					$total = $this->input->post('total_'.$i, TRUE);
					
					$this->db->query(" UPDATE tm_nota_retur_wip_detail SET harga = '$harga', diskon = '$diskon', subtotal = '$total' 
						WHERE id = '$id_nota_retur_detail' ");	
					
					// 13-01-2014
					// ambil harga barang jahit
					$query3	= $this->db->query(" SELECT id FROM tm_harga_hasil_jahit
									WHERE kode_brg_jadi = '$kode_brg_jadi' AND kode_unit = '$unit_jahit' ");
					if ($query3->num_rows() == 0){
						$data_harga = array(
							'kode_brg_jadi'=>$kode_brg_jadi,
							'kode_unit'=>$unit_jahit,
							'harga'=>$harga,
							'tgl_input'=>$tgl,
							'tgl_update'=>$tgl
							);
						$this->db->insert('tm_harga_hasil_jahit',$data_harga);
					}
					else {
						if ($harga != $harga_lama) {
							$hasilrow3 = $query3->row();
							$id_harga	= $hasilrow3->id;
							$this->db->query(" UPDATE tm_harga_hasil_jahit SET harga = '$harga', tgl_update='$tgl' 
											WHERE id = '$id_harga' ");
						}
					}
					
				} // end for
					
					if ($carinya == '') $carinya = "all";
					$url_redirectnya = "wip/cform/notareturwipview/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
					
					redirect($url_redirectnya);
			}
  }
  
  function notareturwipview(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
	$keywordcari 	= $this->input->post('cari', TRUE);  
	$date_from 	= $this->input->post('date_from', TRUE); 
	$date_to 	= $this->input->post('date_to', TRUE);
    
	if ($date_from == '')
		$date_from = $this->uri->segment(4);
	if ($date_to == '')
		$date_to = $this->uri->segment(5);
	if ($keywordcari == '')
		$keywordcari = $this->uri->segment(6);
	
	if ($keywordcari == '')
		$keywordcari 	= "all";
	if ($date_from == '')
		$date_from = "00-00-0000";
	if ($date_to == '')
		$date_to = "00-00-0000";
	
	$jum_total = $this->mmaster->getAllnotareturwiptanpalimit($keywordcari, $date_from, $date_to);
							$config['base_url'] = base_url().'index.php/wip/cform/notareturwipview/'.$date_from.'/'.$date_to.'/'.$keywordcari.'/';
							$config['total_rows'] = count($jum_total); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);		
	$data['query'] = $this->mmaster->getAllnotareturwip($config['per_page'],$this->uri->segment(7), $keywordcari, $date_from, $date_to);
	$data['jum_total'] = count($jum_total);
	if ($config['cur_page'] == '')
		$cur_page = 0;
	else
		$cur_page = $config['cur_page'];
	$data['cur_page'] = $cur_page;
	$data['is_cari'] = 1;
	
	$data['isi'] = 'wip/vformviewnotareturwip';
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	if ($date_from=="00-00-0000")
		$data['date_from'] = '';
	else
		$data['date_from'] = $date_from;
	
	if ($date_to=="00-00-0000")
		$data['date_to'] = '';
	else
		$data['date_to'] = $date_to;
	
	$this->load->view('template',$data);
  }
  
  function notareturwipdelete(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
	  
    $kode 	= $this->uri->segment(4);
    $cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
    
    $this->mmaster->deletenotareturwip($kode);
    
    if ($carinya == '') $carinya = "all";
	$url_redirectnya = "wip/cform/notareturwipview/".$tgl_awal."/".$tgl_akhir."/".$carinya."/".$cur_page;
		
	redirect($url_redirectnya);
  }
  
  function notareturwipedit(){
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		//$this->load->view('loginform', $data);
		redirect('loginform');
	}

	$id_nota 	= $this->uri->segment(4);
	$cur_page 	= $this->uri->segment(5);
	$tgl_awal 	= $this->uri->segment(6);
	$tgl_akhir 	= $this->uri->segment(7);
	$carinya 	= $this->uri->segment(8);
	
	$data['cur_page'] = $cur_page;
	$data['tgl_awal'] = $tgl_awal;
	$data['tgl_akhir'] = $tgl_akhir;
	$data['carinya'] = $carinya;
	
	$data['query'] = $this->mmaster->get_notareturwip($id_nota); 
	$data['list_unit_jahit'] = $this->mmaster->get_unit_jahit();
	$data['msg'] = '';
	$data['isi'] = 'wip/veditformnotareturwip';
	$this->load->view('template',$data);
  }
  
  // 16-01-2014, cetak nota retur
  function print_nota_retur() {
		
		$id_nota	= $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$nowdate	= date('Y-m-d');
		$logfile	= 'logs'.'-'.$nowdate;
		$data['log_destination']= 'logs/'.$logfile;
		
		$queryxx	= $this->db->query(" SELECT printer_uri FROM tm_printer_ipp ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$printer_uri	= $hasilxx->printer_uri;
		}
		else {
			echo "Printer URI belum disetting. Silahkan ke menu Setting > Printer IPP"; 
			die();
		}
		
		$datanota = $this->mmaster->get_notareturwipgroupbyproduk($id_nota);
		//print_r($datanota); die();
		$data['ip_address'] = $ip_address;
		$data['printer_uri'] = $printer_uri;
		$data['query'] = $datanota;
		
		$this->load->view('wip/vprintnotaretur',$data);
		// ===============================================================================================================
	}
	
	// 28-01-2014. modif 01-03-2014, pake kode_warna aja, ga usah id_warna_brg_jadi
	function additemwarna(){

		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna_brg_jadi'=> $rowxx->id,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('wip/vlistwarna', $data); 
		return true;
  }
  
  // 07-02-2014, skrip utk reset nomor SJ keluar WIP
  function resetnosj2014(){
	  $tglskrg = date("Y-m-d");
	  $noawal = "201400001";
	  $sqlxx = $this->db->query(" SELECT * FROM tm_sjkeluarwip WHERE tgl_sj >='2014-01-01' AND tgl_sj <='$tglskrg' ORDER BY tgl_sj ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$noawal' WHERE no_sj = '$rowxx->no_sj' ");
				$noawal++;
			}
	  }
	  $noakhir = $noawal-1;
	  echo "reset nomor SJ 2014 sukses, nomor terakhir adalah: ".$noakhir."<br>";
	  echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
  }
  
  // modif 13-02-2014
  function editnosj2013(){
	  $sqlxx = $this->db->query(" SELECT id, no_sj, tgl_sj, kode_unit_jahit FROM tm_sjkeluarwip WHERE tgl_sj <'2014-01-01' 
						AND kode_unit_jahit <> '0' AND kode_unit_packing = '0' ORDER BY kode_unit_jahit, tgl_sj ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			$unitnya = ''; 
			foreach ($hasilxx as $rowxx) {
				//$tahun = substr($rowxx->no_sj, 0, 4);
				//$noakhir = substr($rowxx->no_sj, 4);
				//if ($tahun == 2014) {
					//echo $rowxx->no_sj." ".$rowxx->tgl_sj." ".$noakhir."<br>";
					// cek nomor terakhir thn 2013
					//$sqlxx2 = $this->db->query(" SELECT no_sj FROM tm_sjkeluarwip WHERE tgl_sj <'2014-01-01' AND no_sj like '2013%' ORDER BY no_sj DESC LIMIT 1 ");
					//if ($sqlxx2->num_rows() > 0){
					//	$hasilxx2 = $sqlxx2->row();
					//	$nosjbaru = $hasilxx2->no_sj+1;
					if ($unitnya != $rowxx->kode_unit_jahit) {
						$nosjnya = 201300001;
						$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$nosjnya' WHERE id = '".$rowxx->id."' ");
						$unitnya = $rowxx->kode_unit_jahit;
					}
					else {
						$nosjnya++;
						$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$nosjnya' WHERE id = '".$rowxx->id."' ");
					}
				//}
			} // end foreach
	  }
	  //echo "edit nomor SJ thn 2013 sukses. Nomor SJ 2013 terakhir adalah ".$noawal."<br>";
	  echo "edit nomor SJ thn 2013 utk unit jahit sukses. <br>";
	  echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
  }
  
  function editnosj2013unitpacking(){
	  $sqlxx = $this->db->query(" SELECT id, no_sj, tgl_sj, kode_unit_packing FROM tm_sjkeluarwip WHERE tgl_sj <'2014-01-01' 
						AND kode_unit_packing <> '0' AND kode_unit_jahit = '0' ORDER BY kode_unit_jahit, tgl_sj ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			$unitnya = ''; 
			foreach ($hasilxx as $rowxx) {
					if ($unitnya != $rowxx->kode_unit_packing) {
						$nosjnya = 201300001;
						$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$nosjnya' WHERE id = '".$rowxx->id."' ");
						$unitnya = $rowxx->kode_unit_packing;
					}
					else {
						$nosjnya++;
						$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$nosjnya' WHERE id = '".$rowxx->id."' ");
					}
				//}
			} // end foreach
	  }
	  //echo "edit nomor SJ thn 2013 sukses. Nomor SJ 2013 terakhir adalah ".$noawal."<br>";
	  echo "edit nomor SJ thn 2013 utk unit packing sukses. <br>";
	  echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
  }
  
  // modif 13-02-2014
  function editnosj2014(){
	  $sqlxx = $this->db->query(" SELECT id, no_sj, tgl_sj, kode_unit_jahit FROM tm_sjkeluarwip WHERE tgl_sj >='2014-01-01' 
								AND kode_unit_jahit <> '0' AND kode_unit_packing = '0' ORDER BY kode_unit_jahit, tgl_sj ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			$unitnya = ''; //$nosjnya = 201400001;
			foreach ($hasilxx as $rowxx) {
				if ($unitnya != $rowxx->kode_unit_jahit) {
					$nosjnya = 201400001;
					$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$nosjnya' WHERE id = '".$rowxx->id."' ");
					$unitnya = $rowxx->kode_unit_jahit;
				}
				else {
					/*$sqlxx2 = $this->db->query(" SELECT no_sj FROM tm_sjkeluarwip WHERE tgl_sj >='2014-01-01' 
										AND kode_unit_jahit = '".$rowxx->kode_unit_jahit."' AND no_sj < '201400016'
										ORDER BY no_sj DESC LIMIT 1 ");
					if ($sqlxx2->num_rows() > 0){
						$hasilxx2 = $sqlxx2->row();
						$nosjbaru = $hasilxx2->no_sj+1;
						$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$nosjbaru' WHERE id = '".$rowxx->id."' ");
					} */
					$nosjnya++;
					$this->db->query(" UPDATE tm_sjkeluarwip SET no_sj = '$nosjnya' WHERE id = '".$rowxx->id."' ");
				}
				//$tahun = substr($rowxx->no_sj, 0, 4);
			} // end foreach
	  }
	  echo "edit nomor SJ thn 2014 berdasarkan unit jahit sukses <br>";
	  echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
  }
  
  // 17-02-2014
  function editnonotaretur2014() {
	$sqlxx = $this->db->query(" SELECT id, no_nota FROM tm_nota_retur_wip ORDER BY no_nota ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$no_nota = $rowxx->no_nota;
				
				$thn = substr($no_nota, 0,4);
				$no_urut = substr($no_nota, 4,5);
				$no_urut = intval($no_urut);
				
				if ($no_urut < 10)
					$no_urut_baru = "000".$no_urut;
				else
					$no_urut_baru = "00".$no_urut;
				
				$no_urut_baru = $thn.$no_urut_baru;
				
				$this->db->query(" UPDATE tm_nota_retur_wip SET no_nota = '$no_urut_baru' WHERE id='$id' ");
			} // end foreach
			echo "edit nomor nota thn ".$thn." sukses <br>";
			echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
	  }
  }
  
  // 01-03-2014, skrip utk konversi id_warna_brg_jadi ke kode_warna di tabel tm_sjmasukwip_detail_warna, tm_sjkeluarwip_detail_warna, 
	//tm_stok_hasil_jahit_warna, tm_stok_unit_jahit_warna. Relasi ke tabel tm_warna_brg_jadi dan tm_warna 
  function editidwarnabrgjadi2kodewarna() {
	  // 1. query ke tm_sjmasukwip_detail_warna
	  $sqlxx = $this->db->query(" SELECT * FROM tm_sjmasukwip_detail_warna ORDER BY id ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;
					
					$this->db->query(" UPDATE tm_sjmasukwip_detail_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit sj masuk detail warna sukses <br>";
			//echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
	  }
	  
	  // 2. query ke tm_sjkeluarwip_detail_warna
	  $sqlxx = $this->db->query(" SELECT * FROM tm_sjkeluarwip_detail_warna ORDER BY id ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;
					
					$this->db->query(" UPDATE tm_sjkeluarwip_detail_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit sj keluar detail warna sukses <br>";
			//echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
	  }
	  
	  // 3. query ke tm_stok_hasil_jahit_warna
	  $sqlxx = $this->db->query(" SELECT * FROM tm_stok_hasil_jahit_warna ORDER BY id ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;
					
					$this->db->query(" UPDATE tm_stok_hasil_jahit_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit stok hasil jahit warna sukses <br>";
			//echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
	  }
	  
	  // 4. query ke tm_stok_unit_jahit_warna
	  $sqlxx = $this->db->query(" SELECT * FROM tm_stok_unit_jahit_warna ORDER BY id ASC ");
	  if ($sqlxx->num_rows() > 0){
			$hasilxx = $sqlxx->result();
			foreach ($hasilxx as $rowxx) {
				$id = $rowxx->id;
				$id_warna_brg_jadi = $rowxx->id_warna_brg_jadi;
				//----------------------------------------------------------
				$sqlxx2	= $this->db->query(" SELECT b.kode FROM tm_warna_brg_jadi a, tm_warna b WHERE a.kode_warna = b.kode 
									AND a.id = '$id_warna_brg_jadi' ");
				if ($sqlxx2->num_rows() > 0){
					$hasilxx2 = $sqlxx2->row();
					$kode_warna	= $hasilxx2->kode;
					
					$this->db->query(" UPDATE tm_stok_unit_jahit_warna SET kode_warna = '$kode_warna', id_warna_brg_jadi = '0' 
								 WHERE id='$id' ");
				}
				//----------------------------------------------------------
			} // end foreach
			echo "edit stok unit jahit warna sukses <br>";
			echo "<a href='".base_url()."'>Kembali ke halaman depan</a>";
	  }
  }
  
  // 25-03-2014
  function caribrgjadi(){
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		
		// query ke tabel brg jadi utk ambil kode, nama, dan stok
		$queryxx = $this->db->query(" SELECT e_product_motifname FROM tr_product_motif
									WHERE i_product_motif = '".$kode_brg_jadi."' ");
				
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->row();
			$nama_brg_jadi = $hasilxx->e_product_motifname;
		}
		else
			$nama_brg_jadi = '';
		//echo $nama_brg_jadi; die();
		$data['nama_brg_jadi'] = $nama_brg_jadi;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		$this->load->view('wip/vinfobrgjadi', $data); 
		return true;
  }
  
  // 08-04-2014
  function ceknosjmasuk(){
		$no_sj 	= $this->input->post('no_sj', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);
		$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
		$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);
		$jenis_masuk 	= $this->input->post('jenis_masuk', TRUE);
		
		$pisah1 = explode("-", $tgl_sj);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		
		$cek_data = $this->mmaster->cek_data_sjmasukwip($no_sj, $thn1, $kode_unit_jahit, $kode_unit_packing, $jenis_masuk);
		if (count($cek_data) > 0)
			$ada = "ada";	
		else
			$ada = "tidak";
		
		echo $ada; 
		return true;
  }
  
  function ceknosjkeluar(){
		$no_sj 	= $this->input->post('no_sj', TRUE);
		$tgl_sj 	= $this->input->post('tgl_sj', TRUE);
		$kode_unit_jahit 	= $this->input->post('kode_unit_jahit', TRUE);
		$kode_unit_packing 	= $this->input->post('kode_unit_packing', TRUE);
		$jenis_keluar 	= $this->input->post('jenis_keluar', TRUE);
		
		$pisah1 = explode("-", $tgl_sj);
		$tgl1= $pisah1[0];
		$bln1= $pisah1[1];
		$thn1= $pisah1[2];
		$cek_data = $this->mmaster->cek_data_sjkeluarwip($no_sj, $thn1, $kode_unit_jahit, $kode_unit_packing, $jenis_keluar);
		if (count($cek_data) > 0)
			$ada = "ada";	
		else
			$ada = "tidak";
		
		echo $ada; 
		return true;
  }
  
  // 04-12-2014
  // 11-11-2014, set stok awal unit jahit PERTAMA KALI
  function addstokawalunit(){
// =======================
	// disini coding utk pengecekan user login	
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
	if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
	}
	$th_now	= date("Y");

	$data['isi'] = 'wip/vform1stokawalunit';
	$data['msg'] = '';
	$data['list_unit'] = $this->mmaster->get_unit_jahit();
	$this->load->view('template',$data);
  }
  
  function submitstokawalunit(){
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
		redirect('loginform');
		}
	  
		$kode_unit 	= $this->input->post('kode_unit', TRUE);  
		$no 	= $this->input->post('no', TRUE);
		$jumlah_input=$no-1;
			
		for ($i=1;$i<=$jumlah_input;$i++)
		{
			$this->mmaster->savestokawalunit($kode_unit, $this->input->post('kode_'.$i, TRUE),$this->input->post('nama_'.$i, TRUE), 
						$this->input->post('temp_qty_'.$i, TRUE), $this->input->post('kode_warna_'.$i, TRUE),
						$this->input->post('qty_warna_'.$i, TRUE),
						$this->input->post('temp_qty2_'.$i, TRUE), $this->input->post('kode_warna2_'.$i, TRUE),
						$this->input->post('qty_warna2_'.$i, TRUE) );
		}
		redirect('wip/cform/viewstokawalunit');
  }
  
  function viewstokawalunit(){
	  $is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '')
			$keywordcari = $this->uri->segment(4);
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
		$jum_total = $this->mmaster->getAllstokawalunittanpalimit($keywordcari);
								$config['base_url'] = base_url().'index.php/wip/cform/viewstokawalunit/'.$keywordcari.'/';
								$config['total_rows'] = count($jum_total); 
								$config['per_page'] = '20';
								$config['first_link'] = 'Awal';
								$config['last_link'] = 'Akhir';
								$config['next_link'] = 'Selanjutnya';
								$config['prev_link'] = 'Sebelumnya';
								$config['cur_page'] = $this->uri->segment(5);
								$this->pagination->initialize($config);		
		$data['query'] = $this->mmaster->getAllstokawalunit($config['per_page'],$this->uri->segment(5), $keywordcari);
		$data['jum_total'] = count($jum_total);
		if ($config['cur_page'] == '')
			$cur_page = 0;
		else
			$cur_page = $config['cur_page'];
		$data['cur_page'] = $cur_page;
		$data['startnya'] = $config['cur_page'];
		
		$data['isi'] = 'wip/vformviewstokawalunit';
		if ($keywordcari == "all")
			$data['cari'] = '';
		else
			$data['cari'] = $keywordcari;
		
		$this->load->view('template',$data);
  }
  
  function show_popup_brgjadi_stokawalunit(){
	// =======================
	// disini coding utk pengecekan user login
//========================
		$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			redirect('loginform');
		}
		
		$kode_unit 	= $this->input->post('kode_unit', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);  
		$keywordcari 	= $this->input->post('cari', TRUE);  
		
		if ($keywordcari == '' && $posisi == '' && $kode_unit == '') {
			$kode_unit 	= $this->uri->segment(4);
			$posisi 	= $this->uri->segment(5);
			$keywordcari 	= $this->uri->segment(6);
		}
		
		if ($keywordcari == '')
			$keywordcari 	= "all";
		
	// +++++++++++++++

		$qjum_total = $this->mmaster->get_brgjadi_stokawalunittanpalimit($kode_unit, $keywordcari);
		
				$config['base_url'] = base_url()."index.php/wip/cform/show_popup_brgjadi_stokawalunit/".$kode_unit."/".$posisi."/".$keywordcari."/";
							$config['total_rows'] = count($qjum_total);
							$config['per_page'] = 10;
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(7);
							$this->pagination->initialize($config);
	$data['query'] = $this->mmaster->get_brgjadi_stokawalunit($config['per_page'],$config['cur_page'], $kode_unit, $keywordcari);
	$data['jum_total'] = count($qjum_total);
	$data['posisi'] = $posisi;
	$data['jumdata'] = $posisi-1;
	
	$query3	= $this->db->query(" SELECT nama FROM tm_unit_jahit WHERE kode_unit = '$kode_unit' ");
	if ($query3->num_rows() > 0){
		$hasilrow = $query3->row();
		$nama_unit	= $hasilrow->nama;
	}
	else {
		$nama_unit = '';
	}
	$data['kode_unit'] = $kode_unit;
	$data['nama_unit'] = $nama_unit;
	
	if ($keywordcari == "all")
		$data['cari'] = '';
	else
		$data['cari'] = $keywordcari;
	$data['startnya'] = $config['cur_page'];	
	$this->load->view('wip/vpopupbrgjadistokawalunit',$data);
  }
  
  // 12-07-2013
  function editstokawalunit(){ // 
// =======================
	// disini coding utk pengecekan user login
//========================
	$is_logged_in = $this->session->userdata('is_logged_in');
		if (!isset($is_logged_in) || $is_logged_in!= true) {
			//$this->load->view('loginform', $data);
			redirect('loginform');
		}
	
	$is_simpan 	= $this->input->post('is_simpan', TRUE);
	
	if ($is_simpan == '') {
		$id_stok 	= $this->uri->segment(4);
		$cur_page 	= $this->uri->segment(5);
		$carinya 	= $this->uri->segment(6);
		
		$query3	= $this->db->query(" SELECT a.kode_unit, a.kode_brg_jadi, a.stok, b.e_product_motifname 
								FROM tm_stok_unit_jahit a, tr_product_motif b 
								WHERE a.kode_brg_jadi = b.i_product_motif AND a.id = '$id_stok' ");
		if ($query3->num_rows() > 0){
			$hasilrow = $query3->row();
			$kode_unit = $hasilrow->kode_unit;
			$kode_brg_jadi = $hasilrow->kode_brg_jadi;
			$nama_brg_jadi = $hasilrow->e_product_motifname;
			$stok = $hasilrow->stok;
						
			// ambil data nama unit
			$query3	= $this->db->query(" SELECT nama FROM tm_gudang WHERE kode_unit = '$kode_unit' ");
			$hasilrow = $query3->row();
			$kode_unit	= $hasilrow->kode_unit;
			$nama_unit	= $hasilrow->nama_unit;
		}
		
		$data['msg'] = '';
		$data['id_stok'] = $id_stok;
		$data['cur_page'] = $cur_page;
		if ($carinya == '')
			$data['carinya'] = "all";
		else
			$data['carinya'] = $carinya;
		$data['unit_jahit'] = $kode_unit." - ".$nama_unit;
		$data['brg_jadi'] = $kode_brg_jadi." - ".$nama_brg_jadi;
		$data['stok'] = $stok;

		$data['isi'] = 'wip/veditstokawalunit';
		$this->load->view('template',$data);
	}
	else { // simpan
		$id_stok 	= $this->input->post('id_stok', TRUE);
		$cur_page 	= $this->input->post('cur_page', TRUE);  
		$carinya 	= $this->input->post('carinya', TRUE);  
		$stok = $this->input->post('stok', TRUE);
		$tgl = date("Y-m-d");
		
		// update stoknya
		$this->db->query(" UPDATE tm_stok_unit_jahit SET stok = '$stok', tgl_update_stok = '$tgl'
							WHERE id= '$id_stok' ");
		
		if ($carinya == '') $carinya = "all";
		$url_redirectnya = "wip/cform/viewstokawalunit/".$carinya."/".$cur_page;
			
		redirect($url_redirectnya);
	}
  }
  
  function additemwarnastokawal(){
		$kode_brg_jadi 	= $this->input->post('kode_brg_jadi', TRUE);
		$posisi 	= $this->input->post('posisi', TRUE);
		$jenis 	= $this->input->post('jenis', TRUE);
				
		// query ambil data2 warna berdasarkan kode brgnya
		$queryxx = $this->db->query(" SELECT a.id, a.kode_warna, b.nama FROM tm_warna_brg_jadi a, tm_warna b
									WHERE a.kode_warna = b.kode AND a.kode_brg_jadi = '".$kode_brg_jadi."' ORDER BY b.nama");
		if ($queryxx->num_rows() > 0){
			$hasilxx = $queryxx->result();
			foreach ($hasilxx as $rowxx) {
				$detailwarna[] = array(	'id_warna_brg_jadi'=> $rowxx->id,
										'kode_warna'=> $rowxx->kode_warna,
										'nama_warna'=> $rowxx->nama
									);
			}
		}
		else
			$detailwarna = '';
		
		$data['detailwarna'] = $detailwarna;
		$data['kode_brg_jadi'] = $kode_brg_jadi;
		$data['posisi'] = $posisi;
		if ($jenis==1)
			$this->load->view('wip/vlistwarnastokawal', $data); 
		else
			$this->load->view('wip/vlistwarnastokawal2', $data); 
		return true;
  }
  
}
